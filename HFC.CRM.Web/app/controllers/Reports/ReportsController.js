﻿'use strict';

app.controller('ReportsController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
    'HFCService', 'NavbarService', 'ReportsService', 'kendoService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http,
        HFCService, NavbarService, ReportsService, kendoService) {
        $scope.ReportFilter = {
            VendorID: '',
            IndustryId: '',
            CommercialTypeId: '',
            CustomerName: '',
            StartDate: null,
            EndDate: null,
            Year: null,
            Month: null,
            PersonID: null,
            SourceId: null,
            SourceList: [],
            campaignId: null,
            Zip: '',
            FP_ReportType: '',
            ST_ReportType: '',
            ProductCategoryId: '',
            TerritoryId: null,
            Value: null,
            FromYear: null,
            FromMonth: null,
            ToYear: null,
            ToMonth: null,
        }
        var offsetvalue = 0;
        HFCService.setHeaderTitle("Reports #");

        $scope.BrandId = HFCService.CurrentBrand;
        if ($scope.BrandId == 1) {
            $scope.FranchisePerformanceReportType = ["Sales Person", "Territory", "Franchise", "Zip", "Vendor", "Product Category"];
        }
        else {
            $scope.FranchisePerformanceReportType = ["Sales Person", "Territory", "Franchise", "Zip", "Product Category"];
        }


        $scope.SalesTaxReportType = ["Zip", "City", "County", "State", "Special", "Tax Code"];


        $scope.selectedDateText = "Select";

        $scope.HFCService = HFCService;
        $scope.ReportsService = ReportsService;
        $scope.NavbarService = NavbarService;

        $scope.Permission = {};
        var Reportspermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Reports')

        $scope.Permission.Repots = Reportspermission.CanRead; //HFCService.GetPermissionTP('TBD-Reports').CanAccess;

        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;

        $scope.NavbarService.SelectReports();

        $scope.NavbarService.EnableReportsSalesAgentTab();

        //flag for date validation
        $scope.ValidDate = true;
        $scope.DateRange = false;
        $scope.ValidDatelist = [];

        //hiding raw data in ipad
        $scope.ShowRawdata = true;
        if (/iPad|iPhone/.test(navigator.userAgent)) {
            $scope.ShowRawdata = false;
        }

        //if (window.location.href.includes("SalesandPurchasingDetailReport"))
        //    $scope.NavbarService.EnableReportsSalesAgentTab();

        //if (window.location.href.includes("FranchisePerformanceReport"))
        //    $scope.NavbarService.EnableReportsFranchisePerformanceTab();

        //------------ Datetype change -------------------------------------//
        $scope.setDateRange = function (date) {
            //for date validation
            for (i = 0; i < $scope.ValidDatelist.length; i++) {
                if (!$scope.ValidDatelist[i].valid) {
                    $scope.ValidDatelist[i].valid = true;
                    $("#" + $scope.ValidDatelist[i].id + "").removeClass("invalid_Date");
                    var a = $('#' + $scope.ValidDatelist[i].id + '').siblings();
                    $(a[0]).removeClass("icon_Background");
                    $("#" + $scope.ValidDatelist[i].id + "").addClass("frm_controllead1");
                }
            }
            $scope.DateRange = false;
            var dates = ReportsService.SelectDateRange(date);
            //$scope.ReportFilter.StartDate = dates.StartDate;
            //$scope.ReportFilter.EndDate = dates.EndDate;
            $scope.selectedDateText = date;
            // $scope.ReportFilter.StartDate = kendo.toString(kendo.parseDate(dates.StartDate[0]), 'MM/dd/yyyy');
            // $scope.ReportFilter.EndDate = kendo.toString(kendo.parseDate(dates.EndDate[0]), 'MM/dd/yyyy');
            $scope.ReportFilter.StartDate = HFCService.KendoDateFormat(dates.StartDate[0]);
            $scope.ReportFilter.EndDate = HFCService.KendoDateFormat(dates.EndDate[0]);
        }

        $scope.openDropdown = function (e) {
            e.stopPropagation();
        }

        /// Refresh Date Range
        $scope.RefreshDateRange = function () {
            if ($scope.ReportFilter.StartDate == "Invalid date") {
                $scope.ReportFilter.StartDate = null;
            } if ($scope.ReportFilter.EndDate == "Invalid date") {
                $scope.ReportFilter.EndDate = null;
            }


            if ($scope.ReportFilter.StartDate && $scope.ReportFilter.EndDate) {
                $scope.selectedDateText = ($scope.ReportFilter.StartDate) + ' to ' + ($scope.ReportFilter.EndDate)
            } else {
                $scope.selectedDateText = "";
            }

        }

        // Format Date yyyy-MM-DD
        function formatDate(date) {
            if (date == null)
                return "";

            var d = new XDate(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [month, day, year].join('/');

            //return [year, month, day].join('/');
        }

        // Format Phone Number
        function maskPhoneNumber(text) {
            if (text != null && text !== undefined) {
                text = text.toString();
                return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
            } else {
                return '';
            }
        }

        function maskedDisplayPhone(phone) {
            var Div;
            if (phone) {
                Div = "<span>";
                if (phone && phone != "")
                    Div += maskPhoneNumber(phone)
                Div += "</span>";
            } else {
                Div = "";
            }


            return Div;
        }
        //------------ Datetype change -------------------------------------//

        //----------------Local Variable Declaration ---------------------------//

        var exportFlag = false;

        //----------------Local Variable Declaration ---------------------------//
        //------------Sales and Purchasing Details
        $scope.SalesandPurchasingDetailReport = function (data) {
            $scope.SalesandPurchasingDetailReport_options = {
                cache: false,
                excel: {
                    fileName: "SalesandPurchasingDetailReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                //height: 650,
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getSalesandPurchasingDetail',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    VendorID: $scope.ReportFilter.VendorID,
                                    IndustryId: $scope.ReportFilter.IndustryId,
                                    CommercialTypeId: $scope.ReportFilter.CommercialTypeId,
                                    CustomerName: $scope.ReportFilter.CustomerName,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                TerritoryName: { type: "string" },
                                Salesperson: { type: "string" },
                                CustName: { type: "string" },
                                CustOrder: { type: "number" },
                                MPO: { type: "number" },
                                VPO: { type: "number" },
                                MPOLine: { type: "number" },
                                VPOLine: { type: "number" },
                                LineNumber: { type: "number" },
                                OrderLine: { type: "number" },
                                VendorName: { type: "string" },
                                ProductCategory: { type: "string" },
                                ProductModel: { type: "string" },
                                WindowLocation: { type: "string" },
                                Description: { type: "string" },
                                LineCost: { type: "number" },
                                LineValue: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "number" },
                                CustOrderDate: { type: "date" },
                                MPODate: { type: "date" },
                                VPODate: { type: "date" },
                                PromiseByDate: { type: "date" },
                                EstShipDate: { type: "date" },
                                actualshipdate: { type: "date" },
                                InvoiceDate: { type: "date" },
                                VPOStatus: { type: "string" },
                                Errors: { type: "string" }
                            }
                        }
                    },
                    aggregate: [{ field: "LineCost", aggregate: "sum" },
                    { field: "LineValue", aggregate: "sum" },
                    { field: "GPValue", aggregate: "sum" },
                    { field: "GPPercenatge", aggregate: "average" }],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                excelExport: function (e) {
                    var columns = e.sender.columns;
                    if (!exportFlag) {
                        for (var i = 0; i < columns.length; i++) {
                            if (columns[i].hidden == true)
                                e.sender.showColumn(i);
                        }
                        e.preventDefault();
                        exportFlag = true;
                        setTimeout(function () {
                            e.sender.saveAsExcel();
                        });
                    } else {
                        for (var i = 0; i < columns.length; i++) {
                            if (columns[i].hidden == false)
                                e.sender.hideColumn(i);
                        }
                        exportFlag = false;
                    }

                    var rows = e.workbook.sheets[0].rows;

                    for (var ri = 0; ri < rows.length; ri++) {
                        var row = rows[ri];

                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];

                            if (row.type === "header") {
                                if (rows[0].cells[cellIndex].value == "TerritoryName") {
                                    cell.value = "Territory Name";
                                }
                                if (rows[0].cells[cellIndex].value == "Salesperson") {
                                    cell.value = "Sales Person";
                                }
                                if (rows[0].cells[cellIndex].value == "CustName") {
                                    cell.value = "Cust Name";
                                }
                                if (rows[0].cells[cellIndex].value == "CustOrder") {
                                    cell.value = "Cust Order #";
                                }
                                if (rows[0].cells[cellIndex].value == "LineNumber") {
                                    cell.value = "Line #";
                                }
                                if (rows[0].cells[cellIndex].value == "VendorName") {
                                    cell.value = "Vendor Name";
                                }
                                if (rows[0].cells[cellIndex].value == "ProductCategory") {
                                    cell.value = "Product Category";
                                }
                                //if (rows[0].cells[cellIndex].value == "LineNumber") {
                                //    cell.value = "Line #";
                                //}
                                if (rows[0].cells[cellIndex].value == "WindowLocation") {
                                    cell.value = "Window Location";
                                }
                                if (rows[0].cells[cellIndex].value == "LineCost") {
                                    cell.value = "Line Cost";
                                }
                                if (rows[0].cells[cellIndex].value == "LineValue") {
                                    cell.value = "Line Value";
                                }
                                if (rows[0].cells[cellIndex].value == "GPValue") {
                                    cell.value = "GP $";
                                }
                                if (rows[0].cells[cellIndex].value == "GPPercenatge") {
                                    cell.value = "GP %";
                                }
                                if (rows[0].cells[cellIndex].value == "CustOrderDate") {
                                    cell.value = "Cust Order Date";
                                }
                                if (rows[0].cells[cellIndex].value == "MPODate") {
                                    cell.value = "MPO Date";
                                }
                                if (rows[0].cells[cellIndex].value == "VPODate") {
                                    cell.value = "VPO Date";
                                }
                                if (rows[0].cells[cellIndex].value == "PromiseByDate") {
                                    cell.value = "Promise By Date";
                                }
                                if (rows[0].cells[cellIndex].value == "EstShipDate") {
                                    cell.value = "Est Ship Date";
                                }
                                if (rows[0].cells[cellIndex].value == "actualshipdate") {
                                    cell.value = "actual ship date";
                                }
                                if (rows[0].cells[cellIndex].value == "InvoiceDate") {
                                    cell.value = "Invoice Date";
                                }
                                if (rows[0].cells[cellIndex].value == "VPOStatus") {
                                    cell.value = "VPO Status";
                                }
                            }

                            if (row.type === "data") {
                                if (rows[0].cells[cellIndex].value == "Line Cost" || rows[0].cells[cellIndex].value == "Line Value" || rows[0].cells[cellIndex].value == "GP $") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (rows[0].cells[cellIndex].value == "GP %") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }

                        if (row.type == "group-footer" || row.type == "footer") {
                            for (var ci = 0; ci < row.cells.length; ci++) {
                                var cell = row.cells[ci];
                                if (cell.value) {
                                    if (cell.value.contains('div')) {
                                        var classname = $(cell.value).attr('class')                                                                               
                                        cell.value = $("#gridSalesAndPurchaseSearch ." + classname + "").text();// $(cell.value).text();                                        
                                        // Set the alignment
                                        cell.hAlign = "right";
                                    }
                                }
                            }
                        }
                    }
                },
                dataBound: function (e) {
                    var dataSource = $("#gridSalesAndPurchaseSearch").data("kendoGrid").dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: dataSource.data(),
                        filter: dataSource.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();

                    if (data.length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    }
                    else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    //var myDiv = $('.k-grid-content.k-auto-scrollable');
                    //if (myDiv[0]) {
                    //    myDiv[0].scrollLeft = 0;
                    //}
                    var CustOrderWrapper = e.sender.element.find(".CustOrderWrapper");
                    var MPOWrapper = e.sender.element.find(".MPOWrapper");
                    var VPOWrapper = e.sender.element.find(".VPOWrapper");

                    var CustOrdersummary = {};
                    var MPOsummary = {};
                    var VPOsummary = {};

                    var CustOrderCount = 0;
                    var MPOCount = 0;
                    var VPOCount = 0;

                    for (var i = 0; i < data.length; i++) {
                        if (!CustOrdersummary[data[i].CustOrder]) {
                            CustOrdersummary[data[i].CustOrder] = 1;
                            CustOrderCount++;
                        }
                        if (!MPOsummary[data[i].MPO]) {
                            MPOsummary[data[i].MPO] = 1;
                            MPOCount++;
                        }
                        if (!VPOsummary[data[i].VPO]) {
                            VPOsummary[data[i].VPO] = 1;
                            if (data[i].VPO > 0)
                                VPOCount++;
                        }
                    }

                    //var items = e.sender.items();
                    //items.each(function () {
                    //    var dataItem = e.sender.dataItem(this);
                    //    if (!CustOrdersummary[dataItem.CustOrder]) {
                    //        CustOrdersummary[dataItem.CustOrder] = 1;
                    //        CustOrderCount++;
                    //    }
                    //})

                    CustOrderWrapper.html(CustOrderCount);
                    MPOWrapper.html(MPOCount);
                    VPOWrapper.html(VPOCount);

                    //$("#gridSalesAndPurchaseSearch").find(".k-grid-footer").insertAfter($("#gridSalesAndPurchaseSearch .k-grid-header"))
                    if ($("#gridSalesAndPurchaseSearch").find(".k-grid-header").next()[0] != $("#gridSalesAndPurchaseSearch").find(".k-grid-footer")[0])
                        $("#gridSalesAndPurchaseSearch").find(".k-grid-footer").insertAfter($("#gridSalesAndPurchaseSearch .k-grid-header"));
                },
                columns: [
                    {
                        field: "TerritoryName",
                        //title: "Territory Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Territory Name" class="report_tooltip">Territory Name</label>',
                        filterable: { search: true },
                        width: "200px"
                    },
                    {
                        field: "Salesperson",
                        //title: "Sales Person",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Sales Person" class="report_tooltip" >Sales Person</label>',
                        filterable: { search: true },
                        width: "120px"
                    },
                    {
                        field: "CustName",
                        //title: "Cust Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Cust Name" class="report_tooltip" >Cust Name</label>',
                        filterable: { search: true },
                        width: "120px"
                    },
                    {
                        field: "CustOrder",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Count of Orders" class="report_tooltip" >Cust Order #</label>',
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.CustOrder + "</span>";
                        },
                        filterable: { search: true },
                        width: "120px",
                        footerTemplate: "<div class='CustOrderWrapper'></div>"
                    },
                    {
                        field: "MPO",
                        //title: "MPO",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of MPOs" class="report_tooltip" >MPO</label>',
                        template: function (dataItem) {
                            if (dataItem.MPO != null && dataItem.MPO != '' && dataItem.MPO != 0)
                                return "<span class='Grid_Textalign'>" + dataItem.MPO + "</span>";
                            else return "";
                        },
                        filterable: { search: true },
                        width: "100px",
                        footerTemplate: "<div class='MPOWrapper'></div>"
                    },
                    {
                        field: "VPO",
                        //title: "VPO",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of VPOs" class="report_tooltip" >VPO</label>',
                        template: function (dataItem) {
                            if (dataItem.VPO != null && dataItem.VPO != '' && dataItem.VPO != 0)
                                return "<span class='Grid_Textalign'>" + dataItem.VPO + "</span>";
                            else return "";
                        },
                        filterable: { search: true },
                        width: "100px",
                        footerTemplate: "<div class='VPOWrapper'></div>"
                    },
                    {
                        field: "LineNumber",
                        //title: "Line #",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Line #" class="report_tooltip" >Line #</label>',
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.LineNumber + "</span>";
                        },
                        filterable: { search: true },
                        width: "100px"
                    },
                    {
                        field: "VendorName",
                        //title: "Vendor Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Territory Name" class="report_tooltip" >Vendor Name</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },
                    {
                        field: "ProductCategory",
                        //title: "Product Category",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Product Category" class="report_tooltip" >Product Category</label>',
                        filterable: { search: true },
                        width: "120px"
                    },
                    //{
                    //    field: "ProductModel",
                    //    title: "Product Model",
                    //    filterable: { multi: true, search: true },
                    //    width: "120px",
                    //    hidden: true
                    //},
                    {
                        field: "WindowLocation",
                        //title: "Window Location",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Window Location" class="report_tooltip" >Window Location</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px",
                    },
                    {
                        field: "Description",
                        //title: "Description",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Description" class="report_tooltip" >Description</label>',
                        filterable: { search: true },
                        width: "600px",
                        hidden: true
                    },
                    {
                        field: "LineCost",
                        //title: "Line Cost",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Line Cost" class="report_tooltip" >Line Cost</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.LineCost) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "LineValue",
                        //title: "Line Value",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Line Value" class="report_tooltip" >Line Value</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.LineValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPValue",
                        //title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit [Line Value-Line Cost]" class="report_tooltip" >GP $</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPPercenatge",
                        //title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit % [Gross Profit/Line Value]" class="report_tooltip" >GP %</label>',
                        filterable: { search: true },
                        aggregates: ["average"],
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p2}", GPPercenatge / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridSalesAndPurchaseSearch").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.LineValue.sum > 0) {
                                var gpPer = (aggregates.GPValue.sum / aggregates.LineValue.sum) * 100;
                                return gpPer.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "150px"
                    },
                    {
                        field: "CustOrderDate",
                        //title: "Cust Order Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cust Order Date" class="report_tooltip" >Cust Order Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.CustOrderDate) {
                                return HFCService.KendoDateFormat(dataItem.CustOrderDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseCustOrderFilterCalendar", offsetvalue),
                    },
                    {
                        field: "MPODate",
                        //title: "MPO Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="MPO Date" class="report_tooltip" >MPO Date</label>',
                        // filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.MPODate) {
                                return HFCService.KendoDateFormat(dataItem.MPODate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseMPODateFilterCalendar", offsetvalue),
                    },
                    {
                        field: "VPODate",
                        //title: "VPO Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="VPO Date" class="report_tooltip" >VPO Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.VPODate) {
                                return HFCService.KendoDateFormat(dataItem.VPODate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseVPODateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "PromiseByDate",
                        //title: "Promise By Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Promise By Date" class="report_tooltip" >Promise By Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.PromiseByDate) {
                                return HFCService.KendoDateFormat(dataItem.PromiseByDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchasePromiseByDateFilterCalendar", offsetvalue),
                        //  format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "EstShipDate",
                        //title: "Est Ship Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Est Ship Date" class="report_tooltip" >Est Ship Date</label>',
                        // filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.EstShipDate) {
                                return HFCService.KendoDateFormat(dataItem.EstShipDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseEstShipDateFilterCalendar", offsetvalue),
                        //  format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "actualshipdate",
                        //title: "actual ship date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="actual ship date" class="report_tooltip" >actual ship date</label>',
                        // filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.actualshipdate) {
                                return HFCService.KendoDateFormat(dataItem.actualshipdate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseactualshipdateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "InvoiceDate",
                        //title: "Invoice Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Invoice Date" class="report_tooltip" >Invoice Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.InvoiceDate) {
                                return HFCService.KendoDateFormat(dataItem.InvoiceDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseInvoiceDateFilterCalendar", offsetvalue),
                        //  format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "VPOStatus",
                        //title: "VPO Status",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="VPO Status" class="report_tooltip" >VPO Status</label>',
                        filterable: { multi: true, search: true },
                        width: "120px"
                    },
                    {
                        field: "Errors",
                        //title: "Errors",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Errors" class="report_tooltip" >Errors</label>',
                        filterable: { search: true },
                        width: "200px"
                    },
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    //$(e.container).css("max-height", "263px");
                    //$(e.container).css("overflow", "scroll");
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                },
                sortable: ({ field: "CustOrderDate", dir: "desc" }),
                groupable: true,
                scrollable: true,
                resizable: true
            };
        };
        //---2.TL
        $scope.TLSalesandPurchasingDetailReport = function (data) {
            $scope.SalesandPurchasingDetailReport_options = {
                cache: false,
                excel: {
                    fileName: "SalesandPurchasingDetailReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                //height: 650,
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getSalesandPurchasingDetail',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    VendorID: $scope.ReportFilter.VendorID,
                                    IndustryId: $scope.ReportFilter.IndustryId,
                                    CommercialTypeId: $scope.ReportFilter.CommercialTypeId,
                                    CustomerName: $scope.ReportFilter.CustomerName,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                TerritoryName: { type: "string" },
                                Salesperson: { type: "string" },
                                CustName: { type: "string" },
                                CustOrder: { type: "number" },
                                MPO: { type: "number" },
                                VPO: { type: "number" },
                                MPOLine: { type: "number" },
                                VPOLine: { type: "number" },
                                LineNumber: { type: "number" },
                                OrderLine: { type: "number" },
                                VendorName: { type: "string" },
                                ProductCategory: { type: "string" },
                                ProductModel: { type: "string" },
                                ProductName: { type: "string" },
                                Description: { type: "string" },
                                LineCost: { type: "number" },
                                LineValue: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "string" },
                                CustOrderDate: { type: "date" },
                                MPODate: { type: "date" },
                                VPODate: { type: "date" },
                                PromiseByDate: { type: "date" },
                                EstShipDate: { type: "date" },
                                actualshipdate: { type: "date" },
                                InvoiceDate: { type: "date" },
                                VPOStatus: { type: "string" },
                                Errors: { type: "string" }
                            }
                        }
                    },
                    aggregate: [{ field: "LineCost", aggregate: "sum" },
                    { field: "LineValue", aggregate: "sum" },
                    { field: "GPValue", aggregate: "sum" },
                    { field: "GPPercenatge", aggregate: "average" }],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                excelExport: function (e) {
                    var columns = e.sender.columns;
                    if (!exportFlag) {
                        for (var i = 0; i < columns.length; i++) {
                            if (columns[i].hidden == true)
                                e.sender.showColumn(i);
                        }
                        e.preventDefault();
                        exportFlag = true;
                        setTimeout(function () {
                            e.sender.saveAsExcel();
                        });
                    } else {
                        for (var i = 0; i < columns.length; i++) {
                            if (columns[i].hidden == false)
                                e.sender.hideColumn(i);
                        }
                        exportFlag = false;
                    }

                    var rows = e.workbook.sheets[0].rows;

                    for (var ri = 0; ri < rows.length; ri++) {
                        var row = rows[ri];


                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];

                            if (row.type === "header") {
                                if (rows[0].cells[cellIndex].value == "TerritoryName") {
                                    cell.value = "Territory Name";
                                }
                                if (rows[0].cells[cellIndex].value == "Salesperson") {
                                    cell.value = "Sales Person";
                                }
                                if (rows[0].cells[cellIndex].value == "CustName") {
                                    cell.value = "Cust Name";
                                }
                                if (rows[0].cells[cellIndex].value == "CustOrder") {
                                    cell.value = "Cust Order #";
                                }
                                if (rows[0].cells[cellIndex].value == "LineNumber") {
                                    cell.value = "Line #";
                                }
                                if (rows[0].cells[cellIndex].value == "VendorName") {
                                    cell.value = "Vendor Name";
                                }
                                if (rows[0].cells[cellIndex].value == "ProductCategory") {
                                    cell.value = "Product Category";
                                }
                                if (rows[0].cells[cellIndex].value == "LineNumber") {
                                    cell.value = "Line #";
                                }
                                if (rows[0].cells[cellIndex].value == "ProductName") {
                                    cell.value = "Product Name";
                                }
                                if (rows[0].cells[cellIndex].value == "LineCost") {
                                    cell.value = "Line Cost";
                                }
                                if (rows[0].cells[cellIndex].value == "LineValue") {
                                    cell.value = "Line Value";
                                }
                                if (rows[0].cells[cellIndex].value == "GPValue") {
                                    cell.value = "GP $";
                                }
                                if (rows[0].cells[cellIndex].value == "GPPercenatge") {
                                    cell.value = "GP %";
                                }
                                if (rows[0].cells[cellIndex].value == "CustOrderDate") {
                                    cell.value = "Cust Order Date";
                                }
                                if (rows[0].cells[cellIndex].value == "MPODate") {
                                    cell.value = "MPO Date";
                                }
                                if (rows[0].cells[cellIndex].value == "VPODate") {
                                    cell.value = "VPO Date";
                                }
                                if (rows[0].cells[cellIndex].value == "PromiseByDate") {
                                    cell.value = "Promise By Date";
                                }
                                if (rows[0].cells[cellIndex].value == "EstShipDate") {
                                    cell.value = "Est Ship Date";
                                }
                                if (rows[0].cells[cellIndex].value == "actualshipdate") {
                                    cell.value = "actual ship date";
                                }
                                if (rows[0].cells[cellIndex].value == "InvoiceDate") {
                                    cell.value = "Invoice Date";
                                }
                                if (rows[0].cells[cellIndex].value == "VPOStatus") {
                                    cell.value = "VPO Status";
                                }

                            }

                            if (row.type === "data") {
                                if (rows[0].cells[cellIndex].value == "Line Cost" || rows[0].cells[cellIndex].value == "Line Value" || rows[0].cells[cellIndex].value == "GP $") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (rows[0].cells[cellIndex].value == "GP %") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }

                        if (row.type == "group-footer" || row.type == "footer") {
                            for (var ci = 0; ci < row.cells.length; ci++) {
                                var cell = row.cells[ci];
                                if (cell.value) {
                                    if (cell.value.contains('div')) {
                                        var classname = $(cell.value).attr('class')
                                        cell.value = $("#gridSalesAndPurchaseSearch ." + classname + "").text();// $(cell.value).text();
                                        // Set the alignment
                                        cell.hAlign = "right";
                                    }

                                }
                            }
                        }
                    }
                },
                dataBound: function (e) {
                    var dataSource = $("#gridSalesAndPurchaseSearch").data("kendoGrid").dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: dataSource.data(),
                        filter: dataSource.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();

                    if (data.length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    var CustOrderWrapper = e.sender.element.find(".CustOrderWrapper");
                    var MPOWrapper = e.sender.element.find(".MPOWrapper");
                    var VPOWrapper = e.sender.element.find(".VPOWrapper");

                    var CustOrdersummary = {};
                    var MPOsummary = {};
                    var VPOsummary = {};

                    var CustOrderCount = 0;
                    var MPOCount = 0;
                    var VPOCount = 0;

                    for (var i = 0; i < data.length; i++) {
                        if (!CustOrdersummary[data[i].CustOrder]) {
                            CustOrdersummary[data[i].CustOrder] = 1;
                            CustOrderCount++;
                        }
                        if (!MPOsummary[data[i].MPO]) {
                            MPOsummary[data[i].MPO] = 1;
                            MPOCount++;
                        }
                        if (!VPOsummary[data[i].VPO]) {
                            VPOsummary[data[i].VPO] = 1;
                            if (data[i].VPO > 0)
                                VPOCount++;
                        }
                    }


                    //var items = e.sender.items();
                    //items.each(function () {
                    //    var dataItem = e.sender.dataItem(this);
                    //    if (!CustOrdersummary[dataItem.CustOrder]) {
                    //        CustOrdersummary[dataItem.CustOrder] = 1;
                    //        CustOrderCount++;
                    //    }
                    //})

                    CustOrderWrapper.html(CustOrderCount);
                    MPOWrapper.html(MPOCount);
                    VPOWrapper.html(VPOCount);
                    //$("#gridSalesAndPurchaseSearch").find(".k-grid-footer").insertAfter($("#gridSalesAndPurchaseSearch .k-grid-header"))
                    if ($("#gridSalesAndPurchaseSearch").find(".k-grid-header").next()[0] != $("#gridSalesAndPurchaseSearch").find(".k-grid-footer")[0])
                        $("#gridSalesAndPurchaseSearch").find(".k-grid-footer").insertAfter($("#gridSalesAndPurchaseSearch .k-grid-header"));
                },
                columns: [
                    {
                        field: "TerritoryName",
                        //title: "Territory Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Territory Name" class="report_tooltip">Territory Name</label>',
                        filterable: { multi: true, search: true },
                        width: "120px"
                    },
                    {
                        field: "Salesperson",
                        //title: "Sales Person",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Sales Person" class="report_tooltip" >Sales Person</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },
                    {
                        field: "CustName",
                        //title: "Cust Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Cust Name" class="report_tooltip" >Cust Name</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },
                    {
                        field: "CustOrder",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Count of Orders" class="report_tooltip" >Cust Order #</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        template: function (dataItem) {
                            if (dataItem.CustOrder)
                                return "<span class='Grid_Textalign'>" + dataItem.CustOrder + "</span>";
                            else
                                return "";
                        },
                        width: "120px",
                        footerTemplate: "<div class='CustOrderWrapper'></div>"
                    },
                    {
                        field: "MPO",
                        //title: "MPO",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of MPOs" class="report_tooltip" >MPO</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        template: function (dataItem) {
                            if (dataItem.MPO)
                                return "<span class='Grid_Textalign'>" + dataItem.MPO + "</span>";
                            else
                                return "";
                        },
                        width: "100px",
                        footerTemplate: "<div class='MPOWrapper'></div>"
                    },
                    {
                        field: "VPO",
                        //title: "VPO",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of VPOs" class="report_tooltip" >VPO</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        template: function (dataItem) {
                            if (dataItem.VPO)
                                return "<span class='Grid_Textalign'>" + dataItem.VPO + "</span>";
                            else
                                return "";
                        },
                        width: "100px",
                        footerTemplate: "<div class='VPOWrapper'></div>"
                    },
                    {
                        field: "LineNumber",
                        //title: "Line #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Line #" class="report_tooltip" >Line #</label>',
                        template: function (dataItem) {
                            if (dataItem.LineNumber)
                                return "<span class='Grid_Textalign'>" + dataItem.LineNumber + "</span>";
                            else
                                return "";
                        },
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "100px"
                    },
                    {
                        field: "VendorName",
                        //title: "Vendor Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Territory Name" class="report_tooltip" >Vendor Name</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },
                    {
                        field: "ProductCategory",
                        //title: "Product Category",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Product Category" class="report_tooltip" >Product Category</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },

                    {
                        field: "ProductName",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Product Name" class="report_tooltip" >Product Name</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px",

                    },
                    {
                        field: "Description",
                        //title: "Description",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Description" class="report_tooltip" >Description</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "600px",
                        hidden: true
                    },
                    {
                        field: "LineCost",
                        //title: "Line Cost",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Line Cost" class="report_tooltip" >Line Cost</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.LineCost) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "100px"
                    },
                    {
                        field: "LineValue",
                        //title: "Line Value",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Line Value" class="report_tooltip" >Line Value</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.LineValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "100px"

                    },
                    {
                        field: "GPValue",
                        //title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit [Line Value-Line Cost]" class="report_tooltip" >GP $</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "100px"
                    },
                    {
                        field: "GPPercenatge",
                        //title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit % [Gross Profit/Line Value]" class="report_tooltip" >GP %</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        aggregates: ["average"],
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p2}", GPPercenatge / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridSalesAndPurchaseSearch").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.LineValue.sum > 0) {
                                var gpPer = (aggregates.GPValue.sum / aggregates.LineValue.sum) * 100;
                                return  gpPer.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }

                        },
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "100px"
                    },
                    {
                        field: "CustOrderDate",
                        //title: "Cust Order Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cust Order Date" class="report_tooltip" >Cust Order Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.CustOrderDate) {
                                return HFCService.KendoDateFormat(dataItem.CustOrderDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseTlCustOrderDateFilterCalendar", offsetvalue),
                        //format: "{0:MM/dd/yyyy}"
                    }, {
                        field: "MPODate",
                        //title: "MPO Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="MPO Date" class="report_tooltip" >MPO Date</label>',
                        // filterable: { multi: true, search: true },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseTlMPODateFilterCalendar", offsetvalue),
                        //  format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "VPODate",
                        //title: "VPO Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="VPO Date" class="report_tooltip" >VPO Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.VPODate) {
                                return HFCService.KendoDateFormat(dataItem.VPODate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseTlVPODateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "PromiseByDate",
                        //title: "Promise By Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Promise By Date" class="report_tooltip" >Promise By Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.PromiseByDate) {
                                return HFCService.KendoDateFormat(dataItem.PromiseByDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseTlPromiseByDateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "EstShipDate",
                        //title: "Est Ship Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Est Ship Date" class="report_tooltip" >Est Ship Date</label>',
                        // filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.EstShipDate) {
                                return HFCService.KendoDateFormat(dataItem.EstShipDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseTlEstShipDateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "actualshipdate",
                        //title: "actual ship date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="actual ship date" class="report_tooltip" >actual ship date</label>',
                        // filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.actualshipdate) {
                                return HFCService.KendoDateFormat(dataItem.actualshipdate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseTlactualshipdateFilterCalendar", offsetvalue),
                        //  format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "InvoiceDate",
                        //title: "Invoice Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Invoice Date" class="report_tooltip" >Invoice Date</label>',
                        // filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.InvoiceDate) {
                                return HFCService.KendoDateFormat(dataItem.InvoiceDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseTlInvoiceDateFilterCalendar", offsetvalue),
                        //  format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "VPOStatus",
                        //title: "VPO Status",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="VPO Status" class="report_tooltip" >VPO Status</label>',
                        filterable: { multi: true, search: true },
                        width: "120px"
                    },
                    {
                        field: "Errors",
                        //title: "Errors",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Errors" class="report_tooltip" >Errors</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "200px"
                    },
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },

                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                },
                sortable: ({ field: "CustOrderDate", dir: "desc" }),
                groupable: true,
                scrollable: true,
                resizable: true
            };
        };
        //---3.CC
        $scope.CCSalesandPurchasingDetailReport = function (data) {
            $scope.SalesandPurchasingDetailReport_options = {
                cache: false,
                excel: {
                    fileName: "SalesandPurchasingDetailReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                //height: 650,
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getSalesandPurchasingDetail',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    VendorID: $scope.ReportFilter.VendorID,
                                    IndustryId: $scope.ReportFilter.IndustryId,
                                    CommercialTypeId: $scope.ReportFilter.CommercialTypeId,
                                    CustomerName: $scope.ReportFilter.CustomerName,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                TerritoryName: { type: "string" },
                                Salesperson: { type: "string" },
                                CustName: { type: "string" },
                                CustOrder: { type: "number" },
                                MPO: { type: "number" },
                                VPO: { type: "number" },
                                MPOLine: { type: "number" },
                                VPOLine: { type: "number" },
                                LineNumber: { type: "number" },
                                OrderLine: { type: "number" },
                                VendorName: { type: "string" },
                                ProductCategory: { type: "string" },
                                ProductModel: { type: "string" },
                                ProductName: { type: "string" },
                                Description: { type: "string" },
                                LineCost: { type: "number" },
                                LineValue: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "string" },
                                CustOrderDate: { type: "date" },
                                MPODate: { type: "date" },
                                VPODate: { type: "date" },
                                PromiseByDate: { type: "date" },
                                EstShipDate: { type: "date" },
                                actualshipdate: { type: "date" },
                                InvoiceDate: { type: "date" },
                                VPOStatus: { type: "string" },
                                Errors: { type: "string" }
                            }
                        }
                    },
                    aggregate: [{ field: "LineCost", aggregate: "sum" },
                    { field: "LineValue", aggregate: "sum" },
                    { field: "GPValue", aggregate: "sum" },
                    { field: "GPPercenatge", aggregate: "average" }],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                excelExport: function (e) {
                    var columns = e.sender.columns;
                    if (!exportFlag) {
                        for (var i = 0; i < columns.length; i++) {
                            if (columns[i].hidden == true)
                                e.sender.showColumn(i);
                        }
                        e.preventDefault();
                        exportFlag = true;
                        setTimeout(function () {
                            e.sender.saveAsExcel();
                        });
                    } else {
                        for (var i = 0; i < columns.length; i++) {
                            if (columns[i].hidden == false)
                                e.sender.hideColumn(i);
                        }
                        exportFlag = false;
                    }

                    var rows = e.workbook.sheets[0].rows;

                    for (var ri = 0; ri < rows.length; ri++) {
                        var row = rows[ri];


                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];

                            if (row.type === "header") {
                                if (rows[0].cells[cellIndex].value == "TerritoryName") {
                                    cell.value = "Territory Name";
                                }
                                if (rows[0].cells[cellIndex].value == "Salesperson") {
                                    cell.value = "Sales Person";
                                }
                                if (rows[0].cells[cellIndex].value == "CustName") {
                                    cell.value = "Cust Name";
                                }
                                if (rows[0].cells[cellIndex].value == "CustOrder") {
                                    cell.value = "Cust Order #";
                                }
                                if (rows[0].cells[cellIndex].value == "LineNumber") {
                                    cell.value = "Line #";
                                }
                                if (rows[0].cells[cellIndex].value == "VendorName") {
                                    cell.value = "Vendor Name";
                                }
                                if (rows[0].cells[cellIndex].value == "ProductCategory") {
                                    cell.value = "Product Category";
                                }
                                if (rows[0].cells[cellIndex].value == "LineNumber") {
                                    cell.value = "Line #";
                                }
                                if (rows[0].cells[cellIndex].value == "ProductName") {
                                    cell.value = "Product Name";
                                }
                                if (rows[0].cells[cellIndex].value == "LineCost") {
                                    cell.value = "Line Cost";
                                }
                                if (rows[0].cells[cellIndex].value == "LineValue") {
                                    cell.value = "Line Value";
                                }
                                if (rows[0].cells[cellIndex].value == "GPValue") {
                                    cell.value = "GP $";
                                }
                                if (rows[0].cells[cellIndex].value == "GPPercenatge") {
                                    cell.value = "GP %";
                                }
                                if (rows[0].cells[cellIndex].value == "CustOrderDate") {
                                    cell.value = "Cust Order Date";
                                }
                                if (rows[0].cells[cellIndex].value == "MPODate") {
                                    cell.value = "MPO Date";
                                }
                                if (rows[0].cells[cellIndex].value == "VPODate") {
                                    cell.value = "VPO Date";
                                }
                                if (rows[0].cells[cellIndex].value == "PromiseByDate") {
                                    cell.value = "Promise By Date";
                                }
                                if (rows[0].cells[cellIndex].value == "EstShipDate") {
                                    cell.value = "Est Ship Date";
                                }
                                if (rows[0].cells[cellIndex].value == "actualshipdate") {
                                    cell.value = "actual ship date";
                                }
                                if (rows[0].cells[cellIndex].value == "InvoiceDate") {
                                    cell.value = "Invoice Date";
                                }
                                if (rows[0].cells[cellIndex].value == "VPOStatus") {
                                    cell.value = "VPO Status";
                                }

                            }

                            if (row.type === "data") {
                                if (rows[0].cells[cellIndex].value == "Line Cost" || rows[0].cells[cellIndex].value == "Line Value" || rows[0].cells[cellIndex].value == "GP $") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (rows[0].cells[cellIndex].value == "GP %") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }

                        if (row.type == "group-footer" || row.type == "footer") {
                            for (var ci = 0; ci < row.cells.length; ci++) {
                                var cell = row.cells[ci];
                                if (cell.value) {
                                    if (cell.value.contains('div')) {
                                        var classname = $(cell.value).attr('class')
                                        cell.value = $("#gridSalesAndPurchaseSearch ." + classname + "").text();// $(cell.value).text();
                                        // Set the alignment
                                        cell.hAlign = "right";
                                    }

                                }
                            }
                        }
                    }
                },
                dataBound: function (e) {
                    var dataSource = $("#gridSalesAndPurchaseSearch").data("kendoGrid").dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: dataSource.data(),
                        filter: dataSource.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();

                    if (data.length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    var CustOrderWrapper = e.sender.element.find(".CustOrderWrapper");
                    var MPOWrapper = e.sender.element.find(".MPOWrapper");
                    var VPOWrapper = e.sender.element.find(".VPOWrapper");

                    var CustOrdersummary = {};
                    var MPOsummary = {};
                    var VPOsummary = {};

                    var CustOrderCount = 0;
                    var MPOCount = 0;
                    var VPOCount = 0;

                    for (var i = 0; i < data.length; i++) {
                        if (!CustOrdersummary[data[i].CustOrder]) {
                            CustOrdersummary[data[i].CustOrder] = 1;
                            CustOrderCount++;
                        }
                        if (!MPOsummary[data[i].MPO]) {
                            MPOsummary[data[i].MPO] = 1;
                            MPOCount++;
                        }
                        if (!VPOsummary[data[i].VPO]) {
                            VPOsummary[data[i].VPO] = 1;
                            if (data[i].VPO > 0)
                                VPOCount++;
                        }
                    }


                    //var items = e.sender.items();
                    //items.each(function () {
                    //    var dataItem = e.sender.dataItem(this);
                    //    if (!CustOrdersummary[dataItem.CustOrder]) {
                    //        CustOrdersummary[dataItem.CustOrder] = 1;
                    //        CustOrderCount++;
                    //    }
                    //})

                    CustOrderWrapper.html(CustOrderCount);
                    MPOWrapper.html(MPOCount);
                    VPOWrapper.html(VPOCount);
                    //$("#gridSalesAndPurchaseSearch").find(".k-grid-footer").insertAfter($("#gridSalesAndPurchaseSearch .k-grid-header"))
                    if ($("#gridSalesAndPurchaseSearch").find(".k-grid-header").next()[0] != $("#gridSalesAndPurchaseSearch").find(".k-grid-footer")[0])
                        $("#gridSalesAndPurchaseSearch").find(".k-grid-footer").insertAfter($("#gridSalesAndPurchaseSearch .k-grid-header"));
                },
                columns: [
                    {
                        field: "TerritoryName",
                        //title: "Territory Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Territory Name" class="report_tooltip">Territory Name</label>',
                        filterable: {
                            multi: true,
                            search: true
                        },
                        width: "120px"
                    },
                    {
                        field: "Salesperson",
                        //title: "Sales Person",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Sales Person" class="report_tooltip" >Sales Person</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },
                    {
                        field: "CustName",
                        //title: "Cust Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Cust Name" class="report_tooltip" >Cust Name</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },
                    {
                        field: "CustOrder",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Count of Orders" class="report_tooltip" >Cust Order #</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        template: function (dataItem) {
                            if (dataItem.CustOrder)
                                return "<span class='Grid_Textalign'>" + dataItem.CustOrder + "</span>";
                            else
                                return "";
                        },

                        width: "120px",
                        footerTemplate: "<div class='CustOrderWrapper'></div>"
                    },
                    {
                        field: "MPO",
                        //title: "MPO",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of MPOs" class="report_tooltip" >MPO</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        template: function (dataItem) {
                            if (dataItem.MPO)
                                return "<span class='Grid_Textalign'>" + dataItem.MPO + "</span>";
                            else
                                return "";
                        },
                        width: "100px",
                        footerTemplate: "<div class='MPOWrapper'></div>"
                    },
                    {
                        field: "VPO",
                        //title: "VPO",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of VPOs" class="report_tooltip" >VPO</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        template: function (dataItem) {
                            if (dataItem.VPO)
                                return "<span class='Grid_Textalign'>" + dataItem.VPO + "</span>";
                            else
                                return "";
                        },
                        width: "100px",
                        footerTemplate: "<div class='VPOWrapper'></div>"
                    },
                    {
                        field: "LineNumber",
                        //title: "Line #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Line #" class="report_tooltip" >Line #</label>',
                        template: function (dataItem) {
                            if (dataItem.LineNumber)
                                return "<span class='Grid_Textalign'>" + dataItem.LineNumber + "</span>";
                            else
                                return "";
                        },
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "100px"
                    },
                    {
                        field: "VendorName",
                        //title: "Vendor Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Territory Name" class="report_tooltip" >Vendor Name</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },
                    {
                        field: "ProductCategory",
                        //title: "Product Category",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Product Category" class="report_tooltip" >Product Category</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px"
                    },

                    {
                        field: "ProductName",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Product Name" class="report_tooltip" >Product Name</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "120px",

                    },
                    {
                        field: "Description",
                        //title: "Description",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Description" class="report_tooltip" >Description</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "600px",
                        hidden: true
                    },
                    {
                        field: "LineCost",
                        //title: "Line Cost",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Line Cost" class="report_tooltip" >Line Cost</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.LineCost) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "LineValue",
                        //title: "Line Value",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Line Value" class="report_tooltip" >Line Value</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.LineValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "100px"

                    },
                    {
                        field: "GPValue",
                        //title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit [Line Value-Line Cost]" class="report_tooltip" >GP $</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "100px"
                    },
                    {
                        field: "GPPercenatge",
                        //title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit % [Gross Profit/Line Value]" class="report_tooltip" >GP %</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        aggregates: ["average"],
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p2}", GPPercenatge / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridSalesAndPurchaseSearch").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.LineValue.sum > 0) {
                                var gpPer = (aggregates.GPValue.sum / aggregates.LineValue.sum) * 100;
                                return  gpPer.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }

                        },
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "100px"
                    },
                    {
                        field: "CustOrderDate",
                        //title: "Cust Order Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cust Order Date" class="report_tooltip" >Cust Order Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.CustOrderDate) {
                                return HFCService.KendoDateFormat(dataItem.CustOrderDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseCCCustOrderDateFilterCalendar", offsetvalue),
                        //  format: "{0:MM/dd/yyyy}"
                    }, {
                        field: "MPODate",
                        //title: "MPO Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="MPO Date" class="report_tooltip" >MPO Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.MPODate) {
                                return HFCService.KendoDateFormat(dataItem.MPODate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseCCMPODateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "VPODate",
                        //title: "VPO Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="VPO Date" class="report_tooltip" >VPO Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.VPODate) {
                                return HFCService.KendoDateFormat(dataItem.VPODate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseCCVPODateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "PromiseByDate",
                        //title: "Promise By Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Promise By Date" class="report_tooltip" >Promise By Date</label>',
                        // filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.PromiseByDate) {
                                return HFCService.KendoDateFormat(dataItem.PromiseByDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseCCPromiseByDateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "EstShipDate",
                        //title: "Est Ship Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Est Ship Date" class="report_tooltip" >Est Ship Date</label>',
                        // filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.EstShipDate) {
                                return HFCService.KendoDateFormat(dataItem.EstShipDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseCCEstShipDateFilterCalendar", offsetvalue),
                        //  format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "actualshipdate",
                        //title: "actual ship date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="actual ship date" class="report_tooltip" >actual ship date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.actualshipdate) {
                                return HFCService.KendoDateFormat(dataItem.actualshipdate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseCCactualshipdateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "InvoiceDate",
                        //title: "Invoice Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Invoice Date" class="report_tooltip" >Invoice Date</label>',
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.InvoiceDate) {
                                return HFCService.KendoDateFormat(dataItem.InvoiceDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "120px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesPurchaseCCInvoiceDateFilterCalendar", offsetvalue),
                        //format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "VPOStatus",
                        //title: "VPO Status",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="VPO Status" class="report_tooltip" >VPO Status</label>',
                        filterable: { multi: true, search: true },
                        width: "120px"
                    },
                    {
                        field: "Errors",
                        //title: "Errors",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Errors" class="report_tooltip" >Errors</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "200px"
                    },
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },

                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                },
                sortable: ({ field: "CustOrderDate", dir: "desc" }),
                groupable: true,
                scrollable: true,
                resizable: true
            };
        };

        //----------FranchisePerformanceReport
        $scope.FranchisePerformanceReport_SalesAgent = function (data) {
            $scope.FranchisePerformanceReport_SalesAgent_options = {
                cache: false,
                excel: {
                    fileName: "FranchisePerformanceReport_SalesAgent.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getFranchisePerformance_SalesAgent',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                SalesPerson: { type: "string" },
                                opportunities: { type: "number" },
                                Quote: { type: "number" },
                                OpenQuote: { type: "number" },
                                OpenQuoteAmount: { type: "number" },
                                Orders: { type: "number" },
                                Unit: { type: "number" },
                                ClosingRate: { type: "number" },
                                QuotetoClose: { type: "number" },
                                COGS: { type: "number" },
                                TotalSales: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "number" },
                                AvgOrder: { type: "number" },
                                COGSProd: { type: "number" },
                                TotalSalesProd: { type: "number" },
                                GPValueProd: { type: "number" },
                                GPPercentageProd: { type: "number" },
                                AvgOrderProd: { type: "number" }
                            }
                        }
                    },
                    aggregate: [
                        { field: "opportunities", aggregate: "sum" },
                        { field: "Quote", aggregate: "sum" },
                        { field: "OpenQuote", aggregate: "sum" },
                        { field: "OpenQuoteAmount", aggregate: "sum" },
                        { field: "Orders", aggregate: "sum" },
                        { field: "Unit", aggregate: "sum" },
                        { field: "ClosingRate", aggregate: "average" },
                        { field: "QuotetoClose", aggregate: "average" },
                        { field: "COGS", aggregate: "sum" },
                        { field: "TotalSales", aggregate: "sum" },
                        { field: "GPValue", aggregate: "sum" },
                        { field: "GPPercenatge", aggregate: "average" },
                        { field: "COGSProd", aggregate: "sum" },
                        { field: "TotalSalesProd", aggregate: "sum" },
                        { field: "GPValueProd", aggregate: "sum" },
                        { field: "GPPercentageProd", aggregate: "average" }
                    ],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    //$("#gridFranchisePerformanceReport_SalesAgent").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_SalesAgent .k-grid-header"))
                    if ($("#gridFranchisePerformanceReport_SalesAgent").find(".k-grid-header").next()[0] != $("#gridFranchisePerformanceReport_SalesAgent").find(".k-grid-footer")[0])
                        $("#gridFranchisePerformanceReport_SalesAgent").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_SalesAgent .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "(primary) Open Quote $" || sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Total Sales" || sheet.rows[0].cells[cellIndex].value == "GP $" || sheet.rows[0].cells[cellIndex].value == "Avg Sale" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Total Sales (Product)" || sheet.rows[0].cells[cellIndex].value == "GP $ (Product)" || sheet.rows[0].cells[cellIndex].value == "Avg Sale (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Closing Rate" || sheet.rows[0].cells[cellIndex].value == "Quote to close %" || sheet.rows[0].cells[cellIndex].value == "GP %" || sheet.rows[0].cells[cellIndex].value == "GP % (Product)") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "SalesPerson",
                        title: "Sales Person",
                        locked: true,
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Sales Person" class="report_tooltip">Sales Person</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "opportunities",
                        title: "Opportunity #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Opportunities" class="report_tooltip">Opportunity #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.opportunities)
                                return "<span class='Grid_Textalign'>" + dataItem.opportunities + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Quote",
                        title: "(primary) Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Quotes" class="report_tooltip">(primary) Quote # </label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Quote)
                                return "<span class='Grid_Textalign'>" + dataItem.Quote + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuote",
                        title: "(primary) Open Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Open Quotes" class="report_tooltip">(primary) Open Quote #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.OpenQuote)
                                return "<span class='Grid_Textalign'>" + dataItem.OpenQuote + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuoteAmount",
                        title: "(primary) Open Quote $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total of Open Quotes" class="report_tooltip">(primary) Open Quote $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.OpenQuoteAmount) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "Orders",
                        title: "Orders #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Orders" class="report_tooltip">Orders #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Orders)
                                return "<span class='Grid_Textalign'>" + dataItem.Orders + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Unit",
                        title: "Unit #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Units" class="report_tooltip">Unit #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Unit)
                                return "<span class='Grid_Textalign'>" + dataItem.Unit + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "ClosingRate",
                        title: "Closing Rate",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Closing Rate [Count of Orders/Count of Opportunities]" class="report_tooltip">Closing Rate</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", ClosingRate / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_SalesAgent").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.opportunities.sum > 0) {
                                var agg = (aggregates.Orders.sum / aggregates.opportunities.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "QuotetoClose",
                        title: "Quote to close %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Quote to Closing Rate [Count of Orders/Count of Primary Quotes]" class="report_tooltip">Quote to close %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", QuotetoClose / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_SalesAgent").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Quote.sum > 0) {
                                var agg = (aggregates.Orders.sum / aggregates.Quote.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        //format: "{0:c2}",
                        width: "130px"
                    },
                    {
                        field: "TotalSales",
                        title: "Total Sales",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales" class="report_tooltip">Total Sales</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPValue",
                        title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS]" class="report_tooltip">GP $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPPercenatge",
                        title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales]" class="report_tooltip">GP %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercenatge / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_SalesAgent").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSales.sum > 0) {
                                var agg = (aggregates.GPValue.sum / aggregates.TotalSales.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "AvgOrder",
                        title: "Avg Sale",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders]" class="report_tooltip">Avg Sale</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrder) + "</span>";
                        },
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_SalesAgent").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Orders.sum > 0) {
                                var agg = (aggregates.TotalSales.sum / aggregates.Orders.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                            } else {
                                return "$0";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "COGSProd",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold (Product)" class="report_tooltip">COGS (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "TotalSalesProd",
                        title: "Total Sales (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales (Product)" class="report_tooltip">Total Sales (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSalesProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "160px"
                    },
                    {
                        field: "GPValueProd",
                        title: "GP $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS] (Product)" class="report_tooltip">GP $ (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValueProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPPercentageProd",
                        title: "GP % (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales] (Product)" class="report_tooltip">GP % (Product)</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercentageProd / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_SalesAgent").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSalesProd.sum > 0) {
                                var agg = (aggregates.GPValueProd.sum / aggregates.TotalSalesProd.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "150px"
                    },
                    {
                        field: "AvgOrderProd",
                        title: "Avg Sale (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders] (Product)" class="report_tooltip">Avg Sale (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrderProd) + "</span>";
                        },
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_SalesAgent").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Orders.sum > 0) {
                                var agg = (aggregates.TotalSalesProd.sum / aggregates.Orders.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                            } else {
                                return "$0";
                            }
                        },
                        width: "160px"
                    }

                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "SalesPerson", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        $scope.FranchisePerformanceReport_Territory = function (data) {
            $scope.FranchisePerformanceReport_Territory_options = {
                cache: false,
                excel: {
                    fileName: "FranchisePerformanceReport_Territory.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getFranchisePerformance_Territory',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                Territory: { type: "string" },
                                opportunities: { type: "number" },
                                Quote: { type: "number" },
                                OpenQuote: { type: "number" },
                                OpenQuoteAmount: { type: "number" },
                                Orders: { type: "number" },
                                Unit: { type: "number" },
                                ClosingRate: { type: "number" },
                                QuotetoClose: { type: "number" },
                                COGS: { type: "number" },
                                TotalSales: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "number" },
                                AvgOrder: { type: "number" },
                                COGSProd: { type: "number" },
                                TotalSalesProd: { type: "number" },
                                GPValueProd: { type: "number" },
                                GPPercentageProd: { type: "number" },
                                AvgOrderProd: { type: "number" }
                            }
                        }
                    },
                    aggregate: [
                        { field: "opportunities", aggregate: "sum" },
                        { field: "Quote", aggregate: "sum" },
                        { field: "OpenQuote", aggregate: "sum" },
                        { field: "OpenQuoteAmount", aggregate: "sum" },
                        { field: "Orders", aggregate: "sum" },
                        { field: "Unit", aggregate: "sum" },
                        { field: "ClosingRate", aggregate: "average" },
                        { field: "QuotetoClose", aggregate: "average" },
                        { field: "COGS", aggregate: "sum" },
                        { field: "TotalSales", aggregate: "sum" },
                        { field: "GPValue", aggregate: "sum" },
                        { field: "GPPercenatge", aggregate: "average" },
                        { field: "COGSProd", aggregate: "sum" },
                        { field: "TotalSalesProd", aggregate: "sum" },
                        { field: "GPValueProd", aggregate: "sum" },
                        { field: "GPPercentageProd", aggregate: "average" }
                    ],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    //var myDiv = $('.k-grid-content.k-auto-scrollable');
                    //if (myDiv[0]) {
                    //    myDiv[0].scrollLeft = 0;
                    //}
                    //$("#gridFranchisePerformanceReport_Territory").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_Territory .k-grid-header"))
                    if ($("#gridFranchisePerformanceReport_Territory").find(".k-grid-header").next()[0] != $("#gridFranchisePerformanceReport_Territory").find(".k-grid-footer")[0])
                        $("#gridFranchisePerformanceReport_Territory").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_Territory .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "(primary) Open Quote $" || sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Total Sales" || sheet.rows[0].cells[cellIndex].value == "GP $" || sheet.rows[0].cells[cellIndex].value == "Avg Sale" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Total Sales (Product)" || sheet.rows[0].cells[cellIndex].value == "GP $ (Product)" || sheet.rows[0].cells[cellIndex].value == "Avg Sale (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Closing Rate" || sheet.rows[0].cells[cellIndex].value == "Quote to close %" || sheet.rows[0].cells[cellIndex].value == "GP %" || sheet.rows[0].cells[cellIndex].value == "GP % (Product)") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "Territory",
                        title: "Territory",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Territory" class="report_tooltip">Territory</label>',
                        filterable: { search: true },
                        locked: true,
                        width: "130px"
                    },
                    {
                        field: "opportunities",
                        title: "Opportunity #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Opportunities" class="report_tooltip">Opportunity #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.opportunities + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Quote",
                        title: "(primary) Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Quotes" class="report_tooltip">(primary) Quote # </label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Quote + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuote",
                        title: "(primary) Open Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Open Quotes" class="report_tooltip">(primary) Open Quote #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.OpenQuote + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuoteAmount",
                        title: "(primary) Open Quote $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total of Open Quotes" class="report_tooltip">(primary) Open Quote $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.OpenQuoteAmount) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "Orders",
                        title: "Orders #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Orders" class="report_tooltip">Orders #</label>',
                        template: function (dataItem) {
                            if (dataItem.Orders)
                                return "<span class='Grid_Textalign'>" + dataItem.Orders + "</span>";
                            else
                                return "";
                        },
                        filterable: { search: true },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Unit",
                        title: "Unit #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Units" class="report_tooltip">Unit #</label>',
                        template: function (dataItem) {
                            if (dataItem.Unit)
                                return "<span class='Grid_Textalign'>" + dataItem.Unit + "</span>";
                            else
                                return "";
                        },
                        filterable: { search: true },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "ClosingRate",
                        title: "Closing Rate",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Closing Rate [Count of Orders/Count of Opportunities]" class="report_tooltip">Closing Rate</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", ClosingRate / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Territory").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.opportunities.sum > 0) {
                                var agg = (aggregates.Orders.sum / aggregates.opportunities.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "QuotetoClose",
                        title: "Quote to close %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Quote to Closing Rate [Count of Orders/Count of Primary Quotes]" class="report_tooltip">Quote to close %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", QuotetoClose / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Territory").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Quote.sum > 0) {
                                var agg = (aggregates.Orders.sum / aggregates.Quote.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "TotalSales",
                        title: "Total Sales",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales" class="report_tooltip">Total Sales</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPValue",
                        title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS]" class="report_tooltip">GP $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPPercenatge",
                        title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales]" class="report_tooltip">GP %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercenatge / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Territory").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSales.sum > 0) {
                                var agg = (aggregates.GPValue.sum / aggregates.TotalSales.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "AvgOrder",
                        title: "Avg Sale",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders]" class="report_tooltip">Avg Sale</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrder) + "</span>";
                        },
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Territory").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Orders.sum > 0) {
                                var agg = (aggregates.TotalSales.sum / aggregates.Orders.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                            } else {
                                return "$0";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "COGSProd",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold (Product)" class="report_tooltip">COGS (Product)</label>',
                        filterable: { search: true },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProd) + "</span>";
                        },
                        width: "150px"
                    },
                    {
                        field: "TotalSalesProd",
                        title: "Total Sales (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales (Product)" class="report_tooltip">Total Sales (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSalesProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "160px"
                    },
                    {
                        field: "GPValueProd",
                        title: "GP $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS] (Product)" class="report_tooltip">GP $ (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValueProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPPercentageProd",
                        title: "GP % (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales] (Product)" class="report_tooltip">GP % (Product)</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercentageProd / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Territory").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSalesProd.sum > 0) {
                                var agg = (aggregates.GPValueProd.sum / aggregates.TotalSalesProd.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "150px"
                    },
                    {
                        field: "AvgOrderProd",
                        title: "Avg Sale (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders] (Product)" class="report_tooltip">Avg Sale (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrderProd) + "</span>";
                        },
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Territory").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Orders.sum > 0) {
                                var agg = (aggregates.TotalSalesProd.sum / aggregates.Orders.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                            } else {
                                return "$0";
                            }
                        },
                        width: "160px"
                    }

                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "Territory", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        $scope.FranchisePerformanceReport_Franchise = function (data) {
            $scope.FranchisePerformanceReport_Franchise_options = {
                cache: false,
                excel: {
                    fileName: "FranchisePerformanceReport_Franchise.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getFranchisePerformance_Franchise',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                AccountCode: { type: "string" },
                                opportunities: { type: "number" },
                                Quote: { type: "number" },
                                OpenQuote: { type: "number" },
                                OpenQuoteAmount: { type: "number" },
                                Orders: { type: "number" },
                                Unit: { type: "number" },
                                ClosingRate: { type: "number" },
                                QuotetoClose: { type: "number" },
                                COGS: { type: "number" },
                                TotalSales: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "number" },
                                AvgOrder: { type: "number" },
                                COGSProd: { type: "number" },
                                TotalSalesProd: { type: "number" },
                                GPValueProd: { type: "number" },
                                GPPercentageProd: { type: "number" },
                                AvgOrderProd: { type: "number" },
                            }
                        }
                    },
                    //aggregate: [
                    //    { field: "opportunities", aggregate: "sum" },
                    //    { field: "Quote", aggregate: "sum" },
                    //    { field: "OpenQuote", aggregate: "sum" },
                    //    { field: "OpenQuoteAmount", aggregate: "sum" },
                    //    { field: "Orders", aggregate: "sum" },
                    //    { field: "Unit", aggregate: "sum" },
                    //    { field: "ClosingRate", aggregate: "average" },
                    //    { field: "QuotetoClose", aggregate: "average" },
                    //    { field: "COGS", aggregate: "sum" },
                    //    { field: "TotalSales", aggregate: "sum" },
                    //    { field: "GPValue", aggregate: "sum" },
                    //    { field: "GPPercenatge", aggregate: "average" }
                    //],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    //var myDiv = $('.k-grid-content.k-auto-scrollable');
                    //if (myDiv[0]) {
                    //    myDiv[0].scrollLeft = 0;
                    //}
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "(primary) Open Quote $" || sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Total Sales" || sheet.rows[0].cells[cellIndex].value == "GP $" || sheet.rows[0].cells[cellIndex].value == "Avg Sale" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Total Sales (Product)" || sheet.rows[0].cells[cellIndex].value == "GP $ (Product)" || sheet.rows[0].cells[cellIndex].value == "Avg Sale (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Closing Rate" || sheet.rows[0].cells[cellIndex].value == "Quote to close %" || sheet.rows[0].cells[cellIndex].value == "GP %" || sheet.rows[0].cells[cellIndex].value == "GP % (Product)") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "AccountCode",
                        title: "AccountCode",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="AccountCode" class="report_tooltip" >AccountCode</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.AccountCode)
                                return "<span class='Grid_Textalign'>" + dataItem.AccountCode + "</span>";
                            else
                                return "";
                        },
                        locked: true,
                        width: "130px"
                    },
                    {
                        field: "opportunities",
                        title: "Opportunity #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Opportunities" class="report_tooltip">Opportunity #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.opportunities)
                                return "<span class='Grid_Textalign'>" + dataItem.opportunities + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Quote",
                        title: "(primary) Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Quotes" class="report_tooltip">(primary) Quote # </label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Quote)
                                return "<span class='Grid_Textalign'>" + dataItem.Quote + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuote",
                        title: "(primary) Open Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Open Quotes" class="report_tooltip">(primary) Open Quote #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.OpenQuote)
                                return "<span class='Grid_Textalign'>" + dataItem.OpenQuote + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuoteAmount",
                        title: "(primary) Open Quote $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total of Open Quotes" class="report_tooltip">(primary) Open Quote $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.OpenQuoteAmount) + "</span>";
                        },
                        //footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "Orders",
                        title: "Orders #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Orders" class="report_tooltip">Orders #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Orders)
                                return "<span class='Grid_Textalign'>" + dataItem.Orders + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Unit",
                        title: "Unit #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Units" class="report_tooltip">Unit #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Unit)
                                return "<span class='Grid_Textalign'>" + dataItem.Unit + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "ClosingRate",
                        title: "Closing Rate",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Closing Rate [Count of Orders/Count of Opportunities]" class="report_tooltip">Closing Rate</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", ClosingRate / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "QuotetoClose",
                        title: "Quote to close %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Quote to Closing Rate [Count of Orders/Count of Primary Quotes]" class="report_tooltip">Quote to close %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", QuotetoClose / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        //footerTemplate: "#=kendo.toString(sum, 'C')#",
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "TotalSales",
                        title: "Total Sales",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales" class="report_tooltip">Total Sales</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                        },
                        //footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPValue",
                        title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS]" class="report_tooltip">GP $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        //footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPPercenatge",
                        title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales]" class="report_tooltip">GP %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercenatge / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "AvgOrder",
                        title: "Avg Sale",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders]" class="report_tooltip">Avg Sale</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrder) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "COGSProd",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold (Product)" class="report_tooltip">COGS (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProd) + "</span>";
                        },
                        width: "150px"
                    },
                    {
                        field: "TotalSalesProd",
                        title: "Total Sales (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales (Product)" class="report_tooltip">Total Sales (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSalesProd) + "</span>";
                        },
                        width: "165px"
                    },
                    {
                        field: "GPValueProd",
                        title: "GP $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS] (Product)" class="report_tooltip">GP $ (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValueProd) + "</span>";
                        },
                        width: "150px"
                    },
                    {
                        field: "GPPercentageProd",
                        title: "GP % (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales] (Product)" class="report_tooltip">GP % (Product)</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercentageProd / 100)#</span>',
                        width: "150px"
                    },
                    {
                        field: "AvgOrderProd",
                        title: "Avg Sale (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders] (Product)" class="report_tooltip">Avg Sale (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrderProd) + "</span>";
                        },
                        width: "160px"
                    }

                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "AccountCode", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        $scope.FranchisePerformanceReport_Zip = function (data) {
            $scope.FranchisePerformanceReport_Zip_options = {
                cache: false,
                excel: {
                    fileName: "FranchisePerformanceReport_ZipCode.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getFranchisePerformance_Zip',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                ZipCode: { type: "string" },
                                opportunities: { type: "number" },
                                Quote: { type: "number" },
                                OpenQuote: { type: "number" },
                                OpenQuoteAmount: { type: "number" },
                                Orders: { type: "number" },
                                Unit: { type: "number" },
                                ClosingRate: { type: "number" },
                                QuotetoClose: { type: "number" },
                                COGS: { type: "number" },
                                TotalSales: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "number" },
                                AvgOrder: { type: "number" },
                                COGSProd: { type: "number" },
                                TotalSalesProd: { type: "number" },
                                GPValueProd: { type: "number" },
                                GPPercenatgeProd: { type: "number" },
                                AvgOrderProd: { type: "number" }
                            }
                        }
                    },
                    aggregate: [
                        { field: "opportunities", aggregate: "sum" },
                        { field: "Quote", aggregate: "sum" },
                        { field: "OpenQuote", aggregate: "sum" },
                        { field: "OpenQuoteAmount", aggregate: "sum" },
                        { field: "Orders", aggregate: "sum" },
                        { field: "Unit", aggregate: "sum" },
                        { field: "ClosingRate", aggregate: "average" },
                        { field: "QuotetoClose", aggregate: "average" },
                        { field: "COGS", aggregate: "sum" },
                        { field: "TotalSales", aggregate: "sum" },
                        { field: "GPValue", aggregate: "sum" },
                        { field: "GPPercenatge", aggregate: "average" },
                        { field: "COGSProd", aggregate: "sum" },
                        { field: "TotalSalesProd", aggregate: "sum" },
                        { field: "GPValueProd", aggregate: "sum" },
                        { field: "GPPercenatgeProd", aggregate: "average" }
                    ],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    //$("#gridFranchisePerformanceReport_Zip").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_Zip .k-grid-header"))
                    if ($("#gridFranchisePerformanceReport_Zip").find(".k-grid-header").next()[0] != $("#gridFranchisePerformanceReport_Zip").find(".k-grid-footer")[0])
                        $("#gridFranchisePerformanceReport_Zip").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_Zip .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "(primary) Open Quote $" || sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Total Sales" || sheet.rows[0].cells[cellIndex].value == "GP $" || sheet.rows[0].cells[cellIndex].value == "Avg Sale" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Total Sales (Product)" || sheet.rows[0].cells[cellIndex].value == "GP $ (Product)" || sheet.rows[0].cells[cellIndex].value == "Avg Sale (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Closing Rate" || sheet.rows[0].cells[cellIndex].value == "Quote to close %" || sheet.rows[0].cells[cellIndex].value == "GP %" || sheet.rows[0].cells[cellIndex].value == "GP % (Product)") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "ZipCode",
                        title: "Zip",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Zip" class="report_tooltip">Zip</label>',
                        filterable: { search: true },
                        locked: true,
                        width: "130px"
                    },
                    {
                        field: "opportunities",
                        title: "Opportunity #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Opportunities" class="report_tooltip">Opportunity #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.opportunities)
                                return "<span class='Grid_Textalign'>" + dataItem.opportunities + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Quote",
                        title: "(primary) Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Quotes" class="report_tooltip">(primary) Quote # </label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Quote)
                                return "<span class='Grid_Textalign'>" + dataItem.Quote + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuote",
                        title: "(primary) Open Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Open Quotes" class="report_tooltip">(primary) Open Quote #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.OpenQuote)
                                return "<span class='Grid_Textalign'>" + dataItem.OpenQuote + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuoteAmount",
                        title: "(primary) Open Quote $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total of Open Quotes" class="report_tooltip">(primary) Open Quote $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.OpenQuoteAmount) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "Orders",
                        title: "Orders #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Orders" class="report_tooltip">Orders #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Orders)
                                return "<span class='Grid_Textalign'>" + dataItem.Orders + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Unit",
                        title: "Unit #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Units" class="report_tooltip">Unit #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Unit)
                                return "<span class='Grid_Textalign'>" + dataItem.Unit + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "ClosingRate",
                        title: "Closing Rate",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Closing Rate [Count of Orders/Count of Opportunities]" class="report_tooltip">Closing Rate</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", ClosingRate / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Zip").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.opportunities.sum > 0) {
                                var agg = (aggregates.Orders.sum / aggregates.opportunities.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },

                        width: "130px"
                    },
                    {
                        field: "QuotetoClose",
                        title: "Quote to close %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Quote to Closing Rate [Count of Orders/Count of Primary Quotes]" class="report_tooltip">Quote to close %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", QuotetoClose / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Zip").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Quote.sum > 0) {
                                var agg = (aggregates.Orders.sum / aggregates.Quote.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        //format: "{0:c2}",
                        width: "130px"
                    },
                    {
                        field: "TotalSales",
                        title: "Total Sales",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales" class="report_tooltip">Total Sales</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPValue",
                        title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS]" class="report_tooltip">GP $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPPercenatge",
                        title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales]" class="report_tooltip">GP %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercenatge / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Zip").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSales.sum > 0) {
                                var agg = (aggregates.GPValue.sum / aggregates.TotalSales.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "AvgOrder",
                        title: "Avg Sale",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders]" class="report_tooltip">Avg Sale</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrder) + "</span>";
                        },
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Zip").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Orders.sum > 0) {
                                var agg = (aggregates.TotalSales.sum / aggregates.Orders.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                            } else {
                                return "$0";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "COGSProd",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold (Product)" class="report_tooltip">COGS (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "TotalSalesProd",
                        title: "Total Sales (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales (Product)" class="report_tooltip">Total Sales (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSalesProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "165px"
                    },
                    {
                        field: "GPValueProd",
                        title: "GP $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS] (Product)" class="report_tooltip">GP $ (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValueProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPPercenatgeProd",
                        title: "GP % (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales] (Product)" class="report_tooltip">GP % (Product)</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercenatgeProd / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Zip").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSalesProd.sum > 0) {
                                var agg = (aggregates.GPValueProd.sum / aggregates.TotalSalesProd.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "150px"
                    },
                    {
                        field: "AvgOrderProd",
                        title: "Avg Sale (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders] (Product)" class="report_tooltip">Avg Sale (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrderProd) + "</span>";
                        },
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Zip").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Orders.sum > 0) {
                                var agg = (aggregates.TotalSalesProd.sum / aggregates.Orders.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                            } else {
                                return "$0";
                            }
                        },
                        width: "160px"
                    }

                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "ZipCode", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        $scope.FranchisePerformanceReport_Vendor = function (data) {
            $scope.FranchisePerformanceReport_Vendor_options = {
                cache: false,
                excel: {
                    fileName: "FranchisePerformanceReport_Vendor.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getFranchisePerformance_Vendor',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                Vendor: { type: "string" },
                                opportunities: { type: "number" },
                                Quote: { type: "number" },
                                OpenQuote: { type: "number" },
                                OpenQuoteAmount: { type: "number" },
                                Orders: { type: "number" },
                                Unit: { type: "number" },
                                ClosingRate: { type: "number" },
                                QuotetoClose: { type: "number" },
                                COGS: { type: "number" },
                                TotalSales: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "number" },
                                AvgOrder: { type: "number" },
                                COGSProd: { type: "number" },
                                TotalSalesProd: { type: "number" },
                                GPValueProd: { type: "number" },
                                GPPercenatgeProd: { type: "number" },
                                AvgOrderProd: { type: "number" }
                            }
                        }
                    },
                    aggregate: [
                        { field: "opportunities", aggregate: "sum" },
                        { field: "Quote", aggregate: "sum" },
                        { field: "OpenQuote", aggregate: "sum" },
                        { field: "OpenQuoteAmount", aggregate: "sum" },
                        { field: "Orders", aggregate: "sum" },
                        { field: "Unit", aggregate: "sum" },
                        { field: "ClosingRate", aggregate: "average" },
                        { field: "QuotetoClose", aggregate: "average" },
                        { field: "COGS", aggregate: "sum" },
                        { field: "TotalSales", aggregate: "sum" },
                        { field: "GPValue", aggregate: "sum" },
                        { field: "GPPercenatge", aggregate: "average" },
                        { field: "COGSProd", aggregate: "sum" },
                        { field: "TotalSalesProd", aggregate: "sum" },
                        { field: "GPValueProd", aggregate: "sum" },
                        { field: "GPPercenatgeProd", aggregate: "average" }
                    ],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    //$("#gridFranchisePerformanceReport_Vendor").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_Vendor .k-grid-header"))
                    if ($("#gridFranchisePerformanceReport_Vendor").find(".k-grid-header").next()[0] != $("#gridFranchisePerformanceReport_Vendor").find(".k-grid-footer")[0])
                        $("#gridFranchisePerformanceReport_Vendor").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_Vendor .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "(primary) Open Quote $" || sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Total Sales" || sheet.rows[0].cells[cellIndex].value == "GP $" || sheet.rows[0].cells[cellIndex].value == "Avg Sale" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Total Sales (Product)" || sheet.rows[0].cells[cellIndex].value == "GP $ (Product)" || sheet.rows[0].cells[cellIndex].value == "Avg Sale (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Closing Rate" || sheet.rows[0].cells[cellIndex].value == "Quote to close %" || sheet.rows[0].cells[cellIndex].value == "GP %" || sheet.rows[0].cells[cellIndex].value == "GP % (Product)") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "Vendor",
                        title: "Vendor",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Vendor" class="report_tooltip">Vendor</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Vendor)
                                return "<span class='Grid_Textalign'>" + dataItem.Vendor + "</span>";
                            else
                                return "";
                        },
                        locked: true,
                        width: "130px"
                    },
                    {
                        field: "opportunities",
                        title: "Opportunity #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Opportunities" class="report_tooltip">Opportunity #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.opportunities)
                                return "<span class='Grid_Textalign'>" + dataItem.opportunities + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Quote",
                        title: "(primary) Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Quotes" class="report_tooltip">(primary) Quote # </label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Quote)
                                return "<span class='Grid_Textalign'>" + dataItem.Quote + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuote",
                        title: "(primary) Open Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Open Quotes" class="report_tooltip">(primary) Open Quote #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.OpenQuote)
                                return "<span class='Grid_Textalign'>" + dataItem.OpenQuote + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuoteAmount",
                        title: "(primary) Open Quote $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total of Open Quotes" class="report_tooltip">(primary) Open Quote $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.OpenQuoteAmount) + "</span>";
                        },
                        //footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "Orders",
                        title: "Orders #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Orders" class="report_tooltip">Orders #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Orders)
                                return "<span class='Grid_Textalign'>" + dataItem.Orders + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Unit",
                        title: "Unit #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Units" class="report_tooltip">Unit #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Unit)
                                return "<span class='Grid_Textalign'>" + dataItem.Unit + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "ClosingRate",
                        title: "Closing Rate",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Closing Rate [Count of Orders/Count of Opportunities]" class="report_tooltip">Closing Rate</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", ClosingRate / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "QuotetoClose",
                        title: "Quote to close %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Quote to Closing Rate [Count of Orders/Count of Primary Quotes]" class="report_tooltip">Quote to close %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", QuotetoClose / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        footerTemplate: "<span class='Grid_Textalign'>#=kendo.toString(sum, 'C')#</span>",
                        //format: "{0:c2}",
                        width: "130px"
                    },
                    {
                        field: "TotalSales",
                        title: "Total Sales",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales" class="report_tooltip">Total Sales</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                        },
                        footerTemplate: "<span class='Grid_Textalign'>#=kendo.toString(sum, 'C')#</span>",
                        width: "130px"
                    },
                    {
                        field: "GPValue",
                        title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS]" class="report_tooltip">GP $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "<span class='Grid_Textalign'>#=kendo.toString(sum, 'C')#</span>",
                        width: "130px"
                    },
                    {
                        field: "GPPercenatge",
                        title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        filterable: { search: true },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales]" class="report_tooltip">GP %</label>',
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercenatge / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Vendor").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSales.sum > 0) {
                                var agg = (aggregates.GPValue.sum / aggregates.TotalSales.sum) * 100;
                                return "<span class='Grid_Textalign'>" + agg.toFixed(2) + "%</span>";
                            } else {
                                return "<span class='Grid_Textalign'>0%</span>";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "AvgOrder",
                        title: "Avg Sale",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders]" class="report_tooltip">Avg Sale</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrder) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "COGSProd",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold (Product)" class="report_tooltip">COGS (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "TotalSalesProd",
                        title: "Total Sales (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales (Product)" class="report_tooltip">Total Sales (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSalesProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "165px"
                    },
                    {
                        field: "GPValueProd",
                        title: "GP $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS] (Product)" class="report_tooltip">GP $ (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValueProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPPercenatgeProd",
                        title: "GP % (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales] (Product)" class="report_tooltip">GP % (Product)</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercenatgeProd / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_Vendor").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSalesProd.sum > 0) {
                                var agg = (aggregates.GPValueProd.sum / aggregates.TotalSalesProd.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "150px"
                    },
                    {
                        field: "AvgOrderProd",
                        title: "Avg Sale (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders] (Product)" class="report_tooltip">Avg Sale (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrderProd) + "</span>";
                        },
                        width: "160px"
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "ZipCode", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        $scope.FranchisePerformanceReport_ProductCategory = function (data) {
            $scope.FranchisePerformanceReport_ProductCategory_options = {
                cache: false,
                excel: {
                    fileName: "FranchisePerformanceReport_ProductCategory.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getFranchisePerformance_ProductCategory',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                ProductCategory: { type: "string" },
                                opportunities: { type: "number" },
                                Quote: { type: "number" },
                                OpenQuote: { type: "number" },
                                OpenQuoteAmount: { type: "number" },
                                Orders: { type: "number" },
                                Unit: { type: "number" },
                                ClosingRate: { type: "number" },
                                QuotetoClose: { type: "number" },
                                COGS: { type: "number" },
                                TotalSales: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercentage: { type: "number" },
                                AvgOrder: { type: "number" },
                                COGSProd: { type: "number" },
                                TotalSalesProd: { type: "number" },
                                GPValueProd: { type: "number" },
                                GPPercentageProd: { type: "number" },
                                AvgOrderProd: { type: "number" }
                            }
                        }
                    },
                    aggregate: [
                        { field: "opportunities", aggregate: "sum" },
                        { field: "Quote", aggregate: "sum" },
                        { field: "OpenQuote", aggregate: "sum" },
                        { field: "OpenQuoteAmount", aggregate: "sum" },
                        { field: "Orders", aggregate: "sum" },
                        { field: "Unit", aggregate: "sum" },
                        { field: "ClosingRate", aggregate: "average" },
                        { field: "QuotetoClose", aggregate: "average" },
                        { field: "COGS", aggregate: "sum" },
                        { field: "TotalSales", aggregate: "sum" },
                        { field: "GPValue", aggregate: "sum" },
                        { field: "GPPercentage", aggregate: "average" },
                        { field: "COGSProd", aggregate: "sum" },
                        { field: "TotalSalesProd", aggregate: "sum" },
                        { field: "GPValueProd", aggregate: "sum" },
                        { field: "GPPercentageProd", aggregate: "average" }
                    ],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    //$("#gridFranchisePerformanceReport_ProductCategory").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_ProductCategory .k-grid-header"))
                    if ($("#gridFranchisePerformanceReport_ProductCategory").find(".k-grid-header").next()[0] != $("#gridFranchisePerformanceReport_ProductCategory").find(".k-grid-footer")[0])
                        $("#gridFranchisePerformanceReport_ProductCategory").find(".k-grid-footer").insertAfter($("#gridFranchisePerformanceReport_ProductCategory .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "(primary) Open Quote $" || sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Total Sales" || sheet.rows[0].cells[cellIndex].value == "GP $" || sheet.rows[0].cells[cellIndex].value == "Avg Sale" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Total Sales (Product)" || sheet.rows[0].cells[cellIndex].value == "GP $ (Product)" || sheet.rows[0].cells[cellIndex].value == "Avg Sale (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Closing Rate" || sheet.rows[0].cells[cellIndex].value == "Quote to close %" || sheet.rows[0].cells[cellIndex].value == "GP %" || sheet.rows[0].cells[cellIndex].value == "GP % (Product)") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "ProductCategory",
                        title: "Product Category",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Product Category" class="report_tooltip">Product Category</label>',
                        filterable: { search: true },
                        locked: true,
                        width: "130px"
                    },
                    {
                        field: "opportunities",
                        title: "Opportunity #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Opportunities" class="report_tooltip">Opportunity #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.opportunities)
                                return "<span class='Grid_Textalign'>" + dataItem.opportunities + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Quote",
                        title: "(primary) Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Quotes" class="report_tooltip">(primary) Quote # </label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Quote)
                                return "<span class='Grid_Textalign'>" + dataItem.Quote + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuote",
                        title: "(primary) Open Quote #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Primary Open Quotes" class="report_tooltip">(primary) Open Quote #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.OpenQuote)
                                return "<span class='Grid_Textalign'>" + dataItem.OpenQuote + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "OpenQuoteAmount",
                        title: "(primary) Open Quote $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total of Open Quotes" class="report_tooltip">(primary) Open Quote $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.OpenQuoteAmount) + "</span>";
                        },
                        //footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "Orders",
                        title: "Orders #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Orders" class="report_tooltip">Orders #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Orders)
                                return "<span class='Grid_Textalign'>" + dataItem.Orders + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "Unit",
                        title: "Unit #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Units" class="report_tooltip">Unit #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Unit)
                                return "<span class='Grid_Textalign'>" + dataItem.Unit + "</span>";
                            else
                                return "";
                        },
                        //footerTemplate: "#=kendo.toString(sum)#",
                        width: "130px"
                    },
                    {
                        field: "ClosingRate",
                        title: "Closing Rate",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Closing Rate [Count of Orders/Count of Opportunities]" class="report_tooltip">Closing Rate</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", ClosingRate / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "QuotetoClose",
                        title: "Quote to close %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Quote to Closing Rate [Count of Orders/Count of Primary Quotes]" class="report_tooltip">Quote to close %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", QuotetoClose / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "130px"
                    },
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "TotalSales",
                        title: "Total Sales",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales" class="report_tooltip">Total Sales</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPValue",
                        title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS]" class="report_tooltip">GP $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "GPPercentage",
                        title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales]" class="report_tooltip">GP %</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercentage / 100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_ProductCategory").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSales.sum > 0) {
                                var agg = (aggregates.GPValue.sum / aggregates.TotalSales.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "130px"
                    },
                    {
                        field: "AvgOrder",
                        title: "Avg Sale",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders]" class="report_tooltip">Avg Sale</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrder) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "COGSProd",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold (Product)" class="report_tooltip">COGS (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "TotalSalesProd",
                        title: "Total Sales (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales (Product)" class="report_tooltip">Total Sales (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSalesProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "165px"
                    },
                    {
                        field: "GPValueProd",
                        title: "GP $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Gross Profit[Total Sales-Total COGS] (Product)" class="report_tooltip">GP $ (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValueProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPPercentageProd",
                        title: "GP % (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Avg Gross Profit %[Gross Profit/Total Sales] (Product)" class="report_tooltip">GP % (Product)</label>',
                        filterable: { search: true },
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", GPPercentageProd / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridFranchisePerformanceReport_ProductCategory").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSalesProd.sum > 0) {
                                var agg = (aggregates.GPValueProd.sum / aggregates.TotalSalesProd.sum) * 100;
                                return agg.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "150px"
                    },
                    {
                        field: "AvgOrderProd",
                        title: "Avg Sale (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders] (Product)" class="report_tooltip">Avg Sale (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgOrderProd) + "</span>";
                        },
                        width: "160px"
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "ZipCode", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        //----------FranchisePerformanceReport

        //------SalesSummary
        $scope.SalesSummary_ThisYear = function (data) {
            $scope.SalesSummary_ThisYear_options = {
                excel: {
                    fileName: "SalesSummary.xlsx",
                },
                dataSource: $scope.ThisYearDataSource,
                resizable: true,
                columns: [
                    {
                        field: "ThisYear",
                        title: "THIS YEAR",
                        width: "100px"
                    },
                    {
                        field: "Jan",
                        title: "Jan",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jan + "</span>";
                        },
                    },
                    {
                        field: "Feb",
                        title: "Feb",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Feb + "</span>";
                        },
                    },
                    {
                        field: "Mar",
                        title: "Mar",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Mar + "</span>";
                        },
                    },
                    {
                        field: "Apr",
                        title: "Apr",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Apr + "</span>";
                        },
                    },
                    {
                        field: "May",
                        title: "May",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.May + "</span>";
                        },
                    },
                    {
                        field: "Jun",
                        title: "Jun",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jun + "</span>";
                        },
                    },
                    {
                        field: "Jul",
                        title: "Jul",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jul + "</span>";
                        },
                    },
                    {
                        field: "Aug",
                        title: "Aug",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Aug + "</span>";
                        },
                    },
                    {
                        field: "Sep",
                        title: "Sep",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Sep + "</span>";
                        },
                    },
                    {
                        field: "Oct",
                        title: "Oct",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Oct + "</span>";
                        },
                    },
                    {
                        field: "Nov",
                        title: "Nov",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Nov + "</span>";
                        },
                    },
                    {
                        field: "Dec",
                        title: "Dec",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Dec + "</span>";
                        },
                    },
                    {
                        field: "tot",
                        title: "Totals:",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.tot + "</span>";
                        },
                    },

                ],
                noRecords: { template: "No records found" },
                scrollable: true,
                sortable: true,
            };
        };

        $scope.SalesSummary_LastYear = function (data) {
            $scope.SalesSummary_LastYear_options = {
                cache: false,
                excel: {
                    fileName: "SalesSummary.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: $scope.LastYearDataSource,
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    //var myDiv = $('.k-grid-content.k-auto-scrollable');
                    //if (myDiv[0]) {
                    //    myDiv[0].scrollLeft = 0;
                    //}
                },
                resizable: true,
                columns: [
                    {
                        field: "ThisYear",
                        title: "LAST YEAR",
                        width: "100px",
                    },
                    {
                        field: "Jan",
                        title: "Jan",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jan + "</span>";
                        },
                    },
                    {
                        field: "Feb",
                        title: "Feb",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Feb + "</span>";
                        },
                    },
                    {
                        field: "Mar",
                        title: "Mar",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Mar + "</span>";
                        },
                    },
                    {
                        field: "Apr",
                        title: "Apr",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Apr + "</span>";
                        },
                    },
                    {
                        field: "May",
                        title: "May",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.May + "</span>";
                        },
                    },
                    {
                        field: "Jun",
                        title: "Jun",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jun + "</span>";
                        },
                    },
                    {
                        field: "Jul",
                        title: "Jul",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jul + "</span>";
                        },
                    },
                    {
                        field: "Aug",
                        title: "Aug",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Aug + "</span>";
                        },
                    },
                    {
                        field: "Sep",
                        title: "Sep",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Sep + "</span>";
                        },
                    },
                    {
                        field: "Oct",
                        title: "Oct",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Oct + "</span>";
                        },
                    },
                    {
                        field: "Nov",
                        title: "Nov",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Nov + "</span>";
                        },
                    },
                    {
                        field: "Dec",
                        title: "Dec",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Dec + "</span>";
                        },
                    },
                    {
                        field: "tot",
                        title: "Totals:",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.tot + "</span>";
                        },
                    },

                ],
                noRecords: { template: "No records found" },
                scrollable: true,
                sortable: true,
            };
        };

        $scope.SalesSummary_TwoYearsOld = function (data) {
            $scope.SalesSummary_TwoYearsOld_options = {
                excel: {
                    fileName: "SalesSummary.xlsx",
                },
                dataSource: $scope.TwoYearsOldDataSource,
                resizable: true,
                columns: [
                    {
                        field: "ThisYear",
                        //headerTemplate: '<label title="' + dataItem.Year + '" class="report_tooltip">"' + dataItem.Year + '"</label>',
                        width: "100px"
                    },
                    {
                        field: "Jan",
                        title: "Jan",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jan + "</span>";
                        },
                    },
                    {
                        field: "Feb",
                        title: "Feb",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Feb + "</span>";
                        },
                    },
                    {
                        field: "Mar",
                        title: "Mar",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Mar + "</span>";
                        },
                    },
                    {
                        field: "Apr",
                        title: "Apr",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Apr + "</span>";
                        },
                    },
                    {
                        field: "May",
                        title: "May",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.May + "</span>";
                        },
                    },
                    {
                        field: "Jun",
                        title: "Jun",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jun + "</span>";
                        },
                    },
                    {
                        field: "Jul",
                        title: "Jul",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jul + "</span>";
                        },
                    },
                    {
                        field: "Aug",
                        title: "Aug",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Aug + "</span>";
                        },
                    },
                    {
                        field: "Sep",
                        title: "Sep",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Sep + "</span>";
                        },
                    },
                    {
                        field: "Oct",
                        title: "Oct",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Oct + "</span>";
                        },
                    },
                    {
                        field: "Nov",
                        title: "Nov",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Nov + "</span>";
                        },
                    },
                    {
                        field: "Dec",
                        title: "Dec",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Dec + "</span>";
                        },
                    },
                    {
                        field: "tot",
                        title: "Totals:",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.tot + "</span>";
                        },
                    },

                ],
                noRecords: { template: "No records found" },
                scrollable: true,
                sortable: true,
                dataBound: function (e) {
                    if (this.dataSource.view().length > 0)
                        $("#gridSalesSummary_TwoYearsOld thead [data-field=ThisYear] .k-link").html(this.dataSource.view()[0].Year);
                }
            };
        };

        $scope.SalesSummary_ThreeYearsOld = function (data) {
            $scope.SalesSummary_ThreeYearsOld_options = {
                excel: {
                    fileName: "SalesSummary.xlsx",
                },
                dataSource: $scope.ThreeYearsOldDataSource,
                resizable: true,
                columns: [
                    {
                        field: "ThisYear",
                        //headerTemplate: '<label title="' + dataItem.Year + '" class="report_tooltip">"' + dataItem.Year + '"</label>',
                        width: "100px"
                    },
                    {
                        field: "Jan",
                        title: "Jan",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jan + "</span>";
                        },
                    },
                    {
                        field: "Feb",
                        title: "Feb",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Feb + "</span>";
                        },
                    },
                    {
                        field: "Mar",
                        title: "Mar",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Mar + "</span>";
                        },
                    },
                    {
                        field: "Apr",
                        title: "Apr",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Apr + "</span>";
                        },
                    },
                    {
                        field: "May",
                        title: "May",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.May + "</span>";
                        },
                    },
                    {
                        field: "Jun",
                        title: "Jun",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jun + "</span>";
                        },
                    },
                    {
                        field: "Jul",
                        title: "Jul",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jul + "</span>";
                        },
                    },
                    {
                        field: "Aug",
                        title: "Aug",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Aug + "</span>";
                        },
                    },
                    {
                        field: "Sep",
                        title: "Sep",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Sep + "</span>";
                        },
                    },
                    {
                        field: "Oct",
                        title: "Oct",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Oct + "</span>";
                        },
                    },
                    {
                        field: "Nov",
                        title: "Nov",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Nov + "</span>";
                        },
                    },
                    {
                        field: "Dec",
                        title: "Dec",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Dec + "</span>";
                        },
                    },
                    {
                        field: "tot",
                        title: "Totals:",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.tot + "</span>";
                        },
                    },

                ],
                noRecords: { template: "No records found" },
                scrollable: true,
                sortable: true,
                dataBound: function (e) {
                    if (this.dataSource.view().length>0)
                        $("#gridSalesSummary_ThreeYearsOld thead [data-field=ThisYear] .k-link").html(this.dataSource.view()[0].Year);
                    //$("#gridSalesTaxSummary thead [data-field=ReportColValue] .k-link").html("Zip");
                }
            };
        };

        $scope.SalesSummary_FourYearsOld = function (data) {
            $scope.SalesSummary_FourYearsOld_options = {
                excel: {
                    fileName: "SalesSummary.xlsx",
                },
                dataSource: $scope.FourYearsOldDataSource,
                resizable: true,
                columns: [
                    {
                        field: "ThisYear",
                        //headerTemplate: '<label title="' + dataItem.Year + '" class="report_tooltip">"' + dataItem.Year + '"</label>',
                        width: "100px"
                    },
                    {
                        field: "Jan",
                        title: "Jan",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jan + "</span>";
                        },
                    },
                    {
                        field: "Feb",
                        title: "Feb",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Feb + "</span>";
                        },
                    },
                    {
                        field: "Mar",
                        title: "Mar",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Mar + "</span>";
                        },
                    },
                    {
                        field: "Apr",
                        title: "Apr",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Apr + "</span>";
                        },
                    },
                    {
                        field: "May",
                        title: "May",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.May + "</span>";
                        },
                    },
                    {
                        field: "Jun",
                        title: "Jun",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jun + "</span>";
                        },
                    },
                    {
                        field: "Jul",
                        title: "Jul",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Jul + "</span>";
                        },
                    },
                    {
                        field: "Aug",
                        title: "Aug",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Aug + "</span>";
                        },
                    },
                    {
                        field: "Sep",
                        title: "Sep",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Sep + "</span>";
                        },
                    },
                    {
                        field: "Oct",
                        title: "Oct",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Oct + "</span>";
                        },
                    },
                    {
                        field: "Nov",
                        title: "Nov",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Nov + "</span>";
                        },
                    },
                    {
                        field: "Dec",
                        title: "Dec",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Dec + "</span>";
                        },
                    },
                    {
                        field: "tot",
                        title: "Totals:",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.tot + "</span>";
                        },
                    },

                ],
                noRecords: { template: "No records found" },
                scrollable: true,
                sortable: true,
                dataBound: function (e) {
                    if (this.dataSource.view().length > 0)
                        $("#gridSalesSummary_FourYearsOld thead [data-field=ThisYear] .k-link").html(this.dataSource.view()[0].Year);
                }
            };
        };
        //------SalesSummary

        $scope.VendorSalesPerformanceReport = function (data) {
            $scope.VendorSalesPerformanceReport_options = {
                cache: false,
                excel: {
                    fileName: "VendorPerformanceReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getVendorSalesPerformance',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    VendorID: $scope.ReportFilter.VendorID,
                                    PersonID: $scope.ReportFilter.PersonID,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate),
                                    ProductCategory: $scope.ReportFilter.ProductCategory
                                }
                                return data;
                            }
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                VendorName: { type: "string" },
                                ProductCategory: { type: "string" },
                                COGS_Jan: { type: "number" },
                                TotSale_Jan: { type: "number" },
                                GPValue_Jan: { type: "number" },
                                GPPerc_Jan: { type: "number" },
                                COGS_Feb: { type: "number" },
                                TotSale_Feb: { type: "number" },
                                GPValue_Feb: { type: "number" },
                                GPPerc_Feb: { type: "number" },
                                COGS_Mar: { type: "number" },
                                TotSale_Mar: { type: "number" },
                                GPValue_Mar: { type: "number" },
                                GPPerc_Mar: { type: "number" },
                                COGS_Apr: { type: "number" },
                                TotSale_Apr: { type: "number" },
                                GPValue_Apr: { type: "number" },
                                GPPerc_Apr: { type: "number" },
                                COGS_May: { type: "number" },
                                TotSale_May: { type: "number" },
                                GPValue_May: { type: "number" },
                                GPPerc_May: { type: "number" },
                                COGS_Jun: { type: "number" },
                                TotSale_Jun: { type: "number" },
                                GPValue_Jun: { type: "number" },
                                GPPerc_Jun: { type: "number" },
                                COGS_Jul: { type: "number" },
                                TotSale_Jul: { type: "number" },
                                GPValue_Jul: { type: "number" },
                                GPPerc_Jul: { type: "number" },
                                COGS_Aug: { type: "number" },
                                TotSale_Aug: { type: "number" },
                                GPValue_Aug: { type: "number" },
                                GPPerc_Aug: { type: "number" },
                                COGS_Sep: { type: "number" },
                                TotSale_Sep: { type: "number" },
                                GPValue_Sep: { type: "number" },
                                GPPerc_Sep: { type: "number" },
                                COGS_Oct: { type: "number" },
                                TotSale_Oct: { type: "number" },
                                GPValue_Oct: { type: "number" },
                                GPPerc_Oct: { type: "number" },
                                COGS_Nov: { type: "number" },
                                TotSale_Nov: { type: "number" },
                                GPValue_Nov: { type: "number" },
                                GPPerc_Nov: { type: "number" },
                                COGS_Dec: { type: "number" },
                                TotSale_Dec: { type: "number" },
                                GPValue_Dec: { type: "number" },
                                GPPerc_Dec: { type: "number" },
                                TotSale: { type: "number" },
                            }
                        }
                    },

                    aggregate: [{ field: "COGS_Jan", aggregate: "sum" },
                    { field: "TotSale_Jan", aggregate: "sum" },
                    { field: "GPValue_Jan", aggregate: "sum" },
                    { field: "GPPerc_Jan", aggregate: "sum" },
                    { field: "COGS_Feb", aggregate: "sum" },
                    { field: "TotSale_Feb", aggregate: "sum" },
                    { field: "GPValue_Feb", aggregate: "sum" },
                    { field: "GPPerc_Feb", aggregate: "sum" },
                    { field: "COGS_Mar", aggregate: "sum" },
                    { field: "TotSale_Mar", aggregate: "sum" },
                    { field: "GPValue_Mar", aggregate: "sum" },
                    { field: "GPPerc_Mar", aggregate: "sum" },
                    { field: "COGS_Apr", aggregate: "sum" },
                    { field: "TotSale_Apr", aggregate: "sum" },
                    { field: "GPValue_Apr", aggregate: "sum" },
                    { field: "GPPerc_Apr", aggregate: "sum" },
                    { field: "COGS_May", aggregate: "sum" },
                    { field: "TotSale_May", aggregate: "sum" },
                    { field: "GPValue_May", aggregate: "sum" },
                    { field: "GPPerc_May", aggregate: "sum" },
                    { field: "COGS_Jun", aggregate: "sum" },
                    { field: "TotSale_Jun", aggregate: "sum" },
                    { field: "GPValue_Jun", aggregate: "sum" },
                    { field: "GPPerc_Jun", aggregate: "sum" },
                    { field: "COGS_Jul", aggregate: "sum" },
                    { field: "TotSale_Jul", aggregate: "sum" },
                    { field: "GPValue_Jul", aggregate: "sum" },
                    { field: "GPPerc_Jul", aggregate: "sum" },
                    { field: "COGS_Aug", aggregate: "sum" },
                    { field: "TotSale_Aug", aggregate: "sum" },
                    { field: "GPValue_Aug", aggregate: "sum" },
                    { field: "GPPerc_Aug", aggregate: "sum" },
                    { field: "COGS_Sep", aggregate: "sum" },
                    { field: "TotSale_Sep", aggregate: "sum" },
                    { field: "GPValue_Sep", aggregate: "sum" },
                    { field: "GPPerc_Sep", aggregate: "sum" },
                    { field: "COGS_Oct", aggregate: "sum" },
                    { field: "TotSale_Oct", aggregate: "sum" },
                    { field: "GPValue_Oct", aggregate: "sum" },
                    { field: "GPPerc_Oct", aggregate: "sum" },
                    { field: "COGS_Nov", aggregate: "sum" },
                    { field: "TotSale_Nov", aggregate: "sum" },
                    { field: "GPValue_Nov", aggregate: "sum" },
                    { field: "GPPerc_Nov", aggregate: "sum" },
                    { field: "COGS_Dec", aggregate: "sum" },
                    { field: "TotSale_Dec", aggregate: "sum" },
                    { field: "GPValue_Dec", aggregate: "sum" },
                    { field: "GPPerc_Dec", aggregate: "sum" },
                    { field: "TotSale", aggregate: "sum" }],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    if ($("#gridVendorSalesPerformance").find(".k-grid-header").next()[0] != $("#gridVendorSalesPerformance").find(".k-grid-footer")[0])
                        $("#gridVendorSalesPerformance").find(".k-grid-footer").insertAfter($("#gridVendorSalesPerformance .k-grid-header"));
                    // var data = this.dataSource._data;
                    //for (var i = 0; i < data.length; i++) { // to hide YTD total sales $0 vendors
                    //    if (data[i].TotSale == 0.0 || data[i].TotSale == 0) {
                    //        e.preventDefault();
                    //        var grid = $("#gridVendorSalesPerformance").data("kendoGrid");
                    //        grid.tbody.find("tr[data-uid=" + data[i].uid + "]").hide()
                    //    }
                    //}
                    //var myDiv = $('.k-grid-content.k-auto-scrollable');
                    //if (myDiv[0]) {
                    //    myDiv[0].scrollLeft = 0;
                    //}
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length - 1; cellIndex++) {
                            var cell = row.cells[cellIndex + 1];
                            if (row.type === "data") {
                                if (sheet.rows[2].cells[cellIndex].value == "COGS" || sheet.rows[2].cells[cellIndex].value == "Total Sales $" || sheet.rows[2].cells[cellIndex].value == "GP $") {
                                    cell.value = parseFloat(cell.value);
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[2].cells[cellIndex].value == "GP %") {
                                    cell.value = parseFloat(cell.value);
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "VendorName",
                        title: "Vendor",
                        filterable: { search: true },
                        headerTemplate: '<label title="Vendor" class="report_tooltip">Vendor</label>',
                        locked: true,
                        width: "180px",

                    },
                    {
                        field: "TotSale",
                        title: "YTD Total Sales $",
                        filterable: { search: true },
                        headerTemplate: '<label title="YTD Total Sales $" class="report_tooltip">YTD Total Sales $</label>',
                        locked: true,
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",

                    },
                    {
                        field: "ProductCategory",
                        title: "Product Category",
                        filterable: { search: true },
                        headerTemplate: '<label title="Product Category" class="report_tooltip">Product Category</label>',
                        locked: true,
                        width: "100px",
                    },
                    {
                        title: "Jan",
                        columns: [
                            {
                                field: "COGS_Jan",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Jan" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Jan) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",// "<span class='Grid_Textalign'>#=kendo.toString(sum)#</span>",
                            },
                            {
                                field: "TotSale_Jan",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Jan" class="report_tooltip">Total Sales $</label>',
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Jan) + "</span>";
                                },
                                width: "100px",
                                height: "100px",
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Jan",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Jan" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Jan) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Jan",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Jan" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Jan.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Jan.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Jan.sum - aggregates.COGS_Jan.sum) / aggregates.TotSale_Jan.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Feb",
                        columns: [
                            {
                                field: "COGS_Feb",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Feb" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Feb) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Feb",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Feb" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Feb) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Feb",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Feb" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Feb) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Feb",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Feb" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Feb.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Feb.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Feb.sum - aggregates.COGS_Feb.sum) / aggregates.TotSale_Feb.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Mar",
                        columns: [
                            {
                                field: "COGS_Mar",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Mar" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Mar) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Mar",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Mar" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Mar) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Mar",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Mar" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Mar) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Mar",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Mar" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Mar.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Mar.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Mar.sum - aggregates.COGS_Mar.sum) / aggregates.TotSale_Mar.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Apr",
                        columns: [
                            {
                                field: "COGS_Apr",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Apr" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Apr) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Apr",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Apr" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Apr) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Apr",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Apr" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Apr) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Apr",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Apr" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Apr.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Apr.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Apr.sum - aggregates.COGS_Apr.sum) / aggregates.TotSale_Apr.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "May",
                        columns: [
                            {
                                field: "COGS_May",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_May" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_May) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_May",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_May" class="report_tooltip">Total Sales $"</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_May) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_May",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_May" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_May) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_May",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_May" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_May.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_May.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_May.sum - aggregates.COGS_May.sum) / aggregates.TotSale_May.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Jun",
                        columns: [
                            {
                                field: "COGS_Jun",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Jun" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Jun) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Jun",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Jun" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Jun) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Jun",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Jun" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Jun) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Jun",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Jun" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Jun.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Jun.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Jun.sum - aggregates.COGS_Jun.sum) / aggregates.TotSale_Jun.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Jul",
                        columns: [
                            {
                                field: "COGS_Jul",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Jul" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Jul) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Jul",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Jul" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Jul) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Jun",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Jun" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Jun.toFixed(2) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Jul",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Jul" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Jul.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Jul.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Jul.sum - aggregates.COGS_Jul.sum) / aggregates.TotSale_Jul.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Aug",
                        columns: [
                            {
                                field: "COGS_Aug",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Aug" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Aug) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Aug",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Aug" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Aug) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Aug",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Aug" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Aug) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Aug",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Aug" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Aug.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Aug.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Aug.sum - aggregates.COGS_Aug.sum) / aggregates.TotSale_Aug.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Sep",
                        columns: [
                            {
                                field: "COGS_Sep",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Sep" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Sep) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Sep",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Sep" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Sep) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Sep",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Sep" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Sep) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Sep",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Sep" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Sep.toFixed(2); + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Sep.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Sep.sum - aggregates.COGS_Sep.sum) / aggregates.TotSale_Sep.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Oct",
                        columns: [
                            {
                                field: "COGS_Oct",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Oct" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Oct) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Oct",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Oct" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Oct) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Oct",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Oct" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Oct) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Oct",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Oct" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Oct.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Oct.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Oct.sum - aggregates.COGS_Oct.sum) / aggregates.TotSale_Oct.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Nov",
                        columns: [
                            {
                                field: "COGS_Nov",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Nov" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Nov) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Nov",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Nov" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Nov) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Nov",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Nov" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Nov) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Nov",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Nov" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Nov.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Nov.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Nov.sum - aggregates.COGS_Nov.sum) / aggregates.TotSale_Nov.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: "Dec",
                        columns: [
                            {
                                field: "COGS_Dec",
                                title: "COGS",
                                headerTemplate: '<label title="COGS_Dec" class="report_tooltip">COGS</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS_Dec) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "TotSale_Dec",
                                title: "Total Sales $",
                                headerAttributes: { style: "white-space: normal" },
                                headerTemplate: '<label title="TotSale_Dec" class="report_tooltip">Total Sales $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotSale_Dec) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPValue_Dec",
                                title: "GP $",
                                headerTemplate: '<label title="GPValue_Dec" class="report_tooltip">GP $</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue_Dec) + "</span>";
                                },
                                footerTemplate: "#=kendo.toString(sum, 'C')#",
                            },
                            {
                                field: "GPPerc_Dec",
                                title: "GP %",
                                headerTemplate: '<label title="GPPerc_Dec" class="report_tooltip">GP %</label>',
                                width: "100px",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.GPPerc_Dec.toFixed(2) + " %</span>";
                                },
                                footerTemplate: function () {
                                    var ds = $("#gridVendorSalesPerformance").data("kendoGrid").dataSource;
                                    var aggregates = ds.aggregates();
                                    if (aggregates.TotSale_Dec.sum > 0) {
                                        var gpPer = ((aggregates.TotSale_Dec.sum - aggregates.COGS_Dec.sum) / aggregates.TotSale_Dec.sum) * 100;
                                        return gpPer.toFixed(2) + "%";
                                    } else {
                                        return "0%";
                                    }
                                }
                            }
                        ]
                    },

                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "VendorName", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        // end kendo table Marketing Performance resizing
        $scope.MarketingPerformanceReport = function (data) {
            $scope.MarketingPerformanceReport_options = {
                cache: false,
                excel: {
                    fileName: "MarketingPerformanceReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                //height: 650,
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getMarketingPerformance',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    SourceId: $scope.ReportFilter.SourceId,
                                    campaignId: $scope.ReportFilter.campaignId,
                                    IndustryId: $scope.ReportFilter.IndustryId,
                                    CommercialTypeId: $scope.ReportFilter.CommercialTypeId,
                                    Zip: $scope.ReportFilter.Zip,
                                    PersonID: $scope.ReportFilter.PersonID,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                Source: { type: "string" },
                                Campaign: { type: "string" },
                                ParentCampaign: { type: "string" },
                                Leads: { type: "number" },
                                Accounts: { type: "number" },
                                Opportunities: { type: "number" },
                                AdSpend: { type: "number" },
                                CostperRawLead: { type: "number" },
                                Appointments: { type: "number" },
                                AppointmentRatio: { type: "number" },
                                AppointmentsCancelled: { type: "number" },
                                AppointmentsCancelledRatio: { type: "number" },
                                Orders: { type: "number" },
                                ClosingRatio: { type: "number" },
                                TotalSales: { type: "number" },
                                ForROAI: { type: "number" },
                                TotalROAI: { type: "number" },
                                AvgSale: { type: "number" }
                            }
                        }
                    },
                    aggregate: [{ field: "Leads", aggregate: "sum" },
                    { field: "Accounts", aggregate: "sum" },
                    { field: "Opportunities", aggregate: "sum" },
                    { field: "AdSpend", aggregate: "sum" },
                    //{ field: "CostperRawLead", aggregate: "average" },
                    { field: "Appointments", aggregate: "sum" },
                    { field: "AppointmentRatio", aggregate: "average" },
                    { field: "AppointmentsCancelled", aggregate: "sum" },
                    { field: "AppointmentsCancelledRatio", aggregate: "average" },
                    { field: "Orders", aggregate: "sum" },
                    { field: "ClosingRatio", aggregate: "average" },
                    { field: "TotalSales", aggregate: "sum" },
                    //{ field: "ForROAI", aggregate: "average" },
                    //{ field: "TotalROAI", aggregate: "sum" },
                    { field: "AvgSale", aggregate: "average" }],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                dataBound: function (e) {
                    $("#gridMarketingPerformance").data("kendoGrid").autoFitColumn("Source");
                    var dataSource = $("#gridMarketingPerformance").data("kendoGrid").dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: dataSource.data(),
                        filter: dataSource.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();
                    if (data != undefined && data.length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    //$("#gridMarketingPerformance").find(".k-grid-footer").insertAfter($("#gridMarketingPerformance .k-grid-header"))
                    if ($("#gridMarketingPerformance").find(".k-grid-header").next()[0] != $("#gridMarketingPerformance").find(".k-grid-footer")[0])
                        $("#gridMarketingPerformance").find(".k-grid-footer").insertAfter($("#gridMarketingPerformance .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "TotalSales" || sheet.rows[0].cells[cellIndex].value == "AdSpend" || sheet.rows[0].cells[cellIndex].value == "CostperRawLead" || sheet.rows[0].cells[cellIndex].value == "ForROAI" || sheet.rows[0].cells[cellIndex].value == "TotalROAI" || sheet.rows[0].cells[cellIndex].value == "AvgSale") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "AppointmentsCancelledRatio" || sheet.rows[0].cells[cellIndex].value == "AppointmentRatio" || sheet.rows[0].cells[cellIndex].value == "ClosingRatio") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                            if (row.type === "footer") {
                                cell.hAlign = "right";
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "Source",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Source" class="report_tooltip">Source</label>',
                        filterable: { search: true },
                        locked: true,
                        width: "120px"
                    },
                    {
                        field: "Campaign",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Campaign" class="report_tooltip">Campaign</label>',
                        filterable: { search: true },
                        locked: true,
                        width: "120px"
                    },
                    //{
                    //    field: "ParentCampaign ",
                    //    headerTemplate: '<label  title="Parent Campaign" class="tooltip-top" >Parent Campaign</label>',
                    //    filterable: { multi: true, search: true },
                    //    width: "160px"
                    //},
                    {
                        field: "Leads",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Count of Raw Leads" class="report_tooltip">Total Raw Leads #</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        template: function (dataItem) {
                            if (dataItem.Leads)
                                return "<span class='Grid_Textalign'>" + dataItem.Leads + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "170px",
                    },
                    {
                        field: "Accounts",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Raw Accounts" class="report_tooltip">Accounts Created #</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        template: function (dataItem) {
                            if (dataItem.Accounts)
                                return "<span class='Grid_Textalign'>" + dataItem.Accounts + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "170px",
                    },
                    {
                        field: "Opportunities",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Opportunities" class="report_tooltip">Opportunity #</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        template: function (dataItem) {
                            if (dataItem.Opportunities)
                                return "<span class='Grid_Textalign'>" + dataItem.Opportunities + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "150px",
                    },
                    {
                        field: "AdSpend",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Ad Spend" class="report_tooltip">Ad Spend $</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AdSpend) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px",
                    },
                    {
                        field: "CostperRawLead",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Cost Per Raw Lead [Ad Spend / Count of Raw Leads]" class="report_tooltip">Cost per Raw Lead  $</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.CostperRawLead) + "</span>";
                        },
                        //aggregates: ["average"],
                        footerTemplate: function () {
                            var ds = $("#gridMarketingPerformance").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Leads.sum > 0) {
                                var agg = (aggregates.AdSpend.sum / aggregates.Leads.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + "";
                            } else {
                                return "$0.00";
                            }
                        },
                        width: "150px",
                    },
                    {
                        field: "Appointments",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of First Appointments" class="report_tooltip">Appointments Scheduled #</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        template: function (dataItem) {
                            if (dataItem.Appointments)
                                return "<span class='Grid_Textalign'>" + dataItem.Appointments + "</span>";
                            else
                                return "";
                        },

                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "180px"
                    },
                    {
                        field: "AppointmentRatio",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Lead to Appointment Ratio [Count of 1st Appts / Count of Raw Leads]" class="report_tooltip">Lead to Appointment Ratio %</label>',
                        filterable: { search: true },
                        aggregates: ["average"],
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p2}", AppointmentRatio/100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridMarketingPerformance").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Leads.sum > 0) {
                                var agg = (aggregates.Appointments.sum / aggregates.Leads.sum) * 100;
                                return  ""+agg.toFixed(2) + "%";
                            } else {
                                return "0.00%";
                            }
                        },
                        width: "180px"
                    },
                    {
                        field: "AppointmentsCancelled",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of 1st Appts Cancelled" class="report_tooltip">Appointments Cancelled #</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        template: function (dataItem) {
                            if (dataItem.AppointmentsCancelled)
                                return "<span class='Grid_Textalign'>" + dataItem.AppointmentsCancelled + "</span>";
                            else
                                return "";
                        },

                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "200px"
                    },
                    {
                        field: "AppointmentsCancelledRatio",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average cancelation of 1st Appointments [Count of 1st Appts Canceled / Count of 1st Appts]" class="report_tooltip">Appointment Cancel Ratio %</label>',
                        filterable: { search: true },
                        aggregates: ["average"],
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p2}", AppointmentsCancelledRatio/100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridMarketingPerformance").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Appointments.sum > 0) {
                                var agg = (aggregates.AppointmentsCancelled.sum / aggregates.Appointments.sum) * 100;
                                return "" + agg.toFixed(2) + "%";
                            } else {
                                return "0.00%";
                            }
                        },
                        width: "200px"
                    },
                    {
                        field: "Orders",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Count of Orders" class="report_tooltip">Orders #</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        template: function (dataItem) {
                            if (dataItem.Orders)
                                return "<span class='Grid_Textalign'>" + dataItem.Orders + "</span>";
                            else
                                return "";
                        },
                        footerTemplate: "#=kendo.toString(sum)#",
                        width: "150px"
                    },
                    {
                        field: "ClosingRatio",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Closing Ratio [Count of Orders / Count of 1st Appts" class="report_tooltip">Closing Ratio %</label>',
                        filterable: { search: true },
                        aggregates: ["average"],
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p2}", ClosingRatio/100)#</span>',
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        footerTemplate: function () {
                            var ds = $("#gridMarketingPerformance").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Appointments.sum > 0) {
                                var agg = (aggregates.Orders.sum / aggregates.Appointments.sum) * 100;
                                return "" + agg.toFixed(2) + "%";
                            } else {
                                return "0.00%";
                            }
                        },
                        width: "150px"
                    },
                    {
                        field: "TotalSales",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales" class="report_tooltip">Total Sales</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "ForROAI",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Dollar for Dollar Return On Ad Investment [Total Sales / Total Ad Spend]" class="report_tooltip" >$ for $ - ROAI</label>',
                        filterable: { search: true },
                        //aggregates: ["sum"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.ForROAI) + "</span>";
                        },
                        footerTemplate: function () {
                            var ds = $("#gridMarketingPerformance").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.AdSpend.sum > 0) {
                                var agg = (aggregates.TotalSales.sum / aggregates.AdSpend.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + "";
                            } else {
                                return "$0.00";
                            }
                        },
                        width: "150px",
                    },
                    {
                        field: "TotalROAI",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Return On Ad Investment [Total Sales - Total Ad Spend]" class="report_tooltip">Total ROAI</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalROAI) + "</span>";
                        },
                        //aggregates: ["sum"],
                        //footerTemplate: "#=kendo.toString(sum)#",
                        footerTemplate: function () {
                            var ds = $("#gridMarketingPerformance").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            var agg = (aggregates.TotalSales.sum - aggregates.AdSpend.sum);
                            return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + "";
                        },
                        width: "150px",
                    },
                    {
                        field: "AvgSale",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Average Sale [Total Sales / Count of Orders]" class="report_tooltip">Avg Sale</label>',
                        filterable: { search: true },
                        aggregates: ["average"],
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AvgSale) + "</span>";
                        },
                        //footerTemplate: "#=kendo.toString(average/100, 'C')#",
                        footerTemplate: function () {
                            var ds = $("#gridMarketingPerformance").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.Orders.sum > 0) {
                                var agg = (aggregates.TotalSales.sum / aggregates.Orders.sum);
                                return "$" + agg.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + "";;
                            } else {
                                return "$0";
                            }
                        },
                        width: "150px",
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "Source", dir: "asc" }),
                sortable: true,
                groupable: true,
                scrollable: true,
                resizable: true
            };
        };

        //SalesTaxSummaryReport

        $scope.SalesTaxSummaryReport = function (data) {
            $scope.SalesTaxSummaryReport_options = {
                excel: {
                    fileName: "SalesTaxSummary.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getSalesTaxSummary',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate),
                                    ReportType: $scope.ReportFilter.ST_ReportType == "Tax Code" ? "Code" : $scope.ReportFilter.ST_ReportType,
                                }
                                return data;
                            }
                        }
                    },
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                excelExport: function (e) {
                    e.workbook.fileName = "SalesTaxSummary_" + $scope.ReportFilter.ST_ReportType + ".xlsx";
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "Tax Amount" || sheet.rows[0].cells[cellIndex].value == "Amount Taxable") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Rate") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                //dataBound: function (e) {
                //    $scope.ST_ReportTypeChange();
                //},
                columns: [
                    {
                        field: "ReportColValue",
                        title: "Special",
                        headerTemplate: function () {
                            return '<label title="' + $scope.ReportFilter.ST_ReportType + '" class="report_tooltip">' + $scope.ReportFilter.ST_ReportType + '</label>';
                        },
                        width: "50px",
                    },
                    {
                        field: "TaxableAmount",
                        type: "number",
                        title: "Taxable Amount",
                        headerTemplate: '<label title="Taxable Amount" class="report_tooltip">Taxable Amount</label>',
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TaxableAmount) + "</span>";
                        },
                        width: "50px"
                    },
                    {
                        field: "Rate",
                        title: "Rate",
                        type: "number",
                        headerTemplate: '<label title="Rate" class="report_tooltip">Rate</label>',
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", Rate / 100)#</span>',
                        width: "50px"
                    },
                    {
                        field: "Amount",
                        title: "Tax Amount",
                        type: "number",
                        headerTemplate: '<label title="Tax Amount" class="report_tooltip">Tax Amount</label>',
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                        },
                        width: "50px"
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "Source", dir: "asc" }),
                sortable: true,
                groupable: true,
                scrollable: true,
                resizable: true
            };
        };

        $scope.SalesTax = function (data) {
            $scope.SalesTaxReport_options = {
                excel: {
                    fileName: "SalesTax.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getSalesTax',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    },
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "Tax Amount" || sheet.rows[0].cells[cellIndex].value == "Amount Taxable") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Rate") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Jurisdiction Type") {
                                    if (cell.value == "State") {
                                        cell.value = "State/Province";
                                    }
                                }
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "ZipCode",
                        title: "Zip/Postal",
                        headerTemplate: '<label title="Zip/Postal" class="report_tooltip">Zip/Postal</label>',
                        width: "50px",
                    },
                    {
                        field: "OrderNumber",
                        title: "Order Number",
                        headerTemplate: '<label title="Order Number" class="report_tooltip">Order Number</label>',
                        template: function (dataItem) {
                            return "<a href='/#!/Orders/" + dataItem.OrderID + "' style='color: rgb(61,125,139);' target='_blank'>" + dataItem.OrderNumber + "</a>";
                        },
                        width: "75px"
                    },
                    {
                        field: "JurisType",
                        title: "Jurisdiction Type",
                        headerTemplate: '<label title="Jurisdiction Type" class="report_tooltip">Jurisdiction Type</label>',
                        template: function (dataItem) {
                            if (dataItem.JurisType == "State") {
                                return "State/Province";
                            }
                            return dataItem.JurisType;
                        },
                        width: "75px"
                    },
                    {
                        field: "Jurisdiction",
                        title: "Jurisdiction",
                        headerTemplate: '<label title="Jurisdiction" class="report_tooltip">Jurisdiction</label>',
                        width: "50px"
                    },
                    {
                        field: "Code",
                        title: "Tax Code",
                        headerTemplate: '<label title="Tax Code" class="report_tooltip">Tax Code</label>',
                        width: "50px"
                    },
                    {
                        field: "TaxableAmount",
                        type: "number",
                        title: "Amount Taxable",
                        headerTemplate: '<label title="Amount Taxable" class="report_tooltip">Amount Taxable</label>',
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TaxableAmount) + "</span>";
                        },
                        width: "100px"
                    },
                    {
                        field: "Rate",
                        title: "Rate",
                        type: "number",
                        headerTemplate: '<label title="Rate" class="report_tooltip">Rate</label>',
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", Rate / 100)#</span>',
                        width: "100px"
                    },
                    {
                        field: "Amount",
                        title: "Tax Amount",
                        type: "number",
                        headerTemplate: '<label title="Tax Amount" class="report_tooltip">Tax Amount</label>',
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                        },
                        width: "150px"
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "Source", dir: "asc" }),
                sortable: true,
                groupable: true,
                scrollable: true,
                resizable: true
            };
        };
        //SalesSummaryDetailReport
        $scope.SalesSummaryDetailReport = function (data) {
            $scope.SalesSummary_Detail_Report = {
                cache: false,
                excel: {
                    fileName: "SalesSummaryDetailReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getSalesSummaryDetail',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    PersonID: $scope.ReportFilter.PersonID,
                                    IndustryId: $scope.ReportFilter.IndustryId,
                                    CommercialTypeId: $scope.ReportFilter.CommercialTypeId,
                                    CustomerName: $scope.ReportFilter.CustomerName,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                FirstName: { type: "string" },
                                LastName: { type: "string" },
                                Company: { type: "string" },
                                SalesPerson: { type: "string" },
                                Source: { type: "string" },
                                Campaign: { type: "string" },
                                OrderNumber: { type: "number" },
                                OrderStatus: { type: "string" },
                                MasterPONumber: { type: "number" },
                                MPOStatus: { type: "string" },
                                OrderDate: { type: "date" },
                                TotalSales: { type: "number" },
                                COGS: { type: "number" },
                                GPValue: { type: "number" },
                                GPPercenatge: { type: "number" },
                                TotalSalesProd: { type: "number" },
                                COGSProd: { type: "number" },
                                GPValueProd: { type: "number" },
                                GPPercenatgeProd: { type: "number" },
                            }
                        }
                    },
                    aggregate: [
                        { field: "COGS", aggregate: "sum" },
                        { field: "TotalSales", aggregate: "sum" },
                        { field: "GPValue", aggregate: "sum" },
                        { field: "GPPercenatge", aggregate: "average" },
                        { field: "TotalSalesProd", aggregate: "sum" },
                        { field: "GPValueProd", aggregate: "sum" },
                        { field: "GPPercenatgeProd", aggregate: "average" },
                        { field: "COGSProd", aggregate: "sum" },
                    ],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    //$("#gridSalesSummaryDetailReport").find(".k-grid-footer").insertAfter($("#gridSalesSummaryDetailReport .k-grid-header"))
                    if ($("#gridSalesSummaryDetailReport").find(".k-grid-header").next()[0] != $("#gridSalesSummaryDetailReport").find(".k-grid-footer")[0])
                        $("#gridSalesSummaryDetailReport").find(".k-grid-footer").insertAfter($("#gridSalesSummaryDetailReport .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Total Sales" || sheet.rows[0].cells[cellIndex].value == "GP $" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Total Sales (Product)" || sheet.rows[0].cells[cellIndex].value == "GP $ (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "GP %" || sheet.rows[0].cells[cellIndex].value == "GP % (Product)") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "FirstName",
                        title: "First Name",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="First Name" class="report_tooltip">First Name</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "LastName",
                        title: "Last Name",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Last Name" class="report_tooltip">Last Name</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "Company",
                        title: "Company",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Company" class="report_tooltip">Company</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "SalesPerson",
                        title: "Sales Person",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Sales Person" class="report_tooltip">Sales Person</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "Source",
                        title: "Source",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Source" class="report_tooltip">Source</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "Campaign",
                        title: "Campaign",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Campaign" class="report_tooltip">Campaign</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "OrderNumber",
                        title: "Order #",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Order" class="report_tooltip">Order #</label>',
                        template: function (dataItem) {
                            if (dataItem.OrderNumber)
                                return "<span class='Grid_Textalign'>" + dataItem.OrderNumber + "</span>";
                            else
                                return "";
                        },
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "OrderStatus",
                        title: "Order Status",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Order Status" class="report_tooltip">Order Status</label>',
                        filterable: { multi: true, search: true },
                        width: "130px"
                    },
                    {
                        field: "MasterPONumber",
                        title: "MPO #",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="MPO" class="report_tooltip">MPO #</label>',
                        template: function (dataItem) {
                            if (dataItem.MasterPONumber)
                                return "<span class='Grid_Textalign'>" + dataItem.MasterPONumber + "</span>";
                            else
                                return "";
                        },
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "MPOStatus",
                        title: "MPO Status",

                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="MPO Status" class="report_tooltip">MPO Status</label>',
                        filterable: { multi: true, search: true },
                        width: "130px"
                    },
                    {
                        field: "OrderDate",
                        title: "Order Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Order Date" class="report_tooltip">Order Date</label>',
                        // filterable: { multi: true, search: true },
                        width: "130px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesSummaryFilterCalendar", offsetvalue),
                        //format: "{0:MM/dd/yyyy}"
                        template: function (dataItem) {
                            if (dataItem.OrderDate) {
                                return HFCService.KendoDateFormat(dataItem.OrderDate);
                            }
                            else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "TotalSales",
                        title: "Total Sales",
                        width: "130px",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Sum of Order Total amount" class="report_tooltip">Total Sales</label>',
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                    },

                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        //format: "{0:c2}",
                        width: "130px"
                    },
                    {
                        field: "GPValue",
                        title: "GP $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales - COGS" class="report_tooltip">GP $</label>',
                        filterable: { search: true },
                        // format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    //{
                    //    field: "GPPercenatge",
                    //    title: "GP %",
                    //    headerAttributes: { style: "white-space: normal" },
                    //    headerTemplate: '<label title="GP $ / Total Sales" class="report_tooltip">GP %</label>',
                    //    filterable: { multi: true, search: true },
                    //    template: '#=kendo.format("{0:p}", GPPercenatge / 100)#',
                    //    footerTemplate: "#=kendo.toString(average/100, 'p')#",
                    //    width: "130px"
                    //},
                    {
                        field: "GPPercenatge",
                        //title: "GP %",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="GP $ / Total Sales" class="report_tooltip" >GP %</label>',
                        filterable: { search: true },
                        aggregates: ["average"],
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p2}", GPPercenatge / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridSalesSummaryDetailReport").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSales.sum > 0) {
                                var gpPer = (aggregates.GPValue.sum / aggregates.TotalSales.sum) * 100;
                                return gpPer.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }

                        },
                        //footerTemplate: "#=kendo.toString(average/100, 'p')#",
                        width: "100px"
                    },
                    {
                        field: "TotalSalesProd",
                        title: "Total Sales (Product)",
                        width: "165px",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Sum of Order Total amount (Product)" class="report_tooltip">Total Sales (Product)</label>',
                        filterable: {
                            search: true
                        },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSalesProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                    },
                    {
                        field: "COGSProd",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold (Product)" class="report_tooltip">COGS (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPValueProd",
                        title: "GP $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sales - COGS (Product)" class="report_tooltip">GP $ (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.GPValueProd) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "150px"
                    },
                    {
                        field: "GPPercenatgeProd",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="GP $ / Total Sales (Product)" class="report_tooltip" >GP % (Product)</label>',
                        filterable: { search: true },
                        aggregates: ["average"],
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p2}", GPPercenatgeProd / 100)#</span>',
                        footerTemplate: function () {
                            var ds = $("#gridSalesSummaryDetailReport").data("kendoGrid").dataSource;
                            var aggregates = ds.aggregates();
                            if (aggregates.TotalSalesProd.sum > 0) {
                                var gpPer = (aggregates.GPValueProd.sum / aggregates.TotalSalesProd.sum) * 100;
                                return gpPer.toFixed(2) + "%";
                            } else {
                                return "0%";
                            }
                        },
                        width: "150px"
                    },

                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "SalesPerson", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        //OpenBalanceReport
        $scope.OpenARReport = function (data) {
            $scope.Open_AR_Report = {
                cache: false,
                excel: {
                    fileName: "OpenBalanceReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getOpenArDetail',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    PersonID: $scope.ReportFilter.PersonID,
                                    IndustryId: $scope.ReportFilter.IndustryId,
                                    CommercialTypeId: $scope.ReportFilter.CommercialTypeId,
                                    CustomerName: $scope.ReportFilter.CustomerName,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                //FirstName: { type: "string" },
                                //LastName: { type: "string" },
                                //Company: { type: "string" },
                                OrderID: { type: "number" },
                                CustName: { type: "string" },
                                AccountNumber: { type: "number" },
                                CustOrder: { type: "number" },
                                ContractDate: { type: "date" },
                                LastPaymentDate: { type: "date" },
                                Amount: { type: "number" },
                                PaymentsReceived: { type: "number" },
                                BalanceDue: { type: "number" },
                                OrderStatus: { type: "string" },
                                DaysOpen: { type: "number" },
                            }
                        }
                    },
                    aggregate: [
                        { field: "Amount", aggregate: "sum" },
                        { field: "PaymentsReceived", aggregate: "sum" },
                        { field: "BalanceDue", aggregate: "sum" },

                    ],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }


                    //$("#gridOpenARReport").find(".k-grid-footer").insertAfter($("#gridOpenARReport .k-grid-header"))
                    if ($("#gridOpenARReport").find(".k-grid-header").next()[0] != $("#gridOpenARReport").find(".k-grid-footer")[0])
                        $("#gridOpenARReport").find(".k-grid-footer").insertAfter($("#gridOpenARReport .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "Amount" || sheet.rows[0].cells[cellIndex].value == "Payments Received" || sheet.rows[0].cells[cellIndex].value == "Balance") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }

                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    //{
                    //    field: "FirstName",
                    //    title: "First Name",

                    //    headerAttributes: { style: "white-space: normal" },
                    //    headerTemplate: '<label title="First Name" class="report_tooltip">First Name</label>',
                    //    filterable: { multi: true, search: true },
                    //    width: "130px"
                    //},
                    // {
                    //     field: "LastName",
                    //     title: "Last Name",

                    //     headerAttributes: { style: "white-space: normal" },
                    //     headerTemplate: '<label title="Last Name" class="report_tooltip">Last Name</label>',
                    //     filterable: { multi: true, search: true },
                    //     width: "130px"
                    // },
                    //  {
                    //      field: "Company",
                    //      title: "Company",

                    //      headerAttributes: { style: "white-space: normal" },
                    //      headerTemplate: '<label title="Company" class="report_tooltip">Company</label>',
                    //      filterable: { multi: true, search: true },
                    //      width: "130px"
                    //  },
                    {
                        field: "CustName",
                        title: "Customer Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Customer Name" class="report_tooltip">Customer Name</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "AccountNumber",
                        title: "Account #",
                        template: function (dataItem) {
                            if (dataItem.AccountNumber)
                                return "<span class='Grid_Textalign'>" + dataItem.AccountNumber + "</span>";
                            else
                                return "";
                        },
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Account" class="report_tooltip">Account #</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "CustOrder",
                        title: "Order #",
                        template: function (dataItem) {
                            if (dataItem.CustOrder)
                                return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><a href='/#!/Orders/" + dataItem.OrderID + "' style='color: rgb(61,125,139);'>" + dataItem.CustOrder + "</a></span></span>";
                            else
                                return "";
                        },
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Order" class="report_tooltip">Order #</label>',
                        filterable: { search: true },
                        width: "130px"
                    },


                    {
                        field: "ContractDate",
                        title: "Contract Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Contract Date" class="report_tooltip">Contract Date</label>',
                        template: function (dataItem) {
                            if (dataItem.ContractDate) {
                                return HFCService.KendoDateFormat(dataItem.ContractDate);
                            }
                            else {
                                return "";
                            }
                        },
                        //filterable: { multi: true, search: true },
                        width: "130px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "OpenArContractDateFilterCalendar", offsetvalue),
                        // format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "LastPaymentDate",
                        title: "Last Payment",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Last Payment" class="report_tooltip">Last Payment</label>',
                        template: function (dataItem) {
                            if (dataItem.LastPaymentDate) {
                                return HFCService.KendoDateFormat(dataItem.LastPaymentDate);
                            }
                            else {
                                return "";
                            }
                        },
                        width: "130px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "OpenArLastPaymentDateFilterCalendar", offsetvalue),
                    },
                    {
                        field: "Amount",
                        title: "Amount",
                        width: "130px",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Amount" class="report_tooltip">Amount</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                    },

                    {
                        field: "PaymentsReceived",
                        title: "Payments Received",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Payments Received" class="report_tooltip">Payments Received</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.PaymentsReceived) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        //format: "{0:c2}",
                        width: "130px"
                    },
                    {
                        field: "BalanceDue",
                        title: "Balance",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Balance Due" class="report_tooltip">Balance</label>',
                        filterable: { search: true },
                        //format: "{0:c2}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.BalanceDue) + "</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C')#",
                        width: "130px"
                    },
                    {
                        field: "OrderStatus",
                        title: "Job Status",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Job Status" class="report_tooltip">Job Status</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "DaysOpen",
                        title: "Days Open Since Complete",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Days Open Since Job Completion" class="report_tooltip">Days Open Since Complete</label>',
                        filterable: { search: true },
                        width: "200px"
                    },

                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    //$(e.container).find('.k-check-all').click()
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "FirstName", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        //SalesJournalReprot
        $scope.SalesJournalReport = function (data) {
            $scope.SalesJournalReport_options = {
                cache: false,
                excel: {
                    fileName: "SalesJournalReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getSalesJournalDetail',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    Value: $scope.ReportFilter.Value,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                }
                                return data;
                            }
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                Territory: { type: "string" },
                                ContractedDate: { type: "date" },
                                CompletedDate: { type: "date" },
                                SalesPerson: { type: "string" },
                                OrderNo: { type: "number" },
                                //ProductType: { type: "string" },
                                COGS: { type: "number" },
                                Sale: { type: "number" },
                                COGSProduct: { type: "number" },
                                SaleProduct: { type: "number" },
                                Tax: { type: "number" },
                                Customer: { type: "string" },
                                AccountType: { type: "string" },
                                AccountNo: { type: "number" },
                                QBInvoiceNo: { type: "string" }
                            }
                        }
                    },
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    //var myDiv = $('.k-grid-content.k-auto-scrollable');
                    //if (myDiv[0]) {
                    //    myDiv[0].scrollLeft = 0;
                    //}
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];
                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Sale $" || sheet.rows[0].cells[cellIndex].value == "Tax" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Sale $ (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "Territory",
                        title: "Territory",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Territory" class="report_tooltip">Territory</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "ContractedDate",
                        title: "Contracted Date",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Contracted Date" class="report_tooltip">Contracted Date</label>',
                        width: "150px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesJournalContractedDateFilterCalendar", offsetvalue),
                        //format: "{0:MM/dd/yyyy}"
                        template: function (dataItem) {
                            if (dataItem.ContractedDate) {
                                return HFCService.KendoDateFormat(dataItem.ContractedDate);
                            }
                            else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "CompletedDate",
                        title: "Completed Date",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Completed Date" class="report_tooltip">Completed Date</label>',
                        width: "150px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "SalesJournalCompletedDateFilterCalendar", offsetvalue),
                        //format: "{0:MM/dd/yyyy}"
                        template: function (dataItem) {
                            if (dataItem.CompletedDate) {
                                return HFCService.KendoDateFormat(dataItem.CompletedDate);
                            }
                            else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "SalesPerson",
                        title: "Sales Person",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Last Name" class="report_tooltip">Sales Person</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "OrderNo",
                        title: "Order #",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Order" class="report_tooltip">Order #</label>',
                        template: function (dataItem) {
                            if (dataItem.OrderNo)
                                return "<span class='Grid_Textalign'>" + dataItem.OrderNo + "</span>";
                            else
                                return "";
                        },
                        filterable: { search: true },
                        width: "120px"
                    },
                    //{
                    //    field: "ProductType",
                    //    title: "Product Type",
                    //    headerAttributes: { style: "white-space: normal" },
                    //    //headerTemplate: '<label title="Company" class="report_tooltip">Product Type</label>',
                    //    filterable: { multi: true, search: true },
                    //    width: "130px"
                    //},
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "Sale",
                        title: "Sale $",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">Sale $</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Sale) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "COGSProduct",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProduct) + "</span>";
                        },
                        width: "150px"
                    },
                    {
                        field: "SaleProduct",
                        title: "Sale $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.SaleProduct) + "</span>";
                        },
                        width: "150px"
                    },
                    {
                        field: "Tax",
                        title: "Tax",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">Tax</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Tax) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "Customer",
                        title: "Customer",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Sales Person" class="report_tooltip">Customer</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "AccountType",
                        title: "Account Type",
                        headerAttributes: { style: "white-space:normal" },
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "AccountNo",
                        title: "Account #",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Order" class="report_tooltip">Account #</label>',
                        template: function (dataItem) {
                            if (dataItem.AccountNo)
                                return "<span class='Grid_Textalign'>" + dataItem.AccountNo + "</span>";
                            else
                                return "";
                        },
                        filterable: { search: true },
                        width: "120px"
                    },
                    {
                        field: "QBInvoiceNo",
                        title: "QB Invoice #",
                        headerAttributes: { style: "white-space: normal" },
                        //headerTemplate: '<label title="Source" class="report_tooltip">QB Invoice #</label>',
                        filterable: { search: true },
                        width: "120px"
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "SalesPerson", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        //OrdersPaidInFull
        $scope.OrdersPaidInFullReport = function () {
            $scope.OrdersPaidInFull_options = {
                cache: false,
                excel: {
                    fileName: "OrdersPaidInFullReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/GetOrdersPaidInFullDetail',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    //Value: $scope.ReportFilter.Value,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                }
                                return data;
                            }
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                Territory: { type: "string" },
                                ContractedDate: { type: "date" },
                                PaidDate: { type: "date" },
                                SalesPerson: { type: "string" },
                                OrderNo: { type: "number" },
                                //ProductType: { type: "string" },
                                COGS: { type: "number" },
                                Sale: { type: "number" },
                                COGSProduct: { type: "number" },
                                SaleProduct: { type: "number" },
                                Tax: { type: "number" },
                                Customer: { type: "string" },
                                AccountType: { type: "string" },
                                AccountNo: { type: "number" },
                                QBInvoiceNo: { type: "string" }
                            }
                        }
                    },
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    //var myDiv = $('.k-grid-content.k-auto-scrollable');
                    //if (myDiv[0]) {
                    //    myDiv[0].scrollLeft = 0;
                    //}
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];
                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "COGS" || sheet.rows[0].cells[cellIndex].value == "Sale $" || sheet.rows[0].cells[cellIndex].value == "Tax" || sheet.rows[0].cells[cellIndex].value == "COGS (Product)" || sheet.rows[0].cells[cellIndex].value == "Sale $ (Product)") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "Territory",
                        title: "Territory",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Territory" class="report_tooltip">Territory</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "ContractedDate",
                        title: "Contracted Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Contracted Date" class="report_tooltip">Contracted Date</label>',
                        width: "150px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "OrdersPaidContractedDateFilterCalendar", offsetvalue),
                        //format: "{0:MM/dd/yyyy}"
                        template: function (dataItem) {
                            if (dataItem.ContractedDate) {
                                return HFCService.KendoDateFormat(dataItem.ContractedDate);
                            }
                            else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "PaidDate",
                        title: "Final Payment Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Final Payment Date" class="report_tooltip">Final Payment Date</label>',
                        width: "150px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "OrdersPaidFinalPaymentDateFilterCalendar", offsetvalue),
                        //format: "{0:MM/dd/yyyy}"
                        template: function (dataItem) {
                            if (dataItem.PaidDate) {
                                return HFCService.KendoDateFormat(dataItem.PaidDate);
                            }
                            else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "SalesPerson",
                        title: "Sales Person",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Sales Person" class="report_tooltip">Sales Person</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "OrderNo",
                        title: "Order #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Order #" class="report_tooltip">Order #</label>',
                        template: function (dataItem) {
                            if (dataItem.OrderNo)
                                return "<span class='Grid_Textalign'>" + dataItem.OrderNo + "</span>";
                            else
                                return "";
                        },
                        filterable: { search: true },
                        width: "120px"
                    },
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "Sale",
                        title: "Sale $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Sale $" class="report_tooltip">Sale $</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Sale) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "COGSProduct",
                        title: "COGS (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold (Product)" class="report_tooltip">COGS (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGSProduct) + "</span>";
                        },
                        width: "150px"
                    },
                    {
                        field: "SaleProduct",
                        title: "Sale $ (Product)",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Sale $ (Product)" class="report_tooltip">Sale $ (Product)</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.SaleProduct) + "</span>";
                        },
                        width: "150px"
                    },
                    {
                        field: "Tax",
                        title: "Tax",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Tax" class="report_tooltip">Tax</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Tax) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "Customer",
                        title: "Customer",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Customer" class="report_tooltip">Customer</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "AccountType",
                        title: "Account Type",
                        headerAttributes: { style: "white-space:normal" },
                        headerTemplate: '<label title="Account Type" class="report_tooltip">Account Type</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "AccountNo",
                        title: "Account #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Account #" class="report_tooltip">Account #</label>',
                        template: function (dataItem) {
                            if (dataItem.AccountNo)
                                return "<span class='Grid_Textalign'>" + dataItem.AccountNo + "</span>";
                            else
                                return "";
                        },
                        filterable: { search: true },
                        width: "120px"
                    },
                    {
                        field: "QBInvoiceNo",
                        title: "QB Invoice #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="QB Invoice #" class="report_tooltip">QB Invoice #</label>',
                        filterable: { search: true },
                        width: "130px"
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "SalesPerson", dir: "asc" }),
                scrollable: true,
                sortable: true,
            };
        };

        //PPCMatchBack
        $scope.PPCMatchBackReport = function () {
            $scope.PPCMatchBackReport_options = {
                cache: false,
                excel: {
                    fileName: "PPCMatchBackReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getPPCMatchBack',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                //if All is selected, then we need to pass empty value to api
                                var tempSourceList = $scope.ReportFilter.SourceList;
                                for (var i = 0; i < tempSourceList.length; i++) {
                                    if (tempSourceList[i] == 0) {
                                        tempSourceList = [];
                                        break;
                                    }
                                }
                                var data = {
                                    SourceList: tempSourceList.join(),
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                Territory: { type: "string" },
                                CustomerName: { type: "string" },
                                ZipCode: { type: "string" },
                                EmailAddress: { type: "string" },
                                LeadSource: { type: "string" },
                                OrderDate: { type: "date" },
                                TotalSales: { type: "number" },
                                Phone: { type: "number" }
                            }
                        }
                    },
                    aggregate: [{ field: "TotalSales", aggregate: "sum" }],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                dataBound: function (e) {
                    var dataSource = $("#gridPPCMatchBackReport").data("kendoGrid").dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: dataSource.data(),
                        filter: dataSource.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();
                    if (data != undefined && data.length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    var LeadSourcesWrapper = e.sender.element.find(".LeadSourcesWrapper");

                    var LeadSourcesCount = 0;
                    if (data != undefined && data.length != 0)
                        LeadSourcesCount = data.length;

                    LeadSourcesWrapper.html(LeadSourcesCount);

                    if ($("#gridPPCMatchBackReport").find(".k-grid-header").next()[0] != $("#gridPPCMatchBackReport").find(".k-grid-footer")[0])
                        $("#gridPPCMatchBackReport").find(".k-grid-footer").insertAfter($("#gridPPCMatchBackReport .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "TotalSales") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Phone") {
                                    cell.format = "(###) ###-####";
                                    cell.hAlign = "left";
                                }
                            }
                            if (row.type === "footer") {
                                if (sheet.rows[0].cells[cellIndex].value == "Lead Source") {
                                    cell.value = sheet.rows.length - 2;
                                }
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "LeadSource",
                        title: "Lead Source",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Lead Source" class="report_tooltip">Lead Source</label>',
                        filterable: { search: true },
                        width: "120px",
                        footerTemplate: "<span class='Grid_Textalign'><div class='LeadSourcesWrapper'></div></span>"
                    },
                    {
                        field: "Territory",
                        title: "Territory",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Territory" class="report_tooltip">Territory</label>',
                        filterable: { search: true },
                        width: "120px"
                    },
                    {
                        field: "CustomerName",
                        title: "Account Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Account Name" class="report_tooltip">Account Name</label>',
                        filterable: { search: true },
                        width: "170px",
                    },
                    {
                        field: "ZipCode",
                        title: "Zip/Postal",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Zip/Postal" class="report_tooltip">Zip/Postal</label>',
                        filterable: { search: true },
                        width: "130px",
                    },
                    {
                        field: "EmailAddress",
                        title: "Email",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Email" class="report_tooltip">Email</label>',
                        filterable: { search: true },
                        width: "150px",
                    },
                    {
                        field: "Phone",
                        title: "Phone",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Phone" class="report_tooltip">Phone</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return maskedDisplayPhone(dataItem.Phone);
                        },
                        width: "150px",
                    },
                    {
                        field: "OrderDate",
                        title: "Contract Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Contract Date" class="report_tooltip">Contract Date</label>',
                        filterable: HFCService.gridfilterableDate("date", "PPCMatchBackFilterCalendar", offsetvalue),

                        width: "150px",
                        template: function (dataItem) {
                            if (dataItem.OrderDate) {
                                return HFCService.KendoDateFormat(dataItem.OrderDate);
                            }
                            else {
                                return "";
                            }
                        },
                        //format: "{0:MM/dd/yyyy}"
                    },
                    {
                        field: "TotalSales",
                        title: "Total $",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total $" class="report_tooltip">Total $</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        template: function (dataItem) {
                            if (dataItem.TotalSales)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSales) + "</span>";
                            else
                                return "";
                        },

                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "180px"
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "Territory", dir: "asc" }),
                sortable: true,
                scrollable: true,
                resizable: true
            };
        };

        //PaymentReport
        $scope.PaymentReport = function () {
            $scope.PaymentReport_options = {
                cache: false,
                excel: {
                    fileName: "PaymentReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/getPaymentReport',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    EndDate: formatDate($scope.ReportFilter.EndDate)
                                }
                                return data;
                            }
                        }
                    }, schema: {
                        model: {
                            fields: {
                                AccountName: { type: "string" },
                                PaymentDate: { type: "date" },
                                TransactionType: { type: "string" },
                                MemoDescription: { type: "string" },
                                OrderNo: { type: "number" },
                                Amount: { type: "number" }
                            }
                        }
                    },
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                dataBound: function (e) {
                    var dataSource = $("#gridPaymentReport").data("kendoGrid").dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: dataSource.data(),
                        filter: dataSource.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();
                    if (data != undefined && data.length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    if ($("#gridPaymentReport").find(".k-grid-header").next()[0] != $("#gridPaymentReport").find(".k-grid-footer")[0])
                        $("#gridPaymentReport").find(".k-grid-footer").insertAfter($("#gridPaymentReport .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "Amount") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "AccountName",
                        title: "Account Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Account Name" class="report_tooltip">Account Name</label>',
                        filterable: { search: true },
                        width: "120px"
                    },
                    {
                        field: "PaymentDate",
                        title: "Payment Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Payment Date" class="report_tooltip">Payment Date</label>',
                        filterable: HFCService.gridfilterableDate("date", "PaymentReportFilterCalendar", offsetvalue),

                        width: "120px",
                        //format: "{0:MM/dd/yyyy}"
                        template: function (dataItem) {
                            if (dataItem.PaymentDate) {
                                return HFCService.KendoDateFormat(dataItem.PaymentDate);
                            }
                            else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "TransactionType",
                        title: "Transaction Type",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Transaction Type" class="report_tooltip">Transaction Type</label>',
                        filterable: { search: true },
                        width: "150px",
                    },
                    {
                        field: "MemoDescription",
                        title: "Memo Description",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Memo Description" class="report_tooltip">Memo Description</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "OrderNo",
                        title: "Order #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Order #" class="report_tooltip">Order #</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.OrderNo)
                                return "<span class='Grid_Textalign'>" + dataItem.OrderNo + "</span>";
                            else
                                return "";
                        },
                        width: "150px"
                    },
                    {
                        field: "Amount",
                        title: "Amount",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Amount" class="report_tooltip">Amount</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Amount)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                            else
                                return "";
                        },
                        width: "150px",
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: true,
                scrollable: true,
                resizable: true
            };
        };

        //UseTax
        $scope.UseTaxReport = function () {
            $scope.UseTax_options = {
                cache: false,
                excel: {
                    fileName: "UseTaxReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/GetUseTaxDetail',
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                var data = {
                                    //Value: $scope.ReportFilter.Value,
                                    StartDate: formatDate($scope.ReportFilter.StartDate),
                                    Zip: $scope.ReportFilter.Zip,
                                    TerritoryId: $scope.ReportFilter.TerritoryId
                                }
                                return data;
                            }
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                OrderNumber: { type: "number" },
                                Product: { type: "string" },
                                ZipCode: { type: "string" },
                                Dt: { type: "date" },
                                UseTaxPercentage:{ type: "number" },
                                COGS: { type: "number" },
                                UseTax: { type: "number" },
                            }
                        }
                    },
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];
                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data") {
                                if (sheet.rows[0].cells[cellIndex].value == "Order #") {
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Use Tax Value" || sheet.rows[0].cells[cellIndex].value == "COGS" ) {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Use Tax Factor") {
                                    cell.value = cell.value / 100;
                                    cell.format = "#,##0.00%";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "OrderNumber",
                        title: "Order #",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Order #" class="report_tooltip">Order #</label>',
                        filterable: { search: true },
                        width: "130px",
                        template: function (dataItem) {
                            if (dataItem.OrderNumber)
                                return "<span class='Grid_Textalign'>" + dataItem.OrderNumber + "</span>";
                            else
                                return "";
                        },
                    },
                    {
                        field: "Product",
                        title: "Product",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Product" class="report_tooltip">Product</label>',
                        width: "150px",
                        filterable: { search: true, multi: true }
                    },
                    {
                        field: "ZipCode",
                        title: "Zip",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Zip" class="report_tooltip">Zip</label>',
                        width: "150px",
                        filterable: { search: true, multi: true }
                    },
                    {
                        field: "Dt",
                        title: "Date",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Date" class="report_tooltip">Date</label>',
                        width: "150px",
                        filterable: HFCService.gridfilterableDate("date", "UseTaxDateFilterCalendar", offsetvalue),
                        //format: "{0:MM/dd/yyyy}"
                        template: function (dataItem) {
                            if (dataItem.Dt) {
                                return HFCService.KendoDateFormat(dataItem.Dt);
                            }
                            else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "UseTaxPercentage",
                        title: "Use Tax Factor",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Use Tax Factor" class="report_tooltip">Use Tax Factor</label>',
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", UseTaxPercentage / 100)#</span>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "COGS",
                        title: "COGS",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Cost of Goods Sold" class="report_tooltip">COGS</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.COGS) + "</span>";
                        },
                        width: "130px"
                    },
                    {
                        field: "UseTax",
                        title: "Use Tax Value",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Use Tax Value" class="report_tooltip">Use Tax Value</label>',
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.UseTax) + "</span>";
                        },
                        filterable: { search: true },
                        width: "130px"
                    },
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                scrollable: true,
                sortable: true,
            };
        };

        //MsrReports

        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        $scope.TerritoryValChange = function () {

            $("#MsrTerritoryval").css('color', 'black');
        }

        $scope.LoadMsrReport = function () {

            if ($scope.ReportFilter.TerritoryId == "" || $scope.ReportFilter.TerritoryId == undefined || $scope.ReportFilter.TerritoryId == null) {
                alert("Please select Territory");
                return;
            }
            else if ($scope.ReportFilter.Year == "" || $scope.ReportFilter.Year == undefined || $scope.ReportFilter.Year == null) {
                alert("Please enter valid Year");
                return;
            }
            else {
                $scope.ReportFilter.startdate = $scope.ReportFilter.Year.toString() + "/" + $scope.ReportFilter.Month + "/01";
                //$scope.ReportFilter.startdate = $scope.ReportFilter.Month + "-" + "01" + "-" + $scope.ReportFilter.Year.toString();
                var startDateVal = formatDate($scope.ReportFilter.startdate);
                window.open('/api/Reports/0/getMSRReport_PDF?TerritoryID=' + $scope.ReportFilter.TerritoryId + '&Mth=' + startDateVal + '', '_blank');
            }
        }



        $scope.EmailMsrReport = function () {

            if ($scope.ReportFilter.TerritoryId == "" || $scope.ReportFilter.TerritoryId == undefined || $scope.ReportFilter.TerritoryId == null) {
                alert("Please select Territory");
                return;
            }
            else if ($scope.ReportFilter.Year == "" || $scope.ReportFilter.Year == undefined || $scope.ReportFilter.Year == null) {
                alert("Please enter valid Year");
                return;
            }
            else {
                //call's for e-mail (as did in global email at top)
                for (var i = 0; i < $scope.LookupFETerritory.length; i++) {
                    if ($scope.ReportFilter.TerritoryId == $scope.LookupFETerritory[i].TerritoryId) {
                        $scope.ReportFilter.TerritoryCode = $scope.LookupFETerritory[i].Code;
                    }
                }
                $scope.ReportFilter.startdate = $scope.ReportFilter.Year.toString() + "/" + $scope.ReportFilter.Month + "/01";
                //$scope.ReportFilter.startdate = $scope.ReportFilter.Month + "-" + "01" + "-" + $scope.ReportFilter.Year.toString();
                var MsrReportStartdate = new Date($scope.ReportFilter.startdate);
                var SelectedMnthName = monthNames[MsrReportStartdate.getMonth()]
                $scope.ReportFilter.MonthName = SelectedMnthName;
                $scope.NavbarService.ReportDatas = $scope.ReportFilter;//pass the value to service and can be used in mail template
                $scope.NavbarService.emailReport();
                //$scope.NavbarService.emailQuote();
            }

        }

        //IncomeSummaryReport
        $scope.IncomeSummaryReport = function () {
            $scope.IncomeSummaryReport_options = {
                cache: false,
                excel: {
                    fileName: "IncomeSummaryReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: function (option) {
                            var input = {
                                Year: $scope.ReportFilter.Year
                            };
                            $http.post('/api/Reports/0/getIncomeSummaryReport', input).then(function (response) {
                                var data = response.data;
                                var returndata = [];
                                for (var i = 0; i < data.length; i++) {
                                    for (var j in data[i]) {
                                        if (j == "Total") {
                                            if (data[i].Total > 0) returndata.push(data[i]);
                                        }
                                    }
                                }
                                if (returndata.length > 0)
                                    returndata.sort(compare);

                                option.success(returndata);
                            });
                        }
                    }, schema: {
                        model: {
                            fields: {
                                IncomeType: { type: "string" },
                                January: { type: "number" },
                                February: { type: "number" },
                                March: { type: "number" },
                                April: { type: "number" },
                                May: { type: "number" },
                                June: { type: "number" },
                                July: { type: "number" },
                                August: { type: "number" },
                                September: { type: "number" },
                                October: { type: "number" },
                                November: { type: "number" },
                                December: { type: "number" },
                                Total: { type: "number" },
                            }
                        }
                    },
                    aggregate: [{ field: "January", aggregate: "sum" },
                    { field: "February", aggregate: "sum" },
                    { field: "March", aggregate: "sum" },
                    { field: "April", aggregate: "sum" },
                    { field: "May", aggregate: "sum" },
                    { field: "June", aggregate: "sum" },
                    { field: "July", aggregate: "sum" },
                    { field: "August", aggregate: "sum" },
                    { field: "September", aggregate: "sum" },
                    { field: "October", aggregate: "sum" },
                    { field: "November", aggregate: "sum" },
                    { field: "December", aggregate: "sum" },
                    { field: "Total", aggregate: "sum" }],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    },
                },
                dataBound: function (e) {
                    var dataSource = $("#gridIncomeSummaryReport").data("kendoGrid").dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: dataSource.data(),
                        filter: dataSource.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();
                    if (data != undefined && data.length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    if ($("#gridIncomeSummaryReport").find(".k-grid-header").next()[0] != $("#gridIncomeSummaryReport").find(".k-grid-footer")[0])
                        $("#gridIncomeSummaryReport").find(".k-grid-footer").insertAfter($("#gridIncomeSummaryReport .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data" || row.type === "footer") {
                                if (sheet.rows[0].cells[cellIndex].value != "Income Type") {
                                    cell.format = "$#,##0.00";
                                    cell.hAlign = "right";
                                }
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "IncomeType",
                        title: "Income Type",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Income Type" class="report_tooltip">Income Type</label>',
                        filterable: { search: true },
                        width: "130px"
                    },
                    {
                        field: "January",
                        title: "January",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="January" class="report_tooltip">January</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.January)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.January) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "February",
                        title: "February",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="February" class="report_tooltip">February</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.February)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.February) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "March",
                        title: "March",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="March" class="report_tooltip">March</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.March)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.March) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "April",
                        title: "April",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="April" class="report_tooltip">April</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.April)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.April) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "May",
                        title: "May",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="May" class="report_tooltip">May</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.May)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.May) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "June",
                        title: "June",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="June" class="report_tooltip">June</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.June)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.June) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "July",
                        title: "July",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="July" class="report_tooltip">July</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.July)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.July) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "August",
                        title: "August",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="August" class="report_tooltip">August</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.August)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.August) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "September",
                        title: "September",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="September" class="report_tooltip">September</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.September)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.September) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "October",
                        title: "October",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="October" class="report_tooltip">October</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.October)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.October) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "November",
                        title: "November",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="November" class="report_tooltip">November</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.November)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.November) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "December",
                        title: "December",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="December" class="report_tooltip">December</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.December)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.December) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    },
                    {
                        field: "Total",
                        title: "Total",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Total" class="report_tooltip">Total</label>',
                        filterable: { search: true },
                        template: function (dataItem) {
                            if (dataItem.Total)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Total) + "</span>";
                            else
                                return "<span class='Grid_Textalign'>$0.00</span>";
                        },
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        width: "120px",
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: true,
                scrollable: true,
                resizable: true
            };
        }
        //Sort function for IncomeSummaryReport
        function compare(a, b) {
            const bandA = a.IncomeType.toUpperCase();
            const bandB = b.IncomeType.toUpperCase();

            let comparison = 0;
            if (bandA > bandB) {
                comparison = 1;
            } else if (bandA < bandB) {
                comparison = -1;
            }
            return comparison;
        }

        //--End--
        // Kendo Resizing
        if (window.location.href.toUpperCase().includes("SALESANDPURCHASINGDETAILREPORT") || window.location.href.toUpperCase().includes("MARKETINGPERFORMANCE") || window.location.href.toUpperCase().includes("VENDORPERFORMANCE")) {
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 345);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 345);
                };
            });
        }
        else if ($window.location.href.toUpperCase().includes('SALESJOURNALREPORT') || window.location.href.toUpperCase().includes("PAYMENTREPORT")) {
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 276);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 276);
                };
            });
        }
        else {
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 310);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 310);
                };
            });
        }

        $scope.ExcelRow = function (titlValue,gridData) {
            var rows = [];
            var row = {};
            var cells = [];
            var cell = {};

            cells = [{ value: titlValue, background: "#7a7a7a" },
            { value: "Jan", background: "#7a7a7a" }, { value: "Feb", background: "#7a7a7a" },
            { value: "Mar", background: "#7a7a7a" }, { value: "Apr", background: "#7a7a7a" },
            { value: "May", background: "#7a7a7a" }, { value: "Jun", background: "#7a7a7a" },
            { value: "Jul", background: "#7a7a7a" }, { value: "Aug", background: "#7a7a7a" },
            { value: "Sep", background: "#7a7a7a" }, { value: "Oct", background: "#7a7a7a" },
            { value: "Nov", background: "#7a7a7a" }, { value: "Dec", background: "#7a7a7a" },
            { value: "Totals", background: "#7a7a7a" }];

            row.cells = cells; rows.push(row)
            for (var i = 0; i < gridData.length; i++) {
                row = {}; cells = [];
                if (gridData[i].ThisYear == "GP %") {
                    cell = {}; cell.value = gridData[i].ThisYear; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Jan.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Feb.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Mar.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Apr.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].May.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Jun.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Jul.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Aug.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Sep.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Oct.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Nov.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Dec.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].tot.replace(/[$,%]+/g, "")) / 100; cell.format = "#,##0.00%"; cell.hAlign = "right"; cells.push(cell);
                }
                else if (gridData[i].ThisYear == "Order count") {
                    cell = {}; cell.value = gridData[i].ThisYear; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Jan); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Feb); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Mar); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Apr); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].May); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Jun); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Jul); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Aug); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Sep); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Oct); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Nov); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].Dec); cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseInt(gridData[i].tot); cell.hAlign = "right"; cells.push(cell);
                }
                else {
                    cell = {}; cell.value = gridData[i].ThisYear; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Jan.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Feb.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Mar.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Apr.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].May.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Jun.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Jul.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Aug.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Sep.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Oct.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Nov.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].Dec.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                    cell = {}; cell.value = parseFloat(gridData[i].tot.replace(/[$,%]+/g, "")); cell.format = "$#,##0.00"; cell.hAlign = "right"; cells.push(cell);
                }
                row.cells = cells; rows.push(row)
            }

            return rows;
        }

        // End Kendo Resizing

        //--------------- Download Excel ---------------------//

        $scope.DownloadSalesandPurchasingDetailReport_Excel = function () {
            var grid = $("#gridSalesAndPurchaseSearch").data("kendoGrid");
            grid.saveAsExcel();
        }

        $scope.DownloadFranchisePerformanceReport_Excel = function () {
            if ($scope.ReportFilter.FP_ReportType == "Sales Person") {
                $("#gridFranchisePerformanceReport_SalesAgent").data("kendoGrid").saveAsExcel();
            }
            if ($scope.ReportFilter.FP_ReportType == "Territory") {
                $("#gridFranchisePerformanceReport_Territory").data("kendoGrid").saveAsExcel();
            }
            if ($scope.ReportFilter.FP_ReportType == "Franchise") {
                $("#gridFranchisePerformanceReport_Franchise").data("kendoGrid").saveAsExcel();
            }
            if ($scope.ReportFilter.FP_ReportType == "Zip") {
                $("#gridFranchisePerformanceReport_Zip").data("kendoGrid").saveAsExcel();
            }
            if ($scope.ReportFilter.FP_ReportType == "Vendor") {
                $("#gridFranchisePerformanceReport_Vendor").data("kendoGrid").saveAsExcel();
            }
            if ($scope.ReportFilter.FP_ReportType == "Product Category") {
                $("#gridFranchisePerformanceReport_ProductCategory").data("kendoGrid").saveAsExcel();
            }
        }

        $scope.DownloadSalesSummaryByMonth_Excel = function () {
            //if (sheet.rows[1].cells[cellIndex].value == "COGS" || sheet.rows[1].cells[cellIndex].value == "Total Sales $" || sheet.rows[1].cells[cellIndex].value == "GP $") {
            //    cell.value = parseFloat(cell.value.replace(/[$,%]+/g, ""));
            //    cell.format = "$#,##0.00";
            //    cell.hAlign = "right";
            //}
            //if (sheet.rows[1].cells[cellIndex].value == "GP %") {
            //    cell.value = parseFloat(cell.value.replace(/[$,%]+/g, "")) / 100;
            //    cell.format = "#,##0.00%";
            //    cell.hAlign = "right";
            //}
            var sheets = [];
            var rows = [];
            var tydata = $("#gridSalesSummary_ThisYear").data("kendoGrid").dataSource.data();
            rows = $scope.ExcelRow('THIS YEAR', tydata);
            var sheet = {
                title: "THIS YEAR",
                columns: [
                    { autoWidth: true },
                    { autoWidth: true }
                ],
                rows: rows
            };
            sheets.push(sheet);
            if ($("#gridSalesSummary_LastYear").data("kendoGrid") != undefined) {
                tydata = $("#gridSalesSummary_LastYear").data("kendoGrid").dataSource.data();
                if (tydata) {
                    rows = $scope.ExcelRow('LAST YEAR', tydata);
                    sheet = {
                        title: "LAST YEAR",
                        columns: [
                            { autoWidth: true },
                            { autoWidth: true }
                        ],
                        rows: rows
                    };
                    sheets.push(sheet);
                }
            }
            if ($("#gridSalesSummary_TwoYearsOld").data("kendoGrid") != undefined) {
                tydata = $("#gridSalesSummary_TwoYearsOld").data("kendoGrid").dataSource.data();
                if (tydata) {
                    rows = $scope.ExcelRow(tydata[0].Year.toString(), tydata);
                    sheet = {
                        title: tydata[0].Year.toString(),
                        columns: [
                            { autoWidth: true },
                            { autoWidth: true }
                        ],
                        rows: rows
                    };
                    sheets.push(sheet);
                }
            }
            if ($("#gridSalesSummary_ThreeYearsOld").data("kendoGrid") != undefined) {
                tydata = $("#gridSalesSummary_ThreeYearsOld").data("kendoGrid").dataSource.data();
                if (tydata) {
                    rows = $scope.ExcelRow(tydata[0].Year.toString(), tydata);
                    sheet = {
                        title: tydata[0].Year.toString(),
                        columns: [
                            { autoWidth: true },
                            { autoWidth: true }
                        ],
                        rows: rows
                    };
                    sheets.push(sheet);
                }
            }
            if ($("#gridSalesSummary_FourYearsOld").data("kendoGrid") != undefined) {
                tydata = $("#gridSalesSummary_FourYearsOld").data("kendoGrid").dataSource.data();
                if (tydata) {
                    rows = $scope.ExcelRow(tydata[0].Year.toString(), tydata);
                    sheet = {
                        title: tydata[0].Year.toString(),
                        columns: [
                            { autoWidth: true },
                            { autoWidth: true }
                        ],
                        rows: rows
                    };
                    sheets.push(sheet);
                }
            }
            var workbook = new kendo.ooxml.Workbook({
                sheets: sheets
            });
            kendo.saveAs({
                dataURI: workbook.toDataURL(),
                fileName: "SalesSummary.xlsx"
            });
        }

        $scope.DownloadVendorSalesPerformanceReport_Excel = function () {
            $("#gridVendorSalesPerformance").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadMarketingPerformanceReport_Excel = function () {
            $("#gridMarketingPerformance").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadSalesTaxReport_Excel = function () {
            $("#gridSalesTax").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadSalesTaxSummaryReport_Excel = function () {
            $("#gridSalesTaxSummary").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadSalesSummaryReport_Excel = function () {
            $("#gridSalesSummaryDetailReport").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadOpenARReport_Excel = function () {
            $("#gridOpenARReport").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadSalesJournalReportReport_Excel = function () {
            $("#gridSalesJournal").data("kendoGrid").saveAsExcel();
        }
        $scope.DownloadUseTax_Excel = function () {
            $("#gridUseTax").data("kendoGrid").saveAsExcel();
        }
        $scope.DownloadOrderPaidInFullReport_Excel = function () {
            $("#gridOrdersPaidInFull").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadPPCMatchBackReport_Excel = function () {
            $("#gridPPCMatchBackReport").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadPaymentReport_Excel = function () {
            $("#gridPaymentReport").data("kendoGrid").saveAsExcel();
        }
        $scope.DownloadAgingReport_Excel = function () {
            $("#GridAging").data("kendoGrid").saveAsExcel();
        }

        $scope.DownloadIncomeSummaryReport_Excel = function () {
            $("#gridIncomeSummaryReport").data("kendoGrid").saveAsExcel();
        }
        //--------------- Download Excel ---------------------//

        //------ Check Data Format on click the load/download button --------- //
        $scope.ValidateDate = function (startdate, enddate) {

            var dateFormat = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
            if (dateFormat.test(startdate) && dateFormat.test(enddate)) {
                //HFC.DisplayAlert("Invalid Date Format");
                return false;
            }
            else return true;

        }

        //-------------  Load Data button click -------------//

        $scope.LoadSalesandPurchasingDetailReport = function () {
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }
            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {

                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }

                if ($('#gridSalesAndPurchaseSearch').data('kendoGrid') == undefined) {

                    if ($scope.BrandId == 1)
                        $scope.SalesandPurchasingDetailReport();
                    else if ($scope.BrandId == 2)
                        $scope.TLSalesandPurchasingDetailReport();
                    else if ($scope.BrandId == 3)
                        $scope.CCSalesandPurchasingDetailReport();

                }
                else {
                    $('#gridSalesAndPurchaseSearch').data('kendoGrid').dataSource.filter({});
                    $('#gridSalesAndPurchaseSearch').data('kendoGrid').dataSource.read();
                    //$('#gridSalesAndPurchaseSearch').data('kendoGrid').refresh();
                }
            }
            var myDiv = $('.k-grid-content.k-auto-scrollable');
            if (myDiv[0]) {
                myDiv[0].scrollLeft = 0;
            }
        }

        $scope.LoadFranchisePerformanceReport = function () {
            if ($scope.ReportFilter.FP_ReportType == undefined || $scope.ReportFilter.FP_ReportType == "") {
                alert("Please select Report Type");
                return;
            }
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }

            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {

                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }

                if ($scope.ReportFilter.FP_ReportType == "Sales Person") {
                    $('#gridFranchisePerformanceReport_SalesAgent').removeClass('hidegrid');
                    if ($('#gridFranchisePerformanceReport_SalesAgent').data('kendoGrid') == undefined) {
                        $scope.FranchisePerformanceReport_SalesAgent();
                    } else {
                        $('#gridFranchisePerformanceReport_SalesAgent').data('kendoGrid').dataSource.filter({});
                        $('#gridFranchisePerformanceReport_SalesAgent').data('kendoGrid').dataSource.read();
                        //$('#gridFranchisePerformanceReport_SalesAgent').data('kendoGrid').refresh();
                    }
                }
                if ($scope.ReportFilter.FP_ReportType == "Territory") {
                    $('#gridFranchisePerformanceReport_Territory').removeClass('hidegrid');
                    if ($('#gridFranchisePerformanceReport_Territory').data('kendoGrid') == undefined) {
                        $scope.FranchisePerformanceReport_Territory();
                    } else {
                        $('#gridFranchisePerformanceReport_Territory').data('kendoGrid').dataSource.filter({});
                        $('#gridFranchisePerformanceReport_Territory').data('kendoGrid').dataSource.read();
                        //$('#gridFranchisePerformanceReport_Territory').data('kendoGrid').refresh();
                    }
                }
                if ($scope.ReportFilter.FP_ReportType == "Franchise") {
                    $('#gridFranchisePerformanceReport_Franchise').removeClass('hidegrid');
                    if ($('#gridFranchisePerformanceReport_Franchise').data('kendoGrid') == undefined) {
                        $scope.FranchisePerformanceReport_Franchise();
                    } else {
                        $('#gridFranchisePerformanceReport_Franchise').data('kendoGrid').dataSource.filter({});
                        $('#gridFranchisePerformanceReport_Franchise').data('kendoGrid').dataSource.read();
                        //$('#gridFranchisePerformanceReport_Franchise').data('kendoGrid').refresh();
                    }
                }
                if ($scope.ReportFilter.FP_ReportType == "Zip") {
                    $('#gridFranchisePerformanceReport_Zip').removeClass('hidegrid');
                    if ($('#gridFranchisePerformanceReport_Zip').data('kendoGrid') == undefined) {
                        $scope.FranchisePerformanceReport_Zip();
                    } else {
                        $('#gridFranchisePerformanceReport_Zip').data('kendoGrid').dataSource.filter({});
                        $('#gridFranchisePerformanceReport_Zip').data('kendoGrid').dataSource.read();
                        //$('#gridFranchisePerformanceReport_Zip').data('kendoGrid').refresh();
                    }
                }
                if ($scope.ReportFilter.FP_ReportType == "Vendor") {
                    $('#gridFranchisePerformanceReport_Vendor').removeClass('hidegrid');
                    if ($('#gridFranchisePerformanceReport_Vendor').data('kendoGrid') == undefined) {
                        $scope.FranchisePerformanceReport_Vendor();
                    } else {
                        $('#gridFranchisePerformanceReport_Vendor').data('kendoGrid').dataSource.filter({});
                        $('#gridFranchisePerformanceReport_Vendor').data('kendoGrid').dataSource.read();
                        //$('#gridFranchisePerformanceReport_Vendor').data('kendoGrid').refresh();
                    }
                }
                if ($scope.ReportFilter.FP_ReportType == "Product Category") {
                    $('#gridFranchisePerformanceReport_ProductCategory').removeClass('hidegrid');
                    if ($('#gridFranchisePerformanceReport_ProductCategory').data('kendoGrid') == undefined) {
                        $scope.FranchisePerformanceReport_ProductCategory();
                    } else {
                        $('#gridFranchisePerformanceReport_ProductCategory').data('kendoGrid').dataSource.filter({});
                        $('#gridFranchisePerformanceReport_ProductCategory').data('kendoGrid').dataSource.read();
                        //$('#gridFranchisePerformanceReport_ProductCategory').data('kendoGrid').refresh();
                    }
                }
            }
            var myDiv = $('.k-grid-content.k-auto-scrollable');
            if (myDiv[0]) {
                myDiv[0].scrollLeft = 0;
            }
        }

        $scope.LoadVendorSalesPerformanceReport = function () {
            if ($scope.ReportFilter.Year == null) {
                alert("Please select Year");
                return;
            }

            $scope.ReportFilter.StartDate = $scope.ReportFilter.Year.toString() + "-01-01";
            $scope.ReportFilter.EndDate = $scope.ReportFilter.Year.toString() + "-12-31";

            if ($('#gridVendorSalesPerformance').data('kendoGrid') == undefined) {
                $scope.VendorSalesPerformanceReport();
            }
            else {
                $('#gridVendorSalesPerformance').data('kendoGrid').dataSource.filter({});
                $('#gridVendorSalesPerformance').data('kendoGrid').dataSource.read();
                //$('#gridVendorSalesPerformance').data('kendoGrid').refresh();
            }
        }

        $scope.LoadMarketingSummaryReport = function () {
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }

            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {
                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }

                if ($('#gridMarketingPerformance').data('kendoGrid') == undefined) {
                    $scope.MarketingPerformanceReport();
                }
                else {
                    $('#gridMarketingPerformance').data('kendoGrid').dataSource.filter({});
                    $('#gridMarketingPerformance').data('kendoGrid').dataSource.read();
                    //$('#gridMarketingPerformance').data('kendoGrid').refresh();
                }
            }
            var myDiv = $('.k-grid-content.k-auto-scrollable');
            if (myDiv[0]) {
                myDiv[0].scrollLeft = 0;
            }
        }

        $scope.LoadSalesSummaryReport = function () {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            // need to pass SourceList as null because it resolve the issue raised by PPCMatchBackReport
            // and its not need for SalesSummaryByMonth
            $scope.ReportFilter.SourceList = null;
            var SalesSummaryByMonthvalues = ReportsService.SalesSummaryByMonth($scope.ReportFilter);
            SalesSummaryByMonthvalues.then(function (data) {
                $scope.SalesSummaryByMonth = data.data;

                $scope.LastYearDataSource = new kendo.data.DataSource({
                    data: data.data.LastYearData
                });

                $scope.ThisYearDataSource = new kendo.data.DataSource({
                    data: data.data.ThisYearData
                });

                $scope.TwoYearsOldDataSource = new kendo.data.DataSource({
                    data: data.data.TwoYearOldData
                });

                $scope.ThreeYearsOldDataSource = new kendo.data.DataSource({
                    data: data.data.ThreeYearOldData
                });

                $scope.FourYearsOldDataSource = new kendo.data.DataSource({
                    data: data.data.FourYearOldData
                });

                if ($('#gridSalesSummary_ThisYear').data('kendoGrid') == undefined) {
                    $scope.SalesSummary_ThisYear();
                } else {
                    var grid = $("#gridSalesSummary_ThisYear").data("kendoGrid");
                    grid.setDataSource($scope.ThisYearDataSource);
                }
                if ($('#gridSalesSummary_LastYear').data('kendoGrid') == undefined) {
                    $scope.SalesSummary_LastYear();
                } else {
                    var grid = $("#gridSalesSummary_LastYear").data("kendoGrid");
                    grid.setDataSource($scope.LastYearDataSource);
                }
                if ($scope.BrandId !== 3) {
                    if ($('#gridSalesSummary_TwoYearsOld').data('kendoGrid') == undefined) {
                        $scope.SalesSummary_TwoYearsOld();
                    } else {
                        var grid = $("#gridSalesSummary_TwoYearsOld").data("kendoGrid");
                        grid.setDataSource($scope.TwoYearsOldDataSource);
                    }

                    if ($('#gridSalesSummary_ThreeYearsOld').data('kendoGrid') == undefined) {
                        $scope.SalesSummary_ThreeYearsOld();
                    } else {
                        var grid = $("#gridSalesSummary_ThreeYearsOld").data("kendoGrid");
                        grid.setDataSource($scope.ThreeYearsOldDataSource);
                    }

                    if ($('#gridSalesSummary_FourYearsOld').data('kendoGrid') == undefined) {
                        $scope.SalesSummary_FourYearsOld();
                    } else {
                        var grid = $("#gridSalesSummary_FourYearsOld").data("kendoGrid");
                        grid.setDataSource($scope.FourYearsOldDataSource);
                    }
                }

                loadingElement.style.display = 'none';
            }, function (e) {
                HFC.DisplayAlert(e.statusText);
                loadingElement.style.display = 'none';
            }).catch(function (e) {
                loadingElement.style.display = 'none';
            });
        }

        $scope.LoadSalesTaxReport = function () {
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }
            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {

                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }

                if ($('#gridSalesTax').data('kendoGrid') == undefined) {
                    $scope.SalesTax();
                }
                else {
                    $('#gridSalesTax').data('kendoGrid').dataSource.filter({});
                    $('#gridSalesTax').data('kendoGrid').dataSource.read();
                    //$('#gridSalesTax').data('kendoGrid').refresh();
                }
            }

        }

        $scope.LoadSalesTaxSummaryReport = function () {
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }
            if ($scope.ReportFilter.ST_ReportType == undefined || $scope.ReportFilter.ST_ReportType == "") {
                alert("Please select Report Type");
                return;
            }
            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {

                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }


                var grid = $('#gridSalesTaxSummary').data("kendoGrid");
                if (grid != null && grid != undefined) {
                    if ($scope.ReportFilter.ST_ReportType == "Zip") {
                        $("#gridSalesTaxSummary thead [data-field=ReportColValue] .k-link").html("Zip");
                        //grid.hideColumn("Zip");
                    }
                    if ($scope.ReportFilter.ST_ReportType == "County") {
                        $("#gridSalesTaxSummary thead [data-field=ReportColValue] .k-link").html("County");
                    }
                    if ($scope.ReportFilter.ST_ReportType == "City") {
                        $("#gridSalesTaxSummary thead [data-field=ReportColValue] .k-link").html("City");
                    }
                    if ($scope.ReportFilter.ST_ReportType == "State") {
                        $("#gridSalesTaxSummary thead [data-field=ReportColValue] .k-link").html("State");
                    }
                    if ($scope.ReportFilter.ST_ReportType == "Special") {
                        $("#gridSalesTaxSummary thead [data-field=ReportColValue] .k-link").html("Special");
                    }
                    if ($scope.ReportFilter.ST_ReportType == "Tax Code") {
                        $("#gridSalesTaxSummary thead [data-field=ReportColValue] .k-link").html("Tax Code");
                    }
                }
                if ($('#gridSalesTaxSummary').data('kendoGrid') == undefined) {
                    $scope.SalesTaxSummaryReport();
                }
                else {
                    $('#gridSalesTaxSummary').data('kendoGrid').dataSource.filter({});
                    $('#gridSalesTaxSummary').data('kendoGrid').dataSource.read();
                    //$('#gridSalesTaxSummary').data('kendoGrid').refresh();
                }
            }

        }
        $scope.LoadSalesSummaryReports_data = function () {
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }
            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {

                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }

                if ($('#gridSalesSummaryDetailReport').data('kendoGrid') == undefined) {
                    $scope.SalesSummaryDetailReport();
                }
                else {
                    $('#gridSalesSummaryDetailReport').data('kendoGrid').dataSource.filter({});
                    $('#gridSalesSummaryDetailReport').data('kendoGrid').dataSource.read();
                    //$('#gridSalesSummaryDetailReport').data('kendoGrid').refresh();
                }
            }
            var myDiv = $('.k-grid-content.k-auto-scrollable');
            if (myDiv[0]) {
                myDiv[0].scrollLeft = 0;
            }
        }

        $scope.LoadOpenARReports_data = function () {
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }
            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {

                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }

                if ($('#gridOpenARReport').data('kendoGrid') == undefined) {
                    $scope.OpenARReport();
                }
                else {
                    $('#gridOpenARReport').data('kendoGrid').dataSource.filter({});
                    $('#gridOpenARReport').data('kendoGrid').dataSource.read();
                    //$('#gridOpenARReport').data('kendoGrid').refresh();
                }
            }
            var myDiv = $('.k-grid-content.k-auto-scrollable');
            if (myDiv[0]) {
                myDiv[0].scrollLeft = 0;
            }
        }

        $scope.LoadPPCMatchBackReport = function () {
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }

            if ($scope.ReportFilter.SourceList.length == 0) {
                alert("Please select Source");
                return;
            }

            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {
                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }

                if ($('#gridPPCMatchBackReport').data('kendoGrid') == undefined) {
                    $scope.PPCMatchBackReport();
                }
                else {
                    $('#gridPPCMatchBackReport').data('kendoGrid').dataSource.filter({});
                    $('#gridPPCMatchBackReport').data('kendoGrid').dataSource.read();
                    //$('#gridPPCMatchBackReport').data('kendoGrid').refresh();
                }
            }
            var myDiv = $('.k-grid-content.k-auto-scrollable');
            if (myDiv[0]) {
                myDiv[0].scrollLeft = 0;
            }
        }

        $scope.LoadPaymentReport = function () {
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }

            var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
            if (checkformat) {
                HFC.DisplayAlert("Invalid Date Format");
                return;
            }
            else {
                if (new Date($scope.ReportFilter.StartDate) > new Date($scope.ReportFilter.EndDate)) {
                    alert("Start Date should be less than End Date");
                    return;
                }

                if ($('#gridPaymentReport').data('kendoGrid') == undefined) {
                    $scope.PaymentReport();
                }
                else {
                    $('#gridPaymentReport').data('kendoGrid').dataSource.filter({});
                    $('#gridPaymentReport').data('kendoGrid').dataSource.read();
                }
            }
            var myDiv = $('.k-grid-content.k-auto-scrollable');
            if (myDiv[0]) {
                myDiv[0].scrollLeft = 0;
            }
        }

        $scope.LoadIncomeSummaryReport = function () {
            if ($scope.ReportFilter.Year == undefined) {
                alert("Please enter valid Year");
                return;
            }

            if ($('#gridIncomeSummaryReport').data('kendoGrid') == undefined) {
                $scope.IncomeSummaryReport();
            }
            else {
                $('#gridIncomeSummaryReport').data('kendoGrid').dataSource.filter({});
                $('#gridIncomeSummaryReport').data('kendoGrid').dataSource.read();
            }

        }

        $scope.ButtonData = "Download";

        $scope.LoadRawDReports_data = function () {
            //$scope.submitted = true;
            //if ($scope.reportRawData.$invalid) {
            //    return;
            //}
            //restrict saving for invalid date
            if ($scope.DateRange) {
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        $scope.IsBusy = false;
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
            }
            if ($scope.ReportFilter.StartDate == null || $scope.ReportFilter.EndDate == null) {
                alert("Please select Date Range");
                return;
            }
            else {
                var checkformat = $scope.ValidateDate($scope.ReportFilter.StartDate, $scope.ReportFilter.EndDate);
                if (checkformat) {
                    HFC.DisplayAlert("Invalid Date Format");
                    return;
                }
                else {
                    var startdate = $scope.ReportFilter.StartDate;
                    $scope.MinendDate = startdate;
                    var addyear = new Date(startdate);
                    var addmonth = addyear.getMonth() + 1;
                    if (addmonth < 10) {
                        addmonth = 0 + "" + addmonth;
                    }
                    var adddate = addyear.getDate();
                    if (adddate < 10) {
                        adddate = 0 + "" + adddate;
                    }
                    // addyear = (addyear.getFullYear() + 1) + "-" + addmonth + "-" + adddate;
                    addyear = addmonth + "/" + adddate + "/" + (addyear.getFullYear() + 1);
                    $scope.MaxendDate = addyear;
                    var TempMaxDate = new Date($scope.MaxendDate);
                    var TempEndDate = new Date($scope.ReportFilter.EndDate);
                    var TempStartDate = new Date($scope.ReportFilter.StartDate);

                    if (TempEndDate <= TempMaxDate && TempEndDate >= TempStartDate) {
                        $scope.ReportFilter.StartDate = new Date($scope.ReportFilter.StartDate);
                        $scope.ReportFilter.EndDate = new Date($scope.ReportFilter.EndDate);

                        window.open('/export/DownloadRawDataExcel?StartDate=' + $scope.ReportFilter.StartDate.toDateString() + '&EndDate=' + $scope.ReportFilter.EndDate.toDateString() + '', '_blank');
                        //$scope.ReportFilter.StartDate = kendo.toString(kendo.parseDate($scope.ReportFilter.StartDate), 'MM/dd/yyyy');
                        //$scope.ReportFilter.EndDate = kendo.toString(kendo.parseDate($scope.ReportFilter.EndDate), 'MM/dd/yyyy');
                        $scope.ReportFilter.StartDate = HFCService.KendoDateFormat($scope.ReportFilter.StartDate);;
                        $scope.ReportFilter.EndDate = HFCService.KendoDateFormat($scope.ReportFilter.EndDate);;
                    } else {
                        HFC.DisplayAlert("Date range must be no greater than 1 year");
                    }
                }

            }
        }

        $scope.SalesJournalChange = function () {
            $("#SalesJournalAccounting").css('color', 'black');
        }

        $scope.OrderPaidInFullChange = function () {
            $("#OrderPaidInFullAccounting").css('color', 'black');
        }

        $scope.LoadUseTaxReport = function () {
            //if ($scope.ReportFilter.TerritoryId == "" || $scope.ReportFilter.TerritoryId == undefined || $scope.ReportFilter.TerritoryId == null) {
            //    alert("Please select Territory");
            //    return;
            //}
            if ($scope.ReportFilter.Year == "" || $scope.ReportFilter.Year == undefined || $scope.ReportFilter.Year == null) {
                alert("Please enter valid Year");
                return;
            }
            $scope.ReportFilter.StartDate = $scope.ReportFilter.Year.toString() + "-" + $scope.ReportFilter.Month + "-01";

            if ($('#gridUseTax').data('kendoGrid') == undefined) {
                $scope.UseTaxReport();
            }
            else {
                $('#gridUseTax').data('kendoGrid').dataSource.filter({});
                $('#gridUseTax').data('kendoGrid').dataSource.read();
            }
        }
        $scope.LoadSalesJournalReports = function () {

            if ($scope.ReportFilter.Value == null) {
                alert("Please select Accounting Method");
                return;
            }
            if ($scope.ReportFilter.Year == undefined) {
                alert("Please enter valid Year");
                return;
            }

            $scope.ReportFilter.StartDate = $scope.ReportFilter.Year.toString() + "-" + $scope.ReportFilter.Month + "-01";

            if ($('#gridSalesJournal').data('kendoGrid') == undefined) {
                $scope.SalesJournalReport();
            }
            else {
                $('#gridSalesJournal').data('kendoGrid').dataSource.filter({});
                $('#gridSalesJournal').data('kendoGrid').dataSource.read();
                //$('#gridSalesJournal').data('kendoGrid').refresh();
            }
        }

        $scope.LoadOrderPaidInFullReports = function () {

            //if ($scope.ReportFilter.Value == null) {
            //    alert("Please select Accounting Method");
            //    return;
            //}
            if ($scope.ReportFilter.Year == undefined) {
                alert("Please enter valid Year");
                return;
            }

            $scope.ReportFilter.StartDate = $scope.ReportFilter.Year.toString() + "-" + $scope.ReportFilter.Month + "-01";

            if ($('#gridOrdersPaidInFull').data('kendoGrid') == undefined) {
                $scope.OrdersPaidInFullReport();
            }
            else {
                $('#gridOrdersPaidInFull').data('kendoGrid').dataSource.filter({});
                $('#gridOrdersPaidInFull').data('kendoGrid').dataSource.read();
            }
        }

        $scope.MinstartDate = null;
        $scope.MaxstartDate = null;
        $scope.MinendDate = null;
        $scope.MaxendDate = null;
        //-------------  Load Data button click -------------//

        //-------------  Page Load Setup -------------//
        $scope.VendorList = function () {
            $scope.VendorList = {
                optionLabel: {
                    Name: "All",
                    VendorID: ""
                },
                dataTextField: "Name",
                dataValueField: "VendorID",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/VendorNameList",
                        }
                    }
                },
            };
        }
        $scope.CommercialTypeList = function () {
            $scope.CommercialTypeList = {
                optionLabel: {
                    Name: "All",
                    Id: ""
                },
                dataTextField: "Name",
                dataValueField: "Id",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/2/Type_LookUpValues",
                        }
                    }
                },
            };
        }
        $scope.IndustryList = function () {
            $scope.IndustryList = {
                optionLabel: {
                    Name: "All",
                    Id: ""
                },
                dataTextField: "Name",
                dataValueField: "Id",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/1/Type_LookUpValues",
                        }
                    }
                },
            };
        }
        $scope.SalePersonList = function () {
            $scope.SalePersonList = {
                optionLabel: {
                    PersonName: "All",
                    PersonId: ""
                },
                dataTextField: "PersonName",
                dataValueField: "PersonId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/GetFESalesPerson",
                        }
                    }
                },
            };
        }
        $scope.ZipCodeList = function () {
            $scope.ZipCodeList = {
                optionLabel: {
                    ZipCode: "All"
                },
                dataTextField: "ZipCode",
                dataValueField: "ZipCode",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/GetFEZip",
                        }
                    }
                },
                change: function (e) {
                    var value = this.value();
                    if (value == "All")
                        $scope.ReportFilter.Zip = null;
                }
            };
        }
        $scope.SourceList = function () {
            $scope.SourceList = {
                optionLabel: {
                    Name: "All",
                    SourceId: ""
                },
                dataTextField: "Name",
                dataValueField: "SourceId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/GetFESource",
                        }
                    }
                },
            };
        }
        $scope.CampaignList = function () {
            $scope.CampaignList = {
                optionLabel: {
                    Name: "All",
                    CampaignId: ""
                },
                dataTextField: "Name",
                dataValueField: "CampaignId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/GetFECampaign",
                        }
                    }
                },
            };
        }
        function contains(value, values) {
            for (var idx = 0; idx < values.length; idx++) {
                if (values[idx] === value) {
                    return true;
                }
            }
            return false;
        }

        $scope.SourceListIds = function () {
            $scope.ReportFilter.SourceList = [0];
            $scope.SourceListIds = {
                placeholder: "Select",
                dataTextField: "Name",
                dataValueField: "SourceId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                select: function (e) {
                    var dataItemValue = this.dataSource.view()[e.item.index()].SourceId;
                    var values = this.value();

                    if (dataItemValue !== 0 && contains(dataItemValue, values)) {
                        return;
                    }

                    if (dataItemValue === 0) {
                        values = [];
                    } else if (values.indexOf(0) !== -1) {
                        values = $.grep(values, function (value) {
                            return value !== 0;
                        });
                    }

                    values.push(dataItemValue);
                    this.value(values);
                    this.trigger("change"); //notify others for the updated values

                    e.preventDefault();
                },
                dataSource: {
                    transport: {
                        read: function (options) {
                            $http.get('/api/lookup/0/GetFESource').then(function (response) {
                                $scope.FullSourceList = response.data;
                                if ($scope.FullSourceList != null && $scope.FullSourceList.Length != 0) {
                                    $scope.FullSourceList.splice(0, 0, { SourceId: 0, Name: "ALL" })
                                    options.success($scope.FullSourceList);
                                }
                            });
                        }
                    }
                },
            };
        }

        $scope.ProductCategoryList = function () {
            $scope.ProductCategoryList = {
                optionLabel: {
                    PICProductGroupName: "All",
                    PICProductGroupName: "All"
                },
                dataTextField: "PICProductGroupName",
                dataValueField: "PICProductGroupName",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: function (e) {
                            $scope.FranchiseCode = '';
                            $scope.VendorLogin = false;
                            $http.get('/api/Reports/0/GetPICProductGroupForConfigurator?franchiseCode=' + $scope.FranchiseCode + '&vendorId=' + $scope.ReportFilter.VendorID).then(function (response) {
                                $scope.ProductCategory = response.data;
                                e.success($scope.ProductCategory);
                            }).catch(function (error) {
                            });
                        }
                    }
                },
            };
        }

        if (window.location.href.includes("SalesandPurchasingDetailReport")) {
            //var Lookupvendorvalues = ReportsService.Lookupvendor();
            //Lookupvendorvalues.then(function (data) {
            //    $scope.Lookupvendor = data.data;
            //}, function () {
            //});

            //var LookupIndustriesvalues = ReportsService.LookupIndustries();
            //LookupIndustriesvalues.then(function (data) {
            //    $scope.LookupIndustries = data.data;
            //}, function () {
            //});

            //var LookupCommercialTypevalues = ReportsService.LookupCommercialType();
            //LookupCommercialTypevalues.then(function (data) {
            //    $scope.LookupCommercialType = data.data;
            //}, function () {
            //});

            $scope.VendorList();
            $scope.IndustryList();
            $scope.CommercialTypeList();

        }

        if (window.location.href.includes("SalesSummaryByMonth")) {
            //var LookupFEZipcodes = ReportsService.LookupFEZip();
            //LookupFEZipcodes.then(function (data) {
            //    $scope.LookupFEZip = data.data;
            //}, function () {
            //});
            $scope.ZipCodeList();
            var LookupGetFETerritorys = ReportsService.LookupFETerritory();
            LookupGetFETerritorys.then(function (data) {
                $scope.LookupFETerritory = data.data;
            }, function () {
            });
            // Load Lookup value for sales person
            //var LookupGetFESalesPersonvalues = ReportsService.LookupGetFESalesPerson();
            //LookupGetFESalesPersonvalues.then(function (data) {
            //    $scope.LookupGetFESalesPerson = data.data;
            //}, function () {
            //});
            $scope.SalePersonList();
        }

        if (window.location.href.includes("VendorPerformance")) {
            //var Lookupvendorvalues = ReportsService.Lookupvendor();
            //Lookupvendorvalues.then(function (data) {
            //    $scope.Lookupvendor = data.data;
            //}, function () {
            //});

            // Load Lookup value for sales person
            //var LookupGetFESalesPersonvalues = ReportsService.LookupGetFESalesPerson();
            //LookupGetFESalesPersonvalues.then(function (data) {
            //    $scope.LookupGetFESalesPerson = data.data;
            //}, function () {
            //});
            $scope.VendorList();
            $scope.SalePersonList();
            $scope.ProductCategoryList();
            $scope.ReportFilter.Year = (new Date()).getFullYear();
        }

        if (window.location.href.includes("MarketingPerformance")) {
            //var LookupFESources = ReportsService.LookupFESource();
            //LookupFESources.then(function (data) {
            //    $scope.LookupFESource = data.data;
            //}, function () {
            //});

            //var LookupFECampaigns = ReportsService.LookupFECampaign();
            //LookupFECampaigns.then(function (data) {
            //    $scope.LookupFECampaign = data.data;
            //}, function () {
            //});

            //var LookupIndustriesvalues = ReportsService.LookupIndustries();
            //LookupIndustriesvalues.then(function (data) {
            //    $scope.LookupIndustries = data.data;
            //}, function () {
            //});

            //var LookupCommercialTypevalues = ReportsService.LookupCommercialType();
            //LookupCommercialTypevalues.then(function (data) {
            //    $scope.LookupCommercialType = data.data;
            //}, function () {
            //});

            //var LookupFEZipcodes = ReportsService.LookupFEZip();
            //LookupFEZipcodes.then(function (data) {
            //    $scope.LookupFEZip = data.data;
            //}, function () {
            //});

            //var LookupGetFESalesPersonvalues = ReportsService.LookupGetFESalesPerson();
            //LookupGetFESalesPersonvalues.then(function (data) {
            //    $scope.LookupGetFESalesPerson = data.data;
            //}, function () {
            //});
            $scope.SourceList();
            $scope.CampaignList();
            $scope.IndustryList();
            $scope.CommercialTypeList();
            $scope.ZipCodeList();
            $scope.SalePersonList();
        }

        if (window.location.href.includes("SalesSummaryReport")) {
            //sales person value
            //var LookupGetFESalesPersonvalues = ReportsService.LookupGetFESalesPerson();
            //LookupGetFESalesPersonvalues.then(function (data) {
            //    $scope.LookupGetFESalesPerson = data.data;
            //}, function () {
            //});
            $scope.SalePersonList();

            //commercial value
            //var LookupCommercialTypevalues = ReportsService.LookupCommercialType();
            //LookupCommercialTypevalues.then(function (data) {
            //    $scope.LookupCommercialType = data.data;
            //}, function () {
            //});
            $scope.CommercialTypeList();

            //industrial value
            //var LookupIndustriesvalues = ReportsService.LookupIndustries();
            //LookupIndustriesvalues.then(function (data) {
            //    $scope.LookupIndustries = data.data;
            //}, function () {
            //});
            $scope.IndustryList();
        }

        if (window.location.href.includes("OpenBalanceReport")) {
            //sales person value
            //var LookupGetFESalesPersonvalues = ReportsService.LookupGetFESalesPerson();
            //LookupGetFESalesPersonvalues.then(function (data) {
            //    $scope.LookupGetFESalesPerson = data.data;
            //}, function () {
            //});

            ////commercial value
            //var LookupCommercialTypevalues = ReportsService.LookupCommercialType();
            //LookupCommercialTypevalues.then(function (data) {
            //    $scope.LookupCommercialType = data.data;
            //}, function () {
            //});

            ////industrial value
            //var LookupIndustriesvalues = ReportsService.LookupIndustries();
            //LookupIndustriesvalues.then(function (data) {
            //    $scope.LookupIndustries = data.data;
            //}, function () {
            //});
            $scope.SalePersonList();
            $scope.CommercialTypeList();
            $scope.IndustryList();
        }

        if (window.location.href.includes("SalesJournalReport")) {
            $scope.ReportFilter.Year = (new Date()).getFullYear();
            //$scope.ReportFilter.Month = ((new Date()).getMonth() + 1).toString();
            var month = ((new Date()).getMonth() + 1).toString();
            if (month.length < 2) month = '0' + month;
            $scope.ReportFilter.Month = month;
        }

        if (window.location.href.includes("OrdersPaidInFull")) {
            $scope.ReportFilter.Year = (new Date()).getFullYear();
            //$scope.ReportFilter.Month = ((new Date()).getMonth() + 1).toString();
            var month = ((new Date()).getMonth() + 1).toString();
            if (month.length < 2) month = '0' + month;
            $scope.ReportFilter.Month = month;
        }


        // MSRReport
        if (window.location.href.includes("MsrReport")) {
            $scope.ReportFilter.Year = (new Date()).getFullYear();
            $scope.ReportFilter.Month = ((new Date()).getMonth() + 1).toString();
            if ($scope.ReportFilter.Month < 10) {
                $scope.ReportFilter.Month = '0' + $scope.ReportFilter.Month;
            }
            var LookupGetFETerritorys = ReportsService.LookupFETerritory();
            LookupGetFETerritorys.then(function (data) {
                $scope.LookupFETerritory = data.data;
                //for (i = 0; i < $scope.LookupFETerritory.length; i++) {
                //    $scope.LookupFETerritory[i].Name = $scope.LookupFETerritory[i].Name +" - "+ $scope.LookupFETerritory[i].Code
                //}
            }, function () {
            });
        }

        //PPCMatchbackReport
        if (window.location.href.includes("PPCMatchbackReport")) {
            $scope.SourceListIds();
        }

        //IncomeSummaryReport
        if (window.location.href.includes("IncomeSummaryReport")) {
            $scope.ReportFilter.Year = (new Date()).getFullYear();
        }

        //UseTax
        if (window.location.href.includes("UseTax")) {
            $scope.ReportFilter.Year = (new Date()).getFullYear();
            var month = ((new Date()).getMonth()).toString();
            if (month === '0') {
                month = '12';
                $scope.ReportFilter.Year = (new Date()).getFullYear() - 1;
            }
            if (month.length < 2) month = '0' + month;
            $scope.ReportFilter.Month = month;
            $scope.ZipCodeList();
            var LookupGetFETerritorys = ReportsService.LookupFETerritory();
            LookupGetFETerritorys.then(function (data) {
                var grayAreaOption = { TerritoryId:-1,Name:"Gray Area" }
                data.data.splice(0, 0, grayAreaOption);
                $scope.LookupFETerritory = data.data;
            }, function () {
            });
        }

        //-------------  Page Load Setup -------------//

        // ------------------- On Change -------------------------//
        $scope.ST_ReportTypeChange = function () {
            $("#SalesTaxSummaryReport").css('color', 'black');
        }

        $scope.FP_ReportTypeChange = function () {
            $("#franchiseperformreport").css('color', 'black');
            if ($scope.ReportFilter.FP_ReportType == "Sales Person") {
                if ($('#gridFranchisePerformanceReport_SalesAgent').data('kendoGrid') != undefined) {
                    $('#gridFranchisePerformanceReport_SalesAgent').addClass('hidegrid');
                    $('#gridFranchisePerformanceReport_SalesAgent').data('kendoGrid').dataSource.data([]);
                }
            }
            if ($scope.ReportFilter.FP_ReportType == "Territory") {
                if ($('#gridFranchisePerformanceReport_Territory').data('kendoGrid') != undefined) {
                    $('#gridFranchisePerformanceReport_Territory').addClass('hidegrid');
                    $('#gridFranchisePerformanceReport_Territory').data('kendoGrid').dataSource.data([]);
                }
            }
            if ($scope.ReportFilter.FP_ReportType == "Franchise") {
                if ($('#gridFranchisePerformanceReport_Franchise').data('kendoGrid') != undefined) {
                    $('#gridFranchisePerformanceReport_Franchise').addClass('hidegrid');
                    $('#gridFranchisePerformanceReport_Franchise').data('kendoGrid').dataSource.data([]);
                }
            }
            if ($scope.ReportFilter.FP_ReportType == "Zip") {
                if ($('#gridFranchisePerformanceReport_Zip').data('kendoGrid') != undefined) {
                    $('#gridFranchisePerformanceReport_Zip').addClass('hidegrid');
                    $('#gridFranchisePerformanceReport_Zip').data('kendoGrid').dataSource.data([]);
                }
            }
            if ($scope.ReportFilter.FP_ReportType == "Vendor") {
                if ($('#gridFranchisePerformanceReport_Vendor').data('kendoGrid') != undefined) {
                    $('#gridFranchisePerformanceReport_Vendor').addClass('hidegrid');
                    $('#gridFranchisePerformanceReport_Vendor').data('kendoGrid').dataSource.data([]);
                }
            }
            if ($scope.ReportFilter.FP_ReportType == "Product Category") {
                if ($('#gridFranchisePerformanceReport_ProductCategory').data('kendoGrid') != undefined) {
                    $('#gridFranchisePerformanceReport_ProductCategory').addClass('hidegrid');
                    $('#gridFranchisePerformanceReport_ProductCategory').data('kendoGrid').dataSource.data([]);
                }
            }
        }

        // ------------------- On Change -------------------------//

        //for date validation
        $scope.ValidatingDate = function (date, id) {
            $scope.DateRange = true;
            var ValidDateobj = {};
            if (!date)
                date = $("#" + id + "").val();

            if (date == "")
                $scope.ValidDate = true;
            else
                $scope.ValidDate = HFCService.ValidateDate(date, "date");

            if ($scope.ValidDatelist.find(item => item.id === id)) {
                var index = $scope.ValidDatelist.findIndex(item => item.id === id);
                $scope.ValidDatelist[index].valid = $scope.ValidDate;
            }
            else {
                ValidDateobj["id"] = id;
                ValidDateobj["valid"] = $scope.ValidDate;
                $scope.ValidDatelist.push(ValidDateobj);
            }

            if ($scope.ValidDate) {
                $("#" + id + "").removeClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).removeClass("icon_Background");
                $("#" + id + "").addClass("frm_controllead1");
            }
            else {
                $("#" + id + "").addClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).addClass("icon_Background");
                $("#" + id + "").removeClass("frm_controllead1");
            }
        }


        $scope.LoadAgingReport = function () {
            $scope.AgingOptions = {
                cache: false,
                excel: {
                    fileName: "AgingReport.xlsx",
                    allPages: true,
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '/api/Reports/0/GetAgingReport',
                            dataType: "json",
                            type: "GET",
                        }
                    }, schema: {
                        model: {
                            fields: {
                                CustName: { type: "string" },
                                CurrentPaid: { type: "number" },
                                Day1: { type: "number" },
                                Day31: { type: "number" },
                                Day61: { type: "number" },
                                Day91: { type: "number" },
                                OpenAgainst: { type: "number" },
                                TotalSale: { type: "number" }
                            }
                        },
                    },
                    aggregate: [
                        { field: "CurrentPaid", aggregate: "sum" },
                        { field: "Day1", aggregate: "sum" },
                        { field: "Day31", aggregate: "sum" },
                        { field: "Day61", aggregate: "sum" },
                        { field: "Day91", aggregate: "sum" },
                        { field: "OpenAgainst", aggregate: "sum" },
                        { field: "TotalSale", aggregate: "sum" }
                    ],
                    error: function (e) {
                        if (e.errorThrown != null && e.errorThrown != "")
                            HFC.DisplayAlert(e.errorThrown);
                        else if (e.xhr != null && e.xhr.responseText != null && e.xhr.responseText != "")
                            HFC.DisplayAlert(e.xhr.responseText);
                    }
                },
                dataBound: function (e) {
                    var dataSource = $("#GridAging").data("kendoGrid").dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: dataSource.data(),
                        filter: dataSource.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();
                    if (data != undefined && data.length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    if ($("#GridAging").find(".k-grid-header").next()[0] != $("#GridAging").find(".k-grid-footer")[0])
                        $("#GridAging").find(".k-grid-footer").insertAfter($("#GridAging .k-grid-header"));
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            var cell = row.cells[cellIndex];
                            if (row.type === "data" || row.type === "footer") {
                                switch (sheet.rows[0].cells[cellIndex].value) {
                                    case 'Current Paid':
                                    case '1-30':
                                    case '31-60':
                                    case '61-90':
                                    case '91-Older':
                                    case 'Open Against':
                                    case 'Total Sale':
                                        {
                                            if (cell.value === 0) {
                                                sheet.rows[rowIndex].cells[cellIndex].value = '';
                                            } else {
                                                cell.format = "$#,##0.00";
                                                cell.hAlign = "right";
                                            }
                                        }
                                }
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "CustName",
                        title: "Account Name",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Account Name" class="report_tooltip">Account Name</label>',
                        filterable: { search: true },
                        footerTemplate: function () {
                            var temp = new Date().getMonth() + 1;
                            return "Generated " + temp + "/" + new Date().getDate() + "/" + new Date().getFullYear();
                        },
                        width: "200px"
                    },
                    {
                        field: "CurrentPaid",
                        title: "Current Paid",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="Current Paid" class="report_tooltip">Current Paid</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        template: function (dataItem) {
                            if (dataItem.CurrentPaid)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.CurrentPaid) + "</span>";
                            else
                                return "";
                        },
                        width: "150px",
                    },
                    {
                        field: "Day1",
                        title: "1-30",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label  title="1-30" class="report_tooltip">1-30</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        template: function (dataItem) {
                            if (dataItem.Day1)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Day1) + "</span>";
                            else
                                return "";
                        },
                        width: "150px",
                    },
                    {
                        field: "Day31",
                        title: "31-60",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="31-60" class="report_tooltip">31-60</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        template: function (dataItem) {
                            if (dataItem.Day31)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Day31) + "</span>";
                            else
                                return "";
                        },
                        width: "150px"
                    },
                    {
                        field: "Day61",
                        title: "61-90",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="61-90" class="report_tooltip">61-90</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        template: function (dataItem) {
                            if (dataItem.Day61)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Day61) + "</span>";
                            else
                                return "";
                        },
                        width: "150px"
                    },
                    {
                        field: "Day91",
                        title: "91-Older",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="91-Older" class="report_tooltip">91-Older</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        template: function (dataItem) {
                            if (dataItem.Day91)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Day91) + "</span>";
                            else
                                return "";
                        },
                        width: "150px",
                    },
                    {
                        field: "OpenAgainst",
                        title: "Open Against",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Open Against" class="report_tooltip">Open Against</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        template: function (dataItem) {
                            if (dataItem.OpenAgainst)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.OpenAgainst) + "</span>";
                            else
                                return "";
                        },
                        width: "150px",
                    },
                    {
                        field: "TotalSale",
                        title: "Total Sale",
                        headerAttributes: { style: "white-space: normal" },
                        headerTemplate: '<label title="Total Sale" class="report_tooltip">Total Sale</label>',
                        filterable: { search: true },
                        aggregates: ["sum"],
                        footerTemplate: "#=kendo.toString(sum, 'C') #",
                        template: function (dataItem) {
                            if (dataItem.TotalSale)
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.TotalSale) + "</span>";
                            else
                                return "";
                        },
                        width: "150px",
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                filterMenuInit: function (e) {
                    $(e.container).find('.k-check-all').click();
                    e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: true,
                scrollable: true,
                resizable: true
            };
        };

    }]);
