﻿app.controller('purchaseOrderController', ['$http', '$scope', 'HFCService', 'NavbarService', 'PurchaseSearchService', 'AddressService', 'AdditionalAddressServiceTP', 'PrintService', '$routeParams', 'kendoService', 'AddressValidationService', '$filter','OrderService',
    function ($http, $scope, HFCService, NavbarService, PurchaseSearchService, AddressService, AdditionalAddressServiceTP, PrintService, $routeParams, kendoService, AddressValidationService, $filter, OrderService) {
        $scope.HFCService = HFCService;
        $scope.FranchiseName = HFCService.FranchiseName;
        $scope.BrandId = HFCService.CurrentBrand;
        $scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
        $scope.OrderService = OrderService;

        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;

        $scope.IsBusy = false;
        //enable case
        $scope.FranciseLevelCase = $scope.HFCService.FranciseLevelCase;
        $scope.CanCreateCase = false;

        $scope.Permission = {};
        var PurchaseOrderpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'PurchaseOrder')

        $scope.Permission.ListPurchase = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Master Purchase Order List').CanAccess
        $scope.Permission.SubmitOrder = PurchaseOrderpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Submit Master Purchase Order List').CanAccess
        $scope.Permission.ViewVendorPurchaseOrder = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Vendor Purchase Order').CanAccess
        $scope.Permission.ViewPurchase = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Master Purchase Order').CanAccess
        $scope.Permission.PurchaseEdit = PurchaseOrderpermission.CanUpdate;

        $scope.AddressService = AddressService;

        $scope.PrintService = PrintService; //added for the rendering printout page to take printout
        $scope.PurchaseSearchService = PurchaseSearchService;

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableOperationsProcurementDashboardTab();
        $scope.CancelReasonValue = '';
        $scope.VPOCancelReason = false;
        $scope.PurchaseSearchService.Get();
        $scope.masterPO = $routeParams.masterPO;
        $scope.shiplocation = 0;
        $scope.selectedShiplocation = 0;
        $scope.Address =
        {
            IsResidential: false,
            IsCommercial: false,
            AddressType: 'false',
            AttentionText: '',
            AddressId: '',
            Address1: '',
            Address2: '',
            City: '',
            Country: 'US',
            State: '',
            ZipCode: '',
            CountryCode2Digits: HFCService.FranchiseCountryCode,
            CrossStreet: '',
            Location: '',
            CreatedOn: '',
            CreatedBy: '',
            LastUpdatedOn: '',
            LastUpdatedBy: ''
        };
        $scope.VPOCancelReasonValue = '';
        $scope.MPO = {};
        $scope.CancelReasonList = [
            { ID: 1, Name: 'Customer Cancellation' },
            { ID: 2, Name: 'Entered  Incorrectly' },
            { ID: 3, Name: 'Incorrect Measurement / Product Configuration' }
        ];
        var record = 0;
        $scope.MultipleShippingAddress = function () {
            if (!$scope.MPO.DropShip) {
                $("#drpshipdiv").hide();

            }
            $scope.ShiplocationChange();

            $scope.popup("MultipleShippingAddress");
            if ($scope.MPO.DropShip && $scope.MPO.DropShipAddress != null) {
                $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.MPO.DropShipAddress.Address1);
            }
        }
        $scope.intToString = function (key) {
            return (!isNaN(key)) ? parseInt(key) : key;
        };
        $scope.convertToInt = function (id) {
            return parseInt(id, 10);
        };

        $scope.MPORelatedIds = {
            VendorAckIds: [],
            ShipmentIds: [],
            VendorInvoiceIds: [],
            PurchaseOrderId: '',           
        };

        $scope.GetMPORelatedIds = function () {
            $http.get('/api/PurchaseOrders/' + $scope.masterPO + '/GetMPORelatedIds').then(function (data) {
                var MPORelatedIds = data.data.data;
                if (MPORelatedIds.length > 0) {
                    for (var i = 0; i < MPORelatedIds.length; i++) {
                        if (MPORelatedIds[i].vendorReference) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.VendorAckIds))
                                $scope.MPORelatedIds.VendorAckIds.push({ vendorReference: MPORelatedIds[i].vendorReference, PurchaseOrderId: MPORelatedIds[i].PurchaseOrderId });
                            else if ($scope.MPORelatedIds.VendorAckIds.length > 0) {
                                var exists = $scope.MPORelatedIds.VendorAckIds.some(el => el.vendorReference === MPORelatedIds[i].vendorReference);
                                if (!exists)
                                    $scope.MPORelatedIds.VendorAckIds.push({ vendorReference: MPORelatedIds[i].vendorReference, PurchaseOrderId: MPORelatedIds[i].PurchaseOrderId });
                            }
                        }
                        if (MPORelatedIds[i].ShipmentId) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.ShipmentIds))
                                $scope.MPORelatedIds.ShipmentIds.push({ ShipmentId: MPORelatedIds[i].ShipmentId, ShipNoticeId: MPORelatedIds[i].ShipNoticeId });
                            else if ($scope.MPORelatedIds.ShipmentIds.length > 0) {
                                var exists = $scope.MPORelatedIds.ShipmentIds.some(el => el.ShipmentId === MPORelatedIds[i].ShipmentId);
                                if (!exists)
                                    $scope.MPORelatedIds.ShipmentIds.push({ ShipmentId: MPORelatedIds[i].ShipmentId, ShipNoticeId: MPORelatedIds[i].ShipNoticeId });
                            }
                        }

                        if (MPORelatedIds[i].VendorInvoiceId) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.VendorInvoiceIds))
                                $scope.MPORelatedIds.VendorInvoiceIds.push({ VendorInvoiceId: MPORelatedIds[i].VendorInvoiceId, InvoiceNumber: MPORelatedIds[i].InvoiceNumber });
                            else if ($scope.MPORelatedIds.VendorInvoiceIds.length > 0) {
                                var exists = $scope.MPORelatedIds.VendorInvoiceIds.some(el => el.InvoiceNumber === MPORelatedIds[i].InvoiceNumber);
                                if (!exists)
                                    $scope.MPORelatedIds.VendorInvoiceIds.push({ VendorInvoiceId: MPORelatedIds[i].VendorInvoiceId, InvoiceNumber: MPORelatedIds[i].InvoiceNumber });
                            }
                        }
                        //$scope.mporelatedids.vendorackids = MPORelatedIds.find(mpo => mpo.vendorreference);
                    }
                }
            });
        }

        if ($scope.masterPO > 0) {
            $http.get("/api/PurchaseOrders/" + $scope.masterPO + "/CheckMPOCancelled").then(function (response) {
                $scope.MPOCancelled = response.data;
                $scope.GetMPORelatedIds();
            });
        }

        $scope.EditConfiguration = function () {
            window.location.href =
                '/#!/MPOCoreProductconfigurator/' + $scope.MPO.OpportunityId + '/' + $scope.MPO.QuoteId + '/' + $scope.MPO.PurchaseOrderId;
        }

        $scope.mpoDetails = function () {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $http.get('/api/PurchaseOrders/' + $scope.masterPO + '/GetMPO/').then(function (response) {
                if (response.data.error === "") {
                    $scope.MPO = response.data.data;
                    //disable create case
                    for (i = 0; i < $scope.MPO.CoreProductVPOs.length; i++) {
                        if ($scope.MPO.CoreProductVPOs[i].ProductTypeId != 3 && $scope.MPO.CoreProductVPOs[i].ProductTypeId != 4 && $scope.MPO.CoreProductVPOs[i].ProductTypeId != 0)
                            $scope.CanCreateCase = true;
                    }

                    for (i = 0; i < $scope.MPO.MyProductVPOs.length; i++) {
                        if ($scope.MPO.MyProductVPOs[i].ProductTypeId != 3 && $scope.MPO.MyProductVPOs[i].ProductTypeId != 4 && $scope.MPO.MyProductVPOs[i].ProductTypeId != 0)
                            $scope.CanCreateCase = true;
                    }


                    if ($scope.MPO.CancelReason) {
                        $scope.CancelReasonValue = $scope.MPO.CancelReason;
                    }

                    $scope.OrderId = response.data.data.OrderId;
                    $scope.mpoDetailsGrid();
                   // $scope.vpoDetailsGrid();
                    $scope.vpoDetailsmyProductGrid();
                    $scope.Address = response.data.data.DropShipAddress;

                    $scope.ShipAddressId = $scope.MPO.ShipAddressId;
                    $scope.shiplocation = $scope.MPO.ShipToLocation;
                    $scope.FullAddress = response.data.data.Address;

                    //if (!$scope.MPO.ConfirmAddress) {
                    //    $scope.popup("MultipleShippingAddress");
                    //
                    //    //$("#shiptoAddress").kendoDropDownList({
                    //    //    optionLabel: "Select",
                    //    //    dataTextField: "ShipToName",
                    //    //    dataValueField: "Id",
                    //    //    dataSource: $scope.MPO.ShipToLocationList
                    //    //});

                    //}
                    if (!$scope.MPO.DropShip) {
                        $("#drpshipAddress :input").attr("disabled", true);
                        $("#countryBox").attr("disabled", true);
                    } else {
                        $("#drpshipAddress :input").attr("disabled", false);
                        $("#countryBox").attr("disabled", false);
                    }
                }
                else {
                    $scope.MPO = response.data.data;
                    $scope.OrderId = response.data.data.OrderId;
                    $scope.mpoDetailsGrid();
                   // $scope.vpoDetailsGrid();
                    $scope.vpoDetailsmyProductGrid();
                    $scope.Address = response.data.data.DropShipAddress;

                    $scope.ShipAddressId = $scope.MPO.ShipAddressId;
                    $scope.shiplocation = $scope.MPO.ShipToLocation;
                    $scope.FullAddress = response.data.data.Address;

                    HFC.DisplayAlert(response.data.error);
                }
                HFCService.setHeaderTitle("Purchase Order #" + $scope.MPO.MasterPONumber);
                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        };
        $scope.selectShipLocationOptions = {
            placeholder: "Select",
            dataTextField: "ShipToLocationName",
            dataValueField: "ShipToLocation",
            valuePrimitive: true,
            autoBind: true,
            dataSource: { data: $scope.MPO.ShipToLocationList }
        };
        $scope.ShiplocationChange = function () {
            for (var i = 0; i < $scope.MPO.ShipToLocationList.length; i++) {
                if ($scope.MPO.ShipToLocationList[i].Id === $scope.MPO.ShipToLocation) {
                    $scope.MPO.Address = $scope.MPO.ShipToLocationList[i].Address;
                    $scope.MPO.IsValidated = $scope.MPO.ShipToLocationList[i].IsValidated;
                    $scope.MPO.ShipAddressId = Number($scope.MPO.ShipToLocationList[i].AddressId);
                    //$scope.$apply();
                }
            }
        }

        $scope.popup = function (modalId) {
            $("#" + modalId).modal("show");
        }
        $scope.CancelMPO = function () {
            var myDiv = $('.shipaddres_popup');
            myDiv[0].scrollTop = 0;
            $scope.MPO.Address = $scope.FullAddress;
            $scope.MPO.ShipAddressId = $scope.ShipAddressId;
            $scope.MPO.ShipToLocation = $scope.shiplocation;
            $("#MultipleShippingAddress").modal("hide");
        }

        $scope.SaveShippingLocation = function (modalId) {
            if (modalId == 'Activeshiplocation' || modalId == 'DeActiveshiplocation') {
                $("#" + modalId).modal("hide");
                var dto = $scope.Ship;
                $http.post('/api/Shipping/0/ActivateDeactivateShip', dto).then(function (response) {
                    var status = response;
                    $scope.NullifyObject();
                    //$interval(function () { $scope.GetShipToLocation() }, 5000);
                    $scope.GetShipToLocation();
                });
            }

            else {
                $scope.submitted = true;
                $scope.form_ship.formAddressValidation.zipCode.$commitViewValue(true);

                // Check whether the forms contain value for all the required fields.
                // when any one of the filed is not given a value the following will return true.
                if ($scope.form_ship.$invalid) {
                    // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
                    return;
                }

                var value = $scope.Ship.ShipToName;
                $http.get('/api/Shipping/' + $scope.Ship.Id + '/DuplicateShippingName/?value=' + value).then(function (response) {
                    var status = response.data;
                    if (status) {
                        HFC.DisplayAlert("This Ship Name is Already Exist!");
                    }
                });
                $("#" + modalId).modal("hide");
                var dto = $scope.Ship;
                $scope.Addresses = {};
                dto.Addresses = $scope.Address;
                $scope.NullifyObject();
                $http.post('/api/Shipping/0/SaveShipToLocation', dto).then(function (response) {
                    var status = response;
                    if (status.statusText == "OK")
                        $("#gridShipLocation").data("kendoGrid").dataSource.read()
                });
            }
        }

        $http({ method: 'GET', url: '/api/address/', params: { leadIds: 0, includeStates: true } })
            .then(function (response) {
                $scope.AddressService.States = response.data.States || [];
                $scope.AddressService.Countries = response.data.Countries || [];
                $scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
            }, function (response) {
            });

        $scope.confirmAddress = function () {
            $http.get('/api/PurchaseOrders/' + $scope.masterPO + '/ConfirmAddress/', $scope.MPO).then(function (response) {
                if (response.data.error === "") {
                    $scope.CancelMPO();
                }
                else {
                    HFC.DisplayAlert(response.data.error);
                }
            });
        };
        $scope.GetmpoList = function () {
            $http.get('/api/PurchaseOrders/0/GetMPOList/').then(function (response) {
                if (response.data.error === "") {
                    $scope.MPOList = response.data.data;
                    $scope.mpoListGrid();
                }
                else {
                    HFC.DisplayAlert(response.data.error);
                }
            });
        };

        if ($scope.masterPO != undefined) {
            $scope.mpoDetails();
        } else {
            $scope.GetmpoList();
        }
        $scope.mpoListGrid = function () {
            $scope.Purchase_options = {
                excel: {
                    fileName: "PurchaseOrders_Export.xlsx",
                    allPages: true
                },
                dataSource: {
                    transport: {
                        read: function (e) {
                            e.success($scope.MPOList);
                        }
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                },
                dataBound: function (e) {
                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                },

                noRecords: { template: "No records found" },
                columns: [
                     {
                         field: "Territory",
                         title: "Territory",
                         filterable: { multi: true, search: true }
                     },
                            {
                                field: "SubmittedDate",
                                title: "Submitted Date",
                                type: "date",
                                filterable: HFCService.gridfilterableDate("date", "", ""),
                                template: function (dataitem) {
                                    if (dataitem.SubmittedDate == null || dataitem.SubmittedDate == '')
                                        return "";
                                    else
                                        return HFCService.KendoDateFormat(dataitem.SubmittedDate);
                                }
                                    //"#=  (SubmittedDate == null)? '' : kendo.toString(kendo.parseDate(SubmittedDate, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                            },

                            {
                                field: "MasterPONumber",
                                title: "Master PO #",
                                hidden: false,
                                filterable: { multi: true, search: true },
                                template: "<a href='\\\\#!/purchasemasterview/#= PurchaseOrderId #' style='color: rgb(61,125,139);'><span>#= MasterPONumber #</span></a> "
                            },

                             {
                                 field: "AccountName",
                                 title: "Account Name",
                                 hidden: false,
                                 template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= AccountName #</span></a> "
                             },
                              {
                                  field: "OpportunityName",
                                  title: "Opportunity",
                                  hidden: false,
                                  template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span>#= OpportunityName #</span></a> "
                              },

                             {
                                 field: "OrderNumber",
                                 title: "Sales Order",
                                 hidden: false,
                                 template: "<a href='\\\\#!/Orders/#= OrderId #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a> "
                             },
                    {
                        field: "Status",
                        title: "Status",
                        hidden: false
                    }
                ],
                editable: false,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                },
                scrollable: true,
                toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="tp_leadsearch"><div class="leadsearch_topleft col-sm-10 col-md-10 col-xs-10 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="MPOSearch()" type="search" id="searchBox" placeholder="Search Purchase Order" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 no-padding hfc_leadbut"><input type="button" id="btnReset" ng-click="MPOSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="col-sm-2 col-xs-2 col-md-2 no-padding"><div class="leadsearch_icon"><button type="button" class="plus_but tooltip-bottom" data-tooltip="Download Purchase Order" ng-click="DownloadPuchaseOrder_Excel()"><i class="far fa-cloud-download"></i></button>&nbsp;</div></div></div></script>').html()) }],
            };
        };

        $scope.mpoDetailsGrid = function () {
            $scope.MPODetails = {
                dataSource: {
                    transport: {
                        read: function (e) {
                            e.success($scope.MPO.MPOHeaderTable);
                        }
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                }, dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                dataBound: function (e) {
                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                },
                group: {
                    field: "TotalLines"
                },

                noRecords: { template: "No records found" },
                columns: [
                            {
                                field: "LineNumber",
                                title: "Line ID",
                                template: function (dataItem) {
                                    if (dataItem.LineNumber)
                                        return "<span class='Grid_Textalign'>" + dataItem.LineNumber + "</span>";
                                    else
                                        return "";
                                },
                            },
                            {
                                field: "PICPO",
                                title: "PO#",
                                hidden: false,
                                filterable: { multi: true, search: true },
                                template: function (dataItem) {
                                    if (dataItem.PICPO)
                                        return "<span class='Grid_Textalign'>" + dataItem.PICPO + "</span>";
                                    else
                                        return "";
                                },
                            },
                            {
                                field: "VendorName",
                                title: "Vendor",
                                hidden: false,
                                filterable: { multi: true, search: true }
                            },

                             {
                                 field: "TotalLines",
                                 title: "Total Lines",
                                 hidden: false,
                                 template: function (dataItem) {
                                     if (dataItem.TotalLines)
                                         return "<span class='Grid_Textalign'>" + dataItem.TotalLines + "</span>";
                                     else
                                         return "";
                                 },
                             },
                              {
                                  field: "TotalQuantity",
                                  title: "Total Quantity",
                                  hidden: false,
                                  template: function (dataItem) {
                                      if (dataItem.TotalQuantity)
                                          return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.TotalQuantity, 'number') + "</span>";
                                      else
                                          return "";
                                  },
                              },
                             {
                                 field: "ShipDate",
                                 title: "Est. Ship Date",
                                 hidden: false,
                                 filterable: HFCService.gridfilterableDate("date", "", ""),
                                 template: function (dataitem) {
                                     if (dataitem.ShipDate == null || dataitem.ShipDate == '')
                                         return "";
                                     else
                                         return HFCService.KendoDateFormat(dataitem.ShipDate);
                                 }
                                     //"#=  (ShipDate == null)? '' : kendo.toString(kendo.parseDate(ShipDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                             },
                    {
                        field: "Status",
                        title: "Status",
                        hidden: false,
                        template: function (dataItem) {
                            if (dataItem.Status === "Error") {
                                return "<span style='color: red;font-weight: 900;'>" +
                                    dataItem.Status +
                                    "</span>";
                            } else if (dataItem.Status === null) {
                                return '';
                            } else {
                                return dataItem.Status;
                            }
                        },
                    },

                ],

                editable: false,
                resizable: true,
                scrollable: true,
            };
        };
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 240);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 240);
            };
        });
        // End Kendo Resizing

        function currency(data) {
            if (data.Freight > 40) {
                return kendo.toString(data.Freight, "£###.##")
            }
            return kendo.toString(data.Freight, "$###.##")
        }
        $scope.coreVPOGroupHeader = function coreVPOGroupHeader(data) {
            //
            var coreVPOs = $scope.MPO.CoreProductVPOs;
            var statusId;
            var cancelbtn = '<input type="button" ng-click="CancelVPO(' +
                data.value +
                ')"  style="float: right;" class="k-button btn btn-primary cancel_but" value="Cancel Vendor PO" />';
            for (var i = 0; i < coreVPOs.length; i++) {
                if (coreVPOs[i].PICPO === data.value) {
                    statusId = coreVPOs[i].StatusId;
                }
            }
            if (statusId === 8) {
                cancelbtn = "";
            }
            return '<div>   <div class="row">     <div class="col-sm-4"> <label class="required_label">Purchase Order #  </label><label style="margin-left: 2%;" class="ldet_label"> ' +
                data.value +
                '</label></div>     <div class="col-sm-4"><label class="required_label">Shipment Id </label> <label style="margin-left: 2%;" class="ldet_label"></label> </div> 	<div class="col-sm-4">' + cancelbtn + '</div>   </div>   </div>';
        }
        $scope.CancelGroupVPOPopup = function (val) {
            $scope.VPOCancelReasonValue = '';
            $scope.vpoId = val;
            $("#cancelVPOmodal").modal("show");
        }
        $scope.CancellineVPOPopup = function (val) {
            var temp = [];
            temp.push(val);
            $scope.vpoId = temp;
            $scope.VPOCancelReasonValue = '';
            $("#cancelVPOmodal").modal("show");
        }
        $scope.CancelVPOPopup = function (val) {
            $scope.VPOCancelReasonValue = '';
            $("#cancelVPOmodal").modal("hide");
        }
        $scope.CancelVPO = function (dataItem) {
            var cnre = $scope.VPOCancelReason;
            $scope.vpoId = dataItem;
            $("#cancelVPOmodal").modal("show");
        };
        $scope.SelectVPOCancelReason = function () {
            $scope.VPOCancelReason = true;
            $scope.CancelReasonList = [
               { ID: 1, Name: 'Customer Cancellation' },
               { ID: 2, Name: 'Entered  Incorrectly' },
               { ID: 3, Name: 'Incorrect Measurement / Product Configuration' }

            ]
        }

        $scope.ExpandAll = function () {
            $("#panelbar").data("kendoPanelBar").expand($("li", "#panelbar"));
            $("#expandbtn").hide();
            $("#collapsebtn").show();
        }
        $scope.CollapseAll = function () {
            $("#panelbar").data("kendoPanelBar").collapse($("li", "#panelbar"));
            $("#collapsebtn").hide();
            $("#expandbtn").show();
        }
        $("#collapsebtn").hide();
        $("#expandbtn").show();



        $scope.MyProductStatusUpdate = function (pod) {
            if (pod.Status === "Canceled") {
                $scope.CancellineVPOPopup(pod);
            }

        }

        $scope.ContinueCancelVPO = function (vpo) {
            var cnre = $scope.VPOCancelReasonValue;
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";

            var vpoList = [];

          //  var cnre = $scope.GroupCancelVPOReasonValue;
            $.each(vpo, function (key, value) {
                vpoList.push(value.PurchaseOrdersDetailId);
            });

            var vpos = JSON.stringify(vpoList);
            $http.get('/api/PurchaseOrders/' + $scope.masterPO + '/CancelVPO?VPO=' + vpos + "&cancelReason=" + cnre).then(function (response) {
                if (response.data.error === "") {
                    $scope.MPO = response.data.data;

                    if ($('#VendorGrid').data('kendoGrid') != undefined) {
                        $('#VendorGrid').data('kendoGrid').dataSource.read();
                        $('#VendorGrid').data('kendoGrid').refresh();
                    }

                    if ($('#VpoCoreProductGrid').data('kendoGrid') != undefined) {
                        $('#VpoCoreProductGrid').data('kendoGrid').dataSource.read();
                        $('#VpoCoreProductGrid').data('kendoGrid').refresh();
                    }

                    if ($('#VpoMyVendorGrid').data('kendoGrid') != undefined) {
                        $('#VpoMyVendorGrid').data('kendoGrid').dataSource.read();
                        $('#VpoMyVendorGrid').data('kendoGrid').refresh();
                    }
                } else {
                    HFC.DisplayAlert(response.data.error);
                }
                $scope.CancelVPOPopup();
                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });
        }
        $scope.PrintMPO = function () {
            $scope.PrintService.PrintMPO($scope.masterPO);
        }

        $scope.vpoDetailsmyProductGrid = function () {
            $scope.vpomyProductDetails = {
                dataSource: {
                    transport: {
                        read: function (e) {
                            e.success($scope.MPO.MyProductVPOs);
                        },
                        update: function (e) {
                            $http.post('/api/PurchaseOrders/0/SaveMyProductInlineVPO', e.data).then(function (response) {
                                if (response.data.error == '') {
                                    $scope.MPO = response.data.data;

                                    $('#VendorGrid').data('kendoGrid').dataSource.read();
                                    $('#VendorGrid').data('kendoGrid').refresh();

                                    $('#VpoCoreProductGrid').data('kendoGrid').dataSource.read();
                                    $('#VpoCoreProductGrid').data('kendoGrid').refresh();

                                    $('#VpoMyVendorGrid').data('kendoGrid').dataSource.read();
                                    $('#VpoMyVendorGrid').data('kendoGrid').refresh();

                                    HFC.DisplaySuccess("VPO Updated Sucessfully.");
                                }
                                else {
                                    HFC.DisplayAlert(response.error);
                                }
                            });
                        },
                    },
                    //group: [{ field: "VendorName" }, { field: "PONumber" }],
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    schema: {
                        model: {
                            id: "PurchaseOrdersDetailId",
                            fields: {
                                PurchaseOrdersDetailId: { type: "number", editable: false, nullable: false },
                                VendorName: { type: "string", editable: false, visable: false },
                                ProductName: { type: "string", editable: false, visable: false },
                                PartNumber: { type: "string", editable: false, visable: false },
                                Description: { type: "string", editable: false, visable: false },
                                Quantity: { type: "number", editable: false, visable: false },
                                ExtendedPrice: { type: "number", visable: false },
                                purchased: { type: "number", editable: true, visable: false },

                                PickedBy: { type: "string", editable: true, visable: false },

                                ShipDate: { type: "date", editable: true, visable: false },
                                Status: { type: "string", editable: true, visable: false }
                            }
                        }
                    }
                }, dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                dataBound: function (e) {
                    //console.log("dataBound");
                    $scope.MyProductVPOsCount = this.dataSource.view().length;//$("#VpoMyVendorGrid").data("kendoGrid").dataSource._data.length;
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.c
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show()
                    }
                },
                //group: { field: "VendorName" },
                noRecords: { template: "No records found" },
                columns: [{
                    field: "LineNumber",
                    title: "Line Num",
                    hidden: false,
                    editable: false,
                    filterable: { multi: true, search: true },
                    width: "15%",
                },
                             {
                                 field: "VendorName",
                                 title: "Vendor Name",
                                 hidden: false,
                                 editable: false,
                                 groupHeaderTemplate: "Vendor Name: #= VendorName #"
                             },
                    {
                        field: "PONumber",
                        title: "PO #",
                        hidden: false,
                        editable: false,
                        filterable: { multi: true, search: true }
                    },
                             {
                                 field: "ProductName",
                                 title: "Product",
                                 hidden: false,
                                 editable: false,
                                 filterable: { multi: true, search: true }
                             },

                              {
                                  field: "PartNumber",
                                  title: "Product Number",
                                  hidden: false,
                                  editable: false,
                              },
                               {
                                   field: "Description",
                                   title: "Description",
                                   hidden: false,
                                   editable: false,

                                   template: "<div>#=Description#</div>"
                               },
                     {
                         field: "Quantity",
                         title: "QTY",
                         hidden: false,
                         editable: false,
                         width: "100px",
                     },
                     {
                         field: "ExtendedPrice",
                         title: "Cost",
                         hidden: false,
                         format: "{0:c2}",
                         width: "100px"
                     },
                     {
                         field: "purchased",
                         title: "Purchased / Picked",
                         hidden: false,
                         editable: false,
                     },
                     {
                         field: "PickedBy",
                         title: "By",
                         hidden: false,
                         editable: false,
                     },

                              {
                                  field: "ShipDate",
                                  title: "Date",
                                  hidden: false,
                                  filterable: HFCService.gridfilterableDate("date", "", ""),
                                  template: function (dataitem) {
                                      if (dataitem.ShipDate == null || dataitem.ShipDate == '')
                                          return "";
                                      else
                                          return HFCService.KendoDateFormat(dataitem.ShipDate);
                                  }
                                      //"#=  (ShipDate == null)? '' : kendo.toString(kendo.parseDate(ShipDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                              },
                     {
                         field: "Status",
                         title: "Status",
                         editor: StatusDropdown,
                         hidden: false,
                         width: "150px",
                     },
                     { command: ["edit"], title: "&nbsp;", width: "100px" }

                ],
                editable: "inline",
                resizable: true,
                scrollable: true,
            };
        };
        $scope.editmyprdVPO = function (vDetails) {
            var dispcls = '.disp' + vDetails.PurchaseOrdersDetailId;
            var editcls1 = '#edit' + vDetails.PurchaseOrdersDetailId;
            var editcls = '.edit' + vDetails.PurchaseOrdersDetailId;

            $(dispcls).hide();
            $(editcls1).show();
            $(editcls).show();
            $(document).click();
        }
        $scope.UpdatemyprdVPO = function (vDetails) {
            var dispcls = '.disp' + vDetails.PurchaseOrdersDetailId;
            var editcls = '.edit' + vDetails.PurchaseOrdersDetailId;
            var editcls1 = '#edit' + vDetails.PurchaseOrdersDetailId;
            $http.post('/api/PurchaseOrders/0/SaveMyProductInlineVPO', vDetails).then(function (response) {
                if (response.data.error == '') {
                    $scope.MPO = response.data.data;

                    $('#VendorGrid').data('kendoGrid').dataSource.read();
                    $('#VendorGrid').data('kendoGrid').refresh();

                    HFC.DisplaySuccess("VPO Updated Sucessfully.");
                    $(dispcls).show();
                    $(editcls).hide();
                    $(editcls1).hide();
                }
                else {
                    HFC.DisplayAlert(response.error);
                }
            });
        }
        $scope.CancelmyprdVPO = function (vDetails) {
            var dispcls = '.disp' + vDetails.PurchaseOrdersDetailId;
            var editcls = '.edit' + vDetails.PurchaseOrdersDetailId;
            var editcls1 = '#edit' + vDetails.PurchaseOrdersDetailId;
            for (var i = 0; i < $scope.MPO.MyProductVPOs.length; i++) {
                if ($scope.MPO.MyProductVPOs[i].PurchaseOrdersDetailId === vDetails.PurchaseOrdersDetailId) {
                    for (var j = 0; j < $scope.MPO.MyProductGroupVpos.length; j++) {
                        for (var k = 0; k < $scope.MPO.MyProductGroupVpos[j].VPODetails.length; k++) {
                            if ($scope.MPO.MyProductGroupVpos[j].VPODetails[k].PurchaseOrdersDetailId === vDetails.PurchaseOrdersDetailId) {
                                $scope.MPO.MyProductGroupVpos[j].VPODetails[k] = $scope.MPO.MyProductVPOs[i];
                            }
                        }
                    }
                }
            }

            $(dispcls).show();
            $(editcls).hide();
            $(editcls1).hide();
        }
        $scope.SubmitPurchaseOrder = function () {
            $scope.IsBusy = true;

            var loadingElement = document.getElementById("loading");

            ///Start-remove after Async
            var node = document.createElement("span");
            var textnode = document.createTextNode("Please wait, this may take a few moments...... ");
            node.style.fontSize = "30px";
            node.style.marginTop = "100px";
            node.appendChild(textnode);
            loadingElement.appendChild(node);
            //End remmove after Async

            loadingElement.style.display = "block";

            $http.get('/api/PurchaseOrders/' + $scope.OrderId + '/SubmitMPO?mpoId=' + $scope.masterPO).then(function (response) {
                if (response.data.error === "") {
                    $scope.MPO = response.data.data;
                    $scope.MPO = response.data.data;

                    $('#VendorGrid').data('kendoGrid').dataSource.read();
                    $('#VendorGrid').data('kendoGrid').refresh();

                    $('#VpoCoreProductGrid').data('kendoGrid').dataSource.read();
                    $('#VpoCoreProductGrid').data('kendoGrid').refresh();

                    $('#VpoMyVendorGrid').data('kendoGrid').dataSource.read();
                    $('#VpoMyVendorGrid').data('kendoGrid').refresh();
                } else {
                    HFC.DisplayAlert(response.data.error);
                }
                loadingElement.removeChild(node);
                loadingElement.style.display = "none";
                $scope.IsBusy = false;
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.removeChild(node);
                loadingElement.style.display = "none";
                $scope.IsBusy = false;

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });
        };

        //$scope.tooltipOptions = {
        //    filter: "th",
        //    position: "top",
        //    hide: function (e) {
        //        this.content.parent().css('visibility', 'hidden');
        //    },
        //    show: function (e) {
        //        if (this.content.text().length > 1) {
        //            this.content.parent().css('visibility', 'visible');
        //        } else {
        //            this.content.parent().css('visibility', 'hidden');
        //        }
        //    },
        //    content: function (e) {
        //        var target = e.target.data().title; // element for which the tooltip is shown
        //        return target;//$(target).text();
        //    }
        //};

        $scope.addEditNotestoVendor = function (dataItem) {
            $scope.vpoDetails = dataItem;
            $("#notetoVendormodal").modal("show");
        };

        $scope.CancelWarningOrInfo = function () {
            $("#warningOrInfoModel").modal("hide");
        }
        $scope.showWarningOrInfo = function (dataItem) {
            //$scope.warningOrinfoMsg = dataItem.Information + dataItem.Warning + dataItem.Errors;
            //$("#warningOrInfoModel").modal("show");
            $scope.vpoDetails = dataItem;
            $("#Errormodal").modal("show");
        }
        $scope.CancelMPOError = function () {
            //$scope.vpoDetails.Errors = $scope.MPO.Message;
            $("#MPOErrormodal").modal("hide");
        }

        $scope.ViewMPOError = function () {
            var temp = $scope.MPO;
            $("#MPOErrormodal").modal("show");
        }
        $scope.ViewVPOError = function (dataItem) {
            $scope.vpoDetails = dataItem;
            $("#Errormodal").modal("show");
        }
        $scope.CancelError = function (dataItem) {
            //$scope.vpoDetails = dataItem;
            $("#Errormodal").modal("hide");
        }
        $scope.SavenoteToVendor = function (data) {
            $http.post('/api/PurchaseOrders/0/SaveNotestoVendor', data).then(function (response) {
            });
            $("#notetoVendormodal").modal("hide");
        }
        $scope.UpdatAndConfirmAddress = function (mpo, addr) {
            var myDiv = $('.shipaddres_popup');
            myDiv[0].scrollTop = 0;

            $scope.MPO.DropShipAddress = addr;
            //var loadingElement = document.getElementById("loading");
            //loadingElement.style.display = "block";
            if ($scope.MPO.DropShip && $scope.MPO.DropShipAddress != null) {
                saveAddressHandler($scope.MPO);
            }
            else
                AfterValidateAddress($scope.MPO);
            //$http.post('/api/PurchaseOrders/0/AddUpdateShipAddress', $scope.MPO).then(function (response) {
            //    if (response.data.error === "") {
            //        $scope.MPO = response.data.data;
            //        $scope.OrderId = response.data.data.OrderId;
            //        $scope.mpoDetailsGrid();
            //        $scope.vpoDetailsGrid();
            //        $scope.vpoDetailsmyProductGrid();
            //        $scope.Address = response.data.data.DropShipAddress;

            //        if (!$scope.MPO.DropShip) {
            //            $("#drpshipAddress :input").attr("disabled", true);
            //            $("#countryBox").attr("disabled", true);
            //        } else {
            //            $("#drpshipAddress :input").attr("disabled", false);
            //            $("#countryBox").attr("disabled", false);
            //        }
            //        $("#MultipleShippingAddress").modal("hide");

            //    }
            //    else {
            //        HFC.DisplayAlert(response.data.error);
            //    }
            //    loadingElement.style.display = "none";

            //    }).catch(function (error) {
            //        var loadingElement = document.getElementById("loading");
            //        loadingElement.style.display = "none";
            //    });
        }

        function saveAddressHandler(dto) {
            if (dto.DropShip) {
                validateAddress(dto.DropShipAddress)
                    .then(function (response) {
                        var objaddress = response.data;

                        if (objaddress.Status == "error") {
                            //TP-1793: CLONE - The request failed with HTTP status 503: Service Temporarily Unavailable.
                            AfterValidateAddress(dto);
                            //HFC.DisplayAlert(objaddress.Message);
                            //$scope.IsBusy = false;
                        } else if (objaddress.Status == "true") {
                            var validAddress = objaddress.AddressResults[0];
                            dto.DropShipAddress.Address1 = validAddress.address1;
                            $scope.Addressvalues = dto.DropShipAddress.Address1;
                            dto.DropShipAddress.Address2 = validAddress.address2;
                            dto.DropShipAddress.City = validAddress.City;
                            dto.DropShipAddress.State = validAddress.StateOrProvinceCode;

                            // Update that the address validated to true.
                            dto.DropShipAddress.IsValidated = true;

                            // assign the zipcode back to the model before saving.
                            if (dto.DropShipAddress.CountryCode2Digits == "US") {
                                dto.DropShipAddress.ZipCode = validAddress.PostalCode.substring(0, 5);
                            }
                            else {
                                dto.DropShipAddress.ZipCode = validAddress.PostalCode;
                            }

                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                            AfterValidateAddress(dto);
                        } else if (objaddress.Status == "false") {
                            dto.DropShipAddress.IsValidated = false;

                            var errorMessage = "Invalid Address. Do you wish to continue?";

                            $scope.Addressvalues = dto.DropShipAddress.Address1;
                            //address get empty so assiging the value here.

                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);

                            if (confirm(errorMessage)) {
                                AfterValidateAddress(dto);
                            }
                            else {
                                $scope.IsBusy = false;
                            }
                        } else { //SkipValidation
                            AfterValidateAddress(dto);
                        }
                    },
                    function (response) {
                        HFC.DisplayAlert(response.statusText);
                        $scope.IsBusy = false;
                    });
            }
        };

        function validateAddress(obj) {
            var addressModeltp = {
                address1: obj.Address1,
                address2: obj.Address2,
                City: obj.City,
                StateOrProvinceCode: obj.State,
                PostalCode: obj.ZipCode,
                CountryCode: obj.CountryCode2Digits
            };

            if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                var result = {
                    data: {
                        Status: "SkipValidation",
                        Message: "No Address validation required."
                    },
                    status: 200,
                    statusText: "OK"
                }

                return $q.resolve(result);;
            }

            // Call the api to Return the result.
            return AddressValidationService.validateAddress(addressModeltp);
        }

        function AfterValidateAddress(obj) {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $scope.MPO = obj;

            //code for spacing in Canada Postal/ZipCode
            if ($scope.MPO.DropShipAddress != null)
            {
                if (!$scope.MPO.DropShipAddress.IsValidated) {
                    if ($scope.MPO.DropShipAddress.CountryCode2Digits == "CA") {
                        if ($scope.MPO.DropShipAddress.ZipCode) {
                            var Ca_Zipcode = $scope.MPO.DropShipAddress.ZipCode;
                            Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                            $scope.MPO.DropShipAddress.ZipCode = Ca_Zipcode;
                        }
                    }
                }
            }

            $http.post('/api/PurchaseOrders/0/AddUpdateShipAddress', $scope.MPO).then(function (response) {
                if (response.data.error === "") {
                    $scope.MPO = response.data.data;
                    $scope.OrderId = response.data.data.OrderId;
                    //$scope.mpoDetailsGrid();
                    //$scope.vpoDetailsGrid();
                    //$scope.vpoDetailsmyProductGrid();
                    $scope.Address = response.data.data.DropShipAddress;

                    //displaying previously saved address on cancel
                    $scope.FullAddress = response.data.data.Address;
                    $scope.MPO.ShipToLocation = response.data.data.ShipToLocation;
                    $scope.shiplocation = response.data.data.ShipToLocation;

                    if (!$scope.MPO.DropShip) {
                        $("#drpshipAddress :input").attr("disabled", true);
                        $("#countryBox").attr("disabled", true);
                    } else {
                        $("#drpshipAddress :input").attr("disabled", false);
                        $("#countryBox").attr("disabled", false);
                    }
                    $("#MultipleShippingAddress").modal("hide");
                }
                else {
                    HFC.DisplayAlert(response.data.error);
                }
                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }

        $scope.dropShipSelected = function () {
            var mpo = $scope.MPO;
            if (!$scope.MPO.DropShip) {
                $("#drpshipAddress :input").attr("disabled", true);
                $("#countryBox").attr("disabled", true);
                $("#ddlShiptoLocation").attr("disabled", false);
                $("#drpshipdiv").hide();
            } else {
                $("#drpshipdiv").show();
                $scope.MPO.ShipAddressId = null;
                $scope.MPO.ShipToLocation = null;
                $("#drpshipAddress :input").attr("disabled", false);
                $("#countryBox").attr("disabled", false);
                $("#ddlShiptoLocation").attr("disabled", true);
                $scope.Address = {};
                $scope.Address.CountryCode2Digits = $scope.HFCService.FranchiseCountryCode;
                //$scope.Address.Address1 = "";
                //$scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Address.Address1);
            }
        };
        $scope.CancelNotetoVendor = function () {
            $("#notetoVendormodal").modal("hide");
        }
        //render the popup when purchasepo is selected
        $scope.VendorPo = function () {
            $("#purchasemodal").modal("show");
        }
        //rendered popup cancel button functionality and for the vendor-cancel-PO
        $scope.Cancel = function () {
            $("#purchasemodal").modal("hide");
        }
        //vendor-cancel-PO first popup
        $scope.Poaccept = function () {
            $("#purchasemodal").modal("hide");
        }
        //render second popup when purchasepo is selected
        $scope.VendorPoDate = function () {
            $("#purchasemodalsecond").modal("show");
        }
        //second popup cancel
        $scope.Poacceptdata = function () {
            $("#purchasemodalsecond").modal("hide");
        }

        //generating the Popup for Print-selection
        $scope.Fileprint = function (model, id) {
            if (model == "PrintPopup") {
                $("#" + model).modal("show");
            }
        }
        //Popup for Print-selection cancel
        $scope.PrintCancel = function () {
            $("#PrintPopup").modal("hide");
        }
        //added for the print page dispaly to take print-out
        $scope.ApplyPrint = function () {
            var data = $('input[name="Check1"]:checked').val();
            var data2 = $('input[name="Check2"]:checked').val();
            if (data == "on" || data2 == "on") {
                $scope.PrintService.PrintPurchaseOrder(data, data2);
            }
        }

        $scope.MPOSearch = function () {
            $("#gridOrderSearch").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                    {
                        field: "OrderNumber",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "OpportunityName",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "AccountName",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "Status",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "MasterPONumber",
                        operator: "contains",
                        value: searchValue
                    }
                ]
            });
        };
        $scope.MPOSearchClear = function () {
            $('#gridOrderSearch').val('');
            $("#gridOrderSearch").data('kendoGrid').dataSource.filter({
            });
        };

        //code for clear button in search of kendo list
        $scope.PurchasegridSearchClear = function () {
            $('#searchBox').val('');
            $("#gridpurchaseList").data('kendoGrid').dataSource.filter({
            });
        }

        //code to search data in kendo -- need to change the field values
        $scope.PurchasegridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#gridpurchaseList").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  //{
                  //    field: "VendorId",
                  //    operator: "contains",
                  //    value: searchValue
                  //},
                  {
                      field: "ProductName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "VendorName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "ProductCategory",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "ProductSubCategory",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "Description",
                      operator: "contains",
                      value: searchValue
                  }
                ]
            });
        }

        //$scope.tooltipOptions = {
        //    filter: "td,th",
        //    position: "top",
        //    hide: function (e) {
        //        this.content.parent().css('visibility', 'hidden');
        //    },
        //    show: function (e) {
        //        if (this.content.text().length > 1) {
        //            if (this.content.text().trim() === 'Edit  QualifyAppointment Print') {
        //                this.content.parent().css('visibility', 'hidden');
        //            } else {
        //                this.content.parent().css('visibility', 'visible');
        //            }

        //        }
        //        else {
        //            this.content.parent().css('visibility', 'hidden');
        //        }
        //    },
        //    content: function (e) {
        //        return e.target.context.textContent;
        //    }
        //};

        $scope.MyprdStatusDDL = [{
            text: "Open", value: "Open"
        }, {
            text: "Printed", value: "Printed"
        }, {
            text: "Ordered", value: "Ordered "
        }, {
            text: "Shipped", value: "Shipped "
        }, {
            text: "Received", value: "Received "
        }, {
            text: "Canceled", value: "Canceled "
        }, {
            text: "Closed", value: "Closed"
        }];

        function StatusDropdown(container, options) {
//$('<input data-value-field="value" id="status" required style="width: 60%;background-color: #edecec !important;"  name="' + options.field + '" data-bind="value:' + options.field + '"/>')
            $('<input id="status" style="width: 60%;background-color: #edecec !important;"  name="Status" data-bind="value:Status"/>').appendTo(container)
                .kendoDropDownList({
                    valuePrimitive: true,
                    autoBind: true,
                    optionLabel: "Select",
                    dataTextField: "text",
                    dataValueField: "value",
                    template: "#=value #",
                    dataSource: [{
                        text: "Open", value: "Open"
                    }, {
                        text: "Printed", value: "Printed"
                    }, {
                        text: "Closed", value: "Closed"
                    }, {
                        text: "canceled", value: "canceled"
                    }]
                });
        }

        // Download purchase order
        $scope.DownloadPuchaseOrder_Excel = function () {
            var grid = $("#gridOrderSearch").data("kendoGrid");
            grid.saveAsExcel();
        }

        // Selected Cancel Reason Value for MPO
        $scope.OnCancelUpdateMPOStatus = function () {
            alert("This will cancel your order within Touchpoint. You must contact the vendor directly to cancel orders.");
            if ($scope.MPO.CancelReason != undefined) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "block";
                $http.post('/api/PurchaseOrders/0/OnCancelUpdateStatus', $scope.MPO).then(
                function (response) {
                    //$http.get('/api/PurchaseOrders/' + $scope.masterPO + '/CancelMPO').then(function(result) {});
                    var order = response.data.data;
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                    if (order) {
                            $scope.mpoDetails();
                            $scope.MPO = response.data.MPO;
                            if ($('#VendorGrid').data('kendoGrid')) {
                            $('#VendorGrid').data('kendoGrid').dataSource.read();
                            $('#VendorGrid').data('kendoGrid').refresh();
                            }
                            if ($('#VpoCoreProductGrid').data('kendoGrid')) {
                            $('#VpoCoreProductGrid').data('kendoGrid').dataSource.read();
                            $('#VpoCoreProductGrid').data('kendoGrid').refresh();
                            }
                            if ($('#VpoMyVendorGrid').data('kendoGrid')) {
                            $('#VpoMyVendorGrid').data('kendoGrid').dataSource.read();
                            $('#VpoMyVendorGrid').data('kendoGrid').refresh();
                        }


                        $scope.CancelReasonValue = $scope.MPO.CancelReason;
                    }
                });
            }
        }


        $scope.cancelReasonTapped = function () {
            $http.get('/api/Orders/'+ $scope.MPO.OrderId +'/Get').then(function(response)
            {
                var ordervalue = response.data;
                if (ordervalue) {
                    var reversePayment = ordervalue.OrderTotal - ordervalue.BalanceDue;
                    if (reversePayment > 0) {
                        //hfcConfirm("A Payment in the amount of " +
                        //    $filter('currency')(reversePayment) +
                        //    " has been applied to this sales Order. Please reverse this transaction.");
                        $scope.Amount = $filter('currency')(reversePayment);
                        $scope.popup("calcelReversePayment");
                        $scope.OrderService.setOrderStatusChangerMethod($scope.OnCancelUpdateMPOStatus, $scope.MPO, true, true);
                    }
                    else {
                        $scope.OrderService.setOrderStatusChangerMethod(null, null, false, false);
                        $scope.OnCancelUpdateMPOStatus();
                    }
                }
            });
        }


        // Selected Cancel Reason Value for VPO
        $scope.OnCancelUpdateVPOStatus = function () {
            $scope.VPOCancelReason = false;
            alert("Call The Appropriate function from here");
        }

        //Warning for the Cancelled order and reverse payment

        function hfcConfirm(message, id) {
            bootbox.dialog({
                closeButton: false,
                message: message,
                title: "Warning",
                buttons: {
                    ok: {
                        label: "ok",
                        className: "btn-warning",
                        callback: function (params) {
                            $scope.ReverseAllPayment($scope.MPO.OrderId);
                        }
                    },
                }
            });
        }

        $scope.ReverseAllPayment = function (OrderId) {
            $("#calcelReversePayment").modal("hide");
            location.href = '/#!/ReverseOrderPayment/' + OrderId;
        }

        $scope.CancelMasterPO = function () {
            //angular.forEach($scope.MPO.MPOHeaderTable, function (vendorName) {
            //    var vendorCoreProduct = $.grep($scope.MPO.CoreProductVPOs, function (s) {
            //        return s.VendorName == vendorName.VendorName;
            //    });

            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";

            $http.get('/api/PurchaseOrders/' + $scope.masterPO + '/CancelMPO').then(function (response) {
                if (response.data.error === "") {
                    $scope.MPO = response.data.data;
                    $scope.MPO = response.data.data;

                    $('#VendorGrid').data('kendoGrid').dataSource.read();
                    $('#VendorGrid').data('kendoGrid').refresh();

                    $('#VpoCoreProductGrid').data('kendoGrid').dataSource.read();
                    $('#VpoCoreProductGrid').data('kendoGrid').refresh();

                    $('#VpoMyVendorGrid').data('kendoGrid').dataSource.read();
                    $('#VpoMyVendorGrid').data('kendoGrid').refresh();
                } else {
                    HFC.DisplayAlert(response.data.error);
                }

                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
            $scope.CancelReasonList = [
               { ID: 1, Name: 'Customer Cancellation' },
               { ID: 2, Name: 'Entered  Incorrectly' },
               { ID: 3, Name: 'Incorrect Measurement / Product Configuration' }

            ]
        };

        $scope.gotoAddCaseEditWithMPO = function () {
            location.href = '/#!/createCaseMPO/' + $scope.masterPO + '/0';
        }
        $scope.gotoAddCaseEditWithVPO = function (LineNumber, OrderId) {
            location.href = '/#!/createCaseVPO/' + $scope.masterPO + '/' + LineNumber + '/' + OrderId;
        }
    }])