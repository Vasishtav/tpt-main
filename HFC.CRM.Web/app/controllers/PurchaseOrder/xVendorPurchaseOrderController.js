﻿app.controller('xVendorPurchaseOrderController', ['$http', '$scope', '$routeParams', 'NavbarService', 'HFCService', 'AddressService', 'AddressValidationService',
    function ($http, $scope, $routeParams, NavbarService, HFCService, AddressService, AddressValidationService) {

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableOperationsProcurementDashboardTab();
        $scope.FranchiseName = HFCService.FranchiseName;
        $scope.BrandId = HFCService.CurrentBrand;
        $scope.xMPO = [];
        $scope.tempNotesVendor = '';
        $scope.AddressService = AddressService;

        $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
        // $scope.CanCreateCase = false;

        $scope.Permission = {};
        var PurchaseOrderpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'PurchaseOrder')

        $scope.Permission.ListPurchase = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Master Purchase Order List').CanAccess
        $scope.Permission.SubmitOrder = PurchaseOrderpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Submit Master Purchase Order List').CanAccess
        $scope.Permission.ViewVendorPurchaseOrder = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Vendor Purchase Order').CanAccess
        $scope.Permission.ViewPurchase = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Master Purchase Order').CanAccess


        $scope.CancelReasonList = [
            { ID: 1, Name: 'Customer Cancellation' },
            { ID: 2, Name: 'Entered  Incorrectly' },
            { ID: 3, Name: 'Incorrect Measurement / Product Configuration' }
        ];

        HFCService.setHeaderTitle("Purchase Order View #" + " " + $routeParams.PurchaseOrderId);

        $scope.ViewProcurementDetails = function () {
            $scope.Id = $routeParams.PurchaseOrderId;
            window.location.href = "#!/xSalesOrderView/" + $scope.Id;
        }

        $scope.xVendorPurchaseGridOptions = {
            dataSource: {
                data: $scope.xMPO.LineItems,
                schema: {
                    model: {
                        fields: {
                            LineNumber: { type: "number" },
                            WindowName: { type: "string" },
                            ProductName: { type: "string" },
                            ProductNumber: { type: "number" },
                            Description: { type: "string" },
                            Quantity: { type: "number" },
                            Cost: { type: "number" },
                            PromiseByDate: { type: "date" },
                            EstimatedShipDate: { type: "date" },
                            Status: { type: "string" },
                        }
                    }
                },
                error: function (e) {
                    HFC.DisplayAlert(e.errorThrown);
                },
                pageSize: 25
            },
            dataBound: function (e) {
                //if (this.dataSource.view().length == 0) {
                //    $('.k-pager-nav').hide();
                //    $('.k-pager-numbers').hide();
                //    $('.k-pager-sizes').hide();
                //} else {
                //    $('.k-pager-nav').show();
                //    $('.k-pager-numbers').show();
                //    $('.k-pager-sizes').show();
                //}
                //if (kendotooltipService.columnWidth) {
                //    kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                //} else if (window.innerWidth < 1280) {
                //    kendotooltipService.restrictTooltip(null);
                //}
                //$scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
            },
            columns: [
                {

                    field: "LineNumber",
                    title: "Line Num",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                },
                {

                    field: "WindowName",
                    title: "Window Name",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "ProductName",
                    title: "Product",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "ProductNumber",
                    title: "Product Number",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Description",
                    title: "Description",
                    width: "650px",
                    filterable: { multi: true, search: true },
                    attributes: {

                        //style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                    },
                    template: function (dataitem) {
                        return ProductDescription(dataitem);
                    },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Quantity",
                    title: "Qty",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Cost",
                    title: "Cost",
                    width: "120px",
                    format: "{0:c}",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                },
                {

                    field: "OrderNumber",
                    title: "Order Id",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "PromiseByDate",
                    title: "Promise By Date",
                    width: "120px",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    //  template: "#= kendo.toString(kendo.parseDate(PromiseByDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                    template: function (dataItem) {
                        if (dataItem.PromiseByDate != null)
                            return HFCService.KendoDateFormat(dataItem.PromiseByDate);
                        else
                            return '';
                    }

                }, {

                    field: "EstimatedShipDate",
                    title: "Est Ship Date",
                    width: "120px",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    // template: "#= kendo.toString(kendo.parseDate(EstimatedShipDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                    template: function (dataItem) {
                        if (dataItem.PromiseByDate != null)
                            return HFCService.KendoDateFormat(dataItem.EstimatedShipDate);
                        else
                            return '';
                    }

                }, {

                    field: "Status",
                    title: "Status",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    template: function (dataItem) {
                        // if (dataItem.Status !== 'Canceled')
                        return dataItem.Status;
                        // else
                        // return "Canceled"
                        // return $scope.temp_CaseNo(dataItem);
                    }

                },

                {
                    field: "",
                    title: "",
                    width: "100px",
                    template: function (dataItem) {
                        if (dataItem.Status !== 'Canceled') {
                            var can = 'Canceled';
                            var pro = 'Processing';
                            var err = 'Error';
                            var template = '<ul><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="" ng-click="addEditNotestoVendor(dataItem)" >Note to Vendor</a></li>  <li><a ng-show="FranciseLevelCase && MPOCancelled  && dataItem.Status != can && dataItem.Status != pro && dataItem.Status != err" href="javascript:void(0)" ng-click="gotoAddCaseEditWithVPO(dataItem)">Create Case</a></li>';
                            if (dataItem.Status == 'Error') {
                                template += '<li><a href=""  ng-click="ViewVPOError(dataItem)" >View Error</a></li>';
                            }
                            template += '</ul></li></ul>';

                            return template;
                        }
                        else
                            return '';
                    }
                }



            ],
            noRecords: { template: "No records found" },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            sortable: true,
            resizable: true,
            pageable: true,
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                resizable: true
            },

            columnResize: function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
            }
        };

        function ProductDescription(dataitem) {
            var template = "";
            // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
            var erer = dataitem.Description.replace('"', "");
            erer = erer.replace(/<[^>]*>/g, '');
            template += '<span>' + dataitem.Description + '</span>'

            return template;
        }


        if ($routeParams.PurchaseOrderId > 0) {
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/GetXMpo')
                .then(function (response) {
                    $scope.xMPO = response.data.data;
                    //if ($scope.xMPO.Status === "Canceled")
                    //    $scope.xMPO.Status = "Cancelled";
                    if ($scope.xMPO.LineItems.length > 0) {
                        var grid = $('#xVPOList').data("kendoGrid");
                        grid.dataSource.data($scope.xMPO.LineItems);
                    }


                    var panelBar = $("#panelul").data("kendoPanelBar");
                    panelBar.expand($("#panelli"));
                });
        }

        $scope.CancelNotetoVendor = function () {
            $("#notetoVendormodal").modal("hide");
            $scope.vpoDetails = {};
            $scope.tempNotesVendor = '';
        }


        $scope.SavenoteToVendor = function (data) {
            data.NotesVendor = $scope.tempNotesVendor;
            $http.post('/api/PurchaseOrders/0/SaveNotestoVendor', data).then(function (response) {
                $scope.vpoDetails = {};
            });
            $("#notetoVendormodal").modal("hide");
        }


        $scope.addEditNotestoVendor = function (dataItem) {
            $scope.tempNotesVendor = dataItem.NotesVendor;
            $scope.vpoDetails = dataItem;
            setTimeout(function () {
                $("#notetoVendormodal").modal("show");
            }, 100);

        };

        $scope.ViewVPOError = function (dataItem) {
            $scope.vpoDetails = dataItem;
            $scope.QuoteKey = $scope.vpoDetails.QuoteKey;
            $scope.OpportunityId = $scope.vpoDetails.OpportunityId;
            $scope.PurchaseOrderId = $scope.vpoDetails.PurchaseOrderId;
            $("#Errormodal").modal("show");
        }

        $scope.CancelError = function (dataItem) {
            $scope.vpoDetails = {};
            $("#Errormodal").modal("hide");
        }

        $scope.EditConfiguration = function () {
            // console.log($scope.xMPO);
            window.location.href =
                '/#!/xVPOCoreProductconfigurator/' + $scope.OpportunityId + '/' + $scope.QuoteKey + '/' + $scope.PurchaseOrderId;
        }

        $scope.cancelReasonTapped = function () {
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/CancelXMpo?cancelReason=' + $scope.xMPO.CancelReasonText).then(function (response) {

                $scope.xMPO = response.data.data;

                //if ($scope.xMPO.Status === "Canceled")
                //    $scope.xMPO.Status = "Cancelled";

                //  console.log($scope.xMPO.LineItems);
                if ($scope.xMPO.LineItems.length > 0) {
                    //  var dataSource = new kendo.data.DataSource($scope.xMPO.LineItems);
                    var grid = $('#xVPOList').data("kendoGrid");
                    grid.dataSource.data($scope.xMPO.LineItems);
                }

                var panelBar = $("#panelul").data("kendoPanelBar");
                // expand the element with ID, "item1"
                panelBar.expand($("#panelli"));

                //var ordervalue = response.data;
                //if (ordervalue) {
                //    var reversePayment = ordervalue.OrderTotal - ordervalue.BalanceDue;
                //    if (reversePayment > 0) {
                //        hfcConfirm("A Payment in the amount of " +
                //            $filter('currency')(reversePayment) +
                //            " has been applied to this sales Order. Please reverse this transaction.");
                //        $scope.OrderService.setOrderStatusChangerMethod($scope.OnCancelUpdateMPOStatus, $scope.MPO, true, true);
                //    }
                //    else {
                //        $scope.OrderService.setOrderStatusChangerMethod(null, null, false, false);
                //        $scope.OnCancelUpdateMPOStatus();
                //    }
                //}
            });
        }
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 326);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 326);
            };
        });
        // End Kendo Resizing
        //click on address open pop-up
        $scope.MultipleShippingAddress = function () {
            if (!$scope.xMPO.DropShip) {
                $("#drpshipdiv").hide();

            }
            $scope.ShiplocationChange();

            $scope.popup("MultipleShippingAddress");
            if ($scope.xMPO.DropShip && $scope.xMPO.DropShipAddress != null) {
                $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.xMPO.DropShipAddress.Address1);
            }
        }

        //change the drop location in drop down.
        $scope.ShiplocationChange = function () {
            for (var i = 0; i < $scope.xMPO.ShipToLocationList.length; i++) {
                if ($scope.xMPO.ShipToLocationList[i].Id === $scope.xMPO.ShipToLocation) {
                    $scope.xMPO.Address = $scope.xMPO.ShipToLocationList[i].Address;
                    $scope.xMPO.IsValidated = $scope.xMPO.ShipToLocationList[i].IsValidated;
                    $scope.xMPO.ShipAddressId = Number($scope.xMPO.ShipToLocationList[i].AddressId);
                    //$scope.$apply();
                }
            }
        }

        $scope.popup = function (modalId) {
            $("#" + modalId).modal("show");
        }

        //on click the radio button drop ship loc
        $scope.dropShipSelected = function () {
            var xMPO = $scope.xMPO;
            if (!$scope.xMPO.DropShip) {
                $("#drpshipAddress :input").attr("disabled", true);
                $("#countryBox").attr("disabled", true);
                $("#ddlShiptoLocation").attr("disabled", false);
                $("#drpshipdiv").hide();
            } else {
                $("#drpshipdiv").show();
                $scope.xMPO.ShipAddressId = null;
                $scope.xMPO.ShipToLocation = null;
                $("#drpshipAddress :input").attr("disabled", false);
                $("#countryBox").attr("disabled", false);
                $("#ddlShiptoLocation").attr("disabled", true);
                $scope.Address = {};
                $scope.Address.CountryCode2Digits = HFCService.FranchiseCountryCode;

            }
        };

        //Cancel/close the Pop-up
        $scope.CancelDropPopup = function () {
            var myDiv = $('.shipaddres_popup');
            myDiv[0].scrollTop = 0;
            //$scope.xMPO.Address = $scope.FullAddress;
            //$scope.xMPO.ShipAddressId = $scope.ShipAddressId;
            //$scope.xMPO.ShipToLocation = $scope.shiplocation;
            $("#MultipleShippingAddress").modal("hide");
        }

        //to get country for drop down
        $http({ method: 'GET', url: '/api/address/', params: { leadIds: 0, includeStates: true } })
            .then(function (response) {
                //$scope.AddressService.States = response.data.States || [];
                $scope.AddressService.Countries = response.data.Countries || [];
                //$scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
            }, function (response) {
            });

        //Update the address -- Save
        $scope.UpdatAndConfirmAddress = function (xMPO, addr) {
            var myDiv = $('.shipaddres_popup');
            myDiv[0].scrollTop = 0;

            $scope.xMPO.DropShipAddress = addr;

            if ($scope.xMPO.DropShip && $scope.xMPO.DropShipAddress != null) {
                saveAddressHandler($scope.xMPO);
            }
            else
                AfterValidateAddress($scope.xMPO);

        }

        function saveAddressHandler(dto) {
            if (dto.DropShip) {
                validateAddress(dto.DropShipAddress)
                    .then(function (response) {
                        var objaddress = response.data;

                        if (objaddress.Status == "error") {

                            AfterValidateAddress(dto);

                        } else if (objaddress.Status == "true") {
                            var validAddress = objaddress.AddressResults[0];
                            dto.DropShipAddress.Address1 = validAddress.address1;
                            $scope.Addressvalues = dto.DropShipAddress.Address1;
                            dto.DropShipAddress.Address2 = validAddress.address2;
                            dto.DropShipAddress.City = validAddress.City;
                            dto.DropShipAddress.State = validAddress.StateOrProvinceCode;

                            // Update that the address validated to true.
                            dto.DropShipAddress.IsValidated = true;

                            // assign the zipcode back to the model before saving.
                            if (dto.DropShipAddress.CountryCode2Digits == "US") {
                                dto.DropShipAddress.ZipCode = validAddress.PostalCode.substring(0, 5);
                            }
                            else {
                                dto.DropShipAddress.ZipCode = validAddress.PostalCode;
                            }

                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                            AfterValidateAddress(dto);
                        } else if (objaddress.Status == "false") {
                            dto.DropShipAddress.IsValidated = false;

                            var errorMessage = "Invalid Address. Do you wish to continue?";

                            $scope.Addressvalues = dto.DropShipAddress.Address1;
                            //address get empty so assiging the value here.

                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);

                            if (confirm(errorMessage)) {
                                AfterValidateAddress(dto);
                            }
                            else {
                                $scope.IsBusy = false;
                            }
                        } else { //SkipValidation
                            AfterValidateAddress(dto);
                        }
                    },
                        function (response) {
                            HFC.DisplayAlert(response.statusText);
                            $scope.IsBusy = false;
                        });
            }
        };


        function AfterValidateAddress(obj) {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $scope.xMPO = obj;

            //code for spacing in Canada Postal/ZipCode
            if ($scope.xMPO.DropShipAddress) {
                if (!$scope.xMPO.DropShipAddress.IsValidated) {
                    if ($scope.xMPO.DropShipAddress.CountryCode2Digits == "CA") {
                        if ($scope.xMPO.DropShipAddress.ZipCode) {
                            var Ca_Zipcode = $scope.xMPO.DropShipAddress.ZipCode;
                            Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                            $scope.xMPO.DropShipAddress.ZipCode = Ca_Zipcode;
                        }
                    }
                }
            }


            $http.post('/api/PurchaseOrders/0/AddUpdateXmpoShipAddress', $scope.xMPO).then(function (response) {
                if (response.data.error === "") {

                    $scope.xMPO = response.data.data;
                    //$scope.OrderId = response.data.data.OrderId;

                    $scope.Address = response.data.data.DropShipAddress;

                    if (!$scope.xMPO.DropShip) {
                        $("#drpshipAddress :input").attr("disabled", true);
                        $("#countryBox").attr("disabled", true);
                    } else {
                        $("#drpshipAddress :input").attr("disabled", false);
                        $("#countryBox").attr("disabled", false);
                    }
                    $("#MultipleShippingAddress").modal("hide");
                }
                else {
                    HFC.DisplayAlert(response.data.error);
                }
                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }

        function validateAddress(obj) {
            var addressModeltp = {
                address1: obj.Address1,
                address2: obj.Address2,
                City: obj.City,
                StateOrProvinceCode: obj.State,
                PostalCode: obj.ZipCode,
                CountryCode: obj.CountryCode2Digits
            };

            if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                var result = {
                    data: {
                        Status: "SkipValidation",
                        Message: "No Address validation required."
                    },
                    status: 200,
                    statusText: "OK"
                }

                return $q.resolve(result);;
            }

            // Call the api to Return the result.
            return AddressValidationService.validateAddress(addressModeltp);
        }

        // -------- END -------

        $scope.SubmitPurchaseOrder = function () {
            $scope.IsBusy = true;


            var loadingElement = document.getElementById("loading");

            ///Start-remove after Async
            var node = document.createElement("span");
            var textnode = document.createTextNode("Please wait, this may take a few moments...... ");
            node.style.fontSize = "30px";
            node.style.marginTop = "100px";
            node.appendChild(textnode);
            loadingElement.appendChild(node);
            //End remmove after Async

            loadingElement.style.display = "block";
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/SubmitXMpo').then(function (response) {

                if (response.data.error === "") {
                    $scope.xMPO = response.data.data;
                    // $scope.xMPO.SubmittedTOPIC = true;

                    if ($routeParams.PurchaseOrderId > 0) {
                        $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/GetXMpo')
                            .then(function (response) {
                                $scope.xMPO = response.data.data;
                                //if ($scope.xMPO.Status === "Canceled")
                                //    $scope.xMPO.Status = "Cancelled";
                                if ($scope.xMPO.LineItems.length > 0) {
                                    var grid = $('#xVPOList').data("kendoGrid");
                                    grid.dataSource.data($scope.xMPO.LineItems);
                                }


                                var panelBar = $("#panelul").data("kendoPanelBar");
                                panelBar.expand($("#panelli"));

                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "none";
                                $scope.IsBusy = false;
                            }).catch(function (error) {
                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "none";
                                $scope.IsBusy = false;

                                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                            });
                    }

                } else {

                }
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
                $scope.IsBusy = false;
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.removeChild(node);
                loadingElement.style.display = "none";
                $scope.IsBusy = false;

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });


        }

        $scope.gotoAddCaseEditWithMPO = function () {
            location.href = '/#!/createCasexMPO/' + $routeParams.PurchaseOrderId + '/0';
        }
        $scope.gotoAddCaseEditWithVPO = function (dataItem) {
            location.href = '/#!/createCasexVPO/' + $routeParams.PurchaseOrderId + '/' + dataItem.LineNumber + '/' + dataItem.OrderId;
        }

        if ($routeParams.PurchaseOrderId > 0) {
            $http.get("/api/PurchaseOrders/" + $routeParams.PurchaseOrderId + "/CheckMPOCancelled").then(function (response) {
                $scope.MPOCancelled = response.data;
            });
        }


    }
]); app.controller('xVendorPurchaseOrderController', ['$http', '$scope', '$routeParams', 'NavbarService', 'HFCService', 'AddressService', 'AddressValidationService',
    function ($http, $scope, $routeParams, NavbarService, HFCService, AddressService, AddressValidationService) {

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableOperationsProcurementDashboardTab();
        $scope.FranchiseName = HFCService.FranchiseName;
        $scope.BrandId = HFCService.CurrentBrand;
        $scope.xMPO = [];
        $scope.tempNotesVendor = '';
        $scope.AddressService = AddressService;

        $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
        // $scope.CanCreateCase = false;

        $scope.Permission = {};
        var PurchaseOrderpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'PurchaseOrder')

        $scope.Permission.ListPurchase = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Master Purchase Order List').CanAccess
        $scope.Permission.SubmitOrder = PurchaseOrderpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Submit Master Purchase Order List').CanAccess
        $scope.Permission.ViewVendorPurchaseOrder = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Vendor Purchase Order').CanAccess
        $scope.Permission.ViewPurchase = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Master Purchase Order').CanAccess


        $scope.CancelReasonList = [
            { ID: 1, Name: 'Customer Cancellation' },
            { ID: 2, Name: 'Entered  Incorrectly' },
            { ID: 3, Name: 'Incorrect Measurement / Product Configuration' }
        ];

        HFCService.setHeaderTitle("Purchase Order View #" + " " + $routeParams.PurchaseOrderId);

        $scope.ViewProcurementDetails = function () {
            $scope.Id = $routeParams.PurchaseOrderId;
            window.location.href = "#!/xSalesOrderView/" + $scope.Id;
        }

        $scope.xVendorPurchaseGridOptions = {
            dataSource: {
                data: $scope.xMPO.LineItems,
                schema: {
                    model: {
                        fields: {
                            LineNumber: { type: "number" },
                            WindowName: { type: "string" },
                            ProductName: { type: "string" },
                            ProductNumber: { type: "number" },
                            Description: { type: "string" },
                            Quantity: { type: "number" },
                            Cost: { type: "number" },
                            PromiseByDate: { type: "date" },
                            EstimatedShipDate: { type: "date" },
                            Status: { type: "string" },
                        }
                    }
                },
                error: function (e) {
                    HFC.DisplayAlert(e.errorThrown);
                },
                pageSize: 25
            },
            dataBound: function (e) {
                //if (this.dataSource.view().length == 0) {
                //    $('.k-pager-nav').hide();
                //    $('.k-pager-numbers').hide();
                //    $('.k-pager-sizes').hide();
                //} else {
                //    $('.k-pager-nav').show();
                //    $('.k-pager-numbers').show();
                //    $('.k-pager-sizes').show();
                //}
                //if (kendotooltipService.columnWidth) {
                //    kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                //} else if (window.innerWidth < 1280) {
                //    kendotooltipService.restrictTooltip(null);
                //}
                //$scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
            },
            columns: [
                {

                    field: "LineNumber",
                    title: "Line Num",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                },
                {

                    field: "WindowName",
                    title: "Window Name",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "ProductName",
                    title: "Product",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "ProductNumber",
                    title: "Product Number",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Description",
                    title: "Description",
                    width: "650px",
                    filterable: { multi: true, search: true },
                    attributes: {

                        //style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                    },
                    template: function (dataitem) {
                        return ProductDescription(dataitem);
                    },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Quantity",
                    title: "Qty",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Cost",
                    title: "Cost",
                    width: "120px",
                    format: "{0:c}",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                },
                {

                    field: "OrderNumber",
                    title: "Order Id",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "PromiseByDate",
                    title: "Promise By Date",
                    width: "120px",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    //  template: "#= kendo.toString(kendo.parseDate(PromiseByDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                    template: function (dataItem) {
                        if (dataItem.PromiseByDate != null)
                            return HFCService.KendoDateFormat(dataItem.PromiseByDate);
                        else
                            return '';
                    }

                }, {

                    field: "EstimatedShipDate",
                    title: "Est Ship Date",
                    width: "120px",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    // template: "#= kendo.toString(kendo.parseDate(EstimatedShipDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                    template: function (dataItem) {
                        if (dataItem.PromiseByDate != null)
                            return HFCService.KendoDateFormat(dataItem.EstimatedShipDate);
                        else
                            return '';
                    }

                }, {

                    field: "Status",
                    title: "Status",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    template: function (dataItem) {
                        // if (dataItem.Status !== 'Canceled')
                        return dataItem.Status;
                        // else
                        // return "Canceled"
                        // return $scope.temp_CaseNo(dataItem);
                    }

                },

                {
                    field: "",
                    title: "",
                    width: "100px",
                    template: function (dataItem) {
                        if (dataItem.Status !== 'Canceled') {
                            var can = 'Canceled';
                            var pro = 'Processing';
                            var err = 'Error';
                            var template = '<ul><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="" ng-click="addEditNotestoVendor(dataItem)" >Note to Vendor</a></li>  <li><a ng-show="FranciseLevelCase && MPOCancelled  && dataItem.Status != can && dataItem.Status != pro && dataItem.Status != err" href="javascript:void(0)" ng-click="gotoAddCaseEditWithVPO(dataItem)">Create Case</a></li>';
                            if (dataItem.Status == 'Error') {
                                template += '<li><a href=""  ng-click="ViewVPOError(dataItem)" >View Error</a></li>';
                            }
                            template += '</ul></li></ul>';

                            return template;
                        }
                        else
                            return '';
                    }
                }



            ],
            noRecords: { template: "No records found" },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            sortable: true,
            resizable: true,
            pageable: true,
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                resizable: true
            },

            columnResize: function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
            }
        };

        function ProductDescription(dataitem) {
            var template = "";
            // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
            var erer = dataitem.Description.replace('"', "");
            erer = erer.replace(/<[^>]*>/g, '');
            template += '<span>' + dataitem.Description + '</span>'

            return template;
        }


        if ($routeParams.PurchaseOrderId > 0) {
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/GetXMpo')
                .then(function (response) {
                    $scope.xMPO = response.data.data;
                    //if ($scope.xMPO.Status === "Canceled")
                    //    $scope.xMPO.Status = "Cancelled";
                    if ($scope.xMPO.LineItems.length > 0) {
                        var grid = $('#xVPOList').data("kendoGrid");
                        grid.dataSource.data($scope.xMPO.LineItems);
                    }


                    var panelBar = $("#panelul").data("kendoPanelBar");
                    panelBar.expand($("#panelli"));
                });
        }

        $scope.CancelNotetoVendor = function () {
            $("#notetoVendormodal").modal("hide");
            $scope.vpoDetails = {};
            $scope.tempNotesVendor = '';
        }


        $scope.SavenoteToVendor = function (data) {
            data.NotesVendor = $scope.tempNotesVendor;
            $http.post('/api/PurchaseOrders/0/SaveNotestoVendor', data).then(function (response) {
                $scope.vpoDetails = {};
            });
            $("#notetoVendormodal").modal("hide");
        }


        $scope.addEditNotestoVendor = function (dataItem) {
            $scope.tempNotesVendor = dataItem.NotesVendor;
            $scope.vpoDetails = dataItem;
            setTimeout(function () {
                $("#notetoVendormodal").modal("show");
            }, 100);

        };

        $scope.ViewVPOError = function (dataItem) {
            $scope.vpoDetails = dataItem;
            $scope.QuoteKey = $scope.vpoDetails.QuoteKey;
            $scope.OpportunityId = $scope.vpoDetails.OpportunityId;
            $scope.PurchaseOrderId = $scope.vpoDetails.PurchaseOrderId;
            $("#Errormodal").modal("show");
        }

        $scope.CancelError = function (dataItem) {
            $scope.vpoDetails = {};
            $("#Errormodal").modal("hide");
        }

        $scope.EditConfiguration = function () {
            // console.log($scope.xMPO);
            window.location.href =
                '/#!/xVPOCoreProductconfigurator/' + $scope.OpportunityId + '/' + $scope.QuoteKey + '/' + $scope.PurchaseOrderId;
        }

        $scope.cancelReasonTapped = function () {
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/CancelXMpo?cancelReason=' + $scope.xMPO.CancelReasonText).then(function (response) {

                $scope.xMPO = response.data.data;

                //if ($scope.xMPO.Status === "Canceled")
                //    $scope.xMPO.Status = "Cancelled";

                //  console.log($scope.xMPO.LineItems);
                if ($scope.xMPO.LineItems.length > 0) {
                    //  var dataSource = new kendo.data.DataSource($scope.xMPO.LineItems);
                    var grid = $('#xVPOList').data("kendoGrid");
                    grid.dataSource.data($scope.xMPO.LineItems);
                }

                var panelBar = $("#panelul").data("kendoPanelBar");
                // expand the element with ID, "item1"
                panelBar.expand($("#panelli"));

                //var ordervalue = response.data;
                //if (ordervalue) {
                //    var reversePayment = ordervalue.OrderTotal - ordervalue.BalanceDue;
                //    if (reversePayment > 0) {
                //        hfcConfirm("A Payment in the amount of " +
                //            $filter('currency')(reversePayment) +
                //            " has been applied to this sales Order. Please reverse this transaction.");
                //        $scope.OrderService.setOrderStatusChangerMethod($scope.OnCancelUpdateMPOStatus, $scope.MPO, true, true);
                //    }
                //    else {
                //        $scope.OrderService.setOrderStatusChangerMethod(null, null, false, false);
                //        $scope.OnCancelUpdateMPOStatus();
                //    }
                //}
            });
        }
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 326);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 326);
            };
        });
        // End Kendo Resizing
        //click on address open pop-up
        $scope.MultipleShippingAddress = function () {
            if (!$scope.xMPO.DropShip) {
                $("#drpshipdiv").hide();

            }
            $scope.ShiplocationChange();

            $scope.popup("MultipleShippingAddress");
            if ($scope.xMPO.DropShip && $scope.xMPO.DropShipAddress != null) {
                $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.xMPO.DropShipAddress.Address1);
            }
        }

        //change the drop location in drop down.
        $scope.ShiplocationChange = function () {
            for (var i = 0; i < $scope.xMPO.ShipToLocationList.length; i++) {
                if ($scope.xMPO.ShipToLocationList[i].Id === $scope.xMPO.ShipToLocation) {
                    $scope.xMPO.Address = $scope.xMPO.ShipToLocationList[i].Address;
                    $scope.xMPO.IsValidated = $scope.xMPO.ShipToLocationList[i].IsValidated;
                    $scope.xMPO.ShipAddressId = Number($scope.xMPO.ShipToLocationList[i].AddressId);
                    //$scope.$apply();
                }
            }
        }

        $scope.popup = function (modalId) {
            $("#" + modalId).modal("show");
        }

        //on click the radio button drop ship loc
        $scope.dropShipSelected = function () {
            var xMPO = $scope.xMPO;
            if (!$scope.xMPO.DropShip) {
                $("#drpshipAddress :input").attr("disabled", true);
                $("#countryBox").attr("disabled", true);
                $("#ddlShiptoLocation").attr("disabled", false);
                $("#drpshipdiv").hide();
            } else {
                $("#drpshipdiv").show();
                $scope.xMPO.ShipAddressId = null;
                $scope.xMPO.ShipToLocation = null;
                $("#drpshipAddress :input").attr("disabled", false);
                $("#countryBox").attr("disabled", false);
                $("#ddlShiptoLocation").attr("disabled", true);
                $scope.Address = {};
                $scope.Address.CountryCode2Digits = HFCService.FranchiseCountryCode;

            }
        };

        //Cancel/close the Pop-up
        $scope.CancelDropPopup = function () {
            var myDiv = $('.shipaddres_popup');
            myDiv[0].scrollTop = 0;
            //$scope.xMPO.Address = $scope.FullAddress;
            //$scope.xMPO.ShipAddressId = $scope.ShipAddressId;
            //$scope.xMPO.ShipToLocation = $scope.shiplocation;
            $("#MultipleShippingAddress").modal("hide");
        }

        //to get country for drop down
        $http({ method: 'GET', url: '/api/address/', params: { leadIds: 0, includeStates: true } })
            .then(function (response) {
                //$scope.AddressService.States = response.data.States || [];
                $scope.AddressService.Countries = response.data.Countries || [];
                //$scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
            }, function (response) {
            });

        //Update the address -- Save
        $scope.UpdatAndConfirmAddress = function (xMPO, addr) {
            var myDiv = $('.shipaddres_popup');
            myDiv[0].scrollTop = 0;

            $scope.xMPO.DropShipAddress = addr;

            if ($scope.xMPO.DropShip && $scope.xMPO.DropShipAddress != null) {
                saveAddressHandler($scope.xMPO);
            }
            else
                AfterValidateAddress($scope.xMPO);

        }

        function saveAddressHandler(dto) {
            if (dto.DropShip) {
                validateAddress(dto.DropShipAddress)
                    .then(function (response) {
                        var objaddress = response.data;

                        if (objaddress.Status == "error") {

                            AfterValidateAddress(dto);

                        } else if (objaddress.Status == "true") {
                            var validAddress = objaddress.AddressResults[0];
                            dto.DropShipAddress.Address1 = validAddress.address1;
                            $scope.Addressvalues = dto.DropShipAddress.Address1;
                            dto.DropShipAddress.Address2 = validAddress.address2;
                            dto.DropShipAddress.City = validAddress.City;
                            dto.DropShipAddress.State = validAddress.StateOrProvinceCode;

                            // Update that the address validated to true.
                            dto.DropShipAddress.IsValidated = true;

                            // assign the zipcode back to the model before saving.
                            if (dto.DropShipAddress.CountryCode2Digits == "US") {
                                dto.DropShipAddress.ZipCode = validAddress.PostalCode.substring(0, 5);
                            }
                            else {
                                dto.DropShipAddress.ZipCode = validAddress.PostalCode;
                            }

                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                            AfterValidateAddress(dto);
                        } else if (objaddress.Status == "false") {
                            dto.DropShipAddress.IsValidated = false;

                            var errorMessage = "Invalid Address. Do you wish to continue?";

                            $scope.Addressvalues = dto.DropShipAddress.Address1;
                            //address get empty so assiging the value here.

                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);

                            if (confirm(errorMessage)) {
                                AfterValidateAddress(dto);
                            }
                            else {
                                $scope.IsBusy = false;
                            }
                        } else { //SkipValidation
                            AfterValidateAddress(dto);
                        }
                    },
                        function (response) {
                            HFC.DisplayAlert(response.statusText);
                            $scope.IsBusy = false;
                        });
            }
        };


        function AfterValidateAddress(obj) {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $scope.xMPO = obj;

            //code for spacing in Canada Postal/ZipCode
            if ($scope.xMPO.DropShipAddress) {
                if (!$scope.xMPO.DropShipAddress.IsValidated) {
                    if ($scope.xMPO.DropShipAddress.CountryCode2Digits == "CA") {
                        if ($scope.xMPO.DropShipAddress.ZipCode) {
                            var Ca_Zipcode = $scope.xMPO.DropShipAddress.ZipCode;
                            Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                            $scope.xMPO.DropShipAddress.ZipCode = Ca_Zipcode;
                        }
                    }
                }
            }


            $http.post('/api/PurchaseOrders/0/AddUpdateXmpoShipAddress', $scope.xMPO).then(function (response) {
                if (response.data.error === "") {

                    $scope.xMPO = response.data.data;
                    //$scope.OrderId = response.data.data.OrderId;

                    $scope.Address = response.data.data.DropShipAddress;

                    if (!$scope.xMPO.DropShip) {
                        $("#drpshipAddress :input").attr("disabled", true);
                        $("#countryBox").attr("disabled", true);
                    } else {
                        $("#drpshipAddress :input").attr("disabled", false);
                        $("#countryBox").attr("disabled", false);
                    }
                    $("#MultipleShippingAddress").modal("hide");
                }
                else {
                    HFC.DisplayAlert(response.data.error);
                }
                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }

        function validateAddress(obj) {
            var addressModeltp = {
                address1: obj.Address1,
                address2: obj.Address2,
                City: obj.City,
                StateOrProvinceCode: obj.State,
                PostalCode: obj.ZipCode,
                CountryCode: obj.CountryCode2Digits
            };

            if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                var result = {
                    data: {
                        Status: "SkipValidation",
                        Message: "No Address validation required."
                    },
                    status: 200,
                    statusText: "OK"
                }

                return $q.resolve(result);;
            }

            // Call the api to Return the result.
            return AddressValidationService.validateAddress(addressModeltp);
        }

        // -------- END -------

        $scope.SubmitPurchaseOrder = function () {
            $scope.IsBusy = true;


            var loadingElement = document.getElementById("loading");

            ///Start-remove after Async
            var node = document.createElement("span");
            var textnode = document.createTextNode("Please wait, this may take a few moments...... ");
            node.style.fontSize = "30px";
            node.style.marginTop = "100px";
            node.appendChild(textnode);
            loadingElement.appendChild(node);
            //End remmove after Async

            loadingElement.style.display = "block";
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/SubmitXMpo').then(function (response) {

                if (response.data.error === "") {
                    $scope.xMPO = response.data.data;
                    // $scope.xMPO.SubmittedTOPIC = true;

                    if ($routeParams.PurchaseOrderId > 0) {
                        $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/GetXMpo')
                            .then(function (response) {
                                $scope.xMPO = response.data.data;
                                //if ($scope.xMPO.Status === "Canceled")
                                //    $scope.xMPO.Status = "Cancelled";
                                if ($scope.xMPO.LineItems.length > 0) {
                                    var grid = $('#xVPOList').data("kendoGrid");
                                    grid.dataSource.data($scope.xMPO.LineItems);
                                }


                                var panelBar = $("#panelul").data("kendoPanelBar");
                                panelBar.expand($("#panelli"));

                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "none";
                                $scope.IsBusy = false;
                            }).catch(function (error) {
                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "none";
                                $scope.IsBusy = false;

                                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                            });
                    }

                } else {

                }
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
                $scope.IsBusy = false;
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.removeChild(node);
                loadingElement.style.display = "none";
                $scope.IsBusy = false;

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });


        }

        $scope.gotoAddCaseEditWithMPO = function () {
            location.href = '/#!/createCasexMPO/' + $routeParams.PurchaseOrderId + '/0';
        }
        $scope.gotoAddCaseEditWithVPO = function (dataItem) {
            location.href = '/#!/createCasexVPO/' + $routeParams.PurchaseOrderId + '/' + dataItem.LineNumber + '/' + dataItem.OrderId;
        }

        if ($routeParams.PurchaseOrderId > 0) {
            $http.get("/api/PurchaseOrders/" + $routeParams.PurchaseOrderId + "/CheckMPOCancelled").then(function (response) {
                $scope.MPOCancelled = response.data;
            });
        }


    }
]); app.controller('xVendorPurchaseOrderController', ['$http', '$scope', '$routeParams', 'NavbarService', 'HFCService', 'AddressService', 'AddressValidationService',
    function ($http, $scope, $routeParams, NavbarService, HFCService, AddressService, AddressValidationService) {

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableOperationsProcurementDashboardTab();
        $scope.FranchiseName = HFCService.FranchiseName;
        $scope.BrandId = HFCService.CurrentBrand;
        $scope.xMPO = [];
        $scope.tempNotesVendor = '';
        $scope.AddressService = AddressService;

        $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
        // $scope.CanCreateCase = false;

        $scope.Permission = {};
        var PurchaseOrderpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'PurchaseOrder')

        $scope.Permission.ListPurchase = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Master Purchase Order List').CanAccess
        $scope.Permission.SubmitOrder = PurchaseOrderpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Submit Master Purchase Order List').CanAccess
        $scope.Permission.ViewVendorPurchaseOrder = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Vendor Purchase Order').CanAccess
        $scope.Permission.ViewPurchase = PurchaseOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('View Master Purchase Order').CanAccess


        $scope.CancelReasonList = [
            { ID: 1, Name: 'Customer Cancellation' },
            { ID: 2, Name: 'Entered  Incorrectly' },
            { ID: 3, Name: 'Incorrect Measurement / Product Configuration' }
        ];

        HFCService.setHeaderTitle("Purchase Order View #" + " " + $routeParams.PurchaseOrderId);

        $scope.ViewProcurementDetails = function () {
            $scope.Id = $routeParams.PurchaseOrderId;
            window.location.href = "#!/xSalesOrderView/" + $scope.Id;
        }

        $scope.xVendorPurchaseGridOptions = {
            dataSource: {
                data: $scope.xMPO.LineItems,
                schema: {
                    model: {
                        fields: {
                            LineNumber: { type: "number" },
                            WindowName: { type: "string" },
                            ProductName: { type: "string" },
                            ProductNumber: { type: "number" },
                            Description: { type: "string" },
                            Quantity: { type: "number" },
                            Cost: { type: "number" },
                            PromiseByDate: { type: "date" },
                            EstimatedShipDate: { type: "date" },
                            Status: { type: "string" },
                        }
                    }
                },
                error: function (e) {
                    HFC.DisplayAlert(e.errorThrown);
                },
                pageSize: 25
            },
            dataBound: function (e) {
                //if (this.dataSource.view().length == 0) {
                //    $('.k-pager-nav').hide();
                //    $('.k-pager-numbers').hide();
                //    $('.k-pager-sizes').hide();
                //} else {
                //    $('.k-pager-nav').show();
                //    $('.k-pager-numbers').show();
                //    $('.k-pager-sizes').show();
                //}
                //if (kendotooltipService.columnWidth) {
                //    kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                //} else if (window.innerWidth < 1280) {
                //    kendotooltipService.restrictTooltip(null);
                //}
                //$scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
            },
            columns: [
                {

                    field: "LineNumber",
                    title: "Line Num",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                },
                {

                    field: "WindowName",
                    title: "Window Name",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "ProductName",
                    title: "Product",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "ProductNumber",
                    title: "Product Number",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Description",
                    title: "Description",
                    width: "650px",
                    filterable: { multi: true, search: true },
                    attributes: {

                        //style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                    },
                    template: function (dataitem) {
                        return ProductDescription(dataitem);
                    },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Quantity",
                    title: "Qty",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "Cost",
                    title: "Cost",
                    width: "120px",
                    format: "{0:c}",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                },
                {

                    field: "OrderNumber",
                    title: "Order Id",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    //template: function (dataItem) {
                    //   return $scope.temp_CaseNo(dataItem);
                    //}

                }, {

                    field: "PromiseByDate",
                    title: "Promise By Date",
                    width: "120px",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    //  template: "#= kendo.toString(kendo.parseDate(PromiseByDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                    template: function (dataItem) {
                        if (dataItem.PromiseByDate != null)
                            return HFCService.KendoDateFormat(dataItem.PromiseByDate);
                        else
                            return '';
                    }

                }, {

                    field: "EstimatedShipDate",
                    title: "Est Ship Date",
                    width: "120px",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    // template: "#= kendo.toString(kendo.parseDate(EstimatedShipDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                    template: function (dataItem) {
                        if (dataItem.PromiseByDate != null)
                            return HFCService.KendoDateFormat(dataItem.EstimatedShipDate);
                        else
                            return '';
                    }

                }, {

                    field: "Status",
                    title: "Status",
                    width: "120px",
                    filterable: { multi: true, search: true },
                    template: function (dataItem) {
                        // if (dataItem.Status !== 'Canceled')
                        return dataItem.Status;
                        // else
                        // return "Canceled"
                        // return $scope.temp_CaseNo(dataItem);
                    }

                },

                {
                    field: "",
                    title: "",
                    width: "100px",
                    template: function (dataItem) {
                        if (dataItem.Status !== 'Canceled') {
                            var can = 'Canceled';
                            var pro = 'Processing';
                            var err = 'Error';
                            var template = '<ul><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="" ng-click="addEditNotestoVendor(dataItem)" >Note to Vendor</a></li>  <li><a ng-show="FranciseLevelCase && MPOCancelled  && dataItem.Status != can && dataItem.Status != pro && dataItem.Status != err" href="javascript:void(0)" ng-click="gotoAddCaseEditWithVPO(dataItem)">Create Case</a></li>';
                            if (dataItem.Status == 'Error') {
                                template += '<li><a href=""  ng-click="ViewVPOError(dataItem)" >View Error</a></li>';
                            }
                            template += '</ul></li></ul>';

                            return template;
                        }
                        else
                            return '';
                    }
                }



            ],
            noRecords: { template: "No records found" },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            sortable: true,
            resizable: true,
            pageable: true,
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                resizable: true
            },

            columnResize: function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
            }
        };

        function ProductDescription(dataitem) {
            var template = "";
            // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
            var erer = dataitem.Description.replace('"', "");
            erer = erer.replace(/<[^>]*>/g, '');
            template += '<span>' + dataitem.Description + '</span>'

            return template;
        }


        if ($routeParams.PurchaseOrderId > 0) {
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/GetXMpo')
                .then(function (response) {
                    $scope.xMPO = response.data.data;
                    //if ($scope.xMPO.Status === "Canceled")
                    //    $scope.xMPO.Status = "Cancelled";
                    if ($scope.xMPO.LineItems.length > 0) {
                        var grid = $('#xVPOList').data("kendoGrid");
                        grid.dataSource.data($scope.xMPO.LineItems);
                    }


                    var panelBar = $("#panelul").data("kendoPanelBar");
                    panelBar.expand($("#panelli"));
                });
        }

        $scope.CancelNotetoVendor = function () {
            $("#notetoVendormodal").modal("hide");
            $scope.vpoDetails = {};
            $scope.tempNotesVendor = '';
        }


        $scope.SavenoteToVendor = function (data) {
            data.NotesVendor = $scope.tempNotesVendor;
            $http.post('/api/PurchaseOrders/0/SaveNotestoVendor', data).then(function (response) {
                $scope.vpoDetails = {};
            });
            $("#notetoVendormodal").modal("hide");
        }


        $scope.addEditNotestoVendor = function (dataItem) {
            $scope.tempNotesVendor = dataItem.NotesVendor;
            $scope.vpoDetails = dataItem;
            setTimeout(function () {
                $("#notetoVendormodal").modal("show");
            }, 100);

        };

        $scope.ViewVPOError = function (dataItem) {
            $scope.vpoDetails = dataItem;
            $scope.QuoteKey = $scope.vpoDetails.QuoteKey;
            $scope.OpportunityId = $scope.vpoDetails.OpportunityId;
            $scope.PurchaseOrderId = $scope.vpoDetails.PurchaseOrderId;
            $("#Errormodal").modal("show");
        }

        $scope.CancelError = function (dataItem) {
            $scope.vpoDetails = {};
            $("#Errormodal").modal("hide");
        }

        $scope.EditConfiguration = function () {
            // console.log($scope.xMPO);
            window.location.href =
                '/#!/xVPOCoreProductconfigurator/' + $scope.OpportunityId + '/' + $scope.QuoteKey + '/' + $scope.PurchaseOrderId;
        }

        $scope.cancelReasonTapped = function () {
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/CancelXMpo?cancelReason=' + $scope.xMPO.CancelReasonText).then(function (response) {

                $scope.xMPO = response.data.data;

                //if ($scope.xMPO.Status === "Canceled")
                //    $scope.xMPO.Status = "Cancelled";

                //  console.log($scope.xMPO.LineItems);
                if ($scope.xMPO.LineItems.length > 0) {
                    //  var dataSource = new kendo.data.DataSource($scope.xMPO.LineItems);
                    var grid = $('#xVPOList').data("kendoGrid");
                    grid.dataSource.data($scope.xMPO.LineItems);
                }

                var panelBar = $("#panelul").data("kendoPanelBar");
                // expand the element with ID, "item1"
                panelBar.expand($("#panelli"));

                //var ordervalue = response.data;
                //if (ordervalue) {
                //    var reversePayment = ordervalue.OrderTotal - ordervalue.BalanceDue;
                //    if (reversePayment > 0) {
                //        hfcConfirm("A Payment in the amount of " +
                //            $filter('currency')(reversePayment) +
                //            " has been applied to this sales Order. Please reverse this transaction.");
                //        $scope.OrderService.setOrderStatusChangerMethod($scope.OnCancelUpdateMPOStatus, $scope.MPO, true, true);
                //    }
                //    else {
                //        $scope.OrderService.setOrderStatusChangerMethod(null, null, false, false);
                //        $scope.OnCancelUpdateMPOStatus();
                //    }
                //}
            });
        }
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 326);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 326);
            };
        });
        // End Kendo Resizing
        //click on address open pop-up
        $scope.MultipleShippingAddress = function () {
            if (!$scope.xMPO.DropShip) {
                $("#drpshipdiv").hide();

            }
            $scope.ShiplocationChange();

            $scope.popup("MultipleShippingAddress");
            if ($scope.xMPO.DropShip && $scope.xMPO.DropShipAddress != null) {
                $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.xMPO.DropShipAddress.Address1);
            }
        }

        //change the drop location in drop down.
        $scope.ShiplocationChange = function () {
            for (var i = 0; i < $scope.xMPO.ShipToLocationList.length; i++) {
                if ($scope.xMPO.ShipToLocationList[i].Id === $scope.xMPO.ShipToLocation) {
                    $scope.xMPO.Address = $scope.xMPO.ShipToLocationList[i].Address;
                    $scope.xMPO.IsValidated = $scope.xMPO.ShipToLocationList[i].IsValidated;
                    $scope.xMPO.ShipAddressId = Number($scope.xMPO.ShipToLocationList[i].AddressId);
                    //$scope.$apply();
                }
            }
        }

        $scope.popup = function (modalId) {
            $("#" + modalId).modal("show");
        }

        //on click the radio button drop ship loc
        $scope.dropShipSelected = function () {
            var xMPO = $scope.xMPO;
            if (!$scope.xMPO.DropShip) {
                $("#drpshipAddress :input").attr("disabled", true);
                $("#countryBox").attr("disabled", true);
                $("#ddlShiptoLocation").attr("disabled", false);
                $("#drpshipdiv").hide();
            } else {
                $("#drpshipdiv").show();
                $scope.xMPO.ShipAddressId = null;
                $scope.xMPO.ShipToLocation = null;
                $("#drpshipAddress :input").attr("disabled", false);
                $("#countryBox").attr("disabled", false);
                $("#ddlShiptoLocation").attr("disabled", true);
                $scope.Address = {};
                $scope.Address.CountryCode2Digits = HFCService.FranchiseCountryCode;

            }
        };

        //Cancel/close the Pop-up
        $scope.CancelDropPopup = function () {
            var myDiv = $('.shipaddres_popup');
            myDiv[0].scrollTop = 0;
            //$scope.xMPO.Address = $scope.FullAddress;
            //$scope.xMPO.ShipAddressId = $scope.ShipAddressId;
            //$scope.xMPO.ShipToLocation = $scope.shiplocation;
            $("#MultipleShippingAddress").modal("hide");
        }

        //to get country for drop down
        $http({ method: 'GET', url: '/api/address/', params: { leadIds: 0, includeStates: true } })
            .then(function (response) {
                //$scope.AddressService.States = response.data.States || [];
                $scope.AddressService.Countries = response.data.Countries || [];
                //$scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
            }, function (response) {
            });

        //Update the address -- Save
        $scope.UpdatAndConfirmAddress = function (xMPO, addr) {
            var myDiv = $('.shipaddres_popup');
            myDiv[0].scrollTop = 0;

            $scope.xMPO.DropShipAddress = addr;

            if ($scope.xMPO.DropShip && $scope.xMPO.DropShipAddress != null) {
                saveAddressHandler($scope.xMPO);
            }
            else
                AfterValidateAddress($scope.xMPO);

        }

        function saveAddressHandler(dto) {
            if (dto.DropShip) {
                validateAddress(dto.DropShipAddress)
                    .then(function (response) {
                        var objaddress = response.data;

                        if (objaddress.Status == "error") {

                            AfterValidateAddress(dto);

                        } else if (objaddress.Status == "true") {
                            var validAddress = objaddress.AddressResults[0];
                            dto.DropShipAddress.Address1 = validAddress.address1;
                            $scope.Addressvalues = dto.DropShipAddress.Address1;
                            dto.DropShipAddress.Address2 = validAddress.address2;
                            dto.DropShipAddress.City = validAddress.City;
                            dto.DropShipAddress.State = validAddress.StateOrProvinceCode;

                            // Update that the address validated to true.
                            dto.DropShipAddress.IsValidated = true;

                            // assign the zipcode back to the model before saving.
                            if (dto.DropShipAddress.CountryCode2Digits == "US") {
                                dto.DropShipAddress.ZipCode = validAddress.PostalCode.substring(0, 5);
                            }
                            else {
                                dto.DropShipAddress.ZipCode = validAddress.PostalCode;
                            }

                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                            AfterValidateAddress(dto);
                        } else if (objaddress.Status == "false") {
                            dto.DropShipAddress.IsValidated = false;

                            var errorMessage = "Invalid Address. Do you wish to continue?";

                            $scope.Addressvalues = dto.DropShipAddress.Address1;
                            //address get empty so assiging the value here.

                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);

                            if (confirm(errorMessage)) {
                                AfterValidateAddress(dto);
                            }
                            else {
                                $scope.IsBusy = false;
                            }
                        } else { //SkipValidation
                            AfterValidateAddress(dto);
                        }
                    },
                        function (response) {
                            HFC.DisplayAlert(response.statusText);
                            $scope.IsBusy = false;
                        });
            }
        };


        function AfterValidateAddress(obj) {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $scope.xMPO = obj;

            //code for spacing in Canada Postal/ZipCode
            if ($scope.xMPO.DropShipAddress) {
                if (!$scope.xMPO.DropShipAddress.IsValidated) {
                    if ($scope.xMPO.DropShipAddress.CountryCode2Digits == "CA") {
                        if ($scope.xMPO.DropShipAddress.ZipCode) {
                            var Ca_Zipcode = $scope.xMPO.DropShipAddress.ZipCode;
                            Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                            $scope.xMPO.DropShipAddress.ZipCode = Ca_Zipcode;
                        }
                    }
                }
            }


            $http.post('/api/PurchaseOrders/0/AddUpdateXmpoShipAddress', $scope.xMPO).then(function (response) {
                if (response.data.error === "") {

                    $scope.xMPO = response.data.data;
                    //$scope.OrderId = response.data.data.OrderId;

                    $scope.Address = response.data.data.DropShipAddress;

                    if (!$scope.xMPO.DropShip) {
                        $("#drpshipAddress :input").attr("disabled", true);
                        $("#countryBox").attr("disabled", true);
                    } else {
                        $("#drpshipAddress :input").attr("disabled", false);
                        $("#countryBox").attr("disabled", false);
                    }
                    $("#MultipleShippingAddress").modal("hide");
                }
                else {
                    HFC.DisplayAlert(response.data.error);
                }
                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }

        function validateAddress(obj) {
            var addressModeltp = {
                address1: obj.Address1,
                address2: obj.Address2,
                City: obj.City,
                StateOrProvinceCode: obj.State,
                PostalCode: obj.ZipCode,
                CountryCode: obj.CountryCode2Digits
            };

            if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                var result = {
                    data: {
                        Status: "SkipValidation",
                        Message: "No Address validation required."
                    },
                    status: 200,
                    statusText: "OK"
                }

                return $q.resolve(result);;
            }

            // Call the api to Return the result.
            return AddressValidationService.validateAddress(addressModeltp);
        }

        // -------- END -------

        $scope.SubmitPurchaseOrder = function () {
            $scope.IsBusy = true;


            var loadingElement = document.getElementById("loading");

            ///Start-remove after Async
            var node = document.createElement("span");
            var textnode = document.createTextNode("Please wait, this may take a few moments...... ");
            node.style.fontSize = "30px";
            node.style.marginTop = "100px";
            node.appendChild(textnode);
            loadingElement.appendChild(node);
            //End remmove after Async

            loadingElement.style.display = "block";
            $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/SubmitXMpo').then(function (response) {

                if (response.data.error === "") {
                    $scope.xMPO = response.data.data;
                    // $scope.xMPO.SubmittedTOPIC = true;

                    if ($routeParams.PurchaseOrderId > 0) {
                        $http.get('/api/PurchaseOrders/' + $routeParams.PurchaseOrderId + '/GetXMpo')
                            .then(function (response) {
                                $scope.xMPO = response.data.data;
                                //if ($scope.xMPO.Status === "Canceled")
                                //    $scope.xMPO.Status = "Cancelled";
                                if ($scope.xMPO.LineItems.length > 0) {
                                    var grid = $('#xVPOList').data("kendoGrid");
                                    grid.dataSource.data($scope.xMPO.LineItems);
                                }


                                var panelBar = $("#panelul").data("kendoPanelBar");
                                panelBar.expand($("#panelli"));

                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "none";
                                $scope.IsBusy = false;
                            }).catch(function (error) {
                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "none";
                                $scope.IsBusy = false;

                                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                            });
                    }

                } else {

                }
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
                $scope.IsBusy = false;
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.removeChild(node);
                loadingElement.style.display = "none";
                $scope.IsBusy = false;

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });


        }

        $scope.gotoAddCaseEditWithMPO = function () {
            location.href = '/#!/createCasexMPO/' + $routeParams.PurchaseOrderId + '/0';
        }
        $scope.gotoAddCaseEditWithVPO = function (dataItem) {
            location.href = '/#!/createCasexVPO/' + $routeParams.PurchaseOrderId + '/' + dataItem.LineNumber + '/' + dataItem.OrderId;
        }

        if ($routeParams.PurchaseOrderId > 0) {
            $http.get("/api/PurchaseOrders/" + $routeParams.PurchaseOrderId + "/CheckMPOCancelled").then(function (response) {
                $scope.MPOCancelled = response.data;
            });
        }


    }
]);