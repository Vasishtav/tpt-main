﻿'use strict';
app.controller('TaxSettingsController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http', '$interval',
    'HFCService', 'kendoService', 'NavbarService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http, $interval,
        HFCService, kendoService, NavbarService) {
        var ctrl = this;

        HFCService.setHeaderTitle("Tax Settings #");

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsOperationsTab();

        $scope.HFCService = HFCService;
        $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;
        $scope.Permission = {};

        var TaxSettings = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'TaxSettings')
        $scope.Permission.AddTaxSettings = TaxSettings.CanCreate;
        $scope.Permission.EditTaxSettings = TaxSettings.CanUpdate;
        $scope.Permission.ListTaxSettings = TaxSettings.CanRead;

        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;
        $scope.edtGrd = true;

        $scope.getTaxData = {};
        $scope.getTaxDataGet = function () {
            var geturl = '/api/Tax/0/Get';
            $http.get(geturl).then(function (response) {
                if (response.data.valid === true) {
                    $scope.getTaxData = response.data;
                    $scope.getTaxDataCopy = angular.copy(response.data);
                    $scope.CheckUseTaxFlag();
                    //$scope.GetTaxSettings();
                }
            });
        };
        $scope.UseTaxFlag = false;
        $scope.CheckUseTaxFlag = function () {
            $scope.UseTaxFlag = false;
            for (var i = 0; i < $scope.getTaxData.TaxSettings.length; i++) {
                if ($scope.getTaxData.TaxSettings[i].TaxType === 3) {
                    $scope.UseTaxFlag = true;
                }
            }
        };
        $scope.getTaxDataGet();
        /////////////////////////////////////////////////////////////////////////
        $scope.editMode = false;
        $scope.TaxSettingsEdit = function () {
            var loading = $("#loading");
            $(loading).css("display", "block");
            $scope.editMode = true;
            $(loading).css("display", "none");

        }

        ////////////////////////////////////////////////////////////////////////
        $scope.taxTypes = [];
        $scope.getTaxDDL = function () {
            var geturl = '/api/lookup/0/TaxTypes';
            $http.get(geturl).then(function (response) {
                if (response.data) {
                    $scope.taxTypes = response.data;
                }
            });
        };
        $scope.getTaxDDL();
        $scope.bringTooltipPopup = function () {
            $(".statusCanvas").show();
        }
        $scope.EnableDisableTax = function () {
            if ($scope.getTaxData.Franchise.EnableTax == false) {
                $("#WarningDisableTax").modal("show");
            }
        };
        //$scope.CancelEnableDisableTax = function () {
        //    $scope.getTaxData.Franchise.EnableTax = true;
        //    var geturl = '/api/Tax/0/EnableOrDisableTax?taxable=' + $scope.getTaxData.Franchise.EnableTax;
        //    $http.get(geturl).then(function (response) {
        //        if (response.data.valid === true) {
        //            $scope.getTaxData = response.data;
        //            $("#enableorDisableTax").modal("hide");
        //        }
        //    });
        //};

        //////////////////////////////////////////////////////////////////////////

        $scope.ShipToLocations = function () {
            var geturl = '/api/Lookup/0/GetShiptoLocationByFranchiseId';
            $http.get(geturl).then(function (response) {
                if (response.data) {
                    $scope.ShipToLocationsData = response.data;
                }
            });
        };
        $scope.ShipToLocations();
        /////////////////////////////////////////////////////////////////////////
        $scope.SelectAddress = function (index, value) {

            //if (value) {
            var address;
            var offset = index;
            var Shiplocid = value;
            for (i = 0; i < $scope.ShipToLocationsData.length; i++) {
                if ($scope.ShipToLocationsData[i].ShipToLocationId == Shiplocid) {
                    address = $scope.ShipToLocationsData[i];
                    $scope.ShipToLocationsData[i].ShipToLocationId;
                    $scope.getTaxData.TaxSettings[offset].Address = $scope.ShipToLocationsData[i].Address;
                }
            }
            //$http.get('/api/EmailSettings/' + addressId + '/GetAddressLocation').then(function (response) {

            //    var LocationValue = response.data;
            //    $scope.TerritoryEmailData[offset].LocationAddress = LocationValue;
            //});
            //}

        }
        ///Disable tax in  FE table
        $scope.ConfirmDsiableTax = function () {
            var geturl = '/api/Tax/0/EnableOrDisableTax?taxable=' + $scope.getTaxData.Franchise.EnableTax;
            $http.get(geturl).then(function (response) {
                if (response.data.valid === true) {
                    $scope.getTaxData = response.data;
                    $("#enableorDisableTax").modal("hide");
                }
            });
        };

        $scope.ContinueWarning = function () {
            $("#WarningDisableTax").modal("hide");
            $("#enableorDisableTax").modal("show");
        };
        $scope.CancelEnableDisableTax = function () {
            $scope.getTaxData.Franchise.EnableTax = true;
            $("#enableorDisableTax").modal("hide");
        };

        $scope.CancelWarningDisableTax = function () {
            $scope.getTaxData.Franchise.EnableTax = true;
            $("#WarningDisableTax").modal("hide");
        }


        $scope.CancelTaxSettings = function () {
            var orgdata = $scope.getTaxDataCopy;
            $scope.getTaxDataCopy = angular.copy(orgdata);
            $scope.getTaxData = orgdata;

            $scope.editMode = false;
        }
        $scope.saveTaxSettings = function () {
            var orgData = $scope.getTaxDataCopy;
            var loading = $("#loading");
            $(loading).css("display", "block");
            var ChangedData = [];

            /////Validations
            for (var i = 0; i < orgData.TaxSettings.length; i++) {
                if ($scope.getTaxData.TaxSettings[i].TaxType == 3 && $scope.getTaxData.TaxSettings[i].UseTaxPercentage == 0) {
                    var terr = "Grey Area";
                    if ($scope.getTaxData.TaxSettings[i].TerritoryName != null) {
                        terr = $scope.getTaxData.TaxSettings[i].TerritoryName;
                    }
                    alert("Use tax cannot be 0. Please enter Use Tax Percentage for territory " + terr);
                    var loading = $("#loading");
                    $(loading).css("display", "none");
                    return;
                }
                if ($scope.getTaxData.TaxSettings[i].TaxType == 3 && $scope.getTaxData.TaxSettings[i].UseTaxPercentage == 0) {

                }
            }


            for (var i = 0; i < orgData.TaxSettings.length; i++) {
                var change = false;
                if (orgData.TaxSettings[i].TaxType !== $scope.getTaxData.TaxSettings[i].TaxType) {
                    change = true;
                }
                if (orgData.TaxSettings[i].UseTaxPercentage !== $scope.getTaxData.TaxSettings[i].UseTaxPercentage) {
                    change = true;
                }
                if (orgData.TaxSettings[i].UseCustomerAddress !== $scope.getTaxData.TaxSettings[i].UseCustomerAddress) {
                    change = true;
                }
                if (orgData.TaxSettings[i].ShipToLocationId !== $scope.getTaxData.TaxSettings[i].ShipToLocationId) {
                    change = true;
                }
                if (change) {
                    ChangedData.push($scope.getTaxData.TaxSettings[i]);
                }
                if ($scope.getTaxData.TaxSettings[i].UseCustomerAddress === 0 && $scope.getTaxData.TaxSettings[i].ShipToLocationId === null) {
                    $scope.getTaxData.TaxSettings[i].TaxType = 1;
                }

                if (!$scope.getTaxData.TaxSettings[i].UseCustomerAddress && $scope.getTaxData.TaxSettings[i].ShipToLocationId == null && !$scope.overwrite) {
                    $("#WarningPopup").modal("show");
                    var loading = $("#loading");
                    $(loading).css("display", "none");
                    return;
                }
            }

            if (ChangedData.length > 0 || orgData.Franchise.EnableTax !== $scope.getTaxData.Franchise.EnableTax) {

                if (ChangedData.length > 0) {
                    $http.put('/api/Tax/0/UpdateSaveTaxSettings', ChangedData).then(function (response) {
                        if (response.data.valid === true) {
                            $scope.getTaxData = response.data;
                            $scope.getTaxDataCopy = angular.copy(response.data);
                            $scope.CheckUseTaxFlag();
                        }
                        $scope.editMode = false;
                        var loading = $("#loading");
                        $(loading).css("display", "none");
                    }).catch(function (error) {
                        var loading = $("#loading");
                        $(loading).css("display", "none");

                    });
                }


                if (orgData.Franchise.EnableTax !== $scope.getTaxData.Franchise.EnableTax) {
                    var geturl = '/api/Tax/0/EnableOrDisableTax?taxable=' + $scope.getTaxData.Franchise.EnableTax;
                    $http.get(geturl).then(function (response) {
                        if (response.data.valid === true) {
                            $scope.getTaxData = response.data;
                            $scope.editMode = false;
                        }
                        var loading = $("#loading");
                        $(loading).css("display", "none");
                    }).catch(function (error) {
                        var loading = $("#loading");
                        $(loading).css("display", "none");

                    });
                }
            }
            else {
                $scope.editMode = false;
                var loading = $("#loading");
                $(loading).css("display", "none");
            }
        };
        $scope.Cancel = function () {
            $("#WarningPopup").modal("hide");
        }
        $scope.overwrite = false;
        $scope.TaxSettingswarning = function (e) {
            $scope.overwrite = true;
            $scope.saveTaxSettings();
            $("#WarningPopup").modal("hide");
        }
        $scope.EditTaxDetails = function (e) {
            var grid = $("#gridtaxSettings").data("kendoGrid");
            var gridData = grid.dataSource.view();

            for (var i = 0; i < gridData.length; i++) {
                var currentUid = gridData[i].uid;
                if (gridData[i].RecordType != "Model") {
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var editButton = $(currenRow).find(".k-grid-edit");
                    //var delButton = $(currenRow).find(".k-grid-delete");
                    if ($scope.edtGrd) {
                        editButton.show();
                        //delButton.show();
                    }
                    else {
                        editButton.hide();
                        //delButton.hide();
                    }
                }
            }
            $scope.edtGrd = !$scope.edtGrd;
        }
        $scope.onChangeCheckbox = function (e) {
            var checked = $(this).is(':checked');
            var grid = $('#gridtaxSettings').data().kendoGrid;
            //grid.closeCell();
            var dataItem = grid.dataItem($(this).closest('tr'));
            var col = $(this).closest('td');
            //grid.editCell(col);
            dataItem.set(grid.columns[col.index()].field, checked);
            if (!checked) {
                var dropdownlist = $("#ShipToLocationId").data("kendoDropDownList");
                dropdownlist.enable(false);
            }
        };

        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 318);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 318);
            };
        });
        // End Kendo Resizing
    }
]);