﻿appCP.controller('JobCrewSizeController', ['$http', '$scope', '$window', '$timeout'
   , '$rootScope'
   , 'HFCService', 'NewsService'
   , '$routeParams', '$sce', 'NavbarServiceCP'
   , function ($http, $scope, $window, $timeout
       , $rootScope
       , HFCService, NewsService
       , $routeParams, $sce, NavbarServiceCP) {

       $('.JobCrewSizeHTML').css('height', '500px');

       $scope.NavbarServiceCP = NavbarServiceCP;

       $scope.NavbarServiceCP.SetupOperationsCrewSizeTab();

       $scope.HFCService = HFCService;

       $scope.ifEditMode = false;
       $scope.HTMLContent = "";
       $scope.CrewSize;
       
       $scope.BrandSelect = function () {
           if ($scope.BrandId != null) {
               $http.get('/api/JobCrewSize/' + $scope.BrandId).then(function (data) {
                   $scope.CrewSize = data.data;
                   $scope.HTMLContent = $scope.CrewSize.HTML;
                   $scope.formnews.$setPristine();

                   $scope.UpdateCrewSizeHTMLPage();

               });
           }
       }
       
       $scope.SaveCrewSize = function () {
           if ($scope.BrandId == null) {
               HFC.DisplayAlert('Please select a concept');
               return;
           }

           $scope.CrewSize.HTML = $scope.HTMLContent;
           $scope.CrewSize.BrandId = $scope.BrandId;

           if ($scope.CrewSize.HTML == null || $scope.CrewSize.HTML =='')
           {
               $scope.CrewSize.HTML = '';
           }
          
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var url = '/api/JobCrewSize/0/AddUpdateCrewSize';
            return $http.post(url, $scope.CrewSize).then(function (data) {
                $scope.CrewSize = data.data;
                loadingElement.style.display = "none";
                HFC.DisplaySuccess('Job Crew Size HTML successfully saved');
                $scope.formnews.$setPristine();
                $scope.ifEditMode = false;
                $scope.UpdateCrewSizeHTMLPage();

            }).catch(function (error) {
                HFC.DisplayAlert('Error while saving Job Crew Size HTML');
                loadingElement.style.display = "none";
            });
           
       }

       $scope.UpdateCrewSizeHTMLPage = function () {
           if ($scope.CrewSize.HTML == null)
               $scope.CrewSize.HTML = '';
           if ($.trim($scope.CrewSize.HTML) != '') {
               $("#JobCrewSizeHTML").html($scope.CrewSize.HTML);
           }
           else {
               $("#JobCrewSizeHTML").html('<span style="color:red">Job Crew Size Guidance not available</span>');
           }
       }

       $scope.EditCrewSize = function () {
           $scope.ifEditMode = true;

       }

       $scope.CancelCrewSize = function () {
           $scope.ifEditMode = false;
           $scope.BrandSelect();
       }

       $timeout(function () { document.getElementsByName('formnews')[0].reset(); }, 300);

       

   }]);