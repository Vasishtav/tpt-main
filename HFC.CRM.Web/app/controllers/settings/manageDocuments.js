﻿app.controller('manageDocumentsController', [
   '$http', '$scope', '$window', '$location', '$rootScope', '$templateCache'
   , '$sce', '$compile', 'HFCService', 'NavbarService', 'NoteServiceTP', 'NavbarService', 'kendoService', 'kendotooltipService',
   function ($http, $scope, $window, $location, $rootScope, $templateCache
       , $sce, $compile, HFCService, NavbarService, NoteServiceTP, NavbarService, kendoService, kendotooltipService) {
       $scope.NoteServiceTP = NoteServiceTP;
       NoteServiceTP.FranchiseSettings = true;

       // for kendo tooltip
       $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
       kendoService.customTooltip = null;


       // This is no more needed as the configuration permanently associated in the service.
       // NoteServiceTP.GetFranchiseSettingsGridOptions();
       NoteServiceTP.populateFranchiseGrid();
       NoteServiceTP.ParentName = null;

       $scope.HFCService = HFCService;
       $scope.NavbarService = NavbarService;
       $scope.NavbarService.SelectSettings();
       $scope.NavbarService.EnableSettingsGeneralTab();


       $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;
       $scope.Permission = {};
       var ManageDocpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'ManageDocuments')

       $scope.Permission.ListDocument = ManageDocpermission.CanRead; //$scope.HFCService.GetPermissionTP('List Documents').CanAccess;
       NoteServiceTP.FranchiseSettingsAdd = ManageDocpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Documents-ManageDocument').CanAccess;
       NoteServiceTP.FranchiseSettingsEdit = ManageDocpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Documents-ManageDocument').CanAccess;

       HFCService.setHeaderTitle("Manage Document #");

       // QuoteList.Get(data);
       $scope.NoteServiceTP.FranchiseSettingsGridOptions = kendotooltipService.setColumnWidth($scope.NoteServiceTP.FranchiseSettingsGridOptions);
       window.onresize = function () {
           $scope.NoteServiceTP.FranchiseSettingsGridOptions = kendotooltipService.setColumnWidth($scope.NoteServiceTP.FranchiseSettingsGridOptions);
       };

       // Kendo Resizing
       $scope.$on("kendoWidgetCreated", function (e) {
           $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
           window.onresize = function () {
               $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
           };
       });
       // End Kendo Resizing

   }

    ]);