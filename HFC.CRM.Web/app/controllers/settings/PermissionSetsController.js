﻿app.controller('PermissionSetsController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'HFCService', 'NavbarService',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, HFCService, NavbarService) {



        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsSecurityTab();
        $scope.submitted = false;
        $scope.Permission = {};

        var PermissionSetPermissions = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'PermissionSets')
        $scope.Permission.AddPermissionSet = PermissionSetPermissions.CanCreate;
        $scope.Permission.EditPermissionSet = PermissionSetPermissions.CanUpdate;
        $scope.Permission.ListPermissionSet = PermissionSetPermissions.CanRead;

        var dataSource = [];
        $scope.PermissionSetInformation = {
            PermissionSetName: '',
            Description: '',
        }
        $scope.IsView = false;
        
        $scope.PermissionSetId = $routeParams.PermissionSetId == undefined ? 0 : $routeParams.PermissionSetId;
      
        if (window.location.href.includes("permissionSetsView"))
        {
            $scope.IsView = true;
        }
        if ($scope.IsView) {
            $http.get('/api/Permission/' + $scope.PermissionSetId + '/GetAssignedUserList').then(function (data) {
                $scope.AssignedUserList = data.data;
            });
        }
       
        $http.get('/api/Permission/' + $scope.PermissionSetId + '/GetPermission').then(function (data) {
            
            $scope.PermissionSetInformation = data.data;
            if ($scope.PermissionSetInformation.IsActive == true) {
                $scope.PermissionSetInformation.IsActive = "Active";
            }
            else $scope.PermissionSetInformation.IsActive = "InActive";
            $scope.PermissionSets = $scope.PermissionSetInformation.PermissionSets;
            dataSource = new kendo.data.TreeListDataSource({
                data: $scope.PermissionSets
            });

            $scope.KendoTreeListOptions();
        });
       

        
        $scope.SavePermission = function() {
            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
            
            $scope.submitted = true;
            if ($scope.IsDuplicateSetName) {
                $window.alert("Permission Set with this name is already exist")
                return;
            }
           
           var checkSpecialPermission = JSLINQ(dataSource._data)
                             .Where(function (item) { return item.SpecialPermission == true; });
     
           var checkReadPermission = JSLINQ(dataSource._data)
                            .Where(function (item) { return item.Read == true; });
           var checkUpdatePermission = JSLINQ(dataSource._data)
                           .Where(function (item) { return item.Update == true; });
           var checkDeletePermission = JSLINQ(dataSource._data)
                           .Where(function (item) { return item.Delete == true; });
           var checkCreatePermission = JSLINQ(dataSource._data)
                           .Where(function (item) { return item.Create == true; });

           if (checkReadPermission.items.length == 0 && checkUpdatePermission.items.length == 0 && checkDeletePermission.items.length == 0
               && checkCreatePermission.items.length == 0 && checkSpecialPermission.items.length == 0) {
               $window.alert("At least one check box should be checked, otherwise the Permission Set will be meaningless.")
               return;
           }
          

            if ($scope.formpermissions.$valid == false) {
               
                return;
            }
            var PermissionSets = [];
            var dto = $scope.PermissionSetInformation;
            dto.PermissionSets = dataSource._data;

            $http.post('/api/Permission/0/UpdatePermissionSet', dto).then(function (data) {
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                $scope.formpermissions.$setPristine();
                window.location.href = '#!/permissionSetsView/' + data.data;
            }).catch(function(e)
            {
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            });
           
        }
        $scope.KendoTreeListOptions= function()
        {
            $scope.Height;
            if ($scope.IsView) {
                $scope.Height = 344;//628
                //$scope.Height = 423;
            } else {
                $scope.Height = window.innerHeight - 254;
            }
            $("#treelist").kendoTreeList({
                dataSource: dataSource,
                height: $scope.Height,
                
                columns: [
                    {
                        field: "Name",
                        width: "360px",
                        expandable: true
                    },

                    {
                        field: "SpecialPermission",
                        title: "Is Accessible",
                       
                        template: function (e) {
                            if (e.IsSpecialPermission && !$scope.IsView) {
                                return "<input type='checkbox' data-bind='checked: SpecialPermission' style='margin: 0px 45% !important'/>";
                            }
                            else if (e.IsSpecialPermission && $scope.IsView)
                            {
                                //return "<input type='checkbox' disabled data-bind='checked: SpecialPermission' />";
                                return ShowSpecialDetail(e);
                            }
                        },
                    },
                  {
                      field: "Create",
                      
                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Create' value='Create' ng-checked='" + $scope.UpdateRead(event, dataSource, e) + "' style='margin: 0px 45% !important'/>";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return ShowCreateDetail(e)
                              //return "<input type='checkbox' disabled data-bind='checked: Create' />";
                          }
                      },

                  },
                  {
                      field: "Read",
                     
                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Read' style='margin: 0px 45% !important'/>";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              //return "<input type='checkbox' disabled data-bind='checked: Read' />";
                              return ShowReadDetail(e);
                          }
                          
                      },

                  },
                  {
                      field: "Update",
                      
                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Update' value='Update' ng-checked='" + $scope.UpdateRead(event, dataSource, e) + "' style='margin: 0px 45% !important'/>";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              //return "<input type='checkbox' disabled data-bind='checked: Update' />";
                              return ShowUpdateDetail(e);
                          }
                      },
                  },
                  {
                      field: "Delete",
                      hidden:true,
                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Delete' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Delete' />";
                          }
                      },

                  },



                ],
                dataBound: function () {
                    var view = this.dataSource.view();
                    this.items().each(function (index, row) {
                        kendo.bind(row, view[index]);
                    });
                }
            });
            if ($scope.IsView) {
                $("body").css("overflow-y", "scroll");
            }
            else {
                $("html").css("overflow", "hidden");
                $("body").css("overflow", "hidden");
            }
        }
        $scope.Cancel = function () {
            window.location.href = '#!/permissionSetsSearch';
        }

        $scope.UpdateRead = function (abc,data,val) {
             
           
            if (abc && abc.target && abc.target.value == "Create" || abc.target.value=="Update") {
                var target = abc.target.value;
                if (abc.srcElement.kendoBindingTarget != undefined) {
                    var data = abc.srcElement.kendoBindingTarget.source;
                }

                if (data[target] && data.ModuleId == val.ModuleId) data.Read = true;
                else if (!data[target] && data.ModuleId == val.ModuleId) data.Read = false;
            }
           
        }

       

        //Redirect to Create Permission set Page
        $scope.NewPermissionSet = function () {
            window.location.href = '#!/permissionSet';
            //var url = "http://" + $window.location.host + "/#!/leads/";
            //$window.location.href = url;
        };
        $scope.EditPermissionSet = function () {
            window.location.href = "#!/permissionSets/" + $scope.PermissionSetId;
            //var url = "http://" + $window.location.host + "/#!/leads/";
            //$window.location.href = url;
        };

        $scope.CheckDuplicatePermissionName = function (name) {
            $scope.IsDuplicateSetName = false;
            if (name == undefined) {
                $scope.submitted = true;
                return;
            }
            else {
                $http.post('/api/Permission/' + $scope.PermissionSetId + '/CheckDuplicatePermissionSet/?name=' + name).then(function (data) {
                    
                    
                    var permissionCheck = data.data;
                    if (permissionCheck)
                    {
                        $scope.IsDuplicateSetName = true;
                        $window.alert("Permission Set with this name is already exist")
                    }
                    else {
                        $scope.IsDuplicateSetName = false;
                    }
                });
            }
        }

        function ShowSpecialDetail(details) {
            var Div = '';
            if (details.IsSpecialPermission) {
                Div += '<span class="field index_cbox" ng-if="' + details.IsSpecialPermission + '" style="margin: 0px 45% !important">';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.IsSpecialPermission + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }

            return Div;
        }

        function ShowCreateDetail(details) {

            var Div = '';
            if (details.Create) {
                Div += '<span class="field index_cbox" ng-if="' + details.Create + '" style="margin: 0px 45% !important">';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.Create + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }

            return Div;

        }
        function ShowReadDetail(details) {
            var Div = '';
            if (details.Read) {
                Div += '<span class="field index_cbox" ng-if="' + details.Read + '" style="margin: 0px 45% !important">';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.Read + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }

            return Div;
        }
        function ShowUpdateDetail(details) {
            var Div = '';
            if (details.Update) {
                Div += '<span class="field index_cbox" ng-if="' + details.Update + '" style="margin: 0px 45% !important">';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.Update + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }

            return Div;
        }

        window.onresize = function () {
            if($scope.IsView){
                // $('#treelist .k-grid-content.k-auto-scrollable').css("height", window.innerHeight - 344);
            }
        else{
             $('#treelist .k-grid-content.k-auto-scrollable').css("height", window.innerHeight - 289);
            }
        };
    }
]);