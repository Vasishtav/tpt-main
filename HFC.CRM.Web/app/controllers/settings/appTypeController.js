﻿app
    .directive('colorPickerAppType', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.ColorPickerSliders({
                    // initialize the color to the color on the scope
                    flat: false,
                    order: {
                        hsl: 1,
                        opacity: 2
                    },
                    color: ngModel.$modelValue != "" ? ngModel.$modelValue : 'rgb(194, 194, 194)',
                    // update the ngModel whenever we pick a new color
                    onchange: function (id, newValue) {
                        scope.$apply(function () {
                            if (newValue && newValue != "") {
                                ngModel.$setViewValue(newValue.tiny.toRgbString());
                            }
                        });
                        //var span = $("#previewhsl");

                        //span.css("background-color", newValue.tiny.toRgbString());

                    }
                });

                // update the color picker whenever the value on the scope changes
                ngModel.$render = function () {
                    element.val(ngModel.$modelValue != "" ? ngModel.$modelValue : 'rgb(194, 194, 194)');
                    element.change();
                };
            }
        }
    })
    .controller('appTypeController', [
   '$http', '$scope', '$window', '$location', '$rootScope', '$templateCache', '$sce', '$compile', 'HFCService', 'NavbarService', 'HFCService',
   function ($http, $scope, $window, $location, $rootScope, $templateCache, $sce, $compile, HFCService, NavbarService, HFCService) {

       $scope.HFCService = HFCService;
       $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;

       $scope.NavbarService = NavbarService;
       $scope.NavbarService.SelectSettings();
       $scope.NavbarService.EnableSettingsGeneralTab();


       HFCService.setHeaderTitle("Appointment Type #");

       $scope.AppointmentTypes = {
           FranchiseAppointmentTypeColorId: 0,
           FranchiseId: 0,
           Appointment1: '',
           Appointment2: '',
           Appointment3: '',
           Appointment4: '',
           Sales_Design: '',
           Installation: '',
           DayOff: '',
           Other: '',
           Personal: '',
           Meeting_Training: '',
           Service: '',
           Vacation: '',
           Holiday: '',
           Followup: '',
           TimeBlock: '',
           EnableBorders: false,
           Enable: false,

       }

       $scope.Permission = {};
       
       var AppointmentTypeSettingspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'AppointmentTypeSettings')
       $scope.Permission.ViewAppointment = AppointmentTypeSettingspermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Appointment').CanAccess;
       $scope.Permission.EditAppointment = AppointmentTypeSettingspermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Appointment').CanAccess;



       $scope.setAction = function () {
           if ($scope.AppointmentTypes.FranchiseAppointmentTypeColorId > 0) {
               var url = "/api/AppointmentTypeColor/0/UpdateColors";
               $scope.Save(url);

           }
           else {
               var url = "/api/AppointmentTypeColor/0/SaveColors";
               $scope.Save(url);
           }
       }
       $scope.EditColors = function () {
           window.location.href = "#!/editAppointmentTypes";
       }
       $scope.cancel = function () {
           window.location.href = "#!/appointmentTypes";
       }

       $scope.Save = function (url) {
           var dto = $scope.AppointmentTypes;
           
           $http.post(url, dto).then(function (response) {
               
               HFC.DisplaySuccess("Appointment Type colors saved!");
               $scope.formnewuser_edit.$setPristine();
               window.location.href = "#!/appointmentTypes";
           }, function (response) {
               HFC.DisplayAlert(response.statusText);
           });
       }

       $scope.GetColorSettings = function () {
           $http.get('/api/AppointmentTypeColor/0/GetColors').then(function (response) {
               
               if (response.data != null) {
                   $scope.AppointmentTypes = response.data;
               }
           });
       }

       $(document).click(function (event) {
           var elementFlag = $(event.target).hasClass("frm_controllead1");
           var popUpId = $(event.target).attr("aria-describedby");
           var popupDom = "#" + popUpId;
           if (!elementFlag) {
               var openFlag = $(".popover.fade").hasClass("show");
               if (openFlag) {
                   $(".popover.fade").removeClass("show");
                   $(".popover.fade").remove();
               }
           } else {
               $(".popover.fade").removeClass("show");
               $(popupDom).addClass("show");
           }
       });

       $scope.GetColorSettings();
   }
    ]);