﻿'use strict';
app.controller('PermissionSearchController', function ($scope, $http) {

   
    function ActivateDeActivateTemplate(dataitem) {
       

        var EditOrder = "";
        if (PermissionEditOrder == true) {
            EditOrder = '<li><a href="#!/orderEdit/' + dataitem.OrderId + '">Edit</a></li> ';

        }
        return '<ul> <li class="dropdown note1 ad_user"> <div class="btn-group">' +
            ' <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
            ' <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu">' +
            EditOrder +
           
            
            '</ul> </li>  </ul>'

    }
    $scope.GetPermissionSets = function () {
        $scope.GridEditable = false;
        $scope.PermissionSets = {
            dataSource: {
                transport: {
                    read: {
                        url: function (params) {

                            var url1 = '/api/Shipping/0/Get';
                            return url1;
                        },
                        dataType: "json"
                    }

                },
                error: function (e) {

                    HFC.DisplayAlert(e.errorThrown);
                },

                schema: {
                    model: {
                        id: "id",
                        fields: {
                          
                            Name: { editable: false},
                            Description: { editable: false },
                            Created: { editable: false },
                            Modified: { editable: false },
                        }
                    }
                }
            },
            columns: [
                {
                    field: "Name",
                    title: "Name",
                    filterable: { multi: true, search: true },
                    hidden: false,
                },
                {
                    field: "Description",
                    title: "Description",
                    filterable: { multi: true, search: true },
                    hidden: false,


                },
                        {
                            field: "Created",
                            title: "Created",
                            filterable: { multi: true, search: true },
                            hidden: false,

                        },
                         {
                             field: "Modified",
                             title: "Modified",

                             hidden: false,

                         },
                        
                           {
                               field: "",
                               title: "",
                               template: function (dataitem) {
                                   return ActivateDeActivateTemplate(dataitem);
                               }
                           },


            ],

            filterable: true,
            resizable: true,
            autoSync: true,
            noRecords: { template: "No records found" },
            scrollable: true,
            toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="row vendor_search" style="margin:0 !important;"><div class="leadsearch_topleft col-sm-9 col-md-9 col-xs-9 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="ShipgridSearchEdit()" type="search" id="searchBox" placeholder="Search" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search" style="padding:0 !important;"><input type="button" id="btnReset" ng-click="ShipgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="col-sm-3 col-xs-3 no-padding"><div class="leadsearch_icon" ng-if="Permission.AddProcurement"><div class="plus_but tooltip-bottom procurement_tooltip" data-tooltip="Add New Ship To Location"  onclick="return false;" ng-click="AddShipping()"><i class="fa fa-plus-square-o"></i></div></div></div></div></div></script>').html()) }],

        };

    };

    $scope.CampaigngridSearchClear = function () {
        $('#searchBox').val('');
        $("#gridCampaigns").data('kendoGrid').dataSource.filter({
        });
    }

    $scope.CampaigngridSearch = function () {
        var searchValue = $('#searchBox').val();
        $("#gridCampaigns").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
              {
                  field: "Channel",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "Source",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "Campaign",
                  operator: "contains",
                  value: searchValue
              }

            ]
        });

    }
})