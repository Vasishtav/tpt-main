﻿app.directive('onlyDigits', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var digits = val.replace(/[^0-9.]/g, '');

                    if (digits.split('.').length > 2) {
                        digits = digits.substring(0, digits.length - 1);
                    }

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();
                    }
                    return parseFloat(digits);
                }
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
})

    .directive('confirmOnExit', function () {
        return {
            link: function ($scope, elem, attrs) {
                window.onbeforeunload = function () {
                    if ($scope.formfranchisepricing.$dirty) {
                        return "Any unsaved changes will be lost. do want to continue?";
                    }
                }
                $scope.$on('$locationChangeStart', function (event, next, current) {
                    if ($scope.formfranchisepricing.$dirty) {
                        if (!confirm("Any unsaved changes will be lost. do want to continue?")) {
                            event.preventDefault();
                        }
                    }
                });
            }
        };
    })

    .controller('pricingController', [
    '$http', '$scope', '$window', '$location', '$rootScope', '$templateCache', '$sce', '$compile', '$http', 'HFCService', 'NavbarService',
    function ($http, $scope, $window, $location, $rootScope, $templateCache, $sce, $compile, $http, HFCService, NavbarService) {

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsOperationsTab();
        $scope.HFCService = HFCService;

        HFCService.setHeaderTitle("Pricing Management #");

        $scope.Permission = {};
        var AdvancedPricingSettingspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'AdvancedPricingSettings')

        $scope.Permission.DisplayFranchisePricing = AdvancedPricingSettingspermission.CanRead; //$scope.HFCService.GetPermissionTP('Display FranchisePricing').CanAccess;
        $scope.Permission.AddFranchisePricing = AdvancedPricingSettingspermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add FranchisePricing').CanAccess;
        $scope.Permission.DisplayAdvancedPricing = AdvancedPricingSettingspermission.CanRead; //$scope.HFCService.GetPermissionTP('Display AdvancedPricing').CanAccess;

        $scope.roundupfactor = '';
        $scope.PricingSubmitted = false;

        $scope.Pricing = {
            Id: '',
            FranchiseId: '',
            PricingStrategyTypeId: '',
            VendorId: null,
            ProductCategoryId: null,
            RetailBasis: '',
            MarkUp: '',
            MarkUpFactor: '%',
            Discount: '',
            PriceRoundingOpt: '',
            DiscountFactor: ''
        }
        $scope.editMode = false;

        $scope.saveFranchiseLevelPricing = function () {
            $scope.PricingSubmitted = true;
            $("#markup").removeClass("dropdown-validation-error");
            var otherthing = $scope.formfranchisepricing.$error;
            var mng = $scope.formfranchisepricing.$invalid;
            if ($scope.formfranchisepricing.$invalid) {
                return;
            }

            var dto = $scope.Pricing;
            if (dto.MarkUp !== undefined && dto.MarkUp != '' && dto.MarkUp != null && parseFloat(dto.MarkUp) < 0) {
                HFC.DisplayAlert("Markup cannot be Negative");
                $("#markup").addClass("dropdown-validation-error");
                return;
            } else {
                $http.post('/api/PricingManagement/0/SaveFranchiseLevel', dto).then(function (response) {
                    var saveStatus = response.data;
                    if (saveStatus === "Success") {
                        $scope.PricingSubmitted = false;
                        $scope.editMode = false;
                        $scope.getPricing();
                        $scope.formfranchisepricing.$setPristine();
                        $scope.formfranchisepricing.$dirty = false;
                        HFC.DisplaySuccess("Success");
                    }
                    else {
                        HFC.DisplayAlert("Error Saving Pricing.");
                    }

                });
            }

            //if (dto.MarkUp !== undefined) {
            //    if (dto.MarkUp == 0 ||dto.MarkUp < 0||dto.MarkUp > 99) {
            //        HFC.DisplayAlert("MarkUp value must be of 0 - 99");
            //        return;
            //    }
                //if (dto.PricingStrategyTypeId == '1') {
                //    if (dto.MarkUp == 0||dto.MarkUp < 0||dto.MarkUp > 99) {
                //        HFC.DisplayAlert("MarkUp value must be of 0 - 99");
                //        return;
                //    }
                //   else if (parseFloat(dto.MarkUp) < 0 || parseFloat(dto.MarkUp) > 99) {
                //       //$("#Model2").modal("show");
                //       HFC.DisplayAlert("MarkUp value must be of 0 - 99");
                //    return;
                //}
                    //if (dto.MarkUp === '' || dto.MarkUp === null) {
                    //    //$("#Model2").modal("show");
                    //    return;
                    //}
                    //else if (parseFloat(dto.MarkUp) < 0 || parseFloat(dto.MarkUp) > 100) {
                    //   // $("#Model2").modal("show");
                    //    return;
                    ////}
                //}
                //API call to save or update the pricing

             

            //}
            //else {
            //   // $("#Model2").modal("show");
            //    return;
            //}
            //console.log(dto);
        }
        $scope.bringTooltipPopup = function () {
            $(".statusCanvas").show();
        }
        $scope.EditPricing = function() {
            $scope.editMode = true;
        }
        $scope.onPricingCancel = function () {
            $scope.getPricing();
            $scope.editMode = false;
            $scope.formfranchisepricing.$dirty = false;
        }
        $scope.getPricing = function () {
            var geturl = '/api/PricingManagement/0/GetFranchiseLevel';
            $http.get(geturl).then(function (response) {
                if (response.data) {
                    $scope.Pricing = response.data;
                }
            });
        }


        $scope.cancel = function () {
            $scope.formfranchisepricing.$setPristine();
            $window.location.reload();
        }
        $scope.getPricing();
    }
    ]);