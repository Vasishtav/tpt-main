﻿'use strict';
app.controller('RoleSearchController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'HFCService', 'NavbarService',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, HFCService, NavbarService) {


        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsSecurityTab();
        HFCService.setHeaderTitle("Roles #");
        $scope.Permission = {};
        var offsetvalue = 0;
        var RolesPermissions = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Roles')
        $scope.Permission.AddRolesPermissions = RolesPermissions.CanCreate;
        $scope.Permission.EditRolesPermissions = RolesPermissions.CanUpdate;
        $scope.Permission.ListRolesPermissions = RolesPermissions.CanRead;

        function ActivateDeActivateTemplate(dataitem) {
            return '<ul> <li class="dropdown note1 ad_user"> <div class="btn-group">' +
                ' <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                ' <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu">' +
                '<li><a href="#!/rolesView/' + dataitem.RoleId + '">Edit</a></li> ' +
                '</ul> </li>  </ul>'

        }
        $scope.GetRolesList = function () {

            $scope.RolesList = {
                dataSource: {
                    transport: {
                        read: {
                            url: function (params) {

                                var url1 = '/api/Permission/0/GetRolesList';
                                return url1;
                            },
                            dataType: "json"
                        }

                    },
                    requestStart: function () {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "block";
                       // kendo.ui.progress($(".k-grid-content.k-auto-scrollable"), true);
                    },
                    requestEnd: function () {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                           // kendo.ui.progress($(".k-grid-content.k-auto-scrollable"), false);                       
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                        // kendo.ui.progress($(".k-grid-content.k-auto-scrollable"), false);
                    },                    
                    schema: {
                        model: {
                            id: "RoleId",
                            fields: {
                                RoleId: { editable: false },
                                RoleName: { editable: false },
                                DisplayName: { editable: false },
                                FranchiseId: { editable: false },
                                Description: { editable: false },
                                CreatedOnUtc: { type: 'date', editable: false },
                                
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "DisplayName",
                        title: "Name",
                        template: "<a href='\\\\#!/rolesView/#= RoleId #'><span>#= DisplayName #</span></a> ",
                        filterable: { search: true },
                        hidden: false,
                    },
                    {
                        field: "Description",
                        title: "Description",
                        //filterable: { multi: true, search: true },
                        hidden: false,


                    },
                    {
                        field: "CreatedOnUtc",
                        title: "Created",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "RolesFilterCalendar", offsetvalue),
                        hidden: false,
                        template: function (dataItem) {
                            return HFCService.NewKendoDateFormat(dataItem.CreatedOnUtc);
                        }
                            //"#= kendo.toString(new Date(CreatedOnUtc), 'MM/dd/yyyy') #",

                    },
                    

                    //{
                    //    field: "",
                    //    title: "",
                    //    template: function (dataitem) {
                    //        return ActivateDeActivateTemplate(dataitem);
                    //    }
                    //},


                ],
               
                //filterable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                sortable: true,
                resizable: true,
                autoSync: true,
                noRecords: { template: "No records found" },
                scrollable: true,
                toolbar: [
                        {
                            template: kendo.template(
                                $(
                                    ' <script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="RolesSearchgridSearch()" type="search" id="searchBox" placeholder="Search Role" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="RolesSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>')
                                .html())
                        }
                ],

            };

        };
        if ($scope.Permission.ListRolesPermissions) {
            $scope.GetRolesList();
        }

        $scope.RolesSearchClear = function () {
            $('#searchBox').val('');
            $("#gridrolesSearch").data('kendoGrid').dataSource.filter({
            });
        }

        $scope.RolesSearchgridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#gridrolesSearch").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  {
                      field: "DisplayName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "Description",
                      operator: "contains",
                      value: searchValue
                  }
                ]
            });

        }

        //Redirect to Create Permission set Page
        $scope.NewPermissionSet = function () {
            window.location.href = '#!/permissionSet';
            //var url = "http://" + $window.location.host + "/#!/leads/";
            //$window.location.href = url;
        };
    }])