﻿app.controller('RolesController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'HFCService', 'NavbarService',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, HFCService, NavbarService)
    {
        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsSecurityTab();

        $scope.Permission = {};

        var RolesPermissions = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Roles')
        $scope.Permission.AddRolesPermissions = RolesPermissions.CanCreate;
        $scope.Permission.EditRolesPermissions = RolesPermissions.CanUpdate;
        $scope.Permission.ListRolesPermissions = RolesPermissions.CanRead;

        var dataSource = [];
        $scope.RoleId = $routeParams.RoleId == undefined ? 0 : $routeParams.RoleId;
        $http.get('/api/Permission/' + $scope.RoleId + '/GetPermissionByRolesId').then(function (data) {
            
           
            $scope.PermissionSets = data.data.PermissionSets;
            dataSource = new kendo.data.TreeListDataSource({
                data: $scope.PermissionSets
            });

            $scope.KendoTreeListOptions();
        });

        $http.get('/api/Permission/' + $scope.RoleId + '/GetRoleName').then(function (data) {
             
            var DisplayRolename = data.data;
            $scope.RoleName = DisplayRolename.DisplayName;
        })

        $scope.KendoTreeListOptions = function () {
            $("#treelist").kendoTreeList({
                dataSource: dataSource,
                height: window.innerHeight - 178,
                columns: [
                    {
                        field: "Name",
                        width: "320px",
                        expandabe: true
                    },

                    {
                        field: "SpecialPermission",
                        title: "Is Accessible",
                       
                        template: function (e) {
                            
                            if (e.IsSpecialPermission) {
                                return ShowSpecialDetail(e);
                                //return "<input type='checkbox' disabled data-bind='checked: SpecialPermission' />";
                            }
                        },
                    },
                  {
                      field: "Create",
                    
                      template: function (e) {
                          
                          if (e.ModuleId != null && !e.IsSpecialPermission) {
                              return ShowCreateDetail(e);
                              //return "<input type='checkbox' disabled data-bind='checked: Create' />";
                                
                            }
                      },

                  },
                  {
                      field: "Read",
                     
                      template: function (e) {
                         
                          if (e.ModuleId != null && !e.IsSpecialPermission) {
                              return ShowReadDetail(e);
                              //return "<input type='checkbox' disabled data-bind='checked: Read' />";
                          }

                      },

                  },
                  {
                      field: "Update",
                     
                      template: function (e) {
                         
                          if (e.ModuleId != null && !e.IsSpecialPermission) {
                              return ShowUpdateDetail(e);
                              //return "<input type='checkbox' disabled data-bind='checked: Update' />";
                          }
                      },
                  },
                  {
                      field: "Delete",
                      hidden:true,
                      template: function (e) {
                        
                          if (e.ModuleId != null && !e.IsSpecialPermission) {
                              return ShowDeleteDetail(e);
                              //return "<input type='checkbox' disabled data-bind='checked: Delete' />";
                          }
                      },

                  },



                ],

                
                filterable: true,
                resizable: true,
                noRecords: { template: "No records found" },
                scrollable: true,
                dataBound: function () {
                    var view = this.dataSource.view();
                    this.items().each(function (index, row) {
                        kendo.bind(row, view[index]);
                    });
                }

            });
           
        }

      
      

        function ShowSpecialDetail(details) {
            var Div = '';
            if (details.SpecialPermission) {
                Div += '<span class="field index_cbox" ng-if="' + details.SpecialPermission + '" style="margin: 0px 45% !important">';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.SpecialPermission + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }

            return Div;
        }

        function ShowCreateDetail(details) {
           
            var Div = '';
            if (details.Create) {
                Div += '<span class="field index_cbox" ng-if="' + details.Create + '" style="margin: 0px 45% !important">';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.Create + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }
            
            return Div;
            
        }
        function ShowReadDetail(details) {
            var Div = '';
            if (details.Read) {
                Div += '<span class="field index_cbox" ng-if="' + details.Read + '" style="margin: 0px 45% !important" >';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.Read + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }

            return Div;
        }
        function ShowUpdateDetail(details) {
            var Div = '';
            if (details.Update) {
                Div += '<span class="field index_cbox" ng-if="' + details.Update + '" style="margin: 0px 45% !important">';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.Update + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }

            return Div;
        }
        function ShowDeleteDetail(details) {
            var Div = '';
            if (details.Delete) {
                Div += '<span class="field index_cbox" ng-if="' + details.Delete + '" >';
                Div += '<input disabled="true" type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + details.Delete + '" style="border:0px solid #b2bfca">';
                Div += '<label class="checkbox" disabled="true" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
                Div += '</span>';
            }

            return Div;
        }

        window.onresize = function () {
            var resize = $('#treelist .k-grid-content.k-auto-scrollable').css("height", window.innerHeight - 218);
            //178
           // height(window.innerHeight - $("#MenuDiv").height() - 370);
        };
            
       
    }
]);