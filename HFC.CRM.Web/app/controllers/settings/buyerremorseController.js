﻿
// TODO: rquired refactoring:

'use strict';
app
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('buyerremorseController',
    [
        '$scope', '$window', 'HFCService',  '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', 'NavbarService', '$q',

        function ($scope, $window, HFCService,  $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, NavbarService, $q) {

            $scope.editMode = false;
            $scope.isDisabled = true;           

            $scope.NavbarService = NavbarService;
            $scope.NavbarService.SelectSettings();
            $scope.NavbarService.EnableSettingsOperationsTab();

            $scope.HFCService = HFCService;
            HFCService.setHeaderTitle("Sales Settings #");
            //flag for digital signature
            $scope.AdminElectronicSign = HFCService.AdminElectronicSign;
            $scope.Permission = {};
            var BuyerRemorseSettingspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'BuyerRemorseSettings')

            $scope.Permission.RemorseView = BuyerRemorseSettingspermission.CanRead; //$scope.HFCService.GetPermissionTP('Buyer Remorse Settings View').CanAccess;
            $scope.Permission.RemorseEdit = BuyerRemorseSettingspermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Buyer Remorse Settings Edit').CanAccess;

            //clicking on icon to show the pop-up
            $scope.DisplayPopup = function () {
                $("#Buyerremorseinfo").modal("show");
            }

            $scope.DisplayQuotePopup = function () {
                $("#Quoteinfo").modal("show");
            }

            //cancel the pop-up
            $scope.Cancel = function (modalId) {
                $("#" + modalId).modal("hide");
            };



            $scope.Buyers = {
                FranchiseId: '',
                Name: '',
                BuyerremorseDay: '',
                QuoteExpiryDays: '',
                EnableElectronicSignFranchise: false
            }


            $scope.GetRemorseValue = function () {
                $http.get('/api/FranchiseSettings/0/BuyerRemorse/').then(function (response) {
                    //
                    $scope.Buyers = response.data;
                    $scope.Buyers.BuyerremorseDay = response.data.BuyerremorseDay.toString();
                    $scope.Buyers.QuoteExpiryDays = response.data.QuoteExpiryDays.toString();
                    $scope.Buyers.EnableElectronicSignFranchise = response.data.EnableElectronicSignFranchise;
                    $scope.HFCService.FranciseLevelElectronicSign = response.data.EnableElectronicSignFranchise;
                    if ($scope.Buyers.QuoteExpiryDays == 0)
                        $scope.Buyers.QuoteExpiryDays = '';
                    if ($scope.Buyers.BuyerremorseDay == 0)
                        $scope.Buyers.BuyerremorseDay = '';
                    
                });
            }
           
            $scope.GetRemorseValue();

            $scope.EditRemorse = function () {
                $scope.editMode = true;
                $scope.isDisabled = false;
            }

            $scope.onRemorseCancel = function () {
                $scope.editMode = false;
                $scope.isDisabled = true;
                $scope.GetRemorseValue();
            }


            $scope.saveRemorseValue = function () {

                if (!$scope.Buyers.BuyerremorseDay)
                    $scope.Buyers.BuyerremorseDay = 0;

                if ($scope.Buyers.QuoteExpiryDays == "")
                {
                    HFC.DisplayAlert("Quote expiration days must be greater than 0");
                    return;
                }


                $http.get('/api/FranchiseSettings/' + $scope.Buyers.QuoteExpiryDays + '/SaveBuyerRemose?BuyerremorseDay=' + parseInt($scope.Buyers.BuyerremorseDay) + '&EnableElectSign=' + $scope.Buyers.EnableElectronicSignFranchise).then(function (response) {
                    if (response.data == "Success") {
                        $scope.editMode = false;
                       
                        $scope.GetRemorseValue();
                        $scope.isDisabled = true;
                    }
                    else HFC.DisplayAlert(response.data);
                });
               
            }

        }
          
    ])
