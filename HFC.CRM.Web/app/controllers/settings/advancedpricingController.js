﻿app.directive('onlyDigits', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var digits = val.replace(/[^0-9.]/g, '');

                    if (digits.split('.').length > 2) {
                        digits = digits.substring(0, digits.length - 1);
                    }

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();
                    }
                    return parseFloat(digits);
                }
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
})
    .directive('confirmOnExit', function () {
        return {
            link: function ($scope, elem, attrs) {
                window.onbeforeunload = function () {
                    if ($scope.formadvancedpricing.$dirty) {
                        return "Any unsaved changes will be lost. do want to continue?";
                    }
                    if ($scope.editformadvancedpricing.$dirty) {
                        return "Any unsaved changes will be lost. do want to continue?";
                    }
                }
                $scope.$on('$locationChangeStart', function (event, next, current) {
                    if ($scope.formadvancedpricing.$dirty) {
                        if (!confirm("Any unsaved changes will be lost. do want to continue?")) {
                            event.preventDefault();
                        }
                    }
                    if ($scope.editformadvancedpricing.$dirty) {
                        if (!confirm("Any unsaved changes will be lost. do want to continue?")) {
                            event.preventDefault();
                        }
                    }
                });
            }
        };
    })
    .service('AdvPricingService', ['$http', '$location', 'HFCService', function ($http, $location, HFCService) {
        var srv = this;
        srv.Pricing = {
            Id: '',
            FranchiseId: '',
            PricingStrategyTypeId: '',
            VendorId: '',
            ProductCategoryId: '',
            RetailBasis: '',
            MarkUp: '',
            MarkUpFactor: '%',
            Discount: '',
            PriceRoundingOpt: '',
            DiscountFactor: '%'
        }

        srv.ProductCategoryDDL = {
            ProductCategoryId: '',
            ProductCategory: ''
        }

        srv.PriceStrategyDDL = {
            Id: '',
            PricingStrategyType: ''
        }

        srv.VendorDDL = {
            VendorId: '',
            VendorName: ''
        }

        srv.PricingList = [srv.Pricing];


    }])
    .controller('advancedpricingController', [
    '$http', '$scope', '$window', '$location', '$rootScope', '$templateCache', '$sce', '$compile', '$http', 'HFCService', 'NavbarService', 'AdvPricingService',
    function ($http, $scope, $window, $location, $rootScope, $templateCache, $sce, $compile, $http, HFCService, NavbarService, AdvPricingService) {

        $scope.NavbarService = NavbarService;
        $scope.AdvPricingService = AdvPricingService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsOperationsTab();
        $scope.HFCService = HFCService;
        $scope.addpricingCount = [];
        $scope.editPrice = 0;
        $scope.roundupfactor = '';
        $scope.PricingSubmitted = false;
        $scope.addpricing = false;
        $scope.SaveAndAddRow = false;
        $scope.EditMode = false;
        $scope.EditPricingSubmitted = false;
        $scope.ListPricing = AdvPricingService.PricingList;

        HFCService.setHeaderTitle("Pricing Management #");

        $scope.Permission = {};
        var AdvancedPricingSettingspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'AdvancedPricingSettings')

        $scope.Permission.AddAdvancedPricing = AdvancedPricingSettingspermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add AdvancedPricing').CanAccess;
        $scope.Permission.EditAdvancedPricing = AdvancedPricingSettingspermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit AdvancedPricing').CanAccess;
        $scope.Permission.DisplayAdvancedPricing = AdvancedPricingSettingspermission.CanRead; //$scope.HFCService.GetPermissionTP('Display AdvancedPricing').CanAccess;

        $scope.SwitchPricingStgyDDL = '';

        $scope.NewPrice = angular.copy(AdvPricingService.Pricing);


        $scope.addNewPricing = function () {
            
           
            var rowEdit = $('#gridPricingManagement').find('.k-grid-edit-row');

            if (rowEdit.length) {
                //grid is not in edit mode
                var validator = $scope.kendoValidator("gridPricingManagement");
                if (!validator.validate()) {
                    return false;
                }
                else {
                    $scope.SaveAndAddRow = true;
                    $scope.saveAdvPricing();
                }
            }
            
            
            //if we search and click add button
            if (!$scope.searchBoxEnable) {
                $('#searchBox').val('');
                $("#gridPricingManagement").data('kendoGrid').dataSource.filter({
                });
            }
            var grid = $("#gridPricingManagement").data("kendoGrid");
            
           
            grid.addRow();
           
            $scope.EditMode = true;
            $scope.advancedpricing_form.$setDirty();
        };

        $scope.closeadvancedpricehelp = function () {
            var myDiv = $('.closeadvancedpricehelp');
            myDiv[0].scrollTop = 0;
            
            $("#helppopup").modal("hide");
        }

        $scope.removeNewPricing = function (e) {
           
            if ($scope.editedPriceValue != undefined)
                $scope.editedPriceValue = undefined;
            var dataItem = e.dataItem;
            var grid = $("#gridPricingManagement").data("kendoGrid");
            var dataSource = grid.dataSource;
            dataSource.remove(dataItem);
            $http.delete('/api/PricingManagement/' + dataItem.Id).then(function (response) {
                if (response.data == "Success") {
                    $scope.EditMode = false;
                    $("#gridPricingManagement").data("kendoGrid").dataSource.read();
                }
                
            });
            
        };

        $scope.saveAdvPricing = function () {
            var grid = $('#gridPricingManagement').data().kendoGrid.dataSource.view();
            var validator = $scope.kendoValidator("gridPricingManagement");
           
            if (validator.validate()) {
                $scope.PricingSubmitted = true;
                if (grid.length && $scope.editedPriceValue == undefined)
                    var dto = grid[grid.length-1];
                
                else if ($scope.editedPriceValue) {
                    var dto = $scope.editedPriceValue;
                    $scope.editedPriceValue = undefined;
                }
                if (dto.PricingStrategyTypeId == '1') {

                    
                }
                else if (dto.PricingStrategyTypeId == '2') {

                    if (dto.VendorId == '' || dto.VendorId == null) {
                        return;
                    }
                }
                else if (dto.PricingStrategyTypeId == '3') {
                    if (dto.ProductCategoryId == '' || dto.ProductCategoryId == null) {
                        return;
                    }

                }
                else {
                    return
                }
                    var markup = 0, disc = 0;
                    if (dto.MarkUp !== undefined && dto.MarkUp !== '' && dto.MarkUp !== null) {
                        if (parseFloat(dto.MarkUp) < 0) {
                            HFC.DisplayAlert("Markup cannot be negitive");
                            return;
                        }
                        else {
                            markup = dto.MarkUp;
                       
                        }
                    }
                    if (dto.Discount !== undefined && dto.Discount !== '' && dto.Discount !== null) {
                        if (parseFloat(dto.Discount) < 0) {
                            HFC.DisplayAlert("Discount cannot be negitive");
                            return;
                        }
                        else if (parseFloat(dto.Discount) > 99) {
                            HFC.DisplayAlert("Discount cannot be more than 100");
                            return;
                        }
                        else {
                            disc = dto.Discount;
                        } 
                    }
                    //if (disc == 0 && markup === 0) {
                    //    return;
                    //}
                    else if (disc === markup && dto.MarkUpFactor == dto.DiscountFactor) {
                        //$("#Model1").modal("show");
                        HFC.DisplayAlert("Discounts are applied to the Marked Up value.The Discount Value is equal to the entered Markup Value for this rule. This can result in a negative profit margin. Please review.");
                        //HFC.DisplayAlert("Markup and Discount cannot be same");
                        return;
                    }

                //API call to save or update the pricing
                //var loadingElement = document.getElementById("loading");
                //loadingElement.style.display = "block";
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
                $http.post('/api/PricingManagement/0/SaveAdvancedPricing', dto).then(function (response) {
                    
                    var saveStatus = response.data;
                    
                    if (response.data.error !== "Success") {
                        HFC.DisplayAlert(response.data.error);
                        $scope.EditMode = false;

                        $("#gridPricingManagement").data("kendoGrid").dataSource.read();
                    }
                    else {
                        //$scope.formadvancedpricing.$dirty = false;
                        HFC.DisplaySuccess("Pricing Saved Successfully!");
                        $scope.PricingSubmitted = false;
                        $scope.EditPricingSubmitted = false;
                        $scope.EditMode = false;
                        
                        $scope.editPrice = 0;
                        $scope.EditPricingSubmitted = false;
                        $("#gridPricingManagement").data("kendoGrid").dataSource.read();
                        if( $scope.SaveAndAddRow)
                        {
                            $scope.SaveAndAddRow = false;
                            $scope.addNewPricing();
                        }


                        // $scope.formadvancedpricing.$setPristine();
                        //$scope.removeNewPricing();
                    }
                    //loadingElement.style.display = "none";
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                    $scope.advancedpricing_form.$setPristine();
                }).catch(function(e)
                {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                });

            }
            else
                return false;
        }

        $scope.getProductCategoryDDL = function () {
            var geturl = '/api/PricingManagement/0/GetProductCategoryDropdown';
            $http.get(geturl).then(function (response) {

                

                if (response.data) {
                    AdvPricingService.ProductCategoryDDL = response.data;
                }
            });
        }
      

      
        //Kendo grid start
        $scope.GetAdvPricingManagement = function () {
            $scope.GridEditable = false;
            $scope.AdvancePricingManagement = {
                dataSource: {
                    transport: {
                        read: {
                            url: function (params) {

                                var url1 = '/api/PricingManagement/0/GetAdvancedPricing';
                                return url1;
                            },
                            dataType: "json"
                        }

                    },
                    error: function (e) {

                        HFC.DisplayAlert(e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                Id: { editable: false },
                                PricingStrategyType: { editable: true, validation: { required: true}},
                                ProductCategory: {editable: true, validation: { required: true } },
                                Vendor: { validation: { required: true } },
                                RetailBasis: { editable:false, validation: { required: true }},
                                MarkUp: { type: "number", defaultValue: null, validation: { min: 0 } },
                                MarkUpFactor: { type: "string" },
                                Discount: { type: "number", defaultValue: null },
                                DiscountFactor: { editable: false, type: "string"},

                            }
                        }
                    }
                },
                dataBound: function (e) {
                    
                    if (this.dataSource._data.length == 0) {
                        $scope.searchBoxEnable = true;
                    } else {
                        $scope.searchBoxEnable = false;
                    }
                },
                columns: [
                    {


                        field: "Id",
                        title: "Rule",
                        
                        filterable: { multi: true, search: true },
                        hidden: false,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Id + "</span>";
                        },

                    },
                    {
                        field: "PricingStrategyType",
                        title: "Pricing Strategy",
                        filterable: { multi: true, search: true },
                        hidden: false,
                        editor: function (container, options) {
                            $('<input id="PricingStrategyType"  required style=" "   name="' + options.field + '" data-bind="value:PricingStrategyTypeId"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    autoBind: false,
                                    optionLabel: "Select",
                                    value:"Select",
                                    valuePrimitive: true,
                                    dataTextField: "PricingStrategyType",
                                    dataValueField: "Id",
                                    template: "#=PricingStrategyType #",
                                    change: function (e) {
                                        var item = $('#gridPricingManagement').find('.k-grid-edit-row');
                                        item.data().uid
                                        var dataItem = item.data().$scope.dataItem;
                                        APMDropdownChange(dataItem);
                                    },
                                    select: function (e) {
                                        if (e.dataItem.PricingStrategyType === "Select") {
                                            $scope.PreventClose = true;
                                            e.preventDefault();
                                        }

                                    },
                                    close: function (e) {
                                        if ($scope.PreventClose == true)
                                            e.preventDefault();

                                        $scope.PreventClose = false;
                                    },
                                    dataSource: {
                                        transport: {
                                            read: {
                                                url: '/api/PricingManagement/0/GetPricingStrategyDropdown',
                                            }
                                        },
                                    }

                                });
                        }


                    },
                            {
                                field: "ProductCategory",
                                title: "Product Category",
                                filterable: { multi: true, search: true },
                                hidden: false,
                                editor: function (container, options) {
                                    $('<input id="ProductCategoryId" disabled required style=" " name="ProductCategoryId" data-bind="value:ProductCategoryId"/>')
                                        .appendTo(container)
                                        .kendoDropDownList({
                                            autoBind: false,
                                            optionLabel: "Select",
                                            valuePrimitive: true,
                                            dataTextField: "ProductCategory",
                                            dataValueField: "ProductCategoryId",
                                            template: "#=ProductCategory #",
                                            change: function (e) {
                                                var item = $('#gridPricingManagement').find('.k-grid-edit-row');
                                                item.data().uid
                                                var dataItem = item.data().$scope.dataItem;
                                                APMDropdownChange(dataItem);
                                            },
                                            dataSource: {
                                                transport: {
                                                    read: {
                                                        url: '/api/PricingManagement/0/GetProductCategoryDropdown',
                                                    }
                                                },
                                            }

                                        });
                                }

                            },
                             {
                                 field: "Vendor",
                                 title: "Vendor",
                                 filterable: { multi: true, search: true },
                                 hidden: false,
                                 editor: function (container, options) {
                                     $('<input id="Vendor" disabled required style=" " name="VendorId" data-bind="value:VendorId"/>')
                                         .appendTo(container)
                                         .kendoDropDownList({
                                             autoBind: false,
                                             optionLabel: "Select",
                                             valuePrimitive: true,
                                             dataTextField: "VendorName",
                                             dataValueField: "VendorId",
                                             template: "#=VendorName #",
                                             change: function (e) {
                                                 var item = $('#gridPricingManagement').find('.k-grid-edit-row');
                                                 item.data().uid
                                                 var dataItem = item.data().$scope.dataItem;
                                                 SetRetailBasis(dataItem);
                                             },
                                             dataSource: {
                                                 transport: {
                                                     read: {
                                                         url: function (params) {
                                                             if (options.model.ProductCategoryId) {
                                                                 var url1 = '/api/PricingManagement/0/GetVendorDropdown?ProductCategory=' + options.model.ProductCategoryId;
                                                                 return url1;
                                                             }
                                                             else {
                                                                 var url1 = '/api/PricingManagement/0/GetVendorDropdown';
                                                                 return url1;
                                                             }
                                                         },
                                                         dataType: "json",
                                                     }
                                                 },
                                             }

                                         });
                                 }
                             },
                             {
                                 field: "RetailBasis",
                                 title: "Retail Basis",
                                 hidden: false,
                                 filterable: { multi: true, search: true },
                             },
                            {
                                field: "MarkUp",
                                title: "MarkUp",
                                filterable: { multi: true, search: true },
                                hidden: false,
                                editor: '<input data-type="number" id="MarkUp" class="k-textbox" ng-keyup="onMarkupChange(dataItem)" name="MarkUp"/>',
                                //template: GetMarkUpColumn


                            },


                              {
                                  field: "MarkUpFactor",
                                  title: "Factor",
                                  filterable: { multi: true, search: true },
                                  editor: function (container, options) {
                                      $('<input id="markUpFactor"  style="width: 60%; " name="MarkUpFactor" data-bind="value:MarkUpFactor"/>')
                                        .appendTo(container)
                                        .kendoDropDownList({
                                         valuePrimitive: true,
                                         optionLabel: "Select",
                                         autoBind: true,
                                         dataTextField: "Value",
                                         dataValueField: "Key",
                                         template: "#=Value #",
                                         dataSource: {
                                         data:
                                          [{ Key: "%", Value: "%" }, { Key: "$", Value: "$" }]
                                         }
                                     });
                                  }

                              },
                              {
                                  field: "Discount",
                                  title: "Discount",
                                  filterable: { multi: true, search: true },
                                  hidden: false,
                                  editor: '<input data-type="number" id="Discount" class="k-textbox"  ng-keyup="onDiscountChange(dataItem)"   name="Discount"/>',
                                 
                                 // template: GetDiscountColumn


                              },

                               {
                                   field: "DiscountFactor",
                                   title: "Factor",
                                   filterable: { multi: true, search: true },

                               },
                                {
                                    field: "",
                                    title: "",
                                    hidden: false,
                                    editable: false,
                                    width: "80px",
                                    template: '<ul ng-show="Permission.EditAdvancedPricing" ><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="beginEditPricing(dataItem)">Edit</a></li>  <li><a  href="javascript:void(0)" ng-click="removeNewPricing(this)">Delete</a></li> </ul> </li> </ul>'
                                },

                ],
                editable: {
                    mode: "inline",
                    createAt: "bottom"
                
                } ,
                filterable: true,
                resizable: true,
                autoSync: true,
                noRecords: { template: "No records found" },
                scrollable: true,
                //toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search"><input ng-keyup="ShipgridSearchEdit()" type="search" id="searchBox" placeholder="Search" class="k-textbox leadsearch_tbox"><input type="button" id="btnReset" ng-click="ShipgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"><div class="pull-right"><button id="btnprocurement" type="button" onclick="return false;" ng-click="AddShipping()"><img src="/Content/images/add.png"><br>Add Pricing Rule</button></div></div></div></script>').html()) }],
                toolbar: [
                       {
                           template: kendo.template($("#headToolbarTemplate").html()),
                       }]
            };
            
        };
        //kendo grid end



        $scope.onMarkupChange = function (dataItem) {
            
            var markupVal = $.isNumeric($("#MarkUp").val());
            if (!markupVal) {
                $("#MarkUp").val('');
              
            }
            if (dataItem.MarkUp === null || dataItem.MarkUp === "" || dataItem.MarkUp === 0 ) {
                $("#markUpFactor").removeAttr('required');
            } else {
                $("#markUpFactor").attr('required', '');
            }
        }

        //if Discount has some string value
        $scope.onDiscountChange = function (dataItem) {
            
            var discountVal = $.isNumeric($("#Discount").val());
            if (!discountVal) {
                $("#Discount").val('');
                // $("#DiscountFactor").val('');
                dataItem.DiscountFactor = '';
            }
            else
            {
                dataItem.DiscountFactor = "%";
            }
        }

        //DropDown Conditional show and hide
        function APMDropdownChange(obj) {
            var PricingStrategyType = $("#PricingStrategyType").data("kendoDropDownList").select();
            var markUpFactor = $("#markUpFactor").data("kendoDropDownList");
            //Set the Retail basis value on change of parent dropdown
           
            var Vendor = $("#Vendor").data("kendoDropDownList");
            Vendor.value("0");
            Vendor.enable(false);

            var ProductCategoryId = $("#ProductCategoryId").data("kendoDropDownList");
            ProductCategoryId.enable(false);
            $('input[name="MarkUp"]').removeClass("k-invalid");
            
            if (PricingStrategyType == 0)
            {
                if (obj.ProductCategoryId) {
                    obj.ProductCategoryId = null;
                    ProductCategoryId.value("0");
                }
                if (obj.VendorId) {
                    obj.VendorId = null;
                    Vendor.value("0");
                }
            }
            else if(obj.PricingStrategyTypeId==1)
            {
                if (obj.ProductCategoryId){
                    obj.ProductCategoryId = null;
                    ProductCategoryId.value("0");
                }
                if (obj.VendorId) {
                    obj.VendorId = null;
                    Vendor.value("0");
                }
            }
            
           else if (obj.PricingStrategyTypeId == 2) {
               // $('input[name="MarkUp"]').prop('required', true);
               $("#Vendor").prop('required', true);
                if (obj.ProductCategoryId)
                {
                    obj.ProductCategoryId = null;
                    ProductCategoryId.value("0");
                }
                Vendor.enable(true);
                Vendor.dataSource.read();
            } 
            else if (obj.PricingStrategyTypeId == 3) {
              //  $('input[name="MarkUp"]').prop('required', true);
                ProductCategoryId.enable(true);
                $("#Vendor").prop('required', false);
                if(obj.ProductCategoryId)
                {
                    Vendor.enable(true);
                    Vendor.dataSource.read();
                }
            }
            SetRetailBasis(obj);
        }

        // Set the Value of retail basis on change of Vendor LOV
        function SetRetailBasis(obj)
        {
            
            var Vendor = $("#Vendor").data("kendoDropDownList");
            
            if (obj.VendorId) {
                var vendorLength = Vendor.dataSource._data.length;
                for (var i = 0; i < vendorLength; i++) {
                    if (Vendor.dataSource._data[i].VendorId === obj.VendorId) {
                       // $('input[name="RetailBasis"]').val(Vendor.dataSource._data[i].RetailBasis);
                       //var retailbasis = $("#RetailBasis").val(Vendor.dataSource._data[i].RetailBasis);
                       //console.log(retailbasis);
                        obj.RetailBasis = Vendor.dataSource._data[i].RetailBasis;
                        
                    }

                }
            }
            else
                obj.RetailBasis = "";
        }
        //Search Grid
        $scope.AdvPricingGridSearch = function () {
            var validator = $scope.kendoValidator("gridPricingManagement");
            if ($scope.searchBoxEnable)
                $('#searchBox').val('');
            if (!validator.validate()) {
                $('#searchBox').val('');
               
                return false;
            }
           
                var searchValue = $('#searchBox').val();
                
                $("#gridPricingManagement").data("kendoGrid").dataSource.filter({

                        logic: "or",
                        filters: [
                            
                            {
                                field: "PricingStrategyType",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "ProductCategory",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "Vendor",
                                operator: "contains",
                                value: searchValue

                            },

                            {
                                field: "RetailBasis",
                                operator: "contains",
                                value: searchValue
                            }
                        ]
                    });
                
               

        };
        $scope.tooltipOptions = {
            filter: "td,th",
            position: "top",
            hide: function (e) {
                this.content.parent().css('visibility', 'hidden');
            },
            show: function (e) {
                if (this.content.text().length > 1) {
                    if (this.content.text().trim() === "Edit QualifyAppointment Print") {
                        this.content.parent().css('visibility', 'hidden');
                    } else {
                        this.content.parent().css('visibility', 'visible');
                    }

                }
                else {
                    this.content.parent().css('visibility', 'hidden');
                }
            },
            content: function (e) {

                return e.target.context.textContent;
            }
        };

        //Clear Search and Filters
        $scope.AdvPricingSearchClear = function () {
            var rowEdit = $('#gridPricingManagement').find('.k-grid-edit-row');
            if (rowEdit.length) {
                //grid is not in edit mode
                $('#gridPricingManagement').data("kendoGrid").cancelChanges();
            };
            $('#searchBox').val('');
            $("#gridPricingManagement").data('kendoGrid').dataSource.filter({
            });
            $scope.EditMode = false;
            
        };

        $scope.beginEditPricing = function (obj) {
            var validator = $scope.kendoValidator("gridPricingManagement");
            var grid = $("#gridPricingManagement").data("kendoGrid");

            var item = $('#gridPricingManagement').find('.k-grid-edit-row');
            if (!validator.validate()) {
                return false;
            }
          
            var dataSource = grid.dataSource;
            var index = dataSource._data.length;
            if (dataSource._data[index - 1].Id == '' || dataSource._data[index - 1].Id == null)
            {
                $scope.saveAdvPricing();
            }
            $scope.editedPriceValue = obj;
            $scope.EditMode = true;
            obj.DiscountFactor = "%";
           
            if (obj) {
                grid.refresh();
                grid.editRow(obj);
            };
          
            var PricingStrategyType = $("#PricingStrategyType").data("kendoDropDownList");

            var Vendor = $("#Vendor").data("kendoDropDownList");
            Vendor.enable(false);
            var ProductCategoryId = $("#ProductCategoryId").data("kendoDropDownList");
            ProductCategoryId.enable(false);
            if (obj.PricingStrategyTypeId == 1) {
                if (obj.ProductCategoryId) {
                    obj.ProductCategoryId = null;
                    ProductCategoryId.value("-1");
                }
                if (obj.VendorId) {
                    obj.VendorId = null;
                    Vendor.value("-1");
                }
            }

            else if (obj.PricingStrategyTypeId == 2) {
                $('input[name="MarkUp"]').prop('required', true);

                if (obj.ProductCategoryId) {
                    obj.ProductCategoryId = null;
                    ProductCategoryId.value("-1");
                }
                Vendor.enable(true);
                Vendor.dataSource.read();
            }
            else if (obj.PricingStrategyTypeId == 3) {
                $('input[name="MarkUp"]').prop('required', true);
                ProductCategoryId.enable(true);

                if (obj.ProductCategoryId) {

                    Vendor.enable(true);
                    Vendor.dataSource.read();
                }
            }
            
         
           
           
                
        };

        $scope.cancelEditPricing = function () {
            
            if (confirm('You will lose unsaved changes if you leave this page')) {
                $('#gridPricingManagement').data("kendoGrid").cancelChanges();
                $scope.EditMode = false;
                $scope.editedPriceValue = undefined;

                $scope.advancedpricing_form.$setPristine();
            }
        };

       

       

        //$scope.cancel = function () {
        //    $scope.formadvancedpricing.$dirty = false;
        //    window.location.href = '#!/pricingSettings';
        //}

        $scope.deletePricing = function (Id) {
            if ($scope.editedPriceValue != undefined)
                $scope.editedPriceValue == undefined;
            var deleteurl = '/api/PricingManagement/' + Id;
            $http.delete(deleteurl).then(function (response) {
                var res = response.data;
                if (!res) {
                    HFC.DisplayAlert(saveStatus);
                }
                else {
                    $scope.formadvancedpricing.$dirty = false;
                    $scope.GetAdvPricingManagement();
                }
            });
        }
        $scope.clearFields = function () {
            $scope.NewPrice.ProductCategoryId = '';
            $scope.NewPrice.VendorId = '';
            $scope.NewPrice.Discount = '';
            $scope.NewPrice.MarkUp = '';
            $scope.NewPrice.MarkUpFactor = '%';
        }
        $scope.clearBasedonPricingStrategy = function (Item) {
            if (Item) {
                if (Item != null && Item.PricingStrategyTypeId == 1) {
                    Item.ProductCategoryId = '';
                    Item.VendorId = '';
                    Item.Discount = '';
                }
                if (Item != null && Item.PricingStrategyTypeId == 2) {
                    Item.ProductCategoryId = '';

                }
            }
        }

        $scope.ShowHelpPopup = function () {
            $("#helppopup").modal("show");
            return;
        }

        $scope.GetAdvPricingManagement();
       

        $scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                validate: function (e) {
                    $("span.k-invalid-msg").hide();
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find(".k-widget.k-dropdown.k-header");
                        if (input.size() > 0) {
                            $(this).addClass("dropdown-validation-error");
                        } else {
                            $(this).removeClass("dropdown-validation-error");
                        }
                    });
                }
            }).getKendoValidator();
        }
        function GetMarkUpColumn(dataItem) {

            var val = '';
            if (dataItem.MarkUp !== null && dataItem.MarkUp !== undefined) {
                val = dataItem.MarkUp;
            }
            return val;

        }
        function GetDiscountColumn(dataItem) {

            var val = '';
            if (dataItem.Discount !== null && dataItem.Discount !== undefined) {
                val = dataItem.Discount;
            }
            return val;

        }
        
   
    }
    ]);
