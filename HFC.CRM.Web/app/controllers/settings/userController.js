﻿var app = angular.module('HFC_UserApp', [])
    .service('UserService', ['$http', '$location', 'HFCService', function ($http, $location, HFCService) {
        var srv = this;

        srv.Permission = {};
        var SecurityUsersAndPermissionspermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'SecurityUsersAndPermissions')
        if (SecurityUsersAndPermissionspermission == undefined)
            SecurityUsersAndPermissionspermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Security')
        srv.Permission.AddNewUser = SecurityUsersAndPermissionspermission.CanCreate; //HFCService.GetPermissionTP('Add Security').CanAccess;
        srv.Permission.EditNewUser = SecurityUsersAndPermissionspermission.CanUpdate; //HFCService.GetPermissionTP('Edit Security').CanAccess;


        srv.RolesList = {
            RoleId: '',
            RoleName: '',
            Selected: false,
            Disabled: false
        }


        srv.PersonModel = {
            PersonId: 0,
            FirstName: '',
            LastName: '',
            CompanyName: '',
            PreferredTFN: '',
            HomePhone: '',
            CellPhone: '',
            WorkTitle: '',
            WorkPhone: '',
            WorkPhoneExt: '',
            FaxPhone: '',
            PrimaryEmail: '',
            IsReceiveEmails: '',
            EnableEmailSignature: false,
            AddEmailSignAllEmails: false
        }

        srv.AddressModel = {
            AddressId: 0,
            AttentionText: '',
            IsResidential: false,
            Address1: '',
            Address2: '',
            City: '',
            State: '',
            ZipCode: '',
            CrossStreet: '',
            CountryCode2Digits: 'US',
            IsValidated: ''
        };

        srv.NewUserModel = {
            CalSync: false,
            UserName: '',
            FirstName: '',
            LastName: '',
            HomePhone: '',
            CellPhone: '',
            PreferredTFN: '',
            ZipCode: '',
            City: '',
            State: '',
            CountryCode2Digits: 'US',
            IsResidential: true,
            Calendar1sthr: '6',
            Comments: '',
            BGColor: 'rgb(194, 194, 194)',
            FGColor: 'rgb(0, 0, 0)',
            RoleIds: {}
        }


        srv.EditUserModel = {
            FranchiseId: 0,
            PersonId: 0,
            CalSync: false,
            UserName: '',
            CreationDateUtc: '',
            SyncStartsOnUtc: '',
            LastLoginDateUtc: '',
            IsADUser: true,
            AddressType: '',
            IsLockedOut: false,
            IsDisabled: false,

            CalendarFirstHour: '',
            Comments: '',
            BGColor: '',
            FGColor: '',
            //Roles: srv.RolesList, Is this needed???
            Person: srv.PersonModel,
            Address: srv.AddressModel,
            RoleIds: {}
        }



        function FormatContactNumber(text) {
            if (text != '' && text != undefined && text != null) {
                return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
            } else
                return '';

        }

        function MainGridContactTemplate(dataItem) {
            var Div = '<address>';
            if (dataItem.Person.HomePhone && dataItem.Person.HomePhone != null)
                Div += '<div><abbr title="Home phone">H:</abbr>' + FormatContactNumber(dataItem.Person.HomePhone) + '</div>';

            if (dataItem.Person.CellPhone && dataItem.Person.CellPhone != null)
                Div += '<div><abbr title="Cell phone">C:</abbr>' + FormatContactNumber(dataItem.Person.CellPhone) + '</div>';

            if (dataItem.Person.WorkPhone && dataItem.Person.WorkPhone != null)
                Div += '<div><abbr title="Work phone">W:</abbr>' + FormatContactNumber(dataItem.Person.WorkPhone);
            if (dataItem.Person.WorkPhoneExt && dataItem.Person.WorkPhoneExt != null)
                Div += '<text>x</text>' + dataItem.Person.WorkPhoneExt + '</div>';

            Div += '</address>';


            return Div;
        }

        function MainGridButtonTemplate(dataItem) {
            var ValueEdit = srv.Permission.EditNewUser;
            var value1 = "";
            var value2 = "";
            var value3 = "";

            if (dataItem.IsDisabled == null && ValueEdit == true) {
                value1 = '<li><a href="#!/edituser/' + dataItem.UserId + '">Edit</a> </li> <li><a href="javascript:void(0)" ng-click="DeleteUserInfo(\'' + dataItem.UserId + '\')">Disable</a></li>';
            }
            if (dataItem.IsDisabled != null && dataItem.IsDisabled == false && ValueEdit == true) {
                value2 = '<li><a href="#!/edituser/' + dataItem.UserId + '">Edit</a> </li> <li><a href="javascript:void(0)" ng-click="DeleteUserInfo(\'' + dataItem.UserId + '\')">Disable</a></li>';
            }
            else if ((dataItem.IsDisabled != null || dataItem.IsDisabled == true) && ValueEdit == true) {
                value3 = '<li><a href="javascript:void(0)" ng-click="EnableUserInfo(\'' + dataItem.UserId + '\')">Enable</a></li>';
            }
            return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                '<ul class="dropdown-menu pull-right">' + value1 + value2 + value3 + '</ul> </li> </ul>';
        }





        srv.UserGridOptions = {
            cache: false,
            //dataSource: ds,
            dataSource: {
                transport: {
                    read: {
                        url: '/api/users/0/GetUsers',
                        dataType: "json"
                    }

                },
                schema: {
                    model: {
                        fields: {
                            IsDisabled: { type: "boolean" },
                        }
                    }
                },
                error: function (e) {

                    HFC.DisplayAlert(e.errorThrown);
                },
                pageSize: 25,
                change: function (e) {
                    var myDiv = $('.k-grid-content.k-auto-scrollable');
                    myDiv[0].scrollTop = 0;
                }
            },

            resizable: true,
            columns: [
                {
                    field: "Person.FullName",
                    title: "Name",
                    filterable: { search: true },
                    template: "<a href='\\\\#!/edituser/#= UserId #' style='color: rgb(61,125,139);'><span>#= Person.FullName #</span></a>",

                },
                {
                    field: "Person.WorkTitle",
                    title: "Title",
                    filterable: { search: true }
                },
                {
                    //field: "UserName",
                    field: "Email",
                    title: "Primary Email",
                    filterable: { search: true }
                },
                {
                    field: "",
                    title: "Contact Info",
                    template: function (dataItem) {
                        return MainGridContactTemplate(dataItem);
                    }
                },
                //{
                //    field: "IsDisabled",
                //    title: "Is Disabled",
                //    template: "\#= IsDisabled ? 'False' : 'True'\#",
                //    filterable: { multi: true, search: true, dataSource: ds }
                //},
                {
                    field: "IsDisabled",
                    title: "Is Disabled",
                    hidden: false,
                    //width: "100px",
                    template: '<input type="checkbox" #= IsDisabled ? "checked=checked" : "" # disabled="disabled" class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched"></input>',
                    filterable: { messages: { isTrue: "True", isFalse: "False" } }
                },
                {
                    field: "",
                    title: "",
                    template: function (dataItem) {
                        return MainGridButtonTemplate(dataItem);
                    }
                }
            ],
            noRecords: { template: "No records found" },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isempty: "Empty",
                        isnotempty: "Not empty"


                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gte: "Greater than or equal to",
                        lte: "Less than or equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                change: function (e) {
                    var myDiv = $('.k-grid-content.k-auto-scrollable');
                    myDiv[0].scrollTop = 0;
                }
            },
            sortable: ({ field: "Name", dir: "desc" }),
            toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="row no-margin vendor_search"><div class="leadsearch_topleft no-padding row no-margin col-sm-9 col-md-9 col-xs-9"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="UserService.UserGridSearch()" type="search" id="searchBox" placeholder="Search Users" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="UserService.UserGridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="col-sm-3 col-xs-3 no-padding"><div class="leadsearch_icon"><div class="plus_but tooltip-bottom" data-tooltip="Add User" ng-if="UserService.Permission.AddNewUser" ng-click="AddNewUser()"><i class="far fa-user-plus"></i></div></div></div></div></div></script>').html())
            //toolbar: kendo.template($("#kendo-header-template-user").html()),
        };


        srv.UserGridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#gridUsers").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                    {
                        field: "Person.FullName",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "Person.WorkTitle",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "Email",
                        operator: "contains",
                        value: searchValue
                    },

                ]
            });

        }

        srv.UserGridSearchClear = function () {
            $('#searchBox').val('');
            $("#gridUsers").data('kendoGrid').dataSource.filter({
            });
        }
        //};

    }])
    // The following filters not used anywhere --murugan
    //.filter('fromNow', function () {
    //    return function (dateString) {
    //        return moment(dateString).fromNow()
    //    };
    //})
    .directive('colorPicker', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.ColorPickerSliders({
                    // initialize the color to the color on the scope
                    flat: false,
                    order: {
                        hsl: 1,
                        opacity: 2
                    },
                    color: '#c2c2c2',
                    // update the ngModel whenever we pick a new color
                    onchange: function (id, newValue) {
                        scope.$apply(function () {
                            ngModel.$setViewValue(newValue.tiny.toRgbString());
                        });
                        var span = $("#previewhsl");

                        span.css("background-color", newValue.tiny.toRgbString());


                    }
                });

                // update the color picker whenever the value on the scope changes
                ngModel.$render = function () {
                    element.val(ngModel.$modelValue);
                    element.change();
                };
            }
        }
    })
    .directive('colorPickerNonSugg', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.ColorPickerSliders({
                    // initialize the color to the color on the scope
                    flat: false,
                    swatches: false,
                    order: {
                        hsl: 1
                    },
                    color: '#000',
                    // update the ngModel whenever we pick a new color
                    onchange: function (id, newValue) {
                        scope.$apply(function () {
                            ngModel.$setViewValue(newValue.tiny.toRgbString());
                        });
                        var span = $("#previewhsl");

                        span.css("color", newValue.tiny.toRgbString());


                    }
                });

                // update the color picker whenever the value on the scope changes
                ngModel.$render = function () {
                    element.val(ngModel.$modelValue);
                    element.change();
                };
            }
        }
    })
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {

                var updateModel = function () {
                    scope.$apply(function () {
                        ngModelCtrl.$modelValue = elem.val();
                    });
                };
                if (attrs.value) {
                    elem.datepicker('setDate', new Date(attrs.value));
                }
                elem.datepicker({
                    //useCurrent: false,
                    format: 'mm-dd-yyyy',
                });
                elem.on("change", function (e) {
                    updateModel();
                });
            }
        }
    })
    .controller('userController', [
        '$http', '$scope', '$window', '$routeParams', '$location', '$filter', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', 'HFCService', 'AddressService'
        , 'UserService', 'kendoService', 'AddressValidationService', '$q',
        'NavbarService', 'NavbarServiceCP',
        function ($http, $scope, $window, $routeParams, $location, $filter, $rootScope
            , $templateCache, $sce, $compile, $http, HFCService, AddressService
            , UserService, kendoService, AddressValidationService, $q,
            NavbarService, NavbarServiceCP) {

            $scope.HFCService = HFCService;
            $scope.listroles = {};
            $scope.UserService = UserService;
            $scope.AddressService = AddressService;
            $scope.NewUserModel = angular.copy(UserService.NewUserModel);
            $scope.EditUserModel = angular.copy(UserService.EditUserModel);


            $scope.NavbarService = NavbarService;
            $scope.NavbarServiceCP = NavbarServiceCP;



            // for kendo tooltip
            $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
            kendoService.customTooltip = null;
            $scope.adminFlag = false;

            $scope.AddressValidationService = AddressValidationService;
            var url = window.location.href;
            var SecurityUsersAndPermissionspermission;
            if (url.toLowerCase().includes("controlpanel")) {
                $scope.adminFlag = true;
                SecurityUsersAndPermissionspermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Security');
                $scope.NavbarServiceCP.GlobalPermissionSets();
            }
            else {
                SecurityUsersAndPermissionspermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'SecurityUsersAndPermissions')
                $scope.adminFlag = false;

                $scope.NavbarService.SelectSettings();
                $scope.NavbarService.EnableSettingsSecurityTab();
            }
            $scope.Permission = {};
            //var SecurityUsersAndPermissionspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SecurityUsersAndPermissions')
            //if (SecurityUsersAndPermissionspermission == undefined) {
            //    SecurityUsersAndPermissionspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Security');
            //    //$scope.adminFlag = true;
            //}

            //$scope.IsLockedOutpermission = SecurityUsersAndPermissionspermission.SpecialPermission.find(x=>x.PermissionCode == 'IsLockedOut').CanAccess;
            if (SecurityUsersAndPermissionspermission) {
                $scope.Permission.ListUsers = SecurityUsersAndPermissionspermission.CanRead; //$scope.HFCService.GetPermissionTP('List Security').CanAccess;
                $scope.Permission.AddUser = SecurityUsersAndPermissionspermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Security').CanAccess;
                $scope.Permission.EditUser = SecurityUsersAndPermissionspermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Security').CanAccess;
            }

            HFCService.setHeaderTitle("User #");

            $scope.AddressType = null;
            $scope.URLCallBack = window.location.href;
            $scope.URLCallBack = window.location.href.split('#!');
            $scope.URLCallBack = $scope.URLCallBack[1].split('/');


            $scope.UserId = $routeParams.userid;

            $scope.submitted = false;


            $scope.roleList = angular.copy(HFCService.RolesTP);
            // $scope.roleList = $filter('filter')(tempRoles, { RoleId: '!D6BAAB7B-D94F-41D3-88E9-601FD1C27398' });
            // $scope.roleList = $filter('filter')($scope.roleList, { RoleId: '!911DE115-8F4F-48C4-BB97-633B27516865' });


            for (i = 0; i < $scope.roleList.length; i++) {

                //if ($scope.roleList[i].RoleName == "User")
                if ($scope.roleList[i].DisplayName == "User") {
                    $scope.roleList[i].Selected = true;
                    $scope.roleList[i].Disabled = true;
                } else {
                    ////// TP-2323: Don't allow a User to remove User Edit capability from themselves
                    ////// When the mode is edit and logged in user and edit userid are same
                    ////// we need disable all the roles.
                    ////// TODO: what about Permision sets???
                    ////
                    ////var roleTemp = $scope.roleList[i];
                    ////if ($scope.UserId && $scope.UserId == HFCService.CurrentUser.UserId && 
                    ////    1) {
                    ////    roleTemp.Disabled = true;
                    ////}
                }
            }

            $http.get('../api/Timezones/0/Get').then(function (response) {
                $scope.TimeZoneCodeList = response.data;
            }, function (response) {
                $scope.TimeZoneCodeList = {};
            });

            $http.get('/api/Permission/0/GetPermissionSetsList').then(function (data) {

                $scope.PermissionSetList = data.data;

                ////// TP-2323: Don't allow a User to remove User Edit capability from themselves
                ////// When the mode is edit and logged in user and edit userid are same
                ////// we need disable all the roles.
                ////for (i = 0; i < $scope.PermissionSetList.length; i++) {
                ////    var psTemp = $scope.PermissionSetList[i];
                ////    if ($scope.UserId && $scope.UserId == HFCService.CurrentUser.UserId) {
                ////        psTemp.Disabled = true;
                ////    }
                ////}
            });

            function setAddress1Value(value) {
                if (!value || value == "") {
                    // We need to initialize to empty string so that the previous value will
                    // not be used the special autofill control.
                    $rootScope.$broadcast('angucomplete-alt:clearInput', 'autocomp_');
                    return;
                }

                // Need to set the initial value for the address one, because it is an 
                // autocomplete component needs a spcial handling.
                $rootScope.$broadcast('angucomplete-alt:changeInput', 'autocomp_'
                    , value);
            }
            //code for spinning wheel
            function errorHandler(reseponse) {


                // Inform that the modal is ready to consume:
                $scope.modalState.loaded = true;
                HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            }

            $scope.modalState = {
                loaded: true
            }

            $scope.$watch('modalState.loaded', function () {
                var loadingElement = document.getElementById("loading");

                if (!$scope.modalState.loaded) {
                    loadingElement.style.display = "block";
                } else {
                    loadingElement.style.display = "none";
                }
            });
            //-----

            const RoleOwner = "B77161AC-2FF6-4672-A3D4-5008061FAA34";
            const RoleFranchiseAdmin = "A725282F-E606-4353-BB53-693F695D42A5";

            var isInRole = function (roleList, roleId) {
                var result = $.grep(roleList, function (e) {
                    return e.RoleId.toUpperCase() == roleId.toUpperCase();
                });

                if (result && result.length > 0) return true;

                // The expected role is not in the list.
                return false;
            }

            var disableRole = function (roleList, roleId) {
                var result = $.grep(roleList, function (e) {
                    return e.RoleId.toUpperCase() == roleId.toUpperCase();
                });

                if (result && result.length > 0) result[0].Disabled = true;
            }



            $scope.GetUserDetails = function (ID) {
                $http.get('/api/Users?userid=' + ID).then(function (response) {



                    $scope.EditUserModel = response.data;
                    $scope.EditUserModel.Comments = $scope.EditUserModel.Comment;
                    if (!$scope.adminFlag) {
                        if ($scope.EditUserModel.Address != null) {
                            if ($scope.EditUserModel.Address.IsResidential == true) {
                                $scope.EditUserModel.Address.IsBusiness = true;
                            }
                            else $scope.EditUserModel.Address.IsBusiness = false;
                        }

                        if ($scope.EditUserModel.PermissionSets) {
                            for (i = 0; i < $scope.EditUserModel.PermissionSets.length; i++) {
                                var result = $.grep($scope.PermissionSetList, function (e) {
                                    return e.Id == $scope.EditUserModel.PermissionSets[i];
                                });
                                if (result && result.length > 0) {
                                    result[0].Selected = true;
                                }
                            }
                        }
                    }

                    if ($scope.EditUserModel.CalendarFirstHour != null) {

                        $scope.EditUserModel.CalendarFirstHour = $scope.EditUserModel.CalendarFirstHour.toString();
                    }
                    if ($scope.EditUserModel.SyncStartsOnUtc) {

                        $scope.EditUserModel.SyncStartsOnUtc = $filter('date')(new Date($scope.EditUserModel.SyncStartsOnUtc), 'MM-dd-yyyy');
                    }

                    if ($scope.EditUserModel.PersonId) {
                        var username = $scope.EditUserModel.UserName
                        $scope.EditUserModel.UserName = username;
                        //$scope.EditUserModel.UserName = username.substring(0, username.indexOf("@"))
                    }

                    //var rolearray = $scope.EditUserModel.Roles;
                    //for (i = 0; i < rolearray.length; i++) {
                    //    //var selected = $.grep(UserService.RolesList, function (e) { return e.RoleId == rolearray[i].RoleId; });
                    //    if (selected && selected != null) {
                    //        selected[0].Selected = true;
                    //    }
                    //}

                    var userRoles = $scope.EditUserModel.Roles;

                    if (!$scope.roleList || $scope.roleList.length == 0) {

                        alert("Role list is empty, Please try again");
                    }

                    for (i = 0; i < userRoles.length; i++) {
                        var result = $.grep($scope.roleList, function (e) {
                            return e.RoleId == userRoles[i].RoleId;
                        });
                        if (result && result.length > 0) {
                            result[0].Selected = true;
                        }
                    }

                    // TP-2323: Don't allow a User to remove User Edit capability from themselves
                    // When the mode is edit and logged in user and edit userid are same
                    // we need disable all the roles.

                    // var roleIdTemp = result[0];
                    if (HFCService.CurrentUser != null) {


                        if (ID == HFCService.CurrentUser.UserId) {
                            if (isInRole(userRoles, RoleOwner)) {
                                disableRole($scope.roleList, RoleOwner);
                            }
                            else if (isInRole(userRoles, RoleFranchiseAdmin)) {
                                disableRole($scope.roleList, RoleOwner);
                                disableRole($scope.roleList, RoleFranchiseAdmin);
                            }
                        } else {
                            var currentuserrole = HFCService.CurrentUser.Roles;
                            userRoles = currentuserrole;
                            if (isInRole(userRoles, RoleOwner)) {
                                // He can able to edit any roles in the application
                            }
                            else if (isInRole(userRoles, RoleFranchiseAdmin)) {
                                disableRole($scope.roleList, RoleOwner);
                            }
                            else {
                                // For safe purpose... The user is not able to edit any role.
                                angular.forEach(userRoles, function (roleobj, key) {
                                    roleobj.Disabled = true;
                                });
                            }
                        }
                    }

                    if (!$scope.adminFlag)
                        setAddress1Value($scope.EditUserModel.Address.Address1);

                    if ($scope.EditUserModel.ColorType) {
                        if ($scope.EditUserModel.ColorType.Alpha == 1) {
                            $scope.EditUserModel.BGColor = "rgb(" + $scope.EditUserModel.ColorType.Red + "," + $scope.EditUserModel.ColorType.Green + "," + $scope.EditUserModel.ColorType.Blue + ")";
                        }
                        else {
                            $scope.EditUserModel.BGColor = "rgba(" + $scope.EditUserModel.ColorType.Red + "," + $scope.EditUserModel.ColorType.Green + "," + $scope.EditUserModel.ColorType.Blue + "," + $scope.EditUserModel.ColorType.Alpha + ")";
                        }
                        if ($scope.EditUserModel.ColorType.FGAlpha == 1) {
                            $scope.EditUserModel.FGColor = "rgb(" + $scope.EditUserModel.ColorType.FGRed + "," + $scope.EditUserModel.ColorType.FGGreen + "," + $scope.EditUserModel.ColorType.FGBlue + ")";
                        }
                        else {
                            $scope.EditUserModel.FGColor = "rgba(" + $scope.EditUserModel.ColorType.FGRed + "," + $scope.EditUserModel.ColorType.FGGreen + "," + $scope.EditUserModel.ColorType.FGBlue + "," + $scope.EditUserModel.ColorType.FGAlpha + ")";
                        }

                        var span = $("#previewhsl");
                        span.css("color", $scope.EditUserModel.FGColor);
                        span.css("background-color", $scope.EditUserModel.BGColor);
                    }
                    HFCService.setHeaderTitle("User #" + " " + $scope.EditUserModel.Person.FullName);
                });
            }



            //This is not required
            //$scope.RolesList = UserService.RolesList;

            $scope.AddNewUser = function () {
                window.location.href = '#!/newuser';
            };

            $scope.Cancel = function () {
                window.location.href = '#!/users';
            };

            $scope.DeleteUserInfo = function (userid) {

                $http.delete('/api/users/' + userid).then(function (res) {
                    if (res.data) {
                        HFC.DisplaySuccess("User deleted sucessfully!");
                        $('#gridUsers').data('kendoGrid').dataSource.read();
                        $('#gridUsers').data('kendoGrid').refresh();
                    }
                    else {
                        HFC.DisplayAlert("Unable to delete!");
                        $('#gridUsers').data('kendoGrid').dataSource.read();
                        $('#gridUsers').data('kendoGrid').refresh();
                    }
                });
            }

            $scope.EnableUserInfo = function (userid) {
                $http.get('/api/users/' + userid + '/Enable/').then(function (res) {

                    if (res.data == true) {
                        HFC.DisplaySuccess("User enabled sucessfully!");
                        $('#gridUsers').data('kendoGrid').dataSource.read();
                        $('#gridUsers').data('kendoGrid').refresh();
                    }
                    else {
                        HFC.DisplayAlert("Unable to delete!");
                        $('#gridUsers').data('kendoGrid').dataSource.read();
                        $('#gridUsers').data('kendoGrid').refresh();
                    }
                });
            }

            $scope.SaveUser = function () {

                $scope.submitted = true;

                var dto = $scope.NewUserModel;


                // Check whether the forms contain value for all the required fields.
                // when any one of the filed is not given a value the following will return true.
                if ($scope.formnewuser.$invalid) {
                    // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
                    return;
                }

                if (!$scope.adminFlag) {
                    var permissionSetArray = [];
                    for (i = 0; i < $scope.PermissionSetList.length; i++) {
                        if ($scope.PermissionSetList[i].Selected) {
                            permissionSetArray.push($scope.PermissionSetList[i].Id);
                        }
                    }
                    dto.PermissionSetIds = permissionSetArray;
                }
                var rolearray = [];
                for (i = 0; i < $scope.roleList.length; i++) {
                    if ($scope.roleList[i].Selected) {
                        rolearray.push($scope.roleList[i].RoleId);
                    }
                }

                dto.RoleIds = rolearray;

                if (!$scope.adminFlag && dto.ZipCode) {
                    $http.get('/api/lookup/0/ZipCodesforFranchise?q=' + dto.ZipCode + "&country=" + dto.CountryCode2Digits).then(function (response) {

                        if (response.data.zipcodes.length > 0) {
                            dto.City = response.data.zipcodes[0].City;
                            dto = response.data.zipcodes[0].State;
                        } else {
                            $scope.EditUserModel.Address.City = '';
                            $scope.EditUserModel.Address.State = '';
                        }
                    });
                }

                if (!$scope.adminFlag && dto.CountryCode2Digits == "CA") {
                    if (dto.ZipCode) {
                        var Ca_Zipcode = dto.ZipCode;
                        if (Ca_Zipcode.contains(" ")) {
                            dto.ZipCode = Ca_Zipcode;
                        }
                        else {
                            Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                            dto.ZipCode = Ca_Zipcode;
                        }
                    }
                }
                $scope.modalState.loaded = false;
                dto.Comment = dto.Comments;
                $http.post('/api/users', dto).then(function (response) {

                    //Reload Userinfo
                    HFCService.GetUserInfo();

                    if (response.data.UserId == undefined) {
                        HFC.DisplayAlert(response.data);
                        $scope.modalState.loaded = true;
                    }
                    else {
                        var result = response.data.UserId;
                        if (result != "00000000-0000-0000-0000-000000000000" && result != null) {
                            $scope.NewUserModel = angular.copy(UserService.NewUserModel);
                            HFC.DisplaySuccess("User saved!");
                            $scope.formnewuser.$setPristine();
                            window.location.href = "#!/users";
                            $scope.modalState.loaded = true;
                        }
                        else {
                            HFC.DisplayAlert(response.data.status);
                            $scope.modalState.loaded = true;
                        }
                    }

                    //}, function (response) {
                    //    HFC.DisplayAlert(response.data.status);
                });
            };



            $scope.EditUser = function () {

                $scope.submitted = true;
                // Check whether the forms contain value for all the required fields.
                // when any one of the filed is not given a value the following will return true.

                if (!$scope.adminFlag)
                    $scope.formedituser.formAddressValidation.zipCode.$commitViewValue(true);
                if ($scope.formedituser.$invalid) {
                    // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
                    return;
                }

                //No longer used - validation
                if ($scope.EditUserModel.Person.EnableEmailSignature == false && $scope.EditUserModel.Person.AddEmailSignAllEmails == true) {
                    var alerttext = "Enable Email Signature must be selected when selecting Add Email Signature on all Emails. ";
                    HFC.DisplayAlert(alerttext);
                    return;
                }

                if ($scope.EditUserModel.Person.EnableEmailSignature) {
                    var alerttext = "";
                    var alerttextphone = "";
                    if ($scope.EditUserModel.Person.WorkTitle == null || $scope.EditUserModel.Person.WorkTitle == "")
                        alerttext = alerttext + "Please enter your Title, it is required to enable email signatures.";

                    if ($scope.EditUserModel.Person.CellPhone == null || $scope.EditUserModel.Person.CellPhone == "")
                        alerttextphone = alerttextphone + "Please enter your cell phone number if you would like it to be used in your email signature."

                    if (alerttext != "") {
                        HFC.DisplayAlert(alerttext);
                        return;
                    }
                    if (alerttextphone != "") {
                        if (confirm(alerttextphone)) {
                            // If confirm then rest of the code run, if cancel then stop save
                        } else {
                            return;
                        }
                    }

                }

                var dto = $scope.EditUserModel;

                var rolearray = [];
                //for (i = 0; i < UserService.RolesList.length; i++) {
                //    if (UserService.RolesList[i].Selected) {
                //        rolearray.push(UserService.RolesList[i].RoleId);
                //    }
                //}
                if (!$scope.adminFlag) {
                    var permissionSetArray = [];
                    for (i = 0; i < $scope.PermissionSetList.length; i++) {
                        if ($scope.PermissionSetList[i].Selected) {
                            permissionSetArray.push($scope.PermissionSetList[i].Id);
                        }
                    }
                    dto.PermissionSetIds = permissionSetArray;
                }

                for (i = 0; i < $scope.roleList.length; i++) {
                    if ($scope.roleList[i].Selected) {
                        rolearray.push($scope.roleList[i].RoleId);
                    }
                }

                dto.RoleIds = rolearray;

                dto.Comment = dto.Comments;
                if (!$scope.adminFlag)
                    saveUserHandler(dto);
                else
                    AfterValidateAddress(dto)
            };

            function AfterValidateAddress(dto) {
                $scope.modalState.loaded = false;
                //

                if (!$scope.adminFlag && !dto.Address.IsValidated) {
                    if (dto.Address.CountryCode2Digits == "CA") {
                        if (dto.Address.ZipCode) {
                            var Ca_Zipcode = dto.Address.ZipCode;
                            Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                            dto.Address.ZipCode = Ca_Zipcode;
                        }
                    }
                }

                $http.put('/api/users/' + $scope.UserId, dto).then(function (response) {

                    //Reload Userinfo
                    HFCService.GetUserInfo();

                    var result = response.data;
                    //if (result != true)
                    if (result != "Success") {
                        HFC.DisplayAlert(result);
                        $scope.modalState.loaded = true;
                    }
                    //else if (result == true) 
                    else if (result === "Success") {

                        $scope.EditUserModel = angular.copy(UserService.EditUserModel);
                        HFC.DisplaySuccess("User saved!");
                        $scope.formedituser.$setPristine();
                        //window.location.href = "#!/users";
                        history.go(-1);
                        $scope.modalState.loaded = true;
                    }
                });
            }

            function saveUserHandler(dto) {
                validateAddress(dto.Address)
                    .then(function (response) {
                        var objaddress = response.data;

                        if (objaddress.Status == "error") {
                            HFC.DisplayAlert(objaddress.Message);
                            $scope.IsBusy = false;
                        } else if (objaddress.Status == "true") {
                            var validAddress = objaddress.AddressResults[0];
                            dto.Address.Address1 = validAddress.address1;
                            dto.Address.Address2 = validAddress.address2;
                            dto.Address.City = validAddress.City;
                            dto.Address.State = validAddress.StateOrProvinceCode;

                            // Update that the address validated to true.
                            dto.Address.IsValidated = true;

                            // assign the zipcode back to the model before saving.
                            if (dto.Address.CountryCode2Digits == "US") {
                                dto.Address.ZipCode = validAddress.PostalCode.substring(0, 5);
                            }
                            else {
                                dto.Address.ZipCode = validAddress.PostalCode;
                            }

                            AfterValidateAddress(dto);
                        } else if (objaddress.Status == "false") {

                            dto.Address.IsValidated = false;

                            var errorMessage = "Invalid Address. Do you wish to continue?";

                            if (confirm(errorMessage)) {
                                AfterValidateAddress(dto);
                            }
                            else {
                                $scope.IsBusy = false;
                            }
                        } else { //SkipValidation
                            AfterValidateAddress(dto);
                        }
                    },
                        function (response) {
                            HFC.DisplayAlert(response.statusText);
                            $scope.IsBusy = false;
                        });
            };

            function validateAddress(obj) {
                var addressModeltp = {
                    address1: obj.Address1,
                    address2: obj.Address2,
                    City: obj.City,
                    StateOrProvinceCode: obj.State,
                    PostalCode: obj.ZipCode,
                    CountryCode: obj.CountryCode2Digits
                };

                if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                    var result = {
                        data: {
                            Status: "SkipValidation",
                            Message: "No Address validation required."
                        },
                        status: 200,
                        statusText: "OK"
                    }

                    return $q.resolve(result);;
                }

                // Call the api to Return the result.
                return AddressValidationService.validateAddress(addressModeltp);
            }

            $scope.ZipCodeChanged = function () {
                $scope.EditUserModel.Address.City = '';
                $scope.EditUserModel.Address.State = '';
                if ($scope.EditUserModel.Address.ZipCode) {
                    $http.get('/api/lookup/0/ZipCodesforFranchise?q=' + $scope.EditUserModel.Address.ZipCode + "&country=" + $scope.EditUserModel.Address.CountryCode2Digits).then(function (response) {

                        if (response.data.zipcodes.length > 0) {
                            $scope.EditUserModel.Address.City = response.data.zipcodes[0].City;
                            $scope.EditUserModel.Address.State = response.data.zipcodes[0].State;
                        } else {
                            $scope.EditUserModel.Address.City = '';
                            $scope.EditUserModel.Address.State = '';
                        }
                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                    });
                }

            };

            $scope.GetZipCodeMask = function (countryISO) {
                if ($scope.AddressService && countryISO)
                    var cc = $.grep($scope.AddressService.Countries, function (c) {
                        return c.ISOCode2Digits == (countryISO || "US");
                    });

                if (cc && cc.length)
                    return cc[0].ZipCodeMask;
                else
                    return "";
            };


            $scope.CountryChanged = function () {
                $scope.NewUserModel.ZipCode = '';
                if ($scope.UserId && $scope.UserId != null) {
                    $scope.EditUserModel.Address.City = '';
                    $scope.EditUserModel.Address.State = '';
                    $scope.EditUserModel.Address.ZipCode = '';
                    $scope.EditUserModel.Address.CrossStreet = '';
                }
            }

            $scope.EnableEmailSignatureChanged = function () {
                if (!$scope.EditUserModel.Person.EnableEmailSignature)
                    $scope.EditUserModel.Person.AddEmailSignAllEmails = false;
            }


            // Renaming that method name to GetRoleList as it it not specific to user
            //$scope.GetUserRolesList()
            //$scope.GetRoleList()

            //if (document.location.href.toString().indexOf("/newuser") > 0) {
            //    // This can be commonly called
            //    //$scope.GetUserRolesList();
            //}
            if (document.location.href.toString().indexOf("/edituser/") > 0) {
                // This can be commonly called
                //$scope.GetUserRolesList();

                if ($scope.UserId && $scope.UserId != null) {
                    $scope.GetUserDetails($scope.UserId);
                }
            }
            // color pallete
            $(document).click(function (event) {
                var elementFlag = $(event.target).hasClass("frm_controllead1");
                var popUpId = $(event.target).attr("aria-describedby");
                var popupDom = "#" + popUpId;
                if (!elementFlag) {
                    var openFlag = $(".popover.fade").hasClass("show");
                    if (openFlag) {
                        $(".popover.fade").removeClass("show");
                        $(".popover.fade").remove();
                    }
                } else {
                    $(".popover.fade").removeClass("show");
                    $(popupDom).addClass("show");
                }
            });
            // end color pallete
            //UserService.GetUserGridOptions();
            if ($scope.URLCallBack[1] == "manageprofile" && ($scope.URLCallBack[2] != null ||
                $scope.URLCallBack[2] != "" || $scope.URLCallBack[2] != undefined)) {
                //THis is not required
                //$scope.RolesList = UserService.RolesList;
                // This can be commonly called
                //$scope.GetUserRolesList();
                $scope.GetUserDetails($scope.URLCallBack[2]);
            }
            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 285);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 285);
                };
            });
            // End Kendo Resizing

        }
    ]);
