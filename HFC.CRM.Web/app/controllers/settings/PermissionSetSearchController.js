﻿'use strict';
app.controller('PermissionSetSearchController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'HFCService', 'NavbarService',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, HFCService, NavbarService) {
   
        HFCService.setHeaderTitle("Permission Sets #");

    $scope.NavbarService = NavbarService;
    $scope.NavbarService.SelectSettings();
    $scope.NavbarService.EnableSettingsSecurityTab();
    $scope.Permission = {};
    var offsetvalue = 0;
    var PermissionSetPermissions = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'PermissionSets')
    $scope.Permission.AddPermissionSet = PermissionSetPermissions.CanCreate;
    $scope.Permission.EditPermissionSet = PermissionSetPermissions.CanUpdate;
    $scope.Permission.ListPermissionSet = PermissionSetPermissions.CanRead;

    function ActivateDeActivateTemplate(dataitem) {
        var value1 = ""; var value2 = "";
      
        if (dataitem.FranchiseId && $scope.Permission.EditPermissionSet) {
            if (dataitem.IsActive) {
                value1 = '<li><a href="#!/permissionSets/' + dataitem.Id + '">Edit</a> </li> <li><a href="javascript:void(0)" ng-click="DisablePermissionSets(\'' + dataitem.Id + '\')">Disable</a></li>';
            }
            else if (!dataitem.IsActive) {
                value2 = '<li><a href="javascript:void(0)" ng-click="EnablePermissionSets(\'' + dataitem.Id + '\')">Enable</a></li>';
            }
            return '<ul> <li class="dropdown note1 ad_user"> <div class="btn-group">' +
                ' <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                ' <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu">' +
                    value1 + value2
            //'<li><a href="#!/permissionSets/' + dataitem.Id + '">Edit</a></li> ' +
            '</ul> </li>  </ul>'
        }
        else {
            return '';
        }
       

    }
   
    $scope.GetPermissionSets = function () {
       
        $scope.PermissionSets = {
            dataSource: {
                transport: {
                    read: {
                        url: function (params) {

                            var url1 = '/api/Permission/0/GetPermissionSetsList';
                            return url1;
                        },
                        dataType: "json"
                    }

                },
                error: function (e) {

                    HFC.DisplayAlert(e.errorThrown);
                },

                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false },
                            PermissionSetName: { editable: false },
                            FranchiseId: { editable: false },
                            Description: { editable: false },
                            CreatedOn: { type: 'date', editable: false },
                            LastUpdatedOn: { type: 'date', editable: false },
                            IsActive:{ type: "boolean", editable:false},
                        }
                    }
                }
            },
            columns: [
                {
                    field: "PermissionSetName",
                    title: "Name",
                    template: "<a href='\\\\#!/permissionSetsView/#= Id #' style='color: rgb(61,125,139);'><span>#= PermissionSetName #</span></a> ",
                    filterable: { search: true },
                    hidden: false,
                },
                {
                    field: "Description",
                    title: "Description",
                    filterable: { search: true },
                    hidden: false,


                },
                {
                    field: "CreatedOn",
                    title: "Created",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "PermissionCreatedOnFilterCalendar", offsetvalue),
                    hidden: false,
                    template: function (dataItem) {
                        return HFCService.NewKendoDateFormat(dataItem.CreatedOn);
                    },
                        //"#= kendo.toString(new Date(CreatedOn), 'MM/dd/yyyy') #",

                },
                {
                    field: "LastUpdatedOn",
                    title: "Modified",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "PermissionLastUpdatedOnFilterCalendar", offsetvalue),
                    template: function (dataItem) {
                        return HFCService.NewKendoDateFormat(dataItem.LastUpdatedOn);
                    },
                        //"#= kendo.toString(new Date(LastUpdatedOn), 'MM/dd/yyyy') #",
                    hidden: false,

                },
                 {
                     field: "IsActive",
                     title: "Status",
                    
                     template: '<input type="checkbox" #= IsActive ? "checked=checked" : "" # disabled="disabled" class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched"></input>',
                     hidden: false,
                    
                 },
                        
                {
                    field: "",
                    title: "",
                    template: function (dataitem) {
                        return ActivateDeActivateTemplate(dataitem);
                    }
                },


            ],

            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isempty: "Empty",
                        isnotempty: "Not empty"


                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gte: "Greater than or equal to",
                        lte: "Less than or equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            sortable: true,
            resizable: true,
            autoSync: true,
            noRecords: { template: "No records found" },
            scrollable: true,
            toolbar: [
                    {
                        template: kendo.template(
                            $(
                                ' <script id="template" type="text/x-kendo-template"><div class="row no-margin vendor_search"><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="PermissionSearchgridSearch()" type="search" id="searchBox" placeholder="Search Permission Sets" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="PermissionSetsSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>')
                            .html())
                    }
            ],

        };

    };
    $scope.GetPermissionSets();

    $scope.PermissionSetsSearchClear = function () {
        $('#searchBox').val('');
        $("#gridPermissionSetsSearch").data('kendoGrid').dataSource.filter({
        });
    }

    $scope.PermissionSearchgridSearch = function () {
        var searchValue = $('#searchBox').val();
        $("#gridPermissionSetsSearch").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
              {
                  field: "PermissionSetName",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "Description",
                  operator: "contains",
                  value: searchValue
              }
            ]
        });

    }

    $scope.DisablePermissionSets = function (id) {
         
        var disable_id = id;
        var SetType = "Disable";
        $http.get('/api/Permission/' + disable_id + '/DisableEnableDataSet?Type=' + SetType).then(function (response) {
             
            var value = response.data;
            if (value == "Success") {
                $('#gridPermissionSetsSearch').data('kendoGrid').dataSource.read();
                $('#gridPermissionSetsSearch').data('kendoGrid').refresh();
            }
            else {
                HFC.DisplayAlert(response.data);
            }
        });
    }

    $scope.EnablePermissionSets=function(Id){
         
        var disable_id = Id;
        var SetType = "Enable";
        $http.get('/api/Permission/' + disable_id + '/DisableEnableDataSet?Type=' + SetType).then(function (response) {
             
            var value = response.data;
            if (value == "Success") {
                $('#gridPermissionSetsSearch').data('kendoGrid').dataSource.read();
                $('#gridPermissionSetsSearch').data('kendoGrid').refresh();
            }
            else {
                HFC.DisplayAlert(response.data);
            }
        });
    }

        //Redirect to Create Permission set Page
    $scope.NewPermissionSet = function () {
        window.location.href = '#!/permissionSet';
        //var url = "http://" + $window.location.host + "/#!/leads/";
        //$window.location.href = url;
    };
}])