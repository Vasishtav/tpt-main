﻿app.controller('sourceController', [
    '$http', '$scope', '$window', '$location', '$rootScope', 'sourceService', 'NavbarService', 'HFCService', 'kendoService','kendotooltipService',
    function ($http, $scope, $window, $location, $rootScope, sourceService, NavbarService, HFCService, kendoService, kendotooltipService) {

        $scope.sourceService = sourceService;
        $scope.permissions = {};

        $scope.NavbarService = NavbarService;
        $scope.HFCService = HFCService;
        $scope.NavbarService.SelectMarketing();
        $scope.NavbarService.EnableMarketingGeneralTab();

        $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;

        var offsetvalue = 0;
        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;

        HFCService.setHeaderTitle("Source #");

        //flag for date validation
        $scope.ValidDate = true;
        $scope.ValidDatelist = [];
        $scope.min = 1;
        $scope.max = 12;
        $scope.durationChange = false;

        $scope.Permission = {};
        var FranchiseSourceandCampaignspermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'FranchiseSourceandCampaigns')

        $scope.Permission.ListCampaign = FranchiseSourceandCampaignspermission.CanRead; //$scope.HFCService.GetPermissionTP('List Campaign Source').CanAccess;
        $scope.Permission.AddCampaign = FranchiseSourceandCampaignspermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Campaign Source').CanAccess;
        $scope.Permission.EditCampaign = FranchiseSourceandCampaignspermission.CanUpdate;
        $scope.Permission.DeleteCampaign = FranchiseSourceandCampaignspermission.CanDelete;

        // data list definition
        $scope.channelList = [];
        $scope.showDelete = false;
        $scope.includeInactive = false;
        $scope.editCampaign = false;
        $scope.submitted = false;
        $scope.Campaign = {
            CampaignId: 0,
            Name: '',
            Description: '',
            StartDate: '',
            EndDate: '',
            IsDeleted: false,
            IsActive: false,
            SourcesTPId: 0,
            Amount: 0,
            Memo: '',
            FranchiseId: 0,
            ChannelId: 0,
            MonthlyBudget: '',
            DeactivateOnEndDate: false,
            RecurringType: 0,
            Duration: '',
            Channel: '',
            Source: ''
        }
        
        //for displaying loader
        $scope.modalState = {
            loaded: true
        }

        $scope.$watch('modalState.loaded', function () {
            var loadingElement = document.getElementById("loading");

            if (!$scope.modalState.loaded) {
                loadingElement.style.display = "block";
            } else {
                loadingElement.style.display = "none";
            }
        });


        $scope.EnableGridToolbarButtons = function () {
            //enable/disable the add/clear search buttons based on conditions.
            var searchValue = $('#searchBox').val();
            if (searchValue != "") {
                $('#btnReset').prop('disabled', false);
            }
            else {
                $('#btnReset').prop('disabled', true);
            }
        }

        //campaign enhancement
        //for date validation
        $scope.ValidateDate = function (date, id) {
            var ValidDateobj = {};
            $scope.ValidDate = HFCService.ValidateDate(date, "date");

            if (id == "endDate" && $scope.ValidDate && Date.parse($scope.Campaign.EndDate) < Date.parse($scope.Campaign.StartDate)) {
                var InvalidEnd = true;
                $scope.ValidDate = false;
            }
            else
                var InvalidEnd = false;

            if ($scope.ValidDatelist.find(item => item.id === id)) {
                var index = $scope.ValidDatelist.findIndex(item => item.id === id);
                $scope.ValidDatelist[index].valid = $scope.ValidDate;
                $scope.ValidDatelist[index].validend = InvalidEnd;
            }
            else {
                ValidDateobj["id"] = id;
                ValidDateobj["valid"] = $scope.ValidDate;
                ValidDateobj["validend"] = InvalidEnd;
                $scope.ValidDatelist.push(ValidDateobj);
            }

            if ($scope.ValidDate) {
                $("#" + id + "").removeClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).removeClass("icon_Background");
                if (id == "startDate") {
                    $scope.setEndDate();
                    var dateee = new Date(date);
                    var cuDate = new Date();
                    cuDate.setHours(0);
                    cuDate.setMinutes(0);
                    cuDate.setSeconds(0);
                    cuDate.setMilliseconds(0)
                    if (dateee.getTime() <= cuDate.getTime())
                        $scope.Campaign.IsActive = true;
                    else
                        $scope.Campaign.IsActive = false;                    
                }
            }
            else {
                $("#" + id + "").addClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).addClass("icon_Background");
            }
        }
        $scope.options = {
            format: "c", decimals: 0, spinners: false, min: 1, placeholder: "", restrictDecimals: true,
            change: function () {
               // $scope.calcMonthlyBudget();
            }
        }
        $scope.campaign_source = {
            autoBind: true,
            valuePrimitive: true,
            filter: "contains",
            filtering: function (ev) {
                var filterValue = ev.filter != undefined ? ev.filter.value : "";
                ev.preventDefault();
                this.dataSource.filter({
                    logic: "or",
                    filters: [
                        {
                            field: "Channel",
                            operator: "contains",
                            value: filterValue
                        },
                        {
                            field: "Source",
                            operator: "contains",
                            value: filterValue
                        }
                    ]
                });
            },
            dataTextField: "Source",
            dataValueField: "SourcesTPId",
            template: "#=Channel # > #=Source #",
            change: function (e) {
                $(".k-widget.k-dropdown.k-header").css("border", "1px solid #ceced2 !important");
                $("#recurring .k-input").removeClass("required-text");
            },
            select: function (e) {
                if (e.dataItem.Source === "Select") {
                    $scope.preventClose = true;
                    e.preventDefault();
                }
                else {
                    $('#sourceWrapper .k-widget.k-dropdown').css('border', '1px solid #b2bfca');
                    $scope.Campaign.Channel = e.dataItem.Channel;
                    $scope.Campaign.Source = e.dataItem.Source;
                }
            },
            close: function (e) {
                if ($scope.preventClose == true)
                    e.preventDefault();

                $scope.preventClose = false;
            },
            dataSource: {
                transport: {
                    read: {
                        url: '/api/lookup/0/GetAllSources',
                    }
                },
            }
        };
        $scope.recurring_source = {
            autoBind: true,
            valuePrimitive: true,
            select: function (e) {
                $scope.Campaign.RecurringType = Number(e.dataItem.value);
                $scope.recurrenceChange(e.dataItem.value);
            }
        };
        $("input[type='number'][name='duration']").on('paste', function () {
            var $el = $(this);
            setTimeout(function () {
                $el.val(function (i, val) {
                    var data = val.replace(/\./g, '');
                    $('#duration').val(data);
                    $scope.calcMonthlyBudget();
                    $scope.setEndDate();
                    $scope.durationChanged();
                    return data;
                })
            })
        });
        $scope.enableDisableFields = function (disabled) {
            $('#campaign').prop('disabled', disabled);
            $('#startDate').prop('disabled', disabled);
            $('#recurring').prop('disabled', disabled);
            $('#campaign_source').prop('disabled', disabled);
            $('#amount').prop('disabled', disabled);
            $('#duration').prop('disabled', disabled);
            $('#duration').prop('readonly', false);
            $('#activeCheck').prop('disabled', disabled);
            $('#deactiveCheck').prop('disabled', disabled);
            $('#memo').prop('disabled', disabled);
            if (disabled)
                disabled = false;
            else
                disabled = true;
            $('#searchBox').prop('disabled', disabled);
            $('#SourceCheck').prop('disabled', disabled);
            $('#btnReset').prop('disabled', disabled);
        }

        $scope.editCampaigns = function (data) {
            $("#recurring .k-input").addClass("required-text");
            $scope.editCampaign = true;
            $('.leadgrid_box input[type=checkbox]').prop('disabled', true);
            $scope.enableDisableFields(false);
            $scope.Campaign = angular.copy(data);
            $scope.recurrenceChange($scope.Campaign.RecurringType);
            $scope.tempMonthlyBudget = kendo.toString($scope.Campaign.MonthlyBudget, "c");
            $scope.Campaign.StartDate = data.StartDate != null ? data.StartDate.toLocaleString() : "";
            $scope.Campaign.EndDate = data.EndDate != null ? data.EndDate.toLocaleString() : "";
        }

        $scope.addCampaign = function () {                      
            $scope.editCampaign = true;
            $('.leadgrid_box input[type=checkbox]').prop('disabled', true);
            $scope.enableDisableFields(false);
            $scope.Source_form.duration.$invalid = false;
        }

        $scope.campaignCancel = function () {
            $scope.editCampaign = false;
            if ($scope.Permission.DeleteCampaign)
                $('.leadgrid_box input[type=checkbox]').prop('disabled', false);
            $('#sourceWrapper .k-widget.k-dropdown').css('border', '1px solid #b2bfca');
            $('#recurrWrapper .k-widget.k-dropdown').css('border', '1px solid #b2bfca');
            $("#startDate").removeClass("invalid_Date");
            $("#endDate").removeClass("invalid_Date");
            $('#endDate').prop('disabled', true);
            $scope.submitted = false;
            $scope.ValidDatelist = [];
            $scope.enableDisableFields(true);
            $scope.tempMonthlyBudget = "";
            $scope.Campaign.CampaignId = 0;
            $scope.Campaign.Name = "";
            $scope.Campaign.Description = "";
            $scope.Campaign.StartDate = "";
            $scope.Campaign.EndDate = "";
            $scope.Campaign.IsDeleted = false;
            $scope.Campaign.IsActive = false;
            $scope.Campaign.SourcesTPId = 0;
            $scope.Campaign.Amount = "";
            $scope.Campaign.Memo = "";
            $scope.Campaign.FranchiseId = 0;
            $scope.Campaign.ChannelId = 0;
            $scope.Campaign.MonthlyBudget = "";
            $scope.Campaign.DeactivateOnEndDate = false;
            $scope.Campaign.RecurringType = 0;
            $scope.Campaign.Duration = "";
            $scope.Campaign.Channel = "";
            $scope.Campaign.Source = "";
            $scope.durationChange = false;
            $scope.min = 1;
            $scope.max = 12;
        }

        $scope.recurrenceChange = function (data) {
            if (data == "1") {
                $('#endDate').prop('disabled', false);
                $scope.Campaign.Duration = 1;
                $('#duration').prop('readonly', true);
            }
            else if (data == "2") {
                $('#endDate').prop('disabled', true);
                $('#duration').prop('readonly', false);
                $scope.min = 1;
                $scope.max = 12;
                $scope.setEndDate();
            }
            else if (data == "3") {
                $('#endDate').prop('disabled', true);
                $('#duration').prop('readonly', false);
                $scope.min = 1;
                $scope.max = 4;
                $scope.setEndDate();
            }
            else if (data == "4") {
                $('#endDate').prop('disabled', true);
                $('#duration').prop('readonly', true);
                $scope.Campaign.Duration = 1;
                $scope.setEndDate();
            }
            $scope.calcMonthlyBudget();
            $('#recurrWrapper .k-widget.k-dropdown').css('border', '1px solid #b2bfca');
        }

        $scope.durationChanged = function () {
            $scope.durationChange = true;
        }

        $scope.setEndDate = function () {
            var startDate = $('#startDate').val();
            var valid = HFCService.ValidateDate(startDate, "date");
            if (!valid)
                return;
            var duration = $scope.Campaign.Duration;
            if (!duration || duration=="0")
                duration = 1;
            var recurrance = $scope.Campaign.RecurringType;
            var months = new Date(startDate).getMonth();
            $("#endDate").removeClass("invalid_Date");
            var options = {
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit"
            };
            if (recurrance == 2) {
                var enddate = new Date(startDate);
                enddate.setMonth(months + Number(duration));
                enddate = enddate.toLocaleString("en", options);
                enddate = enddate.replace(/:\d{2}\s/, ' ');
                enddate = enddate.replace(',', '')
                var vals = enddate.split(' ');
                $scope.Campaign.EndDate = vals[0];
                $("#endDate").val($scope.Campaign.EndDate);
            }
            else if (recurrance == 3) {
                var enddate = new Date(startDate);
                enddate.setMonth(months + (Number(duration) * 3));
                enddate = enddate.toLocaleString("en", options);
                enddate = enddate.replace(/:\d{2}\s/, ' ');
                enddate = enddate.replace(',', '')
                var vals = enddate.split(' ');
                $scope.Campaign.EndDate = vals[0];
                $("#endDate").val($scope.Campaign.EndDate);
            }
            else if (recurrance == 4) {
                var enddate = new Date(startDate);
                enddate.setMonth(months + (Number(duration) * 12));
                enddate = enddate.toLocaleString("en", options);
                enddate = enddate.replace(/:\d{2}\s/, ' ');
                enddate = enddate.replace(',', '')
                var vals = enddate.split(' ');
                $scope.Campaign.EndDate = vals[0];
                $("#endDate").val($scope.Campaign.EndDate);
            }
        }

        $scope.calcMonthlyBudget = function () {
            var totalAmount = $scope.Campaign.Amount;
            if (!totalAmount) {
                $scope.Campaign.MonthlyBudget = 0;
                $scope.tempMonthlyBudget = "";
                return;
            }
            var duration = $scope.Campaign.Duration;
            if (!duration || duration=="0")
                return;

            var recurrance = $scope.Campaign.RecurringType;            
            if (recurrance == 1) {
                $scope.Campaign.MonthlyBudget = totalAmount;
                if ($scope.Campaign.MonthlyBudget)
                    $scope.tempMonthlyBudget = kendo.toString(Number($scope.Campaign.MonthlyBudget), "c");
            }
            else if (recurrance == 2) {
                $scope.Campaign.MonthlyBudget = totalAmount / (duration);
                $scope.tempMonthlyBudget = kendo.toString(Number($scope.Campaign.MonthlyBudget), "c");
            }
            else if (recurrance == 3) {
                $scope.Campaign.MonthlyBudget = totalAmount / (3 * duration);
                $scope.tempMonthlyBudget = kendo.toString(Number($scope.Campaign.MonthlyBudget), "c");
            }
            else if (recurrance == 4) {
                $scope.Campaign.MonthlyBudget = totalAmount / (12 * duration);
                $scope.tempMonthlyBudget = kendo.toString(Number($scope.Campaign.MonthlyBudget), "c");
            }
            else
                $scope.Campaign.MonthlyBudget = '';
        }

        $scope.campaignSave = function () { 
            $scope.submitted = true;
            if (!$scope.Campaign.Source)
                $('#sourceWrapper .k-widget.k-dropdown').css('border', '1px solid #a94442');
            if (!$scope.Campaign.RecurringType)
                $('#recurrWrapper .k-widget.k-dropdown').css('border', '1px solid #a94442');
            if (!$scope.Campaign.Duration)
                $scope.Source_form.duration.$invalid = true;
            if (!$scope.Campaign.Amount)
                $scope.Source_form.amount.$invalid = true;

            if ($scope.Source_form.$invalid) {
                return;
            }
            //restrict saving for invalid date
            for (i = 0; i < $scope.ValidDatelist.length; i++) {
                if ($scope.ValidDatelist[i].validend) {
                    HFC.DisplayAlert("End date cannot be before start date.");
                    return
                }
                else if (!$scope.ValidDatelist[i].valid) {
                    HFC.DisplayAlert("Invalid Date!");
                    return;
                }
            }
            $scope.modalState.loaded = false;
            if ($scope.Campaign.CampaignId) {
                $http.put('/api/Campaigns/0/UpdateCampaign', $scope.Campaign).then(function (response) {
                    if (response.data) {
                        $scope.submitted = false;
                        $scope.campaignCancel();
                        $scope.getOrderDetails();
                    }
                }).catch(function (error) {
                    HFC.DisplayAlert(error.statusText);
                    $scope.modalState.loaded = true;
                });
            }
            else {
                $http.post('/api/Campaigns/0/AddCampaign', $scope.Campaign).then(function (response) {
                    if (response.data) {
                        $scope.submitted = false;
                        $scope.campaignCancel();
                        $scope.getOrderDetails();
                    }
                }).catch(function (error) {
                    HFC.DisplayAlert(error.statusText);
                    $scope.modalState.loaded = true;
                });
            }
        }

        $scope.deleteCampaigns = function () {
            $scope.modalState.loaded = false;
            var deleteCampaignId = [];
            for (i = 0; i < $scope.channelList.length; i++) {
                $("#channelGrid" + i + "").find("input:checked").each(function () {
                    var grid = $("#channelGrid" + i + "").data("kendoGrid");

                    if (!$(this).parents('th').length) {
                        var dataitem = grid.dataItem($(this).closest('tr'));
                        deleteCampaignId.push(dataitem.CampaignId);
                        //grid.dataSource.remove(dataitem);
                    }
                })
            }
            if (deleteCampaignId.length) {
                $http.post('/api/Campaigns/0/DeleteCampaign', deleteCampaignId).then(function (response) {
                    if (response.data.data == "Success") {
                        $scope.showDelete = false;
                        $('#addcampaign').prop('disabled', false);
                        $scope.EnableGridToolbarButtons();
                        $('#searchBox').prop('disabled', false);
                        $('#SourceCheck').prop('disabled', false);
                        $scope.getOrderDetails();
                    }
                }).catch(function (error) {
                    HFC.DisplayAlert(error);
                    $scope.modalState.loaded = true;
                });
            }
        }
        // panel bar grid configurations
        var getGridConfiguration = function (dataList) {
            if (dataList) {
                for (var i = 0; i < dataList.length; i++) {
                    var gridConfigName = "gridConfig" + i;
                    $scope[gridConfigName] = {
                        dataSource: dataList[i].Campaigns,
                        columns: [
                            {
                                selectable: true,
                                width: "70px"
                            },
                            {
                                field: "Name",
                                title: "Campaign",
                                template: function (dataItem) {
                                    if (dataItem.Name == null)
                                        return "";
                                    else
                                        return dataItem.Name;
                                },
                                editable: false,
                            },
                            {
                                field: "Channel",
                                title: "Channel",
                                hidden: true,
                                editable: false,
                            },
                            {
                                field: "Source",
                                title: "Source",
                                template: function (dataItem) {
                                    if (dataItem.Source == null)
                                        return "";
                                    else
                                        return dataItem.Source; 
                                },
                                editable: false,
                            },
                            {
                                field: "StartDate",
                                title: "Start Date",
                                type: "date",
                                template: function (dataItem) {
                                    if (dataItem.StartDate == null)
                                        return "";
                                    else
                                        return HFCService.KendoDateFormat(dataItem.StartDate);
                                },
                                editable: false,
                            },
                            {
                                field: "EndDate",
                                title: "End Date",
                                type: "date",
                                template: function (dataItem) {
                                    if (dataItem.EndDate == null)
                                        return "";
                                    else
                                        return HFCService.KendoDateFormat(dataItem.EndDate);
                                },
                                editable: false,
                            },
                            {
                                field: "Amount",
                                title: "Yearly Cost",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                                },
                                editable: false,
                            },
                            {
                                field: "MonthlyBudget",
                                title: "Monthly Cost",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.MonthlyBudget) + "</span>";
                                },
                                editable: false,
                            },
                            {
                                field: "Memo",
                                title: "Memo",
                                template: function (dataItem) {
                                    if (dataItem.Memo == null)
                                        return "";
                                    else
                                        return "<span >" + dataItem.Memo + "</span>"                                   
                                },
                                editable: false,
                            },
                            {
                                field: "IsActive",
                                title: "Status",
                                template: " #if(IsActive){# Active #}else{# Inactive#}# ",
                                editable: false,
                            },
                            {
                                field: "",
                                title: "",
                                template: function (dataItem) {
                                    return "<input type='button' ng-click='editCampaigns(dataItem)' ng-disabled='showDelete || editCampaign || !Permission.EditCampaign' class='k-button btn btn-primary cancel_but' value='Edit'>";
                                },
                                width: "80px"
                            },
                        ],
                        noRecords: {
                            template: "No records found"
                        },
                        sortable: true,
                        dataBound: function (e) {                            
                            var id = e.sender.element.attr('id');
                            var index = id.replace(/channelGrid/g, "");
                            index = Number(index);
                            var monthlyCost = 0;
                            for (i = 0; i < e.sender.dataSource._view.length; i++) {
                                monthlyCost = monthlyCost + e.sender.dataSource._view[i].MonthlyBudget;
                            }
                            if (index >= 0 && $scope.channelList && $scope.channelList.length) {
                                $scope.channelList[index].TotalCampaign = e.sender.dataSource.view().length;
                                $scope.channelList[index].MonthlyCost = monthlyCost;
                            }

                            $scope.EnableGridToolbarButtons();
                        },
                    };
                    
                }
                setTimeout(function () {
                    $(".leadgrid_box").kendoTooltip({
                        filter: "td",
                        show: function (e) {
                            if (this.content.text() != "") {
                                $('[role="tooltip"]').css("visibility", "visible");
                            }
                        },
                        hide: function () {
                            $('[role="tooltip"]').css("visibility", "hidden");
                        },
                        content: function (e) {
                            var element = e.target[0];
                            if (element.offsetWidth < element.scrollWidth) {
                                return e.target.text();
                            } else {
                                return "";
                            }
                        }
                    })
                }, 100);
            }
        }
        $scope.CampaigngridSearchbyChannel = function () {
            var searchValue = $('#searchBox').val();
            if (searchValue) {
                for (i = 0; i < $scope.channelList.length; i++) {
                    $("#channelGrid" + i + "").data("kendoGrid").dataSource.filter({
                        logic: "or",
                        filters: [
                            //{
                            //    field: "Channel",
                            //    operator: "contains",
                            //    value: searchValue
                            //},
                            {
                                field: "Source",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "Name",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "Memo",
                                operator: "contains",
                                value: searchValue
                            }

                        ]
                    });
                    var panelBar = $("#panelbar" + i + "").data("kendoPanelBar");
                    panelBar.expand($("#panelbarlist" + i + ""));
                }
            }
            else {
                $scope.CampaigngridSearchClear();
                for (i = 0; i < $scope.channelList.length; i++) {
                    var panelBar = $("#panelbar" + i + "").data("kendoPanelBar");
                    panelBar.collapse($("#panelbarlist" + i + ""));
                }
            }
        }
        $scope.CampaigngridSearchClear = function () {
            $('#searchBox').val('');
            $scope.EnableGridToolbarButtons();
            for (i = 0; i < $scope.channelList.length; i++) {
                $("#channelGrid" + i + "").data("kendoGrid").dataSource.filter({
                });
                var panelBar = $("#panelbar" + i + "").data("kendoPanelBar");
                panelBar.collapse($("#panelbarlist" + i + ""));
            }
        }

        $(document).on('click', '.k-checkbox-label', function (e) {
            
            setTimeout(function () {
                var len = $('.leadgrid_box td input[type=checkbox][aria-checked=true]').length;
                if (len > 0) {
                    $scope.showDelete = true;
                    $('#searchBox').prop('disabled', true);
                    $('#SourceCheck').prop('disabled', true);
                    $('#btnReset').prop('disabled', true); 
                    $('#addcampaign').prop('disabled', true);
                    $scope.$apply();
                }
                else {
                    $scope.showDelete = false;
                    $scope.EnableGridToolbarButtons();
                    $('#searchBox').prop('disabled', false);
                    $('#SourceCheck').prop('disabled', false);
                    $('#addcampaign').prop('disabled', false);
                    $scope.$apply();
                }
            }, 100);
            
        });

        $scope.getOrderDetails = function () {
            var setvalue = $('input[name="SourceCheck"]:checked').val();
            if (setvalue == "on")
                $scope.includeInactive = true;
            else if (setvalue == undefined)
                $scope.includeInactive = false;

            $scope.modalState.loaded = false;
            $scope.channelList = [];
            $http.get('/api/Campaigns/0/GetCampaignByFranchiseTP?includeInactive=' + $scope.includeInactive).then(function (response) {
                $scope.channelList = response.data;
                getGridConfiguration($scope.channelList);
                $scope.modalState.loaded = true;
            }).catch(function (e) {
                    $scope.modalState.loaded = true;
            });
        }

        $scope.getOrderDetails();

        $scope.setWidth = function () {
            setTimeout(function () {
                $scope.CampaigngridSearchbyChannel();
                if (!$scope.Permission.DeleteCampaign)
                    $('.leadgrid_box input[type=checkbox]').prop('disabled', true);
                $scope.modalState.loaded = true;
                $scope.$apply();
            }, 50);
            setTimeout(function () {
                var widths = new Array();
                $('.channelwidth').each(function () {
                // Need to let sizes be whatever they want so no overflow on resize
                $(this).css('min-width', '0');
                $(this).css('max-width', 'none');
                $(this).css('width', 'auto');
                // Then add size (no units) to array
                widths.push($(this).width());
                });
                var max = Math.max.apply(Math, widths);
                max = max + 5;
                // Set all widths to max width
                $('.channelwidth').each(function () {
                    $(this).css('width', max + 'px');
                });  
                widths = [];
                $('.count').each(function () {
                    // Need to let sizes be whatever they want so no overflow on resize
                    $(this).css('min-width', '0');
                    $(this).css('max-width', 'none');
                    $(this).css('width', 'auto');
                    // Then add size (no units) to array
                    widths.push($(this).width());
                });
                max = Math.max.apply(Math, widths);
                max = max + 15;
                // Set all widths to max width
                $('.count').each(function () {
                    $(this).css('width', max + 'px');
                });    
                widths = [];
                $('.monthlyCost').each(function () {
                    // Need to let sizes be whatever they want so no overflow on resize
                    $(this).css('min-width', '0');
                    $(this).css('max-width', 'none');
                    $(this).css('width', 'auto');
                    // Then add size (no units) to array
                    widths.push($(this).width());
                });
                max = Math.max.apply(Math, widths);
                max = max + 15;
                // Set all widths to max width
                $('.monthlyCost').each(function () {
                    $(this).css('width', max + 'px');
                });   
            }, 5);
            
        }
    }
]);