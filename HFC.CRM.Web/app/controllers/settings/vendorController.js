﻿
// TODO: rquired refactoring:

'use strict';
app
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('vendorController',
    [
        '$scope', '$window', 'HFCService', 'vendorService', '$routeParams', '$location', '$rootScope', 'kendotooltipService'
        , '$templateCache', '$sce', '$compile', '$http', 'AddressService', 'NavbarService'
        , 'AddressValidationService', '$q', 'kendoService',
        function ($scope, $window, HFCService, vendorService, $routeParams, $location, $rootScope, kendotooltipService
        , $templateCache, $sce, $compile, $http, AddressService, NavbarService
        , AddressValidationService, $q, kendoService) {

            $scope.NavbarService = NavbarService;
            $scope.NavbarService.SelectSettings();
            $scope.NavbarService.EnableSettingsOperationsTab();

            HFCService.setHeaderTitle("Vendor #");

            $scope.AddressService = AddressService;
            $scope.vendorService = vendorService;
            $scope.HFCService = HFCService;
            $scope.BrandId = $scope.HFCService.CurrentBrand;
            $scope.filter_CounterSelectedSalesAgent = 'Nothing selected';
            $scope.filter_CounterSelectedJobStatus = 'Nothing selected';
            $scope.selectedSales = null;
            $scope.selectedAccountStatuses = null;
            $scope.selectedSources = null;
            $scope.SearchFilter = 'Auto_Detect';
            $scope.vendorId = $routeParams.VendorId;
            $scope.CreditLimitCurrencyControl = {};

            $scope.RouteUrl = $routeParams;

            // for kendo tooltip
            $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
            kendoService.customTooltip = null;



            // This is very much required for Angular to track otherwise the current implementation won't work.
            $scope.StateList = [];
            if ($location.$$path.match("vendorview")) {
                $scope.EditVendor = false;
            }
            else
                $scope.EditVendor = true;


            $scope.Permission = {};
            var MyVendorspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyVendors')
            var MyProductspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyProducts')

            $scope.Permission.ListVendors = MyVendorspermission.CanRead; //$scope.HFCService.GetPermissionTP('List Vendors').CanAccess;
            $scope.Permission.AddVendor = MyVendorspermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Vendor').CanAccess;
            $scope.Permission.EditVendor = MyVendorspermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Vendor').CanAccess;
            $scope.Permission.DisplayVendor = MyVendorspermission.CanRead; // $scope.HFCService.GetPermissionTP('Display Vendor').CanAccess;
            //$scope.Permission.DisplayAccountInfo = $scope.HFCService.GetPermissionTP('Display AccountInfo').CanAccess;
            //$scope.Permission.DisplayStreetAddress = $scope.HFCService.GetPermissionTP('Display StreetAddress').CanAccess;
            $scope.Permission.ListProducts = MyProductspermission.CanRead; //$scope.HFCService.GetPermissionTP('List Products').CanAccess;
            $scope.Permission.AddProduct = MyProductspermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Product').CanAccess;

            $scope.ChangeFilterSearch = function (val) {

                $scope.SearchFilter = val;
            }

            $scope.Vendor = {
                VendorId: '',
                Name: '',
                VendorType: '',
                AccountRep: '',
                AccountRepPhone: '',
                AccountRepEmail: '',
                AccountNo: '',
                Terms: '',
                CreditLimit: '',
                OrderingMethod: '',
                ContactDetails: '',
                VendorStatus: '',
                RetailBasis: '',
                Currency: HFCService.FranchiseCountryCode == "US" ? "USD" : "CAD",

                CurrencyId: '',
                AcctStatus: '',
                CurrencyValueCode: '',

                VendorAddress: $scope.VendorAddress,
                ShipAccountNumber: '',
                ShipVendorLocationId: '',
                ShipVendorLocationName: '',
                ShipTerritoryId: '',
                BulkPurchase: false,
                ShipVia: false,
                BPAccountNo: 0,
                BPShipLocation: 0,
                Carrier: '',
                ShipViaAccountNo: 0

            };




            $scope.VendorAddress =
                  {
                      Address1: '',
                      Address2: '',
                      City: '',
                      Country: '',
                      State: '',
                      ZipCode: '',
                      CountryCode2Digits: HFCService.FranchiseCountryCode
                  }

            $scope.ProductDisplay = {
                ProductId: '',
                Category: '',
                Description: '',
                CalculatedSalesPrice: '',
            }


            $scope.CheckContent_Cancel = function () {
                $("#CheckContent_PopUp").modal("hide");
                return;
            }
            $scope.CheckContent_Accept = function () {
                $("#CheckContent_PopUp").modal("hide");
                return;
            }


            $scope.Redirect_Continue = function () {
                var modalId = "Redirect_PopUp";
                $("#" + modalId).modal("hide");

                window.location.href = '/#!/productCreate/' + $scope.vendorId;
            }
            $scope.Redirect_Cancel = function () {
                var modalId = "Redirect_PopUp";
                $("#" + modalId).modal("hide");
                window.location.href = '#!/vendorview/' + $scope.vendorId;

            }



            //on change call to fetch what type of vendor and its id
            $scope.Data = function (value) {

                if (document.location.href.toString().indexOf("/vendor/") > 0) {
                    if (value) {
                        $http.get('/api/lookup/0/CheckName/?value=' + value).then(function (response) {
                            var check = response.data;
                            if (check != null) {
                                HFC.DisplayAlert("Unable to change Vendor Name. Pleas create a Non-Alliance Vendor")
                                $scope.Vendor.Name = $scope.PrevName;
                            }
                        })
                    }

                }
                else {
                    if (value) {
                        $http.get('/api/lookup/0/GetVendorName/?value=' + value).then(function (response) {
                            var data = response.data;
                            if (data != null) {
                                if (response.data.FranchiseId != 0 && response.data.FranchiseId != null && response.data.FranchiseId != '') {
                                    HFC.DisplayAlert("Unable to change Vendor Name. Non-Alliance Vendor already exists")
                                    $scope.Vendor.VendorId = $scope.PreviousId;
                                    $scope.Vendor.VendorName = "My Vendor";
                                    $scope.Vendor.Name = "";
                                    return;
                                }
                                else if (response.data != null && response.data.FranchiseId == 0) {
                                    $window.alert("A Non-Alliance Vendor with this name exists. Vendor Type changed to Non-Alliance Vendor");
                                    $scope.Vendor.VendorId = response.data.VendorID;
                                    $scope.Vendor.VendorName = "Non-Alliance Vendor";
                                    $scope.Vendor.VendorType = response.data.VendorType;

                                }
                                else {
                                    $scope.Vendor.VendorId = $scope.PreviousId;
                                    $scope.Vendor.VendorName = "My Vendor";
                                    $scope.Vendor.Name = value;
                                }
                            }

                        })
                    }
                    else {


                        $scope.Vendor.VendorId = $scope.PreviousId;
                        $scope.Vendor.VendorName = "My Vendor";
                        $scope.Vendor.Name = value;

                    }
                }

            }



            $scope.gridvalidate = function () {
                var grid = $("#gridShipLocation").data("kendoGrid");
                var rowEdit = $('#gridShipLocation').find('.k-grid-edit-row');
                if (!rowEdit.length) {
                    for (var i = 0; i < grid.dataSource.data().length; i++) {
                        if ((grid.dataSource.data()[i].AccountNumber == "" || grid.dataSource.data()[i].AccountNumber == null || grid.dataSource.data()[i].AccountNumber == undefined) || (grid.dataSource.data()[i].ShipToName == "Select" || grid.dataSource.data()[i].ShipToName == "" || grid.dataSource.data()[i].ShipToName == undefined || grid.dataSource.data()[i].ShipToName == null)) {
                            grid.editRow($('#gridShipLocation tr:has(td):eq(' + i + ')'));
                            if ((grid.dataSource.data()[i].AccountNumber == "" || grid.dataSource.data()[i].AccountNumber == null || grid.dataSource.data()[i].AccountNumber == undefined)) {
                                grid.editCell($("#gridShipLocation tr:has(td):eq(" + i + ") td:eq(2)"));
                                break;
                            }
                            if ((grid.dataSource.data()[i].ShipToName == "Select" || grid.dataSource.data()[i].ShipToName == "" || grid.dataSource.data()[i].ShipToName == undefined || grid.dataSource.data()[i].ShipToName == null)) {
                                grid.editCell($("#gridShipLocation tr:has(td):eq(" + i + ") td:eq(3)"));
                            }
                            break;
                        }
                    }
                }
                var validator = $scope.kendoValidator("gridShipLocation");


                if (grid !== undefined && grid !== null && grid.dataSource.hasChanges()) {

                    if (!validator.validate()) {
                        return false;
                    }
                    $scope.Vendor.Shipvalue = grid.dataSource.data();
                    return true;
                }
                else {
                    return true;
                }


            }
            $scope.ValidateBpgrid = function () {
                if ($scope.Vendor.BulkPurchase && document.getElementById("gridBulkPurchasing")) {
                    var grid1 = $("#gridBulkPurchasing").data("kendoGrid");
                    var rowEdit = $('#gridBulkPurchasing').find('.k-grid-edit-row');
                    if (!rowEdit.length) {
                        for (var i = 0; i < grid1.dataSource.data().length; i++) {
                            if ((grid1.dataSource.data()[i].AccountNumber == "" || grid1.dataSource.data()[i].AccountNumber == null || grid1.dataSource.data()[i].AccountNumber == undefined) || (grid1.dataSource.data()[i].ShipToName == "Select" || grid1.dataSource.data()[i].ShipToName == "" || grid1.dataSource.data()[i].ShipToName == undefined || grid1.dataSource.data()[i].ShipToName == null)) {
                                grid1.editRow($('#gridBulkPurchasing tr:has(td):eq(' + i + ')'));
                                if ((grid1.dataSource.data()[i].AccountNumber == "" || grid1.dataSource.data()[i].AccountNumber == null || grid1.dataSource.data()[i].AccountNumber == undefined)) {
                                    grid1.editCell($("#gridBulkPurchasing td:eq(0)"));
                                    break;
                                }
                                if ((grid1.dataSource.data()[i].ShipToName == "Select" || grid1.dataSource.data()[i].ShipToName == "" || grid1.dataSource.data()[i].ShipToName == undefined || grid1.dataSource.data()[i].ShipToName == null)) {
                                    grid1.editCell($("#gridBulkPurchasing td:eq(1)"));
                                }
                                break;
                            }
                        }
                    }
                    var validator = $scope.kendoValidator("gridBulkPurchasing");

                    if (grid1 !== undefined && grid1 !== null && grid1.dataSource.hasChanges()) {

                        if (!validator.validate()) {
                            return false;
                        }
                        return true;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true;
                }
            }
            $scope.Validateshipviagrid = function () {
                if ($scope.Vendor.ShipVia) {
                    var grid = $("#gridShipVia").data("kendoGrid");
                    var rowEdit = $('#gridShipVia').find('.k-grid-edit-row');
                    if (!rowEdit.length) {
                        for (var i = 0; i < grid.dataSource.data().length; i++) {
                            if ((grid.dataSource.data()[i].ShipViaAccountNo == "" || grid.dataSource.data()[i].ShipViaAccountNo == null || grid.dataSource.data()[i].ShipViaAccountNo == undefined) || (grid.dataSource.data()[i].Carrier == "" || grid.dataSource.data()[i].Carrier == undefined || grid.dataSource.data()[i].Carrier == null)) {
                                grid.editRow($('#gridShipVia tr:has(td):eq(' + i + ')'));
                                if ((grid.dataSource.data()[i].Carrier == "" || grid.dataSource.data()[i].Carrier == undefined || grid.dataSource.data()[i].Carrier == null)) {
                                    grid.editCell($("#gridShipVia td:eq(0)"));
                                    break;
                                }

                                if ((grid.dataSource.data()[i].ShipViaAccountNo == "" || grid.dataSource.data()[i].ShipViaAccountNo == null || grid.dataSource.data()[i].ShipViaAccountNo == undefined))
                                    grid.editCell($("#gridShipVia td:eq(1)"));
                                break;
                            }
                        }
                    }
                    var validator = $scope.kendoValidator("gridShipVia");

                    if (grid !== undefined && grid !== null && grid.dataSource.hasChanges()) {

                        if (!validator.validate()) {
                            return false;
                        }
                        return true;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true;
                }
            }

            //save the vendor
            $scope.SaveVendor = function (operation) {
                $scope.Vendoradd.formAddressValidation.zipCode.$commitViewValue(true);
                $scope.vendorvalidate = 0;
                $scope.vendorsubmit = true;

               

                //added for validation
                var otherthing = $scope.Vendoradd.$error;
                if ($scope.Vendoradd.$invalid) {
                    return;
                }

                if ($scope.Vendor.IsAlliance && !$scope.gridvalidate())
                    return;
                
                if ($scope.Vendor.BulkPurchase && !$scope.ValidateBpgrid())
                    return;

                //if (!$scope.Validateshipviagrid()) {
                //    return;
                //}
                var dto = $scope.Vendor;

                var modalId = 'error';
                var grid = $("#gridShipLocation").data("kendoGrid").dataSource;
                var data = grid._data[0];
                if (grid !== undefined && grid !== null && grid.data() != null && grid.hasChanges()) {
                    $scope.Vendor.Shipvalue = grid._data;
                }

                //xmpo
                if (document.getElementById("gridBulkPurchasing")) {
                var grid2 = $("#gridBulkPurchasing").data("kendoGrid").dataSource;
                var data2 = grid2._data[0];
                dto.BPAccountNo = data2.AccountNumber;
                if (data2.ShipToName == "Select")
                    dto.BPShipLocation = 0;
                else
                    dto.BPShipLocation = data2.ShipToLocation;

                if (document.getElementById("BPAccountNo"))
                    dto.BPAccountNo = document.getElementById("BPAccountNo").value;
                }


                //var grid3 = $("#gridShipVia").data("kendoGrid").dataSource;
                //var data3 = grid3._data[0];                    
                //dto.Carrier = data3.Carrier;
                //dto.ShipViaAccountNo = data3.ShipViaAccountNo;
                //if (document.getElementById("Carrier"))
                //    dto.Carrier = document.getElementById("Carrier").value;
                //if (document.getElementById("ShipViaAccountNo"))
                //    dto.ShipViaAccountNo = document.getElementById("ShipViaAccountNo").value;
                //xmpo

                if (document.location.href.toString().indexOf("/vendor/") > 0) {
                    if (dto.VendorType == 3) {
                        $http.get('/api/lookup/0/ChckLocalVendor/?value=' + dto.Name).then(function (response) {
                            var check = response.data;
                            if (check != null && dto.VendorIdPk != check.VendorIdPk && dto.Name == check.Name) {

                                HFC.DisplayAlert("Vendor name already exists");
                                $scope.Vendor.Name = $scope.PrevName;
                                return;
                            }

                            if (dto.VendorId == undefined) {
                                dto.VendorId = $scope.vendorId;
                            }
                            saveVendorHandler(dto);
                        })
                    }
                    else {
                        if (dto.VendorId == undefined) {
                            dto.VendorId = $scope.vendorId;
                        }
                        saveVendorHandler(dto);
                    }
                }
                else {
                    if (dto.VendorName == "My Vendor") {
                        $http.get('/api/lookup/0/ChckLocalVendor/?value=' + dto.Name).then(function (response) {
                            var check = response.data;
                            if (check != null) {
                                HFC.DisplayAlert("Vendor Name already exists");

                                return;
                            }

                            if (dto.VendorId == undefined) {
                                dto.VendorId = $scope.vendorId;
                            }
                            saveVendorHandler(dto);
                        })
                    }
                    else {

                        if (dto.VendorId == undefined) {
                            dto.VendorId = $scope.vendorId;
                        }
                        saveVendorHandler(dto);
                    }
                }

            }

            function saveVendorHandler(dto) {
                validateAddress(dto.VendorAddress)
                    .then(function (response) {
                        var objaddress = response.data;

                        if (objaddress.Status == "error") {
                            //TP-1793: CLONE - The request failed with HTTP status 503: Service Temporarily Unavailable.
                            AfterValidateAddress(dto);
                            //HFC.DisplayAlert(objaddress.Message);
                            //$scope.IsBusy = false;
                        } else if (objaddress.Status == "true") {
                            var validAddress = objaddress.AddressResults[0];
                            dto.VendorAddress.Address1 = validAddress.address1;
                            $scope.Addressvalues = dto.VendorAddress.Address1;
                            dto.VendorAddress.Address2 = validAddress.address2;
                            dto.VendorAddress.City = validAddress.City;
                            dto.VendorAddress.State = validAddress.StateOrProvinceCode;

                            // Update that the address validated to true.
                            dto.VendorAddress.IsValidated = true;

                            // assign the zipcode back to the model before saving.
                            if (dto.VendorAddress.CountryCode2Digits == "US") {
                                dto.VendorAddress.ZipCode = validAddress.PostalCode.substring(0, 5);
                            }
                            else {
                                dto.VendorAddress.ZipCode = validAddress.PostalCode;
                            }
                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                            AfterValidateAddress(dto);
                        } else if (objaddress.Status == "false") {

                            dto.VendorAddress.IsValidated = false;

                            var errorMessage = "Invalid Address. Do you wish to continue?";
                            $scope.Addressvalues = dto.VendorAddress.Address1;
                            //address get empty so assiging the value here.
                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);

                            if (confirm(errorMessage)) {
                                AfterValidateAddress(dto);
                            }
                            else {
                                $scope.IsBusy = false;
                            }
                        } else { //SkipValidation
                            AfterValidateAddress(dto);
                        }
                    },
                    function (response) {
                        HFC.DisplayAlert(response.statusText);
                        $scope.IsBusy = false;
                    });
            };

            function AfterValidateAddress(dto) {

                $scope.Vendor.CurrencyValueCode = dto.Currency;

                //code for spacing in Canada Postal/ZipCode
                if (!dto.VendorAddress.IsValidated) {
                    if (dto.VendorAddress.CountryCode2Digits == "CA") {
                        if (dto.VendorAddress.ZipCode) {
                            var Ca_Zipcode = dto.VendorAddress.ZipCode;
                            Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                            dto.VendorAddress.ZipCode = Ca_Zipcode;
                        }
                    }
                }

                //code to remove format in phone number.. spl for Vendor type 1,2,4
                if (dto.AccountRepPhone)
                    var Phonenumber = dto.AccountRepPhone.replace(/\D+/g, "");
                dto.AccountRepPhone = Phonenumber;

                $http.post('/api/Vendor', dto).then(function (response) {
                    $scope.vendorsubmit = true;
                    $scope.IsBusy = false;
                    if (response.data != null && response.data !== undefined && response.data.data !== 'Success') {
                        var msg = "Vendor/Customer relationship does not exist for </ br><ul>" +
                            response.data.data +
                            "</ul></ br> Please contact the vendor to verify the account information and status.";
                        HFC.DisplayAlert(msg);
                    }

                    if (dto.VendorIdPk != '' || dto.VendorIdPk != null || dto.VendorIdPk != 0) {
                        HFC.DisplaySuccess("Vendor Modified");
                    }
                    else {
                        HFC.DisplaySuccess("Vendor created");
                    }

                    if ($scope.RouteUrl.Guid) {
                        var UrlId = $scope.RouteUrl.Guid;
                        $scope.Vendoradd.$setPristine();
                        window.location.href = '#!/productVendor/' + UrlId;
                    }

                    else {
                        if (dto.VendorType == 1 && dto.VendorStatus == 1 && dto.IsAlliance == false) {
                            var IdValue = dto.VendorId;
                            $http.get('/api/Vendor/' + IdValue + '/GetProductValue').then(function (response) {
                                $scope.ProductDisplay = response.data;
                                if ($scope.ProductDisplay.length != 0) {
                                    $scope.Vendoradd.$setPristine();
                                    window.location.href = '#!/vendorview/' + dto.VendorId;
                                }
                                else {
                                    $scope.Vendoradd.$setPristine();
                                    $("#Redirect_PopUp").modal("show");
                                    return;
                                }
                            });

                        }
                        else {
                            if (response.data != null &&
                                response.data !== undefined &&
                                response.data.data === 'Success') {
                                $scope.Vendoradd.$setPristine();
                                window.location.href = '#!/vendorview/' + response.config.data.VendorId;
                            }

                        }
                    }


                });
            };

            function validateAddress(obj) {
                var addressModeltp = {
                    address1: obj.Address1,
                    address2: obj.Address2,
                    City: obj.City,
                    StateOrProvinceCode: obj.State,
                    PostalCode: obj.ZipCode,
                    CountryCode: obj.CountryCode2Digits
                };

                if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                    var result = {
                        data: {
                            Status: "SkipValidation",
                            Message: "No Address validation required."
                        },
                        status: 200,
                        statusText: "OK"
                    }

                    return $q.resolve(result);;
                }

                // Call the api to Return the result.
                return AddressValidationService.validateAddress(addressModeltp);
            }

            //Country change certain values to be empty
            $scope.CountryChanged = function () {
                $scope.Vendor.VendorAddress.City = '';
                $scope.Vendor.VendorAddress.State = '';
                $scope.Vendor.VendorAddress.ZipCode = '';

            }

            //auto populate the id for vendors vendor
            if (document.location.href.toString().indexOf("/vendor/")) {
                if ($scope.vendorId == null || $scope.vendorId == 0) {
                    $http.get('/api/Vendor/0/spVendor_New').then(function (response) {

                        $scope.Vendor.VendorId = response.data;
                        $scope.Vendor.VendorName = "My Vendor";
                        $scope.Vendor.RetailBasis = "Cost";
                        $scope.PreviousId = response.data;
                        $scope.Vendor.VendorAddress = $scope.VendorAddress;
                        $scope.GetShipToLocation();
                    });

                }
            }


            //to fetch country value
            $http({ method: 'GET', url: '/api/address/', params: { Id: 0, includeStates: true } })
                   .then(function (response) {
                       $scope.AddressService.States = response.data.States || [];
                       $scope.AddressService.Countries = response.data.Countries || [];
                       $scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
                   }, function (response) {
                   });

            //based on zip code result state and city
            $scope.VendorAddressChanges = function () {
                if ($scope.Vendor.VendorAddress.ZipCode) {

                    $http.get('/api/lookup/0/zipcodes?q=' + $scope.Vendor.VendorAddress.ZipCode + "&country=" + $scope.Vendor.VendorAddress.CountryCode2Digits).then(function (response) {
                        $scope.IsBusy = false;
                        if (response.data.zipcodes.length > 0) {
                            $scope.Vendor.VendorAddress.City = response.data.zipcodes[0].City;
                            $scope.Vendor.VendorAddress.State = response.data.zipcodes[0].State;
                            $scope.Vendor.VendorAddress.Country = response.data.zipcodes[0].CountryCode;

                        };
                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                        $scope.IsBusy = false;
                    });
                }

            }

            //vendor status value fetch
            $http.get('/api/lookup/0/VendorStatus').then(function (response) {
                $scope.VendorStatusList = response.data;
            });



            //move to add product from vendor
            $scope.AddProduct = function () {
                window.location.href = '/#!/productCreate/' + $scope.vendorId;

            }

            $scope.EditProduct = function (Productkey, FranchiseId) {
                window.location.href = "#!/productDetail/" + Productkey + '/' + FranchiseId;

            }
            //to get currency code
            $scope.CurrencyValue = function (value) {
                if (value != null) {
                    $http.get('/api/lookup/0/GetCurrencyValue?value=' + value).then(function (response) {
                        $scope.Vendor.Currency = response.data.CurrencyCode;
                        $scope.Vendor.CurrencyId = response.data.CountryCodeId;
                    });

                }
                else {
                    $scope.Vendor.Currency = "";
                }
            }



            //to get saved product from vendor page
            if (document.location.href.toString().indexOf("/vendorview/") > 0) {
                var Value = $scope.vendorId
                if (Value > 0) {
                    $http.get('/api/Vendor/' + Value + '/GetProductValue').then(function (response) {
                        $scope.ProductDisplay = response.data;
                    });
                }
            }

            if (document.location.href.toString().indexOf("/vendor/") > 0) {
                var Value = $scope.vendorId
                if (Value > 0) {
                    $http.get('/api/Vendor/' + Value + '/GetProductValue').then(function (response) {
                        $scope.ProductDisplay = response.data;
                    });
                }
            }

            //Added for Address - State
            //TODO: Can we move this common service??
            $scope.getStates = function (countryISO) {
                if ($scope.StateList.length > 0 && countryISO) {
                    var sourcee = $.grep($scope.StateList, function (s) {
                        return s.CountryISO2 === countryISO;
                    });
                    if (sourcee) return sourcee;
                }

                return null;
            };
            // Populate the list of State
            // TODO: Can we move this to commonservice.
            var geturl = '/api/AdditionalAddress/0/GetState/';
            $http.get(geturl).then(function (responseSuccess) {
                $scope.StateList = responseSuccess.data;

            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });





            $scope.kendoValidator = function (gridId) {
                return $("#" + gridId).kendoValidator({
                    validate: function (e) {
                        $("span.k-invalid-msg").hide();
                        var dropDowns = $(".k-dropdown");
                        $.each(dropDowns, function (key, value) {
                            var input = $(value).find("input.k-invalid");
                            var span = $(this).find(".k-widget.k-dropdown.k-header");
                            if (input.size() > 0) {
                                $(this).addClass("dropdown-validation-error");
                            } else {
                                $(this).removeClass("dropdown-validation-error");
                            }
                        });
                    }
                }).getKendoValidator();
            }

            // QuoteList.Get(data);
            $scope.vendorService.Vendors_details = kendotooltipService.setColumnWidth($scope.vendorService.Vendors_details);
            window.onresize = function () {
                $scope.vendorService.Vendors_details = kendotooltipService.setColumnWidth($scope.vendorService.Vendors_details);
            };
            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 318);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 318);
                };
            });
            // End Kendo Resizing

            //for shipping in vendors
            $scope.GetShipToLocation = function () {
                $scope.GridEditable = false;
                $scope.ShipToLocationVendor = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    if ($scope.vendorId != undefined || $scope.vendorId != null) {
                                        var url1 = '/api/Shipping/0/GetValue?VendorId=' + $scope.vendorId + '&Status=' + "True";
                                    }
                                    else {

                                        var url1 = '/api/Shipping/0/GetValue?VendorId=' + $scope.Vendor.VendorId + '&Status=' + "True";
                                    }
                                    return url1;
                                },
                                dataType: "json"
                            }

                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },
                        batch: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {

                                Id: { editable: false },
                                    TerritoryName: { editable: false },
                                    ShipLocName: { editable: true },
                                    AccountNumber: {  editable: true },
                                    Addressvalue: { editable: false },
                                    IsAlliance: { editable: false }
                                }
                            }
                        }
                    },


                    columns: [
                        {


                            field: "Id",
                            title: "Shipping Id",
                            //filterable: { multi: true, search: true },
                            hidden: true,


                        },
                        {
                            field: "TerritoryName",
                            title: "Territories ID",


                        },
                         {
                             field: "AccountNumber",
                             title: "Account Number",
                             editor:
                                 function (container, options) {
                                     var req = "";
                                     if (options.model.IsAlliance)
                                         req = "required";

                                     $("<input " + req + " class='form-control frm_controllead1' name='Account Number' data-bind='value:AccountNumber' style='width: 150px;' />")
                                     .appendTo(container);
                                     //$('<input  required=' + req + ' name=" Account Number " data-bind="value:AccountNumber" style="width: 130px;border: none "/>')
                                     //    .appendTo(container);
                                 },
                             template: function (dataItem) {
                                 if (dataItem.AccountNumber != null)
                                     return "<span class='Grid_Textalign'>" + dataItem.AccountNumber + "</span>";
                                 else if (dataItem.IsAlliance) return '<span class="Grid_Textalign"><div style="color:red;">Required</div></span>';
                                 else
                                     return '';
                             },
                             hidden: false,

                         },
                        {
                            field: "ShipToName",
                            title: "Ship To Location",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem.ShipToName != null && dataItem.ShipToName != "Select")
                                    return dataItem.ShipToName;
                                else if (dataItem.IsAlliance)
                                    return '<div style="color: red; ">Required</div>';
                                else
                                    return '';
                            },
                            editor: function (container, options) {
                                var req = "";
                                if (options.model.IsAlliance)
                                    req = "required";

                                $('<input ' + req + ' id="ShipValuedrop" name=" Ship To Location " data-bind="value:ShipToLocation"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        valuePrimitive: true,
                                        autoBind: true,
                                        optionLabel: "Select",
                                        dataTextField: "ShipToName",
                                        dataValueField: "Id",
                                        template: "#=ShipToName#",

                                        change: function (e, options) {
                                            var value = "";
                                            var item = $('#gridShipLocation').find('.k-grid-edit-row');
                                            item.data().$scope.dataItem.ShipToName = e.sender.text();
                                            item.data().$scope.dataItem.Id = e.sender.value();
                                            item.data().$scope.dataItem.Addressvalue;
                                            for (var i = 0; i < e.sender.dataSource.data().length; i++) {
                                                if (e.sender.dataSource.data()[i].Id == e.sender.value()) {
                                                    item.data().$scope.dataItem.Addressvalue = e.sender.dataSource.data()[i].Addressvalue;
                                                }
                                            }

                                        },

                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: '/api/lookup/0/Type_ShipToLocVendor?TerritoryValue=' + options.model.TerritoryId,

                                                }
                                            },
                                            sort: { field: "ShipToLocation", dir: "asc" }
                                        },

                                    });

                            }


                        },
                        {
                            field: "Addressvalue",
                            title: "Address",
                            hidden: false,
                        }

                    ],

                    editable: true,
                    //filterable: true,
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    selectable: true,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }



                };

                $scope.BulkPurchasingVendor = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    if ($scope.vendorId != undefined || $scope.vendorId != null) {
                                        var url1 = '/api/Shipping/0/GetBPValue?VendorId=' + $scope.vendorId;
                                    }
                                    else {
                                        var grid = $("#gridBulkPurchasing").data("kendoGrid");
                                        grid.dataSource.add({ ShipLocName: "", AccountNumber: "", Addressvalue: "" });
                                        var url1 = '';
                                    }
                                    return url1;
                                },
                                dataType: "json"
                            }

                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },
                        batch: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    ShipLocName: { editable: true },
                                    AccountNumber: {  editable: true },
                                    Addressvalue: { editable: false },
                                }
                            }
                        }
                    },
                    dataBound: function (e) {
                        var dataSource = $("#gridBulkPurchasing").data("kendoGrid").dataSource;
                        if (dataSource._data.length == 0) {
                            var grid = $("#gridBulkPurchasing").data("kendoGrid");
                            grid.dataSource.add({ ShipLocName: "", AccountNumber: "", Addressvalue: "" });
                        }
                    },

                    columns: [
                         {
                             field: "AccountNumber",
                             title: "Account Number",
                             editor: function (container, options) {
                                 var req = '';
                                 if ($scope.Vendor.BulkPurchase)
                                     req = 'required';

                                 $('<input  id="BPAccountNo" ' + req + ' class="form-control frm_controllead1" name=" Account Number " data-bind="value:AccountNumber" style="width: 150px;border: none "/>')
                                     .appendTo(container);
                             },
                             template: function (dataItem) {
                                 if (dataItem.AccountNumber != null && dataItem.AccountNumber != 0) {
                                     return "<span class='Grid_Textalign'>" + dataItem.AccountNumber + "</span>";
                                 }
                                 else {
                                     dataItem.AccountNumber = '';
                                     return '';
                                 }
                             },
                             hidden: false,
                         },
                        {
                            field: "ShipToName",
                            title: "Ship To Location",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem.ShipToName != null && dataItem.ShipToName != "Select")
                                    return dataItem.ShipToName;
                                else
                                    return '';

                            },
                            editor: function (container, options) {
                                var req = '';
                                if ($scope.Vendor.BulkPurchase)
                                    req = 'required';

                                $('<input id="ShipValuedrop" ' + req + ' name=" ShipToLocation " data-bind="value:ShipToLocation"/>')
                                     .appendTo(container)
                                     .kendoDropDownList({
                                         valuePrimitive: true,
                                         autoBind: true,
                                         optionLabel: "Select",
                                         dataTextField: "ShipToName",
                                         dataValueField: "Id",
                                         template: "#=ShipToName#",

                                         change: function (e, options) {

                                             var value = "";
                                             var item = $('#gridBulkPurchasing').find('.k-grid-edit-row');
                                             item.data().$scope.dataItem.ShipToName = e.sender.text();
                                             item.data().$scope.dataItem.Id = e.sender.value();
                                             item.data().$scope.dataItem.Addressvalue;
                                             for (var i = 0; i < e.sender.dataSource.data().length; i++) {
                                                 if (e.sender.dataSource.data()[i].Id == e.sender.value()) {
                                                     item.data().$scope.dataItem.Addressvalue = e.sender.dataSource.data()[i].Addressvalue;
                                                 }
                                             }

                                         },

                                         dataSource: {
                                             transport: {
                                                 read: {
                                                     url: '/api/lookup/0/Type_ShipToLocBPVendor',
                                                 }
                                             },
                                             sort: { field: "ShipToLocation", dir: "asc" }
                                         },
                                     });
                            }
                        },
                        {
                            field: "Addressvalue",
                            title: "Address",
                            hidden: false,
                        }
                    ],
                    editable: true,
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                };

                $scope.ShipViaVendor = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    if ($scope.vendorId != undefined || $scope.vendorId != null) {
                                        var url1 = '/api/Vendor/' + $scope.vendorId + '/Get/';
                                    }
                                    else {
                                        var grid = $("#gridShipVia").data("kendoGrid");
                                        grid.dataSource.add({ Carrier: "", ShipViaAccountNo: "" });
                                        var url1 = '';
                                    }
                                    return url1;
                                },
                                dataType: "json"
                            }

                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },
                        batch: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    Carrier: { editable: true },
                                    ShipViaAccountNo: {  editable: true },
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            field: "Carrier",
                            title: "Carrier",
                            editor: function (container, options) {
                                var req = '';
                                if ($scope.Vendor.ShipVia) {
                                    req = 'required = "true"';
                                }

                                $('<input id="Carrier" ' + req + ' name=" Carrier " data-bind="value:Carrier" style="border: none " ng-blur="demo(Carrier)"/>')
                                    .appendTo(container);
                            },
                        },
                         {
                             field: "ShipViaAccountNo",
                             title: "Account Number",
                             editor: function (container, options) {
                                 var req = '';
                                 if ($scope.Vendor.ShipVia) {
                                     req = 'required = "true"';
                                 }

                                 $('<input  id="ShipViaAccountNo" ' + req + ' name=" Account Number " data-bind="value:ShipViaAccountNo" style="width: 130px;border: none "/>')
                                     .appendTo(container);
                             },
                             template: function (dataItem) {
                                 if (dataItem.ShipViaAccountNo == 0) {
                                     dataItem.ShipViaAccountNo = null;
                                     return '';
                                 }
                                 else if (dataItem.ShipViaAccountNo != null) {
                                     return "<span class='Grid_Textalign'>" + dataItem.ShipViaAccountNo + "</span>";
                                 }
                                 else {
                                     return '';
                                 }
                             },

                         }
                    ],
                    editable: true,
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                };

            };

            //for view of non editable
            $scope.GetShipToLocationView = function () {
                $scope.GridEditable = false;
                $scope.ShipToLocationVendorview = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    if ($scope.vendorId != undefined || $scope.vendorId != null) {
                                        var url1 = '/api/Shipping/0/GetValue?VendorId=' + $scope.vendorId + '&Status=' + "False";
                                    }
                                    else {

                                        var url1 = '/api/Shipping/0/GetValue?VendorId=' + $scope.Vendor.VendorId + '&Status=' + "False";
                                    }
                                    return url1;
                                },
                                dataType: "json"
                            }

                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },
                        batch: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {

                                    Id: { editable: false },
                                    TerritoryName: { editable: false },
                                    ShipLocName: { editable: false },
                                    AccountNumber: {  editable: false },
                                    Addressvalue: { editable: false }
                                }
                            }
                        }
                    },


                    columns: [
                        {


                            field: "Id",
                            title: "Shipping Id",
                            //filterable: { multi: true, search: true },
                            hidden: true,


                        },
                        {
                            field: "TerritoryName",
                            title: "Territories ID",


                        },
                         {
                             field: "AccountNumber",
                             title: "Account Number",
                             template: function (dataItem) {
                                 if (dataItem.AccountNumber)
                                     return "<span class='Grid_Textalign'>" + dataItem.AccountNumber + "</span>";
                                 else
                                     return "";
                             },
                             hidden: false,

                         },
                        {
                            field: "ShipToName",
                            title: "Ship To Location",
                            hidden: false,



                        },
                        {
                            field: "Addressvalue",
                            title: "Address",
                            hidden: false,
                        }

                    ],

                    editable: false,
                    //filterable: true,
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;

                    }


                };

                $scope.BulkPurchasingVendorview = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    if ($scope.vendorId != undefined || $scope.vendorId != null) {
                                        var url1 = '/api/Shipping/0/GetBPValue?VendorId=' + $scope.vendorId;
                                    }
                                    else {

                                        var url1 = '/api/Shipping/0/GetBPValue?VendorId=' + $scope.Vendor.VendorId;
                                    }
                                    return url1;
                                },
                                dataType: "json"
                            }

                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },
                        batch: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    ShipLocName: { editable: false },
                                    AccountNumber: { editable: false },
                                    Addressvalue: { editable: false }
                                }
                            }
                        }
                    },
                    dataBound: function (e) {
                        var dataSource = $("#gridBulkPurchasing").data("kendoGrid").dataSource;
                        if (dataSource._data.length == 0) {
                            var grid = $("#gridBulkPurchasing").data("kendoGrid");
                            grid.dataSource.add({ ShipLocName: "", AccountNumber: "", Addressvalue: "" });
                        }
                    },
                    columns: [
                         {
                             field: "AccountNumber",
                             title: "Account Number",
                             hidden: false,
                             template: function (dataItem) {
                                 if (dataItem.AccountNumber == 0 || dataItem.AccountNumber == null || dataItem.AccountNumber == undefined) {
                                     return '';
                                 }
                                 else {
                                     return "<span class='Grid_Textalign'>" + dataItem.AccountNumber + "</span>";
                                 }
                             },
                         },
                        {
                            field: "ShipToName",
                            title: "Ship To Location",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem.ShipToName == "Select" || dataItem.ShipToName == undefined) {
                                    return '';
                                }
                                else {
                                    return dataItem.ShipToName;
                                }
                            },
                        },
                        {
                            field: "Addressvalue",
                            title: "Address",
                            hidden: false,
                        }

                    ],

                    editable: false,
                    //filterable: true,
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }


                };

                $scope.ShipViaVendorview = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    if ($scope.vendorId != undefined || $scope.vendorId != null) {
                                        var url1 = '/api/Vendor/' + $scope.vendorId + '/Get/';
                                    }
                                    else {

                                        var url1 = '/api/Vendor/' + $scope.Vendor.VendorId + '/Get/';
                                    }
                                    return url1;
                                },
                                dataType: "json"
                            }

                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },
                        batch: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    Carrier: { editable: false },
                                    ShipViaAccountNo: {  editable: false }
                                }
                            }
                        }
                    },


                    columns: [
                        {
                            field: "Carrier",
                            title: "Carrier",
                        },
                         {
                             field: "ShipViaAccountNo",
                             title: "Account Number",
                             hidden: false,
                             template: function (dataItem) {
                                 if (dataItem.ShipViaAccountNo == 0) {
                                     return '';
                                 }
                                 else if (dataItem.ShipViaAccountNo != null) {
                                     return "<span class='Grid_Textalign'>" + dataItem.ShipViaAccountNo + "</span>";
                                 }
                                 else {
                                     return '';
                                 }
                             },
                         }
                    ],

                    editable: false,
                    //filterable: true,
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }


                };

            };


            //fetch particular data to view
            if (document.location.href.toString().indexOf("/vendorview/") > 0) {
                var VendorId = $scope.vendorId
                if (VendorId > 0) {
                    $http.get('/api/Vendor/' + VendorId + '/Get/').then(function (response) {
                        $scope.Vendor = response.data;

                        if (response.data.Currency == 0) {
                            $scope.Vendor.Currency = "";
                        }
                        if (response.data.Currency == 223) {
                            $scope.Vendor.Currency = "USD";

                        }
                        else if (response.data.Currency == 39) {
                            $scope.Vendor.Currency = "CAD";

                        }
                        else {
                            $scope.Vendor.Currency = "";
                        }
                        if (response.data.RetailBasis == null || response.data.RetailBasis == "") {
                            if (response.data.VendorType == 1) {
                                $scope.Vendor.RetailBasis = "Retail";
                            }
                            else if (response.data.VendorType == 2 || response.data.VendorType == 3) {
                                $scope.Vendor.RetailBasis = "Cost";
                            }

                        }
                        else {
                            $scope.Vendor.RetailBasis = response.data.RetailBasis;
                        }
                        if (response.data.VendorAddress != null) {
                            $scope.IsValidated = response.data.VendorAddress.IsValidated;
                        }

                        HFCService.setHeaderTitle("Vendor #" + $scope.Vendor.VendorId + "-" + $scope.Vendor.VendorName);
                    });


                }
                $scope.GetShipToLocationView();

            }






            //fetch the list based on vendor type
            $scope.ChangeVendor = function () {
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
                $('input[name="VendorCheck"]:checked').attr('checked', false);
                //var value = $scope.Vendor.VendorType;

                var dropselection = $('#VendorDrop').val();
                var value = dropselection;
                if (value == "") {
                    value = 0;
                }
                $http.get('/api/Vendor/' + value + '/VendorOption/').then(function (response) {
                    var data = response.data;
                    var grid = $("#gridVendor").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    //grid.dataSource.data(data);
                    //for displaying status in kendo grid filter
                    var ds = new kendo.data.DataSource({
                        data: data,
                        schema: {
                            model: {
                                fields: {

                                    VendorId: { type: "number" },

                                }
                            }
                        },
                    });
                    grid.setDataSource(ds);
                    if (dataSource._data.length == 0) {
                        grid.dataSource.data(data);
                    }
                    else if (dataSource._data.length > 25 || dataSource._data.length < 25) {
                        grid.dataSource.pageSize(25);
                    }
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                }).catch(function (e)
                {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                });
            }

            //to get all vendor value by clicking the checkbox
            $scope.GetallPrdtDetails = function () {
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
                var setvalue = $('input[name="VendorCheck"]:checked').val();
                if (setvalue == "on") {
                    var dropselection = $('#VendorDrop').val();
                    var value = "";
                    if (dropselection == "") {
                        value = 0;
                    }
                    else {
                        value = dropselection;
                    }
                    $http.get('/api/Vendor/0/GetVendorsClick?Dropvalue=' + value).then(function (response) {
                        var data = response.data;
                        var grid = $("#gridVendor").data("kendoGrid");
                        var dataSource = grid.dataSource;
                        dataSource._data = data;
                        //grid.dataSource.data(data);
                        //for displaying status in kendo grid filter
                        grid.setDataSource(data);
					kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);                    }).catch(function (e) {
                        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                    });
                }
                else if (setvalue == undefined) {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                    $scope.ChangeVendor();
                    
                }
                //kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }


            $scope.VendorgridSearchClear = function () {
                $('#searchBox').val('');
                $("#gridVendor").data('kendoGrid').dataSource.filter({
                });
            }
            $scope.NewVendor = function () {
                window.location.href = '/#!/vendor';
            }

            $scope.setAction = function () {

                $scope.SaveVendor();
            }



            $scope.cancel = function () {
                window.history.back();

            }

            $scope.vendoredit = function (vendorid) {


                window.location.href = '/#!/vendor/' + vendorid;
            }

            //edit the particular vendor
            if (document.location.href.toString().indexOf("/vendor/") > 0) {

                var vendorid = $scope.vendorId;
                $http.get('/api/Vendor/' + vendorid + '/Get/').then(function (response) {
                    //
                    $scope.Vendor = response.data;

                    $scope.PrevName = response.data.Name;
                    $scope.CreditLimitCurrencyControl.CurrenctFormatControl($scope.Vendor.CreditLimit);
                    //&& response.data.VendorAddress == null if needed can be added
                    if (response.data.AddressId == null) {
                        $scope.Vendor.VendorAddress = $scope.VendorAddress;

                    }
                    if (response.data.RetailBasis == null || response.data.RetailBasis == '') {
                        if (response.data.VendorName == "My Vendor" || response.data.VendorName == "NonVendorAliance") {
                            $scope.Vendor.RetailBasis = "Cost";
                        }
                        else {
                            $scope.Vendor.RetailBasis = "Retail";
                        }
                    }

                    if (response.data.Currency == 223) {
                        $scope.Vendor.Currency = "USD";
                        //$scope.Vendor.Currency = "$";
                    }
                    else if (response.data.Currency == 39) {
                        $scope.Vendor.Currency = "CAD";
                        //$scope.Vendor.Currency = "$";
                    }
                    if (response.data.Currency == 0) {
                        if (response.data.VendorAddress.CountryCode2Digits == "US") {
                            $scope.Vendor.Currency = "USD";
                            //$scope.Vendor.Currency = "$";
                        }
                        else {
                            $scope.Vendor.Currency = "CAD";
                            //$scope.Vendor.Currency = "$";
                        }
                    }
                    if (response.data.VendorType == 1) {
                        $scope.isDisabled = true;
                    }
                    else {
                        $scope.isDisabled = false;
                    }


                    // Need to set the initial value for the address one, because it is an 
                    // autocomplete component needs a spcial handling.
                    $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_',
                        $scope.Vendor.VendorAddress.Address1);
                    HFCService.setHeaderTitle("Vendor #" + $scope.Vendor.VendorId + "-" + $scope.Vendor.VendorName);
                });
                //window.location.href = '/#!/vendor/' + vendorid;
                $scope.GetShipToLocation();

                //}
            }
            $scope.deletevendor = function () {
                //alert = 'Do you want to delete this vendor?'
                window.location.href = '/#!/vendors';
            }


            $scope.VendorgridSearch = function () {
                var searchValue = $('#searchBox').val();
                $("#gridVendor").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [

                      {
                          field: "Name",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Location",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "AccountRep",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "AccountRepPhone",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "VendorId",
                          //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          operator: "eq",
                          value: searchValue
                      },
                      {
                          field: "OrderingMethodName",
                          operator: "contains",
                          value: searchValue
                      }
                    ]
                });

            }

            $scope.SortBy = 'createdonutc';
            $scope.SortIsDesc = true;
            $scope.ChangeSortOrder = function (sortBy) {
                if (sortBy) {
                    if (sortBy != $scope.SortBy) {
                        $scope.SortBy = sortBy;
                        $scope.SortIsDesc = true;
                    } else {
                        $scope.SortIsDesc = !$scope.SortIsDesc;

                    }
                    $scope.ApplyFilter();
                }
            }

            $scope.GoToFirstPage = function () {
                $scope.vendorService.Pagination.page = 1;
                $scope.ApplyFilter();
            }

            $scope.GoToPreviousPage = function () {
                if ($scope.vendorService.Pagination.page > 1) {
                    $scope.vendorService.Pagination.page = +($scope.vendorService.Pagination.page) - 1;
                    $scope.ApplyFilter();
                }
            }

            $scope.GoToNextPage = function () {

                if ($scope.vendorService.Pagination.page == $scope.vendorService.Pagination.pageTotal) {
                    return;
                }
                $scope.vendorService.Pagination.page = +($scope.vendorService.Pagination.page) + 1;
                $scope.ApplyFilter();
            }

            $scope.GoToLastPage = function () {
                $scope.vendorService.Pagination.page = $scope.vendorService.Pagination.pageTotal;
                $scope.ApplyFilter();
            }

            $scope.ChangePageSize = function () {
                $scope.ApplyFilter();
            }




            $scope.ApplyFilter = function (isClickedFromUI) {


                var data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };


                if (isClickedFromUI) {
                    data.pageIndex = 1;
                    $scope.vendorService.Pagination.page = 1;
                }

                if ($scope.SearchTerm) {
                    data.searchTerm = $scope.SearchTerm;
                }

                data.pageIndex = $scope.vendorService.Pagination.page;
                data.pageSize = $scope.vendorService.Pagination.size;

                data.ForSearchstartDate = $scope.ForSearchstartDate;
                data.ForSearchendDate = $scope.ForSearchendDate;



                data.selectedDateText = $scope.selectedDateText;



                if ($routeParams.commercialType) {
                    data.commercialType = $routeParams.commercialType;
                }

                if ($routeParams.isReportSearch) {
                    data.isReportSearch = $routeParams.isReportSearch;
                }
                vendorService.Get(data);

            }

            $scope.$on('$viewContentLoaded', function () {
                $location.replace(); //clear last history route
            });

            $scope.countWatchers = function () {
                var q = [$rootScope], watchers = 0, scope;
                while (q.length > 0) {
                    scope = q.pop();
                    if (scope.$$watchers) {
                        watchers += scope.$$watchers.length;
                    }
                    if (scope.$$childHead) {
                        q.push(scope.$$childHead);
                    }
                    if (scope.$$nextSibling) {
                        q.push(scope.$$nextSibling);
                    }
                }
                window.console.log(watchers);
            };

            function addressToString(isSingleLine, model) {
                if (model) {

                    var str = "";
                    if (model.Address1)
                        str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

                    if (model.City)
                        str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
                    else
                        str += (model.ZipCode || "");

                    return str;
                }
            };

            $scope.GetGoogAccountdressHref = function (model) {
                var dest = addressToString(true, model);
                if (model.Address1) {

                    var srcAddr;

                    if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                        srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                    } else {
                        srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                    }

                    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                        return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                    } else {
                        return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                    }
                } else
                    return null;
            }

            $scope.AddressToString = function (model) { return addressToString(true, model) };

            $scope.GetURLParameter = function (sParam) {
                var sPageURL = window.location.search.substring(1);
                var data = $.parseParams(window.location.href);
                if (data) {
                    return data[sParam];
                }
            };

            if ($routeParams.searchFilter != 'undefined') {
                $rootScope.searchMode = $routeParams.searchFilter;
            } else {
                $routeParams.searchFilter = 'Auto_Detect';
                $rootScope.searchMode = $routeParams.searchFilter;
            }




            $scope.customersDataSource = {
                transport: {
                    read: {
                        dataType: "jsonp",
                        url: "//demos.telerik.com/kendo-ui/service/Customers",
                    }
                }
            };
            //$scope.tooltipOptions = {
            //    filter: "td,th",
            //    position: "top",
            //    hide: function (e) {
            //        this.content.parent().css('visibility', 'hidden');
            //    },
            //    show: function (e) {
            //        if (this.content.text().length > 1) {
            //            if (this.content.text().trim() == "Edit QualifyAppointment Print") {
            //                this.content.parent().css('visibility', 'hidden');
            //            } else {
            //                this.content.parent().css('visibility', 'visible');
            //            }

            //        }
            //        else {
            //            this.content.parent().css('visibility', 'hidden');
            //        }
            //    },
            //    content: function (e) {

            //        return e.target.context.textContent;
            //    }
            //};

            $scope.customOptions = {



                valueTemplate: '<span class="selected-value" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span><span>{{dataItem.ContactName}}</span>',

                template: '<span class="k-state-default" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span>' +
                          '<span class="k-state-default"><h3>{{dataItem.ContactName}}</h3><p>{{dataItem.CompanyName}}</p></span>',
            };

            angular.element(document).ready(function () {
                //if ($routeParams.searchTerm)
                $('#search-box').val($routeParams.searchTerm);

                //if ($routeParams.searchFilter) {
                //    $rootScope.searchMode = 'searchFilter';
                //}

                if ($routeParams.searchFilter) {
                    var lis = $('#search-type').find('li');
                    lis.removeClass('active');
                    for (var i = 0; i < lis.length; i++) {
                        if ($(lis[i]).find('input').val() == $routeParams.searchFilter) {
                            $(lis[i]).addClass('active');
                        }
                    }

                    if ($routeParams.searchFilter != 'undefined') {
                        switch ($routeParams.searchFilter) {
                            case 'Customer_Name':
                                $rootScope.searchMode = 'Customer Name';
                                break;
                            case 'Number':
                                $rootScope.searchMode = 'Job/Invoice Number';
                                break;
                            case 'Phone_Number':
                                $rootScope.searchMode = 'Phone Number';
                                break;
                            default:
                                $rootScope.searchMode = $routeParams.searchFilter;
                        }
                    }
                    else {
                        $rootScope.searchMode = 'Auto_Detect';
                        $scope.searchFilter = 'Auto_Detect';
                        $routeParams.searchFilter = 'Auto_Detect';
                    }

                }

                $scope.ApplyFilter();
            });

            function Shipdiable() {

                var dropdownlist = $("#ShipValuedrop").data("kendoDropDownList");
                dropdownlist.enable(false);
                $('input[name="AccountNumber"]').prop('disabled', true);

            }

            $scope.vendorService.Get();

            $scope.ShipVia = function (value) {
                var grid = $("#gridShipVia").data("kendoGrid");
                var theDataSource = grid.dataSource;
                var updatedDataItem = getUpdatedDataItem();
                theDataSource.pushUpdate(updatedDataItem);

                if ($("#Carrier") && value === false) {
                    $("#Carrier").removeAttr('required');
                    $("#Carrier").removeClass("k-invalid");
                    $("#gridShipVia .k-widget.k-tooltip.k-tooltip-validation.k-invalid-msg").hide();
                }
                if ($("#ShipViaAccountNo") && value === false) {
                    $("#ShipViaAccountNo").removeAttr('required');
                    $("#ShipViaAccountNo").removeClass("k-invalid");
                    $("#gridShipVia .k-widget.k-tooltip.k-tooltip-validation.k-invalid-msg").hide();
                }


            }

            function getUpdatedDataItem() {
                var grid3 = $("#gridShipVia").data("kendoGrid").dataSource;
                var data3 = grid3._data[0];
                if (document.getElementById("Carrier")) {
                    var Carrier = document.getElementById("Carrier").value;
                }
                else {
                    var Carrier = data3.Carrier;
                }
                if (document.getElementById("ShipViaAccountNo")) {
                    var ShipViaAccountNo = document.getElementById("ShipViaAccountNo").value;
                }
                else {
                    var ShipViaAccountNo = data3.ShipViaAccountNo;
                }
                return {
                    Carrier: Carrier,
                    ShipViaAccountNo: ShipViaAccountNo
                };
            }

            $scope.BulkPurchase = function (value) {
                var grid = $("#gridBulkPurchasing").data("kendoGrid");
                var theDataSource = grid.dataSource;
                var updatedDataItem = getUpdatedDataItemBP();
                theDataSource.pushUpdate(updatedDataItem);

                if ($("#BPAccountNo") && value === false) {
                    $("#BPAccountNo").removeAttr('required');
                    $("#BPAccountNo").removeClass("k-invalid");
                    $("#gridBulkPurchasing .k-widget.k-tooltip.k-tooltip-validation.k-invalid-msg").hide();
                }

                if ($("#ShipValuedrop") && value === false) {
                    $("#ShipValuedrop").removeAttr('required');
                    $("#ShipValuedrop").removeClass("k-invalid");
                    $("#gridBulkPurchasing .k-widget.k-tooltip.k-tooltip-validation.k-invalid-msg").hide();
                }

            }

            function getUpdatedDataItemBP() {
                var grid2 = $("#gridBulkPurchasing").data("kendoGrid").dataSource;
                var data2 = grid2._data[0];
                if (document.getElementById("BPAccountNo")) {
                    var BPAccountNo = document.getElementById("BPAccountNo").value;
                }
                else {
                    var BPAccountNo = data2.AccountNumber;
                }
                if (data2.ShipToName == "Select") {
                    var BPShipLocation = 0;
                }
                else {
                    var BPShipLocation = data2.ShipToLocation;
                }
                return {
                    AccountNumber: BPAccountNo,
                    ShipToLocation: BPShipLocation
                };
            }

        }

    ]);
