﻿/***************************************************************************\
Module Name:  Settings MSR Controller  .js - AngularJS file
Project: HFC
Created on: 18 September 2019 Thursday
Created By:
Copyright:
Description: Settings MSR Controller
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app.controller('SettingsMsrReportController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
                    'HFCService','NavbarService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http,
        HFCService, NavbarService) {

        var ctrl = this;
        HFCService.setHeaderTitle("Msr Report #");

        $scope.HFCService = HFCService;
        $scope.FranchiseId = $routeParams.FranchiseId;
        $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsGeneralTab();

       //get year in drop down
        $scope.years = function () {
            var TPTStartYear = 2018;
            var Maxyear = new Date().getFullYear() + 1;
            var range = [];
            for (TPTStartYear; TPTStartYear <= Maxyear; TPTStartYear++) {
                range.push(TPTStartYear);
            }
            //var year = new Date().getFullYear()+1;
            //range.push(year);
            //range.push(year + 1);

            $scope.years = range;
        }

        $scope.MSRSettingsPermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'MSRReporting');

        //to fetch the territory value
        $scope.Getvalue = function () {
            $http.get('/api/Franchise/0/GetFeTerritories').then(function (data) {
                $scope.TerritoryValues = data.data;
                $scope.TerritoryValues.Disable = false;
                $scope.TerritoryValues.ButtonValue = "Save";
                for (i = 0; i < $scope.TerritoryValues.length; i++) {
                    if ($scope.TerritoryValues[i].MsrReportDate != null) {
                        $scope.TerritoryValues[i].Disable = true;
                        $scope.TerritoryValues[i].ButtonValue = "Locked";
                        var Datevalue = new Date($scope.TerritoryValues[i].MsrReportDate);
                        var Monthvalue = Datevalue.getMonth() + 1;
                        var Yearvalue = Datevalue.getFullYear();
                        $scope.TerritoryValues[i].MonthId = Monthvalue;
                        $scope.TerritoryValues[i].YearId = Yearvalue;
                    }
                    else {
                        $scope.TerritoryValues[i].Disable = false;
                        $scope.TerritoryValues[i].ButtonValue = "Save";
                    }
                }
            });
        }

        //get list of months in drop down
        $http.get('/api/Franchise/0/GetMonthsValue').then(function (data) {
            $scope.MonthValue = data.data;
        })

        //save call to the api.
        $scope.SaveMsrTerritory=function(value)
        {
                var datas = value;
                if (datas.MonthId != undefined && datas.YearId != undefined) {
                    var Month, year; var date = '01';
                    if (datas.MonthId < 10) {
                        Month = '0' + datas.MonthId;
                    }
                    else Month = datas.MonthId;
                    year = datas.YearId;
                    datas.MsrReportDate = year + '/' + Month + '/' + date;
                    $http.post('api/Franchise/0/UpdateTerritoryDate', datas).then(function (response) {
                        if (response.data == "Success") {
                            $scope.Getvalue();
                        }

                    });

                }
                else {
                    HFC.DisplayAlert("Select Month And Year Value");
                    return;
                }

        }

        $scope.Getvalue();
        $scope.years();

    }
]);

