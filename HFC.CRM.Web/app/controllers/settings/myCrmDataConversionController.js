﻿
// TODO: rquired refactoring:

'use strict';
app
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('myCrmDataConversionController',
    [
        '$scope', '$window', 'HFCService',  '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', 'NavbarService', '$q',

        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, NavbarService, $q) {

            $scope.HFCService = HFCService;
            $scope.NavbarService = NavbarService;
            $scope.NavbarService.SelectSettings();
            $scope.NavbarService.EnableSettingsMigration();
            $scope.ShowAcceptButton = true;
            
            HFCService.setHeaderTitle("MyCRM-TPT SourceMapping #");

            $scope.Permission = {};
            var MyCRMDataConversionpermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyCRMDataConversion')
            $scope.Permission.AddMyCRMData = MyCRMDataConversionpermission.CanCreate;
            $scope.Permission.EditMyCRMData = MyCRMDataConversionpermission.CanUpdate;
            $scope.Permission.DisplayMyCRMData = MyCRMDataConversionpermission.CanRead;

            //get touchpiont drop-down values for edit
            $scope.GetTouchPointData = function () {
                
                $http.get('/api/MyCRMSourceMapping/0/GetTPTSource').then(function (response) {
                    
                    var dropdownvalue = response.data;
                    $scope.CompleteValue = dropdownvalue;
                })
            }
            $scope.GetTouchPointData();

            //check the status and display edit/read only grid
            $scope.GetUiMappingStatus = function () {
                
                $http.get('/api/MyCRMSourceMapping/0/GetConfirmMyCRM_TPT_SourceMap').then(function (response) {
                    
                    var uiMappingStatus = response.data;
                    $scope.MappingStatus = uiMappingStatus.Status;
                    if ($scope.MappingStatus == false) {
                        $scope.GetSourceMappingvalue();
                        var grid = $("#gridmyCrmSourceMapping").data("kendoGrid");
                        if (grid != undefined) {
                            $("#gridmyCrmSourceMapping").data("kendoGrid").dataSource.read();
                            $("#gridmyCrmSourceMapping").data("kendoGrid").refresh();
                        }
                        
                    }
                    else {
                        $scope.GetSourceMappingViewvalue();
                        var grid = $("#gridmyCrmSourceMappingView").data("kendoGrid");
                        if (grid != undefined) {
                            $("#gridmyCrmSourceMappingView").data("kendoGrid").dataSource.read();
                            $("#gridmyCrmSourceMappingView").data("kendoGrid").refresh();
                        }
                    }
                })
            }
            $scope.GetUiMappingStatus();

            //grid on edit mode.
            $scope.GetSourceMappingvalue = function () {
                $scope.GridEditable = false;
                $scope.CrmSourceMapping = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    var url1 = '/api/MyCRMSourceMapping/' + 0 + '/GetmyCRMSource';
                                    return url1;
                                },
                            },
                            batch: true,
                        },
                        error: function (e) {
                            
                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {
                                id: "Id",
                                fields: {
                                    Id: { editable: false },
                                    Name: { editable: false },
                                    TPTSourceName: { editable: true },
                                }
                            }
                        }
                    },
                    dataBound: function (e) {
                        var grid = $("#gridmyCrmSourceMapping").data("kendoGrid");
                        grid.autoFitColumn(i);
                        //--    
                    },
                    columns: [
                        {
                            field: "Name",
                            title: "Your Previous Source",
                            filterable: { multi: true, search: true },
                            hidden: false,
                        },
                        {
                            field: "TPTSourceName",
                            title: "Touchpoint Source",
                           
                            filterable: { multi: true, search: true },
                            hidden: false,
                            editor: function (container, options) {
                                $('<input id="Status_val" name="TouchpointId" data-bind="value:TPTSourceID" style="width: 250px;border: none; "  />')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        autoBind: false,
                                        optionLabel: "Select",
                                        valuePrimitive: true,
                                        dataTextField: "Name",
                                        dataValueField: "Id",
                                        template: "#=Name #",
                                        change: function (e, options) {
                                            var item = $('#gridmyCrmSourceMapping').find('.k-grid-edit-row');
                                            item.data().uid
                                            var dataItem = item.data().$scope.dataItem;
                                            var grid = this;
                                            if (dataItem.TPTSourceID) {
                                                onStatusChangeValue(dataItem.TPTSourceID, dataItem.Id);
                                            }

                                        },
                                        dataSource: $scope.CompleteValue
                                 });
                            }
                        },
                    ],
                    editable:true,
                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],
                    },
                    filterable: true,
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    toolbar: [
                       {
                           template: kendo.template($("#headToolbarTemplate").html()),
                       }]
                    };
                };

            //grid on view mode.
            $scope.GetSourceMappingViewvalue = function () {
                $scope.GridMapping = false;
                $scope.CrmSourceMappingView = {
                    dataSource: {
                    transport: {
                            read: {
                                url: function (params) {
                                    var url1 = '/api/MyCRMSourceMapping/' + 0 + '/GetmyCRMSource';
                                    return url1;
                                },
                            },
                        },
                        error: function (e) {
                            
                            HFC.DisplayAlert(e.errorThrown);
                        },
                        schema: {
                            model: {
                                id: "",
                                fields: {
                                    Id: { editable: false },
                                    Name: { editable: false },
                                    TPTSourceName: { editable: false },
                                }
                            }
                        }
                    },
                    dataBound: function (e) {
                        var grid = $("#gridmyCrmSourceMappingView").data("kendoGrid");
                        grid.autoFitColumn(i);
                        //--    
                    },
                    columns: [
                        {
                            field: "Name",
                            title: "Your Previous Source",
                            filterable: { multi: true, search: true },
                            hidden: false,
                        },
                        {
                            field: "TPTSourceName",
                            title: "Touchpoint Source",
                            filterable: { multi: true, search: true },
                            hidden: false,
                         },
                    ],
                    editable: false,
                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],
                    },
                    filterable: true,
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    toolbar: [
                       {
                           template: kendo.template($("#headToolbarTemplateView").html()),
                       }]
                    };
                };

            $scope.selectedRow = '';

            //on change the drop down value
            function onStatusChangeValue(value, Id) {
                
                var data = $("#Status_val").data("kendoDropDownList");
                value = data.value();

                for (var i = 0; i < data.dataSource._data.length; i++)
                    if (data.dataSource._data[i].Id == value)
                        $scope.StatusvalDesc = data.dataSource._data[i].Name;

                
                var grid = $("#gridmyCrmSourceMapping").data("kendoGrid");
                var rowid = Id;
                
                
                var list = grid.dataSource._data;
                //getting the edit row id and assigning the selected value.
                $scope.selectedRow = rowid;
                
                var offset;
                for (var i = 0; i < list.length; i++) {
                    if (list[i]['Id'] == Id)
                        offset = i;
                }

                grid.dataSource._data[offset].TPTSourceName = $scope.StatusvalDesc;

                
                var grids = $("#gridmyCrmSourceMapping").data("kendoGrid");
                grid.refresh();

                CheckValue();
                return;
            }

            $scope.values = [];

            //check all value's selected and enable confirm button
            function CheckValue(){
                
                var grid = $("#gridmyCrmSourceMapping").data("kendoGrid");
                $scope.dataSource = grid.dataSource._data;
                $scope.ShowAcceptButton = false;
                
                angular.forEach($scope.dataSource, function (value, key) {
                    if (value.TPTSourceID == 0) {
                        $scope.ShowAcceptButton = true;
                        }
                            

                    });
                }

            //normal save
            $scope.SaveData = function () {
                
                $scope.values = [];
                var grid = $("#gridmyCrmSourceMapping").data("kendoGrid");
                $scope.dataSource = grid.dataSource._data;
                angular.forEach($scope.dataSource, function (value, key) {

                    $scope.values.push({ TPTSourceID: value.TPTSourceID, MyCRMSourceID: value.Id });

                });
                
                $http.post('/api/MyCRMSourceMapping/0/SavemyCRMSourcemapping', $scope.values).then(function (response) {
                    
                    if (response.data == "Success") {
                        $scope.GetUiMappingStatus();
                        $window.alert("Data Saved SuccessFully");
                        
                        return;
                    }
                    else {
                        HFC.DisplayAlert(response.data);
                        return;
                    }
                });
            }

          //confirmation save the data
            $scope.SaveCompleteData = function () {
                
                $scope.values=[];
                var grid = $("#gridmyCrmSourceMapping").data("kendoGrid");
                $scope.dataSource = grid.dataSource._data;
                var dto = $scope.values;
                angular.forEach($scope.dataSource, function (value, key) {

                    $scope.values.push({ TPTSourceID: value.TPTSourceID, MyCRMSourceID: value.Id });

                });
                
                
                $http.post('/api/MyCRMSourceMapping/0/SavemyCRMSourcemapping', $scope.values).then(function (response) {
                    
                    if (response.data == "Success") {
                        $http.get('/api/MyCRMSourceMapping/0/ConfirmMyCRM_TPT_SourceMap').then(function (response) {
                            
                            if (response.data == true) {
                                $scope.GetUiMappingStatus();
                                $window.alert("Data Saved SuccessFully");
                                return;
                            }
                            else {
                                HFC.DisplayAlert(response.data);
                                return;
                            }
                        });
                       
                    }
                    else {
                        HFC.DisplayAlert(response.data);
                        return;
                    }
                });
                
            }

            //search the value in grid - view mode
            $scope.SourcegridSearch = function () {
                {
                    var searchValue = $('#searchBox').val();

                    $("#gridmyCrmSourceMappingView").data("kendoGrid").dataSource.filter({
                        logic: "or",
                        filters: [
                         {
                              field: "Name",
                              operator: "contains",
                              value: searchValue
                          },
                          {
                              field: "TPTSourceName",
                              operator: "contains",
                              value: searchValue
                          },
                        ]
                    });
                }
            } //end

          
        }
          
    ])
