﻿app.controller('globalSearchController', ['$http', '$scope', '$rootScope',
    'NavbarService', 'HFCService', '$routeParams', '$location', 'calendarModalService', 'NoteServiceTP', 'PersonService', 'CalendarServiceTP', 'CalendarService', 'AdditionalContactServiceTP', 'AdditionalAddressServiceTP', 'CacheService', '$window', '$anchorScroll', 'kendotooltipService'
    , function ($http, $scope, $rootScope, NavbarService, HFCService, $routeParams, $location, calendarModalService, NoteServiceTP, PersonService, CalendarServiceTP, CalendarService, AdditionalContactServiceTP, AdditionalAddressServiceTP, CacheService, $window, $anchorScroll, kendotooltipService) {

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectHome();
        $scope.NavbarService.EnableDashboardTab();
        $scope.HFCService = HFCService;
        $scope.NoteServiceTP = NoteServiceTP;
        $scope.calendarModalService = calendarModalService;
        $scope.NoteServiceTP.FranchiseSettings = false; 
        $scope.PersonService = PersonService;
        $scope.AdditionalContactServiceTP = AdditionalContactServiceTP;
        $scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
        $scope.AdditionalAddressServiceTP.GlobalSearch = true;
        $scope.activeMenu = 'TopResults';
        $scope.NavbarService.showicon = true;
        $routeParams.value = decodeURIComponent($routeParams.value);
        $routeParams.value = decodeURIComponent($routeParams.value);
        $scope.type = $routeParams.type;
        $scope.value = $routeParams.value;
        //$scope.type = localStorage.getItem("globalSearchtype");
        //$scope.value = localStorage.getItem("globalSearchvalue");        
        //localStorage.clear();

        //permissions
        $scope.Permission = {};
        var Leadpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Lead')
        $scope.Permission.ListLead = Leadpermission.CanRead; 
        $scope.Permission.EditLead = Leadpermission.CanUpdate;
        $scope.Permission.DisplayLead = Leadpermission.CanRead;
    
        var Accountpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Account')
        $scope.Permission.ListAccount = Accountpermission.CanRead;
        $scope.Permission.EditAccount = Accountpermission.CanUpdate;
        $scope.Permission.DisplayAccount = Accountpermission.CanRead;

        var Opportunitypermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity')
        $scope.Permission.ListOpportunity = Opportunitypermission.CanRead;
        $scope.Permission.EditOpportunity = Opportunitypermission.CanUpdate;
        $scope.Permission.DisplayOpportunity = Opportunitypermission.CanRead;

        var SalesOrderpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SalesOrder')
        $scope.Permission.ListSalesOrder = SalesOrderpermission.CanRead;
        $scope.Permission.EditSalesOrder = SalesOrderpermission.CanUpdate;
        $scope.Permission.DisplaySalesOrder = SalesOrderpermission.CanRead;

        var Quotepermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Quote')
        $scope.Permission.QuoteList = Quotepermission.CanRead;
        $scope.Permission.QuoteEdit = Quotepermission.CanUpdate;
        $scope.Permission.QuoteDisplay = Quotepermission.CanRead;

        var ProcurementDashboardpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'ProcurementDashboard')
        $scope.Permission.ProcurementList = ProcurementDashboardpermission.CanRead;
        $scope.Permission.Procurementedit = ProcurementDashboardpermission.CanUpdate;
        $scope.Permission.ProcurementDisplay = ProcurementDashboardpermission.CanRead;

        var MyVendorspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyVendors')
        $scope.Permission.ListVendors = MyVendorspermission.CanRead;
        $scope.Permission.AddVendor = MyVendorspermission.CanCreate;
        $scope.Permission.EditVendor = MyVendorspermission.CanUpdate;
        $scope.Permission.DisplayVendor = MyVendorspermission.CanRead;

        var MyProductspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyProducts')

        var Calendarpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Calendar')
        $scope.Permission.ViewCalendar = Calendarpermission.CanRead;
        $scope.Permission.EditCalendar = Calendarpermission.CanUpdate;
        $scope.Permission.AddEvent = Calendarpermission.CanCreate;
        
        var ManageDocpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'ManageDocuments')
        $scope.Permission.ListDocument = ManageDocpermission.CanRead;
        $scope.Permission.DisplayDocument = ManageDocpermission.CanRead;
        $scope.Permission.EditDocument = ManageDocpermission.CanUpdate;

        var Measurementpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Measurement')
        $scope.Permission.DisplayMeasurements = Measurementpermission.CanRead; 
        $scope.Permission.AddMeasurements = Measurementpermission.CanCreate; 
        $scope.Permission.EditMeasurements = Measurementpermission.CanUpdate;
        $scope.Permission.ListMeasurements = Measurementpermission.CanRead;

        var Paymentpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Payment')
        $scope.Permission.AccountPayment = Paymentpermission.CanRead;

        $scope.$root.globaltype = $scope.type;
        $scope.$root.globalvalue = $scope.value;
        $scope.InProgress = true;
        $scope.Invalidsearch = false;
         
        $scope.GetLeadAccount = function () {
            $http.get("/api/GlobalSearch/0/GetLeads?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllLeads = data.data;
                $scope.AllLeadsCount = data.data.length;
                $scope.FiveLeads = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveLeads[i] = data.data[i];
                }
                $('#LeadsId').data('kendoGrid').setDataSource($scope.FiveLeads);
                $scope.InProgress = false;
            }).catch(function (error) {
                $scope.InProgress = false;
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
            $http.get("/api/GlobalSearch/0/GetAccounts?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllAccounts = data.data;
                $scope.AllAccountsCount = data.data.length;
                $scope.FiveAccounts = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveAccounts[i] = data.data[i];
                }
                $('#AccountsId').data('kendoGrid').setDataSource($scope.FiveAccounts);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetOpportunities = function () {
            $http.get("/api/GlobalSearch/0/GetOpportunities?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllOpportunities = data.data;
                $scope.AllOpportunitiesCount = data.data.length;
                $scope.FiveOpportunities = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveOpportunities[i] = data.data[i];
                }
                $('#OpportunitiesId').data('kendoGrid').setDataSource($scope.FiveOpportunities);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetContacts = function () {
            
            $http.get("/api/GlobalSearch/0/GetContacts?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllContacts = data.data;
                $scope.AllContactsCount = data.data.length;
                $scope.FiveContacts = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveContacts[i] = data.data[i];
                }
                if ($scope.activeMenu == 'ContactsId')
                    $('#ContactsId').data('kendoGrid').setDataSource($scope.AllContacts);
                else
                    $('#ContactsId').data('kendoGrid').setDataSource($scope.FiveContacts);
                return "true";
                
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetQuotes = function () {
            $http.get("/api/GlobalSearch/0/GetQuotes?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllQuotes = data.data;
                $scope.AllQuotesCount = data.data.length;
                $scope.FiveQuotes = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveQuotes[i] = data.data[i];
                }
                $('#QuotesId').data('kendoGrid').setDataSource($scope.FiveQuotes);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetOrders = function () {
            $http.get("/api/GlobalSearch/0/GetOrders?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllOrders = data.data;
                $scope.AllOrdersCount = data.data.length;
                $scope.FiveOrders = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveOrders[i] = data.data[i];
                }
                $('#OrdersId').data('kendoGrid').setDataSource($scope.FiveOrders);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetMpoVpos = function () {
            $http.get("/api/GlobalSearch/0/GetMpoVpos?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllMpoVpos = data.data;
                $scope.AllMpoVposCount = data.data.length;
                $scope.FiveMpoVpos = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveMpoVpos[i] = data.data[i];
                }
                $('#MPOsId').data('kendoGrid').setDataSource($scope.FiveMpoVpos);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetShipments = function () {
            $http.get("/api/GlobalSearch/0/GetShipments?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllShipments = data.data;
                $scope.AllShipmentsCount = data.data.length;
                $scope.FiveShipments = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveShipments[i] = data.data[i];
                }
                $('#ShipmentsId').data('kendoGrid').setDataSource($scope.FiveShipments);
                if ($scope.$root.globaltype != 'All')
                $scope.InProgress = false;
            }).catch(function (error) {
                if ($scope.$root.globaltype != 'All')
                $scope.InProgress = false;
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetCases = function () {
            $http.get("/api/GlobalSearch/0/GetCases?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllCases = data.data;
                $scope.AllCasesCount = data.data.length;
                $scope.FiveCases = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveCases[i] = data.data[i];
                }
                $('#CasesId').data('kendoGrid').setDataSource($scope.FiveCases);
                $scope.InProgress = false;
            }).catch(function (error) {
                $scope.InProgress = false;
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetAppointments = function () {
            $http.get("/api/GlobalSearch/0/GetAppointments?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllAppointments = data.data;
                $scope.AllAppointmentsCount = data.data.length;
                $scope.FiveAppointments = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveAppointments[i] = data.data[i];
                }
                if ($scope.activeMenu == 'AppointmentsId')
                    $('#AppointmentsId').data('kendoGrid').setDataSource($scope.AllAppointments);
                else
                $('#AppointmentsId').data('kendoGrid').setDataSource($scope.FiveAppointments);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetTasks = function () {
            $http.get("/api/GlobalSearch/0/GetTasks?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllTasks = data.data;
                $scope.AllTasksCount = data.data.length;
                $scope.FiveTasks = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveTasks[i] = data.data[i];
                }
                if ($scope.activeMenu == 'TasksId')
                    $('#TasksId').data('kendoGrid').setDataSource($scope.AllTasks);
                else
                $('#TasksId').data('kendoGrid').setDataSource($scope.FiveTasks);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetAddress = function () {
            $http.get("/api/GlobalSearch/0/GetAddress?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllAddress = data.data;
                $scope.AllAddressCount = data.data.length;
                $scope.FiveAddress = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveAddress[i] = data.data[i];
                }
                if ($scope.activeMenu == 'AddressesId')
                    $('#AddressesId').data('kendoGrid').setDataSource($scope.AllAddress);
                else
                $('#AddressesId').data('kendoGrid').setDataSource($scope.FiveAddress);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetPayments = function () {
            $http.get("/api/GlobalSearch/0/GetPayments?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllPayments = data.data;
                $scope.AllPaymentsCount = data.data.length;
                $scope.FivePayments = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FivePayments[i] = data.data[i];
                }
                $('#PaymentsId').data('kendoGrid').setDataSource($scope.FivePayments);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetVendors = function () {
            
            $http.get("/api/GlobalSearch/0/GetVendors?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllVendors = data.data;
                $scope.AllVendorsCount = data.data.length;
                $scope.FiveVendors = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveVendors[i] = data.data[i];
                }
                $('#VendorsId').data('kendoGrid').setDataSource($scope.FiveVendors);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        $scope.GetNotes = function () {
            $http.get("/api/GlobalSearch/0/GetNotes?type=" + $scope.type + "&value=" + $scope.value).then(function (data) {
                $scope.AllNotes = data.data;
                $scope.AllNotesCount = data.data.length;
                $scope.FiveNotes = new Array();
                for (var i = 0; i < 5; i++) {
                    if (data.data[i] == null)
                        break;
                    $scope.FiveNotes[i] = data.data[i];
                }
                if ($scope.activeMenu == 'NotesId')
                    $('#NotesId').data('kendoGrid').setDataSource($scope.AllNotes);
                else
                    $('#NotesId').data('kendoGrid').setDataSource($scope.FiveNotes);
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }

        $scope.EndProcess = function () {
            //$scope.InProgress = false;
        }
       
        $scope.ShowSelectedGrid = function (type) {
            $scope.value = $scope.value.replace(/#/g, "%23");
            if (type == 'All') {
                $scope.DisplayAll = true;
                $scope.DisplayPhone = false;
                $scope.DisplayEmail = false;
                $scope.DisplayName = false;
                $scope.DisplayAddress = false;
                $scope.DisplayCity = false;
                $scope.DisplayID = false;
                Promise.all([$scope.GetNotes(), $scope.GetVendors(), $scope.GetPayments(), $scope.GetAddress(), $scope.GetTasks(), $scope.GetAppointments(), $scope.GetCases(), $scope.GetShipments(), $scope.GetMpoVpos(), $scope.GetOrders(), $scope.GetQuotes(), $scope.GetContacts(), $scope.GetOpportunities(), $scope.GetLeadAccount()]).then(values => {
                    
                });
            }
            else if (type == 'Phone') {
                $scope.DisplayPhone = true;
                $scope.DisplayEmail = false;
                $scope.DisplayName = false;
                $scope.DisplayAddress = false;
                $scope.DisplayCity = false;
                $scope.DisplayID = false;
                $scope.DisplayAll = false;
                Promise.all([$scope.GetContacts(), $scope.GetVendors(), $scope.GetLeadAccount()]).then(values => {

                });
            }
            else if (type == 'Email') {
                $scope.DisplayEmail = true;
                $scope.DisplayName = false;
                $scope.DisplayAddress = false;
                $scope.DisplayCity = false;
                $scope.DisplayID = false;
                $scope.DisplayPhone = false;
                $scope.DisplayAll = false;
                Promise.all([$scope.GetContacts(), $scope.GetVendors(), $scope.GetLeadAccount()]).then(values => {

                });
            }
            else if (type == 'Name') {
                $scope.DisplayName = true;
                $scope.DisplayAddress = false;
                $scope.DisplayCity = false;
                $scope.DisplayID = false;
                $scope.DisplayPhone = false;
                $scope.DisplayEmail = false;
                $scope.DisplayAll = false;
                Promise.all([$scope.GetAddress(), $scope.GetContacts(), $scope.GetVendors(), $scope.GetPayments(), $scope.GetOpportunities(), $scope.GetQuotes(), $scope.GetOrders(), $scope.GetAppointments(), $scope.GetTasks(), $scope.GetNotes(), $scope.GetLeadAccount()]).then(values => {

                });
            }
            else if (type == 'Address') {
                $scope.DisplayAddress = true;
                $scope.DisplayCity = false;
                $scope.DisplayID = false;
                $scope.DisplayPhone = false;
                $scope.DisplayEmail = false;
                $scope.DisplayName = false;
                $scope.DisplayAll = false;
                Promise.all([$scope.GetOpportunities(), $scope.GetVendors(), $scope.GetAddress(), $scope.GetLeadAccount()]).then(values => {

                });
            }
            else if (type == 'City') {
                $scope.DisplayCity = true;
                $scope.DisplayID = false;
                $scope.DisplayPhone = false;
                $scope.DisplayEmail = false;
                $scope.DisplayName = false;
                $scope.DisplayAddress = false;
                $scope.DisplayAll = false;
                Promise.all([$scope.GetOpportunities(), $scope.GetVendors(), $scope.GetAddress(), $scope.GetLeadAccount()]).then(values => {

                });
            }
            else if (type == 'ID') {
                $scope.DisplayID = true;
                $scope.DisplayCity = false;
                $scope.DisplayPhone = false;
                $scope.DisplayEmail = false;
                $scope.DisplayName = false;
                $scope.DisplayAddress = false;
                $scope.DisplayAll = false;
                Promise.all([$scope.GetOpportunities(), $scope.GetQuotes(), $scope.GetOrders(), $scope.GetVendors(), $scope.GetPayments(), $scope.GetMpoVpos(), $scope.GetCases(), $scope.GetShipments()]).then(values => {

                });

            }
        }

        if (($scope.type == null || $scope.type == undefined) && ($scope.value == null || $scope.value == "undefined")) {
            $scope.$root.globaltype = 'All';
            $scope.$root.globalvalue = '';
            $scope.Newglobalsearch = true;
        }
        else if ($scope.type != null && ($scope.value == null || $scope.value == undefined))
        {
            $scope.InProgress = false;
           // $scope.Invalidsearch = true;
        }
        else 
        {
            $scope.Newglobalsearch = false;
            $scope.ShowSelectedGrid($scope.type);
        }
        
        //$scope.$on("RefreshGrids", function (e, newtype, newvalue) {
        //    newvalue = decodeURIComponent(newvalue);
        //    newvalue = decodeURIComponent(newvalue);
        //    if (newvalue != $scope.value || newtype != $scope.type)
        //    {
        //        //if (newvalue != null)
        //        //    var count1 = newvalue.length;

        //        if (newvalue == null || newvalue == undefined) {
        //            $scope.type = newtype;
        //            $scope.value = newvalue;
        //            $scope.EmptyGrids();
        //            $scope.InProgress = false;
        //            //$scope.Invalidsearch = true;
        //            $scope.Newglobalsearch = false;
        //        }
        //        else {
        //            //localStorage.setItem("globalSearchtype", newtype);
        //            //localStorage.setItem("globalSearchvalue", newvalue);
        //            $scope.Invalidsearch = false;
        //            $scope.InProgress = true;
        //            $scope.Newglobalsearch = false;
        //            $scope.activeMenu = 'TopResults';
        //            $scope.ShowLess = " ";
        //            $scope.type = newtype;
        //            $scope.value = newvalue;
        //            $scope.EmptyGrids();
        //            $scope.GridCollapse = false;
        //            //$scope.ShowSelectedGrid($scope.type);
        //        }
        //    }
        //});

        $scope.EmptyGrids = function () {
            $("#LeadsId").data('kendoGrid').dataSource.data([]);
            $("#AccountsId").data('kendoGrid').dataSource.data([]);
            $("#OpportunitiesId").data('kendoGrid').dataSource.data([]);
            $("#ContactsId").data('kendoGrid').dataSource.data([]);
            $("#QuotesId").data('kendoGrid').dataSource.data([]);
            $("#OrdersId").data('kendoGrid').dataSource.data([]);
            $("#MPOsId").data('kendoGrid').dataSource.data([]);
            $("#ShipmentsId").data('kendoGrid').dataSource.data([]);
            $("#CasesId").data('kendoGrid').dataSource.data([]);
            $("#AppointmentsId").data('kendoGrid').dataSource.data([]);
            $("#TasksId").data('kendoGrid').dataSource.data([]);
            $("#AddressesId").data('kendoGrid').dataSource.data([]);
            $("#PaymentsId").data('kendoGrid').dataSource.data([]);
            $("#VendorsId").data('kendoGrid').dataSource.data([]);
            $("#NotesId").data('kendoGrid').dataSource.data([]);
        }
        
        $scope.Selectactive = function (option) {
              
            if ($scope.activeMenu != 'TopResults')
            {
                var grid = $("#" + $scope.activeMenu).data("kendoGrid");
                if (grid) {
                    $scope.GridCollapse = false;
                    $scope.SortFive($scope.activeMenu);
                    $scope.onDataBinding($scope.activeMenu);
                }
                $scope.ShowLess = " ";
                
            }

            $scope.activeMenu = option;

            if (option != 'TopResults') {
                var grid = $("#" + option).data("kendoGrid");
                if (grid) {
                    $scope.ShowAll(option);
                }
                $scope.ShowLess = option;
                $scope.GridCollapse = true;
            }
            $anchorScroll();
        }

        $scope.SortAll = function (gridid) {
            if (gridid == "LeadsId")
                $('#LeadsId').data('kendoGrid').setDataSource($scope.AllLeads);
            else if (gridid == "AccountsId")
                $('#AccountsId').data('kendoGrid').setDataSource($scope.AllAccounts);
            else if (gridid == "OpportunitiesId")
                $('#OpportunitiesId').data('kendoGrid').setDataSource($scope.AllOpportunities);
            else if (gridid == "ContactsId")
                $('#ContactsId').data('kendoGrid').setDataSource($scope.AllContacts);
            else if (gridid == "QuotesId")
                $('#QuotesId').data('kendoGrid').setDataSource($scope.AllQuotes);
            else if (gridid == "OrdersId")
                $('#OrdersId').data('kendoGrid').setDataSource($scope.AllOrders);
            else if (gridid == "MPOsId")
                $('#MPOsId').data('kendoGrid').setDataSource($scope.AllMpoVpos);
            else if (gridid == "ShipmentsId")
                $('#ShipmentsId').data('kendoGrid').setDataSource($scope.AllShipments);
            else if (gridid == "CasesId")
                    $('#CasesId').data('kendoGrid').setDataSource($scope.AllCases);
            else if (gridid == "AppointmentsId")
                $('#AppointmentsId').data('kendoGrid').setDataSource($scope.AllAppointments);
            else if (gridid == "TasksId")
                $('#TasksId').data('kendoGrid').setDataSource($scope.AllTasks);
            else if (gridid == "AddressesId")
                $('#AddressesId').data('kendoGrid').setDataSource($scope.AllAddress);
            else if (gridid == "PaymentsId")
                $('#PaymentsId').data('kendoGrid').setDataSource($scope.AllPayments);
            else if (gridid == "VendorsId")
                $('#VendorsId').data('kendoGrid').setDataSource($scope.AllVendors);
            else if (gridid == "NotesId")
                $('#NotesId').data('kendoGrid').setDataSource($scope.AllNotes);
        }

        $scope.SortFive = function (gridid) {
            if (gridid == "LeadsId")
                $('#LeadsId').data('kendoGrid').setDataSource($scope.FiveLeads);
            else if (gridid == "AccountsId")
                $('#AccountsId').data('kendoGrid').setDataSource($scope.FiveAccounts);
            else if (gridid == "OpportunitiesId")
                $('#OpportunitiesId').data('kendoGrid').setDataSource($scope.FiveOpportunities);
            else if (gridid == "ContactsId")
                $('#ContactsId').data('kendoGrid').setDataSource($scope.FiveContacts);
            else if (gridid == "QuotesId")
                $('#QuotesId').data('kendoGrid').setDataSource($scope.FiveQuotes);
            else if (gridid == "OrdersId")
                $('#OrdersId').data('kendoGrid').setDataSource($scope.FiveOrders);
            else if (gridid == "MPOsId")
                $('#MPOsId').data('kendoGrid').setDataSource($scope.FiveMpoVpos);
            else if (gridid == "ShipmentsId")
                $('#ShipmentsId').data('kendoGrid').setDataSource($scope.FiveShipments);
            else if (gridid == "CasesId")
                $('#CasesId').data('kendoGrid').setDataSource($scope.FiveCases);
            else if (gridid == "AppointmentsId")
                $('#AppointmentsId').data('kendoGrid').setDataSource($scope.FiveAppointments);
            else if (gridid == "TasksId")
                $('#TasksId').data('kendoGrid').setDataSource($scope.FiveTasks);
            else if (gridid == "AddressesId")
                $('#AddressesId').data('kendoGrid').setDataSource($scope.FiveAddress);
            else if (gridid == "PaymentsId")
                $('#PaymentsId').data('kendoGrid').setDataSource($scope.FivePayments);
            else if (gridid == "VendorsId")
                $('#VendorsId').data('kendoGrid').setDataSource($scope.FiveVendors);
            else if (gridid == "NotesId")
                $('#NotesId').data('kendoGrid').setDataSource($scope.FiveNotes);
        }
       
        $scope.onDataBinding = function (gridid) {
            if (!$scope.GridCollapse) {
                var grid = $("#" + gridid).data("kendoGrid");
                $(grid.tbody).find("tr").hide();
                $(grid.tbody).find("tr:eq(0)").show();
                $(grid.tbody).find("tr:eq(1)").show();
                $(grid.tbody).find("tr:eq(2)").show();
                $(grid.tbody).find("tr:eq(3)").show();
                $(grid.tbody).find("tr:eq(4)").show();
            }

        }

        $scope.ShowAll = function (gridid) {
            $scope.SortAll(gridid);
            $scope.GridCollapse = true;
            var grid = $("#" + gridid).data("kendoGrid");
            $(grid.tbody).find("tr").show();
            $scope.activeMenu = gridid;
            $scope.ShowLess = gridid;
        }

        $scope.ShowLessGrid = function (gridid) {
            $scope.ShowLess = "";
            $scope.GridCollapse = false;
            $scope.Selectactive('TopResults');
            var grid = $("#" + gridid).data("kendoGrid");
            $(grid.tbody).find("tr").hide();
            $(grid.tbody).find("tr:eq(0)").show();
            $(grid.tbody).find("tr:eq(1)").show();
            $(grid.tbody).find("tr:eq(2)").show();
            $(grid.tbody).find("tr:eq(3)").show();
            $(grid.tbody).find("tr:eq(4)").show();
            
        }
     
        $scope.Event = function (id, Startdate, EndDate) {
            var Startdate = new Date(Startdate);
            var EndDate = new Date(EndDate);
            calendarModalService.setPopUpDetails(true, "Event", id, $scope.GetAppointments, '', null, true, Startdate, EndDate);
        }

        $scope.viewEvent = function (Id) {
            calendarModalService.setViewFunction($scope.GetAppointments);
            calendarModalService.getAppointmentData(Id, false);
        }

        function maskPhoneNumber(text) {
            return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
        }
   
        function maskedDisplayPhone(phone) {
            var Div = '<address>';
            if (phone && phone != "")
                Div += maskPhoneNumber(phone) + '</div>';
            Div += '<text></text>' + '</div>';
            Div += '</address>';
            return Div;

        }

        $scope.getSelectedTaskId = function (taskId) {
            calendarModalService.setPopUpDetails(true, "Task", taskId, $scope.GetTasks, null, null, true);
        }

        $scope.completeTask = function (moaldId, TaskID) {
            CalendarService.CompleteTask(moaldId, TaskID, $scope.GetTasks)
        }

        $scope.InComplete = function (moaldId, TaskID) {
            CalendarService.InComplete(moaldId, TaskID, $scope.GetTasks)
        }
        function TaskMenuTemplate(dataitem) {
            var template = "";
            if (dataitem.TaskId && $scope.Permission.EditCalendar) {
                template += '<li><a href="javascript:void(0)" ng-click="getSelectedTaskId(' + dataitem.TaskId + ')">Edit</a></li>';
                if (dataitem.completed == null)
                    template += '<li ><a href="javascript:void(0)" ng-click="completeTask(\'persongo\', ' + dataitem.TaskId + ')">Mark Complete</a></li>';
                else
                    template += '<li ><a href="javascript:void(0)" ng-click="InComplete(\'persongo\', ' + dataitem.TaskId + ')">Mark Incomplete</a></li>';
            }
            if (template) {
                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                        '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                        '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                        '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }
            else {
                return '';
            }

        }

        function AddressesMenuTemplate(dataitem) {
            var template = "";
            if (dataitem.IsPrimaryAddress) {
                if (dataitem.AccountId)
                    template += '<li><a href="/#!/accountEdit/' + dataitem.AccountId + ' ">Edit</a></li>';
                else if (dataitem.LeadId)
                    template += '<li><a href="/#!/leadsEdit/' + dataitem.LeadId + ' ">Edit</a></li>';
            }
            else {
                if (dataitem.LeadId && $scope.Permission.DisplayLead) {
                    template += '<li><a href="javascript:void(0)" ng-click="EditGlobalAddressTP(' + dataitem.AddressId + ',' + dataitem.LeadId + ',' + dataitem.AccountId + ')">Edit</a></li>';
                }
                else if ($scope.Permission.DisplayAccount) {
                    template += '<li><a href="javascript:void(0)" ng-click="EditGlobalAddressTP(' + dataitem.AddressId + ',' + dataitem.LeadId + ',' + dataitem.AccountId + ')">Edit</a></li>';
                }
            }
            if (template) {
                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                            '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                            '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                            '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }
            else {
                return '';
            }
        }

        $scope.Notes = function (NoteId) {             
            $scope.NoteServiceTP.NoteId = NoteId;
            NoteServiceTP.DisplayGlobalNoteTP(NoteId);
        }
        $scope.NotesEdit = function (NoteId) {             
            $scope.NoteServiceTP.NoteId = NoteId;
            NoteServiceTP.EditGlobalNoteTP(NoteId);
            $scope.$on("RefreshNoteGrid", function (e, result) {
                if (result == "true") {
                    $scope.GetNotes();                   
                }
            });
        }
        $scope.EditGlobalAddressTP = function (AddressId, LeadId, AccountId) {
            AdditionalAddressServiceTP.EditGlobalAddressTP(AddressId, LeadId, AccountId);
            $scope.$on("RefreshAddressGrid", function (e, result) {
                if (result == "true") {
                    $scope.GetAddress();
                }
            });
        }

        $scope.ContactEdit = function (CustomerId, IsLead) {             
            $scope.AdditionalContactServiceTP.ParentName = "GlobalSearch";
            AdditionalContactServiceTP.EditGlobalContactTP(CustomerId);
        }
        $scope.$on("RefreshContactsGrid", function (e, result) {
            if (result == "true") {
                $scope.GetContacts();
            }
        });

        function addressToString(isSingleLine, model) {
            if (model) {

                var str = "";
                if (model.Address1)
                    str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

                return str;
            }
        };
        function MapDestinationaddress(model) {
            if (model) {

                var str = "";
                if (model.Address1)
                    str = model.Address1 + (model.Address2 ? (", " + model.Address2 + " ") : "");

                if (model.City)
                    str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
                else
                    str += (model.ZipCode || "");

                return str;
            }
        };
        $scope.GetGoogleAddressHref = function (model) {
            var dest = MapDestinationaddress(model);
            if (model.Address1) {

                var srcAddr;

                if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                    srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                } else {
                    srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                }

                if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                    return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                } else {
                    return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                }
            } else
                return null;
        }

        $scope.AddressToString = function (model) { return addressToString(true, model) };

        function LeadTemplate(dataItem) {
            var Div = '';
            if ($scope.Permission.DisplayLead) {
                Div = "<a href='#!/leads/" + dataItem.Id + "' style='color: rgb(61,125,139);'>" + dataItem.Name + "</a>";
            }
            else {
                Div = "<span>" + dataItem.Name + "</span>";
            }
            return Div;
        }

        function AccountTemplate(dataItem) {
            var Div = '';
            if (dataItem.AccountId)
                dataItem.Id = dataItem.AccountId;
            if (dataItem.AccountName)
                dataItem.Name = dataItem.AccountName;
            if (dataItem.Account)
                dataItem.Name = dataItem.Account;

            if ($scope.Permission.DisplayAccount) {
                Div = "<a href='#!/Accounts/" + dataItem.Id + "' style='color: rgb(61,125,139);'>" + dataItem.Name + "</a>";
            }
            else {
                Div = "<span>" + dataItem.Name + "</span>";
            }
            return Div;
        }

        function OpportunityTemplate(dataItem) {
            var Div = '';
            if (dataItem.OpportunityName)
            {
                dataItem.Opportunity = dataItem.OpportunityName;
            }
            if (dataItem.OpportunityId && dataItem.OpportunityId != null) {
                if ($scope.Permission.DisplayOpportunity)
                    Div = "<a href='#!/OpportunityDetail/" + dataItem.OpportunityId + "' style='color: rgb(61,125,139);'>" + dataItem.Opportunity + "</a>";
                else
                    Div = "<span>" + dataItem.Opportunity + "</span>";
            }
            return Div;
        }

        function QuoteTemplate(dataItem) {
            var Div = '';
            if ($scope.Permission.QuoteDisplay) {
                Div = "<a href='#!/quote/" + dataItem.OpportunityId + "/" + dataItem.QuoteKey + "' style='color: rgb(61,125,139);'>" + dataItem.QuoteName + "</a>";
            }
            else {
                Div = "<span>" + dataItem.QuoteName + "</span>";
            }
            return Div;
        }

        function QuoteNumberTemplate(dataItem) {
            var Div = '';
            if (dataItem.OpportunityId && dataItem.OpportunityId != null) {
                if ($scope.Permission.QuoteDisplay)
                    Div = "<a href='#!/quote/" + dataItem.OpportunityId + "/" + dataItem.QuoteKey + "' style='color: rgb(61,125,139);'>" + dataItem.QuoteId + "</a>";
                else
                    Div = "<span>" + dataItem.QuoteId + "</span>";
            }
            return Div;
        }

        function OrderTemplate(dataItem) {
            var Div = '';
            if (dataItem.OrderId && dataItem.OrderId != null) {
                if ($scope.Permission.DisplaySalesOrder)
                    Div = "<a href='#!/Orders/" + dataItem.OrderId + "' style='color: rgb(61,125,139);'>" + dataItem.Order + "</a>";
                else
                    Div = "<span>" + dataItem.Order + "</span>";
            }
            return Div;
        }

        function OrderNumberTemplate(dataItem) {
            var Div = '';
            if (dataItem.OrderId && dataItem.OrderId != null) {
                if ($scope.Permission.DisplaySalesOrder)
                    Div = "<a href='#!/Orders/" + dataItem.OrderId + "' style='color: rgb(61,125,139);'>" + dataItem.OrderNumber + "</a>";
                else
                    Div = "<span>" + dataItem.OrderNumber + "</span>";
            }
            return Div;
        }

        function VendorTemplate(dataItem) {
            var Div = '';
            if (dataItem.VendorId)
                dataItem.Id = dataItem.VendorId;
            if (dataItem.VendorName)
                dataItem.Name = dataItem.VendorName;
            if ($scope.Permission.DisplayVendor) {
                Div = "<a href='#!/vendorview/" + dataItem.Id + "' style='color: rgb(61,125,139);'>" + dataItem.Name + "</a>";
            }
            else
            {
                Div = "<span>" + dataItem.Name + "</span>";
            }
            return Div;
        }

        function CaseMpoTemplate(dataItem) {
            var Div = '';
            if ($scope.Permission.ProcurementDisplay) {
                Div = "<a href='#!/purchasemasterview/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.Mpo + "</a>";
            }
            else {
                Div = "<span>" + dataItem.Mpo + "</span>";
            }
            return Div;
        }
        function CaseVpoTemplate(dataItem) {
            var Div = '';
            if ($scope.Permission.ProcurementDisplay && dataItem.PICPO !=0) {
                Div = "<a href='#!/purchasemasterview/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.PICPO + "</a>";
            }
            else if (dataItem.PICPO != 0)
            {
                Div = "<span>" + dataItem.PICPO + "</span>";
            }
            return Div;
        }


        $scope.LeadsGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Territory: { editable: false },
                            Name: { editable: false },
                            Phone: { editable: false },
                            Email: { editable: false },
                        }
                    }
                },
            },
            
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("LeadsId");
                if (!$scope.Permission.EditLead && !$scope.Permission.AddEvent) {
                    var grid = $("#LeadsId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0)
                {
                if ($scope.AllLeadsCount == 0) {
                    $scope.LeadsResultCount = $scope.AllLeadsCount.toString() + " Result";
                }
                else if ($scope.AllLeadsCount == 1) {
                    $scope.LeadsResultCount = $scope.AllLeadsCount.toString() + " Result";
                }
                else if ($scope.AllLeadsCount < 6) {
                    $scope.LeadsResultCount = $scope.AllLeadsCount.toString() + " Results";
                }
                else {
                    $scope.LeadsResultCount = "5+ Results";
                }
                }
                else {
                    $scope.LeadsResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Territory",
                    title: "Territory",
                    filterable: true,
                },
                {
                    field: "Name",
                    title: "Name",
                    template: function (dataItem) {
                        return LeadTemplate(dataItem);
                    },
                    
                },
                {
                    field: "Phone",
                    title: "Phone",
                    filterable: true,
                    template: function (dataItem) {
                        return maskedDisplayPhone(dataItem.Phone);
                    }
                },
                {
                    field: "Email",
                    title: "Email",
                    filterable: true,
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        if ($scope.Permission.EditLead)
                            EditLead = '<li><a href="#!/leadsEdit/' + dataitem.Id + '">Edit</a> </li>';
                        else
                            EditLead = '';
                        if ($scope.Permission.AddEvent)
                            AddAppointmentLead = '<li><a href="#!/Calendar/leads/' + dataitem.Id + '">Add Appointment</a> </li>';
                        else
                            AddAppointmentLead = '';
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                        + EditLead + AddAppointmentLead +
                                                        `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        return hcontent;
                    }
                },
            ]
        };
        $scope.AccountsGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Territory: { editable: false },
                            Name: { editable: false },
                            Phone: { editable: false },
                            Email: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                
                $scope.onDataBinding("AccountsId");

                if (!$scope.Permission.EditAccount && !$scope.Permission.AddEvent && !$scope.Permission.DisplayMeasurements) {
                    var grid = $("#AccountsId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllAccountsCount == 0) {
                        $scope.AccountResultCount = $scope.AllAccountsCount.toString() + " Result";
                    }
                    else if ($scope.AllAccountsCount == 1) {
                        $scope.AccountResultCount = $scope.AllAccountsCount.toString() + " Result";
                    }
                    else if ($scope.AllAccountsCount < 6) {
                        $scope.AccountResultCount = $scope.AllAccountsCount.toString() + " Results";
                    }
                    else {
                        $scope.AccountResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.AccountResultCount = "0 Result";
                }
            },

            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Territory",
                    title: "Territory",
                    filterable: true,
                },
                {
                    field: "Name",
                    title: "Name",
                    template: function (dataItem) {
                        return AccountTemplate(dataItem);
                    },
                },
                {
                    field: "Phone",
                    title: "Phone",
                    filterable: true,
                    template: function (dataItem) {
                        return maskedDisplayPhone(dataItem.Phone);
                    }
                },
                
            {
                field: "Email",
                title: "Email",
                filterable: true,
            },
            {
                field: "",
                title: "",
                width: "60px",
                template: function (dataitem) {
                    if($scope.Permission.EditAccount)
                        EditAccount = '<li><a href="#!/accountEdit/' + dataitem.Id + '">Edit</a> </li>';
                    else
                        EditAccount = '';
                    if ($scope.Permission.DisplayMeasurements)
                        ViewMeasurementsAccount = '<li><a href="#!/measurementDetails/' + dataitem.Id + '">View Measurements</a> </li>';
                    else
                        ViewMeasurementsAccount = '';
                    if ($scope.Permission.AddEvent)
                        AddAppointmentAccount = '<li><a href="#!/Calendar/Accounts/' + dataitem.Id + '">Add Appointment</a> </li>';
                    else
                        AddAppointmentAccount = '';
                    var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                            + EditAccount + ViewMeasurementsAccount + AddAppointmentAccount +
                                                        `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                    return hcontent;
                }
            },
            
            ]
        };
        $scope.OpportunitiesGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Territory: { editable: false },
                            OpportunityNumber: { editable: false },
                            OpportunityName: { editable: false },
                            AccountName: { editable: false },
                            Status: { editable: false },
                            SalesAgent: { editable: false },
                            OrderAmount: { editable: false },
                            ContractDate: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("OpportunitiesId");
                if (!$scope.Permission.EditOpportunity && !$scope.Permission.DisplayMeasurements && !$scope.Permission.AddEvent) {
                    var grid = $("#OpportunitiesId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllOpportunitiesCount == 0) {
                        $scope.OpportunitiesResultCount = $scope.AllOpportunitiesCount.toString() + " Result";
                    }
                    else if ($scope.AllOpportunitiesCount == 1) {
                        $scope.OpportunitiesResultCount = $scope.AllOpportunitiesCount.toString() + " Result";
                    }
                    else if ($scope.AllOpportunitiesCount < 6) {
                        $scope.OpportunitiesResultCount = $scope.AllOpportunitiesCount.toString() + " Results";
                    }
                    else {
                        $scope.OpportunitiesResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.OpportunitiesResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Territory",
                    title: "Territory",
                    filterable: true,
                },
                {
                    field: "OpportunityNumber",
                    title: "ID",
                    filterable: true,
                },
                {
                    field: "OpportunityName",
                    title: "Opportunity Name",
                    //template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span>#= OpportunityName #</span></a>",
                    template: function (dataItem) {
                        return OpportunityTemplate(dataItem);
                    },
                },
                {
                    field: "AccountName",
                    title: "Account Name",
                    //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= AccountName #</span></a>",
                    template: function (dataItem) {
                        return AccountTemplate(dataItem);
                    },
                },
                {
                    field: "Status",
                    title: "Status",
                    filterable: true,
                },
                {
                    field: "SalesAgent",
                    title: "Sales Agent",
                    filterable: true,
                },
                {
                    field: "OrderAmount",
                    title: "Order Amount",
                    filterable: true,
                    format: "{0:c2}",
                    attributes: { class: 'text-right' },
                },
                {
                    field: "ContractDate",
                    title: "Contract Date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    type: "date",
                    template: function (dataitem) {
                        if (dataitem.ContractDate == null || dataitem.ContractDate == '')
                            return ''
                        else
                            return HFCService.KendoDateFormat(dataitem.ContractDate);
                    }
                        // "#= (ContractDate == null|| ContractDate == '')? '' : HFCService.KendoDateFormat(ContractDate) #",
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        if ($scope.Permission.EditOpportunity)
                            EditOpportunity = '<li><a href="#!/opportunityEdit/' + dataitem.OpportunityId + '">Edit</a> </li>';
                        else
                            EditOpportunity = '';
                        if($scope.Permission.DisplayMeasurements)
                            ViewMeasurementsOpportunity = '<li><a href="#!/measurementDetail/' + dataitem.OpportunityId + '/' + dataitem.InstallationAddressId + ' ">View Measurements</a> </li>';
                        else
                            ViewMeasurementsOpportunity = '';
                        if ($scope.Permission.AddEvent)
                            AddAppointmentOpportunity = '<li><a href="#!/Calendar/measurementDetail/' + dataitem.OpportunityId + '">Add Appointment</a> </li>';
                        else
                            AddAppointmentOpportunity = '';
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + EditOpportunity + ViewMeasurementsOpportunity + AddAppointmentOpportunity +
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        return hcontent;
                    }
                },
            ]
        };
        $scope.ContactsGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Name: { editable: false },
                            LeadAccountName: { editable: false },
                            Phone: { editable: false },
                            Email: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("ContactsId");
                if (!$scope.Permission.EditLead && !$scope.Permission.EditAccount && !$scope.Permission.DisplayLead && !$scope.Permission.DisplayAccount) {
                    var grid = $("#ContactsId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllContactsCount == 0) {
                        $scope.ContactsResultCount = $scope.AllContactsCount.toString() + " Result";
                    }
                    else if ($scope.AllContactsCount == 1) {
                        $scope.ContactsResultCount = $scope.AllContactsCount.toString() + " Result";
                    }
                    else if ($scope.AllContactsCount < 6) {
                        $scope.ContactsResultCount = $scope.AllContactsCount.toString() + " Results";
                    }
                    else {
                        $scope.ContactsResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.ContactsResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Name",
                    title: "Name",
                    filterable: true,
                },
                {
                    field: "LeadAccountName",
                    title: "Account/Lead",
                    filterable: true,
                    template: function (dataitem) {
                        if (dataitem.IsLead)
                            edit = '<a href="#!/leads/' + dataitem.Id + '" style="color: rgb(61,125,139);"> ' + dataitem.LeadAccountName + '</a>';
                        else
                            edit = '<a href="#!/Accounts/' + dataitem.Id + '" style="color: rgb(61,125,139);"> ' + dataitem.LeadAccountName + '</a>';
                        var hcontent = ``+ edit;
                        return hcontent;
                    }
                },
                {
                    field: "Phone",
                    title: "Phone",
                    filterable: true,
                    template: function (dataItem) {
                        return maskedDisplayPhone(dataItem.Phone);
                    }
                },
                {
                    field: "Email",
                    title: "Email",
                    filterable: true,
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                          edit = '';
                        if (dataitem.IsPrimaryCustomer) {
                            if (dataitem.IsLead && $scope.Permission.EditLead)
                                edit = '<li><a href="#!/leadsEdit/' + dataitem.Id + '">Edit</a> </li>';
                            else if($scope.Permission.EditAccount)
                                edit = '<li><a href="#!/accountEdit/' + dataitem.Id + '">Edit</a> </li>';
                        }
                        else if ($scope.Permission.DisplayLead)
                        {
                            edit = '<li><a href="" ng-click="AdditionalContactServiceTP.EditGlobalContactTP(' + dataitem.CustomerId + ',' + dataitem.IsLead + ')">Edit</a></li>';
                        }
                        else if ($scope.Permission.DisplayAccount) {
                            edit = '<li><a href="" ng-click="AdditionalContactServiceTP.EditGlobalContactTP(' + dataitem.CustomerId + ',' + dataitem.IsLead + ')">Edit</a></li>';
                        }
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + edit +
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        if (edit)
                            return hcontent;
                        else
                            return '';
                    }
                },
            ]
        };
        $scope.QuotesGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            QuoteId: { editable: false },
                            QuoteName: { editable: false },
                            SaleDate: { editable: false },
                            Account: { editable: false },
                            Opportunity: { editable: false },
                            Territory: { editable: false },
                            Sidemark: { editable: false },
                            TotalAmount: { editable: false },
                            QuoteStatus: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("QuotesId");
                if (!$scope.Permission.QuoteEdit) {
                    var grid = $("#QuotesId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllQuotesCount == 0) {
                        $scope.QuotesResultCount = $scope.AllQuotesCount.toString() + " Result";
                    }
                    else if ($scope.AllQuotesCount == 1) {
                        $scope.QuotesResultCount = $scope.AllQuotesCount.toString() + " Result";
                    }
                    else if ($scope.AllQuotesCount < 6) {
                        $scope.QuotesResultCount = $scope.AllQuotesCount.toString() + " Results";
                    }
                    else {
                        $scope.QuotesResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.QuotesResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "QuoteId",
                    title: "ID",
                    filterable: true,
                    template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteId #</span></a>",
                },
                {
                    field: "QuoteName",
                    title: "Name",
                    //template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteName #</span></a>",
                    template: function (dataItem) {
                        return QuoteTemplate(dataItem);
                    },
                },
                {
                    field: "SaleDate",
                    title: "Contracted Date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    type: "date",
                    template:function (dataitem) {
                        if (dataitem.SaleDate == null || dataitem.SaleDate == '')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.SaleDate);
                    }
                        //"#= (SaleDate == null|| SaleDate == '')? '' : kendo.toString(kendo.parseDate(SaleDate, 'yyyy-MM-dd'), 'M/d/yyyy')#",
                },
                {
                    field: "Account",
                    title: "Account",
                    //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= Account #</span></a>",
                    template: function (dataItem) {
                        return AccountTemplate(dataItem);
                    },
                },
                {
                    field: "Opportunity",
                    title: "Opportunity",
                    //template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span>#= Opportunity #</span></a>",
                    template: function (dataItem) {
                        return OpportunityTemplate(dataItem);
                    },
                },
                {
                    field: "Territory",
                    title: "Territory",
                    filterable: true,
                },
                {
                    field: "Sidemark",
                    title: "SideMark",
                    filterable: true,
                },
                {
                    field: "TotalAmount",
                    title: "Total Amount",
                    filterable: true,
                    attributes: { class: 'text-right' },
                    hidden: false,
                    format: "{0:c}"
                },
                {
                    field: "QuoteStatus",
                    title: "Status",
                    filterable: true,
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        EditQuote = '<li><a href="javascript:void(0)" ng-click=EditQuoteFromList(dataItem)>Edit</a> </li>';
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + EditQuote +
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        //return hcontent;
                        if (dataitem.OrderExists && dataitem.QuoteStatusId == 3)
                        {
                            return '';
                        }
                        else
                        {
                            return hcontent;
                        }
                    }
                },
            ]
        };


        $scope.EditQuoteFromList = function (quoteKey) {
            if (quoteKey.QuoteStatusId === 2) {
                $http.get("/api/timezones/0/GetFranchiseDateTime").then(function (data) {
                    var dateString = data.data;
                    $scope.date = $scope.calendarModalService.getDateStringValue(new Date(dateString), true, false, false);
                    var expdate = $scope.calendarModalService.getDateStringValue(new Date(quoteKey.ExpiryDate), true, false, false);
                    if ($scope.date.setHours(0, 0, 0, 0) > expdate.setHours(0, 0, 0, 0)) {
                        $scope.selectedOpportunityId = quoteKey.OpportunityId;
                        $scope.selectedQuoteKey = quoteKey.QuoteKey;
                        $scope.DisplayPopup();
                    }
                    else {
                        $window.location.href = "#!/Editquote/" + quoteKey.OpportunityId + "/" + quoteKey.QuoteKey;
                    }
                });
            }
            else {
                $window.location.href = "#!/Editquote/" + quoteKey.OpportunityId + "/" + quoteKey.QuoteKey;
            }
        }

        $scope.DisplayPopup = function () {
            $("#Warninginfo").modal("show");
            return;
        }
        //cancel the pop-up
        $scope.CancelPopup = function (modalId) {
            $("#" + modalId).modal("hide");
        };

        $scope.changeQuote = function () {


            $http.post("/api/Quotes/" + $scope.selectedQuoteKey + "/changeExpiryToDraft").then(function (data) {
                if (data.data === true) {
                    $scope.calendarModalService.Quote_exp_draft = true;
                    $window.location.href = "#!/Editquote/" + $scope.selectedOpportunityId + "/" + $scope.selectedQuoteKey;
                }
            });



        }


        $scope.OrdersGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            OrderNumber: { editable: false },
                            ContractedDate: { editable: false },
                            QuoteId: { editable: false },
                            Account: { editable: false },
                            Opportunity: { editable: false },
                            Territory: { editable: false },
                            Sidemark: { editable: false },
                            TotalAmount: { editable: false },
                            Status: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("OrdersId");
                if (!$scope.Permission.EditSalesOrder) {
                    var grid = $("#OrdersId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllOrdersCount == 0) {
                        $scope.OrdersResultCount = $scope.AllOrdersCount.toString() + " Result";
                    }
                    else if ($scope.AllOrdersCount == 1) {
                        $scope.OrdersResultCount = $scope.AllOrdersCount.toString() + " Result";
                    }
                    else if ($scope.AllOrdersCount < 6) {
                        $scope.OrdersResultCount = $scope.AllOrdersCount.toString() + " Results";
                    }
                    else {
                        $scope.OrdersResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.OrdersResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "OrderNumber",
                    title: "ID",
                    //template: "<a href='\\\\#!/Orders/#= OrderId #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a>",
                    template: function (dataItem) {
                        return OrderNumberTemplate(dataItem);
                    },
                },
                {
                    field: "ContractedDate",
                    title: "Contracted Date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataitem) {
                        if (dataitem.ContractedDate == null || dataitem.ContractedDate == '')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.ContractedDate);
                    }
                        //"#=  (ContractedDate == null)? '' : kendo.toString(kendo.parseDate(ContractedDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                },
                {
                    field: "QuoteId",
                    title: "Quote",
                    //template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteId #</span></a>",
                    template: function (dataItem) {
                        return QuoteNumberTemplate(dataItem);
                    },
                },
                {
                    field: "Account",
                    title: "Account",
                    //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= Account #</span></a>",
                    template: function (dataItem) {
                        return AccountTemplate(dataItem);
                    },
                },
                {
                    field: "Opportunity",
                    title: "Opportunity",
                    //template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span>#= Opportunity #</span></a>",
                    template: function (dataItem) {
                        return OpportunityTemplate(dataItem);
                    },
                },
                {
                    field: "Territory",
                    title: "Territory",
                    filterable: true,
                },
                {
                    field: "Sidemark",
                    title: "Sidemark",
                    filterable: true,
                },
                {
                    field: "TotalAmount",
                    title: "Total Amount",
                    filterable: true,
                    format: "{0:c2}",
                    attributes: { class: 'text-right' },
                },
                {
                    field: "Status",
                    title: "Status",
                    filterable: true,
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        EditOrder = '<li><a href="#!/orderEdit/' + dataitem.OrderId + '">Edit</a> </li>';
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + EditOrder +
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        return hcontent;
                    }
                },
            ]
        };
        $scope.MPOsGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Mpo: { editable: false },
                            Vpo: { editable: false },
                            VendorName: { editable: false },
                            AccountName: { editable: false },
                            OrderNumber: { editable: false },
                            MpoDate: { editable: false },
                            VpoDate: { editable: false },
                            PromiseDate: { editable: false },
                            VpoStatus: { editable: false },
                            Territory: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("MPOsId");
                if (!$scope.Permission.Procurementedit) {
                    var grid = $("#MPOsId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllMpoVposCount == 0) {
                        $scope.MPOsResultCount = $scope.AllMpoVposCount.toString() + " Result";
                    }
                    else if ($scope.AllMpoVposCount == 1) {
                        $scope.MPOsResultCount = $scope.AllMpoVposCount.toString() + " Result";
                    }
                    else if ($scope.AllMpoVposCount < 6) {
                        $scope.MPOsResultCount = $scope.AllMpoVposCount.toString() + " Results";
                    }
                    else {
                        $scope.MPOsResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.MPOsResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Mpo",
                    title: "MPO ID",
                    template: "<a href='\\\\#!/purchasemasterview/#= PurchaseOrderId #' style='color: rgb(61,125,139);'><span>#= Mpo #</span></a>",
                },
                {
                    field: "PICPO",
                    title: "VPO",
                    template: "#if(PICPO==0){# <span></span> #}else{# <a href='\\\\#!/purchasemasterview/#= PurchaseOrderId #' style='color: rgb(61,125,139);'><span>#= PICPO #</span></a>  #}#",
                },
                {
                    field: "VendorName",
                    title: "Vendor",
                    //template: "<a href='\\\\#!/vendorview/#= VendorId #' style='color: rgb(61,125,139);'><span>#= VendorName #</span></a>",
                    template: function (dataItem) {
                        return VendorTemplate(dataItem);
                    },
                },
                {
                    field: "AccountName",
                    title: "Customer Info",
                    template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= AccountName #</span></a>" + " - " + "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span >#=OpportunityName#</span></a>",
                    
                },
                {
                    field: "OrderNumber",
                    title: "Order",
                    //template: "<a href='\\\\#!/Orders/#= OrderId #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a>",
                    template: function (dataItem) {
                        return OrderNumberTemplate(dataItem);
                    },
                },
                {
                    field: "MpoDate",
                    title: "MPO Date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataitem) {
                        if (dataitem.MpoDate == null || dataitem.MpoDate == '')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.MpoDate);
                    }
                        //"#=  (MpoDate == null)? '' : kendo.toString(kendo.parseDate(MpoDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                },
                {
                    field: "VpoDate",
                    title: "VPO Date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataitem) {
                        if (dataitem.VpoDate == null || dataitem.VpoDate == '')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.VpoDate);
                    }
                        //"#=  (VpoDate == null)? '' : kendo.toString(kendo.parseDate(VpoDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                },
                {
                    field: "PromiseDate",
                    title: "Promise Date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataitem) {
                        if (dataitem.PromiseDate == null || dataitem.PromiseDate == '0001-01-01T00:00:00')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.PromiseDate);
                    }
                        //"#=  ( (PromiseDate == null)|| (PromiseDate == '0001-01-01T00:00:00') )? '' : kendo.toString(kendo.parseDate(PromiseDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                },
                {
                    field: "VpoStatus",
                    title: "VPO Status",
                },
                {
                    field: "Territory",
                    title: "Territory",
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        EditMpoVpo = '<li><a href="#!/purchasemasterview/' + dataitem.PurchaseOrderId + '">Edit</a> </li>';
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + EditMpoVpo +
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        return hcontent;
                    }
                },
            ]
        };
        $scope.ShipmentsGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            ShipmentId: { editable: false },
                            ShippedDate: { editable: false },
                            VendorName: { editable: false },
                            OrderId: { editable: false },
                            SideMark: { editable: false },
                            Mpo: { editable: false },
                            Vpo: { editable: false },
                            POLines: { editable: false },
                            POQty: { editable: false },
                            QtyShipped: { editable: false },
                            QtyReceived: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("ShipmentsId");
                if (e.sender._data.length != 0) {
                    if ($scope.AllShipmentsCount == 0) {
                        $scope.ShipmentsResultCount = $scope.AllShipmentsCount.toString() + " Result";
                    }
                    else if ($scope.AllShipmentsCount == 1) {
                        $scope.ShipmentsResultCount = $scope.AllShipmentsCount.toString() + " Result";
                    }
                    else if ($scope.AllShipmentsCount < 6) {
                        $scope.ShipmentsResultCount = $scope.AllShipmentsCount.toString() + " Results";
                    }
                    else {
                        $scope.ShipmentsResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.ShipmentsResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "ShipmentId",
                    title: "Shipment ID",
                    template: "<a href='\\\\#!/shipnotices/#= ShipNoticeId #' style='color: rgb(61,125,139);'><span>#= ShipmentId #</span></a> "
                },
                {
                    field: "ShippedDate",
                    title: "Shipped Date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataitem) {
                        if (dataitem.ShippedDate == null || dataitem.ShippedDate == '')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.ShippedDate);
                    }
                        //"#=  (ShippedDate == null)? '' : kendo.toString(kendo.parseDate(ShippedDate, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                },
                {
                    field: "VendorName",
                    title: "Vendor",
                    // template: "<a href='\\\\#!/vendorview/#= VendorId #' style='color: rgb(61,125,139);'><span>#= VendorName #</span></a>",
                    template: function (dataItem) {
                        return VendorTemplate(dataItem);
                    },
                },
                {
                    field: "OrderNumber",
                    title: "Order",
                    // template: "<a href='\\\\#!/Orders/#= OrderId #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a> ",
                    template: function (dataItem) {
                        return OrderNumberTemplate(dataItem);
                    },
                },
                {
                    field: "SideMark",
                    title: "Sidemark",
                },
                {
                    field: "Mpo",
                    title: "MPO",
                    template: "<a href='\\\\#!/purchasemasterview/#= PurchaseOrderId #' style='color: rgb(61,125,139);'><span>#= Mpo #</span></a>",
                },
                {
                    field: "Vpo",
                    title: "VPO",
                    template: "#if(Vpo==0){# <span></span> #}else{# <a href='\\\\#!/purchasemasterview/#= PurchaseOrderId #' style='color: rgb(61,125,139);'><span>#= Vpo #</span></a>  #}#",
                },
                {
                    field: "POLines",
                    title: "PO Lines",
                },
                {
                    field: "POQty",
                    title: "PO QTY",
                },
                {
                    field: "QtyShipped",
                    title: "QTY Shipped",
                },
                {
                    field: "QtyReceived",
                    title: "QTY Received",
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        EditShipment = '<li><a href="#!/shipnotices/' + dataitem.ShipNoticeId + '">Edit</a> </li>';
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + EditShipment +
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        return hcontent;
                    }
                },
            ]
        };
        $scope.CasesGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            CaseNumber: { editable: false },
                            CaseLineNumber: { editable: false },
                            Mpo: { editable: false },
                            Vpo: { editable: false },
                            VendorName: { editable: false },
                            AccountName: { editable: false },
                            Description: { editable: false },
                            Status: { editable: false },
                            DateOpened: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("CasesId");
                if (e.sender._data.length != 0) {
                    if ($scope.AllCasesCount == 0) {
                        $scope.CasesResultCount = $scope.AllCasesCount.toString() + " Result";
                    }
                    else if ($scope.AllCasesCount == 1) {
                        $scope.CasesResultCount = $scope.AllCasesCount.toString() + " Result";
                    }
                    else if ($scope.AllCasesCount < 6) {
                        $scope.CasesResultCount = $scope.AllCasesCount.toString() + " Results";
                    }
                    else {
                        $scope.CasesResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.CasesResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "CaseNumber",
                    title: "Case No",
                    template: "<a href='\\\\#!/CaseView/#= CaseId #' style='color: rgb(61,125,139);'><span>#= CaseNumber #</span></a>",
                },
                {
                    field: "CaseLineNumber",
                    title: "Case Line No",
                    template: "<a href='\\\\#!/FranchiseVendorView/#= CaseId #/#= VendorCaseNumber #/#= VendorId #' style='color: rgb(61,125,139);'><span>#= CaseLineNumber #</span></a>",
                },
                {
                    field: "Mpo",
                    title: "MPO",
                     template: function (dataItem) {
                        return CaseMpoTemplate(dataItem);
                    },
                },
                {
                    field: "PICPO",
                    title: "VPO",
                    template: function (dataItem) {
                        return CaseVpoTemplate(dataItem);
                    },
                },
                {
                    field: "VendorName",
                    title: "Vendor",
                    template: function (dataItem) {
                        return VendorTemplate(dataItem);
                    },
                },
                {
                    field: "AccountName",
                    title: "Account Name",
                    //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= AccountName #</span></a>",
                    template: function (dataItem) {
                        return AccountTemplate(dataItem);
                    },
                },
                {
                    field: "Description",
                    title: "Description",
                },
                {
                    field: "Status",
                    title: "Status",
                },
                {
                    field: "DateOpened",
                    title: "Date Opened",
                    filterable: true,
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataitem) {
                        if (dataitem.DateOpened == null || dataitem.DateOpened == '')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.DateOpened);
                    }
                        //"#=  (DateOpened == null)? '' : kendo.toString(kendo.parseDate(DateOpened, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        EditCase = '<li><a href="#!/Case/' + dataitem.CaseId + '">Edit</a> </li>';
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + EditCase +
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        return hcontent;
                    }
                },
            ]
        };
        $scope.AppointmentsGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Subject: { editable: false },
                            Related: { editable: false },
                            AttendeesName: { editable: false },
                            AppointmentType: { editable: false },
                            StartDate: { editable: false },
                            EndDate: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("AppointmentsId");
                if (!$scope.Permission.ViewCalendar && !$scope.Permission.EditCalendar) {
                    var grid = $("#AppointmentsId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllAppointmentsCount == 0) {
                        $scope.AppointmentsResultCount = $scope.AllAppointmentsCount.toString() + " Result";
                    }
                    else if ($scope.AllAppointmentsCount == 1) {
                        $scope.AppointmentsResultCount = $scope.AllAppointmentsCount.toString() + " Result";
                    }
                    else if ($scope.AllAppointmentsCount < 6) {
                        $scope.AppointmentsResultCount = $scope.AllAppointmentsCount.toString() + " Results";
                    }
                    else {
                        $scope.AppointmentsResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.AppointmentsResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Subject",
                    title: "Subject",
                    filterable: true,
                },
                {
                    field: "Related",
                    title: "Related",
                    //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= Related #</span></a>",
                    template: function (dataitem) {
                        edit = '';
                        if (dataitem.OrderId) {
                            if ($scope.Permission.DisplaySalesOrder)
                                edit = '<a href="#!/Orders/' + dataitem.OrderId + '" style="color: rgb(61,125,139);"> ' + dataitem.Related + '</a>';
                            else
                                edit = '<span> ' + dataitem.Related + '</span>';

                        }
                        else if (dataitem.OpportunityId) {
                            if ($scope.Permission.DisplayOpportunity)
                                edit = '<a href="#!/OpportunityDetail/' + dataitem.OpportunityId + '" style="color: rgb(61,125,139);"> ' + dataitem.Related + '</a>';
                            else
                                edit = '<span> ' + dataitem.Related + '</span>';
                            
                        }
                        else if (dataitem.AccountId) {
                            if($scope.Permission.DisplayAccount)
                                edit = '<a href="#!/Accounts/' + dataitem.AccountId + '" style="color: rgb(61,125,139);"> ' + dataitem.Related + '</a>';
                            else
                                edit = '<span> ' + dataitem.Related + '</span>';
                            
                        }
                        else if (dataitem.LeadId) {
                            if($scope.Permission.DisplayLead)
                                edit = '<a href="#!/leads/' + dataitem.LeadId + '" style="color: rgb(61,125,139);"> ' + dataitem.Related + '</a>';
                            else
                                edit = '<span> ' + dataitem.Related + '</span>';        
                        }
                        var hcontent = ``+ edit;
                        return hcontent;
                    },
                    filterable: true,
                },
                {
                    field: "AttendeesName",
                    title: "Attendees",
                    filterable: true,
                },
                {
                    field: "AppointmentType",
                    title: "Type",
                    filterable: true,
                },
                {
                    field: "StartDate",
                    title: "Start Date/Time",
                    filterable: HFCService.gridfilterableDate("datetime", "", ""),
                    template: function (dataitem) {
                        if (dataitem.StartDate == null || dataitem.StartDate == '')
                            return "";
                        else
                            return HFCService.KendoDateTimeFormat(dataitem.StartDate);
                    }
                        //"#=  (StartDate == null)? '' : kendo.toString(kendo.parseDate(StartDate), 'MM/dd/yyyy hh:mm tt') #",
                   // template: "#= kendo.toString(kendo.parseDate(StartDate), 'yyyy-MM-dd hh:mm tt') #"
                },
                {
                    field: "EndDate",
                    title: "End Date/Time",
                    filterable: HFCService.gridfilterableDate("datetime", "", ""),
                    template: function (dataitem) {
                        if (dataitem.EndDate == null || dataitem.EndDate == '')
                            return "";
                        else
                            return HFCService.KendoDateTimeFormat(dataitem.EndDate);
                    }
                        //"#=  (EndDate == null)? '' : kendo.toString(kendo.parseDate(EndDate), 'MM/dd/yyyy hh:mm tt') #",
                   // template: "#= kendo.toString(kendo.parseDate(EndDate), 'yyyy-MM-dd hh:mm tt') #"
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        var StartDate = dataitem.StartDate;
                        var StartDate = Date.parse(StartDate);
                        var EndDate = dataitem.EndDate;
                        var EndDate = Date.parse(EndDate);
                        if (!dataitem.IsCancelled) {
                            if ($scope.Permission.EditCalendar)
                                EditAppointment = '<li><a href="javascript:void(0)" ng-click="Event(' + dataitem.CalendarId + ',' + StartDate + ',' + EndDate + ')">Edit</a></li>';
                            else
                                EditAppointment = '';

                            if($scope.Permission.ViewCalendar)
                            {
                                ViewAppointment = '<li><a href="javascript:void(0)" ng-click="viewEvent(' + dataitem.CalendarId + ')">View</a></li>';
                                
                             }
                            else{
                                ViewAppointment= '';
                             }
                            var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                    + EditAppointment + ViewAppointment +
                                                                `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                            return hcontent;
                        }
                        else {
                            return '';
                        }
                    }
                },
            ]
        };
        $scope.TasksGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Subject: { editable: false },
                            Related: { editable: false },
                            AttendeesName: { editable: false },
                            DueDate: { editable: false },
                            Note: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("TasksId");
                if (!$scope.Permission.EditCalendar) {
                    var grid = $("#TasksId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllTasksCount == 0) {
                        $scope.TasksResultCount = $scope.AllTasksCount.toString() + " Result";
                    }
                    else if ($scope.AllTasksCount == 1) {
                        $scope.TasksResultCount = $scope.AllTasksCount.toString() + " Result";
                    }
                    else if ($scope.AllTasksCount < 6) {
                        $scope.TasksResultCount = $scope.AllTasksCount.toString() + " Results";
                    }
                    else {
                        $scope.TasksResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.TasksResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Subject",
                    title: "Subject",
                    filterable: true,
                },
                {
                    field: "Related",
                    title: "Related",
                    //template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span>#= Related #</span></a>",
                    template: function (dataitem) {
                        edit = '';
                        if (dataitem.OrderId) {
                            if ($scope.Permission.DisplaySalesOrder)
                                edit = '<a href="#!/Orders/' + dataitem.OrderId + '" style="color: rgb(61,125,139);"> ' + dataitem.Related + '</a>';
                            else
                                edit = '<span> ' + dataitem.Related + '</span>';

                        }
                        else if (dataitem.OpportunityId) {
                            if ($scope.Permission.DisplayOpportunity)
                                edit = '<a href="#!/OpportunityDetail/' + dataitem.OpportunityId + '" style="color: rgb(61,125,139);"> ' + dataitem.Related + '</a>';
                            else
                                edit = '<span> ' + dataitem.Related + '</span>';

                        }
                        else if (dataitem.AccountId) {
                            if ($scope.Permission.DisplayAccount)
                                edit = '<a href="#!/Accounts/' + dataitem.AccountId + '" style="color: rgb(61,125,139);"> ' + dataitem.Related + '</a>';
                            else
                                edit = '<span> ' + dataitem.Related + '</span>';

                        }
                        else if (dataitem.LeadId) {
                            if ($scope.Permission.DisplayLead)
                                edit = '<a href="#!/leads/' + dataitem.LeadId + '" style="color: rgb(61,125,139);"> ' + dataitem.Related + '</a>';
                            else
                                edit = '<span> ' + dataitem.Related + '</span>';
                        }
                        var hcontent = `` + edit;
                        return hcontent;
                    },
                },
                {
                    field: "AttendeesName",
                    title: "Attendees",
                    filterable: true,
                },
                {
                    field: "DueDate",
                    title: "Due",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataitem) {
                        if (dataitem.DueDate == null || dataitem.DueDate == '')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.DueDate);
                    }
                        //"#=  (DueDate == null)? '' : kendo.toString(kendo.parseDate(DueDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                },
                {
                    field: "Note",
                    title: "Note",
                    filterable: true,
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        return TaskMenuTemplate(dataitem);
                    }
                },
            ]
        };
        $scope.AddressesGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            AttentionText: { editable: false },
                            Contact: { editable: false },
                            Address1: { editable: false },
                            CrossStreet: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("AddressesId");
                if (!$scope.Permission.EditLead && !$scope.Permission.EditAccount && !$scope.Permission.DisplayLead && !$scope.Permission.DisplayAccount) {
                    var grid = $("#AddressesId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllAddressCount == 0) {
                        $scope.AddressesResultCount = $scope.AllAddressCount.toString() + " Result";
                    }
                    else if ($scope.AllAddressCount == 1) {
                        $scope.AddressesResultCount = $scope.AllAddressCount.toString() + " Result";
                    }
                    else if ($scope.AllAddressCount < 6) {
                        $scope.AddressesResultCount = $scope.AllAddressCount.toString() + " Results";
                    }
                    else {
                        $scope.AddressesResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.AddressesResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "AttentionText",
                    title: "Attention",
                },
                {
                    field: "Contact",
                    title: "Contact",
                    template: function (dataItem) {
                        var templateString = '<div style="position: relative;" hfc-contact-overlay="" con="dataItem"><a style="color: rgb(61,125,139)">' + dataItem.Contact + '</a><div class="contactCanvas" style="height:auto; width:480px !important;"></div></div>';
                        return templateString;
                    }
                },
                {
                    field: "Address1",
                    title: "Address",
                    template: function (dataItem) {
                        var templateString = "<address class=' inline-relative' hfc-googlemaps='' addr='dataItem'><span class='tooltip-wrapper'> <a style='color: rgb(61,125,139);' target='_blank' href='' ng-cloak ng-href='{{ GetGoogleAddressHref(dataItem) }}' ng-bind-html='AddressToString(dataItem) | unsafe'></a></span> <div  class='mapCanvas'></div> </address>";
                        return templateString;
                    }
                },
                {
                    field: "CrossStreet",
                    title: "Cross Street",
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        return AddressesMenuTemplate(dataitem);
                    }
                },
            ]
        };
        $scope.PaymentsGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            PaymentId: { editable: false },
                            Account: { editable: false },
                            OrderNumber: { editable: false },
                            PaymentMethod: { editable: false },
                            Authorisation: { editable: false },
                            PaymentDate: { editable: false },
                            PaymentAmount: { editable: false },
                            Memo: { editable: false },
                            ReverseReason: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("PaymentsId");
                //if (!$scope.Permission.EditSalesOrder) {
                //    var grid = $("#PaymentsId").data("kendoGrid");
                //    grid.hideColumn("");
                //}
                if (e.sender._data.length != 0) {
                    if ($scope.AllPaymentsCount == 0) {
                        $scope.PaymentsResultCount = $scope.AllPaymentsCount.toString() + " Result";
                    }
                    else if ($scope.AllPaymentsCount == 1) {
                        $scope.PaymentsResultCount = $scope.AllPaymentsCount.toString() + " Result";
                    }
                    else if ($scope.AllPaymentsCount < 6) {
                        $scope.PaymentsResultCount = $scope.AllPaymentsCount.toString() + " Results";
                    }
                    else {
                        $scope.PaymentsResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.PaymentsResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "PaymentId",
                    title: "Payment ID",
                    filterable: true,
                },
                {
                    field: "Account",
                    title: "Account Name",
                    //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= Account #</span></a>",
                    template: function (dataItem) {
                        return AccountTemplate(dataItem);
                    },
                },
                {
                    field: "OrderNumber",
                    title: "Order ID",
                    //template: "<a href='\\\\#!/Orders/#= OrderId #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a>",
                    template: function (dataItem) {
                        return OrderNumberTemplate(dataItem);
                    },
                },
                {
                    field: "PaymentMethod",
                    title: "Method",
                    filterable: true,
                },
                {
                    field: "Authorisation",
                    title: "Authorization",
                    filterable: true,
                },
                {
                    field: "PaymentDate",
                    title: "Payment Date",
                    filterable: HFCService.gridfilterableDate("datetime", "", ""),
                    template: function (dataitem) {
                        if (dataitem.PaymentDate == null || dataitem.PaymentDate == '')
                            return "";
                        else
                            return HFCService.KendoDateTimeFormat(dataitem.PaymentDate);
                    }
                        //"#=  (PaymentDate == null)? '' : kendo.toString(kendo.parseDate(PaymentDate), 'MM/dd/yyyy hh:mm tt') #",
                },
                {
                    field: "PaymentAmount",
                    title: "Amount",
                    filterable: true,
                    format: "{0:c2}",
                    attributes: { class: 'text-right' },
                },
                {
                    field: "Memo",
                    title: "Memo",
                    filterable: true,
                },
                {
                    field: "ReverseReason",
                    title: "Reverse Reason",
                    filterable: true,
                },
               
            ]
        };
        $scope.VendorsGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Name: { editable: false },
                            VendorType: { editable: false },
                            Location: { editable: false },
                            AccountRep: { editable: false },
                            RepPhone: { editable: false },
                            Status: { editable: false },
                            OrderingMethod: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("VendorsId");
                if (!$scope.Permission.EditVendor) {
                    var grid = $("#VendorsId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllVendorsCount == 0) {
                        $scope.VendorsResultCount = $scope.AllVendorsCount.toString() + " Result";
                    }
                    else if ($scope.AllVendorsCount == 1) {
                        $scope.VendorsResultCount = $scope.AllVendorsCount.toString() + " Result";
                    }
                    else if ($scope.AllVendorsCount < 6) {
                        $scope.VendorsResultCount = $scope.AllVendorsCount.toString() + " Results";
                    }
                    else {
                        $scope.VendorsResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.VendorsResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Name",
                    title: "Name",
                    //template: "<a href='\\\\#!/vendorview/#= Id #' style='color: rgb(61,125,139);'><span>#= Name #</span></a>",
                    template: function (dataItem) {
                        return VendorTemplate(dataItem);
                    },
                },
                {
                    field: "VendorType",
                    title: "Type",
                },
                {
                    field: "Location",
                    title: "Location",
                    template: function (dataItem) {
                        var templateString = "<address class=' inline-relative' hfc-googlemaps='' addr='dataItem'><span class='tooltip-wrapper'> <a style='color: rgb(61,125,139);' target='_blank' href='' ng-cloak ng-href='{{ GetGoogleAddressHref(dataItem) }}' ng-bind-html='AddressToString(dataItem) | unsafe'></a></span> <div  class='mapCanvas'></div> </address>";
                        return templateString;
                    }
                },
                {
                    field: "AccountRep",
                    title: "Account Rep",
                },
                {
                    field: "RepPhone",
                    title: "Rep Phone",
                    template: function (dataItem) {
                        return maskedDisplayPhone(dataItem.RepPhone);
                    }
                },
                {
                    field: "Status",
                    title: "Status",
                },
                {
                    field: "OrderingMethod",
                    title: "Ordering Method",
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                        EditVendor = '<li><a href="#!/vendor/' + dataitem.Id + '">Edit</a> </li>';
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + EditVendor + 
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        return hcontent;
                    }
                },
            ]
        };
        $scope.NotesGridOptions = {
            dataSource: {
                schema: {
                    model: {
                        fields: {
                            Type: { editable: false },
                            Title: { editable: false },
                            CategoriesName: { editable: false },
                            Account: { editable: false },
                            Opportunity: { editable: false },
                            Order: { editable: false },
                            FileName: { editable: false },
                            CreatedOn: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            dataBound: function (e) {
                $scope.onDataBinding("NotesId");
                if (!$scope.Permission.DisplaySalesOrder && !$scope.Permission.DisplayOpportunity && !$scope.Permission.DisplayAccount && !$scope.Permission.DisplayLead && !$scope.Permission.EditDocument) {
                    var grid = $("#NotesId").data("kendoGrid");
                    grid.hideColumn("");
                }
                if (e.sender._data.length != 0) {
                    if ($scope.AllNotesCount == 0) {
                        $scope.NotesResultCount = $scope.AllNotesCount.toString() + " Result";
                    }
                    else if ($scope.AllNotesCount == 1) {
                        $scope.NotesResultCount = $scope.AllNotesCount.toString() + " Result";
                    }
                    else if ($scope.AllNotesCount < 6) {
                        $scope.NotesResultCount = $scope.AllNotesCount.toString() + " Results";
                    }
                    else {
                        $scope.NotesResultCount = "5+ Results";
                    }
                }
                else {
                    $scope.NotesResultCount = "0 Result";
                }
            },
            sortable: true,
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "Type",
                    title: "Type",
                    filterable: true,
                },
                {
                    field: "Title",
                    title: "Title",
                    filterable: true,
                    template: function (dataitem) {
                        View = '<span>' + dataitem.Title + '</span>';
                        if(dataitem.FranchiseId)
                        {
                            if($scope.Permission.DisplayDocument)
                            View = '<a class="leaddet_hfclink" ng-click="Notes(' + dataitem.NoteId + ')" style="color: rgb(61,125,139);">' + dataitem.Title + '</a>';
                        }
                        else
                        {
                            if (dataitem.OrderId && $scope.Permission.DisplaySalesOrder)
                            View = '<a class="leaddet_hfclink" ng-click="Notes(' + dataitem.NoteId + ')" style="color: rgb(61,125,139);">' + dataitem.Title + '</a>';
                            else if (dataitem.OpportunityId && $scope.Permission.DisplayOpportunity)
                            View = '<a class="leaddet_hfclink" ng-click="Notes(' + dataitem.NoteId + ')" style="color: rgb(61,125,139);">' + dataitem.Title + '</a>';
                            else if (dataitem.AccountId && $scope.Permission.DisplayAccount)
                            View = '<a class="leaddet_hfclink" ng-click="Notes(' + dataitem.NoteId + ')" style="color: rgb(61,125,139);">' + dataitem.Title + '</a>';
                            else if (dataitem.LeadId && $scope.Permission.DisplayLead)
                            View = '<a class="leaddet_hfclink" ng-click="Notes(' + dataitem.NoteId + ')" style="color: rgb(61,125,139);">' + dataitem.Title + '</a>';
                        }                       
                        var hcontent = `` + View + ``;
                        return hcontent;
                    }
                },
                {
                    field: "CategoriesName",
                    title: "Category",
                    filterable: true,
                },
                {
                    field: "Account",
                    title: "Account",
                    //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= Account #</span></a>",
                    template: function (dataItem) {
                        return AccountTemplate(dataItem);
                    },
                },
                {
                    field: "Opportunity",
                    title: "Opportunity",
                    template: function (dataItem) {
                        return OpportunityTemplate(dataItem);
                    },
                },
                {
                    field: "Order",
                    title: "Order",
                    template: function (dataItem) {
                        return OrderTemplate(dataItem);
                    },
                },
                {
                    field: "FileName",
                    title: "View",
                    filterable: true,
                    template: function (dataitem) {
                        View = '<span>' + dataitem.FileName + '</span>';
                        if (dataitem.FileName) {
                            if (dataitem.FranchiseId) {
                                if ($scope.Permission.DisplayDocument)
                                    View = '<a class="leaddet_hfclink" href="/api/Download?streamid=' + dataitem.AttachmentStreamId + ' " target="_blank" style="color: rgb(61,125,139);">' + dataitem.FileName + '</a>';
                            }
                            else {
                                if (dataitem.OrderId && $scope.Permission.DisplaySalesOrder)
                                    View = '<a class="leaddet_hfclink" href="/api/Download?streamid=' + dataitem.AttachmentStreamId + ' " target="_blank" style="color: rgb(61,125,139);">' + dataitem.FileName + '</a>';
                                else if (dataitem.OpportunityId && $scope.Permission.DisplayOpportunity)
                                    View = '<a class="leaddet_hfclink" href="/api/Download?streamid=' + dataitem.AttachmentStreamId + ' " target="_blank" style="color: rgb(61,125,139);">' + dataitem.FileName + '</a>';
                                else if (dataitem.AccountId && $scope.Permission.DisplayAccount)
                                    View = '<a class="leaddet_hfclink" href="/api/Download?streamid=' + dataitem.AttachmentStreamId + ' " target="_blank" style="color: rgb(61,125,139);">' + dataitem.FileName + '</a>';
                                else if (dataitem.LeadId && $scope.Permission.DisplayLead)
                                    View = '<a class="leaddet_hfclink" href="/api/Download?streamid=' + dataitem.AttachmentStreamId + ' " target="_blank" style="color: rgb(61,125,139);">' + dataitem.FileName + '</a>';
                            }
                        }
                        else
                        {
                            View = '';
                        }
                        var hcontent = `` + View + ``;
                            return hcontent;
                    }
                },
                {
                    field: "CreatedOn",
                    title: "Created",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataitem) {
                        if (dataitem.CreatedOn == null || dataitem.CreatedOn == '')
                            return "";
                        else
                            return HFCService.KendoDateFormat(dataitem.CreatedOn);
                    }
                        //"#=  (CreatedOn == null)? '' : kendo.toString(kendo.parseDate(CreatedOn, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template: function (dataitem) {
                      EditNote = '';
                        if (dataitem.FranchiseId) {
                            if ($scope.Permission.EditDocument)
                                EditNote = '<li><a href="" ng-click="NotesEdit(' + dataitem.NoteId + ')">Edit</a> </li>';
                        }
                        else {
                            if (dataitem.OrderId && $scope.Permission.DisplaySalesOrder)
                                EditNote = '<li><a href="" ng-click="NotesEdit(' + dataitem.NoteId + ')">Edit</a> </li>';
                            else if (dataitem.OpportunityId && $scope.Permission.DisplayOpportunity)
                                EditNote = '<li><a href="" ng-click="NotesEdit(' + dataitem.NoteId + ')">Edit</a> </li>';
                            else if (dataitem.AccountId && $scope.Permission.DisplayAccount)
                                EditNote = '<li><a href="" ng-click="NotesEdit(' + dataitem.NoteId + ')">Edit</a> </li>';
                            else if (dataitem.LeadId && $scope.Permission.DisplayLead)
                                EditNote = '<li><a href="" ng-click="NotesEdit(' + dataitem.NoteId + ')">Edit</a> </li>';
                        }
                        var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">`
                                                                + EditNote +
                                                            `</ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                        if (EditNote)
                            return hcontent;
                        else
                            return '';
                    }
                },
            ]
        };
    }]);

