﻿'use strict';
appCP.controller('HFCPermissionSetSearchController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'HFCService', 'NavbarServiceCP',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, HFCService, NavbarServiceCP) {

        HFCService.setHeaderTitle("Permission Sets #");
        $scope.NavbarServiceCP = NavbarServiceCP;
       
        $scope.NavbarServiceCP.GlobalPermissionSets();
        var offsetvalue = 0;
        $scope.Permission = {};
        var Securitypermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Security')
        if (Securitypermission){
        $scope.Permission.ViewSecurity = Securitypermission.CanRead;
        $scope.Permission.AddSecurity = Securitypermission.CanCreate;
        $scope.Permission.EditSecurity = Securitypermission.CanUpdate;
        }
        function ActivateDeActivateTemplate(dataitem) {
            if ($scope.Permission.EditSecurity) {
                return '<ul> <li class="dropdown note1 ad_user"> <div class="btn-group">' +
                                ' <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                                ' <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu">' +
                                '<li><a href="#!/hfcPermissionSets/' + dataitem.Id + '">Edit</a></li> ' +
                                '</ul> </li>  </ul>'
            }
            else return "";
        }

        $scope.GetPermissionSets = function () {

            $scope.PermissionSets = {
                dataSource: {
                    transport: {
                        read: {
                            url: function (params) {

                                var url1 = '/api/Permission/0/GetPermissionSetsList';
                                return url1;
                            },
                            dataType: "json"
                        }

                    },
                    error: function (e) {

                        HFC.DisplayAlert(e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                PermissionSetName: { editable: false },
                                FranchiseId: { editable: false },
                                Description: { editable: false },
                                CreatedOn: { type: 'date', editable: false },
                                LastUpdatedOn: { type: 'date', editable: false },
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "PermissionSetName",
                        title: "Name",
                        template: "<a href='\\\\#!/hfcPermissionSetsView/#= Id #' style='color: rgb(61,125,139);'><span>#= PermissionSetName #</span></a> ",
                        filterable: {
                           
                            search: true
                        },
                        hidden: false,
                    },
                    {
                        field: "Description",
                        title: "Description",
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        hidden: false,


                    },
                    {
                        field: "CreatedOn",
                        title: "Created",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "HfcPermissionSetCreatedonFilterCalendar", offsetvalue),
                        hidden: false,
                        template: function (dataItem) {
                            return HFCService.NewKendoDateFormat(dataItem.CreatedOn);
                        }
                            //"#= kendo.toString(new Date(CreatedOn),'MM/dd/yyyy') #",

                    },
                    {
                        field: "LastUpdatedOn",
                        title: "Modified",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "HfcPermissionSetLastUpdatedonFilterCalendar", offsetvalue),
                        template:function (dataItem) {
                            return HFCService.NewKendoDateFormat(dataItem.LastUpdatedOn);
                        },
                            //"#= kendo.toString(new Date(LastUpdatedOn),'MM/dd/yyyy') #",
                        hidden: false,

                    },

                    {
                        field: "",
                        title: "",
                        template: function (dataitem) {
                            return ActivateDeActivateTemplate(dataitem);
                        }
                    },


                ],

                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                resizable: true,
                sortable: true,
                autoSync: true,
                noRecords: { template: "No records found" },
                scrollable: true,
                toolbar: [
                        {
                            template: kendo.template(
                                $(
                                    ' <script id="template" type="text/x-kendo-template"><div class="row no-margin vendor_search"><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="PermissionSearchgridSearch()" type="search" id="searchBox" placeholder="Search Permission Sets" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="PermissionSetsSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>')
                                .html())
                        }
                ],

            };

        };
        $scope.GetPermissionSets();

        $scope.PermissionSetsSearchClear = function () {
            $('#searchBox').val('');
            $("#gridPermissionSetsSearch").data('kendoGrid').dataSource.filter({
            });
        }

        $scope.PermissionSearchgridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#gridPermissionSetsSearch").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  {
                      field: "PermissionSetName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "Description",
                      operator: "contains",
                      value: searchValue
                  }
                ]
            });

        }

        //Redirect to Create Permission set Page
        $scope.NewPermissionSet = function () {
            window.location.href = '#!/hfcPermissionSet';
            //var url = "http://" + $window.location.host + "/#!/leads/";
            //$window.location.href = url;
        };
    }])