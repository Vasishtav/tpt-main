﻿appCP.controller('HFCPermissionSetController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'HFCService', 'NavbarServiceCP',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, HFCService, NavbarServiceCP) {



        $scope.NavbarServiceCP = NavbarServiceCP;
       
        $scope.NavbarServiceCP.GlobalPermissionSets();
        $scope.submitted = false;
        var dataSource = [];
        $scope.PermissionSetInformation = {
            PermissionSetName: '',
            Description: '',
        }
        $scope.IsView = false;

        $scope.Permission = {};
        var Securitypermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Security')

        $scope.Permission.ViewSecurity = Securitypermission.CanRead;
        $scope.Permission.AddSecurity = Securitypermission.CanCreate;
        $scope.Permission.EditSecurity = Securitypermission.CanUpdate;

        $scope.PermissionSetId = $routeParams.PermissionSetId == undefined ? 0 : $routeParams.PermissionSetId;

        if (window.location.href.includes("hfcPermissionSetsView")) {
            $scope.IsView = true;
        }
        if ($scope.IsView) {
            $http.get('/api/Permission/' + $scope.PermissionSetId + '/GetAssignedUserList').then(function (data) {
                $scope.AssignedUserList = data.data;
            });
        }

        $http.get('/api/Permission/' + $scope.PermissionSetId + '/GetPermission').then(function (data) {
            
            $scope.PermissionSetInformation = data.data;
            $scope.PermissionSets = $scope.PermissionSetInformation.PermissionSets;
            dataSource = new kendo.data.TreeListDataSource({
                data: $scope.PermissionSets
            });

            $scope.KendoTreeListOptions();
        });



        $scope.SavePermission = function () {
            

            $scope.submitted = true;
            if ($scope.IsDuplicateSetName) {
                $window.alert("Permission Set with this name is already exist")
                return;
            }

            var checkSpecialPermission = JSLINQ(dataSource._data)
                              .Where(function (item) { return item.SpecialPermission == true; });

            var checkReadPermission = JSLINQ(dataSource._data)
                             .Where(function (item) { return item.Read == true; });
            var checkUpdatePermission = JSLINQ(dataSource._data)
                            .Where(function (item) { return item.Update == true; });
            var checkDeletePermission = JSLINQ(dataSource._data)
                            .Where(function (item) { return item.Delete == true; });
            var checkCreatePermission = JSLINQ(dataSource._data)
                            .Where(function (item) { return item.Create == true; });

            if (checkReadPermission.items.length == 0 && checkUpdatePermission.items.length == 0 && checkDeletePermission.items.length == 0
                && checkCreatePermission.items.length == 0 && checkSpecialPermission.items.length == 0) {
                $window.alert("At least one check box should be checked, otherwise the Permission Set will be meaningless.")
                return;
            }


            if ($scope.HFCformpermissions.$valid == false) {

                return;
            }
            var PermissionSets = [];
            var dto = $scope.PermissionSetInformation;
            dto.PermissionSets = dataSource._data;

            $http.post('/api/Permission/0/UpdatePermissionSet', dto).then(function (data) {
                
                $scope.HFCformpermissions.$setPristine();
                window.location.href = '#!/hfcPermissionSetsView/' + data.data;
            });

        }
        $scope.KendoTreeListOptions = function () {
            $("#treelist").kendoTreeList({
                dataSource: dataSource,

                columns: [
                    {
                        field: "Name",
                        width: "260px",
                        expandable: true
                    },

                    {
                        field: "SpecialPermission",
                        title: "Is Accessible",

                        template: function (e) {
                            if (e.IsSpecialPermission && !$scope.IsView) {
                                return "<input type='checkbox' data-bind='checked: SpecialPermission' />";
                            }
                            else if (e.IsSpecialPermission && $scope.IsView) {
                                return "<input type='checkbox' disabled data-bind='checked: SpecialPermission' />";
                            }
                        },
                    },
                  {
                      field: "Create",

                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Create' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Create' />";
                          }
                      },

                  },
                  {
                      field: "Read",

                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Read' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Read' />";
                          }

                      },

                  },
                  {
                      field: "Update",

                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Update' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Update' />";
                          }
                      },
                  },
                  {
                      field: "Delete",

                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Delete' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Delete' />";
                          }
                      },

                  },



                ],
                dataBound: function () {
                    var view = this.dataSource.view();
                    this.items().each(function (index, row) {
                        kendo.bind(row, view[index]);
                    });
                }
            });
        }
        $scope.Cancel = function () {
            window.location.href = '#!/hfcPermissionSetsSearch';
        }

        //Redirect to Create Permission set Page
        $scope.NewPermissionSet = function () {
            window.location.href = '#!/hfcPermissionSet';
            //var url = "http://" + $window.location.host + "/#!/leads/";
            //$window.location.href = url;
        };
        $scope.EditPermissionSet = function () {
            window.location.href = "#!/hfcPermissionSets/" + $scope.PermissionSetId;
            //var url = "http://" + $window.location.host + "/#!/leads/";
            //$window.location.href = url;
        };

        $scope.CheckDuplicatePermissionName = function (name) {
            $scope.IsDuplicateSetName = false;
            if (name == undefined) {
                $scope.submitted = true;
                return;
            }
            else {
                $http.post('/api/Permission/' + $scope.PermissionSetId + '/CheckDuplicatePermissionSet/?name=' + name).then(function (data) {
                    

                    var permissionCheck = data.data;
                    if (permissionCheck) {
                        $scope.IsDuplicateSetName = true;
                        $window.alert("Permission Set with this name is already exist")
                    }
                    else {
                        $scope.IsDuplicateSetName = false;
                    }
                });
            }
        }

    }
]);