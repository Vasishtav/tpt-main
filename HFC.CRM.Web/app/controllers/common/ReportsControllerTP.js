﻿app.controller('ReportsControllerTP', ['$scope', 'HFCService', 'NavbarService', function ($scope, HFCService, NavbarService) {

   
    $scope.HFCService = HFCService;
   
    HFCService.setHeaderTitle("Reports #");

    $scope.NavbarService = NavbarService;
    $scope.NavbarService.SelectReports();
    $scope.NavbarService.EnableReportsSalesAgentTab();

   }])
