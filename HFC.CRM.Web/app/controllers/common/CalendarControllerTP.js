﻿app.controller('CalendarControllerTP', ['$scope', '$rootScope', '$http', '$routeParams', '$modal', '$window',
    '$location', 'HFCService', 'NavbarService', 'PersonService', '$route', 'calendarModalService', 'alertModalService', function ($scope, $rootScope, $http, $routeParams, $modal, $window, $location, HFCService, NavbarService, PersonService, $route, calendarModalService, alertModalService) {

        $scope.HFCService = HFCService;
        $scope.PersonService = PersonService;
        //$scope.CalendarService = CalendarService;
        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectCalendar();
        $scope.calendarModalService = calendarModalService;
        $scope.bussinessHourFlag = true;
        $scope.bussinessFlag = false;


        $scope.Permission = {};
        $scope.Userslist;
        var Calendarpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Calendar')
        var CalendarViewpermission = Calendarpermission.SpecialPermission.find(x=>x.PermissionCode == 'SaveCalendarViews').CanAccess;
        var OthersCalendarpermission = Calendarpermission.SpecialPermission.find(x=>x.PermissionCode == 'OtherCalendars ').CanAccess;

        $scope.Permission.AddTask = Calendarpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Task').CanAccess;
        $scope.Permission.AddEvent = Calendarpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Event').CanAccess;
        $scope.Permission.ViewCalendar = Calendarpermission.CanRead; //$scope.HFCService.GetPermissionTP('View MyOwn Calendar').CanAccess;
        $scope.Permission.AddSaveCalendar = CalendarViewpermission; //$scope.HFCService.GetPermissionTP('Add SaveCalendar').CanAccess;
        $scope.Permission.EditSaveCalendar = CalendarViewpermission; //$scope.HFCService.GetPermissionTP('Edit SaveCalendar').CanAccess;
        $scope.Permission.AddOtherCalendar = OthersCalendarpermission; //$scope.HFCService.GetPermissionTP('Add Other Calendar').CanAccess;
        $scope.Permission.EditOtherCalendar = OthersCalendarpermission; //$scope.HFCService.GetPermissionTP('Edit Other Calendar').CanAccess;
        $scope.Permission.ViewOtherCalendar = OthersCalendarpermission; //$scope.HFCService.GetPermissionTP('View Other Calendar').CanAccess;

        $scope.IsFullDay = true;
        $scope.UserEmail = $scope.HFCService.CurrentUser.Email;
        $scope.currentDateFlag = false;
        $scope.initializeFlag = false;


        HFCService.setHeaderTitle("Calendar #");


        $scope.tabselection = $routeParams.tabselection;
        $scope.URLCallBack = window.location.href.split('#!');
        $scope.URLCallBack = $scope.URLCallBack[1].split('/');
        $scope.pathType = $scope.URLCallBack[2];


        //if (window.location.href.includes("VendorcaseView"))
        //    $scope.CalendarService.setVendorcasevisible();
        //else
        //    $scope.CalendarService.setVendorcaseinvisible();


        


        if ($scope.pathType != undefined) {

            if ($scope.pathType.includes("opportunitySearch") && ($scope.pathId == null || $scope.pathId == "" || $scope.pathId == undefined)) {
                $scope.sectionType = "Opp-Search";
            }
            else if ($scope.pathType.includes("leadsSearch") && ($scope.pathId == null || $scope.pathId == "" || $scope.pathId == undefined)) {
                $scope.sectionType = "Leads-Search";
            }
            else if ($scope.pathType.includes("accountSearch") && ($scope.pathId == null || $scope.pathId == "" || $scope.pathId == undefined)) {
                $scope.sectionType = "Acct-Search";
            }
            else if ($scope.pathType.includes("quotesSearch") && ($scope.pathId == null || $scope.pathId == "" || $scope.pathId == undefined)) {
                $scope.sectionType = "Quote-Search";
            }
                //Lead
            else if ($scope.pathType.includes("leadAppointments")) {
                $scope.sectionType = "Lead-Appts";
            }
            else if ($scope.pathType.includes("Qualify")) {
                $scope.sectionType = "Qualify";
            }
            else if ($scope.pathType.includes("leadCommunication")) {
                $scope.sectionType = "Lead-Communication";
            }
            else if ($scope.pathType.includes("lead")) {
                $scope.sectionType = "Lead";
            }
                //Account
            else if ($scope.pathType.includes("accountCommunication")) {
                $scope.sectionType = "Acct-Communication";
            }
            else if ($scope.pathType.includes("accountAppointments")) {
                $scope.sectionType = "Acct-Appts";
            }
            else if ($scope.pathType.includes("accountPaymentSearch")) {
                $scope.sectionType = "Acct-Payment";
            }
            else if ($scope.pathType.includes("accountordersSearch")) {
                $scope.sectionType = "Acct-Order List";
            }
            else if ($scope.pathType.includes("accountopportunitySearch")) {
                $scope.sectionType = "Acct-Opp List";
            }
            else if ($scope.pathType.includes("account")) {
                $scope.sectionType = "Acct";
            }
            else if ($scope.pathType.includes("Accounts")) {
                $scope.sectionType = "Acct";
            }
            else if ($scope.pathType.includes("measurementDetails")) {
                $scope.sectionType = "Acct-Measurements";
            }
                //Opportunity
            else if ($scope.pathType.includes("measurementDetail")) {
                $scope.sectionType = "Opp-Measurement";
            }
            else if ($scope.pathType.includes("opportunityAppointments")) {
                $scope.sectionType = "Opp-Appts";
            }
            else if ($scope.pathType.includes("opportunityCommunication")) {
                $scope.sectionType = "Opp-Communication";
            }
            else if ($scope.pathType.includes("OpportunityDetail")) {
                $scope.sectionType = "Opp";
            }
            else if ($scope.pathType.includes("opportunityEdit")) {
                $scope.sectionType = "Opp";
            }
            else if ($scope.pathType.includes("Editquote")) {
                $scope.sectionType = "Quote";
            }
            else if ($scope.pathType.includes("quote")) {
                $scope.sectionType = "Quote";
            }
            else if ($scope.pathType.includes("moveQuote")) {
                $scope.sectionType = "Move Quote";
            }
                //Order
            else if ($scope.pathType.includes("orderSearch")) {
                $scope.sectionType = "Orders";
            }
            else if ($scope.pathType.includes("orderEdit")) {
                $scope.sectionType = "Order";
            }
            else if ($scope.pathType.includes("Orders")) {
                $scope.sectionType = "Order";
            }
            else if ($scope.pathType.includes("CaseView")) {
                $scope.sectionType = "CaseView";
                //  $scope.AddEvent('persongo');
            }
            else if ($scope.pathType.includes("Case")) {
                $scope.sectionType = "Case";
            }
            else if ($scope.pathType.includes("VendorcaseView")) {
                $scope.sectionType = "VendorcaseView";
            } else if ($scope.pathType.includes("dashboard")) {
                $scope.pathType = undefined;
                $scope.sectionType = null;
            }
            else if ($scope.pathType.includes("globalSearch")) {
                $scope.sectionType = "Search Results";
            }
            else if ($scope.pathType.includes("Reports")) {
                $scope.sectionType = "Reports";
            }
            else if ($scope.pathType.includes("SalesandPurchasingDetailReport")) {
                $scope.sectionType = "Sales Detail Report";
            }
            else if ($scope.pathType.includes("SalesSummaryReport")) {
                $scope.sectionType = "Sales Summary Report";
            }
            else if ($scope.pathType.includes("FranchisePerformanceReport")) {
                $scope.sectionType = "Performance Report";
            }
            else if ($scope.pathType.includes("SalesSummaryByMonth")) {
                $scope.sectionType = "Monthly Statistics Report";
            }
            else if ($scope.pathType.includes("VendorPerformance")) {
                $scope.sectionType = "Vendor Performance Report";
            }
            else if ($scope.pathType.includes("MarketingPerformance")) {
                $scope.sectionType = "Marketing Performance Report";
            }
            else if ($scope.pathType.includes("SalesTax")) {
                $scope.sectionType = "Sales Tax Report";
            }
            else if ($scope.pathType.includes("SalesJournalReport")) {
                $scope.sectionType = "Sales Journal Report";
            }
            else if ($scope.pathType.includes("MsrReport")) {
                $scope.sectionType = "MSR Report";
            }
            else if ($scope.pathType.includes("OpenBalanceReport")) {
                $scope.sectionType = "Open Balance Report";
            }
            else if ($scope.pathType.includes("RawDataReport")) {
                $scope.sectionType = "Raw Data Report";
            }
            else if ($scope.pathType.includes("procurementboard")) {
                $scope.sectionType = "Procurement";
            }
            else if ($scope.pathType.includes("bulkPurchasing")) {
                $scope.sectionType = "Bulk Purchasing";
            }
            else if ($scope.pathType.includes("ShippingNoticeDashboard")) {
                $scope.sectionType = "Shipments";
            }
            else if ($scope.pathType.includes("vendorinvoicePPVlist")) {
                $scope.sectionType = "Vendor Invoice/PPV List";
            }
            else if ($scope.pathType.includes("franchiseCase") || $scope.pathType.includes("caseAddEdit")) {
                $scope.sectionType = "Case Management";
            }
            else if ($scope.pathType.includes("KanbanView")) {
                $scope.sectionType = "Kanban View";
            }
            else if ($scope.pathType.includes("xPurchaseMasterView")) {
                $scope.sectionType = "xVendor Purchase Orders";
            }
            else if ($scope.pathType.includes("purchasemasterview")) {
                $scope.sectionType = "Purchase Orders";
            }
            else if ($scope.pathType.includes("sources")) {
                $scope.sectionType = "Campaigns";
            }
            else if ($scope.pathType.includes("listDocuments")) {
                $scope.sectionType = "Manage Documents";
            }
            else if ($scope.pathType.includes("ViewEmail")) {
                $scope.sectionType = "Email Settings";
            }
            else if ($scope.pathType.includes("disclaimerView") || $scope.pathType.includes("disclaimerEdit")) {
                $scope.sectionType = "Legal Disclaimer";
            }
            else if ($scope.pathType.includes("QuickbookSetup")) {
                $scope.sectionType = "QuickBook Setup";
            }
            else if ($scope.pathType.includes("appointmentTypes")) {
                $scope.sectionType = "Appointment Types";
            }
            else if ($scope.pathType.includes("vendors") || $scope.pathType.includes("vendorview") || $scope.pathType.includes("vendor")) {
                $scope.sectionType = "Vendors";
            }
            else if ($scope.pathType.includes("productSearch") || $scope.pathType.includes("productDetail") || $scope.pathType.includes("productEdit") || $scope.pathType.includes("productCreate")) {
                $scope.sectionType = "Products/Services";
            }
            else if ($scope.pathType.includes("pricingSettings") || $scope.pathType.includes("advancedpricingSettings")) {
                $scope.sectionType = "Pricing Management";
            }
            else if ($scope.pathType.includes("procurement")) {
                $scope.sectionType = "Procurement Settings";
            }
            else if ($scope.pathType.includes("buyerremorse")) {
                $scope.sectionType = "Sales Settings";
            } else if ($scope.pathType.includes("TaxSettings")) {
                $scope.sectionType = "Tax Settings";
            }
            else if ($scope.pathType.includes("users") || $scope.pathType.includes("newuser") || $scope.pathType.includes("edituser/")) {
                $scope.sectionType = "Users";
            }
            else if ($scope.pathType.includes("rolesSearch") || $scope.pathType.includes("rolesView/")) {
                $scope.sectionType = "Roles";
            }
            else if ($scope.pathType.includes("permissionSetsSearch") || $scope.pathType.includes("permissionSet") || $scope.pathType.includes("permissionSets") || $scope.pathType.includes("permissionSetsView")) {
                $scope.sectionType = "Permission Sets";
            }
            else if ($scope.pathType.includes("migrateMyCrmSource")) {
                $scope.sectionType = "MyCRM Data Conversion";
            }

        }
        $scope.pathId = $scope.URLCallBack[3];
        //
        if ($scope.URLCallBack[3] != undefined) {
            $scope.pathId2 = $scope.URLCallBack[4];
        }

        $scope.callBackFromAppointment = function () {
            if ($scope.pathType == 'leadsSearch') window.location.href = '/#!/leadsSearch';
            else if ($scope.pathType == 'accountSearch') window.location.href = '#!/accountSearch';
            else if ($scope.pathType == 'opportunitySearch') window.location.href = '#!/opportunitySearch';
            else if ($scope.pathType == 'quotesSearch') window.location.href = '#!/quotesSearch';
            else if ($scope.pathId2 != null && $scope.pathId2 != "" && $scope.pathId2 != undefined) window.location.href = '/#!/' + $scope.pathType + '/' + $scope.pathId + '/' + $scope.pathId2;
            else if ($scope.pathId != null && $scope.pathId != "" && $scope.pathId != undefined) window.location.href = '/#!/' + $scope.pathType + '/' + $scope.pathId;
            else window.location.href = '/#!/' + $scope.pathType;

        }

        // validate the calendar open flow from appointment screens
        if ($scope.calendarModalService.storedCalendarData) {
            $scope.calendarModalService.setPopUpDetails(true,
                                                        'Event',
                                                        $scope.calendarModalService.storedCalendarData.CalendarId,
                                                        $scope.calendarModalService.storedCalendarData.eventSuccessCallback,
                                                        null,
                                                        null,
                                                        null,
                                                        $scope.calendarModalService.storedCalendarData.StartDate,
                                                        $scope.calendarModalService.storedCalendarData.EndDate);
            $scope.calendarModalService.storedCalendarData = null;
        }

        $scope.Dayview = false;
        $scope.Weekview = false;
        $scope.Monthview = false;
        $scope.WorkWeekview = false;
        $scope.MainTabActive = 'active';
        $scope.activeTab = 0;
        $scope.selectedAssigneeUser = [];
        var checkValidate;
        //-----------------------------Save Calendar--------------------------------

        $scope.SetCalendarViewType = function (viewType) {

             
            $scope.tabselection = viewType;
            if ($scope.tabselection == 'day') {
                $scope.Dayview = true;
                $scope.Weekview = false;
                $scope.Monthview = false;
                $scope.WorkWeekview = false;
            }
            if ($scope.tabselection == 'week') {
                $scope.Dayview = false;
                $scope.Weekview = true;
                $scope.Monthview = false;
                $scope.WorkWeekview = false;
            }
            if ($scope.tabselection == 'month') {
                $scope.Dayview = false;
                $scope.Weekview = false;
                $scope.Monthview = true;
                $scope.WorkWeekview = false;
            }
            if ($scope.tabselection == 'workWeek') {
                $scope.Dayview = false;
                $scope.Weekview = false;
                $scope.Monthview = false;
                $scope.WorkWeekview = true;
            }
        }
        $scope.LoadDefaultCalendar = function () {
            $http.get('/api/CalTasks/0/ReadSavedcalendar').then(function (data) {
                $scope.SavedCalendarList = data.data;
                 
                var scheduler;
                var ActiveViewMode = '';
                $scope.selectedAssigneeUser = [];
                if ($scope.SavedCalendarList.length != 0) {
                    checkValidate = JSLINQ($scope.SavedCalendarList)
                              .Where(function (item) { return item.IsDefault == true; });

                    //Commented due to Error: items[]

                    //checkValidate.items[0].active = "active";
                    if (checkValidate.items.length > 0) {
                        $scope.MainTabActive = '';
                        //Added due to Error: items[]
                        checkValidate.items[0].active = "active";
                        // bussiness flag setter
                        $scope.bussinessHourFlag = checkValidate.items[0]['IsBussinessHour'];

                        // added by azhagu

                        // $scope.ResetSavedCalendar(checkValidate.items[0].SaveCalendarId);
                        $scope.activeTab = checkValidate.items[0].SaveCalendarId;
                        if (checkValidate.items[0].Viewmode == '1')
                            ActiveViewMode = 'day';
                        else if (checkValidate.items[0].Viewmode == '2')
                            ActiveViewMode = 'week';
                        else if (checkValidate.items[0].Viewmode == '3')
                            ActiveViewMode = 'month';
                        else if (checkValidate.items[0].Viewmode == '4')
                            ActiveViewMode = 'workWeek';


                        //Set the selected users
                        if ($("#a_selectAllSales").hasClass("active")) {
                            $("#a_selectAllSales").removeClass('active');
                        }
                        if ($("#a_selectAllInstallers").hasClass("active")) {
                            $("#a_selectAllInstallers").removeClass('active');
                        }
                        if ($("#a_selectAllOthers").hasClass("active")) {
                            $("#a_selectAllOthers").removeClass('active');
                        }
                        if ($scope.SavedCalendarList && $scope.Userslist) {

                            for (var i = 0; i < $scope.Userslist.Users.length; i++) {
                                for (var j = 0 ; j < checkValidate.items[0].SavedCalendarUsers.length ; j++) {
                                    if ($scope.Userslist.Users[i].PersonId == checkValidate.items[0].SavedCalendarUsers[j].SelectedUserid) {
                                        $scope.Userslist.Users[i].SelectColor = "list-group-item active";
                                        $scope.selectedAssigneeUser.push(checkValidate.items[0].SavedCalendarUsers[j].SelectedUserid);
                                        break;
                                    }
                                    else
                                        $scope.Userslist.Users[i].SelectColor = "list-group-item";
                                }
                            }

                        }
                        $scope.SetCalendarViewType(ActiveViewMode);
                        if (!$scope.initializeFlag) {
                            $scope.KendoSchedulerIntitilize();
                        }
                        $scope.Value = $scope.Userslist;
                        var sbhbutton = $("#showBusinessHourButton");
                        if (!$scope.bussinessHourFlag) {
                            if (sbhbutton && sbhbutton[0] && sbhbutton[0].innerText != "Show business hours") {
                                $(".k-scheduler-fullday").click();
                            }
                        } else {
                            if (sbhbutton && sbhbutton[0] && sbhbutton[0].innerText == "Show business hours") {
                                $(".k-scheduler-fullday").click();
                            }
                        }
                    } else {
                        loadMainView();
                    }
                }
                else {
                    loadMainView();
                }
            });
        }

        var loadMainView = function () {
            $scope.selectedAssigneeUser.push(HFCService.CurrentUser.PersonId);
            if (!$scope.initializeFlag) {
                $scope.KendoSchedulerIntitilize();
            }
        }

        $scope.SetMainTabActive = function () {
            $scope.MainTabActive = 'active';
            $scope.MainCalendarSelected = true;
            $scope.selectedAssigneeUser = [];
            var GetPreviousActiveTab = JSLINQ($scope.SavedCalendarList)
                     .Where(function (item) { return item.active == "active"; });
            if (GetPreviousActiveTab && GetPreviousActiveTab.items && GetPreviousActiveTab.items.length != 0) {
                GetPreviousActiveTab.items[0].active = '';
            }
            //$scope.ResetSavedCalendar(0);
            // HFCService.CurrentUser.PersonId;

            $scope.activeTab = 0;

            if ($("#a_selectAllSales").hasClass("active")) {
                $("#a_selectAllSales").removeClass('active');
            }
            if ($("#a_selectAllInstallers").hasClass("active")) {
                $("#a_selectAllInstallers").removeClass('active');
            }
            if ($("#a_selectAllOthers").hasClass("active")) {
                $("#a_selectAllOthers").removeClass('active');
            }

            // updated
            for (var i = 0; i < $scope.Userslist.Users.length; i++) {
                    if ($scope.Userslist.Users[i].PersonId == HFCService.CurrentUser.PersonId) {
                        $scope.Userslist.Users[i].SelectColor = "list-group-item active";
                        if ($scope.Userslist.Users[i].InInstall) {
                            var aId = $('#' + 'installer_' + $scope.Userslist.Users[i].PersonId);
                            $(aId).addClass('active');
                        }
                        if ($scope.Userslist.Users[i].InSales) {
                            var aId = $('#' + 'sale_' + $scope.Userslist.Users[i].PersonId);
                            $(aId).addClass('active');
                        }
                        if (!$scope.Userslist.Users[i].InInstall && !$scope.Userslist.Users[i].InSales) {
                            var aId = $('#' + 'other_' + $scope.Userslist.Users[i].PersonId)
                            $(aId).addClass('active');
                        }
                        $scope.selectedAssigneeUser.push(HFCService.CurrentUser.PersonId);
                    }
                    else
                        $scope.Userslist.Users[i].SelectColor = "list-group-item";
            }
            for (var i = 0; i < $scope.Userslist.Users.length; i++) {
                if ($scope.Userslist.Users[i].PersonId == HFCService.CurrentUser.PersonId) {
                    $scope.Userslist.Users[i].SelectColor = "list-group-item active";
                    if ($scope.Userslist.Users[i].InInstall) {
                        var aId = $('#' + 'installer_' + $scope.Userslist.Users[i].PersonId)
                        $(aId).addClass('active');
                    }
                    if ($scope.Userslist.Users[i].InSales) {
                        $(aId).addClass('active');
                    }
                    if (!$scope.Userslist.Users[i].InInstall && !$scope.Userslist.Users[i].InSales) {
                        var aId = $('#' + 'other_' + $scope.Userslist.Users[i].PersonId)
                        $(aId).addClass('active');
                    }
                    var id = "#other_" + $scope.Userslist.Users[i].PersonId;
                    $(id).addClass("active");

                }
                else {
                    if ($scope.Userslist.Users[i].InInstall) {
                        var aId = $('#' + 'installer_' + $scope.Userslist.Users[i].PersonId)
                        if ($(aId).hasClass("active")) {
                            $(aId).removeClass('active');
                        }
                    }
                    if ($scope.Userslist.Users[i].InSales) {
                        var aId = $('#' + 'sale_' + $scope.Userslist.Users[i].PersonId)
                        if ($(aId).hasClass("active")) {
                            $(aId).removeClass('active');
                        }
                    }
                    if (!$scope.Userslist.Users[i].InInstall && !$scope.Userslist.Users[i].InSales) {
                        var aId = $('#' + 'other_' + $scope.Userslist.Users[i].PersonId)
                        if ($(aId).hasClass("active")) {
                            $(aId).removeClass('active');
                        }
                    }
                }
            }
            var sbhbutton = $("#showBusinessHourButton");
            if (sbhbutton && sbhbutton[0] && sbhbutton[0].innerText == "Show business hours") {
                $(".k-scheduler-fullday").click();
            }
            var scheduler = $("#scheduler").data("kendoScheduler");
            scheduler.date(new Date($scope.selectedDate));
            scheduler.view('day');
        }

        $scope.ResetSavedCalendar = function (id) {

            $http.get('/api/CalTasks/' + id + '/ReadSavedcalendar/').then(function (data) {

                $scope.SavedCalendarList = data.data;
                checkValidate = JSLINQ($scope.SavedCalendarList)
                      .Where(function (item) { return item.IsDefault == true; });

                if (checkValidate.items.length > 0) {
                    $scope.MainTabActive = '';
                    var ActiveViewMode = '';

                    // bussiness flag setter
                    $scope.bussinessHourFlag = checkValidate.items[0]['IsBussinessHour'];

                    // added by azhagu

                    // $scope.ResetSavedCalendar(checkValidate.items[0].SaveCalendarId);
                    $scope.activeTab = checkValidate.items[0].SaveCalendarId;
                    if (checkValidate.items[0].Viewmode == '1')
                        ActiveViewMode = 'day';
                    else if (checkValidate.items[0].Viewmode == '2')
                        ActiveViewMode = 'week';
                    else if (checkValidate.items[0].Viewmode == '3')
                        ActiveViewMode = 'month';
                    else if (checkValidate.items[0].Viewmode == '4')
                        ActiveViewMode = 'workWeek';
                    var scheduler = $("#scheduler").data("kendoScheduler");
                    scheduler.view(ActiveViewMode);

                }
            });
        }
        $scope.UpdateSavedCalendar = function (saveorder) {
            var myDiv = $('.calendarmanage_popup');
            myDiv[0].scrollTop = 0;
            //Id = 1;
            //https://archive.codeplex.com/?p=jslinq

            var ValidateviewTitle = JSLINQ(saveorder)
                       .Where(function (item) { return item.ViewTitle == undefined || item.ViewTitle == ''; });
            if (ValidateviewTitle != null) {
                if (ValidateviewTitle.items.length > 0) {
                    HFC.DisplayAlert('Enter View Title');
                    return;

                }
            }

            for (var idx = 0; idx <= saveorder.length - 1; idx++) {
                var ValidateuniqueviewTitle = JSLINQ(saveorder)
                     .Where(function (item) { return item.ViewTitle == saveorder[idx].ViewTitle && item.SaveCalendarId != saveorder[idx].SaveCalendarId; });
                if (ValidateuniqueviewTitle.items.length > 0) {
                    HFC.DisplayAlert('Calendar View Title Should be unique');
                    return;
                }
            }

            $http.put('/api/CalTasks/1/UpdateSavedCalendar', saveorder).then(function (response) {
                $scope.IsBusy = false;
                HFC.DisplaySuccess("Update Saved Calendar");
                // $scope.SavedCalendarList = $scope.savedCalendarListBackup;
                var resData = response.config.data;
                var temp = [];
                // reorder the views
                for (var i = 0; i < resData.length; i++) {
                    for (var j = 0; j < $scope.savedCalendarListBackup.length; j++) {
                        if ($scope.savedCalendarListBackup[j]['SavedCalendarOrder'][0]['Id'] == resData[i]['Id']) {
                            temp.push($scope.savedCalendarListBackup[j]);
                        }
                    }
                }
                $scope.SavedCalendarList = temp;
                for (var i = 0; i < $scope.SavedCalendarList.length; i++) {
                    for (var j = 0; j < response.config.data.length; j++) {
                        if ($scope.SavedCalendarList[i]['SavedCalendarOrder'][0]['SaveCalendarId'] === response.config.data[j]['SaveCalendarId']) {
                            $scope.SavedCalendarList[i]['SavedCalendarOrder'] = [response.config.data[j]];
                            $scope.SavedCalendarList[i]['IsDefault'] = response.config.data[j]['IsDefault'];
                        }
                    }
                }
                var defaultCalendar = JSLINQ($scope.SavedCalendarList)
                    .Where(function (item) { return item.IsDefault == true; });
                if (defaultCalendar.items.length > 0) {
                    $scope.bussinessHourFlag = defaultCalendar.items[0]['IsBussinessHour'];
                    $scope.loadTab(defaultCalendar.items[0].SaveCalendarId)
                }
                else
                    $scope.ResetSavedCalendar(0);

            });
            $("#manageCalendarSaveModal").modal("hide");
        }
        $scope.cancelmanagecalendar = function () {
            var myDiv = $('.calendarmanage_popup');
            myDiv[0].scrollTop = 0;
            $("#manageCalendarSaveModal").modal("hide");
            $scope.SavedCalendarList = $scope.savedCalendarListBackup;

        }
        $scope.MoveUp = function (index, index1, cal, SavedCalendarList) {
            if (index1 < 0) {
                return;
            }
           // var value1 = cal;
           // var value2 = SavedCalendarList[index1];
           // SavedCalendarList[index1] = value1;
          //  SavedCalendarList[index] = value2;
            var temp = SavedCalendarList[index1];
            SavedCalendarList[index1] = SavedCalendarList[index];
            SavedCalendarList[index] = temp;
        }
        $scope.MoveDown = function (index, index1, cal, SavedCalendarList) {
            if (index1 > SavedCalendarList.length - 1) {
                return;
            }
          //  var value1 = cal;
           // var value2 = SavedCalendarList[index1];
           // SavedCalendarList[index1] = value1;
            // SavedCalendarList[index] = value2;
            var temp = SavedCalendarList[index1];
            SavedCalendarList[index1] = SavedCalendarList[index];
            SavedCalendarList[index] = temp;
        }
        $scope.SetDefaultSavedCalendar = function (Id) {
            //  alert(Id);
            var defaultCalendar = JSLINQ($scope.SavedCalendarList)
                  .Where(function (item) { return item.Id == Id; });
            if (defaultCalendar != null) {
                if (defaultCalendar.items.length > 0) {
                    for (var idx = 0; idx <= $scope.SavedCalendarList.length - 1; idx++) {
                        if (defaultCalendar.items[0].Id == $scope.SavedCalendarList[idx].Id) {
                            $scope.SavedCalendarList[idx].IsDefault = true;
                        }
                        else {
                            $scope.SavedCalendarList[idx].IsDefault = false;
                        }

                    }
                    return;
                }
            }

        }
        $scope.DeleteSavedCalendar = function (Id) {
            var deleteurl = '/api/CalTasks/' + Id + '/DeleteSavedCalendar/';
            $http.delete(deleteurl).then(function (response) {
                var res = response.data;
                $("#manageCalendarSaveModal").modal("hide");
                if (res == 'Success') {
                    HFC.DisplaySuccess("Success");
                    $scope.ResetSavedCalendar(0);
                    // window.location.reload();
                    // $route.reload();
                    init();
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
        }
        $scope.AddCalendar = function () {
            $("#calendarSaveModal").modal("show");
            //Commented due to Error: [$rootScope:inprog] $apply already in progress
            //$scope.$apply();
        }
        $scope.ManageCalendar = function () {
            $http.get('/api/CalTasks/0/GetManagedCalendarListbyUserId')
                 .then(function (data) {
                     $scope.savedCalendarListBackup = $scope.SavedCalendarList;
                     $scope.SavedCalendarList = data.data;

                     // $scope.$apply();  //Removed causing error : $digest is already in progress
                     $("#manageCalendarSaveModal").modal("show");
                 });
            $("#manageCalendarSaveModal").modal("show");
        }
        $scope.ManageSaveCalendar = function () {
            var dto = null;
            dto = {
                PersonIds: [1, 2],
                StartDate: new Date(),
                EndDate: new Date(),
                ViewType: 'Month'
            }
            $http.post('/api/CalTasks/0/ManageSaveCalendar/', dto).then(function (res) {
                $scope.IsBusy = false;
                HFC.DisplaySuccess("Manage Calendar Saved");
                alert("Manage Calendar Saved");
                $("#manageCalendarSaveModal").modal("hide");
            }, function (res) {
                HFC.DisplayAlert(res.statusText);
                $scope.IsBusy = false;
            });
        }

        $scope.SaveCalendar = function (savemodel) {

            var selectedusers = $scope.getSelectedUsers();
            var viewtitle = savemodel.viewtitle;
            if (selectedusers.length == 0) {
                HFC.DisplayAlert('Select User');
                return;
            }
            if (viewtitle == null || viewtitle == undefined) {
                HFC.DisplayAlert('Enter View Title');
                return;
            }

            var ValidateuniqueviewTitle = JSLINQ($scope.SavedCalendarList)
                   .Where(function (item) { return item.ViewTitle == savemodel.viewtitle; });
            if (ValidateuniqueviewTitle.items.length > 0) {
                HFC.DisplayAlert('Calendar View Title Should be unique');
                return;
            }

            var ViewName = $("#scheduler").data("kendoScheduler")._selectedViewName;
            var IsDefault = savemodel.IsDefault;
            var dto = null;
            var isBussinessHour = true;
            var hourElement = $("#showBusinessHourButton");
            if (hourElement[0] && hourElement[0].innerText != "Show Full Days") {
                isBussinessHour = false;
            }
            dto = {
                ViewTitle: viewtitle,
                PersonIds: selectedusers,
                StartDate: new Date(),
                EndDate: new Date(),
                ViewType: ViewName,
                IsDefault: IsDefault,
                isBussinessHour: isBussinessHour
            }

            $http.post('/api/CalTasks/0/SaveCalendar/', dto).then(function (res) {
                $scope.IsBusy = false;
                if (res.data == 'Maximum 5 tabs can be defined') {
                    HFC.DisplayAlert(res.data);
                }
                else {
                    HFC.DisplaySuccess("Calendar Saved");
                    // $route.reload();
                    init();
                }

                $("#calendarSaveModal").modal("hide");
            }, function (res) {
                HFC.DisplayAlert(res.statusText);
                $scope.IsBusy = false;
            });
        }
        //-----------------------------Save Calendar-------------------------------- 



        var filter = {
            personIds: HFCService.CurrentUser.PersonId,
            IsMainCalendar: true
        };
        $scope.getSelectedUsers = function () {
            var arr = [];
            $("#divSales a").each(function () {
                if ($(this).is(':visible')) {
                    if ($(this).hasClass("active")) {
                        var value = $(this).attr("data-bind");
                        arr.push(value);
                    }
                }
            });
            $("#divInstall a").each(function () {
                if ($(this).is(':visible')) {
                    if ($(this).hasClass("active")) {
                        var value = $(this).attr("data-bind");
                        arr.push(value);
                    }
                }
            });
            $("#divOther a").each(function () {
                if ($(this).is(':visible')) {
                    if ($(this).hasClass("active")) {
                        var value = $(this).attr("data-bind");
                        arr.push(value);
                    }
                }
            });
            return arr;
        }

        $scope.FilterCalendar = function (e) {
            $scope.selectedAssigneeUser = $scope.getSelectedUsers();
             var scheduler = $("#scheduler").data("kendoScheduler");
             scheduler.dataSource.read();
        }

        // kendo scheduler data source seperate object
        var schedulerDataSourceCreator = function () {
            $scope.schedulerDataSource = new kendo.data.SchedulerDataSource({
                batch: true,
                transport: {
                    read: {
                        url: function (params) {
                            var scheduler = $("#scheduler").data("kendoScheduler");
                            var view = scheduler.view();

                            var endDateObject = view.endDate();
                            var startDateObject = view.startDate();

                            var startDate = (startDateObject.getMonth() + 1) + '/' + startDateObject.getDate() + '/' + startDateObject.getFullYear();
                            var endDate = (endDateObject.getMonth() + 1) + '/' + endDateObject.getDate() + '/' + endDateObject.getFullYear();
                                
                            var url1 = '/api/CalTasks/0/ReadSelection?PersonIds=' + $scope.selectedAssigneeUser + '&start=' + startDate + '&end=' + endDate;
                            return url1;
                        },
                        dataType: "json",
                        data: filter
                    },
                    destroy: {
                        url: function (params) {
                            var url1 = '/api/CalTasks/' + params.TaskID + '/Delete';
                            return url1;
                        },
                        dataType: "jsonp"
                    }
                },
                serverFiltering: true,
                schema: {
                    timezone: "Etc/UTC",
                    model: {
                        id: "TaskID",
                        fields: {
                            Id: { from: "TaskID" },
                            TaskID: { type: "number" },
                            title: {
                                from: "Title",
                                defaultValue: "No title",
                                title: "Subject",
                                validation: { required: true }
                            },
                            start: { type: "date", from: "Start" },
                            end: { type: "date", from: "End" },
                            startTimezone: { from: "StartTimezone" },
                            endTimezone: { from: "EndTimezone" },
                            description: { from: "Description" },
                            recurrenceId: { from: "RecurrenceID" },
                            recurrenceRule: { from: "RecurrenceRule" },
                            recurrenceException: { from: "RecurrenceException" },
                            ownerId: { from: "OwnerID" },
                            isAllDay: { type: "boolean", from: "IsAllDay" },
                            ReminderId: { from: "ReminderId" },
                            AppointmentTypeId: { from: "AppointmentType", defaultValue: 1 },
                            location: { from: "Location" },
                            IsPrivate: { from: "IsPrivate" },
                            AppointmentTypeColor: { from: "AppointmentTypeColor" },
                            UserRoleColor: { from: "UserRoleColor" },
                            UserTextColor: { from: "UserTextColor" },
                            IsTask: { from: "IsTask" },
                            AccountId: { from: "AccountId" },
                            OpportunityId: { from: "OpportunityId", defaultValue: 0 },
                            OpportunityName: { from: "OpportunityName", defaultValue: "" }
                        }
                    }
                }
            });
        }

        $scope.KendoSchedulerIntitilize = function (e) {
            var scheduler = $("#scheduler").data("kendoScheduler");
            schedulerDataSourceCreator();
            $scope.initializeFlag = true;
            if (scheduler != undefined) {
                var selectedViewName = scheduler._selectedViewName;
                $scope.SetCalendarViewType(selectedViewName);
            }

            var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
            if (e === 'manualSelected') {
                $scope.selectedAssigneeUser = $scope.getSelectedUsers();
            }
          
            if ($scope.Permission.ViewOtherCalendar==false) {
                $scope.selectedAssigneeUser = HFCService.CurrentUser.PersonId;
            }
            var start = utc;
            var end = utc;
            // var filterurl = '/CalTasks/Read/?LeadId=0&PersonIds=' + PersonIds + '&start=' + start + '&end=' + end;
            ResetScheduler();
            var calendarModule = $("#calendar").data("kendoCalendar");
            if (calendarModule) { // added for undefined case
                $scope.selectedDate = $("#calendar").data("kendoCalendar")._current;
                calendarModule.bind("change", function () {
                    var scheduler = $("#scheduler").data("kendoScheduler");
                    var selectedViewName = scheduler._selectedViewName;
                    $scope.selectedDate = this.value();
                    scheduler.date(new Date($scope.selectedDate));
                });
            }

            if (!$scope.selectedDate)
                $scope.selectedDate = new Date();

            $("#scheduler").kendoScheduler({

                date: $scope.selectedDate,

                footer: false,
                workDayStart: new Date("2019/1/1 08:00 AM"),
                workDayEnd: new Date("2019/1/1 08:00 PM"),

                showWorkHours: true,
                moveStart: $scope.moveStart,
                moveEnd: $scope.moveEnd,

                dataBound: function (e) {


                    var view = this.view();
                    if (!$scope.bussinessFlag) {
                        if (view) {
                            var calendar = $("#calendar").data("kendoCalendar");
                            var now;
                            var selectedDate;

                            if (view._startDate) {
                                now = new Date(view._startDate);
                                selectedDate = new Date(view._startDate);
                            } else {
                                now = new Date();
                                selectedDate = new Date();
                            }
                            if (calendar) {
                                if (view.title == "Work Week") {
                                    var dayIndex = now.getDay();
                                    var reducedValue = 1 - dayIndex;
                                    if (reducedValue < 0) {
                                        selectedDate.setHours(reducedValue * 24);
                                    }
                                    calendar.value(selectedDate);
                                } else if (view.title == "Week") {
                                    var dayIndex = now.getDay();
                                    var reducedValue = 0 - dayIndex;
                                    if (reducedValue < 0) {
                                        selectedDate.setHours(reducedValue * 24);
                                    }
                                    calendar.value(selectedDate);
                                } else if (view.title == "Day") {
                                    //var current = $("#scheduler").data("kendoScheduler").date();
                                    calendar.value($scope.selectedDate);
                                } else if (view.title == "Month") {
                                    calendar.value(new Date(view._firstDayOfMonth));
                                }
                            }
                        }
                    } else {
                        $scope.bussinessFlag = false;
                    }
                    var events = this.dataSource.view();
                    var eventElement;
                    var event;

                    // condition added because _selectedViewName raising issue
                    if ($("#scheduler").data("kendoScheduler")) {
                        var ViewName = $("#scheduler").data("kendoScheduler")._selectedViewName;
                        for (var idx = 0, length = events.length; idx < length; idx++) {
                            event = events[idx];

                            //get event element

                            eventElement = view.element.find("[data-uid=" + event.uid + "]");

                            //set the backgroud of the element
                            eventElement.css("border-color", event.AppointmentTypeColor);//appointment color
                            eventElement.css("background-color", event.UserRoleColor);//user color
                            eventElement.css("color", event.UserTextColor);//text color
                            eventElement.css("border-width", "3px");
                            eventElement.css("cursor", "pointer");
                            if (event.eventType == 2) {
                                    $scope.TextToDisplayInView(event.TimeSlot, ViewName, eventElement, event);                                
                            }
                            if (event.eventType == 1) {
                                $scope.TextToDisplayTaskView(eventElement, event)
                            }

                            if (event.recurrenceRule != null) {
                                var recurenceItems = JSLINQ(e.sender._data)
                                                             .Where(function (item) { return item.recurrenceId == event.recurrenceId });
                                if (recurenceItems != null) {
                                    if (recurenceItems.items.length > 0) {
                                        for (var rdx = 0; rdx <= recurenceItems.items.length - 1; rdx++) {
                                            if (recurenceItems.items[rdx].uid != null) {
                                                if (recurenceItems.items[rdx]._defaultId == 0) {
                                                    recurenceItems.items[rdx]._defaultId = "";
                                                }
                                                eventElement = view.element.find("[data-uid=" + recurenceItems.items[rdx].uid + "]");
                                                eventElement.css("border-color", recurenceItems.items[rdx].AppointmentTypeColor);//appointment color
                                                eventElement.css("background-color", recurenceItems.items[rdx].UserRoleColor);
                                                eventElement.css("color", event.UserTextColor);//text color
                                                eventElement.css("border-width", "3px");
                                                eventElement.css("cursor", "pointer");
                                                if (event.eventType == 2) {                                                   
                                                        $scope.TextToDisplayInView(event.TimeSlot, ViewName, eventElement, event, true);                                                   
                                                }
                                            }

                                        }
                                    }
                                }

                            }
                        }
                        initializeFunctionalBehaviour();
                    }
                },
                editable: {
                    editRecurringMode: "occurrence"
                },

                views: [

                  {
                      type: "day", selected: $scope.Dayview, minorTickCount: 4, majorTick: 60, slotHeight: 10
                  },
                   { type: "workWeek", selected: $scope.WorkWeekview, minorTickCount: 4, majorTick: 60, slotHeight: 10 },
                  { type: "week", selected: $scope.Weekview, minorTickCount: 4, majorTick: 60, slotHeight: 10 },
                 { type: "month", selected: $scope.Monthview, minorTickCount: 4, majorTick: 60, slotHeight: 10 },
                    //"agenda",
                   // { type: "timeline", eventHeight: 500 },
                   //{eventTimeTemplate: kendo.template("<span>#=kendo.toString(start, 'HH:mm')#</span> - <span>#=kendo.toString(end, 'HH:mm')#</span>") },

                ],
                timezone: "Etc/UTC",
                dataSource: $scope.schedulerDataSource,
                add: function (e) {
                    e.preventDefault();
                  //  calendarModalService.setPopUpDetails(true, "Event", null, $scope.reloadScheduler, $scope.sectionType, e, true); - implement through the single click event handler
                },
                edit: function (e) {
                    e.preventDefault();
                    if ($scope.Permission.AddEvent == true) {
                        if (e['event'] && e['event']['IsTask']) {
                            calendarModalService.setPopUpDetails(true, "Task", e['event']['Id'], $scope.reloadScheduler, $scope.sectionType, null, true, e['event']['start'], e['event']['end']);
                        } else {
                            calendarModalService.setPopUpDetails(true, "Event", e['event']['Id'], $scope.reloadScheduler, $scope.sectionType, null, true, e['event']['start'], e['event']['end']);
                        }
                    }
                    else {
                        e.preventDefault();
                    }
                   
                },

            });

            $('<a id="showBusinessHourButton" href="#"   class="k-link k-button k-scheduler-fullday">Show Full Days</a>').appendTo($(".k-scheduler-toolbar"));
            // $('<a ng-if="IsFullDay==false" href="#"   class="k-link k-button k-scheduler-fullday"><span class="k-icon k-i-clock"></span>Show Full Days</a>').appendTo($(".k-scheduler-toolbar"));

            // Throws error while completing a task-murugan
             
            var scheduler = $("#scheduler").data("kendoScheduler");
            if (scheduler) {
                scheduler.element.on("click", ".k-scheduler-fullday", function (e) {
                    e.preventDefault();
                    $scope.bussinessFlag = true;
                    var view = scheduler.view();

                    view.trigger("navigate", {
                        view: view.name || view.options.name,
                        date: new Date($scope.selectedDate),
                        isWorkDay: !view.options.showWorkHours
                    });
                    var sbhbutton = $("#showBusinessHourButton");
                    if (view.options.showWorkHours) {
                        sbhbutton.text('Show business hours');
                    }
                    else {
                        sbhbutton.text('Show Full Days');
                    }
                });
            }


            var schedulerFooter = $(".k-scheduler-footer");
            if (schedulerFooter) {
                schedulerFooter.text('');
            }
        }
        $("#calendar").kendoCalendar();

        //perform single-click action
        var clicks = 0;
        $scope.handleEventTileClick = function (e) {
            if ($scope.Permission.AddEvent) {
                if (!($(e.target).hasClass('tile-hover-wrapper'))) {
                    clicks++;
                    setTimeout(function () {
                        $(".tile-hover-wrapper").css("display", "none");
                        if (clicks === 1) {
                            openViewPopup(e);
                        } else {
                            e.preventDefault();
                        }
                        clicks = 0;
                    }, 700);
                }
            }
            else
                e.preventDefault();
        }

        // open view pop up for event /task
        function openViewPopup(e) {
            if ($scope.Permission.AddEvent) {
            var target = $(e.target);
            var scheduler = $("#scheduler").getKendoScheduler();
            var event;
            var i = 0;
            do {
                if (i > 0) {
                    target = $(target).parent();
                }
                if (target.data("uid")) {
                    event = scheduler.occurrenceByUid(target.data("uid"));
                }
                i++;
            } while (!event);

            $scope.calendarModalService.getAppointmentData(event.Id, event.IsTask, event.start, event.end);
                $scope.$apply();
            }
            else
                e.preventDefault();
        }

        function ResetScheduler() {
            var scheduler = $("#scheduler").data("kendoScheduler");
            if (scheduler != null) {
                scheduler.destroy();
                $("#scheduler").empty();
            }
        }

        var init = function () {
            if (!$scope.Userslist) {
                $.ajax({
                    url: '/api/users',
                    async: false,
                    success: function (data) {
                        var temp = [];
                        for (var i = 0; i < data['Users'].length; i++) {
                            if (data['Users'][i]['Domain']) {
                                temp.push(data['Users'][i]);
                            }
                        }
                        data['Users'] = temp;
                        $scope.Userslist = data;
                        $scope.LoadDefaultCalendar();
                    }
                });
            } else {
                $scope.LoadDefaultCalendar();
            }
        }

        // initial call
        init();
        $scope.ChangeColor = function (id) {
            var aId = $('#' + id)
            if ($(aId).hasClass("active")) {
                $(aId).removeClass('active');
            }
            else {
                $(aId).addClass('active');
            }
        }
        $scope.ChangeColorByAllUsers = function (id) {
            var aId = $('#' + id)
            if ($(aId).hasClass("active")) {
                $(aId).removeClass('active');
                //Sales Select All
                if (id == 'a_selectAllSales') {
                    $("#divSales a").each(function () {
                        $(this).removeClass('active');
                    });
                }
                //Installers
                if (id == 'a_selectAllInstallers') {
                    $("#divInstall a").each(function () {
                        $(this).removeClass('active');
                    });
                }
                //Others
                if (id == 'a_selectAllOthers') {
                    $("#divOther a").each(function () {
                        $(this).removeClass('active');
                    });
                }
            }
            else {
                $(aId).addClass('active');
                if (id == 'a_selectAllSales') {
                    $("#divSales a").each(function () {
                        $(this).addClass('active');
                    });
                }
                if (id == 'a_selectAllInstallers') {
                    $("#divInstall a").each(function () {
                        $(this).addClass('active');
                    });
                }
                if (id == 'a_selectAllOthers') {
                    $("#divOther a").each(function () {
                        $(this).addClass('active');
                    });
                }
            }
        }


        $scope.ReInitializeScheduler = function () {
            // $scope.KendoSchedulerIntitilize();
            // var scheduler = $("#scheduler").data("kendoScheduler");
           // scheduler.dataSource.read();
            $scope.loadTab();
            var myDiv = $('.k-calendar .k-content');
            document.documentElement.scrollTop = 0;
        }

        var tileOffset = 0;
        $scope.TextToDisplayInView = function (timeslot, mode, eventElement, event, recurenceflag) {
            var string = '';
            var starttime = DisplayDateTime(event.start);
            var endtime = DisplayDateTime(event.end);
            //for displaying only subject and removing tooltip in multi day event
            var multidayflag = false;
            var eventStartTime = event.start.getTime();
            var eventStopTime = event.end.getTime();
            var hours = (eventStopTime - eventStartTime) / (1000 * 60 * 60);
            var startdate = event.start.getDate();
            var enddate = event.end.getDate();
            var startmonth = event.start.getMonth();
            var endmonth = event.end.getMonth();
            var startyear = event.start.getFullYear();
            var endyear = event.end.getFullYear();
            if (hours > 24 || startdate != enddate || startmonth != endmonth || startyear != endyear)
                multidayflag = true;

            if (event.IsPrivateAccess == false) {
                string += "<i class='fas fa-lock-alt' style='font-size:24px; color:#666'></i>";
                eventElement.html(string);
            } else {
                if (!$(eventElement).hasClass("single-occurrence")) {
                    string += '<div class="tile-title-wrapper">';
                    if (event.title) {
                        string += '<span style="font-weight: bold;" class="event-tile-title">' + event.title + '</span></br>';
                    }

                    if (!event.isAllDay && !multidayflag) {

                        if (event.location) {
                            string += 'Location: ' + event.location + '</br>';
                        }
                        if (event.CaseId && event.CaseId > 0) {
                            string += 'CASE: <a style="text-decoration: none" href="#!/CaseView/' + event.CaseId + '">' + event.CalName + '</a></br>';
                        } else if (event.OrderId && event.OrderId > 0) {
                            string += 'ORD: <a style="text-decoration: none" href="#!/Orders/' + event.OrderId + '">' + event.CalName + '</a></br>';
                        } else if (event.OpportunityId && event.OpportunityId > 0) {
                            string += '<span>OPP: <a style="text-decoration: none" href="#!/OpportunityDetail/' + event.OpportunityId + '">' + event.CalName + '</a></br>';
                        } else if (event.AccountId && event.AccountId > 0) {
                            string += 'ACC: <a style="text-decoration: none" href="#!/Accounts/' + event.AccountId + '">' + event.CalName + '</a></br>';
                        } else if (event.LeadId && event.LeadId > 0) {
                            string += 'LEAD: <a style="text-decoration: none" href="#!/leads/' + event.LeadId + '">' + event.CalName + '</a></br>';
                        }
                        /*  if (event.AppointmentTypeName) {
                              string += event.AppointmentTypeName + '</br>';
                          }
                          string += event.AttendeeName + '</br>'; */
                        if (!event.isAllDay && !multidayflag) {
                            string += 'Start Time: ' + starttime + '</br>End Time: ' + endtime;
                        }

                    }

                    string += "</div>";

                    // back up framing the div for hover display

                    if (!event.isAllDay && !multidayflag) {

                        string += '<div class="hover-container"><div class="tile-hover-wrapper" id="appointmentTilehover' + tileOffset + '">';


                        if (event.location) {
                            string += '<span class="hover-tile-arrow"></span><span class="hover-tile-arrow-bottom"></span><span>Location: ' + event.location + '</span></br>';
                        }
                        if (event.CaseId && event.CaseId > 0) {
                            string += '<span>CASE: <a style="text-decoration: none" href="#!/CaseView/' + event.CaseId + '">' + event.CalName + '</a></span></br>';
                        } else if (event.OrderId && event.OrderId > 0) {
                            string += '<span>ORD: <a style="text-decoration: none" href="#!/Orders/' + event.OrderId + '">' + event.CalName + '</a></span></br>';
                        } else if (event.OpportunityId && event.OpportunityId > 0) {
                            string += '<span><span>OPP: <a style="text-decoration: none" href="#!/OpportunityDetail/' + event.OpportunityId + '">' + event.CalName + '</a></span></br>';
                        } else if (event.AccountId && event.AccountId > 0) {
                            string += '<span>ACC: <a style="text-decoration: none" href="#!/Accounts/' + event.AccountId + '">' + event.CalName + '</a></span></br>';
                        } else if (event.LeadId && event.LeadId > 0) {
                            string += '<span>LEAD: <a style="text-decoration: none" href="#!/leads/' + event.LeadId + '">' + event.CalName + '</a></span></br>';
                        }
                        if (!event.isAllDay) {
                            string += '<span>Start Time: ' + starttime + '</span></br><span>End Time: ' + endtime + '</span>';
                        }

                        // message display
                        if (event.description) {
                            string += '</br><span>Description: ' + event.description + '</span>';
                        }

                        string += "</div></div>";
                    }
                    eventElement.html(string);
                }
                if (event.recurrenceId && !event.recurrenceRule) {
                    $(eventElement).addClass("single-occurrence");
                }
            }
        }
        // hover behaviour initialization
        var initializeFunctionalBehaviour = function () {
            // set height function
            var eventTiles = $(".k-event");
            for (var i = 0; i < eventTiles.length; i++) {
                var tileHeight = $(eventTiles[i]).css("height");
                var heightValue = (parseInt(tileHeight) - 7) + "px";
                var children = $(eventTiles[i]).children();
                $(children[0]).css("height", heightValue);
            }
            // click functionality
            $('.k-event').click(function (event) {
                $scope.handleEventTileClick(event);
            });
            $scope.clickOffset = 0;
            // trigger create behaviour on click
            $("#scheduler .k-scheduler-layout .k-scheduler-content .k-scheduler-table td").click(function (event) {
                if ($scope.Permission.AddEvent) {
                    $scope.clickOffset++;
                    openCalendarPopUp(event);
                }
                else {
                    event.preventDefault();
                }
            });
            // hover functionality
            $('.k-event').mouseenter(function (event) {
                $(".tile-hover-wrapper").css("display", "none");
                displayHoverTile(event);
            });
            var timer;
            // blur functionality
            $('.k-event').on('mouseleave', function (event) {
                var selectedTile = event.target;
                if (!selectedTile.dataset || (selectedTile.dataset && !selectedTile.dataset.uid)) {
                    selectedTile = $(selectedTile).parent()[0];
                }
                if (selectedTile && selectedTile.dataset && selectedTile.dataset.uid) {
                    var children = $(selectedTile).children();
                    var tileChild = $(children[1]).children()[0];
                    timer = setTimeout(function () {
                        if (!$scope.hoverTileTooptipFlag)
                            $(tileChild).css("display", "none");
                    }, 100);
                }
            });

            $(".tile-hover-wrapper").mouseleave(function (event) {
                $(this).css("display", "none");
                $scope.hoverTileTooptipFlag = false;
                clearTimeout(timer);
            });
        }

        // hover tile display
        var displayHoverTile = function (event) {
            var selectedEvent;
            if (event.target && (!event.target.dataset || (event.target.dataset && !event.target.dataset.uid))) {
                selectedEvent = $(event.target).parent()[0];
            } else {
                selectedEvent = event.target;
            }
                var leftValue;
                var changeLeftArrowFlag = false;
                var changeRightArrowFlag = false;
                var positionedValue = $(selectedEvent).css("left");
                var calendarWidth = $(".calendar_leftbox").width();
                var windowWidth = window.innerWidth;
                var positionLeftValue = parseInt(positionedValue);
                var highDifference = windowWidth - (2 * calendarWidth) - 100;
                var lowDifference = calendarWidth + (calendarWidth / 2) - 100;

            // left right manipulation
                if (positionLeftValue > highDifference) {
                    leftValue = -147 + "px";
                    changeLeftArrowFlag = true;
                } else if (positionLeftValue < lowDifference) {
                    leftValue = 0 + "px";
                    changeRightArrowFlag = true;
                } else {
                    leftValue = (($(selectedEvent).width() / 2) - 95) + "px";
                }
                if (tileChild) {
                    $(".tile-hover-wrapper").css("display", "none");
                }
                var children = $(selectedEvent).children();
                var tileChild = $(children[1]).children()[0];
                // top bottom manipulation
                var positionTopValue = $(selectedEvent).css("top");
                var positionedTopValue = parseInt(positionTopValue);
                
                //for tooltip hiding issue
                var scrolledheight = $(window).scrollTop();
                if (positionedTopValue != NaN) {
                    var alldayheight = $('.k-scheduler-times-all-day').closest('tr').height();
                    positionedTopValue = (positionedTopValue - scrolledheight);
                    positionedTopValue = positionedTopValue + alldayheight + 30;
                }
                
                var windowHeight = window.innerHeight;
                var heightValue = parseInt($(tileChild).css("height"));
                var tileHeight = $(selectedEvent).height();
                var thresoldHeight = windowHeight - heightValue - tileHeight - 227;
                var topFlag;
                // need to manipulate for top display tile hover
                if (positionedTopValue > thresoldHeight) {
                    topValue = (-(heightValue + tileHeight + 12)) + "px";
                    topFlag = true;
                } else {
                    topValue = 11 + "px";
                    topFlag = false;
                }
                $(tileChild).css({ "display": "block", "left": leftValue, "top": topValue });
                var arrowChild = $(tileChild).children()[0];
                var bottomArrowChild = $(tileChild).children()[1];
                if (changeRightArrowFlag) {
                    $(arrowChild).css("left", "3px");
                    $(bottomArrowChild).css("left", "3px");
                }
                if (changeLeftArrowFlag) {
                    $(arrowChild).css("left", "177px");
                    $(bottomArrowChild).css("left", "177px");
                }
                if (topFlag) {
                    $(arrowChild).css("display", "none");
                    $(bottomArrowChild).css("display", "block");
                } else {
                    $(arrowChild).css("display", "block");
                    $(bottomArrowChild).css("display", "none");
                }
                $(tileChild).mouseenter(function (e) {
                    $scope.hoverTileTooptipFlag = true;
                    $(".tile-hover-wrapper").css("display", "none");
                    $(tileChild).css({ "display": "block", "left": leftValue });
                });
        }

        // open the calendar modal on click the scheduler slot
        var openCalendarPopUp = function (event) {
            if ($scope.Permission.AddEvent) {
                if ($scope.clickOffset === 1) {
                    var scheduler = $("#scheduler").getKendoScheduler();
                    var slot = scheduler.slotByElement($(event.target));
                    var e = {
                        "event": {
                            "start": slot['startDate'],
                            "end": slot['endDate']
                        }
                    };
                    calendarModalService.setPopUpDetails(true, "Event", null, $scope.reloadScheduler, $scope.sectionType, e, true, null, null, null, true);
                    var timer = setTimeout(function () {
                        $scope.clickOffset = 0;
                    }, 500);
                    clearTimeout(timer);
                }
            }
            else {
                event.preventDefault();
            }
        }
        $scope.TextToDisplayTaskView = function (eventElement, event) {

            var string = '';
            if (event.IsPrivateAccess == false) {
                string += "<i class='fas fa-lock-alt' style='font-size:24px; color:#666'></i>";
            } else {
                string += '<div class="tile-title-wrapper">';
                if (event.title) {
                    string += '<span style="font-weight: bold;" class="event-tile-title">' + event.title + '</span>';
                }
                string += '</div>';
            }
            eventElement.html(string);
        }
        function DisplayDateTime(date) {
            var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
            var am_pm = date.getHours() >= 12 ? "PM" : "AM";
            hours = hours < 10 ? "0" + hours : hours;
            var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
            time = hours + ":" + minutes + " " + am_pm;
            return time;
        };

        // cancel the task
        $scope.cancelAppointment = function (id) {
            var loadingElement = $("#loading");
            if (loadingElement) {
                loadingElement[0].style.display = "block";
            }
            var cancelUrl = '/api/tasks/' + id + '/CancelEvent';
            $http.put(cancelUrl).then(function (response) {
                var res = response.data;
                if (loadingElement) {
                    loadingElement[0].style.display = "none";
                }
                if (res) {
                    HFC.DisplaySuccess("Successful");
                } else {
                    HFC.DisplayAlert("Error");
                }
                $scope.ReInitializeScheduler();
                $("#_calendarModal_").modal("hide");
            });
        }


        // $scope.KendoSchedulerIntitilize();  intial load the scheduler, it needs to be in the success of readcalendar view data
        //// added by azhagu

        // to load custom calendar views
        $scope.loadTab = function (id) {
            //$scope.ResetSavedCalendar(id);
            var GetPreviousActiveTab = JSLINQ($scope.SavedCalendarList)
                      .Where(function (item) { return item.active == "active"; });
            if (!GetPreviousActiveTab.items.length == 0)
                GetPreviousActiveTab.items[0].active = '';

            // for load view after saving the appointment
            if (!id) {
                if (!GetPreviousActiveTab.items[0]) {
                    $scope.SetMainTabActive();
                } else {
                    id = GetPreviousActiveTab.items[0].SaveCalendarId;
                }
            }

            if (id) {

                $scope.activeTab = id;

                var GetSelectedTab = JSLINQ($scope.SavedCalendarList)
                          .Where(function (item) { return item.SaveCalendarId == id; });
                GetSelectedTab.items[0].active = "active";
                var selectedUsers = [];
                selectedUsers = GetSelectedTab.items[0].SavedCalendarUsers;

                $scope.bussinessHourFlag = GetSelectedTab.items[0]['IsBussinessHour'];
                var ActiveViewMode = '';
                if (GetSelectedTab.items[0].Viewmode == '1')
                    ActiveViewMode = 'day';
                else if (GetSelectedTab.items[0].Viewmode == '2')
                    ActiveViewMode = 'week';
                else if (GetSelectedTab.items[0].Viewmode == '3')
                    ActiveViewMode = 'month';
                else if (GetSelectedTab.items[0].Viewmode == '4')
                    ActiveViewMode = 'workWeek';

                $scope.MainTabActive = '';
                $scope.selectedAssigneeUser = [];
                var scheduler = $("#scheduler").data("kendoScheduler");
                if ($("#a_selectAllSales").hasClass("active")) {
                    $("#a_selectAllSales").removeClass('active');
                }
                if ($("#a_selectAllInstallers").hasClass("active")) {
                    $("#a_selectAllInstallers").removeClass('active');
                }
                if ($("#a_selectAllOthers").hasClass("active")) {
                    $("#a_selectAllOthers").removeClass('active');
                }
                if (selectedUsers != undefined) {
                    for (var i = 0; i < $scope.Userslist.Users.length; i++) {
                        for (var j = 0 ; j < selectedUsers.length ; j++) {
                            if ($scope.Userslist.Users[i].PersonId == selectedUsers[j].SelectedUserid) {
                                $scope.Userslist.Users[i].SelectColor = "list-group-item active";

                                if ($scope.Userslist.Users[i].InInstall) {
                                    var aId = $('#' + 'installer_' + $scope.Userslist.Users[i].PersonId);
                                    $(aId).addClass('active');
                                }
                                if ($scope.Userslist.Users[i].InSales) {
                                    var aId = $('#' + 'sale_' + $scope.Userslist.Users[i].PersonId);
                                    $(aId).addClass('active');
                                }
                                if (!$scope.Userslist.Users[i].InInstall && !$scope.Userslist.Users[i].InSales) {
                                    var aId = $('#' + 'other_' + $scope.Userslist.Users[i].PersonId)
                                    $(aId).addClass('active');
                                }
                                $scope.selectedAssigneeUser.push(selectedUsers[j].SelectedUserid);
                                break;
                            }
                            else
                                if ($scope.Userslist.Users[i].InInstall) {
                                    var aId = $('#' + 'installer_' + $scope.Userslist.Users[i].PersonId)
                                    if ($(aId).hasClass("active")) {
                                        $(aId).removeClass('active');
                                    }
                                }
                            if ($scope.Userslist.Users[i].InSales) {
                                var aId = $('#' + 'sale_' + $scope.Userslist.Users[i].PersonId)
                                if ($(aId).hasClass("active")) {
                                    $(aId).removeClass('active');
                                }
                            }
                            if (!$scope.Userslist.Users[i].InInstall && !$scope.Userslist.Users[i].InSales) {
                                var aId = $('#' + 'other_' + $scope.Userslist.Users[i].PersonId)
                                if ($(aId).hasClass("active")) {
                                    $(aId).removeClass('active');
                                }
                            }
                        }

                    }
                }
                var sbhbutton = $("#showBusinessHourButton");
                if (!$scope.bussinessHourFlag) {
                    if (sbhbutton && sbhbutton[0] && sbhbutton[0].innerText != "Show business hours") {
                        $(".k-scheduler-fullday").click();
                    }
                } else {
                    if (sbhbutton && sbhbutton[0] && sbhbutton[0].innerText == "Show business hours") {
                        $(".k-scheduler-fullday").click();
                    }
                }
                if (ActiveViewMode == "day") {
                    scheduler.date($scope.selectedDate);
                }
                scheduler.view(ActiveViewMode);
            }
        }

        $scope.SendReminder = function (EventId) {
            var dto = { EventId: EventId };
            $http.put('/api/Calendar/' + EventId + '/UpdateEvent').then(function (response) {
                $scope.IsBusy = false;

                if ($routeParams.orderId > 0) {
                    HFC.DisplaySuccess("Event Created");
                }
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });
        }
        //$scope.EditTask = function (TaskID, TaskModel) {
        //    $("#calendarTaskModal").modal("hide");
        //    var editTask = CalendarService.EditTaskFromCalendarSceduler('newtask', TaskModel)
        //}
        //$scope.EditEvent = function (modalId, EventId, attendees) {
        //    $("#_calendarModal_").modal("hide");
        //    var editTask = CalendarService.GetEventsFromScheduler(modalId, EventId, null, attendees);
        //    //  $scope.$apply();
        //}
        $scope.DeleteEventInDetailsView = function (moaldId, EventId) {

            var Id = EventId;
            var deleteurl = '/api/calendar/' + Id;
            $http.delete(deleteurl).then(function (response) {
                var res = response.data;
                if (res) {
                    var saveStatus = "Successful";
                    HFC.DisplaySuccess(saveStatus);

                    // $scope.KendoSchedulerIntitilize();
                    var scheduler = $("#scheduler").data("kendoScheduler");
                    scheduler.dataSource.read();
                }
                else {

                }
            });
            $("#_calendarModal_").modal("hide");
        }
        $scope.DeleteTaskInDetailsView = function (moaldId, TaskID) {
            var Id = TaskID;
            var deleteurl = '/api/Tasks/' + Id;
            $http.delete(deleteurl).then(function (response) {
                var res = response.data;
                if (res) {
                    var saveStatus = "Successful";
                    HFC.DisplaySuccess(saveStatus);

                    // $scope.KendoSchedulerIntitilize();
                    var scheduler = $("#scheduler").data("kendoScheduler");
                    scheduler.dataSource.read();
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
            $("#" + moaldId).modal("hide");
        }
        $scope.CompeleteTaskInDetailsView = function (moaldId, TaskID) {
            var Id = TaskID;
            var markurl = '/api/Tasks/' + Id + '/Mark?IsComplete=true';
            $http.get(markurl).then(function (response) {
                var res = response.data;
                if (res) {
                    var saveStatus = "Successful";
                    HFC.DisplaySuccess(saveStatus);
                    // $scope.KendoSchedulerIntitilize();
                    var scheduler = $("#scheduler").data("kendoScheduler");
                    scheduler.dataSource.read();
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
            $("#" + moaldId).modal("hide");
        }
        $scope.TaskReminderInDetailsView = function (moaldId, TaskID) {
            var Id = TaskID;
            var reminderurl = '/api/Tasks/' + Id + '/Reminder';
            $http.put(reminderurl).then(function (response) {
                var res = response.data;
                if (res) {
                    var saveStatus = "Reminder Sent Successful"
                    HFC.DisplaySuccess(saveStatus);
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
            $("#" + moaldId).modal("hide");
        }
        $scope.EventReminderInDetailsView = function (moaldId, TaskID) {
            var Id = TaskID;
            var reminderurl = '/api/Tasks/' + Id + '/EventReminder';
            $http.put(reminderurl).then(function (response) {
                var res = response.data;
                if (res) {
                    var saveStatus = "Reminder Sent Successful"
                    HFC.DisplaySuccess(saveStatus);
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
            $("#" + moaldId).modal("hide");
        }
        $scope.RedirectToOpportunity = function (OpportunityId) {
            $("#_calendarModal_").modal("hide");
            window.location.href = '#!/OpportunityDetail/' + OpportunityId;
        }
        $scope.RedirectToAccount = function (OpportunityId) {
            $("#_calendarModal_").modal("hide");
            window.location.href = '#!/Accounts/' + OpportunityId;
        }

        // for calendar component refresh method
        $scope.reloadScheduler = function (flag) {
            if (flag) {
                // $scope.KendoSchedulerIntitilize();
                 var scheduler = $("#scheduler").data("kendoScheduler");
                 scheduler.dataSource.read();
               // $scope.loadTab();
            } else {
                $scope.callBackFromAppointment();
            }
        }

        // set method in service for view edit reload
        $scope.calendarModalService.setViewFunction($scope.reloadScheduler, $scope.sectionType);

        // for drag & drop functionality
        var movingFlag = false;
        var moveEndFlag = false;

        $scope.moveStart = function (e) {
            var selectedEvent = e.event;
            var DroppedOnScheduler = $("#scheduler");
            $scope.scheduler = $("#scheduler").data("kendoScheduler");
            movingFlag = true;
            $scope.draggedStartDate = e.event.start;
            var startDateBackup = selectedEvent.start;
            var endDateBackup = selectedEvent.end;
            var startDate = new Date(startDateBackup);
            var endDate = new Date(endDateBackup);
            startDate.setHours(0);
            startDate.setMinutes(0);
            endDate.setHours(0);
            endDate.setMinutes(0);
            if (selectedEvent.isAllDay || (startDate.getTime() != endDate.getTime())) {
                alert("cannot drag AllDay / MultiDay event");
                e.preventDefault();
                return;
            }
            $("#scheduler").on("mouseup", function (e) {
                alertModalService.bindFunctionFlag = false;
                var offset = $(e.target).offset();
                var scheduler = $("#scheduler").data("kendoScheduler");
                // scheduler.setDataSource($scope.schedulerDataSource);
                var slot = $scope.scheduler.slotByPosition(offset.left, offset.top);
                var eventList;
                if (!selectedEvent['isAllDay'] && !selectedEvent['IsTask']) {
                    var loadingElement = $("#loading");
                    if (loadingElement) {
                        loadingElement[0].style.display = "block";
                    }
                    var url = "/api/calendar/?CalendarEdit=true&CalendarId=" + selectedEvent['Id'] + "&includeLookup=true"
                    $http.get(url).then(function (data) {
                        eventList = data['data']['Events'];
                        for (var i = 0; i < eventList.length; i++) {
                            if (eventList[i]['id'] == selectedEvent['Id']) {
                                selectEvent = eventList[i];
                            }
                        }
                        if (loadingElement) {
                            loadingElement[0].style.display = "none";
                        }
                        if (slot && selectEvent) {
                            if (typeof (selectEvent['start']) == "string") {
                                if (selectEvent['start'].indexOf("Z") != -1) {
                                    // currentDateTime = currentDateTime.slice(0, currentDateTime.length - 1); for ios change, it doesn't support for latest versions
                                    selectEvent['start'] = selectEvent['start'].substring(0, selectEvent['start'].length - 1);
                                }
                                selectEvent['start'] = new Date(selectEvent['start']);
                                selectEvent['originalStartDate'] = calendarModalService.getDateStringValue(selectEvent['start'], false, true);
                            }
                            if (typeof (selectEvent['end']) == "string") {
                                if (selectEvent['end'].indexOf("Z") != -1) {
                                    // currentDateTime = currentDateTime.slice(0, currentDateTime.length - 1); for ios change, it doesn't support for latest versions
                                    selectEvent['end'] = selectEvent['end'].substring(0, selectEvent['end'].length - 1);
                                }
                                selectEvent['end'] = new Date(selectEvent['end']);
                                selectEvent['originalEndDate'] = calendarModalService.getDateStringValue(selectEvent['end'], false, true);
                            }
                            // need to find difference
                            var endDate = new Date(slot['startDate'].getTime() + (selectEvent['end'] - selectEvent['start']));
                            selectEvent['end'] = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear() + " " + endDate.getHours() + ":" + endDate.getMinutes();
                            selectEvent['start'] = (slot['startDate'].getMonth() + 1) + "/" + slot['startDate'].getDate() + "/" + slot['startDate'].getFullYear() + " " + slot['startDate'].getHours() + ":" + slot['startDate'].getMinutes();
                        }
                        var currentDate = new Date();
                        $scope.draggedEvent = selectEvent;
                        if (selectEvent['EventRecurring'] && selectEvent['EventType'] == 2) {
                            if ($scope.Permission.AddEvent) {
                                selectEvent['AddRecurrence'] = true;
                                // need to raise alert pop up for getting confirmation from user regarding the single occurrence or series optimisation
                                $("html").addClass("scroll-hidden");
                                $("body").addClass("scroll-hidden");
                                if ((new Date(selectEvent.start) < currentDate || new Date(selectEvent.end) < currentDate) && !selectEvent.IsTask) {
                                    alertModalService.bindFunctionFlag = true;
                                    alertModalService.setPopUpDetails(true, "Confirmation", "You have chosen the From Date past date. Do you want to continue?", $scope.pastDateEvent, []);
                                } else {
                                    alertModalService.bindFunctionFlag = true;
                                    alertModalService.setPopUpDetails(true, "Edit Recurring Item", "Do you want to edit only this event occurrence or the whole series?", initialDataLoad, null, null, [{ key: 'single', title: 'Edit Current Occurence' }, { key: 'series', title: 'Edit the series' }]);
                                }
                            }
                            else {
                                e.preventDefault();
                            }
                        } else {
                            if (selectEvent['EventRecurring'] && selectEvent['EventType'] == 1) {
                                selectEvent.EventRecurring = null;
                            }
                            if ((selectEvent.IsNotifyemails || selectEvent.IsNotifyText) && !selectEvent.IsTask) {
                                // trigger confirmation pop up for email
                                $("html").addClass("scroll-hidden");
                                $("body").addClass("scroll-hidden");
                                if ((new Date(selectEvent.start) < currentDate || new Date(selectEvent.end) < currentDate) && !selectEvent.IsTask) {
                                    alertModalService.bindFunctionFlag = true;
                                    alertModalService.setPopUpDetails(true, "Confirmation", "You have chosen the From Date past date. Do you want to continue?", $scope.pastDateEvent, []);
                                }
                                if (selectEvent.IsNotifyemails && !selectEvent.IsNotifyText) {
                                    alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation email to the Customer?", $scope.saveAppointment, [selectEvent], null, null, true);
                                }
                                else if (!selectEvent.IsNotifyemails && selectEvent.IsNotifyText) {
                                    alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation text to the Customer?", $scope.saveAppointment, [selectEvent], null, null, true);
                                }
                                else {
                                    alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation email/text to the Customer?", $scope.saveAppointment, [selectEvent], null, null, true);
                                }
                            } else {
                                if ((new Date(selectEvent.start) < currentDate || new Date(selectEvent.end) < currentDate) && !selectEvent.IsTask) {
                                    alertModalService.bindFunctionFlag = true;
                                    alertModalService.setPopUpDetails(true, "Confirmation", "You have chosen the From Date past date. Do you want to continue?", $scope.pastDateEvent, []);
                                } else {
                                    if (!selectEvent.IsTask)
                                        $scope.saveAppointment(selectEvent);
                                }
                            }
                        }
                    });
                } else {
                    $scope.moveEnd();
                    initScheduler();
                    // $scope.KendoSchedulerIntitilize();
                    // var scheduler = $("#scheduler").data("kendoScheduler");
                    // scheduler.dataSource.read();
                }

                movingFlag = false;
                moveEndFlag = false;
            });
        }

        //past date event
        $scope.pastDateEvent = function (param1, param2, param3, flag) {
            if (flag == 'triggerReload') {
                $("html").removeClass("scroll-hidden");
                $("body").removeClass("scroll-hidden");
                initScheduler();
            } else {
                if (selectEvent['EventRecurring'] && selectEvent['EventType'] == 2) {
                    if ($scope.Permission.AddEvent) {
                        setTimeout(function () {
                            alertModalService.bindFunctionFlag = true;
                            alertModalService.setPopUpDetails(true, "Edit Recurring Item", "Do you want to edit only this event occurrence or the whole series?", initialDataLoad, null, null, [{ key: 'single', title: 'Edit Current Occurence' }, { key: 'series', title: 'Edit the series' }]);
                        }, 100);
                    }
                } else {
                    if (($scope.draggedEvent.IsNotifyemails && !selectEvent.IsNotifyText) && !$scope.draggedEvent.IsTask) {
                        setTimeout(function () {
                            alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation email to the Customer?", $scope.saveAppointment, [selectEvent], null, null, true);
                        }, 100);
                    }
                    else if ((!$scope.draggedEvent.IsNotifyemails && selectEvent.IsNotifyText) && !$scope.draggedEvent.IsTask) {
                        setTimeout(function () {
                            alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation text to the Customer?", $scope.saveAppointment, [selectEvent], null, null, true);
                        }, 100);
                    }
                    else if (($scope.draggedEvent.IsNotifyemails && selectEvent.IsNotifyText) && !$scope.draggedEvent.IsTask) {
                        setTimeout(function () {
                            alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation email/text to the Customer?", $scope.saveAppointment, [selectEvent], null, null, true);
                        }, 100);
                    }
                    else {
                        $scope.saveAppointment($scope.draggedEvent);
                    }
                }
            }
        }

        $scope.moveEnd = function (e) {
            if (moveEndFlag && e) {
                e.preventDefault();
            }
            $("#scheduler").unbind("mouseup");
            $("#scheduler").unbind("mousemove");
        }

        // validate the reccurrence appointment on drag & drop
        var initialDataLoad = function (selectFlag, dummy1, dummy2, flag) {
            if (flag == 'triggerReload') {
                $("html").removeClass("scroll-hidden");
                $("body").removeClass("scroll-hidden");
                initScheduler();
            } else {
                // recurrence optimisation selection flag
                if (selectFlag && selectFlag == "single") {
                    $scope.draggedEvent.EventType = 1;
                    $scope.draggedEvent.EventRecurring = null;
                    $scope.draggedEvent.id = 0;
                    // update original start date
                    $scope.draggedEvent.originalStartDate = calendarModalService.getDateStringValue($scope.draggedStartDate, false, true);
                } else if (selectFlag && selectFlag == "series") {
                    $scope.draggedEvent.EventType = 2;
                    // update original start date
                    //  var originalStartDate = ($scope.draggedEvent.originalStartDate).split(" ")[0];
                    //  var originalStartTime = ($scope.draggedEvent.start).split(" ");
                    //  var originalStartDateTime = originalStartDate + " " + originalStartTime[1];

                    //  var originalEndDate = ($scope.draggedEvent.originalEndDate).split(" ")[0];
                    //  var originalEndTime = ($scope.draggedEvent.end).split(" ");
                    //  var originalEndDateTime = originalEndDate + " " + originalEndTime[1];

                    ////  $scope.draggedEvent.start = originalStartDateTime;
                    // // $scope.draggedEvent.end = originalEndDateTime;
                }
                if (($scope.draggedEvent.IsNotifyemails && selectEvent.IsNotifyText) && !$scope.draggedEvent.IsTask) {
                    // trigger confirmation pop up for email
                    $("html").addClass("scroll-hidden");
                    $("body").addClass("scroll-hidden");
                    setTimeout(function () {
                        alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation email/text to the Customer?", $scope.saveAppointment, [$scope.draggedEvent], null, null, true);
                    }, 100);
                }
                else if (($scope.draggedEvent.IsNotifyemails && !selectEvent.IsNotifyText) && !$scope.draggedEvent.IsTask) {
                    // trigger confirmation pop up for email
                    $("html").addClass("scroll-hidden");
                    $("body").addClass("scroll-hidden");
                    setTimeout(function () {
                        alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation email to the Customer?", $scope.saveAppointment, [$scope.draggedEvent], null, null, true);
                    }, 100);
                }
                else if ((!$scope.draggedEvent.IsNotifyemails && selectEvent.IsNotifyText) && !$scope.draggedEvent.IsTask) {
                    // trigger confirmation pop up for email
                    $("html").addClass("scroll-hidden");
                    $("body").addClass("scroll-hidden");
                    setTimeout(function () {
                        alertModalService.setPopUpDetails(true, "Confirmation", "Do you want to send an appointment confirmation text to the Customer?", $scope.saveAppointment, [$scope.draggedEvent], null, null, true);
                    }, 100);
                }
                else {
                    $scope.saveAppointment($scope.draggedEvent);
                }
            }
        }

        // save appointment
        $scope.saveAppointment = function (selectEvent) {
            var loadingElement = $("#loading");
            if (loadingElement) {
                loadingElement[0].style.display = "block";
            }
            selectEvent['SendEmail'] = alertModalService.emailSendFlag;
            var url = selectEvent.IsTask ? "api/Tasks/" : "/api/calendar/";
            $http.post(url, selectEvent).then(function (response) {
                if (loadingElement) {
                    loadingElement[0].style.display = "none";
                }
                $("html").removeClass("scroll-hidden");
                $("body").removeClass("scroll-hidden");
                initScheduler();
                HFC.DisplaySuccess(response.statusText);
            });
        }

        // intialize scheduler
        var initScheduler = function () {
            var sbhbutton = $("#showBusinessHourButton");
            if (sbhbutton && sbhbutton[0]) {
                var flag = sbhbutton[0].innerText;
            }
            $scope.KendoSchedulerIntitilize();
            if (flag == "Show business hours") {
                $(".k-scheduler-fullday").click();
            }
        }

        // delete the task
        $scope.deleteAppointment = function (flag, event, recurrenceFlag) {
            var id = event.id;
            var loadingElement = $("#loading");
            if (loadingElement) {
                loadingElement[0].style.display = "block";
            }
            var deleteurl;
            if (flag) {
                deleteurl = '/api/Tasks/' + id;
            } else {
                deleteurl = '/api/calendar/' + id;
                if (recurrenceFlag == "single") {
                    var originalDate = ((event.startDate).getMonth() + 1) + '/' + (event.startDate).getDate() + '/' + (event.startDate).getFullYear() + ' ' + (event.startDate).getHours() + ':' + (event.startDate).getMinutes();
                    deleteurl += '?occurStartDate=' + originalDate;
                }
            }
            $http.delete(deleteurl).then(function (response) {
                var res = response.data;
                if (loadingElement) {
                    loadingElement[0].style.display = "none";
                }
                if (res) {
                    HFC.DisplaySuccess("Successful");
                } else {
                    HFC.DisplayAlert("Error");
                }
                // $scope.KendoSchedulerIntitilize();
                var scheduler = $("#scheduler").data("kendoScheduler");
                scheduler.dataSource.read();
                $("#_calendarModal_").modal("hide");
                $("html").removeClass("scroll-hidden");
                $("body").removeClass("scroll-hidden");
            });
        }

        // delete recurrence Appointment
        $scope.deleteAppt = function (event) {
            if (event.recurrenceId && event.EventType == 2) {
                $("html").addClass("scroll-hidden");
                $("body").addClass("scroll-hidden");
                alertModalService.setPopUpDetails(true, "Delete Recurring Item", "Do you want to delete only this event occurrence or the whole series?", $scope.deleteAppointment, [false, event], null, [{ key: 'single', title: 'delete Current Occurence' }, { key: 'series', title: 'delete the series' }], false, true);
            } else {
                $("html").addClass("scroll-hidden");
                $("body").addClass("scroll-hidden");
                alertModalService.setPopUpDetails(true, "Delete Recurring Item", "Do you want to delete this event?", $scope.deleteAppointment, [false, event], null, [], false, true);
                // $scope.deleteAppointment(false, event);
            }
        }

        //for hiding tooltip on scroll
        $(window).scroll(function () {
            $(".tile-hover-wrapper").css("display", "none");
        });

    }]);
app.directive('hfcCalendarEventModal', ['calendarModalService',
             function (calendarModalService) {
                 return {
                     restrict: 'E',
                     scope: true,
                     templateUrl: '/templates/NG/Calendar/calendar-event-modal.html',
                     replace: true,
                     link: function (scope) {
                         scope.calendarModalService = calendarModalService;
                         scope.editAppointment = function () {
                             $("#_calendarModal_").modal("hide");
                             scope.calendarModalService.setPopUpDetails(true, 'Event', scope.calendarModalService.eventview.id, scope.calendarModalService.viewFunction, scope.calendarModalService.viewReturnText, null, true, scope.calendarModalService.eventview.startDate, scope.calendarModalService.eventview.endDate);
                         }
                     }
                 }
             }
]);
app.directive('hfcCalendarTaskModal', ['calendarModalService',
             function (calendarModalService) {
                 return {
                     restrict: 'E',
                     scope: true,
                     templateUrl: '/templates/NG/Calendar/calendar-task-modal.html',
                     replace: true,
                     link: function (scope) {
                         scope.calendarModalService = calendarModalService;
                         scope.editTask = function () {
                             $("#calendarTaskModal").modal("hide");
                             scope.calendarModalService.setPopUpDetails(true, 'Task', scope.calendarModalService.eventview.id, scope.calendarModalService.viewFunction, scope.calendarModalService.viewReturnText, null, true, scope.calendarModalService.eventview.startDate, scope.calendarModalService.eventview.endDate);
                         }
                     }
                 }
             }
]);
app.directive('hfcCalendarSaveModal', [
             function () {
                 return {
                     restrict: 'E',
                     scope: true,
                     templateUrl: '/templates/NG/Calendar/calendar-save-modal.html',
                     replace: true
                 }
             }
]);
app.directive('hfcManageCalendarSaveModal', [
             function () {
                 return {
                     restrict: 'E',
                     scope: true,
                     templateUrl: '/templates/NG/Calendar/calendar-manage-save-modal.html',
                     replace: true
                 }
             }
]);
