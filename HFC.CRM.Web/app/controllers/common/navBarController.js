﻿app.controller('navBarController', [
        '$http', '$scope', '$window', '$location', '$rootScope', 'saveCriteriaService', 'sourceService', '$routeParams', 'saveSearchService', function($http, $scope, $window, $location, $rootScope, saveCriteriaService, sourceService, $routeParams, saveSearchService) {
            $scope.saveSearchService = saveSearchService;
            $scope.newJobCounter = 0;
            $rootScope.enableSaveSearch = false;

            $scope.newLeadCounter = 0;
            $rootScope.last30Date = encodeURIComponent(encodeURIComponent((new XDate()).addDays(-30).toString('MM/dd/yyyy')));

            //$scope.GetNewLeadCounter = function (data) {

            //    $http.get('/api/leads/0/GetNewLeadCount').then(
            //        function(result) {
            //            $scope.newLeadCounter = result.data.count;
            //        });
            //}
            //$scope.GetNewJobCounter = function (data) {

            //    $http.get('/api/jobs/0/GetNewJobCount').then(
            //        function (result) {
            //            $scope.newJobCounter = result.data.count;
            //        });
            //}

            $scope.GetSavedSearches = function(data) {
                $http.get('/api/Search/0/GetSavedSearch').then(
                    function(result) {
                        $rootScope.SavedSearches = result.data.results;
                    });
            }

            $scope.GetSavedSearches();

           // //
            //$rootScope.searchMode = 'Auto_Detect';
            //$scope.setSearchMode = function (mode) {

            //    var lis = $('#search-type li');

            //    for (var i = 0; i < lis.length; i++) {
            //        $(lis[i]).removeClass('active');
            //        if ($(lis[i]).find('input').length > 0 && mode == 'Auto_Detect') {
            //            $(lis[i]).addClass('active');
            //        } else {
            //            if ($(lis[i]).find('a').text().indexOf(mode) != -1) {
            //                $(lis[i]).addClass('active');
            //            }
            //        }
            //    }
            //    // //
            //    $rootScope.searchMode = mode;
            //}
            $scope.setSearchMode = function(mode) {

                var lis = $('#search-type li');

                for (var i = 0; i < lis.length; i++) {
                    $(lis[i]).removeClass('active');
                    if ($(lis[i]).find('a').text().indexOf(mode) != -1) {
                        $(lis[i]).addClass('active');
                    }
                }
               // //
                $rootScope.searchMode = mode;
            }

            // setInterval(function() {
            //$scope.GetNewLeadCounter();
            //$scope.GetNewJobCounter();
            //}, 5000);


            $scope.SalesOptions = {
                dataTextField: "PersonId",
                dataValueField: "PersonId",
                valuePrimitive: true,
                //autoBind: true,
                itemTemplate: '<span class="color-box color-#: data.ColorId #"></span> #: data.FullName #',
                tagTemplate: '<span class="color-box color-#: data.ColorId #"></span> #: data.FullName #',
                placeholder: "Sales Agent(s)",
                dataSource: sales
            };

            $scope.LeadStatusOptions = {
                dataTextField: "Id",
                dataValueField: "Id",
                itemTemplate: '<span>&nbsp; # if (data.IsChiled) { # &nbsp;&nbsp;   # } # #: data.Name # </span> ',
                tagTemplate: '<span >#: data.Name #</span> ',
                dataSource: leadStatuses,
                placeholder: "Lead Status(es)",
            };

            $scope.SourceOptions = {
                dataTextField: "Description",
                dataValueField: "SourceId",
                itemTemplate: '<span>&nbsp; # if (data.ParentId != null) { # &nbsp;&nbsp;   # } # #: data.Name # </span> ',
                tagTemplate: '<span >#: data.Name #</span> ',
                valuePrimitive: true,
                autoBind: true,
                dataSource: sourceStatuses,
                //dataSource: {
                //    transport: {
                //        read: function(options) {
                //            $(".k-loading-mask").remove();

                //            sourceService.GetSources().then(function(result) {

                //                options.success(result.data.sourses);
                //            });
                //        }
                //    }
                //},
                placeholder: "Source(es)",
            };

            $scope.JobsStatusOptions = {
                dataTextField: "Id",
                dataValueField: "Id",
                itemTemplate: '<span>&nbsp; # if (data.IsChiled) { # &nbsp;&nbsp;   # } # #: data.Name # </span> ',
                tagTemplate: '<span >#: data.Name #</span> ',
                dataSource: jobsStatuses,
                placeholder: "Job Status(es)",
            };

            $scope.invoiceStatusesOption = {
                placeholder: "Invoice Status(es)",
            };

            $scope.DateRangeArray = ["All Date & Time", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year", "Last Year"];

            $scope.selectedDateText = "All Date & Time";

            $scope.$watch('startDate', function(newVal) {
                if (newVal != null) {
                    $scope.ForSearchstartDate = newVal;
                    if ($scope.endDate == null) {
                        $scope.selectedDateText = newVal + ' to ';
                    } else {
                        $scope.selectedDateText = newVal + ' to ' + $scope.endDate;
                    }
                }
            });

            $scope.$watch('endDate', function(newVal) {
                if (newVal != null) {
                    $scope.ForSearchendDate = newVal;
                    if ($scope.startDate == null) {
                        $scope.selectedDateText = ' to ' + newVal;
                    } else {
                        $scope.selectedDateText = $scope.startDate + ' to ' + newVal;
                    }
                }
            });

            $scope.endDate = null;
            $scope.startDate = null;
            $scope.ForSearchstartDate = null;
            $scope.ForSearchendDate = null;

            $scope.setDateRange = function(date) {
                var range = $scope.getDateRange(date);
                if (range.StartDate) {
                    $scope.ForSearchstartDate = range.StartDate.toString('MM/dd/yyyy');
                    //$scope.startDate = range.StartDate.toString('MM/dd/yyyy');
                } else {
                    //$scope.startDate = null;
                    $scope.ForSearchstartDate = null;
                }

                if (range.EndDate) {
                    $scope.ForSearchendDate = range.EndDate.toString('MM/dd/yyyy');
                } else {
                    $scope.ForSearchendDate = null;
                }

                $scope.selectedDateText = date;
            }

            $scope.getDateRange = function(range) {
                var endDate = new XDate(),
                    startDate = new XDate();
                switch (range.toLowerCase()) {
                case "all date & time":
                    startDate = null;
                    endDate = null;
                    break;
                case "this week":
                    startDate.addDays(-startDate.getDay());
                    endDate = startDate.clone().addDays(6);
                    break;
                case "this month":
                    startDate.setDate(1);
                    endDate = startDate.clone().addMonths(1).addDays(-1);
                    break;
                case "this year":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addYears(1).addDays(-1);
                    break;
                case "last week":
                    startDate.addDays(-startDate.getDay() - 7);
                    endDate = startDate.clone().addDays(6);
                    break;
                case "last month":
                    startDate.setDate(1).addMonths(-1);
                    endDate = startDate.clone().addMonths(1).addDays(-1);
                    break;
                case "last year":
                    startDate.setDate(1).setMonth(0).addYears(-1);
                    endDate.setDate(1).setMonth(0).addDays(-1);
                    break;
                case "last 7 days":
                    startDate.addDays(-7);
                    break;
                case "last 30 days":
                    startDate.addDays(-30);
                    break;
                case "1st quarter":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addMonths(3).addDays(-1);
                    break;
                case "2nd quarter":
                    startDate.setDate(1).setMonth(3);
                    endDate = startDate.clone().addMonths(3).addDays(-1);
                    break;
                case "3rd quarter":
                    startDate.setDate(1).setMonth(6);
                    endDate = startDate.clone().addMonths(3).addDays(-1);
                    break;
                case "4th quarter":
                    startDate.setDate(1).setMonth(9);
                    endDate = startDate.clone().addMonths(3).addDays(-1);
                    break;
                case "1st half":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addMonths(6).addDays(-1);
                    break;
                case "2nd half":
                    startDate.setDate(1).setMonth(6);
                    endDate = startDate.clone().addMonths(6).addDays(-1);
                    break;
                default:
                    startDate = null;
                    endDate = null;
                    break;
                }

                return { StartDate: startDate, EndDate: endDate };
            }

            $scope.searchValue = '';
            $scope.searchTypeSelected = 'leads';

            $scope.ConvertStringToArray = function(str) {
                var arr = [];
                if (str) {
                    var st = str.split('-');
                    for (var i = 0; i < st.length; i++) {
                        arr.push(st[i]);
                    }
                }
                return arr;
            }

            $scope.$watch('searchTypeSelected', function(newValue, oldValue) {

                $scope.invoiceStatusesSearch = [];
                $scope.selectedSales = [];
                $scope.selectedJobsStatuses = [];
                $scope.selectedleadStatuses = [];
                $scope.selectedSources = [];
                $scope.selectedDateText = 'All Date & Time';
                $scope.ForSearchstartDate = null;
                $scope.ForSearchendDate = null;
                $scope.startDate = null;
                $scope.endDate = null;

                var data = {
                    salesPersonIds: null,
                    jobStatusIds: null,
                    leadStatusIds: null,
                    invoiceStatuses: null,
                    selectedDateText: null
                };

                //if (newValue == 'leads') {
                //    if ($.cookie('LeadsSearchCriteria')) {
                //        data = JSON.parse($.cookie('LeadsSearchCriteria'));
                //    }
                //} else if (newValue == 'jobs') {
                //    if ($.cookie('JobsSearchCriteria')) {
                //        data = JSON.parse($.cookie('JobsSearchCriteria'));
                //    }
                //} else {
                //    if ($.cookie('InvoicesSearchCriteria')) {
                //        data = JSON.parse($.cookie('InvoicesSearchCriteria'));
                //    }
                //}

                if (data.salesPersonIds) {
                    if ($.isNumeric(data.salesPersonIds)) {
                        $scope.selectedSales = [];
                        $scope.selectedSales.push(data.salesPersonIds);
                    } else {
                        $scope.selectedSales = data.salesPersonIds;
                    }
                }

                if (data.jobStatusIds) {
                    if ($.isNumeric(data.jobStatusIds)) {
                        $scope.selectedJobsStatuses = [];
                        $scope.selectedJobsStatuses.push(+data.jobStatusIds);
                    } else {
                        $scope.selectedJobsStatuses = data.jobStatusIds;

                    }
                }

                if (data.sourceIds) {
                    if ($.isNumeric(data.sourceIds)) {
                        $scope.selectedSources = [];
                        $scope.selectedSources.push(+data.sourceIds);
                    } else {
                        $scope.selectedSources = data.sourceIds;

                    }
                }

                if (data.leadStatusIds) {
                    if ($.isNumeric(data.leadStatusIds)) {

                        $scope.selectedleadStatuses = [];
                        $scope.selectedleadStatuses.push(+data.leadStatusIds);
                    } else {
                        $scope.selectedleadStatuses = data.leadStatusIds;
                    }
                }

                if (data.invoiceStatuses) {
                    if ($.isNumeric(data.invoiceStatuses)) {
                        $scope.invoiceStatusesSearch = [];
                        $scope.invoiceStatusesSearch.push(data.invoiceStatuses);
                    } else {
                        $scope.invoiceStatusesSearch = data.invoiceStatuses;
                    }
                }

                if (data.selectedDateText) {
                    $scope.selectedDateText = data.selectedDateText;
                    if (data.createdOnUtcStart) {
                        $scope.ForSearchstartDate = data.createdOnUtcStart.remove("'", "");
                        $scope.startDate = data.createdOnUtcStart;
                    }

                    if (data.createdOnUtcEnd) {
                        $scope.ForSearchendDate = data.createdOnUtcEnd.remove("'", "");
                        $scope.endDate = data.createdOnUtcEnd.remove("'", "");
                    }
                } else {
                    if (data.createdOnUtcStart) {
                        $scope.ForSearchstartDate = data.createdOnUtcStart;
                        $scope.startDate = data.createdOnUtcStart;
                    }
                    if (data.createdOnUtcEnd) {
                        $scope.ForSearchendDate = data.createdOnUtcEnd;
                        $scope.endDate = data.createdOnUtcEnd;
                    }
                }

            });

            $scope.$on('$routeChangeStart', function(next, current) {
                if (current.$$route) {

                    if (current.$$route.originalPath.indexOf('leadsSearch') != -1) {
                        setTimeout(function() {
                            if ($('#collapseSearch').hasClass('collapsed')) {

                                $('#collapseSearch').click();
                            }
                        }, 200);

                        $scope.searchTypeSelected = 'leads';
                    } else if (current.$$route.originalPath.indexOf('jobsSearch') != -1) {
                        setTimeout(function() {
                            if ($('#collapseSearch').hasClass('collapsed')) {

                                $('#collapseSearch').click();
                            }
                        }, 200);

                        $scope.searchTypeSelected = 'jobs';
                    } else if (current.$$route.originalPath.indexOf('invoicesSearch') != -1) {
                        setTimeout(function() {

                            if ($('#collapseSearch').hasClass('collapsed')) {
                                $('#collapseSearch').click();
                            }
                        }, 200);


                        $scope.searchTypeSelected = 'invoices';
                    } else if (current.$$route.originalPath.indexOf('dashboard') != -1) {
                        $scope.searchTypeSelected = 'leads';
                    } else {
                        if ($('#collapseSearch').hasClass('collapsed') == false) {
                            $('#collapseSearch').click();
                        }
                        
                    }
                }
            });

            $scope.ConvertArrayToString = function(arr) {
                if (!arr) {
                    return '';
                }
                if ($.isNumeric(arr)) {
                    return arr;
                }
                return arr.join('-');
            }

            $scope.submitSearch = function() {
                var data = null;
                $("#crm-alertbox").hide();
                if ($scope.searchTypeSelected == 'jobs') {
                    data = { orderByDirection: "Desc", orderBy: 'createdonutc' };
                    if ($scope.selectedSales) {
                        data.salesPersonIds = $scope.selectedSales;
                    }

                    if ($scope.selectedJobsStatuses) {
                        data.jobStatusIds = $scope.selectedJobsStatuses;
                    }

                    if ($scope.selectedleadStatuses) {
                        data.leadStatusIds = $scope.selectedleadStatuses;
                    }

                    if ($scope.invoiceStatusesSearch) {
                        data.invoiceStatuses = $scope.invoiceStatusesSearch;
                    }

                    if ($scope.selectedSources) {
                        data.sourceIds = $scope.selectedSources;
                    }

                    if ($scope.ForSearchstartDate)
                        data.createdOnUtcStart = $scope.ForSearchstartDate;

                    if ($scope.ForSearchendDate)
                        data.createdOnUtcEnd = $scope.ForSearchendDate;

                    data.searchTerm = $('#search-box').val();

                    data.searchFilter = $('#search-type').find('.active').find('input').val();

                    if (!data.searchFilter) {
                        data.searchFilter = 'Auto_Detect';
                    }

                    var url = '/jobsSearch/' + data.searchTerm + '/' + data.searchFilter + '/' + $scope.ConvertArrayToString(data.salesPersonIds) + '/' + $scope.ConvertArrayToString(data.jobStatusIds) +
                        '/' + $scope.ConvertArrayToString(data.leadStatusIds) +
                        '/' + $scope.ConvertArrayToString(data.invoiceStatuses) + '/' + $scope.ConvertArrayToString(data.sourceIds) +
                        '/' + encodeURIComponent($scope.selectedDateText) + '/' + (data.createdOnUtcStart ? encodeURIComponent(data.createdOnUtcStart) : '') + '/'
                        + (data.createdOnUtcEnd ? encodeURIComponent(data.createdOnUtcEnd) : '');

                    $location.path(url);

                }

                if ($scope.searchTypeSelected == 'leads') {

                    data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };
                    //if ($scope.selectedSales) {
                    //    data.salesPersonIds = $scope.selectedSales;
                    //}

                    if ($scope.selectedSales) {
                        data.salesPersonIds = $scope.selectedSales;
                    }

                    if ($scope.selectedJobsStatuses) {
                        data.jobStatusIds = $scope.selectedJobsStatuses;
                    }

                    if ($scope.selectedleadStatuses) {
                        data.leadStatusIds = $scope.selectedleadStatuses;
                    }

                    if ($scope.selectedSources) {
                        data.sourceIds = $scope.selectedSources;
                    }

                    if ($scope.invoiceStatusesSearch) {
                        data.invoiceStatuses = $scope.invoiceStatusesSearch;
                    }

                    if ($scope.ForSearchstartDate)
                        data.createdOnUtcStart = $scope.ForSearchstartDate;
                    if ($scope.ForSearchendDate)
                        data.createdOnUtcEnd = $scope.ForSearchendDate;

                    data.searchTerm = $('#search-box').val();

                    data.searchFilter = $('#search-type').find('.active').find('input').val();

                    //if (data.searchFilter != 'Auto_Detect') {
                    //    data.searchFilter = $scope.SearchFilter;
                    //}

                    if ($scope.SearchTerm) {
                        data.searchTerm = $scope.SearchTerm;
                    }

                    var url = '/leadsSearch/' + data.searchTerm + '/' + data.searchFilter + '/' + $scope.ConvertArrayToString(data.salesPersonIds) + '/' + $scope.ConvertArrayToString(data.jobStatusIds) +
                        '/' + $scope.ConvertArrayToString(data.leadStatusIds) +
                        '/' + $scope.ConvertArrayToString(data.invoiceStatuses) + '/' + $scope.ConvertArrayToString(data.sourceIds) +
                        '/' + encodeURIComponent($scope.selectedDateText) + '/' + (data.createdOnUtcStart ? encodeURIComponent(data.createdOnUtcStart) : '') + '/'
                        + (data.createdOnUtcEnd ? encodeURIComponent(data.createdOnUtcEnd) : '');


                    $location.path(url);
                    //$location.path("/leadsSearch/" + $scope.searchValue + "/" + $('#search-type').find('.active').find('input').val() + "/All%20Date%20&%20Time/");
                }
                if ($scope.searchTypeSelected == 'invoices') {
                    data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };
                    //if ($scope.selectedSales) {
                    //    data.salesPersonIds = $scope.selectedSales;
                    //}

                    if ($scope.selectedSales) {
                        data.salesPersonIds = $scope.selectedSales;
                    }

                    if ($scope.selectedJobsStatuses) {
                        data.jobStatusIds = $scope.selectedJobsStatuses;
                    }

                    if ($scope.selectedleadStatuses) {
                        data.leadStatusIds = $scope.selectedleadStatuses;
                    }

                    if ($scope.selectedSources) {
                        data.sourceIds = $scope.selectedSources;
                    }

                    if ($scope.invoiceStatusesSearch) {
                        data.invoiceStatuses = $scope.invoiceStatusesSearch;
                    }

                    if ($scope.ForSearchstartDate)
                        data.createdOnUtcStart = $scope.ForSearchstartDate;
                    if ($scope.ForSearchendDate)
                        data.createdOnUtcEnd = $scope.ForSearchendDate;

                    data.searchTerm = $('#search-box').val();

                    data.searchFilter = $('#search-type').find('.active').find('input').val();

                    //if (data.searchFilter != 'Auto_Detect') {
                    //    data.searchFilter = $scope.SearchFilter;
                    //}

                    if ($scope.SearchTerm) {
                        data.searchTerm = $scope.SearchTerm;
                    }

                    var url = '/invoicesSearch/' + data.searchTerm + '/' + data.searchFilter + '/' + $scope.ConvertArrayToString(data.salesPersonIds) + '/' + $scope.ConvertArrayToString(data.jobStatusIds) +
                        '/' + $scope.ConvertArrayToString(data.leadStatusIds) +
                        '/' + $scope.ConvertArrayToString(data.invoiceStatuses) + '/' + $scope.ConvertArrayToString(data.sourceIds) +
                        '/' + encodeURIComponent($scope.selectedDateText) + '/' + (data.createdOnUtcStart ? encodeURIComponent(data.createdOnUtcStart) : '') + '/'
                        + (data.createdOnUtcEnd ? encodeURIComponent(data.createdOnUtcEnd) : '');


                    $location.path(url);
                }
            }

            $scope.ConvertStringToArray = function(str) {
                var arr = [];
                if (str) {
                    var st = str.split('-');
                    for (var i = 0; i < st.length; i++) {
                        arr.push(st[i]);
                    }
                }
                return arr;
            }

            $rootScope.$on('applyMainSearchFilter', function() {
                $scope.selectedJobsStatuses = saveCriteriaService.selectedJobsStatuses;
                $scope.selectedleadStatuses = saveCriteriaService.selectedleadStatuses;
                $scope.selectedSources = saveCriteriaService.selectedSources;

                $scope.selectedSales = saveCriteriaService.selectedSales;
                $scope.invoiceStatusesSearch = saveCriteriaService.invoiceStatusesSearch;
                $scope.selectedDateText = saveCriteriaService.selectedDateText;
                $scope.ForSearchstartDate = saveCriteriaService.ForSearchstartDate;
                $scope.ForSearchendDate = saveCriteriaService.ForSearchendDate;
                $scope.startDate = saveCriteriaService.ForSearchstartDate;
                $scope.endDate = saveCriteriaService.ForSearchendDate;
            });

            angular.element(document).ready(function() {
                $('[name="startDate"]').kendoDatePicker(
                    {
                        change: function(event) {
                            var startDate = this.value();

                            var endDate = new Date($(this.element).closest('div').parent().find("[name='endDate']").val());
                            if (endDate < startDate) {
                                $(this.element).closest('div').parent().find("#validDateRange").css('display', 'inline');
                            } else {
                                $(this.element).closest('div').parent().find("#validDateRange").css('display', 'none');
                            }
                            $('#searchBtn').prop('disabled', endDate < startDate);
                            $('#exportBtn').prop('disabled', endDate < startDate);
                        }
                    }
                );
                $('[name="endDate"]').kendoDatePicker(
                    {
                        change: function(event) {

                            var endDate = this.value();

                            var startDate = new Date($(this.element).closest('div').parent().find("[name='startDate']").val());
                            if (endDate < startDate) {
                                $(this.element).closest('div').parent().find("#validDateRange").css('display', 'inline');
                            } else {
                                $(this.element).closest('div').parent().find("#validDateRange").css('display', 'none');
                            }
                            $('#searchBtn').prop('disabled', endDate < startDate);
                            $('#exportBtn').prop('disabled', endDate < startDate);
                        }
                    }
                );
            });

            $scope.redirectToAllLeads = function() {

                //if ($.cookie('LeadsSearchCriteria')) {
                //    var data = JSON.parse($.cookie('LeadsSearchCriteria'));
                //    if ($scope.DateRangeArray.indexOf(data.selectedDateText) && data.selectedDateText != "All Date & Time") {
                //        var dateRange = $scope.getDateRange(data.selectedDateText);
                //        data.createdOnUtcStart = dateRange.StartDate.toString('MM/dd/yyyy');
                //        data.createdOnUtcEnd = dateRange.EndDate.toString('MM/dd/yyyy');
                //    }
                //    var url = '/leadsSearch/' + data.searchTerm + '/' + data.searchFilter + '/' + $scope.ConvertArrayToString(data.salesPersonIds) + '/' + $scope.ConvertArrayToString(data.jobStatusIds) +
                //        '/' + $scope.ConvertArrayToString(data.leadStatusIds) +
                //        '/' + $scope.ConvertArrayToString(data.invoiceStatuses) + '/' + $scope.ConvertArrayToString(data.sourceIds) +
                //        '/' + encodeURIComponent(data.selectedDateText) + '/' + (data.createdOnUtcStart ? encodeURIComponent(data.createdOnUtcStart) : '') + '/'
                //        + (data.createdOnUtcEnd ? encodeURIComponent(data.createdOnUtcEnd) : '');

                //    $location.path(url);
                //} else {
                $location.path('/leadsSearch/');
                //}
            }

            $scope.redirectToAllJobs = function() {

                //if ($.cookie('JobsSearchCriteria')) {
                //    var data = JSON.parse($.cookie('JobsSearchCriteria'));
                //    if ($scope.DateRangeArray.indexOf(data.selectedDateText) && data.selectedDateText != "All Date & Time") {
                //        var dateRange = $scope.getDateRange(data.selectedDateText);
                //        data.createdOnUtcStart = dateRange.StartDate.toString('MM/dd/yyyy');
                //        data.createdOnUtcEnd = dateRange.EndDate.toString('MM/dd/yyyy');
                //    }
                //    var url = '/jobsSearch/' + data.searchTerm + '/' + data.searchFilter + '/' + $scope.ConvertArrayToString(data.salesPersonIds) + '/' + $scope.ConvertArrayToString(data.jobStatusIds) +
                //        '/' + $scope.ConvertArrayToString(data.leadStatusIds) +
                //        '/' + $scope.ConvertArrayToString(data.invoiceStatuses) + '/' + $scope.ConvertArrayToString(data.sourceIds) +
                //        '/' + encodeURIComponent(data.selectedDateText) + '/' + (data.createdOnUtcStart ? encodeURIComponent(data.createdOnUtcStart) : '') + '/'
                //        + (data.createdOnUtcEnd ? encodeURIComponent(data.createdOnUtcEnd) : '');

                //    $location.path(url);
                //} else {
                $location.path('/jobsSearch//Auto_Detect//////All%20Date%20&%20Time//');
                //}
            }

            $scope.redirectToAllInvoices = function() {
                //if ($.cookie('InvoicesSearchCriteria')) {
                //    var data = JSON.parse($.cookie('InvoicesSearchCriteria'));
                //    if ($scope.DateRangeArray.indexOf(data.selectedDateText) && data.selectedDateText != "All Date & Time") {
                //        var dateRange = $scope.getDateRange(data.selectedDateText);
                //        data.createdOnUtcStart = dateRange.StartDate.toString('MM/dd/yyyy');
                //        data.createdOnUtcEnd = dateRange.EndDate.toString('MM/dd/yyyy');
                //    }
                //    var url = '/invoicesSearch/' + (data.searchTerm ? data.searchTerm : '') + '/' + data.searchFilter + '/' + $scope.ConvertArrayToString(data.salesPersonIds) + '/' + $scope.ConvertArrayToString(data.jobStatusIds) +
                //        '/' + $scope.ConvertArrayToString(data.leadStatusIds) +
                //        '/' + $scope.ConvertArrayToString(data.invoiceStatuses) + '/' + $scope.ConvertArrayToString(data.sourceIds) +
                //        '/' + encodeURIComponent(data.selectedDateText) + '/' + (data.createdOnUtcStart ? encodeURIComponent(data.createdOnUtcStart) : '') + '/'
                //        + (data.createdOnUtcEnd ? encodeURIComponent(data.createdOnUtcEnd) : '');

                //    $location.path(url);
                //} else {
                $location.path('/invoicesSearch//Auto_Detect//////All%20Date%20&%20Time//');
                //}
            }

            //$scope.isSaveVisible = $location.$$path.indexOf('Search') != -1;

            $scope.saveFilters = function () {
                var SearchUrl = window.location.hash;
                if (SearchUrl.indexOf("/leadsSearch/") >= 0 && SearchUrl.indexOf("//////") >=0) {
                    HFC.DisplayAlert("To save search first we need to make a search with the text filter applied. Please make a search and filter your criteria.");
                    return false;
                }
                else {
                    $("#search-save").modal("show");
                }
            }

            $scope.clearFilters = function() {
                var path = $location.$$path;
                if (path.indexOf("invoicesSearch") != -1) {
                    //$.removeCookie('InvoicesSearchCriteria', { path: '/' });
                    $location.path('/invoicesSearch//Auto_Detect//////All%20Date%20&%20Time//');

                } else if (path.indexOf("jobsSearch") != -1) {
                    //$.removeCookie('JobsSearchCriteria', { path: '/' });
                    $location.path('/jobsSearch//Auto_Detect//////All%20Date%20&%20Time//');

                } else if (path.indexOf("leadsSearch") != -1) {
                    // $.removeCookie('LeadsSearchCriteria', { path: '/' });
                    $location.path('/leadsSearch/');
                }

                //$.removeCookie('PurchaseOrderSearchCriteria', { path: '/accounting' });
                //$.removeCookie('PurchaseOrderSearchCriteria', { path: '/' });
                HFC.DisplaySuccess("Cleared");


            }
        }
    ]).service('saveSearchService', [
        '$http', '$rootScope', function($http, $rootScope) {
            var srv = this;
            srv.nameValue = '';
            srv.isDublicate = false;
            srv.isMaxSavesCount = false;
            srv.save = function() {
                var data = {};
                data.Name = srv.nameValue;
                data.SearchParams = window.location.hash;    
                $http.post('/api/Search/0/SavedSearch', data).then(
                    function(result) {
                        if (result.data.results) {
                            $("#search-save").modal("hide");
                            srv.nameValue = '';
                            srv.isDublicate = false;
                            $http.get('/api/Search/0/GetSavedSearch').then(
                                function(result) {
                                    $rootScope.SavedSearches = result.data.results;
                                });

                        }
                    });
            }

            srv.close = function() {
                $("#search-save").modal("hide");
                srv.isMaxSavesCount = false;
                srv.nameValue = '';
                srv.isDublicate = false;
            }

            srv.noClick = function() {
                srv.isDublicate = false;
            }

            srv.yesClick = function() {
                srv.save();
            }

            srv.saveClick = function() {
                if (srv.nameValue.length <= 2) {
                    return;
                }
                
                for (var i = 0; i < $rootScope.SavedSearches.length; i++) {
                    if ($rootScope.SavedSearches[i].Name.toLowerCase() == srv.nameValue.toLowerCase()) {
                        srv.isDublicate = true;
                        return;
                    }
                }

                if ($rootScope.SavedSearches.length >= 10) {
                    srv.isMaxSavesCount = true;
                    return;
                }

                srv.save();
            }
            srv.deleteSearch = function(id) {
                $http.delete('/api/Search/' + id + '/DeleteSavedSearch').then(
                    function(result) {
                        if (result.data.results) {
                            $("#search-save").modal("hide");
                            srv.nameValue = '';
                            $http.get('/api/Search/0/GetSavedSearch').then(
                                function(result2) {
                                    $rootScope.SavedSearches = result2.data.results;
                                });
                        }
                    });
            }
        }
    ]).
    controller('saveSearchController', [
        '$http', '$scope', '$window', '$location', '$rootScope', 'saveSearchService', function($http, $scope, $window, $location, $rootScope, saveSearchService) {
            $scope.saveSearchService = saveSearchService;            
        }
    ])
    .directive('hfcSaveSearchModal', [
        'cacheBustSuffix', function(cacheBustSuffix) {
            return {
                restrict: 'E',
                replace: true,
                scope: {},
                //scope: true,
                controller: 'saveSearchController',
                templateUrl: '/templates/NG/Lead/search-save.html?cache-bust=' + cacheBustSuffix
            }
        }
    ]);