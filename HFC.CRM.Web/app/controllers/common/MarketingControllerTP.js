﻿app.controller('MarketingControllerTP', ['$scope', 'HFCService', 'NavbarService', function ($scope, HFCService, NavbarService) {

   
    $scope.HFCService = HFCService;
   

    $scope.NavbarService = NavbarService;
    $scope.NavbarService.SelectMarketing();
    //This Grid is for the Territory 
    $scope.GetFranchiseSetUpTerritory = function () {
        $scope.FranchiseSetupTerritory = {
            dataSource: {
                transport: {
                    read: {
                        url: function (params) {
                            var url1 = '/api/lookup/0/DemoFranchise';
                            return url1;
                        },
                        dataType: "json"
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {

                            TerritoryId: { editable: false },
                            TerritoryName: { editable: true, nullable: true },
                            MinionId: { editable: true },
                            WebsiteUrl: { editable: true },
                        }
                    }
                }
            },
            columns: [
                        {


                            field: "TerritoryId",
                            title: "Territory ID",

                            filterable: { multi: true, search: true },



                        },
                        {


                            field: "TerritoryName",
                            title: "Territory Name",

                            filterable: { multi: true, search: true },
                            hidden: false,


                        },
                        {
                            field: "MinionId",
                            title: "Minion ID",
                            filterable: { multi: true, search: true },
                            hidden: false,


                        },
                        {
                            field: "WebsiteUrl",
                            title: "Website URL",

                        },
                        {
                            field: "",
                            title: "",
                            hidden: false,
                            editable: false,
                            width: "80px",
                            template: '<ul ><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="beginEditPricing(dataItem)">View Zip Codes</a></li>  <li><a  href="javascript:void(0)" ng-click="removeNewPricing(this)">View Territory Map</a></li> </ul> </li> </ul>'
                        },
            ],
            editable: true,
            filterable: true,
            resizable: true,
            autoSync: true,
            noRecords: { template: "No records found" },
            scrollable: true,
            toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search"><input ng-keyup="ShipgridSearchEdit()" type="search" id="searchBox" placeholder="Search" class="k-textbox leadsearch_tbox"><input type="button" id="btnReset" ng-click="ShipgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"><div class="pull-right" ><button id="btnprocurement" type="button" onclick="return false;" ng-click="AddTerritory()"><img src="/Content/images/add.png"><br>Add Territory</button></div></div></div></script>').html()) }],

        };
    };
    $scope.GetFranchiseSetUpTerritory();

    // This Grid is for the Additional Owner
    $scope.GetFranchiseSetUpOwner = function () {
        $scope.FranchiseSetupOwner = {
            dataSource: {
                transport: {
                    read: {
                        url: function (params) {
                            var url1 = '/api/lookup/0/GetDemoOwner';
                            return url1;
                        },
                        dataType: "json"
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            FirstName: { editable: true, nullable: true },
                            LastName: { editable: true, nullable: true },
                        }
                    }
                }
            },
            columns: [
                        {


                            field: "FirstName",
                            title: "First Name",

                            filterable: { multi: true, search: true },



                        },
                        {


                            field: "LastName",
                            title: "Last Name",

                            filterable: { multi: true, search: true },
                            hidden: false,


                        },
                        
                        
            ],
            editable:true,
            filterable: true,
            resizable: true,
            autoSync: true,
            noRecords: { template: "No records found" },
            scrollable: true,
            toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search"><div class="pull-right" ><button id="btnprocurement" type="button" onclick="return false;" ng-click="AddOwner()"><img src="/Content/images/add.png"><br>Add Owner</button></div></div></div></script>').html()) }],

        };
    };
    $scope.GetFranchiseSetUpOwner();


    //Add Territory
    $scope.AddTerritory = function ()
    {
        var grid = $("#gridFranchiseSetupTerritory").data("kendoGrid");

        grid.addRow();
    }
    //Add Owner
    $scope.AddOwner =function()
    {
        var grid = $("#gridFranchiseOwner").data("kendoGrid");

        grid.addRow();
    }
    //Pop Calling Function
    $scope.Show = function () {

        $("#territoryId").modal("show");

    }
    $scope.Cancel = function () {
       
       
        $("#territoryId").modal("hide");
    };
   }])
