﻿app.controller('SaveCalendarControllerTP', ['$scope', '$rootScope', '$http', '$routeParams', '$modal', 'HFCService', 'NavbarService', 'PersonService', 'CalendarService', function ($scope, $rootScope, $http, $routeParams, $modal, HFCService, NavbarService, PersonService, CalendarService) {

    $scope.HFCService = HFCService;
    $scope.PersonService = PersonService;
    $scope.CalendarService = CalendarService;
    $scope.NavbarService = NavbarService;
    $scope.NavbarService.SelectCalendar();

    $scope.tabselection = $routeParams.tabselection;
    $scope.SaveCalendarId = $routeParams.Id;
    $scope.Dayview = true;
    $scope.Weekview = false;
    $scope.Monthview = false;
    $scope.active = 'active';
    //Get Method to load selected calendarview for view1/view2...5
    //Get the details to select menu to show active
    //Role grouping
    //Individual Employee
    //View mode: Month/Week/Day
    $scope.PersonService.CallendarSuccessCallback = function () {
        $scope.FilterCalendar();
    }
    //--------------Save Calendar Kendo Events -----------------
    function AddNewEvent(e) {
        var event = e;
        var startDate = event.event.start;
        var endDate = event.event.end;
        var res = $scope.CalendarService.NewApt('persongo', null, null, null, null, null, '', startDate, endDate);
    }
    $scope.AddTask = function () {
        var modalId = 'newtask';
        var res = $scope.PersonService.CalendarService.NewTask(modalId);
    }
    $scope.AddEvent = function (modalId) {
        //  $scope.PersonService.CalendarService.ResetFields();
        var res = $scope.PersonService.AddOrEditAppointment(modalId, this, 0, true);
        $scope.$apply();
    }
    $scope.Cancel = function () {
    }
    $scope.SendReminder = function (EventId) {
        var dto = { EventId: EventId };
        $http.put('/api/Calendar/' + EventId + '/UpdateEvent').then(function (response) {
            $scope.IsBusy = false;

            if ($routeParams.orderId > 0) {
                HFC.DisplaySuccess("Event Created");
            }
        }, function (response) {
            HFC.DisplayAlert(response.statusText);
            $scope.IsBusy = false;
        });
    }
    $scope.EditTask = function (TaskId, TaskModel) {
        $("#calendarTaskModal").modal("hide");
        var editTask = CalendarService.EditTaskFromCalendarSceduler('newtask', TaskModel)
    }
    $scope.EditEvent = function (modalId, EventId, attendees) {
        $("#_calendarModal_").modal("hide");
        var editTask = CalendarService.GetEventsFromScheduler(modalId, EventId, null, attendees);
        //  $scope.$apply();
    }
    $scope.DeleteEventInDetailsView = function (moaldId, EventId) {
        var Id = EventId;
        var deleteurl = '/api/calendar/' + Id;
        $http.delete(deleteurl).then(function (response) {
            var res = response.data;
            if (res) {
                var saveStatus = "Successful";
                HFC.DisplaySuccess(saveStatus);
                $scope.FilterCalendar();
            }
            else {

            }
        });
        $("#_calendarModal_").modal("hide");
    }
    $scope.DeleteTaskInDetailsView = function (moaldId, TaskId) {
        var Id = TaskId;
        var deleteurl = '/api/Tasks/' + Id;
        $http.delete(deleteurl).then(function (response) {
            var res = response.data;
            if (res) {
                var saveStatus = "Successful";
                HFC.DisplaySuccess(saveStatus);
                $scope.FilterCalendar();
            }
            else {
                HFC.DisplayAlert("Error");
            }
        });
        $("#" + moaldId).modal("hide");
    }
    $scope.CompeleteTaskInDetailsView = function (moaldId, TaskId) {
        var Id = TaskId;
        var markurl = '/api/Tasks/' + Id + '/Mark?IsComplete=true';
        $http.get(markurl).then(function (response) {
            var res = response.data;
            if (!res) {
                HFC.DisplaySuccess(saveStatus);
            }
            else {
                HFC.DisplayAlert("Error");
            }
        });
        $("#" + moaldId).modal("hide");
    }
    $scope.TaskReminderInDetailsView = function (moaldId, TaskId) {
        var Id = TaskId;
        var reminderurl = '/api/Tasks/' + Id + '/Reminder';
        $http.put(reminderurl).then(function (response) {
            var res = response.data;
            if (res) {
                var saveStatus = "Reminder Sent Successful"
                HFC.DisplaySuccess(saveStatus);
            }
            else {
                HFC.DisplayAlert("Error");
            }
        });
        $("#" + moaldId).modal("hide");
    }
    $scope.EventReminderInDetailsView = function (moaldId, TaskId) {
        var Id = TaskId;
        var reminderurl = '/api/Tasks/' + Id + '/EventReminder';
        $http.put(reminderurl).then(function (response) {
            var res = response.data;
            if (res) {
                var saveStatus = "Reminder Sent Successful"
                HFC.DisplaySuccess(saveStatus);
            }
            else {
                HFC.DisplayAlert("Error");
            }
        });
        $("#" + moaldId).modal("hide");
    }
    $scope.ViewOpportunityPage = function (OpportunityId) {
        $("#_calendarModal_").modal("hide");
        window.location.href = '#!/Opportunitys/' + OpportunityId;
    }
    //--------------Save Calendar Kendo Events -----------------
    //----------- Calendar filter---------------------------
    $scope.ChangeColor = function (id) {
        var aId = $('#' + id)
        if ($(aId).hasClass("active")) {
            $(aId).removeClass('active');
        }
        else {
            $(aId).addClass('active');
        }
    }
    $scope.ChangeColorByAllUsers = function (id) {
        var aId = $('#' + id)
        if ($(aId).hasClass("active")) {
            $(aId).removeClass('active');
            //Sales Select All
            if (id == 'a_selectAllSales') {
                $("#divSales a").each(function () {
                    $(this).removeClass('active');
                });
            }
            //Installers
            if (id == 'a_selectAllInstallers') {
                $("#divInstall a").each(function () {
                    $(this).removeClass('active');
                });
            }
            //Others
            if (id == 'a_selectAllOthers') {
                $("#divOther a").each(function () {
                    $(this).removeClass('active');
                });
            }
        }
        else {
            $(aId).addClass('active');
            if (id == 'a_selectAllSales') {
                $("#divSales a").each(function () {
                    $(this).addClass('active');
                });
            }
            if (id == 'a_selectAllInstallers') {
                $("#divInstall a").each(function () {
                    $(this).addClass('active');
                });
            }
            if (id == 'a_selectAllOthers') {
                $("#divOther a").each(function () {
                    $(this).addClass('active');
                });
            }
        }
    }
    $scope.getSelectedUsers = function () {
        var arr = [];
        $("#divSales a").each(function () {
            if ($(this).is(':visible')) {
                if ($(this).hasClass("active")) {
                    var value = $(this).attr("data-bind");
                    arr.push(value);
                }
            }
        });
        $("#divInstall a").each(function () {
            if ($(this).is(':visible')) {
                if ($(this).hasClass("active")) {
                    var value = $(this).attr("data-bind");
                    arr.push(value);
                }
            }
        });
        $("#divOther a").each(function () {
            if ($(this).is(':visible')) {
                if ($(this).hasClass("active")) {
                    var value = $(this).attr("data-bind");
                    arr.push(value);
                }
            }
        });
        return arr;
    }
    $scope.FilterCalendarByAllUsers = function (id) {
        $scope.FilterCalendar();
    }
    $scope.ReInitializeScheduler = function () {
        $scope.KendoSchedulerIntitilize();
    }
    $scope.FilterCalendar = function (Id) {
        var scheduler = $("#Savedscheduler").data("kendoScheduler");
        var selectedViewName = scheduler._selectedViewName;
        $scope.SetCalendarViewType(selectedViewName);
        var filrerdata = $scope.getSelectedUsers();
        var datepicker = $("#calendar").data("kendoCalendar");
        datefilter = datepicker._current;
        var date = new Date(datefilter);
        var currentdate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
        //yyyy/mm/dd;
        datefilter = new Date(currentdate);//date
        ResetScheduler();
        $("#Savedscheduler").kendoScheduler({
            date: datefilter,
            //startTime: datefilter,
            height: 600,
            showWorkHours: true,
            // eventTemplate: $("#event-template").html(),
            dataBound: function (e) {
                //alert(e);
                var view = this.view();
                var events = this.dataSource.view();
                var eventElement;
                var event;
                var ViewName = $("#Savedscheduler").data("kendoScheduler")._selectedViewName;
                for (var idx = 0, length = events.length; idx < length; idx++) {
                    event = events[idx];

                    //c6af4a5d-ac72-47b4-becb-d5d57d854061
                    //a51de880-e63f-4e8a-ae39-d635398ff268

                    //get event element
                    eventElement = view.element.find("[data-uid=" + event.uid + "]");

                    //set the backgroud of the element
                    eventElement.css("border-color", event.UserRoleColor);
                    eventElement.css("background-color", event.AppointmentTypeColor);
                    eventElement.css("border-width", "3px");
                    //Event Calendar View Settings
                    if (event.eventType == 2) {
                        // eventElement.css("height", "40px");
                        $scope.TextToDisplayInView(event.TimeSlot, ViewName, eventElement, event);
                    }
                    if (event.recurrenceRule != null) {
                        var recurenceItems = JSLINQ(e.sender._data)
                                                     .Where(function (item) { return item.recurrenceId == event.recurrenceId });
                        if (recurenceItems != null) {
                            if (recurenceItems.items.length > 0) {
                                for (var rdx = 0; rdx <= recurenceItems.items.length - 1; rdx++) {
                                    if (recurenceItems.items[rdx].uid != null) {
                                        //var uid = e.sender._data[1].uid;
                                        eventElement = view.element.find("[data-uid=" + recurenceItems.items[rdx].uid + "]");
                                        //set the backgroud of the element
                                        eventElement.css("border-color", event.UserRoleColor);
                                        eventElement.css("background-color", event.AppointmentTypeColor);
                                        eventElement.css("border-width", "3px");
                                        //Event Calendar View Settings
                                        if (event.eventType == 2) {
                                            // eventElement.css("height", "40px");
                                            $scope.TextToDisplayInView(event.TimeSlot, ViewName, eventElement, event, true);
                                        }
                                    }

                                }
                            }
                        }

                    }

                }
            },
            editable: {
                editRecurringMode: "occurrence"
            },
            views: [
              { type: "day", selected: $scope.Dayview },
              { type: "week", selected: $scope.Weekview },
             { type: "month", selected: $scope.Monthview },
                //"agenda",
                //{ type: "timeline", eventHeight: 50 }
            ],
            //editable: {
            //    template: $("#customEditorTemplate").html(),
            //},
            // eventTemplate: $("#event-template").html(),
            timezone: "Etc/UTC",
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: '/CalTasks/ReadSelection?PersonIds=' + filrerdata,
                        dataType: "json",
                        data: filter
                    },
                    update: {
                        url: '/CalTasks/update',
                        dataType: "jsonp"
                    },
                    create: {
                        url: '/CalTasks/create',
                        dataType: "jsonp"
                    },
                    destroy: {
                        url: '/CalTasks/Delete',
                        dataType: "jsonp"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "create" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        if (operation === "update" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        if (operation === "destroy" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                    }
                },
                serverFiltering: true,
                schema: {
                    model: {
                        id: "taskID",
                        fields: {
                            Id: { from: "TaskID" },
                            taskID: { from: "TaskID" },
                            title: { from: "Title", defaultValue: "No title", title: "Subject", validation: { required: true } },
                            start: { type: "date", from: "Start" },
                            end: { type: "date", from: "End" },
                            startTimezone: { from: "StartTimezone" },
                            endTimezone: { from: "EndTimezone" },
                            description: { from: "Description" },
                            recurrenceId: { from: "RecurrenceID" },
                            recurrenceRule: { from: "RecurrenceRule" },
                            recurrenceException: { from: "RecurrenceException" },
                            ownerId: { from: "OwnerID" },
                            isAllDay: { type: "boolean", from: "IsAllDay" },
                            ReminderId: { from: "ReminderId" },
                            AppointmentTypeId: { from: "AppointmentType", defaultValue: 1 },
                            location: { from: "Location" },
                            IsPrivate: { from: "IsPrivate" },
                            AppointmentTypeColor: { from: "AppointmentTypeColor" },
                            UserRoleColor: { from: "UserRoleColor" },
                            IsTask: { from: "IsTask" }
                        }
                    }
                }

            },
            add: function (e) {
                AddNewEvent(e);
            },
            resources: [
                {
                    field: "ownerId",
                    title: "Assigned To",
                    dataSource: {
                        transport: {
                            read: {
                                url: 'CalTasks/GetInstallers/',
                                dataType: "json"
                            }
                        }
                    }
                },
                {
                    field: "ReminderId",
                    title: "Reminder",
                    dataSource: {
                        transport: {
                            read: {
                                url: 'CalTasks/GetReminders/',
                                dataType: "json"
                            }
                        }
                    }
                },
                    {
                        field: "AppointmentTypeId",
                        title: "Type Of Appointment",
                        dataSource: {
                            transport: {
                                read: {
                                    url: 'CalTasks/GetTypeAppointments/',
                                    dataType: "json"
                                }
                            }
                        }
                    }
            ]
        });

    }
    $scope.SetCalendarViewType = function (viewType) {
        $scope.tabselection = viewType;
        if ($scope.tabselection == 'day') {
            $scope.Dayview = true;
            $scope.Weekview = false;
            $scope.Monthview = false;
        }
        if ($scope.tabselection == 'week') {
            $scope.Dayview = false;
            $scope.Weekview = true;
            $scope.Monthview = false;
        }
        if ($scope.tabselection == 'month') {
            $scope.Dayview = false;
            $scope.Weekview = false;
            $scope.Monthview = true;
        }
    }
    //-------------------------------------------------

   $scope.SetCalendarMode=function(mode)
    {
        if (mode == 1) { mode = 'day'; }
        if (mode == 2) { mode = 'week'; }
        if (mode == 3) { mode = 'month'; }
        if (mode == 'day') {
            $scope.Dayview = true;
            $scope.Weekview = false;
            $scope.Monthview = false;
        }
        if (mode == 'week') {
            $scope.Dayview = false;
            $scope.Weekview = true;
            $scope.Monthview = false;
        }
        if (mode == 'month') {
            $scope.Dayview = false;
            $scope.Weekview = false;
            $scope.Monthview = true;
        }
    }
    $http.get('/CalTasks/ReadSavedcalendar/' + $scope.SaveCalendarId).then(function (data) {
        $scope.SavedCalendarList = data.data;
        $scope.SavedCalendarViewMode = $scope.SavedCalendarList[$scope.SaveCalendarId - 1].Viewmode;
        $scope.SetCalendarMode($scope.SavedCalendarViewMode);
        $scope.KendoSchedulerIntitilize();
     
    });
    var filter = {
        personIds: HFCService.CurrentUser.PersonId,
        IsMainCalendar: true
    };
    $scope.getSelectedUsers = function () {
        var arr = [];
        $("#divSales a").each(function () {
            if ($(this).is(':visible')) {
                if ($(this).hasClass("active")) {
                    var value = $(this).attr("data-bind");
                    arr.push(value);
                }
            }
        });
        $("#divInstall a").each(function () {
            if ($(this).is(':visible')) {
                if ($(this).hasClass("active")) {
                    var value = $(this).attr("data-bind");
                    arr.push(value);
                }
            }
        });
        $("#divOther a").each(function () {
            if ($(this).is(':visible')) {
                if ($(this).hasClass("active")) {
                    var value = $(this).attr("data-bind");
                    arr.push(value);
                }
            }
        });
        return arr;
    }
    $scope.InitializeUsers=function()
    {
        var PersonIds ;
        if ($scope.Userslist != undefined)
        {
            var userId='';
            if ($scope.Userslist.length > 0)
            {
                for (var idx = 0; idx <= $scope.Userslist.length - 1; idx++) {
                    if ($scope.Userslist[idx].IsDisabled != undefined)
                    {
                        if ($scope.Userslist[idx].IsDisabled == false)
                        {
                            if ($scope.Userslist[idx].PersonId != undefined) {
                                userId = $scope.Userslist[idx].PersonId;
                            }
                            if (PersonIds == undefined || PersonIds == null) {
                                PersonIds = userId;
                            }
                            else {
                                PersonIds = PersonIds + "," + userId;
                            }
                        }
                    }
                     
                }
            }
        }
        return PersonIds;
    }
    $scope.KendoSchedulerIntitilize = function () {
        var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
        var PersonIds = HFCService.CurrentUser.PersonId;
        var start = utc;
        var end = utc;
        var filterurl = '/CalTasks/ReadSelection/?LeadId=0&PersonIds=' + PersonIds + '&start=' + start + '&end=' + end;
        var filrerdata = $scope.getSelectedUsers();
        if (filrerdata.length == 0 || filrerdata == null)
        {
            filrerdata = $scope.InitializeUsers();
            if(filrerdata==undefined)
            {
                return;
            }
        }
        
        ResetScheduler();
        $("#Savedscheduler").kendoScheduler({
          //  date: datefilter,
            //startTime: datefilter,
            height: 600,
            showWorkHours: true,
            // eventTemplate: $("#event-template").html(),
            dataBound: function (e) {
                //alert(e);
                var view = this.view();
                var events = this.dataSource.view();
                var eventElement;
                var event;
                var ViewName = $("#Savedscheduler").data("kendoScheduler")._selectedViewName;
                for (var idx = 0, length = events.length; idx < length; idx++) {
                    event = events[idx];

                    //c6af4a5d-ac72-47b4-becb-d5d57d854061
                    //a51de880-e63f-4e8a-ae39-d635398ff268

                    //get event element
                    eventElement = view.element.find("[data-uid=" + event.uid + "]");

                    //set the backgroud of the element
                    eventElement.css("border-color", event.UserRoleColor);
                    eventElement.css("background-color", event.AppointmentTypeColor);
                    eventElement.css("border-width", "3px");
                    //Event Calendar View Settings
                    if (event.eventType == 2) {
                        // eventElement.css("height", "40px");
                        $scope.TextToDisplayInView(event.TimeSlot, ViewName, eventElement, event);
                    }
                    if (event.recurrenceRule != null) {
                        var recurenceItems = JSLINQ(e.sender._data)
                                                     .Where(function (item) { return item.recurrenceId == event.recurrenceId });
                        if (recurenceItems != null) {
                            if (recurenceItems.items.length > 0) {
                                for (var rdx = 0; rdx <= recurenceItems.items.length - 1; rdx++) {
                                    if (recurenceItems.items[rdx].uid != null) {
                                        //var uid = e.sender._data[1].uid;
                                        eventElement = view.element.find("[data-uid=" + recurenceItems.items[rdx].uid + "]");
                                        //set the backgroud of the element
                                        eventElement.css("border-color", event.UserRoleColor);
                                        eventElement.css("background-color", event.AppointmentTypeColor);
                                        eventElement.css("border-width", "3px");
                                        //Event Calendar View Settings
                                        if (event.eventType == 2) {
                                            // eventElement.css("height", "40px");
                                            $scope.TextToDisplayInView(event.TimeSlot, ViewName, eventElement, event, true);
                                        }
                                    }

                                }
                            }
                        }

                    }

                }
            },
            editable: {
                editRecurringMode: "occurrence"
            },
            views: [
              { type: "day", selected: $scope.Dayview },
              { type: "week", selected: $scope.Weekview },
             { type: "month", selected: $scope.Monthview },
            ],
            timezone: "Etc/UTC",
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: '/CalTasks/ReadSelection?PersonIds=' + filrerdata,
                        dataType: "json",
                        data: filter
                    },
                    update: {
                        url: '/CalTasks/update',
                        dataType: "jsonp"
                    },
                    create: {
                        url: '/CalTasks/create',
                        dataType: "jsonp"
                    },
                    destroy: {
                        url: '/CalTasks/Delete',
                        dataType: "jsonp"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "create" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        if (operation === "update" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        if (operation === "destroy" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                    }
                },
                serverFiltering: true,
                schema: {
                    model: {
                        id: "taskID",
                        fields: {
                            Id: { from: "TaskID" },
                            taskID: { from: "TaskID" },
                            title: { from: "Title", defaultValue: "No title", title: "Subject", validation: { required: true } },
                            start: { type: "date", from: "Start" },
                            end: { type: "date", from: "End" },
                            startTimezone: { from: "StartTimezone" },
                            endTimezone: { from: "EndTimezone" },
                            description: { from: "Description" },
                            recurrenceId: { from: "RecurrenceID" },
                            recurrenceRule: { from: "RecurrenceRule" },
                            recurrenceException: { from: "RecurrenceException" },
                            ownerId: { from: "OwnerID" },
                            isAllDay: { type: "boolean", from: "IsAllDay" },
                            ReminderId: { from: "ReminderId" },
                            AppointmentTypeId: { from: "AppointmentType", defaultValue: 1 },
                            location: { from: "Location" },
                            IsPrivate: { from: "IsPrivate" },
                            AppointmentTypeColor: { from: "AppointmentTypeColor" },
                            UserRoleColor: { from: "UserRoleColor" },
                            IsTask: { from: "IsTask" }
                        }
                    }
                }

            },
            add: function (e) {
                AddNewEvent(e);
            },
            resources: [
                {
                    field: "ownerId",
                    title: "Assigned To",
                    dataSource: {
                        transport: {
                            read: {
                                url: 'CalTasks/GetInstallers/',
                                dataType: "json"
                            }
                        }
                    }
                },
                {
                    field: "ReminderId",
                    title: "Reminder",
                    dataSource: {
                        transport: {
                            read: {
                                url: 'CalTasks/GetReminders/',
                                dataType: "json"
                            }
                        }
                    }
                },
                    {
                        field: "AppointmentTypeId",
                        title: "Type Of Appointment",
                        dataSource: {
                            transport: {
                                read: {
                                    url: 'CalTasks/GetTypeAppointments/',
                                    dataType: "json"
                                }
                            }
                        }
                    }
            ]
        });
    }
    function DisplayDateTime(date) {
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        time = hours + ":" + minutes + " " + am_pm;
        return time;
    };
    $scope.TextToDisplayInView1 = function (timeslot, mode, eventElement, event, recurenceflag) {
        //eventElement.css("height", "40px");
        var celltext = null;
        if (mode == 'day') {
            if (timeslot <= 30) {
                var height;//small
                var starttime;
                var location;
                var subject;
                eventElement.css("height", "60");
            }
            if (timeslot > 30 && timeslot <= 60) {
                var height;//medium
                var starttime;
                var location;
                var subject;
            }
            if (timeslot > 60) {
                var height;//large
                var location;
                var Assignee;
                var OppointmentDetails;
                var starttime; var endtime;
                var subject;
            }
        }
        if (mode == 'week') {
            if (timeslot <= 30) {
                var height;//small
                var starttime;
                var location;
                var subject;
            }
            if (timeslot > 30 && timeslot <= 60) {
                var height;//medium
                var starttime;
                var location;
                var subject;
            }
            if (timeslot > 60) {
                var height;//large
                var location;
                var Assignee;
                var OppointmentDetails;
                var starttime; var endtime;
                var subject;
            }
        }
        if (mode == 'month') {
            if (timeslot <= 30) {
                var height = '40px';//small
                var starttime;
                var location;
                var subject;
                eventElement.css("height", height);
                subject = event.title;
                location = event.location;
                if (location == null) {
                    location = '';
                }
                starttime = DisplayDateTime(event.start)
                eventElement.css("height", height);

                if (recurenceflag == true) {
                    var string = '<span class="k-event-actions"><span class="k-icon k-i-refresh"></span></span><p style="font-size:12px; color:#00; margin:5px !important;">';
                }
                else {
                    var string = '<span class="k-event-actions"><p style="font-size:12px; color:#00; margin:5px !important;">';
                }
                string = string + starttime + ' ' + location + '</br>';
                string = string + subject + '</br>';
                string = string + '</p>';
                eventElement.html(string)
            }
            if (timeslot > 30 && timeslot <= 60) {
                var height = '60px';//medium
                var starttime;
                var location;
                var subject;
                subject = event.title;
                location = event.location;
                if (location == null) {
                    location = '';
                }
                starttime = DisplayDateTime(event.start)
                eventElement.css("height", height);
                if (recurenceflag == true) {
                    var string = '<span class="k-event-actions"><span class="k-icon k-i-refresh"></span></span><p style="font-size:12px; color:#00; margin:5px !important;">';
                }
                else {
                    var string = '<span class="k-event-actions"><p style="font-size:12px; color:#00; margin:5px !important;">';
                }
                string = string + starttime + ' ' + location + '</br>';
                string = string + subject + '</br>';
                string = string + '</p>';
                eventElement.html(string)
            }
            if (timeslot > 60) {
                var height = '90px';//large
                var location;
                var Assignee;
                var OppointmentDetails;
                var starttime; var endtime;
                var subject;
                subject = event.title;
                location = event.location;
                if (location == null) {
                    location = '';
                }
                if (event.opportunity != null) {
                    OppointmentDetails = event.opportunity;
                }
                else {
                    OppointmentDetails = '';
                }
                if (event.Attendee != null) {
                    Assignee = event.Attendee;
                }
                else {
                    Assignee = '';
                }
                starttime = DisplayDateTime(event.start)
                endtime = DisplayDateTime(event.end)
                eventElement.css("height", height);
                if (recurenceflag == true) {
                    var string = '<span class="k-event-actions"><span class="k-icon k-i-refresh"></span></span><p style="font-size:12px; color:#00; margin:5px !important;">';
                }
                else {
                    var string = '<span class="k-event-actions"><p style="font-size:12px; color:#00; margin:5px !important;">';
                }
                string = string + location + '</br>';
                if (Assignee != '' && Assignee != null) {
                    string = string + Assignee + '</br>';
                }
                string = string + 'Opp:' + OppointmentDetails + '</br>';
                string = string + starttime + '-' + endtime + '</br>';
                string = string + subject + '</br>';
                string = string + '</p>';
                //  eventElement.html(string)
            }
        }

    }
    $scope.TextToDisplayInView = function (timeslot, mode, eventElement, event, recurenceflag) {
        var celltext = null;
        if (timeslot <= 30) {
            var height = '40px';//small
            var starttime;
            var location;
            var subject;
            eventElement.css("height", height);
            subject = event.title;
            location = event.location;
            if (location == null) {
                location = '';
            }
            starttime = DisplayDateTime(event.start)
            eventElement.css("height", height);

            if (recurenceflag == true) {
                var string = '<span class="k-event-actions"><span class="k-icon k-i-refresh"></span></span><p style="font-size:12px; color:#00; margin:0px !important;">';
            }
            else {
                var string = '<span class="k-event-actions"><p style="font-size:12px; color:#00; margin:0px !important;">';
            }
            string = string + starttime + ' ' + location + '</br>';
            string = string + subject + '</br>';
            string = string + '</p>';
            eventElement.html(string)
        }
        if (timeslot > 30 && timeslot <= 60) {
            var height = '60px';//medium

            var starttime;
            var location;
            var subject;
            subject = event.title;
            location = event.location;
            if (location == null) {
                location = '';
            }
            starttime = DisplayDateTime(event.start)
            eventElement.css("height", height);
            if (recurenceflag == true) {
                var string = '<span class="k-event-actions"><span class="k-icon k-i-refresh"></span></span><p style="font-size:12px; color:#00; margin:0px !important;">';
            }
            else {
                var string = '<span class="k-event-actions"><p style="font-size:12px; color:#00; margin:0px !important;">';
            }
            string = string + starttime + ' ' + location + '</br>';
            string = string + subject + '</br>';
            string = string + '</p>';
            eventElement.html(string)
        }
        if (timeslot > 60) {
            var height = '90px';//large
            var location;
            var Assignee;
            var OppointmentDetails;
            var starttime; var endtime;
            var subject;
            subject = event.title;
            location = event.location;
            if (location == null) {
                location = '';
            }
            if (event.opportunity != null) {
                OppointmentDetails = event.opportunity;
            }
            else {
                OppointmentDetails = '';
            }
            if (event.Attendee != null) {
                Assignee = event.Attendee;
            }
            else {
                Assignee = '';
            }
            starttime = DisplayDateTime(event.start)
            endtime = DisplayDateTime(event.end)
            eventElement.css("height", height);
            if (recurenceflag == true) {
                var string = '<span class="k-event-actions"><span class="k-icon k-i-refresh"></span></span><p style="font-size:12px; color:#00; margin:0px !important;">';
            }
            else {
                var string = '<span class="k-event-actions"><p style="font-size:12px; color:#00; margin:0px !important;">';
            }
            string = string + location + '</br>';
            if (Assignee != '' && Assignee != null) {
                string = string + Assignee + '</br>';
            }
            string = string + 'Opp:' + OppointmentDetails + '</br>';
            string = string + starttime + '-' + endtime + '</br>';
            string = string + subject + '</br>';
            string = string + '</p>';
            eventElement.html(string);
        }
    }
    $scope.KendoSchedulerIntitilize();
    
    function ResetScheduler() {
        var scheduler = $("#Savedscheduler").data("kendoScheduler");
        if (scheduler != null) {
            scheduler.destroy();
            $("#Savedscheduler").empty();
        }

    }
    $http.get('/CalTasks/GetSavedCalendarUsers?SaveCalendarid=' + $scope.SaveCalendarId).then(function (data) {
        $scope.Userslist = data.data;
        $scope.KendoSchedulerIntitilize();
    });
    $("#calendar").kendoCalendar();

    $("#Savedscheduler").on("dblclick", '.k-event', function (e) {
        var scheduler = $("#Savedscheduler").getKendoScheduler();
        var element = $(e.target).is(".k-event") ? $(e.target) : $(e.target).closest(".k-event");
        var event = scheduler.occurrenceByUid(element.data("uid"));
        if (event.OrderId == 0) { event.OrderId = '' };
        if (event.AccountId == 0) { event.AccountId = '' };
        if (event.OpportunityId == 0) { event.OpportunityId = '' };
        if (event.IsTask == true) {
            $scope.eventview = {
                Id: event.Id,
                start: event.start,
                end: event.end,
                subject: event.title,
                location: event.location,
                phone: event.phone,
                note: event.description,
                IsPrivate: event.IsPrivate,
                taskID: event.taskID,
                ReminderId: event.ReminderId,
                IsCompleted: event.CompletedDateUtc,
                OrganizerPersonId: event.OrganizerPersonId,
                OwnerName: event.OwnerName,
                LeadId: event.LeadId,
                AccountId: event.AccountId,
                OpportunityId: event.OpportunityId,
                OrderId: event.OrderId,
                PersonId: event.PersonId,
                Attendee: event.Attendee,
                OwnerID2: event.OwnerID2,
               
            };
            var editTask = $scope.EditTask($scope.eventview.Id, $scope.eventview);
        }
        else {
            $scope.eventview = {
                Id: event.Id,
                start: event.start,
                end: event.end,
                subject: event.title,
                location: event.location,
                phone: event.phone,
                note: event.description,
                OrganizerPersonId: event.OrganizerPersonId,
                OpportunityId: event.OpportunityId,
                OpportunityName: event.OpportunityName,
                OwnerName: event.OwnerName,
                LeadId: event.LeadId,
                AccountId: event.AccountId,
                OpportunityId: event.OpportunityId,
                OrderId: event.OrderId,
                PersonId: event.PersonId,
                Attendee: event.Attendee,
                OwnerID2: event.OwnerID2 
            };
            if (event.recurrenceId > 0) {
                $scope.EditEvent('persongo', $scope.eventview.Id, $scope.eventview.OwnerID2, event.recurrenceId);
            }
            else {
                $scope.EditEvent('persongo', $scope.eventview.Id, $scope.eventview.OwnerID2);
            }

        }
        $scope.$apply();
    });
    $("#contextMenu").kendoContextMenu({
        filter: ".k-event, .k-scheduler-table td",
        target: "#Savedscheduler",
        select: function (e) {
            var target = $(e.target);

            if (target.hasClass("k-event")) {
                var scheduler = $("#Savedscheduler").getKendoScheduler();
                var event = scheduler.occurrenceByUid(target.data("uid"));
                if (event.OrderId == 0) { event.OrderId = '' };
                if (event.AccountId == 0) { event.AccountId = '' };
                if (event.OpportunityId == 0) { event.OpportunityId = '' };
                if (event.IsTask == true) {
                    $scope.eventview = {
                        Id: event.Id,
                        start: event.start,
                        end: event.end,
                        subject: event.title,
                        location: event.location,
                        phone: event.phone,
                        note: event.description,
                        IsPrivate: event.IsPrivate,
                        taskID: event.taskID,
                        ReminderId: event.ReminderId,
                        IsCompleted: event.CompletedDateUtc,
                        OrganizerPersonId: event.OrganizerPersonId,
                        OwnerName: event.OwnerName,
                        LeadId: event.LeadId,
                        AccountId: event.AccountId,
                        OpportunityId: event.OpportunityId,
                        OrderId: event.OrderId,
                        PersonId: event.PersonId,
                        Attendee: event.Attendee,
                        OwnerID2: event.OwnerID2
                    };
                    $("#calendarTaskModal").modal("show");
                }
                else {
                    if (event.opportunity == null || event.opportunity == undefined) {
                        OpportunityName = '';
                        OpportunityId = '';
                    }
                    else {
                        OpportunityId = event.opportunity.OpportunityId;
                        OpportunityName = event.opportunity.OpportunityName;
                    }
                    $scope.eventview = {
                        Id: event.Id,
                        start: event.start,
                        end: event.end,
                        subject: event.title,
                        location: event.location,
                        phone: event.phone,
                        note: event.description,
                        OrganizerPersonId: event.OrganizerPersonId,
                        OpportunityId: OpportunityId,
                        OpportunityName: OpportunityName,
                        OwnerName: event.OwnerName,
                        LeadId: event.LeadId,
                        AccountId: event.AccountId,
                        OpportunityId: event.OpportunityId,
                        OrderId: event.OrderId,
                        PersonId: event.PersonId,
                        Attendee: event.Attendee,
                        OwnerID2: event.OwnerID2
                    };
                    $("#_calendarModal_").modal("show");
                }
                $scope.$apply();
            } else {
                var slot = scheduler.slotByElement(target);

                scheduler.addEvent({
                    start: slot.startDate,
                    end: slot.endDate
                });
            }
        },
        open: function (e) {
            var menu = e.sender;
            var text = $(e.target).hasClass("k-event") ? "View" : "Add Event";
            menu.remove(".myClass");
            menu.append([{ text: text, cssClass: "myClass" }]);
        }
    });
}]);
app.directive('hfcSaveCalendarEventModal', [
             function () {
                 return {
                     restrict: 'E',
                     scope: true,
                     templateUrl: '/templates/NG/Calendar/calendar-event-modal.html',
                     replace: true
                 }
             }
]);
app.directive('hfcSaveCalendarTaskModal', [
             function () {
                 return {
                     restrict: 'E',
                     scope: true,
                     templateUrl: '/templates/NG/Calendar/calendar-task-modal.html',
                     replace: true
                 }
             }
]);
