﻿app.controller('EmailController', ['$scope', 'HFCService', 'EmailService', function ($scope, HFCService, EmailService) {
    $scope.HFCService = HFCService;
    $scope.EmailService = EmailService;

    var tags = $.map(HFCService.Users, function (usr) {
        var text = usr.FullName || usr.Email;
        if (usr.FullName)
            text += " <" + usr.Email + ">";
        return { id: usr.Email, text: text, FullName: usr.FullName }
    });
    $scope.SelectOption = {
        multiple: true,
        simple_tags: true,
        width: '100%',
        placeholder: "Recipients",
        tokenSeparators: [",", " ", ";"],
        maximumSelectionSize: 10,
        maximumInputLength: 256,
        minimumInputLength: 1,
        tags: tags,
        formatSelection: function (email) {
            return "<span title='" + email.id + "'>" + (email.FullName || email.id) + "</span>";
        },
        createSearchChoice: function (term) {
            if (HFC.Regex.EmailPattern.test(term))
                return { id: term, text: term };
            else
                return null;
        },
        query: function (query) {
            var data = { results: [] };
            $.each(tags, function () {
                if (query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0) {
                    data.results.push(this);
                }
            });
            return query.callback(data);
        }
    };
    $scope.TinyMCEOpt = {
        height: 350,
        resize: true,
        browser_spellcheck: true,
        forced_root_block: "div",
        plugins: [
            "advlist autolink lists link image hr charmap print preview anchor textcolor",
            "searchreplace visualblocks code",
            "insertdatetime table contextmenu paste"
        ],
        menu: {
            file: { title: 'File', items: 'print' },
            edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall searchreplace' },
            insert: { title: 'Insert', items: 'link image | hr' },
            view: { title: 'View', items: 'visualblocks visualaid preview' },
            format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | removeformat' },
            table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' }
        },
        toolbar: "styleselect | fontselect | fontsizeselect | forecolor backcolor bullist numlist"
    }
}])