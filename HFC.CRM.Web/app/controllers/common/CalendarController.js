﻿app.controller('CalendarController', ['$scope', 'HFCService', 'CalendarService', function ($scope, HFCService, CalendarService) {

    $scope.CalendarService = CalendarService;
    $scope.HFCService = HFCService;
    $scope.CalendarService.EditableRecurringEvent = {


        recurrancepattern: 1,//Daily/weekly/monthly/yearly,
        //Range of reaccurance
        startOn: new Date(),
        endBy: 1,//set 1 as no end date/2 as end after/ 3 as endby,
        endsAfterOccurances: 1,
        endByDate: new Date(),
        //Daily/weekly/monthly/yearly
        occurs: 1,//it can be daily/weekly/monthly/yearly dependson recurrancepattern
        days: { Sunday: false, Monday: false, Tuesday: false, Wednesday: false, Thursday: false, Friday: false, Saturday: false },//1 through 7 for Sunday thorugh saturday
        //monthly specific
        flagMonthlyDate: 1,
        monthlyDate: 1,
        monthlyFlagDay: false,
        //for both Month and Year
        weekOfTheMonth: 0,
        dayoftheMonth: 1,//First to 5 days 
        //for both Month and Year
        dayName: 1,
        //only for year
        yearlyMontRepeatsOn: 1,
        DayOfWeek: 1,
        flagYearly: 1,
        flagMonthly: 1



    };

    // This is cuasing a run-time error, so this was commented out -- murugan Nov 15, 2018.
    //$scope.$watch('CalendarService.EditableEvent.start', function (newVal, oldVal) {
    //    if (new Date(newVal)) {
    //        if (new Date($scope.CalendarService.EditableEvent.end) < new Date(newVal)) {
    //            $scope.CalendarService.EditableEvent.end = new Date(JSON.parse(JSON.stringify(newVal)));
    //            var dateStart = new Date(newVal);
    //            $scope.CalendarService.EditableEvent.end = new Date(dateStart.setHours(dateStart.getHours() + 1));
    //        }
    //    }
    //});

    //$scope.$watch('CalendarService.EditableEvent.end', function (newVal, oldVal) {
    //    if (newVal) {
    //        if ($scope.CalendarService.EditableEvent.start > newVal) {
    //            $scope.CalendarService.EditableEvent.start = new Date(JSON.parse(JSON.stringify(newVal)));
    //        }
    //    }
    //});

    
    //$scope.$watch('CalendarService.EditableEvent.AssignedPersonId', function (newVal, oldVal, $scope) {
    //    if (oldVal != undefined) {
    //        for (var i = 0; i < $scope.HFCService.Users.length; i++) {
    //            if ($scope.HFCService.Users[i].PersonId == oldVal) {
    //                var index = $scope.CalendarService.EditableEvent.AdditionalPeople.indexOf($scope.HFCService.Users[i].Email);
    //                if (index != -1) {
    //                    $scope.CalendarService.EditableEvent.AdditionalPeople.splice(index, 1);
    //                }
    //            }
    //        }

    //        for (var i = 0; i < $scope.HFCService.Users.length; i++) {
    //            if ($scope.HFCService.Users[i].PersonId == newVal) {
    //                var index = $scope.CalendarService.EditableEvent.AdditionalPeople.indexOf($scope.HFCService.Users[i].Email);
    //                if (index == -1) {
    //                    $scope.CalendarService.EditableEvent.AdditionalPeople.push($scope.HFCService.Users[i].Email);
    //                }
    //            }
    //        }
    //    }
    //});
    
    //$scope.$watch('CalendarService.EditableEvent.AdditionalPeople', function (newVal, oldVal, $scope) {
        
    //    if (oldVal != undefined) {
    //        for (var i = 0; i < $scope.HFCService.Users.length; i++) {
    //            if ($scope.HFCService.Users[i].PersonId == $scope.CalendarService.EditableEvent.AssignedPersonId) {
    //                var index = $scope.CalendarService.EditableEvent.AdditionalPeople.indexOf($scope.HFCService.Users[i].Email);
    //                if (index == -1) {
    //                    $scope.CalendarService.EditableEvent.AdditionalPeople.push($scope.HFCService.Users[i].Email);
    //                }
    //            }
    //        }
    //    }
    //});

    var tags = $.map(HFCService.Users, function (usr) {
        var text = usr.FullName || usr.Email;
        if (usr.FullName)
            text += " <" + usr.Email + ">";
        return { id: usr.Email, text: text, FullName: usr.FullName }
    });
    $scope.SelectOption = {
        multiple: true,
        simple_tags: true,
        width: '100%',
        placeholder: "Attendees",
        tokenSeparators: [",", " ", ";"],
        maximumSelectionSize: 10,
        maximumInputLength: 256,
        minimumInputLength: 1,
        tags: tags,
        formatSelection: function (email) {
            return "<span title='" + email.id + "'>" + (email.FullName || email.id) + "</span>";
        },
        createSearchChoice: function (term) {
            if (HFC.Regex.EmailPattern.test(term))
                return { id: term, text: term };
            else
                return null;
        },
        query: function (query) {
            var data = { results: [] };
            $.each(tags, function () {
                if (query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0) {
                    data.results.push(this);
                }
            });
            return query.callback(data);
        }
    };





}])
