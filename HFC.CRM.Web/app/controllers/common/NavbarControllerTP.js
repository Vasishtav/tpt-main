﻿app.controller('NavbarControllerTP', [
        '$http', '$scope', '$window', '$location', '$rootScope', 'saveCriteriaService'
        , 'sourceService', '$routeParams', 'saveSearchService', 'NavbarService', 'EmailQuoteService', 'HFCService', 'calendarModalService'
        , function ($http, $scope, $window, $location, $rootScope
            , saveCriteriaService, sourceService, $routeParams
            , saveSearchService, NavbarService, EmailQuoteService, HFCService, calendarModalService)
        {
         
            $scope.calendarModalService = calendarModalService;
           
            if (window.location.href.includes('/news/')) {
                $("#navv").css({ "display": "none" });
                $("#navv_side").css({ "display": "none" });
                $("#showUserProfile").css({ "display": "none" })

            } else { $("#navv").css({ "display": "block" }); $("#navv_side").css({ "display": "block" }); $("#showUserProfile").css({ "display": "block" }); }
           // $("#navv1").css({ "display": "block" });
            $scope.saveSearchService = saveSearchService;
          
            $scope.NavbarService = NavbarService;
            $scope.NavbarService.showicon = false;
            $scope.Notifications = "Azure Stephenson C";
            $http.get('/api/search/' + 1 + '/GetFranchiseNotifications')
             .then(function (data) {
                 $scope.Notifications = data.data;
                });  
            //search
            $scope.$root.globaltype = 'All';
            $scope.$root.globalvalue = '';

            $scope.OnChange = function () {
                $scope.$root.globalvalue = '';
            }
            $scope.Search = function () {
                var count = $scope.$root.globalvalue.length;

                if (count<2) {
                    HFC.DisplayAlert("Please enter at least 2 numbers/characters to search");
                }                    
                else {
                    if ($location.path().contains('globalSearch')) {
                        //$scope.globalvalue = $scope.$root.globalvalue.replace(/#/g, '%23');
                        $scope.globalvalue = encodeURIComponent($scope.$root.globalvalue);
                        $scope.globalvalue = encodeURIComponent($scope.globalvalue);
                        history.pushState({}, null, '#!/globalSearch/' + $scope.globaltype + '/' + $scope.globalvalue)
                        $scope.Refresh();
                    }
                    else {
                        $scope.$root.globaltypecopy = $scope.$root.globaltype;
                        $scope.$root.globalvaluecopy = $scope.$root.globalvalue;
                        $scope.$root.globaltype = 'All';
                        $scope.$root.globalvalue = '';
                        //localStorage.setItem("globalSearchtype", $scope.$root.globaltypecopy);
                        //localStorage.setItem("globalSearchvalue", $scope.$root.globalvaluecopy);
                        // $scope.globalvaluecopy = $scope.$root.globalvaluecopy.replace(/#/g, '%23');
                        $scope.globalvaluecopy = encodeURIComponent($scope.$root.globalvaluecopy);
                        $scope.globalvaluecopy = encodeURIComponent($scope.globalvaluecopy);
                        window.open('#!/globalSearch/' + $scope.globaltypecopy + '/' + $scope.globalvaluecopy);
                        // window.open('#!/globalSearch');
                    }
                }
            }

            $scope.myFunct = function (keyEvent) {
                if (keyEvent.which === 13)
                    $scope.Search();
                if (keyEvent.which === 37)
                    keyEvent.preventDefault();
            }
            //$(document).ready(function () {
            //    $('#globalsearchinput').bind("paste", function (e) {
            //        e.preventDefault();
            //    });
            //});
            $('input').on('paste', function () {
                var $el = $(this);
                setTimeout(function () {
                    $el.val(function (i, val) {
                        return val.replace(/%/g, '')
                    })
                })
            });
            $scope.Refresh = function () {
                //localStorage.setItem("globalSearchtype", $scope.globaltype);
                //localStorage.setItem("globalSearchvalue", $scope.globalvalue);
              //  $rootScope.$broadcast("RefreshGrids", $scope.globaltype, $scope.globalvalue);
            }

            $scope.NewSearch = function () {
                //localStorage.clear();
                window.open('#!/globalSearch');

            }
        }
])
    .service('NavbarService', [
        '$http', '$rootScope', 'PersonService', 'HFCService', 'NoteServiceTP', 'PrintService', 'EmailQuoteService', 'HFCService', function ($http, $rootScope, PersonService, HFCService, NoteServiceTP, PrintService, EmailQuoteService, HFCService) {
            
            var srv = this;
            srv.hidemenus = true;
            srv.HomeSelected = "selected";
            srv.Permission = {};
            srv.showicon;
            srv.HeaderPermission = {};
            getHeaderPermission();

            function DeslectAll() {
                srv.HomeSelected = "";
                srv.SalesSelected = "";
                srv.CalendarSelected = "";
                srv.OperationSelected = "";
                srv.ReportsSelected = "";
                srv.MarketingSelected = "";
                srv.SettingsSelected = "";
                //srv.EmailQuoteService = EmailQuoteService;
               
            }

            $.ajax({
                url: '/api/Color/0/Headercolor',
                async: false,
                success: function (data) {
                    srv.headercolor = data;
                }
            });
            srv.SelectHome = function () {
                
                DeslectAll();
                srv.HomeSelected = "selected";
            }

            srv.SelectSales = function () {
                DeslectAll();
                srv.SalesSelected = "selected";
                
            }

            srv.SelectCalendar = function () {
                DeslectAll();
                srv.CalendarSelected = "selected";
            }

            srv.SelectOperation = function () {
                DeslectAll();
                srv.OperationSelected = "selected";
            }

            srv.SelectReports = function () {
                DeslectAll();
                srv.ReportsSelected = "selected";
            }

            srv.SelectMarketing = function () {
                DeslectAll();
                srv.MarketingSelected = "selected";
            }

            srv.SelectSettings = function () {
                DeslectAll();
                srv.SettingsSelected = "selected";
            }
            
            srv.ReportDatas = "";
            // Refining Top Level Navigation 

            function DeselectAllTop() {
                
                srv.SalesLeads = "";
                srv.SalesAccounts = "";
                srv.SalesOpportunities = "";
                srv.SalesQuotes = "";
                srv.SalesOrders = "";
                srv.OperartionsPurchaseOrders = "";
                srv.OperationsProcurementDashboard = "";
                srv.OperartionsShippingNoticeDashboard = "";
                srv.OperartionsVendorPPVList = "";
                srv.OperartionsCaseManagement = "";
                srv.OperartionsVendorCaseManagement = "";
                srv.ReportsSalesAgent = "";
                srv.ReportsFranchisePerformance = "";
                srv.ReportsMarketing = "";
                srv.ReportsMonthlyStatistics = "";
                srv.ReportsRAW = "";
                srv.MarketingGeneral = "";
                srv.SettingsGeneral = "";
                srv.SettingsOperations = "";
                srv.SettingsSecurity = "";
                srv.SettingsMigration = "";
                srv.OperartionsBatchProcessing = "";
                srv.OperartionsVendorManagement = "";
                srv.hidemenus = false;
            }
           

            function getHeaderPermission() {
                
                var promiseHeader = new Promise(function (resolve, reject) {
                    var AllPermission = HFCService.getPermissionn();
                    resolve(AllPermission);

                });

                promiseHeader.then(function (value) {
                   var Leadpermission = value.find(x => x.ModuleCode == 'Lead');
                    srv.HeaderPermission.addLead = Leadpermission.CanUpdate;
                    var Calenderpermission = value.find(x => x.ModuleCode == 'Calendar');
                    srv.HeaderPermission.addCalender = Calenderpermission.CanUpdate; // EmailSettings
                    var Emailpermission = value.find(x => x.ModuleCode == 'EmailSettings');
                    srv.HeaderPermission.addEmail = Emailpermission.CanUpdate;


                });
            }

            function getPermission() {
                
                var Opportunitypermission = [];
                var promise2 = new Promise(function (resolve, reject) {
                    var rrrr = HFCService.getPermissionn();
                    resolve(rrrr);
                   
                });

                promise2.then(function (value) {
                    Opportunitypermission = value.find(x=>x.ModuleCode == 'Opportunity');
                    srv.Permission.AddOpportunity = Opportunitypermission.CanCreate;
                    srv.Permission.EditOpportunity = Opportunitypermission.CanUpdate;
                    srv.Permission.DisplayOpportunity = Opportunitypermission.CanRead; 
                    srv.Permission.DisplayMeasurements = Opportunitypermission.CanRead;
                    srv.Permission.DisplayAccountInfo = Opportunitypermission.CanRead; 
                    srv.Permission.DisplayOrderInfo = Opportunitypermission.CanRead;


                });


                var Quotepermission = [];
                var promise3 = new Promise(function (resolve, reject) {
                    var rrr = HFCService.getPermissionn();
                    resolve(rrr);
                   // resolve(rrr.find(x=>x.ModuleCode == 'Quote'));
                });

                promise3.then(function (value) {
                    Quotepermission = value.find(x=>x.ModuleCode == 'Quote');
                    srv.Permission.ListQuotes = Quotepermission.CanRead;
                    srv.Permission.AddQuotes = Quotepermission.CanCreate;
                });


                //var Opportunitypermission = [];
                //var promise2 = new Promise(function (resolve, reject) {
                //    resolve(HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity'));
                //});

                //promise2.then(function (value) {
                //    Opportunitypermission = value;
                //    srv.Permission.AddOpportunity = Opportunitypermission.CanCreate;
                //    srv.Permission.EditOpportunity = Opportunitypermission.CanUpdate;
                //    srv.Permission.DisplayOpportunity = Opportunitypermission.CanRead; 
                //    srv.Permission.DisplayMeasurements = Opportunitypermission.CanRead;
                //    srv.Permission.DisplayAccountInfo = Opportunitypermission.CanRead; 
                //    srv.Permission.DisplayOrderInfo = Opportunitypermission.CanRead;                
                //});


                //var Quotepermission = [];
                //var promise3 = new Promise(function (resolve, reject) {
                //    resolve(HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Quote'));
                //});

                //promise3.then(function (value) {
                //    Quotepermission = value;
                //    srv.Permission.ListQuotes = Quotepermission.CanRead;
                //    srv.Permission.AddQuotes = Quotepermission.CanCreate;
                //});



                //var Leadpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Lead')
                //var Opportunitypermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity')
                //var Quotepermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Quote')


                    HFCService.GetUrl();
            }

          

            // dashboard
            srv.EnableDashboardTab = function () {
                DeselectAllTop();
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }


            //Sales
            srv.EnableSalesLeadsTab = function () {
                
                DeselectAllTop();
                srv.SalesLeads = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                if (srv.HFCService.PathId != null && srv.HFCService.PathId != "" && srv.HFCService.PathId != undefined)
                    srv.NoteServiceTP.LeadId = srv.HFCService.PathId;
                else
                    srv.NoteServiceTP.LeadId = 0;

                srv.NoteServiceTP.ParentName = "Lead";               

                // Fix for TP-1588	Attachments - duplicating
                srv.NoteServiceTP.FranchiseSettings = false;

                var Leadpermission = [];
                var promise3 = new Promise(function (resolve, reject) {
                    resolve(HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Lead'));
                });

                promise3.then(function (value) {
                    Leadpermission = value;
                    srv.NoteServiceTP.showAddNotes = Leadpermission.CanCreate; //HFCService.GetPermissionTP('Add Notes').CanAccess;
                    srv.NoteServiceTP.showEditNotes = Leadpermission.CanUpdate
                });
            
                srv.NoteServiceTP.Initialize();
                srv.PersonService = PersonService;
            }


            srv.EnableSalesAccountsTab = function () {
              //  
                HFCService.GetUrl();
                if (HFCService.CurrentURL[1] != 'leadConvert') {
                    DeselectAllTop();
                    srv.SalesAccounts = "active";
                    getPermission();
                   

                    srv.HFCService = HFCService;
                    srv.NoteServiceTP = NoteServiceTP;
                    srv.PersonService = PersonService;
                }


            }

            srv.EnableSalesOpportunitiesTab = function () {
                DeselectAllTop();
                srv.SalesOpportunities = "active";
                getPermission();


                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableSalesQutoesTab = function () {
                DeselectAllTop();
                srv.SalesQuotes = "active";
                getPermission();


                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableSalesOrdersTab = function () {
                DeselectAllTop();               
                srv.SalesOrders = "active";
                getPermission();


                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            // calendar

            // Operations
            srv.EnableOperartionsPurchaseOrdersTab = function () {
                DeselectAllTop();
                srv.OperartionsPurchaseOrders = "active";
                getPermission();
                
                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableOperationsProcurementDashboardTab = function () {
                DeselectAllTop();
                srv.OperationsProcurementDashboard = "active";
                srv.OperartionsPurchaseOrders="active"
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableOperartionsShippingNoticeDashboardTab = function () {
                DeselectAllTop();
                srv.OperartionsShippingNoticeDashboard = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableOperartionsVendorPPVList = function () {
                DeselectAllTop();
                srv.OperartionsVendorPPVList = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableCaseMangementTab = function () {
                DeselectAllTop();
                srv.OperartionsCaseManagement = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableVenorCaseMangementTab = function () {
                DeselectAllTop();
                srv.OperartionsVendorCaseManagement = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }

            srv.EnableBulkPurchaseTab = function () {
                DeselectAllTop();
                srv.OperartionsBatchProcessing = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }

            srv.EnableVendorManagementTab = function () {
                DeselectAllTop();
                srv.OperartionsVendorManagement = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }

            // reports
            srv.EnableReportsSalesAgentTab = function () {
                DeselectAllTop();
                srv.ReportsSalesAgent = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            //ReportsFranchisePerformance
            srv.EnableReportsFranchisePerformanceTab = function () {
                DeselectAllTop();
                srv.ReportsFranchisePerformance = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableReportsMarketingTab = function () {
                DeselectAllTop();
                srv.ReportsMarketing = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableReportsMonthlyStatisticsTab = function () {
                DeselectAllTop();
                srv.ReportsMonthlyStatistics = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableReportsRAWTab = function () {
                DeselectAllTop();
                srv.ReportsRAW = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }

            // marketing
            srv.EnableMarketingGeneralTab = function () {
                DeselectAllTop();
                srv.MarketingGeneral = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }

            // settings
            srv.EnableSettingsMigration = function () {
                DeselectAllTop();
                srv.SettingsMigration = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableSettingsGeneralTab = function () {
                DeselectAllTop();
                srv.SettingsGeneral = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableSettingsOperationsTab = function () {
                DeselectAllTop();
                srv.SettingsOperations = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }
            srv.EnableSettingsSecurityTab = function () {
                DeselectAllTop();
                srv.SettingsSecurity = "active";
                getPermission();

                srv.HFCService = HFCService;
                srv.NoteServiceTP = NoteServiceTP;
                srv.PersonService = PersonService;
            }

            function DeslectPrint() {
                srv.leadIdPrint = "";
                srv.accountIdPrint = "";
                srv.orderIdPrint = "";
                srv.opportunityIdPrint = "";
                srv.AssociatedWith = "";
                srv.quoteIdPrint = "";
                srv.pem = "";
                srv.packettype = "";
            }


            srv.printControl = {};

            srv.printorder = function (pathidd) {
                
                srv.orderIdPrint = pathidd;
                srv.AssociatedWith = "Order";
                srv.pem = "false";
             //   srv.packettype = "InstallationPacket";

                srv.printControl.showPrintModal("InstallationPacket");
               // EmailQuoteService.printControl.showPrintModal('SalesPacket');
            }

            srv.printquotepreview = function (QuoteKey) {
                srv.quoteIdPrint = QuoteKey;
                srv.opportunityIdPrint = srv.HFCService.PathId;
                
                srv.AssociatedWith = "Opportunity";
                srv.pem = "true";
               // srv.packettype = "SalesPacket";

                srv.printControl.showPrintModal("SalesPacket");
               
            };
            srv.emailQuote = function () {
                
                DeslectPrint();
                if (srv.HFCService.PathId && srv.HFCService.CurrentURL[1] != 'myVendorConfigurator' && srv.HFCService.CurrentURL[1] != 'configurator' && srv.HFCService.CurrentURL[1] != 'leadConvert' && srv.HFCService.CurrentURL[1] != 'moveQuote') {
                    if (srv.SalesLeads == "active" || srv.SalesOpportunities == "active" || srv.SalesOrders == "active") {
                        
                        if (srv.SalesLeads == "active") {
                            srv.leadIdPrint = srv.HFCService.PathId;
                            srv.AssociatedWith = "Lead";
                            srv.pem = "false";
                            // srv.packettype = "SalesPacket";
                            
                           // srv.qqqqqq
                            srv.printControl.showPrintModaltt('SalesPacket');
                          
                          //  EmailQuoteService.confirmDialog();
                        }
                        if (srv.SalesAccounts == "active") {
                            srv.accountIdPrint = srv.HFCService.PathId;
                            srv.AssociatedWith = "Account";
                            srv.pem = "true";
                            // srv.packettype = "SalesPacket";
                            srv.printControl.showPrintModaltt('SalesPacket');
                           // EmailQuoteService.confirmDialog();
                        }
                        if (srv.SalesOpportunities == "active") {

                            if (srv.HFCService.CurrentURL.length > 3 && srv.HFCService.PathType == 'quote') {
                                srv.quoteIdPrint = srv.HFCService.CurrentURL[3];
                                srv.opportunityIdPrint = srv.HFCService.PathId;
                                srv.AssociatedWith = "Opportunity";
                                srv.pem = "true";
                                // srv.packettype = "SalesPacket";
                                 srv.printControl.showPrintModaltt('SalesPacket');
                               // EmailQuoteService.confirmDialog();

                            } else {
                                srv.opportunityIdPrint = srv.HFCService.PathId;
                                srv.AssociatedWith = "Opportunity";
                                srv.pem = "true";
                                // srv.packettype = "SalesPacket";
                                srv.printControl.showPrintModaltt('SalesPacket');
                               // EmailQuoteService.confirmDialog();
                            }

                        }
                        if (srv.SalesOrders == "active") {
                            srv.orderIdPrint = srv.HFCService.PathId;
                            srv.AssociatedWith = "Order";
                            srv.pem = "false";
                            // srv.packettype = "InstallationPacket";
                           
                            srv.printControl.showPrintModaltt('InstallationPacket');
                           // EmailQuoteService.confirmDialog();

                        }

                    }
                    else {
                        alert("Please view a specific item to Email (Lead, Opportunity, Quote, Order)");
                    }
                }
              
                else {
                    alert("Please view a specific item to Email (Lead, Opportunity, Quote, Order)")
                }
                //EmailQuoteService.confirmDialog();
            } 

            //separate method to trigger for email in reports.
            srv.emailReport = function () {
                
                //check condition for msr report
            if (srv.HFCService.PathId == undefined && srv.HFCService.CurrentURL[1] == 'MsrReport') {
                    if (srv.ReportsSalesAgent == "active") {
                        srv.AssociatedWith = "Report";
                        //srv.printControl.showPrintModaltt('');
                        if (srv.ReportDatas)
                            srv.printControl.showReportPrintModaltt(srv.ReportDatas); //Make call for the mail
                    }
                    else {
                        alert("Please view a specific item to Email (Lead, Opportunity, Quote, Order)")
                    }
                }
            }

            srv.printSalesPacket = function () {
                
                DeslectPrint();
                
                if (srv.HFCService.PathId && srv.HFCService.CurrentURL[1] != 'myVendorConfigurator' && srv.HFCService.CurrentURL[1] != 'configurator' && srv.HFCService.CurrentURL[1] != 'leadConvert' && srv.HFCService.CurrentURL[1] != 'moveQuote') {
                    if (srv.SalesLeads == "active" || srv.SalesOpportunities == "active" || srv.SalesOrders == "active" || srv.OperartionsPurchaseOrders == "active" || srv.OperartionsVendorPPVList == "active" || srv.OperartionsShippingNoticeDashboard == "active") {
                        
                        if (srv.SalesLeads == "active") {
                            srv.leadIdPrint = srv.HFCService.PathId;
                            srv.AssociatedWith = "Lead";
                            srv.pem = "false";
                            // srv.packettype = "SalesPacket";                            
                            srv.printControl.showPrintModal('SalesPacket');
                        }
                        if (srv.SalesAccounts == "active") {
                            srv.accountIdPrint = srv.HFCService.PathId;
                            srv.AssociatedWith = "Account";
                            srv.pem = "true";
                          // srv.packettype = "SalesPacket";
                            srv.printControl.showPrintModal('SalesPacket');
                        }
                        if (srv.SalesOpportunities == "active") {
                            
                            if (srv.HFCService.CurrentURL.length > 3 && srv.HFCService.PathType == 'quote') {
                                srv.quoteIdPrint = srv.HFCService.CurrentURL[3];
                                srv.opportunityIdPrint = srv.HFCService.PathId;
                                srv.AssociatedWith = "Quote";
                                srv.pem = "true";
                               // srv.packettype = "SalesPacket";
                                srv.printControl.showPrintModal('SalesPacket');

                            } else {
                                srv.opportunityIdPrint = srv.HFCService.PathId;
                                srv.AssociatedWith = "Opportunity";
                                srv.pem = "true";
                               // srv.packettype = "SalesPacket";
                                srv.printControl.showPrintModal('SalesPacket');
                            }

                        }
                        if (srv.SalesOrders == "active") {
                            srv.orderIdPrint = srv.HFCService.PathId;
                            srv.AssociatedWith = "Order";
                            srv.pem = "false";
                           // srv.packettype = "InstallationPacket";
                            srv.printControl.showPrintModal('InstallationPacket');

                        }

                        if (srv.OperartionsPurchaseOrders == "active") {
                                PrintService.PrintMPO(srv.HFCService.PathId);
                        }

                        // Todo Get Id from HFCService.PathId
                        if (srv.OperartionsVendorPPVList == "active") {
                            if (window.location.href.toUpperCase().includes('VENDORINVOICE') || window.location.href.toUpperCase().includes('REVISEDINVOICE')) {
                                var url = window.location.hash;
                                var id = url.substring(url.lastIndexOf('/') + 1);
                                PrintService.PrintInvoice(id);
                            }
                            else
                                PrintService.PrintMPO(srv.HFCService.PathId);
                        }
                        if (srv.OperartionsShippingNoticeDashboard == "active" && srv.HFCService.CurrentBrand == 1)
                            PrintService.PrintShipnotice(srv.HFCService.PathId);
                        else if (srv.OperartionsShippingNoticeDashboard == "active" && srv.HFCService.CurrentBrand != 1)
                            HFC.DisplayAlert("Please view a specific item to print (Lead, Opportunity, Quote, Order, MPO)");
                    }
                    else
                    {
                        $(".task_tpbut").blur();
                        HFC.DisplayAlert("Please view a specific item to print (Lead, Opportunity, Quote, Order, MPO)");
                    }
                }
                else {
                    $(".task_tpbut").blur();
                    HFC.DisplayAlert("Please view a specific item to print (Lead, Opportunity, Quote, Order, MPO)");
                } 
            }

            srv.printLead = function (printId) {
                if (srv.SalesLeads == "active") {
                    srv.leadIdPrint = printId;
                    srv.AssociatedWith = "Lead";
                    srv.pem = "false";
                 //   srv.packettype = "SalesPacket";

                    srv.printControl.showPrintModal('SalesPacket');
                }
            }


            srv.addTask = function () {
                
                if (srv.SalesLeads == 'active' && HFCService.PathId != null && HFCService.PathId != "" && HFCService.PathId != undefined)
                    PersonService.AddLeadAppointment('persongo', HFCService.PathId, 'task')
                else if (srv.SalesAccounts == 'active' && HFCService.PathId != null && HFCService.PathId != "" && HFCService.PathId != undefined)
                    PersonService.AddAccountAppointment('persongo', HFCService.PathId, 'task')
                else if (srv.SalesOpportunities == 'active' && HFCService.PathId != null && HFCService.PathId != "" && HFCService.PathId != undefined)
                     PersonService.AddOpportunityAppointment('persongo',HFCService.PathId,'task')
                else if (srv.SalesOrders == 'active' && HFCService.PathId != null && HFCService.PathId != "" && HFCService.PathId != undefined)
                    PersonService.AddOrderAppointment('persongo', HFCService.PathId, 'task')
                else if (srv.OperartionsCaseManagement == 'active' && HFCService.PathId != null && HFCService.PathId != "" && HFCService.PathId != undefined)
                    PersonService.AddCaseAppointment('persongo', HFCService.PathId, 'task')
                else if (srv.OperartionsVendorCaseManagement == 'active' && HFCService.PathId != null && HFCService.PathId != "" && HFCService.PathId != undefined)
                    PersonService.AddVendorCaseAppointment('persongo', HFCService.PathId, 'task')
                 else 
                     PersonService.CalendarService.CreateApt('newtask', null, null, null, null, null, null, null,null, 'task');
                
            }
        }
    ]);