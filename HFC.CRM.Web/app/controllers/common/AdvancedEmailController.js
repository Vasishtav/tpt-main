﻿// TODO: required refacatoring.

app.controller('AdvancedEmailController', ['$scope', 'HFCService', 'AdvancedEmailService', function ($scope, HFCService, AdvancedEmailService) {
    $scope.HFCService = HFCService;
    $scope.AdvancedEmailService = AdvancedEmailService;
    var tags = $.map(HFCService.Users, function (usr) {
        var text = usr.FullName || usr.Email;
        if (usr.FullName)
            text += " <" + usr.Email + ">";
        return { id: usr.Email, text: text, FullName: usr.FullName }
    });

    $scope.AddressesSelectOption = {
        multiple: true,
        simple_tags: true,
        width: '100%',
        placeholder: "",
        tokenSeparators: [",", " ", ";"],
        maximumSelectionSize: 10,
        maximumInputLength: 256,
        minimumInputLength: 1,
        tags: tags,
        formatSelection: function (email) {
            return "<span title='" + email.id + "'>" + (email.FullName || email.id) + "</span>";
        },
        createSearchChoice: function (term) {
            if (HFC.Regex.EmailPattern.test(term))
                return { id: term, text: term };
            else
                return null;
        },

        query: function (query) {
            var data = { results: [] };
            $.each(tags, function () {
                if (query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0) {
                    data.results.push(this);
                }
            });
            return query.callback(data);
        }
    };

    $scope.GetSelectOptions = function () {
        return {
            multiple: true,
            width: '100%',
            placeholder: "Files",
            maximumSelectionSize: 10,
            maximumInputLength: 256,

            id: function (file) {
                return file.PhysicalFileId;
            },

            formatResult: function (file) {
                return file.FileName;
            },

            formatSelection: function (file) {
                return file.FileName;
            },

            query: function (query) {
                var data = { results: [] };
                $.each($scope.AdvancedEmailService.Files, function () {
                    data.results.push(this);
                });
                return query.callback(data);
            }
        }
    };
}])
