﻿'use strict';
appVendor
    
    .controller('VendorcaseManagementViewController',
    [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'FranchiseService', 'NavbarServiceVendor', 'FileUploadService',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, FranchiseService, NavbarServiceVendor, FileUploadService) {

            //$scope.NavbarService = NavbarService;
            //$scope.NavbarService.SelectOperation();
            //$scope.NavbarService.EnableVenorCaseMangementTab();

            $scope.NavbarServiceVendor = NavbarServiceVendor;
            $scope.NavbarServiceVendor.SelectHomeTab();

            $scope.VendorCaseId = $routeParams.VendorCaseId;
            $scope.CaseId = $routeParams.CaseId;
            HFCService.setHeaderTitle("Vendor CaseView #"+$scope.CaseId);
            $scope.Permission = {};
            var Casepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'CaseManagement')

            $scope.Permission.ViewCase = Casepermission.CanRead;
            $scope.Permission.AddCase = Casepermission.CanCreate;
            $scope.Permission.EditCase = Casepermission.CanUpdate;

         //   $scope.CalendarService = CalendarService;
            $scope.FileUploadService = FileUploadService;
            if ($scope.VendorCaseId)
                $scope.forComments = { id: $scope.CaseId, refId: 0, module: 3 };


            // file upload
            $scope.FileUploadService.ShowFranchisetimezone(true);
            if ($scope.VendorCaseId) {
               $scope.FileUploadService.SetCaseId($scope.CaseId, 'FranchiseCase');
               
            }
            else $scope.FileUploadService.clearCaseId();





            $scope.FileUploadService.css1 = "fileupload_ltgrid";
            $scope.FileUploadService.css2 = "";
            $scope.FileUploadService.css3 = "fileupload_rtgrid";


           
            //$scope.$on('$routeChangeStart', function ($event, next, current) {
            //    $scope.CalendarService.viewvendorcase = false;
            //});


          

            HFCService.setHeaderTitle("VendorCaseView #");

           // $scope.CalendarService.viewvendorcase = true;
           // $scope.CalendarService.setVendorcasevisible()
            //view page grid
            $scope.CaseinfoGridViewOptions = {
                dataSource: {

                    transport: {
                        cache: false,
                        read: function (e) {
                            
                            if ($scope.VendorCaseId == null || $scope.VendorCaseId == "" || $scope.VendorCaseId == undefined) {
                                var caseid_val = 0;
                            }
                            else caseid_val = $scope.VendorCaseId;
                            $http.get('/api/CaseManageAddEdit/0/GetFranchiseVendorCaseValue/?caseid=' + caseid_val, e)
                            .then(function (data) {
                                 
                                e.success(data.data);
                            }).catch(function (error) {
                                HFC.DisplayAlert(error);
                            });
                        },
                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                QuoteLineNumber: { editable: false },
                                ProductName: { editable: false },
                                ProductId: { editable: false },
                                Description: { editable: false },
                                Typevalue: { editable: false },
                                CaseReason: { editable: false },
                                IssueDescription: { editable: false },
                                ExpediteReq: { type: "boolean", editable: false },
                                ExpediteApproved: { type: "boolean", editable: false },
                                ReqTripCharge: { type: "boolean", editable: false },
                                TripChargeName: { editable: false },
                                StatusName: { editable: false },
                                ResolutionName: { editable: false }
                            }
                        }
                    },
                },


                batch: true,
                cache: false,
                toolbar: [

                ],
                sortable: true,
                filterable: true,
                resizable: true,
                editable: "inline",
                detailInit: detailinitParent,

                noRecords: { template: "No records found" },
                columns: [
                     {
                         field: "Id",
                         title: "VPO-Line",
                         filterable: { multi: true, search: true },
                         hidden: true,
                     },
                    {
                        field: "QuoteLineNumber",
                        title: "VPO-Line",
                        width: "120px",
                        filterable: { multi: true, search: true },
                        hidden: false,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                        },
                    },

                      {
                          field: "ProductName",
                          title: "Product",
                          width: "200px",
                          filterable: { multi: true, search: true },
                          hidden: true,
                      },
                      {
                          field: "ProductId",
                          title: "Product No",
                          width: "200px",
                          filterable: { multi: true, search: true },
                          hidden: true,
                      },
                      {

                          field: "Description",
                          title: "Product Description",
                          width: "220px",
                          filterable: { multi: true, search: true },
                          hidden: true,
                      },
                       {

                           field: "Typevalue",
                           title: "Issue Type",
                           width: "220px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                       },
                       {

                           field: "CaseReason",
                           title: "Case Reason",
                           width: "220px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                       },

                     {
                         field: "IssueDescription",
                         title: "Issue Description",
                         width: "150px",
                         filterable: { multi: true, search: true },
                         hidden: true,


                     },
                    {
                        field: "ExpediteReq",
                        title: "Expedite Request",
                        width: "150px",
                        filterable: { multi: true, search: true },
                        template: '<input type="checkbox"  #= ExpediteReq ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                    },
                      {
                          field: "ReqTripCharge",
                          title: "Trip Charge Request",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          template: '<input type="checkbox" #= ReqTripCharge ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                      },
                       // {
                       //     field: "StatusName",
                       //     title: "Status",
                       //     width: "150px",
                       //     filterable: { multi: true, search: true },
                       //     hidden: false,
                       // },
                       //{
                       //    field: "ResolutionName",
                       //    title: "Resolution",
                       //    width: "150px",
                       //    filterable: { multi: true, search: true },
                       //    hidden: false,
                       //},
                    //  {
                    //      field: "ExpediteApproved",
                    //      title: "Expedite App",
                    //      width: "150px",
                    //      filterable: { multi: true, search: true },
                    //      template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                    //  },
                     
                    //{
                    //    field: "TripChargeName",
                    //    title: "Trip Charge App",
                    //    width: "150px",
                    //    filterable: { multi: true, search: true },
                    //    hidden: false,
                    //},
                     
                ]
            };

            function detailinitParent(e) {
                var grdId = "grd" + e.data.uid;
                $("<div id = '" + grdId + "' class='sub-grid' />").appendTo(e.detailCell).kendoGrid({
                    dataSource: [e.data],
                    columns: [
                          
                            {
                                field: "ExpediteApproved",
                                title: "Expedite Approved",
                                width: "150px",
                                filterable: { multi: true, search: true },
                                template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                            },

                    {
                        field: "TripChargeName",
                        title: "Approved Trip Charge",
                        width: "150px",
                        template: function(dataItem){
                            if (dataItem.TripChargeName === "Approved") {
                                if (dataItem.Amount) {
                                    const formatter = new Intl.NumberFormat('en-US', {
                                        style: 'currency',
                                        currency: 'USD',
                                        minimumFractionDigits: 2
                                    })

                                    return formatter.format(dataItem.Amount);
                                }
                                else
                                    return '';
                            }
                            else
                            {
                                if (dataItem.TripChargeName)
                                    return dataItem.TripChargeName;
                                else
                                    return '';
                            }
                              
                        },
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                     {
                         field: "StatusName",
                         title: "Status",
                         width: "150px",
                         filterable: { multi: true, search: true },
                         hidden: false,
                     },
                       {
                           field: "ResolutionName",
                           title: "Resolution",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                       },
                      
                        //{ field: "VendorName", title: "Vendor" },
                        //{ field: "Description", title: "Item Description", template: '#=Description#' },
                        //{ field: "SuggestedResale", title: "Suggested Resale", format: "{0:c}" },
                        //{ field: "UnitPrice", title: "Unit Price", format: "{0:c}" },
                        //{ field: "Discount", title: "Discount", template: '#=kendo.format("{0:p}", Discount / 100)#' },
                    ],
                });
            }

            //code for edit button
            $scope.EditVendorCase = function () {
                
                window.location.href = "#!/VendorcaseEdit/"+$scope.CaseId +"/" + $scope.VendorCaseId;
            }

            //
           


            //get other data details.
            if ($scope.VendorCaseId > 0) {
                
                var caseid_val = $scope.VendorCaseId;
                //var id = $scope.Id;
                $http.get('/api/CaseManageAddEdit/0/GetFranchiseVendorCase/?caseid=' + caseid_val).then(function (response) {
                     
                    if (response.data != null) {
                        $scope.VendorCaseView = response.data;
                    }
                });
            }

            $scope.RelatedCaseHistoryGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.VendorCaseId) {
                                var url = "api/CaseManageAddEdit/" + $scope.CaseId + "/GetVendorCaseHistoryForCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },
                    },

                },
                batch: true,
                cache: false,
                pageable: {
                    //  refresh: true,
                    pageSize: 10,
                    pageSizes: [10, 20, 30, 50, 100],
                    buttonCount: 5
                },
                noRecords: { template: "No case history found" },
                columns: [
                    {
                        field: "LastUpdatedOn", title: "Date",
                        template: "#= kendo.toString(kendo.parseDate(LastUpdatedOn), 'MM/dd/yyyy hh:mm tt') + ' ' + FranTimezone #"
                        //template: function (dataitem) {
                        //    var nnn = HFCService.KendoDateTimeFormat(dataitem.LastUpdatedOn)
                        //    return dataitem.LastUpdatedOn;
                           
                        //}
                            //"#= kendo.toString(kendo.parseDate(LastUpdatedOn), 'MM/dd/yyyy hh:mm tt') + ' ' + FranTimezone #"
                    },
                    {
                        field: "Field",
                        title: "Field",
                        template: function (dataitem) {
                            
                            
                                return '<div>' + dataitem.Field + '</div>'
                        }
                    },
                    { field: "UserName", title: "User" },
                    { field: "OriginalValue", title: "Original Value" },
                    { field: "NewValue", title: "New Value" }
                ],


            };





            // for task


         
            $scope.VendorRelatedTasksGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.VendorCaseId) {
                                var url = "api/calendar/" + $scope.VendorCaseId + "/GetTasksForVendorCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },
                    },

                },
                batch: true,
                cache: false,
                //pageable: {
                //    refresh: true,
                //    pageSize: 25,
                //    pageSizes: [25, 50, 100, 'All'],
                //    buttonCount: 5
                //},

                noRecords: { template: "No tasks found" },
                columns: [
                    { field: "Subject", title: "Subject" },
                    { field: "AttendeesName", title: "Attendees" },
                    {
                        field: "DueDate", title: "Due"
                        , template: function (dataitem) {
                            return HFCService.KendoDateFormat(dataitem.DueDate);
                        }
                            //"#= kendo.toString(kendo.parseDate(DueDate), 'yyyy-MM-dd') #"
                    },
                     {
                         field: "Message", title: "Note"
                     },
                     {
                         field: "",
                         title: "",
                         template: function (dataitem) {
                             return EventTaskTemplates(dataitem);
                         }//,
                         //width: "5%"
                     }
                ],


            };

            function EventTaskTemplates(dataitem) {
                var template = "";
                template += '<li><a href="javascript:void(0)" ng-click="editTask(dataItem)">Edit</a></li>';
                template += '<li ng-show="dataItem.CompletedDate == null"><a href="javascript:void(0)" ng-click="CompleteTask(\'persongo\', ' + dataitem.TaskId + ')">Complete</a></li>';
                template += '<li ng-show="dataItem.CompletedDate != null"><a href="javascript:void(0)" ng-click="InCompleteTask(\'persongo\', ' + dataitem.TaskId + ')">Incomplete</a></li>';


                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                       '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                       '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                       '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }

            $scope.taskSuccessCallback = function (param) {
                $("#MyTasksGrid11").data("kendoGrid").dataSource.read();
            }

            //$scope.editTask = function (model) {
            //    $scope.CalendarService.EditTaskFromCalendarSceduler('newtask', model, true, $scope.taskSuccessCallback);
            //}

            //$scope.CompleteTask = function (modalId, EventId) {
            //    $scope.CalendarService.CompleteTaskVendor(modalId, EventId, $scope.taskSuccessCallback)
            //}
            //$scope.InCompleteTask = function (modalId, EventId) {
            //    $scope.CalendarService.InCompleteTaskVendor(modalId, EventId, $scope.taskSuccessCallback)
            //}


            // for event
            $scope.VendorRelatedEventsGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.VendorCaseId) {
                                var url = "api/calendar/" + $scope.VendorCaseId + "/GetEventsForVendorCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },

                    },

                },
                batch: true,
                cache: false,

                noRecords: { template: "No events found" },
                columns: [
                    { field: "Subject", title: "Subject" },
                    { field: "AttendeesNames", title: "Attendees" },
                    {
                        field: "StartDate", title: "Start"
                        , template: function (dataitem) {
                            return HFCService.KendoDateTimeFormat(dataitem.StartDate);
                        }
                            //"#= kendo.toString(kendo.parseDate(StartDate), 'yyyy-MM-dd hh:mm tt') #"
                    },
                     {
                         field: "EndDate", title: "End"
                         , template: function (dataitem) {
                             return HFCService.KendoDateTimeFormat(dataitem.EndDate);
                         }
                             //"#= kendo.toString(kendo.parseDate(EndDate), 'yyyy-MM-dd hh:mm tt') #"
                     },
                     {
                         field: "",
                         title: "",
                         template: function (dataitem) {
                             return EventMenuTemplate(dataitem);
                         }
                     }
                ],


            };



            function EventMenuTemplate(dataitem) {
                var template = "";
                template += '<li><a href="javascript:void(0)" ng-click="editEvent(\'persongo\', ' + dataitem.CalendarId + ')">Edit</a></li>';

                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                       '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                       '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                       '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }


            $scope.eventSuccessCallback = function (param) {
                if ($("#MyAppointmentsGrid11").data("kendoGrid").dataSource)
                    $("#MyAppointmentsGrid11").data("kendoGrid").dataSource.read();
            }


            //$scope.editEvent = function (modalId, EventId) {
            //    $scope.CalendarService.GetEventsFromScheduler(modalId, EventId, $scope.eventSuccessCallback);
            //}



            //$scope.AddTasksForVendorCase = function () {
            //    NavbarService.addTask()
            //}


            //$scope.CalendarService.VendorCaseTaskCallback = function () {
            //    $scope.taskSuccessCallback();
            //}


            $http.get('/api/comments/' + $scope.CaseId + '/FindFranchiseTimeZone').then(function (response) {
                $scope.TimeZone = response.data;
            });


           
     }

    ])


