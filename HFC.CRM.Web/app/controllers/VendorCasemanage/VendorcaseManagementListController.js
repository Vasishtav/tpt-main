﻿'use strict';
appVendor
    
    .controller('VendorcaseManagementListController',
    [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope', '$templateCache', '$sce', '$compile', '$http', '$q', 'FranchiseService', 'NavbarServiceVendor', 'kendotooltipService'
        , 
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, FranchiseService, NavbarServiceVendor, kendotooltipService) {

            //$scope.NavbarService = NavbarService;
            //$scope.NavbarService.SelectOperation();
            //$scope.NavbarService.EnableVenorCaseMangementTab();
            $scope.NavbarServiceVendor = NavbarServiceVendor;
            $scope.NavbarServiceVendor.SelectHomeTab();
            HFCService.setHeaderTitle("Vendor Case Management #");
            $scope.Permission = {};
            var Casepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'CaseManagement')

            var offsetvalue = 0;
            $scope.Permission.ViewCase = Casepermission.CanRead;
            $scope.Permission.AddCase =  Casepermission.CanCreate;
            $scope.Permission.EditCase = Casepermission.CanUpdate;

            $scope.VLCaseTypes = [{ Id: 1, Name: 'All Cases' }, { Id: 2, Name: 'In Process Cases' }];

            $scope.SelectedType = 2;
            $scope.Closed = false;
            $scope.FranchiseSelected = 0;
           
            //$scope.GetFranchiseList();
            //set window title
            HFCService.setHeaderTitle("Case Management  #");

            $scope.Getvalue = function () {
                $scope.GridEditable = false;
                $scope.CasegridList = {
                    dataSource: {

                        transport: {
                            cache: false,
                            read: function (e) {

                                //if ($scope.CaseId == null || $scope.CaseId == "" || $scope.CaseId == undefined) {
                                //    var caseid_val = 0;
                                //}
                                //else caseid_val = $scope.CaseId;

                                //if ($scope.Id == null || $scope.Id == "" || $scope.Id == undefined) {
                                //    var id = 0;
                                //}
                                //else id = $scope.Id;

                                $http.get('/api/CaseManageAddEdit/0/GetVendorGridList/?SelectedCase=' + $scope.SelectedType + '&Closed=' + $scope.Closed + '&FranchiseId=' + $scope.FranchiseSelected)

                                    .then(function (data) {
                                        e.success(data.data);
                                    }).catch(function (error) {
                                        HFC.DisplayAlert(error);


                                    });
                            },
                        },
                        schema: {
                            model: {
                                id: "VendorCaseId",
                                fields: {
                                    CaseId: { editable: false },
                                    VendorCaseId: { type: 'number', editable: false },
                                    FranchiseName: { editable: false },
                                    VendorCaseNumber: {  editable: false },
                                    MPO: { type: 'number', editable: false },
                                    VPO: { type: 'number', editable: false },
                                    AccountName: { editable: false },
                                    IssueDescription: { editable: false },
                                    StatusName: { editable: false },
                                    TypeName: { editable: false },
                                    IncidentDate: { type: 'date', editable: false },
                                    CaseOwner: { editable: false },
                                    CreatedOn: { type: 'date', editable: false },
                                }
                            }
                        },
                    },

                    //autoSync: true,
                    batch: true,
                    cache: false,
                    toolbar: [
                        //{
                        //    template: function (headerData) {
                        //        return $("#headToolbarTemplate").html()
                        //    }
                        //}
                    ],
                    sortable: true,
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    resizable: true,
                    editable: "inline",

                    noRecords: { template: "No records found" },
                    dataBound: function (e) {
                        // auto fit column
                        //for (var i = 0; i < this.columns.length; i++) {
                        //    this.autoFitColumn(i);
                        //}
                        // end auto fit column
                        //console.log("dataBound");
                        if (this.dataSource.view().length == 0) {
                            // The Grid contains No recrods so hide the footer.
                            $('.k-pager-nav').hide();
                            $('.k-pager-numbers').hide();
                            $('.k-pager-sizes').hide();
                        } else {
                            // The Grid contains recrods so show the footer.
                            $('.k-pager-nav').show();
                            $('.k-pager-numbers').show();
                            $('.k-pager-sizes').show();
                        }
                        if (kendotooltipService.columnWidth) {
                            kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                        } else if (window.innerWidth < 1280) {
                            kendotooltipService.restrictTooltip(null);
                        }
                        $scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
                    },
                    columns: [
                        {
                            field: "CaseId",
                            title: "CaseId",
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: true,

                        },
                         {
                             field: "VendorCaseId",
                             title: "VendorCaseId",
                             width: "150px",
                             filterable: {
                                 //multi: true,
                                 search: true
                             },
                             hidden: true,
                         },
                        {
                            field: "FranchiseName",
                            title: "Franchise Name",
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.FranchiseName + "</div><span>" + dataItem.FranchiseName + "</span></span>";
                            },
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,

                        },

                          {
                              field: "VendorCaseNumber",
                              title: "Vendor Case",
                              width: "140px",
                              filterable: {
                                  //multi: true,
                                  search: true
                              },
                              template: function (dataItem) {
                                  return "<span class='Grid_Textalign'>" + $scope.temp_VendorCaseNo(dataItem) + "</span>";
                              },
                              hidden: false,
                          },
                          {
                              field: "MPO",
                              title: "MPO",
                              width: "100px",
                              template: function (dataItem) {
                                  return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.MPO + "</div><span>" + dataItem.MPO + "</span></span></span>";
                              },
                              filterable: {
                                  //multi: true,
                                  search: true
                              },
                              hidden: false,
                          },
                          {

                              field: "VPO",
                              title: "VPO",
                              template: function (dataItem) {
                                  return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.VPO + "</div><span>" + dataItem.VPO + "</span></span></span>";
                              },
                              width: "100px",
                              filterable: {
                                  //multi: true,
                                  search: true
                              },
                              hidden: false,
                          },
                           {

                               field: "AccountName",
                               title: "Account Name",
                               template: function (dataItem) {
                                   return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountName + "</div><span>" + dataItem.AccountName + "</span></span>";
                               },
                               width: "200px",
                               filterable: {
                                   //multi: true, 
                                   search: true
                               },
                               hidden: false,
                           },
                           {

                               field: "IssueDescription",
                               title: "Subject",
                               width: "150px",
                               attributes: {

                                   style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                               },
                               filterable: {
                                   //multi: true, 
                                   search: true
                               },
                               template: function (dataitem) {
                                   var template = "";
                                   // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
                                   var erer = dataitem.IssueDescription.replace('"', "");
                                   erer = erer.replace(/<[^>]*>/g, '');
                                   template += '<span title="' + erer + '">' + dataitem.IssueDescription + '</span>'

                                   return template;
                               },

                               hidden: false,
                           },

                         {
                             field: "StatusName",
                             title: "Status",
                             template: function (dataItem) {
                                 return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.StatusName + "</div><span>" + dataItem.StatusName + "</span></span>";
                             },
                             width: "150px",
                             filterable: { multi: true, search: true },
                             hidden: false,
                         },
                        {
                            field: "TypeName",
                            title: "Issue Type",
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.TypeName + "</div><span>" + dataItem.TypeName + "</span></span>";
                            },
                            width: "150px",
                            filterable: { multi: true, search: true },
                        },
                          {
                              field: "CreatedOn",
                              title: "Date/Time Opened",
                              width: "140px",
                              type: "date",
                              filterable: HFCService.gridfilterableDate("date", "VendorCaseCreatedFilterCalender", offsetvalue),
                              template: function (dataItem) {
                                  return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + ' ' + dataItem.Timezone + "</div><span>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + ' ' + dataItem.Timezone + "</span></span>";
                              },
                           
                              //template: "#= kendo.toString(kendo.parseDate(CreatedOn), 'MM/dd/yyyy h:mm tt') + ' '+Timezone #"
                          },
                           {
                               field: "CaseOwner",
                               title: "Case Owner",
                               template: function (dataItem) {
                                   return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CaseOwner + "</div><span>" + dataItem.CaseOwner + "</span></span>";
                               },
                               width: "150px",
                               filterable: {
                                   //multi: true,
                                   search: true
                               },
                               hidden: false,
                           },
                            {
                                field: "",
                                title: "",
                                width: "80px",
                                template: function (dataitem) {
                                    return CaseTemplate(dataitem);
                                    

                                }
                            },

                    ],
                    noRecords: { template: "No records found" },
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],
                        buttonCount: 5
                    },
                    resizable: true,
                    toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="Gridsearch()" type="search" id="searchBox" placeholder="Search" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 no-padding hfc_leadbut"><input type="button" id="btnReset" ng-click="GridSearchClearr()" ng-model="Closed" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div><div class="col-xs-8 col-sm-8 hfc_leadchkbox"><input class="option-input checkbox" name="CaselistClosedCancelled" style="margin-left:10px;" type="checkbox" ng-model="Closed" ng-click="Change_CheckCaseList()"><label class="ldet_label">&nbsp; Show Closed / Canceled</label></div></div></script>').html()),
                    columnResize: function (e) {
                        $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                        getUpdatedColumnList(e);
                    }
                };
                 $scope.CasegridList = kendotooltipService.setColumnWidth($scope.CasegridList);
            }

            var getUpdatedColumnList = function (e) {
                kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
            }

            function CaseTemplate(dataItem) {
                var hcontent = "";
                if ($scope.Permission.EditCase == true) {
                    hcontent = '<ul><li class ="dropdown note1 ad_user">' +
                              '<div class ="btn-group"><button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">' +
                              '<i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>' +
                              '</button><ul class ="dropdown-menu pull-right">' +
                              '<li><a href="javascript: void (0) "  ng-click="EditRowDetailsData(dataItem) ">Edit</a></li>' +
                              '</ul></div></li></ul>';
                }
                return hcontent;
            }
            //$scope.Getvalue();

            //get the current fanchise
            $http.get('/api/CaseManageAddEdit/0/GetFranchiseList').then(function (response) {
                
                if (response.data.length) {
                    
                    $scope.FranchiseDetails = response.data;
                    if ($scope.FranchiseDetails.length!=0)
                    angular.forEach($scope.FranchiseDetails, function (value, key) {
                        if (value.Valid == true) {
                            $scope.FranchiseSelected = value.FranchiseId;
                            //after getting current franchise assign franchise value & call the grid code
                            $scope.Getvalue();
                        }
                        else {
                            $scope.Getvalue();
                        }
                    });
                }
                else {
                    $scope.Getvalue();
                    }
            });

            //edit button code
            $scope.EditRowDetailsData = function (value) {
                
                var data = value;
                if (data) {
                    window.location.href = "#!/VendorcaseEdit/" + value.CaseId + "/" + value.VendorCaseId;

                   
                }
            }
            
            //code for the grid vendor Case number
            $scope.temp_VendorCaseNo = function (dataItem) {
                if (dataItem.VendorCaseNumber)

                    //return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.FranchiseName + "</div><span>" + dataItem.FranchiseName + "</span></span>";


                    return "<a href='#!/VendorcaseView/" + dataItem.CaseId + "/" + dataItem.VendorCaseId + "' style='color: rgb(61,125,139);'>" + dataItem.VendorCaseNumber + "</a>";
                else
                    return '';

            }

            //when check-box and the type of case & franchise selected in drop-down
            $scope.Change_CheckCaseList = function () {               
                var drp_val = $scope.SelectedType;
                var check_val = $scope.Closed;
                var Fra_drpval = $scope.FranchiseSelected;
                if ($scope.FranchiseSelected == null) $scope.FranchiseSelected = 0;               
                var grid = $('#CasegridLists').data("kendoGrid");
                grid.dataSource.read();                       
            }

            $scope.Gridsearch = function () {
                var searchValue = $('#searchBox').val();
                $("#CasegridLists").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "VendorCaseNumber",
                          operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          value: searchValue
                      },
                      {

                          field: "FranchiseName",
                          operator: "contains",
                          value: searchValue
                      },
                       {
                           field: "MPO",
                           operator: (value) => (value + "").indexOf(searchValue) >= 0,
                           value: searchValue
                       },
                        {
                            field: "VPO",
                            operator: (value) => (value + "").indexOf(searchValue) >= 0,
                            value: searchValue
                        },
                      {
                          field: "AccountName",
                          operator: "contains",
                          value: searchValue
                      },
                       {
                           field: "IssueDescription",
                           operator: "contains",
                           value: searchValue
                       },
                        {
                            field: "StatusName",
                            operator: "contains",
                            value: searchValue
                        },
                         {
                             field: "TypeName",
                             operator: "contains",
                             value: searchValue
                         },
                          //{
                          //    field: "IncidentDate",
                          //    operator: "contains",
                          //    value: searchValue
                          //},
                           {
                               field: "CaseOwner",
                               operator: "contains",
                               value: searchValue
                           },
                            
                    ]
                });

            }

            $scope.GridSearchClearr = function () {
                $('#searchBox').val('');
                $("#CasegridLists").data('kendoGrid').dataSource.filter({
                });
            }

            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 302);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 302);
                };
            });
            // End Kendo Resizing
           
     }

    ])


