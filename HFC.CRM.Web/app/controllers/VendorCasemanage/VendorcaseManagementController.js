﻿'use strict';
appVendor
    
    .controller('VendorcaseManagementController',
    [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'FranchiseService', 'NavbarServiceVendor', 'FileUploadService',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, FranchiseService, NavbarServiceVendor, FileUploadService) {

            //$scope.NavbarService = NavbarService;
            //$scope.NavbarService.SelectOperation();
            //$scope.NavbarService.EnableVenorCaseMangementTab();
            $scope.VendorCaseId = $routeParams.VendorCaseId;
            $scope.CaseId = $routeParams.CaseId;

            $scope.Permission = {};
            var Casepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'CaseManagement')

            $scope.Permission.ViewCase = Casepermission.CanRead;
            $scope.Permission.AddCase = Casepermission.CanCreate;
            $scope.Permission.EditCase = Casepermission.CanUpdate;

            $scope.NavbarServiceVendor = NavbarServiceVendor;
            $scope.NavbarServiceVendor.SelectHomeTab();
            $scope.amounttocurrency = 0;


            // file upload
            $scope.FileUploadService = FileUploadService;
            $scope.FileUploadService.ShowFranchisetimezone(true);
            if ($scope.CaseId) {
                $scope.FileUploadService.SetCaseId($scope.CaseId, 'FranchiseCase');
            }
            else $scope.FileUploadService.clearCaseId();

         


          $scope.FileUploadService.css1 = "col-lg-7";
          $scope.FileUploadService.css2 = "col-sm-6 no-padding";
          $scope.FileUploadService.css3 = "col-lg-5";

            //$scope.Id = $routeParams.Id;

            //if ($scope.VendorCaseId) {
            //    $scope.FileUploadService.SetVendorCaseId($scope.VendorCaseId);
            //}
            //else $scope.FileUploadService.clearCaseId();

            //if (window.location.href.includes('/VendorcaseEdit/')) {
                
            //    $scope.FileUploadService.css1 = "col-sm-7";
            //    $scope.FileUploadService.css2 = "col-sm-6 no-padding";
            //    $scope.FileUploadService.css3 = "col-sm-5";
               
            //}


            //if ($scope.VendorCaseId) {
            //    $scope.FileUploadService.SetCaseId($scope.VendorCaseId, 'VendorCase');
            //}
            //else $scope.FileUploadService.clearCaseId();


            //get other data values like case owner,date etc..
            if ($scope.VendorCaseId > 0) {
                
                var caseid_val = $scope.VendorCaseId;
                //var id = $scope.Id;
                $http.get('/api/CaseManageAddEdit/0/GetFranchiseVendorCase/?caseid=' + caseid_val).then(function (response) {
                    
                    if(response.data!=null)
                        $scope.VendorCaseListValue = response.data;
                });
            }

            $scope.kendoValidator = function (gridId) {
                return $("#" + gridId).kendoValidator({
                    validate: function (e) {
                        $("span.k-invalid-msg").hide();
                        var dropDowns = $(".k-dropdown");
                        $.each(dropDowns, function (key, value) {
                            var input = $(value).find("input.k-invalid");
                            var span = $(this).find(".k-widget.k-dropdown.k-header");
                            // 
                            if (input.size() > 0) {
                                $(this).addClass("dropdown-validation-error");
                            } else {
                                $(this).removeClass("dropdown-validation-error");
                            }
                        });
                    }
                }).getKendoValidator();
            }

            //edit grid code.
            $scope.CaseinfoGridOptions = {
                dataSource: {
                    
                    transport: {
                        cache: false,
                        read: function (e) {
                            
                            if ($scope.VendorCaseId == null || $scope.VendorCaseId == "" || $scope.VendorCaseId == undefined) {
                                var caseid_val = 0;
                            }
                            else caseid_val = $scope.VendorCaseId;

                            //if ($scope.Id == null || $scope.Id == "" || $scope.Id == undefined) {
                            //    var id = 0;
                            //}
                            //else id = $scope.Id;
                            
                            $http.get('/api/CaseManageAddEdit/0/GetFranchiseVendorCaseValue/?caseid=' + caseid_val, e)
                                .then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    HFC.DisplayAlert(error);
                                    

                                });
                        },
                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                QuoteLineNumber: { editable: false },
                                ProductName: { editable: false },
                                ProductId: { editable: false },
                                Description: { editable: false },
                                
                                Typevalue: { editable: false },
                                CaseReason: { editable: false },
                                IssueDescription: { editable: false },
                                ExpediteReq: { type: "boolean", editable: false },
                                ExpediteApproved: { type: "boolean", editable: true },
                                ReqTripCharge: { type: "boolean", editable: false },
                                TripChargeName:{editable:true},
                                StatusName: { editable: true },
                                ResolutionName: { editable: true }
                            }
                        }
                    },
                },

                //autoSync: true,
                batch: true,
                cache: false,
                toolbar: [
                    //{
                    //    template: function (headerData) {
                    //        return $("#headToolbarTemplate").html()
                    //    }
                    //}
                ],
                sortable: true,
                filterable: true,
                resizable: true,
                editable: "inline",
                detailInit: detailinitParent,
                //dataBound: function (e) {
                //    $scope.quoteLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource._data.length;
                //},
              
                noRecords: { template: "No records found" },
                columns: [
                     {
                         field: "Id",
                         title: "VPO-Line",
                         filterable: { multi: true, search: true },
                         hidden: true,
                     },
                    {
                        field: "QuoteLineNumber",
                        title: "Line",
                        width: "120px",
                        filterable: { multi: true, search: true },
                        hidden: false,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                        },
                    },
                    
                      {
                          field: "ProductName",
                          title: "Product",
                          width: "200px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                      },
                      {
                          field: "ProductId",
                          title: "Product No",
                          width: "200px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          template: function (dataItem) {
                              return "<span class='Grid_Textalign'>" + dataItem.ProductId + "</span>";
                          },
                      },
                      {
                          
                          field: "Description",
                          title: "Product Description",
                          attributes: {
                                 
                              style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                          },
                          width: "220px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          template: function (dataitem) {
                              return ProductDescriptionn(dataitem);
                          }

                      },
                       {
                           
                           field: "Typevalue",
                           title: "Issue Type",
                           width: "220px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                       },
                       {

                           field: "CaseReason",
                           title: "Case Reason",
                           width: "220px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                       },
                      
                     {
                         field: "IssueDescription",
                         title: "Issue Description",
                         width: "150px",
                         filterable: { multi: true, search: true },
                         hidden: false,
                         

                     },
                    {
                        field: "ExpediteReq",
                        title: "Expedite Req",
                        width: "150px",
                        filterable: { multi: true, search: true },
                        template: '<input type="checkbox"  #= ExpediteReq ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                    },
                      //{
                      //    field: "ExpediteApproved",
                      //    title: "Expedite Approved",
                      //    width: "150px",
                      //    filterable: { multi: true, search: true },
                      //    template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                      //},
                       {
                           field: "ReqTripCharge",
                           title: "Req Trip Charge",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           template: '<input type="checkbox" #= ReqTripCharge ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                       },
                    //{
                    //    field: "TripChargeName",
                    //    title: "Approved Trip Charge",
                    //    width: "150px",
                    //    filterable: { multi: true, search: true },
                    //    hidden: false,
                    //    editor: function (container, options) {
                    //        $('<input required=true style="width:130px;" id="Trip_val" name="TripCharge" data-bind="value:TripChargeApproved" style="border: none "  />')
                    //            .appendTo(container)
                    //            .kendoDropDownList({
                    //                autoBind: false,
                    //                optionLabel: "Select",

                    //                valuePrimitive: true,
                    //                dataTextField: "TripChargevalue",
                    //                dataValueField: "TripChargeApproved",
                    //                template: "#=TripChargevalue #",

                    //                change: function (e, options) {
                    //                    var item = $('#caseGrid01').find('.k-grid-edit-row');
                    //                    item.data().uid
                    //                    var dataItem = item.data().$scope.dataItem;
                    //                    if (dataItem.TripChargeApproved) {
                    //                        onTripChargeValue(dataItem.TripChargeApproved);
                    //                    }

                    //                },

                    //                dataSource: {
                    //                    transport: {
                    //                        read: {

                    //                            url: '/api/CaseManageAddEdit/0/GetTripChargeValue?Tableid=' + 9,
                    //                        }
                    //                    },
                    //                }

                    //            });
                    //    }


                    //},
                    //   {
                    //       field: "StatusName",
                    //       title: "Status",
                    //       width: "150px",
                    //       filterable: { multi: true, search: true },
                    //       hidden: false,
                    //       editor: function (container, options) {
                    //           $('<input required=true style="width:130px;" id="Status_val" name="Status" data-bind="value:Status" style="border: none "  />')
                    //               .appendTo(container)
                    //               .kendoDropDownList({
                    //                   autoBind: false,
                    //                   optionLabel: "Select",

                    //                   valuePrimitive: true,
                    //                   dataTextField: "Statusvalue",
                    //                   dataValueField: "Status",
                    //                   template: "#=Statusvalue #",

                    //                   change: function (e, options) {
                    //                       var item = $('#caseGrid01').find('.k-grid-edit-row');
                    //                       item.data().uid
                    //                       var dataItem = item.data().$scope.dataItem;
                    //                       if (dataItem.Status) {
                    //                           onStatusChangeValue(dataItem.Status);
                    //                       }

                    //                   },

                    //                   dataSource: {
                    //                       transport: {
                    //                           read: {

                    //                               url: '/api/CaseManageAddEdit/0/GetCaseStatus?Tableid=' + 7,
                    //                           }
                    //                       },
                    //                   }

                    //               });
                    //       }


                    //   },
                    //   {
                    //       field: "ResolutionName",
                    //       title: "Resolution",
                    //       width: "150px",
                    //       filterable: { multi: true, search: true },
                    //       hidden: false,
                    //       editor: function (container, options) {
                    //           $('<input required=true  style="width:130px;" id="Resolution_val" name="Resolution" data-bind="value:Resolution" style="border: none "  />')
                    //               .appendTo(container)
                    //               .kendoDropDownList({
                    //                   autoBind: false,
                    //                   optionLabel: "Select",

                    //                   valuePrimitive: true,
                    //                   dataTextField: "Resolutionvalue",
                    //                   dataValueField: "Resolution",
                    //                   template: "#=Resolutionvalue #",

                    //                   change: function (e, options) {
                    //                       var item = $('#caseGrid01').find('.k-grid-edit-row');
                    //                       item.data().uid
                    //                       var dataItem = item.data().$scope.dataItem;
                    //                       if (dataItem.Resolution) {
                    //                           onResolutionChangeValue(dataItem.Resolution);
                    //                       }

                    //                   },

                    //                   dataSource: {
                    //                       transport: {
                    //                           read: {

                    //                               url: '/api/CaseManageAddEdit/0/GetCaseResolution?Tableid=' + 8,
                    //                           }
                    //                       },
                    //                   }

                    //               });
                    //       }


                    //   },
                        {
                            field: "",
                            title: "",
                            width: "60px",
                            
                            template: function (dataitem) {
                                var hcontent = `<ul>
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown" ng-click="bringDropdown($event,dataItem )">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                       
                                                    </div>
                                                   </li>
                                                </ul>`;

                                return hcontent;

                            }
                        },

                ]

            };

            function detailinitParent(e) {
                var grdId = "grd" + e.data.uid;
                $("<div id = '" + grdId + "' class='sub-grid' />").appendTo(e.detailCell).kendoGrid({
                    dataSource: [e.data],
                    editable: "inline",
                   
                    columns: [
                          {
                              field: "ExpediteApproved",
                              title: "Expedite Approved",
                              width: "150px",
                              filterable: { multi: true, search: true },
                              template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                          },
                           
                       {
                           field: "TripChargeName",
                           title: "Approved Trip Charge",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           editor: function (container, options) {
                               $('<input required=true style="width:130px;" id="Trip_val" name="TripCharge" data-bind="value:TripChargeApproved" style="border: none "  />')
                                   .appendTo(container)
                                   .kendoDropDownList({
                                       autoBind: false,
                                       optionLabel: "Select",

                                       valuePrimitive: true,
                                       dataTextField: "TripChargevalue",
                                       dataValueField: "TripChargeApproved",
                                       template: "#=TripChargevalue #",

                                       change: function (e, options) {
                                           var item = $('#caseGrid01').find('.k-grid-edit-row');
                                           item.data().uid
                                           var dataItem = item.data().$scope.dataItem;
                                           if (dataItem.TripChargeApproved) {
                                               onTripChargeValue(dataItem.TripChargeApproved);
                                           }

                                       },

                                       dataSource: {
                                           transport: {
                                               read: {

                                                   url: '/api/CaseManageAddEdit/0/GetTripChargeValue?Tableid=' + 9,
                                               }
                                           },
                                       }

                                   });
                           }


                       }, {
                           field: "Amount",
                           title: "$ Amount",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           template: function (dataItem) {
                               if (dataItem.TripChargeApproved == 9000) {
                                   if (dataItem.Amount) {
                                       const formatter = new Intl.NumberFormat('en-US', {
                                           style: 'currency',
                                           currency: 'USD',
                                           minimumFractionDigits: 2
                                       })

                                       return formatter.format(dataItem.Amount);
                                   }
                                   else
                                       return '';
                               }
                               else {
                                   return '';
                               }
                           },
                           editor: '<input required=true name="Amount" id="Amount" type="number"   title=""  class="k-textbox" data-bind="value:Amount" style="border: none "/>',

                       },
                       {
                           field: "StatusName",
                           title: "Status",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           editor: function (container, options) {
                               $('<input required=true style="width:130px;" id="Status_val" name="Status" data-bind="value:Status" style="border: none "  />')
                                   .appendTo(container)
                                   .kendoDropDownList({
                                       autoBind: false,
                                       optionLabel: "Select",

                                       valuePrimitive: true,
                                       dataTextField: "Statusvalue",
                                       dataValueField: "Status",
                                       template: "#=Statusvalue #",

                                       change: function (e, options) {
                                           var item = $('#caseGrid01').find('.k-grid-edit-row');
                                           item.data().uid
                                           var dataItem = item.data().$scope.dataItem;
                                           if (dataItem.Status) {
                                               onStatusChangeValue(dataItem.Status);
                                           }

                                       },

                                       dataSource: {
                                           transport: {
                                               read: {

                                                   url: '/api/CaseManageAddEdit/0/GetCaseStatus?Tableid=' + 7,
                                               }
                                           },
                                       }

                                   });
                           }


                       },
                       {
                           field: "ResolutionName",
                           title: "Resolution",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           editor: function (container, options) {
                               $('<input required=true  style="width:130px;" id="Resolution_val" name="Resolution" data-bind="value:Resolution" style="border: none "  />')
                                   .appendTo(container)
                                   .kendoDropDownList({
                                       autoBind: false,
                                       optionLabel: "Select",

                                       valuePrimitive: true,
                                       dataTextField: "Resolutionvalue",
                                       dataValueField: "Resolution",
                                       template: "#=Resolutionvalue #",

                                       change: function (e, options) {
                                           var item = $('#caseGrid01').find('.k-grid-edit-row');
                                           item.data().uid
                                           var dataItem = item.data().$scope.dataItem;
                                           if (dataItem.Resolution) {
                                               onResolutionChangeValue(dataItem.Resolution);
                                           }

                                       },

                                       dataSource: {
                                           transport: {
                                               read: {

                                                   url: '/api/CaseManageAddEdit/0/GetCaseResolution?Tableid=' + 8,
                                               }
                                           },
                                       }

                                   });
                           }


                       },

                    ],
                });
            }

            var i = 0;
            $scope.bringDropdown = function (e, data) {
                $scope.dataitemToedit = data;
                var event = e;
                i++;
                setTimeout(function () {
                    i = 0;
                }, 500);
                if (i == 1) {
                    setDropdown(event);
                }
                e.stopPropagation();
                e.preventDefault();
            }

        
            function ProductDescriptionn(dataitem) {
                var template = "";
                // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
                if (dataitem.Description) {
                    var erer = dataitem.Description.replace('"', "");

                    erer = erer.replace(/<[^>]*>/g, '');
                    template += '<span title="' + erer + '">' + dataitem.Description + '</span>'
                    return template;
                }
                else {
                    return '';
                }
                //}
                //else return '';
                // else return '<input id="productdescri_txt" readonly data-type="text" class="k-textbox"  name="Description" data-bind="value:Description"/>';
            }

            var setDropdown = function (e) {
                if ($('#dropforGrid').hasClass("openDrop") && (($scope.offset + 32) > e.pageY && ($scope.offset - 32) < e.pageY)) {
                    $('#dropforGrid').removeClass("openDrop")
                    $('#dropforGrid').css({
                        'display': 'none'
                    });
                }
                else {
                    $scope.offset = e.pageY;
                    var top = ($scope.offset - 110) + "px";
                    $('#dropforGrid').css({
                        'top': top,
                        'display': 'block'
                    });
                    $('#dropforGrid').addClass("openDrop");
                }
            }

            $scope.selectedRow = '';

            //edit grid data
            $scope.EditRowDetailsData = function () {
                if ($('#dropforGrid').hasClass("openDrop")) {
                    $('#dropforGrid').removeClass("openDrop")
                    $('#dropforGrid').css({
                        'display': 'none'
                    });
                }
                var obj = $scope.dataitemToedit;
                var grid = $("#caseGrid01").data("kendoGrid");
                var validator = $scope.kendoValidator("caseGrid01");
                if (validator.validate()) {

                    if ($scope.selectedRow >= 0) {
                        
                        if ($scope.StatusvalDesc)
                            grid.dataSource._data[$scope.selectedRow].StatusName = $scope.StatusvalDesc;
                        if ($scope.ResolutionvalDesc)
                            grid.dataSource._data[$scope.selectedRow].ResolutionName = $scope.ResolutionvalDesc;
                        if ($scope.TripChargeApprovedvalDesc)
                            grid.dataSource._data[$scope.selectedRow].TripChargeName = $scope.TripChargeApprovedvalDesc;

                        if ($("#Amount"))
                        {
                            if ($("#Amount").val())
                                grid.dataSource._data[$scope.selectedRow].Amount = $("#Amount").val();
                        }
                        
                       

                    }

                    // get the selected row
                    for (var i = 0; i < grid.dataSource._data.length ; i++) {
                        if (grid.dataSource._data[i].uid == obj.uid) {
                            $scope.selectedRow = i;
                        }
                    }

                    var grids = $("#caseGrid01").data("kendoGrid");
                    grid.refresh();
                    grids.editRow(obj);

                    var row = grid.tbody.find("tr[data-uid='" + obj.uid + "']");
                    grid.expandRow(row);
                    //  $scope.childGridUpdate();

                    var grdId = "#grd" + obj.uid;
                    var childgrid = $(grdId).data("kendoGrid");
                    childgrid.editRow(obj);

                    if (grid.dataSource._data[$scope.selectedRow].TripChargeApproved !== 9000) {
                        $("#Amount").val("");
                        $("#Amount").prop("readonly", true);
                        if ($("#Amount").data("kendoNumericTextBox")) {
                            $("#Amount").data("kendoNumericTextBox").value('');
                            $("#Amount").data("kendoNumericTextBox").readonly();
                        }
                    }
                    else
                        $("#Amount").prop("readonly", false);


                    $("#Amount").kendoNumericTextBox({
                        format: "c",
                        decimals: 2,
                        spinners: false
                    });

                    if (grid.dataSource._data[$scope.selectedRow].TripChargeApproved !== 9000) {                       
                        if ($("#Amount").data("kendoNumericTextBox")) {
                            $("#Amount").data("kendoNumericTextBox").value('');
                            $("#Amount").data("kendoNumericTextBox").readonly();
                        }
                    }

                } else {

                    return false;
                }
            };

            //<--- On change functions on grid line drop-down code --->
            function onStatusChangeValue(value) {
                
                var data = $("#Status_val").data("kendoDropDownList");
                value = data.value();

                for (var i = 0; i < data.dataSource._data.length; i++)
                    if (data.dataSource._data[i].Status == value) $scope.StatusvalDesc = data.dataSource._data[i].Statusvalue;
            }

          
            
            function onResolutionChangeValue(value) {
                
                var data = $("#Resolution_val").data("kendoDropDownList");
                value = data.value();

                for (var i = 0; i < data.dataSource._data.length; i++)
                    if (data.dataSource._data[i].Resolution == value) $scope.ResolutionvalDesc = data.dataSource._data[i].Resolutionvalue;
            }

            //var convertamountToCurrency = function()
            //{
            //    var amountValue = $("#Amount").val();
            //    if (amountValue) {
            //        var decimalValue = amountValue % 1;
            //        $("#Amount").val(formatter.format(amountValue));
            //    }
            //}

            function onTripChargeValue(value) {
                
                var data = $("#Trip_val").data("kendoDropDownList");
                value = data.value();
                var grid = $("#caseGrid01").data("kendoGrid");
                for (var i = 0; i < data.dataSource._data.length; i++)
                    if (data.dataSource._data[i].TripChargeApproved == value) $scope.TripChargeApprovedvalDesc = data.dataSource._data[i].TripChargevalue;
                if ($("#Amount")) {
                    if ($("#Amount").val())
                        grid.dataSource._data[$scope.selectedRow].Amount = $("#Amount").val();
                }

                if ($scope.TripChargeApprovedvalDesc !== "Approved") {
                    $("#Amount").val("");
                    $("#Amount").prop("readonly", true);
                    $("#Amount").required = "";
                    $("#Amount").removeClass("k-invalid")
                    grid.dataSource._data[$scope.selectedRow].Amount = 0;
                    if ($("#Amount").data("kendoNumericTextBox"))
                    {
                        $("#Amount").data("kendoNumericTextBox").value('');
                        $("#Amount").data("kendoNumericTextBox").readonly();
                    }
                    if ($(".k-numerictextbox"))
                        $(".k-numerictextbox").removeClass("error");
                   
                }
                else {
                    if ($("#Amount").data("kendoNumericTextBox")) {
                        $("#Amount").data("kendoNumericTextBox").value('');
                        $("#Amount").data("kendoNumericTextBox").enable(true);
                    }
                    $("#Amount").prop("readonly", false);
                    $("#Amount").required = "required";
                   
                }

                
                console.log($("#Amount").data("kendoNumericTextBox"));
            }

            //<--- End --->

            //Code for save
            $scope.SaveDataValue = function () {
                
                var validator = $scope.kendoValidator("caseGrid01");
                if ($(".k-numerictextbox"))
                    $(".k-numerictextbox").removeClass("error");
                if (validator.validate()) {
                    var grid = $("#caseGrid01").data("kendoGrid");
                    if ($("#Amount")) {
                        if ($("#Amount").val())
                            grid.dataSource._data[$scope.selectedRow].Amount = $("#Amount").val();
                    }
                    var dataSource = grid.dataSource._data;

                   }
                var dto = dataSource;
                if(dto)
                for (var tt = 0; tt < dto.length; tt++)
                {
                    if (!dto[tt].Amount)
                        dto[tt].Amount = 0;
                }

               
                if (dto) {
                    $http.post('/api/CaseManageAddEdit/' + dataSource[0].VendorCaseId + '/SaveCaseVendorDetails', dto).then(function (response) {
                        
                        if (response.data) {
                            
                            window.location.href = "#!/VendorcaseView/"+$scope.CaseId+ "/" + response.data;
                            }
                            else {
                                HFC.DisplayAlert(response.data);
                                return;
                            }

                        });
                }
               
                if (!($("#Amount").data("kendoNumericTextBox").value()) && $("#Trip_val").data("kendoDropDownList").text() === "Approved") {
                        if ($(".k-numerictextbox"))
                        $(".k-numerictextbox").addClass("error");                   
                }
                return;
            }


            if ($("#Amount").data("kendoNumericTextBox")) {
                $("#Amount").data("kendoNumericTextBox").value('');
                $("#Amount").data("kendoNumericTextBox").readonly();
            }

            //code for cancel button
            $scope.CancelEditMode = function () {
                
                window.location.href = "#!/VendorcaseView/" + $scope.CaseId  + "/" + $scope.VendorCaseId;
            }


            $http.get('/api/comments/' + $scope.CaseId + '/FindFranchiseTimeZone').then(function (response) {
                $scope.TimeZone = response.data;
            });


     }

    ])



