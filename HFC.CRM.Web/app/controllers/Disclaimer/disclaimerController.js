﻿/***************************************************************************\
Module Name:  Disclaimer Controller  .js - AngularJS file
Project: HFC
Created on: 21 December 2017 Thursday
Created By:
Copyright:
Description: Disclaimer Controller
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app.controller('disclaimerController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
                    'HFCService','NavbarService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http,
        HFCService, NavbarService) {

        var ctrl = this;
        HFCService.setHeaderTitle("Legal Disclaimer #");
       
        $scope.HFCService = HFCService;
        $scope.FranchiseId = $routeParams.FranchiseId;
        $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsGeneralTab();

        $scope.Disclaimer = {
            Id: '',
            ShortDisclaimer: '',
            LongDisclaimer: '',
            FranchiseId:'',
        };



        $scope.Permission = {};
        var Disclaimerpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'LegalDisclaimer')

        $scope.Permission.ViewDisclaimer = Disclaimerpermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Disclaimer').CanAccess;
        $scope.Permission.EditDisclaimer = Disclaimerpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Disclaimer').CanAccess;


        //when cancel is been clicked
        
        $scope.cancel = function () {
            window.location.href = " #!/disclaimerView";
        }
       
        //on saving the disclaimer
        $scope.SaveDisclaimer = function (operation) {
            var dto = $scope.Disclaimer;
            $http.post('/api/Disclaimer',dto).then (function(response){
                $scope.IsBusy = false;
                if (dto.Id != '' || dto.Id != null || dto.Id != 0) {
                    HFC.DisplaySuccess("Disclaimer Modified");
                }
                else {
                    HFC.DisplaySuccess("Disclaimer created");
                }
            });
            $scope.Disclaimer_edit.$setPristine();
            window.location.href = " #!/disclaimerView";
            $scope.Getvalue();
            }
            
        //to fetch the value for view
        //var franchiseId = $scope.FranchiseId;
        $scope.Getvalue = function () {
            $http.get('/api/Disclaimer/0/Getvalue').then(function (data) {
                // var DisclaimerData = data.data;
                $scope.Disclaimer = data.data;
            });
        }


        //on edit the disclaimer
        
        $scope.Edit = function (FranchiseId, Id) {
            if (FranchiseId != 0 && Id != 0) {
                $http.get('/api/Disclaimer/0/GetDataEdit?FranchiseId=' + FranchiseId + "&Id=" + Id).then(function (response) {
                    $scope.Disclaimer = response.data;
                });
                
            }
            //var FranchiseId = $scope.FranchiseId
            window.location.href = " #!/disclaimerEdit" ;
        }

        $scope.Getvalue();

    }
]);

