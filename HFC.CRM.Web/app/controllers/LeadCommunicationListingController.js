﻿/***************************************************************************\
Module Name:  LeadCommunicationController.js - lead AngularJS
Project: HFC
Created on: 
Created By:
Copyright:
Description: Lead Communication log information
Change History:
Date  By  Description

\***************************************************************************/

'use strict';

app.controller('LeadCommunicationListingController', [
    '$scope', '$routeParams', '$rootScope', '$http', '$window',
    '$location', '$modal', 'CalendarService', 'NoteServiceTP', 'PersonService', 'FileprintService', 'LeadService', 'LeadAccountService',
    'AccountService', 'HFCService', 'NavbarService', 'EmailQuoteService', 'CommunicationService', 'kendotooltipService','$sce',
function (
    $scope, $routeParams, $rootScope, $http, $window,
    $location, $modal, CalendarService, NoteServiceTP, PersonService, FileprintService, leadService, leadAccountService, AccountService,
    HFCService, NavbarService, EmailQuoteService, CommunicationService, kendotooltipService, $sce) {

    var self = this;
    //self.Lead = {};
    $scope.HFCService = HFCService;
    $scope.BrandId = $scope.HFCService.CurrentBrand;
    $scope.NoteServiceTP = NoteServiceTP;
    $scope.PersonService = PersonService;
    $scope.FileprintService = FileprintService;
    $scope.EmailQuoteService = EmailQuoteService;
    var offsetvalue = 0;

    self.leadId = $routeParams.leadId;
    
    $scope.EmailSent = false;

    $scope.NavbarService = NavbarService;
    var ul = $location.absUrl().split('#!')[1];
    if (ul.includes('leadCommunication')) {
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesLeadsTab();
    }

    //Render html properly in subject pop-up
    $scope.trustAsHtml = function (html) {
        return $sce.trustAsHtml(html);
    }

    $scope.Permission = {};
    var Leadpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Lead')
    var ConvertLeadpermission = Leadpermission.SpecialPermission.find(x=>x.PermissionCode == 'ConvertLead').CanAccess;
    var LeadCommunicationpermission = Leadpermission.SpecialPermission.find(x => x.PermissionCode == 'LeadCommunication').CanAccess;

    $scope.Permission.AddLead = Leadpermission.CanCreate; 
    $scope.Permission.EditLead = Leadpermission.CanUpdate;
    $scope.Permission.ConvertLead = ConvertLeadpermission;
    $scope.Permission.AddEvent = Leadpermission.CanCreate;

    $scope.Permission.ListCommunication = LeadCommunicationpermission; 

   //For Appointment
    HFCService.GetUrl();

    //https://rclayton.silvrback.com/pubsub-controller-communication
    // Sample publisher to update the grid after editing a calender
    // or newly added calendar. This code should be moved to the new 
    // appointment popup.
    self.updateCommunicationGrid = function () {
        $rootScope.$broadcast("message_Communication_update", new Date());
    }
   
    $scope.modalState = {
        loaded: true
    }

    $scope.$watch('modalState.loaded', function () {
        var loadingElement = document.getElementById("loading");

        if (!$scope.modalState.loaded) {
            loadingElement.style.display = "block";
        } else {
            loadingElement.style.display = "none";
        }
    });

    function errorHandler(reseponse) {
        

        // Inform that the modal is ready to consume:
        $scope.modalState.loaded = true;
        HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
    }
    //get lead
    if (self.leadId > 0) {
        // Inform that the modal is not yet ready
        $scope.modalState.loaded = false;
        $http.get('/api/leads/' + self.leadId).then(function (response) {
            
            var lead = response.data.Lead;
            self.Lead = lead;

            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFCService.setHeaderTitle("Lead Communication #" + self.leadId + " - " + self.Lead.PrimCustomer.FirstName + " " + self.Lead.PrimCustomer.LastName);
        }, errorHandler);
    }

    //populate grid
    $scope.populateLeadCommunications = function () {
        if ($scope.SelectedFilterOption == undefined)
            $scope.SelectedFilterOption = null;
        CommunicationService.getLeadCommunication(self.leadId, $scope.SelectedFilterOption)
            .then(function (response) {
                $scope.communicationList = response.data;
                var grid = $("#gridCommunicationLog").data("kendoGrid");
                if (grid)
                grid.setDataSource($scope.communicationList);
            },
                function (responseError) {
                    $scope.errorMessage = responseError.statusText;
                });
    }

    $scope.populateLeadCommunications();

    //Grid Code
    $scope.ComminicationLogGrid = function () {
        $scope.LeadCommunication_log = {
            dataSource: $scope.communicationList,
            error: function (e) {
                HFC.DisplayAlert(e.errorThrown);
            },
            dataBound: function (e) {
                if (this.dataSource.view().length == 0) {
                    // The Grid contains No recrods so hide the footer.
                    $('.k-pager-nav').hide();
                    $('.k-pager-numbers').hide();
                    $('.k-pager-sizes').hide();
                } else {
                    // The Grid contains recrods so show the footer.
                    $('.k-pager-nav').show();
                    $('.k-pager-numbers').show();
                    $('.k-pager-sizes').show();
                }
                if (kendotooltipService.columnWidth) {
                    kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                } else if (window.innerWidth < 1280) {
                    kendotooltipService.restrictTooltip(null);
                }

            },
            resizable: true,
            
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "SentEmailId",
                    title: "SentEmailId",
                    type: "number",
                    hidden: true,
                },
                {
                    field: "DateValue",
                    title: "Date",
                    
                    template: function (dataitem) {
                        if (dataitem.DateValue != null && dataitem.DateValue != "")
                            return HFCService.KendoDateFormat(dataitem.DateValue)
                        else return "";
                    },
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "LeadLogFilterCalendar", offsetvalue),
                    
                },
                {
                    field: "Type",
                    title: "Type",
                    filterable: {
                        multi: true,
                        search: true
                    }
                   
                },
                {
                    field: "RecipientValue",
                    title: "User",
                    
                    template: function (dataitem) {
                        if (dataitem.RecipientValue != null && dataitem.RecipientValue != "")
                            return maskedDisplayPhone(dataitem.RecipientValue);
                            
                        else return "";
                    },
                    filterable: {
                        //multi: true,
                        search: true
                    } 
                    
                },
                {
                    field: "SubjectDescription",
                    title: "Description/Email Subject",
                   
                    template: function (dataitem) {
                        if ((dataitem.TemplateId != null) || (dataitem.TextingTemplateTypeId != null)) {
                            
                            return "<span><div class='description' style='color: rgb(61,125,139);cursor: pointer;' ng-click='displayemaildata(" + dataitem.SentEmailId + ")'>" + dataitem.SubjectDescription + "</div></span>";
                        } else return "<span><div class='description'>" + dataitem.SubjectDescription + "</div></span>";
                    },
                    filterable: {
                        //multi: true,
                        search: true
                    }
                    
                },

                


            ],
            //filterable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isempty: "Empty",
                        isnotempty: "Not empty"


                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gte: "Greater than or equal to",
                        lte: "Less than or equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                change: function (e) {
                    var myDiv = $('.k-grid-content.k-auto-scrollable');
                    myDiv[0].scrollTop = 0;
                }
            },
            //columnResize: function (e) {
            //    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
            //    getUpdatedColumnList(e);
            //}
        };
        //$scope.LeadCommunication_log = kendotooltipService.setColumnWidth($scope.LeadCommunication_log);
        //console.log($scope.LeadCommunication_log);
    };
    // - End

    //var getUpdatedColumnList = function (e) {
    //    kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
    //}

    $scope.ComminicationLogGrid();

    function maskPhoneNumber(text) {
        if (text != null && text !== undefined) {
            return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
        } else {
            return '';
        }
    }
    //phone num mask
    function maskedDisplayPhone(phone) {
        var Div;
        if (phone) {
            Div = "";
            if (phone && phone != "")
                Div += maskPhoneNumber(phone);
        } else {
            Div = "";
        }
        return Div;
    }

    // - filter selection code
    $scope.FilterBySelectedLog = function () {
        
        $scope.SelectedFilterOption = $scope.Valueselected();
        $scope.populateLeadCommunications();
    }
    // - end

    $scope.Valueselected = function () {
        var arr = [];
        var result = '';
        if ($('#Check1').is(":checked")) {
            result = 18001;
            arr.push(result);
        }
        if ($('#Check2').is(":checked")) {
            result = 18002;
            arr.push(result);
        }
        if ($('#Check3').is(":checked")) {
            result =18003;
            arr.push(result);
        }
        if ($('#Check4').is(":checked")) {
            result = 18004;
            arr.push(result);
        }
        if ($('#Check5').is(":checked")) {
            result = 18005;
            arr.push(result);
        }

        return arr;
    }

    //show add log pop-up & close code
    $scope.AddCommunicationLog = function () {
        
        $("#Show_AddCommunicationLog").modal("show");
        CommunicationService.setControllerMethod($scope.populateLeadCommunications);
    }

    //display subject/ description in pop-up
   $scope.displayemaildata = function (Id) {
        var id = Id; 
        var url = '/api/Communication/' + id + '/GetEmaildeatils';
        $http.get(url).then(function (response) {
            var data = response.data;
            $scope.showdata = data;
            if (data.Type == "Email" && data.Body != "" && data.Body != null)
                $("#Show_EmailSentData").modal("show");
            else if (data.Type == "Text" && data.Body != "" && data.Body != null)
                $("#Show_TextingData").modal("show");
            else $("#Show_AddedData").modal("show");
        });
    }
    // --end
    //close e-mail subj pop-up
    $scope.ClosePop_up = function () {
        $("#Show_EmailSentData").modal("hide");
    }
    //--end

    //close texting sub pop-up
    $scope.ClosePop_up_Texting = function () {
       $("#Show_TextingData").modal("hide");
    }
    //--end

    //close added description pop-up
    $scope.ClosePop_up_Added = function () {
        $("#Show_AddedData").modal("hide");
    }
    //--end

    //Resend E-mail confirmation pop-up for email need to send
    $scope.reSend = function (emailId) {

        $scope.ResendMailId = emailId;
        var resendurl = '/api/Communication/' + $scope.ResendMailId + '/GetResendEmailData';
        $http.get(resendurl).then(function (response) {
            var ResendEmaildata = response.data;
            $scope.ShowEmail = ResendEmaildata;
            $("#Show_ReSentEmailData").modal("show");
        })
    }
    // End

    //Re-Send the e-mail again 
    $scope.ReSentEmailData = function (Emailid) {

        var Id = Emailid;
        $("#Show_ReSentEmailData").modal("hide");
        var reminderurl = '/api/Communication/' + Id + '/ResendEmail';
        $http.get(reminderurl).then(function (response) {
            var res = response.data;
            if (res == true) {
                HFC.DisplaySuccess("Success");

                $scope.populateAccountCommunications();
            }
            else {
                HFC.DisplayAlert("Error");
            }
        });
    }
    //- End

    //edit icon
    $scope.ClickMeToRedirectEditLead = function () {
        $window.location.href = "#!/leadsEdit/" + self.leadId;
    };
    //end

    //add lead
    $scope.Newleadpage = function () {
        $window.location.href = "#!/leads";
    };
    //end

    //convert lead icon
    $scope.ConvertLead = function () {
        $http.get('/api/search/' + self.leadId + '/GetLeadMatchingAccounts').then(function (data) {
            $scope.MatchLeadAccount = data.data;
        });
        window.location = '#!/leadConvert/' + self.leadId;
    }
    //end

    $scope.EmailQuoteService.EmailSuccessCallback = function () {
        $scope.populateLeadCommunications();
        //self.updateCommunicationGrid();
    }

    $scope.GetEmailstatus = function () {
        
        var leadid = self.leadId;
        if(leadid)
        {
            $http.get('/api/Leads/0/GetEmailStatus?leadid=' + leadid).then(function (response) {
                
                if (response.data == "True") {
                    $scope.EmailSent = true;
                }
                else $scope.EmailSent = false;

            });
        }
    }

    $scope.GetEmailstatus();

    //code for sending email when email button clicked.
    $scope.SendLeadEmail = function () {
        $scope.EmailNotify = false;
        var leadid = self.leadId;
        var isnotifyemail = self.Lead.IsNotifyemails;
        if (isnotifyemail == true) {

        
        $scope.modalState.loaded = false;
        $http.get('/api/Leads/0/SendEmailToLead?leadid=' + leadid).then(function (response) {

            if (response.data == "True") {
                $scope.EmailSent = true;
                $scope.MailSent = true;

                if (self.Lead.PrimCustomer.SecondaryEmail && self.Lead.PrimCustomer.PrimaryEmail)
                    $scope.Mailid = self.Lead.PrimCustomer.PrimaryEmail + ',' + self.Lead.PrimCustomer.SecondaryEmail;
                else if (self.Lead.PrimCustomer.PrimaryEmail)
                    $scope.Mailid = self.Lead.PrimCustomer.PrimaryEmail;
                else if (self.Lead.PrimCustomer.SecondaryEmail)
                    $scope.Mailid = self.Lead.PrimCustomer.SecondaryEmail;

                $scope.modalState.loaded = true;
                //self.updateCommunicationGrid();
                $scope.populateLeadCommunications();
                $("#AfetrThanks_EmailSend").modal("show");
                return;
            }
            else if(response.data=="Success") 
            {
                $scope.MailSent = false;
                $scope.EmailSent = false;
                $scope.modalState.loaded = true;
                $("#AfetrThanks_EmailSend").modal("show");
                return;
            }
        else {
            $scope.EmailNotify = true;
            $scope.modalState.loaded = true;
            $("#AfetrThanks_EmailSend").modal("show");
            return;
        }

            });
        }
        else {
            $scope.EmailNotify = true;
            $scope.modalState.loaded = true;
            $("#AfetrThanks_EmailSend").modal("show");
            return;
        }
    }
    //end

    $scope.EmailMailClose = function () {
        
        $("#AfetrThanks_EmailSend").modal("hide");
        return;
    }

    // print integration -- murugan
    $scope.printControl = {};
    $scope.printSalesPacket = function (modalid) {

        if (self.leadId) {
            $scope.leadIdPrint = $routeParams.leadId;
        }
        else if (self.opportunityId) {
            $scope.opportunityIdPrint = $routeParams.opportunityId;
        }

        //if ($routeParams.leadId) $scope.leadIdPrint = $routeParams.leadId;

        // $("#" + modalid).modal("show");
        $scope.printControl.showPrintModal();
    }

    //$scope.printControl = {};
    //$scope.printSalesPacket = function (modalid) {
    //    if ($routeParams.opportunityId) $scope.opportunityIdPrint = $routeParams.opportunityId;

    //    // $("#" + modalid).modal("show");
    //    $scope.printControl.showPrintModal();
    //}
}
]);





