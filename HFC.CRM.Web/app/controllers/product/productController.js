﻿/***************************************************************************\
Module Name:  Product Controller .js - AngularJS file
Project: HFC
Created on: 02 November 2017 Thursday
Created By:
Copyright:
Description: Product Controller
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app.controller('productController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
    'HFCService', 'ProductSearchService', 'ProductService', 'NavbarService', 'kendotooltipService', 'ResurfacingProductService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http,
        HFCService, ProductSearchService, ProductService, NavbarService, kendotooltipService, ResurfacingProductService) {
        //
        var ctrl = this;


        $scope.Product = null;
        $scope.HFCService = HFCService;
        $scope.BrandId = $scope.HFCService.CurrentBrand;
        $scope.ProductSearchService = ProductSearchService;
        $scope.ProductService = ProductService;
        $scope.Productkey = $routeParams.Productkey;
        $scope.FranchiseId = $routeParams.FranchiseId;
        $scope.VendorId = $routeParams.VendorId;
        $scope.Guid = $routeParams.Guid;

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsOperationsTab();
        $scope.ResurfacingProductService = ResurfacingProductService;
        $scope.Permission = {};
        var MyProductspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyProducts')
        var MyVendorspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyVendors')

        var MyLaborSetuppermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyProducts')

        $scope.Permission.ListProducts = MyProductspermission.CanRead; //$scope.HFCService.GetPermissionTP('List Products').CanAccess;
        $scope.Permission.AddProduct = MyProductspermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Product').CanAccess;
        $scope.Permission.EditProduct = MyProductspermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Product').CanAccess;
        $scope.Permission.DisplayProduct = MyProductspermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Product').CanAccess;
        $scope.Permission.DisplayVendor = MyVendorspermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Vendor').CanAccess;

        $scope.Permission.ListLaborSetup = MyLaborSetuppermission.CanRead;
        $scope.Permission.AddLaborSetup = MyLaborSetuppermission.CanCreate;
        $scope.Permission.EditLaborSetup = MyLaborSetuppermission.CanUpdate;

        HFCService.setHeaderTitle("Product #");

        $scope.rowNumberr = 0;
        $scope.rowNo = 0;
        $scope.currentEditId = null;
        $scope.CostCurrencyControl = {};
        $scope.SalePriceCurrencyControl = {};

        $scope.isSurfacingLabour = false;

        $scope.Product = {
            ProductTypeCore: '',
            ProductKey: '',
            ProductID: '',
            ProductType: '',
            ProductName: '',
            Description: '',
            ProductStatus: '',
            ProductCategory: '',
            ProductSubCategory: '',
            PicProductId: '',
            VendorName: '',
            VendorId: '',
            VendorProductSKU: '',
            Cost: '',
            SalePrice: '',
            VendorID: '',
            ProductGroupDesc: '',
            DiscontinuedDate: '',
            PhasedoutDate: '',
            Model: '',
            ProductCollection: '',
            ProductModelID: '',
            Model: '',
            Collection: '',
            Color: '',
            VendorType: '',
            ProductCategoryId: '',
            Discount: '',
            DiscountType: '%',
            Taxable: '',
            MultipartSurfacing: false,
            ProductSurfaceSetup: {
                MinimumJobSize: '',
                MaximumJobSize: ''
            },
            MultipleSurfaceProductSet: null,
            MultipleSurfaceProduct: {},
            SurfaceLaborSet: null,
            MultipartAttributeSet: null,
            MasterSurfacingProductID: null,
            Attributes: {
                color1: '',
                color2: '',
                color3: '',
                color4: '',
                color5: '',
                color6: '',
                color7: '',
                color8: '',
                color9: '',
                color10: '',
                color11: '',
                color12: '',
                color13: '',
                color14: '',
                color15: '',
                color16: '',
                colorEnabled: false,
                finishes1: '',
                finishes2: '',
                finishes3: '',
                finishes4: '',
                finishes5: '',
                finishes6: '',
                finishes7: '',
                finishes8: '',
                finishes9: '',
                finishes10: '',
                finishes11: '',
                finishes12: '',
                finishes13: '',
                finishes14: '',
                finishes15: '',
                finishes16: '',
                finishesEnabled: false,
            }
        };

        var offsetvalue = 0;
        $scope.ProductView = {
            ProductTypeCore: '',
            ProductKey: '',
            ProductID: '',
            ProductType: '',
            ProductName: '',
            Description: '',
            ProductStatus: '',
            ProductCategory: '',
            ProductSubCategory: '',
            PicProductId: '',
            VendorName: '',
            VendorId: '',
            VendorProductSKU: '',
            Cost: '',
            SalePrice: '',
            VendorID: '',
            ProductGroupDesc: '',
            DiscontinuedDate: '',
            PhasedoutDate: '',
            Model: '',
            ProductCollection: '',
            ProductModelID: '',
            Model: '',
            Collection: '',
            Color: '',
            VendorType: '',
            ProductCategoryId: '',
            Discount: '',
            DiscountType: '',
            Taxable: '',
            MultipartSurfacing: false,
            MultipleSurfaceProductSet: null,
            MultipleSurfaceProduct: [],
            ProductSurfaceSetup: {},
            SurfaceLaborSet: null,
            MultipartAttributeSet: null,
            MasterSurfacingProductID: null,
            Attributes: {
                color1: '',
                color2: '',
                color3: '',
                color4: '',
                color5: '',
                color6: '',
                color7: '',
                color8: '',
                color9: '',
                color10: '',
                color11: '',
                color12: '',
                color13: '',
                color14: '',
                color15: '',
                color16: '',
                colorEnabled: false,
                finishes1: '',
                finishes2: '',
                finishes3: '',
                finishes4: '',
                finishes5: '',
                finishes6: '',
                finishes7: '',
                finishes8: '',
                finishes9: '',
                finishes10: '',
                finishes11: '',
                finishes12: '',
                finishes13: '',
                finishes14: '',
                finishes15: '',
                finishes16: '',
                finishesEnabled: false,
            }
        };

        $scope.rowNumber = 0;
        function renderNumber() {
            $scope.rowNumber = $scope.rowNumber + 1;
            $scope.rowNumberr = $scope.rowNumber;
            return $scope.rowNumber;
        }

        function resetRowNumber(e) {
            $scope.rowNumber = 0;
            var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");
            var dataSourcee = grid.dataSource;
            if (dataSourcee._data.length === 0) {
                $scope.rowNumberr = 0;
            }

            if (kendotooltipService.columnWidth) {
                kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
            } else if (window.innerWidth < 1280) {
                kendotooltipService.restrictTooltip(null);
            }
            var view = dataSourcee.view();
            $scope.rowNumberr = view.length;

        }

        function resetRow(e) {
            $scope.row = 0;
            var grid = $("#gridMultipartSurface").data("kendoGrid");
            var dataSourcee = grid.dataSource;
            if (dataSourcee._data.length === 0) {
                $scope.rowNo = 0;
            }

            if (kendotooltipService.columnWidth) {
                kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
            } else if (window.innerWidth < 1280) {
                kendotooltipService.restrictTooltip(null);
            }
            var view = dataSourcee.view();
            $scope.rowNo = view.length;

        }
        $scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                validate: function (e) {
                    //var textBox = $(".k-textbox");                     
                    //$.each(textBox, function (key, value) {  // need to change key,value pair, its for dropdown only.                     
                    //    var input = $(value).find("input.k-invalid");
                    //    if (input.size() > 0) {
                    //        $(this).addClass("dropdown-validation-error");
                    //        alert('error');
                    //    } else {
                    //        $(this).removeClass("dropdown-validation-error");
                    //    }
                    //})
                }
            }).getKendoValidator();
        }

        $scope.GetLaborSetup = function () {
            if (!(window.location.href.includes('/productEdit') || window.location.href.includes('/productDetail') || window.location.href.includes('/productCreate')))
                return;
            //$scope.GridEditable = false;


            if ($scope.BrandId == 2) {
                $scope.EditLaborSetup = {
                    dataSource: {
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {
                                Id: "Id",
                                fields: {
                                    Id: { editable: false, nullable: true },
                                    Day: { editable: true, type: "number", min: 1 },
                                    Process: { editable: true, type: "string" },
                                    FixedVariable: { editable: true, type: "string" },
                                    VariableLaborBasis: { editable: true, type: "number", min: 1 },
                                    VariableLaborFactor: { editable: true, type: "number" },
                                    FixedLaborHours: { editable: true, type: "number" }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            field: "Day",
                            title: "Day",
                            editor: function (container, options) {
                                $('<input id="Day" name="Day" ng-keyup="TLKeyDown($event)" data-bind="value:Day"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "#",
                                        spinners: false,
                                        decimals: 0,
                                        //min: 1,
                                        //max: 3
                                    });
                            }
                        },
                        {
                            field: "Process",
                            title: "Process",
                            editor: '<input data-type="text" class="k-textbox " id="Process" name="Process" data-bind="value:Process"/>'
                        },
                        {
                            field: "FixedVariable",
                            title: "Fixed/Variable",
                            editor: function (container, options) {
                                $('<input id="FixedVariable" name="FixedVariable" data-bind="value:FixedVariable"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        autoBind: true,
                                        optionLabel: "Select",
                                        valuePrimitive: true,
                                        dataTextField: "Value",
                                        dataValueField: "Key",
                                        template: "#=Value #",
                                        change: function (e) {
                                            var item = $('#gridSurfaceLaborSetup').find('.k-grid-edit-row');
                                            var dataItem = item.data().$scope.dataItem;
                                            TLDropdownChange(dataItem);
                                        },
                                        select: function (e) {
                                            if (e.dataItem.Value === "Select") {
                                                $scope.SystemPreventClose = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#FixedVariable").parent();
                                                var b = a[0].children[0].children;
                                                $(b[0]).removeClass("requireddropfield");
                                                $(b[0]).addClass("k-input");
                                                $scope.SystemPreventClose = false;
                                            }

                                        },
                                        close: function (e) {
                                            if ($scope.SystemPreventClose == true)
                                                e.preventDefault();

                                            $scope.SystemPreventClose = false;
                                        },
                                        dataSource: {
                                            data:
                                                [{ Key: "Fixed", Value: "Fixed" },
                                                { Key: "Variable", Value: "Variable" }]
                                        }
                                    });
                                if ($scope.NewEditRow) {
                                    var a = $("#FixedVariable").parent();
                                    var b = a[0].children[0].children;
                                    $(b[0]).removeClass("k-input");
                                    $(b[0]).addClass("requireddropfield");
                                }
                            }
                        },
                        {
                            field: "VariableLaborBasis",
                            title: "Variable Labor Basis",
                            editor: function (container, options) {
                                $('<input id="VariableLaborBasis" name="VariableLaborBasis" ng-keyup="TLKeyDown($event)" data-bind="value:VariableLaborBasis"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "#",
                                        spinners: false,
                                        decimals: 0,
                                        //min: 1,
                                        //max: 999
                                    });
                            }
                        },
                        {
                            field: "VariableLaborFactor",
                            title: "Variable Labor Factor",
                            editor: function (container, options) {
                                $('<input id="VariableLaborFactor" name="VariableLaborFactor" ng-keyup="TLKeyDown($event)" data-bind="value:VariableLaborFactor"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "{0:n2}",
                                        decimals: 2,
                                        spinners: false,
                                        //min: 0,
                                        //max: 5
                                    });
                            }
                        },
                        {
                            field: "FixedLaborHours",
                            title: "Fixed Labor Hours",
                            editor: function (container, options) {
                                $('<input id="FixedLaborHours" name="FixedLaborHours" ng-keyup="TLKeyDown($event)" data-bind="value:FixedLaborHours"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "{0:n2}",
                                        spinners: false,
                                        decimals: 2,
                                        //min: 0,
                                        //max: 19.99
                                    });
                            }
                        },
                        {
                            field: "",
                            title: "",
                            width: "60px",
                            hidden: !((window.location.href.includes('/productEdit')) || (window.location.href.includes('/productCreate'))),
                            template: optionButtonsTemplate
                        }
                    ],
                    noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                    filterable: false,
                    resizable: true,
                    sortable: false,
                    scrollable: true,
                    pageable: false,
                    dataBound: resetRowNumber,
                    editable: "inline",
                    edit: function (e) {
                        e.container.find(".k-edit-label:last").hide();
                        e.container.find(".k-edit-field:last").hide();
                    },
                    toolbar: [
                        {
                            template: kendo.template($("#headToolbarTemplate").html())
                        }],
                    columnResize: function (e) {
                        getUpdatedColumnList(e);
                    }
                };
            }
            else if ($scope.BrandId == 3) {
                $scope.EditLaborSetup = {
                    dataSource: {
                        //data: CCData,
                        error: function (e) {
                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {
                                Id: "Id",
                                fields: {
                                    Id: { editable: false, nullable: true },
                                    Day: { editable: true, type: "number", },
                                    Process: { editable: true, type: "string", validation: { required: true }, },
                                    SuggestedCrewSize: { editable: true, type: "number", },
                                    SuggestedHours: { editable: true, type: "number", },
                                }
                            }
                        }
                    },
                    columns: [{ title: "#", width: 40, template: renderNumber, hidden: true },
                    {
                        field: "Day",
                        title: "Day",
                        width: "150px",
                        filterable: false,// { multi: true, search: true },
                        hidden: false,
                        editor: numericEditor,// '<input data-type="number" required=true ng-keyup="CCKeyDown($event)" class="k-textbox"  name="Day" id="Day" value="${Day}"/>'
                    },
                    {
                        field: "Process",
                        title: "Process",
                        width: "150px",
                        filterable: false,// { multi: true, search: true },
                        hidden: false,
                        editor: '<input data-type="string" required=true  class="k-textbox"  name="Process" id="Process" value="${Process}"/>'
                    },
                    {
                        field: "SuggestedCrewSize",
                        title: "Suggested Crew Size",
                        width: "150px",
                        filterable: false,// { multi: true, search: true },
                        hidden: false,
                        editor: numericEditor, //'<input data-type="number" required=true  class="k-textbox" ng-keyup="CCKeyDown($event)"  name="SuggestedCrewSize" id="SuggestedCrewSize" value="${SuggestedCrewSize}"/>'
                    },
                    {
                        field: "SuggestedHours",
                        title: "Suggested Hours",
                        width: "100px",
                        hidden: false,
                        filterable: false,
                        editor: numericEditor,// '<input data-type="number" required=true  class="k-textbox" ng-keyup="CCKeyDown($event)" name="SuggestedHours" id="SuggestedHours" value="${SuggestedHours}"/>'

                    }, {
                        field: "",
                        title: "",
                        hidden: !((window.location.href.includes('/productEdit')) || (window.location.href.includes('/productCreate'))),
                        editable: false,
                        width: "80px",
                        template: optionButtonsTemplate
                    },
                    ],
                    dataBound: resetRowNumber,
                    editable: "inline",
                    filterable: true,
                    resizable: true,
                    autoSync: true,
                    pageable: false,
                    noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                    scrollable: true,
                    toolbar: [
                        {
                            template: kendo.template($("#headToolbarTemplate").html()),
                        }],
                    columnResize: function (e) {
                        getUpdatedColumnList(e);
                    }
                };
            }

            if ($scope.BrandId == 2 || $scope.BrandId == 3) {
                $scope.EditMultipartSurface = {
                    dataSource: {
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {
                                Id: "Id",
                                fields: {
                                    Id: { editable: false },
                                    ProductID: { editable: false, type: "string" },
                                    VendorName: { editable: true, type: "string" },
                                    VendorID: { editable: true, type: "number" },
                                    VendorSKU: { editable: true, type: "string" },
                                    UnitofMeasure: { editable: true, type: "string" },
                                    Description: { editable: true, type: "string" },
                                    ContainerSize: { editable: true, type: "number", min: 1 },
                                    Coverage: { editable: true, type: "number", min: 1 },
                                    Mixratio: { editable: true, type: "number" },
                                    WasteReclaim: { editable: true, type: "number", min: 1 },
                                    WasteType: { editable: true, type: "string" },
                                    CostPrice: { editable: true, type: "number", min: 1 },
                                    UnitPrice: { editable: true, type: "number", min: 1 },
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            field: "ProductID",
                            title: "Product ID",
                            width: 75,
                            editor: '<input data-type="text" readonly class="k-textbox " id="ProductID" name="ProductID" data-bind="value:ProductID"/>'
                        },
                        {
                            field: "VendorName",
                            title: "Vendor Name",
                            width: 125,
                            template: function (dataItem) {
                                if (dataItem.VendorName) {
                                    return "<span class='tooltip-wrapper'>" + dataItem.VendorName + "</span>";
                                } else {
                                    return "";
                                }
                            },
                            editor: function (container, options) {
                                $('<input id="VendorName" name="' + options.field + '" data-bind="value:VendorName"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        valuePrimitive: true,
                                        autoBind: false,
                                        optionLabel: "Select",
                                        dataTextField: "Name",
                                        dataValueField: "Name",
                                        template: "#=Name #",
                                        change: function (e, options) {
                                            var item = $('#gridMultipartSurface').find('.k-grid-edit-row');
                                            item.data().uid
                                            var dataItem = item.data().$scope.dataItem;
                                            SetVendorId(dataItem);
                                        },
                                        select: function (e) {
                                            if (e.dataItem.Value === "Select") {
                                                $scope.SystemPrevent = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#VendorName").parent();
                                                var b = a[0].children[0].children;
                                                $(b[0]).removeClass("requireddropfield");
                                                $(b[0]).addClass("k-input");
                                                $scope.SystemPrevent = false;
                                            }

                                        },
                                        close: function (e) {
                                            if ($scope.SystemPrevent == true)
                                                e.preventDefault();

                                            $scope.SystemPrevent = false;
                                        },
                                        dataSource: new kendo.data.DataSource({
                                            transport: {
                                                read: {
                                                    url: '/api/lookup/' + $scope.Product.ProductType + '/ProductVendorName',
                                                    type: "Get",
                                                    dataType: "json"
                                                }
                                            },
                                            schema: {
                                                model: {
                                                    Name: "Name",
                                                    VendorID: "VendorID",
                                                    VendorIdPk: "VendorIdPk",
                                                    VendorType: "VendorType"
                                                }
                                            }
                                        }),
                                    });
                            }
                        },
                        {
                            field: "VendorID",
                            title: "Vendor ID",
                            width: 75,
                            editor: '<input data-type="text" readonly class="k-textbox " id="VendorID" name="VendorID" data-bind="value:VendorID"/>'
                        },
                        {
                            field: "VendorSKU",
                            title: "Vendor Product SKU",
                            width: 125,
                            editor: '<input data-type="text" class="k-textbox " ng-keyup="desckeydown($event)" id="VendorSKU" name="VendorSKU" data-bind="value:VendorSKU"/>'
                        },
                        {
                            field: "UnitofMeasure",
                            title: "Unit of Measure",
                            width: 100,
                            editor: function (container, options) {
                                $('<input id="UnitofMeasure" name="UnitofMeasure" data-bind="value:UnitofMeasure"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        autoBind: true,
                                        optionLabel: "Select",
                                        valuePrimitive: true,
                                        dataTextField: "Value",
                                        dataValueField: "Key",
                                        template: "#=Value #",
                                        select: function (e) {
                                            if (e.dataItem.Value === "Select") {
                                                $scope.SystemPreventClose = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#UnitofMeasure").parent();
                                                var b = a[0].children;
                                                $(b).removeClass("required-error");
                                                $(b).addClass("k-state-default");
                                                $scope.SystemPreventClose = false;
                                            }

                                        },
                                        close: function (e) {
                                            if ($scope.SystemPreventClose == true)
                                                e.preventDefault();

                                            $scope.SystemPreventClose = false;
                                        },
                                        dataSource: {
                                            data:
                                                [{ Key: "Gallon", Value: "Gallon" },
                                                { Key: "Pound", Value: "Pound" }]
                                        }
                                    });
                            }
                        },
                        {
                            field: "Description",
                            title: "Description",
                            width: 125,
                            editor: '<input data-type="text" class="k-textbox " ng-keyup="desckeydown($event)" id="Description" name="Description" data-bind="value:Description"/>'
                        },
                        {
                            field: "ContainerSize",
                            title: "Container Size (gallons)",
                            width: 125,
                            editor: function (container, options) {
                                $('<input id="ContainerSize" name="ContainerSize" ng-keyup="MPKeyDown($event)" data-bind="value:ContainerSize"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "#",
                                        spinners: false,
                                        decimals: 0
                                    });
                            }
                        },
                        {
                            field: "Coverage",
                            title: "Coverage (sq ft/gallon)",
                            width: 125,
                            editor: function (container, options) {
                                $('<input id="Coverage" name="Coverage" ng-keyup="MPKeyDown($event)" data-bind="value:Coverage"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "#",
                                        spinners: false,
                                        decimals: 0
                                    });
                            }
                        },
                        {
                            field: "Mixratio",
                            title: "Mix Ratio",
                            width: 80,
                            editor: function (container, options) {
                                $('<input id="Mixratio" name="Mixratio" ng-keyup="MPKeyDown($event)" data-bind="value:Mixratio"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "{0:n1}",
                                        spinners: false,
                                        decimals: 1
                                    });
                            }
                        },
                        {
                            field: "WasteReclaim",
                            title: "Waste/Reclaim",
                            width: 120,
                            editor: function (container, options) {
                                $('<input id="WasteReclaim" name="WasteReclaim" ng-keyup="MPKeyDown($event)" data-bind="value:WasteReclaim"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "#",
                                        spinners: false,
                                        decimals: 0
                                    });
                            }
                        },
                        {
                            field: "WasteType",
                            title: "Waste Type",
                            width: 150,
                            //editor: '<input data-type="text" class="k-textbox " id="WasteType" name="WasteType" data-bind="value:WasteType"/>'
                            //template: "<label>First<input disabled type='radio' value='#: WasteType #' #= WasteType== 'F' ? 'checked' : ''# >" +
                            //            "<label>Second<input disabled type='radio' value='#: WasteType #' #= WasteType== 'S' ? 'checked' : ''# >",
                            editor: "<label><input name='WasteType' class='option-input radio radio_space' type='radio' data-bind='checked:WasteType' value='Waste (gal/100 sq ft)'> Waste (gal/100 sq ft)<br>" +
                                "<label><input name='WasteType' class='option-input radio radio_space' type='radio' data-bind='checked:WasteType' value='Reclaim (%)'> Reclaim (%)",
                        },
                        {
                            field: "CostPrice",
                            title: "Cost",
                            width: 75,
                            template: function (dataItem) {
                                if (dataItem.CostPrice) {
                                    return "<span>" + CurrencyFormat(dataItem.CostPrice) + "</span>";
                                } else {
                                    return "";
                                }
                            },
                            editor: function (container, options) {
                                $('<input id="CostPrice" name="CostPrice" ng-keyup="MPKeyDown($event)" data-bind="value:CostPrice"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "{0:c3}",
                                        spinners: false,
                                        decimals: 3
                                    });
                            }
                        },
                        {
                            field: "UnitPrice",
                            title: "Unit Price",
                            width: 75,
                            template: function (dataItem) {
                                if (dataItem.UnitPrice) {
                                    return "<span>" + CurrencyFormat(dataItem.UnitPrice) + "</span>";
                                } else {
                                    return "";
                                }
                            },
                            editor: function (container, options) {
                                $('<input id="UnitPrice" name="UnitPrice" ng-keyup="MPKeyDown($event)" data-bind="value:UnitPrice"/>')
                                    .appendTo(container)
                                    .kendoNumericTextBox({
                                        format: "{0:c3}",
                                        spinners: false,
                                        decimals: 3
                                    });
                            }
                        },
                        {
                            field: "",
                            title: "",
                            width: "60px",
                            hidden: !((window.location.href.includes('/productEdit')) || (window.location.href.includes('/productCreate'))),
                            template: optionButtonsTemplateforMultipart
                        }
                    ],
                    noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                    sortable: true,
                    resizable: true,
                    filterable: false,
                    pageable: false,
                    autoSync: true,
                    scrollable: true,
                    dataBound: resetRow,
                    editable: "inline",
                    edit: function (e) {
                        $(e.container).find("input[name='ProductID']").prop('disabled', true);
                    },
                    toolbar: [
                        {
                            template: kendo.template($("#MultipartToolbar").html())
                        }],
                    columnResize: function (e) {
                        getUpdatedColumnList(e);
                    }
                };
            }
        }

        function CurrencyFormat(data) {
            var result = kendo.toString(data, "c3")
            return result;
        }
        function SetVendorId(obj) {
            var grid = $("#gridMultipartSurface").data("kendoGrid");
            var abbrValue = "";
            if (obj.VendorName) {
                for (var i = 0; i < $scope.VendorNameDrop.length; i++) {
                    if ($scope.VendorNameDrop[i].Name == obj.VendorName) {
                        abbrValue = $scope.VendorNameDrop[i].VendorID;
                    }
                }
                var defaultValue = abbrValue;
                obj.VendorID = defaultValue;
                if (defaultValue != "") {
                    $("#VendorID").removeClass("required-error");
                    var a = $("#VendorName").parent();
                    var b = a[0].children;
                    $(b).addClass("k-state-default");
                    $(b).removeClass("required-error");
                }

                $("#VendorID").val(defaultValue);
            } else {
                if (obj.VendorName == null) {
                    obj.VendorID = null;
                    $("#VendorID").val(null);
                }
            }

        };

        function optionButtonsTemplate() {
            return '<div class="dropdown"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i></button><ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" ng-click="EditRowDetails(dataItem)">Edit</a></li><li><a href="javascript:void(0)" ng-click="DeleteRowDetails(this)">Delete</a></li></ul></div>'
        }
        function optionButtonsTemplateforMultipart() {
            return '<div class="dropdown"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i></button><ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" ng-click="EditMultipartRowDetails(dataItem)">Edit</a></li><li><a href="javascript:void(0)" ng-click="DeleteMultipartRowDetails(this)">Delete</a></li></ul></div>'
        }
        function numericEditor(container, options) {
            $('<input id="' + options.field + '" name="' + options.field + '" data-bind="value:' + options.field + '" ng-keyup="CCKeyDown($event)" />')
                .appendTo(container)
                .kendoNumericTextBox({
                    format: "{0:n0}",
                    decimals: 0,
                    min: 1,
                    spinners: false,
                });
        }

        function TLDropdownChange(obj) {
            var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");
            var row = null;
            for (i = 0; i <= grid.dataSource._data.length; i++) {
                if (grid.dataSource._data[i].Id == obj.Id) {
                    row = grid.dataSource._data[i];
                    break;
                }
            }
            var selecteditem = obj.FixedVariable;
            var VLaborBasis = $("#VariableLaborBasis").data("kendoNumericTextBox");
            var VLaborFactor = $("#VariableLaborFactor").data("kendoNumericTextBox");
            var FLaborFactor = $("#FixedLaborHours").data("kendoNumericTextBox");

            if (selecteditem === 'Fixed') {
                VLaborBasis.value(null);
                VLaborFactor.value(null);
                VLaborBasis.enable(false);
                VLaborFactor.enable(false);
                FLaborFactor.enable(true);
            } else if (selecteditem === 'Variable') {
                FLaborFactor.value(null);
                VLaborBasis.enable(true);
                VLaborFactor.enable(true);
                FLaborFactor.enable(false);
            }

            if ($("#FixedVariable").data("kendoDropDownList").selectedIndex === 0) {
                $('input[name="FixedVariable"]').removeClass("k-invalid");

                $.each(dropDowns, function (key, value) {
                    var input = $(value).find("input.k-invalid");
                    var span = $(this).find(".k-widget.k-dropdown.k-header");
                });
            }
        }

        $scope.validateCurrentRecord = function () {
            var returnval = true;

            if ($scope.BrandId == 2) {
                var Process = $("#Process").val();
                var Day = $("#Day").data("kendoNumericTextBox");
                if (!Day)
                    return true;

                var VariableLaborBasis = $("#VariableLaborBasis").data("kendoNumericTextBox");
                var VariableLaborFactor = $("#VariableLaborFactor").data("kendoNumericTextBox");
                var FixedLaborHours = $("#FixedLaborHours").data("kendoNumericTextBox");
                var VariableLaborBasisisDisabled = $("#VariableLaborBasis").prop('disabled');
                var VariableLaborFactorisDisabled = $("#VariableLaborFactor").prop('disabled');
                var FixedLaborHoursisDisabled = $("#FixedLaborHours").prop('disabled');

                if (Process) {
                    $("#Process").removeClass("required-error");
                }
                else {
                    $("#Process").addClass("required-error");
                    returnval = false;
                }

                if (Day) {
                    if (Day.value()) {
                        var parent1 = $("#Day").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#Day").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (VariableLaborBasis) {
                    if (VariableLaborBasis.value() || VariableLaborBasisisDisabled) {
                        var parent1 = $("#VariableLaborBasis").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#VariableLaborBasis").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (VariableLaborFactor) {
                    if (VariableLaborFactor.value() || VariableLaborFactorisDisabled) {
                        var parent1 = $("#VariableLaborFactor").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#VariableLaborFactor").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (FixedLaborHours) {
                    if (FixedLaborHours.value() || FixedLaborHoursisDisabled) {
                        var parent1 = $("#FixedLaborHours").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#FixedLaborHours").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
            }
            if ($scope.BrandId == 3) {
                var Process = $("#Process").val();
                var Day = $("#Day").data("kendoNumericTextBox");
                var SuggestedCrewSize = $("#SuggestedCrewSize").data("kendoNumericTextBox");
                var SuggestedHours = $("#SuggestedHours").data("kendoNumericTextBox");

                if (!Day)
                    return true;

                if (Process) {
                    $("#Process").removeClass("required-error");
                }
                else {
                    $("#Process").addClass("required-error");
                    returnval = false;
                }
                if (Day) {
                    if (Day.value()) {
                        var parent1 = $("#Day").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#Day").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (SuggestedCrewSize) {
                    if (SuggestedCrewSize.value()) {
                        var parent1 = $("#SuggestedCrewSize").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#SuggestedCrewSize").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (SuggestedHours) {
                    if (SuggestedHours.value()) {
                        var parent1 = $("#SuggestedHours").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#SuggestedHours").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
            }

            return returnval;
        }

        $scope.validateCurrentRecordForMultipart = function () {
            var returnval = true;

            if ($scope.Product.MultipartSurfacing == true) {
                var Vname = $('#VendorName').val();
                var Vid = $('#VendorID').val();
                var VSKU = $('#VendorSKU').val();
                var UnitMeasure = $('#UnitofMeasure').val();
                var Description = $("#Description").val();
                var WasteType = $("input[name='WasteType']:checked").val();
                var ContainerSize = $("#ContainerSize").data("kendoNumericTextBox");
                if (!ContainerSize) return true;
                var Coverage = $("#Coverage").data("kendoNumericTextBox");
                var Mixratio = $("#Mixratio").data("kendoNumericTextBox");
                var WasteReclaim = $("#WasteReclaim").data("kendoNumericTextBox");
                var CostPrice = $("#CostPrice").data("kendoNumericTextBox");
                var UnitPrice = $("#UnitPrice").data("kendoNumericTextBox");
                if (Vname != undefined) {
                    if (Vname) {
                        var a = $("#VendorName").parent();
                        var b = a[0].children;
                        $(b).addClass("k-state-default");
                        $(b).removeClass("required-error");
                    }
                    else {
                        var a = $("#VendorName").parent();
                        var b = a[0].children;
                        $(b).removeClass("k-state-default");
                        $(b).addClass("required-error");
                        returnval = false;
                    }
                }
                if (Vid != undefined) {
                    if (Vid) {
                        $("#VendorID").removeClass("required-error");
                    }
                    else {
                        $("#VendorID").addClass("required-error");
                        returnval = false;
                    }
                }

                if (VSKU != undefined) {
                    if (VSKU) {
                        $("#VendorSKU").removeClass("required-error");
                    }
                    else {
                        $("#VendorSKU").addClass("required-error");
                        returnval = false;
                    }
                }

                if (UnitMeasure != undefined) {
                    if (UnitMeasure) {
                        var a = $("#UnitofMeasure").parent();
                        var b = a[0].children;
                        $(b).addClass("k-state-default");
                        $(b).removeClass("required-error");
                    }
                    else {
                        var a = $("#UnitofMeasure").parent();
                        var b = a[0].children;
                        $(b).removeClass("k-state-default");
                        $(b).addClass("required-error");
                        returnval = false;
                    }
                }
                if (Description != undefined) {
                    if (Description) {
                        $("#Description").removeClass("required-error");
                    }
                    else {
                        $("#Description").addClass("required-error");
                        returnval = false;
                    }
                }
                if (WasteType) {
                    $("#WasteType").removeClass("required-error");
                }
                else {
                    //var parent1 = $("#WasteType").parent()[0];
                    //var parentElement = $(parent1).parent()[0];
                    //$(parentElement).addClass("required-error");
                    HFC.DisplayAlert("Please select a Waste Type.")
                    returnval = false;
                }



                if (ContainerSize) {
                    if (ContainerSize.value()) {
                        var parent1 = $("#ContainerSize").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#ContainerSize").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (Coverage) {
                    if (Coverage.value()) {
                        var parent1 = $("#Coverage").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#Coverage").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (Mixratio) {
                    if (Mixratio.value()) {
                        var parent1 = $("#Mixratio").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#Mixratio").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (WasteReclaim) {
                    if (WasteReclaim.value()) {
                        var parent1 = $("#WasteReclaim").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#WasteReclaim").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (CostPrice) {
                    if (CostPrice.value()) {
                        var parent1 = $("#CostPrice").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#CostPrice").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
                if (UnitPrice) {
                    if (UnitPrice.value()) {
                        var parent1 = $("#UnitPrice").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).removeClass("required-error");
                    }
                    else {
                        var parent1 = $("#UnitPrice").parent()[0];
                        var parentElement = $(parent1).parent()[0];
                        $(parentElement).addClass("required-error");
                        returnval = false;
                    }
                }
            }


            return returnval;
        }
        // for grid rows manipulation
        var getUpdatedColumnList = function (e) {
            kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
        }

        $scope.ProductSubCategoryChanged = function (id) {
            $.each($scope.ProductSubCategoryList, function (key, value) {
                if (value.ProductCategoryId == id) {
                    if (value.ProductCategory == "Concrete System Installation" || value.ProductCategory == "Flooring System Installation") {
                        $scope.isSurfacingLabour = true;
                        $scope.LoadSurfaceLabourData();
                        return false;
                    }
                }
                else
                    $scope.isSurfacingLabour = false;
            });
        }

        $scope.LoadSurfaceLabourData = function () {
            var grid = $('#gridSurfaceLaborSetup').data("kendoGrid");
            if ($scope.Product.ProductSurfaceSetup) {
                if ($scope.BrandId == 2) {
                    if ($scope.Product.ProductSurfaceSetup.ProductSurfaceSetupForTL) {
                        var dataSource = new kendo.data.DataSource({ data: $scope.Product.ProductSurfaceSetup.ProductSurfaceSetupForTL, pageSize: 25 });
                        dataSource.read();
                        grid.setDataSource(dataSource);
                    }
                }
                if ($scope.BrandId == 3) {
                    if ($scope.Product.ProductSurfaceSetup.ProductSurfaceSetupForCC) {
                        var dataSource = new kendo.data.DataSource({ data: $scope.Product.ProductSurfaceSetup.ProductSurfaceSetupForCC, pageSize: 25 });
                        dataSource.read();
                        grid.setDataSource(dataSource);
                    }
                }
            }
        }

        $scope.LoadMultipartSurfaceData = function () {
            var grid = $('#gridMultipartSurface').data("kendoGrid");
            if ($scope.Product && $scope.Product.MultipleSurfaceProduct) {
                if ($scope.Product.MultipleSurfaceProduct.Id) {
                    var dataSource = new kendo.data.DataSource({ data: $scope.Product.MultipleSurfaceProduct, pageSize: 10 });
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }
            }
        }

        $scope.CCKeyDown = function (controlId) {
            var cId = controlId.currentTarget.id;
            var value = $("#" + cId).val();
            if (cId == "Day") {
                if (value > 3 || value < 1) {
                    $("#" + cId).val('');
                    $("#" + cId).focus();
                }
            }
            if (cId == "SuggestedCrewSize") {
                if (value > 9 || value < 1) {
                    $("#" + cId).val('');
                    $("#" + cId).focus();
                }
            }
            if (cId == "SuggestedHours") {
                if (value > 99 || value < 1) {
                    $("#" + cId).val('');
                    $("#" + cId).focus();
                }
            }
        }

        $scope.TLKeyDown = function (controlId) {
            var cId = controlId.currentTarget.id;
            var value = $("#" + cId).val();
            if (cId == "Day") {
                if (value > 3 || value < 1) {
                    $("#" + cId).val('');
                    $("#" + cId).focus();
                }
            }
            if (cId == "VariableLaborBasis") {
                if (value > 999 || value < 1) {
                    $("#" + cId).val('');
                    $("#" + cId).focus();
                }
            }
            if (cId == "VariableLaborFactor") {
                if (value > 5 || value < 0) {
                    $("#" + cId).val('');
                    $("#" + cId).focus();
                }
            }
            if (cId == "FixedLaborHours") {
                if (value > 19.99 || value < 0) {
                    $("#" + cId).val('');
                    $("#" + cId).focus();
                }
            }

        }

        $scope.MPKeyDown = function (controlId) {
            var cId = controlId.currentTarget.id;
            var letterNumber = /^[1-9a-zA-Z]+$/;
            var value = $("#" + cId).val();
            var returnval;
            if (cId == "ContainerSize") {
                if (value > 9 || value < 1) {
                    returnval = true;
                } else {
                    returnval = false;
                }
            }
            if (cId == "Coverage") {
                if (value > 999 || value < 1) {
                    returnval = true;
                } else {
                    returnval = false;
                }
            }
            if (cId == "Mixratio") {
                if (value > 9.5 || value < 0.5) {
                    returnval = true;
                } else {
                    returnval = false;
                }
            }
            if (cId == "WasteReclaim") {
                if (value > 99 || value < 1) {
                    returnval = true;
                } else {
                    returnval = false;
                }
            }
            if (cId == "CostPrice") {
                if (value > 99.999 || value < 1) {
                    returnval = true;
                } else {
                    returnval = false;
                }
            }
            if (cId == "UnitPrice") {
                if (value > 99.999 || value < 1) {
                    returnval = true;
                } else {
                    returnval = false;
                }
            }

            if (returnval) {
                $("#" + cId).val('');
                $("#" + cId).focus();
                var parent1 = $("#" + cId).parent()[0];
                var parentElement = $(parent1).parent()[0];
                $(parentElement).addClass("required-error");
            } else {
                var parent1 = $("#" + cId).parent()[0];
                var parentElement = $(parent1).parent()[0];
                $(parentElement).removeClass("required-error");
            }
        }

        $scope.desckeydown = function (ctrlId) {
            var cId = ctrlId.currentTarget.id;
            var value = $("#" + cId).val();
            if (cId == "Description") {
                if (value.length > 0) {
                    $("#" + cId).removeClass("required-error");
                }
            }
            if (cId == "VendorSKU") {
                if (value.length > 0) {
                    $("#" + cId).removeClass("required-error");
                }
            }
        }
        //Editing row
        $scope.EditRowDetails = function (obj) {
            $scope.NewEditRow = false;
            $scope.EditMode = true;
            var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");
            var validator = $scope.kendoValidator("gridSurfaceLaborSetup");

            if (validator.validate() && $scope.validateCurrentRecord()) {
                $scope.currentEditId = obj.Id;
                var dataSource = $('#gridSurfaceLaborSetup').data().kendoGrid.dataSource;
                // dataSource.read();
                if (dataSource) {
                    for (var p = 0; p <= dataSource._data.length ; p++) {
                        if (dataSource._data[p].Id == obj.Id) {
                            var EditRow = dataSource._data[p];
                            grid.setDataSource(dataSource);
                            grid.editRow(EditRow);
                            if ($scope.BrandId == 2) TLDropdownChange(EditRow);
                            break;
                        }
                    }
                }
            }
        }

        $scope.EditMultipartRowDetails = function (obj) {
            var grid = $("#gridMultipartSurface").data("kendoGrid");
            var validator = $scope.kendoValidator("gridMultipartSurface");

            if (validator.validate() && $scope.validateCurrentRecordForMultipart()) {
                $scope.currentEditId = obj.Id;
                var dataSource = $('#gridMultipartSurface').data().kendoGrid.dataSource;
                if (dataSource) {
                    for (var p = 0; p <= dataSource._data.length ; p++) {
                        if (dataSource._data[p].Id == obj.Id) {
                            var EditRow = dataSource._data[p];
                            grid.setDataSource(dataSource);
                            grid.editRow(EditRow);
                            break;
                        }
                    }
                }
            }
        }

        //Custom Delete
        $scope.DeleteRowDetails = function (e) {

            $scope.currentEditId = null;
            $scope.EditMode = true;
            var rowEdit = $('#gridSurfaceLaborSetup').find('.k-grid-edit-row');
            if (rowEdit.length != 0 && rowEdit[0].dataset.uid != e.dataItem.uid) {
                var validator = $scope.kendoValidator("gridSurfaceLaborSetup");
                if (validator.validate() && $scope.validateCurrentRecord()) {
                    var dataItem = e.dataItem;
                    var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource.remove(dataItem);

                } else {

                    return false;
                }
            }
            else {
                var dataItem = e.dataItem;
                var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");
                var dataSource = grid.dataSource;
                dataSource.remove(dataItem);

            }

            $scope.TextboxDisabled = false;
            var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");
            var dataSourcee = grid.dataSource;
            if (dataSourcee._data.length === 0) {
                $scope.rowNumberr = 0;
            }
        };

        $scope.DeleteMultipartRowDetails = function (e) {
            $scope.currentEditId = null;
            var rowEdit = $('#gridMultipartSurface').find('.k-grid-edit-row');
            if (rowEdit.length != 0 && rowEdit[0].dataset.uid != e.dataItem.uid) {
                if ($scope.validateCurrentRecordForMultipart()) {
                    var dataItem = e.dataItem;
                    var grid = $("#gridMultipartSurface").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource.remove(dataItem);
                    var nextid = dataItem.Id;
                    var currentid = dataItem.Id - 1;
                    if (grid._data[currentid] != undefined) {
                        for (var i = currentid; i < grid._data.length; i++) {
                            grid._data[i].Id = nextid;
                            grid._data[i].ProductID = $scope.Product.ProductID + "-" + nextid;
                            nextid += 1;
                        }
                    }

                } else {

                    return false;
                }
            }
            else {
                var dataItem = e.dataItem;
                var grid = $("#gridMultipartSurface").data("kendoGrid");
                var dataSource = grid.dataSource;
                dataSource.remove(dataItem);
                var nextid = dataItem.Id;
                var currentid = dataItem.Id - 1;
                if (grid._data[currentid] != undefined) {
                    for (var i = currentid; i < grid._data.length; i++) {
                        grid._data[i].Id = nextid;
                        grid._data[i].ProductID = $scope.Product.ProductID + "-" + nextid;
                        nextid += 1;
                    }
                }
            }
        };

        /// Adding new row
        $scope.AddRow = function () {
            var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");
            var count = grid.dataSource._data.length
            if (count == 3) {
                return false;
            }

            $scope.NewEditRow = true;
            var rowEdit = $('#gridSurfaceLaborSetup').find('.k-grid-edit-row');

            if (rowEdit.length) {
                //grid is not in edit mode
                var validator = $scope.kendoValidator("gridSurfaceLaborSetup"); //$(rowEdit).kendoValidator().data("kendoValidator");      
                if (!validator.validate() || !$scope.validateCurrentRecord()) {
                    return false;
                }
            }
            $scope.EditMode = true;
            $("#gridSurfaceLaborSetup").data('kendoGrid').dataSource.filter({
            });
            $scope.AddNewRow = true;
            var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");
            var dataSource = grid.dataSource;

            $scope.currentEditId = dataSource._data.length + 1;

            //if (dataSource._data.length > 0)
            //    $scope.currentEditId = dataSource._data.length + 1;
            //else
            //    $scope.currentEditId = 1;
            $scope.AddNewRow = false;
            $scope.AddProductsData();
        };

        $scope.AddMultipartRow = function () {
            var rowEdit = $('#gridMultipartSurface').find('.k-grid-edit-row');

            if (rowEdit.length) {
                if (!$scope.validateCurrentRecordForMultipart()) {
                    return false;
                }
            }
            $("#gridMultipartSurface").data('kendoGrid').dataSource.filter({
            });
            $scope.AddNewRowMultipart = true;
            var grid = $("#gridMultipartSurface").data("kendoGrid");
            var dataSource = grid.dataSource;

            $scope.currentEditId = dataSource._data.length + 1;

            $scope.AddNewRowMultipart = false;
            $scope.AddMultipartData();
        };

        $scope.AddProductsData = function () {

            var grid = $("#gridSurfaceLaborSetup").data("kendoGrid");

            var dataSource = grid.dataSource;
            var index = dataSource._data.length;

            if (index != 0) {
                $scope.Id = index + 1;
            }
            else
                $scope.Id = 1;

            var validator = $scope.kendoValidator("gridSurfaceLaborSetup");
            //var validAreaMoisture = $scope.validateAreaMoisture();
            //if (!validAreaMoisture)
            //return false;
            if (validator.validate()) {
                var newItem = {
                    Id: $scope.Id
                }

                var newItem = dataSource.insert(index, newItem); //, {});                 
                var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                grid.editRow(newRow);

                grid.element.find(".k-grid-content").animate({
                    scrollTop: newRow.offset().top
                }, 400);

                //$("#gridEditMeasurements").data("kendoGrid").addRow(c);

            } else {

                return false;
            }

        };

        $scope.AddMultipartData = function () {

            var grid = $("#gridMultipartSurface").data("kendoGrid");

            var dataSource = grid.dataSource;
            var index = dataSource._data.length;

            if (index != 0) {
                $scope.MultipartId = index + 1;
            }
            else
                $scope.MultipartId = 1;

            var validator = $scope.kendoValidator("gridMultipartSurface");
            if (validator.validate()) {
                var newItem = {
                    Id: $scope.MultipartId,
                    ProductID: $scope.Product.ProductID + "-" + $scope.MultipartId
                }

                var newItem = dataSource.insert(index, newItem);
                var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                grid.editRow(newRow);

                grid.element.find(".k-grid-content").animate({
                    scrollTop: newRow.offset().top
                }, 400);
            } else {
                return false;
            }
        };

        //Initializing CC Surfacing Labour Grid;
        $scope.GetLaborSetup();
        //to get product list in view based on value selection
        $scope.ChangeProduct = function () {
            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
            $('input[name="ProductCheck"]:checked').attr('checked', false);
            //var value = $scope.Product.ProductType;
            var dropvalue = $('#ProductDrop').val();
            var value = dropvalue;
            if (value == "") {
                value = 0;
            }
            $http.get('/api/ProductTP/' + value + '/ProductOption/').then(function (response) {
                var data = response.data;
                var grid = $("#gridProductSearch").data("kendoGrid");
                var dataSource = grid.dataSource;
                dataSource._data = data;
                //grid.dataSource.data(data);
                //for displaying status in kendo grid filter
                var ds = new kendo.data.DataSource({
                    data: data,
                    schema: {
                        model: {
                            fields: {

                                ProductID: { type: "number" },

                            }
                        }
                    },
                });
                grid.setDataSource(ds);
                if (dataSource._data.length == 0) {
                    //grid.dataSource.data(data);
                    //for displaying status in kendo grid filter
                    grid.setDataSource(data);
                }
                else if (dataSource._data.length > 25 || dataSource._data.length < 25) {
                    grid.dataSource.pageSize(25);
                }
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }).catch(function(e)
            {
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            });
        }

        $scope.DisplayPopup = function () {
            $scope.ResurfacingProductService.ShowConfigurator()
            //$("#Warninginfo").modal("show");
            //return;
        }
        //cancel the pop-up
        $scope.CancelPopup = function (modalId) {
            $("#" + modalId).modal("hide");
        };

        $scope.changeProductGrid = function () {
            $("#Warninginfo").modal("hide");
            $http.get('/api/ProductTP/0/ProductsFromHFCList').then(function (response) {
                if (response.data.length) {
                    var data = response.data;
                    var grid = $("#gridProductSearch").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                } else {
                    HFC.DisplayAlert("There is no product in the HFC Master list for your vendors.");
                }

            });
        }

        $scope.VendorNameList = function (value) {
            $scope.Vendor_Name_Option = {
                optionLabel: {
                    Name: "Select",
                    VendorID: ""
                },
                dataTextField: "Name",
                dataValueField: "VendorID",
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val = this.value();
                    if (val != null && val != "") {
                        this.element.removeClass("required-error")
                        $scope.ProductVendorId(val);
                        $scope.VendorTypeValue(val);
                    }
                },
                dataSource: {
                    transport: {
                        read: function (option) {
                            $http.get('/api/lookup/' + value + '/ProductVendorName').then(function (data) {
                                option.success(data.data);
                            });
                        }
                    }
                },
            };
        }

        $scope.VendorTypeChange = function (value) {
            if (value != null) {

                //making it as default true as it is taxable.
                if ($scope.Product.ProductType == 3)
                    $scope.Product.Taxable = true;

                $http.get('/api/lookup/' + value + '/ProductVendorName').then(function (data) {
                    if (data.data != null) {
                        $scope.ProductVendorName = data.data;
                        $scope.Product.VendorType = data.data.VendorType;
                    }


                    if (document.location.href.toString().indexOf("/productCreate/") > 0) {
                        var VendorId = $scope.VendorId;
                        if (VendorId != undefined && VendorId != "")
                            $scope.Product.VendorID = parseInt(VendorId);
                        $scope.VendorTypeValue($scope.Product.VendorID)
                    }
                })
                $scope.VendorNameList(value);
            }
        }

        $scope.LoadVendorName = function () {
            $http.get('/api/lookup/' + $scope.Product.ProductType + '/ProductVendorName').then(function (response) {
                $scope.VendorNameDrop = response.data;
            });
        }

        //to make taxable in reverse
        $scope.Changetaxable = function () {

            var chek_val = $('input[name=isTaxable]').is(':checked');
            if (chek_val == false) {
                $scope.Product.Taxable = true;
            }
            else
                $scope.Product.Taxable = false;
        }

        $scope.ChangeSurfacing = function () {

            var chek_val = $('input[name=MultipartSurfacing]').is(':checked');
            if (chek_val == true) {
                $scope.Product.MultipartSurfacing = true;
                $scope.LoadMultipartSurfaceData();
                $scope.LoadVendorName();
            }
            else
                $scope.Product.MultipartSurfacing = false;
        }

        $scope.ChangedataVendor = function (value) {
            if (value != null) {
                $http.get('/api/lookup/' + value + '/ProductVendorName').then(function (data) {
                    $scope.ProductVendorName = data.data;
                });
            }
        }

        $scope.VendorTypeValue = function (value) {
            if (value == undefined) {
                $scope.Product.VendorType = "";
            }
            else {
                $http.get('/api/lookup/' + value + '/GetVendorType').then(function (data) {
                    $scope.Product.VendorType = data.data.VendorType;
                })
            }

        }
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 317);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 317);
            };
        });
        // End Kendo Resizing


        //to get all product when check box is clicked
        $scope.GetallPrdtDetails = function () {
            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
            var setvalue = $('input[name="ProductCheck"]:checked').val();
            if (setvalue == "on") {
                var dropvalue = $('#ProductDrop').val();
                var value = ""
                if (dropvalue == "") {
                    value = 0;
                }
                else {
                    value = dropvalue;
                }
                $http.get('/api/ProductTP/0/GetallPrdtDetails?DropValue=' + value).then(function (response) {
                    var data = response.data;
                    var grid = $("#gridProductSearch").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    //grid.dataSource.data(data);
                    //for displaying status in kendo grid filter
                    grid.setDataSource(data);
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                }).catch(function(e)
                {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                });
            }
            else if (setvalue == undefined) {
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                $scope.ChangeProduct();
            }
        }

        function costformat(cost) {
            var checkvalue = "";
            var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
            if (format.test(cost)) {
                checkvalue = true;
            }
            else {
                checkvalue = false;
            }
            if (checkvalue == true) {
                if (cost.includes("$")) {
                    cost = cost.replace('$', "");
                }
                if (cost.includes(",")) {
                    cost = cost.replace(',', "");
                }
                if (cost.includes(".00")) {
                    cost = cost.replace('.00', "");
                }

                return cost;
            }
            else {
                return cost;
            }
        }

        //validate CC required controls
        $scope.ValidateCCControls = function () {
            if ($scope.isSurfacingLabour) {
                if ($scope.Product.ProductSurfaceSetup) {
                    if ($scope.Product.ProductSurfaceSetup.MinimumJobSize > 999 || $scope.Product.ProductSurfaceSetup.MinimumJobSize < 1) {
                        $("#MinJobSize").val('');
                        $("#MinJobSize").focus();
                        // $("#MinJobSize").addClass("required-error");
                        return false;
                    }
                    else {
                        $("#MinJobSize").removeClass("required-error");
                    }
                    if ($scope.Product.ProductSurfaceSetup.MaximumJobSize > 9999 || $scope.Product.ProductSurfaceSetup.MaximumJobSize < 100) {
                        $("#MaxJobSize").val('');
                        $("#MaxJobSize").focus();
                        //$("#MaxJobSize").addClass("required-error");
                        return false;
                    }
                    else {
                        $("#MaxJobSize").removeClass("required-error");
                    }
                    if ($scope.Product.ProductSurfaceSetup.MinimumJobSize > $scope.Product.ProductSurfaceSetup.MaximumJobSize) {
                        HFC.DisplayAlert("Maximum Job Size can not be lesser than Minimum Job Size");
                        return false;
                    }
                    var rowEdit = $('#gridSurfaceLaborSetup').find('.k-grid-edit-row');
                    if (rowEdit.length) {
                        if (!$scope.validateCurrentRecord())
                            return false;
                    }
                    return true;
                }
            }
            return true;
        }
        //save the product details
        $scope.SaveProduct = function (operation) {
            var errorflag = false;
            var SurfaceLaborSetup = null;
            if ($('#gridSurfaceLaborSetup').data() && $scope.isSurfacingLabour == true) {
                SurfaceLaborSetup = $('#gridSurfaceLaborSetup').data().kendoGrid.dataSource.data();
                if ($scope.BrandId == 2) {
                    var rowEdit = $('#gridSurfaceLaborSetup').find('.k-grid-edit-row');
                    if (rowEdit.length) {
                        var validator = $scope.kendoValidator("gridSurfaceLaborSetup");
                        if (!validator.validate() || !$scope.validateCurrentRecord()) {
                            return false;
                        }
                    }
                    if (SurfaceLaborSetup.length == 0) {
                        HFC.DisplayAlert("Multi-Step Flooring System Installation Grid is empty.");
                        return;
                    }
                    $scope.Product.ProductSurfaceSetup.ProductSurfaceSetupForTL = SurfaceLaborSetup;
                }
                else if ($scope.BrandId == 3) {
                    if (!$scope.ValidateCCControls())
                        return;
                    if (SurfaceLaborSetup.length == 0) {
                        HFC.DisplayAlert("Multi-Step Concrete System Installation Grid is empty.");
                        return;
                    }
                    $scope.Product.ProductSurfaceSetup.ProductSurfaceSetupForCC = SurfaceLaborSetup;
                }
            } else {
                $scope.Product.ProductSurfaceSetup = null;
            }
            if ($scope.BrandId == 2 || $scope.BrandId == 3) {
                var MultipleSurfaceGrid = $('#gridMultipartSurface').data().kendoGrid.dataSource.data();

                if ($scope.Product.MultipartSurfacing == true) {

                    if (!$scope.validateCurrentRecordForMultipart()) return false;

                    if (MultipleSurfaceGrid.length == 0) {
                        HFC.DisplayAlert("Component Grid is empty.");
                        return;
                    }

                    $scope.Product.MultipleSurfaceProduct = MultipleSurfaceGrid;
                    $scope.Product.Cost = null;
                    $scope.Product.SalePrice = null;
                } else {
                    $('#gridMultipartSurface').data("kendoGrid").dataSource.data([]);
                    $scope.Product.MultipleSurfaceProduct = null;
                    $scope.Product.Attributes = null;
                    $scope.Product.MultipartAttributeSet = null;
                }
            }
            var dto = $scope.Product;
            $scope.ProductSubmitted = true;

            if ($scope.Product.ProductType == 2 || $scope.Product.ProductType == 6) {
                var vendorvalue = $("#Vendor_Name").data("kendoDropDownList").value();
                if (vendorvalue == null || vendorvalue == "") {
                    $("#Vendor_Name").addClass("required-error");
                    errorflag = false;
                }
            }
            if ($scope.Product.ProductType == 2 || $scope.Product.ProductType == 3 || $scope.Product.ProductType == 6) {
                var Pdt_categ_value = $("#Pdt_categ").data("kendoDropDownList").value();
                if (Pdt_categ_value == null || Pdt_categ_value == "") {
                    $("#Pdt_categ").addClass("required-error");
                    errorflag = false;
                }
                if ($scope.Product.VendorType == 1 || $scope.Product.VendorType == 4) {
                    var Pdt_subcateg_value = $("#Pdt_subcateg").data("kendoDropDownList").value();
                    if (Pdt_subcateg_value == null || Pdt_subcateg_value == "") {
                        $("#Pdt_subcateg").addClass("required-error");
                        errorflag = false;
                    }
                }
            }
            if (errorflag == true)
                return;

            var otherthing = $scope.productadd.$error;
            var mng = $scope.productadd.$invalid;
            if ($scope.productadd.$invalid) {
                // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
                return;
            }

            var checkvalue = "";
            var salespricevalue = dto.SalePrice;
            var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
            if (format.test(salespricevalue)) {
                checkvalue = true;
            }
            else {
                checkvalue = false;
            }
            if (checkvalue == true) {
                if (salespricevalue.replace) {
                    salespricevalue = salespricevalue.replace('$', "");
                    salespricevalue = salespricevalue.replace(',', "");
                    salespricevalue = salespricevalue.replace('.00', "");
                    dto.SalePrice = salespricevalue;
                }
                else
                    dto.SalePrice = salespricevalue;

            }
            else {
                dto.SalePrice = salespricevalue;
            }

            //dto.Cost = costformat(dto.Cost);

            $http.post('/api/ProductTP', dto).then(function (response) {
                if (response.data.Status == "My Product Already Exists") {
                    HFC.DisplayAlert(response.data.Status);
                    return;
                }
                else if (response.data.Status == "Service Name Already Exists") {
                    HFC.DisplayAlert(response.data.Status);
                    return;
                }
                else if (response.data.Status == "Discount Name Already Exists") {
                    HFC.DisplayAlert(response.data.Status);
                    return;
                }
                else {
                    if (dto.Name != '' || dto.Name != null || dto.Name != 0) {
                        HFC.DisplaySuccess("Products Modified");
                    }
                    else {
                        HFC.DisplaySuccess("Products created");
                    }
                    if ($scope.Productkey) {
                        $scope.productadd.$setPristine();
                        window.location.href = "#!/productDetail/" + $scope.Productkey + '/' + $scope.FranchiseId;
                    }
                    else {
                        var str = response.data.Status;
                        var res = str.split("|");
                        $scope.productadd.$setPristine();
                        window.location.href = "#!/productDetail/" + res[0] + '/' + res[1];
                    }
                }

            })

        };

        $scope.setAction = function () {
            $scope.SaveProduct();
            $(".save_tpbut").blur();
            //window.location = "#!/productEdit/" + ProductId;;
        }

        //getting auto generated productnumber

        if ($scope.Productkey == null || $scope.Productkey == 0) {
            $http.get('/api/ProductTP/0/GetProductNoNew').then(function (response) {

                $scope.Product.ProductID = response.data;
                $scope.Product.ProductType = "";
                $scope.Product.ProductStatus = 1;
                //$scope.Product.ProductType = "MyProduct";
            });
        }
        //function myfunction(item) {
        //    var s = item.VendorName;
        //    var n = s.indexOf(" - ");
        //    if (n != -1) s = s.substring(n + 2, s.length);
        //    item.VendorName = s;
        //}
        //to view the selected product details. we are passing 2 id value productkey value and franchiseid value
        $scope.ViewDetails = function () {
            if ($scope.Productkey > 0) {
                $http.get('/api/ProductTP/0/Get?Productkey=' + $scope.Productkey + "&FranchiseId=" + $scope.FranchiseId).then(function (response) {
                    $scope.ProductView = response.data;
                    if (($scope.BrandId == 3 || $scope.BrandId == 2) && ($scope.ProductView.ProductSubCategory == "Concrete System Installation" || $scope.ProductView.ProductSubCategory == "Flooring System Installation")) {
                        $scope.isSurfacingLabour = true;
                        if ($scope.ProductView.ProductSurfaceSetup) {
                            $scope.isSurfacingLabour = true;
                            var grid = $('#gridSurfaceLaborSetup').data("kendoGrid");
                            if ($scope.BrandId == 2) {
                                var dataSource = new kendo.data.DataSource({ data: $scope.ProductView.ProductSurfaceSetup.ProductSurfaceSetupForTL, pageSize: 25 });
                                dataSource.read();
                                grid.setDataSource(dataSource);
                            }
                            if ($scope.BrandId == 3) {
                                var dataSource = new kendo.data.DataSource({ data: $scope.ProductView.ProductSurfaceSetup.ProductSurfaceSetupForCC, pageSize: 25 });
                                dataSource.read();
                                grid.setDataSource(dataSource);
                            }
                        }
                    }
                    if ($scope.BrandId == 2 || $scope.BrandId == 3) {
                        if ($scope.ProductView.MultipartSurfacing == true) {
                            //$scope.ProductView.MultipleSurfaceProduct.foreach(myfunction);
                            var dataSource = new kendo.data.DataSource({ data: $scope.ProductView.MultipleSurfaceProduct, pageSize: 25 });
                            var grid = $('#gridMultipartSurface').data("kendoGrid");
                            dataSource.read();
                            grid.setDataSource(dataSource);
                        }
                    }

                    HFCService.setHeaderTitle("Product #" + $scope.ProductView.ProductID + "-" + $scope.ProductView.ProductName);
                });
            }
        }

        $scope.EditProduct1 = function () {
            window.location.href = "#!/productEdit/" + $scope.Productkey + '/' + $scope.FranchiseId;

        }

        if ($scope.Guid) {

            var gid = $scope.Guid;
            if (gid != null || gid != "" || gid != undefined) {
                $http.get('/api/ProductTP/0/GetTempProduct?Guid=' + gid).then(function (response) {

                    var data = response.data;
                    $scope.Product = data;
                    if ($scope.Product.Cost != null) {
                        $scope.CostCurrencyControl.CurrenctFormatControl($scope.Product.Cost);
                    }
                    if ($scope.Product.SalePrice != null)
                        $scope.SalePriceCurrencyControl.CurrenctFormatControl($scope.Product.SalePrice);

                    $scope.Product.VendorName = response.data.VendorName;

                    $scope.Product.VendorType = response.data.VendorType;
                    $scope.PrductTypeSelect($scope.Product.ProductType);
                    $scope.ChangedataVendor($scope.Product.ProductType);

                    $scope.Productkey = data.ProductKey;
                    $scope.FranchiseId = data.FranchiseId;

                    HFCService.setHeaderTitle("Product #" + $scope.Product.ProductID + "-" + $scope.Product.ProductName);
                });
            }
        }

        //edit product
        if (document.location.href.toString().indexOf("/productEdit/") > 0) {

            var Productkey = $scope.Productkey
            var FranchiseId = $scope.FranchiseId;
            if (Productkey > 0) {
                $http.get('/api/ProductTP/0/GetParticular?ProductId=' + Productkey + "&FranchiseId=" + FranchiseId).then(function (response) {

                    $scope.Product = response.data;
                    if ($scope.Product.Cost != null) {
                        $scope.CostCurrencyControl.CurrenctFormatControl($scope.Product.Cost);
                    }
                    if ($scope.Product.SalePrice != null)
                        $scope.SalePriceCurrencyControl.CurrenctFormatControl($scope.Product.SalePrice);

                    $scope.Product.VendorName = response.data.VendorName;
                    if ($scope.Product.ProductType == 1) {
                        $scope.Product.ProductTypeCore = "Core Product";
                    }

                    $scope.Product.VendorType = response.data.VendorType;

                    $scope.PrductTypeSelect($scope.Product.ProductType);

                    $scope.ChangedataVendor($scope.Product.ProductType);

                    $scope.VendorNameList($scope.Product.ProductType);

                    $scope.LoadVendorName();

                    $scope.Pdt_subcateg_List($scope.Product.ProductCategory);

                    HFCService.setHeaderTitle("Product #" + $scope.Product.ProductID + "-" + $scope.Product.ProductName);
                });
            }
        }

        if (document.location.href.toString().indexOf("/productCreate/") > 0) {
            $scope.Product.VendorID = $scope.VendorId;
            $scope.VendorTypeChange($scope.VendorId);
        }

        //to get the product status for the dropdown
        $http.get('/api/lookup/0/ProductStatus').then(function (data) {
            $scope.ProductstatusList = data.data;
        });

        //to fetch Product type for dropdown
        $http.get('/api/ProductTP/0/GetProductType').then(function (data) {
            $scope.ProductTypeList = data.data;
        })

        $scope.ProductVendorId = function (id) {
            $scope.Product.VendorId = id;
            $scope.Product.VendorName = id;
            $scope.Product.VendorID = $scope.Product.VendorName;
        }

        $scope.PrductTypeSelect = function (value) {
            var datavalue = value;
            if (datavalue != 0) {
                $http.get('/api/lookup/0/ProductCategory?Dropcategory=' + datavalue).then(function (data) {
                    $scope.ProductCategoryList = data.data;
                    //var Value = $scope.Product.ProductCategory;
                    var ValueId = $scope.Product.ProductCategoryId;
                    var DataCheck = $scope.Product.ProductCategory;
                    if (typeof DataCheck === 'string') {
                        $scope.Subcategory(ValueId);
                        if (ValueId != null && ValueId != "") $scope.Pdt_subcateg_List(ValueId);
                    }
                    else if (typeof DataCheck === 'number') {
                        $scope.Subcategory($scope.Product.ProductCategory);
                        if ($scope.Product.ProductCategory != null && $scope.Product.ProductCategory != 0) $scope.Pdt_subcateg_List($scope.Product.ProductCategory);
                    }



                });
                $scope.Pdt_categ_List(datavalue);
            }
            if (datavalue != 2)
                $scope.Product.MultipartSurfacing = false;
        }



        //to get the sub category based on category selected
        $scope.Subcategory = function (id) {
            if (id != 0 && id != null) {
                $http.get('/api/lookup/' + id + '/ProductSubCategory').then(function (data) {
                    $scope.ProductSubCategoryList = data.data;
                    if ($("#Pdt_subcateg").data("kendoDropDownList") != undefined) {
                        $("#Pdt_subcateg").data("kendoDropDownList").dataSource.data($scope.ProductSubCategoryList);
                    }
                    if ($scope.Product && $scope.Product.ProductSubCategory)
                        $scope.ProductSubCategoryChanged($scope.Product.ProductSubCategory);

                });
            }
            else {
                $scope.ProductSubCategoryList = '';
            }
        }

        $scope.ProductgridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#gridProductSearch").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [

                    {
                        field: "ProductName",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "VendorName",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "ProductCategory",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "ProductSubCategory",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "Description",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "ProductID",
                        //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                        operator: "eq",
                        value: searchValue
                    },
                    {
                        field: "VendorProductSKU",
                        value: searchValue
                    },
                    {
                        //field: "SalePrice",
                        //value: searchValue
                        field: "SalePrice",
                        // operator: (value) => (value + "").indexOf(searchValue) >= 0,
                        operator: "eq",
                        value: searchValue
                    }
                ]
            });

        }

        $scope.ProductgridSearchClear = function () {
            $('#searchBox').val('');
            $("#gridProductSearch").data('kendoGrid').dataSource.filter({
            });
        }

        $scope.AddNewVendor = function () {

            var dto = $scope.Product;
            $http.post('/api/ProductTP/0/SaveTemp', dto).then(function (response) {
                if (response.data != "" || response.data != null) {
                    //HFC.DisplayAlert(response.data.Status);
                    $scope.Guid = response.data;
                    $scope.productadd.$setPristine();
                    window.location.href = "#!/vendorProduct" + '/' + $scope.Guid;
                }
                else {
                    HFC.DisplayAlert(response.data);
                    return;
                }
            });
        }

        

        $scope.Pdt_categ_List = function (value) {
            $scope.Pdt_categ_Option = {
                optionLabel: {
                    ProductCategory: "Select",
                    ProductCategoryId: ""
                },
                dataTextField: "ProductCategory",
                dataValueField: "ProductCategoryId",
                value: $scope.Product.ProductSubCategory,
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val = this.value();
                    if (val != null && val != "") {
                        this.element.removeClass("required-error")
                        $scope.Subcategory(val);
                        if ($("#Pdt_subcateg").data("kendoDropDownList") == undefined) {
                            $scope.Pdt_subcateg_List(val);
                        } else {
                            $("#Pdt_subcateg").data("kendoDropDownList").dataSource.data($scope.ProductSubCategoryList);
                        }
                    }
                },
                dataSource: {
                    transport: {
                        read: function (option) {
                            $http.get('/api/lookup/0/ProductCategory?Dropcategory=' + value).then(function (data) {
                                option.success(data.data);
                            });
                        }
                    }
                },
            };
        }

        $scope.Pdt_subcateg_List = function (value) {
            $scope.Pdt_subcateg_Option = {
                cache: false,
                optionLabel: {
                    ProductCategory: "Select",
                    ProductCategoryId: ""
                },
                dataTextField: "ProductCategory",
                dataValueField: "ProductCategoryId",
                filter: "contains",
                valuePrimitive: true,
                change: function () {
                    var val = this.value();
                    if (val != null && val != "") {
                        this.element.removeClass("required-error")
                    }
                }, dataSource: {
                    transport: {
                        read: function (option) {
                            $http.get('/api/lookup/' + value + '/ProductSubCategory').then(function (data) {
                                option.success(data.data);
                            });
                        }
                    }
                },
            };
            //$('#Pdt_subcateg').data('kendoDropDownList').dataSource.read();
        }

        $scope.cancel = function (productid, franchiseid) {
            if (productid == "" && franchiseid == undefined) {
                window.location = "#!/productSearch";
            }
            else {
                window.location.href = "#!/productDetail/" + productid + '/' + franchiseid;
            }
        }

        //to clear the subcategory if any other option is been selected
        $scope.ClearProductSubCategory = function () {
            $scope.Product.ProductSubCategory = '';
        }

        $scope.CreateNewProduct = function () {
            window.location = "#!/productCreate";
        }

        $scope.GoToFirstPage = function () {
            $scope.ProductSearchService.Pagination.page = 1;
            $scope.ApplyFilter();
        }

        $scope.GoToPreviousPage = function () {
            if ($scope.ProductSearchService.Pagination.page > 1) {
                $scope.ProductSearchService.Pagination.page = +($scope.ProductSearchService.Pagination.page) - 1;
                $scope.ApplyFilter();
            }
        }

        $scope.GoToNextPage = function () {

            if ($scope.ProductSearchService.Pagination.page == $scope.ProductSearchService.Pagination.pageTotal) {
                return;
            }
            $scope.ProductSearchService.Pagination.page = +($scope.ProductSearchService.Pagination.page) + 1;
            $scope.ApplyFilter();
        }

        $scope.GoToLastPage = function () {
            $scope.ProductSearchService.Pagination.page = $scope.ProductSearchService.Pagination.pageTotal;
            $scope.ApplyFilter();
        }

        $scope.ChangePageSize = function () {
            $scope.ApplyFilter();
        }



        $scope.ApplyFilter = function (isClickedFromUI) {

            var data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };

            if (isClickedFromUI) {
                data.pageIndex = 1;
                $scope.ProductSearchService.Pagination.page = 1;
            }

            if ($scope.selectedSales) {
                data.salesPersonIds = $scope.selectedSales;
            }

            if ($scope.selectedJobsStatuses) {
                data.jobStatusIds = $scope.selectedJobsStatuses;
            }

            if ($scope.selectedProductStatuses) {
                data.ProductStatusIds = $scope.selectedProductStatuses;
            }

            if ($scope.selectedSources) {
                data.sourceIds = $scope.selectedSources;
            }

            if ($scope.invoiceStatusesSearch) {
                data.invoiceStatuses = $scope.invoiceStatusesSearch;
            }

            if ($scope.ForSearchstartDate)
                data.createdOnUtcStart = $scope.ForSearchstartDate;
            if ($scope.ForSearchendDate)
                data.createdOnUtcEnd = $scope.ForSearchendDate;

            data.searchTerm = $('#search-box').val();

            data.searchFilter = $('#search-type').find('.active').find('input').val();

            if ($scope.SearchTerm) {
                data.searchTerm = $scope.SearchTerm;
            }

            data.pageIndex = $scope.ProductSearchService.Pagination.page;
            data.pageSize = $scope.ProductSearchService.Pagination.size;

            data.ForSearchstartDate = $scope.ForSearchstartDate;
            data.ForSearchendDate = $scope.ForSearchendDate;

            data.selectedDateText = $scope.selectedDateText;

            if ($routeParams.commercialType) {
                data.commercialType = $routeParams.commercialType;
            }

            if ($routeParams.isReportSearch) {
                data.isReportSearch = $routeParams.isReportSearch;
            }
            ProductSearchService.Get(data);

        }

        $scope.ProductSearchService.Get();
        $scope.ViewDetails();

        // testing

        $scope.MyTaskGridOptions = {
            dataSource: {
                type: "json",
                transport: {
                    cache: false,
                    read: {
                        url: "/api/ProductTP/" + $scope.Productkey + "/GetHistoryProductPricingLog",
                    }
                },
                schema: {
                    model: {
                        fields: {
                            CreatedOn: { type: 'date', editable: false },
                            Cost: { editable: false },
                            UnitPrice: { editable: false },
                            UserName: { editable: false },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            sortable: true,
            resizable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isempty: "Empty",
                        isnotempty: "Not empty"


                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gte: "Greater than or equal to",
                        lte: "Less than or equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 25,
                scrollable: true
            },
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "CreatedOn", title: "Date Changed",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "ProductFilterCalender", offsetvalue ),
                    template: function (dataitem) {
                        return HFCService.KendoDateFormat(dataitem.CreatedOn);
                    }
                    //"#=  kendo.toString(kendo.parseDate(CreatedOn), 'yyyy-MM-dd') #"
                },
                {
                    field: "Cost",
                    title: "Cost",
                    filterable: true,
                    //template: "#= kendo.toString(Cost, 'c') #"
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</span>";
                    },
                },
                {
                    field: "UnitPrice",
                    title: "Unit Price",
                    // template: "#= kendo.toString(UnitPrice, 'c') #"
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.UnitPrice) + "</span>";
                    },
                },
                {
                    field: "UserName", title: "Changed By"
                }
            ]

        };

    }

]);
