﻿/***************************************************************************\
Module Name:  AccountCommunicationListingController.js - Accont AngularJS
Project: HFC
Created on: 15 Oct 2020
Created By:
Copyright:
Description: Communication log information
Change History:
Date  By  Description

\***************************************************************************/

'use strict';

app.controller('AccountCommunicationListingController', [
    '$scope', '$routeParams', '$rootScope', '$http', '$window',
    '$location', '$modal', 'CalendarService', 'NoteServiceTP', 'PersonService', 'FileprintService', 'LeadService', 'LeadAccountService',
    'AccountService', 'HFCService', 'NavbarService', 'EmailQuoteService', 'CommunicationService', 'kendotooltipService','$sce',
function (
    $scope, $routeParams, $rootScope, $http, $window,
    $location, $modal, CalendarService, NoteServiceTP, PersonService, FileprintService, leadService, leadAccountService, AccountService,
    HFCService, NavbarService, EmailQuoteService, CommunicationService, kendotooltipService,$sce) {

    var self = this;
    //self.Lead = {};
    $scope.HFCService = HFCService;
    $scope.BrandId = $scope.HFCService.CurrentBrand;
    $scope.NoteServiceTP = NoteServiceTP;
    $scope.PersonService = PersonService;
    $scope.FileprintService = FileprintService;
    $scope.EmailQuoteService = EmailQuoteService;
    $scope.CommunicationService = CommunicationService;
    $scope.communicationList = {};
    $scope.errorMessage = "";
    $scope.flag = false;
    var offsetvalue = 0;
    
    self.accountId = $routeParams.accountId;

    //Render html properly in subject pop-up
    $scope.trustAsHtml = function (html) {
        return $sce.trustAsHtml(html);
    }

    $scope.NavbarService = NavbarService;
    var ul = $location.absUrl().split('#!')[1];
   
    if (ul.includes('accountCommunicationLog')) {
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesAccountsTab();
    }
   

    $scope.Permission = {};
   
    var Accountpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Account')
    var AccountCommunicationpermission = Accountpermission.SpecialPermission.find(x => x.PermissionCode == 'AccountCommunication').CanAccess;
    $scope.Permission.ListCommunicationAccount = AccountCommunicationpermission; 
    

    $scope.Permission.AddAccount = Accountpermission.CanCreate; 
    $scope.Permission.AccountEdit = Accountpermission.CanUpdate;

    //For Appointment
    HFCService.GetUrl();

    //https://rclayton.silvrback.com/pubsub-controller-communication
    // Sample publisher to update the grid after editing a calender
    // or newly added calendar. This code should be moved to the new 
    // appointment popup.
    self.updateCommunicationGrid = function () {
        $rootScope.$broadcast("message_Communication_update", new Date());
    }
   
    $scope.modalState = {
        loaded: true
    }

    $scope.$watch('modalState.loaded', function () {
        var loadingElement = document.getElementById("loading");

        if (!$scope.modalState.loaded) {
            loadingElement.style.display = "block";
        } else {
            loadingElement.style.display = "none";
        }
    });

    function errorHandler(reseponse) {
        

        // Inform that the modal is ready to consume:
        $scope.modalState.loaded = true;
        HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
    }

    // - Fetch Account values
    if (self.accountId > 0) {
        // Inform that the modal is not yet ready
        $scope.modalState.loaded = false;
        $http.get('/api/accounts/' + self.accountId).then(function (response) {
        //leadAccountService.Get(self.accountId, function (response) {
            var account = response.data.Account;
            self.Account = account;

            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFCService.setHeaderTitle("Account Communication #" + self.accountId + " - " + self.Account.PrimCustomer.FirstName + " " + self.Account.PrimCustomer.LastName);
        }, errorHandler);
    }
     // - End

    // -Fetch grid data & populate.
    $scope.populateAccountCommunications = function () {
        if ($scope.SelectedFilterOption == undefined)
            $scope.SelectedFilterOption = null;
        $scope.CommunicationService.getAccountCommunication(self.accountId, $scope.SelectedFilterOption)
            .then(function (response) {
                $scope.communicationList = response.data;
                //$scope.ShowOrder = true;
                //$scope.ComminicationLogGrid();
                CommunicationService.setControllerMethod(null);
                var grid = $("#gridCommunicationLog").data("kendoGrid");
                if (grid)
                grid.setDataSource($scope.communicationList);
            },
                function (responseError) {
                    $scope.errorMessage = responseError.statusText;
                    
                });
    }
    $scope.populateAccountCommunications();
    // - End

    //Grid Code
    $scope.ComminicationLogGrid = function () {
        $scope.AccountCommunication_log = {
            dataSource: $scope.communicationList,
            error: function (e) {
                HFC.DisplayAlert(e.errorThrown);
            },
            dataBound: function (e) {
                if (this.dataSource.view().length == 0) {
                    // The Grid contains No recrods so hide the footer.
                    $('.k-pager-nav').hide();
                    $('.k-pager-numbers').hide();
                    $('.k-pager-sizes').hide();
                } else {
                    // The Grid contains recrods so show the footer.
                    $('.k-pager-nav').show();
                    $('.k-pager-numbers').show();
                    $('.k-pager-sizes').show();
                }
                if (kendotooltipService.columnWidth) {
                    kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                } else if (window.innerWidth < 1280) {
                    kendotooltipService.restrictTooltip(null);
                }

            },
            resizable: true,
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "SentEmailId",
                    title: "SentEmailId",
                    type: "number",
                    hidden: true,
                },
                {
                    field: "DateValue",
                    title: "Date",
                    template: function (dataitem) {
                        if (dataitem.DateValue != null && dataitem.DateValue != "")
                            return HFCService.KendoDateFormat(dataitem.DateValue)
                        else return "";
                        
                    },
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "AccountLogFilterCalendar", offsetvalue),
                },
                {
                    field: "Type",
                    title: "Type",
                    filterable: {
                        multi: true,
                        search: true
                    }
                },
                {
                    field: "RecipientValue",
                    title: "User",
                    template: function (dataitem) {
                        if (dataitem.RecipientValue != null && dataitem.RecipientValue != "")
                            return maskedDisplayPhone(dataitem.RecipientValue);
                        else return "";
                    },
                    filterable: {
                        //multi: true,
                        search: true
                    } 
                },
                {
                    field: "SubjectDescription",
                    title: "Description/Email Subject",
                    template: function (dataitem) {
                        if ((dataitem.TemplateId != null) || (dataitem.TextingTemplateTypeId != null)) {

                            return "<span><div style='color: rgb(61,125,139);cursor: pointer;' ng-click='displayemaildata(" + dataitem.SentEmailId + ")'>" + dataitem.SubjectDescription + "</div></span>";
                        } else return "<span><div class='description'>" + dataitem.SubjectDescription + "</div></span>";
                    }
                },

                

            ],
            //filterable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isempty: "Empty",
                        isnotempty: "Not empty"


                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gte: "Greater than or equal to",
                        lte: "Less than or equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },

            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                change: function (e) {
                    var myDiv = $('.k-grid-content.k-auto-scrollable');
                    myDiv[0].scrollTop = 0;
                }
            },
        };
        

    };
    // - End

    $scope.ComminicationLogGrid();

    function maskPhoneNumber(text) {
        if (text != null && text !== undefined) {
            return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
        } else {
            return '';
        }
    }
    //phone num mask
    function maskedDisplayPhone(phone) {
        var Div;
        if (phone) {
            Div = "";
            if (phone && phone != "")
                Div += maskPhoneNumber(phone);
            
        } else {
            Div = "";
        }
        return Div;
    }

    //Resend E-mail confirmation pop-up for email need to send
    $scope.reSend = function (emailId) {

        $scope.ResendMailId = emailId;
        var resendurl = '/api/Communication/' + $scope.ResendMailId + '/GetResendEmailData';
        $http.get(resendurl).then(function (response) {
            var ResendEmaildata = response.data;
            $scope.ShowEmail = ResendEmaildata;
            $("#Show_ReSentEmailData").modal("show");
        })
    }
    // End

    //Re-Send the e-mail again 
    $scope.ReSentEmailData = function (Emailid) {

        var Id = Emailid;
        $("#Show_ReSentEmailData").modal("hide");
        var reminderurl = '/api/Communication/' + Id + '/ResendEmail';
        $http.get(reminderurl).then(function (response) {
            var res = response.data;
            if (res == true) {
                HFC.DisplaySuccess("Success");
                
                $scope.populateAccountCommunications();
            }
            else {
                HFC.DisplayAlert("Error");
            }
        });
    }
    //- End

    //Close resend e-mail confirmation pop-up
    $scope.Close_ReSentEmailData = function () {
        $("#Show_ReSentEmailData").modal("hide");
        
    }
    //- end

    // - add/Create new account
    $scope.CreateNewAccount = function () {
        $window.location.href = "#!/account";
    }
    //-end
    // - edit particular account
    $scope.EditAccount = function (accountId) {
        $window.location.href = "#!/accountEdit/" + accountId;
    }
    // - end

    $scope.displayemaildata = function (Id) {
        var id = Id;
        var url = '/api/Communication/' + id + '/GetEmaildeatils';
        $http.get(url).then(function (response) {
            var data = response.data;
            $scope.showdata = data;
            if (data.Type == "Email" && data.Body != "" && data.Body != null)
                $("#Show_EmailSentData").modal("show");
            else if (data.Type == "Text" && data.Body != "" && data.Body != null)
                $("#Show_TextingData").modal("show");
            else $("#Show_AddedData").modal("show");
        });
    }

    $scope.ClosePop_up = function () {
        $("#Show_EmailSentData").modal("hide");
    }

    $scope.ClosePop_up_Texting = function () {
        $("#Show_TextingData").modal("hide");
    }

    $scope.ClosePop_up_Added = function () {
        $("#Show_AddedData").modal("hide");
    }


    // - filter selection code
    $scope.FilterBySelectedLog = function () {
        
        $scope.SelectedFilterOption = $scope.Valueselected();
        $scope.populateAccountCommunications();
    }
    // - end

    $scope.Valueselected = function () {
        var arr = [];
        var result = '';
        if ($('#Check1').is(":checked")) {
            result = 18001;
            arr.push(result);
        }
        if ($('#Check2').is(":checked")) {
            result = 18002;
            arr.push(result);
        }
        if ($('#Check3').is(":checked")) {
            result = 18003;
            arr.push(result);
        }
        if ($('#Check4').is(":checked")) {
            result = 18004;
            arr.push(result);
        }
        if ($('#Check5').is(":checked")) {
            result = 18005;
            arr.push(result);
        }

        return arr;
    }

    //function InitializeCommunication() {
    //    $scope.AddCommunications = {
    //        Email: false,
    //        Text: false,
    //        Phone: false,
    //        Meeting: false,
    //        Mail: false,
    //        CreatedDate: '',
    //        Description: ''
    //    };
    //}
    //InitializeCommunication();

    //show add log pop-up & close code
    $scope.AddCommunicationLog = function () {
        
        $("#Show_AddCommunicationLog").modal("show");
        CommunicationService.setControllerMethod($scope.populateAccountCommunications);
    }
    
    //$scope.CloseAddCommunication = function () {
    //   
    //    $("#Show_AddCommunicationLog").modal("hide");
    //}
    //-- end

    //Save communication log
    //$scope.SaveCommunication = function () {
    //    
    //    $scope.submitted = true;
    //    var dto = $scope.AddCommunications;
    //    if (dto.Email == false && dto.Text == false && dto.Phone == false && dto.Meeting == false && dto.Mail == false) {
    //        HFC.DisplayAlert("Plese select any one value!");
    //        return;
    //    }
    //    if ($scope.Add_Communication.$invalid) {
    //        return;
    //    }

    //    var saveAddurl = '/api/CONTROLLER/0/METHOD?Model=' + dto;
    //    $http.get(saveAddurl).then(function (response) {
    //        var res = response.data;
    //        if (res == true) {
    //            //HFC.DisplaySuccess("Success");
    //            $("#Show_AddCommunicationLog").modal("hide");
    //            InitializeCommunication();
    //            $scope.populateAccountCommunications();
    //        }
    //        else {
    //            $("#Show_AddCommunicationLog").modal("hide");
    //            InitializeCommunication();
    //            HFC.DisplayAlert("Error");
    //        }
    //    });

    //}

    $scope.EmailQuoteService.EmailSuccessCallback = function () {
        $scope.populateAccountCommunications();
        //self.updateCommunicationGrid();
    }

    
}
]);




