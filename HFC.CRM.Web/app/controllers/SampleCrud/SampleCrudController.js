﻿
'use strict';

app.controller('SampleCrudController', ['$scope', '$routeParams', '$rootScope'
    , '$window', '$location', '$http', 'HFCService', 'ErrorService', 'NavbarService'
    , '$modal', 'DropzoneService',
    function ($scope, $routeParams, $rootScope
        , $window, $location, $http, HFCService, ErrorService, NavbarService
        , $modal, DropzoneService) {

        //we will use local ctrl var so we don't clutter up $scope that doesn't need to be shared with nested controllers and directives
        var ctrl = this;

        var sampleId = $routeParams.sampleId;

        // This is part of the sales module, so select the sales menu
        $scope.NavbarService = NavbarService;

        $scope.cancel = function () {
            $scope.showValidate = false;
            window.location.href = '#!/leadsSearch//';
        }
		
        $scope.return = function () {
            $scope.showValidate = false;
            window.location.href = '#!/leadsSearch//';
        }

        $scope.DropzoneOption = {
                    'options': {
                        //   passed into the Dropzone constructor
                        url: '/Leads/UploadFile/' + sampleId, //+ "/mydescription" , //The webapi to be called when a file uploaded...???
                        paramName: "file", // The name that will be used to transfer the file
                        maxFilesize: 10, // MB
                        parallelUploads: 5,
                        addRemoveLinks: true,
                        autoProcessQueue: false,
                        //accept: function (file, done) {
                        //    // This modal in our case can be hardcoded.
                        //    $scope.closeModal = function(ok, modal) {
                        //        if (!ok) {
                        //            modal.$hide();
                        //            return done("File is not uploaded");
                        //        }

                        //        modal.$hide();
                        //        $scope.fileDescription = modal.fileDescription;
                        //        $scope.fileCategory = modal.fileCategory;

                        //        return done();
                        //    }

                        //    $modal({ show: true, title: 'File Upload', scope: $scope, template: '/templates/NG/upload-confirm-modal-tp.html' });
                        //},
                        init: function() {
                            debugger;
                            var submitButton = document.querySelector("#act-on-upload")
                            var myDropzonexyz = this;
                            submitButton.addEventListener("click", function () {
                                alert("looks like working");
                                myDropzonexyz.processQueue();
                            });

                            // Forces to select only one file.
                            //this.hiddenFileInput.removeAttribute('multiple');
                        },
                        drop: function(event, err) {
                            if (event.dataTransfer.files.length > 1) {
                                alert('One file at time');
                                event.stopPropagation();
                                event.preventDefault();
                                event.dataTransfer.files = [];
                            }
                        },
                        thumbnailWidth: 120,
                        thumbnailHeight: 120,
                        addedFiles: 0,
                        dictRemoveFileConfirmation: "This will permanently remove the file, do you want to continue?",
                        //acceptedFiles: "image/*,application/pdf,.eps,.tiff,.doc,.docx,.html,.htm,.txt,.csv,.xls,xlsx",
                        acceptedFiles: $scope.allowedfiles,
                        previewTemplate: '<div class="dz-preview dz-file-preview"><a target="_blank" class="dz-details" style="display:block"><div class="dz-filename"><span data-dz-name></span></div><div class="dz-size" data-dz-size></div><img data-dz-thumbnail /></a><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-success-mark"><span>✔</span></div><div class="dz-error-mark"><span>✘</span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div>'
                    },
                    'eventHandlers': {
                        'addedfile': function(file) {
                        },
                        "removedfile": function (file) {
                            debugger;
                            var fileId = file.fileId;
                            if (fileId) { //&& fileId > 0) {
                                $http.delete('/jobs/file/' + fileId).then(
                                    function(res) {
                                        $(file.previewElement).popover('destroy');
                                        var myDropzone = null;
                                        $.each(DropzoneService.Dropzones, function(i, dz) {
                                            if (dz.id == "dz12345"){ //+ $scope.sampleId) {
                                                myDropzone = dz.dropzonetp;
                                                return;
                                            }
                                        });

                                        $scope.FilesCount = $scope.FilesCount - 1;
                                        if (myDropzone && myDropzone.files.length == 0)
                                            $(file.previewElement).find(".dz-message").show();
                                        HFC.DisplaySuccess(file.name + " deleted");
                                    }, function(res) {
                                        self.options.addedfile.call(self, file); //add it back if it failed deletion for any reason
                                        self.options.thumbnail.call(self, file, file.ThumbUrl || $(file.previewTemplate).find(".dz-details").attr("href")); //add it back if it failed deletion for any reason								
                                    }
                                );
                            }
                        },
                        'sending': function (file, xhr, formData) {
                            debugger;
                            formData.append('Description', $scope.fileDescription);
                            formData.append('Category', $scope.fileCategory);
                            //can add additional info here if needed
                        },
                        'success': function(file, response) {
                            file.fileId = response.FileId;
                            file.description = $scope.fileDescription;
                            file.category = $scope.fileCategory;
                            file.createdOn = new Date();
                            file.creatorFullName = HFCService.CurrentUser.FullName;
                            file.Url = response.Url;
                            file.ThumbUrl = response.ThumbUrl;

                            $scope.FilesCount = $scope.FilesCount + 1;
                            $scope.$apply();

                            $scope.fileDescription = "";
                            $(file.previewElement).find(".dz-details").attr("href", file.Url);
                            HFC.DisplaySuccess(file.name + " uploaded");
                            $(file.previewElement).popover({
                                trigger: 'hover',
                                title: file.creatorFullName + ' - ' + new XDate(file.createdOn, true).toString("M/d/yy h:mm tt"),
                                content: (file.description || "No description") + " (" + (file.category || "No Catgory") + ")",
                                placement: 'auto',
                                animation: !HFC.Browser.isMobile()
                            });
                        }
                    }
                };
       

        //To save Qualify
        $scope.SaveSampleCrud = function () {
            
            
            HFC.DisplayAlert("Qualify saved");

            // Move the url the Lead listing page.
            //var goLeadQualify = '#!/leadsQualify/' + $scope.Lead.LeadId;
            //$window.open(goLeadQualify, '_self');
        }
        

        if (sampleId > 0) {
            // we are in edit mode.
            
        }


    }
]);

