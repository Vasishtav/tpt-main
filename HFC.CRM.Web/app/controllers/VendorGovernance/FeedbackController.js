﻿'use strict';
var feedback = angular.module('feedbackModule', [])
.controller('FeedbackController',
    ['$scope', '$http', '$window', '$location', 'HFCService', 'NavbarServiceVendor', 'NavbarServiceCP', 'NavbarServicePIC', 'kendotooltipService', '$routeParams', 'FileUploadService', 'FranchiseService', 'NavbarService',
function ($scope, $http, $window, $location, HFCService, NavbarServiceVendor, NavbarServiceCP, NavbarServicePIC, kendotooltipService, $routeParams, FileUploadService, FranchiseService, NavbarService) {
    $scope.HFCService = HFCService;
    $scope.Roles = $scope.HFCService.Roles;
    $scope.FranciseLevelVendorMng = HFCService.FranciseLevelVendorMng;
    $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
    $scope.BrandId = HFCService.CurrentBrand;
    $scope.RolesString = "";
    var offsetvalue = 0;
    $scope.VendorLogin = false;
    $scope.PicLogin = false;
    $scope.FranchiseLogin = false;
    $scope.AdminLogin = false;
    //flag to show grid loader on initial load
    $scope.ProgressLoad = true;
    if ($scope.Roles != "Franchise") {
        for (i = 0; i < $scope.Roles.length; i++) {
            if ($scope.RolesString == "")
                $scope.RolesString = $scope.Roles[i].RoleName;
            else
                $scope.RolesString = $scope.RolesString + ', ' + $scope.Roles[i].RoleName;
        }
    }
    else
        $scope.RolesString = "Franchise";

    if ($scope.RolesString == "" || $scope.RolesString.includes("HFC Product Strategy Mgt (PSM)")) {
        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.NavbarServiceCP.VendorGovernanceTab();
        if (!$scope.RolesString.includes("HFC Product Strategy Mgt (PSM)"))
            $scope.AdminLogin = true;
    }
    else if ($scope.RolesString.includes("Vendor")) {
        $scope.NavbarServiceVendor = NavbarServiceVendor;
        $scope.NavbarServiceVendor.SelectHomeTab();
        $scope.VendorLogin = true;
    }
    else if ($scope.RolesString.includes("Supply Chain Partner (PIC)")) {
        $scope.NavbarServicePIC = NavbarServicePIC;
        $scope.NavbarServicePIC.PICTab();
        $scope.PicLogin = true;
    }
    else if ($scope.RolesString.includes("Franchise")) {
        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableVendorManagementTab();
        $scope.FranchiseLogin = true;
    }

    //permission
    $scope.Permission = {};
    if ($scope.RolesString.includes("Franchise") && $scope.BrandId == 1) {
        //permission for vendor management
        var Vendormanagementpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'VendorManagement')
        $scope.Permission.ListVendorData = Vendormanagementpermission.CanRead;
        $scope.Permission.ViewVendorData = Vendormanagementpermission.CanRead;
        $scope.Permission.EditVendorData = Vendormanagementpermission.CanUpdate;
    }
    else if ($scope.RolesString.includes("Franchise") && $scope.BrandId != 1) {
        $scope.FranciseLevelVendorMng = false;
        $scope.FranciseLevelCase = false;
        $scope.Permission.ListVendorData = false;
        $scope.Permission.ViewVendorData = false;
        $scope.Permission.EditVendorData = false;
    }
    else {
        $scope.FranciseLevelVendorMng = true;
        $scope.FranciseLevelCase = true;
        $scope.Permission.ListVendorData = true;
		$scope.Permission.ViewVendorData = true;
		$scope.Permission.EditVendorData = true;
    }

    $scope.FranchiseService = FranchiseService;
    $scope.searchvalue = "";
    $scope.IsCloseReason = false;
    $scope.IsChangeRequestSubmitted = false;
    $scope.IsFeedbackClosed = false;
    $scope.Closed = false;
    $scope.Feedbacklist = {};
    //for displaying loader
    $scope.modalState = {
        loaded: true
    }

    $scope.$watch('modalState.loaded', function () {
        var loadingElement = document.getElementById("loading");

        if (!$scope.modalState.loaded) {
            loadingElement.style.display = "block";
        } else {
            loadingElement.style.display = "none";
        }
    });
    //set window title
    HFCService.setHeaderTitle("Feedback  #");
    $scope.InitialLoad = function () {
        if (($window.location.href.toUpperCase().includes('FEEDBACKKANBANVIEW'))) {
            $scope.modalState.loaded = false;
        }
        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
        if ($scope.RolesString.includes("Vendor")) {
            $http.get('/api/Case/0/GetFeedbackListByVendor?IncludeClosed=' + $scope.Closed).then(function (response) {
                $scope.ds = response.data;
                var grid = $('#gridFeedbackSearch').data("kendoGrid");
                var dataSource = new kendo.data.DataSource({
                    data: $scope.ds, pageSize: 25,
                    schema: {
                        model: {
                            fields: {
                                Created: { type: 'date', editable: false },
                                LastModified: { type: 'date', editable: false },
                                DaysOpen: { type: 'number', editable: false },
                                DaysSinceModified: { type: 'number', editable: false },
                            }
                        }
                    },
                });
                dataSource.read();
                $scope.Feedbacklist = $scope.ds;
                $scope.copyFeedbacklist = $scope.Feedbacklist;
                if (grid) {
                    grid.setDataSource(dataSource);
                    grid.dataSource.page(1);
                    $scope.FeedbackgridSearch();
                }
                else {
                    $scope.GetKanban();
                    $scope.KanbanSearch($scope.searchvalue);
                    $scope.modalState.loaded = true;
                }
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }, function (error) {
                HFC.DisplayAlert(error);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                $scope.modalState.loaded = true;
            });
        }
        else if ($scope.RolesString.includes("Franchise")) {
            $http.get('/api/Case/0/GetFeedbackListByFranchise?IncludeClosed=' + $scope.Closed).then(function (response) {
                $scope.ds = response.data;
                var grid = $('#gridFeedbackSearch').data("kendoGrid");
                var dataSource = new kendo.data.DataSource({
                    data: $scope.ds, pageSize: 25,
                    schema: {
                        model: {
                            fields: {
                                Created: { type: 'date', editable: false },
                                LastModified: { type: 'date', editable: false },
                                DaysOpen: { type: 'number', editable: false },
                                DaysSinceModified: { type: 'number', editable: false },
                            }
                        }
                    },
                });
                dataSource.read();
                $scope.Feedbacklist = $scope.ds;
                $scope.copyFeedbacklist = $scope.Feedbacklist;
                if (grid) {
                    grid.setDataSource(dataSource);
                    grid.dataSource.page(1);
                    $scope.FeedbackgridSearch();
                }
                else {
                    $scope.GetKanban();
                    $scope.KanbanSearch($scope.searchvalue);
                    $scope.modalState.loaded = true;
                }
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }, function (error) {
                HFC.DisplayAlert(error);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                $scope.modalState.loaded = true;
            });
        }
        else {
            $http.get('/api/Case/0/GetFeedbackList?IncludeClosed=' + $scope.Closed).then(function (response) {
                $scope.ds = response.data;
                var grid = $('#gridFeedbackSearch').data("kendoGrid");
                var dataSource = new kendo.data.DataSource({
                    data: $scope.ds, pageSize: 25,
                    schema: {
                        model: {
                            fields: {
                                Created: { type: 'date', editable: false },
                                LastModified: { type: 'date', editable: false },
                                DaysOpen: { type: 'number', editable: false },
                                DaysSinceModified: { type: 'number', editable: false },
                            }
                        }
                    },
                });
                dataSource.read();
                $scope.Feedbacklist = $scope.ds;
                $scope.copyFeedbacklist = $scope.Feedbacklist;
                if (grid) {
                    grid.setDataSource(dataSource);
                    grid.dataSource.page(1);
                    $scope.FeedbackgridSearch();
                }
                else {
                    $scope.GetKanban();
                    $scope.KanbanSearch($scope.searchvalue);
                    $scope.modalState.loaded = true;
                }
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);

            }, function (error) {
                HFC.DisplayAlert(error);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                $scope.modalState.loaded = true;
            });
        }

    }

    if ($window.location.href.toUpperCase().includes('FEEDBACK') && !$window.location.href.toUpperCase().includes('FEEDBACKUPDATE') && !$window.location.href.toUpperCase().includes('FEEDBACKVIEW'))
        $scope.InitialLoad();

    //kanban view
    $scope.GetKanban = function () {
        $scope.TodoKanbanDataList = [];
        $scope.InprocessKanbanDataList = [];
        $scope.CompleteKanbanDataList = [];
        for (var i = 0; i < $scope.Feedbacklist.length; i++) {
            if ($scope.Feedbacklist[i].Status == "Submitted")
                $scope.TodoKanbanDataList.push($scope.Feedbacklist[i]);
            else if ($scope.Feedbacklist[i].Status == "Vendor Review")
                $scope.InprocessKanbanDataList.push($scope.Feedbacklist[i]);
            else if ($scope.Feedbacklist[i].Status == "Closed")
                $scope.CompleteKanbanDataList.push($scope.Feedbacklist[i]);

        }
        setTimeout(function () {
            $scope.fixCardHeights();
        }, 50);
    }

    $scope.Change_List = function (value) {
        if (value)
            $scope.Closed = true;
        else
            $scope.Closed = false;

        $scope.InitialLoad();
    }

    //kanban view

    //$scope.PersonId = FranchiseService.UserData.PersonId;
    //console.log($scope.PersonId);

    $scope.KanbanSearch = function (searchvalue) {
        searchvalue = searchvalue.toLowerCase();
        if (searchvalue == "") {
            $scope.ClearKanbanSearch();
            return;
        }
        $scope.KanbanDataList = [];
        for (var i = 0; i < $scope.copyFeedbacklist.length; i++) {
            var id = ($scope.copyFeedbacklist[i].ID) ? $scope.copyFeedbacklist[i].ID.toLowerCase() : "";
            var originator = ($scope.copyFeedbacklist[i].Originator) ? $scope.copyFeedbacklist[i].Originator.toLowerCase() : "";
            var changetype = 'feedback';
            var status = ($scope.copyFeedbacklist[i].Status) ? $scope.copyFeedbacklist[i].Status.toLowerCase() : "";
            if ($scope.copyFeedbacklist[i].CloseReason) {
                var statusclosereason = $scope.copyFeedbacklist[i].CloseReason.toLowerCase();
                status = status + " - " + statusclosereason;
            }
            var vendor = ($scope.copyFeedbacklist[i].Vendor) ? $scope.copyFeedbacklist[i].Vendor.toLowerCase() : "";

            if (id.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.copyFeedbacklist[i]);
            }
            else if (originator.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.copyFeedbacklist[i]);
            }
            else if (changetype.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.copyFeedbacklist[i]);
            }
            else if (status.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.copyFeedbacklist[i]);
            }
            else if (vendor.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.copyFeedbacklist[i]);
            }

        }
        $scope.Feedbacklist = $scope.KanbanDataList;
        $scope.GetKanban();
    }

    $scope.ClearKanbanSearch = function () {
        $scope.searchvalue = "";
        $scope.Feedbacklist = $scope.copyFeedbacklist;
        $scope.GetKanban();
    }

    $scope.gotoListview = function () {
        window.location.href = "#!/Feedback";
    }

    $scope.FeedbackgridSearchClear = function () {
        $('#FeedbackBoxValue').val('');
        $("#gridFeedbackSearch").data('kendoGrid').dataSource.filter({
        });
    }

    $scope.FeedbackgridSearch = function () {
        var searchValue = $('#FeedbackBoxValue').val();
        $("#gridFeedbackSearch").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
              {
                  field: "Vendor",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "ID",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "ErrorType",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "Status",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "DaysOpen",
                  operator: "eq",
                  //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                  value: searchValue
              },
              {
                  field: "DaysSinceModified",
                  operator: "eq",
                  //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                  value: searchValue
              },
            ]
        });
    }

    $scope.FeedbackgridList = {
        dataSource: {
            error: function (e) {
                HFC.DisplayAlert(e.errorThrown);
            },

        },
        batch: true,
        cache: false,
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    isempty: "Empty",
                    isnotempty: "Not empty"


                },
                number: {
                    eq: "Equal to",
                    neq: "Not equal to",
                    gte: "Greater than or equal to",
                    lte: "Less than or equal to"
                },
                date: {
                    gt: "After (Excludes)",
                    lt: "Before (Includes)"
                },
            }
        },
        resizable: true,
        editable: "inline",
        noRecords: { template: "No records found" },
        dataBound: function (e) {
            //for showing loader on initial load
            if ($scope.ProgressLoad == true)
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
            $scope.ProgressLoad = false;

            if (this.dataSource.view().length == 0) {
                // The Grid contains No recrods so hide the footer.
                $('.k-pager-nav').hide();
                $('.k-pager-numbers').hide();
                $('.k-pager-sizes').hide();
            } else {
                // The Grid contains recrods so show the footer.
                $('.k-pager-nav').show();
                $('.k-pager-numbers').show();
                $('.k-pager-sizes').show();
            }
            if (kendotooltipService.columnWidth) {
                kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
            } else if (window.innerWidth < 1280) {
                kendotooltipService.restrictTooltip(null);
            }
            $scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
        },
        columns: [
             {
                 field: "Vendor",
                 title: "Vendor",
                 width: "250px",
                 filterable: {
                     //multi: true,
                     search: true
                 },
                 template: function Vendor(dataItem) {
                     var template = "";
                     if (dataItem.Vendor) {
                         template += "<span >" + dataItem.Vendor + "</span>";
                         return template;
                     }
                     else {
                         return '';
                     }

                 }
             },
            {
                field: "ID",
                title: "ID",
                width: "150px",
                filterable: {
                    //multi: true,
                    search: true
                },
                template: function (dataItem) {
                    return "<a href='#!/FeedbackView/" + dataItem.CaseId + "'style='color: rgb(61,125,139);'>" + dataItem.ID + "</a>";
                },
            },
              {
                  field: "ErrorType",
                  title: "Error Type",
                  width: "140px",
                  filterable: { multi: true, search: true },
                  template: function ErrorType(dataItem) {
                      var template = "";
                      if (dataItem.ErrorType) {
                          template += "<span>" + dataItem.ErrorType + "</span>";
                          return template;
                      }
                      else {
                          return '';
                      }

                  }
              },
              {
                  field: "Status",
                  title: " Status",
                  width: "100px",
                  filterable: { multi: true, search: true },
                  template: function Status(dataItem) {
                      var template = "";
                      if (dataItem.Status) {
                          template += "<span>" + dataItem.Status + "</span>";
                          return template;
                      }
                      else {
                          return '';
                      }

                  },
              },
              {
                  field: "Created",
                  title: "Created",
                  width: "100px",
                  template: function Created(dataItem) {
                      var template = "";
                      if (dataItem.Created) {
                          template += "<span >" + HFCService.KendoDateFormat(dataItem.Created) + "</span>";
                          return template;
                      }
                      else {
                          return '';
                      }

                  },
                  filterable: HFCService.gridfilterableDate("date", "FeedbackCreatedFilterCalendar", offsetvalue),
              },
               {
                   field: "LastModified",
                   title: "Last Modified",
                   width: "150px",
                   template: function LastModified(dataItem) {
                       var template = "";
                       if (dataItem.LastModified) {
                           template += "<span>" + HFCService.KendoDateFormat(dataItem.LastModified) + "</span>";
                           return template;
                       }
                       else {
                           return '';
                       }

                   },
                   filterable: HFCService.gridfilterableDate("date", "FeedbackLastModifiedFilterCalendar", offsetvalue),
               },
             {
                 field: "DaysOpen",
                 title: " Days Open",
                 width: "150px",
                 filterable: {
                     //multi: true,
                     search: true
                 },
                 template: function DaysOpen(dataItem) {
                     var template = "";
                     if (dataItem.DaysOpen) {
                         template += "<span>" + dataItem.DaysOpen + "</span>";
                         return template;
                     }
                     else {
                         return '';
                     }

                 },
             },
            {
                field: " DaysSinceModified",
                title: " Days Since Modified",
                width: "150px",
                filterable: {
                    //multi: true,
                    search: true
                },
                template: function DaysSinceModified(dataItem) {
                    var template = "";
                    if (dataItem.DaysSinceModified) {
                        template += "<span>" + dataItem.DaysSinceModified + "</span>";
                        return template;
                    }
                    else {
                        return '';
                    }

                },
            },
        ],
        pageable: {
            refresh: true,
            pageSize: 25,
            pageSizes: [25, 50, 100],
            buttonCount: 5
        },
        columnResize: function (e) {
            if ($scope.PicLogin)
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
            else
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
            getUpdatedColumnList(e);
        }
    };


    //$scope.FeedbackgridList = kendotooltipService.setColumnWidth($scope.FeedbackgridList);
    var getUpdatedColumnList = function (e) {
        kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
    }
    $scope.gotoKanbanview = function () {
        window.location.href = "#!/FeedbackKanbanView";
    }

    //Feedback update    

    $scope.CaseId = $routeParams.CaseId;
    $scope.ReportDetails = {
        CaseFeedbackId: '',
        CaseId: '',
        CurrentStatus: '',
        CurrentStatusId: '',
        ReportID: '',
        Newstatus: '',
        Currentuser: '',
        Currenttime: '',
        CloseReason: '',
        ChangeRequest: '',
        ChangeCaseId: '',
        ProblemType: '',
        Vendor: '',
        VendorId: '',
        ProductCategory: '',
        ProblemDetails: '',
        PICJson: '',
        PICData: '',
        UploadedDate: '',
        FileSize: '',
        CreatedBy: '',
        CreatedOn: '',
        TouchpointURL: '',
        TouchpointVersion: '',
        BrowserDetails: '',
        LastUpdatedBy: '',
        LastUpdatedOn: '',
        CreatedByName: '',
        UpdatedBy: '',
        ChangeCaseNumber: '',
        FranchiseName: '',
        OwnerId: '',
        PhoneNumber: '',
        VendorAccountNo: ''
    };

    $scope.EditFeedback = function () {
        window.location.href = "#!/FeedbackUpdate/" + $scope.CaseId;
    };

    $scope.gotoFeedbackView = function () {
        //if (confirm('You will lose unsaved changes if you reload this page')) {
            window.location.href = "#!/FeedbackView/" + $scope.CaseId;
        //}
    };

    $("#PICData").click(function () {
        $("<a />", {
            "download": $scope.ReportDetails.ReportID + ".json",
            "href": "data:application/json," + encodeURIComponent(JSON.stringify($(this).data().value))
        }).appendTo("body")
        .click(function () {
            $(this).remove()
        })[0].click()
    });

    $("#FieldErrorsData").click(function () {
        $("<a />", {
            "download": $scope.ReportDetails.ReportID + "_FieldErrors.txt",
            "href": "data:application/csv," + encodeURIComponent(JSON.stringify($(this).data().value))
        }).appendTo("body")
        .click(function () {
            $(this).remove()
        })[0].click()
    })

    $scope.FileUploadService = FileUploadService;

    //get data on inital page load
    $scope.loadclosedlist = function () {
        $http.get('/api/Case/0/GetFeedbackChangeLookup?TableName=ClosedReason').then(function (response) {
            $scope.closedlist = response.data;
        });
    }

    $scope.loadChangeRequestList = function () {
        $http.get('/api/Case/' + $scope.ReportDetails.VendorId + '/GetChangeRequestCaseNumberByVendor').then(function (response) {
            $scope.ChangeRequestList = response.data;
        });
    }

    $scope.loadstatus = function () {
        $http.get('/api/Case/0/GetFeedbackChangeLookup?TableName=FeedbackStatus').then(function (response) {
            if (response.data != null) {
                if ($scope.RolesString == "" && $scope.ReportDetails.CurrentStatus == "Submitted")
                    response.data.splice(1, 1);
                else
                    response.data.splice(0, 1);
                $scope.statuslist = response.data;
            }
            //$scope.statuslist.splice(0, 1);
            //$scope.statuslist.splice(0, 2);
        });
    }
    $scope.UpdateFeedback = function () {
        $scope.modalState.loaded = false;
        if ($scope.ValidateFeedBackUpdate()) {
            $scope.modalState.loaded = true;
            return;
        }
        var feedback = $scope.ReportDetails;
        feedback.StatusId = $scope.ReportDetails.Newstatus;
        if (feedback.CloseReason != 14000) feedback.ChangeCaseId = 0;
        $http.post('/api/Case', $scope.ReportDetails).then(function (response) {
            //console.log(response);
            $scope.formFeedback.$setPristine();
            $scope.modalState.loaded = true;
            window.location.href = "#!/FeedbackView/" + $scope.CaseId;
        });
        //$scope.modalState.loaded = true;
    }

    $scope.Initialvalue = function () {
        $scope.modalState.loaded = false;
        $http.get('/api/Case/' + $scope.CaseId + '/GetConfiguratorFeedbackById?IsVendor=' + true).then(function (response) {
            //console.log(response);
            $scope.ReportDetails = response.data;
            $scope.ReportHistory();
            $scope.ReportDetails.Newstatus = response.data.CurrentStatusId;
            $scope.ReportDetails.Currentuser = FranchiseService.UserData != null ? FranchiseService.UserData.FullName : "";
            $scope.ReportDetails.Currenttime = HFCService.KendoDateTimeFormat(new Date($scope.ReportDetails.CurrentTime));
            $scope.ReportDetails.LastUpdatedOn = HFCService.KendoDateTimeFormat(new Date($scope.ReportDetails.LastUpdatedOn));
            $scope.ReportDetails.CreatedOn = HFCService.KendoDateTimeFormat(new Date($scope.ReportDetails.CreatedOn));
            $scope.ReportDetails.UploadedDate = $scope.ReportDetails.CreatedOn
            if ($scope.ReportDetails.PICJson != null && $scope.ReportDetails.PICJson != undefined)
                $scope.ReportDetails.FileSize = $scope.ReportDetails.PICJson != null ? (Object.keys($scope.ReportDetails.PICJson).length / 1024).toFixed(2) == 0 ? "0 KB" : (Object.keys($scope.ReportDetails.PICJson).length / 1024).toFixed(2) + ' KB' : "0 KB";
            else
                $scope.ReportDetails.FileSize = "0 KB";

            if ($scope.ReportDetails.FieldErrors != null && $scope.ReportDetails.FieldErrors != undefined)
                $scope.ReportDetails.FieldErrorsFileSize = $scope.ReportDetails.FieldErrors != null ? (Object.keys($scope.ReportDetails.FieldErrors).length / 1024).toFixed(2) == 0 ? "0 KB" : (Object.keys($scope.ReportDetails.FieldErrors).length / 1024).toFixed(2) + ' KB' : "0 KB";
            else
                $scope.ReportDetails.FieldErrorsFileSize = "0 KB";

            $scope.loadstatus();
            if ($scope.ReportDetails.CurrentStatus == "Closed") {
                $scope.IsCloseReason = true; // display closed reason if the status is closed
                $scope.IsFeedbackClosed = true;// to hide the edit button in view page
            }
            if ($scope.ReportDetails.CloseReason != null && $scope.ReportDetails.CloseReason == "Product Change Submitted") {
                $scope.IsChangeRequestSubmitted = true; // display change request case number if the closed reason product change submitted
            }
            $scope.modalState.loaded = true;
        }).catch(function (error) {
            $scope.modalState.loaded = true;
            HFC.DisplayAlert(error);
        });

        if ($scope.RolesString.includes("HFC Product Strategy Mgt (PSM)") || $scope.RolesString.includes("Supply Chain Partner (PIC)") || $scope.RolesString.includes("Franchise"))
            $scope.IsFeedbackClosed = true;
    }

    if (($window.location.href.toUpperCase().includes('FEEDBACKUPDATE/')))
        $scope.Initialvalue();

    if (($window.location.href.toUpperCase().includes('FEEDBACKVIEW/')))
        $scope.Initialvalue();

    $scope.StatusChange = function (value) {
        //$scope.formFeedback.$setPristine();
        var ClosedStatus = $.grep($scope.statuslist, function (e) { return e.Name == 'Closed' });

        if (value == ClosedStatus[0].Id) {
            $scope.loadclosedlist();
            $scope.IsCloseReason = true;
        } else {
            $scope.IsCloseReason = false;
            $scope.IsChangeRequestSubmitted = false;
            $scope.ReportDetails.CloseReason = '';
            $scope.ReportDetails.ChangeCaseId = '';
        }
        $scope.RemoveRequiredError();
    }

    $scope.CloseReasonChange = function (value) {
        if (value == 14000) {
            $scope.loadChangeRequestList();
            $scope.IsChangeRequestSubmitted = true;
        } else {
            $scope.IsChangeRequestSubmitted = false;
            $scope.ReportDetails.ChangeCaseId = '';
        }
        $scope.RemoveRequiredError();
    }

    $scope.ChangeRequestChange = function (value) {
        if (value) $scope.RemoveRequiredError();
    }
    $scope.EditCase = function () {
        window.location.href = "#!/FeedbackUpdate/" + $scope.CaseId
    }
    $scope.VendorAttachment = {
        dataSource: {
            error: function (e) {
                HFC.DisplayAlert(e.errorThrown);
            },
            schema: {
                model: {
                    Id: "Id",
                    fields: {
                        Id: { editable: false, nullable: true },
                        Attachment: { editable: false, type: "string" },
                        CreatedOn: { editable: false, type: "string" },
                        Size: { editable: false, type: "string" }
                    }
                }
            }
        },
        columns: [
                {
                    field: "Attachment",
                    title: "Attachment"
                },
                {
                    field: "CreatedOn",
                    title: "Created On",
                },
                {
                    field: "Size",
                    title: ""
                }
        ],
        noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
        filterable: false,
        resizable: false,
        sortable: false,
        scrollable: false,
        pageable: false,
        //toolbar: [
        //        {
        //            template: kendo.template($("#headToolbarTemplate").html())
        //        }]
    };

    // case history
    $scope.ReportHistory = function () {
        $scope.ReportDetailHistory = {
            dataSource: {
                type: "jsonp",
                transport: {
                    cache: false,
                    read: function (e) {
                        //if ($scope.ReportID) {
                        var url = "/api/Case/" + $scope.CaseId + "/GetCaseHistoryForCase";
                        $http.get(url, e).then(function (data) {
                            e.success(data.data);
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                        });
                        //}
                    },
                },

            },
            batch: true,
            cache: false,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            pageable: {
                pageSize: 10,
                pageSizes: [10, 20, 30, 50, 100],
                buttonCount: 5
            },
            scrollable: true,
            noRecords: { template: "No report history found" },
            columns: [
                {
                    field: "LastUpdatedOn", title: "Date",
                    template: "#= kendo.toString(kendo.parseDate(LastUpdatedOn), 'MM/dd/yyyy hh:mm tt') #"
                },
                {
                    field: "Field",
                    title: "Field",
                    template: function (dataitem) {

                        if (dataitem.Field == 'VPO_PICPO_PODId')
                            return '<div>VPO</div>'
                        else if (dataitem.Field == 'MPO_MasterPONum_POId')
                            return '<div>MPO</div>'
                        else if (dataitem.Field == 'SalesOrderId')
                            return '<div>Sales</div>'
                        else if (dataitem.Field == 'Description')
                            return '<div>Desc</div>'
                        else
                            return '<div>' + dataitem.Field + '</div>'
                    }
                },
                { field: "UserName", title: "User" },
                { field: "OriginalValue", title: "Original Value" },
                { field: "NewValue", title: "New Value" }
            ],
        };
    }
    

    $scope.forComments = { CaseId: $scope.CaseId };
    if ($scope.CaseId) {
        $scope.FileUploadService.SetCaseId($scope.CaseId, 'FranchiseCase');
    }
    else $scope.FileUploadService.clearCaseId();

    if (window.location.href.includes('/FeedbackUpdate/')) {
        $scope.FileUploadService.css1 = "fileupload_ltgrid";
        $scope.FileUploadService.css2 = "";
        $scope.FileUploadService.css3 = "fileupload_rtgrid";
    }

    // Kendo Resizing
    $scope.$on("kendoWidgetCreated", function (e) {
        if ($scope.PicLogin)
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
        else
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
        window.onresize = function () {           
            setTimeout(function () {
                if ($scope.PicLogin)
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
                else
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
            }, 50);
        };
    });
    // End Kendo Resizing

    $scope.ValidateFeedBackUpdate = function () {
        if ($scope.ReportDetails.Newstatus == 0 || $scope.ReportDetails.Newstatus == undefined) {
            $('#New_Status').addClass("required-error");
            return true;
        } else {
            if ($scope.ReportDetails.Newstatus == 12002) {
                if ($scope.ReportDetails.CloseReason == 0 || $scope.ReportDetails.CloseReason == undefined) {
                    $('#Close_Reason').addClass("required-error");
                    return true;
                } else {
                    if ($scope.ReportDetails.CloseReason == 14000) {
                        if ($scope.ReportDetails.ChangeCaseId == 0 || $scope.ReportDetails.ChangeCaseId == undefined) {
                            $('#Change_Request').addClass("required-error");
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }

    $scope.RemoveRequiredError = function () {
        $('#New_Status').removeClass("required-error");
        $('#Close_Reason').removeClass("required-error");
        $('#Change_Request').removeClass("required-error");
    }

    //for scrolling in kanban view
    if (($window.location.href.toUpperCase().includes('FEEDBACKKANBANVIEW'))) {
        HFCService.setHeaderTitle("FeedbackKanbanView  #");
        var desiredHeight = window.innerHeight - $("#MenuDiv1").height() - 215;
        $("#KanbanDiv").css("height", desiredHeight);
    }
    window.onresize = function () {               
        setTimeout(function () {
			//for resizing card heights on page resize
        	$scope.fixCardHeights();
            var desiredHeight = window.innerHeight - $("#MenuDiv1").height() - 215;
            $("#KanbanDiv").css("height", desiredHeight);
        }, 50);

    };
    //for setting heights to kanban cards
    $scope.fixCardHeights = function () {
        var heights = new Array();
        // Loop to get all element heights
        $('.card').each(function () {
            // Need to let sizes be whatever they want so no overflow on resize
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            // Then add size (no units) to array
            heights.push($(this).height());
        });
        // Find max height of all elements
        var max = Math.max.apply(Math, heights);
        max = max + 15;
        // Set all heights to max height
        $('.card').each(function () {
            $(this).css('height', max + 'px');
        });
    }
}
    ]);