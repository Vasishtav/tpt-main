﻿'use strict';
var overview = angular.module('overviewModule', [])
.controller('OverviewController',
   ['$scope', '$http', '$window', '$location', 'HFCService', 'NavbarServiceVendor', 'NavbarServiceCP', 'NavbarServicePIC', 'kendotooltipService', '$routeParams', 'FileUploadService', 'FranchiseService','NavbarService',
function ($scope, $http, $window, $location, HFCService, NavbarServiceVendor, NavbarServiceCP, NavbarServicePIC, kendotooltipService, $routeParams, FileUploadService, FranchiseService, NavbarService) {
    $scope.HFCService = HFCService;
    $scope.Roles = $scope.HFCService.Roles;
    $scope.FranciseLevelVendorMng = HFCService.FranciseLevelVendorMng;
    $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
    $scope.BrandId = HFCService.CurrentBrand;
    $scope.RolesString = "";
    var offsetvalue = 0;
    $scope.VendorLogin = false;
    $scope.PicLogin = false;
    $scope.FranchiseLogin = false;
    $scope.AdminLogin = false;
    //flag to show grid loader on initial load
    $scope.ProgressLoad = true;
    if ($scope.Roles != "Franchise") {
        for (i = 0; i < $scope.Roles.length; i++) {
            if ($scope.RolesString == "")
                $scope.RolesString = $scope.Roles[i].RoleName;
            else
                $scope.RolesString = $scope.RolesString + ', ' + $scope.Roles[i].RoleName;
        }
    }
    else
        $scope.RolesString = "Franchise";

    if ($scope.RolesString == "" || $scope.RolesString.includes("HFC Product Strategy Mgt (PSM)")) {
        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.NavbarServiceCP.VendorGovernanceTab();
        if (!$scope.RolesString.includes("HFC Product Strategy Mgt (PSM)"))
            $scope.AdminLogin = true;
    }
    else if ($scope.RolesString.includes("Vendor")) {
        $scope.NavbarServiceVendor = NavbarServiceVendor;
        $scope.NavbarServiceVendor.SelectHomeTab();
        $scope.VendorLogin = true;
    }
    else if ($scope.RolesString.includes("Supply Chain Partner (PIC)")) {
        $scope.NavbarServicePIC = NavbarServicePIC;
        $scope.NavbarServicePIC.PICTab();
        $scope.PicLogin = true;
    }
    else if ($scope.RolesString.includes("Franchise")) {
        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableVendorManagementTab();
        $scope.FranchiseLogin = true;
    }

    //permission
    $scope.Permission = {};
    if ($scope.RolesString.includes("Franchise") && $scope.BrandId == 1) {
        //permission for case management
        var Casemanagementpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'CaseManagement ')
        $scope.Permission.ListCaseData = Casemanagementpermission.CanRead;
        //permission for vendor management
        var Vendormanagementpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'VendorManagement')
        $scope.Permission.ListVendorData = Vendormanagementpermission.CanRead;
    }
    else if ($scope.RolesString.includes("Franchise") && $scope.BrandId != 1) {
        $scope.FranciseLevelVendorMng = false;
        $scope.FranciseLevelCase = false;
        $scope.Permission.ListCaseData = false;
        $scope.Permission.ListVendorData = false;
    }
    else {
        $scope.FranciseLevelVendorMng = true;
        $scope.FranciseLevelCase = true;
        $scope.Permission.ListCaseData = true;
        $scope.Permission.ListVendorData = true;
    }

    $scope.FranchiseService = FranchiseService;
    $scope.searchvalue = "";
    $scope.IsCloseReason = false;
    $scope.IsChangeRequestSubmitted = false;
    $scope.Overviewlist = {};
    $scope.Closed = false;
    HFCService.setHeaderTitle("Overview #");
    //for displaying loader
    $scope.modalState = {
        loaded: true
    }
    $scope.$watch('modalState.loaded', function () {
        var loadingElement = document.getElementById("loading");

        if (!$scope.modalState.loaded)
            loadingElement.style.display = "block";
        else
            loadingElement.style.display = "none";
    });
    $scope.InitialLoad = function () {
        if (($window.location.href.toUpperCase().includes('OVERVIEWKANBANVIEW'))) {
            $scope.modalState.loaded = false;
        }
        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
        if ($scope.RolesString.includes("Vendor")) {
            $http.get('/api/Case/0/GetOverviewOfCaseFeedbackChangeListByVendor?IncludeClosed=' + $scope.Closed).then(function (response) {
                $scope.ds = response.data;
                var grid = $('#gridOverviewSearch').data("kendoGrid");
                var dataSource = new kendo.data.DataSource({
                    data: $scope.ds, pageSize: 25,
                    schema: {
                        model: {
                            fields: {
                                Created: { type: 'date', editable: false },
                                EffectiveDate: { type: 'date', editable: false },
                                LastModified: { type: 'date', editable: false },
                                DaysOpen: { type: 'number', editable: false },
                                DaysSinceModified: { type: 'number', editable: false },
                            }
                        }
                    },
                });
                dataSource.read();
                $scope.Overviewlist = $scope.ds;
                $scope.Overviewlistcopy = $scope.Overviewlist;
                if (grid) {
                    grid.setDataSource(dataSource);
                    grid.dataSource.page(1);
                    $scope.OverviewgridSearch();
                }
                else {
                    $scope.GetKanban();
                    $scope.OverviewKanbanSearch($scope.searchvalue);
                    $scope.modalState.loaded = true;                    
                }
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }, function (error) {
                HFC.DisplayAlert(error);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                $scope.modalState.loaded = true;
            });
        }
        else if ($scope.RolesString.includes("Franchise")) {
            $http.get('/api/Case/0/GetOverviewListByFranchise?IncludeClosed=' + $scope.Closed).then(function (response) {
                $scope.ds = [];
                if ($scope.FranciseLevelVendorMng && $scope.Permission.ListVendorData && (!$scope.FranciseLevelCase || !$scope.Permission.ListCaseData)) {
                    for (i = 0; i < response.data.length; i++) {
                        if (response.data[i].Type != "Case") {
                            $scope.ds.push(response.data[i]);
                        }
                    }
                }
                else if ((!$scope.FranciseLevelVendorMng || !$scope.Permission.ListVendorData) && $scope.FranciseLevelCase && $scope.Permission.ListCaseData) {
                    for (i = 0; i < response.data.length; i++) {
                        if (response.data[i].Type == "Case") {
                            $scope.ds.push(response.data[i]);
                        }
                    }
                }
                else
                    $scope.ds = response.data;

                var grid = $('#gridOverviewSearch').data("kendoGrid");
                var dataSource = new kendo.data.DataSource({
                    data: $scope.ds, pageSize: 25,
                    schema: {
                        model: {
                            fields: {
                                Created: { type: 'date', editable: false },
                                EffectiveDate: { type: 'date', editable: false },
                                LastModified: { type: 'date', editable: false },
                                DaysOpen: { type: 'number', editable: false },
                                DaysSinceModified: { type: 'number', editable: false },
                            }
                        }
                    },
                });
                dataSource.read();
              
                $scope.Overviewlist = $scope.ds;
                $scope.Overviewlistcopy = $scope.Overviewlist;
                if (grid) {
                    grid.setDataSource(dataSource);
                    grid.dataSource.page(1);
                    $scope.OverviewgridSearch();
                }
                else {
                    $scope.GetKanban();
                    $scope.OverviewKanbanSearch($scope.searchvalue);
                    $scope.modalState.loaded = true;                    
                }
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }, function (error) {
                HFC.DisplayAlert(error);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                $scope.modalState.loaded = true; 
            });
        }
        else {
            $http.get('/api/Case/0/GetOverviewOfCaseFeedbackChangeList?IsPIC=' + $scope.PicLogin + '&IncludeClosed=' + $scope.Closed).then(function (response) {
                $scope.ds = response.data;
                var grid = $('#gridOverviewSearch').data("kendoGrid");
                var dataSource = new kendo.data.DataSource({
                    data: $scope.ds, pageSize: 25,
                    schema: {
                        model: {
                            fields: {
                                Created: { type: 'date', editable: false },
                                EffectiveDate: { type: 'date', editable: false },
                                LastModified: { type: 'date', editable: false },
                                DaysOpen: { type: 'number', editable: false },
                                DaysSinceModified: { type: 'number', editable: false },
                            }
                        }
                    },
                });
                dataSource.read();
                $scope.Overviewlist = $scope.ds;
                $scope.Overviewlistcopy = $scope.Overviewlist;
                if (grid) {
                    grid.setDataSource(dataSource);
                    grid.dataSource.page(1);
                    $scope.OverviewgridSearch();
                }
                else {
                    $scope.GetKanban();
                    $scope.OverviewKanbanSearch($scope.searchvalue);
                    $scope.modalState.loaded = true; 
                }
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }, function (error) {
                HFC.DisplayAlert(error);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                $scope.modalState.loaded = true; 
            });
        }
    }

    if (($window.location.href.toUpperCase().includes('OVERVIEW')))
        $scope.InitialLoad();

    //kanban view
    $scope.GetKanban = function () {
        $scope.TodoKanbanDataList = [];
        $scope.InprocessKanbanDataList = [];
        $scope.CompleteKanbanDataList = [];
        $scope.EscalatedKanbanDataList = [];
        $scope.OnHoldKanbanDataList = [];
        $scope.VendorKanbanDataList = [];
        $scope.NeedsReviewKanbanDataList = [];
        $scope.CanceledKanbanDataList = [];
        for (var i = 0; i < $scope.Overviewlist.length; i++) {
            if ($scope.Overviewlist[i].Status == "Submitted" || $scope.Overviewlist[i].Status == "New")
                $scope.TodoKanbanDataList.push($scope.Overviewlist[i]);
            else if ($scope.Overviewlist[i].Status == "Vendor Review" || $scope.Overviewlist[i].Status == "Approved" || $scope.Overviewlist[i].Status == "Declined" || $scope.Overviewlist[i].Status == "Validating" || $scope.Overviewlist[i].Status == "Validated" || $scope.Overviewlist[i].Status == "In Process")
                $scope.InprocessKanbanDataList.push($scope.Overviewlist[i]);
            else if ($scope.Overviewlist[i].Status == "Closed" || $scope.Overviewlist[i].Status == "In Production" || $scope.Overviewlist[i].Status == "Completed")
                $scope.CompleteKanbanDataList.push($scope.Overviewlist[i]);
            else if ($scope.Overviewlist[i].Status == "Escalated")
                $scope.EscalatedKanbanDataList.push($scope.Overviewlist[i]);
            else if ($scope.Overviewlist[i].Status == "On Hold")
                $scope.OnHoldKanbanDataList.push($scope.Overviewlist[i]);
            else if ($scope.Overviewlist[i].Status == "Vendor")
                $scope.VendorKanbanDataList.push($scope.Overviewlist[i]);
            else if ($scope.Overviewlist[i].Status == "Needs Review")
                $scope.NeedsReviewKanbanDataList.push($scope.Overviewlist[i]);
            else if ($scope.Overviewlist[i].Status == "Canceled")
                $scope.CanceledKanbanDataList.push($scope.Overviewlist[i]);
        }
        setTimeout(function () {
            $scope.fixCardHeights();
        }, 50);
    }

    $scope.Change_List = function (value) {
        if (value)
            $scope.Closed = true;
        else
            $scope.Closed = false;

        $scope.InitialLoad();
    }

    $scope.OverviewKanbanSearch = function (searchvalue) {
        searchvalue = searchvalue.toLowerCase();
        if (searchvalue == "") {
            $scope.ClearKanbanSearch();
            return;
        }
        $scope.KanbanDataList = [];
        for (var i = 0; i < $scope.Overviewlistcopy.length; i++) {
            var id = ($scope.Overviewlistcopy[i].ID) ? $scope.Overviewlistcopy[i].ID.toLowerCase() : "";
            var originator = ($scope.Overviewlistcopy[i].Originator) ? $scope.Overviewlistcopy[i].Originator.toLowerCase() : "";
            var type = ($scope.Overviewlistcopy[i].Type) ? $scope.Overviewlistcopy[i].Type.toLowerCase() : "";
            var status = ($scope.Overviewlistcopy[i].Status) ? $scope.Overviewlistcopy[i].Status.toLowerCase() : "";
            if ($scope.Overviewlistcopy[i].CloseReason) {
                var statusclosedreason = $scope.Overviewlistcopy[i].CloseReason.toLowerCase();
                status = status + " - " + statusclosedreason;
            }
            var vendor = ($scope.Overviewlistcopy[i].Vendor) ? $scope.Overviewlistcopy[i].Vendor.toLowerCase() : "";
            if (id.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.Overviewlistcopy[i]);
            }
            else if (originator.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.Overviewlistcopy[i]);
            }
            else if (type.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.Overviewlistcopy[i]);
            }
            else if (status.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.Overviewlistcopy[i]);
            }
            else if (vendor.includes(searchvalue)) {
                $scope.KanbanDataList.push($scope.Overviewlistcopy[i]);
            }
        }
        $scope.Overviewlist = $scope.KanbanDataList;
        $scope.GetKanban();
    }

    $scope.ClearKanbanSearch = function () {
        $scope.searchvalue = "";
        $scope.Overviewlist = $scope.Overviewlistcopy;
        $scope.GetKanban();
    }

     $scope.OverviewgridSearchClear = function () {
        $('#BoxValue').val('');
        $("#gridOverviewSearch").data('kendoGrid').dataSource.filter({
        });
    }

    $scope.OverviewgridSearch = function () {
        var searchValue = $('#BoxValue').val();
        $("#gridOverviewSearch").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
              {
                  field: "Type",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "Vendor",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "ID",
                  operator: "contains",
                  //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                  value: searchValue
              },
              {
                  field: "RecordType",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "Status",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "DaysOpen",
                                    operator: "eq",
                  //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                  value: searchValue
              },
              {
                  field: "DaysSinceModified",
                  operator: "eq",
                  //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                  value: searchValue
              },
            ]
        });

    }

    $scope.OverviewgridList = {
        dataSource: {
            error: function (e) {
                HFC.DisplayAlert(e.errorThrown);
            },
                  },
        batch: true,
        cache: false,
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    isempty: "Empty",
                    isnotempty: "Not empty"


                },
                number: {
                    eq: "Equal to",
                    neq: "Not equal to",
                    gte: "Greater than or equal to",
                    lte: "Less than or equal to"
                },
                date: {
                    gt: "After (Excludes)",
                    lt: "Before (Includes)"
                },
            }
        },
        resizable: true,
        editable: "inline",
        noRecords: { template: "No records found" },
        dataBound: function (e) {
            //for showing loader on initial load
            if($scope.ProgressLoad == true)
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
            $scope.ProgressLoad = false;
            //for hiding effective date column in FE login
            if ($scope.Roles == "Franchise") {
                var grid = $("#gridOverviewSearch").data("kendoGrid");
                grid.hideColumn("EffectiveDate");
            }
            
            if (this.dataSource.view().length == 0) {
                // The Grid contains No recrods so hide the footer.
                $('.k-pager-nav').hide();
                $('.k-pager-numbers').hide();
                $('.k-pager-sizes').hide();
            } else {
                // The Grid contains recrods so show the footer.
                $('.k-pager-nav').show();
                $('.k-pager-numbers').show();
                $('.k-pager-sizes').show();
            }
            if (kendotooltipService.columnWidth) {
                kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
            } else if (window.innerWidth < 1280) {
                kendotooltipService.restrictTooltip(null);
            }
            $scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
        },
        columns: [
            {
                field: "Type",
                title: "Type",
                width: "100px",
                filterable: { search: true },
                template: function Type(dataItem) {
                    var template = "";
                    if (dataItem.Type) {
                        template += "<span>" + dataItem.Type + "</span>";
                        return template;
                    }
                    else {
                        return '';
                    }

                }
            },
             {
                 field: "Vendor",
                 title: "Vendor",
                 width: "300px",
                 filterable: { search: true },
                 template: function Vendor(dataItem) {
                     var template = "";
                     if (dataItem.Vendor) {
                         template += "<span >" + dataItem.Vendor +  "</span>";
                         return template;
                     }
                     else {
                         return '';
                     }

                 }
             },
            {
                field: "ID",
                title: "ID",
                width: "150px",
                filterable: { search: true },
                template: function (dataItem) {
                    if (dataItem.Type == "Change")
                        return "<a href='#!/ChangeRequestView/" + dataItem.CaseId + "'style='color: rgb(61,125,139);'>" + dataItem.ID + "</a>";
                    else if (dataItem.Type == "Feedback")
                        return "<a href='#!/FeedbackView/" + dataItem.CaseId + "'style='color: rgb(61,125,139);'>" + dataItem.ID + "</a>";
                    else if ($scope.FranchiseLogin == true)
                        return "<a href='#!/CaseView/" + dataItem.CaseId +"' style='color: rgb(61,125,139);'>" + dataItem.ID + "</a>";
                    else
                        return "<a href='#!/VendorcaseView/" + dataItem.CaseId + "/" + dataItem.VendorCaseId + "' style='color: rgb(61,125,139);'>" + dataItem.ID + "</a>";
                },
            },

              {
                  field: "RecordType",
                  title: "Record Type",
                  width: "225px",
                  filterable: { search: true },
                  template: function RecordType(dataItem) {
                      var template = "";
                      if (dataItem.RecordType) {
                          template += "<span>" + dataItem.RecordType + "</span>";
                          return template;
                      }
                      else {
                          return '';
                      }

                  }
              },
              {
                  field: "Status",
                  title: "Status",
                  width: "175px",
                  filterable: {search: true },
                  template: function Status(dataItem) {
                      var template = "";
                      if (dataItem.Status) {
                          template += "<span>" + dataItem.Status +  "</span>";
                          return template;
                      }
                      else {
                          return '';
                      }

                  }
              },
              {
                  field: "Created",
                  title: "Created",
                  width: "150px",
                  type: "date",
                  filterable: HFCService.gridfilterableDate("date", "OverviewCreatedFilterCalendar", offsetvalue),
                  template: function Created(dataItem) {
                      var template = "";
                      if (dataItem.Created) {
                          template += "<span >" + HFCService.KendoDateFormat(dataItem.Created) + "</span>";
                          return template;
                      }
                      else {
                          return '';
                      }

                  }
              },
              {
                  field: "EffectiveDate",
                  title: "Effective Date",
                  width: "150px",
                  type: "date",
                  filterable: HFCService.gridfilterableDate("date", "OverviewEffectiveDateFilterCalendar", offsetvalue),
                  template:   function EffectiveDate(dataItem) {
                      var template = "";
                      if (dataItem.EffectiveDate) {
                          template += "<span >" + HFCService.KendoDateFormat(dataItem.EffectiveDate) + "</span>";
                          return template;
                      }
                      else {
                          return '';
                      }
                  
                  }
              },
              {
                  field: "LastModified",
                  title: "Last Modified",
                  width: "150px",
                  type: "date",
                  filterable: HFCService.gridfilterableDate("date", "OverviewLastModifiedFilterCalendar", offsetvalue),
                  template: function LastModified(dataItem) {
                      var template = "";
                      if (dataItem.LastModified) {
                          template += "<span>" + HFCService.KendoDateFormat(dataItem.LastModified) + "</span>";
                          return template;
                      }
                      else {
                          return '';
                      }

                  }
              },
             {
                 field: "DaysOpen",
                 title: " Days Open",
                 width: "150px",
                 filterable: { search: true },
                 template: function DaysOpen(dataItem) {
                     var template = "";
                     if (dataItem.DaysOpen) {
                         template += "<span>" + dataItem.DaysOpen + "</span>";
                         return template;
                     }
                     else {
                         return '';
                     }

                 },
             },
            {
                field: "DaysSinceModified",
                title: " Days Since Modified",
                width: "175px",
                filterable: { search: true },
                template: function DaysSinceModified(dataItem) {
                    var template = "";
                    if (dataItem.DaysSinceModified) {
                        template += "<span>" + dataItem.DaysSinceModified + "</span>";
                        return template;
                    }
                    else {
                        return '';
                    }

                },
            },
        ],
        pageable: {
            refresh: true,
            pageSize: 25,
            pageSizes: [25, 50, 100],
            buttonCount: 5
        },
        columnResize: function (e) {
            if ($scope.PicLogin)
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
            else
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
            getUpdatedColumnList(e);
        }
    };
    var getUpdatedColumnList = function (e) {
        kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
    }
    $scope.gotoListview = function () {
        window.location.href = "#!/Overview";
         }
        $scope.gotooverviewkanbanview = function () {
        window.location.href = "#!/OverviewKanbanView";
           }  
    // Kendo Resizing
    $scope.$on("kendoWidgetCreated", function (e) {
        if ($scope.PicLogin)
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
        else
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
        window.onresize = function () {           
            setTimeout(function () {
                if ($scope.PicLogin)
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
                else
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
            }, 50);
        };
    });
    
    // End Kendo Resizing

    //for scrolling in kanban view
    if (($window.location.href.toUpperCase().includes('OVERVIEWKANBANVIEW'))) {
        HFCService.setHeaderTitle("OverviewKanbanView #");
        var desiredHeight = window.innerHeight - $("#MenuDiv1").height() - 215;
        $("#KanbanDiv").css("height", desiredHeight);
    }
    window.onresize = function () {               
        setTimeout(function () {
		    //for resizing card heights on page resize
            $scope.fixCardHeights();
            var desiredHeight = window.innerHeight - $("#MenuDiv1").height() - 215;
            $("#KanbanDiv").css("height", desiredHeight);
        }, 50);
    };

    //for setting heights to kanban cards
    $scope.fixCardHeights = function() {
        var heights = new Array();
        // Loop to get all element heights
        $('.card').each(function () {
            // Need to let sizes be whatever they want so no overflow on resize
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            // Then add size (no units) to array
            heights.push($(this).height());
        });
        // Find max height of all elements
        var max = Math.max.apply(Math, heights);
        max = max + 15;
        // Set all heights to max height
        $('.card').each(function () {
            $(this).css('height', max + 'px');
        });
    }

}

   ])



