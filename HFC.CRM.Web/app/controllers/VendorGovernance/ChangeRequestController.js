﻿'use strict';
var changeRequest = angular.module('changeRequestModule', [])
    .controller('ChangeRequestController',
        [
            '$scope', '$http', '$window', '$location', 'HFCService', 'NavbarServiceVendor', 'NavbarServiceCP', 'NavbarServicePIC', 'kendotooltipService', '$routeParams', 'FileUploadServiceVg', 'FranchiseService', 'NavbarService',
            function ($scope, $http, $window, $location, HFCService, NavbarServiceVendor, NavbarServiceCP, NavbarServicePIC, kendotooltipService, $routeParams, FileUploadServiceVg, FranchiseService, NavbarService) {
                $scope.HFCService = HFCService;
                $scope.Roles = $scope.HFCService.Roles;
                $scope.RolesString = "";
                var offsetvalue = 0;
                $scope.VendorLogin = false;
                $scope.PicLogin = false;
                $scope.FranchiseLogin = false;
                $scope.AdminLogin = false;
                //flag to show grid loader on initial load
                $scope.ProgressLoad = true;

                if ($scope.Roles != "Franchise") {
                    for (i = 0; i < $scope.Roles.length; i++) {
                        if ($scope.RolesString == "")
                            $scope.RolesString = $scope.Roles[i].RoleName;
                        else
                            $scope.RolesString = $scope.RolesString + ', ' + $scope.Roles[i].RoleName;
                    }
                }
                else
                    $scope.RolesString = "Franchise";

                if ($scope.RolesString == "" || $scope.RolesString.includes("HFC Product Strategy Mgt (PSM)")) {
                    $scope.NavbarServiceCP = NavbarServiceCP;
                    $scope.NavbarServiceCP.VendorGovernanceTab();
                    if (!$scope.RolesString.includes("HFC Product Strategy Mgt (PSM)"))
                        $scope.AdminLogin = true;
                }
                else if ($scope.RolesString.includes("Vendor")) {
                    $scope.NavbarServiceVendor = NavbarServiceVendor;
                    $scope.NavbarServiceVendor.SelectHomeTab();
                    $scope.VendorLogin = true;
                }
                else if ($scope.RolesString.includes("Supply Chain Partner (PIC)")) {
                    $scope.NavbarServicePIC = NavbarServicePIC;
                    $scope.NavbarServicePIC.PICTab();
                    $scope.PicLogin = true;
                }
                else if ($scope.RolesString.includes("Franchise")) {
                    $scope.NavbarService = NavbarService;
                    $scope.NavbarService.SelectOperation();
                    $scope.NavbarService.EnableVendorManagementTab();
                    $scope.FranchiseLogin = true;
                }

                $scope.FranchiseService = FranchiseService;
                $scope.searchvalue = "";
                $scope.IsClosed = false;
                $scope.IsPMApproved = false;
                $scope.VendorCanEdit = false;
                $scope.IsChangeRequestSubmitted = false;
                $scope.ChangeRequestList = {};
                $scope.Closed = false;
                $scope.minDate = new Date();
                $scope.minDate.setDate($scope.minDate.getDate() + 1);
                $scope.EffectiveWarning = false;
                //for displaying loader
                $scope.modalState = {
                    loaded: true
                }

                $scope.$watch('modalState.loaded', function () {
                    var loadingElement = document.getElementById("loading");

                    if (!$scope.modalState.loaded) {
                        loadingElement.style.display = "block";
                    } else {
                        loadingElement.style.display = "none";
                    }
                });
                //set window title
                HFCService.setHeaderTitle("ChangeRequest #");
                $scope.InitialLoad = function () {
                    if (($window.location.href.toUpperCase().includes('CHANGEREQUESTKANBANVIEW'))) {
                        $scope.modalState.loaded = false;
                    }
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
                    if ($scope.RolesString.includes("Vendor")) {
                        $http.get('/api/Case/0/GetChangeRequestListByVendor?IncludeClosed=' + $scope.Closed).then(function (response) {
                            $scope.ds = response.data;
                            var grid = $('#changerequest').data("kendoGrid");
                            var dataSource = new kendo.data.DataSource({
                                data: $scope.ds, pageSize: 25,
                                schema: {
                                    model: {
                                        fields: {
                                            RequestDate: { type: 'date', editable: false },
                                            EffectiveDate: { type: 'date', editable: false },
                                            LastModified: { type: 'date', editable: false },
                                            DaysOpen: { type: 'number', editable: false },
                                            DaysSinceModified: { type: 'number', editable: false },
                                        }
                                    }
                                },
                            });
                            dataSource.read();
                            $scope.ChangeRequestList = $scope.ds;
                            $scope.copyChangeRequestList = $scope.ChangeRequestList;
                            if (grid) {
                                grid.setDataSource(dataSource);
                                grid.dataSource.page(1);
                                $scope.Gridsearch();
                            }
                            else {
                                $scope.GetKanban();
                                $scope.KanbanSearch($scope.searchvalue);
                                $scope.modalState.loaded = true;
                            }

                            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                        }, function (error) {
                            HFC.DisplayAlert(error);
                            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                            $scope.modalState.loaded = true;
                        });
                    }
                    else {
                        $http.get('/api/Case/0/GetChangeRequestList?IsPIC=' + $scope.PicLogin + '&IncludeClosed=' + $scope.Closed).then(function (response) {
                            $scope.ds = response.data;
                            var grid = $('#changerequest').data("kendoGrid");
                            var dataSource = new kendo.data.DataSource({
                                data: $scope.ds, pageSize: 25,
                                schema: {
                                    model: {
                                        fields: {
                                            RequestDate: { type: 'date', editable: false },
                                            EffectiveDate: { type: 'date', editable: false },
                                            LastModified: { type: 'date', editable: false },
                                            DaysOpen: { type: 'number', editable: false },
                                            DaysSinceModified: { type: 'number', editable: false },
                                        }
                                    }
                                },
                            });
                            dataSource.read();
                            $scope.ChangeRequestList = $scope.ds;
                            $scope.copyChangeRequestList = $scope.ChangeRequestList;
                            if (grid) {
                                grid.setDataSource(dataSource);
                                grid.dataSource.page(1);
                                $scope.Gridsearch();
                            }
                            else {
                                $scope.GetKanban();
                                $scope.KanbanSearch($scope.searchvalue);
                                $scope.modalState.loaded = true;
                            }
                            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                        }, function (error) {
                            HFC.DisplayAlert(error);
                            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                            $scope.modalState.loaded = true;
                        });
                    }

                    if ($scope.RolesString.includes("Vendor")) {
                        $http.get('/api/Case/0/GetChangeRequestListByVendor?IncludeClosed=' + $scope.Closed).then(function (response) {
                            $scope.ds = response.data;
                            var grid = $('#changerequest').data("kendoGrid");
                            var dataSource = new kendo.data.DataSource({
                                data: $scope.ds, pageSize: 25,
                                schema: {
                                    model: {
                                        fields: {
                                            RequestDate: { type: 'date', editable: false },
                                            EffectiveDate: { type: 'date', editable: false },
                                            LastModified: { type: 'date', editable: false },
                                            DaysOpen: { type: 'number', editable: false },
                                            DaysSinceModified: { type: 'number', editable: false },
                                        }
                                    }
                                },
                            });
                            dataSource.read();
                            $scope.ChangeRequestList = $scope.ds;
                            $scope.copyChangeRequestList = $scope.ChangeRequestList;
                            if (grid) {
                                grid.setDataSource(dataSource);
                                grid.dataSource.page(1);
                                $scope.Gridsearch();
                            }
                            else {
                                $scope.GetKanban();
                                $scope.KanbanSearch($scope.searchvalue);
                                $scope.modalState.loaded = true;
                            }
                            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                        }, function (error) {
                            HFC.DisplayAlert(error);
                            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                            $scope.modalState.loaded = true;
                        });
                    }

                }

                if ($window.location.href.toUpperCase().includes('CHANGEREQUEST') && !$window.location.href.toUpperCase().includes('CHANGEREQUESTCREATE') && !$window.location.href.toUpperCase().includes('CHANGEREQUESTVIEW') && !$window.location.href.toUpperCase().includes('CHANGEREQUESTEDIT'))
                    $scope.InitialLoad();

                //kanban view
                $scope.GetKanban = function () {
                    $scope.TodoKanbanDataList = [];
                    $scope.InprocessKanbanDataList = [];
                    $scope.CompleteKanbanDataList = [];
                    for (var i = 0; i < $scope.ChangeRequestList.length; i++) {
                        if ($scope.ChangeRequestList[i].Status == "Submitted")
                            $scope.TodoKanbanDataList.push($scope.ChangeRequestList[i]);
                        else if ($scope.ChangeRequestList[i].Status == "Approved" || $scope.ChangeRequestList[i].Status == "Declined" || $scope.ChangeRequestList[i].Status == "Validating" || $scope.ChangeRequestList[i].Status == "Validated")
                            $scope.InprocessKanbanDataList.push($scope.ChangeRequestList[i]);
                        else if ($scope.ChangeRequestList[i].Status == "In Production")
                            $scope.CompleteKanbanDataList.push($scope.ChangeRequestList[i]);
                    }
                    setTimeout(function () {
                        $scope.fixCardHeights();
                    }, 50);
                }

                $scope.Change_List = function (value) {
                    if (value)
                        $scope.Closed = true;
                    else
                        $scope.Closed = false;

                    $scope.InitialLoad();
                }

                $scope.KanbanSearch = function (searchvalue) {
                    searchvalue = searchvalue.toLowerCase();
                    if (searchvalue == "") {
                        $scope.GetKanban();
                        return;
                    }
                    $scope.KanbanDataList = [];
                    for (var i = 0; i < $scope.copyChangeRequestList.length; i++) {
                        var id = ($scope.copyChangeRequestList[i].ID) ? $scope.copyChangeRequestList[i].ID.toLowerCase() : "";
                        var originator = ($scope.copyChangeRequestList[i].Originator) ? $scope.copyChangeRequestList[i].Originator.toLowerCase() : "";
                        var Vendor = ($scope.copyChangeRequestList[i].Vendor) ? $scope.copyChangeRequestList[i].Vendor.toLowerCase() : "";
                        var changetype = 'change';
                        var status = ($scope.copyChangeRequestList[i].Status) ? $scope.copyChangeRequestList[i].Status.toLowerCase() : "";
                        if ($scope.copyChangeRequestList[i].CloseReason) {
                            var statusCloseReason = $scope.copyChangeRequestList[i].CloseReason.toLowerCase();
                            status = status + " - " + statusCloseReason;
                        }

                        if (id.includes(searchvalue)) {
                            $scope.KanbanDataList.push($scope.copyChangeRequestList[i]);
                        }
                        else if (originator.includes(searchvalue)) {
                            $scope.KanbanDataList.push($scope.copyChangeRequestList[i]);
                        }
                        else if (Vendor.includes(searchvalue)) {
                            $scope.KanbanDataList.push($scope.copyChangeRequestList[i]);
                        }
                        else if (changetype.includes(searchvalue)) {
                            $scope.KanbanDataList.push($scope.copyChangeRequestList[i]);
                        }
                        else if (status.includes(searchvalue)) {
                            $scope.KanbanDataList.push($scope.copyChangeRequestList[i]);
                        }
                    }
                    $scope.ChangeRequestList = $scope.KanbanDataList;
                    $scope.GetKanban();
                }

                $scope.ClearKanbanSearch = function () {
                    $scope.searchvalue = "";
                    $scope.ChangeRequestList = $scope.copyChangeRequestList;
                    $scope.GetKanban();
                }

                $scope.changerequestgrid = {
                    dataSource: {
                        error: function (e) {
                            HFC.DisplayAlert(e.errorThrown);
                        },

                    },
                    batch: true,
                    cache: false,
                    sortable: true,
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                isempty: "Empty",
                                isnotempty: "Not empty"
                            },
                            number: {
                                eq: "Equal to",
                                neq: "Not equal to",
                                gte: "Greater than or equal to",
                                lte: "Less than or equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    resizable: true,
                    editable: "inline",
                    noRecords: { template: "No records found" },
                    dataBound: function (e) {
                        //for showing loader on initial load
                        if ($scope.ProgressLoad == true)
                            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
                        $scope.ProgressLoad = false;

                        if (this.dataSource.view().length == 0) {
                            // The Grid contains No recrods so hide the footer.
                            $('.k-pager-nav').hide();
                            $('.k-pager-numbers').hide();
                            $('.k-pager-sizes').hide();
                        } else {
                            // The Grid contains recrods so show the footer.
                            $('.k-pager-nav').show();
                            $('.k-pager-numbers').show();
                            $('.k-pager-sizes').show();
                        }
                        if (kendotooltipService.columnWidth) {
                            kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                        } else if (window.innerWidth < 1280) {
                            kendotooltipService.restrictTooltip(null);
                        }
                        $scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
                    },
                    columns: [
                        {
                            field: "Vendor",
                            title: "Vendor",
                            width: "250px",
                            template: function Vendor(dataItem) {
                                var template = "";
                                if (dataItem.Vendor) {
                                    template += "<span >" + dataItem.Vendor + "</span>";
                                    return template;
                                }
                                else {
                                    return '';
                                }

                            },
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,
                        },
                        {
                            field: "ID",
                            title: "ID",
                            width: "100px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,
                            template: function (dataItem) {
                                return "<a href='#!/ChangeRequestView/" + dataItem.CaseId + "'style='color: rgb(61,125,139);'>" + dataItem.ID + "</a>";
                            },
                        },
                        {
                            field: "ChangeType",
                            title: "Change Type",
                            width: "150px",
                            template: function ChangeType(dataItem) {
                                var template = "";
                                if (dataItem.ChangeType) {
                                    template += "<span>" + dataItem.ChangeType + "</span>";
                                    return template;
                                }
                                else {
                                    return '';
                                }

                            },
                            filterable: { multi: true, search: true },
                            hidden: false,
                        },
                        {
                            field: "Status",
                            title: "Status",
                            width: "100px",
                            template: function Status(dataItem) {
                                var template = "";
                                if (dataItem.Status) {
                                    template += "<span>" + dataItem.Status + "</span>";
                                    return template;
                                }
                                else {
                                    return '';
                                }

                            },
                            filterable: { multi: true, search: true },
                            hidden: false,
                        },
                        {
                            field: "RequestDate",
                            title: "Request Date",
                            width: "140px",
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ChangeRequestRequestDateFilterCalendar", offsetvalue),
                            template: function RequestDate(dataItem) {
                                var template = "";
                                if (dataItem.RequestDate) {
                                    template += "<span>" + HFCService.KendoDateFormat(dataItem.RequestDate) + "</span>";
                                    return template;
                                }
                                else {
                                    return '';
                                }

                            }
                        },
                        {
                            field: "EffectiveDate",
                            title: "Effective Date",
                            width: "140px",
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ChangeRequestEffectiveDateFilterCalendar", offsetvalue),
                            template: function EffectiveDate(dataItem) {
                                var template = "";
                                if (dataItem.EffectiveDate) {
                                    template += "<span >" + HFCService.KendoDateFormat(dataItem.EffectiveDate) + "</span>";
                                    return template;
                                }
                                else {
                                    return '';
                                }

                            }
                        },
                        {
                            field: "LastModified",
                            title: "Last Modified",
                            width: "140px",
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ChangeRequestLastModifiedFilterCalendar", offsetvalue),
                            template: function LastModified(dataItem) {
                                var template = "";
                                if (dataItem.LastModified) {
                                    template += "<span>" + HFCService.KendoDateFormat(dataItem.LastModified) + "</span>";
                                    return template;
                                }
                                else {
                                    return '';
                                }

                            },
                        },
                        {
                            field: "DaysOpen",
                            title: "Days Open",
                            width: "120px",
                            template: function DaysOpen(dataItem) {
                                var template = "";
                                if (dataItem.DaysOpen) {
                                    template += "<span>" + dataItem.DaysOpen + "</span>";
                                    return template;
                                }
                                else {
                                    return '';
                                }

                            },
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,
                        },
                        {
                            field: "DaysSinceModified",
                            title: "Days Since Modified",
                            width: "130px",
                            template: function DaysSinceModified(dataItem) {
                                var template = "";
                                if (dataItem.DaysSinceModified) {
                                    template += "<span>" + dataItem.DaysSinceModified + "</span>";
                                    return template;
                                }
                                else {
                                    return '';
                                }

                            },
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,
                        },
                    ],
                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],
                        buttonCount: 5
                    },
                    columnResize: function (e) {
                        if ($scope.PicLogin)
                            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
                        else
                            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
                        getUpdatedColumnList(e);
                    }
                };

                //$scope.changerequestgrid = kendotooltipService.setColumnWidth($scope.changerequestgrid);

                var getUpdatedColumnList = function (e) {
                    kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
                }

                $scope.Gridsearch = function () {
                    var searchValue = $('#searchBox').val();
                    $("#changerequest").data("kendoGrid").dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Vendor",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "ID",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "ChangeType",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "Status",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "DaysOpen",
                                operator: "eq",
                                //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                                value: searchValue
                            },
                            {
                                field: "DaysSinceModified",
                                operator: "eq",
                                //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                                value: searchValue
                            },
                        ]
                    });

                }

                $scope.GridSearchClearr = function () {
                    $('#searchBox').val('');
                    $("#changerequest").data('kendoGrid').dataSource.filter({
                    });
                }

                $scope.gotokanbanview = function () {
                    window.location.href = "#!/ChangeRequestKanbanView";
                }

                $scope.gotoListview = function () {
                    window.location.href = "#!/ChangeRequest";
                }

                // Kendo Resizing
                $scope.$on("kendoWidgetCreated", function (e) {
                    if ($scope.PicLogin)
                        $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
                    else
                        $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
                    window.onresize = function () {
                        if ($scope.PicLogin)
                            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 245);
                        else
                            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 253);
                    };
                });
                // End Kendo Resizing
                //
                //Change Request create                

                $scope.CaseId = 0;
                $scope.RequestDetails = {
                    CaseChangeId: '',
                    ProductGroupId: '',
                    CaseId: '',
                    CurrentStatus: '',
                    ReportID: '',
                    StatusId: '',
                    Currentuser: '',
                    Currenttime: '',
                    RequestType: '',
                    Vendor: '',
                    VendorId: '',
                    ProductCategory: '',
                    Details: '',
                    UploadedDate: '',
                    FileSize: '',
                    CreatedBy: '',
                    CreatedOn: '',
                    LastUpdateByName: '',
                    LastUpdatedOn: '',
                    CreatedByName: '',
                    UpdatedBy: '',
                    CaseType: 'Change',
                    RecordTypeId: '',
                    EffectiveDate: '',
                    PublishMethod: '',
                    AttachmentURL: '',
                    RequestedBy: '',
                    RequestedDate: '',
                    Status: '',
                    CaseNumber: '',
                    DeclinedReasonId: '',
                    Comments: '',
                    NewStatus: '',
                    IsAllSelected: false
                };
                $scope.RequestDetails.RequestedBy = FranchiseService.UserData != null ? FranchiseService.UserData.FullName : '';

                $scope.RequestDate = HFCService.KendoDateFormat(new Date());

                if (window.location.href.toUpperCase().includes('CHANGEREQUESTCREATE')) {
                    $scope.RequestDetails.LastUpdateByName = FranchiseService.UserData != null ? FranchiseService.UserData.FullName : '';
                    //$scope.RequestDetails.LastUpdatedOn = HFCService.KendoDateTimeFormat(new Date());
                }
                $scope.FileUploadServiceVg = FileUploadServiceVg;

                //get data on inital page load
                $scope.loadChangeRequestTypelist = function () {
                    $http.get('/api/Case/0/GetFeedbackChangeLookup?TableName=RequestType').then(function (response) {
                        $scope.RequestTypes = response.data;
                    });
                }

                $scope.EditProduct1 = function () {
                    window.location.href = "#!/ChangeRequestEdit/" + $scope.CaseId;
                };

                $scope.FranchiseCode = "19001001";
                $scope.loadProductList = function () {
                    $http.get('/api/PIC/0/GetPICProductGroupForConfigurator?franchiseCode=' + $scope.FranchiseCode + '&vendorConfig=' + $scope.VendorLogin).then(function (response) {
                        $scope.ProductCategory = response.data;
                        if ($scope.ProductCategory != null && $scope.ProductCategory.Length != 0) {
                            $.each($scope.ProductCategory, function (index, jsonObject) {
                                jsonObject.PICProductGroupId = parseInt(jsonObject.PICProductGroupId);
                            });
                            $scope.RequestDetails.Vendor = $scope.ProductCategory[0].VendorName;
                            $scope.RequestDetails.VendorId = $scope.ProductCategory[0].VendorId;
                            $scope.RequestDetails.CaseNumber = 'PCR-' + $scope.ProductCategory[0].PICVendorId;
                        }
                    });
                }

                $scope.ListofStatus = [];
                $scope.CanChangeStatus = false;
                $scope.PMCanEdit = false;

                $scope.loadstatus = function () {
                    $http.get('/api/Case/0/GetFeedbackChangeLookup?TableName=ChangeRequestStatus').then(function (response) {
                        $scope.statuslist = response.data;
                        $scope.IsClosed = true;
                        if ($scope.RolesString.includes("Vendor") && $scope.RequestDetails.CurrentStatus == "Declined") {
                            $scope.GetDeclinedReasonList();
                            $scope.ListofStatus.push({ Name: "Submitted", Id: 0 });
                            $scope.ListofStatus.push({ Name: "Declined", Id: 0 });
                            $scope.CanChangeStatus = true;
                            $scope.VendorCanEdit = true;
                            $scope.IsClosed = false;
                        }
                        //when status is submitted or declined, Vendors can modify any of their Change Requests
                        if ($scope.RolesString.includes("Vendor") && ($scope.RequestDetails.CurrentStatus == "Submitted")) {
                            $scope.ListofStatus.push({ Name: "Submitted", Id: 0 });
                            $scope.CanChangeStatus = true;
                            $scope.VendorCanEdit = true;
                            $scope.IsClosed = false;
                        }
                        if ($scope.RolesString.includes("HFC Product Strategy Mgt (PSM)") && $scope.RequestDetails.CurrentStatus == "Submitted") {
                            {
                                $scope.ListofStatus.push({ Name: "Submitted", Id: 0 });
                                $scope.ListofStatus.push({ Name: "Approved", Id: 0 });
                                $scope.ListofStatus.push({ Name: "Declined", Id: 0 });
                                $scope.PMCanEdit = true;
                                $scope.CanChangeStatus = true;
                                $scope.IsClosed = false;
                            }
                        }
                        if ($scope.RolesString.includes("Supply Chain Partner (PIC)") && $scope.RequestDetails.CurrentStatus == "Approved") {
                            $scope.ListofStatus.push({ Name: "Approved", Id: 0 });
                            $scope.ListofStatus.push({ Name: "Validating", Id: 0 });
                            $scope.CanChangeStatus = true;
                            $scope.IsClosed = false;
                        }
                        if ($scope.RolesString.includes("Supply Chain Partner (PIC)") && $scope.RequestDetails.CurrentStatus == "Validating") {
                            $scope.ListofStatus.push({ Name: "Validating", Id: 0 });
                            $scope.ListofStatus.push({ Name: "Validated", Id: 0 });
                            $scope.CanChangeStatus = true;
                            $scope.IsClosed = false;
                        }
                        if ($scope.RolesString.includes("Supply Chain Partner (PIC)") && $scope.RequestDetails.CurrentStatus == "Validated") {
                            $scope.ListofStatus.push({ Name: "Validated", Id: 0 });
                            $scope.ListofStatus.push({ Name: "In Production", Id: 0 });
                            $scope.CanChangeStatus = true;
                            $scope.IsClosed = false;
                        }
                        // set visibility to publish method
                        if ($scope.RequestDetails.CurrentStatus != "Submitted" && $scope.RequestDetails.CurrentStatus != "Declined") {
                            $scope.IsPublished = true;
                            //$scope.IsClosed = false;
                        }
                        // to hide edit button in view page
                        if ($scope.RequestDetails.CurrentStatus == "In Production") {
                            $scope.IsClosed = true;
                            //$scope.IsFeedbackClosed = true;
                        }

                        for (var i = 0; i < $scope.statuslist.length; i++) {
                            for (var j = 0; j < $scope.ListofStatus.length; j++) {
                                if ($scope.statuslist[i].Name == $scope.ListofStatus[j].Name) {
                                    $scope.ListofStatus[j].Id = $scope.statuslist[i].Id;
                                }
                            }
                        }
                        $scope.statuslist = $scope.ListofStatus;
                    });
                }

                $scope.SaveChangeRequest = function () {
                    //$scope.formCaseManage.$setPristine();
                    if (!$scope.ValidateControls())
                        return false;
                    $scope.modalState.loaded = false;
                    var ChangeRequest = $scope.RequestDetails;
                    //if ($scope.RequestDetails.ProductGroupId)
                    $scope.GetValue();
                    //feedback.StatusId = $scope.RequestDetails.Newstatus;
                    $http.post('/api/Case/0/SaveChangeRequest', ChangeRequest).then(function (response) {
                        if (response.data != null) {
                            $scope.formCaseManage.$setPristine();
                            //$scope.formCaseManage.setUntouched();
                            var result = response.data.result;
                            $scope.RequestDetails.CaseNumber = result.CaseNumber;
                            $scope.FileUploadServiceVg.CaseId = result.CaseId;
                            $scope.FileUploadServiceVg.UploadFile(true);
                            $scope.BringConfirmModel();
                        }
                        $scope.modalState.loaded = true;
                    });
                }

                $scope.ValidateControls = function () {
                    var IsValid = true;
                    if ($scope.RequestDetails.RecordTypeId == 0) {
                        HFC.DisplayAlert("Please select Request Type");
                        IsValid = false;
                    }
                    if ($scope.RequestDetails.Details == "") {
                        $('textarea[name ="Details"]').addClass("required-error");
                        IsValid = false;
                    }
                    if ($scope.RequestDetails.ProductGroupId == 0) {
                        $('select[name ="ProductCategory"]').addClass("required-error");
                        IsValid = false;
                    }
                    if ($scope.RequestDetails.EffectiveDate == "") {
                        $('input[name ="EffectiveDate"]').addClass("required-error");
                        IsValid = false;
                    }
                    else if (!Date.parse($scope.RequestDetails.EffectiveDate)) {
                        $('input[name ="EffectiveDate"]').addClass("required-error");
                        HFC.DisplayAlert("Invalid date format");
                        IsValid = false;
                    }
                    else if (Date.parse($scope.RequestDetails.EffectiveDate)) {
                        var selectedDate = new Date($scope.RequestDetails.EffectiveDate);
                        var todayDate = new Date();
                        //todayDate.setDate(datelimit.getDate() + 30);
                        if (selectedDate <= todayDate) {
                            HFC.DisplayAlert("Effective Date should be greater than current date");
                            IsValid = false;
                        }
                    }
                    if ($scope.RequestDetails.CurrentStatus == "Declined" && $scope.RequestDetails.StatusId == 0) {
                        $('select[name ="NewStatus"]').addClass("required-error");
                        IsValid = false;
                    }
                    //if (new Date($scope.RequestDetails.EffectiveDate) <= new Date()) {
                    //    HFC.DisplayAlert("Requested effective date should be greater than the current date");
                    //}
                    return IsValid;
                };
                $scope.validateStatus = function () {
                    if ($scope.RequestDetails.StatusId == 0) {
                        $('select[name ="NewStatus"]').addClass("required-error");
                        return false;
                    }
                    else if ($scope.RequestDetails.StatusId != 0 && $scope.RequestDetails.NewStatus == "Approved" && $scope.RequestDetails.PublishMethod == "") {
                        $('select[name ="Publishmethod"]').addClass("required-error");
                        return false;
                    }
                    else if ($scope.RequestDetails.StatusId != 0 && $scope.RequestDetails.NewStatus == "Declined") {
                        if ($scope.RequestDetails.DeclinedReasonId == 0) {
                            $('select[name ="DeclinedReason"]').addClass("required-error");
                            return false;
                        }
                        else if ($scope.RequestDetails.Comments == "") {
                            $('textarea[name ="Comments"]').addClass("required-error");
                            return false;
                        }
                    }

                    return true;
                };

                $scope.UpdateChangeRequest = function () {
                    var ChangeRequest = $scope.RequestDetails;
                    if ($scope.VendorCanEdit && !$scope.ValidateControls())
                        return false;
                    if ($scope.CanChangeStatus && !$scope.validateStatus())
                        return false;
                    $scope.modalState.loaded = false;

                    if ($scope.RequestDetails.StatusId == 13001) {
                        $scope.RequestDetails.DeclinedReasonId = 0;
                        $scope.RequestDetails.Comments = "";
                    }
                    else if ($scope.RequestDetails.StatusId == 13002)
                        $scope.RequestDetails.PublishMethod = "";

                    $http.post('/api/Case/0/SaveChangeRequest', ChangeRequest).then(function (response) {
                        $scope.formCaseManage.$setPristine();
                        //$scope.formCaseManage.setUntouched();
                        if (response.data != null) {
                            var result = response.data.result;
                            //$scope.RequestDetails.CaseNumber = result.CaseNumber;
                            //$scope.BringConfirmModel();
                            $scope.modalState.loaded = true;
                            $window.location.href = "#!/ChangeRequestView/" + $scope.CaseId;
                        }
                        $scope.modalState.loaded = true;
                    });
                }
                $scope.CancelChangeRequest = function () {
                    $window.location.href = "#!/ChangeRequestView/" + $scope.CaseId;
                }

                $scope.BackToChangeRequestList = function () {
                    $window.location.href = "#!/ChangeRequest/";
                }

                //cancel the pop-up
                $scope.CancelPopup = function (modalId) {
                    $("body").removeClass("modal-open");
                    $window.location.href = "#!/ChangeRequest/";
                };
                $scope.ShowPopup = function (modalId) {
                    $("#" + modalId).modal("show");
                }
                $scope.BringConfirmModel = function () {
                    //$scope.ConfiguratorFeedbackService.Configurator.Vendor = $scope.Vendor;
                    //$scope.ConfiguratorFeedbackService.Configurator.ProductCategory = $scope.ProductCategory;
                    //$scope.ConfiguratorFeedbackService.ErrorType = $scope.RequestDetails.RequestType;
                    //$scope.ConfiguratorFeedbackService.Configurator.CaseNumber = $scope.RequestDetails.CaseNumber;
                    //$scope.ConfiguratorFeedbackService.Configurator.CaseType = "Case";
                    $scope.ShowPopup('changeRequestConfirmModal');
                    //$scope.ConfiguratorFeedbackService.ShowConfigurator($scope.CalculatePopupClosed, $scope.receiveDigestCallback);
                };

                $scope.CheckDateValidate = function (date) {
                    var getdate = new Date(date);
                    var datelimit = new Date();
                    datelimit.setDate(datelimit.getDate() + 30);
                    if (datelimit > getdate) {
                        //$scope.EffectiveWarning = true;
                        $scope.RequestDetails.EffectiveDate = "";
                        HFC.DisplayAlert('Request Effective Date should not be less than 30 days.');

                    }
                      //  else {
                    //    //$scope.EffectiveWarning = false;
                    //}
                }

                $scope.StatusChange = function (StatusId) {
                    for (var i = 0; i < $scope.statuslist.length; i++) {
                        if ($scope.statuslist[i].Id == $scope.RequestDetails.StatusId) {//Declined id 13002
                            $scope.RequestDetails.NewStatus = $scope.statuslist[i].Name;
                            break;
                        }
                    }
                    if (StatusId == 13002)
                        $scope.GetDeclinedReasonList();
                    $('select[name ="NewStatus"]').removeClass("required-error");
                }

                $scope.PublishMethodChange = function (PublishMethod) {
                    $('select[name ="Publishmethod"]').removeClass("required-error");
                }

                $scope.ReasonChange = function (ReasonId) {
                    $('select[name ="DeclinedReason"]').removeClass("required-error");
                }

                function OnProductCategoryChange(e) {
                    $('select[name ="ProductCategory"]').removeClass("required-error");
                    if ($scope.RequestDetails.IsAllSelected) {
                        //if ($scope.RequestDetails.ProductGroupId.length > 1) {
                        //    for (var i = 0; i < $scope.RequestDetails.ProductGroupId.length; i++) {
                        //        if ($scope.RequestDetails.ProductGroupId[i] !== 0) {
                        //            $scope.RequestDetails.ProductGroupId.splice(i, 1);
                        //        }
                        //    }

                        //}
                        $scope.RequestDetails.ProductGroupId = [];
                        for (var i = 0; i < $scope.ProductCategory.length; i++) {
                            if ($scope.ProductCategory[i].PICProductGroupId != 0) {
                                $scope.RequestDetails.ProductGroupId.push($scope.ProductCategory[i].PICProductGroupId);
                            }
                        }
                        setTimeout(function () {
                            $scope.$apply();
                        }, 100);
                    }
                    else {
                        if ($scope.RequestDetails.ProductGroupId.length > 1) {
                            for (var i = 0; i < $scope.RequestDetails.ProductGroupId.length; i++) {
                                if ($scope.RequestDetails.ProductGroupId[i] === 0) {
                                    $scope.RequestDetails.ProductGroupId.splice(i, 1);
                                }
                            }

                        }
                    }
                }

                function OnProductCategorySelect(e) {
                    var dataItem = e.dataItem;
                    if (dataItem.PICProductGroupId == 0) {
                        $scope.RequestDetails.IsAllSelected = true;
                    }
                    else {
                        $scope.RequestDetails.IsAllSelected = false;
                    }
                }

                function OnProductCategoryDeSelect(e) {
                    if (e.dataItem.PICProductGroupId != 0)
                        $scope.RequestDetails.IsAllSelected = false;
                    //var dataItem = e.dataItem;
                    //if (dataItem.PICProductGroupId == 0) {
                    //    $scope.RequestDetails.IsAllSelected = false;
                    //}
                    //else {
                    //    $scope.RequestDetails.IsAllSelected = true;
                    //}
                }

                $scope.GetDeclinedReasonList = function () {
                    $http.get('/api/Case/0/GetFeedbackChangeLookup?TableName=DeclinedReason').then(function (response) {
                        $scope.DeclinedReasons = response.data;
                        //$.each($scope.DeclinedReasons, function (index, jsonObject) {
                        //    jsonObject.DeclinedReasonId = parseInt(jsonObject.DeclinedReasonId);
                        //});
                        //console.log($scope.DeclinedReasons);
                        ////$scope.DeclinedReasons = DeclinedList;
                    });
                }

                $scope.RemoveRequiredClass = function (controlId) {
                    var cId = controlId.currentTarget.name;
                    $("textarea[name=" + cId + "]").removeClass("required-error");
                }

                $scope.GetValue = function () {
                    var ErrorTypeId = $scope.RequestDetails.RecordTypeId;
                    for (var i = 0; i < $scope.RequestTypes.length; i++) {
                        if ($scope.RequestTypes[i].Id == ErrorTypeId) {
                            $scope.RequestDetails.RequestType = $scope.RequestTypes[i].Name;
                            break;
                        }
                    }
                };

                $scope.Initialvalue = function () {
                    $scope.ProductCategoryOptions = {
                        placeholder: "Select",
                        dataTextField: "PICProductGroupName",
                        dataValueField: "PICProductGroupId",
                        valuePrimitive: true,
                        height: 300,
                        autoBind: true,
                        select: OnProductCategorySelect,
                        deselect: OnProductCategoryDeSelect,
                        change: OnProductCategoryChange,
                        filter: "contains",
                        filtering: function (e) {
                            var filter = e.filter;
                        },
                        dataSource: {
                            transport: {
                                read: function (options) {
                                    $http.get('/api/PIC/0/GetPICProductGroupForConfigurator?franchiseCode=' + $scope.FranchiseCode + '&vendorConfig=' + $scope.VendorLogin).then(function (response) {
                                        $scope.ProductCategory = response.data;
                                        if ($scope.ProductCategory != null && $scope.ProductCategory.Length != 0) {
                                            $scope.ProductCategory.splice(0, 0, { PICProductGroupId: "0", PICProductGroupName: "ALL" })
                                            $.each($scope.ProductCategory, function (index, jsonObject) {
                                                jsonObject.PICProductGroupId = parseInt(jsonObject.PICProductGroupId);
                                            });
                                            $scope.RequestDetails.Vendor = $scope.ProductCategory[1].VendorName;
                                            $scope.RequestDetails.VendorId = $scope.ProductCategory[1].VendorId;
                                            $scope.RequestDetails.CaseNumber = 'PCR-' + $scope.ProductCategory[1].PICVendorId;
                                            options.success($scope.ProductCategory);
                                        }
                                    });
                                }
                            }
                        }
                    };
                    //$scope.loadProductList();
                    $scope.loadChangeRequestTypelist();
                };

                if (($window.location.href.toUpperCase().includes('CHANGEREQUESTCREATE'))) {
                    HFCService.setHeaderTitle("Change Request Create");
                    $scope.Initialvalue();
                }


                //View & edit
                $scope.GetChangeRequest = function () {
                    if (($window.location.href.toUpperCase().includes('CHANGEREQUESTEDIT')))
                        HFCService.setHeaderTitle("Change Request Edit " + $scope.CaseId);
                    else if (($window.location.href.toUpperCase().includes('CHANGEREQUESTVIEW')))
                        HFCService.setHeaderTitle("Change Request View " + $scope.CaseId);

                    $scope.Initialvalue();
                    $scope.modalState.loaded = false;
                    $http.get('/api/Case/' + $scope.CaseId + '/GetChangeRequestById').then(function (response) {
                        //console.log(response);
                        $scope.RequestDetails = response.data;
                        $scope.RequestDetails.Currentuser = FranchiseService.UserData.FullName;
                        $scope.RequestDetails.Currenttime = HFCService.KendoDateTimeFormat(new Date($scope.RequestDetails.CurrentTime));
                        $scope.RequestDetails.LastUpdatedOn = HFCService.KendoDateTimeFormat(new Date($scope.RequestDetails.LastUpdatedOn));

                        $scope.RequestDetails.CreatedOn = HFCService.KendoDateTimeFormat(new Date($scope.RequestDetails.CreatedOn));
                        $scope.RequestDate = HFCService.KendoDateFormat(new Date($scope.RequestDetails.CreatedOn));
                        $scope.RequestDetails.EffectiveDate = HFCService.KendoDateFormat(new Date($scope.RequestDetails.EffectiveDate));
                        //if (!($scope.RolesString.includes("Vendor") && $scope.RequestDetails.CurrentStatus == "Submitted"))
                        //{
                        //    $scope.RequestDetails.StatusId = 0;
                        //}
                        //else if ($scope.RequestDetails.CurrentStatus == "Approved") {
                        //    $scope.IsPMApproved = true;
                        //}                                       
                        //$scope.IsPICPM = true;
                        // $scope.PMCanEdit = true;
                        //if ($scope.RequestDetails.CurrentStatus == "Declined") {
                        $scope.loadstatus();
                        //};
                        $scope.modalState.loaded = true;

                    }).catch(function () { $scope.modalState.loaded = true; });
                };

                //set caseId for comments only in view and edit mode
                if (($window.location.href.toUpperCase().includes('CHANGEREQUESTVIEW')) || ($window.location.href.toUpperCase().includes('CHANGEREQUESTEDIT'))) {
                    $scope.CaseId = $routeParams.CaseId;
                    $scope.forComments = { CaseId: $scope.CaseId };
                    $scope.GetChangeRequest();
                }

                ////Edit
                //if (($window.location.href.toUpperCase().includes('CHANGEREQUESTEDIT'))) {
                //    $scope.CaseId = $routeParams.CaseId;
                //    $scope.forComments = { CaseId: $scope.CaseId };
                //    $scope.GetChangeRequest();
                //}

                $scope.VendorAttachment = {
                    dataSource: {
                        error: function (e) {
                            HFC.DisplayAlert(e.errorThrown);
                        },
                        schema: {
                            model: {
                                Id: "Id",
                                fields: {
                                    Id: { editable: false, nullable: true },
                                    Attachment: { editable: false, type: "string" },
                                    CreatedOn: { editable: false, type: "string" },
                                    Size: { editable: false, type: "string" }
                                }
                            }
                        }
                    },
                    columns: [
                        {
                            field: "Attachment",
                            title: "Attachment"
                        },
                        {
                            field: "CreatedOn",
                            title: "Created On",
                        },
                        {
                            field: "Size",
                            title: ""
                        }
                    ],
                    noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                    filterable: false,
                    resizable: false,
                    sortable: false,
                    scrollable: false,
                    pageable: false,
                    //toolbar: [
                    //        {
                    //            template: kendo.template($("#headToolbarTemplate").html())
                    //        }]
                };

                // case history
                $scope.ReportDetailHistory = {
                    dataSource: {
                        type: "jsonp",
                        transport: {
                            cache: false,
                            read: function (e) {
                                //if ($scope.ReportID) {
                                var url = "/api/Case/" + $scope.CaseId + "/GetCaseHistoryForCase";
                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                                //}
                            },
                        },

                    },
                    batch: true,
                    cache: false,
                    sortable: {
                        mode: "single",
                        allowUnsort: false
                    },
                    pageable: {
                        pageSize: 10,
                        pageSizes: [10, 20, 30, 50, 100],
                        buttonCount: 5
                    },
                    scrollable: true,
                    noRecords: { template: "No report history found" },
                    columns: [
                        {
                            field: "LastUpdatedOn", title: "Date",
                            width: "150px",
                            template: "#= kendo.toString(kendo.parseDate(LastUpdatedOn), 'MM/dd/yyyy hh:mm tt') #"
                        },
                        {
                            field: "Field",
                            title: "Field",
                            width: "120px",
                            template: function (dataitem) {

                                if (dataitem.Field == 'VPO_PICPO_PODId')
                                    return '<div>VPO</div>'
                                else if (dataitem.Field == 'MPO_MasterPONum_POId')
                                    return '<div>MPO</div>'
                                else if (dataitem.Field == 'SalesOrderId')
                                    return '<div>Sales</div>'
                                else if (dataitem.Field == 'Description')
                                    return '<div>Desc</div>'
                                else
                                    return '<div>' + dataitem.Field + '</div>'
                            }
                        },
                        { field: "UserName", title: "User", width: "120px", },
                        { field: "OriginalValue", title: "Original Value", width: "150px", },
                        { field: "NewValue", title: "New Value", width: "150px", }
                    ],
                    columnResize: function (e) {
                        getUpdatedColumnList(e);
                    }
                };

                if ($scope.CaseId) {
                    $scope.FileUploadServiceVg.SetCaseId($scope.CaseId, 'Change');
                }
                else $scope.FileUploadServiceVg.clearCaseId();

                if (window.location.href.toUpperCase().includes("CHANGEREQUESTCREATE"))
                    $scope.FileUploadServiceVg.SetCaseId($scope.CaseId, 'Change');

                if (window.location.href.includes('/FeedbackUpdate/')) {
                    $scope.FileUploadServiceVg.css1 = "fileupload_ltgrid";
                    $scope.FileUploadServiceVg.css2 = "";
                    $scope.FileUploadServiceVg.css3 = "fileupload_rtgrid";
                }

                $scope.CreateChangeRequest = function () {
                    $window.location.href = "#!/ChangeRequestCreate";
                }

                //for scrolling in kanban view
                if (($window.location.href.toUpperCase().includes('CHANGEREQUESTKANBANVIEW'))) {
                    HFCService.setHeaderTitle("ChangeRequestKanbanView #");
                    var desiredHeight = window.innerHeight - $("#MenuDiv1").height() - 215;
                    $("#KanbanDiv").css("height", desiredHeight);
                }
                window.onresize = function () {
                    setTimeout(function () {
                        //for resizing card heights on page resize
                        $scope.fixCardHeights();
                        var desiredHeight = window.innerHeight - $("#MenuDiv1").height() - 215;
                        $("#KanbanDiv").css("height", desiredHeight);
                    }, 50);
                };
                //for setting heights to kanban cards
                $scope.fixCardHeights = function () {
                    var heights = new Array();
                    // Loop to get all element heights
                    $('.card').each(function () {
                        // Need to let sizes be whatever they want so no overflow on resize
                        $(this).css('min-height', '0');
                        $(this).css('max-height', 'none');
                        $(this).css('height', 'auto');
                        // Then add size (no units) to array
                        heights.push($(this).height());
                    });
                    // Find max height of all elements
                    var max = Math.max.apply(Math, heights);
                    max = max + 15;
                    // Set all heights to max height
                    $('.card').each(function () {
                        $(this).css('height', max + 'px');
                    });
                }
                $scope.$watch('FileUploadServiceVg.files', function (newVal, oldVal, scope) {
                    if (newVal.length == 0 && document.getElementById("uploadFileInput")) {
                        document.getElementById("uploadFileInput").value = null;
                    }
                });
            }
        ])