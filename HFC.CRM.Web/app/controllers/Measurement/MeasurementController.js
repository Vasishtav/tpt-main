﻿
app.controller('MeasurementController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'HFCService', 'FileprintService', 'NavbarService',
    'NoteServiceTP', 'PersonService', 'AccountService', 'DialogService', 'LeadAccountService', 'kendoService', 'kendotooltipService', 'SquareFeetCalculatorService',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, HFCService, FileprintService, NavbarService,
        NoteServiceTP, PersonService, AccountService, dialogService, LeadAccountService, kendoService, kendotooltipService, SquareFeetCalculatorService) {

        $scope.GridSizeCalled = 0;
        $scope.NavbarService = NavbarService;
        SquareFeetCalculatorService.SquareFeetGrid();
        $scope.NavbarService.SelectSales();

        $scope.copyLineText = "Copy Line #";
        $scope.preventChange = false;
        $scope.measurementKeyList = ["id", "RoomLocation", "RoomName", "WindowLocation", "MountTypeName", "fileName", "Stream_id", "thumb_Stream_id", "QuoteLineId", "QuoteLineNumber", "Width", "FranctionalValueWidth", "Height", "FranctionalValueHeight", "Comments"];
        // for route handling in new quote
        $scope.enableAddressvalidationFlag = false;
        $scope.routeQuoteFlag = false;

        if (window.location.href.includes('measurementDetails'))
            $scope.NavbarService.EnableSalesAccountsTab();
        else if (window.location.href.includes('measurementDetail'))
            $scope.NavbarService.EnableSalesOpportunitiesTab();
        else
            $scope.NavbarService.EnableSalesAccountsTab();

        $scope.selectedFiles = [];
        $scope.PhotoFilename = null;
        $scope.isLoadData = false;
        $scope.treeData = [];
        $scope.editableCampaign = {};
        $scope.editableSource = {};
        $scope.permissions = {};
        $scope.opportunityId = null;
        $scope.sources = [];
        $scope.rootSources = [];
        $scope.newSource = {};
        $scope.HFCService = HFCService;
        $scope.currentEditId = null;
        $scope.rowNumberr = 0;

        $scope.EditDataItem = {};
        $scope.isAddMode = false;
        $scope.brandid = $scope.HFCService.CurrentBrand;
        $scope.FileprintService = FileprintService;
        $scope.NoteServiceTP = NoteServiceTP;
        $scope.PersonService = PersonService;
        $scope.AccountService = AccountService;
        $scope.LeadAccountService = LeadAccountService;
        // Appointment
        HFCService.GetUrl();
        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;
        //grey select option
        $scope.BrandId = $scope.HFCService.CurrentBrand;

        $scope.Permission = {};
        var Measurementpermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Measurement')
        var Accountpermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Account')
        var Opportunitypermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Opportunity')
        var Invoicepermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Invoice')
        var SalesOrderpermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'SalesOrder')
        var Paymentpermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Payment')

        $scope.Permission.DisplayMeasurements = Measurementpermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Measurements').CanAccess;
        $scope.Permission.AddMeasurements = Measurementpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Measurements').CanAccess;
        $scope.Permission.EditMeasurements = Measurementpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Measurements').CanAccess;
        $scope.Permission.ListMeasurements = Measurementpermission.CanRead; //$scope.HFCService.GetPermissionTP('List Measurements').CanAccess;
        //$scope.Permission.PrintOpportunity = $scope.HFCService.GetPermissionTP('Print Installation Packet').CanAccess;

        //$scope.Permission.AddEvent = $scope.HFCService.GetPermissionTP('Add Event').CanAccess;
        $scope.Permission.AddAccount = Accountpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Account').CanAccess;
        $scope.Permission.ListOpportunity = Opportunitypermission.CanRead; //$scope.HFCService.GetPermissionTP('List Opportunity').CanAccess;
        $scope.Permission.AccountInvoice = Invoicepermission.CanRead; //$scope.HFCService.GetPermissionTP('List Invoice').CanAccess;
        $scope.Permission.AccountPayment = Paymentpermission.CanRead; //$scope.HFCService.GetPermissionTP('List Payments').CanAccess;
        $scope.Permission.AccountOrderList = SalesOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('List Order').CanAccess;
        $scope.Permission.AccountView = Accountpermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Account').CanAccess;
        $scope.Permission.AccountEdit = Accountpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Account').CanAccess;
        $scope.Permission.AccountQualify = Accountpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Qaulify Account').CanAccess;
        $scope.Permission.AddNotesAccount = Accountpermission.CanCreate; //HFCService.GetPermissionTP('Add Notes&Attachments-Account').CanAccess;

        $scope.AccountName = '';
        $scope.InstallationAddressId = 0;
        $scope.saveRedirectCancel = false;
        $scope.EditMode = false;
        $scope.TextboxDisabled = false;
        //$scope.measurementAccountId = $routeParams.AccountId;
        $scope.AccountId = $routeParams.AccountId;
        $scope.OpportunityId = $routeParams.OpportunityId;
        $scope.InstallationAddressId = $routeParams.AddressId;
        $scope.MeasurementDatasource = {};
        $scope.InstallationAddress = '';

        $scope.SquareFeetHeader = [];

        function GetOpportunityDetails() {
            $scope.opportunity = $http.get('/api/Measurement/' + $scope.OpportunityId + '/GetOpportunityMeasurement').then(function (response) {
                if (response.data.valid === true) {
                    $scope.AccountAppointmet = response.data.data.AccountId;
                    $scope.AccountName = response.data.data.AccountName;
                    $scope.InstallationAddress = response.data.data.InstallationAddress;
                    $scope.IsValidated = response.data.data.IsValidated;
                    $scope.OpportunityName = response.data.data.OpportunityName;
                    $scope.GetMeasurmentData();
                    HFCService.setHeaderTitle("Opportunity Measurement #" + $scope.OpportunityId + " - " + $scope.AccountName);
                } else {
                    HFC.DisplayAlert(response.data.error);
                }
            });
        }

        if ($scope.OpportunityId != undefined && $scope.OpportunityId != 0 && $scope.OpportunityId !== null) {

            GetOpportunityDetails();
        }

        function getInstallationAddress() {
            $http.get("/api/Measurement/" + $scope.AccountId + "/InstallationAddresses").then(function (response) {

                if (response.data.valid === true) {
                    $scope.InstallationAddresslist = response.data.data;
                    $scope.AccountName = response.data.data[0].AccountName;
                    if ($scope.InstallationAddressId === 0 || $scope.InstallationAddressId === undefined) {
                        $scope.InstallationAddressId = response.data.data[0].InstallAddressId;
                    }
                    $scope.IsValidated = response.data.data[0].IsValidated;
                    $scope.GetMeasurmentData();
                    HFCService.setHeaderTitle("Account Measurement #" + $scope.AccountId + " - " + $scope.AccountName);
                } else {
                    HFC.DisplayAlert(response.data.error);
                }
            });
        }

        $http.get('/api/Lookup/0/GetRoomLocation').then(function (response) {
            $scope.RoomLocationDrop = response.data;
        })

        $scope.measurementGridReload = function (val) {

            if (val) {
                var address_data = $scope.InstallationAddresslist;
                for (var i = 0; i < address_data.length; i++) {
                    if (address_data[i].InstallAddressId == val) {
                        $scope.IsValidated = address_data[i].IsValidated;
                    }
                }
                $scope.InstallationAddressId = val;
                $scope.GetMeasurmentData();
            }
            else {
                $scope.InstallationAddressId = "";
            }

            $("#searchBox").val("");
            $scope.Measurement_detail_form.$setPristine();
        }

        if ($scope.AccountId != undefined && $scope.AccountId != 0 && $scope.AccountId !== null) {
            getInstallationAddress();
            if ($scope.AccountId > 0) {

                LeadAccountService.Get($scope.AccountId, function (response) {
                    $scope.appointAccount = response;
                });
            }
        }

        $scope.modalState = {
            loaded: true
        }

        $scope.$watch('modalState.loaded', function () {
            var loadingElement = document.getElementById("loading");

            if (!$scope.modalState.loaded) {
                loadingElement.style.display = "block";
            } else {
                loadingElement.style.display = "none";
            }
        });

        // The callback for successful calculatepopupclosed
        $scope.CalculatePopupClosed = function () {
            if (SquareFeetCalculatorService.NetSurfaceArea) {
                if ((SquareFeetCalculatorService.SquareFeetHeader.Add != null && SquareFeetCalculatorService.SquareFeetHeader.Add.length > 0) || (SquareFeetCalculatorService.SquareFeetHeader.Sub != null && SquareFeetCalculatorService.SquareFeetHeader.Sub.length > 0)) {
                    var Area = $("#Area").data("kendoNumericTextBox");
                    Area.value(SquareFeetCalculatorService.NetSurfaceArea);
                    Area.enable(false);
                }
                else {
                    var Area = $("#Area").data("kendoNumericTextBox");
                    Area.value(SquareFeetCalculatorService.NetSurfaceArea);
                    Area.enable(true);
                }

            }

            var dataSource = $("#gridEditMeasurements").data("kendoGrid").dataSource._data;

            angular.forEach(dataSource, function (value, key) {
                if (value.id == $scope.currentEditId) {
                    value.SquareFeetAdd = SquareFeetCalculatorService.SquareFeetHeader.Add;
                    value.SquareFeetSub = SquareFeetCalculatorService.SquareFeetHeader.Sub;
                    value.Area = SquareFeetCalculatorService.NetSurfaceArea;
                }
            });
        }

        $scope.receiveDigestCallback = function () {
            $rootScope.$digest();
        }

        function errorHandler(reseponse) {
            // Inform that the modal is ready to consume:
            //$scope.modalState.loaded = true;
            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
        }

        // Added for [Add new quote] button in Opportunity measurement display page.
        $scope.preserveQuoteId = '';
        $scope.Newquotepage = function (id) {
            if ((($scope.EditMode || !$scope.enableAddressvalidationFlag) && ($scope.EnablCopyFlag)) && $scope.brandid == 1) {
                $scope.routeQuoteFlag = true;
                $scope.preserveQuoteId = id;
                $scope.cancelMeasurementEdit();
            } else {
                $scope.preserveQuoteId = '';
                $scope.routeQuoteFlag = false; $scope.EnablCopyFlag = false;
                $scope.enableAddressvalidationFlag = false;
                if ($scope.OpportunityId == id) {

                    $http.get('/api/Opportunities/' + $scope.OpportunityId).then(function (data) {
                        var opportunity = data.data.Opportunity;
                        if (opportunity.InstallationAddressId) {
                            $http.get('/api/opportunities/' + opportunity.InstallationAddressId + '/getInstalltionAddressName')
                                .then(function (data) {
                                    $scope.IsValidate = data.data.IsValidated;
                                    if ($scope.IsValidate == false) {

                                        var errorMessage = 'The Installation Address is not validated, do you still wish to proceed?';
                                        if (confirm(errorMessage)) {
                                            $scope.NewQuote();

                                        } else {
                                            $scope.IsBusy = false;
                                            $scope.preventChange = false;
                                        }
                                    } else
                                        $scope.NewQuote();

                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";

                                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                                });
                        }
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";

                        //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                    });
                }
            }
        };

        $scope.NewQuote = function () {
            $scope.preventChange = false;
            if ($scope.Quotation.QuoteKey == 0) {
                //  var url = "http://" + $window.location.host + "/#!/quote/" + $scope.OpportunityId;
                $window.location.href = "#!/quote/" + $scope.OpportunityId;;
            } else {
                $window.location.href = "#!/quote/" + $scope.OpportunityId + "/" + $scope.Quotation.QuoteKey;;
            }
        }

        $scope.GetQuotationdeatils = function () {
            if ($scope.OpportunityId !== undefined && $scope.OpportunityId !== null && $scope.OpportunityId !== "")
                $http.get('/api/Quotes/0/GetDetails?oppurtunityId=' + $scope.OpportunityId).then(function (data) {
                    $scope.Quotation = data.data;
                });
        }

        $scope.GetQuotationdeatils();
        //---End---

        $scope.GetMeasurmentData = function () {
            if ($scope.InstallationAddressId != undefined &&
                $scope.InstallationAddressId != 0 &&
                $scope.InstallationAddressId !== null) {

                // Inform that the modal is not yet ready
                //$scope.modalState.loaded = false;
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);

                $http.get('/api/Measurement/' + $scope.InstallationAddressId + '/Get').then(function (response) {
                    $scope.MeasurementDatasource = null;

                    if (response.data != null && response.data !== '' && response.data != undefined) {
                        $scope.MeasurementDatasource = response.data;
                        var dataSource = new kendo.data.DataSource({ data: response.data, pageSize: 1000 });

                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        dataSource.read();
                        $scope.OldMeasurementValues = dataSource;
                        grid.setDataSource(dataSource);

                    }
                    else {
                        var dataSource = new kendo.data.DataSource({ data: null });
                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        dataSource.read();
                        $scope.OldMeasurementValues = [];
                        grid.setDataSource(null);
                    }

                    // Inform that the modal is ready to consume:
                    $scope.modalState.loaded = true;
                    $('#btnReset').prop('disabled', true);
                    //$scope.GetDataLength();
                }, errorHandler);


                $(".k-grid").off("mousedown", ".k-grid-header th");
                $(".k-grid").on("mousedown", ".k-grid-header th", function (e) {
                    // prevent sorting/filtering for the current Grid only
                    var grid = $(this).closest(".k-grid");
                    var editRow = grid.find(".k-grid-edit-row");

                    if (editRow.length > 0) {
                        alert("Please complete the editing operation before sorting or filtering");
                        e.preventDefault();
                    }
                });
            }
        };

        $scope.photoIconsShows = function () {
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            var dataSource = grid.dataSource._data;
            var unsavedList = [];
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                    unsavedList.push(dataSource[i]);
                }
            }
            for (var j = 0; j < unsavedList.length; j++) {
                if (unsavedList[j]['Stream_id'] || unsavedList[j]['fileName']) {
                    $("#photoFileName" + j).text(unsavedList[j]['fileName']);

                    $("#photoFileName" + j).css("visibility", "visible");
                    $("#photoupload" + j).css("visibility", "hidden");
                    $("#photoCameraIcon" + j).css("visibility", "visible");
                    $("#photoMain" + j).css("display", "none");
                    $("#photoTitile" + j).css("display", "block");

                } else {
                    $("#photoFileName" + j).css("visibility", "hidden");
                    $("#photoupload" + j).css("visibility", "visible");
                    $("#photoCameraIcon" + j).css("visibility", "hidden");
                    $("#photoMain" + j).css("display", "block");
                    $("#photoTitile" + j).css("display", "none");
                    //$(".photo-title").css("Visibility", "none");
                }
            }
        }


        function readFileBase64(file, max_size) {
            max_size_bytes = max_size * 1048576;
            return new Promise((resolve, reject) => {
                if (file.size > max_size_bytes) {
                    console.log("file is too big at " + (file.size / 1048576) + "MB");
                    reject("file exceeds max size of " + max_size + "MB");
                }
                else {
                    var fr = new FileReader();
                    fr.onloadend = () => {
                        data = fr.result;
                        resolve(data)
                    };
                    fr.readAsDataURL(file);
                }
            });
        }

        $scope.fileNameChanged = function (flag, datasValue) {
            $scope.grid = $("#gridEditMeasurements").data("kendoGrid");
            $scope.dataSource = $scope.grid.dataSource._data;
            var photodata = []; var Currentdata = ""; $scope.idOffset;
            if (flag) {
                if (this.dataItem)
                    Currentdata = this.dataItem;
                else Currentdata = datasValue;
                for (var i = 0; i < $scope.dataSource.length; i++) {
                    if ($scope.dataSource[i]['NotSave'] == true || $scope.dataSource[i]['editable'] == true) {
                        photodata.push($scope.dataSource[i]);
                    }
                }
                for (var j = 0; j < photodata.length; j++) {
                    if (photodata[j]['id'] == Currentdata.id) {
                        $scope.idOffset = j;
                    }
                }
                $scope.files = document.getElementById('photoupload' + $scope.idOffset).files[0];
                $scope.currentEditId = Currentdata.id;
            } else {
                $scope.files = document.getElementById('uploadFileInput_uploadPhoto').files[0];
                $scope.idOffset = null;
            }


            var nnn = readFileBase64($scope.files, 200000);
            nnn.then(function (base64) {
                $scope.Base64 = base64;
                if ($scope.files.type.includes("image/")) {
                    var www = $scope.currentEditId;

                    // console.log($scope.selectedFiles);

                    if ($scope.files) {

                        var ext = $scope.files.name.split('.').pop();
                        if ($scope.idOffset != null) {
                            $("#photoFileName" + $scope.idOffset).text($scope.files.name);
                            $("#photoFileName" + $scope.idOffset).css("visibility", "visible");
                            $("#photoupload" + $scope.idOffset).css("visibility", "hidden");
                            $("#photoCameraIcon" + $scope.idOffset).css("visibility", "visible");
                            $("#photoMain" + $scope.idOffset).css("display", "none");
                            $("#photoTitile" + $scope.idOffset).css("display", "block");

                        } else {
                            $scope.PhotoFilename = $scope.files.name;
                        }


                        $scope.Base64 = $scope.Base64.split(",").pop();
                        //  $scope.selectedFiles.push({ id: $scope.currentEditId, name: $scope.files.name, base64: $scope.Base64 });
                        angular.forEach($scope.dataSource, function (value, key) {

                            if (value.id == $scope.currentEditId) {
                                $scope.selectedFiles.push({ id: $scope.currentEditId, name: $scope.files.name, base64: $scope.Base64, uid: value.uid });
                                value.fileName = $scope.files.name;
                                value.Stream_id = null;
                            }

                        });
                        $scope.changeFlag = true; //to state user made changes
                        $scope.$apply();
                    }
                }
                else {
                    alert("Only image files are allowed")
                    $scope.files = null;
                }

            });
        }

        $scope.uploadFileInput_uploadPhotoClick = function () {

        }

        $scope.showCameraIcon = function (e) {
            $scope.PhotoFilename = null;
            $scope.grid = $("#gridEditMeasurements").data("kendoGrid");
            $scope.dataSource = $scope.grid.dataSource._data;

            if (e) {//added for the copy and multi copy
                var idStr = e.target.id;
                var selectedElementId = idStr.split("n")[1];

                var unsavedList = [];
                for (var i = 0; i < $scope.dataSource.length; i++) {
                    if ($scope.dataSource[i]['NotSave'] || $scope.dataSource[i]['editable']) {
                        unsavedList.push($scope.dataSource[i]);
                    }
                }
                var dataItem = unsavedList[selectedElementId];
                //dataItem.RoomLocation = updatedRoomvalue;
                angular.forEach(unsavedList, function (value, key) {

                    if (value.id == dataItem.id) {
                        value.fileName = $scope.PhotoFilename;
                        value.Stream_id = null;
                    }
                });
                //added since if cancel the photo icon data is exists and data is saved empty.
                angular.forEach($scope.selectedFiles, function (value, key) {
                    if (value.id == dataItem.id) {
                        $scope.selectedFiles.splice(key, 1);
                    }
                    var mm = $scope.selectedFiles;
                });

                angular.forEach($scope.dataSource, function (value, key) {

                    if (value.id == dataItem.id) {
                        value.fileName = $scope.PhotoFilename;
                        value.Stream_id = null;
                    }
                });
                //end.
                $("#photoFileName" + selectedElementId).css("visibility", "hidden");
                $("#photoupload" + selectedElementId).css("visibility", "visible");
                $("#photoCameraIcon" + selectedElementId).css("visibility", "hidden");
                $("#photoMain" + selectedElementId).css("display", "block");
                $("#photoTitile" + selectedElementId).css("display", "none");
                $("#photoupload" + selectedElementId).val(""); //added since when cancel and try to add same image it will not.
                $scope.changeFlag = true; //to state user made changes

            } else {
                angular.forEach($scope.selectedFiles, function (value, key) {
                    if (value.id == $scope.currentEditId) {
                        $scope.selectedFiles.splice(key, 1);
                    }
                    var mm = $scope.selectedFiles;
                });

                angular.forEach($scope.dataSource, function (value, key) {

                    if (value.id == $scope.currentEditId) {
                        value.fileName = $scope.PhotoFilename;
                        value.Stream_id = null;
                    }
                });
            }
        }

        $scope.rowNumber = 0;
        function renderNumber() {
            $scope.rowNumber = $scope.rowNumber + 1;
            $scope.rowNumberr = $scope.rowNumber;
            return $scope.rowNumber;
        }

        function resetRowNumber(e) {
            $scope.rowNumber = 0;
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            var dataSourcee = grid.dataSource;
            if (dataSourcee._data.length === 0) {
                $scope.rowNumberr = 0;
            }

            if (kendotooltipService.columnWidth) {
                kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
            } else if (window.innerWidth < 1280) {
                kendotooltipService.restrictTooltip(null);
            }
            //var view = dataSourcee.view();
            //$scope.rowNumberr = dataSourcee._data.length;

            //$scope.rowNumber = dataSourcee.pageSize() * (dataSourcee.page() - 1);
            var headerHeight = $(".k-header.k-grid-toolbar").height() + "px";
            $(".header-hide-wrapper").css("top", headerHeight);
            setWidthHeightColor(dataSourcee._data);


            var data = e.sender.dataSource._data;
            var flag = false;
            for (var i = 0; i < data.length; i++) {
                if (data[i]['NotSave'] || data[i]['editable']) {
                    flag = true;
                }
            }
            if (flag) {
                // intialize dropddowns in grid
                intializeDropdownFields(e);
                $scope.photoIconsShows();
            }


        }

        // set color
        function setWidthHeightColor(data) {
            if (data && data.length !== 0 && $scope.brandid != 3) {
                var gridRows = $("#gridEditMeasurements .k-grid-content.k-auto-scrollable tr");
                var lastOffset;
                for (var i = 0; i < data.length; i++) {
                    if (data[i]['NotSave']) {
                        $($($(gridRows)[i]).children()[5]).css("padding-right", "0px");
                        $($($($(gridRows)[i]).children()[5]).children()[0]).css({ "border": "1px solid #3e7d8a", "border-right": "none", "padding-right": "0px" });
                        $($($(gridRows)[i]).children()[6]).css("padding-left", "0px");
                        $($($($(gridRows)[i]).children()[6]).children()[0]).css({ "padding-left": "0px", "border": "1px solid #3e7d8a", "border-left": "none" });
                        lastOffset = i;
                    }
                }
            }
        }

        // get data for window & mount
        var getDropdownData = function () {
            if ($scope.brandid === 1) {
                $http.get('/api/Lookup/0/GetWindowLocation').then(function (data) {
                    $scope.windowDataBackup = data.data;
                });

                $http.get('/api/Lookup/0/GetMountType').then(function (data) {
                    $scope.mountDataBackup = data.data;
                });
            } else if ($scope.brandid === 2 || $scope.brandid === 3) {
                $http.get('/api/Lookup/0/GetSystem').then(function (data) {
                    $scope.systemDataBackup = data.data;
                });
            }
        }

        // get data for copy scenario
        setTimeout(function () {
            getDropdownData();
        }, 1000);

        // intialize the dropdown fields in grid template to make it editable
        var intializeDropdownFields = function (e) {
            $scope.dataCount = 0;
            var roomLocation = $(".room-location");
            for (var i = 0; i < roomLocation.length; i++) {
                var idValue = 'roomLocation' + i;
                $(roomLocation[i]).prop("id", idValue);
            }

            var windowLocation = $(".window-location");
            for (var i = 0; i < windowLocation.length; i++) {
                var idValue = 'windowLocation' + i;
                $(windowLocation[i]).prop("id", idValue);
            }

            var mountList = $('.mount');
            for (var i = 0; i < mountList.length; i++) {
                var idValue = 'mount' + i;
                $(mountList[i]).prop("id", idValue);
            }

            var widthList = $(".width-field");
            for (var i = 0; i < widthList.length; i++) {
                var idValue = 'width' + i;
                $(widthList[i]).prop("id", idValue);
            }

            var widthFractionList = $(".width-fraction");
            for (var i = 0; i < widthFractionList.length; i++) {
                var idValue = 'widthFraction' + i;
                $(widthFractionList[i]).prop("id", idValue);
            }

            var heightList = $(".height-field");
            for (var i = 0; i < heightList.length; i++) {
                var idValue = 'height' + i;
                $(heightList[i]).prop("id", idValue);
            }

            var heightFractionList = $(".height-fraction");
            for (var i = 0; i < heightFractionList.length; i++) {
                var idValue = 'heightFraction' + i;
                $(heightFractionList[i]).prop("id", idValue);
            }

            var RoomNameArea = $(".roomname");
            for (var i = 0; i < RoomNameArea.length; i++) {
                var idValue = 'roomname' + i;
                $(RoomNameArea[i]).prop("id", idValue);
            }

            var PhotoArea = $(".photoupload");
            for (var i = 0; i < PhotoArea.length; i++) {
                var idValue = 'photoupload' + i;
                $(PhotoArea[i]).prop("id", idValue);
            }

            var PhotoName = $(".photo-file-name");
            for (var i = 0; i < PhotoName.length; i++) {
                var idValue = 'photoFileName' + i;
                $(PhotoName[i]).prop("id", idValue);
            }

            var PhotoMain = $(".photo_main");
            for (var i = 0; i < PhotoMain.length; i++) {
                var idValue = 'photoMain' + i;
                $(PhotoMain[i]).prop("id", idValue);
            }

            var PhotoCamera = $(".show_cameraicon");
            for (var i = 0; i < PhotoCamera.length; i++) {
                var idValue = 'photoCameraIcon' + i;
                $(PhotoCamera[i]).prop("id", idValue);
            }

            var PhotoTitile = $(".photo-title");
            for (var i = 0; i < PhotoTitile.length; i++) {
                var idValue = 'photoTitile' + i;
                $(PhotoTitile[i]).prop("id", idValue);
            }

            var widthField = $(".width-field")[0];
            if (!$scope.actionFlag) {
                $(widthField).focus();
            }
            var commentList = $('.comment_area');
            for (var i = 0; i < commentList.length; i++) {
                var idValue = 'comment_area' + i;
                $(commentList[i]).prop("id", idValue);
            }
            $(".show_cameraicon").css("visibility", "hidden");

            var loading = $("#loading");
            $(loading).css("display", "none");
        }

        $scope.renderControl = function (e, dataItem) {
            var ctrlId = e.target.id;
            var data = dataItem;
            var className = e.target.className;
            $("#" + ctrlId).removeClass('measurement-dropdown');
            //  $('#' + ctrlId).siblings().remove();//.find('span').removeClass('caret');
            $('#' + ctrlId).siblings('.caret').remove();
            //room location field
            if (className.contains('room-location')) {
                $("#" + ctrlId).kendoDropDownList({
                    valuePrimitive: true,
                    filter: "contains",
                    autoBind: true,
                    optionLabel: "Select",
                    dataTextField: "Value",
                    dataValueField: "Key",
                    template: "#=Value #",
                    change: function (e, options) {
                        //var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                        //item.data().uid
                        //var dataItem = item.data().$scope.dataItem;
                        var idStr = e.sender.element[0].id;
                        var updatedRoomvalue = e.sender.element[0].value;
                        var selectedElementId = idStr.split("n")[1];
                        var dataItem;
                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        var dataSource = grid.dataSource._data;
                        var unsavedList = [];
                        for (var i = 0; i < dataSource.length; i++) {
                            if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                                unsavedList.push(dataSource[i]);
                            }
                        }
                        var dataItem = unsavedList[selectedElementId];
                        dataItem.RoomLocation = updatedRoomvalue;
                        $scope.changeFlag = true;
                        SetRoomNameDefault(dataItem, null, selectedElementId);
                    },
                    select: function (e) {
                        if (e.dataItem.Value === "Select") {
                            $scope.RoomPreventClose = true;
                            e.preventDefault();
                        }
                        else {
                            var a = $(e.sender.element[0]).parent();
                            var b = a[0].children[0].children;
                            $(b[0]).removeClass("requireddropfield");
                            $(b[0]).addClass("k-input");
                            $scope.RoomPreventClose = false;
                        }

                    },
                    close: function (e) {
                        if ($scope.RoomPreventClose == true)
                            e.preventDefault();

                        $scope.RoomPreventClose = false;
                    },
                    dataSource: $scope.RoomLocationDrop,
                });
                $("#" + ctrlId).data("kendoDropDownList").text(dataItem.RoomLocation);
            }
            //window location field
            else if (className.contains('window-location')) {
                $('#' + ctrlId).kendoDropDownList({
                    valuePrimitive: true,
                    filter: "contains",
                    optionLabel: "Select",
                    autoBind: true,
                    dataTextField: "Value",
                    dataValueField: "Key",
                    template: "#=Value #",
                    change: function (e, options) {
                        //var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                        //item.data().uid
                        //var dataItem = item.data().$scope.dataItem;
                        var idStr = e.sender.element[0].id;
                        var updatedWindowvalue = e.sender.element[0].value;
                        var selectedElementId = idStr.split("n")[2];
                        var dataItem;
                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        var dataSource = grid.dataSource._data;
                        var unsavedList = [];
                        for (var i = 0; i < dataSource.length; i++) {
                            if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                                unsavedList.push(dataSource[i]);
                            }
                        }
                        var dataItem = unsavedList[selectedElementId];
                        dataItem.WindowLocation = updatedWindowvalue;
                        $scope.changeFlag = true;
                        SetRoomNameDefault(dataItem, null, selectedElementId);
                    },
                    select: function (e) {
                        if (e.dataItem.Value === "Select") {
                            $scope.WindowPreventClose = true;
                            e.preventDefault();
                        }
                        else {
                            var a = $(e.sender.element[0]).parent();
                            var b = a[0].children[0].children;
                            $(b[0]).removeClass("requireddropfield");
                            $(b[0]).addClass("k-input");
                            $scope.WindowPreventClose = false;
                        }

                    },
                    close: function (e) {
                        if ($scope.WindowPreventClose == true)
                            e.preventDefault();

                        $scope.WindowPreventClose = false;
                    },
                    dataSource: $scope.windowDataBackup
                });
                $("#" + ctrlId).data("kendoDropDownList").text(dataItem.WindowLocation);
            }
            // mount field
            else if (className.contains('mount')) {
                $('#' + ctrlId).kendoDropDownList({
                    valuePrimitive: true,
                    autoBind: true,
                    filter: "contains",
                    optionLabel: "Select",
                    dataTextField: "Value",
                    dataValueField: "Key",
                    template: "#=Key #",
                    select: function (e) {
                        if (e.dataItem.Value === "Select") {
                            e.preventDefault();
                            MountPreventClose = true;
                        }
                        else {
                            var idStr = e.sender.element[0].id;
                            var updatedMountvalue = e.dataItem.Value;
                            var selectedElementId = idStr.split("t")[1];
                            var dataItem;
                            var grid = $('#gridEditMeasurements').data("kendoGrid");
                            var dataSource = grid.dataSource._data;
                            var unsavedList = [];
                            for (var i = 0; i < dataSource.length; i++) {
                                if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                                    unsavedList.push(dataSource[i]);
                                }
                            }
                            var dataItem = unsavedList[selectedElementId];
                            dataItem.MountTypeName = updatedMountvalue;

                            var a = $(e.sender.element[0]).parent();
                            var b = a[0].children[0].children;
                            $(b[0]).removeClass("requireddropfield");
                            $(b[0]).addClass("k-input");
                            $scope.changeFlag = true;
                        }

                    },
                    close: function (e) {
                        if (MountPreventClose)
                            e.preventDefault();
                        MountPreventClose = false;
                    },
                    dataSource: $scope.mountDataBackup
                });
                $("#" + ctrlId).data("kendoDropDownList").text(dataItem.MountTypeName);
            }
            // width field  
            else if (className.contains('width-fraction')) {
                $('#' + ctrlId).kendoDropDownList({
                    valuePrimitive: true,
                    optionLabel: "Select",
                    autoBind: true,
                    autoWidth: true,
                    dataTextField: "Value",
                    dataValueField: "Key",
                    template: "#=Value #",
                    dataSource: {
                        data:
                            [{ Key: "1/16", Value: "1/16" },
                            { Key: "1/8", Value: "1/8" },
                            { Key: "3/16", Value: "3/16" },
                            { Key: "1/4", Value: "1/4" },
                            { Key: "5/16", Value: "5/16" },
                            { Key: "3/8", Value: "3/8" },
                            { Key: "7/16", Value: "7/16" },
                            { Key: "1/2", Value: "1/2" },
                            { Key: "9/16", Value: "9/16" },
                            { Key: "5/8", Value: "5/8" },
                            { Key: "11/16", Value: "11/16" },
                            { Key: "3/4", Value: "3/4" },
                            { Key: "13/16", Value: "13/16" },
                            { Key: "7/8", Value: "7/8" },
                            { Key: "15/16", Value: "15/16" }]
                    },
                    select: function (e) {
                        var idStr = e.sender.element[0].id;
                        var selectedElementId = idStr.split("n")[1];
                        var dataItem;
                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        var dataSource = grid.dataSource._data;
                        var unsavedList = [];
                        for (var i = 0; i < dataSource.length; i++) {
                            if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                                unsavedList.push(dataSource[i]);
                            }
                        }
                        var dataItem = unsavedList[selectedElementId];
                        if (e.dataItem !== undefined || e.dataItem !== '') {
                            dataItem['FranctionalValueWidth'] = e.dataItem.Key;
                        }
                        var fractionId = "#widthFraction" + selectedElementId;
                        $(fractionId).val(dataItem['FranctionalValueWidth']);
                        $scope.changeFlag = true;
                    }
                });
                $("#" + ctrlId).data("kendoDropDownList").text(dataItem.FranctionalValueWidth);
            }
            // height field
            else if (className.contains('height-fraction')) {
                $('#' + ctrlId).kendoDropDownList({
                    valuePrimitive: true,
                    autoBind: true,
                    autoWidth: true,
                    optionLabel: "Select",
                    dataTextField: "Value",
                    dataValueField: "Key",
                    template: "#=Value #",
                    dataSource: {
                        data:
                            [{ Key: "1/16", Value: "1/16" },
                            { Key: "1/8", Value: "1/8" },
                            { Key: "3/16", Value: "3/16" },
                            { Key: "1/4", Value: "1/4" },
                            { Key: "5/16", Value: "5/16" },
                            { Key: "3/8", Value: "3/8" },
                            { Key: "7/16", Value: "7/16" },
                            { Key: "1/2", Value: "1/2" },
                            { Key: "9/16", Value: "9/16" },
                            { Key: "5/8", Value: "5/8" },
                            { Key: "11/16", Value: "11/16" },
                            { Key: "3/4", Value: "3/4" },
                            { Key: "13/16", Value: "13/16" },
                            { Key: "7/8", Value: "7/8" },
                            { Key: "15/16", Value: "15/16" }]
                    },
                    select: function (e) {
                        var idStr = e.sender.element[0].id;
                        var selectedElementId = idStr.split("n")[1];
                        var dataItem;
                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        var dataSource = grid.dataSource._data;
                        var unsavedList = [];
                        for (var i = 0; i < dataSource.length; i++) {
                            if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                                unsavedList.push(dataSource[i]);
                            }
                        }
                        var dataItem = unsavedList[selectedElementId];
                        if (e.dataItem !== undefined && e.dataItem !== '') {
                            dataItem['FranctionalValueHeight'] = e.dataItem.Key;
                        }
                        var fractionId = "#heightFraction" + selectedElementId;
                        $(fractionId).val(dataItem['FranctionalValueHeight']);
                        $scope.changeFlag = true;
                    }
                });
                $("#" + ctrlId).data("kendoDropDownList").text(dataItem.FranctionalValueHeight);
            }
            $("#" + ctrlId).data("kendoDropDownList").open();
        }

        $scope.UpdateComments = function (e) {

            var idStr = e.target.id;
            var updatedCommentvalue = e.target.value;
            var selectedElementId = idStr.split("a")[2];
            var dataItem;
            var grid = $('#gridEditMeasurements').data("kendoGrid");
            var dataSource = grid.dataSource._data;
            var unsavedList = [];
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                    unsavedList.push(dataSource[i]);
                }
            }
            var dataItem = unsavedList[selectedElementId];
            dataItem.Comments = updatedCommentvalue;
            $scope.changeFlag = true; //to state user made changes
        }

        $scope.MeasurementGrid = function () {
            //Budget Blind

            if ($scope.brandid === 1) {
                $scope.EditMeasurements = {

                    dataSource: {
                        transport: {

                            read: {
                                //url: '/api/Measurement/' + 1245 + '/Get',
                                //type:"Get"
                            },
                            update: {

                                url: '/api/Measurement/0/AddEditMeasurement',
                                type: "POST",
                                complete: function (e) {

                                    $("#gridMeasurements").data("kendoGrid").dataSource.read();
                                    HFC.DisplaySuccess("Measurement Updated Successfully.");
                                }
                            }
                            ,
                            create: {
                                url: '/api/Measurement/0/AddEditMeasurement',
                                type: "POST",
                                complete: function (e) {

                                    $("#gridMeasurements").data("kendoGrid").dataSource.read();
                                    HFC.DisplaySuccess("New Measurement added sucessfully.");
                                }
                            },

                            destroy: function (e) {
                                e.success();
                            },

                            parameterMap: function (options, operation) {

                                if (operation === "create") {
                                    return {
                                        id: options.id,
                                        RoomLocation: options.RoomLocation.RoomLocation,
                                        WindowLocation: options.WindowLocation,
                                        RoomName: options.RoomName,
                                        Amount: options.Amount,
                                        MountTypeName: options.MountTypeName.MountTypeName,
                                        Width: options.Width,
                                        FranctionalValueWidth: options.FranctionalValueWidth.FranctionalValueWidth,
                                        Height: options.Height,
                                        FranctionalValueHeight: options.FranctionalValueHeight.FranctionalValueHeight,
                                        Comments: options.Comments
                                    };
                                }
                                else if (operation === "update") {
                                    var grid = $('#gridEditMeasurements').data().kendoGrid.dataSource.data();
                                    return {
                                        id: options.id,
                                        RoomLocation: options.RoomLocation,
                                        WindowLocation: options.WindowLocation,
                                        RoomName: options.RoomName,
                                        Amount: options.Amount,
                                        MountTypeName: options.MountTypeName,
                                        Width: options.Width,
                                        FranctionalValueWidth: options.FranctionalValueWidth.Key,
                                        Height: options.Height,
                                        FranctionalValueHeight: options.FranctionalValueHeight.Key,
                                        Comments: options.Comments
                                    };
                                }
                            }
                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },
                        batch: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    id: { editable: false, nullable: true },
                                    RoomLocation: { validation: { required: true }, defaultValue: '' },
                                    WindowLocation: { validation: { required: true }, defaultValue: '' },
                                    RoomName: { validation: { required: true } },
                                    MountTypeName: { validation: { required: true }, defaultValue: 'IB' },
                                    FranctionalValueWidth: {
                                        editable: true, validation: {
                                            required: false
                                        }, defaultValue: "0"
                                    },
                                    Height: {
                                        type: "number", editable: true, validation: {
                                            required: true
                                        }
                                    },
                                    Width: {
                                        type: "number", editable: true, validation: {
                                            required: true
                                        }
                                    },
                                    FranctionalValueHeight: { editable: true, validation: { required: false }, defaultValue: "0" },
                                    Comments: { editable: true }
                                }
                            }
                        }
                    },
                    cache: false,

                    columns: [
                        { title: "#", width: 40, template: renderNumber },
                        {
                            field: "RoomLocation",
                            title: "Room",
                            width: '100px',
                            hidden: false,
                            template: function (dataItem) {
                                // base condition for tracking the copied rows amkes editable
                                if (dataItem['editable']) {
                                    return '<input id="Room" required=true class="room-location measurement-dropdown" name="room" placeholder="Select" value="' + dataItem.RoomLocation + '" ng-click="renderControl($event,dataItem)"/><span class="caret"></span>';
                                } else {
                                    if (dataItem.RoomLocation) {
                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.RoomLocation + "</div><span>" + dataItem.RoomLocation + "</span></span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            editor: function (container, options) {
                                $('<input id="Room" required=true name="' + options.field + '" data-bind="value:RoomLocation"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        valuePrimitive: true,
                                        filter: "contains",
                                        autoBind: true,
                                        optionLabel: "Select",
                                        dataTextField: "Value",
                                        dataValueField: "Key",
                                        template: "#=Value #",
                                        change: function (e, options) {
                                            var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                                            item.data().uid
                                            var dataItem = item.data().$scope.dataItem;
                                            SetRoomNameDefault(dataItem);
                                        },
                                        select: function (e) {
                                            if (e.dataItem.Value === "Select") {
                                                $scope.RoomPreventClose = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#Room").parent();
                                                var b = a[0].children[0].children;
                                                $(b[0]).removeClass("requireddropfield");
                                                $(b[0]).addClass("k-input");
                                                $scope.RoomPreventClose = false;
                                            }

                                        },
                                        close: function (e) {
                                            if ($scope.RoomPreventClose == true)
                                                e.preventDefault();

                                            $scope.RoomPreventClose = false;
                                        },
                                        dataSource: $scope.RoomLocationDrop,
                                    });
                            }
                        },
                        {
                            field: "WindowLocation",
                            title: "Window Location",
                            width: '150px',
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem['editable']) {
                                    return '<input id="Window" name="window-location" required=true class="window-location measurement-dropdown" placeholder="Select" value="' + dataItem.WindowLocation + '" ng-click="renderControl($event,dataItem)"/><span class="caret"></span>';

                                } else {
                                    if (dataItem.WindowLocation) {
                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.WindowLocation + "</div><span>" + dataItem.WindowLocation + "</span></span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            filterable: { search: true },
                            editor: function (container, options) {
                                if ($scope.NewEditRow) {
                                    var a = $("#Room").parent();
                                    var b = a[0].children[0].children;
                                    $(b[0]).removeClass("k-input");
                                    $(b[0]).addClass("requireddropfield");
                                }

                                $('<input id="Window" required=true name="' + options.field + '"data-bind="value:WindowLocation"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        valuePrimitive: true,
                                        filter: "contains",
                                        optionLabel: "Select",
                                        autoBind: true,
                                        dataTextField: "Value",
                                        dataValueField: "Key",
                                        template: "#=Value #",


                                        change: function (e, options) {
                                            var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                                            item.data().uid
                                            var dataItem = item.data().$scope.dataItem;
                                            SetRoomNameDefault(dataItem);
                                        },
                                        select: function (e) {
                                            if (e.dataItem.Value === "Select") {
                                                $scope.WindowPreventClose = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#Window").parent();
                                                var b = a[0].children[0].children;
                                                $(b[0]).removeClass("requireddropfield");
                                                $(b[0]).addClass("k-input");
                                                $scope.WindowPreventClose = false;
                                            }

                                        },
                                        close: function (e) {
                                            if ($scope.WindowPreventClose == true)
                                                e.preventDefault();

                                            $scope.WindowPreventClose = false;
                                        },
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: '/api/Lookup/0/GetWindowLocation',
                                                }
                                            },
                                        }
                                    });
                            }
                        },
                        {
                            field: "RoomName",
                            title: "Window Name",
                            width: '150px',
                            template: function (dataItem) {
                                if (dataItem['editable']) {
                                    return '<input data-type="string" class="roomname"  ng-keyup="RoomNmeKeyDown($event,true)"  required=true  class="k-textbox"  name="RoomName" value="' + dataItem.RoomName + '"/>';
                                } else {
                                    if (dataItem.RoomName) {
                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.RoomName + "</div><span>" + dataItem.RoomName + "</span></span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            editor: '<input data-type="string"  ng-keyup="RoomNmeKeyDown($event)"  required=true  class="k-textbox"  name="RoomName" id="RoomName" value="${RoomName}"/>'
                        },
                        {
                            field: "MountTypeName",
                            title: "Mount Type",
                            width: "150px",
                            hidden: false,
                            filterable: { multi: true, search: true },
                            template: function (dataItem) {
                                if (dataItem['editable']) {
                                    return '<input id="Mount" required class="mount measurement-dropdown" name="mount" placeholder="Select" value="' + dataItem.MountTypeName + '" ng-click="renderControl($event,dataItem)"/><span class="caret"></span>';
                                } else {
                                    if (dataItem.MountTypeName) {
                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.MountTypeName + "</div><span>" + dataItem.MountTypeName + "</span></span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            editor: MountTypeDropDownEditor
                        },
                        {
                            field: "Width",
                            title: "Width",
                            width: "200px",
                            editor: FractionDropDownEditorWidth,
                            template: GetWidthColumn,
                            filterable: false
                        },
                        {
                            field: "Height",
                            title: "Height",
                            width: "200px",
                            editor: FractionDropDownEditorHeight,
                            filterable: false,
                            template: GetHeightColumn
                        },
                        {
                            field: "fileName",
                            title: "fileName",
                            filterable: false,
                            hidden: true,
                            template: fileNameTemplate
                        },
                        {
                            title: "Photo",
                            hidden: false,
                            width: "100px",
                            edit: false,
                            template: photoTemplate
                        },
                        {
                            field: "Comments",
                            title: "Comments",
                            width: "150px",
                            hidden: false,
                            edit: false,
                            editor: AreaCodesEditor,
                            template: function (dataItem) {
                                if (dataItem['editable']) {
                                    if (dataItem.Comments) {
                                        return '<textarea class="comment_area" ng-keyup="UpdateComments($event)" style=" width: 100% !important;">' + dataItem.Comments + '</textarea>';
                                    } else {
                                        return '<textarea class="comment_area" ng-keyup="UpdateComments($event)" style=" width: 100% !important;"></textarea>';
                                    }
                                } else {
                                    if (dataItem.Comments) {
                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Comments + "</div><span>" + dataItem.Comments + "</span></span>";
                                    } else {
                                        return "";
                                    }
                                }
                            }
                        },
                        {
                            field: "",
                            title: "",
                            hidden: false,
                            editable: false,
                            width: "80px",
                            template: '<ul ng-show="EditMode==true" ng-if="Permission.EditMeasurements" ng-disabled="Disableotherfields">' +
                                '<li class="dropdown note1 ad_user"><div class="dropdown">'
                                + '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>'
                                + '</button> <ul class="dropdown-menu pull-right">'
                                + '<li><a href="javascript:void(0)"  ng-click="cloneMeasurementLine(this)">Copy</a></li>'
                                + '&nbsp;'
                                + '<li><a  href="javascript:void(0)" ng-click="deleteRowBB(this)">Delete</a></li>'

                                + '</ul> </li> </ul>'
                        },
                    ],
                    dataBound: resetRowNumber,
                    editable: "inline",
                    resizable: true,
                    noRecords: { template: noRecordFound },
                    edit: function (e) {
                        e.container.find(".k-edit-label:last").hide();
                        e.container.find(".k-edit-field:last").hide();
                    },
                    filterable: true,
                    filterMenuInit: function (e) {
                        var filterButton = $(e.container).find('.k-primary')
                        $(filterButton).click(function (e) {
                            $('#btnReset').prop('disabled', false);
                        })
                    },
                    autoSync: true,
                    scrollable: true,
                    sortable: true,
                    resizable: true,
                    pageable: {
                        numeric: false,
                        refresh: true,
                        previousNext: false,
                        input: false,
                    },
                    toolbar: [
                        {
                            template: kendo.template($("#headToolbarTemplate").html()),
                        }],
                    columnResize: function (e) {
                        //$('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                        getUpdatedColumnList(e);
                    }
                };
                $scope.EditMeasurements = kendotooltipService.setColumnWidth($scope.EditMeasurements);
            }
            /// Tailored Living
            else if ($scope.brandid === 2) {

                $scope.GetStorageTypes = function () {
                    $http.get('/api/Lookup/0/GetStorageType').then(function (response) {
                        $scope.StorageTypesList = response.data;

                    });
                }
                $scope.GetStorageTypes();

                $scope.EditMeasurements = {
                    dataSource: {
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {

                                id: "id",
                                fields: {
                                    id: { editable: false, nullable: true },
                                    System: { editable: true, validation: { required: true } },
                                    SystemDescription: { editable: true, type: "string" },
                                    StorageType: { editable: true, validation: { required: true, min: 1 } },
                                    StorageDescription: { editable: true, type: "string" },
                                    CustomData: { editable: true },
                                    Width: { editable: true, type: "number" },
                                    Height: { editable: true, type: "number" },
                                    Depth: { editable: true, type: "number" },
                                    Comments: { editable: true },
                                    //Area: { editable: true, type: "number" },
                                    //Moisture: { editable: true, type: "number" },
                                    Qty: { editable: true, type: "number" },

                                }
                            }
                        }
                    },
                    columns: [
                        { title: "#", width: 40, template: renderNumber },
                        {
                            field: "System",
                            title: "System",
                            filterable: { multi: true, search: true },
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {

                                    return '<input required=true class="system" name="system" data-bind="value:' + dataItem.System + '"/>';
                                } else {
                                    if (dataItem.System) {
                                        return "<span class='tooltip-wrapper'>" + dataItem.System + "</span>";
                                    } else {
                                        return "";
                                    }
                                }

                            },
                            editor: function (container, options) {
                                $('<input id="System" style=" " required  name="' + options.field + '" data-bind="value:System"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        autoBind: false,
                                        optionLabel: "Select",
                                        filter: "contains",
                                        valuePrimitive: true,
                                        dataTextField: "Value",
                                        dataValueField: "Key",
                                        template: "#=Value #",
                                        change: function (e) {
                                            var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                                            item.data().uid
                                            var dataItem = item.data().$scope.dataItem;
                                            TLDropdownChange(dataItem, 'System');

                                            var a = $("#storageType").parent();
                                            var b = a[0].children[0].children;
                                            $(b[0]).removeClass("k-input");
                                            $(b[0]).addClass("requireddropfield");


                                            var a = $("#CustomData").parent();
                                            var b = a[0].children[0].children;
                                            $(b[0]).removeClass("requireddropfield");
                                            $(b[0]).addClass("k-input");

                                        },
                                        select: function (e) {
                                            if (e.dataItem.Value === "Select") {
                                                $scope.SystemPreventClose = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#System").parent();
                                                var b = a[0].children[0].children;
                                                $(b[0]).removeClass("requireddropfield");
                                                $(b[0]).addClass("k-input");
                                                $scope.SystemPreventClose = false;
                                            }

                                        },
                                        close: function (e) {
                                            if ($scope.SystemPreventClose == true)
                                                e.preventDefault();

                                            $scope.SystemPreventClose = false;
                                        },
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: '/api/Lookup/0/GetSystem',
                                                }
                                            },
                                        }
                                    });
                                if ($scope.NewEditRow) {
                                    var a = $("#System").parent();
                                    var b = a[0].children[0].children;
                                    $(b[0]).removeClass("k-input");
                                    $(b[0]).addClass("requireddropfield");
                                }
                            }
                        },
                        {
                            field: "SystemDescription",
                            title: "System Desc",
                            filterable: { multi: true, search: true },
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    if (dataItem.SystemDescription)
                                        return '<input data-type="text" class="k-textbox "  name="SystemDescription" data-bind="value:' + dataItem.SystemDescription + '"/>';
                                    else
                                        return '<input data-type="text" disabled class="k-textbox "  name="SystemDescription" data-bind="value:' + dataItem.SystemDescription + '"/>';
                                } else {
                                    if (dataItem.SystemDescription) {
                                        return "<span class='tooltip-wrapper'>" + dataItem.SystemDescription + "</span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            editor: '<input data-type="text" disabled class="k-textbox "  name="SystemDescription" data-bind="value:SystemDescription"/>'
                        },
                        {
                            field: "StorageType",
                            title: "Type",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input style="height: 24px !important" required=true class="storagetype" name="storagetype" data-bind="value:' + dataItem.StorageType + '"/>';
                                } else {
                                    if (dataItem.StorageType) {
                                        return "<span class='tooltip-wrapper'>" + dataItem.StorageType + "</span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },

                            editor: function (container, options) {
                                $('<input id = "storageType" disabled required style=" " name="' + options.field + '"data-bind="value:StorageType"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        valuePrimitive: true,
                                        optionLabel: "Select",
                                        filter: "contains",
                                        autoBind: true,
                                        dataTextField: "Name",
                                        dataValueField: "Name",
                                        template: "#=Name #",
                                        //cascadeFrom: "System",
                                        change: function (e, options) {
                                            var a = $("#CustomData").parent();
                                            var b = a[0].children[0].children;
                                            $(b[0]).removeClass("requireddropfield");
                                            $(b[0]).addClass("k-input");

                                            var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                                            item.data().uid
                                            var dataItem = item.data().$scope.dataItem;
                                            TLDropdownChange(dataItem, 'storageType');
                                        },
                                        select: function (e) {
                                            if (e.dataItem.Name === "") {
                                                $scope.storageTypePreventClose = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#storageType").parent();
                                                var b = a[0].children[0].children;
                                                $(b[0]).removeClass("requireddropfield");
                                                $(b[0]).addClass("k-input");
                                                $scope.storageTypePreventClose = false;
                                            }

                                        },
                                        close: function (e) {
                                            if ($scope.storageTypePreventClose == true)
                                                e.preventDefault();

                                            $scope.storageTypePreventClose = false;
                                        },
                                    });

                                var list = $scope.StorageTypesList;
                                var objList = [];
                                for (var i = 0; i < list.length; i++) {
                                    if (list[i].Value === options.model.System) {
                                        var obj = {};
                                        obj.Id = list[i].Id;
                                        obj.Name = list[i].Name;
                                        obj.Value = list[i].Value;

                                        objList.push(obj);
                                    }
                                }

                                var storageTypeddl = $("#storageType").data("kendoDropDownList");
                                storageTypeddl.setDataSource(objList);
                                storageTypeddl.dataSource.read();
                            }
                        },
                        {
                            field: "StorageDescription",
                            title: "Desc",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    if (dataItem.StorageDescription)
                                        return '<input data-type="text" class="k-textbox "  name="StorageDescription" data-bind="value:' + dataItem.StorageDescription + '"/>';
                                    else
                                        return '<input data-type="text" disabled class="k-textbox "  name="StorageDescription" data-bind="value:' + dataItem.StorageDescription + '"/>';
                                } else {
                                    if (dataItem.StorageDescription) {
                                        return "<span class='tooltip-wrapper'>" + dataItem.StorageDescription + "</span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            editor: '<input data-type="text" disabled class="k-textbox "  name="StorageDescription" data-bind="value:StorageDescription"/>'
                        },
                        {
                            field: "Width",
                            title: "Width",
                            editor: FractionDropDownEditorWidth,
                            filterable: false,
                            template: GetWidthColumn,
                            width: "150px"
                        },
                        {
                            field: "Height",
                            title: "Height",
                            editor: FractionDropDownEditorHeight,
                            filterable: false,
                            template: GetHeightColumn,
                            width: "150px"
                        },
                        {
                            field: "Depth",
                            title: "Depth",
                            editor: FractionDropDownEditorDepth,
                            filterable: false,
                            template: GetDepthColumn,
                            width: "150px"
                        },
                        {
                            field: "Qty",
                            title: "Qty",
                            hidden: false,
                            editor: numericEditor,
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input disabled  id="qty" name="qty" class="qty_val" data-bind="value:' + dataItem.Qty + '"/>';
                                } else {

                                    if (dataItem.Qty)
                                        return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Qty, 'number') + "</span>";
                                    else
                                        return "";
                                }
                            }
                        },
                        //{
                        //    field: "Area",
                        //    title: "Area (square feet)",
                        //    hidden: false,
                        //    // format: "{0:n3}",
                        //    editor: numericEditor3,
                        //    template: function (dataItem) {
                        //        if (dataItem.Area)
                        //            return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Area, '3decimals') + "</span>";
                        //        else
                        //            return "";
                        //    }
                        //},
                        //{
                        //    field: "Moisture",
                        //    title: "Moisture %",
                        //    hidden: false,
                        //    format: "{0:n2}",
                        //    editor: numericEditor2,
                        //    template: function (dataItem) {
                        //        if (dataItem.Moisture)
                        //            return "<span class='Grid_Textalign'>" + dataItem.Moisture + "%</span>";
                        //        else
                        //            return "";
                        //    }
                        //},
                        {
                            field: "CustomData",
                            title: "Workstation",
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input class="customdata" disabled required style=" " name="customdata"data-bind="value:' + dataItem.CustomData + '"/>';
                                } else {
                                    if (dataItem.CustomData) {
                                        return "<span class='tooltip-wrapper'>" + dataItem.CustomData + "</span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            editor: function (container, options) {
                                $('<input id="CustomData" class = "customdata" disabled required style=" " name="' + options.field + '"data-bind="value:CustomData"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        valuePrimitive: true,
                                        optionLabel: "Select",
                                        filter: "contains",
                                        autoBind: false,
                                        dataTextField: "Key",
                                        dataValueField: "Value",
                                        template: "#=Value #",
                                        //cascadeFrom: "storageType",

                                        //cascadeFromField: "Name",
                                        select: function (e) {
                                            if (e.dataItem.Value === "") {
                                                $scope.CustomDataPreventClose = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#CustomData").parent();
                                                var b = a[0].children[0].children;
                                                $(b[0]).removeClass("requireddropfield");
                                                $(b[0]).addClass("k-input");
                                                $scope.CustomDataPreventClose = false;
                                            }

                                        },
                                        close: function (e) {
                                            if ($scope.CustomDataPreventClose == true)
                                                e.preventDefault();

                                            $scope.CustomDataPreventClose = false;
                                        },
                                        dataSource: {
                                            data:
                                                [{ Key: "Laptop", Value: "Laptop", Name: "Workstation" }, { Key: "Desktop", Value: "Desktop", Name: "Workstation" }]
                                        }
                                    });
                            },
                            hidden: false,
                        },
                        {
                            field: "fileName",
                            title: "fileName",
                            filterable: false,
                            hidden: true,
                            template: fileNameTemplate
                        },
                        {
                            title: "Photo",
                            hidden: false,
                            width: "100px",
                            edit: false,
                            template: photoTemplate
                        },
                        {
                            field: "Comments",
                            title: "Comments",
                            hidden: false,
                            editor: '<input data-type="text" disabled class="k-textbox "  name="Comments" id="Comments" data-bind="value:Comments"/>',
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input data-type="text" disabled class="k-textbox "  name="Comments" id="Comments" data-bind="value:' + dataItem.Comments + '"/>';
                                } else {
                                    if (dataItem.Comments) {
                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Comments + "</div><span>" + dataItem.Comments + "</span></span>";
                                    } else {
                                        return "";
                                    }
                                }
                            }
                        },
                        {
                            field: "",
                            title: "",
                            width: "60px",
                            template: '<ul ng-if="Permission.EditMeasurements"><li class="dropdown note1 ad_user">'
                                + '<div class="btn-group"><button ng-disabled="Disableotherfields" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'
                                + '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button>'
                                + '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="EditRowDetailsBB(dataItem)">Edit</a></li>'
                                + '<li><a href="javascript:void(0)"  ng-click="AddMesurementRowWithSameRoom(this)">Copy</a></li>'
                                + '&nbsp;'
                                + '<li><a  href="javascript:void(0)" ng-click="DeleteRowDetails(this)">Delete</a></li>'
                                + '</li></ul> </li> </ul>'
                        }
                    ],
                    noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                    filterable: true,
                    filterMenuInit: function (e) {
                        var filterButton = $(e.container).find('.k-primary')
                        $(filterButton).click(function (e) {
                            $('#btnReset').prop('disabled', false);
                        })
                    },
                    resizable: true,
                    sortable: true,
                    scrollable: true,
                    dataBound: resetRowNumber,
                    editable: "inline",
                    pageable: {
                        numeric: false,
                        refresh: true,
                        previousNext: false,
                        input: false,
                    },
                    noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                    toolbar: [
                        {
                            template: kendo.template($("#headToolbarTemplate").html()),
                        }],
                    columnResize: function (e) {
                        //$('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                        getUpdatedColumnList(e);
                    }
                };
                //$scope.EditMeasurements = kendotooltipService.setColumnWidth($scope.EditMeasurements);
            }
            //Concrete Craft
            else if ($scope.brandid === 3) {
                $scope.EditMeasurements = {
                    dataSource: {
                        error: function (e) {
                            HFC.DisplayAlert(e.errorThrown);
                        },
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    id: { editable: false, nullable: true },
                                    System: { editable: true, validation: { required: true } },
                                    SystemDescription: { editable: true, type: "string" },
                                    Area: { editable: true, type: "number" },
                                    Moisture: { editable: true, type: "number" },
                                    Comments: { editable: true },
                                    Qty: { editable: true, type: "number" },
                                }
                            },

                        }
                    },

                    columns: [
                        { title: "#", width: 40, template: renderNumber },
                        {
                            field: "System",
                            title: "System",
                            filterable: { multi: true, search: true },
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input id="System" class="system" required  name="system" data-bind="value:' + dataItem.System + '"/>';
                                }
                                else {
                                    if (dataItem.System) {
                                        return "<span class='tooltip-wrapper'>" + dataItem.System + "</span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            editor: function (container, options) {
                                $('<input id="System" class="system" required  name="' + options.field + '" data-bind="value:System"/>')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        //autoBind: true,
                                        optionLabel: "Select",
                                        valuePrimitive: true,
                                        filter: "contains",
                                        dataTextField: "Value",
                                        dataValueField: "Key",
                                        //template: "#=Value #",
                                        change: function (e) {
                                            var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                                            //item.data().uid
                                            var dataItem = item.data().$scope.dataItem;
                                            CCDropdownChange(dataItem, 'System');
                                        },
                                        select: function (e) {
                                            if (e.dataItem.Value === "Select") {
                                                $scope.SystemPreventClose = true;
                                                e.preventDefault();
                                            }
                                            else {
                                                var a = $("#System").parent();
                                                var b = a[0].children[0].children;
                                                $(b[0]).removeClass("requireddropfield");
                                                $(b[0]).addClass("k-input");
                                                $scope.SystemPreventClose = false;
                                            }
                                        },
                                        close: function (e) {
                                            if ($scope.SystemPreventClose == true)
                                                e.preventDefault();

                                            $scope.SystemPreventClose = false;
                                        },
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: '/api/Lookup/0/GetSystem',
                                                }
                                            },
                                        }
                                    });
                                if ($scope.NewEditRow) {
                                    var a = $("#System").parent();
                                    var b = a[0].children[0].children;
                                    $(b[0]).removeClass("k-input");
                                    $(b[0]).addClass("requireddropfield");
                                }
                            }
                        },
                        {
                            field: "SystemDescription",
                            title: "System Desc",
                            filterable: { multi: true, search: true },
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input data-type="text" disabled class="k-textbox "  name="SystemDescription" data-bind="value:' + dataItem.SystemDescription + '"/>';
                                }
                                else {
                                    if (dataItem.SystemDescription) {
                                        return "<span class='tooltip-wrapper'>" + dataItem.SystemDescription + "</span>";
                                    } else {
                                        return "";
                                    }
                                }
                            },
                            editor: '<input data-type="text" disabled class="k-textbox "  name="SystemDescription" data-bind="value:SystemDescription"/>'
                        },
                        {
                            field: "Qty",
                            title: "Qty",
                            hidden: false,
                            editor: numericEditor,
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input disabled id="qty" class="qty_val"name=" qty" data-bind="value:' + dataItem.Qty + '"/>';
                                } else {
                                    if (dataItem.Qty)
                                        return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Qty, 'number') + "</span>";
                                    else
                                        return "";
                                }
                            }
                        },
                        {
                            field: "Area",
                            title: "Area (square feet)",
                            hidden: false,
                            // format: "{0:n3}",
                            editor: numericEditor3,
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input class="area" disabled id="area" placeholder="0.000" name="area" data-bind="value:' + dataItem.Area + '"/>';
                                } else {
                                    if (dataItem.Area)
                                        return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Area, '3decimals') + "</span>";
                                    else
                                        return "";
                                }
                            }
                        },
                        {
                            field: "Moisture",
                            title: "Moisture %",
                            hidden: false,
                            format: "{0:n2}",
                            editor: numericEditor2,
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input class="moisture_val" disabled id="moisture" placeholder="0.00" name="moisture" data-bind="value:' + dataItem.Moisture + '"/>';
                                } else {
                                    if (dataItem.Moisture)
                                        return "<span class='Grid_Textalign'>" + dataItem.Moisture + "%</span>";
                                    else
                                        return "";
                                }

                            }
                        },
                        {
                            field: "fileName",
                            title: "fileName",
                            filterable: false,
                            hidden: true,
                            template: fileNameTemplate
                        },
                        {
                            title: "Photo",
                            hidden: false,
                            width: "100px",
                            edit: false,
                            template: photoTemplate
                        },
                        {
                            field: "Comments",
                            title: "Comments",
                            hidden: false,
                            editor: '<input data-type="text" class="k-textbox "  name="Comments" id="Comments" data-bind="value:Comments"/>',
                            template: function (dataItem) {
                                if (dataItem['NotSave']) {
                                    return '<input data-type="text" class="k-textbox "  name="Comments" id="Comments" data-bind="value:' + dataItem.Comments + '"/>'
                                } else {
                                    if (dataItem.Comments) {
                                        return "<span class='tooltip-wrapper'><div class='tooltip-content tooltip-extra' >" + dataItem.Comments + "</div><span>" + dataItem.Comments + "</span></span>";
                                    } else {
                                        return "";
                                    }
                                }

                            }
                        },
                        {
                            field: "",
                            title: "",
                            width: "60px",
                            template: '<div class="dropdown" ng-if="Permission.EditMeasurements">'
                                + '<button ng-disabled="Disableotherfields" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                + ' <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button>'
                                + '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="EditRowDetailsBB(dataItem)">Edit</a></li>'
                                + '<li><a href="javascript:void(0)"  ng-click="AddMesurementRowWithSameRoom(this)">Copy</a></li>'
                                + '&nbsp;'
                                + '<li><a  href="javascript:void(0)" ng-click="DeleteRowDetails(this)">Delete</a></li></ul></div>'
                        }
                    ],
                    noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                    filterable: true,
                    filterMenuInit: function (e) {
                        var filterButton = $(e.container).find('.k-primary')
                        $(filterButton).click(function (e) {
                            $('#btnReset').prop('disabled', false);
                        })
                    },
                    resizable: true,
                    sortable: true,
                    scrollable: true,
                    pageable: {
                        numeric: false,
                        refresh: true,
                        previousNext: false,
                        input: false,
                    },
                    dataBound: resetRowNumber,
                    editable: "inline",
                    edit: function (e) {
                        e.container.find(".k-edit-label:last").hide();
                        e.container.find(".k-edit-field:last").hide();
                    },
                    toolbar: [
                        {
                            template: kendo.template($("#headToolbarTemplate").html()),
                        }],
                    columnResize: function (e) {
                        $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                        getUpdatedColumnList(e);
                    }
                };
                $scope.EditMeasurements = kendotooltipService.setColumnWidth($scope.EditMeasurements);
            }
        }

        function noRecordFound() {
            $scope.rowNumberr = 0;
            return '<span style="margin-top:20px">No records found</span>';
        }

        function fileNameTemplate(dataItem) {
            if (dataItem.fileName) {
                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.fileName + "</div><span>" + dataItem.fileName + "</span></span>";
            } else {
                return "";
            }
        }

        function photoTemplate(dataItem) {

            if (dataItem['NotSave'] || dataItem['editable']) {
                //return '<ul ng-if="!PhotoFilename" ><li class="dropdown note1 ad_user"><div class="btn-group"><button class="plus_but dropdown-toggle" style="font-size:19px;" data-toggle="dropdown"> <i class="fa fa-camera" aria-hidden="true"></i> </button> <ul id="uploadmenu" class="dropdown-menu pull-right"><li> <div class="upload-field">Upload Photo<input type="file" id="uploadFileInput_uploadPhoto"  onchange="angular.element(this).scope().fileNameChanged()" accept="image/*" /></div></li></ul></li></ul> <div ng-if="PhotoFilename != null || PhotoFilename != undefined" title="{{PhotoFilename}}"><div class="photo-file-name">{{PhotoFilename}}</div><i class="fas fa-times" ng-click="showCameraIcon()"></i></div>';

                return '<ul class="photo_main"><li class="dropdown note1 ad_user"><div class="btn-group"><button class="plus_but dropdown-toggle" style="font-size:19px;" data-toggle="dropdown"> <i class="fa fa-camera" aria-hidden="true"></i> </button> <ul id="uploadmenu" class="dropdown-menu pull-right"><li> <div class="upload-field">Upload Photo<input type="file" class="photoupload" onchange="angular.element(this).scope().fileNameChanged(true)" accept="image/*" /></div></li></ul></li></ul> <div class="photo-title" title="{{PhotoFilename}}"><div class="photo-file-name"></div><i class="fas fa-times show_cameraicon" ng-click="showCameraIcon($event)"></i></div>';


            } else {
                if ($scope.currentEditId == dataItem.id)
                    return '<ul ng-if="!PhotoFilename" ><li class="dropdown note1 ad_user"><div class="btn-group"><button class="plus_but dropdown-toggle" style="font-size:19px;" data-toggle="dropdown"> <i class="fa fa-camera" aria-hidden="true"></i> </button> <ul id="uploadmenu" class="dropdown-menu pull-right"><li> <div class="upload-field">Upload Photo<input type="file" id="uploadFileInput_uploadPhoto"  onchange="angular.element(this).scope().fileNameChanged()" accept="image/*" /></div></li></ul></li></ul> <div ng-if="PhotoFilename != null || PhotoFilename != undefined" title="{{PhotoFilename}}"><div class="photo-file-name">{{PhotoFilename}}</div><i class="fas fa-times" ng-click="showCameraIcon()"></i></div>';
                else {
                    var Div = '';
                    if (dataItem.Stream_id) {
                        if (dataItem.thumb_Stream_id)
                            Div = '<a visible="{{dataItem.thumb_Stream_id}}" ng-if="dataItem.Stream_id" target="_blank" href="/api/Download?streamid={{dataItem.Stream_id}}"> <img title="{{dataItem.fileName}}" style="width: 37px;height: 24px;" ng-src="/api/Download?streamid={{dataItem.thumb_Stream_id}}"> </a>'
                        else
                            Div = "<a href='/api/Download?streamid=" + dataItem.Stream_id + "' target=\"_blank\" style='color: rgb(61,125,139);'><span style='padding: 10px;'>" + dataItem.fileName + "</span></a> ";
                    }
                    else {
                        if (dataItem.fileName != null) {
                            Div = "<label>" + dataItem.fileName + "</label>";
                        }
                        else
                            Div = "<span style='padding: 10px;'> </span> ";
                    }
                    return Div;
                }
            }
        }

        function numericEditor(container, options) {
            var disabledtxt = '';
            if ($scope.brandid != 3)
                disabledtxt = 'disabled';
            $('<input  class="qty_val" ' + disabledtxt + '  id="' + options.field + '" name="' + options.field + '" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoNumericTextBox({
                    format: "{0:n0}",
                    decimals: 0,
                    min: 0,
                    spinners: false,
                    //change: function () {
                    //    var value = this.value();
                    //    var areaValue = $("#Area").data("kendoNumericTextBox").value();
                    //    if (value || areaValue) {
                    //        var parent1 = $("#Qty").parent()[0];
                    //        var parentElement = $(parent1).parent()[0];
                    //        $(parentElement).removeClass("required-error");

                    //        parent1 = $("#Area").parent()[0];
                    //        parentElement = $(parent1).parent()[0];
                    //        $(parentElement).removeClass("required-error");
                    //    } else {
                    //        var parent1 = $("#Qty").parent()[0];
                    //        var parentElement = $(parent1).parent()[0];
                    //        $(parentElement).addClass("required-error");

                    //        parent1 = $("#Area").parent()[0];
                    //        parentElement = $(parent1).parent()[0];
                    //        $(parentElement).addClass("required-error");
                    //    }
                    //}
                });

            if (options.field && $scope.brandid == 3) {
                var parent1 = $("#Qty").parent()[0];
                var parentElement = $(parent1).parent()[0];
                $(parentElement).addClass("required-error");
            }
        }

        function numericEditor2(container, options) {
            var disabledtxt = '';
            if ($scope.brandid != 3)
                disabledtxt = 'disabled';
            $('<input class="moisture_val" ' + disabledtxt + ' id="' + options.field + '" placeholder="0.00" name="' + options.field + '" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoNumericTextBox({
                    format: "{0:n2}",
                    decimals: 2,
                    min: 0,
                    max: 99,
                    spinners: false
                });
        }

        function numericEditor3(container, options) {
            var disabledtxt = '';
            if ($scope.brandid != 3)
                disabledtxt = 'disabled';
            $('<input  ' + disabledtxt + ' id="' + options.field + '" placeholder="0.000" class="area" name="' + options.field + '" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoNumericTextBox({
                    format: "{0:n3}",
                    decimals: 3,
                    min: 0,
                    spinners: false,
                    change: function () {
                        var value = this.value();
                        var quantityValue = $("#Qty").data("kendoNumericTextBox").value();
                        if (value || quantityValue) {
                            var parent1 = $("#Area").parent()[0];
                            var parentElement = $(parent1).parent()[0];
                            $(parentElement).removeClass("required-error");

                            parent1 = $("#Qty").parent()[0];
                            parentElement = $(parent1).parent()[0];
                            $(parentElement).removeClass("required-error");

                        } else {
                            var parent1 = $("#Area").parent()[0];
                            var parentElement = $(parent1).parent()[0];
                            $(parentElement).addClass("required-error");

                            parent1 = $("#Qty").parent()[0];
                            parentElement = $(parent1).parent()[0];
                            $(parentElement).addClass("required-error");
                        }
                    }
                });
            if (options.field && $scope.brandid == 3) {
                var parent1 = $("#Area").parent()[0];
                var parentElement = $(parent1).parent()[0];
                $(parentElement).addClass("required-error");
            }
        }

        // for grid rows manipulation
        var getUpdatedColumnList = function (e) {
            kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
        }

        //Intial load of Kendo Grid
        $scope.MeasurementGrid();

        // On address Change.
        $scope.$watch('InstallationAddressId', function () {
            if ($scope.InstallationAddressId !== 0) {
                $scope.GetMeasurmentData();
            }
        });

        function TLDropdownChange(obj, clicked_id) {
            if (clicked_id == 'System') $('input[name="SystemDescription"]').val('');

            var list = $scope.StorageTypesList;
            var objList = [];
            for (var i = 0; i < list.length; i++) {
                if (list[i].Value === obj.System) {
                    var item = {};
                    item.Id = list[i].Id;
                    item.Name = list[i].Name;
                    item.Value = list[i].Value;

                    objList.push(item);
                }
            }

            var storageTypeddl = $("#storageType").data("kendoDropDownList");
            storageTypeddl.setDataSource(objList);
            storageTypeddl.dataSource.read();

            var FranctionalValueWidth = $("#FranctionalValueWidth").data("kendoDropDownList");
            var FranctionalValueHeight = $("#FranctionalValueHeight").data("kendoDropDownList");
            var FranctionalValueDepth = $("#FranctionalValueDepth").data("kendoDropDownList");
            var CustomData = $("#CustomData").data("kendoDropDownList");
            //var Area = $("#Area").data("kendoNumericTextBox");
            //var Moisture = $("#Moisture").data("kendoNumericTextBox");
            var Quantity = $("#Qty").data("kendoNumericTextBox");

            CustomData.enable(false);
            FranctionalValueWidth.enable(false);
            FranctionalValueHeight.enable(false);
            FranctionalValueDepth.enable(false);
            //Area.enable(false);
            //Moisture.enable(false);
            Quantity.enable(false);

            var grid = $("#gridEditMeasurements").data("kendoGrid");

            var row = grid.dataSource.get(obj.id);

            $('input[name="SystemDescription"]').prop('disabled', true);
            $('input[name="StorageDescription"]').prop('disabled', true);
            $('input[name="Comments"]').prop('disabled', true);
            $('input[name="Width"]').prop('disabled', true);
            $('input[name="Height"]').prop('disabled', true);
            $('input[name="Depth"]').prop('disabled', true);
            //$('input[name="Qty"]').prop('disabled', true);
            //$('input[name="Area"]').prop('disabled', true).prev().prop('disabled', true);
            //$('input[name="Moisture"]').prop('disabled', true);

            $('input[name="Height"]').removeClass("k-invalid");
            $('input[name="Width"]').removeClass("k-invalid");
            $('input[name="Depth"]').removeClass("k-invalid");
            $('input[name="Qty"]').removeClass("k-invalid");

            if (!$scope.clone) {
                row.set("Comments", null);
                row.set("Depth", null);
                row.set("Qty", null);
                //row.set("Area", null);
                //row.set("Moisture", null);
                row.set("Width", null);
                row.set("Height", null);
                row.set("FranctionalValueDepth", null);
                row.set("FranctionalValueWidth", null);
                row.set("FranctionalValueHeight", null);
                row.set("StorageDescription", null);
            }

            if (obj.System == "Closet") {
                storageTypeddl.enable(true);
                $('input[name="SystemDescription"]').prop('disabled', true);
                if (obj.StorageType == "Slacks" ||
                    obj.StorageType == "Short Hang" ||
                    obj.StorageType == "Medium Hang" ||
                    obj.StorageType == "Long Hang" ||
                    obj.StorageType == "Short Hang" ||
                    obj.StorageType == "Folded") {
                    //Enable the Text Box

                    $('input[name="SystemDescription"]').prop('disabled', false);
                    $('input[name="StorageDescription"]').prop('disabled', false);
                    $('input[name="Comments"]').prop('disabled', false);
                    $('input[name="Width"]').prop('disabled', false);
                    $('input[name="Height"]').prop('disabled', false);
                    // Enable the dropdown
                    FranctionalValueWidth.enable(true);
                    FranctionalValueHeight.enable(true);

                    //Set The required field true
                    $('input[name="Height"]').prop('required', true);
                    $('input[name="Width"]').prop('required', true);

                    row.set("Depth", null);

                    row.set("Qty", null);
                    row.set("FranctionalValueDepth", null);

                    row.Depth = null;

                    row.Quantity = null;
                    row.FranctionalValueDepth = null;

                }
                else if (obj.StorageType == "Accessories") {
                    //Enable the Text Box
                    $('input[name="Width"]').prop('disabled', false);
                    $('input[name="Height"]').prop('disabled', false);
                    $('input[name="Depth"]').prop('disabled', false);
                    //$('input[name="Qty"]').prop('disabled', false);
                    Quantity.enable(true);
                    $('input[name="SystemDescription"]').prop('disabled', false);
                    $('input[name="StorageDescription"]').prop('disabled', false);
                    $('input[name="Comments"]').prop('disabled', false);

                    //set the required field true
                    $('input[name="Height"]').prop('required', true);
                    $('input[name="Width"]').prop('required', true);
                    $('input[name="Depth"]').prop('required', true);
                    $('input[name="Qty"]').prop('required', true);
                    row.CustomData = null;
                    row.Quantity = null;

                    row.set("CustomData", null);
                    //row.set("Qty", null);

                    FranctionalValueWidth.enable(true);
                    FranctionalValueHeight.enable(true);
                    FranctionalValueDepth.enable(true);

                }
            }
            else if (obj.System == "Office") {
                storageTypeddl.enable(true);
                $('input[name="SystemDescription"]').prop('disabled', true);
                if (obj.StorageType == "Printer" ||
                    obj.StorageType == "Fax" ||
                    obj.StorageType == "Copier" ||
                    obj.StorageType == "Television" ||
                    obj.StorageType == "Couch/Chair" ||
                    obj.StorageType == "Stereo" ||
                    obj.StorageType == "File Cabinet") {

                    $('input[name="Width"]').prop('disabled', false);
                    $('input[name="Height"]').prop('disabled', false);
                    $('input[name="Depth"]').prop('disabled', false);
                    $('input[name="SystemDescription"]').prop('disabled', false);
                    $('input[name="StorageDescription"]').prop('disabled', false);
                    $('input[name="Comments"]').prop('disabled', false);

                    $('input[name="Height"]').prop('required', true);
                    $('input[name="Width"]').prop('required', true);
                    $('input[name="Depth"]').prop('required', true);


                    row.set("CustomData", null);
                    row.set("Qty", null);

                    FranctionalValueWidth.enable(true);
                    FranctionalValueHeight.enable(true);
                    FranctionalValueDepth.enable(true);
                }
                else if (obj.StorageType == "Workstation") {
                    // $('input[name="CustomData"]').prop('disabled', false);
                    $("#CustomData").data("kendoDropDownList").enable(true);

                    var a = $("#CustomData").parent();
                    if (a[0])
                        if (a[0].children[0]) {
                            var b = a[0].children[0].children;
                            $(b[0]).removeClass("k-input");
                            $(b[0]).addClass("requireddropfield");
                        }

                    if (!$scope.clone) {
                        row.set("CustomData", null);
                    }

                    $('input[name="Height"]').prop('required', false);
                    $('input[name="Width"]').prop('required', false);
                    $('input[name="Depth"]').prop('required', false);

                    $('input[name="Height"]').prop('disabled', true);
                    $('input[name="Width"]').prop('disabled', true);
                    $('input[name="Depth"]').prop('disabled', true);
                    $('input[name="SystemDescription"]').prop('disabled', false);
                    $('input[name="StorageDescription"]').prop('disabled', false);
                    $('input[name="Comments"]').prop('disabled', false);

                    $('input[name="Height"]').removeClass("k-invalid");
                    $('input[name="Width"]').removeClass("k-invalid");
                    $('input[name="Depth"]').removeClass("k-invalid");

                }

                //row.set("Area", null);
                //row.set("Moisture", null);

            }
            else if (obj.System == "Garage") {
                storageTypeddl.enable(true);
                $('input[name="SystemDescription"]').prop('disabled', true);
                if (obj.StorageType == "Vehicles" ||
                    obj.StorageType == "Tools" ||
                    obj.StorageType == "Appliances" || obj.StorageType == "Bikes") {

                    $('input[name="Width"]').prop('disabled', false);
                    $('input[name="Height"]').prop('disabled', false);
                    $('input[name="Depth"]').prop('disabled', false);
                    $('input[name="SystemDescription"]').prop('disabled', false);
                    $('input[name="StorageDescription"]').prop('disabled', false);
                    $('input[name="Comments"]').prop('disabled', false);

                    $('input[name="Height"]').prop('required', true);
                    $('input[name="Width"]').prop('required', true);
                    $('input[name="Depth"]').prop('required', true);

                    row.set("Qty", null);

                    FranctionalValueWidth.enable(true);
                    FranctionalValueHeight.enable(true);
                    FranctionalValueDepth.enable(true);
                }
                //row.set("Area", null);
                //row.set("Moisture", null);
            }
            else if (obj.System == "Floor") {
                storageTypeddl.enable(true);
                if (obj.StorageType == "Size" ||
                    obj.StorageType == "Tramex" ||
                    obj.StorageType == "Stem Wall" || obj.StorageType == "Bikes") {
                    $('input[name="SystemDescription"]').prop('disabled', false);
                    $('input[name="StorageDescription"]').prop('disabled', false);
                    $('input[name="Comments"]').prop('disabled', false);

                }
                //if (obj.StorageType == "Tramex")
                //    Moisture.enable(true);
                //else
                //    row.set("Moisture", null);

                //Area.enable(true);
            }

            if ($scope.clone) {
                $('input[name="System"]').removeClass("requireddropfield");

                row.set("Comments", obj.Comments);
                row.set("Depth", obj.Depth);
                row.set("Qty", obj.Qty);
                //row.set("Area", obj.Area);
                //row.set("Moisture", obj.Moisture);

                row.set("Width", obj.Width);
                row.set("Height", obj.Height);
                row.set("StorageDescription", obj.StorageDescription);
                row.set("SystemDescription", obj.SystemDescription);

                storageTypeddl.value(obj.StorageType);
                FranctionalValueWidth.value(obj.FranctionalValFranctionalValueWidthueDepth);
                FranctionalValueHeight.value(obj.FranctionalValueHeight);
                FranctionalValueDepth.value(obj.FranctionalValueDepth);
                CustomData.value(obj.CustomData);
            }

            $('input[name="Height"]').removeClass("k-invalid");
            $('input[name="Width"]').removeClass("k-invalid");
            $('input[name="Depth"]').removeClass("k-invalid");
            $('input[name="Qty"]').removeClass("k-invalid");
            //$('input[name="Area"]').removeClass("k-invalid");
            //$('input[name="Moisture"]').removeClass("k-invalid");
            $('input[name="CustomData"]').removeClass("k-invalid");

            $('input[name="SystemDescription"]').prop('disabled', false);

            $("span.k-invalid-msg").hide();
            var dropDowns = $(".k-dropdown");
            $.each(dropDowns, function (key, value) {
                var input = $(value).find("input.k-invalid");
                var span = $(this).find(".k-widget.k-dropdown.k-header");
                //
                if (input.size() > 0) {
                    $(this).addClass("dropdown-validation-error");
                } else {
                    $(this).removeClass("dropdown-validation-error");
                }
            });

            if ($("#System").data("kendoDropDownList").selectedIndex === 0) {

                $("#storageType").data("kendoDropDownList").select(0);
                $("#storageType").data("kendoDropDownList").enable(false);

                $('input[name="storageType"]').removeClass("k-invalid");
                $('input[name="System"]').removeClass("k-invalid");

                $.each(dropDowns, function (key, value) {
                    var input = $(value).find("input.k-invalid");
                    var span = $(this).find(".k-widget.k-dropdown.k-header");

                    $(this).removeClass("dropdown-validation-error");

                });
            }

            if ($("#storageType").data("kendoDropDownList").selectedIndex > 0) {
                $.each(dropDowns, function (key, value) {
                    var input = $(value).find("input.k-invalid");
                    var span = $(this).find(".k-widget.k-dropdown.k-header");

                    $(this).removeClass("dropdown-validation-error");

                });
            }

            //if (obj.SquareFeetAdd || obj.SquareFeetSub) {
            //    if (obj.SquareFeetAdd.length > 0 || obj.SquareFeetSub.length > 0)
            //        Area.enable(false);
            //    else
            //        Area.enable(true);
            //}
            //else
            //    Area.enable(true);

            //$scope.validateAreaMoisture();
        }

        function CCDropdownChange(obj, clicked_id) {

            if (clicked_id == 'System') $('input[name="SystemDescription"]').val('');

            var grid = $("#gridEditMeasurements").data("kendoGrid");

            var row = grid.dataSource.get(obj.id);
            var Area = $("#Area").data("kendoNumericTextBox");

            $('input[name="SystemDescription"]').prop('disabled', true);

            $('input[name="Qty"]').removeClass("k-invalid");

            if (obj.System == "Closet") {
                $('input[name="SystemDescription"]').prop('disabled', true);

            }
            else if (obj.System == "Office") {
                $('input[name="SystemDescription"]').prop('disabled', true);
            }
            else if (obj.System == "Garage") {

                $('input[name="SystemDescription"]').prop('disabled', true);
            }

            if ($scope.clone) {
                row.set("SystemDescription", obj.SystemDescription);
                row.set("Area", obj.Area);
                row.set("Moisture", obj.Moisture);
                row.set("Comments", obj.Comments);
                row.set("Qty", obj.Qty);
                $('input[name="System"]').removeClass("requireddropfield");
            }
            $('input[name="Qty"]').removeClass("k-invalid");

            $('input[name="SystemDescription"]').prop('disabled', false);

            $("span.k-invalid-msg").hide();
            var dropDowns = $(".k-dropdown");
            $.each(dropDowns, function (key, value) {
                var input = $(value).find("input.k-invalid");
                var span = $(this).find(".k-widget.k-dropdown.k-header");
                //
                if (input.size() > 0) {
                    $(this).addClass("dropdown-validation-error");
                } else {
                    $(this).removeClass("dropdown-validation-error");
                }
            });

            if ($("#System").data("kendoDropDownList").selectedIndex === 0) {
                $('input[name="System"]').removeClass("k-invalid");

                $.each(dropDowns, function (key, value) {
                    var input = $(value).find("input.k-invalid");
                    var span = $(this).find(".k-widget.k-dropdown.k-header");

                    $(this).removeClass("dropdown-validation-error");

                });
            }

            if (obj.SquareFeetAdd || obj.SquareFeetSub) {
                if (obj.SquareFeetAdd.length > 0 || obj.SquareFeetSub.length > 0)
                    Area.enable(false);
                else
                    Area.enable(true);
            }
            else
                Area.enable(true);

            $scope.validateAreaMoisture();
        }

        function GetWidthColumn(dataItem) {


            if (dataItem['editable']) {
                if (dataItem.FranctionalValueWidth)
                    return '<div class="width-wrapper"><input  value="' + dataItem.Width + '" tabindex="1" type="number" data-type="number" required style="width: 42.2%; margin-right:1px;" ng-keypress="change_width($event, true)" ng-blur="Update_width_fraction($event,true)" class="k-textbox width-field"  name="Width" /><input tabindex="1" style="width: 56.2%;" class="measurement_wfield width-fraction measurement-dropdown" ng-click="renderControl($event,dataItem)" name="FranctionalValueWidth" placeholder="Select" value="' + dataItem.FranctionalValueWidth + '"/><span class="caret"></span></div>';
                else
                    return '<div class="width-wrapper"><input  value="' + dataItem.Width + '" tabindex="1" type="number" data-type="number" required style="width: 42.2%; margin-right:1px;" ng-keypress="change_width($event, true)" ng-blur="Update_width_fraction($event,true)" class="k-textbox width-field"  name="Width" /><input tabindex="1" style="width: 56.2%;" class="measurement_wfield width-fraction measurement-dropdown" ng-click="renderControl($event,dataItem)" name="FranctionalValueWidth" placeholder="Select"/><span class="caret"></span></div>';
            } else {
                var val = '';
                if (dataItem.Width !== null && dataItem.Width !== undefined) {
                    val = dataItem.Width;
                }
                if (dataItem.FranctionalValueWidth !== null && dataItem.FranctionalValueWidth !== undefined) {
                    val = val + ' ' + dataItem.FranctionalValueWidth;
                }
                return val;
            }
        }

        function GetHeightColumn(dataItem) {
            if (dataItem['editable']) {
                if (dataItem.FranctionalValueHeight)
                    return '<div class="height-wrapper"><input tabindex="1" value="' + dataItem.Height + '"  type="number" data-type="number" required  style="width:42.2%; margin-right:1px;" ng-blur="Update_height_fraction($event,true)" ng-keypress="change_height($event, true)"  class="k-textbox height-field"  name="Height" /><input style="width:56.2%;" tabindex="1" class="measurement_wfield height-fraction measurement-dropdown" ng-click="renderControl($event,dataItem)" name="FranctionalValueHeight" value="' + dataItem.FranctionalValueHeight + '"/><span class="caret"></span></div>';
                else
                    return '<div class="height-wrapper"><input tabindex="1" value="' + dataItem.Height + '"  type="number" data-type="number" required  style="width:42.2%; margin-right:1px;" ng-blur="Update_height_fraction($event,true)" ng-keypress="change_height($event, true)"  class="k-textbox height-field"  name="Height" /><input style="width:56.2%;" tabindex="1" class="measurement_wfield height-fraction measurement-dropdown" ng-click="renderControl($event,dataItem)" name="FranctionalValueHeight" placeholder="Select"/><span class="caret"></span></div>';
            } else {
                var val = '';
                if (dataItem.Height !== null && dataItem.Height !== undefined) {
                    val = dataItem.Height;
                }
                if (dataItem.FranctionalValueHeight !== null && dataItem.FranctionalValueHeight !== undefined) {
                    val = val + ' ' + dataItem.FranctionalValueHeight;
                }
                return val;
                return dataItem.Height + " " + dataItem.FranctionalValueHeight;
            }
        }

        function GetDepthColumn(dataItem) {
            if (dataItem['NotSave']) {
                return '<input data-type="number" disabled style="width:49.2%; margin-right:1px" class="k-textbox" ng-keypress="change_depth($event)" ng-blur="Update_depth_fraction()" name="Depth"  id="Depth"/><input id="FranctionalValueDepth" disabled style="width:49.2%; " name="FranctionalValueDepth" class="measurement_dfield depth-fraction" data-bind="value:' + dataItem.FranctionalValueDepth + '"/>'
            }
            else {
                var val = '';
                if (dataItem.Depth !== null && dataItem.Depth !== undefined) {
                    val = dataItem.Depth;
                }
                if (dataItem.FranctionalValueDepth !== null && dataItem.FranctionalValueDepth !== undefined) {
                    val = val + ' ' + dataItem.FranctionalValueDepth;
                }
                return val;
            }

        }

        // adding new measurement row for bb work around
        $scope.AddMeasurementLineBB = function () {
            // remove the filter & search
            var dataItem = {};
            $scope.EditMode = true;
            $('#searchBox').val('');
            setBackToOrginalState(); // added so that multi copy text box are reset to orginal state.
            $scope.MultipleEditMode = false; //--> this make multiple copy txt to disable mode.
            //$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
            //});
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            if (grid) {
                var loading = $("#loading");
                $(loading).css("display", "block");
                $scope.actionFlag = true;
                setTimeout(function () {
                    //added the value to set value for the grid.
                    $scope.newLineDataSource = grid.dataSource._data;
                    dataItem['id'] = $scope.newLineDataSource.length + 1;
                    dataItem['editable'] = true;
                    dataItem['WindowLocation'] = "";
                    dataItem['RoomLocation'] = "";
                    dataItem['RoomName'] = "";
                    dataItem['WindowLocation'] = "";
                    dataItem['MountTypeName'] = "";
                    dataItem['fileName'] = "";
                    dataItem['Stream_id'] = "";
                    dataItem['thumb_Stream_id'] = "";
                    dataItem['QuoteLineId'] = 0;
                    dataItem['QuoteLineNumber'] = 0;
                    dataItem['isAdded'] = false;
                    dataItem['NotSave'] = false;
                    dataItem['Width'] = "";
                    dataItem['FranctionalValueWidth'] = null;
                    dataItem['Height'] = "";
                    dataItem['FranctionalValueHeight'] = null;
                    dataItem['Comments'] = null;
                    $scope.newLineDataSource.push(dataItem);
                    grid.setDataSource($scope.newLineDataSource);
                }, 10);
            }
            //added to avoid negative sign in width & height.
            setTimeout(function () {
                var grid = $("#gridEditMeasurements").data("kendoGrid");
                grid.dataSource.page(1);

                $scope.actionFlag = false;
                // scrolling to copied row
                var dataSourceLength = grid.dataSource._data.length - 1;
                var widthId = '#width' + dataSourceLength;
                $(widthId).focus();
                $scope.changeFlag = true;
                setTimeout(function () { $scope.Changenegativenumber(); }, 100)
            }, 500);
            //$scope.AddMeasurementsData();

        }

        /// Adding new row
        $scope.AddRow = function () {

            $scope.NewEditRow = true;

            var rowEdit = $('#gridEditMeasurements').find('.k-grid-edit-row');

            if (rowEdit.length) {
                //grid is not in edit mode
                var validator = $scope.kendoValidator("gridEditMeasurements"); //$(rowEdit).kendoValidator().data("kendoValidator");
                var validAreaMoisture = $scope.validateAreaMoisture();
                if (!validAreaMoisture)
                    return false;

                if (!validator.validate()) {
                    return false;
                }
            }
            $scope.EditMode = true;
            $('#searchBox').val('');
            //$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
            //});
            $scope.AddNewRow = true;
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            var dataSource = grid.dataSource;
            dataSource._data = updateGridValue(dataSource._data);
            $scope.currentEditId = dataSource._data.length + 1;

            var savedData = null;
            if (dataSource.options.data != null)
                savedData = dataSource.options.data.length;
            var index = dataSource._data.length;
            if (index >= 1) {
                $scope.saveRedirectCancel = true;
                $scope.saveMeasurements_addrow();
                $scope.PhotoFilename = null;
            }
            else {
                $scope.AddNewRow = false;
                $scope.AddMeasurementsData();
            }
            $scope.PhotoFilename = null;
            //added newly now
            //$scope.Disableotherfields = true;
            $scope.MultipleEditMode = false;
            setBackToOrginalState();
            //
        };

        $scope.AddMeasurementsData = function () {
            var grid = $("#gridEditMeasurements").data("kendoGrid");

            var dataSource = grid.dataSource;

            var index = dataSource._data.length;

            if (index != 0) {
                $scope.id = index + 1;
            }
            else
                $scope.id = 1;

            var validator = $scope.kendoValidator("gridEditMeasurements");
            var validAreaMoisture = $scope.validateAreaMoisture();
            if (!validAreaMoisture)
                return false;
            if (validator.validate()) {
                var newItem = {
                    id: $scope.id
                }

                var newItem = dataSource.insert(index, newItem); //, {});
                newItem.WindowLocation = "";


                //grid.dataSource.page(grid.dataSource.pageSize());
                var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                grid.editRow(newRow);

                setTimeout(function () {
                    var scrollHeight = $("#gridEditMeasurements .k-grid-content >table").height() + 100;
                    $("#gridEditMeasurements .k-grid-content").scrollTop(scrollHeight);
                    $scope.Changenegativenumber();
                }, 300);

            } else {
                return false;
            }

        };

        $scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                validate: function (e) {
                    $("span.k-invalid-msg").hide();
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find(".k-widget.k-dropdown.k-header");
                        //
                        if (input.size() > 0) {
                            $(this).addClass("dropdown-validation-error");
                        } else {
                            $(this).removeClass("dropdown-validation-error");
                        }
                    });
                }
            }).getKendoValidator();
        }

        $scope.cancelMeasurementEdit = function (event, redirectFlag) {
            if ($scope.changeFlag && $scope.brandid == 1) {
                if (confirm('You will lose unsaved changes if you reload this page')) {
                    $scope.currentEditId = null;
                    $scope.PhotoFilename = null; $scope.EnablCopyFlag = false;
                    $('#gridEditMeasurements').data("kendoGrid").cancelChanges();
                    //var dataSource = $('#gridEditMeasurements').data("kendoGrid");
                    //dataSource = dataSource._data;
                    //var flag = checkFlagValue(dataSource);

                    //    if (flag && !redirectFlag) {
                    //        // dataSource = updateGridValue(dataSource);
                    //        dataSource = removeUnsavedLines(dataSource);
                    //        var loading = $("#loading");
                    //        $(loading).css("display", "block");
                    //        setTimeout(function () {
                    //            $('#gridEditMeasurements').data("kendoGrid").setDataSource(dataSource);
                    //            setTimeout(function () {
                    //                var loading = $("#loading");
                    //                $(loading).css("display", "none");
                    //            }, 100);
                    //            var grid = $('#gridEditMeasurements').data("kendoGrid");
                    //            grid.dataSource.page(1);
                    //        }, 50);
                    //        // $scope.saveMeasurements(true,dataSource,false,false);

                    //    }
                    if ($scope.brandid == 1 && !redirectFlag) {
                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        if ($scope.OldMeasurementValues && $scope.OldMeasurementValues.options) {
                            grid.setDataSource($scope.OldMeasurementValues.options.data);
                            grid.dataSource.page(1);
                        } else { grid.setDataSource(null); grid.dataSource.page(1); }
                    }
                    $scope.EditMode = false;
                    $scope.TextboxDisabled = false;

                    //added newly now
                    //$scope.Disableotherfields = false;
                    if ($scope.Disableotherfields) {
                        $scope.Disableotherfields = false;
                    }
                    $scope.MultipleEditMode = false;
                    setBackToOrginalState();
                    //
                    $scope.Measurement_detail_form.$setPristine();
                    $scope.enableAddressvalidationFlag = true;
                    if ($scope.routeQuoteFlag) {
                        $scope.Newquotepage($scope.preserveQuoteId);
                    }
                    $scope.preventChange = true;
                } else {
                    if (event) {
                        event.preventDefault();
                        $scope.preventChange = true;
                    }
                }
            } else if ($scope.brandid != 1) {
                if (confirm('You will lose unsaved changes if you reload this page')) {
                    $scope.currentEditId = null;
                    $scope.PhotoFilename = null; $scope.EnablCopyFlag = false;
                    $('#gridEditMeasurements').data("kendoGrid").cancelChanges();
                    //var dataSource = $('#gridEditMeasurements').data("kendoGrid");
                    //dataSource = dataSource._data;
                    //var flag = checkFlagValue(dataSource);

                    //    if (flag && !redirectFlag) {
                    //        // dataSource = updateGridValue(dataSource);
                    //        dataSource = removeUnsavedLines(dataSource);
                    //        var loading = $("#loading");
                    //        $(loading).css("display", "block");
                    //        setTimeout(function () {
                    //            $('#gridEditMeasurements').data("kendoGrid").setDataSource(dataSource);
                    //            setTimeout(function () {
                    //                var loading = $("#loading");
                    //                $(loading).css("display", "none");
                    //            }, 100);
                    //            var grid = $('#gridEditMeasurements').data("kendoGrid");
                    //            grid.dataSource.page(1);
                    //        }, 50);
                    //        // $scope.saveMeasurements(true,dataSource,false,false);

                    //    }

                    $scope.EditMode = false;
                    $scope.TextboxDisabled = false;

                    //added newly now
                    //$scope.Disableotherfields = false;
                    if ($scope.Disableotherfields) {
                        $scope.Disableotherfields = false;
                    }
                    $scope.MultipleEditMode = false;
                    setBackToOrginalState();
                    //
                    $scope.Measurement_detail_form.$setPristine();
                    $scope.enableAddressvalidationFlag = true;
                    if ($scope.routeQuoteFlag) {
                        $scope.Newquotepage($scope.preserveQuoteId);
                    }
                    $scope.preventChange = true;
                } else {
                    if (event) {
                        event.preventDefault();
                        $scope.preventChange = true;
                    }
                }
            } else if (!$scope.changeFlag && $scope.brandid == 1) {
                $scope.EditMode = false;
                setBackToOrginalState(); // added so that multi copy text box are reset to orginal state.
                $scope.MultipleEditMode = false; //--> this make multiple copy txt to disable mode.
                var grid = $('#gridEditMeasurements').data("kendoGrid");
                if ($scope.OldMeasurementValues && $scope.OldMeasurementValues.options) {
                    grid.setDataSource($scope.OldMeasurementValues.options.data);
                    grid.dataSource.page(1);
                } else { grid.setDataSource(null); grid.dataSource.page(1); }
            }
        }

        $scope.validateAreaMoisture = function () {
            //commented for tlcc rollback changes.
            //if ($scope.brandid == 2 || $scope.brandid == 3) {
            //    var areaInput = $("#Area").data("kendoNumericTextBox");
            //    if (!areaInput)
            //        return true;
            //    var qtyInput = $("#Qty").data("kendoNumericTextBox");
            //    var areaInputValue;
            //    var qtyInputValue;
            //    if (areaInput) {
            //        areaInputValue = areaInput.value();
            //    }
            //    if (qtyInput) {
            //        qtyInputValue = qtyInput.value();
            //    }

            //    var qtyEnabled = $("#Qty").prop('disabled');
            //    var areaEnabled = $("#Area").prop('disabled');

            //    if (areaInputValue || qtyInputValue) {

            //        var parent1 = $("#Area").parent()[0];
            //        var parentElement = $(parent1).parent()[0];
            //        $(parentElement).removeClass("required-error");

            //        parent1 = $("#Qty").parent()[0];
            //        parentElement = $(parent1).parent()[0];
            //        $(parentElement).removeClass("required-error");
            //        return true;
            //    } else {
            //        var returnval = true;
            //        if (!areaEnabled) {
            //            var parent1 = $("#Area").parent()[0];
            //            var parentElement = $(parent1).parent()[0];
            //            $(parentElement).addClass("required-error");
            //            returnval = false;
            //        }
            //        else {
            //            var parent1 = $("#Area").parent()[0];
            //            var parentElement = $(parent1).parent()[0];
            //            $(parentElement).removeClass("required-error");
            //        }

            //        if (!qtyEnabled) {
            //            var parent1 = $("#Qty").parent()[0];
            //            var parentElement = $(parent1).parent()[0];
            //            $(parentElement).addClass("required-error");
            //            returnval = false;
            //        }
            //        else {
            //            var parent1 = $("#Qty").parent()[0];
            //            var parentElement = $(parent1).parent()[0];
            //            $(parentElement).removeClass("required-error");
            //        }

            //        return returnval;
            //    }
            //}
            return true;
        }

        $scope.saveMeasurements_addrow = function () {

            var validAreaMoisture = $scope.validateAreaMoisture();

            var gridData = $('#gridEditMeasurements').data().kendoGrid.dataSource.data();
            var validator = $scope.kendoValidator("gridEditMeasurements");

            if (validator.validate() && validAreaMoisture) {
                for (var i = 0; i < gridData.length; i++) {
                    gridData[i].id = i + 1;
                }
                $scope.MeasurementHeader = {};
                if ($scope.brandid == 1)
                    $scope.MeasurementHeader.MeasurementBB = gridData;
                else if ($scope.brandid == 2)
                    $scope.MeasurementHeader.MeasurementTL = gridData;
                else if ($scope.brandid == 3)
                    $scope.MeasurementHeader.MeasurementCC = gridData;

                $scope.MeasurementHeader.OpportunityId = $scope.OpportunityId;
                $scope.InstallationAddressId.InstallationAddressId = $scope.InstallationAddressId;
                $scope.MeasurementHeader.InstallationAddressId = $scope.InstallationAddressId;
                var dto = $scope.MeasurementHeader;

                var formData = new FormData();
                formData.append("dto", JSON.stringify(dto));
                formData.append("Files", JSON.stringify($scope.selectedFiles));

                $http.post('/api/Measurement/0', formData,
                    {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).then(function (response) {
                        $scope.PhotoFilename = null;
                        $scope.selectedFiles = [];

                        $scope.EditMode = false;
                        $scope.TextboxDisabled = false;
                        $scope.MeasurementDatasource = null;
                        if (response.data != null && response.data !== '')
                            $scope.MeasurementDatasource = response.data;
                        var dataSource = new kendo.data.DataSource({ data: response.data, pageSize: 1000 });
                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        grid.setDataSource(dataSource);
                        grid.dataSource.page(1);
                        grid.dataSource.read();
                        if ($scope.saveRedirectCancel == true) {
                            $scope.EditMode = true;
                            $scope.saveRedirectCancel = false;
                            if ($scope.AddNewRow) {
                                $scope.AddNewRow = false;
                                $scope.AddMeasurementsData();
                            }
                        }
                    });
            }
        };
        function getBase64(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => {
                    //  console.log("*****    ", data);
                    resolve(reader['result']);
                }
            });
        }

        ////Save Measurement
        $scope.saveMeasurements = function (flag, gridDataList, editFlag, controlFlag) {

            var validAreaMoisture = $scope.validateAreaMoisture();
            var grid;
            // set array for copy scenario
            if (flag) {
                grid = gridDataList;
            } else {
                grid = $('#gridEditMeasurements').data().kendoGrid.dataSource.data();
                //updateGridValue(grid);
            }

            var validator = $scope.kendoValidator("gridEditMeasurements");

            if (validator.validate() && validAreaMoisture) {
                updateGridValue(grid);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
                for (var i = 0; i < grid.length; i++) {
                    grid[i].id = i + 1;
                }
                var loading = $("#loading");
                $(loading).css("display", "block");

                $scope.MeasurementHeader = {};
                if ($scope.brandid == 1) {
                    $scope.MeasurementHeader.MeasurementBB = grid;
                }
                else if ($scope.brandid == 2)
                    $scope.MeasurementHeader.MeasurementTL = grid;
                else if ($scope.brandid == 3)
                    $scope.MeasurementHeader.MeasurementCC = grid;

                $scope.MeasurementHeader.OpportunityId = $scope.OpportunityId;
                $scope.InstallationAddressId.InstallationAddressId = $scope.InstallationAddressId;
                $scope.MeasurementHeader.InstallationAddressId = $scope.InstallationAddressId;
                var dto = $scope.MeasurementHeader;

                var dddd = $scope.base;

                // pushing the images
                //for (var i = 0; i < dto.MeasurementBB.length; i++) {
                //    if (dto.MeasurementBB[i]['fileName'] && !dto.MeasurementBB[i]['Stream_id']) {
                //        $scope.fileNameChanged(true, dto.MeasurementBB[i]);
                //    }
                //}
                setBackToOrginalState(); // added so that multi copy text box are reset to orginal state.
                $scope.MultipleEditMode = false; //--> this make multiple copy txt to disable mode.
                var formData = new FormData();
                formData.append("dto", JSON.stringify(dto));
                formData.append("Files", JSON.stringify($scope.selectedFiles));

                $http.post('/api/Measurement/0', formData,
                    {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).then(function (response) {
                        //  HFC.DisplaySuccess("Action Done Successfully");
                        //$scope.Disableotherfields = false;
                        $scope.EnablCopyFlag = false;
                        $scope.Measurement_detail_form.$setPristine();
                        var grid = $("#gridEditMeasurements").data("kendoGrid");
                        var dataSource = grid.dataSource;
                        $scope.PhotoFilename = null;
                        $scope.selectedFiles = [];
                        $scope.currentEditId = null;
                        if (controlFlag) {
                            $scope.EditMode = true;
                        } else {
                            $scope.EditMode = false;
                        }
                        if ($scope.Disableotherfields) {
                            $scope.Disableotherfields = false;
                        }
                        $scope.TextboxDisabled = false;
                        $scope.MeasurementDatasource = null;
                        if (response.data != null && response.data !== '')
                            $scope.MeasurementDatasource = response.data;
                        var dataSource = new kendo.data.DataSource({ data: response.data, pageSize: 1000 });
                        var grid = $('#gridEditMeasurements').data("kendoGrid");
                        dataSource.read();
                        grid.setDataSource(dataSource);
                        $('.k-grid-content.k-auto-scrollable').scrollTop(0);
                        // for getting back up data
                        $scope.GetMeasurmentData();
                        if (editFlag) {
                            var editDataId = dataSource._data.length - 1;
                            var editData = dataSource._data[editDataId];
                            grid.editRow(editData);
                            setTimeout(function () {
                                makeRowEditable(editData);
                                $("#Width").focus();
                            }, 100)


                        }
                        else {
                            if ($scope.saveRedirectCancel == true) {
                                $scope.EditMode = true;
                                $scope.saveRedirectCancel = false;
                                if ($scope.AddNewRow) {
                                    $scope.AddNewRow = false;
                                    $scope.AddMeasurementsData();
                                }
                            }

                            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                            if ($scope.SaveDataEdit) {
                                $scope.SaveDataEdit = false;
                                if ($scope.SingleCopy) {
                                    $scope.SingleCopy = false;
                                    $scope.IncrementallyCopy();
                                } else if ($scope.MultiCopy) {
                                    $scope.MultiCopy = false;
                                    $scope.Copymultipletime();
                                }
                            }
                            $(loading).css("display", "none");
                            //$scope.GetDataLength();
                        }
                    }).catch(function (e) {
                        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                    });
            }
        };

        //Enable Edit mode in Kendo Grid
        $scope.MeasurementEdit = function () {
            var loading = $("#loading");
            $(loading).css("display", "block");
            setTimeout(function () {
                $scope.EditMode = true;
                $scope.changeFlag = false;
                $("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
                });
                $('#searchBox').val('');
                //$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({});
                var grid = $('#gridEditMeasurements').data('kendoGrid');
                var data = grid.dataSource.data();
                for (var i = 0; i < data.length; i++) {
                    //data[i].NotSave = true; this for single/multi copy wrongly assigned.
                    data[i].editable = true;
                }
                grid.setDataSource(data);
                grid.dataSource.page(1);
                if (data == null || (data != null && data.length == 0)) {
                    $(loading).css("display", "none");
                }

            }, 10);
        }

        $scope.saveonDel_Edit = function () {
            var grid = $('#gridEditMeasurements').data().kendoGrid.dataSource.data();

            for (var i = 0; i < grid.length; i++) {
                grid[i].id = i + 1;
            }
            $scope.MeasurementHeader = {};
            if ($scope.brandid == 1)
                $scope.MeasurementHeader.MeasurementBB = grid;
            else if ($scope.brandid == 2)
                $scope.MeasurementHeader.MeasurementTL = grid;
            else if ($scope.brandid == 3)
                $scope.MeasurementHeader.MeasurementCC = grid;

            $scope.MeasurementHeader.OpportunityId = $scope.OpportunityId;
            $scope.InstallationAddressId.InstallationAddressId = $scope.InstallationAddressId;
            $scope.MeasurementHeader.InstallationAddressId = $scope.InstallationAddressId;
            var dto = $scope.MeasurementHeader;

            angular.forEach($scope.selectedFiles, function (value, key) {
                for (var k = 0; k < dto.MeasurementBB.length; k++) {

                    if (value.uid == dto.MeasurementBB[k].uid) {
                        value.id = dto.MeasurementBB[k].id;
                    }
                }
            });

            var formData = new FormData();
            formData.append("dto", JSON.stringify(dto));
            formData.append("Files", JSON.stringify($scope.selectedFiles));

            $http.post('/api/Measurement/0', formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).then(function (response) {
                    // HFC.DisplaySuccess("Action Done Successfully");
                    //  $scope.Measurement_detail_form.$setPristine();
                    $scope.PhotoFilename = null;
                    $scope.selectedFiles = [];

                    $scope.MeasurementDatasource = null;
                    if (response.data != null && response.data !== '')
                        $scope.MeasurementDatasource = response.data;
                    var dataSource = new kendo.data.DataSource({ data: response.data, pageSize: 1000 });
                    var grid = $('#gridEditMeasurements').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                });
        }

        // delete row for BB workaround
        $scope.deleteRowBB = function (e) {
            // clearing filter & search in grid
            $('#searchBox').val('');
            //$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
            //});
            $scope.EditMode = true;
            var dataItem = angular.copy(e.dataItem);
            $scope.deleteDataArray = [];
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            if (grid) {
                var loading = $("#loading");
                $(loading).css("display", "block");
                $scope.actionFlag = true;
                setTimeout(function () {
                    var dataSource = grid.dataSource._data;
                    for (var i = 0; i < dataSource.length; i++) {
                        if (dataItem['id'] !== dataSource[i]['id']) {
                            $scope.deleteDataArray.push(dataSource[i]);
                        }
                    }
                    $scope.deleteDataArray = removeUnsavedLines($scope.deleteDataArray);//call the function and update the id in dataItem.
                    if ($scope.deleteDataArray && $scope.deleteDataArray.length !== 0) {
                        grid.setDataSource($scope.deleteDataArray);
                        grid.dataSource.page(1);
                        setTimeout(function () {
                            $scope.actionFlag = false;
                        }, 100);
                    } else {
                        var loading = $("#loading");
                        grid.setDataSource(null);
                        grid.dataSource.page(1);
                        $(loading).css("display", "none");
                    }
                    $scope.changeFlag = true;
                }, 10);
            }
        }

        //Custom Delete
        $scope.DeleteRowDetails = function (e) {

            $scope.currentEditId = null;
            $scope.EditMode = true;

            var rowEdit = $('#gridEditMeasurements').find('.k-grid-edit-row');
            if (rowEdit.length != 0 && rowEdit[0].dataset.uid != e.dataItem.uid) {
                var validAreaMoisture = $scope.validateAreaMoisture();
                if (!validAreaMoisture)
                    return false;
                var validator = $scope.kendoValidator("gridEditMeasurements");
                if (validator.validate()) {
                    var dataItem = e.dataItem;
                    var grid = $("#gridEditMeasurements").data("kendoGrid");
                    var dataSource = grid.dataSource;

                    dataSource.remove(dataItem);

                } else {

                    return false;
                }
            }
            else {
                var dataItem = e.dataItem;
                var grid = $("#gridEditMeasurements").data("kendoGrid");
                var dataSource = grid.dataSource;

                dataSource.remove(dataItem);
            }

            $scope.TextboxDisabled = false;

            $scope.saveonDel_Edit();

            var grid = $("#gridEditMeasurements").data("kendoGrid");
            var dataSourcee = grid.dataSource;

            if (dataSourcee._data.length === 0) {
                $scope.rowNumberr = 0;
            }
            //$scope.GetDataLength(); $scope.EnablCopyFlag = false;
            //$scope.Disableotherfields = false; $scope.EditMode = false; //added newly
        };

        // make edit row work around
        $scope.editRowBB = function (obj) {
            // clearing filter & search in grid
            $('#searchBox').val('');
            //$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
            //});
            var dataItem = angular.copy(obj);
            $scope.EditMode = true;
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            if (grid) {
                dataItem['editable'] = true;
                var dataSource = grid.dataSource._data;
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i]['id'] === dataItem['id']) {
                        dataSource[i] = dataItem;
                    }
                }
                grid.setDataSource(dataSource);
            }

        }

        $scope.EditRowDetailsBB = function (obj) {

            $scope.NewEditRow = false;
            $scope.EditMode = true;

            var validAreaMoisture = $scope.validateAreaMoisture();
            if (!validAreaMoisture)
                return false;

            var grid = $("#gridEditMeasurements").data("kendoGrid");
            var validator = $scope.kendoValidator("gridEditMeasurements");

            if (validator.validate()) {

                // for photo option
                $scope.currentEditId = obj.id;
                if (obj.fileName)
                    $scope.PhotoFilename = obj.fileName;
                else
                    $scope.PhotoFilename = null;

                var grid = $("#gridEditMeasurements").data("kendoGrid");
                var dataSource = grid.dataSource;
                if (obj) {

                    var grid = $('#gridEditMeasurements').data().kendoGrid.dataSource.data();



                    for (var i = 0; i < grid.length; i++) {
                        grid[i].id = i + 1;
                    }
                    $scope.MeasurementHeader = {};
                    if ($scope.brandid == 1)
                        $scope.MeasurementHeader.MeasurementBB = grid;
                    else if ($scope.brandid == 2)
                        $scope.MeasurementHeader.MeasurementTL = grid;
                    else if ($scope.brandid == 3)
                        $scope.MeasurementHeader.MeasurementCC = grid;

                    $scope.MeasurementHeader.OpportunityId = $scope.OpportunityId;
                    $scope.InstallationAddressId.InstallationAddressId = $scope.InstallationAddressId;
                    $scope.MeasurementHeader.InstallationAddressId = $scope.InstallationAddressId;
                    var dto = $scope.MeasurementHeader;

                    angular.forEach($scope.selectedFiles, function (value, key) {
                        for (var k = 0; k < dto.MeasurementBB.length; k++) {

                            if (value.uid == dto.MeasurementBB[k].uid) {
                                value.id = dto.MeasurementBB[k].id;
                            }
                        }

                    });

                    var formData = new FormData();
                    formData.append("dto", JSON.stringify(dto));
                    formData.append("Files", JSON.stringify($scope.selectedFiles));

                    $http.post('/api/Measurement/0', formData,
                        {
                            withCredentials: true,
                            headers: { 'Content-Type': undefined },
                            transformRequest: angular.identity
                        }).then(function (response) {
                            // HFC.DisplaySuccess("Action Done Successfully");
                            //  $scope.Measurement_detail_form.$setPristine();
                            $scope.PhotoFilename = null;
                            $scope.selectedFiles = [];
                            $scope.EnablCopyFlag = false;
                            $scope.MeasurementDatasource = null;
                            if (response.data != null && response.data !== '')
                                $scope.MeasurementDatasource = response.data;
                            var dataSource = new kendo.data.DataSource({ data: response.data, pageSize: 1000 });
                            var grid = $('#gridEditMeasurements').data("kendoGrid");
                            dataSource.read();
                            grid.setDataSource(dataSource);

                            // grid.refresh();
                            var EditRow = null;
                            for (var p = 0; p <= dataSource._data.length; p++) {
                                if (dataSource._data[p].id == obj.id) {
                                    $scope.PhotoFilename = dataSource._data[p].fileName;
                                    EditRow = dataSource._data[p];
                                    grid.editRow(EditRow);
                                    break;
                                }
                            }
                            // grid.editRow(obj);
                            makeRowEditable(obj);
                            //$scope.GetDataLength();
                            //if ($scope.brandid === 2) {
                            //    var FranctionalValueWidth = $("#FranctionalValueWidth").data("kendoDropDownList");
                            //    var FranctionalValueHeight = $("#FranctionalValueHeight").data("kendoDropDownList");
                            //    var FranctionalValueDepth = $("#FranctionalValueDepth").data("kendoDropDownList");
                            //    //var Area = $("#Area").data("kendoNumericTextBox");
                            //    //var Moisture = $("#Moisture").data("kendoNumericTextBox");
                            //    var Quantity = $("#Qty").data("kendoNumericTextBox");

                            //    var storageTypeddl = $("#storageType").data("kendoDropDownList");
                            //    if (obj.System == "Closet") {
                            //        storageTypeddl.enable(true);
                            //        if (obj.StorageType == "Slacks" ||
                            //            obj.StorageType == "Short Hang" ||
                            //            obj.StorageType == "Medium Hang" ||
                            //            obj.StorageType == "Long Hang" ||
                            //            obj.StorageType == "Short Hang" ||
                            //            obj.StorageType == "Folded") {
                            //            //Enable the Text Box


                            //            $('input[name="StorageDescription"]').prop('disabled', false);
                            //            $('input[name="Comments"]').prop('disabled', false);
                            //            $('input[name="Width"]').prop('disabled', false);
                            //            $('input[name="Height"]').prop('disabled', false);
                            //            $('input[name="SystemDescription"]').prop('disabled', false);
                            //            $('input[name="StorageDescription"]').prop('disabled', false);
                            //            // Enable the dropdown
                            //            FranctionalValueWidth.enable(true);
                            //            FranctionalValueHeight.enable(true);

                            //            //Set The required field true
                            //            $('input[name="Height"]').prop('required', true);
                            //            $('input[name="Width"]').prop('required', true);
                            //        }
                            //        else if (obj.StorageType == "Accessories") {

                            //            $('input[name="Width"]').prop('disabled', false);
                            //            $('input[name="Height"]').prop('disabled', false);
                            //            $('input[name="Depth"]').prop('disabled', false);
                            //            //$('input[name="Qty"]').prop('disabled', false);
                            //            Quantity.enable(true);
                            //            $('input[name="SystemDescription"]').prop('disabled', false);
                            //            $('input[name="StorageDescription"]').prop('disabled', false);
                            //            $('input[name="Comments"]').prop('disabled', false);

                            //            //set the required field true
                            //            $('input[name="Height"]').prop('required', true);
                            //            $('input[name="Width"]').prop('required', true);
                            //            $('input[name="Depth"]').prop('required', true);
                            //            $('input[name="Qty"]').prop('required', true);

                            //            FranctionalValueWidth.enable(true);
                            //            FranctionalValueHeight.enable(true);
                            //            FranctionalValueDepth.enable(true);
                            //        }
                            //    }
                            //    else if (obj.System == "Office") {
                            //        storageTypeddl.enable(true);
                            //        if (obj.StorageType == "Printer" ||
                            //            obj.StorageType == "Fax" ||
                            //            obj.StorageType == "Copier" ||
                            //            obj.StorageType == "Television" ||
                            //            obj.StorageType == "Couch/Chair" ||
                            //            obj.StorageType == "Stereo" ||
                            //            obj.StorageType == "File Cabinet") {

                            //            $('input[name="Width"]').prop('disabled', false);
                            //            $('input[name="Height"]').prop('disabled', false);
                            //            $('input[name="Depth"]').prop('disabled', false);
                            //            $('input[name="SystemDescription"]').prop('disabled', false);
                            //            $('input[name="StorageDescription"]').prop('disabled', false);

                            //            $('input[name="Comments"]').prop('disabled', false);

                            //            $('input[name="Height"]').prop('required', true);
                            //            $('input[name="Width"]').prop('required', true);
                            //            $('input[name="Depth"]').prop('required', true);

                            //            FranctionalValueWidth.enable(true);
                            //            FranctionalValueHeight.enable(true);
                            //            FranctionalValueDepth.enable(true);
                            //        }
                            //        else if (obj.StorageType == "Workstation") {

                            //            $("#CustomData").data("kendoDropDownList").enable(true);

                            //            $('input[name="Height"]').prop('required', false);
                            //            $('input[name="Width"]').prop('required', false);
                            //            $('input[name="Depth"]').prop('required', false);

                            //            $('input[name="Height"]').prop('disabled', true);
                            //            $('input[name="Width"]').prop('disabled', true);
                            //            $('input[name="Depth"]').prop('disabled', true);
                            //            $('input[name="SystemDescription"]').prop('disabled', false);
                            //            $('input[name="StorageDescription"]').prop('disabled', false);
                            //            $('input[name="Comments"]').prop('disabled', false);

                            //            $('input[name="Height"]').removeClass("k-invalid");
                            //            $('input[name="Width"]').removeClass("k-invalid");
                            //            $('input[name="Depth"]').removeClass("k-invalid");
                            //        }
                            //    }
                            //    else if (obj.System == "Garage") {
                            //        storageTypeddl.enable(true);
                            //        if (obj.StorageType == "Vehicles" ||
                            //            obj.StorageType == "Tools" ||
                            //            obj.StorageType == "Appliances" || obj.StorageType == "Bikes") {

                            //            $('input[name="Width"]').prop('disabled', false);
                            //            $('input[name="Height"]').prop('disabled', false);
                            //            $('input[name="Depth"]').prop('disabled', false);
                            //            $('input[name="SystemDescription"]').prop('disabled', false);
                            //            $('input[name="StorageDescription"]').prop('disabled', false);
                            //            $('input[name="Comments"]').prop('disabled', false);

                            //            $('input[name="Height"]').prop('required', true);
                            //            $('input[name="Width"]').prop('required', true);
                            //            $('input[name="Depth"]').prop('required', true);

                            //            FranctionalValueWidth.enable(true);
                            //            FranctionalValueHeight.enable(true);
                            //            FranctionalValueDepth.enable(true);
                            //        }
                            //    }
                            //    else if (obj.System == "Floor") {
                            //        storageTypeddl.enable(true);
                            //        if (obj.StorageType == "Size" ||
                            //            obj.StorageType == "Tramex" ||
                            //            obj.StorageType == "Stem Wall" || obj.StorageType == "Bikes") {
                            //            $('input[name="SystemDescription"]').prop('disabled', false);
                            //            $('input[name="StorageDescription"]').prop('disabled', false);
                            //            $('input[name="Comments"]').prop('disabled', false);

                            //        }
                            //        //if (obj.StorageType == "Tramex")
                            //        //    Moisture.enable(true);

                            //        //Area.enable(true);
                            //    }

                            //    //if ((EditRow.SquareFeetAdd != null && EditRow.SquareFeetAdd.length > 0) || (EditRow.SquareFeetSub != null && EditRow.SquareFeetSub.length > 0)) {
                            //    //    Area.enable(false);
                            //    //}

                            //    $scope.validateAreaMoisture();
                            //}

                            //if ($scope.brandid === 3) {

                            //    var Area = $("#Area").data("kendoNumericTextBox");
                            //    var Moisture = $("#Moisture").data("kendoNumericTextBox");
                            //    var Quantity = $("#Qty").data("kendoNumericTextBox");
                            //    $('input[name="Comments"]').prop('disabled', false);
                            //    $('input[name="SystemDescription"]').prop('disabled', false);
                            //    Quantity.enable(true);
                            //    //$('input[name="Qty"]').prop('disabled', false);
                            //    Moisture.enable(true);
                            //    Area.enable(true);

                            //    if ((EditRow.SquareFeetAdd != null && EditRow.SquareFeetAdd.length > 0) || (EditRow.SquareFeetSub != null && EditRow.SquareFeetSub.length > 0)) {
                            //        Area.enable(false);
                            //    }

                            //    $scope.validateAreaMoisture();
                            //}

                            setTimeout(function () {
                                $scope.Changenegativenumber();
                            }, 500)
                        });
                }


            } else {

                return false;
            }
        };

        //Search BB Grid
        $scope.MeasurementgridSearchEdit = function () {
            var validAreaMoisture = $scope.validateAreaMoisture();
            if (!validAreaMoisture) {
                $('#searchBox').val('');

                $scope.TextboxDisabled = true;
            }

            $('#btnReset').prop('disabled', false);
            var validator = $scope.kendoValidator("gridEditMeasurements");
            if (validator.validate()) {
                var searchValue = $('#searchBox').val();
                if ($scope.brandid === 1) {
                    $("#gridEditMeasurements").data("kendoGrid").dataSource.filter({

                        logic: "or",
                        filters: [
                            {
                                field: "RoomLocation",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "RoomName",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "WindowLocation",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "MountTypeName",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "Comments",
                                operator: "contains",
                                value: searchValue
                            }
                        ]
                    });
                }
                if ($scope.brandid === 2) {
                    $("#gridEditMeasurements").data("kendoGrid").dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "System",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "SystemDescription",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "StorageType",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "StorageDescription",
                                operator: "contains",
                                value: searchValue
                            },

                            {
                                field: "CustomData",
                                operator: "contains",
                                value: searchValue
                            },

                            {
                                field: "Comments",
                                operator: "contains",
                                value: searchValue
                            }
                        ]
                    });
                }
                if ($scope.brandid === 3) {
                    $("#gridEditMeasurements").data("kendoGrid").dataSource.filter({

                        logic: "or",
                        filters: [
                            {
                                field: "System",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "SystemDescription",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "Comments",
                                operator: "contains",
                                value: searchValue
                            }
                        ]
                    });
                }
            }
            else {
                $('#searchBox').val('');

                $scope.TextboxDisabled = true;
            }
        };

        //Clear Search and Filters
        $scope.MeasurementgridEditSearchClear = function () {
            if ($('#gridEditMeasurements').find('.k-grid-edit-row').length < 1) {
                //grid is not in edit mode
                $scope.currentEditId = null;
                var meas = $("#gridEditMeasurements").data('kendoGrid').dataSource;
                //if (meas.options.data.length < meas._data.length) {
                //    meas.remove(meas._data[0]);
                //}
                $('#searchBox').val('');
                $("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
                });
                $scope.EditMode = false;
                $('#btnReset').prop('disabled', true);
            }

        };

        $scope.ShowCalculator = function (data) {
            $('#gridSquareFeetAdd>.k-grid-content.k-auto-scrollable').height(200);
            $('#gridSquareFeetSub>.k-grid-content.k-auto-scrollable').height(200);
            $scope.EditRowDetailsBB(data);
            SquareFeetCalculatorService.SquareFeetHeader.Add = null;
            SquareFeetCalculatorService.SquareFeetHeader.Add = null;

            var dataSource = $("#gridEditMeasurements").data("kendoGrid").dataSource._data;
            angular.forEach(dataSource, function (value, key) {
                if (value.id == $scope.currentEditId) {
                    SquareFeetCalculatorService.SquareFeetHeader.Add = value.SquareFeetAdd;
                    SquareFeetCalculatorService.SquareFeetHeader.Sub = value.SquareFeetSub;
                }
            });

            SquareFeetCalculatorService.ShowCalculator($scope.CalculatePopupClosed, $scope.receiveDigestCallback);
        }

        // clone workaround fix
        $scope.cloneMeasurementLine = function (data) {
            // clearing filter & search in grid
            $('#searchBox').val('');
            //$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
            //});
            $scope.EditMode = true;
            var dataItem = angular.copy(data['dataItem']);
            var idbackup = data['dataItem']['id'];
            //var onCopyData = dataItem;
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            if (grid) {
                var loading = $("#loading");
                $(loading).css("display", "block");
                $scope.actionFlag = true;
                setTimeout(function () {
                    var grid = $("#gridEditMeasurements").data("kendoGrid");
                    $scope.cloneDataSource = grid.dataSource._data;
                    var clonedDataId = $scope.cloneDataSource.length;
                    // update the id & flag value in copied line
                    dataItem['id'] = clonedDataId + 1;
                    dataItem['WindowLocation'] = ""; //during clone this need to be empty
                    dataItem['uid'] = dataItem['id'];
                    // @ramasamy - add editable falg in or condition with notsave flag
                    dataItem['editable'] = true;

                    //added the code to copy image too if exists.
                    if (dataItem['fileName']) {
                        //$scope.copyPhotoData = angular.copy($scope.selectedFiles);
                        for (var i = 0; i < $scope.selectedFiles.length; i++) {
                            if (idbackup == $scope.selectedFiles[i]['id'] && dataItem['fileName'] == $scope.selectedFiles[i]['name']) {
                                $scope.selectedFiles.push({ id: dataItem['id'], name: $scope.selectedFiles[i]['name'], base64: $scope.selectedFiles[i]['base64'], uid: dataItem['uid'] });

                            }
                        }
                        //$scope.selectedFiles.push($scope.copyPhotoData);
                    }
                    //end..

                    $scope.cloneDataSource.push(dataItem);
                    grid.setDataSource($scope.cloneDataSource);
                    grid.dataSource.page(1);

                    // scrolling to copied row
                    var grid = $("#gridEditMeasurements").data("kendoGrid");
                    var dataSourceLength = grid.dataSource._data.length - 1;
                    var widthId = '#width' + dataSourceLength;
                    $(widthId).focus();
                    $scope.changeFlag = true;
                    setTimeout(function () {
                        $scope.actionFlag = false;
                    }, 100);
                }, 10);
            }
        }

        //Clone row
        $scope.AddMesurementRowWithSameRoom = function (data) {

            $scope.EditMode = true;
            $scope.clone = true;
            $('#searchBox').val('');
            var validator = $scope.kendoValidator("gridEditMeasurements");
            var validAreaMoisture = $scope.validateAreaMoisture();

            if (validator.validate() && validAreaMoisture) {

                var grid = $("#gridEditMeasurements").data("kendoGrid");
                var dataSource = grid.dataSource;
                var index = dataSource._data.length;
                if (index !== 0) {
                    var dataItem = data.dataItem;
                    $scope.clonedId = data.dataItem.id;

                    if ($scope.brandid === 1 || $scope.brandid === 2 || $scope.brandid === 3) {
                        // start saving

                        var grid1 = $('#gridEditMeasurements').data().kendoGrid.dataSource.data();


                        for (var i = 0; i < grid1.length; i++) {
                            grid1[i].id = i + 1;
                        }
                        $scope.MeasurementHeader = {};
                        if ($scope.brandid == 1)
                            $scope.MeasurementHeader.MeasurementBB = grid1;
                        else if ($scope.brandid == 2)
                            $scope.MeasurementHeader.MeasurementTL = grid1;
                        else if ($scope.brandid == 3)
                            $scope.MeasurementHeader.MeasurementCC = grid1;

                        $scope.MeasurementHeader.OpportunityId = $scope.OpportunityId;
                        $scope.InstallationAddressId.InstallationAddressId = $scope.InstallationAddressId;
                        $scope.MeasurementHeader.InstallationAddressId = $scope.InstallationAddressId;
                        var dto = $scope.MeasurementHeader;

                        var formData = new FormData();
                        formData.append("dto", JSON.stringify(dto));
                        formData.append("Files", JSON.stringify($scope.selectedFiles));

                        $http.post('/api/Measurement/0', formData,
                            {
                                withCredentials: true,
                                headers: { 'Content-Type': undefined },
                                transformRequest: angular.identity
                            }).then(function (response) {
                                $scope.PhotoFilename = null;
                                $scope.selectedFiles = [];

                                $scope.EditMode = false;
                                $scope.TextboxDisabled = false;
                                $scope.EnablCopyFlag = false;
                                $scope.MeasurementDatasource = null;
                                if (response.data != null && response.data !== '')
                                    $scope.MeasurementDatasource = response.data;

                                var dataSource = new kendo.data.DataSource({ data: response.data, pageSize: 1000 });
                                var grid1 = $('#gridEditMeasurements').data("kendoGrid");
                                dataSource.read();
                                grid1.setDataSource(dataSource);

                                if ($scope.brandid == 1) {
                                    for (var r = 0; r < dataSource._data.length; r++) {
                                        if (dataSource._data[r].id == $scope.clonedId) {

                                            $scope.currentEditId = dataSource._data.length + 1;
                                            $scope.PhotoFilename = dataItem.fileName;

                                            var newItem = {
                                                id: $scope.currentEditId,
                                                MountTypeName: dataSource._data[r].MountTypeName,
                                                RoomLocation: dataSource._data[r].RoomLocation,
                                                RoomName: dataSource._data[r].RoomName,
                                                WindowLocation: dataSource._data[r].WindowLocation,
                                                Comments: dataSource._data[r].Comments,
                                                Width: dataSource._data[r].Width,
                                                Height: dataSource._data[r].Height,
                                                FranctionalValueWidth: dataSource._data[r].FranctionalValueWidth,
                                                FranctionalValueHeight: dataSource._data[r].FranctionalValueHeight,
                                                fileName: dataSource._data[r].fileName,
                                                Stream_id: dataSource._data[r].Stream_id,
                                                thumb_Stream_id: dataSource._data[r].thumb_Stream_id
                                            };
                                            if (!newItem.thumb_Stream_id)
                                                newItem.fileName = "";
                                            var newItem = dataSource.insert(index, newItem); //, {});
                                            newItem.WindowLocation = "";
                                            //grid.dataSource.page(grid.dataSource.pageSize());
                                            var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                                            if (!newRow.thumb_Stream_id)
                                                newRow.fileName = "";
                                            grid.editRow(newRow);

                                            if ($("#System"))
                                                if ($("#System").data("kendoDropDownList"))
                                                    $("#System").data("kendoDropDownList").focus();
                                            //grid.element.find(".k-grid-content").animate({
                                            //    scrollTop: newRow.offset().top
                                            //}, 400);

                                            var a = $("#Room").parent();
                                            var b = a[0].children[0].children;
                                            $(b[0]).removeClass("requireddropfield");
                                            $(b[0]).addClass("k-input");

                                            var a = $("#Mount").parent();
                                            var b = a[0].children[0].children;
                                            $(b[0]).removeClass("requireddropfield");
                                            $(b[0]).addClass("k-input");

                                            $scope.EditMode = true;
                                            setTimeout(function () {
                                                var scrollHeight = $("#gridEditMeasurements .k-grid-content >table").height() + 100;
                                                $("#gridEditMeasurements .k-grid-content").scrollTop(scrollHeight);
                                            }, 300);
                                        }
                                    }
                                    //$scope.GetDataLength();
                                } else if ($scope.brandid == 2) {
                                    for (var r = 0; r < dataSource._data.length; r++) {
                                        if (dataSource._data[r].id == $scope.clonedId) {

                                            $scope.currentEditId = dataSource._data.length + 1;
                                            $scope.PhotoFilename = dataItem.fileName;

                                            newItem = {
                                                id: $scope.currentEditId,
                                                System: dataSource._data[r].System,
                                                SystemDescription: dataSource._data[r].SystemDescription,
                                                StorageType: dataSource._data[r].StorageType,
                                                StorageDescription: dataSource._data[r].StorageDescription,
                                                Width: dataSource._data[r].Width,
                                                FranctionalValueWidth: dataSource._data[r].FranctionalValueWidth,
                                                Height: dataSource._data[r].Height,
                                                FranctionalValueHeight: dataSource._data[r].FranctionalValueHeight,
                                                Depth: dataSource._data[r].Depth,
                                                FranctionalValueDepth: dataSource._data[r].FranctionalValueDepth,
                                                Comments: dataSource._data[r].Comments,
                                                Laptop: dataSource._data[r].Laptop,
                                                Desktop: dataSource._data[r].Desktop,
                                                UseTP: dataSource._data[r].UseTP,
                                                Store: dataSource._data[r].Store,
                                                //Area: dataSource._data[r].Area,
                                                //Moisture: dataSource._data[r].Moisture,
                                                Qty: dataSource._data[r].Qty,
                                                CustomData: dataSource._data[r].CustomData,
                                                fileName: dataSource._data[r].fileName,
                                                Stream_id: dataSource._data[r].Stream_id,
                                                thumb_Stream_id: dataSource._data[r].thumb_Stream_id,
                                                SquareFeetAdd: dataSource._data[r].SquareFeetAdd,
                                                SquareFeetSub: dataSource._data[r].SquareFeetSub
                                            }
                                            if (!newItem.thumb_Stream_id)
                                                newItem.fileName = "";
                                            var newItem = dataSource.insert(index, newItem);
                                            //grid.dataSource.page(grid.dataSource.pageSize());
                                            var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                                            if (!newRow.thumb_Stream_id)
                                                newRow.fileName = "";
                                            grid.editRow(newRow);

                                            setTimeout(function () {
                                                var scrollHeight = $("#gridEditMeasurements .k-grid-content >table").height() + 100;
                                                $("#gridEditMeasurements .k-grid-content").scrollTop(scrollHeight);
                                            }, 300);

                                            var a = $("#System").parent();
                                            var b = a[0].children[0].children;
                                            $(b[0]).removeClass("requireddropfield");
                                            $(b[0]).addClass("k-input");
                                            $scope.SystemPreventClose = false;

                                            TLDropdownChange(newItem);

                                            var a = $("#CustomData").parent();
                                            var b = a[0].children[0].children;
                                            $(b[0]).removeClass("requireddropfield");
                                            $(b[0]).addClass("k-input");
                                            $scope.CustomDataPreventClose = false;

                                            // $scope.clone = false;
                                            $scope.EditMode = true;
                                        }
                                    }
                                }
                                else if ($scope.brandid == 3) {
                                    for (var r = 0; r < dataSource._data.length; r++) {
                                        if (dataSource._data[r].id == $scope.clonedId) {

                                            $scope.currentEditId = dataSource._data.length + 1;
                                            $scope.PhotoFilename = dataItem.fileName;

                                            newItem = {
                                                id: $scope.currentEditId,
                                                System: dataSource._data[r].System,
                                                SystemDescription: dataSource._data[r].SystemDescription,
                                                Area: dataSource._data[r].Area,
                                                Moisture: dataSource._data[r].Moisture,
                                                Comments: dataSource._data[r].Comments,
                                                Qty: dataSource._data[r].Qty,
                                                CustomData: dataSource._data[r].CustomData,
                                                fileName: dataSource._data[r].fileName,
                                                Stream_id: dataSource._data[r].Stream_id,
                                                thumb_Stream_id: dataSource._data[r].thumb_Stream_id,
                                                SquareFeetAdd: dataSource._data[r].SquareFeetAdd,
                                                SquareFeetSub: dataSource._data[r].SquareFeetSub
                                            }
                                            if (!newItem.thumb_Stream_id)
                                                newItem.fileName = "";
                                            var newItem = dataSource.insert(index, newItem);
                                            //grid.dataSource.page(grid.dataSource.pageSize());
                                            var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                                            if (!newRow.thumb_Stream_id)
                                                newRow.fileName = "";
                                            grid.editRow(newRow);
                                            setTimeout(function () {
                                                var scrollHeight = $("#gridEditMeasurements .k-grid-content >table").height() + 100;
                                                $("#gridEditMeasurements .k-grid-content").scrollTop(scrollHeight);
                                            }, 300);

                                            var a = $("#System").parent();
                                            var b = a[0].children[0].children;
                                            $(b[0]).removeClass("requireddropfield");
                                            $(b[0]).addClass("k-input");
                                            $scope.SystemPreventClose = false;

                                            CCDropdownChange(newItem);
                                            // $scope.clone = false;
                                            $scope.EditMode = true;
                                        }
                                    }
                                }
                                setTimeout(function () {
                                    $scope.Changenegativenumber();
                                }, 500)
                            });
                    }
                    //else if ($scope.brandid == 2) {
                    //    $scope.PhotoFilename = dataItem.fileName;
                    //    newItem = {
                    //        id: index,
                    //        System: dataItem.System,
                    //        SystemDescription: dataItem.SystemDescription,
                    //        StorageType: dataItem.StorageType,
                    //        StorageDescription: dataItem.StorageDescription,
                    //        Width: dataItem.Width,
                    //        FranctionalValueWidth: dataItem.FranctionalValueWidth,
                    //        Height: dataItem.Height,
                    //        FranctionalValueHeight: dataItem.FranctionalValueHeight,
                    //        Depth: dataItem.Depth,
                    //        FranctionalValueDepth: dataItem.FranctionalValueDepth,
                    //        Comments: dataItem.Comments,
                    //        Laptop: dataItem.Laptop,
                    //        Desktop: dataItem.Desktop,
                    //        UseTP: dataItem.UseTP,
                    //        Store: dataItem.Store,
                    //        Area: dataItem.Area,
                    //        Moisture: dataItem.Moisture,
                    //        Qty: dataItem.Qty,
                    //        CustomData: dataItem.CustomData,
                    //        fileName: dataItem.fileName,
                    //        Stream_id: dataItem.Stream_id,
                    //        thumb_Stream_id: dataItem.thumb_Stream_id,
                    //        SquareFeetAdd: dataItem.SquareFeetAdd,
                    //        SquareFeetSub: dataItem.SquareFeetSub
                    //    }
                    //    if (!newItem.thumb_Stream_id)
                    //        newItem.fileName = "";
                    //    var newItem = dataSource.insert(index, newItem);
                    //    grid.dataSource.page(grid.dataSource.pageSize());
                    //    var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                    //    if (!newRow.thumb_Stream_id)
                    //        newRow.fileName = "";
                    //    grid.editRow(newRow);
                    //    $("#System").data("kendoDropDownList").focus();
                    //    //grid.element.find(".k-grid-content").animate({
                    //    //    scrollTop: newRow.offset().top
                    //    //}, 400);

                    //    var a = $("#System").parent();
                    //    var b = a[0].children[0].children;
                    //    $(b[0]).removeClass("requireddropfield");
                    //    $(b[0]).addClass("k-input");
                    //    $scope.SystemPreventClose = false;

                    //    TLDropdownChange(newItem);

                    //    var a = $("#CustomData").parent();
                    //    var b = a[0].children[0].children;
                    //    $(b[0]).removeClass("requireddropfield");
                    //    $(b[0]).addClass("k-input");
                    //    $scope.CustomDataPreventClose = false;

                    //    $scope.clone = false;
                    //}
                    //else if ($scope.brandid == 3) {
                    //    $scope.PhotoFilename = dataItem.fileName;
                    //    newItem = {
                    //        id: index,
                    //        System: dataItem.System,
                    //        SystemDescription: dataItem.SystemDescription,
                    //        Area: dataItem.Area,
                    //        Moisture: dataItem.Moisture,
                    //        Comments: dataItem.Comments,
                    //        Qty: dataItem.Qty,
                    //        CustomData: dataItem.CustomData,
                    //        fileName: dataItem.fileName,
                    //        Stream_id: dataItem.Stream_id,
                    //        thumb_Stream_id: dataItem.thumb_Stream_id,
                    //        SquareFeetAdd: dataItem.SquareFeetAdd,
                    //        SquareFeetSub: dataItem.SquareFeetSub
                    //    }
                    //    if (!newItem.thumb_Stream_id)
                    //        newItem.fileName = "";
                    //    var newItem = dataSource.insert(index, newItem);
                    //    grid.dataSource.page(grid.dataSource.pageSize());
                    //    var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                    //    if (!newRow.thumb_Stream_id)
                    //        newRow.fileName = "";
                    //    grid.editRow(newRow);
                    //    //grid.element.find(".k-grid-content").animate({
                    //    //    scrollTop: newRow.offset().top
                    //    //}, 400);
                    //    $("#System").data("kendoDropDownList").focus();
                    //    var a = $("#System").parent();
                    //    var b = a[0].children[0].children;
                    //    $(b[0]).removeClass("requireddropfield");
                    //    $(b[0]).addClass("k-input");
                    //    $scope.SystemPreventClose = false;

                    //    CCDropdownChange(newItem);
                    //    $scope.clone = false;
                    //};
                }
                else {
                    HFC.DisplaySuccess("wa! No Data Is Available");
                }
            };
        }

        $scope.Update_width_fraction = function ($event, flag) {
            var dataItem;
            var widthId;
            var fractionWidthId;
            if (flag) {
                var idStr = $event.target.id;
                var selectedElementId = idStr.split("h")[1];
                var grid = $('#gridEditMeasurements').data("kendoGrid");
                var dataSource = grid.dataSource._data;
                var unsavedList = [];
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                        unsavedList.push(dataSource[i]);
                    }
                }
                dataItem = unsavedList[selectedElementId];
                dataItem['Width'] = $event.target.value;
                widthId = "#width" + selectedElementId;
                fractionWidthId = "#widthFraction" + selectedElementId;
            } else {
                var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                item.data().uid
                dataItem = item.data().$scope.dataItem;
                widthId = "#Width";
                fractionWidthId = "#FranctionalValueWidth";
            }

            var radixPos = String($(widthId).val()).indexOf('.');
            var value = parseFloat(String($(widthId).val()).slice(radixPos));

            if ($(widthId).val() === '') {

                $(widthId).val('');
                dataItem.Width = '';
                dataItem.FranctionalValueWidth = null;
                if ($(fractionWidthId).data('kendoDropDownList') != null && $(fractionWidthId).data('kendoDropDownList') != undefined)
                    $(fractionWidthId).data('kendoDropDownList').value(null);
                else
                    $(fractionWidthId).val(null);

            }
            else {
                dataItem.Width = Math.trunc(dataItem.Width);
                if (dataItem['Width'] < 0) {
                    dataItem['Width'] = (dataItem['Width']) * -1;
                }
                $(widthId).val(Math.trunc($(widthId).val()));

                if (radixPos != -1) {

                    var fraction = $scope.getfraction(value);
                    dataItem.FranctionalValueWidth = fraction;
                    if ($(fractionWidthId).data('kendoDropDownList') != null && $(fractionWidthId).data('kendoDropDownList') != undefined)
                        $(fractionWidthId).data('kendoDropDownList').value(fraction);
                    else
                        $(fractionWidthId).val(fraction);
                }
            }
        };

        $scope.Changenegativenumber = function () {
            $("#Width").change(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $("#Width").keyup(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $("#Height").change(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $("#Height").keyup(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            // for template multi edit scenario
            $(".height-field").keyup(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $(".height-field").change(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $(".width-field").keyup(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $(".width-field").change(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });
        }

        $scope.getfraction = function (value) {

            if (value > 0 && value < 0.125) { return "1/16"; }
            else if (value < 0.1875) { return "1/8"; }
            else if (value < 0.25) { return "3/16"; }
            else if (value < 0.3125) { return "1/4"; }
            else if (value < 0.375) { return "5/16"; }
            else if (value < 0.4375) { return "3/8"; }
            else if (value < 0.5) { return "7/16"; }
            else if (value < 0.5625) { return "1/2"; }
            else if (value < 0.625) { return "9/16"; }
            else if (value < 0.6875) { return "5/8"; }
            else if (value < 0.75) { return "11/16"; }
            else if (value < 0.8125) { return "3/4"; }
            else if (value < 0.875) { return "13/16"; }
            else if (value < 0.9375) { return "7/8"; }
            else if (value < 1) { return "15/16"; }
            else { return null; }
        }

        $scope.change_width = function ($event, flag) {
            var dataItem;
            if (flag) {
                var idStr = $event.target.id;
                var selectedElementId = idStr.split("h")[1];
                var grid = $('#gridEditMeasurements').data("kendoGrid");
                var dataSource = grid.dataSource._data;
                var unsavedList = [];
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                        unsavedList.push(dataSource[i]);
                    }
                }
                dataItem = unsavedList[selectedElementId];
                dataItem.FranctionalValueWidth = null;
                var fractionId = '#widthFraction' + selectedElementId;
                if ($(fractionId).data('kendoDropDownList') != null && $(fractionId).data('kendoDropDownList') != undefined)
                    $(fractionId).data('kendoDropDownList').value(null);
                else
                    $(fractionId).val(null);
                $scope.changeFlag = true; //to state user made changes
            } else {
                var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                item.data().uid
                dataItem = item.data().$scope.dataItem;
                dataItem.FranctionalValueWidth = null;
                if ($("#FranctionalValueWidth").data('kendoDropDownList') != null && $("#FranctionalValueWidth").data('kendoDropDownList') != undefined)
                    $("#FranctionalValueWidth").data('kendoDropDownList').value(null);
                else
                    $("#FranctionalValueWidth").val(null);
            }
        };

        //Width fractionDropdown
        function FractionDropDownEditorWidth(container, options) {
            //
            if ($scope.NewEditRow && ($scope.BrandId != 2)) {
                var a = $("#Mount").parent();
                var b = a[0].children[0].children;
                $(b[0]).removeClass("k-input");
                $(b[0]).addClass("requireddropfield");
            }
            if ($scope.brandid == 1) {
                $('<input type="number" data-type="number" required style="width: 42.2%; margin-right:1px;" ng-keypress="change_width($event)" ng-blur="Update_width_fraction()" class="k-textbox"  name="Width"  id="Width" value="${Width}"/>').appendTo(container);
                $('<input id="FranctionalValueWidth"  style="width: 57.2%;" class="measurement_wfield" name="FranctionalValueWidth" data-bind="value:FranctionalValueWidth"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        valuePrimitive: true,
                        optionLabel: "Select",
                        autoBind: true,
                        autoWidth: true,
                        dataTextField: "Value",
                        dataValueField: "Key",
                        template: "#=Value #",
                        dataSource: {
                            data:
                                [{ Key: "1/16", Value: "1/16" },
                                { Key: "1/8", Value: "1/8" },
                                { Key: "3/16", Value: "3/16" },
                                { Key: "1/4", Value: "1/4" },
                                { Key: "5/16", Value: "5/16" },
                                { Key: "3/8", Value: "3/8" },
                                { Key: "7/16", Value: "7/16" },
                                { Key: "1/2", Value: "1/2" },
                                { Key: "9/16", Value: "9/16" },
                                { Key: "5/8", Value: "5/8" },
                                { Key: "11/16", Value: "11/16" },
                                { Key: "3/4", Value: "3/4" },
                                { Key: "13/16", Value: "13/16" },
                                { Key: "7/8", Value: "7/8" },
                                { Key: "15/16", Value: "15/16" }]
                        }
                    });
            }
            else if ($scope.brandid == 2) {
                $('<input data-type="number" disabled  style="width: 49.2%; margin-right:1px;" ng-keypress="change_width($event)"  ng-blur="Update_width_fraction()" class="k-textbox"  name="Width"  id="Width" value="${Width}"/>').appendTo(container);
                $('<input id="FranctionalValueWidth" disabled style="width: 49.2%;" class="measurement_wfield" name="FranctionalValueWidth" data-bind="value:FranctionalValueWidth"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        valuePrimitive: true,
                        optionLabel: "Select",
                        autoBind: true,
                        autoWidth: true,
                        dataTextField: "Value",
                        dataValueField: "Key",
                        template: "#=Value #",
                        dataSource: {
                            data:
                                [{ Key: "1/16", Value: "1/16" },
                                { Key: "1/8", Value: "1/8" },
                                { Key: "3/16", Value: "3/16" },
                                { Key: "1/4", Value: "1/4" },
                                { Key: "5/16", Value: "5/16" },
                                { Key: "3/8", Value: "3/8" },
                                { Key: "7/16", Value: "7/16" },
                                { Key: "1/2", Value: "1/2" },
                                { Key: "9/16", Value: "9/16" },
                                { Key: "5/8", Value: "5/8" },
                                { Key: "11/16", Value: "11/16" },
                                { Key: "3/4", Value: "3/4" },
                                { Key: "13/16", Value: "13/16" },
                                { Key: "7/8", Value: "7/8" },
                                { Key: "15/16", Value: "15/16" }]
                        }

                    });
            }
        }

        $scope.Update_height_fraction = function ($event, flag) {
            var dataItem;
            var widthId;
            var fractionWidthId;
            if (flag) {
                var idStr = $event.target.id;
                var selectedElementId = idStr.split("t")[1];
                var grid = $('#gridEditMeasurements').data("kendoGrid");
                var dataSource = grid.dataSource._data;
                var unsavedList = [];
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                        unsavedList.push(dataSource[i]);
                    }
                }
                dataItem = unsavedList[selectedElementId];
                dataItem['Height'] = $event.target.value;
                heightId = "#height" + selectedElementId;
                fractionHeightId = "#heightFraction" + selectedElementId;
            } else {
                var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                item.data().uid
                dataItem = item.data().$scope.dataItem;
                heightId = "#height";
                fractionHeightId = "#FranctionalValueHeight";
            }
            var radixPos = String(dataItem.Height).indexOf('.');
            var value = parseFloat(String(dataItem.Height).slice(radixPos));
            if ($(heightId).val() === '') {

                $(heightId).val('');
                dataItem.Height = '';

                dataItem.FranctionalValueHeight = null;
                if ($(fractionHeightId).data('kendoDropDownList') != null && $(fractionHeightId).data('kendoDropDownList') != undefined)
                    $(fractionHeightId).data('kendoDropDownList').value(null);
                else
                    $(fractionHeightId).val(null);
            }
            else {

                dataItem.Height = Math.trunc(dataItem.Height);
                //added to avoid neg value
                if (dataItem['Height'] < 0) {
                    dataItem['Height'] = (dataItem['Height']) * -1;
                }
                //end
                $(heightId).val(dataItem.Height);

                if (radixPos != -1) {

                    var fraction = $scope.getfraction(value);

                    dataItem.FranctionalValueHeight = fraction;
                    if ($(fractionHeightId).data('kendoDropDownList') != null && $(fractionHeightId).data('kendoDropDownList') != undefined)
                        $(fractionHeightId).data('kendoDropDownList').value(fraction);
                    else
                        $(fractionHeightId).val(fraction);
                }
            }
        };



        $scope.change_height = function ($event, flag) {
            var dataItem;
            if (flag) {
                var idStr = $event.target.id;
                var selectedElementId = idStr.split("t")[1];
                var grid = $('#gridEditMeasurements').data("kendoGrid");
                var dataSource = grid.dataSource._data;
                var unsavedList = [];
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                        unsavedList.push(dataSource[i]);
                    }
                }
                dataItem = unsavedList[selectedElementId];
                dataItem.FranctionalValueHeight = null;
                var fractionId = '#heightFraction' + selectedElementId;
                if ($(fractionId).data('kendoDropDownList') != null && $(fractionId).data('kendoDropDownList') != undefined)
                    $(fractionId).data('kendoDropDownList').value(null);
                else
                    $(fractionId).val(null);
                $scope.changeFlag = true; //to state user made changes
            } else {
                var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
                item.data().uid
                var dataItem = item.data().$scope.dataItem;

                dataItem.FranctionalValueHeight = null;
                if ($("#FranctionalValueHeight").data('kendoDropDownList') != null && $("#FranctionalValueHeight").data('kendoDropDownList') != undefined)
                    $("#FranctionalValueHeight").data('kendoDropDownList').value(null);
                else
                    $("#FranctionalValueHeight").val(null);
            }
        };

        //Height Fraction
        function FractionDropDownEditorHeight(container, options) {
            //
            if ($scope.brandid == 1) {
                $('<input type="number" data-type="number" required  style="width:42.2%; margin-right:1px;" ng-blur="Update_height_fraction()" ng-keypress="change_height($event)"  class="k-textbox"  name="Height"  id="Height" value="${Height}"/>').appendTo(container);
                $('<input id="FranctionalValueHeight" style="width:57.2%;" class="measurement_wfield" name="FranctionalValueHeight" data-bind="value:FranctionalValueHeight"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        valuePrimitive: true,
                        autoBind: true,
                        autoWidth: true,
                        optionLabel: "Select",
                        dataTextField: "Value",
                        dataValueField: "Key",
                        template: "#=Value #",
                        dataSource: {
                            data:
                                [{ Key: "1/16", Value: "1/16" },
                                { Key: "1/8", Value: "1/8" },
                                { Key: "3/16", Value: "3/16" },
                                { Key: "1/4", Value: "1/4" },
                                { Key: "5/16", Value: "5/16" },
                                { Key: "3/8", Value: "3/8" },
                                { Key: "7/16", Value: "7/16" },
                                { Key: "1/2", Value: "1/2" },
                                { Key: "9/16", Value: "9/16" },
                                { Key: "5/8", Value: "5/8" },
                                { Key: "11/16", Value: "11/16" },
                                { Key: "3/4", Value: "3/4" },
                                { Key: "13/16", Value: "13/16" },
                                { Key: "7/8", Value: "7/8" },
                                { Key: "15/16", Value: "15/16" }]
                        }
                    });
            }
            else if ($scope.brandid == 2) {
                $('<input data-type="number" disabled  style="width: 49.2%; margin-right:1px;"  ng-blur="Update_height_fraction()"  ng-keypress="change_height($event)"  class="k-textbox"  name="Height"   id="Height" value="${Height}" />').appendTo(container);
                $('<input id="FranctionalValueHeight" disabled style="width: 49.2%; " name="FranctionalValueHeight" data-bind="value:FranctionalValueHeight"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        valuePrimitive: true,
                        autoBind: true,
                        autoWidth: true,
                        optionLabel: "Select",
                        dataTextField: "Value",
                        dataValueField: "Key",
                        template: "#=Value #",
                        dataSource: {
                            data:
                                [{ Key: "1/16", Value: "1/16" },
                                { Key: "1/8", Value: "1/8" },
                                { Key: "3/16", Value: "3/16" },
                                { Key: "1/4", Value: "1/4" },
                                { Key: "5/16", Value: "5/16" },
                                { Key: "3/8", Value: "3/8" },
                                { Key: "7/16", Value: "7/16" },
                                { Key: "1/2", Value: "1/2" },
                                { Key: "9/16", Value: "9/16" },
                                { Key: "5/8", Value: "5/8" },
                                { Key: "11/16", Value: "11/16" },
                                { Key: "3/4", Value: "3/4" },
                                { Key: "13/16", Value: "13/16" },
                                { Key: "7/8", Value: "7/8" },
                                { Key: "15/16", Value: "15/16" }]
                        }
                    });
            }
        }

        $scope.setdirtyOrNot = function () {
            if ($scope.Measurement_detail_form)
                if ($scope.Measurement_detail_form.$dirty != true) {

                    if ($('#gridEditMeasurements').find('.k-grid-edit-row').length) {
                        $scope.Measurement_detail_form.$dirty = true;
                        $scope.preventChange = true;
                    } else {
                        $scope.Measurement_detail_form.$setPristine();
                        $scope.preventChange = false;
                    }
                } else {
                    $scope.preventChange = true;
                }
        };

        $scope.$on('$routeChangeStart', function ($event, next, current) {
            if (!$scope.preventChange) {
                var grid = $('#gridEditMeasurements').data().kendoGrid.dataSource.data();
                var validateFlag = checkFlagValue(grid);
                if (validateFlag) {
                    $scope.Measurement_detail_form.$setPristine();
                    setTimeout(function () { $scope.preventChange = false; }, 100);

                    $scope.cancelMeasurementEdit($event, true);
                } else {
                    $scope.setdirtyOrNot();
                }

            } else {
                //if (current.$$route.originalPath.contains("measurementDetail"))
                if (current.$$route.originalPath.includes("measurementDetail")) {
                    $event.preventDefault();
                    $scope.preventChange = false;
                    if ($scope.Measurement_detail_form.$dirty)
                        $scope.Measurement_detail_form.$setPristine();
                }

            }
        });

        $scope.Update_depth_fraction = function () {
            var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
            item.data().uid
            var dataItem = item.data().$scope.dataItem;
            var radixPos = String(dataItem.Depth).indexOf('.');
            var value = parseFloat(String(dataItem.Depth).slice(radixPos));


            if ($("#Depth").val() === '') {

                $("#Depth").val('');
                dataItem.Depth = '';

                dataItem.FranctionalValueDepth = null;
                $("#FranctionalValueDepth").data('kendoDropDownList').value(null);

            }
            else {

                dataItem.Depth = Math.trunc(dataItem.Depth);
                $("#Depth").val(dataItem.Depth);

                if (radixPos != -1) {

                    var fraction = $scope.getfraction(value);

                    dataItem.FranctionalValueDepth = fraction;
                    $("#FranctionalValueDepth").data('kendoDropDownList').value(fraction);
                }
            }
        };

        $scope.change_depth = function ($event) {

            var item = $('#gridEditMeasurements').find('.k-grid-edit-row');
            item.data().uid
            var dataItem = item.data().$scope.dataItem;

            dataItem.FranctionalValueDepth = null;
            $("#FranctionalValueDepth").data('kendoDropDownList').value(null);
        };

        //Depth Fraction Dropdown
        function FractionDropDownEditorDepth(container, options) {
            if ($scope.brandid == 1)
                $('<input data-type="number" disabled  style="width:49.2%; margin-right:1px" class="k-textbox"  name="Depth"/>').appendTo(container);
            else
                $('<input data-type="number" disabled  style="width:49.2%; margin-right:1px" class="k-textbox"    ng-keypress="change_depth($event)"  ng-blur="Update_depth_fraction()"  name="Depth"   id="Depth" value="${Depth}" />').appendTo(container);
            $('<input id="FranctionalValueDepth" disabled style="width:49.2%; " name="FranctionalValueDepth" data-bind="value:FranctionalValueDepth"/>')
                .appendTo(container)
                .kendoDropDownList({
                    valuePrimitive: true,
                    optionLabel: "Select",
                    autoBind: true,
                    autoWidth: true,
                    dataTextField: "Value",
                    dataValueField: "Key",
                    template: "#=Value #",
                    dataSource: {
                        data:
                            [{ Key: "1/16", Value: "1/16" },
                            { Key: "1/8", Value: "1/8" },
                            { Key: "3/16", Value: "3/16" },
                            { Key: "1/4", Value: "1/4" },
                            { Key: "5/16", Value: "5/16" },
                            { Key: "3/8", Value: "3/8" },
                            { Key: "7/16", Value: "7/16" },
                            { Key: "1/2", Value: "1/2" },
                            { Key: "9/16", Value: "9/16" },
                            { Key: "5/8", Value: "5/8" },
                            { Key: "11/16", Value: "11/16" },
                            { Key: "3/4", Value: "3/4" },
                            { Key: "13/16", Value: "13/16" },
                            { Key: "7/8", Value: "7/8" },
                            { Key: "15/16", Value: "15/16" }]
                    }
                });
        }

        function SetRoomNameDefault(obj, Rooms, id) {
            var grid = $("#gridEditMeasurements").data("kendoGrid");

            var row = grid.dataSource.get(obj.id);
            var roomDetails = $scope.RoomLocationDrop;
            var abbrValue = "";
            if (obj.RoomLocation && obj.WindowLocation) {
                for (var i = 0; i < $scope.RoomLocationDrop.length; i++) {
                    if ($scope.RoomLocationDrop[i].Value == obj.RoomLocation) {
                        abbrValue = $scope.RoomLocationDrop[i].AbbrValue;
                    }
                }

                //var defaultValue = obj.RoomLocation + " -" + obj.WindowLocation.split("/")[1]
                var defaultValue = abbrValue + " -" + obj.WindowLocation.split("/")[1]
                //row.set("RoomName", defaultValue);
                obj.RoomName = defaultValue;
                if (id) {
                    var selectedId = "#roomname" + id;
                    $(selectedId).val(defaultValue)
                } else {
                    $("#RoomName").val(defaultValue);
                }
            }

        };

        // print integration -- murugan
        $scope.printControl = {};
        $scope.printSalesPacket = function (modalid) {
            if ($routeParams.OpportunityId) $scope.opportunityIdPrint = $routeParams.OpportunityId;

            // $("#" + modalid).modal("show");
            $scope.printControl.showPrintModal();
        }

        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {

            $scope.GridSizeCalled = $scope.GridSizeCalled + 1;

            var size = 248;

            if ($scope.GridSizeCalled == 3 && SquareFeetCalculatorService.OneTimeLoad) {
                size = size + 53;
                SquareFeetCalculatorService.OneTimeLoad = false;
            }

            if (!window.location.href.includes('/measurementDetails'))
                size = size + 23;

            var menuDivHeight = $("#MenuDiv").height();

            var height = window.innerHeight - menuDivHeight - size;

            $('#gridEditMeasurements>.k-grid-content.k-auto-scrollable').height(height);

            window.onresize = function () {
                $('#gridEditMeasurements>.k-grid-content.k-auto-scrollable').height(window.innerHeight - menuDivHeight - size);
            };
            $('#gridSquareFeetAdd>.k-grid-content.k-auto-scrollable').height(170);
            $('#gridSquareFeetSub>.k-grid-content.k-auto-scrollable').height(170);

            if ((window.innerHeight - menuDivHeight - 30) > 661) {
                $('.modal-body').height(661);
            }
            else {
                if (!window.location.href.includes('/measurementDetails'))
                    $('.modal-body').height(window.innerHeight - menuDivHeight - 30);
                else
                    $('.modal-body').height(window.innerHeight - menuDivHeight - 50);
            }
            setTimeout(function () { //$scope.GetDataLength();
            }, 500);

        });
        // End Kendo Resizing

        $scope.RoomNmeKeyDown = function (controlId, flag) {
            var cId = controlId.currentTarget.id;
            var value = $("#" + cId).val();
            $scope.changeFlag = true; //added to state changes is been made.
            if (flag) {
                var selectedElementId = cId.split("e")[1];
                var grid = $('#gridEditMeasurements').data("kendoGrid");
                var dataSource = grid.dataSource._data;
                var unsavedList = [];
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i]['NotSave'] || dataSource[i]['editable']) {
                        unsavedList.push(dataSource[i]);
                    }
                }
                var dataItem = unsavedList[selectedElementId];
                dataItem.RoomName = value;
            } else {
                if (value.indexOf('&') != -1) $("#" + cId).val(value.replace(/\&/g, ""));
                if (value.indexOf('\'') != -1) $("#" + cId).val(value.replace(/\'/g, ""));
                if (value.indexOf('\"') != -1) $("#" + cId).val(value.replace(/\"/g, ""));
            }
        }
        //--end

        //to make a row editable these are used for tl & CC
        function makeRowEditable(datas) {

            var obj = datas;
            if ($scope.brandid === 2) {
                var FranctionalValueWidth = $("#FranctionalValueWidth").data("kendoDropDownList");
                var FranctionalValueHeight = $("#FranctionalValueHeight").data("kendoDropDownList");
                var FranctionalValueDepth = $("#FranctionalValueDepth").data("kendoDropDownList");
                //var Area = $("#Area").data("kendoNumericTextBox");
                //var Moisture = $("#Moisture").data("kendoNumericTextBox");
                var Quantity = $("#Qty").data("kendoNumericTextBox");

                var storageTypeddl = $("#storageType").data("kendoDropDownList");
                if (obj.System == "Closet") {
                    storageTypeddl.enable(true);
                    if (obj.StorageType == "Slacks" ||
                        obj.StorageType == "Short Hang" ||
                        obj.StorageType == "Medium Hang" ||
                        obj.StorageType == "Long Hang" ||
                        obj.StorageType == "Short Hang" ||
                        obj.StorageType == "Folded") {
                        //Enable the Text Box


                        $('input[name="StorageDescription"]').prop('disabled', false);
                        $('input[name="Comments"]').prop('disabled', false);
                        $('input[name="Width"]').prop('disabled', false);
                        $('input[name="Height"]').prop('disabled', false);
                        $('input[name="SystemDescription"]').prop('disabled', false);
                        $('input[name="StorageDescription"]').prop('disabled', false);
                        // Enable the dropdown
                        FranctionalValueWidth.enable(true);
                        FranctionalValueHeight.enable(true);

                        //Set The required field true
                        $('input[name="Height"]').prop('required', true);
                        $('input[name="Width"]').prop('required', true);
                    }
                    else if (obj.StorageType == "Accessories") {

                        $('input[name="Width"]').prop('disabled', false);
                        $('input[name="Height"]').prop('disabled', false);
                        $('input[name="Depth"]').prop('disabled', false);
                        //$('input[name="Qty"]').prop('disabled', false);
                        Quantity.enable(true);
                        $('input[name="SystemDescription"]').prop('disabled', false);
                        $('input[name="StorageDescription"]').prop('disabled', false);
                        $('input[name="Comments"]').prop('disabled', false);

                        //set the required field true
                        $('input[name="Height"]').prop('required', true);
                        $('input[name="Width"]').prop('required', true);
                        $('input[name="Depth"]').prop('required', true);
                        $('input[name="Qty"]').prop('required', true);

                        FranctionalValueWidth.enable(true);
                        FranctionalValueHeight.enable(true);
                        FranctionalValueDepth.enable(true);
                    }
                }
                else if (obj.System == "Office") {
                    storageTypeddl.enable(true);
                    if (obj.StorageType == "Printer" ||
                        obj.StorageType == "Fax" ||
                        obj.StorageType == "Copier" ||
                        obj.StorageType == "Television" ||
                        obj.StorageType == "Couch/Chair" ||
                        obj.StorageType == "Stereo" ||
                        obj.StorageType == "File Cabinet") {

                        $('input[name="Width"]').prop('disabled', false);
                        $('input[name="Height"]').prop('disabled', false);
                        $('input[name="Depth"]').prop('disabled', false);
                        $('input[name="SystemDescription"]').prop('disabled', false);
                        $('input[name="StorageDescription"]').prop('disabled', false);

                        $('input[name="Comments"]').prop('disabled', false);

                        $('input[name="Height"]').prop('required', true);
                        $('input[name="Width"]').prop('required', true);
                        $('input[name="Depth"]').prop('required', true);

                        FranctionalValueWidth.enable(true);
                        FranctionalValueHeight.enable(true);
                        FranctionalValueDepth.enable(true);
                    }
                    else if (obj.StorageType == "Workstation") {

                        $("#CustomData").data("kendoDropDownList").enable(true);

                        $('input[name="Height"]').prop('required', false);
                        $('input[name="Width"]').prop('required', false);
                        $('input[name="Depth"]').prop('required', false);

                        $('input[name="Height"]').prop('disabled', true);
                        $('input[name="Width"]').prop('disabled', true);
                        $('input[name="Depth"]').prop('disabled', true);
                        $('input[name="SystemDescription"]').prop('disabled', false);
                        $('input[name="StorageDescription"]').prop('disabled', false);
                        $('input[name="Comments"]').prop('disabled', false);

                        $('input[name="Height"]').removeClass("k-invalid");
                        $('input[name="Width"]').removeClass("k-invalid");
                        $('input[name="Depth"]').removeClass("k-invalid");
                    }
                }
                else if (obj.System == "Garage") {
                    storageTypeddl.enable(true);
                    if (obj.StorageType == "Vehicles" ||
                        obj.StorageType == "Tools" ||
                        obj.StorageType == "Appliances" || obj.StorageType == "Bikes") {

                        $('input[name="Width"]').prop('disabled', false);
                        $('input[name="Height"]').prop('disabled', false);
                        $('input[name="Depth"]').prop('disabled', false);
                        $('input[name="SystemDescription"]').prop('disabled', false);
                        $('input[name="StorageDescription"]').prop('disabled', false);
                        $('input[name="Comments"]').prop('disabled', false);

                        $('input[name="Height"]').prop('required', true);
                        $('input[name="Width"]').prop('required', true);
                        $('input[name="Depth"]').prop('required', true);

                        FranctionalValueWidth.enable(true);
                        FranctionalValueHeight.enable(true);
                        FranctionalValueDepth.enable(true);
                    }
                }
                else if (obj.System == "Floor") {
                    storageTypeddl.enable(true);
                    if (obj.StorageType == "Size" ||
                        obj.StorageType == "Tramex" ||
                        obj.StorageType == "Stem Wall" || obj.StorageType == "Bikes") {
                        $('input[name="SystemDescription"]').prop('disabled', false);
                        $('input[name="StorageDescription"]').prop('disabled', false);
                        $('input[name="Comments"]').prop('disabled', false);

                    }
                    //if (obj.StorageType == "Tramex")
                    //    Moisture.enable(true);

                    //Area.enable(true);
                }

                //if ((EditRow.SquareFeetAdd != null && EditRow.SquareFeetAdd.length > 0) || (EditRow.SquareFeetSub != null && EditRow.SquareFeetSub.length > 0)) {
                //    Area.enable(false);
                //}

                $scope.validateAreaMoisture();
            }

            if ($scope.brandid === 3) {

                var Area = $("#Area").data("kendoNumericTextBox");
                var Moisture = $("#Moisture").data("kendoNumericTextBox");
                var Quantity = $("#Qty").data("kendoNumericTextBox");
                $('input[name="Comments"]').prop('disabled', false);
                $('input[name="SystemDescription"]').prop('disabled', false);
                Quantity.enable(true);
                //$('input[name="Qty"]').prop('disabled', false);
                Moisture.enable(true);
                Area.enable(true);

                if ((EditRow.SquareFeetAdd != null && EditRow.SquareFeetAdd.length > 0) || (EditRow.SquareFeetSub != null && EditRow.SquareFeetSub.length > 0)) {
                    Area.enable(false);
                }

                $scope.validateAreaMoisture();
            }
        }
        //--end

        //copy single line measurement only once
        $scope.Disableotherfields = false; $scope.SaveDataEdit = false;
        $scope.SingleCopy = false; $scope.MultiCopy = false;
        $scope.EnablCopyFlag = false;
        $scope.IncrementallyCopy = function () {

            $scope.NewEditRow = false;
            setBackToOrginalState();

            $scope.MultipleEditMode = false; //--> this make multiple copy to disable mode.
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            var dataSource = grid.dataSource._data;

            //var validAreaMoisture = $scope.validateAreaMoisture();
            //var validator = $scope.kendoValidator("gridEditMeasurements");

            //if (validator.validate() && validAreaMoisture && dataSource.length > 0) {
            $('#searchBox').val('');
            //$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
            //});
            $scope.EnablCopyFlag = true;
            $scope.actionFlag = true;
            //if ($scope.EditMode) {
            //    $scope.SaveDataEdit = true;
            //    $scope.SingleCopy = true;
            //    //$scope.saveMeasurements();
            //} else {
            if (dataSource.length != 0) { //added the condition since if no data when click on single copy it throws error.
                var loading = $("#loading");
                $(loading).css("display", "block");
                $scope.EditMode = true;
                $scope.IncrementId = dataSource.length - 1;
                var datas = angular.copy(dataSource[$scope.IncrementId]);//copy the last data from grid datasource.
                var idbackup = dataSource[$scope.IncrementId]['id'];
                datas.id = datas.id + 1; //increment the id and update the window and room name (increment by 1).
                if (datas.WindowLocation && datas.RoomName && datas.RoomLocation) {
                    var Newwinval = $scope.Updatewindowval(datas.WindowLocation, datas.RoomName, datas.RoomLocation);

                    datas.WindowLocation = Newwinval.windowval;
                    datas.RoomName = Newwinval.Roomnameval;
                }
                datas.Width = ""; datas.Height = "";
                datas.FranctionalValueWidth = ""; datas.FranctionalValueHeight = "";
                delete (datas.uid);
                datas.NotSave = true;
                var Arrayofdatas = angular.copy(dataSource);
                Arrayofdatas = updateGridValue(Arrayofdatas);
                Arrayofdatas.push(datas);
                $scope.Disableotherfields = true;

                //added the code to copy image too if exists.
                if (datas['fileName']) {
                    //$scope.copyPhotoData = angular.copy($scope.selectedFiles);
                    for (var i = 0; i < $scope.selectedFiles.length; i++) {
                        if (idbackup == $scope.selectedFiles[i]['id'] && datas['fileName'] == $scope.selectedFiles[i]['name']) {
                            $scope.selectedFiles.push({ id: datas['id'], name: $scope.selectedFiles[i]['name'], base64: $scope.selectedFiles[i]['base64'], uid: datas['id'] });

                        }
                    }
                    //$scope.selectedFiles.push($scope.copyPhotoData);
                }
                //end..
                // update the grid data source
                setTimeout(function () {
                    grid.setDataSource(Arrayofdatas);
                    grid.dataSource.page(1);
                    var scrollValue = $('#gridEditMeasurements .k-grid-content').innerHeight();
                    // $('#gridEditMeasurements .k-grid-content').scrollTop(scrollValue);
                    setTimeout(function () {
                        $scope.Changenegativenumber();
                        // scrolling to copied row
                        var dataSourceLength = grid.dataSource._data.length - 1;
                        var widthId = '#width' + dataSourceLength;
                        $(widthId).focus();
                        $scope.actionFlag = false;
                    }, 500);
                }, 100);
            }
            // $scope.saveMeasurements(true, Arrayofdatas,true,true);

            //}
            //}

        }
        //end--

        //enable multiple copy test boxes
        $scope.MultipleEditMode = false;
        $scope.LineNumber = "Required";

        $scope.Multiple = {
            Linenum: "",
            Linecopy: "",
        }

        // multi copy button for enable txt box
        $scope.MultipleCopy = function () {

            // var validAreaMoisture = $scope.validateAreaMoisture();
            //var validator = $scope.kendoValidator("gridEditMeasurements");
            //if (validator.validate() && validAreaMoisture) {
            $scope.MultipleEditMode = true;
            $("#linenumber").addClass("required_field");
            $("#linecopy").addClass("required_field");
            //$scope.EditMode = true;
            //$scope.Disableotherfields = true; // needed here ?
            //}
        }
        $scope.submitted = false;

        // multi copy tick button
        $scope.Copymultipletime = function () {

            var Linedetails = $scope.Multiple;

            if ($scope.MultipleEditMode) {
                if ((Linedetails.Linenum == "" || Linedetails.Linenum == null) || (Linedetails.Linecopy == "" || Linedetails.Linecopy == null)) {
                    if (Linedetails.Linenum == "" || Linedetails.Linenum == null)
                        $('#linenumber').css('border-color', 'red');
                    else $('#linenumber').css('border-color', '');
                    if (Linedetails.Linecopy == "" || Linedetails.Linecopy == null)
                        $('#linecopy').css('border-color', 'red');
                    else $('#linecopy').css('border-color', '');
                    return;
                }
                else {
                    $('#linenumber').css('border-color', '');
                    $('#linecopy').css('border-color', '');
                }


                //var validAreaMoisture = $scope.validateAreaMoisture();
                //var validator = $scope.kendoValidator("gridEditMeasurements");
                //if (validator.validate() && validAreaMoisture) {
                $('#searchBox').val('');
                //$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
                //});

                var grid = $("#gridEditMeasurements").data("kendoGrid");
                $scope.submitted = true;



                var dataSource = grid.dataSource._data;
                dataSource = updateGridValue(dataSource);
                var validateFlag = true;
                //validation check if id not in list
                for (var r = 0; r < dataSource.length; r++) {
                    if (dataSource[r].id == Linedetails.Linenum) {

                        validateFlag = false;
                    }
                }

                if (validateFlag == true) {
                    HFC.DisplayAlert("The line number which you are trying to copy does not exist");
                    return;
                }

                if (Linedetails.Linecopy > 50) {
                    HFC.DisplayAlert("Only 50 lines are allowed to copy at a time");
                    return;
                }
                $scope.EnablCopyFlag = true;
                $scope.actionFlag = true;
                //if ($scope.EditMode) {
                //    $scope.SaveDataEdit = true;
                //    $scope.MultiCopy = true;
                //    //$scope.saveMeasurements();
                //} else {

                var Arrayofdatas = []; var Offsets = ""; var Lastrecord = "";
                var LastId = ""; $scope.EditMode = true; var idbackup = "";
                //$scope.EditMode = true;
                for (var k = 0; k < dataSource.length; k++) {
                    if (Linedetails.Linenum == dataSource[k].id) {
                        var Copydatas = dataSource[k];
                        Offsets = k;
                        idbackup = dataSource[k]['id'];
                        Arrayofdatas.push(dataSource[k]);//push until the copy id
                        break;
                        //var Copydatas = angular.copy(dataSource[k]);
                    }
                    Arrayofdatas.push(dataSource[k]); //push value before line id needed to be copied
                }
                var newdatacopied = "";
                if (Copydatas) { //increment the id to num of times
                    //$scope.IncrementallyCopy(Copydatas, Linedetails.Linecopy);
                    for (j = 0; j < Linedetails.Linecopy; j++) {
                        newdatacopied = angular.copy(Copydatas);
                        newdatacopied.id = newdatacopied.id + 1;
                        var Newwinval = $scope.Updatewindowval(newdatacopied.WindowLocation, newdatacopied.RoomName, newdatacopied.RoomLocation);

                        newdatacopied.WindowLocation = Newwinval.windowval;
                        newdatacopied.RoomName = Newwinval.Roomnameval;
                        newdatacopied.Width = "";
                        newdatacopied.Height = "";
                        newdatacopied.FranctionalValueWidth = ""; newdatacopied.FranctionalValueHeight = "";
                        newdatacopied.NotSave = true;
                        delete (newdatacopied.uid);
                        //
                        newdatacopied.uid = newdatacopied.id + 1;
                        //grid.dataSource.add(newdatacopied);
                        if (j == 0) {
                            $scope.firstCopiedLineOffset = Arrayofdatas.length - 1;
                        }

                        if (newdatacopied['fileName']) {
                            //$scope.copyPhotoData = angular.copy($scope.selectedFiles);
                            for (var i = 0; i < $scope.selectedFiles.length; i++) {
                                if (idbackup == $scope.selectedFiles[i]['id'] && newdatacopied['fileName'] == $scope.selectedFiles[i]['name']) {
                                    $scope.selectedFiles.push({ id: newdatacopied['id'], name: $scope.selectedFiles[i]['name'], base64: $scope.selectedFiles[i]['base64'], uid: newdatacopied['id'] });

                                }
                            }
                            //$scope.selectedFiles.push($scope.copyPhotoData);
                        }

                        Arrayofdatas.push(newdatacopied);
                        Copydatas = newdatacopied;


                    }
                }
                //loop other data in list and increment id after copied data.
                for (z = 0; z < dataSource.length; z++) {
                    if ([z] > Offsets) {

                        LastId = Arrayofdatas.length - 1;
                        Lastrecord = Arrayofdatas[LastId];
                        dataSource[z].id = Lastrecord.id + 1;
                        Arrayofdatas.push(dataSource[z]);
                    }
                }
                //grid.setDataSource(Arrayofdatas);
                //grid.dataSource.page(1); //added to resolve Nan issue in grid

                $scope.Disableotherfields = true;
                $scope.MultipleEditMode = false;
                setBackToOrginalState(); //this is needed since the copy & times need to be emptied once it's done
                // update the grid data source
                var loading = $("#loading");
                $(loading).css("display", "block");
                setTimeout(function () {
                    grid.setDataSource(Arrayofdatas);
                    setTimeout(function () {
                        $scope.Changenegativenumber();
                        // scrolling to copied row
                        var widthId = '#width' + ($scope.firstCopiedLineOffset + 1);
                        $(widthId).focus();
                        $scope.actionFlag = false;
                    }, 500)
                    grid.dataSource.page(1);
                }, 100);

                // $scope.saveMeasurements(true, Arrayofdatas,false,true);
                //}
                //}
            }

        }
        //end---
        //update and return the window location & roomname value.
        $scope.Updatewindowval = function (Windowvalue, RoomName, RoomLocation) {
            var Appendval = ""; var Changeval = "";
            var Changeroomnam = ""; var Abbrnam = ""; var Newwinval = "";
            if (Windowvalue) {
                var Winvalue = Windowvalue;
                var Value = Winvalue.split(" ");
                Changeval = parseInt(Value[1]);
                Changeval = Changeval + 1;

                if (Value[0] == "Window") {
                    Appendval = "W"
                }
                else Appendval = "D"

                Newwinval = Value[0] + " " + Changeval + " " + "/" + " " + Appendval + Changeval.toString();
                //return Newwinval;
            }
            if (RoomName && RoomLocation) {
                var Roomnam = RoomName; var Roomloc = RoomLocation;
                var roomDetails = $scope.RoomLocationDrop;
                for (var i = 0; i < roomDetails.length; i++) {
                    if (roomDetails[i].Value == Roomloc) {
                        Abbrnam = roomDetails[i].AbbrValue;
                    }
                }
                Changeroomnam = Abbrnam + " " + "-" + " " + Appendval + Changeval.toString();
            }
            var result = {};
            result.windowval = Newwinval;
            result.Roomnameval = Changeroomnam;
            return (result);
        }
        //end--

        //?mark pop-up code
        $scope.bringTooltipPopup = function () {
            $(".statusCanvas").show();
        }
        //-- end

        //make notsave flag as false
        function updateGridValue(arrayGrid) {

            for (var i = 0; i < arrayGrid.length; i++) {
                arrayGrid[i].editable = true;
                if (arrayGrid[i].NotSave == true) {
                    arrayGrid[i].NotSave = false;
                }
            }
            return arrayGrid;
        }

        // remove the not save measurement lines and reorder the id
        function removeUnsavedLines(arrayGrid) {
            var updatedArray = []; var j = 1;
            for (var i = 0; i < arrayGrid.length; i++) {
                //if (!arrayGrid[i].NotSave) {
                arrayGrid[i]['id'] = j;
                j++;
                updatedArray.push(arrayGrid[i]);
                //}
            }
            return updatedArray;
        }
        //--end

        //make the fields as normal and emty the text box values
        function setBackToOrginalState() {

            $scope.Multiple.Linenum = "";
            $scope.Multiple.Linecopy = "";
            $("#linenumber").removeClass("required_field");
            $("#linecopy").removeClass("required_field");

            $('#linenumber').css('border-color', '');
            $('#linecopy').css('border-color', '');
        }
        //--end
        //--enda

        //Check the notsave is true/false in the list
        function checkFlagValue(gridArray) {
            var checkFlag = false;
            for (var r = 0; r < gridArray.length; r++) {
                if (gridArray[r]['NotSave'] || gridArray[r]['editable']) {
                    checkFlag = true;
                    return checkFlag;
                }
            }
        }
        //--end

        //to disable the copy buttons when no data is present.
        $scope.DisableCopyFields = false;
        $scope.GetDataLength = function () {
            var grid = $("#gridEditMeasurements").data("kendoGrid");
            var dataSource = grid.dataSource._data;
            if (dataSource.length == 0) {
                $scope.DisableCopyFields = true;
            }
            else {
                $scope.DisableCopyFields = false;
            }
        }
        //end
        ////$scope.GetDataLength();


        // resize event handling
        $(window).resize(function () {
            if ($scope.EditMode) {
                $scope.MeasurementEdit();
            }
        });
        //end.
        ////Upload Measurements
        $scope.SelectFile = function (e) {
            //$window.alert(e.target.files[0].name);
            var loading = $("#loading");
            var fileInput = e.target.files[0];
            var regex = /([a-zA-Z0-9\s_\\.\-\(\):])+(.csv)$/;//regex for Checking valid files csv of txt 

            if (regex.test(fileInput.name.trim()) == false) {
                alert("Incorrect file. Please choose correct file to upload...");
                return false;
            }
            var csvData;
            $(loading).css("display", "block");
            readFile = function () {
                var reader = new FileReader();
                reader.onload = function () {
                    csvData = reader.result;
                    var exactMeasurementLine = csvData.split(/\r\n|\n/);
                    //var exactMeasurementLine = [];
                    //for (var i = 0; i < measurementLineList.length; i++) {
                    //    if (measurementLineList[i].startsWith('"[') || measurementLineList[i].startsWith('"_')) {
                    //        exactMeasurementLine.push(measurementLineList[i]);
                    //    }
                    //}
                    //console.log(exactMeasurementLine);
                    if (exactMeasurementLine && exactMeasurementLine.length > 1) {
                        var measurementLineArray = [];
                        var headerList = (exactMeasurementLine[0]).split(',');
                        // compare the listed column is present or not                                             
                        if ($($scope.measurementKeyList).not(headerList).length !== 0) {
                            $(loading).css("display", "none");
                            alert("Incorrect file. Please choose correct file to upload...");
                            return;
                        }
                        //$.each(headerList, function (key, value) {

                        //});
                        // setting the value into object
                        for (var i = 1; i < exactMeasurementLine.length; i++) {
                            var obj = {};
                            var dataList = (exactMeasurementLine[i]).split(',');
                            for (var j = 0; j < dataList.length; j++) {
                                if (dataList[j] == "null") {
                                    dataList[j] = "";
                                }
                                obj[headerList[j]] = dataList[j];
                            }
                            measurementLineArray.push(obj);
                        }
                        // set the data source to grid
                        var existingData = $("#gridEditMeasurements").data("kendoGrid").dataSource.data();
                        var dataLength = existingData.length;
                        var mergedDataList = existingData;
                        for (var i = 0; i < measurementLineArray.length; i++) {
                            measurementLineArray[i]['id'] = dataLength;
                            measurementLineArray[i]['NotSave'] = false;
                            measurementLineArray[i]['editable'] = true;
                            mergedDataList.push(measurementLineArray[i]);
                            dataLength++;
                        }
                        //console.log(mergedDataList)                        
                        $('#searchBox').val('');
                        $("#gridEditMeasurements").data("kendoGrid").setDataSource(mergedDataList);
                        $("#gridEditMeasurements").data("kendoGrid").dataSource.page(1);
                    } else {
                        alert("Empty file. Please select the correct file to upload...");
                    }
                    $(loading).css("display", "none");
                    //console.log(measurementLineArray);
                };
                // start reading the file. When it is done, calls the onload event defined above.
                reader.readAsBinaryString(fileInput);
            }();
        };
        /////Download Measurements
        $scope.DownloadMeasurements = function () {
            var mesu = $scope.MeasurementHeader;
            var mesaData = $("#gridEditMeasurements").data("kendoGrid").dataSource.data();
            var fileTitle = $scope.AccountName + '_' + $scope.InstallationAddress + '_' + moment().format('MM/DD/YYYY hh:mm A')// "measurements";
            var items = mesaData
            if (mesaData && mesaData.length !== 0) {
                // Convert Object to JSON
                var jsonObject = JSON.stringify(items);

                //var csv = convertToCSV1(items);
                var csv = composeString(items);
                var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

                var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
                if (navigator.msSaveBlob) { // IE 10+
                    navigator.msSaveBlob(blob, exportedFilenmae);
                } else {
                    var link = document.createElement("a");
                    if (link.download !== undefined) { // feature detection
                        // Browsers that support HTML5 download attribute
                        var url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", exportedFilenmae);
                        link.style.visibility = 'hidden';
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                }
            } else {
                alert("There is no lines to download");
            }
        }
        //function convertToCSV(objArray) {
        //    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        //    var str = '';

        //    for (var i = 0; i < array.length; i++) {
        //        var line = '';
        //        for (var index in array[i]) {
        //            if (line != '') line += ','

        //            line += array[i][index];
        //        }

        //        str += line + '\r\n';
        //    }

        //    return str;
        //}

        function composeString(items) {
            var objString = '';
            for (var i = 0; i < $scope.measurementKeyList.length; i++) {
                objString += $scope.measurementKeyList[i];
                if (i != $scope.measurementKeyList.length - 1) {
                    objString += ",";
                }
            }

            for (var i = 0; i < items.length; i++) {
                objString += "\n";
                for (var j = 0; j < $scope.measurementKeyList.length; j++) {
                    //if (items[i][$scope.measurementKeyList[j]])
                    //    objString += '"';
                    objString += items[i][$scope.measurementKeyList[j]];
                    //if (items[i][$scope.measurementKeyList[j]])
                    //    objString += '"';
                    if (j != $scope.measurementKeyList.length - 1) {
                        objString += ",";
                    }
                }
            }
            console.log(objString);
            return objString;
        }

        function convertToCSV1(objArray) {
            const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
            let str = `${Object.keys(array[0]).map(value => `"${value}"`).join(",")}` + '\r\n';

            return array.reduce((str, next) => {
                str += `${Object.values(next).map(value => `"${value}"`).join(",")}` + '\r\n';
                return str;
            }, str);
        }

        // method

        // "id","RoomLocation",
        // " value " + 


    }
]);