﻿/***************************************************************************\
Module Name:  Disclaimer Controller  .js - AngularJS file
Project: HFC
Created on: 21 December 2017 Thursday
Created By:
Copyright:
Description: Disclaimer Controller
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app.controller('VendorInvoiceController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http', 'NavbarService',
                    'HFCService', 'kendotooltipService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http, NavbarService,
        HFCService, kendotooltipService) {

        var ctrl = this;
        $scope.InvoiceNumber = $routeParams.invoiceNumber;
        $scope.InvoiceDetails = {};
        $scope.InvoiceHistoryDetails = {};
        $scope.InvoiceDashboardList = [];
        $scope.SummaryList = {};
        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableOperartionsVendorPPVList();

        var offsetvalue = 0;
        $scope.Permission = {};
        var VendorInvoicepermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'VendorInvoice')
        $scope.Permission.VendorInvoiceList = VendorInvoicepermission.CanRead; //$scope.HFCService.GetPermissionTP('Procurement Dashboard List').CanAccess;
        $scope.Permission.VendorInvoiceDetails = VendorInvoicepermission.CanRead; //$scope.HFCService.GetPermissionTP('Procurement Dashboard Details').CanAccess;
        $scope.Permission.VendorInvoiceAdd = VendorInvoicepermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Procurement Dashboard').CanAccess;
        $scope.Permission.VendorInvoiceedit = VendorInvoicepermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Procurement Dashboard').CanAccess;
        $scope.Permission.CreateVendorRevisedInvoice = VendorInvoicepermission.SpecialPermission[0].CanAccess;
        HFCService.setHeaderTitle("Vendor PPV List #");
        $scope.SelectVendorId = 0;
        $scope.VendorDropdown = {
            VendorID: '',
            Id: 'Today'
        }
        $scope.PPVfilter = 1;
        //for displaying loader
        $scope.modalState = {
            loaded: true
        }
        //for setting options to kendo numeric textbox
        $scope.options = {
            format: "c", decimals: 2, spinners: false, min: 0, placeholder: "$0.00",
            change: function () {
                //for checking whether all the kendo numeric textbox is initialized
                $scope.VendorInvoice.AdjTotal = $scope.VendorInvoice.AdjSubtotal - Number($scope.VendorInvoice.AdjDiscount) + Number($scope.VendorInvoice.AdjProcessingFee) + Number($scope.VendorInvoice.AdjMiscCharge) + Number($scope.VendorInvoice.AdjTax) + Number($scope.VendorInvoice.AdjFreight);
                $scope.TempAdjTotal = HFCService.CurrencyFormat($scope.VendorInvoice.AdjTotal);
                $scope.$apply();
            }
        }

        $scope.$watch('modalState.loaded', function () {
            var loadingElement = document.getElementById("loading");

            if (!$scope.modalState.loaded)
                loadingElement.style.display = "block";
            else
                loadingElement.style.display = "none";
        });

        if (!$window.location.href.toUpperCase().includes('VENDORINVOICEPPVLIST'))
            $scope.modalState.loaded = false;

        $scope.InitialLoad = function () {
            if ($window.location.href.toUpperCase().includes('VENDORINVOICEPPVLIST')) {
                //to get active vendor to drop down
                $scope.Vendor_Name = {
                    optionLabel: {
                        Name: "All",
                        VendorID: ""
                    },
                    dataTextField: "Name",
                    dataValueField: "VendorID",
                    filter: "contains",
                    valuePrimitive: true,
                    autoBind: true,
                    change: function (data) {
                        var val = this.value();
                        $scope.VendorChange(val)
                    },
                    dataSource: {
                        transport: {
                            read: {
                                url: "/api/Procurement/0/VendorNameList",
                            }
                        }
                    },
                };

                //to get Dashboard Filter
                $http.get('/api/Procurement/0/Type_DateTime?Tableid=' + 4).then(function (data) {
                    $scope.DateTimeDropdown = data.data;
                    for (var i = 0; i < $scope.DateTimeDropdown.length; i++) {
                        if ($scope.DateTimeDropdown[i]['Name'] == 'Last 30 days (rolling)') {
                            $scope.DateRangeId = $scope.DateTimeDropdown[i]['Id'];
                            $scope.Id = $scope.DateTimeDropdown[i]['Id'];
                            $scope.FilterChange($scope.DateTimeDropdown[i]['Id']);
                        }
                    }
                })
            }
            else if ($window.location.href.toUpperCase().includes('VENDORINVOICE')) {
                $http.get('/api/VendorInvoice/0/Getdetails?invoiceNumber=' + $scope.InvoiceNumber).then(function (response) {
                    $scope.VendorInvoice = response.data;
                    $scope.GetMPORelatedIds();
                    $scope.InvoiceDetails = response.data.VendorInvoiceDetail;
                    $scope.GetInvoiceView();
                    $scope.modalState.loaded = true;
                }, function (error) {
                    HFC.DisplayAlert(error);
                    $scope.modalState.loaded = true;
                });
            }
            else if ($window.location.href.toUpperCase().includes('REVISEDINVOICE')) {
                $http.get('/api/VendorInvoice/0/Getdetailswithhistory?invoiceNumber=' + $scope.InvoiceNumber).then(function (response) {
                    $scope.VendorInvoice = response.data;
                    $scope.TempAdjSubtotal = HFCService.CurrencyFormat($scope.VendorInvoice.AdjSubtotal);
                    $scope.TempAdjTotal = HFCService.CurrencyFormat($scope.VendorInvoice.AdjTotal);
                    $scope.InvoiceDetails = response.data.VendorInvoiceDetail;
                    if (response.data.VendorInvoiceChangeHistory)
                        $scope.InvoiceHistoryDetails = response.data.VendorInvoiceChangeHistory;
                    $scope.GetInvoiceHistory();
                    $scope.GetRevisedInvoice();
                    //binding adj amount initially
                    if (!$window.location.href.toUpperCase().includes('REVISEDINVOICEVIEW')) {
                        var historygrid = $("#InvoiceHistory").data("kendoGrid");
                        if (historygrid)
                            historygrid.setDataSource($scope.InvoiceHistoryDetails);
                    }
                    $scope.modalState.loaded = true;
                }, function (error) {
                    HFC.DisplayAlert(error);
                    $scope.modalState.loaded = true;
                });
            }
        }

        $scope.InitialLoad();

        $scope.MPORelatedIds = {
            VendorAckIds: [],
            ShipmentIds: [],
            VendorInvoiceIds: [],
            PurchaseOrderId: '',
        };

        $scope.GetMPORelatedIds = function () {
            $http.get('/api/PurchaseOrders/' + $scope.VendorInvoice.PurchaseOrderId + '/GetMPORelatedIds').then(function (data) {
                var MPORelatedIds = data.data.data;
                if (MPORelatedIds.length > 0) {
                    for (var i = 0; i < MPORelatedIds.length; i++) {
                        if (MPORelatedIds[i].vendorReference) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.VendorAckIds))
                                $scope.MPORelatedIds.VendorAckIds.push({ vendorReference: MPORelatedIds[i].vendorReference, PurchaseOrderId: MPORelatedIds[i].PurchaseOrderId });
                            else if ($scope.MPORelatedIds.VendorAckIds.length > 0) {
                                var exists = $scope.MPORelatedIds.VendorAckIds.some(el => el.vendorReference === MPORelatedIds[i].vendorReference);
                                if (!exists)
                                    $scope.MPORelatedIds.VendorAckIds.push({ vendorReference: MPORelatedIds[i].vendorReference, PurchaseOrderId: MPORelatedIds[i].PurchaseOrderId });
                            }
                        }
                        if (MPORelatedIds[i].ShipmentId) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.ShipmentIds))
                                $scope.MPORelatedIds.ShipmentIds.push({ ShipmentId: MPORelatedIds[i].ShipmentId, ShipNoticeId: MPORelatedIds[i].ShipNoticeId });
                            else if ($scope.MPORelatedIds.ShipmentIds.length > 0) {
                                var exists = $scope.MPORelatedIds.ShipmentIds.some(el => el.ShipmentId === MPORelatedIds[i].ShipmentId);
                                if (!exists)
                                    $scope.MPORelatedIds.ShipmentIds.push({ ShipmentId: MPORelatedIds[i].ShipmentId, ShipNoticeId: MPORelatedIds[i].ShipNoticeId });
                            }
                        }

                        if (MPORelatedIds[i].VendorInvoiceId) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.VendorInvoiceIds))
                                $scope.MPORelatedIds.VendorInvoiceIds.push({ VendorInvoiceId: MPORelatedIds[i].VendorInvoiceId, InvoiceNumber: MPORelatedIds[i].InvoiceNumber });
                            else if ($scope.MPORelatedIds.VendorInvoiceIds.length > 0) {
                                var exists = $scope.MPORelatedIds.VendorInvoiceIds.some(el => el.InvoiceNumber === MPORelatedIds[i].InvoiceNumber);
                                if (!exists)
                                    $scope.MPORelatedIds.VendorInvoiceIds.push({ VendorInvoiceId: MPORelatedIds[i].VendorInvoiceId, InvoiceNumber: MPORelatedIds[i].InvoiceNumber });
                            }
                        }
                        //$scope.mporelatedids.vendorackids = MPORelatedIds.find(mpo => mpo.vendorreference);
                    }
                    setTimeout(function () {
                        if ($scope.MPORelatedIds.VendorInvoiceIds) {
                            $("#submenu-dropdown li").find('a:contains(' + $scope.InvoiceNumber + ')').addClass('activeLi');
                        }
                    }, 100);
                }
            });
        }

        //for getting dashboard list
        $scope.GetVIdashboardList = function () {
            $scope.VendorInvoicePPVListView();
            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
            $http.get('/api/VendorInvoice/' + $scope.SelectVendorId + '/VendorInvoiceDashboard?ppvFilter=' + $scope.PPVfilter + '&dateFilter=' + $scope.Id).then(function (response) {
                $scope.InvoiceDashboardList = response.data.result;
                if ($scope.PPVfilter != 2)
                    $scope.SummaryList = response.data.SummaryDashboard;
                else
                    $scope.SummaryList = [];
                //for currency format
                if ($scope.SummaryList) {
                    for (i = 0; i < $scope.SummaryList.length; i++)
                        $scope.SummaryList[i].VarianceAmount = HFCService.CurrencyFormat($scope.SummaryList[i].VarianceAmount);
                }
                var VIlistgrid = $("#VendorInvoicePPVList").data("kendoGrid");
                if (VIlistgrid) {
                    var VIlistdatasource = new kendo.data.DataSource({
                        data: $scope.InvoiceDashboardList,
                        pageSize: 25,
                        schema: {
                            model: {
                                fields: {
                                    Date: { type: 'date', editable: false },
                                    InvoiceNumber: {editable: false },
                                    VendorName: { editable: false },
                                    PICVpo: { type: 'number', editable: false },
                                    OrderNumber: { type: 'number', editable: false },
                                    SideMark: { editable: false },
                                    Quantity: { type: 'number', editable: false },
                                    QuotedProductCost: { type: 'number', editable: false },
                                    InvoicedProductCost: { type: 'number', editable: false },
                                    OtherCharges: { type: 'number', editable: false },
                                    InvoiceTotal: { type: 'number', editable: false },
                                    PPV: { type: 'boolean', editable: false },
                                    VarianceAmount: { type: 'number', editable: false },
                                }
                            }
                        },
                    });
                    VIlistgrid.setDataSource(VIlistdatasource);
                    VIlistgrid.dataSource.page(1);
                    //for displaying search results on datachange
                    $scope.gridSearch();
                }
                //for showing scroll bar in kendo grid
                setTimeout(function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 295);
                }, 100);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }, function (error) {
                HFC.DisplayAlert(error);
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            });
        }

        //on selecting drop-down value(vendor)
        $scope.VendorChange = function (id) {
            if (id === null || id === '' || id === undefined) {
                $scope.SelectVendorId = 0;
            } else {
                $scope.SelectVendorId = id;
            }
            $scope.GetVIdashboardList();
        }

        //on selecting drop-down value(vendor)
        $scope.FilterChange = function (id) {
            if (id === null || id === '' || id === undefined) {
                $scope.Id = '';
            } else {
                $scope.Id = id;
            }
            $scope.GetVIdashboardList();
            return;
        }

        //on clicking the checkboxes values to be passed
        $scope.Check = function (value) {
            $scope.PPVfilter = value
            $scope.GetVIdashboardList();
        }

        $scope.EnableRevisedinvoice = function () {
            $http.get('/api/VendorInvoice/0/EnableRevisedInvoice?invoiceNumber=' + $scope.InvoiceNumber).then(function (response) {
                window.location.href = "#!/revisedinvoiceview/" + $scope.InvoiceNumber;
            });
        }

        $scope.Revisedinvoice = function () {
            window.location.href = "#!/revisedinvoice/" + $scope.InvoiceNumber;
        }

        $scope.Revisedinvoiceview = function () {
            window.location.href = "#!/revisedinvoiceview/" + $scope.InvoiceNumber;
        }

        $scope.GetInvoiceView = function () {
            $scope.GridEditable = false;
            $scope.VendorInvoiceview = {
                dataSource: {
                    transport: {
                        read: function (e) {
                            var viDetails = $scope.InvoiceDetails;
                            //var dupes = {};
                            //var singles = [];
                            //if (viDetails.length > 0) {
                            //    $.each(viDetails,
                            //        function (i, el) {
                            //            if (!dupes[el.POLineNum]) {
                            //                dupes[el.POLineNum] = true;
                            //                singles.push(el);
                            //            }
                            //        });
                            //}
                            e.success(viDetails);
                        },
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    batch: true,
                    schema: {
                        model: {
                            id: "VendorInvoiceDetailId",
                            fields: {
                                VendorInvoiceId: { type: 'number', editable: false },
                                VendorInvoiceDetailId: { type: 'number', editable: false },
                                POLineNum: { type: 'number', editable: false },
                                ProductNumber: { type: 'number', editable: false },
                                Item: { editable: false },
                                ItemDescription: { editable: false },
                                QTY: { type: 'number', editable: false },
                                Cost: { type: 'number', editable: false },
                                Discount: { type: 'number', editable: false },
                                Amount: { type: 'number', editable: false },
                                Comment: { editable: false },
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "POLineNum",
                        title: "PO Line #",
                        width: "50px",
                        template: function (dataItem) {
                            if (dataItem.POLineNum !== null) {
                                return "<span class='Grid_Textalign'>" + dataItem.POLineNum + "</span>";
                            } else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "ProductNumber",
                        title: "Product Number",
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.ProductNumber !== null) {
                                return "<span class='Grid_Textalign'>" + dataItem.ProductNumber + "</span>";
                            } else {
                                return "";
                            }
                        },
                    },
                    {
                        field: "Item",
                        title: "Item",
                        width: "100px",
                    },
                    {
                        field: "ItemDescription",
                        title: "Item Description",
                        width: "350px",
                        template: function (dataItem) {
                            return "<p>" + dataItem.ItemDescription + "</p>";
                        },
                    },
                    {
                        field: "QTY",
                        title: "QTY",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.QTY, 'number') + "</span>";
                        },
                    },
                     {
                         field: "Cost",
                         title: "Cost",
                         width: "100px",
                         template: function (dataItem) {
                             return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</span>";
                         },
                     },
                    {
                        field: "Discount",
                        title: "Discount",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Discount) + "</span>";
                        },
                    },
                    {
                        field: "Amount",
                        title: "Amount",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                        },
                    },
                     {
                         field: "Comment",
                         title: "Comment",
                         width: "150px",
                     }
                ],
                editable: false,
                resizable: true,
                noRecords: { template: "No records found" },
                scrollable: true,
            };
        };

        //revised invoice
        $scope.GetRevisedInvoice = function () {
            $scope.GridEditable = true;
            $scope.RevisedInvoice = {
                dataSource: {
                    transport: {
                        read: function (e) {
                            var viDetails = $scope.InvoiceDetails;
                            e.success(viDetails);
                        },
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    batch: true,
                    schema: {
                        model: {
                            id: "VendorInvoiceId",
                            fields: {
                                VendorInvoiceId: { type: "number", editable: false },
                                VendorInvoiceDetailId: { type: 'number', editable: false },
                                POLineNum: { type: "number", editable: false },
                                ProductNumber: { type: "number", editable: false },
                                Item: { editable: false },
                                ItemDescription: { editable: false },
                                QTY: { type: "number", editable: false },
                                Cost: { type: "number", editable: false },
                                Discount: { type: "number", editable: false },
                                Amount: { type: "number", editable: false },
                                Comment: { editable: false },
                                AdjCost: { type: 'number', editable: true },
                                AdjDiscount: { type: 'number', editable: true },
                                AdjAmount: { type: 'number', editable: true },
                                AdjComments: { editable: true },
                            }
                        }
                    },
                    change: function (e) {
                        if (e.action === "itemchange") {
                            if (e.field == "AdjCost" || e.field == "AdjDiscount") {
                                var AdjCost = e.items[0].AdjCost;
                                var AdjDiscount = e.items[0].AdjDiscount;
                                var GridAdjAmount = AdjCost - AdjDiscount;
                                var data = e.items[0]
                                data.set("AdjAmount", GridAdjAmount);
                                $scope.VendorInvoice.AdjSubtotal = 0;
                                var grid = $("#RevisedInvoice").data("kendoGrid").dataSource;
                                for (i = 0; i < grid._data.length; i++) {
                                    if (e.items[0].VendorInvoiceDetailId == grid._data[i].VendorInvoiceDetailId)
                                        $scope.VendorInvoice.AdjSubtotal = $scope.VendorInvoice.AdjSubtotal + GridAdjAmount;
                                    else
                                        $scope.VendorInvoice.AdjSubtotal = $scope.VendorInvoice.AdjSubtotal + grid._data[i].AdjAmount;
                                }
                                $scope.TempAdjSubtotal = HFCService.CurrencyFormat($scope.VendorInvoice.AdjSubtotal);
                                $scope.VendorInvoice.AdjTotal = $scope.VendorInvoice.AdjSubtotal - Number($scope.VendorInvoice.AdjDiscount) + Number($scope.VendorInvoice.AdjProcessingFee) + Number($scope.VendorInvoice.AdjMiscCharge) + Number($scope.VendorInvoice.AdjTax) + Number($scope.VendorInvoice.AdjFreight);
                                $scope.TempAdjTotal = HFCService.CurrencyFormat($scope.VendorInvoice.AdjTotal);
                                $scope.$apply();
                            }
                            $("#RevisedInvoice").data("kendoGrid").bind("dataBinding", function (e) {
                                e.preventDefault();
                            });

                        }
                    },
                },
                columns: [
                    {
                        field: "POLineNum",
                        title: "PO Line #",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.POLineNum + "</span>";
                        },
                    },
                    {
                        field: "ProductNumber",
                        title: "Product Number",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.ProductNumber + "</span>";
                        },
                    },
                    {
                        field: "Item",
                        title: "Item",
                        width: "100px",
                    },
                    {
                        field: "ItemDescription",
                        title: "Item Description",
                        width: "350px",
                        template: function (dataItem) {
                            return "<p>" + dataItem.ItemDescription + "</p>";
                        },
                    },
                    {
                        field: "QTY",
                        title: "QTY",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.QTY, 'number') + "</span>";
                        },
                    },
                     {
                         field: "Cost",
                         title: "Cost",
                         width: "100px",
                         template: function (dataItem) {
                             return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</span>";
                         },
                     },
                    {
                        field: "Discount",
                        title: "Discount",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Discount) + "</span>";
                        },
                    },
                    {
                        field: "Amount",
                        title: "Amount",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                        },
                    },
                     {
                         field: "Comment",
                         width: "150px",
                         title: "Vendor Comments",
                     },
                     {
                         field: "",
                         title: "",
                         width: "60px",
                         template:
                             ' <ul>' +
                                 '<li class="dropdown note1 ad_user"><div class="dropdown"><button  class="btn btn-default dropdown-toggle qlEllipsis" ng-click="qlLineClicked($event)" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button>' +
                                 '<ul class="dropdown-menu pull-right">  ' +
                                 '<li><a href="javascript:void(0)"  ng-click="EditRowDetail(dataItem)">Edit</a></li>' +
                                 '</ul></div></li></ul>'
                     }
                ],
                detailInit: detailinitParent,
                resizable: true,
                noRecords: { template: "No records found" },
                scrollable: true,
                dataBound: function (e) {
                    if ($window.location.href.toUpperCase().includes('REVISEDINVOICEVIEW')) {
                        var grid = $("#RevisedInvoice").data("kendoGrid");
                        grid.hideColumn("");
                    }
                }
            };
        };

        function detailinitParent(e) {
            var grdId = "grd" + e.data.VendorInvoiceDetailId;
            $("<div style='margin-right:20px;' id = '" + grdId + "'/>").appendTo(e.detailCell).kendoGrid({
                dataSource: [e.data],
                columns: [
                    {
                        field: "AdjCost", title: "Adj Cost", format: "{0:c}", width: "80px",
                        editor: function (container, options) {
                            $(' <input id="AdjCost" data-bind="value:AdjCost" name="AdjCost"/>')
                            .appendTo(container).kendoNumericTextBox({
                                format: "$#.00",
                                spinners: false,
                                min: 0,
                            });
                        },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AdjCost) + "</span>";
                        },
                    },
                    {
                        field: "AdjDiscount", title: "Adj Discount", width: "80px", template: '#=kendo.format("{0:p}", AdjDiscount / 100)#',
                        editor: function (container, options) {
                            $('<input id="AdjDiscount" name="AdjDiscount" data-bind="value:' + options.field + '"/>')
                            .appendTo(container).kendoNumericTextBox({
                                format: "$#.00",
                                decimals: 2,
                                min: 0, spinners: false,
                            });
                        },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AdjDiscount) + "</span>";
                        },
                    },
                    {
                        field: "AdjAmount", title: "Adj Amount", format: "{0:c}", width: "80px",
                        editor: function (container, options) {
                            $('<input id="AdjAmount" name="AdjAmount" data-bind="value:' + options.field + '" style="background: #fff !important; border: none !important;" disabled/>')
                            .appendTo(container).kendoNumericTextBox({
                                format: "c2",
                                decimals: 2,
                                spinners: false,
                            });
                        },
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.AdjAmount) + "</span>";
                        },
                    },
                    {
                        field: "AdjComments", title: "Adj Comments", width: "250px",
                    },
                ],
                editable: "inline",
                batch: true,
            });
        }

        //for enabling edit
        $scope.EditRowDetail = function (e) {
            var grid = $("#RevisedInvoice").data("kendoGrid");
            var link = grid.tbody.find("tr[data-uid='" + e.uid + "']").find("td.k-hierarchy-cell .k-icon");
            var row = grid.tbody.find("tr[data-uid='" + e.uid + "']");
            grid.expandRow(row);

            var grdId = "#grd" + e.VendorInvoiceDetailId;
            var childgrid = $(grdId).data("kendoGrid");
            if (childgrid) {
                childgrid.editRow(e);
            }
        }

        //for saving revised invoice
        $scope.SaveInvoice = function () {
            var grid = $("#RevisedInvoice").data("kendoGrid").dataSource;
            for (i = 0; i < grid._data.length; i++) {
                $scope.VendorInvoice.VendorInvoiceDetail[i].AdjCost = grid._data[i].AdjCost;
                $scope.VendorInvoice.VendorInvoiceDetail[i].AdjDiscount = grid._data[i].AdjDiscount;
                $scope.VendorInvoice.VendorInvoiceDetail[i].AdjAmount = grid._data[i].AdjCost - grid._data[i].AdjDiscount;
                $scope.VendorInvoice.VendorInvoiceDetail[i].AdjComments = grid._data[i].AdjComments;
            }
            var data = $scope.VendorInvoice;
            $scope.modalState.loaded = false;
            $http.post('/api/VendorInvoice/0/SaveVendorInvoiceDetail', data).then(function (response) {
                window.location.href = "#!/revisedinvoiceview/" + $scope.InvoiceNumber;
                $scope.modalState.loaded = true;
            }, function (error) {
                HFC.DisplayAlert(error);
                $scope.modalState.loaded = true;
            });
        }

        //invoice change history
        $scope.GetInvoiceHistory = function () {
            $scope.InvoiceHistory = {
                dataSource: {
                    transport: {
                        read: function (e) {
                            var viDetails = $scope.InvoiceHistoryDetails;
                            e.success(viDetails);
                        },
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    batch: true,
                    schema: {
                        model: {
                            id: "VendorInvoiceId",
                            fields: {
                                CreatedOn: { editable: false },
                                PersonName: { editable: false },
                                POLineNumber: { type: "number", editable: false },
                                Change: { editable: false },
                                OrgValue: { editable: false },
                                AdjValue: { editable: false },
                            }
                        }
                    },
                },
                columns: [
                    {
                        field: "CreatedOn",
                        title: "Date",
                        template: function (dataItem) {
                            return "<span>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + "</span>";
                        },
                    },
                    {
                        field: "PersonName",
                        title: "User",
                    },
                    {
                        field: "POLineNumber",
                        title: "Line#",
                        template: function (dataItem) {
                            if (dataItem.POLineNumber)
                                return "<span class='Grid_Textalign'>" + dataItem.POLineNumber + "</span>";
                            else
                                return "";
                        },
                    },
                    {
                        field: "Change",
                        title: "Change",
                    },
                    {
                        field: "OrgValue",
                        title: "Org Value",
                        template: function (dataItem) {
                            if (dataItem.Change == "Comments") {
                                return dataItem.OrgValue != null ? dataItem.OrgValue : "";
                            }
                            else {
                                var OrgValue = Number(dataItem.OrgValue);
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(OrgValue) + "</span>";
                            }
                        },
                    },
                     {
                         field: "AdjValue",
                         title: "Adj Value",
                         template: function (dataItem) {
                             if (dataItem.Change == "Comments") {
                                 return dataItem.AdjValue != null ? dataItem.AdjValue : "";
                             }
                             else {
                                 var AdjValue = Number(dataItem.AdjValue);
                                 return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(AdjValue) + "</span>";
                             }
                         },
                     },
                ],
                pageable: {
                    refresh: true,
                    pageSize: 10,
                    pageSizes: [10, 25, 50, 100],
                    buttonCount: 5
                },
                resizable: true,
                noRecords: { template: "No records found" },
                scrollable: true
            };
        };

        //dashboard grid
        $scope.VendorInvoicePPVListView = function () {
            $scope.VendorInvoicePPVList = {
                dataSource: {
                    transport: {
                        read: function (e) {
                            var viList = $scope.InvoiceDashboardList;
                            e.success(viList);
                        },
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    schema: {
                        model: {
                            fields: {
                                Date: { type: 'date', editable: false },
                                InvoiceNumber: {editable: false },
                                VendorName: { editable: false },
                                PICVpo: { type: 'number', editable: false },
                                OrderNumber: { type: 'number', editable: false },
                                SideMark: { editable: false },
                                Quantity: { type: 'number', editable: false },
                                QuotedProductCost: { type: 'number', editable: false },
                                InvoicedProductCost: { type: 'number', editable: false },
                                OtherCharges: { type: 'number', editable: false },
                                InvoiceTotal: { type: 'number', editable: false },
                                PPV: { type: 'boolean', editable: false },
                                VarianceAmount: { type: 'number', editable: false },
                            }
                        }
                    },
                },
                editable: false,
                resizable: true,
                noRecords: { template: "No records found" },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    if (kendotooltipService.columnWidth) {
                        kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                    } else if (window.innerWidth < 1280) {
                        kendotooltipService.restrictTooltip(null);
                    }
                    $scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
                },
                scrollable: true,
                toolbar: kendo.template($('<script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="gridSearch()" type="search" id="searchBox" placeholder="Search" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="SearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></div></script>').html()),
                columns: [
                    {
                        field: "Date",
                        title: "Invoice Date",
                        width:100,
                        template: function (dataItem) {
                            if (dataItem.Date)
                                return "<span>" + HFCService.KendoDateFormat(dataItem.Date) + "</span>";
                            else
                                return "";
                        },
                        filterable: HFCService.gridfilterableDate("date", "VendorInvoiceDateFilterCalender", offsetvalue),
                    },
                    {
                        field: "InvoiceNumber",
                        title: "Invoice #",
                        width: 100,
                        template: function (dataItem) {
                            if (dataItem.InvoiceNumber)
                                return "<span class='Grid_Textalign'><a href='#!/vendorinvoice/" + dataItem.InvoiceNumber + "' style='color: rgb(61,125,139);'>" + dataItem.InvoiceNumber + "</a></span>";
                            else
                                return "";
                        },
                    },
                    {
                        field: "VendorName",
                        title: "Vendor Name",
                        width:150,
                    },
                    {
                        field: "PICVpo",
                        title: "VPO #",
                        width: 100,
                        template: function (dataItem) {
                            if (dataItem.PICVpo)
                                return "<span class='Grid_Textalign'><a href='#!/purchasemasterview/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.PICVpo + "</a></span>";
                            else
                                return "";
                        },
                    },
                    {
                        field: "OrderNumber",
                        title: "Order #",
                        width: 100,
                        template: function (dataItem) {
                            if (dataItem.OrderNumber)
                                return "<span class='Grid_Textalign'><a href='#!/Orders/" + dataItem.OrderID + "' style='color: rgb(61,125,139);'>" + dataItem.OrderNumber + "</a></span>";
                            else
                                return "";
                        },
                    },
                    {
                        field: "SideMark",
                        title: "Sidemark",
                        width: 150,
                    },
                    {
                        field: "Quantity",
                        title: "Total QTY",
                        width: 70,
                        template: function (dataItem) {
                            if (dataItem.Quantity)
                                return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Quantity, 'number') + "</span>";
                            else
                                return "";
                        },
                    },
                    {
                        field: "QuotedProductCost",
                        title: "Quoted Product Cost",
                        width: 100,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.QuotedProductCost) + "</span>";
                        },
                    },
                    {
                        field: "InvoicedProductCost",
                        title: "Invoiced Product Cost",
                        width: 100,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.InvoicedProductCost) + "</span>";
                        },
                    },
                    {
                        field: "OtherCharges",
                        title: "Other Charges",
                        width: 100,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.OtherCharges) + "</span>";
                        },
                    },
                    {
                        field: "InvoiceTotal",
                        title: "Invoiced Total",
                        width: 100,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.InvoiceTotal) + "</span>";
                        },
                    },
                    {
                        field: "PPV",
                        title: "PPV",
                        width: 80,
                        template: function (dataItem) {
                            if (dataItem.PPV == true)
                                return "Y";
                            else
                                return "";
                        },
                        filterable: { messages: { isTrue: "PPV Yes", isFalse: "PPV No" } },
                    },
                    {
                        field: "VarianceAmount",
                        title: "Variance Amount",
                        width: 100,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.VarianceAmount) + "</span>";
                        },
                    }
                ],
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                },
                columnResize: function (e) {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 295);
                    getUpdatedColumnList(e);
                }
            };

        }

        $scope.VendorInvoicePPVList = kendotooltipService.setColumnWidth($scope.VendorInvoicePPVList);

        var getUpdatedColumnList = function (e) {
            kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
        }
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            if ($window.location.href.toUpperCase().includes('VENDORINVOICEPPVLIST'))
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 295);
            window.onresize = function () {
                if ($window.location.href.toUpperCase().includes('VENDORINVOICEPPVLIST')) {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 295);
                    setTimeout(function () {
                        $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 295);
                    }, 100);
                }
            };
        });
        // End Kendo Resizing

        //dashboard filter
        $scope.gridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#VendorInvoicePPVList").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                    {
                        field: "InvoiceNumber",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "VendorName",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "PICVpo",
                        operator: "eq",
                        value: searchValue
                    },
                    {
                        field: "OrderNumber",
                        operator: "eq",
                        value: searchValue
                    },
                    {
                        field: "SideMark",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "Quantity",
                        operator: "eq",
                        value: searchValue
                    },
                ]
            });

        }

        $scope.SearchClear = function () {
            $('#searchBox').val('');
            $("#VendorInvoicePPVList").data('kendoGrid').dataSource.filter({
            });
        }
        //for navigating to purchasemasterview on clicking the link in panelbar
        $("#redirectpurchasemaster").click(function (e) {
            window.location.href = "#!/purchasemasterview/" + $scope.VendorInvoice.PurchaseOrderId;
            e.preventDefault();
            e.stopPropagation();
        });
        //for focusing history grid on click
        $("#history").click(function (e) {
            $("#panelbar2").kendoPanelBar();
            var panelBar2 = $("#panelbar2").data("kendoPanelBar");
            panelBar2.expand($("#historylistPanel"));
            var elmnt = document.getElementById("comment");
            //elmnt.scrollIntoView();
            setTimeout(function () { elmnt.scrollIntoView(); }, 150);
            e.preventDefault();
            e.stopPropagation();
        });
    }
]);

