﻿/***************************************************************************\
Module Name:  Product Controller .js - AngularJS file
Project: HFC
Created on: 02 November 2017 Thursday
Created By:
Copyright:
Description: Product Controller
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app.controller('emailsettingsController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http', 'kendoService',
                    'HFCService', 'NavbarService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http, kendoService,
        HFCService, NavbarService) {

        var ctrl = this;
        HFCService.setHeaderTitle("Email Settings #");

        $scope.HFCService = HFCService;

        $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsGeneralTab();


        $scope.Edit = function () {
            window.location = "#!/EditEmail";
        }
        $scope.submitted = false;

        $scope.Permission = {};
        var EmailSettingspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'EmailSettings')

        $scope.Permission.ListEmail = EmailSettingspermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Email').CanAccess;
        $scope.Permission.EditEmail = EmailSettingspermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Email').CanAccess;


        $scope.updateEmailSettings = function () {
             
            $scope.submitted = true;
            var dto = $scope.emailConfiguredList;
            var dto1 = $scope.TerritoryEmailData;
            var err = $scope.email_settings.$error;
            if ($scope.email_settings.$invalid) {
                return;
            }
            //for (i = 0; i < dto1.length; i++) {
            //    if (dto1[i].FromEmailPersonId == 0)
            //        return;
            //    if (dto1[i].ShipToLocationId == 0)
            //        return;
            //}
            if (!$scope.DisplayTerritoryId)
            {
                $scope.DisplayTerritoryId = 0;
            }

            $http.post('/api/FranchiseSettings/0/UpdateEmailsConfiguration?DisplayTerritoryId=' + $scope.DisplayTerritoryId, dto).then(function (response) {
                
                if (response.data == "Success") {
                    //backToList_save();
                    $http.post('/api/EmailSettings/0/SaveTerritoryDetails', dto1).then(function (response) {
                        
                        if (response.data == "Success") {
                            backToList_save();
                        }
                        else {
                            HFC.DisplayAlert(response.data);
                            return;
                        }
                    });
                }
                else {
                    HFC.DisplayAlert(response.data);
                    return;
                }
            });
        }

        function backToList_save() {
            $scope.email_settings.$setPristine();
            history.go(-1);
        }

        $http.get('/api/FranchiseSettings/0/GetEmailsConfiguration').then(function (data) {
            $scope.emailConfiguredList = data.data;
        });

        $http.get('/api/FranchiseSettings/0/GetDisplayTerritoryId').then(function (data) {
            if (data.data) {
                $scope.DisplayTerritoryId = data.data.TerritoryId;
                $scope.DisplayTerritoryName = data.data.TerritoryName;
            }

        });


        //get Territory details
        $http.get('/api/EmailSettings/0/GetTerritoryEmailDetails').then(function (response) {
            
            var Displayvalue = response.data;
            $scope.TerritoryEmailData = Displayvalue;
            $scope.TerritoryList = response.data;
        });

        //get Ship to Location details
        $http.get('/api/EmailSettings/0/GetTerritoryLocation').then(function (response) {
            
            var shiplocation = response.data;
            $scope.ShipLocationVal = shiplocation;
        })

        //based on shipto location fill address
        $scope.TerreEmail = function (index, value) {
            
            if (value) {
                var addressId;
                var offset = index;
                var Shiplocid = value;
                for (i = 0; i < $scope.ShipLocationVal.length; i++) {
                    if ($scope.ShipLocationVal[i].ShipToLocationId == Shiplocid) {
                        addressId = $scope.ShipLocationVal[i].AddressId;

                    }
                }
                $http.get('/api/EmailSettings/' + addressId + '/GetAddressLocation').then(function (response) {
                     
                    var LocationValue = response.data;
                    $scope.TerritoryEmailData[offset].LocationAddress = LocationValue;
                });
            }
           
        }

        $http.get('/api/lookup/0/GetEmails').then(function (data) {
            $scope.emailList = data.data;
        });

        $scope.Cancel = function () {
            backToList();
        }
        function backToList() {
            history.go(-1);
        }

    }
]);

