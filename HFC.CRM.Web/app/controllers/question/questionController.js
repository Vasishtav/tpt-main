﻿/***************************************************************************\
Module Name:  questionController.js - questionController AngularJS
Project: HFC
Created on: 15 Oct 2017
Created By:
Copyright:
Description: Adding Question for Qualification details
Change History:
Date  By  Description

\***************************************************************************/

'use strict';

app.controller('questionController', [
    '$scope', '$routeParams', '$http', '$window',
    '$location', '$modal', 'PersonService',
    'HFCService', 'FileprintService',
    'QuestionService', 'NavbarService',
    'NoteServiceTP', 'DialogService', 'calendarModalService',

    function (
        $scope, $routeParams, $http, $window,
        $location, $modal, PersonService,
        HFCService, FileprintService,
        QuestionService, NavbarService, NoteServiceTP, dialogService, calendarModalService
) {

        //Question Controller Start
        var ctrl = this;

        // Qualify is part of Sales module so select that as default.
        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesLeadsTab();

        $scope.calendarModalService = calendarModalService;

        $scope.PersonService = PersonService;
        $scope.QuestionService = QuestionService;
        $scope.HFCService = HFCService;
        $scope.LeadStatuseslist = [];
        $scope.PersonService.People = [];
        $scope.FileprintService = FileprintService;
        $scope.NoteServiceTP = NoteServiceTP;
        //For Appointment
        HFCService.GetUrl();


        $scope.Permission = {};
        var Leadpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Lead')
        var ConvertLeadpermission = Leadpermission.SpecialPermission.find(x=>x.PermissionCode == 'ConvertLead').CanAccess;

        $scope.Permission.AddLead = Leadpermission.CanCreate; //HFCService.GetPermissionTP('Add Lead').CanAccess
        $scope.Permission.ConvertLead = ConvertLeadpermission; //HFCService.GetPermissionTP('Convert Lead').CanAccess
        $scope.Permission.QualifyLead = Leadpermission.CanRead; //HFCService.GetPermissionTP('Qualify Lead').CanAccess
        $scope.Permission.QualifyLeadEdit = Leadpermission.CanUpdate; //HFCService.GetPermissionTP('Qualify Lead Edit').CanAccess
        //$scope.Permission.AddEvent = HFCService.GetPermissionTP('Add Event').CanAccess

        $scope.showValidate = false;
        $scope.LeadDispositionlist = [];
        $scope.minDate = new Date();
        $scope.maxDate = new Date('2099/12/31');
        //flag for date validation
        $scope.ValidDate = true;
        $scope.ValidDatelist = [];

        $scope.Lead = {};
        $scope.LeadId = $routeParams.leadId;

        $scope.location = $location.$$path;
        if ($scope.location.match("leadsQualify")) {
            $scope.isDisabled = true;
        }
        else if ($scope.location.match("leadsEditQualify")) {
            $scope.isDisabled = false;
            setTimeout(function () {
                {
                 $(document).click();
                }
            }, 500);
        }
        var LeadId = $routeParams.leadId;
        var LeadName = "";
        var LeadStatusId = 0;
        var DispositionId = 0;


        $scope.modalState = {
            loaded: true
        };
        ///

        $scope.checkk = true;

        // disposition change event handler
        $scope.dispositionChanged = function (event) {
            
            console.log("^&^&^&^&^&^&      ", event);
            if (event) {
                switch (event.Value) {
                    case "7|0|1": {
                        $scope.calendarModalService.setPopUpDetails(true, "Event", null);
                        break;
                    }
                    case "5|8|2":
                    case "5|9|2":
                    case "5|10|2":
                    case "5|11|2":
                    case "5|12|2":
                    case "13|13|0":
                    case "13|14|0": {
                        $scope.calendarModalService.setPopUpDetails(true, "Task", null);
                        break;
                    }
                }
            }
        }

        $scope.$on('$locationChangeStart', function ($event, next, current) {
            if ($scope.fQlfy.$dirty)
                if ($scope.checkk == true)
                    if (current.includes('leadsEditQualify')) {
                        $scope.checkk = false;
                        $event.preventDefault();
                        $scope.redirectto = next.substring(next.indexOf('#!'), next.length);
                        dialogService.confirmDialog();
                    }

        });

        $scope.PersonService.leadredirectCallBack = function () {
            if ($scope.PersonService.clicked == 'yes') {
                
                $scope.SaveQualify(10);
                // $scope.SaveLead(5); // $scope.SaveLead function needs arg and no meaning behind this arg from this place
            }
            else if ($scope.PersonService.clicked == 'no') {
                $window.location.href = $scope.redirectto;
            }
            else {
                $scope.checkk = true;
            }
        }
        ///



        $scope.$watch('modalState.loaded', function () {
            var loadingElement = document.getElementById("loading");

            if (!$scope.modalState.loaded) {
                loadingElement.style.display = "block";
            } else {
                loadingElement.style.display = "none";
            }
        });

        function errorHandler(reseponse) {


            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
        }

        if (LeadId > 0) {

            // Inform that the modal is not yet ready
            $scope.modalState.loaded = false;

            $scope.QuestionService.GetQuestion(LeadId, function (questionServiceData) {

                $scope.LeadQuestionAns = [];
                $scope.AllQuestions = [];
                
                if (questionServiceData.LeadDetails != null) {
                    var leadsDetails = questionServiceData.LeadDetails.split(";");
                    if (leadsDetails.length > 0) {

                        if (parseInt(leadsDetails[6]) == 1)
                            LeadName = leadsDetails[5] + " / " + leadsDetails[0];
                        else {
                            if (leadsDetails[5] != "")
                                LeadName = leadsDetails[0] + " / " + leadsDetails[5];
                            else
                                LeadName = leadsDetails[0];
                        }


                        LeadStatusId = leadsDetails[1];
                        DispositionId = leadsDetails[2];
                        //Refering in the html pages
                        $scope.LeadName = LeadName;
                        $scope.LeadStatusId = LeadStatusId;
                        $scope.DispositionId = DispositionId;
                        $scope.Lead.LeadId = LeadId;
                        $scope.Lead.LeadNumber = leadsDetails[3];
                        $scope.Lead.PrimCustomer = leadsDetails[0];
                        $scope.Lead.LeadStatusId = leadsDetails[1];
                        $scope.Lead.DispositionId = leadsDetails[2];
                        $scope.Lead.Addresses = leadsDetails[4];
                        $scope.PrimaryEmailid = leadsDetails[7];
                        $scope.SecondaryEmailid = leadsDetails[8];
                        $scope.Lead.IsNotifyemails = leadsDetails[9];
                        if ($scope.Lead.IsNotifyemails == "0") {
                            $scope.Lead.IsNotifyemails = false;
                        }
                        else $scope.Lead.IsNotifyemails = true;
                    }
                }
                //get the disposition
                $http.get('/api/lookup/0/QuickDispositions')
                    .then(function (data) {
                        $scope.QuickDispositionList = data.data;
                    }); //, errorHandler);

                $scope.QuestionService.LeadDispositionByLeadStatus(LeadStatusId);
                if ($scope.QuickDispositionList) {
                    $scope.QuickDispositionList.Value = getDispositionValue(LeadStatusId, DispositionId);
                }

                // Find Brand ID
                var BrandId = 0;
                if (questionServiceData.QuestionList.length > 0)
                    BrandId = questionServiceData.QuestionList[0].BrandId;

                $scope.BrandId = BrandId;
                //Added for Qualification/Question Answers
                $scope.LeadQuestionAns = questionServiceData.QuestionList;
                $scope.AllQuestions = questionServiceData.QuestionList;
                //Added for Qualification/Question Answers
                if (questionServiceData.QuestionList) {
                    var questArray = $.map(questionServiceData.QuestionList, function (qa) {
                        return qa.Question;
                    });


                    $scope.UniqueQuestions = $.grep(questArray, function (el, index) {
                        return index == $.inArray(el, questArray);
                    });

                }
                //$scope.isDisabled = true;
                //Added for Qualification/Question Answers
                $scope.GetQuestion = function (QAcollection, CurrentQuestion) {
                    //Get each question details based on current 
                    //sub question from collection
                    if (QAcollection.length > 0 && CurrentQuestion) {
                        var sourcee = $.grep(QAcollection, function (q) {
                            return (q.Question == CurrentQuestion);
                        });
                        if (sourcee) return sourcee;
                    }

                    return null;
                };

                $scope.showValidate = true;
                HFCService.setHeaderTitle("Lead qualify #" + LeadId + " - " + $scope.LeadName);

                // Inform that the modal is ready to consume:
                $scope.modalState.loaded = true;
            }, errorHandler);



        }
        //for new lead
        $scope.Newleadpage = function () {

            // TODO: Is this casuing the issue too.
            var url = "http://" + $window.location.host + "#!/leads/";
            $window.location.href = url;
        };


        //For editing the Qualify
        $scope.EditQualify = function () {
            //var url = "http://" + $window.location.host + "/#!/leadsEditQualify/" + LeadId;
            //$window.location.href = url;
            // TODO: Verify that the following solves the Refresh issue
            $window.location.href = "#!/leadsEditQualify/" + LeadId;
        };

        //Cancel the Qualify
        $scope.cancelQualify = function () {
            $scope.showValidate = false;
            var goLeadQualify = '#!/leadsQualify/' + LeadId;
            $window.open(goLeadQualify, '_self');
        }

        $scope.ConvertLead = function () {
            $http.get('/api/search/' + LeadId + '/GetLeadMatchingAccounts').then(function (data) {
                $scope.MatchLeadAccount = data.data;
            });
            $scope.fQlfy.$setPristine();
            window.location = '#!/leadConvert/' + LeadId;
        }

        $scope.PersonService.CallendarSuccessCallback = function () {
            //if (PersonService.goPageIndex == 5) {
            //    var goLeadQualify = '#!/leadsQualify/' + LeadId;
            //    //window.open(goLeadQualify, '_self');
            //}
            //else {
            //    //window.location.reload();
            //    window.location.href = '#!/leadsQualify/' + LeadId;
            //}
        }

        function getDispositionValue(LeadStatusId, DispositionId) {
            var dispositionid;
            if (leadStatusId === 7) {
                dispositionid = '7|0|1';
            }
            else if (leadStatusId === 2) {
                dispositionid = '2|' + DispositionId + '|0';
            }
            else if (leadStatusId === 3) {
                dispositionid = '5|' + DispositionId + '|2';
            }
            else {
                dispositionid = '';
            }
            var quickdisp = $scope.QuickDispositionList;
            for (var i = 0 ; i < quickdisp.length; i++) {
                if (quickdisp[i].Value === dispositionid) {
                    return quickdisp[i];
                }
            }
            return '';
        }

        //added when user click on appointment from lead edit qualify
        $scope.$on('$routeChangeStart', function ($event, next, current) {

            //if ((next.$$route.originalPath.includes('leadsEditQualify'))) {
            if ((next.$$route.originalPath.includes('/Calendar/:Section/:Id'))) {//|| (next.$$route.originalPath.includes('leadsEditQualify'))) {
                $scope.fQlfy.$setPristine();
                
                $scope.SaveQualify(1);
            }
        });

        $scope.redirectto = "";
        //To save Qualify
        $scope.SaveQualify = function (val) {
            $scope.IsBusy = true;

            //restrict saving for invalid date
            for (i = 0; i < $scope.ValidDatelist.length; i++) {
                if (!$scope.ValidDatelist[i].valid) {
                    $scope.IsBusy = false;
                    HFC.DisplayAlert("Invalid Date!");
                    return;
                }
            }

            var loadingElement = $("#loading");
            if (loadingElement) {
                loadingElement[0].style.display = "block";
            }
            //Added for Qualification/Question Answers
            $http.post('/api/questions/' + LeadId + '/SaveQuestionAnswers/', $scope.AllQuestions).then(function (res) {
                $scope.IsBusy = false;
                if (loadingElement) {
                    loadingElement[0].style.display = "none";
                }

                if (val == 10) {
                    $scope.fQlfy.$setPristine();
                    $window.location.href = $scope.redirectto;
                }
                // $scope.isDisabled = true;

                // Move the url the Lead listing page.

                $scope.fQlfy.$setPristine();
                if (val === 0) {
                    var goLeadQualify = '#!/leadsQualify/' + LeadId;
                    //
                    //$location.path(goLeadQualify, false);
                    //$window.open(goLeadQualify, '_self', '', true);
                    // $window.location.reload(true);
                    $window.location.href = goLeadQualify;
                }
            });
        }

        $scope.DisplayMore = false;
        $scope.IsBusy = false;

        $scope.initialize = function () {
            $http.get('/api/lookup/0/LeadStatus').then(function (data) {
                $scope.LeadStatuseslist = data.data;
                if (!LeadId) {
                    LeadStatusId = 1;

                }
            });
        }

        $scope.GetEmailstatus = function () {
            
            var leadid = $scope.LeadId;
            $http.get('/api/Leads/0/GetEmailStatus?leadid=' + leadid).then(function (response) {
                
                if (response.data=="True") {
                    $scope.Lead.EmailSent = true;
                }
                else $scope.Lead.EmailSent = false;
                       
                });
         }

        $scope.GetEmailstatus();

        //code for sending email when email button clicked.
        $scope.SendLeadEmail = function () {
            $scope.EmailNotify = false;
            var leadid = $scope.LeadId;
            $scope.modalState.loaded = false;
            if ($scope.Lead.IsNotifyemails == true) {

           
                $http.get('/api/Leads/0/SendEmailToLead?leadid=' + leadid).then(function (response) {

                    if (response.data == "True") {
                        $scope.Lead.EmailSent = true;
                        $scope.MailSent = true;

                        if ($scope.PrimaryEmailid && $scope.SecondaryEmailid)
                            $scope.Mailid = $scope.PrimaryEmailid + ',' + $scope.SecondaryEmailid
                        else if ($scope.PrimaryEmailid)
                            $scope.Mailid = $scope.PrimaryEmailid;
                        else if ($scope.SecondaryEmailid)
                            $scope.Mailid = $scope.SecondaryEmailid;

                        $scope.modalState.loaded = true;

                        $("#AfetrThanks_EmailSend").modal("show");
                        return;
                    }
                    else if(response.data =="Success")
                    {
                        $scope.MailSent = false;
                        $scope.Lead.EmailSent = false;
                        $scope.modalState.loaded = true;
                        $("#AfetrThanks_EmailSend").modal("show");
                        return;
                    }
                else {
                     $scope.EmailNotify = true;
                    $scope.modalState.loaded = true;
                    $("#AfetrThanks_EmailSend").modal("show");
                    return;
                }

                });
            }
            else {
                $scope.EmailNotify = true;
                $scope.modalState.loaded = true;
                $("#AfetrThanks_EmailSend").modal("show");
                return;
            }
        }

        $scope.EmailMailClose = function () {
            
            $("#AfetrThanks_EmailSend").modal("hide");
            return;
        }

        // print integration -- murugan
        $scope.printControl = {};
        $scope.printSalesPacket = function (modalid) {
            if ($routeParams.leadId) $scope.leadIdPrint = $routeParams.leadId;

            // $("#" + modalid).modal("show");
            $scope.printControl.showPrintModal();
        }

        // Initialization code.
        $scope.initialize();

        //for date validation
        $scope.ValidateDate = function (date, id) {
            var ValidDateobj = {};
            if (!date)
                date = $("#" + id + "").val();

            if (date == "")
                $scope.ValidDate = true;
            else
                $scope.ValidDate = HFCService.ValidateDate(date, "date");

            if ($scope.ValidDatelist.find(item => item.id === id)) {
                var index = $scope.ValidDatelist.findIndex(item => item.id === id);
                $scope.ValidDatelist[index].valid = $scope.ValidDate;
            }
            else {
                ValidDateobj["id"] = id;
                ValidDateobj["valid"] = $scope.ValidDate;
                $scope.ValidDatelist.push(ValidDateobj);
            }

            if ($scope.ValidDate) {
                $("#" + id + "").removeClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).removeClass("icon_Background");
            }
            else {
                $("#" + id + "").addClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).addClass("icon_Background");
            }
        }


    }
]);




