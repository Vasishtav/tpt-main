﻿   
       

    app.controller('QuoteLineController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
        var ctrl = this;

        $scope.OriginalItem = null;
        $scope.InEditMode = $scope.item.InEditMode || false;
        $scope.IsCustom = $scope.item.isCustom || false;

        $scope.CalculateQuoteTotal = function () {
            var subtotal = 0,
                costSubtotal = 0,
                taxableSubtotal = 0,
                taxableSurcharge = 0,
                taxableDiscount  =0,
                discountedTaxableSubtotal = 0,
                discountedSubtotal = 0,
                surchargeSubtotal =0,
                discount = 0,
                discountPercent = 0,
                surcharge = 0,
                surchargePercent = 0,
                tax = 0,
                taxPercent = 0,
                total = 0,
                netprofit = 0,
                margin = 0,
                totalLineDiscounts = 0;
         
            angular.forEach($scope.quote.JobItems, function (ji) {
                if (ji.IsTaxable)
                    taxableSubtotal += parseFloat(ji.Subtotal) || 0;
                subtotal += parseFloat(ji.Subtotal) || 0;
                costSubtotal += parseFloat(ji.CostSubtotal) || 0;
                
                var qty = parseInt(ji.Quantity) || 0,
                    sale = parseFloat(ji.SalePrice) || 0;

                /*if (ji.DiscountType == 'Percent') {
                    ji.DiscountAmount = HFC.evenRound(ji.DiscountPercent / 100.00 * (qty * sale), 2);
                }*/

                totalLineDiscounts = totalLineDiscounts + parseFloat(ji.DiscountAmount);
            });


            angular.forEach($scope.quote.SaleAdjustments, function (ji) {
                if (ji.TypeEnum == $scope.QuoteSaleAdjService.SaleAdjTypeEnum.Tax)
                    taxPercent += parseFloat(ji.Percent) || 0;
                else if (ji.TypeEnum == $scope.QuoteSaleAdjService.SaleAdjTypeEnum.Surcharge) {
                    surcharge += parseFloat(ji.Amount) || 0;
                    surchargePercent += parseFloat(ji.Percent) || 0;
              

                    if (ji.IsTaxable) {
                        if (ji.Amount != 0)
                            taxableSurcharge += parseFloat(ji.Amount) || 0;
                        else
                            taxableSurcharge += (subtotal * (parseFloat(ji.Percent) / 100.0));
                    }
                } else if (ji.TypeEnum == $scope.QuoteSaleAdjService.SaleAdjTypeEnum.Discount) {
                    discount += parseFloat(ji.Amount) || 0;
                    discountPercent += parseFloat(ji.Percent) || 0;
                    
                    if (ji.IsTaxable)
                    {
                        if (ji.Amount != 0)
                            taxableDiscount += parseFloat(ji.Amount) || 0;
                        else
                            taxableDiscount += (subtotal * (parseFloat(ji.Percent) / 100.0));
                    }
                }
            });


            taxableSubtotal = taxableSubtotal + taxableSurcharge - taxableDiscount;

            tax = taxableSubtotal * (taxPercent / 100.0);
           
            discount += (subtotal * (discountPercent / 100.0));
            surcharge += (subtotal * (surchargePercent / 100.0));

            //subtotal = taxableSubtotal;
            total = subtotal + surcharge + tax - discount;
            netprofit = subtotal + surcharge - discount - costSubtotal;
            if (subtotal + surcharge - discount > 0)
                margin = (netprofit / (subtotal + surcharge - discount)) * 100;

            $scope.quote.TotalLineAmount = HFC.evenRound(subtotal, 4);
            $scope.quote.Subtotal = HFC.evenRound(subtotal + surcharge - discount, 4);
            $scope.quote.DiscountTotal = HFC.evenRound(discount, 4);
            $scope.quote.SurchargeTotal = HFC.evenRound(surcharge, 4);
            $scope.quote.Taxtotal = HFC.evenRound(tax, 4);
            $scope.quote.TaxPercent = HFC.evenRound(taxPercent, 4);
            $scope.quote.NetTotal = Math.round(total * 100) / 100;
            $scope.quote.NetProfit = HFC.evenRound(netprofit, 2);
            $scope.quote.Margin = HFC.evenRound(margin, 4);
            $scope.quote.CostSubtotal = HFC.evenRound(costSubtotal, 4);
            $scope.quote.TotalLineDiscounts = HFC.evenRound(totalLineDiscounts, 4);
            var paymentSum = 0;

            if (!$scope.quote.Invoices) {
                $scope.quote.Invoices = [];
            }

            $scope.quote.Invoices = $.grep($scope.quote.Invoices, function(invoice) {
                return invoice.IsDeleted !== true;
            });
            
            if ($scope.quote.Invoices.length > 0) {
                for (var i = 0; i < $scope.quote.Invoices[0].Payments.length; i++) {
                    paymentSum += parseFloat($scope.quote.Invoices[0].Payments[i].Amount);
                }
            }

            
            if (!$scope.job.Invoices) {
                $scope.job.Invoices = [];
            }
            if ($scope.quote.IsPrimary) {
                $scope.job.Balance = $scope.quote.NetTotal - paymentSum;
            }
            

            if ($scope.job.Invoices.length > 0) {
                $http.get('/api/payments/' + $scope.job.Invoices[0].InvoiceId + '/CalculatePayments').then(function (response) {
                    if ($scope.job.Invoices[0]) {
                        $scope.job.Invoices[0].LastUpdated = response.data.LastUpdated;
                        $scope.job.Invoices[0].StatusEnum = response.data.StatusEnum;
                        $scope.job.Invoices[0].PaidInFullOn = response.data.PaidInFullOn ? new XDate(response.data.PaidInFullOn, true) : null;
                    }
                });
            }
           
        }

        $scope.CalculateQuoteTotal();

        $rootScope.$on('paymentsUpdated', function (event, data) {
            $scope.job.Invoices = data.invoices;
            $scope.quote.Invoices = data.invoices;
            $scope.job.Balance = data.balance;

            $scope.CalculateQuoteTotal();
        });
    }]);