﻿'use strict';
appCP.controller('HFCRoleSearchController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'NavbarServiceCP','HFCService',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, NavbarServiceCP, HFCService) {

        HFCService.setHeaderTitle("Roles #");
        $scope.NavbarServiceCP = NavbarServiceCP;
        var offsetvalue = 0;
        $scope.NavbarServiceCP.GlobalPermissionSets();

        $scope.Permission = {};
        var Securitypermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Security')
        if (Securitypermission){
        $scope.Permission.ViewSecurity = Securitypermission.CanRead;
        }

        function ActivateDeActivateTemplate(dataitem) {
            return '<ul> <li class="dropdown note1 ad_user"> <div class="btn-group">' +
                ' <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                ' <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu">' +
                '<li><a href="#!/hfcrolesView/' + dataitem.RoleId + '">Edit</a></li> ' +
                '</ul> </li>  </ul>'

        }
        function DisplayName(dataitem) {
            if (dataitem.DisplayName == 'Supply Chain Partner (PIC)' || dataitem.DisplayName == 'HFC Product Strategy Mgt (PSM)')
                return '<a><span>' + dataitem.DisplayName + ' </span></a> '
            else
               return '<a href="#!/hfcrolesView/' + dataitem.RoleId + '"><span>' + dataitem.DisplayName + ' </span></a> '
        }
        $scope.GetRolesList = function () {

            $scope.RolesList = {
                dataSource: {
                    transport: {
                        read: {
                            url: function (params) {

                                var url1 = '/api/Permission/0/GetRolesList';
                                return url1;
                            },
                            dataType: "json"
                        }

                    },
                    error: function (e) {

                        HFC.DisplayAlert(e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "RoleId",
                            fields: {
                                RoleId: { editable: false },
                                RoleName: { editable: false },
                                DisplayName: { editable: false },
                                FranchiseId: { editable: false },
                                Description: { editable: false },
                                CreatedOnUtc: { type: 'date', editable: false },

                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "DisplayName",
                        title: "Name",
                        template:function(dataitem){
                            return DisplayName(dataitem);
                        },
                        //template: "<a href='\\\\#!/hfcrolesView/#= RoleId #'><span>#= DisplayName #</span></a> ",
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                    {
                        field: "Description",
                        title: "Description",
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        hidden: false,


                    },
                    {
                        field: "CreatedOnUtc",
                        title: "Created",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "HfcRoleFilterCalendar", offsetvalue),
                        hidden: false,
                        template: function (dataItem) {
                            return HFCService.NewKendoDateFormat(dataItem.CreatedOnUtc);
                        },
                            //"#= kendo.toString(new Date(CreatedOnUtc), 'MM/dd/yyyy') #",

                    },


                    //{
                    //    field: "",
                    //    title: "",
                    //    template: function (dataitem) {
                    //        return ActivateDeActivateTemplate(dataitem);
                    //    }
                    //},


                ],

                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                sortable: true,
                resizable: true,
                autoSync: true,
                noRecords: { template: "No records found" },
                scrollable: true,
                toolbar: [
                        {
                            template: kendo.template(
                                $(
                                    ' <script id="template" type="text/x-kendo-template"><div class="row no-margin vendor_search"><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="RolesSearchgridSearch()" type="search" id="searchBox" placeholder="Search Role" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="RolesSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>')
                                .html())
                        }
                ],

            };

        };
        $scope.GetRolesList();

        $scope.RolesSearchClear = function () {
            $('#searchBox').val('');
            $("#gridrolesSearch").data('kendoGrid').dataSource.filter({
            });
        }

        $scope.RolesSearchgridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#gridrolesSearch").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  {
                      field: "DisplayName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "Description",
                      operator: "contains",
                      value: searchValue
                  }
                ]
            });

        }

        //Redirect to Create Permission set Page
        $scope.NewPermissionSet = function () {
            window.location.href = '#!/permissionSet';
            //var url = "http://" + $window.location.host + "/#!/leads/";
            //$window.location.href = url;
        };

        $scope.CheckDuplicatePermissionName = function (name) {
            $scope.IsDuplicateSetName = false;
            if (name == undefined) {
                $scope.submitted = true;
                return;
            }
            else {
                $http.post('/api/Permission/' + $scope.PermissionSetId + '/CheckDuplicatePermissionSet/?name=' + name).then(function (data) {
                    

                    var permissionCheck = data.data;
                    if (permissionCheck) {
                        $scope.IsDuplicateSetName = true;
                        $window.alert("Permission Set with this name is already exist")
                    }
                    else {
                        $scope.IsDuplicateSetName = false;
                    }
                });
            }
        }

    }])