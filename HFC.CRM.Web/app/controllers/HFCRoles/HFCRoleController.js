﻿appCP.controller('HFCRoleController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope','NavbarServiceCP','HFCService',
    function ($http, $scope, $routeParams, $window, $location, $rootScope, NavbarServiceCP, HFCService) {
        $scope.NavbarServiceCP = NavbarServiceCP;

        $scope.NavbarServiceCP.GlobalPermissionSets();
        var dataSource = [];
        $scope.IsView = false;

        $scope.PermissionSetId = $routeParams.PermissionSetId == undefined ? 0 : $routeParams.PermissionSetId;

        if (window.location.href.includes("hfcrolesView")) {
            $scope.IsView = true;
        }
       
        $scope.Permission = {};
        var Securitypermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Security')

        $scope.Permission.ViewSecurity = Securitypermission.CanRead;
        $scope.Permission.AddSecurity = Securitypermission.CanCreate;
        $scope.Permission.EditSecurity = Securitypermission.CanUpdate;

        $scope.RoleId = $routeParams.RoleId == undefined ? 0 : $routeParams.RoleId;
        $http.get('/api/Permission/' + $scope.RoleId + '/GetPermissionByRolesId').then(function (data) {
            

            $scope.PermissionSets = data.data.PermissionSets;
            dataSource = new kendo.data.TreeListDataSource({
                data: $scope.PermissionSets
            });

            $scope.KendoTreeListOptions();
        });
        $scope.KendoTreeListOptions = function () {
            $("#treelist").kendoTreeList({
                dataSource: dataSource,

                columns: [
                    {
                        field: "Name",
                        width: "260px",
                        expandable: true
                    },

                    {
                        field: "SpecialPermission",
                        title: "Is Accessible",

                        template: function (e) {
                            if (e.IsSpecialPermission && !$scope.IsView) {
                                return "<input type='checkbox' data-bind='checked: SpecialPermission' />";
                            }
                            else if (e.IsSpecialPermission && $scope.IsView) {
                                return "<input type='checkbox' disabled data-bind='checked: SpecialPermission' />";
                            }
                        },
                    },
                  {
                      field: "Create",

                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Create' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Create' />";
                          }
                      },

                  },
                  {
                      field: "Read",

                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Read' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Read' />";
                          }

                      },

                  },
                  {
                      field: "Update",

                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Update' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Update' />";
                          }
                      },
                  },
                  {
                      field: "Delete",

                      template: function (e) {
                          if (e.ModuleId != null && !e.IsSpecialPermission && !$scope.IsView) {
                              return "<input type='checkbox' data-bind='checked: Delete' />";
                          }
                          else if (e.ModuleId != null && !e.IsSpecialPermission && $scope.IsView) {
                              return "<input type='checkbox' disabled data-bind='checked: Delete' />";
                          }
                      },

                  },



                ],
                dataBound: function () {
                    var view = this.dataSource.view();
                    this.items().each(function (index, row) {
                        kendo.bind(row, view[index]);
                    });
                }
            });
        }

        $scope.EditPermissionSet = function () {
            var url = "#!/hfcrolesSet/"+$scope.RoleId;
            window.location.href = url;
            
        };
        $scope.SavePermission = function () {
            

            $scope.submitted = true;
           
            var checkSpecialPermission = JSLINQ(dataSource._data)
                              .Where(function (item) { return item.SpecialPermission == true; });

            var checkReadPermission = JSLINQ(dataSource._data)
                             .Where(function (item) { return item.Read == true; });
            var checkUpdatePermission = JSLINQ(dataSource._data)
                            .Where(function (item) { return item.Update == true; });
            var checkDeletePermission = JSLINQ(dataSource._data)
                            .Where(function (item) { return item.Delete == true; });
            var checkCreatePermission = JSLINQ(dataSource._data)
                            .Where(function (item) { return item.Create == true; });

            if (checkReadPermission.items.length == 0 && checkUpdatePermission.items.length == 0 && checkDeletePermission.items.length == 0
                && checkCreatePermission.items.length == 0 && checkSpecialPermission.items.length == 0) {
                $window.alert("At least one check box should be checked, otherwise the Permission Set will be meaningless.")
                return;
            }


            if ($scope.HFCformroles.$valid == false) {

                return;
            }
            var PermissionSets = [];
          
            var dto ={};
             dto.RoleId = $scope.RoleId;
            dto.PermissionSets = dataSource._data;

            $http.post('/api/Permission/0/UpdatePermissionSetRole', dto).then(function (data) {
                
                $scope.HFCformroles.$setPristine();
                window.location.href = '#!/hfcrolesView/' + $scope.RoleId;
            });

        };
        $scope.Cancel = function () {
            window.location.href = '#!/hfcrolesView/' + $scope.RoleId;
        };

      
    }
]);