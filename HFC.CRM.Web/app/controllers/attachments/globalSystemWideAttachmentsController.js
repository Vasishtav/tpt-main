﻿appCP.controller('globalSystemWideAttachmentscontroller',
             ['$scope', '$routeParams', '$http', '$window', '$location', 'NavbarServiceCP', 'NoteServiceTP','HFCService',
function ($scope, $routeParams, $http, $window, $location, NavbarServiceCP, NoteServiceTP,HFCService) {
        $scope.attachmentList = [{ id: 1, type: 'Sales Packet' }, { id: 2, type: 'Installation Packet' }, { id: 3, type: 'Installation3' }];
        $scope.attachment = {};
        
        HFCService.setHeaderTitle("System Documents #");
        $scope.NoteServiceTP = NoteServiceTP;
        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.NavbarServiceCP.selectGlobalSystemwideAttachments();

        $scope.Permission = {};
        var GlobalDoc = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'SystemDocuments')
        if (GlobalDoc != undefined) {
            $scope.Permission.PermissionAddDoc = GlobalDoc.CanCreate;
            $scope.Permission.PermissionViewDoc = GlobalDoc.CanRead;
        }
  
        //$scope.AttachmentTemplate = function (dataItem) {
        //    
        //    var Div = '';
        //    if (dataItem.Stream_id) {
        //        Div = "<a href='/api/Download?streamid=" + dataItem.Stream_id + "' target=\"_blank\" style='color: rgb(61,125,139);'><span style='padding: 10px;'>" + dataItem.Document + "</span></a> ";
        //    }
        //    else {
        //        Div = "<span style='padding: 10px;'>" + dataItem.Document + "</span> ";
        //    }
        //    return Div;
        //}

        //$scope.CPSettingsDocumentGridOptions = {
        //    cache: false,
        //    dataSource: {
        //        transport: {
        //            read: {
        //                url: '/api/Source/0/GetGlobaldocList',
        //                dataType: "json"
        //            }
        //        }
        //        ,
        //        error: function (e) {
        //            HFC.DisplayAlert(e.errorThrown);
        //        },
        //        pageSize: 25
        //    },
        //    dataBound: function (e) {
        //        if (this.dataSource.view().length == 0) {
        //            $('.k-pager-nav').hide();
        //            $('.k-pager-numbers').hide();
        //            $('.k-pager-sizes').hide();
        //        } else {
        //            $('.k-pager-nav').show();
        //            $('.k-pager-numbers').show();
        //            $('.k-pager-sizes').show();
        //        }
        //    },
        //    columns: [
        //        {
        //            field: "Id",
        //            title: "Id",
        //           hidden:true
        //        },
        //    {
        //        field: "Stream_id",
        //        title: "Stream_id",
        //hidden:true
        //},
        //              {
        //                  field: "Brand",
        //                  title: "Brand",
        //                  filterable: { multi: true, search: true }
        //              },
        //                {
        //                    field: "Document",
        //                    title: "Document",
        //                    template: function(dataItem)
        //                    {
        //                      return $scope.AttachmentTemplate(dataItem);
        //                    },
        //                   filterable: { multi: true, search: true }
        //                },
        //              {
        //                  title: "Attach",
        //                  template: function (dataItem) {
        //                    var checkedStatus = dataItem.IsEnabled == true ? "checked" : "";
        //                    var html = '<div ><div><u  style="cursor: pointer;" ng-click="NoteServiceTP.addGlobalDocuments(dataItem.Id,dataItem.Brand,dataItem.Document)">attach</u></div></div>';
        //                    return html;
        //                  }
        //              }
        //    ],
        //    noRecords: { template: "No records found" },
        //    filterable: {
        //        extra: false,
        //        operators: {
        //            string: {
        //                contains: "Contains",
        //                eq: "Is equal to",
        //                neq: "Is not equal to"
        //            }
        //        }
        //    },
        //    pageable: {
        //        refresh: true,
        //        pageSize: 25,
        //        pageSizes: [25, 50, 100, 'All'],
        //        buttonCount: 5
        //    },
        //    toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search"><input ng-keyup="gridDocumentsSearch()" type="search" id="searchBox" placeholder="Search Document" class="k-textbox leadsearch_tbox"><input type="button" id="btnReset" ng-click="gridDocumentsSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></script>').html())
        //};
        //// searchbox filter
        //$scope.gridDocumentsSearch = function () {
        //    var searchValue = $('#searchBox').val();
        //    $("#gridDocumentsCP").data("kendoGrid").dataSource.filter({
        //        logic: "or",
        //        filters: [
        //          {
        //              field: "Brand",
        //              operator: "contains",
        //              value: searchValue
        //          },
        //          {
        //              field: "Document",
        //              operator: "contains",
        //              value: searchValue
        //          },
        //        ]
        //    });
        //}
        //// clear grid filter
        //$scope.gridDocumentsSearchClear = function () {
        //    $('#searchBox').val('');
        //    $("#gridDocumentsCP").data('kendoGrid').dataSource.filter({
        //    });
    //}
    // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 280);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 280);
            };
        });
    // End Kendo Resizing
    }]);