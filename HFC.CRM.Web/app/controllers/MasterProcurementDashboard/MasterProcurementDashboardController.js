﻿


'use strict';
appCP
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('MasterProcurementDashboardController',
    [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'NavbarServiceCP',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, NavbarServiceCP) {

            HFCService.setHeaderTitle("Procurement List #");
            $scope.NavbarServiceCP = NavbarServiceCP;
            //$scope.NavbarServiceCP.masterprocurementboardTab();
            
            $scope.NavbarServiceCP.SetupOperationsInfoTab();

            $scope.Permission = {};
            var MasterDashboardpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'ProcurementDashboard')

            $scope.Permission.ViewMasterBoard = MasterDashboardpermission.CanRead;
            
            var offsetvalue = 0;
            $scope.SelectVendorId = 0;
            $scope.Procurement = {
                VendorID: '',
                FranchiseId: '',
                DateValue: '',
                EndDateValue: '',
                Id: 'Today'
            }


            $scope.listProcurement = {};
            $scope.ValueList = [];

            $scope.HFCService = HFCService;
            $scope.SelectFranchiseId = 0;


            $scope.InitialLoad = true;
            //grid code
            $scope.GetProcurementValue = function () {
                $scope.GridEditable = false;
                $scope.ProcurementlineData = {
                    dataSource: {
                        transport: {
                            read: function (e) {
                                
                                $http.get('/api/Procurement/' + $scope.SelectVendorId + '/GetData?franchiseid=' + $scope.SelectFranchiseId + '&dateFilter=' + $scope.Procurement.Id).then(function (data) {
                                    $scope.listProcurement = data.data;
                                    var _unArray = [];
                                    var dupes = [];
                                    data.data.forEach(function (item) {
                                        var isPresent = _unArray.filter(function (elem) {
                                            return (elem.PICPO === item.PICPO &&
                                                elem.MasterPONumber === item.MasterPONumber && elem.VendorId === item.VendorId);
                                        })
                                        if (isPresent.length === 0) {
                                            _unArray.push(item)
                                        }
                                        else {
                                            dupes.push(item)
                                        }
                                    });
                                    for (var i = 0; i < _unArray.length; i++) {
                                        var obj = [];
                                        obj = dupes.filter(
                                            x => x.PurchaseOrderId === _unArray[i].PurchaseOrderId &&
                                            x.PICPO === _unArray[i].PICPO &&
                                            x.VendorId === _unArray[i].VendorId);
                                        obj.push(_unArray[i]);
                                        if (obj != null && obj != undefined && obj.length > 1) {
                                            var error = false,
                                                backordered = false,
                                                partialshipped = false,
                                                shipped = false;
                                            for (var a = 0; a < obj.length; a++) {
                                                if (obj[a].StatusId === 10) {
                                                    error = true;
                                                } else if (obj[a].StatusId === 4) {
                                                    backordered = true;
                                                }
                                                else if (obj[a].StatusId === 3) {
                                                    shipped = true;
                                                }
                                            }
                                            if (error) {
                                                _unArray[i].VPOStatus = "Error";
                                                _unArray[i].StatusId = 10;
                                            } else if (partialshipped) {
                                                _unArray[i].VPOStatus = "Partial Shipped";
                                                _unArray[i].StatusId = 4;
                                            }
                                            else if (backordered) {
                                                _unArray[i].VPOStatus = "Back Ordered";
                                                _unArray[i].StatusId = 4;
                                            }
                                        }

                                    }
                                    e.success(_unArray);

                                });


                            }

                        },
                        //requestStart: function () {
                        //    if (!$scope.InitialLoad)
                        //    kendo.ui.progress($("#ProcurementboardGrid"), true);
                        //},
                        //requestEnd: function () {
                        //    if (!$scope.InitialLoad)
                        //        kendo.ui.progress($("#ProcurementboardGrid"), false);
                        //    else
                        //        $scope.InitialLoad = false;
                        //},                        
                        error: function (e) {
                            HFC.DisplayAlert(e.errorThrown);
                             //kendo.ui.progress($("#ProcurementboardGrid"), false);
                        },

                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    FranchiseName: { editable: false },
                                    MasterPONumber: {type: "number", editable: false },
                                    PurchaseOrderId: { type: "number", editable: false },
                                    VendorName: { editable: false },
                                    PICPO: { type: "number" },
                                    OrderNumber: { type: "string" },
                                    Orderlines: { type: "number" },
                                    MPOCost: { type: "number" },
                                    MPOValue: { type: "number" },
                                    CoreProductMargin: { type: "number" },
                                    CoreProductMarginPercent: { type: "number" },
                                   CompanyName: { editable: false, },
                                    OrderID: { type: "number", editable: false },
                                    OrderDate: { editable: false, type: "date" },
                                    MPODate: { editable: false, type: "date" },
                                    Status: { editable: false },//need vpo date
                                    PromiseByDate: { editable: false, type: "date" },
                                    EstShipDate: { editable: false, type: "date" },
                                    ShippedDate: { editable: false, type: "date" },
                                    VPODate: { editable: false, type: "date" },
                                    Status: { editable: false },
                                    ContractedDate: { editable: false, type: "date" }
                                }
                            },
                            parse: function (d) {
                                
                                $.each(d, function (idx, elem) {
                                    elem.OrderDate = kendo.parseDate(elem.OrderDate, 'yyyy-MM-dd');// kendo.parseDate(elem.OrderDate, "MM/dd/yyyy");
                                    elem.MPODate = kendo.parseDate(elem.MPODate, 'yyyy-MM-dd');
                                    elem.VPODate = kendo.parseDate(elem.VPODate, 'yyyy-MM-dd');
                                    elem.PromiseByDate = kendo.parseDate(elem.PromiseByDate, 'yyyy-MM-dd');
                                    elem.EstShipDate = kendo.parseDate(elem.EstShipDate, 'yyyy-MM-dd');
                                    elem.ShippedDate = kendo.parseDate(elem.ShippedDate, 'yyyy-MM-dd');
                                });
                                return d;
                            }
                        }
                    },

                    columns: [
                        {
                            field: "FranchiseName",
                            title: "FE Name",
                            width: '120px',
                            filterable: {
                                //multi: true,
                                search: true
                            },
                        },
                         {
                             field: "MasterPONumber",
                             title: "MPO",
                             width: '80px',
                             filterable: {
                                 //multi: true,
                                 search: true
                             },
                             hidden: false,
                             template: function (dataItem) {
                                 return "<span class='Grid_Textalign'>" + dataItem.MasterPONumber + "</span>";
                             },
                         },
                        {
                            field: "PICPO",
                            title: "VPO",
                            width: '110px',
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.PICPO + "</span>";
                            },
                        },
                        {
                            field: "VendorName",
                            title: "Vendor Name",
                            width: '250px',
                            hidden: false,
                        },
                        {
                            field: "AccountName",
                            title: "Customer Info",
                            width: '200px',
                            hidden: false,
                            filterable: { search: true },
                        },
                        {
                            field: "OrderNumber",
                            title: "Order",
                           
                            width: '100px',
                            hidden: false,
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.OrderNumber + "</span>";
                            },
                        },
                        {
                            field: "OrderDate",
                            title: "Order Date",
                            width: '130px',
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "MasterPoOrderDateFilterCalendar", offsetvalue),
                            template: function (dataitem) {
                                if (dataitem.OrderDate == null)
                                    return "";
                                else
                                    return HFCService.KendoDateFormat(dataitem.OrderDate);
                            },
                                //"#=  (OrderDate == null)? '' : kendo.toString(kendo.parseDate(OrderDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                            hidden: false,
                        },
                        {
                            field: "Orderlines",
                            title: "Order Lines",
                            width: '120px',
                            hidden: false,
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.Orderlines + "</span>";
                            },
                        },
                        {
                            field: "MPOCost",
                            title: "MPO Cost",
                            width: '120px',
                            type:"number",
                            hidden: false,
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.MPOCost) + "</span>";
                            },
                        },
                        {
                            field: "MPOValue",
                            title: "MPO Value",
                            width: '130px',
                            hidden: false,
                            type: "number",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.MPOValue) + "</span>";
                            },
                        },
                        {
                            field: "CoreProductMargin",
                            title: "Margin$",
                            width: '100px',
                            type: "number",
                            hidden: false,
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.CoreProductMargin) + "</span>";
                            },
                        },
                        {
                            field: "CoreProductMarginPercent",
                            title: "Margin%",
                            width: '100px',
                            hidden: false,
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            //template: function (dataItem) {
                            //    if (dataItem.CoreProductMarginPercent)
                            //        return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.CoreProductMarginPercent, 'decimal') + "</span>";
                            //    else
                            //        return "<span class='Grid_Textalign'>" + dataItem.CoreProductMarginPercent + "</span>";
                            //},
                            template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", CoreProductMarginPercent / 100)#</span>'
                        },
                        {
                            field: "Orderlines",
                            title: "MPO Lines",
                            width: '130px',
                            type: "number",
                            hidden: false,
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.Orderlines + "</span>";
                            },
                        },
                        {
                            field: "MPODate",
                            title: "MPO Date",
                            width: '130px',
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "MasterPoMpoDateFilterCalendar", offsetvalue),
                            template: function (dataitem) {
                                if (dataitem.MPODate == null)
                                    return "";
                                else
                                    return HFCService.KendoDateFormat(dataitem.MPODate);
                            },
                                //"#=  (MPODate == null)? '' : kendo.toString(kendo.parseDate(MPODate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                            hidden: false,
                        },
                        {
                             field: "VPODate",//Vpo Date needed
                             title: "VPO Date",
                             width: '130px',
                             type: "date",
                            filterable: HFCService.gridfilterableDate("date", "MasterPoVpoDateFilterCalendar", offsetvalue),
                             template: function (dataitem) {
                                 if (dataitem.VPODate == null)
                                     return "";
                                 else
                                     return HFCService.KendoDateFormat(dataitem.VPODate);
                             },
                                 //"#=  (VPODate == null)? '' : kendo.toString(kendo.parseDate(VPODate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                             hidden: false,
                        },
                        {
                             field: "PromiseByDate",
                             title: "Promise-By Date",
                             width: '160px',
                             type: "date",
                            filterable: HFCService.gridfilterableDate("date", "MasterPoPromiseFilterCalendar", offsetvalue),
                             template: function (dataitem) {
                                 if ((dataitem.PromiseByDate == null)|| (dataitem.PromiseByDate == '0001-01-01T00:00:00'))
                                     return "";
                                 else
                                     return HFCService.KendoDateFormat(dataitem.PromiseByDate);
                             },
                                 //"#= ( (PromiseByDate == null)|| (PromiseByDate == '0001-01-01T00:00:00') )? '' : kendo.toString(kendo.parseDate(PromiseByDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                             hidden: false,
                        },
                        {
                             field: "EstShipDate",
                             title: "Est Ship Date",
                             width: '130px',
                             type: "date",
                            filterable: HFCService.gridfilterableDate("date", "MasterPoEstShipFilterCalendar", offsetvalue),
                             template: function (dataitem) {
                                 if ((dataitem.EstShipDate == null) || (dataitem.EstShipDate == '0001-01-01T00:00:00'))
                                     return "";
                                 else
                                     return HFCService.KendoDateFormat(dataitem.EstShipDate);
                             },
                                 //"#= ( (EstShipDate == null)|| (EstShipDate == '0001-01-01T00:00:00') )? '' : kendo.toString(kendo.parseDate(EstShipDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                             hidden: false,
                        },
                        {
                             field: "ShippedDate",
                             title: "Ship Date",
                             width: '150px',
                             type: "date",
                            filterable: HFCService.gridfilterableDate("date", "MasterPoShippedFilterCalendar", offsetvalue),
                             template: function (dataitem) {
                                 if ((dataitem.ShippedDate == null) || (dataitem.ShippedDate == '0001-01-01T00:00:00'))
                                     return "";
                                 else
                                     return HFCService.KendoDateFormat(dataitem.ShippedDate);
                             },
                                 //"#= ( (ShippedDate == null)|| (ShippedDate == '0001-01-01T00:00:00') )? '' : kendo.toString(kendo.parseDate(ShippedDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                             hidden: false,
                        },
                        {
                             field: "ContractedDate",
                             title: "Invoice Date",
                             width: '150px',
                             type: "date",
                            filterable: HFCService.gridfilterableDate("date", "MasterPoFilterContractCalendar", offsetvalue),
                             template: function (dataitem) {
                                 if ((dataitem.ContractedDate == null) || (dataitem.ContractedDate == '0001-01-01T00:00:00'))
                                     return "";
                                 else
                                     return HFCService.KendoDateFormat(dataitem.ContractedDate);
                             },
                                 //"#= ( (ContractedDate == null)|| (ContractedDate == '0001-01-01T00:00:00') )? '' : kendo.toString(kendo.parseDate(ContractedDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                             hidden: false,
                        },
                        {
                             field: "VPOStatus",
                             title: "VPO Status",
                             width: '150px',
                             filterable: {
                                 //multi: true,
                                 search: true
                             },
                        },
                        {
                             field: "Errors",
                             title: "Error Message",
                             width: '150px',
                             template: "#= Errors == null? '' : Errors #",
                             //template:"#=Errors#",
                             hidden: false,
                        },
                        
                    ],

                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],
                        change: function (e) {
                            var myDiv = $('.k-grid-content.k-auto-scrollable');
                            myDiv[0].scrollTop = 0;
                        }
                    },
                    toolbar: kendo.template($('<script id="template" type="text/x-kendo-template"><div class="row no-margin"><div class="leadsearch_topleft row no-margin no-padding col-sm-12 col-md-12 col-xs-12"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="prodashboardgridSearch()" type="search" id="searchBox" placeholder="Search..." class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="ProcurementSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></div></script>').html()),

                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                isempty: "Empty",
                                isnotempty: "Not empty"


                            },
                            number: {
                                eq: "Equal to",
                                neq: "Not equal to",
                                gte: "Greater than or equal to",
                                lte: "Less than or equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    resizable: true,
                    sortable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    filterMenuInit: function (e) {
                        //$(e.container).find('.k-check-all').click()
                        e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                    },
                };
            };





            //search clear
            $scope.ProcurementSearchClear = function () {
                $('#searchBox').val('');
                var searchValue = $('#searchBox').val('');
                $("#ProcurementboardGrid").data('kendoGrid').dataSource.filter({
                });
            }

            //to get active vendor to drop down
            //$http.get('/api/Procurement/0/AdminVendorNameList').then(function (data) {
            //    $scope.VendorListName = data.data;
            //});
            $scope.Vendor_NameList = function () {
                $scope.Vendor_NameList = {
                    optionLabel: {
                        Name: "All",
                        VendorID: ""
                    },
                    dataTextField: "Name",
                    dataValueField: "VendorID",
                    filter: "contains",
                    valuePrimitive: true,
                    autoBind: true,
                    change: function (data) {
                        var val = this.value();
                        $scope.VendorChange(val)
                    },
                    dataSource: {
                        transport: {
                            read: {
                                url: "/api/Procurement/0/AdminVendorNameList",
                            }
                        }
                    },
                };
            }
            $scope.Vendor_NameList();

            //to get active franchise name
            //$http.get('/api/Procurement/0/GetFranchiseList').then(function (data) {
            //    $scope.FranchiseListName = data.data;
            //});
            $scope.Franchise_NameList = function () {
                $scope.Franchise_NameList = {
                    optionLabel: {
                        Name: "All",
                        FranchiseId: ""
                    },
                    dataTextField: "Name",
                    dataValueField: "FranchiseId",
                    filter: "contains",
                    valuePrimitive: true,
                    autoBind: true,
                    change: function (data) {
                        var val = this.value();
                        $scope.VendorChange(val)
                    },
                    dataSource: {
                        transport: {
                            read: {
                                url: "/api/Procurement/0/GetFranchiseList",
                            }
                        }
                    },
                };
            }
            $scope.Franchise_NameList();

            //to get Dashboard Filter
            $http.get('/api/Procurement/0/Type_DateTime?Tableid=' + 4).then(function (data) {
                $scope.DateTimeDropdown = data.data;
                for (var i = 0; i < $scope.DateTimeDropdown.length; i++) {
                    if ($scope.DateTimeDropdown[i]['Name'] == 'Last 30 days (rolling)') {
                        $scope.DateRangeId = $scope.DateTimeDropdown[i]['Id'];
                        $scope.Procurement.Id = $scope.DateTimeDropdown[i]['Id'];
                        $scope.FilterChange($scope.DateTimeDropdown[i]['Id']);
                        $scope.GetProcurementValue();
                    }
                }
            });


            //on selecting drop-down value(Franchise)
            $scope.FranchiseChange = function (id) {
                if (id === null || id === '' || id === undefined) {
                    $scope.SelectFranchiseId = 0;
                } else {
                    $scope.SelectFranchiseId = id;
                }

                $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
                //$('#ProcurementboardGrid').data('kendoGrid').refresh();
                
                return;
            }
            //on selecting dashboard drop-down value(Date/time drop-down)
            $scope.FilterChange = function (id) {
                if (id === null || id === '' || id === undefined) {
                    $scope.Id = '';
                } else {
                    $scope.Id = id;
                }
                if ($('#ProcurementboardGrid').data('kendoGrid')) {
                    $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
				}
                return;
            }

            //on selecting drop-down value(vendor)
            $scope.VendorChange = function (id) {
                if (id === null || id === '' || id === undefined) {
                    $scope.SelectVendorId = 0;
                } else {
                    $scope.SelectVendorId = id;
                }

                $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
                //$('#ProcurementboardGrid').data('kendoGrid').refresh();
                 
            }

            //on clicking the checkboxes values to be passed
            $scope.Check = function () {
                $scope.ValueList = $scope.Valueselected();
                $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();               
                //$('#ProcurementboardGrid').data('kendoGrid').refresh();
                

            }

            //getting the check-box value
            $scope.Valueselected = function () {
                var arr = [];
                var result = '';
                if ($('#Check1').is(":checked")) {
                    result = 1;
                    arr.push(result);
                }
                if ($('#Check2').is(":checked")) {
                    result = 2;
                    arr.push(result);
                }
                if ($('#Check3').is(":checked")) {
                    result = 3;
                    arr.push(result);
                }
                if ($('#Check4').is(":checked")) {
                    result = 4;
                    arr.push(result);
                }
                if ($('#Check5').is(":checked")) {
                    result = 10;
                    arr.push(result);
                }
                return arr;
            }

            //search box code
            $scope.prodashboardgridSearch = function () {
                var searchValue = $('#searchBox').val();

                $("#ProcurementboardGrid").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "MasterPONumber",
                          operator: "eq",
                          value: searchValue
                      },
                      {
                          field: "PICPO",
                          operator: "eq",
                          value: searchValue
                      },
                        {
                            field: "PurchaseOrderId",
                            operator: "eq",
                            value: searchValue
                        },
                        {
                            field: "OrderNumber",
                            operator: "contains",
                            value: searchValue
                        },
                      {
                          field: "VendorName",
                          operator: "contains",
                          value: searchValue
                      },
                    {
                        field: "OpportunityName",
                        operator: "contains",
                        value: searchValue
                    },
                        {
                            field: "AccountName",
                            operator: "contains",
                            value: searchValue
                        },
                      {
                          field: "CompanyName",
                          operator: "contains",
                          value: searchValue
                      }

                    ]
                });

            }

            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 390);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 390);
                };
            });
            // End Kendo Resizing
        }

    ])
