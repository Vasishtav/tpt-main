﻿app.controller('dashboardController', ['$http', '$scope', '$window', 'dashboardService'
    , '$rootScope', 'NavbarService', 'HFCService', 'CalendarService'
    , 'NewsService', 'PersonService', 'calendarModalService'
    , function ($http, $scope, $window, dashboardService
        , $rootScope, NavbarService, HFCService, CalendarService
        , NewsService, PersonService, calendarModalService) {

        $scope.calendarModalService = calendarModalService;

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectHome();
        $scope.NavbarService.EnableDashboardTab();

        $scope.HfcService = HFCService;
       
        $scope.CalendarService = CalendarService;
        HFCService.setHeaderTitle("Home #");
        $scope.newsService = NewsService;

        $scope.PersonService = PersonService;
        var offsetvalue = 0; 

        $scope.companyView = false;
        $scope.ViewButtonDisplayName = "Company View";
        $scope.AppointmentsCaption = "Today's Appointments";
        $scope.TasksCaption = "Today's Tasks";
        $scope.TaskDisplayType = "today";

        $scope.toggleView = function () {
            // Toggle the Company view -> <- My view
            $scope.companyView = !$scope.companyView;

            if ($scope.companyView) {
                $scope.ViewButtonDisplayName = "My Day";
                $scope.AppointmentsCaption = "Today's Appointments";
                //$scope.TasksCaption = "All Tasks";
            } else {
                $scope.ViewButtonDisplayName = "Company View";
                $scope.AppointmentsCaption = "Today's Appointments";
               // $scope.TasksCaption = "Today's Tasks";
            }
            
            


            // Refresh the grid to fetch the data from the DB.
            $("#MyAppointmentsGrid").data("kendoGrid").dataSource.read();
            $("#MyTasksGrid").data("kendoGrid").dataSource.read();
        }

        $scope.updateTaskView = function (option) {
            $scope.TaskDisplayType = option

            switch ($scope.TaskDisplayType) {
                case 'open':
                    $scope.TasksCaption = "Open Tasks";
                    break;
                case 'pastdue':
                    $scope.TasksCaption = "Past Due Tasks";
                    break;
                case 'completed':
                    $scope.TasksCaption = "Completed Tasks";
                    break;
                default:
                    $scope.TasksCaption = $scope.companyView ? "All Tasks"
                        : "Today's Tasks";
            }

            $("#MyTasksGrid").data("kendoGrid").dataSource.read();
        }

        $scope.MyAppointmentsGridOptions = {
            dataSource: {
                type: "jsonp",
                transport: {
                    cache: false,
                    read: function (e) {
                        var url = "api/calendar/0/GetMyEvents";
                        if ($scope.companyView == true) {
                            url = "api/calendar/0/GetAllEvents";
                        }
                      
                        $http.get(url, e).then(function (data) {
                            //
                            e.success(data.data);
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "update") {
                            return options;
                        }
                    }
                },
                schema: {
                    model: {
                        id: "CalendarId",
                        fields: {
                            CalendarId: { editable: false },
                            Subject: { editable: false },
                            //LeadName: { editable: false },
                            //AccountName: { editable: false },
                            Related: { editable: false },
                            StartDate: { type: 'date', editable: false },
                            EndDate: { type: 'date', editable: false },
                            AppointmentType: { editable: false },
                            AttendeesNames: { editable: false }
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            sortable: true,           
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isempty: "Empty",
                        isnotempty: "Not empty"


                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gte: "Greater than or equal to",
                        lte: "Less than or equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5
            },

            noRecords: { template: "No records found" },
            columns: [
                { field: "Subject", title: "Subject" },
                {
                    field: "Related",
                    title: "Related",
                    filterable: true,                    
                },
                //{ field: "LeadName", title: "Lead" },
                //{ field: "AccountName", title: "Account" },
                { field: "AttendeesNames", title: "Attendees" },
                { field: "AppointmentType", title: "Type" },
                {
                    field: "StartDate", title: "Start"
                    //, template: "#= kendo.toString(kendo.parseDate(StartDate, 'yyyy-MM-dd'), 'M/d/yyyy')#"
					,
                    type: "date",
                    filterable: HFCService.gridfilterableDate("datetime", "DashboardStartDateFilterCalendar", offsetvalue),
                    template: function (dataitem) {
					    return HFCService.KendoDateTimeFormat(dataitem.StartDate);
					}
                        //"#= kendo.toString(kendo.parseDate(StartDate), 'yyyy-MM-dd hh:mm tt') #"
                },
                 {
                     field: "EndDate", title: "End"
                     //, template: "#= kendo.toString(kendo.parseDate(EndDate, 'yyyy-MM-dd'), 'M/d/yyyy')#"
					 ,
                     type: "date",
                     filterable: HFCService.gridfilterableDate("datetime", "DashboardEndDateFilterCalendar", offsetvalue),
                     template: function (dataitem) {
					     return HFCService.KendoDateTimeFormat(dataitem.EndDate);
					 }
                         //"#= kendo.toString(kendo.parseDate(EndDate), 'yyyy-MM-dd hh:mm tt') #"
                 },
                 {
                     field: "",
                     title: "",
                     template: function (dataitem) {
                         return TaskMenuTemplate(dataitem);
                     }//,
                     //width: "5%"
                 }
            ]

        };

        $scope.MyTaskGridOptions = {
            dataSource: {
                type: "jsonp",
                transport: {
                    cache: false,
                    read: function (e) {
                        
                        var url = "api/calendar/0/GetMytasks";
                        switch ($scope.TaskDisplayType) {
                            case 'open':
                                url = $scope.companyView ? "api/calendar/0/GetAllOpenTasks"
                                     : "api/calendar/0/GetMyOpenTasks";
                                break;
                            case 'pastdue':
                                url = $scope.companyView ? "api/calendar/0/GetAllPastDueTasks"
                                    : "api/calendar/0/GetMyPastDueTasks";
                                break;
                            case 'completed':
                                url = $scope.companyView ? "api/calendar/0/GetAllCompletedTasks"
                                    : "api/calendar/0/GetMyCompletedTasks";
                                break;
                            default:
                                if ($scope.companyView == true) url = "api/calendar/0/GetAlltasks";
                        }

                        ////
                        //var url = "api/calendar/0/GetMytasks";
                        //if ($scope.companyView == true) {
                        //    url = "api/calendar/0/GetAlltasks";
                        //}
                        
                        $http.get(url, e).then(function (data) {
                            e.success(data.data);
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "update") {
                            return options;
                        }
                    }
                },
                schema: {
                    model: {
                        id: "TaskId",
                        fields: {
                            TaskId: { editable: false },
                            Subject: { editable: false },
                            Related: { editable: false },
                           // AccountName: { editable: false },                            
                            DueDate: { type: 'date', editable: false },
                            AttendeesNames: { editable: false },
                            Message: { editable: false }
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            sortable: true,            
            filterable: {
                extra: true,                
                operators: {
                    string:{
                        contains:"Contains",
                        eq:"Is equal to",
                        neq:"Is no equal to"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                scrollable:true
            },            
            noRecords: { template: "No records found" },
            columns: [
                { field: "Subject", title: "Subject" },
              //  { field: "LeadName", title: "Lead" },
                {
                    field: "Related",
                    title: "Related",
                    filterable:true,                    
                },
             //   { field: "AccountName", title: "Account" },
                {
                    field: "AttendeesNames",                    
                    title: "Attendees"
                },
                {
                    field: "DueDate", title: "Due",
                    //, template: "#= kendo.toString(kendo.parseDate(DueDate, 'yyyy-MM-dd'), 'M/d/yyyy')#"
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "DashboardDueDateFilterCalendar", offsetvalue)
					, template: function (dataitem) {
					    return HFCService.KendoDateFormat(dataitem.DueDate);
					}
                        //"#= kendo.toString(kendo.parseDate(DueDate), 'yyyy-MM-dd') #"
                },
                { field: "Message", title: "Note" },
                {
                    field: "",
                    title: "",
                    template: function (dataitem) {
                        return TaskMenuTemplate(dataitem);
                    }//,
                    //width: "5%"
                }
            ]

        };        
        $scope.Permission = {};
        var Dashboardpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'DashboardSettings')

        $scope.Permission.EditEvent = Dashboardpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Event').CanAccess
        $scope.Permission.EditTask = Dashboardpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Task').CanAccess
        $scope.Permission.CompanyView = Dashboardpermission.CanRead;//HFCService.GetPermissionTP('Company View').CanAccess;

        $scope.eventSuccessCallback = function (param) {
            //
            var obj = $("#MyAppointmentsGrid");
            if (obj) {
                var appGrid = obj.data("kendoGrid");
                if (appGrid) appGrid.dataSource.read();
            }
            //$("#MyAppointmentsGrid").data("kendoGrid").dataSource.read();
        }

        // old calendar code
        //$scope.editEvent = function (modalId, EventId) {
        //    //
        //    CalendarService.GetEventsFromScheduler(modalId, EventId, $scope.eventSuccessCallback);
        //}

        $scope.PersonService.CallendarSuccessCallback = function () {
            //
            //self.updateTaskGrid();

            var obj = $("#MyTasksGrid");
            if (obj) {
                var tGrid = obj.data("kendoGrid");
                if (tGrid) tGrid.dataSource.read();
            }
        }

        $scope.taskSuccessCallback = function (param) {
            //
            var obj = $("#MyTasksGrid");
            if (obj) {
                var tGrid = obj.data("kendoGrid");
                if (tGrid) tGrid.dataSource.read();
            } 
            //$("#MyTasksGrid").data("kendoGrid").dataSource.read();

        }
        // old calendar code
        //$scope.editTask = function (model) {
        //    //
        //    CalendarService.EditTaskFromCalendarSceduler('newtask', model, true, $scope.taskSuccessCallback);
        //}

        $scope.completeTask = function (moaldId, TaskID) {
            CalendarService.CompleteTask(moaldId, TaskID, $scope.taskSuccessCallback)
            
        }
        $scope.InComplete = function (moaldId, TaskID) {
            CalendarService.InComplete(moaldId, TaskID, $scope.taskSuccessCallback)
        }

        // get task id & make trigger to calander modal
        $scope.getSelectedTaskId = function (taskId) {
            calendarModalService.setPopUpDetails(true, "Task", taskId, $scope.refreshTaskGrids, null, null, true);
        }
        // get event id & make trigger to calander modal
        $scope.getSelectedEventId = function (eventId, dataitem) {
            calendarModalService.setPopUpDetails(true, "Event", eventId, $scope.refreshEventGrids, null, null, true, dataitem.StartDate, dataitem.EndDate);
        }

        $scope.refreshEventGrids = function () {
            var eventGrid = $("#MyAppointmentsGrid").data("kendoGrid");
            if (eventGrid && eventGrid.dataSource) {
                eventGrid.dataSource.read();
            }
        }

        $scope.refreshTaskGrids = function () {
            var taskGrid = $("#MyTasksGrid").data("kendoGrid");
            if (taskGrid && taskGrid.dataSource) {
                taskGrid.dataSource.read();
            }
        }

        function TaskMenuTemplate(dataitem) {
            //
            
            var template = "";

            if (dataitem.CalendarId) {
                template += '<li ng-show="Permission.EditEvent==true"><a href="javascript:void(0)" ng-click="getSelectedEventId(' + dataitem.CalendarId + ', dataItem)">Edit Appointment</a></li>';   // editEvent(\'persongo\', ' + dataitem.CalendarId + ')"
            }

            if (dataitem.TaskId) {
                // NOTE: dataItem, in editTask(dataItem), is a keyword used by Kendo Component , if you change to some other
                // name it will not work.           // revamped
                template += '<li ng-show="Permission.EditTask"><a href="javascript:void(0)" ng-click="getSelectedTaskId(' + dataitem.TaskId + ')">Edit Task</a></li>';   // ng-click="editTask(dataItem) " fujnc(true, 'task', 66)
                template += '<li ng-show="dataItem.completed == null"><a href="javascript:void(0)" ng-click="completeTask(\'persongo\', ' + dataitem.TaskId + ')">Complete</a></li>';
                template += '<li ng-show="dataItem.completed != null"><a href="javascript:void(0)" ng-click="InComplete(\'persongo\', ' + dataitem.TaskId + ')">Incomplete</a></li>';
            }

            if (dataitem.OrderId) {
                // TODO Edit Task
                template += '<li><a href="#!/Orders/' + dataitem.OrderId + '">View Order</a> </li>';
                template += '<li><a href="#!/OpportunityDetail/' + dataitem.OpportunityId + '">View Opportunity</a> </li>';
                template += '<li><a href="#!/Accounts/' + dataitem.AccountId + '">View Account</a> </li>';
                // TODO view measurement.

                //return template;
            } else if (dataitem.OpportunityId) {
                template += '<li><a href="#!/Accounts/' + dataitem.AccountId + '">View Account</a> </li>';
                template += '<li><a href="#!/measurementDetail/' + dataitem.OpportunityId + '/' + dataitem.InstallationAddressId + '">View Measurements</a> </li>';
                template += '<li><a href="#!/OpportunityDetail/' + dataitem.OpportunityId + '">View Opportunity</a> </li>';
            } else if (dataitem.AccountId) {
                template += '<li><a href="#!/Accounts/' + dataitem.AccountId + '">View Account</a> </li>';
                template += '<li><a href="#!/measurementDetails/' + dataitem.AccountId + '">View Measurements</a> </li>';
            } else if (dataitem.LeadId) {
                template += '<li><a href="#!/leads/' + dataitem.LeadId + '">View Lead</a> </li>';
            }
            
            return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                        '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                        '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                        '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
        }

        //$('head title').text("" + applicationTitle);

        //$scope.SuccessCallback = function (param) {
        //    $("#MyTasksGrid").data("kendoGrid").dataSource.read();
        //}
        $scope.newsService.getCurrentNews().then(function (response) {
            var result = response.data;
            if (result) {
                $scope.CurrentNews = result;
            }
        },
            function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });

        // new method setter function

        var init = function () {
            calendarModalService.setTaskGenericRefreshMethod($scope.refreshTaskGrids);
            calendarModalService.setEventGenericRefreshMethod($scope.refreshEventGrids);
        }

        init();

        //});
    }]);

