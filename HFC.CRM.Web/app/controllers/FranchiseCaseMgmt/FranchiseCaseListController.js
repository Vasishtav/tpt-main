﻿'use strict';
app.controller('FranchiseCaseListController',
    ['$scope', '$http', 'NavbarService', 'kendoService', 'HFCService', 'kendotooltipService'
        , function ($scope, $http, NavbarService, kendoService, HFCService, kendotooltipService) {

            $scope.NavbarService = NavbarService;
            $scope.HFCService = HFCService;
            $scope.FranciseLevelVendorMng = HFCService.FranciseLevelVendorMng;
            $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
            $scope.BrandId = HFCService.CurrentBrand;
            $scope.NavbarService.SelectOperation();
            if ($scope.BrandId == 1)
                $scope.NavbarService.EnableVendorManagementTab();
            else
                $scope.NavbarService.EnableCaseMangementTab();

            var offsetvalue = 0;

            $scope.FLCaseTypes = [{ Id: 1, Name: 'Assigned to Me' },
            { Id: 2, Name: 'Cases I Entered' },
            { Id: 3, Name: 'In Process Cases' },
            { Id: 4, Name: 'Cases I Follow' },
            { Id: 5, Name: 'All Cases' }];

            $scope.SelectedCaseType = 1;
            $scope.Closed = false;
            HFCService.setHeaderTitle("Case Management #");

            $scope.Permission = {};
            var Casemanagementpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'CaseManagement ')
            $scope.Permission.ListCaseData = Casemanagementpermission.CanRead;
            $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
            $scope.Permission.CaseEdit = Casemanagementpermission.CanUpdate;

            $scope.FranchiseCaseListGridOptions = {
                cache: false,
                dataSource: {
                    transport: {
                        read: function (e) {
                            $http.get('/api/CaseManageAddEdit/0/GetFranchiseCaseList/?SelectedCaseType=' + $scope.SelectedCaseType + '&Closed=' + $scope.Closed)

                                .then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    HFC.DisplayAlert(error);

                                });
                        }
                    }
                    ,
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    pageSize: 25,
                    // data: $scope.FCList
                    schema: {
                        model: {
                            fields: {
                                CreatedOn: { type: "date" },
                                CaseNo: { type: "number" },
                                VendorCaseNo: { type: "string" },
                                QuoteLineNumber: { type: "string" },
                                MPO: { type: "number" },
                                VPO: { type: "number" },
                            }
                        }
                    }
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    if (kendotooltipService.columnWidth) {
                        kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                    } else if (window.innerWidth < 1280) {
                        kendotooltipService.restrictTooltip(null);
                    }
                    $scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
                },
                columns: [
                    {

                        field: "CaseNo",
                        title: "Case No",
                        width: "120px",
                        filterable: {  search: true },
                        template: function (dataItem) {
                            //return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CaseNo + "</div><span>" + dataItem.CaseNo + "</span></span>";
                            return "<span class='Grid_Textalign'>" + $scope.temp_CaseNo(dataItem) + "</span>";
                        }
                        //, mytooltip : "asfsadf"

                    },
                      {

                          field: "VendorCaseNo",
                          title: "Vendor Case No",
                          width: "120px",
                          filterable: {  search: true },
                          template: function (dataItem) {
                              return "<span class='Grid_Textalign'>" + $scope.VendorCaseNo(dataItem) + "</span>";
                          }
                          //, mytooltip : "asfsadf"

                      },
                    
                       {

                           field: "QuoteLineNumber",
                           title: "Case Line No",
                           width: "120px",
                           filterable: { search: true },
                           template: function (dataItem) {
                               return "<span class='Grid_Textalign'>" + $scope.temp_VendorCaseNo(dataItem) + "</span>";
                           }
                          

                       },
                          {

                              field: "MPO",
                              title: "MPO",
                              width: "120px",
                              filterable: { search: true },
                              template: function (dataItem) {
                                  return "<span class='Grid_Textalign'>" + $scope.temp_MPO(dataItem) + "</span>";
                              }

                          },
                             {

                                 field: "VPO",
                                 title: "VPO",
                                 width: "120px",
                                 filterable: { search: true },
                                 template: function (dataItem) {
                                     return "<span class='Grid_Textalign'>" + $scope.temp_VPO(dataItem) + "</span>";
                                 }

                             },
                                {

                                    field: "Vendor",
                                    title: "Vendor",
                                    width: "200px",
                                    filterable: {  search: true },
                                    template: function (dataItem) {
                                        return $scope.temp_Vendor(dataItem);
                                    }

                                },
                                   {

                                       field: "AccountName",
                                       title: "Account Name",
                                       width: "150px",
                                       filterable: { search: true },
                                       template: function (dataItem) {
                                           return $scope.temp_AccountName(dataItem);
                                       }

                                   },
                                      {

                                          field: "Description",
                                          title: "Description",
                                          template: function (dataItem) {
                                              return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Description + "</div><span>" + dataItem.Description + "</span></span>";
                                          },
                                          width: "120px",
                                          filterable: {
                                              //multi: true,
                                              search: true
                                          }
                                      },
                                         {

                                             field: "StatusName",
                                             title: "Status",
                                             template: function (dataItem) {
                                                 return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.StatusName + "</div><span>" + dataItem.StatusName + "</span></span>";
                                             },
                                             width: "100px",
                                             filterable: { multi:true, search: true }

                                         },
                                            {

                                                field: "TypeName",
                                                title: "Issue Type",
                                                template: function (dataItem) {
                                                    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.TypeName + "</div><span>" + dataItem.TypeName + "</span></span>";
                                                },
                                                width: "150px",
                                                filterable: { multi:true,search: true }
                                            },
                                               {

                                                   field: "CreatedOn",
                                                   title: "Date/Time Opened",
                                                   type: "date",
                                                   filterable: HFCService.gridfilterableDate("date", "CaseFilterCalendar", offsetvalue),
                                                   template: function (dataItem) {
                                                       return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + "</div><span>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + "</span></span>";
                                                   },
                                                   width: "150px",
                                                   // template: "#= kendo.toString(kendo.parseDate(CreatedOn, 'yyyy-MM-dd'), 'M/d/yyyy')#"
                                                   

                                                   //template: "#= kendo.toString(kendo.parseDate(CreatedOn), 'MM/dd/yyyy h:mm tt') #"

                                               },
                                                  {

                                                      field: "CaseOwner",
                                                      title: "Case Owner",
                                                      template: function (dataItem) {
                                                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CaseOwner + "</div><span>" + dataItem.CaseOwner + "</span></span>";
                                                      },
                                                      width: "150px",
                                                      filterable: { multi: true, search: true }
                                                  }, {
                                                      field: "",
                                                      title: "",
                                                      width: "100px",
                                                      template: function (dataItem) {
                                                          if ($scope.Permission.CaseEdit) {
                                                              return '<ul><li class="dropdown note1 ad_user"><div class="dropdown"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="#!/Case/' + dataItem.CaseId + '"  >Edit</a></li></ul></li></ul>';
                                                          }
                                                          else {
                                                              return '';
                                                          }
                                                      }
                                                  }



                ],
                noRecords: { template: "No records found" },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
               // filterable: true,
                sortable: true,
                resizable: true,
                pageable: true,
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                    buttonCount: 5
                },
                resizable: true,

                toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="Gridsearch()" type="search" id="searchBox" placeholder="Search Document" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 no-padding hfc_leadbut"><input type="button" id="btnReset" ng-click="GridSearchClearr()" ng-model="Closed" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div><div class="col-xs-8 col-sm-8 hfc_leadchkbox"><input class="option-input checkbox" name="CaselistClosedCancelled" style="margin-left:10px;" type="checkbox" ng-model="Closed" ng-click="checkbox_clickForCaseLise()"><label class="ldet_label">Show Closed / Canceled</label></div></div></script>').html()),
                columnResize: function (e) {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 330);
                    getUpdatedColumnList(e);
                }
            };
            $scope.FranchiseCaseListGridOptions = kendotooltipService.setColumnWidth($scope.FranchiseCaseListGridOptions);
        
 
            var getUpdatedColumnList = function (e) {
                kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
            }



            var getUpdatedColumnList = function (e) {
                var heightValue = $scope.wrapperHeightValue + "px";
                kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
                $(".k-grid-content.k-auto-scrollable").css("height", heightValue);
            }

            // for kendo tooltip
            $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;

            kendoService.customTooltip = null;
            //kendoService.customTooltip = function (kgFieldName) {

            //    if (kgFieldName == 'VendorCaseNo') return "This is custom: Vendor Case No";
            //    if (kgFieldName == 'CaseNo') return "This is custom: Case No---";

            //    return null;
            //}


            $scope.callAddCase = function () {
                window.location.href = "#!/caseAddEdit";
            }

            $scope.checkbox_clickForCaseLise = function () {
                var grid = $('#FranchiseCaseList').data("kendoGrid");
                grid.dataSource.read();
            }

            //function AttachmentTemplate(dataItem) {
            //    var Div = '';
            //    if (dataItem.AttachmentStreamId && dataItem.AttachmentStreamId != null) {
            //        Div = "<a href='/api/Download?streamid=" + dataItem.AttachmentStreamId + "' target=\"_blank\" style='color: rgb(61,125,139);'><span style='padding: 10px;'>" + dataItem.FileName + "</span></a> ";
            //    }
            //    return Div;
            //}

            $scope.gotokanbanview = function () {

                window.location.href = "#!/KanbanView";
            }
            
           
            $scope.temp_CaseNo = function (dataItem) {
                if (dataItem.CaseNo)
                    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CaseNo + "</div><a href='#!/CaseView/" + dataItem.CaseId + "' style='color: rgb(61,125,139);'>" + dataItem.CaseNo + "</a></span>";
                else
                    return '';

            }

            $scope.temp_VendorCaseNo = function (dataItem) {
                if (dataItem.QuoteLineNumber)
                    return dataItem.QuoteLineNumber;
                else
                    return '';//QuoteLineNumber
            }

            $scope.VendorCaseNo = function (dataItem) {

                if (dataItem.VendorCaseNo && dataItem.ConvertToVendor)
                    return  dataItem.VendorCaseNo ;  // "<span class='tooltip-wrapper'><div class='tooltip-content'>" +  dataItem.VendorCaseNo + "</div><a href='#!/FranchiseVendorView/" + dataItem.CaseId + "/" + dataItem.VendorCaseNumber + "/" + dataItem.VendorId + "' style='color: rgb(61,125,139);'>" + dataItem.VendorCaseNo + "</a>" +  dataItem.VendorCaseNo + "</a></span>";
                     else
                    return '';

            }
            $scope.temp_MPO = function (dataItem) {

                if (dataItem.MPO)
                    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.MPO + "</div><a href='#!/purchasemasterview/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.MPO + "</a></span>";
                    //return "<a href='#!/purchasemasterview/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.MPO + "</a>";
                else
                    return '';

            }
            $scope.temp_VPO = function (dataItem) {

                if (dataItem.VPO)
                    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.VPO + "</div><a href='#!/purchasemasterview/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.VPO + "</a></span>";
                    //return "<a href='#!/purchasemasterview/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.VPO + "</a>";
                else
                    return '';

            }
            $scope.temp_Vendor = function (dataItem) {

                if (dataItem.Vendor)
                    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Vendor + "</div><a href='#!/vendorview/" + dataItem.VendorId + "' style='color: rgb(61,125,139);'>" + dataItem.Vendor + "</a></span>";
                    //return "<a href='#!/vendorview/" + dataItem.VendorId + "' style='color: rgb(61,125,139);'>" + dataItem.Vendor + "</a>";
                else
                    return '';

            }
            $scope.temp_AccountName = function (dataItem) {

                if (dataItem.AccountName)
                    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountName + "</div><a href='#!/Accounts/" + dataItem.AccountId + "' style='color: rgb(61,125,139);'>" + dataItem.AccountName + "</a></span>";
                    //return "<a href='#!/Accounts/" + dataItem.AccountId + "' style='color: rgb(61,125,139);'>" + dataItem.AccountName + "</a>";
                else
                    return '';

            }

            $scope.Gridsearch = function () {
                var searchValue = $('#searchBox').val();
                $("#FranchiseCaseList").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "CaseNo",
                          //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          operator: "eq",
                          value: searchValue
                      },
                      {
                          field: "VendorCaseNo",
                          //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          operator: "contains",
                          value: searchValue
                      },
                      {

                          field: "MPO",
                          //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          operator: "eq",
                          value: searchValue
                      },
                      {

                          field: "VPO",
                          //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          operator: "eq",
                          value: searchValue
                      },
                      {

                          field: "Vendor",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "AccountName",
                          operator: "contains",
                          value: searchValue
                      },
                       {
                           field: "Description",
                           operator: "contains",
                           value: searchValue
                       },
                        {
                            field: "StatusName",
                            operator: "contains",
                            // operator: (value) => (value + "").indexOf(searchValue) >= 0,
                            value: searchValue
                        },
                         {
                             field: "TypeName",
                             operator: "contains",
                             // operator: (value) => (value + "").indexOf(searchValue) >= 0,
                             value: searchValue
                         },
                          {
                              field: "DateTimeOpened",
                              operator: "contains",
                              value: searchValue
                          },
                           {
                               field: "CaseOwner",
                               operator: "contains",
                               value: searchValue
                           },
                            {
                                field: "Id",
                                //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                                operator: "eq",
                                value: searchValue
                            }
                    ]
                });

            }
            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 330);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 330);
                };
            });
            // End Kendo Resizing

            $scope.GridSearchClearr = function () {
                $('#searchBox').val('');
                $("#FranchiseCaseList").data('kendoGrid').dataSource.filter({
                });
            }

        }]);



