﻿app.controller('FranchiseCaseViewController', [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'FranchiseService', 'NavbarService', 'FileUploadService','CalendarService','PersonService','calendarModalService',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, FranchiseService, NavbarService, FileUploadService, CalendarService, PersonService, calendarModalService) {
            $scope.NavbarService = NavbarService;
            $scope.BrandId = HFCService.CurrentBrand;
            $scope.NavbarService.SelectOperation();
            if ($scope.BrandId == 1)
                $scope.NavbarService.EnableVendorManagementTab();
            else
                $scope.NavbarService.EnableCaseMangementTab();

            $scope.CalendarService = CalendarService;
            $scope.PersonService = PersonService;
            $scope.calendarModalService = calendarModalService;
         

            $scope.FileUploadService = FileUploadService;
            $scope.submitted = false;
            $scope.ShowConvert = false;
            $scope.CaseId = $routeParams.CaseId;
            // $scope.CaseNum = $routeParams.Id;

            //enable case
            $scope.HFCService = HFCService;
            $scope.FranciseLevelCase = $scope.HFCService.FranciseLevelCase;
            $scope.Permission = {};
            var Casemanagementpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'CaseManagement ')
            $scope.Permission.ViewCaseData = Casemanagementpermission.CanRead;
            $scope.Permission.CaseEdit= Casemanagementpermission.CanUpdate;

            if ($scope.CaseId) {
                $scope.FileUploadService.SetCaseId($scope.CaseId, 'FranchiseCase');
            }
            else $scope.FileUploadService.clearCaseId();

            $scope.Hide_SeltoVendor = false;

            $scope.forComments = { id: $scope.CaseId, refId: 0, module: 1 };  // module : 1 for case view page

            HFCService.setHeaderTitle("CaseView #");






            /// TinyMCE Editor

            $scope.tinymceModel = 'Initial content';

            $scope.getContent = function () {
                console.log('Editor content:', $scope.tinymceModel);
            };

            $scope.setContent = function () {
                $scope.tinymceModel = 'Time: ' + (new Date());
            };

            $scope.tinymceOptions = {
                plugins: 'link image code',
                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | paste',
                paste_data_images: true
            };

            ////

            // follow enable disable and get

            $http.get('/api/CaseManageAddEdit/' + $scope.CaseId + '/GetFollowOrNot').then(function (data) {
                $scope.Follow = data.data;
            })

            // for related tab


            if (window.location.href.includes('/CaseView')) {
                
                $scope.FileUploadService.css1 = "fileupload_ltgrid";
                $scope.FileUploadService.css2 = "";
                $scope.FileUploadService.css3 = "fileupload_rtgrid";
            }


            $scope.RelatedEventsGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.CaseId) {
                                var url = "api/calendar/" + $scope.CaseId + "/GetEventsForCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },
                        
                    },
                  
                },
                batch: true,
                cache: false,
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },              
                noRecords: { template: "No events found" },
                columns: [
                    { field: "Subject", title: "Subject" },
                    { field: "AttendeesNames", title: "Attendees" },
                    {
                        field: "StartDate",
                        title: "Start",
                        //template: "#= kendo.toString(kendo.parseDate(StartDate), 'MM/dd/yyyy hh:mm tt') #"
                        template: function (dataitem) {
                            if (dataitem.StartDate)
                                return StartDateValue(dataitem);
                            else return "";
                        }
                    },
                     {
                         field: "EndDate", title: "End",
                         //template: "#= kendo.toString(kendo.parseDate(EndDate), 'MM/dd/yyyy hh:mm tt') #"
                         template: function (dataitem) {
                             if (dataitem.EndDate)
                                 return EndDateValue(dataitem);
                             else return "";
                         }
                     },
                     {
                         field: "",
                         title: "",
                         template: function (dataitem) {
                             return EventMenuTemplate(dataitem);
                         }//,
                         //width: "5%"
                     }
                ],
              

            };

            function EventMenuTemplate(dataitem) {
                var template = "";
                template += '<li><a href="javascript:void(0)" ng-click="editCaseEvent(' + dataitem.CalendarId + ');">Edit</a></li>';           // editEvent(\'persongo\', ' + dataitem.CalendarId + ')

                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                       '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                       '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                       '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }

            function StartDateValue(dataitem) {
                var template = "";
                if (dataitem.IsAllDay == true) {
                    template = HFCService.KendoDateFormat(dataitem.StartDate)
                    //template = kendo.toString(kendo.parseDate(dataitem.StartDate), "MM/dd/yyyy")
                    return template;
                }
                else 
                    return template = HFCService.KendoDateTimeFormat(dataitem.StartDate)
            }

            function EndDateValue(dataitem) {
                var template = "";
                if (dataitem.IsAllDay == true) {
                    template = HFCService.KendoDateFormat(dataitem.EndDate)
                   // template = kendo.toString(kendo.parseDate(dataitem.EndDate), "MM/dd/yyyy")
                    return template;
                }
                else return template = HFCService.KendoDateTimeFormat(dataitem.EndDate)

            }

            $scope.eventSuccessCallback = function (param) {
                if ($("#MyAppointmentsGrid1").data("kendoGrid").dataSource)
                    $("#MyAppointmentsGrid1").data("kendoGrid").dataSource.read();
            }

            // trigger calendar for edit
            $scope.editCaseEvent = function (id) {
                $scope.calendarModalService.setPopUpDetails(true, 'Event', id, $scope.eventSuccessCallback, null, null, true);
            }
            // old calendar code
            //$scope.editEvent = function (modalId, EventId) {
            //    $scope.CalendarService.GetEventsFromScheduler(modalId, EventId, $scope.eventSuccessCallback, null, null, true);
            //}


            // Tasks operations
            $scope.RelatedTasksGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.CaseId) {
                                var url = "api/calendar/" + $scope.CaseId + "/GetTasksForCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },                    
                    },
             
                },
                batch: true,
                cache: false,
                //pageable: {
                //    refresh: true,
                //    pageSize: 25,
                //    pageSizes: [25, 50, 100, 'All'],
                //    buttonCount: 5
                //},
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                noRecords: { template: "No tasks found" },
                columns: [
                    { field: "Subject", title: "Subject" },
                    { field: "AttendeesName", title: "Attendees" },
                    {
                        field: "DueDate",
                        title: "Due",
                        template: function (dataItem) {
                            if (dataItem.DueDate) {
                                var dueDate = (dataItem.DueDate).split("T")[0];
                                return "<span>" + HFCService.KendoDateFormat(dataItem.DueDate) + "</span>";
                            } else {
                                return '';
                            }
                        }
                    },
                     {
                         field: "Message", title: "Note"
                     },
                     {
                         title: "Status",
                         template: function (dataItem) {
                             if (dataItem.CompletedDate)
                                 return 'Completed';
                             else
                                 return 'Incomplete';
                         }
                     },
                     {
                         field: "",
                         title: "",
                         template: function (dataitem) {
                             return EventTaskTemplates(dataitem);
                         }//,
                         //width: "5%"
                     }
                ],


            };

            function EventTaskTemplates(dataitem) {
                var template = "";
                template += '<li><a href="javascript:void(0)" ng-click="editCaseTask(' + dataitem.TaskId + ')">Edit</a></li>';
                template += '<li ng-show="dataItem.CompletedDate == null"><a href="javascript:void(0)" ng-click="CalendarService.CompleteTask(\'persongo\', ' + dataitem.TaskId + ')">Complete</a></li>';
                template += '<li ng-show="dataItem.CompletedDate != null"><a href="javascript:void(0)" ng-click="CalendarService.InComplete(\'persongo\', ' + dataitem.TaskId + ')">Incomplete</a></li>';


                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                       '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                       '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                       '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }

            // trigger calendar for task edit
            $scope.editCaseTask = function (id) {
                $scope.calendarModalService.setPopUpDetails(true, 'Task', id, $scope.taskSuccessCallback);
            }

           
            $scope.AddTasksForCase = function () {
                NavbarService.addTask();
            }

            $scope.taskSuccessCallback = function (param) {
                var grid = $("#MyTasksGrid1").data("kendoGrid");
                if (grid)
                    $("#MyTasksGrid1").data("kendoGrid").dataSource.read();
            }
            $scope.calendarModalService.setTaskGenericRefreshMethod($scope.taskSuccessCallback);

            // old calendar code
            //$scope.editTask = function (model) {
            //    $scope.CalendarService.EditTaskFromCalendarSceduler('newtask', model, true, $scope.taskSuccessCallback);
            //}



            // case history
            $scope.RelatedCaseHistoryGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.CaseId) {
                                var url = "api/CaseManageAddEdit/" + $scope.CaseId + "/GetCaseHistoryForCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },
                    },

                },
                batch: true,
                cache: false,
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                pageable: {
                  //  refresh: true,
                    pageSize: 10,
                    pageSizes: [10,20, 30,50, 100],
                    buttonCount: 5
                },
                scrollable: true,
                noRecords: { template: "No case history found" },
                columns: [
                    {
                        field: "LastUpdatedOn", title: "Date",
                       // template: "#= kendo.toString(kendo.parseDate(Date), 'yyyy-MM-dd') #"
                        //  template: "#= kendo.toString(kendo.parseDate(Date), 'yyyy-MM-dd hh:mm tt') #"
                        //template: function (dataitem) {
                        //    return HFCService.KendoDateTimeFormat(dataitem.LastUpdatedOn);
                        //}
                        template: "#= kendo.toString(kendo.parseDate(LastUpdatedOn), 'MM/dd/yyyy hh:mm tt') #"
                    },
                    {
                        field: "Field",
                        title: "Field",
                        template: function (dataitem) {
                            
                            if (dataitem.Field == 'VPO_PICPO_PODId')
                                return '<div>VPO</div>'
                            else if (dataitem.Field == 'MPO_MasterPONum_POId')
                                return '<div>MPO</div>'
                            else if (dataitem.Field == 'SalesOrderId')
                                return '<div>Sales</div>'
                            else if (dataitem.Field == 'Description')
                                return '<div>Desc</div>'
                            else
                                return '<div>' + dataitem.Field + '</div>'
                             }
                    },
                    { field: "UserName", title: "User" },
                    { field: "OriginalValue", title: "Original Value" },
                    { field: "NewValue", title: "New Value" }
                ],
            };


            // refresh task grid
            $scope.PersonService.CallendarSuccessCallback = function () {
                $scope.taskSuccessCallback();
            }


    
            // end of related tab

            $scope.CaseAddEdit = {
                Name: '',
                Status: '',
                CreatedOn: '',
                DateTimeOpened: '',
                DateTimeClosed: '',
                PurchaseOrdersDetailId: '',
                CellPhone: '',
                PurchaseOrderId: '',
                Email: '',
                OrderId: '',
                AccountName: '',
                SideMark: '',
                CreatedOn: '',
                VPO_PICPO_PODId: '',
                MPO_MasterPONum_POId: '',
                SalesOrderId: '',
                IncidentDate: '',
                DateTimeClosed: '',
                LastUpdatedOn: '',
                Description: '',
                LastUPdatedName: '',
            }

            //get data on inital page load
            $scope.Initialvalue = function () {
                $http.get('/api/CaseManageAddEdit/0/GetUserDetails/').then(function (response) {
                    //
                    $scope.CaseAddEdit.Name = response.data.Name;
                    $scope.CaseAddEdit.CreatedOn = response.data.CreatedOn;
                    $scope.CaseAddEdit.DateTimeOpened = response.data.CreatedOn;
                    $scope.CaseAddEdit.Status = "New";
                    $scope.CaseAddEdit.DateTimeClosed = response.data.DateTimeClosed;
                });
            }

          //  $scope.Initialvalue();
            //on select date update date time open
            $scope.ChangeDate = function (value) {
                $scope.CaseAddEdit.DateTimeOpened = value;
            }

            
            //grid code.
            $scope.CaseinfoGridOptions = {
                dataSource: {
                    //type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            
                            if ($scope.CaseId == null || $scope.CaseId == "" || $scope.CaseId == undefined) {
                                var id_val = 0;
                            }
                            else id_val = $scope.CaseId;
                            //$http.get("/api/CaseManageAddEdit/" + 0 + "/GetCase?MPOId=" + 0 + "&SalesorderId=" + 0, e)
                            $http.get("/api/CaseManageAddEdit/" + 0 + "/GetCase?CaseId=" + id_val, e)
                                .then(function (data) {
                                     
                                    $scope.ShowConvert = false;
                                    for (i = 0; i < data.data.length; i++) {
                                        if ((data.data[i].ConvertToVendor == false || data.data[i].ConvertToVendor == null) && data.data[i].ProductTypeId != 3 && data.data[i].ProductTypeId != 4 && data.data[i].ProductTypeId != 5)
                                            $scope.ShowConvert = true;
                                    }
                                    e.success(data.data);
                                    
                                    
                                }).catch(function (error) {
                                    HFC.DisplayAlert(error);
                                    //var loadingElement = document.getElementById("loading");
                                    //loadingElement.style.display = "none";

                                });
                        },
                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                QuoteLineNumber: { editable: false },
                                ProductName: { editable: false },
                                ProductId: { editable: false },
                                Description: { editable: false },
                                VendorName: { editable: false },
                                Typevalue: { editable: false },
                                CaseReason: { editable: false },
                                IssueDescription: { editable: false },
                                ExpediteReq: { type: "boolean", editable: false },
                                ExpediteApproved: { type: "boolean", editable: false },
                                ReqTripCharge: { type: "boolean", editable: false },
                                TripChargeApprovedName: { editable: false },
                                StatusName: { editable: false },
                                ResolutionName: { editable: false },
                                VendorReference: { editble: false }
                            }
                        }
                    },
                },

                //autoSync: true,
                batch: true,
                cache: false,
        
                sortable: true,
                filterable: true,
                resizable: true,
                editable: "inline",
                detailInit: detailinitParent,
                //dataBound: function (e) {
                //    $scope.quoteLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource._data.length;
                //},
                noRecords: { template: "No records found" },
                columns: [
                     {
                         field: "Id",
                         title: "Id",
                         filterable: { multi: true, search: true },
                         hidden: true,
                     },
                         {

                             field: "VendorCaseNo",
                             title: "Vendor Case No",
                             width: "120px",
                             filterable: { multi: true, search: true },
                             template: function (dataItem) {
                                 return "<span class='Grid_Textalign'>" + $scope.VendorCaseNo(dataItem) + "</span>";
                             }
                             //, mytooltip : "asfsadf"

                         },
                    
                     {
                         field: "VPO",
                         title: "VPO",
                         width: "200px",
                         //template: function (dataItem) {
                         //    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.VPO + "</div><span>" + dataItem.VPO + "</span></span>";
                         //},
                         template: function(dataItem){
                             return "<span class='Grid_Textalign'>" + $scope.temp_VPO(dataItem) + "</span>";
                         },
                         filterable: { multi: true, search: true },
                         hidden: false,
                     },
                     {
                         field: "QuoteLineNumber",
                         title: "Line",
                         width: "120px",
                         filterable: { multi: true, search: true },
                         template: function (dataItem) {
                             return "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                         },
                         hidden: false,
                         editor: function (container, options) {

                             $('<input required=true id="LineNumberDropdown" name="QuoteLineId" data-bind="value:QuoteLineId" style="border: none;"  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: true,
                                      optionLabel: "Select",

                                      valuePrimitive: false,
                                      dataTextField: "QuoteLineNumber",
                                      dataValueField: "QuoteLineId",
                                      template: "#=QuoteLineNumber #",

                                      change: function (e, options) {
                                          var item = $('#caseGrid01').find('.k-grid-edit-row');
                                          item.data().uid
                                          var dataItem = item.data().$scope.dataItem;
                                          onChangeValue(dataItem.QuoteLineId.QuoteLineId);
                                      },

                                      dataSource: {
                                          transport: {
                                              read: {

                                                  url: function (readparam) {

                                                      if ($scope.CaseAddEdit.PurchaseOrdersDetailId != undefined &&
                                                          $scope.CaseAddEdit.PurchaseOrdersDetailId != "" &&
                                                          $scope.CaseAddEdit.PurchaseOrdersDetailId != null) {
                                                          var id = $scope.CaseAddEdit.PurchaseOrdersDetailId;
                                                      }
                                                      else id = 0;
                                                      if ($scope.CaseAddEdit.PurchaseOrderId != undefined &&
                                                          $scope.CaseAddEdit.PurchaseOrderId != "" &&
                                                          $scope.CaseAddEdit.PurchaseOrderId != null) {
                                                          var Mpoid = $scope.CaseAddEdit.PurchaseOrderId;
                                                      }
                                                      else Mpoid = null;
                                                      var url1 = '/api/CaseManageAddEdit/' + id + '/GetLineData?Mpoid= ' + Mpoid;
                                                      return url1;
                                                  }
                                              }
                                          },
                                      }
                                  });

                             var prodctTypedd = $("#LineNumberDropdown").data("kendoDropDownList");
                             prodctTypedd.dataSource.read();
                         }
                     },
                      {
                          field: "ProductName",
                          title: "Product",
                          width: "200px",
                          editor: '<input id="product_txt"  readonly name="ProductName" class="k-textbox " data-bind="value:ProductName" style="border: none "/>',
                          filterable: { multi: true, search: true },
                          hidden: true,
                      },
                      {
                          field: "ProductId",
                          title: "Product No",
                          width: "200px",
                          template: function (dataItem) {
                              return "<span class='Grid_Textalign'>" + dataItem.ProductId + "</span>";
                          },
                          editor: '<input id="productnumber_txt" readonly data-type="text"  class="k-textbox "  name="ProductId" data-bind="value:ProductId"/>',
                          filterable: { multi: true, search: true },
                          hidden: true,
                      },
                      {
                          field: "Description",
                          title: "Product Description",
                          width: "220px",
                          attributes: {

                              style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                          },
                        //  editor: '<input id="productdescri_txt" readonly data-type="text"  class="k-textbox" name="Description" data-bind="value:Description"/>',
                          filterable: { multi: true, search: true },
                          template: function (dataitem) {
                              return ProductDescription(dataitem);
                          },
                          hidden: true,
                      },
                      {
                          field: "VendorName",
                          title: "Vendor",
                          width: "200px",
                         // editor: '<input id="Vendor_txt" readonly data-type="text"  class="k-textbox "  name="VendorName" data-bind="value:VendorName"/>',
                          filterable: { multi: true, search: true },
                          template: function (dataItem) {
                           return $scope.VendorLink(dataItem);
                          },
                          hidden: false,
                      },
                      {
                          field: "Typevalue",
                          title: "Issue Type",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          editor: function (container, options) {
                              $('<input required=true   name="Type" data-bind="value:Type" style="border: none "  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: false,
                                      optionLabel: "Select",

                                      valuePrimitive: true,
                                      dataTextField: "Typevalue",
                                      dataValueField: "Type",
                                      template: "#=Typevalue #",

                                      dataSource: {
                                          transport: {
                                              read: {
                                                  url: '/api/CaseManageAddEdit/0/GetCaseType?Tableid=' + 5,
                                              }
                                          },
                                      }

                                  });
                          }


                      },
                      {
                          field: "CaseReason",
                          title: "Case Reason",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          editor: function (container, options) {
                              $('<input required=true   name="ReasonCode" data-bind="value:ReasonCode" style="border: none "  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: false,
                                      optionLabel: "Select",

                                      valuePrimitive: true,
                                      dataTextField: "Reasonvalue",
                                      dataValueField: "ReasonCode",
                                      template: "#=Reasonvalue #",

                                      dataSource: {
                                          transport: {
                                              read: {
                                                  url: '/api/CaseManageAddEdit/0/GetCaseReason?Tableid=' + 6,
                                              }
                                          },
                                      }

                                  });
                          }


                      },
                     {
                         field: "IssueDescription",
                         title: "Issue Description",
                         width: "150px",
                         filterable: { multi: true, search: true },
                         hidden: true,
                         editor: '<input   required=true name="IssueDescription"  class="k-textbox" data-bind="value:IssueDescription" style="border: none "/>',

                     },
                    {
                        field: "ExpediteReq",
                        title: "Expedite Req",
                        width: "150px",
                        filterable: { multi: true, search: true },
                        template: '<input type="checkbox"  #= ExpediteReq ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                    },
                      //{
                      //    field: "ExpediteApproved",
                      //    title: "Expedite Approved",
                      //    width: "150px",
                      //    filterable: { multi: true, search: true },
                      //    template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                      //},
                       {
                           field: "ReqTripCharge",
                           title: "Req Trip Charge",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           template: '<input type="checkbox" #= ReqTripCharge ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                       },
                       //{
                       //    field: "TripChargeApprovedName",
                       //    title: "Approved Trip Charge",
                       //    width: "200px",
                       //    editor: '<input id="TripChargeApprovedName_txt"  readonly name="TripChargeApprovedName" data-bind="value:TripChargeApprovedName" style="border: none "/>',
                       //    filterable: { multi: true, search: true },
                       //},
                       //{
                       //    field: "StatusName",
                       //    title: "Status",
                       //    width: "150px",
                       //    filterable: { multi: true, search: true },
                       //    hidden: false,
                       //    editor: function (container, options) {
                       //        $('<input required=true   name="Status" data-bind="value:Status" style="border: none "  />')
                       //            .appendTo(container)
                       //            .kendoDropDownList({
                       //                autoBind: false,
                       //                optionLabel: "Select",

                       //                valuePrimitive: true,
                       //                dataTextField: "Statusvalue",
                       //                dataValueField: "Status",
                       //                template: "#=Statusvalue #",

                       //                dataSource: {
                       //                    transport: {
                       //                        read: {

                       //                            url: '/api/CaseManageAddEdit/0/GetCaseStatus?Tableid=' + 7,
                       //                        }
                       //                    },
                       //                }

                       //            });
                       //    }


                       //},
                       //{
                       //    field: "ResolutionName",
                       //    title: "Resolution",
                       //    width: "150px",
                       //    filterable: { multi: true, search: true },
                       //    hidden: false,
                       //    editor: function (container, options) {
                       //        $('<input required=true  name="Resolution" data-bind="value:Resolution" style="border: none "  />')
                       //            .appendTo(container)
                       //            .kendoDropDownList({
                       //                autoBind: false,
                       //                optionLabel: "Select",

                       //                valuePrimitive: true,
                       //                dataTextField: "Resolutionvalue",
                       //                dataValueField: "Resolution",
                       //                template: "#=Resolutionvalue #",

                       //                dataSource: {
                       //                    transport: {
                       //                        read: {

                       //                            url: '/api/CaseManageAddEdit/0/GetCaseResolution?Tableid=' + 8,
                       //                        }
                       //                    },
                       //                }

                       //            });
                       //    }


                       //},
                       // {
                       //     field: "VendorReference",
                       //     title: "Vendor Reference",
                       //     width: "120px",
                       //     filterable: { multi: true, search: true },
                       //     template: function (dataitem) {
                       //         if (dataitem.VendorReference)
                       //             return dataitem.VendorReference;
                       //         else
                       //             return '';
                       //     },
                       //     hidden: false,
                       // },
                        {
                            field: "",
                            title: "",
                            width: "60px",
                            // template: '<ul><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="DeleteRowDetails(dataItem)">Edit</a></li>  <li><a  href="javascript:void(0)" ng-click="DeleteRowDetails(this.dataItem)">Delete</a></li><li><a href="javascript:void(0)"  ng-click="CopyData(dataItem)">Copy</a></li> </ul> </li> </ul>'
                            template: function (dataItem) {
                                if (!dataItem.ConvertToVendor && dataItem.ProductTypeId != 3 && dataItem.ProductTypeId != 4 && dataItem.ProductTypeId != 5) {
                                    var hcontent = '<a href="" data-placement="top" ng-click="VendorCaseEdit(this.dataItem)" data-tooltip="Send to Vendor"><i class="fas fa-exchange-alt"></i></a>'
                                    return hcontent;
                                }
                                else
                                    return '';
                                
                            }
                        },

                ]

            };

            // detailed lines
            function detailinitParent(e) {
                var grdId = "grd" + e.data.QuoteLineId;
                $("<div id = '" + grdId + "' class='sub-grid' />").appendTo(e.detailCell).kendoGrid({
                    dataSource: [e.data],
                    columns: [
                         {
                             field: "ExpediteApproved",
                             title: "Expedite Approved",
                             width: "150px",
                             filterable: { multi: true, search: true },
                             template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                         },
                           {
                               field: "TripChargeApprovedName",
                               title: "Approved Trip Charge",
                               width: "150px",
                               template: function (dataItem) {
                                   if (dataItem.TripChargeApprovedName === "Approved") {
                                       if (dataItem.Amount) {
                                           {
                                               const formatter = new Intl.NumberFormat('en-US', {
                                                   style: 'currency',
                                                   currency: 'USD',
                                                   minimumFractionDigits: 2
                                               })

                                              return formatter.format(dataItem.Amount);
                                           }
                                       }
                                       else
                                           return '';
                                   }
                                   else {
                                       if (dataItem.TripChargeApprovedName)
                                           return dataItem.TripChargeApprovedName;
                                       else
                                           return '';
                                   }
                               },
                               editor: '<input id="TripChargeApprovedName_txt"  readonly name="TripChargeApprovedName" data-bind="value:TripChargeApprovedName" style="border: none "/>',
                               filterable: { multi: true, search: true },
                           },
                              {
                                  field: "StatusName",
                                  title: "Status",
                                  width: "150px",
                                  filterable: { multi: true, search: true },
                                  hidden: false,
                                  editor: function (container, options) {
                                      $('<input required=true   name="Status" data-bind="value:Status" style="border: none "  />')
                                          .appendTo(container)
                                          .kendoDropDownList({
                                              autoBind: false,
                                              optionLabel: "Select",

                                              valuePrimitive: true,
                                              dataTextField: "Statusvalue",
                                              dataValueField: "Status",
                                              template: "#=Statusvalue #",

                                              dataSource: {
                                                  transport: {
                                                      read: {

                                                          url: '/api/CaseManageAddEdit/0/GetCaseStatus?Tableid=' + 7,
                                                      }
                                                  },
                                              }

                                          });
                                  }


                              },
                       {
                           field: "ResolutionName",
                           title: "Resolution",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           editor: function (container, options) {
                               $('<input required=true  name="Resolution" data-bind="value:Resolution" style="border: none "  />')
                                   .appendTo(container)
                                   .kendoDropDownList({
                                       autoBind: false,
                                       optionLabel: "Select",

                                       valuePrimitive: true,
                                       dataTextField: "Resolutionvalue",
                                       dataValueField: "Resolution",
                                       template: "#=Resolutionvalue #",

                                       dataSource: {
                                           transport: {
                                               read: {

                                                   url: '/api/CaseManageAddEdit/0/GetCaseResolution?Tableid=' + 8,
                                               }
                                           },
                                       }

                                   });
                           }


                       },
                        {
                            field: "VendorReference",
                            title: "Vendor Reference",
                            width: "150px",
                            //  editor: '<input id="productdescri_txt" readonly data-type="text"  class="k-textbox" name="Description" data-bind="value:Description"/>',
                            filterable: { multi: true, search: true },
                            template: function (dataitem) {
                                if (dataitem.VendorReference)
                                    return dataitem.VendorReference;
                                else
                                    return '';
                            },
                            hidden: false,
                        },
                        //{ field: "VendorName", title: "Vendor" },
                        //{ field: "Description", title: "Item Description", template: '#=Description#' },
                        //{ field: "SuggestedResale", title: "Suggested Resale", format: "{0:c}" },
                        //{ field: "UnitPrice", title: "Unit Price", format: "{0:c}" },
                        //{ field: "Discount", title: "Discount", template: '#=kendo.format("{0:p}", Discount / 100)#' },
                    ],
                });
            }

            const formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2
            })

            $scope.VendorCaseNo = function (dataItem) {
                //if (dataItem.VendorCaseNo && dataItem.ConvertToVendor)
                //    return dataItem.VendorCaseNo;
                //else
                //    return '';
                // DOnt remove this commented code
                if (dataItem.VendorCaseNo && dataItem.ConvertToVendor)
                    return dataItem.VendorCaseNo    // return "<a href='#!/FranchiseVendorView/" + dataItem.CaseId + "/" + dataItem.VendorCaseNumber + "/" + dataItem.VendorId + "' style='color: rgb(61,125,139);'>" + dataItem.VendorCaseNo + "</a>";
                else
                    return '';

            }

            $scope.temp_VPO = function (dataItem) {

                if (dataItem.VPO && dataItem.VPO != 0)
                    return "<a href='#!/purchasemasterview/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.VPO + "</a>";
                else
                    return dataItem.VPO;

            }

            $scope.VendorLink = function (dataItem) {

                if (dataItem.VendorId && dataItem.VendorId != 0 && dataItem.VendorName)
                    return "<a href='#!/vendorview/" + dataItem.VendorId + "' style='color: rgb(61,125,139);'>" + dataItem.VendorName + "</a>";
                else
                    return '';

            }

            function ProductDescription(dataitem) {
                var template = "";
                // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
                var erer = dataitem.Description.replace('"', "");
                erer = erer.replace(/<[^>]*>/g, '');
                template += '<span title="' + erer + '">' + dataitem.Description + '</span>'

                return template;
            }

      
            //has case id get saved data
            if ($scope.CaseId > 0) {
                
                var Id = $scope.CaseId;
                $http.get('/api/CaseManageAddEdit/' + Id + '/GetSavedValue').then(function (response) {
                    var data = response.data;
                    if (response.data.CaseId != 0) {
                        $scope.CaseAddEdit = response.data;
                        
                        $scope.CaseAddEdit.PurchaseOrdersDetailId = $scope.CaseAddEdit.VPO_PICPO_PODId;
                        $scope.CaseAddEdit.PurchaseOrderId = $scope.CaseAddEdit.MPO_MasterPONum_POId;
                       // $scope.CaseAddEdit.OrderId = $scope.CaseAddEdit.SalesOrderId;
                        $scope.CaseAddEdit.OrderId = $scope.CaseAddEdit.SalesOrderId.split(',');
                        $scope.CaseAddEdit.SalesorderNumber = $scope.CaseAddEdit.SalesorderNumber.split(',');

                        $scope.CaseAddEdit.Email = $scope.CaseAddEdit.PrimaryEmail;
                        //$scope.CaseAddEdit.Status = "New";
                        $scope.CaseAddEdit.Status = $scope.CaseAddEdit.StatusValue;
                        $scope.PreviousValues = response.data;
                        
                       
                    }

                })
            }

            $scope.EditCase = function()
            {
                window.location.href = "#!/Case/"+$scope.CaseId
            }

            $scope.UnfollowThisCase = function()
            {
                $http.get('/api/CaseManageAddEdit/' + $scope.CaseId + '/UnfollowThisCase?module=1').then(function (response) {
                    $scope.Follow = response.data;
                });
            }

            $scope.FollowThisCase = function()
            {
                $http.get('/api/CaseManageAddEdit/' + $scope.CaseId + '/FollowThisCase?module=1').then(function (response) {
                    $scope.Follow = response.data;
                });
            }


            $scope.VendorCaseEdit = function (Value) {
                
                $scope.Linedetail = Value;
                $http.post('/api/CaseManageAddEdit/0/SaveVendorCase', $scope.Linedetail).then(function (response) {
                    
                    if (response.data) {
                         
                        var value_res = response.data;
                        $scope.CaseAddEdit.StatusValue = response.data;
                        //window.location.href = "#!/VendorcaseEdit/" + value_res;
                        //window.location.href = "#!/VendorCaseList";
                        $('#caseGrid01').data('kendoGrid').dataSource.read();
                        $('#caseGrid01').data('kendoGrid').refresh();


                        var gridDS = $('#caseGrid01').data('kendoGrid').dataSource._data;

                        for (var i = 0 ; i < gridDS.length; i++) {

                            if ((gridDS[i].ConvertToVendor === null || gridDS[i].ConvertToVendor === '' || gridDS[i].ConvertToVendor === undefined || gridDS[i].ConvertToVendor === 0) && gridDS[i].ProductTypeId != 3 && gridDS[i].ProductTypeId != 4 && gridDS[i].ProductTypeId != 5) {
                                $scope.ShowConvert = true;
                            }
                        }

                        
                        return;
                        
                    }
                    else {
                        HFC.DisplayAlert(response.data);
                        return;
                    }
                    //window.location.href = "#!/VendorcaseEdit/" + $scope.Linedetail.CaseId + "/" + $scope.Linedetail.Id;
                })
                
                
            }

            $scope.ConvertWhole = function () {
                
                var value = $('#caseGrid01').data('kendoGrid')._data;
                $scope.Completedetails = value;
                $http.post('/api/CaseManageAddEdit/0/SaveVendorCaseComplete', $scope.Completedetails).then(function (response) {
                    
                    if (response.data) {
                        
                        var value_res = response.data;

                            $('#caseGrid01').data('kendoGrid').dataSource.read();
                            $('#caseGrid01').data('kendoGrid').refresh();
                            $scope.ShowConvert = false;
                            $scope.CaseAddEdit.StatusValue = response.data;
                       
                        return;
                    }
                    else {
                        HFC.DisplayAlert(response.data);
                        return;
                    }
                    //window.location.href = "#!/VendorcaseEdit/" + $scope.Linedetail.CaseId + "/" + $scope.Linedetail.Id;
                });
            }
        }]);
