﻿'use strict';
app.controller('KanbanController',
    [
        '$scope', 'NavbarService', '$http', '$window','HFCService',
        function ($scope, NavbarService, $http, $window, HFCService) {


            $scope.NavbarService = NavbarService;
            $scope.HFCService = HFCService;
            $scope.FranciseLevelVendorMng = HFCService.FranciseLevelVendorMng;
            $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
            $scope.BrandId = HFCService.CurrentBrand;
            $scope.NavbarService.SelectOperation();
            if ($scope.BrandId == 1)
                $scope.NavbarService.EnableVendorManagementTab();
            else
                $scope.NavbarService.EnableCaseMangementTab();

            $scope.FLCaseTypes = [{ Id: 1, Name: 'Assigned to Me' },
                                 { Id: 2, Name: 'Cases I Entered' },
                                 { Id: 3, Name: 'In Process Cases' },
                                 { Id: 4, Name: 'Cases I Follow' },
                                 { Id: 5, Name: 'All Cases' }];
            $scope.SelectedCaseType = 1;

            $scope.Closed = false;
            //enable case
            $scope.FranciseLevelCase = HFCService.FranciseLevelCase;

            $scope.Permission = {};
            var Casemanagementpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'CaseManagement ')
            $scope.Permission.ListCaseData = Casemanagementpermission.CanRead;
            $scope.FranciseLevelCase = HFCService.FranciseLevelCase;
            $scope.Permission.CaseEdit = Casemanagementpermission.CanUpdate;

            $scope.GetKanbanCase = function(){
                $http.get('/api/CaseManageAddEdit/0/GetKanbanData/?SelectedCaseType=' + $scope.SelectedCaseType + '&Closed=' + $scope.Closed)
               .then(function (data) {
                   
                   $scope.KanbanDataList = [];
                   $scope.KanbanData = data.data;
                   for (var i = 0; i < $scope.KanbanData.length; i++) {
                       $scope.kanbanValue = {
                           id: '',
                           resourceId: 0,
                           state: '',
                           label: '',
                           tags: '',
                           hex: ''
                       }
                       $scope.kanbanValue.id = $scope.KanbanData[i].Id;
                       $scope.kanbanValue.resourceId = $scope.KanbanData[i].CaseId;
                       $scope.kanbanValue.state = $scope.KanbanData[i].StatusName;
                       $scope.kanbanValue.label = '<ul style="line-height:20px; font-size:12px; color:#555;" >' +
                                                         '<li><a style="color:#3e7d8a;" href="#!/CaseView/' + $scope.KanbanData[i].CaseId + '">' + $scope.KanbanData[i].CaseNumber + '-' + $scope.KanbanData[i].VendorCaseNumber + ' </a></li>' +
                                                         '<li><span style="color:#555; font-weight:600;">Account Name:</span> <a style="color:#3e7d8a;" href="#!/Accounts/' + $scope.KanbanData[i].AccountId + '">' + $scope.KanbanData[i].AccountName + '</a></li> ' +
                                                         '<li><span style="color:#555; font-weight:600;">Description:</span> ' + $scope.KanbanData[i].Description + '</li>' +
                                                         '<li><span style="color:#555; font-weight:600;">Issue Type:</span> ' + $scope.KanbanData[i].Type + '</li>' +
                                                         '<li><span style="color:#555; font-weight:600;">Age:</span> ' + $scope.KanbanData[i].Age + 'Days' + '</li>'

                                                 + '</ul>';
                       $scope.kanbanValue.tags = $scope.KanbanData[i].tags;
                       $scope.kanbanValue.hex = $scope.KanbanData[i].UserColor;

                       $scope.KanbanDataList.push($scope.kanbanValue);
                   }
                   if (data.data.length == 0) {
                       $scope.kanbanValue = {
                           id: '',
                           resourceId: 0,
                           state: '',
                           label: '',
                           tags: '',
                           hex: ''
                       }
                       $scope.KanbanDataList.push($scope.kanbanValue);
                   }
                   $scope.KanbanGrid();
               });
            }
            $scope.GetKanbanCase();
           

            $scope.KanbanGrid = function() {

          

            var fields = [
              { name: "id", map: "id", type: "string" },
              { name: "resourceId", type: "number" },
              { name: "status", map: "state", type: "string" },
              { name: "text", map: "label", type: "string" },
              { name: "tags", type: "string" },
              { name: "color", map: "hex", type: "string" },
              
            ];

           
            var source =
             {
                 localData: $scope.KanbanDataList,
                 dataType: "array",
                 dataFields: fields
             };

            var dataAdapter = new $.jqx.dataAdapter(source);

            //var resourcesAdapterFunc = function () {
            //    var resourcesSource =
            //    {
            //        localData: [
            //              { id: 0, name: "No name", image: "https://www.jqwidgets.com/jquery-widgets-demo/jqwidgets/styles/images/common.png", common: true },
            //              { id: 1, name: "Andrew Fuller", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/andrew.png" },
            //              { id: 2, name: "Janet Leverling", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/janet.png" },
            //              { id: 3, name: "Steven Buchanan", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/steven.png" },
            //              { id: 4, name: "Nancy Davolio", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/nancy.png" },
            //              { id: 5, name: "Michael Buchanan", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/Michael.png" },
            //              { id: 6, name: "Margaret Buchanan", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/margaret.png" },
            //              { id: 7, name: "Robert Buchanan", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/robert.png" },
            //              { id: 8, name: "Laura Buchanan", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/Laura.png" },
            //              { id: 9, name: "Laura Buchanan", image: "https://www.jqwidgets.com/jquery-widgets-demo/images/Anne.png" }
            //        ],
            //        dataType: "array",
            //        dataFields: [
            //             { name: "id", type: "number" },
            //             { name: "name", type: "string" },
            //             { name: "image", type: "string" },
            //             { name: "common", type: "boolean" }
            //        ]
            //    };

            //    var resourcesDataAdapter = new $.jqx.dataAdapter(resourcesSource);
            //    return resourcesDataAdapter;
            //}

           

            $('#kanban').jqxKanban({
                //resources: resourcesAdapterFunc(), -- why is this needed, making the duplicate issue in kanban board!
                source: dataAdapter,
                columns: [
                   { text: "New", dataField: "New" },
                          { text: "In Process", dataField: "In Process" },
                          { text: "Escalated", dataField: "Escalated" },
                          { text: "On Hold", dataField: "On Hold" },
                          { text: "Vendor", dataField: "Vendor" },
                          { text: "Needs Review", dataField: "Needs Review" },
                          { text: "Completed", dataField: "Completed" }                         
                ]
            });

            $('#kanban').on('itemMoved', function (event) {
                var args = event.args;
                var itemId = args.itemData.id;
                var oldParentId = args.oldParentId;
                var newParentId = args.newParentId;
                var itemData = args.itemData;
                var oldColumn = args.oldColumn;
                var newColumn = args.newColumn.dataField;
                $http.post('/api/CaseManageAddEdit/' + itemId + '/UpdateCaseStatus/?CaseStatus=' + newColumn).then(function (response) {
                    var data = response.data;

                })
               
            });

            $('#kanban').on('itemReceived', function (event) {
                
                var args = event.args;
                var itemId = args.itemData.id;
                var oldParentId = args.oldParentId;
                var newParentId = args.newParentId;
                var itemData = args.itemData;
                var oldColumn = args.oldColumn;
                var newColumn = args.newColumn;

                
            });

            $('#kanban').on('columnCollapsed', function (event) {
                var args = event.args;
                var column = args.column;

              
            });

            $('#kanban').on('columnExpanded', function (event) {
                var args = event.args;
                var column = args.column;

              
            });

            $('#kanban').on('itemAttrClicked', function (event) {
                var args = event.args;
                var itemId = args.item.resourceId;
                var attribute = args.attribute; // template, colorStatus, content, keyword, text, avatar
                if (attribute == "keyword")
                    window.location.href = "#!/Case/"+itemId;
            });

            $('#kanban').on('columnAttrClicked', function (event) {
                var args = event.args;
                var column = args.column;
                var cancelToggle = args.cancelToggle; // false by default. Set to true to cancel toggling dynamically.
                var attribute = args.attribute; // title, button

                
            });
            

        }

            $scope.gotoCaseListView = function () {
                window.location.href = "#!/franchiseCase";
            }
            $scope.AddCase = function () {
                window.location.href = "#!/caseAddEdit";
            }
        }
    ])