﻿appCP.controller('HomeOfficeCaseViewController', [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'FranchiseService', 'NavbarServiceCP', 'FileUploadService', 'CalendarService', 'PersonService', 'calendarModalService',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, FranchiseService, NavbarService, FileUploadService, CalendarService, PersonService, calendarModalService) {
            $scope.NavbarService = NavbarService;
            //$scope.NavbarService.SelectOperation();
            $scope.NavbarService.VendorGovernanceTab();
            $scope.CalendarService = CalendarService;
            $scope.PersonService = PersonService;
            $scope.calendarModalService = calendarModalService;


            $scope.FileUploadService = FileUploadService;
            $scope.submitted = false;
            $scope.ShowConvert = false;
            $scope.CaseId = $routeParams.CaseId;
            $scope.VendorCaseNumber = $routeParams.VendorCaseNumber;
            $scope.FranchiseId = $routeParams.FranchiseID;
            // $scope.CaseNum = $routeParams.Id;

            //enable case
            $scope.HFCService = HFCService;
            $scope.FranchiseLevelCase = $scope.HFCService.FranchiseLevelCase;
            $scope.Permission = {};
            var HomeOfficeCasepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'CaseManagement')

            $scope.Permission.ViewHome = HomeOfficeCasepermission.CanRead;

            if ($scope.CaseId) {
                $scope.FileUploadService.SetCaseId($scope.CaseId, 'FranchiseCase');
            }
            else $scope.FileUploadService.clearCaseId();

            $scope.Hide_SeltoVendor = false;

            $scope.forComments = { id: $scope.CaseId, refId: 0, module: 4 };  // module : 4 for case view page

            HFCService.setHeaderTitle("CaseView #");

            /// TinyMCE Editor

            $scope.tinymceModel = 'Initial content';

            $scope.getContent = function () {
                console.log('Editor content:', $scope.tinymceModel);
            };

            $scope.setContent = function () {
                $scope.tinymceModel = 'Time: ' + (new Date());
            };

            $scope.tinymceOptions = {
                plugins: 'link image code',
                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | paste',
                paste_data_images: true
            };

            ////

            // follow enable disable and get

            $http.get('/api/CaseManageAddEdit/' + $scope.CaseId + '/GetFollowOrNot').then(function (data) {
                $scope.Follow = data.data;
            })

            // for related tab

            if (window.location.href.includes('/CaseView') || window.location.href.includes('/CaseViewFullDetails')) {

                $scope.FileUploadService.css1 = "fileupload_ltgrid";
                $scope.FileUploadService.css2 = "";
                $scope.FileUploadService.css3 = "hidden";
                if (window.location.href.includes('/CaseViewFullDetails'))
                    $scope.FileUploadService.css4 = "hidden";
                $scope.FileUploadService.disableLineDropDown = true;
            }

            $scope.RelatedEventsGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.CaseId) {
                                var url = "api/calendar/" + $scope.CaseId + "/GetEventsForCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },
                    },
                },
                batch: true,
                cache: false,
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                noRecords: { template: "No events found" },
                columns: [
                    { field: "Subject", title: "Subject" },
                    { field: "AttendeesNames", title: "Attendees" },
                    {
                        field: "StartDate",
                        title: "Start",
                        //template: "#= kendo.toString(kendo.parseDate(StartDate), 'MM/dd/yyyy hh:mm tt') #"
                        template: function (dataitem) {
                            if (dataitem.StartDate)
                                return StartDateValue(dataitem);
                            else return "";
                        }
                    },
                     {
                         field: "EndDate", title: "End",
                         //template: "#= kendo.toString(kendo.parseDate(EndDate), 'MM/dd/yyyy hh:mm tt') #"
                         template: function (dataitem) {
                             if (dataitem.EndDate)
                                 return EndDateValue(dataitem);
                             else return "";
                         }
                     },
                     //{
                     //    field: "",
                     //    title: "",
                     //    template: function (dataitem) {
                     //        return EventMenuTemplate(dataitem);
                     //    }//,
                     //    //width: "5%"
                     //}
                ],
            };

            function EventMenuTemplate(dataitem) {
                var template = "";
                template += '<li><a href="javascript:void(0)" ng-click="editCaseEvent(' + dataitem.CalendarId + ');">Edit</a></li>';           // editEvent(\'persongo\', ' + dataitem.CalendarId + ')

                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                       '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                       '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                       '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }

            function StartDateValue(dataitem) {
                var template = "";
                if (dataitem.IsAllDay == true) {
                    template = HFCService.KendoDateFormat(dataitem.StartDate)
                    return template;
                }
                else
                    return template = HFCService.KendoDateTimeFormat(dataitem.StartDate)
            }

            function EndDateValue(dataitem) {
                var template = "";
                if (dataitem.IsAllDay == true) {
                    template = HFCService.KendoDateFormat(dataitem.EndDate)
                    return template;
                }
                else return template = HFCService.KendoDateTimeFormat(dataitem.EndDate)
            }

            $scope.eventSuccessCallback = function (param) {
                if ($("#MyAppointmentsGrid1").data("kendoGrid").dataSource)
                    $("#MyAppointmentsGrid1").data("kendoGrid").dataSource.read();
            }

            // trigger calendar for edit
            $scope.editCaseEvent = function (id) {
                $scope.calendarModalService.setPopUpDetails(true, 'Event', id, $scope.eventSuccessCallback, null, null, true);
            }
            // old calendar code
            //$scope.editEvent = function (modalId, EventId) {
            //    $scope.CalendarService.GetEventsFromScheduler(modalId, EventId, $scope.eventSuccessCallback, null, null, true);
            //}


            // Tasks operations
            $scope.RelatedTasksGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.CaseId) {
                                var url = "api/calendar/" + $scope.CaseId + "/GetTasksForCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },
                    },

                },
                batch: true,
                cache: false,
                //pageable: {
                //    refresh: true,
                //    pageSize: 25,
                //    pageSizes: [25, 50, 100, 'All'],
                //    buttonCount: 5
                //},
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                noRecords: { template: "No tasks found" },
                columns: [
                    { field: "Subject", title: "Subject" },
                    { field: "AttendeesName", title: "Attendees" },
                    {
                        field: "DueDate", title: "Due"
                        , template: function (dataitem) {
                            return HFCService.KendoDateFormat(dataitem.DueDate);
                        }
                            //"#= kendo.toString(kendo.parseDate(DueDate), 'MM/dd/yyyy') #"
                    },
                     {
                         field: "Message", title: "Note"
                     },
                     //{
                     //    field: "",
                     //    title: "",
                     //    template: function (dataitem) {
                     //        return EventTaskTemplates(dataitem);
                     //    }//,
                     //    //width: "5%"
                     //}
                ],
            };

            function EventTaskTemplates(dataitem) {
                var template = "";
                template += '<li><a href="javascript:void(0)" ng-click="editCaseTask(' + dataitem.TaskId + ')">Edit</a></li>';
                template += '<li ng-show="dataItem.CompletedDate == null"><a href="javascript:void(0)" ng-click="CalendarService.CompleteTask(\'persongo\', ' + dataitem.TaskId + ')">Complete</a></li>';
                template += '<li ng-show="dataItem.CompletedDate != null"><a href="javascript:void(0)" ng-click="CalendarService.InComplete(\'persongo\', ' + dataitem.TaskId + ')">Incomplete</a></li>';


                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                       '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                       '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                       '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }

            // trigger calendar for task edit
            $scope.editCaseTask = function (id) {
                $scope.calendarModalService.setPopUpDetails(true, 'Task', id, $scope.taskSuccessCallback);
            }


            $scope.AddTasksForCase = function () {
                NavbarService.addTask();
            }

            $scope.taskSuccessCallback = function (param) {
                $("#MyTasksGrid1").data("kendoGrid").dataSource.read();
            }
            $scope.calendarModalService.setTaskGenericRefreshMethod($scope.taskSuccessCallback);

            // old calendar code
            //$scope.editTask = function (model) {
            //    $scope.CalendarService.EditTaskFromCalendarSceduler('newtask', model, true, $scope.taskSuccessCallback);
            //}



            // case history
            $scope.RelatedCaseHistoryGridOptionss = {
                dataSource: {
                    type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            if ($scope.CaseId) {
                                var url = "api/CaseManageAddEdit/" + $scope.CaseId + "/GetCaseHistoryForCase";

                                $http.get(url, e).then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "none";
                                });
                            }
                        },
                    },

                },
                batch: true,
                cache: false,
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                pageable: {
                    //  refresh: true,
                    pageSize: 10,
                    pageSizes: [10, 20, 30, 50, 100],
                    buttonCount: 5
                },
                scrollable: true,
                noRecords: { template: "No case history found" },
                columns: [
                    {
                        field: "LastUpdatedOn", title: "Date",
                        // template: "#= kendo.toString(kendo.parseDate(Date), 'yyyy-MM-dd') #"
                        //  template: "#= kendo.toString(kendo.parseDate(Date), 'yyyy-MM-dd hh:mm tt') #"
                        template: function (dataitem) {
                            return HFCService.KendoDateTimeFormat(dataitem.LastUpdatedOn);
                        }
                            //"#= kendo.toString(kendo.parseDate(LastUpdatedOn), 'MM/dd/yyyy hh:mm tt') #"
                    },
                    {
                        field: "Field",
                        title: "Field",
                        template: function (dataitem) {

                            if (dataitem.Field == 'VPO_PICPO_PODId')
                                return '<div>VPO</div>'
                            else if (dataitem.Field == 'MPO_MasterPONum_POId')
                                return '<div>MPO</div>'
                            else if (dataitem.Field == 'SalesOrderId')
                                return '<div>Sales</div>'
                            else if (dataitem.Field == 'Description')
                                return '<div>Desc</div>'
                            else
                                return '<div>' + dataitem.Field + '</div>'
                        }
                    },
                    { field: "UserName", title: "User" },
                    { field: "OriginalValue", title: "Original Value" },
                    { field: "NewValue", title: "New Value" }
                ],
            };


            // refresh task grid
            $scope.PersonService.CallendarSuccessCallback = function () {
                $scope.taskSuccessCallback();
            }



            // end of related tab

            $scope.CaseAddEdit = {
                Name: '',
                Status: '',
                CreatedOn: '',
                DateTimeOpened: '',
                DateTimeClosed: '',
                PurchaseOrdersDetailId: '',
                CellPhone: '',
                PurchaseOrderId: '',
                Email: '',
                OrderId: '',
                AccountName: '',
                SideMark: '',
                CreatedOn: '',
                VPO_PICPO_PODId: '',
                MPO_MasterPONum_POId: '',
                SalesOrderId: '',
                IncidentDate: '',
                DateTimeClosed: '',
                LastUpdatedOn: '',
                Description: '',
                LastUPdatedName: '',
            }

            //get data on inital page load
            $scope.Initialvalue = function () {
                $http.get('/api/CaseManageAddEdit/0/GetUserDetails/').then(function (response) {
                    //
                    $scope.CaseAddEdit.Name = response.data.Name;
                    $scope.CaseAddEdit.CreatedOn = response.data.CreatedOn;
                    $scope.CaseAddEdit.DateTimeOpened = response.data.CreatedOn;
                    $scope.CaseAddEdit.Status = "New";
                    $scope.CaseAddEdit.DateTimeClosed = response.data.DateTimeClosed;
                });
            }

            //  $scope.Initialvalue();
            //on select date update date time open
            $scope.ChangeDate = function (value) {
                $scope.CaseAddEdit.DateTimeOpened = value;
            }

            //grid code.
            $scope.CaseinfoGridOptions = {
                dataSource: {
                    //type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {

                            if ($scope.CaseId == null || $scope.CaseId == "" || $scope.CaseId == undefined) {
                                var id_val = 0;
                            }
                            else id_val = $scope.CaseId;
                            //$http.get("/api/CaseManageAddEdit/" + 0 + "/GetCase?MPOId=" + 0 + "&SalesorderId=" + 0, e)
                            $http.get("/api/CaseManageAddEdit/" + 0 + "/GetCase?CaseId=" + id_val, e)
                                .then(function (data) {

                                    $scope.ShowConvert = false;
                                    for (i = 0; i < data.data.length; i++) {
                                        if (data.data[i].ConvertToVendor == false || data.data[i].ConvertToVendor == null)
                                            $scope.ShowConvert = true;
                                    }
                                    e.success(data.data);


                                }).catch(function (error) {
                                    HFC.DisplayAlert(error);
                                    //var loadingElement = document.getElementById("loading");
                                    //loadingElement.style.display = "none";

                                });
                        },
                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                QuoteLineNumber: { editable: false },
                                ProductName: { editable: false },
                                ProductId: { editable: false },
                                Description: { editable: false },
                                VendorName: { editable: false },
                                Typevalue: { editable: false },
                                CaseReason: { editable: false },
                                IssueDescription: { editable: false },
                                ExpediteReq: { type: "boolean", editable: false },
                                ExpediteApproved: { type: "boolean", editable: false },
                                ReqTripCharge: { type: "boolean", editable: false },
                                TripChargeApprovedName: { editable: false },
                                StatusName: { editable: false },
                                ResolutionName: { editable: false },
                                VendorReference: { editble: false }
                            }
                        }
                    },
                },

                //autoSync: true,
                batch: true,
                cache: false,

                sortable: true,
                filterable: true,
                resizable: true,
                editable: "inline",
                detailInit: detailinitParent,
                noRecords: { template: "No records found" },
                columns: [
                     {
                         field: "Id",
                         title: "Id",
                         filterable: { multi: true, search: true },
                         hidden: true,
                     },
                     {

                         field: "VendorCaseNo",
                         title: "Vendor Case No",
                         width: "120px",
                         filterable: { multi: true, search: true },
                         template: function (dataItem) {
                             return "<span class='Grid_Textalign'>" + $scope.VendorCaseNo(dataItem) + "</span>";
                         }
                         //, mytooltip : "asfsadf"

                     },
                     {
                         field: "VPO",
                         title: "VPO",
                         width: "200px",
                         template: function (dataItem) {
                             return "<span class='Grid_Textalign'>" + $scope.temp_VPO(dataItem) + "</span>";
                         },
                         filterable: { multi: true, search: true },
                         hidden: $scope.VPOHidden,
                     },
                     {
                         field: "QuoteLineNumber",
                         title: "VPO Line",
                         width: "120px",
                         filterable: { multi: true, search: true },
                         hidden: false,
                         template: function (dataItem) {
                             return "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                         },
                     },
                     {
                         field: "VendorName",
                         title: "Vendor",
                         width: "200px",
                         // editor: '<input id="Vendor_txt" readonly data-type="text"  class="k-textbox "  name="VendorName" data-bind="value:VendorName"/>',
                         filterable: { multi: true, search: true },
                         template: function (dataItem) {
                             return $scope.VendorLink(dataItem);
                         },
                         hidden: false,
                     },
                     // {
                     //     field: "ProductName",
                     //     title: "Product",
                     //     width: "200px",
                     //     editor: '<input id="product_txt"  readonly name="ProductName" class="k-textbox " data-bind="value:ProductName" style="border: none "/>',
                     //     filterable: { multi: true, search: true },
                     //     hidden: false,
                     // },
                     // {
                     //     field: "ProductId",
                     //     title: "Product No",
                     //     width: "200px",
                     //     editor: '<input id="productnumber_txt" readonly data-type="text"  class="k-textbox "  name="ProductId" data-bind="value:ProductId"/>',
                     //     filterable: { multi: true, search: true },
                     //     hidden: false,
                     // },
                     // {
                     //     field: "Description",
                     //     title: "Product Description",
                     //     width: "220px",
                     //     attributes: {

                     //         style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                     //     },
                     //     //  editor: '<input id="productdescri_txt" readonly data-type="text"  class="k-textbox" name="Description" data-bind="value:Description"/>',
                     //     filterable: { multi: true, search: true },
                     //     template: function (dataitem) {
                     //         return ProductDescription(dataitem);
                     //     },
                     //     hidden: false,
                     // },

                      {
                          field: "Typevalue",
                          title: "Issue Type",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          editor: function (container, options) {
                              $('<input required=true   name="Type" data-bind="value:Type" style="border: none "  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: false,
                                      optionLabel: "Select",

                                      valuePrimitive: true,
                                      dataTextField: "Typevalue",
                                      dataValueField: "Type",
                                      template: "#=Typevalue #",

                                      dataSource: {
                                          transport: {
                                              read: {
                                                  url: '/api/CaseManageAddEdit/0/GetCaseType?Tableid=' + 5,
                                              }
                                          },
                                      }

                                  });
                          }


                      },
                      {
                          field: "CaseReason",
                          title: "Case Reason",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          editor: function (container, options) {
                              $('<input required=true   name="ReasonCode" data-bind="value:ReasonCode" style="border: none "  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: false,
                                      optionLabel: "Select",

                                      valuePrimitive: true,
                                      dataTextField: "Reasonvalue",
                                      dataValueField: "ReasonCode",
                                      template: "#=Reasonvalue #",

                                      dataSource: {
                                          transport: {
                                              read: {
                                                  url: '/api/CaseManageAddEdit/0/GetCaseReason?Tableid=' + 6,
                                              }
                                          },
                                      }

                                  });
                          }


                      },
                     //{
                     //    field: "IssueDescription",
                     //    title: "Issue Description",
                     //    width: "150px",
                     //    filterable: { multi: true, search: true },
                     //    hidden: false,
                     //    editor: '<input   required=true name="IssueDescription"  class="k-textbox" data-bind="value:IssueDescription" style="border: none "/>',

                     //},
                    {
                        field: "ExpediteReq",
                        title: "Expedite Req",
                        width: "150px",
                        filterable: { multi: true, search: true },
                        template: '<input type="checkbox"  #= ExpediteReq ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                    },
                      //{
                      //    field: "ExpediteApproved",
                      //    title: "Expedite Approved",
                      //    width: "150px",
                      //    filterable: { multi: true, search: true },
                      //    template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                      //},
                       {
                           field: "ReqTripCharge",
                           title: "Trip Charge Req",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           template: '<input type="checkbox" #= ReqTripCharge ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                       },
                       //{
                       //    field: "TripChargeApprovedName",
                       //    title: "Approved Trip Charge",
                       //    width: "200px",
                       //    editor: '<input id="TripChargeApprovedName_txt"  readonly name="TripChargeApprovedName" data-bind="value:TripChargeApprovedName" style="border: none "/>',
                       //    filterable: { multi: true, search: true },
                       //},
                       //{
                       //    field: "StatusName",
                       //    title: "Status",
                       //    width: "150px",
                       //    filterable: { multi: true, search: true },
                       //    hidden: false,
                       //    editor: function (container, options) {
                       //        $('<input required=true   name="Status" data-bind="value:Status" style="border: none "  />')
                       //            .appendTo(container)
                       //            .kendoDropDownList({
                       //                autoBind: false,
                       //                optionLabel: "Select",

                       //                valuePrimitive: true,
                       //                dataTextField: "Statusvalue",
                       //                dataValueField: "Status",
                       //                template: "#=Statusvalue #",

                       //                dataSource: {
                       //                    transport: {
                       //                        read: {

                       //                            url: '/api/CaseManageAddEdit/0/GetCaseStatus?Tableid=' + 7,
                       //                        }
                       //                    },
                       //                }

                       //            });
                       //    }


                       //},
                       //{
                       //    field: "ResolutionName",
                       //    title: "Resolution",
                       //    width: "150px",
                       //    filterable: { multi: true, search: true },
                       //    hidden: false,
                       //    editor: function (container, options) {
                       //        $('<input required=true  name="Resolution" data-bind="value:Resolution" style="border: none "  />')
                       //            .appendTo(container)
                       //            .kendoDropDownList({
                       //                autoBind: false,
                       //                optionLabel: "Select",

                       //                valuePrimitive: true,
                       //                dataTextField: "Resolutionvalue",
                       //                dataValueField: "Resolution",
                       //                template: "#=Resolutionvalue #",

                       //                dataSource: {
                       //                    transport: {
                       //                        read: {

                       //                            url: '/api/CaseManageAddEdit/0/GetCaseResolution?Tableid=' + 8,
                       //                        }
                       //                    },
                       //                }

                       //            });
                       //    }


                       //},
                        //{
                        //    field: "VendorReference",
                        //    title: "Vendor Reference",
                        //    width: "120px",
                        //    //  editor: '<input id="productdescri_txt" readonly data-type="text"  class="k-textbox" name="Description" data-bind="value:Description"/>',
                        //    filterable: { multi: true, search: true },
                        //    template: function (dataitem) {
                        //        if (dataitem.VendorReference)
                        //            return dataitem.VendorReference;
                        //        else
                        //            return '';
                        //    },
                        //    hidden: false,
                        //},
                        //{
                        //    field: "",
                        //    title: "",
                        //    width: "60px",
                        //    // template: '<ul><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="DeleteRowDetails(dataItem)">Edit</a></li>  <li><a  href="javascript:void(0)" ng-click="DeleteRowDetails(this.dataItem)">Delete</a></li><li><a href="javascript:void(0)"  ng-click="CopyData(dataItem)">Copy</a></li> </ul> </li> </ul>'
                        //    template: function (dataitem) {
                        //        if (!dataitem.ConvertToVendor) {
                        //            var hcontent = '<a href="" ng-click="VendorCaseEdit(this.dataItem)"><i class="fas fa-exchange-alt"></i></a>'
                        //            return hcontent;
                        //        }
                        //        else
                        //            return '';

                        //    }
                        //},

                ]

            };



            //grid code.
            $scope.FullDetailsCaseinfoGridOptions = {
                dataSource: {
                    //type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {

                            if ($scope.CaseId == null || $scope.CaseId == "" || $scope.CaseId == undefined) {
                                var id_val = 0;
                            }
                            else id_val = $scope.CaseId;
                            //$http.get("/api/CaseManageAddEdit/" + 0 + "/GetCase?MPOId=" + 0 + "&SalesorderId=" + 0, e)
                            $http.get("/api/CaseManageAddEdit/" + 0 + "/GetCase?CaseId=" + id_val, e)
                                .then(function (data) {

                                    $scope.ShowConvert = false;
                                    for (i = 0; i < data.data.length; i++) {
                                        if (data.data[i].ConvertToVendor == false || data.data[i].ConvertToVendor == null)
                                            $scope.ShowConvert = true;
                                    }
                                    e.success(data.data);


                                }).catch(function (error) {
                                    HFC.DisplayAlert(error);
                                    //var loadingElement = document.getElementById("loading");
                                    //loadingElement.style.display = "none";

                                });
                        },
                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                QuoteLineNumber: { editable: false },
                                ProductName: { editable: false },
                                ProductId: { editable: false },
                                Description: { editable: false },
                                VendorName: { editable: false },
                                Typevalue: { editable: false },
                                CaseReason: { editable: false },
                                IssueDescription: { editable: false },
                                ExpediteReq: { type: "boolean", editable: false },
                                ExpediteApproved: { type: "boolean", editable: false },
                                ReqTripCharge: { type: "boolean", editable: false },
                                TripChargeApprovedName: { editable: false },
                                StatusName: { editable: false },
                                ResolutionName: { editable: false },
                                VendorReference: { editble: false }
                            }
                        }
                    },
                },

                //autoSync: true,
                batch: true,
                cache: false,

                sortable: true,
                filterable: true,
                resizable: true,
                editable: "inline",
                //detailInit: detailinitParent,
                //dataBound: function (e) {
                //    $scope.quoteLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource._data.length;
                //},
                noRecords: { template: "No records found" },
                columns: [
                     {
                         field: "Id",
                         title: "Id",
                         filterable: { multi: true, search: true },
                         hidden: true,
                     },
                    {
                        field: "QuoteLineNumber",
                        title: "Line",
                        width: "120px",
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                      {
                          field: "ProductName",
                          title: "Product",
                          width: "200px",
                          editor: '<input id="product_txt"  readonly name="ProductName" class="k-textbox " data-bind="value:ProductName" style="border: none "/>',
                          filterable: { multi: true, search: true },
                          hidden: false,
                      },
                      {
                          field: "ProductId",
                          title: "Product No",
                          width: "200px",
                          editor: '<input id="productnumber_txt" readonly data-type="text"  class="k-textbox "  name="ProductId" data-bind="value:ProductId"/>',
                          filterable: { multi: true, search: true },
                          hidden: false,
                      },
                      {
                          field: "Description",
                          title: "Product Description",
                          width: "220px",
                          attributes: {

                              style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                          },
                          //  editor: '<input id="productdescri_txt" readonly data-type="text"  class="k-textbox" name="Description" data-bind="value:Description"/>',
                          filterable: { multi: true, search: true },
                          template: function (dataitem) {
                              return ProductDescription(dataitem);
                          },
                          hidden: false,
                      },
                      {
                          field: "Typevalue",
                          title: "Issue Type",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          editor: function (container, options) {
                              $('<input required=true   name="Type" data-bind="value:Type" style="border: none "  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: false,
                                      optionLabel: "Select",

                                      valuePrimitive: true,
                                      dataTextField: "Typevalue",
                                      dataValueField: "Type",
                                      template: "#=Typevalue #",

                                      dataSource: {
                                          transport: {
                                              read: {
                                                  url: '/api/CaseManageAddEdit/0/GetCaseType?Tableid=' + 5,
                                              }
                                          },
                                      }

                                  });
                          }


                      },
                      {
                          field: "CaseReason",
                          title: "Case Reason",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          editor: function (container, options) {
                              $('<input required=true   name="ReasonCode" data-bind="value:ReasonCode" style="border: none "  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: false,
                                      optionLabel: "Select",

                                      valuePrimitive: true,
                                      dataTextField: "Reasonvalue",
                                      dataValueField: "ReasonCode",
                                      template: "#=Reasonvalue #",

                                      dataSource: {
                                          transport: {
                                              read: {
                                                  url: '/api/CaseManageAddEdit/0/GetCaseReason?Tableid=' + 6,
                                              }
                                          },
                                      }

                                  });
                          }


                      },
                     {
                         field: "IssueDescription",
                         title: "Issue Description",
                         width: "150px",
                         filterable: { multi: true, search: true },
                         hidden: false,
                         editor: '<input   required=true name="IssueDescription"  class="k-textbox" data-bind="value:IssueDescription" style="border: none "/>',

                     },
                    {
                        field: "ExpediteReq",
                        title: "Expedite Req",
                        width: "150px",
                        filterable: { multi: true, search: true },
                        template: '<input type="checkbox"  #= ExpediteReq ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                    },
                      {
                          field: "ExpediteApproved",
                          title: "Expedite Approved",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                      },
                       {
                           field: "ReqTripCharge",
                           title: "Req Trip Charge",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           template: '<input type="checkbox" #= ReqTripCharge ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                       },
                       {
                           field: "TripChargeApprovedName",
                           title: "Approved Trip Charge",
                           width: "200px",
                           editor: '<input id="TripChargeApprovedName_txt"  readonly name="TripChargeApprovedName" data-bind="value:TripChargeApprovedName" style="border: none "/>',
                           filterable: { multi: true, search: true },
                       },
                       {
                           field: "StatusName",
                           title: "Status",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                       },
                       {
                           field: "ResolutionName",
                           title: "Resolution",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                       },
                ]

            };

            $scope.VendorCaseNo = function (dataItem) {

                if (dataItem.VendorCaseNo && dataItem.ConvertToVendor)
                    return "<a href='#!/HomeOfficeVendorView/" + dataItem.CaseId + "/" + dataItem.VendorCaseNumber + "/" + dataItem.VendorId + "' style='color: rgb(61,125,139);'>" + dataItem.VendorCaseNo + "</a>";
                else
                    return '';

            }

            $scope.temp_VPO = function (dataItem) {

                if (dataItem.VPO && dataItem.VPO != 0)
                    return "<a href='#!/PurchaseMasterView/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.VPO + "</a>";
                else
                    return dataItem.VPO;

            }

            $scope.VendorLink = function (dataItem) {

                if (dataItem.VendorId && dataItem.VendorId != 0)
                    return "<a href='#!/VendorDetails/" + dataItem.VendorId + "/" + $scope.FranchiseId + "' style='color: rgb(61,125,139);'>" + dataItem.VendorName + "</a>";
                else
                    return dataItem.VendorName;

            }

            function ProductDescription(dataitem) {
                var template = "";
                // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
                var erer = dataitem.Description.replace('"', "");
                erer = erer.replace(/<[^>]*>/g, '');
                template += '<span title="' + erer + '">' + dataitem.Description + '</span>'

                return template;
            }


            //has case id get saved data
            if ($scope.CaseId > 0) {

                var Id = $scope.CaseId;
                var url = '/api/CaseManageAddEdit/' + Id + '/GetSavedValue';
                if (window.location.href.includes('/CaseViewFullDetails'))
                    url = '/api/CaseManageAddEdit/' + Id + '/GetSavedValueHO'; //?VendorCaseNumber=' + $scope.VendorCaseNumber;

                $http.get(url).then(function (response) {
                    var data = response.data;
                    if (response.data.CaseId != 0) {
                        $scope.CaseAddEdit = response.data;

                        $scope.CaseAddEdit.PurchaseOrdersDetailId = $scope.CaseAddEdit.VPO_PICPO_PODId;
                        $scope.CaseAddEdit.PurchaseOrderId = $scope.CaseAddEdit.MPO_MasterPONum_POId;
                        $scope.CaseAddEdit.OrderId = $scope.CaseAddEdit.SalesOrderId;
                        $scope.CaseAddEdit.Email = $scope.CaseAddEdit.PrimaryEmail;
                        //$scope.CaseAddEdit.Status = "New";
                        $scope.CaseAddEdit.Status = $scope.CaseAddEdit.StatusValue;
                        $scope.PreviousValues = response.data;


                    }

                })
            }

            $scope.EditCase = function () {
                window.location.href = "#!/Case/" + $scope.CaseId
            }

            $scope.UnfollowThisCase = function () {
                $http.get('/api/CaseManageAddEdit/' + $scope.CaseId + '/UnfollowThisCase?module=4').then(function (response) {
                    $scope.Follow = response.data;
                });
            }

            $scope.FollowThisCase = function () {
                $http.get('/api/CaseManageAddEdit/' + $scope.CaseId + '/FollowThisCase?module=4').then(function (response) {
                    $scope.Follow = response.data;
                });
            }


            $scope.VendorCaseEdit = function (Value) {

                $scope.Linedetail = Value;
                $http.post('/api/CaseManageAddEdit/0/SaveVendorCase', $scope.Linedetail).then(function (response) {

                    if (response.data) {

                        var value_res = response.data;
                        $scope.CaseAddEdit.StatusValue = response.data;
                        //window.location.href = "#!/VendorcaseEdit/" + value_res;
                        //window.location.href = "#!/VendorCaseList";
                        $('#caseGrid01').data('kendoGrid').dataSource.read();
                        $('#caseGrid01').data('kendoGrid').refresh();


                        var gridDS = $('#caseGrid01').data('kendoGrid').dataSource._data;

                        for (var i = 0 ; i < gridDS.length; i++) {

                            if (gridDS[i].ConvertToVendor === 0) {
                                $scope.ShowConvert = true;
                            }
                        }


                        return;

                    }
                    else {
                        HFC.DisplayAlert(response.data);
                        return;
                    }
                    //window.location.href = "#!/VendorcaseEdit/" + $scope.Linedetail.CaseId + "/" + $scope.Linedetail.Id;
                })


            }

            $scope.ConvertWhole = function () {

                var value = $('#caseGrid01').data('kendoGrid')._data;
                $scope.Completedetails = value;
                $http.post('/api/CaseManageAddEdit/0/SaveVendorCaseComplete', $scope.Completedetails).then(function (response) {

                    if (response.data) {

                        var value_res = response.data;

                        $('#caseGrid01').data('kendoGrid').dataSource.read();
                        $('#caseGrid01').data('kendoGrid').refresh();
                        $scope.ShowConvert = false;
                        $scope.CaseAddEdit.StatusValue = response.data;

                        return;
                    }
                    else {
                        HFC.DisplayAlert(response.data);
                        return;
                    }
                    //window.location.href = "#!/VendorcaseEdit/" + $scope.Linedetail.CaseId + "/" + $scope.Linedetail.Id;
                });
            }

            // detailed lines
            function detailinitParent(e) {
                var grdId = "grd" + e.data.QuoteLineId;
                $("<div id = '" + grdId + "' class='sub-grid' />").appendTo(e.detailCell).kendoGrid({
                    dataSource: [e.data],
                    columns: [
                         {
                             field: "ExpediteApproved",
                             title: "Expedite Approved",
                             width: "150px",
                             filterable: { multi: true, search: true },
                             template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                         },
                           {
                               field: "TripChargeApprovedName",
                               title: "Trip Charge Approved",
                               width: "150px",
                               editor: '<input id="TripChargeApprovedName_txt"  readonly name="TripChargeApprovedName" data-bind="value:TripChargeApprovedName" style="border: none "/>',
                               filterable: { multi: true, search: true },
                           },
                              {
                                  field: "StatusName",
                                  title: "Status",
                                  width: "150px",
                                  filterable: { multi: true, search: true },
                                  hidden: false,
                                  editor: function (container, options) {
                                      $('<input required=true   name="Status" data-bind="value:Status" style="border: none "  />')
                                          .appendTo(container)
                                          .kendoDropDownList({
                                              autoBind: false,
                                              optionLabel: "Select",

                                              valuePrimitive: true,
                                              dataTextField: "Statusvalue",
                                              dataValueField: "Status",
                                              template: "#=Statusvalue #",

                                              dataSource: {
                                                  transport: {
                                                      read: {

                                                          url: '/api/CaseManageAddEdit/0/GetCaseStatus?Tableid=' + 7,
                                                      }
                                                  },
                                              }

                                          });
                                  }


                              },
                       {
                           field: "ResolutionName",
                           title: "Vendor Resolution",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           editor: function (container, options) {
                               $('<input required=true  name="Resolution" data-bind="value:Resolution" style="border: none "  />')
                                   .appendTo(container)
                                   .kendoDropDownList({
                                       autoBind: false,
                                       optionLabel: "Select",

                                       valuePrimitive: true,
                                       dataTextField: "Resolutionvalue",
                                       dataValueField: "Resolution",
                                       template: "#=Resolutionvalue #",

                                       dataSource: {
                                           transport: {
                                               read: {

                                                   url: '/api/CaseManageAddEdit/0/GetCaseResolution?Tableid=' + 8,
                                               }
                                           },
                                       }

                                   });
                           }


                       },
                        //{
                        //    field: "VendorReference",
                        //    title: "Vendor Reference",
                        //    width: "150px",
                        //    //  editor: '<input id="productdescri_txt" readonly data-type="text"  class="k-textbox" name="Description" data-bind="value:Description"/>',
                        //    filterable: { multi: true, search: true },
                        //    template: function (dataitem) {
                        //        if (dataitem.VendorReference)
                        //            return dataitem.VendorReference;
                        //        else
                        //            return '';
                        //    },
                        //    hidden: false,
                        //},
                        //{ field: "VendorName", title: "Vendor" },
                        //{ field: "Description", title: "Item Description", template: '#=Description#' },
                        //{ field: "SuggestedResale", title: "Suggested Resale", format: "{0:c}" },
                        //{ field: "UnitPrice", title: "Unit Price", format: "{0:c}" },
                        //{ field: "Discount", title: "Discount", template: '#=kendo.format("{0:p}", Discount / 100)#' },
                    ],
                });
            }
        }]);
