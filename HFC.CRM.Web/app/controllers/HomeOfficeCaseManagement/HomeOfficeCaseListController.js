﻿'use strict';
appCP.controller('HomeOfficeCaseListController'
    , ['$scope', '$http', 'NavbarServiceCP', 'kendoService', 'HFCService', 'kendotooltipService'
        , function ($scope, $http, NavbarService, kendoService, HFCService, kendotooltipService) {

            $scope.NavbarService = NavbarService;
            $scope.NavbarService.VendorGovernanceTab();

            $scope.FLCaseTypes = [{ Id: 1, Name: 'Assigned to Me' },
            //{ Id: 2, Name: 'Cases I Entered' },
            { Id: 3, Name: 'Open Cases' },
            { Id: 4, Name: 'Cases I Follow' },
            { Id: 5, Name: 'All Cases' }];

            //to get active vendor to drop down
            //$http.get('/api/CaseManageAddEdit/0/GetAllVendorList').then(function (data) {
            //    $scope.VendorListName = data.data;
            //});
            var offsetvalue = 0;
            //to get active franchise name
            //$http.get('/api/CaseManageAddEdit/0/GetAllFranchiseList').then(function (data) {
            //    $scope.FranchiseListName = data.data;
            //});
            $scope.Vendor_NameList = function () {
                $scope.Vendor_NameList = {
                    optionLabel: {
                        Name: "All",
                        VendorID: ""
                    },
                    dataTextField: "Name",
                    dataValueField: "VendorID",
                    filter: "contains",
                    valuePrimitive: true,
                    autoBind: true,
                    change: function (data) {
                        var val = this.value();
                        $scope.VendorChanged();
                    },
                    dataSource: {
                        transport: {
                            read: {
                                url: "/api/CaseManageAddEdit/0/GetAllVendorList",
                            }
                        }
                    },
                };
            }
            $scope.Vendor_NameList();

            $scope.Franchise_NameList = function () {
                $scope.Franchise_NameList = {
                    optionLabel: {
                        Name: "All",
                        FranchiseId: ""
                    },
                    dataTextField: "Name",
                    dataValueField: "FranchiseId",
                    filter: "contains",
                    valuePrimitive: true,
                    autoBind: true,
                    change: function (data) {
                        $scope.FranchiseChanged()
                    },
                    dataSource: {
                        transport: {
                            read: {
                                url: "/api/CaseManageAddEdit/0/GetAllFranchiseList",
                            }
                        }
                    },
                };
            }
            $scope.Franchise_NameList();

            $scope.SelectedVendor = "";
            $scope.SelectedFranchise = "";
            $scope.SelectedCaseType = 1;
            $scope.Closed = false;
            HFCService.setHeaderTitle("Case Management #");



            $scope.Permission = {};
            var HomeOfficeCasepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'CaseManagement')
            if (HomeOfficeCasepermission){
            $scope.Permission.ViewHome = HomeOfficeCasepermission.CanRead;
            }

            $scope.HomeOfficeCaseListGridOptions = {
                cache: false,
                dataSource: {
                    transport: {
                        read: function (e) {
                            $http.get('/api/CaseManageAddEdit/0/GetAllCaseList/?SelectedCaseType=' + $scope.SelectedCaseType + '&FranchiseID=' + $scope.SelectedFranchise + '&VendorID=' + $scope.SelectedVendor + '&Closed=' + $scope.Closed)
                                .then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    HFC.DisplayAlert(error);
                                });
                        }
                    }
                    ,
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    schema: {
                        model: {
                          
                            fields: {
                                CaseNo: { type: "number", editable: false },
                                VendorCaseNo: { type: "string", editable: false },
                                MPO: { type: "number" },
                                VPO: { type: "number",  },
                                CreatedOn: { type: "date", }
                            }
                                    }
                                },
                                 
                            
                    pageSize: 25
                    // data: $scope.FCList
                },
                dataBound: function (e) {
                    if (this.dataSource.view().length == 0) {
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    if (kendotooltipService.columnWidth) {
                        kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                    } else if (window.innerWidth < 1280) {
                        kendotooltipService.restrictTooltip(null);
                    }
                },
                columns: [
                        {

                            field: "FranchiseName",
                            title: "Franchise",
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.FranchiseName + "</div><span>" + dataItem.FranchiseName + "</span></span>";
                            },
                            width: "100px",
                            filterable: {
                                //multi: true,
                                search: true
                            }

                        },
                        {

                            field: "CaseNo",
                            type:"number",
                            title: "Case No",
                            //template: function (dataItem) {
                            //    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CaseNo + "</div><span>" + dataItem.CaseNo + "</span></span>";
                            //},
                            width: "120px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + $scope.temp_CaseNo(dataItem) + "</span>";
                            }
                            //, mytooltip : "asfsadf"

                        },
                        {

                            field: "VendorCaseNo",
                            title: "Vendor Case No",
                            width: "120px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + $scope.VendorCaseNo(dataItem) + "</span>";
                            }
                            //, mytooltip : "asfsadf"

                        },
                        {

                            field: "MPO",
                            title: "MPO",
                            type: "number",
                            //template: function (dataItem) {
                            //    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.MPO + "</div><span>" + dataItem.MPO + "</span></span>";
                            //},
                            width: "120px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + $scope.temp_MPO(dataItem) + "</span>";
                            }

                        },
                        {

                            field: "VPO",
                            title: "VPO",
                            type: "number",
                            //template: function (dataItem) {
                            //    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.VPO + "</div><span>" + dataItem.VPO + "</span></span>";
                            //},
                            width: "120px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + $scope.temp_VPO(dataItem) + "</span>";
                            }

                        },
                        {

                            field: "Vendor",
                            title: "Vendor",
                            //template: function (dataItem) {
                            //    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Vendor + "</div><span>" + dataItem.Vendor + "</span></span>";
                            //},
                            width: "200px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            template: function (dataItem) {
                                return $scope.temp_Vendor(dataItem);
                            }

                        },
                        {

                            field: "AccountName",
                            title: "Account Name",
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountName + "</div><span>" + dataItem.AccountName + "</span></span>";
                            },
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            //template: function (dataItem) {
                            //    return $scope.temp_AccountName(dataItem);
                            //}

                        },
                        {

                            field: "Description",
                            title: "Description",
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Description + "</div><span>" + dataItem.Description + "</span></span>";
                            },
                            width: "120px",
                            filterable: {
                                //multi: true,
                                search: true
                            }
                        },
                        {

                            field: "StatusName",
                            title: "Status",
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.StatusName + "</div><span>" + dataItem.StatusName + "</span></span>";
                            },
                            width: "100px",
                            filterable: { multi: true, search: true }

                        },
                        {

                            field: "TypeName",
                            title: "Issue Type",
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.TypeName + "</div><span>" + dataItem.TypeName + "</span></span>";
                            },
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            }
                        },
                        {

                            field: "CreatedOn",
                            title: "Date/Time Opened",
                            width: "150px",
                            // filterable: { multi: true, search: true },
                            type: "date",
                            filterable: HFCService.gridfilterableDate("datetime", "HomeOfficeFilterCalendar", offsetvalue),
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateTimeFormat(dataItem.CreatedOn) + "</div><span>" + HFCService.KendoDateTimeFormat(dataItem.CreatedOn) + "</span></span>";
                                // return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + kendo.toString(kendo.parseDate(dataItem.CreatedOn), 'MM/dd/yyyy h:mm tt') + "</div><span>" + kendo.toString(kendo.parseDate(dataItem.CreatedOn), 'MM/dd/yyyy h:mm tt') + "</span></span>";
                            },
                        },
                        {

                            field: "CaseOwner",
                            title: "Case Owner",
                            template: function (dataItem) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CaseOwner + "</div><span>" + dataItem.CaseOwner + "</span></span>";
                            },
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            }
                        },
                        {
                            field: "",
                            title: "",
                            width: "100px",
                            template: function (dataItem) {

                                return '<ul><li class="dropdown note1 ad_user"><div class="dropdown"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="#!/CaseViewFullDetails/' + dataItem.CaseId + '/' + dataItem.FranchiseId + '"  >View Full Details</a></li></ul></li></ul>';
                            }
                        }
                ],
                noRecords: { template: "No records found" },

                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                    buttonCount: 5
                },
                resizable: true,
                toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin"><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="Gridsearch()" type="search" id="searchBox" placeholder="Search Case" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 no-padding hfc_leadbut"><input type="button" id="btnReset" ng-click="GridSearchClearr()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div><div class="col-xs-8 col-sm-8 hfc_leadchkbox"><input class="option-input checkbox" name="CaselistClosedCancelled" id="CaselistClosedCancelled" type="checkbox" ng-model="Closed" ng-click="checkbox_clickForCaseLise()"><label class="ldet_label">Show Closed / Canceled</label></div></div></script>').html()),
                columnResize: function (e) {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                    getUpdatedColumnList(e);
                }
            };



            $scope.HomeOfficeCaseListGridOptions = kendotooltipService.setColumnWidth($scope.HomeOfficeCaseListGridOptions);
            var getUpdatedColumnList = function (e) {
                kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
            }

            // for kendo tooltip
            $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;

            kendoService.customTooltip = null;
            //kendoService.customTooltip = function (kgFieldName) {

            //    if (kgFieldName == 'VendorCaseNo') return "This is custom: Vendor Case No";
            //    if (kgFieldName == 'CaseNo') return "This is custom: Case No---";

            //    return null;
            //}


            $scope.callAddCase = function () {
                window.location.href = "#!/caseAddEdit";
            }

            $scope.FranchiseChanged = function () {
                $scope.checkbox_clickForCaseLise();
            }
            $scope.VendorChanged = function () {
                $scope.checkbox_clickForCaseLise();
            }
            $scope.checkbox_clickForCaseLise = function () {

                $scope.Closed = $('#CaselistClosedCancelled').prop('checked');               
                var grid = $('#HomeOfficeCaseList').data("kendoGrid");
                grid.dataSource.read();                      
            }

            //function AttachmentTemplate(dataItem) {
            //    var Div = '';
            //    if (dataItem.AttachmentStreamId && dataItem.AttachmentStreamId != null) {
            //        Div = "<a href='/api/Download?streamid=" + dataItem.AttachmentStreamId + "' target=\"_blank\" style='color: rgb(61,125,139);'><span style='padding: 10px;'>" + dataItem.FileName + "</span></a> ";
            //    }
            //    return Div;
            //}

            $scope.gotokanbanview = function () {

                window.location.href = "#!/KanbanView";
            }
            $scope.temp_CaseNo = function (dataItem) {
                if (dataItem.CaseNo)
                    return "<a href='#!/CaseView/" + dataItem.CaseId + "/" + dataItem.FranchiseId + "' style='color: rgb(61,125,139);'>" + dataItem.CaseNo + "</a>";
                else
                    return '';

            }

            //$scope.temp_VendorCaseNo = function (dataItem) {

            //    if (dataItem.VendorCaseNumber)
            //        return "<a href='#!/HomeOfficeVendorView/" + dataItem.CaseId + "/" + dataItem.VendorCaseNumber + "/" + dataItem.VendorId + "' style='color: rgb(61,125,139);'>" + dataItem.QuoteLineNumber + "</a>";
            //    else
            //        return '';

            //}
            $scope.VendorCaseNo = function (dataItem) {

                if (dataItem.VendorCaseNo && dataItem.ConvertToVendor)
                    return "<a href='#!/HomeOfficeVendorView/" + dataItem.CaseId + "/" + dataItem.VendorCaseNumber + "/" + dataItem.VendorId + "' style='color: rgb(61,125,139);'>" + dataItem.VendorCaseNo + "</a>";
                else
                    return '';

            }

            $scope.temp_MPO = function (dataItem) {

                if (dataItem.MPO)
                    return "<a href='#!/PurchaseMasterView/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.MPO + "</a>";
                else
                    return '';

            }
            $scope.temp_VPO = function (dataItem) {

                if (dataItem.VPO)
                    return "<a href='#!/PurchaseMasterView/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'>" + dataItem.VPO + "</a>";
                else
                    return '';

            }
            $scope.temp_Vendor = function (dataItem) {

                if (dataItem.Vendor)
                    return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Vendor + "</div><a href='#!/VendorDetails/" + dataItem.VendorId + "/" + dataItem.FranchiseId + "' style='color: rgb(61,125,139);'>" + dataItem.Vendor + "</a></span>";
                else
                    return '';

            }
            $scope.temp_AccountName = function (dataItem) {

                if (dataItem.AccountName)
                    return "<a href='#!/Accounts/" + dataItem.AccountId + "' style='color: rgb(61,125,139);'>" + dataItem.AccountName + "</a>";
                else
                    return '';

            }

            $scope.Gridsearch = function () {
                var searchValue = $('#searchBox').val();
                $("#HomeOfficeCaseList").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "CaseNo",
                          operator: "eq",
                          value: searchValue
                      },
                      {
                          field: "VendorCaseNo",
                          operator: "contains",
                          value: searchValue
                      },
                      {

                          field: "MPO",
                          operator: "eq",
                          value: searchValue
                      },
                      {

                          field: "VPO",
                          operator: "eq",
                          value: searchValue
                      },
                      {

                          field: "Vendor",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "AccountName",
                          operator: "contains",
                          value: searchValue
                      },
                       {
                           field: "Description",
                           operator: "contains",
                           value: searchValue
                       },
                        {
                            field: "StatusName",
                            operator: "contains",
                            // operator: (value) => (value + "").indexOf(searchValue) >= 0,
                            value: searchValue
                        },
                         {
                             field: "TypeName",
                             operator: "contains",
                             // operator: (value) => (value + "").indexOf(searchValue) >= 0,
                             value: searchValue
                         },
                          {
                              field: "DateTimeOpened",
                              operator: "contains",
                              value: searchValue
                          },
                           {
                               field: "CaseOwner",
                               operator: "contains",
                               value: searchValue
                           },
                            {
                                field: "Id",
                                operator: "eq",
                                value: searchValue
                            }
                    ]
                });

            }

            // QuoteList.Get(data);
            $scope.HomeOfficeCaseListGridOptions = kendotooltipService.setColumnWidth($scope.HomeOfficeCaseListGridOptions);
            window.onresize = function () {
                $scope.HomeOfficeCaseListGridOptions = kendotooltipService.setColumnWidth($scope.HomeOfficeCaseListGridOptions);
            };
            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
                };
            });
            // End Kendo Resizing

            $scope.GridSearchClearr = function () {
                $('#searchBox').val('');
                $("#HomeOfficeCaseList").data('kendoGrid').dataSource.filter({
                });
            }

        }]);



