﻿
// TODO: rquired refactoring:

'use strict';
appCP
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('HomeOfficeCaseManagementController',
    [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'FranchiseService', 'NavbarServiceCP', 'FileUploadService',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, FranchiseService, NavbarService, FileUploadService) {
            $scope.NavbarService = NavbarService;
            $scope.NavbarService.SelectOperation();
            $scope.NavbarService.EnableCaseMangementTab();
            $scope.VPOflag = false;


            $scope.RouteMPO = $routeParams.MPO;
            $scope.RouteLineNo = $routeParams.LineNo;
            $scope.RouteSO = $routeParams.SO;

           
            $scope.noOfLines = 0;



            $scope.FileUploadService = FileUploadService;
            $scope.submitted = false;
            $scope.CaseId = $routeParams.CaseId;
            //$scope.CaseNum = $routeParams.Id;
            HFCService.setHeaderTitle("Case Add/Edit #");

            $scope.Permission = {};
            var Casemanagementpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'CaseManagement ')
            $scope.Permission.CreateCaseData = Casemanagementpermission.CanCreate;
            $scope.Permission.EditCaseData = Casemanagementpermission.CanUpdate;
            $scope.FranchiseLevelCase = HFCService.FranchiseLevelCase;

            $scope.ShowEllipse = false;

            if ($scope.CaseId) {
                $scope.FileUploadService.SetCaseId($scope.CaseId, 'FranchiseCase');
            }
            else $scope.FileUploadService.clearCaseId();

            if (window.location.href.includes('/Case/')) {
                $scope.FileUploadService.css1 = "col-xl-7";
                $scope.FileUploadService.css2 = "col-sm-6 no-padding";
                $scope.FileUploadService.css3 = "col-xl-5";
                //$scope.FileUploadService.css4 = "clearfix";
                
            }

            $scope.$on('$locationChangeStart', function ($event, next, current) {
                 
                if ($('#caseGrid01').find('.k-grid-edit-row'))
                {
                    $scope.formCaseManage.$setDirty();
                }
            });


            $scope.CaseAddEdit = {
                Name: '',
                Status: '',
                StatusValue: '',
                CreatedOn: '',
                DateTimeOpened: '',
                DateTimeClosed: '',
                PurchaseOrdersDetailId: '',
                CellPhone: '',
                PurchaseOrderId: '',
                Email: '',
                OrderId: '',
                AccountName: '',
                SideMark: '',
                DateTimeStart: '',
                VPO_PICPO_PODId: '',
                MPO_MasterPONum_POId: '',
                SalesOrderId: '',
                IncidentDate: '',
                DateTimeClosed: '',
                LastUpdatedOn: '',
                Description: '',
                LastUPdatedName: '',
                CaseCreatedDate: '',
            }

            //get data on inital page load

            $scope.Initialvalue = function () {
                $http.get('/api/CaseManageAddEdit/0/GetUserDetails/').then(function (response) {
                   // 
                    $scope.CaseAddEdit.Name = response.data.Name;
                    //$scope.CaseAddEdit.CreatedOn = response.data.CreatedOn;
                    $scope.CaseAddEdit.DateTimeStart = response.data.CreatedOn;
                    $scope.CaseAddEdit.DateTimeOpened = $scope.CaseAddEdit.DateTimeStart;
                    $scope.CaseAddEdit.StatusValue = "New";
                    $scope.CaseAddEdit.DateTimeClosed = response.data.DateTimeClosed;     


                });
            }

            if (!($window.location.href.toUpperCase().includes('CASE/')))
            $scope.Initialvalue();
            //on select date update date time open
            $scope.ChangeDate = function (value) {
                //   $scope.CaseAddEdit.DateTimeOpened = value;
               // $scope.CaseAddEdit.IncidentDate = value;
            }



            ////////
            //$scope.CaseAddEdit.PurchaseOrderId = [];

            //$scope.CaseAddEdit.PurchaseOrderId[0] = 8;
         



            ////////


            //on select vpo on drop down
            $scope.ChangeVPO = function () {
                //
                $scope.clonee = false;
                if ($scope.CaseAddEdit.PurchaseOrdersDetailId.length == 0) {
                    $scope.CaseAddEdit.PurchaseOrderId = 0;
                    $scope.CaseAddEdit.OrderId = 0;
                    $scope.CaseAddEdit.AccountName = "";
                    $scope.CaseAddEdit.CellPhone = "";
                    $scope.CaseAddEdit.Email = "";
                    $scope.CaseAddEdit.SideMark = "";
                    $("#caseGrid01").data("kendoGrid").dataSource.data([]);
                }
                else {
                    if ($scope.CaseAddEdit.PurchaseOrdersDetailId != 0) {
                        var id = $scope.CaseAddEdit.PurchaseOrdersDetailId[0];
                        $http.get('/api/CaseManageAddEdit/' + id + '/ChangeValue').then(function (data) {

                            $scope.noOfLines = data.data.noOfLines;
                             
                            //  $scope.CaseAddEdit.PurchaseOrdersDetailId[0];
                            $scope.CaseAddEdit.PurchaseOrderId = data.data.PurchaseOrderId;
                            $scope.CaseAddEdit.OrderId = data.data.OrderId;
                            $scope.CaseAddEdit.AccountName = data.data.AccountName;
                            $scope.CaseAddEdit.CellPhone = data.data.CellPhone;
                            $scope.CaseAddEdit.Email = data.data.PrimaryEmail;
                            $scope.CaseAddEdit.SideMark = data.data.SideMark;
                          //  if ($scope.CaseId != null && $scope.CaseId != "" && $scope.CaseId != 0 && $scope.CaseId != undefined) {
                                
                            var data = $scope.PreviousValues;
                            if (data) {
                                if ($scope.CaseAddEdit.PurchaseOrdersDetailId[0] == data.VPO_PICPO_PODId) {
                                   $('#caseGrid01').data('kendoGrid').dataSource.read();
                                   $('#caseGrid01').data('kendoGrid').refresh();
                                  //  $scope.AddRow();
                                }
                                else {
                                    $("#caseGrid01").data("kendoGrid").dataSource.data([]);

                                    if($scope.CaseAddEdit.PurchaseOrdersDetailId[0])
                                    {
                                        $scope.VPOflag = true;
                                        $('#caseGrid01').data('kendoGrid').dataSource.read();
                                        $('#caseGrid01').data('kendoGrid').refresh();

                                    } else
                                        $scope.VPOflag = false;

                                    //$http.get('/api/CaseManageAddEdit/' + id + '/GetLinesForVpoChange').then(function (data) {


                                    //    //if (data.data)
                                    //    //{
                                    //    //    angular.forEach(data.data, function (value, key) {
                                    //    //        value

                                    //    //    });
                                    //    //}
                                    //    var grid = $("#caseGrid01").data("kendoGrid");
                                    //    grid.dataSource.add(data.data);

                                    //});
                                   // $scope.AddRow();
                                }
                            }
                            else {
                            
                                if ($scope.CaseAddEdit.PurchaseOrdersDetailId[0]) {
                                    $scope.VPOflag = true;
                                    $('#caseGrid01').data('kendoGrid').dataSource.read();
                                    $('#caseGrid01').data('kendoGrid').refresh();
                                }
                                else
                                    $scope.VPOflag = false;
                              //  $scope.AddRow();                               
                            }

                           
                           // }
                        });
                    }
                  
                }

            }

            //on select sales order on drop down
            $scope.ChangeSalesord = function () {
                $scope.clonee = false;
                if ($scope.CaseAddEdit.OrderId.length == 0) {

                    $scope.CaseAddEdit.PurchaseOrderId = 0;
                    $scope.CaseAddEdit.AccountName = "";
                    $scope.CaseAddEdit.CellPhone = "";
                    $scope.CaseAddEdit.Email = "";
                    $scope.CaseAddEdit.SideMark = "";
                    if ($scope.CaseAddEdit.PurchaseOrdersDetailId.length != 0) {
                        $scope.CaseAddEdit.PurchaseOrdersDetailId = 0;
                    }
                    $("#caseGrid01").data("kendoGrid").dataSource.data([]);
                }
                else {
                    if ($scope.CaseAddEdit.OrderId.length != 0) {
                        var id = $scope.CaseAddEdit.OrderId[0];
                        $http.get('/api/CaseManageAddEdit/' + id + '/ChangeSalesorderValue').then(function (data) {
                            $scope.noOfLines = data.data.noOfLines;
                            $scope.CaseAddEdit.OrderId;
                            $scope.CaseAddEdit.PurchaseOrderId = data.data.PurchaseOrderId;
                            $scope.CaseAddEdit.AccountName = data.data.AccountName;
                            $scope.CaseAddEdit.CellPhone = data.data.CellPhone;
                            $scope.CaseAddEdit.Email = data.data.PrimaryEmail;
                            $scope.CaseAddEdit.SideMark = data.data.SideMark;


                        });
                    }
                }

            }

            //on select mpo on drop down
            $scope.ChangeMPO = function () {
                $scope.clonee = false;
                if ($scope.CaseAddEdit.PurchaseOrderId.length == 0) {
                    $scope.CaseAddEdit.OrderId = 0;
                    $scope.CaseAddEdit.AccountName = "";
                    $scope.CaseAddEdit.CellPhone = "";
                    $scope.CaseAddEdit.Email = "";
                    $scope.CaseAddEdit.SideMark = "";
                    if ($scope.CaseAddEdit.PurchaseOrdersDetailId.length != 0) {
                        $scope.CaseAddEdit.PurchaseOrdersDetailId = 0;
                    }
                    $("#caseGrid01").data("kendoGrid").dataSource.data([]);
                }
                else {
                    if ($scope.CaseAddEdit.PurchaseOrderId.length != 0) {
                        var id = $scope.CaseAddEdit.PurchaseOrderId[0];
                        $http.get('/api/CaseManageAddEdit/' + id + '/ChangeonSelect').then(function (data) {

                            $scope.noOfLines = data.data.noOfLines;
                            
                            $scope.CaseAddEdit.PurchaseOrderId[0];
                            $scope.CaseAddEdit.OrderId = data.data.OrderId;
                            $scope.CaseAddEdit.AccountName = data.data.AccountName;
                            $scope.CaseAddEdit.CellPhone = data.data.CellPhone;
                            $scope.CaseAddEdit.Email = data.data.PrimaryEmail;
                            $scope.CaseAddEdit.SideMark = data.data.SideMark;
                            if ($scope.CaseId != null && $scope.CaseId != "" && $scope.CaseId != 0 && $scope.CaseId != undefined) {
                                
                                var data = $scope.PreviousValues;
                                if ($scope.CaseAddEdit.PurchaseOrderId[0] == data.MPO_MasterPONum_POId) {
                                    $('#caseGrid01').data('kendoGrid').dataSource.read();
                                    $('#caseGrid01').data('kendoGrid').refresh();
                                }
                                else {
                                    $("#caseGrid01").data("kendoGrid").dataSource.data([]);
                                }
                            }

                            //if($scope.QuoteLineIdFromRouteLineNo > 0)
                            //{
                            //    $scope.AddRow();
                            //}
                        });
                    }
                }

            }


            //if ($scope.RouteSO > 0 && $scope.RouteSO) {

            

            //    $scope.ChangeVPO();
            //}
            //else
                if ($scope.RouteLineNo > 0) {
                 

                $http.get('/api/CaseManageAddEdit/0/GetdetailsForMpoLineno?PurchaseOrderId=' + $scope.RouteMPO + '&LineNo=' + $scope.RouteLineNo).then(function (Success) {

                    if(Success.data)
                    {
                         
                        if (Success.data.PICPO && Success.data.PICPO > 0)
                        {
                            $scope.CaseAddEdit.PurchaseOrdersDetailId = [];
                            $scope.CaseAddEdit.PurchaseOrdersDetailId[0] = Success.data.PurchaseOrdersDetailId;

                            $scope.QuoteLineIdFromRouteLineNo = Success.data.QuoteLineId;

                            $scope.ChangeVPO();

                        }
                        else
                        {

                             
                            $scope.CaseAddEdit.PurchaseOrderId = [];

                            $scope.CaseAddEdit.PurchaseOrderId[0] = $scope.RouteMPO;
                            $scope.QuoteLineIdFromRouteLineNo = Success.data.QuoteLineId;
                            $scope.ChangeMPO();

                        }

                    }
                });
              
            }
            else if ($scope.RouteMPO > 0)
            {
                 
                $scope.CaseAddEdit.PurchaseOrderId = [];

                $scope.CaseAddEdit.PurchaseOrderId[0] = $scope.RouteMPO;
                $scope.ChangeMPO();
            }

            //to get vpo drop down value
            $scope.VPODrop = {
                placeholder: "Select",
                dataTextField: "VPO_PICPO_PODId",
                dataValueField: "PurchaseOrdersDetailId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/CaseManageAddEdit/null/GetVPOData",
                        }
                    }
                },
            };

            //to get mpo drop down value
            $scope.MPODrop = {
                placeholder: "Select",
                dataTextField: "MPO_MasterPONum_POId",
                dataValueField: "PurchaseOrderId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/CaseManageAddEdit/0/GetMPOData",
                        }
                    }
                },
            };
            //to get sales order drop down value
            $scope.SalesOrderDrop = {
                placeholder: "Select",
                dataTextField: "SalesOrderId",
                dataValueField: "OrderId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/CaseManageAddEdit/0/GetSalesOrderData",
                        }
                    }
                },
            };

            $scope.kendoValidator = function (gridId) {
                return $("#" + gridId).kendoValidator({
                    validate: function (e) {
                        $("span.k-invalid-msg").hide();
                        var dropDowns = $(".k-dropdown");
                        $.each(dropDowns, function (key, value) {
                            var input = $(value).find("input.k-invalid");
                            var span = $(this).find(".k-widget.k-dropdown.k-header");
                            // 
                            if (input.size() > 0) {
                                $(this).addClass("dropdown-validation-error");
                            } else {
                                $(this).removeClass("dropdown-validation-error");
                            }
                        });
                    }
                }).getKendoValidator();
            }

           

            //add new row to grid
            $scope.AddRow = function () {
                $scope.clonee = false;

                $scope.dataitemToedit = "";
              //  $scope.resetvalues();
                 
               // $("#caseGrid01").data("kendoGrid").dataSource.data([]);
                var grid = $("#caseGrid01").data("kendoGrid");
                var rowEdit = $('#caseGrid01').find('.k-grid-edit-row');

                if (rowEdit.length) {
                    //grid is not in edit mode
                    var validator = $scope.kendoValidator("caseGrid01");
                    if (!validator.validate()) {
                        return false;
                    }
                    $scope.ADD_New();

                }
                else {
                    grid.addRow();
                    $scope.EditMode = true;
                    $scope.resetvalues();
                    $scope.selectedRow = 0;


                    //if ($scope.RouteLineNo && $scope.RouteLineNo > 0)
                    //{
                    //var item = $('#caseGrid01').find('.k-grid-edit-row');
                    //item.data().uid;
                    //var dataItem = item.data().$scope.dataItem;
                    //onChangeValue($scope.QuoteLineIdFromRouteLineNo, dataItem);
                    //}
                    //else {
                    //    if ($scope.CaseAddEdit.PurchaseOrdersDetailId) {
                    //        if ($scope.CaseAddEdit.PurchaseOrdersDetailId[0]) {
                    //            var item = $('#caseGrid01').find('.k-grid-edit-row');
                    //            item.data().uid
                    //            var dataItem = item.data().$scope.dataItem;
                    //            $http.get('/api/CaseManageAddEdit/' + $scope.CaseAddEdit.PurchaseOrdersDetailId[0] + '/getLineNumber').then(function (success) {                  
                    //                if (success.data);
                    //                {
                    //                    $scope.QuoteLineNumberForVpoChange = success.data.QuoteLineNumber;
                    //                    onChangeValue(success.data.QuoteLineId, dataItem);
                    //                }
                    //            });
                                
                    //        }
                    //        }
                    //}
                }
                $scope.formCaseManage.$setDirty();
            }
                   

            $scope.ADD_New = function () {
                 

                var grid = $("#caseGrid01").data("kendoGrid");
                var dataSource = grid.dataSource;
                if ($scope.selectedRow >= 0)
                {
                    if ($scope.ProductNamem)
                        grid.dataSource._data[$scope.selectedRow].ProductName = $scope.ProductNamem;
                    if ($scope.ProductIdm)
                        grid.dataSource._data[$scope.selectedRow].ProductId = $scope.ProductIdm;
                    if ($scope.ProductDescriptionm)
                        grid.dataSource._data[$scope.selectedRow].Description = $scope.ProductDescriptionm;
                    if ($scope.VendorNamem)
                        grid.dataSource._data[$scope.selectedRow].VendorName = $scope.VendorNamem;
                    if ($scope.VendorReferencem)
                        grid.dataSource._data[$scope.selectedRow].VendorReference = $scope.VendorReferencem;
                    if ($scope.vpom)
                        grid.dataSource._data[$scope.selectedRow].VPO = $scope.vpom;
                    if ($scope.LineNumberQuote)
                        grid.dataSource._data[$scope.selectedRow].QuoteLineNumber = $scope.LineNumberQuote;
                    if ($scope.TypeValDesc)
                        grid.dataSource._data[$scope.selectedRow].Typevalue = $scope.TypeValDesc;
                    if ($scope.ReasonvalDesc)
                        grid.dataSource._data[$scope.selectedRow].CaseReason = $scope.ReasonvalDesc;
                    if ($scope.StatusvalDesc)
                        grid.dataSource._data[$scope.selectedRow].StatusName = $scope.StatusvalDesc;
                    if ($scope.ResolutionvalDesc)
                        grid.dataSource._data[$scope.selectedRow].ResolutionName = $scope.ResolutionvalDesc;

                 //   $("#caseGrid01").data("kendoGrid").setDataSource(grid.dataSource._data);
                }
            
                var validator = $scope.kendoValidator("caseGrid01");
                if (validator.validate()) {
                    var newItem = {
                        ExpediteReq: false,
                        ExpediteApproved: false,
                        ReqTripCharge: false,
                        TripChgApp: "$",
                        CaseId:'',
                    }                


                    var gridd = $("#caseGrid01").data("kendoGrid");
                    var dataSource = gridd.dataSource;
                    var newItem = dataSource.insert(0, newItem);
                    var newRow = gridd.items().filter("[data-uid='" + newItem.uid + "']");
                    $scope.selectedRow = 0;
                    gridd.editRow(newRow);
                    $scope.resetvalues();
                }
                else {
                    return false;
                }
            };

            $scope.SaveGridData = function () {
                
                var dto = $scope.CaseAddEdit;
                if (dto.CaseId != 0) {
                    var validator = $scope.kendoValidator("caseGrid01");
                    if (validator.validate()) {
                        var grid = $("#caseGrid01").data("kendoGrid");
                        var dataSource = grid.dataSource._data;
                        for (i = 0; i < dataSource.length; i++) {
                            dataSource[i].QuoteLineId2 = dataSource[i].QuoteLineId;

                        }
                        dto.AdditionalInfo = dataSource;
                        var caseid = dto.CaseId;
                    }
                    else return;

                    $http.post('/api/CaseManageAddEdit/' + caseid + '/SaveGridLine', dto).then(function (response) {

                        var Id = response.data;
                        var arrId = Id.split('|');
                         
                        if (arrId.length != null || arrId.length != "" || arrIdId.length != undefined) {
                            $scope.CaseNum = arrId[0];
                            // $('#caseGrid01').data('kendoGrid').dataSource.read();
                            // $('#caseGrid01').data('kendoGrid').refresh();

                            if (arrId[1])
                            $http.get('/api/CaseManageAddEdit/0/GetCase?CaseId=' + arrId[1]).then(function (data) {
                                
                                var dataSource = new kendo.data.DataSource({ data: data.data });
                                var grid = $('#caseGrid01').data("kendoGrid");
                                dataSource.read();
                                grid.setDataSource(dataSource);
                                $scope.ADD_New();
                                $scope.ShowEllipse = true;
                            })
                        }
                    });

                }
            }

            //on click on cancel button 
            $scope.CancelChanges = function () {

                if ($scope.RouteSO && $scope.RouteSO > 0)
                {
                    $window.location.href = "#!/Orders/" + $scope.RouteSO;
                } else if ($scope.RouteMPO && $scope.RouteMPO > 0) {
                    $window.location.href = "#!/purchasemasterview/" + $scope.RouteMPO;
                }
                else {

                    if (!($window.location.href.toUpperCase().includes('CASEADDEDIT/')))
                        $window.location.href = "#!/CaseView/" + $scope.CaseId;
                    else $window.location.href = "#!/homeofficeCase";
                }

            };


            function ProductDescription(dataitem) {
                var template = "";
                // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
                if (dataitem.Description) {
                    var erer = dataitem.Description.replace('"', "");
                    erer = erer.replace(/<[^>]*>/g, '');
                    template += '<span title="' + erer + '">' + dataitem.Description + '</span>'

                    return template;
                } else return '';
            }

            //$scope.$watch('CaseAddEdit.Description', function () {
            //    if ($scope.CaseAddEdit.Description.length > 1000)
            //    {
            //        $scope.bordercolor = "red";
            //        $scope.textsize = false;
            //    }
            //    else {
            //        $scope.bordercolor = "";
            //        $scope.textsize = true;
            //    }
            //});

            //grid code.
            $scope.CaseinfoGridOptions = {
                dataSource: {
                   // data :$scope.dataGrid,
                    //type: "jsonp",
                    transport: {
                        cache: false,
                        read: function (e) {
                            
                            if ($scope.CaseId == null || $scope.CaseId == "" || $scope.CaseId == undefined) {
                                var id_val = 0;
                            }
                            else id_val = $scope.CaseId;

                           var url = "/api/CaseManageAddEdit/" + 0 + "/GetCase?CaseId=" + id_val;
                            if ($scope.VPOflag == true)
                                url = '/api/CaseManageAddEdit/' + $scope.CaseAddEdit.PurchaseOrdersDetailId[0] + '/GetLinesForVpoChange';

                            $scope.VPOflag = false;
                            $http.get(url, e)
                                .then(function (data) {
                                    e.success(data.data);
                                }).catch(function (error) {
                                    HFC.DisplayAlert(error);
                                    //var loadingElement = document.getElementById("loading");
                                    //loadingElement.style.display = "none";

                                });
                        },
                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                QuoteLineNumber: { editable: true },
                                VPO: { editable: false },
                                ProductName: { editable: false },
                                ProductId: { editable: false },
                                Description: { editable: false},
                                VendorName: { editable: false },
                                Typevalue: { editable: true },
                                CaseReason: { editable: true },
                                IssueDescription: { editable: true },
                                ExpediteReq: { type: "boolean", editable: true },
                                ExpediteApproved: { type: "boolean", editable: false },
                                ReqTripCharge: { type: "boolean", editable: true },
                                TripChargeApprovedName: { editable: false },
                                StatusName: { editable: true },
                                ResolutionName: { editable: true },
                                VendorReference: {editable: false}
                            }
                        }
                    },
                },

                //autoSync: true,
                batch: true,
                cache: false,
                toolbar: [
                    {
                        template: function (headerData) {
                            return $("#headToolbarTemplate").html()
                        }
                    }
                ],
               // sortable: true,
               // filterable: true,
                resizable: true,
                editable: "inline",
                dataBound: function (e) {
                    if ($("#caseGrid01").data("kendoGrid")) {
                        if ($("#caseGrid01").data("kendoGrid").dataSource)
                            if ($("#caseGrid01").data("kendoGrid").dataSource._data) {
                                $scope.noOfLines1 = $("#caseGrid01").data("kendoGrid").dataSource._data.length;
                                console.log($scope.noOfLines1);
                            }
                    }
                  
                },
                //dataBound: function (e) {
                //    $scope.quoteLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource._data.length;
                //},
                noRecords: { template: "No records found" },              
                columns: [
                     {
                         field: "Id",
                         title: "Id",
                         filterable: { multi: true, search: true },
                         hidden: true,
                     },
                    {
                        field: "QuoteLineNumber",
                        title: "Line",
                        width: "120px",
                        filterable: { multi: true, search: true },
                        hidden: false,
                        editor: function (container, options) {
                            
                            $('<input required="true" id="LineNumberDropdown" name="QuoteLineId" data-bind="value:QuoteLineId" style="border: none;"  />')
                                 .appendTo(container)
                                 .kendoDropDownList({
                                     autoBind: true,
                                     optionLabel: "Select",
                                     valuePrimitive: false,
                                     dataTextField: "QuoteLineNumber",
                                     dataValueField: "QuoteLineId",
                                     template: "#=QuoteLineNumber #",
                                     
                                     change: function (e, options) {
                                          
                                         var item = $('#caseGrid01').find('.k-grid-edit-row');
                                         item.data().uid
                                         var dataItem = item.data().$scope.dataItem;
                                         if (dataItem.QuoteLineId.QuoteLineId) {
                                             onChangeValue(dataItem.QuoteLineId.QuoteLineId, dataItem);
                                         }
                                         else
                                             onChangeValue(dataItem.QuoteLineId, dataItem);

                                         if (e.sender.selectedIndex > 0) {
                                             var a = $("#LineNumberDropdown").parent();
                                             $(a).removeClass("dropdown-validation-error");
                                         }
                                         else {
                                             var a = $("#LineNumberDropdown").parent();
                                             $(a).addClass("dropdown-validation-error");
                                         }
                                     },

                                     dataSource: {
                                         transport: {
                                             read: {

                                                 url: function (readparam) {
                                                     
                                                     if ($scope.CaseAddEdit.PurchaseOrdersDetailId != undefined &&
                                                         $scope.CaseAddEdit.PurchaseOrdersDetailId != "" &&
                                                         $scope.CaseAddEdit.PurchaseOrdersDetailId != null) {
                                                         var id = $scope.CaseAddEdit.PurchaseOrdersDetailId;
                                                     }
                                                     else id = 0;
                                                     if ($scope.CaseAddEdit.PurchaseOrderId != undefined &&
                                                         $scope.CaseAddEdit.PurchaseOrderId != "" &&
                                                         $scope.CaseAddEdit.PurchaseOrderId != null) {
                                                         var Mpoid = $scope.CaseAddEdit.PurchaseOrderId;
                                                     }
                                                     else Mpoid = null;

                                                     var aa = $scope.dataitemToedit;
                                                     var linenos = "";
                                                     $("#caseGrid01").data("kendoGrid").dataSource._data
                                                     if ($("#caseGrid01").data("kendoGrid"))
                                                         if ($("#caseGrid01").data("kendoGrid").dataSource)
                                                             if ($("#caseGrid01").data("kendoGrid").dataSource._data)
                                                             {
                                                                 if (aa && $scope.clonee=== false) {
                                                                 for(var t = 0;t < $("#caseGrid01").data("kendoGrid").dataSource._data.length; t++)
                                                                 {
                                                                     
                                                                     if (aa.QuoteLineNumber != $("#caseGrid01").data("kendoGrid").dataSource._data[t].QuoteLineNumber && $("#caseGrid01").data("kendoGrid").dataSource._data[t].QuoteLineNumber)
                                                                         {
                                                                             if (!linenos)
                                                                                 linenos = $("#caseGrid01").data("kendoGrid").dataSource._data[t].QuoteLineNumber;
                                                                             else
                                                                                 linenos = linenos + "," + $("#caseGrid01").data("kendoGrid").dataSource._data[t].QuoteLineNumber;

                                                                         }
                                                                     }
                                                                 }
                                                                 else
                                                                 {
                                                                     for (var t = 0; t < $("#caseGrid01").data("kendoGrid").dataSource._data.length; t++) {

                                                                         if ($("#caseGrid01").data("kendoGrid").dataSource._data[t].QuoteLineNumber) {
                                                                             if (!linenos)
                                                                                 linenos = $("#caseGrid01").data("kendoGrid").dataSource._data[t].QuoteLineNumber;
                                                                             else
                                                                                 linenos = linenos + "," + $("#caseGrid01").data("kendoGrid").dataSource._data[t].QuoteLineNumber;
                                                                         }
                                                                     }
                                                                 }

                                                             }
                                                    
                                                     var url1 = '/api/CaseManageAddEdit/' + id + '/GetLineData?Mpoid= ' + Mpoid + '&linenos=' + linenos;
                                                     return url1;
                                                 }
                                             }
                                         },
                                     }
                                 });

                            var prodctTypedd = $("#LineNumberDropdown").data("kendoDropDownList");
                            prodctTypedd.dataSource.read();
                           
                        }
                    },
                     {
                         field: "VPO",
                         title: "VPO",
                         width: "200px",
                         editor: '<input id="VPO_txt" readonly name="VPO" class="k-textbox"  data-bind="value:VPO" style="border: none "/>',
                         filterable: { multi: true, search: true },                         
                         hidden: false,
                     },
                      {
                          field: "ProductName",
                          title: "Product",
                          width: "200px",
                          editor: '<input id="product_txt"  readonly name="ProductName" class="k-textbox"  data-bind="value:ProductName" style="border: none "/>',
                          filterable: { multi: true, search: true },
                          hidden: false,
                      },
                      {
                          field: "ProductId",
                          title: "Product No",
                          width: "200px",
                          editor: '<input id="productnumber_txt" readonly data-type="text"  class="k-textbox "  name="ProductId" data-bind="value:ProductId"/>',
                          filterable: { multi: true, search: true },
                          hidden: false,
                      },
                      {
                          //Description
                          field: "Description",
                          attributes: {
                                 
                              style: "white-space: nowrap;overflow: hidden !important; text-overflow: ellipsis; "
                          },
                          title: "Product Description",
                          width: "220px",
                        //  template: "#=Description #",
                         // editable:false,
                          editor: '<input id="productdescri_txt" readonly data-type="text" class="k-textbox"  name="Description" data-bind="value:Description"/>',
                          //filterable: { multi: true, search: true },
                          template: function (dataitem) {
                              return ProductDescriptionn(dataitem);
                          },

                          hidden: false,
                      },
                      {
                          field: "VendorName",
                          title: "Vendor",
                          width: "200px",
                          editor: '<input id="Vendor_txt" readonly data-type="text"  class="k-textbox "  name="VendorName" data-bind="value:VendorName"/>',
                          filterable: { multi: true, search: true },
                          hidden: false,
                      },
                      {
                          field: "Typevalue",
                          title: "Issue Type",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          template: function(dataItem){
                              if (dataItem.Typevalue)
                                  return dataItem.Typevalue;
                              else
                                  return '<span style="color:red;">Required</span>'
                          },
                          editor: function (container, options) {
                              $('<input required=true  id="Type_Val" name="Type" data-bind="value:Type" style="border: none "  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: false,
                                      optionLabel: "Select",
                                      valuePrimitive: true,
                                      dataTextField: "Typevalue",
                                      dataValueField: "Type",
                                      template: "#=Typevalue #",

                                      change: function (e, options) {
                                          var item = $('#caseGrid01').find('.k-grid-edit-row');
                                          item.data().uid
                                          var dataItem = item.data().$scope.dataItem;
                                          if (dataItem.Type) {
                                              onTypeChangeValue(dataItem.Type, dataItem);
                                          }
                                      //    $scope.Roweditvalidation(dataItem);


                                          if (e.sender.selectedIndex > 0) {
                                              var a = $("#Type_Val").parent();
                                              $(a).removeClass("dropdown-validation-error");
                                          }
                                          else
                                          {
                                              var a = $("#Type_Val").parent();
                                              $(a).addClass("dropdown-validation-error");
                                          }
                                          
                                      },

                                      dataSource: {
                                          transport: {
                                              read: {
                                                  url: '/api/CaseManageAddEdit/0/GetCaseType?Tableid=' + 5,
                                              }
                                          },
                                      }

                                  });
                          }


                      },
                      {
                          field: "CaseReason",
                          title: "Case Reason",
                          width: "150px",
                          filterable: { multi: true, search: true },
                          hidden: false,
                          template: function (dataItem) {
                              if (dataItem.CaseReason)
                                  return dataItem.CaseReason;
                              else
                                  return '<span style="color:red;">Required</span>'
                          },
                          editor: function (container, options) {
                              $('<input required=true  id="Case_Val"  name="ReasonCode" data-bind="value:ReasonCode" style="border: none "  />')
                                  .appendTo(container)
                                  .kendoDropDownList({
                                      autoBind: false,
                                      optionLabel: "Select",

                                      valuePrimitive: true,
                                      dataTextField: "Reasonvalue",
                                      dataValueField: "ReasonCode",
                                      template: "#=Reasonvalue #",

                                      change: function (e, options) {
                                          var item = $('#caseGrid01').find('.k-grid-edit-row');
                                          item.data().uid
                                          var dataItem = item.data().$scope.dataItem;
                                          if (dataItem.ReasonCode) {
                                              onCaseChangeValue(dataItem.ReasonCode);
                                          }

                                          if (e.sender.selectedIndex > 0) {
                                              var a = $("#Case_Val").parent();
                                              $(a).removeClass("dropdown-validation-error");
                                          }
                                          else {
                                              var a = $("#Case_Val").parent();
                                              $(a).addClass("dropdown-validation-error");
                                          }

                                      },

                                      dataSource: {
                                          transport: {
                                              read: {
                                                  url: '/api/CaseManageAddEdit/0/GetCaseReason?Tableid=' + 6,
                                              }
                                          },
                                      }

                                  });
                          }


                      },
                     {
                         field: "IssueDescription",
                         title: "Issue Description",
                         width: "150px",
                         filterable: { multi: true, search: true },
                         hidden: false,
                         template: function (dataItem) {
                             if (dataItem.IssueDescription)
                                 return dataItem.IssueDescription;
                             else
                                 return '<span style="color:red;">Required</span>'
                         },
                         editor: '<input   required=true name="IssueDescription" id="IssueDescription"  class="k-textbox" data-bind="value:IssueDescription" style="border: none "/>',

                     },
                    {
                        field: "ExpediteReq",
                        title: "Expedite Req",
                        width: "150px",
                        filterable: { multi: true, search: true },
                        template: '<input type="checkbox"  #= ExpediteReq ? "checked=checked" : "" # disabled="disabled" class="option-input checkbox" ></input>',

                    },
                      {
                          field: "ExpediteApproved",
                          title: "Expedite Approved",
                          editable: false,
                          width: "150px",
                          filterable: { multi: true, search: true },
                          template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',
                              //function(dataItem){
                              //if (dataItem.ExpediteApproved == true)
                              //    return '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>';
                              //else
                              //    return '<input  type="checkbox" class="option-input checkbox" disabled="true"></input>';
                          
                         // }
                       //   template: '<input  type="checkbox" #= ExpediteApproved ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',

                      },
                       {
                           field: "ReqTripCharge",
                           title: "Req Trip Charge",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           template: '<input type="checkbox" #= ReqTripCharge ? "checked=checked" : "" #  disabled="disabled" class="option-input checkbox" ></input>',

                       },
                       {
                           field: "TripChargeApprovedName",
                           title: "Approved Trip Charge",
                           width: "200px",
                           editor: function (container, options) {
                               $('<input required=true  id="Trip_val" name="TripCharge" data-bind="value:TripChargeApproved" style="border: none "  />')
                                   .appendTo(container)
                                   .kendoDropDownList({
                                       autoBind: false,
                                       optionLabel: "Select",

                                       valuePrimitive: true,
                                       dataTextField: "TripChargevalue",
                                       dataValueField: "TripChargeApproved",
                                       template: "#=TripChargevalue #",

                                       change: function (e, options) {
                                           var item = $('#caseGrid01').find('.k-grid-edit-row');
                                           item.data().uid
                                           var dataItem = item.data().$scope.dataItem;
                                           if (dataItem.TripChargeApproved) {
                                               onTripChargeValue(dataItem.TripChargeApproved);
                                           }

                                       },

                                       dataSource: {
                                           transport: {
                                               read: {

                                                   url: '/api/CaseManageAddEdit/0/GetTripChargeValue?Tableid=' + 9,
                                               }
                                           },
                                       }

                                   });
                           }
                           //template: function(dataItem){

                           //    if(dataItem.TripChargeApproved != null)
                           //    {
                           //        return '<div style="width: 200px;" readonly>$</div> <input id="TripChg_App" style="width:130px;" readonly name="TripChgApp" data-bind="value:TripChgApp" style="border: none "/>'
                           //    }
                           //    return '';
                           //}
                           //template: '<div style="width: 200px;" readonly>$</div>',
                           //editor: '<input id="TripChg_App" style="width:130px;" readonly name="TripChgApp" data-bind="value:TripChgApp" style="border: none "/>',

                       },
                       {
                           field: "StatusName",
                           title: "Status",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           template: function (dataItem) {
                               if (dataItem.StatusName)
                                   return dataItem.StatusName;
                               else
                                   return '<span style="color:red;">Required</span>'
                           },
                           editor: function (container, options) {
                               $('<input required=true  id="Status_val" name="Status" data-bind="value:Status"  style="border: none "  />')
                                   .appendTo(container)
                                   .kendoDropDownList({
                                       autoBind: false,
                                     optionLabel: "Select",

                                       valuePrimitive: true,
                                       dataTextField: "Statusvalue",
                                       dataValueField: "Status",
                                       template: "#=Statusvalue #",

                                       change: function (e, options) {
                                           var item = $('#caseGrid01').find('.k-grid-edit-row');
                                           item.data().uid
                                           var dataItem = item.data().$scope.dataItem;
                                           if (dataItem.Status) {
                                               onStatusChangeValue(dataItem.Status);
                                           }


                                           if (e.sender.selectedIndex > 0) {
                                               var a = $("#Status_val").parent();
                                               $(a).removeClass("dropdown-validation-error");
                                           }
                                           else {
                                               var a = $("#Status_val").parent();
                                               $(a).addClass("dropdown-validation-error");
                                           }

                                       },

                                       dataSource: {
                                           transport: {
                                               read: {

                                                   url: '/api/CaseManageAddEdit/0/GetCaseStatus?Tableid=' + 7,
                                               }
                                           },
                                       }

                                   });

                               var prodctTypedd = $("#Status_val").data("kendoDropDownList");
                               prodctTypedd.dataSource.read();

                           }


                       },
                       {
                           field: "ResolutionName",
                           title: "Resolution",
                           width: "150px",
                           filterable: { multi: true, search: true },
                           hidden: false,
                           template: function (dataItem) {
                               if (dataItem.ResolutionName)
                                   return dataItem.ResolutionName;
                               else
                                   return '' // '<span style="color:red;">Required</span>'
                           },
                           editor: function (container, options) {
                               $('<input  id="Resolution_val" name="Resolution" data-bind="value:Resolution" style="border: none "  />')
                                   .appendTo(container)
                                   .kendoDropDownList({
                                       autoBind: false,
                                       optionLabel: "Select",

                                       valuePrimitive: true,
                                       dataTextField: "Resolutionvalue",
                                       dataValueField: "Resolution",
                                       template: "#=Resolutionvalue #",

                                       change: function (e, options) {
                                           var item = $('#caseGrid01').find('.k-grid-edit-row');
                                           item.data().uid
                                           var dataItem = item.data().$scope.dataItem;
                                           if (dataItem.Resolution) {
                                               onResolutionChangeValue(dataItem.Resolution);
                                           }


                                           if (e.sender.selectedIndex > 0) {
                                               var a = $("#Resolution_val").parent();
                                               $(a).removeClass("dropdown-validation-error");
                                           }
                                           else {
                                               //var a = $("#Resolution_val").parent();
                                               //$(a).addClass("dropdown-validation-error");
                                           }

                                       },

                                       dataSource: {
                                           transport: {
                                               read: {

                                                   url: '/api/CaseManageAddEdit/0/GetCaseResolution?Tableid=' + 8,
                                               }
                                           },
                                       }

                                   });
                           }


                       }, {
                           field: "VendorReference",
                           title: "Vendor Reference",
                           width: "120px",
                           editor: '<input id="VendorRef_txt" readonly data-type="text"  class="k-textbox "  name="VendorRef_txt" data-bind="value:VendorReference"/>',
                           filterable: { multi: true, search: true },
                           template: function (dataitem) {
                               if (dataitem.VendorReference)
                                   return dataitem.VendorReference;
                               else
                                   return '';
                           },
                           hidden: false,
                       },
                        {
                            field: "",
                            title: "",
                            width: "60px",
                            // template: '<ul><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="DeleteRowDetails(dataItem)">Edit</a></li>  <li><a  href="javascript:void(0)" ng-click="DeleteRowDetails(this.dataItem)">Delete</a></li><li><a href="javascript:void(0)"  ng-click="CopyData(dataItem)">Copy</a></li> </ul> </li> </ul>'
                            template: function (dataitem) {
                                var hcontent = '<ul><li class ="dropdown note1 ad_user"><div class ="dropdown"><button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown" ng-click="bringDropdown($event,dataItem )"><i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i></button></div></li></ul>'

                                return hcontent;

                            }
                        },

                ]

            };


        

            var i = 0;
            $scope.bringDropdown = function (e, data)
            {
                $scope.dataitemToedit = data;
                var event = e;
                i++;
                setTimeout(function () {
                    i = 0;
                }, 500);
                if (i == 1) {
                    setDropdown(event);
                }
                e.stopPropagation();
                e.preventDefault();
            }

            var setDropdown = function (e) {
                if ($('#dropforGrid').hasClass("openDrop") && (($scope.offset + 32) > e.pageY && ($scope.offset - 32) < e.pageY)) {
                    $('#dropforGrid').removeClass("openDrop")
                    $('#dropforGrid').css({
                        'display': 'none'
                    });
                }
                else {
                    $scope.offset = e.pageY;
                    var top = ($scope.offset - 110) + "px";
                    $('#dropforGrid').css({
                        'top': top,
                        'display': 'block'
                    });
                    $('#dropforGrid').addClass("openDrop");
                }
            }

            function ProductDescriptionn(dataitem) {
                var template = "";
                // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
               // if (dataitem.Description) {
                var erer = dataitem.Description.replace('"', "");
                //if (erer[0] == '<') {

                //}
                erer = erer.replace(/<[^>]*>/g, '');
                    template += '<span title="' + erer + '">' + dataitem.Description + '</span>'
                    return template;
                //}
                //else return '';
               // else return '<input id="productdescri_txt" readonly data-type="text" class="k-textbox"  name="Description" data-bind="value:Description"/>';
            }
          

            //save
            $scope.SaveCase = function () {
                 
                var grid = $("#caseGrid01").data("kendoGrid");
                if (grid != undefined) {

                    var dataSourcee = grid.dataSource._data;

                    for (i = 0; i < dataSourcee.length; i++) {
                        //dataSource[i].QuoteLineId2 = dataSource[i].QuoteLineId;
                        if (typeof (dataSourcee[i].QuoteLineId) == 'object') {
                            var localdata = dataSourcee[i].QuoteLineId;
                            dataSourcee[i].QuoteLineId = dataSourcee[i].QuoteLineId.QuoteLineId;
                            dataSourcee[i].QuoteLineNumber = localdata.QuoteLineNumber;
                        }

                        else dataSourcee[i].QuoteLineId = dataSourcee[i].QuoteLineId;

                    }

                    for (var i = 0; i < dataSourcee.length; i++) {
                        for (var j = i; j < dataSourcee.length; j++) {
                            if (i != j && dataSourcee[i].QuoteLineId == dataSourcee[j].QuoteLineId) {
                                alert("Duplicates not allowed in line items")
                                return;
                            }
                        }
                    }
                }

                $scope.submitted = true;

                if ($scope.formCaseManage.$invalid) {
                    return;
                }
                if (!$scope.CaseAddEdit.Description) {
                    HFC.DisplayAlert("Description is required")
                    return;
                }

                if ($scope.CaseAddEdit.PurchaseOrdersDetailId.length == 0 &&
                    $scope.CaseAddEdit.PurchaseOrderId.length == 0) {
                    HFC.DisplayAlert("VPO/MPO is required")
                    return;
                }
                if ($scope.CaseAddEdit.PurchaseOrdersDetailId.length == 0 &&
                   $scope.CaseAddEdit.PurchaseOrderId == 0) {
                    HFC.DisplayAlert("VPO/MPO is required")
                    return;
                }

                var grid = $("#caseGrid01").data("kendoGrid");
                if (grid == undefined) {
                    HFC.DisplayAlert("Case line item required");
                    return;
                }
                if (grid._data == undefined) {
                    HFC.DisplayAlert("Case line item required");
                    return;
                }
                if (!(grid._data.length > 0))
                {
                    HFC.DisplayAlert("Case line item required");
                    return;
                }

                var dto = $scope.CaseAddEdit;

                //vpo id
                if (dto.PurchaseOrdersDetailId == undefined || dto.PurchaseOrdersDetailId == "" || dto.PurchaseOrdersDetailId == null)
                    dto.VPO_PICPO_PODId = 0;
                else dto.VPO_PICPO_PODId = $scope.CaseAddEdit.PurchaseOrdersDetailId[0];

                if (dto.VPO_PICPO_PODId == undefined) dto.VPO_PICPO_PODId = $scope.CaseAddEdit.PurchaseOrdersDetailId;
                else if ($scope.CaseAddEdit.PurchaseOrdersDetailId[0] == null) dto.VPO_PICPO_PODId = 0;
                else dto.VPO_PICPO_PODId = $scope.CaseAddEdit.PurchaseOrdersDetailId[0];
                //mpo id
                if (dto.PurchaseOrderId == undefined || dto.PurchaseOrderId == "" || dto.PurchaseOrderId == null) {
                    dto.MPO_MasterPONum_POId = 0;
                }
                else {
                    if (dto.PurchaseOrderId.length > 1) {
                        dto.MPO_MasterPONum_POId = $scope.CaseAddEdit.PurchaseOrderId;
                    }
                    else
                        dto.MPO_MasterPONum_POId = $scope.CaseAddEdit.PurchaseOrderId[0];
                    if (dto.MPO_MasterPONum_POId == undefined) {
                        dto.MPO_MasterPONum_POId = $scope.CaseAddEdit.PurchaseOrderId;
                    }

                }

                //order id
                if ($scope.CaseAddEdit.OrderId != undefined || $scope.CaseAddEdit.OrderId != null || $scope.CaseAddEdit.OrderId != "") {
                    dto.SalesOrderId = $scope.CaseAddEdit.OrderId[0];
                    if (dto.SalesOrderId == undefined) {
                        dto.SalesOrderId = $scope.CaseAddEdit.OrderId;
                    }
                    else dto.SalesOrderId = $scope.CaseAddEdit.OrderId[0];
                }
                else
                    dto.SalesOrderId = $scope.CaseAddEdit.OrderId;

                $scope.CaseAddEdit.IncidentDate = dto.DateTimeStart;
                var AdditionalInfo = [];
               // if (dto.CaseId) {
                    var validator = $scope.kendoValidator("caseGrid01");
                    if (validator.validate()) {
                        var grid = $("#caseGrid01").data("kendoGrid");
                        var dataSource = grid.dataSource._data;
                        $scope.InCompLines = "Please select the ";
                        var j=0,k=0,l=0,m=0;
                        for (i = 0; i < dataSource.length; i++) {
                            if ((!dataSource[i].Type || dataSource[i].Type < 1) && j === 0) {
                                // $scope.InCompLines = $scope.InCompLines + '  ' + dataSource[i].QuoteLineNumber;
                                if ($scope.InCompLines == "Please select the ")
                                    $scope.InCompLines = $scope.InCompLines + "Issue Type";
                                else
                                    $scope.InCompLines = $scope.InCompLines + ", Issue Type";

                                j = 1;
                            }
                            else if ((!dataSource[i].ReasonCode || dataSource[i].ReasonCode < 1) && k === 0) {
                                //  $scope.InCompLines = $scope.InCompLines + '  ' + dataSource[i].QuoteLineNumber;
                                if ($scope.InCompLines == "Please select the ")
                                    $scope.InCompLines = $scope.InCompLines + "Case Reason";
                                else
                                    $scope.InCompLines = $scope.InCompLines + ", Case Reason";

                                k = 1;
                            }
                            else if (!dataSource[i].IssueDescription && l === 0) {
                                // $scope.InCompLines = $scope.InCompLines + '  ' + dataSource[i].QuoteLineNumber;
                                if ($scope.InCompLines == "Please select the ")
                                    $scope.InCompLines = $scope.InCompLines + "Issue Description";
                                else
                                    $scope.InCompLines = $scope.InCompLines + ", Issue Description";

                                l = 1;
                            }
                            else if ((!dataSource[i].Status || dataSource[i].Status < 1) && m === 0) {
                                // $scope.InCompLines = $scope.InCompLines + '  ' + dataSource[i].QuoteLineNumber;
                                if ($scope.InCompLines == "Please select the ")
                                    $scope.InCompLines = $scope.InCompLines + "Status";
                                else
                                    $scope.InCompLines = $scope.InCompLines + ", Status";

                                m = 1;
                            }
                            //else if (!dataSource[i].Resolution || dataSource[i].Resolution < 1)
                            //    $scope.InCompLines = $scope.InCompLines + '  ' + dataSource[i].QuoteLineNumber;
                        }

                        if ($scope.InCompLines != "Please select the ")
                        {
                            $scope.InCompLines = $scope.InCompLines.replace(/,\s*$/, " and ");
                            HFC.DisplayAlert($scope.InCompLines);
                            return;
                        }


                        for (i = 0; i < dataSource.length; i++) {
                            //dataSource[i].QuoteLineId2 = dataSource[i].QuoteLineId;
                            if (typeof (dataSource[i].QuoteLineId) == 'object') {
                                var localdata = dataSource[i].QuoteLineId;
                                dataSource[i].QuoteLineId = dataSource[i].QuoteLineId.QuoteLineId;
                                dataSource[i].QuoteLineNumber = localdata.QuoteLineNumber;
                            }
                            else dataSource[i].QuoteLineId = dataSource[i].QuoteLineId;

                        }
                        dto.AdditionalInfo = dataSource;
                    }
                    else {
                        var grid = $("#caseGrid01").data("kendoGrid");
                        var dataSource = grid.dataSource._data;
                        $scope.InCompLines = "Please select the ";


                        //var dataStatus_val = $("#Status_val").data("kendoDropDownList");
                        //valuee = dataStatus_val.value();
                        if ($("#LineNumberDropdown").data("kendoDropDownList").value() === "") {
                            if ($scope.InCompLines == "Please select the ")
                                $scope.InCompLines = $scope.InCompLines + "Line No";
                            else
                                $scope.InCompLines = $scope.InCompLines + ", Line No";
                        }
                        if ($("#Type_Val").data("kendoDropDownList").value() === "") {
                            if ($scope.InCompLines == "Please select the ")
                                $scope.InCompLines = $scope.InCompLines + "Issue Type";
                            else
                                $scope.InCompLines = $scope.InCompLines + ", Issue Type";
                        }
                        if ($("#Case_Val").data("kendoDropDownList").value() === "") {
                            if ($scope.InCompLines == "Please select the ")
                                $scope.InCompLines = $scope.InCompLines + "Case Reason";
                            else
                                $scope.InCompLines = $scope.InCompLines + ", Case Reason";
                        }
                        if ($("#IssueDescription").val() === "" || $("#IssueDescription").val() === undefined) {
                            if ($scope.InCompLines == "Please select the ")
                                $scope.InCompLines = $scope.InCompLines + "Description";
                            else
                                $scope.InCompLines = $scope.InCompLines + ", Description";
                        }
                        if ($("#Status_val").data("kendoDropDownList").value() === "")
                        {
                            if ($scope.InCompLines == "Please select the ")
                                $scope.InCompLines = $scope.InCompLines + "Status";
                            else
                                $scope.InCompLines = $scope.InCompLines + ", Status";
                        }

                        if ($scope.InCompLines != "Please select the ") {                         

                            HFC.DisplayAlert($scope.InCompLines);
                            return;
                        }

                        return;
                    }

             //   }
                dto.CaseNumber = $scope.CaseNum;
                $scope.CaseAddEdit.PurchaseOrdersDetailId;

                $scope.CaseAddEdit.PurchaseOrderId = dto.MPO_MasterPONum_POId;

               var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "block";
                $http.post('/api/CaseManageAddEdit/0/SaveFranchiseCase', dto).then(function (response) {
                    
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                    var Id = response.data;
                    var arrId = Id.split('|');
                    $scope.formCaseManage.$setPristine();
                    window.location.href = '/#!/CaseView/' + arrId[1];
                    ////////

                    //if (arrId.length != null || arrId.length != "" || arrIdId.length != undefined) {

                    //    $scope.SecCall = false;
                    //    if (!$scope.CaseId) {
                    //        window.location.href = '/#!/Case/' + arrId[1];
                    //        $scope.SecCall = true;
                    //        $scope.CaseNum = arrId[0];
                    //        $scope.FileUploadService.getlineIdList();
                    //    }
                    //    if ($scope.SecCall == false) {
                    //        window.location.href = '/#!/CaseView/' + arrId[1];
                    //    }
                    //}
                    //else {
                    //    HFC.DisplayAlert(response.data);
                    //    return;
                    //}

                    /////

                    //if (arrId.length != null || arrId.length != "" || arrIdId.length != undefined) {

                    //    $scope.SecCall = false;
                    //    if (!$scope.CaseId) {
                    //        window.location.href = '/#!/Case/' + arrId[1];
                    //        $scope.SecCall = true;
                    //        $scope.CaseNum = arrId[0];
                    //        $scope.FileUploadService.getlineIdList();
                    //    }
                    //    if ($scope.SecCall == false) {
                    //        window.location.href = '/#!/CaseView/' + arrId[1];
                    //    }
                    //}
                    //else {
                    //    HFC.DisplayAlert(response.data);
                    //    return;
                    //}

                });
            }

            function replaceAt(index, replacement) {
                return this.substr(0, index) + replacement + this.substr(index + replacement.length);
            }
            $scope.selectedRow = '';
            //edit row in grid
            $scope.EditRowDetails = function () {
                $scope.clonee = false;
                if ($('#dropforGrid').hasClass("openDrop")) {
                    $('#dropforGrid').removeClass("openDrop")
                    $('#dropforGrid').css({
                        'display': 'none'
                    });
                }
               var obj = $scope.dataitemToedit;
                var grid = $("#caseGrid01").data("kendoGrid");
                var validator = $scope.kendoValidator("caseGrid01");

                var rowInEditMode = $("#caseGrid01").find("tr.k-grid-edit-row");
                var itemBeingEdited = $("#caseGrid01").data("kendoGrid").dataSource.getByUid(rowInEditMode.data("uid"));

              

                if (validator.validate()) {
                    if (itemBeingEdited) {

                        if ($scope.selectedRow >= 0) {
                            if ($scope.ProductNamem)
                                grid.dataSource._data[$scope.selectedRow].ProductName = $scope.ProductNamem;
                            if ($scope.ProductIdm)
                                grid.dataSource._data[$scope.selectedRow].ProductId = $scope.ProductIdm;
                            if ($scope.ProductDescriptionm)
                                grid.dataSource._data[$scope.selectedRow].Description = $scope.ProductDescriptionm;
                            if ($scope.VendorNamem)
                                grid.dataSource._data[$scope.selectedRow].VendorName = $scope.VendorNamem;
                            if ($scope.VendorReferencem)
                                grid.dataSource._data[$scope.selectedRow].VendorReference = $scope.VendorReferencem;
                            if ($scope.vpom)
                                grid.dataSource._data[$scope.selectedRow].VPO = $scope.vpom;
                            if ($scope.LineNumberQuote)
                                grid.dataSource._data[$scope.selectedRow].QuoteLineNumber = $scope.LineNumberQuote;
                            if ($scope.TypeValDesc)
                                grid.dataSource._data[$scope.selectedRow].Typevalue = $scope.TypeValDesc;
                            if ($scope.ReasonvalDesc)
                                grid.dataSource._data[$scope.selectedRow].CaseReason = $scope.ReasonvalDesc;
                            if ($scope.StatusvalDesc)
                                grid.dataSource._data[$scope.selectedRow].StatusName = $scope.StatusvalDesc;
                            if ($scope.ResolutionvalDesc)
                                grid.dataSource._data[$scope.selectedRow].ResolutionName = $scope.ResolutionvalDesc;
                        }

                    }

                  // get the selected row
                    for (var i = 0; i < grid.dataSource._data.length ; i++) {
                        if (grid.dataSource._data[i].uid == obj.uid) {
                            $scope.selectedRow = i;
                        }
                    }

                    var grids = $("#caseGrid01").data("kendoGrid");
                    grid.refresh();
                    grids.editRow(obj);
                    validator.validate();
                    $scope.formCaseManage.$setDirty();

                    $scope.Roweditvalidation(obj);

                } else {

                    return false;
                }
            };

            $scope.Roweditvalidation = function (obj) {
               
              
                if (obj.QuoteLineNumber) {
                    var a = $("#LineNumberDropdown").parent();
                    $(a).removeClass("dropdown-validation-error");
                }


                if (obj.Typevalue) {
                    var a = $("#Type_Val").parent();
                    $(a).removeClass("dropdown-validation-error");
                }


                //if (obj.QuoteLineNumber) {
                //    var a = $("#LineNumberDropdown").parent();
                //    $(a).removeClass("dropdown-validation-error");
                //}


                if (obj.CaseReason) {
                    var a = $("#Case_Val").parent();
                    $(a).removeClass("dropdown-validation-error");
                }


                if (obj.StatusName) {
                    var a = $("#Status_val").parent();
                    $(a).removeClass("dropdown-validation-error");
                }

                //if (obj.ResolutionName) {
                //    var a = $("#Resolution_val").parent();
                //    $(a).removeClass("dropdown-validation-error");
                //}
             


            }

            $scope.resetvalues = function()
            {
                $scope.LineNumberQuote = '';
                $scope.vpom = '';
                $scope.VendorNamem = '';
                $scope.VendorReferencem = '';
                $scope.ProductDescriptionm = '';
                $scope.ProductIdm = '';
                $scope.ProductNamem = '';

                $scope.TypeValDesc = '';
                $scope.ReasonvalDesc = '';
                $scope.StatusvalDesc = '';
                $scope.ResolutionvalDesc = '';
            }
            $scope.resetvalues();



            //function value change on quote line select in grid.
            function onChangeValue(value, valuee) {

                
                 
                var grid = $("#caseGrid01").data("kendoGrid");
                var dataSource = grid.dataSource._data;
               
                var data = value;
                var quoteline = data;
                //if ($scope.RouteLineNo > 0)
                //    quoteline = $scope.QuoteLineIdFromRouteLineNo;

                if (!quoteline)
                    quoteline = data

                if (quoteline > 0 && quoteline) {
                    $http.get('/api/CaseManageAddEdit/' + quoteline + '/GetFullDetails').then(function (data) {

                        var data = data;

                        var Prdtname = $('#product_txt');
                        if (Prdtname) {
                            Prdtname.val(data.data.ProductName);
                            $scope.ProductNamem = data.data.ProductName;
                        } else $scope.ProductNamem = '';

                        var Prdtnumbr = $('#productnumber_txt');
                        if (Prdtnumbr) {
                            Prdtnumbr.val(data.data.ProductId);
                            $scope.ProductIdm = data.data.ProductId;
                        } else $scope.ProductIdm = '';

                        var PrdDesc = $('#productdescri_txt');
                        if (PrdDesc) {
                            //var erer = data.data.Description.replace('"', "");
                            //PrdDesc.val('<p title="' + erer + '">' + data.data.Description + '</p>');
                            //$scope.ProductDescriptionm = '<p title="' + erer + '">' + data.data.Description + '</p>';

                            $scope.ProductDescriptionm = data.data.Description;
                        } else $scope.ProductDescriptionm = '';

                        var gridRow = $(".k-grid-edit-row")[0];
                        if (gridRow) {
                            var descriptionColumn = $(gridRow).children()[5];
                            var erer = $scope.ProductDescriptionm.replace('"', "");
                            erer = erer.replace(/<[^>]*>/g, '');
                            var descTemplate = "<span title='" + erer + "'>" + $scope.ProductDescriptionm + "</span>";
                            if (descriptionColumn)
                            $(descriptionColumn).html(descTemplate);
                        }

                        var Vendrnme = $('#Vendor_txt');
                        if (Vendrnme) {
                            Vendrnme.val(data.data.VendorName);
                            $scope.VendorNamem = data.data.VendorName;
                        } else $scope.VendorNamem = '';

                        var Vendrefnme = $('#VendorRef_txt');
                        if (Vendrefnme) {
                            Vendrefnme.val(data.data.VendorReference);
                            $scope.VendorReferencem = data.data.VendorReference;
                        } else $scope.VendorReferencem = '';

                        var Vpovalu = $('#VPO_txt');
                        if (Vpovalu) {
                            Vpovalu.val(data.data.VPO);
                            $scope.vpom = data.data.VPO;
                        } else $scope.vpom = '';



                        var dataStatus_val = $("#Status_val").data("kendoDropDownList");
                        valuee = dataStatus_val.value();
                        dataStatus_val.select(1);

                        //for (var i = 0; i < dataStatus_val.dataSource._data.length; i++)
                        //    if (dataStatus_val.dataSource._data[i].Status == 7000) {
                        $scope.StatusvalDesc = "New";
                        $scope.Status = 7000;
                        //  }

                        var grid = $("#caseGrid01").data("kendoGrid");
                        var dataSource = grid.dataSource;
                        dataSource._data[0].Status = 7000;
                        dataSource._data[0].VendorReference = data.data.VendorReference;
                        
                        dataSource._data[0].ProductName = $scope.ProductNamem;
                        dataSource._data[0].ProductId = $scope.ProductIdm;
                        dataSource._data[0].VPO = $scope.vpom;
                        dataSource._data[0].Description = $scope.ProductDescriptionm;
                        dataSource._data[0].VendorName = $scope.VendorNamem;
                        dataSource._data[0].VendorReference = $scope.VendorReferencem;

                        var data = $("#LineNumberDropdown").data("kendoDropDownList");
                        //if ($scope.RouteLineNo > 0) {  // $scope.RouteMPO > 0 || 
                        //    if ($scope.QuoteLineIdFromRouteLineNo > 0) {
                        //        value = $scope.QuoteLineIdFromRouteLineNo;
                        //        dataSource._data[0].QuoteLineId = { QuoteLineId: $scope.QuoteLineIdFromRouteLineNo, QuoteLineNumber: $scope.RouteLineNo }
                        //    }
                        //    else {
                        //        dataSource._data[0].QuoteLineId = { QuoteLineId: value, QuoteLineNumber: $scope.QuoteLineNumberForVpoChange }
                        //        value = data.value();

                        //    }
                        //    data.select(1);
                        //}


                        for (var i = 0; i < data.dataSource._data.length; i++)
                            if (data.dataSource._data[i].QuoteLineId == value) $scope.LineNumberQuote = data.dataSource._data[i].QuoteLineNumber;

                        // $scope.LineNumberQuote = valuee.QuoteLineNumber;

                    });
                }
                
            }

            function onTypeChangeValue(value, data) {
                
                var data = $("#Type_Val").data("kendoDropDownList");
                value = data.value();

                for(var i = 0; i< data.dataSource._data.length; i++)
                    if (data.dataSource._data[i].Type == value) $scope.TypeValDesc = data.dataSource._data[i].Typevalue;

            }

            function onCaseChangeValue(value) {
                
                var data = $("#Case_Val").data("kendoDropDownList");
                value = data.value();

                for (var i = 0; i < data.dataSource._data.length; i++)
                    if (data.dataSource._data[i].ReasonCode == value) $scope.ReasonvalDesc = data.dataSource._data[i].Reasonvalue;
            }

            function onStatusChangeValue(value) {
                 
                var data = $("#Status_val").data("kendoDropDownList");
                value = data.value();

                for (var i = 0; i < data.dataSource._data.length; i++)
                    if (data.dataSource._data[i].Status == value)
                        $scope.StatusvalDesc = data.dataSource._data[i].Statusvalue;
            }

            function onResolutionChangeValue(value) {
                
                var data = $("#Resolution_val").data("kendoDropDownList");
                value = data.value();

                for (var i = 0; i < data.dataSource._data.length; i++)
                    if (data.dataSource._data[i].Resolution == value) $scope.ResolutionvalDesc = data.dataSource._data[i].Resolutionvalue;
            }

            // copy/clone the data
            $scope.CloneWithSameold = function () {
                if ($('#dropforGrid').hasClass("openDrop")) {
                    $('#dropforGrid').removeClass("openDrop")
                    $('#dropforGrid').css({
                        'display': 'none'
                    });
                }
                var data = $scope.dataitemToedit;
                var grid = $("#caseGrid01").data("kendoGrid");
                var dataSource = grid.dataSource;
                var validator = $scope.kendoValidator("caseGrid01");

                if ($scope.selectedRow >= 0) {
                    if ($scope.ProductNamem)
                        grid.dataSource._data[$scope.selectedRow].ProductName = $scope.ProductNamem;
                    if ($scope.ProductIdm)
                        grid.dataSource._data[$scope.selectedRow].ProductId = $scope.ProductIdm;
                    if ($scope.ProductDescriptionm)
                        grid.dataSource._data[$scope.selectedRow].Description = $scope.ProductDescriptionm;
                    if ($scope.VendorNamem)
                        grid.dataSource._data[$scope.selectedRow].VendorName = $scope.VendorNamem;
                    if ($scope.VendorReferencem)
                        grid.dataSource._data[$scope.selectedRow].VendorReference = $scope.VendorReferencem;
                    if ($scope.vpom)
                        grid.dataSource._data[$scope.selectedRow].VPO = $scope.vpom;
                    if ($scope.LineNumberQuote)
                        grid.dataSource._data[$scope.selectedRow].QuoteLineNumber = $scope.LineNumberQuote;
                    if ($scope.TypeValDesc)
                        grid.dataSource._data[$scope.selectedRow].Typevalue = $scope.TypeValDesc;
                    if ($scope.ReasonvalDesc)
                        grid.dataSource._data[$scope.selectedRow].CaseReason = $scope.ReasonvalDesc;
                    if ($scope.StatusvalDesc)
                        grid.dataSource._data[$scope.selectedRow].StatusName = $scope.StatusvalDesc;
                    if ($scope.ResolutionvalDesc)
                        grid.dataSource._data[$scope.selectedRow].ResolutionName = $scope.ResolutionvalDesc;
                }
           
               
                
                var validator = $scope.kendoValidator("caseGrid01");
                if (validator.validate()) {
                    var newItem = {                 
                    CaseReason: data.CaseReason,
                    Description: data.Description,
                    ExpediteApproved:data.ExpediteApproved,
                    ExpediteReq: data.ExpediteReq,
                    Id: 0,
                    IssueDescription:data.IssueDescription ,
                    MPO:data.MPO,
                    ProductId: data.ProductId,
                    ProductName: data.ProductName,
                    PurchaseOrderId: data.PurchaseOrderId,
                    QuoteLineId: 0,
                    QuoteLineNumber: 0,
                    ReasonCode: data.ReasonCode,
                    ReasonCodeValue: data.ReasonCodeValue,
                    ReqTripCharge: data.ReqTripCharge,
                    Resolution: data.Resolution,
                    ResolutionName: data.ResolutionName,
                    Status: data.Status,
                    StatusName: data.StatusName,
                    TripChargeApproved: data.TripChargeApproved,
                    TripChgApp: data.TripChgApp,
                    Type: data.Type,
                    Typevalue: data.Typevalue,
                    VPO: data.VPO,
                    VendorCaseNumber: data.VendorCaseNumber,
                    VendorId: data.VendorId,
                    VendorName: data.VendorName
                    }
                    var newItem = dataSource.insert(0, newItem);
                    var datas = dataSource._data;

                    newItem.WindowLocation = "";
                    var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                    var datas = dataSource._data;
                    $scope.selectedRow = 0;
                    $scope.clonee = true;
                    grid.editRow(newRow);
                    $scope.formCaseManage.$setDirty();
                    
                }               
            }
            $scope.clonee = false;
            //Delete row 
            $scope.DeleteRowDetails = function () {
                $scope.clonee = false;
                if ($('#dropforGrid').hasClass("openDrop")) {
                    $('#dropforGrid').removeClass("openDrop")
                    $('#dropforGrid').css({
                        'display': 'none'
                    });
                }
                var value = $scope.dataitemToedit;
                //
                var grid = $("#caseGrid01").data("kendoGrid");
                var rowInEditMode = $("#caseGrid01").find("tr.k-grid-edit-row");
                var itemBeingEdited = $("#caseGrid01").data("kendoGrid").dataSource.getByUid(rowInEditMode.data("uid"));

                if (itemBeingEdited) {
                    
                    if (itemBeingEdited.uid === value.uid)
                    {
                        var grids = $("#caseGrid01").data("kendoGrid");
                        grid.refresh();

                        var dataRow = $('#caseGrid01').data("kendoGrid").dataSource.getByUid(value.uid);
                        $('#caseGrid01').data("kendoGrid").dataSource.remove(dataRow);
                        $scope.formCaseManage.$setDirty();
                        return;
                    }

                    var validator = $scope.kendoValidator("caseGrid01");
                    if (validator.validate()) {

                        if ($scope.selectedRow >= 0) {
                            if ($scope.ProductNamem)
                                grid.dataSource._data[$scope.selectedRow].ProductName = $scope.ProductNamem;
                            if ($scope.ProductIdm)
                                grid.dataSource._data[$scope.selectedRow].ProductId = $scope.ProductIdm;
                            if ($scope.ProductDescriptionm)
                                grid.dataSource._data[$scope.selectedRow].Description = $scope.ProductDescriptionm;
                            if ($scope.VendorNamem)
                                grid.dataSource._data[$scope.selectedRow].VendorName = $scope.VendorNamem;
                            if ($scope.VendorReferencem)
                                grid.dataSource._data[$scope.selectedRow].VendorReference = $scope.VendorReferencem;
                            if ($scope.vpom)
                                grid.dataSource._data[$scope.selectedRow].VPO = $scope.vpom;
                            if ($scope.LineNumberQuote)
                                grid.dataSource._data[$scope.selectedRow].QuoteLineNumber = $scope.LineNumberQuote;
                            if ($scope.TypeValDesc)
                                grid.dataSource._data[$scope.selectedRow].Typevalue = $scope.TypeValDesc;
                            if ($scope.ReasonvalDesc)
                                grid.dataSource._data[$scope.selectedRow].CaseReason = $scope.ReasonvalDesc;
                            if ($scope.StatusvalDesc)
                                grid.dataSource._data[$scope.selectedRow].StatusName = $scope.StatusvalDesc;
                            if ($scope.ResolutionvalDesc)
                                grid.dataSource._data[$scope.selectedRow].ResolutionName = $scope.ResolutionvalDesc;
                        }

                        var grids = $("#caseGrid01").data("kendoGrid");
                        grid.refresh();

                        var dataRow = $('#caseGrid01').data("kendoGrid").dataSource.getByUid(value.uid);
                        $('#caseGrid01').data("kendoGrid").dataSource.remove(dataRow);
                        $scope.formCaseManage.$setDirty();

                    }
                    return;
                }

                // delete row for readonly grid
                        var grids = $("#caseGrid01").data("kendoGrid");
                        grid.refresh();

                        var dataRow = $('#caseGrid01').data("kendoGrid").dataSource.getByUid(value.uid);
                        $('#caseGrid01').data("kendoGrid").dataSource.remove(dataRow);
                        $scope.formCaseManage.$setDirty();
              
            }

            function GetDetail_Data(Id) {
                if (Id) {
                    $http.get('/api/CaseManageAddEdit/' + Id + '/GetSavedValue').then(function (response) {
                        var data = response.data;
                        if (response.data.CaseId != 0) {
                            $scope.CaseAddEdit = response.data;
                            
                            $scope.CaseAddEdit.PurchaseOrdersDetailId = $scope.CaseAddEdit.VPO_PICPO_PODId;
                            $scope.CaseAddEdit.PurchaseOrderId = $scope.CaseAddEdit.MPO_MasterPONum_POId;
                            $scope.CaseAddEdit.OrderId = $scope.CaseAddEdit.SalesOrderId;
                            $scope.CaseAddEdit.Email = $scope.CaseAddEdit.PrimaryEmail;

                            $scope.PreviousValues = response.data;

                        }

                    })
                }

            }

            //has case id get saved data
            if ($scope.CaseId > 0) {
                
                var Id = $scope.CaseId;

                $http.get('/api/CaseManageAddEdit/' + Id + '/GetSavedValue').then(function (response) {
                    var data = response.data;
                    if (response.data.CaseId != 0) {
                        $scope.CaseAddEdit = response.data;
                        $scope.noOfLines = response.data.noOfLines;
                        
                        $scope.CaseAddEdit.PurchaseOrdersDetailId = $scope.CaseAddEdit.VPO_PICPO_PODId;
                        $scope.CaseAddEdit.PurchaseOrderId = $scope.CaseAddEdit.MPO_MasterPONum_POId;
                        $scope.CaseAddEdit.OrderId = $scope.CaseAddEdit.SalesOrderId;
                        $scope.CaseAddEdit.Email = $scope.CaseAddEdit.PrimaryEmail;
                        $scope.CaseAddEdit.CaseCreatedDate = $scope.CaseAddEdit.CreatedOn;
                        $scope.CaseAddEdit.DateTimeStart = $scope.CaseAddEdit.IncidentDate;
                        $scope.PreviousValues = response.data;
                        //$scope.SecCall = true;
                        var grid = $("#caseGrid01").data("kendoGrid");
                        var dataSource = grid.dataSource;

                        //var savedData = dataSource.data.length;
                        var savedData = dataSource._data.length;

                        if (savedData != null && savedData != 0) {
                            $scope.ShowEllipse = true;
                        }
                        else $scope.ShowEllipse = false;


                    }

                })


            }

        }

    ])




