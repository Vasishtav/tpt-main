﻿'use strict';
appCP.controller('VendorCaseController',
    [
        '$scope','HFCService',
        function ($scope, HFCService) {
            $scope.Permission = {};
            var Casepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'CaseManagement')
            if (Casepermission){
            $scope.Permission.ViewCase = Casepermission.CanRead;
            //$scope.Permission.AddCase = Casepermission.CanCreate;
            $scope.Permission.EditCase = Casepermission.CanUpdate;
            }
            //Kendo grid
            
            HFCService.setHeaderTitle("Vendor Management #");
                
            $scope.VendorCase = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    
                                    var url1 = '/api/AdminVendor/0/GetVendorCaseList';
                                    return url1;
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (params) {
                                    
                                    var url1 ='/api/AdminVendor/0/UpdateVendorCase';
                                    return url1;
                                },
                               
                                type: "POST",
                                complete: function (e) {
                                    $("#gridVendorCaseConfig").data("kendoGrid").dataSource.read();
                                    $("#gridVendorCaseConfig").data("kendoGrid").refresh();
                                   
                                }
                            },
                           
                            dataType: "json",
                            parameterMap: function (options, operation) {
                                if (operation === "create") {

                                    return {
                                        Id: options.Id,
                                        VendorId: options.VendorId,
                                        VendorName: options.VendorName,
                                        CaseEnabled: options.CaseEnabled,
                                        CaseLogin: options.CaseLogin,
                                        VendorSupportEmail: options.VendorSupportEmail,
                                       
                                        
                                    };
                                }
                                else if (operation === "update") {
                                    
                                    return {
                                        
                                        Id: options.Id,
                                        VendorId: options.VendorId,
                                        VendorName:options.VendorName,
                                        CaseEnabled: options.CaseEnabled,
                                        CaseLogin: options.CaseLogin,
                                        VendorSupportEmail: options.VendorSupportEmail,
                                        

                                    };
                                }
                            }


                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {
                                id: "Id",
                                fields: {
                                    Id: { editable: false },
                                    VendorId: { type: "number", editable: false },
                                    VendorName: { type: "string", editable: false, validation: { required: true } },
                                    CaseEnabled: { type: "boolean", editable: true, },
                                    CaseLogin: { type: "string", editable: true, validation: { required: {message:"Case Login is required"} } },
                                    VendorSupportEmail: {
                                        editable: true, validation: {
                                            email: true, required: { message: "Vendor Support Email is required" }, email: {
                                                message: "Please enter a valid email address!"
                                            }
                                        }
                                    },
                                 
                                }
                            }
                        }
                    },
                    dataBound: function (e) {
                        if ($scope.Permission.EditCase == false) {
                            e.sender.element.find(".k-grid-edit").hide();
                            e.sender.element.find(".k-grid-add").hide();
                        }
                    },
                    
                    columns: [
                        {


                            field: "Id",
                            title: "Id",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: true,


                        },
                        {


                            field: "VendorId",
                            title: "Vendor ID",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.VendorId + "</span>";
                            },
                        },
                        {


                            field: "VendorName",
                            title: "Vendor Name",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,


                        },
                        {
                            field: "CaseEnabled",
                            title: "Vendor Enabled",
                            hidden: false,
                            template: '<input type="checkbox" #= CaseEnabled ? "checked=checked" : "" # class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched" disabled="true"></input>',
                            filterable: { messages: { isFalse: "False", isTrue: "True" } },
                        },
                  {


                      field: "CaseLogin",
                      title: "Vendor Login",
                      filterable: {
                          //multi: true,
                          search: true
                      },
                      hidden: false,


                  },
                  {


                      field: "VendorSupportEmail",
                      title: "Vendor Support Email",
                      filterable: {
                          //multi: true,
                          search: true
                      },
                      hidden: false,


                  },
                        {
                            command: ["edit"], title: "&nbsp;", width: "100px"
                        }
                                  

                    ],

                    editable: {
                        mode: "inline",
                        //createAt: "bottom"
                    },

                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],
                    },
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                isempty: "Empty",
                                isnotempty: "Not empty"


                            },
                            number: {
                                eq: "Equal to",
                                neq: "Not equal to",
                                gte: "Greater than or equal to",
                                lte: "Less than or equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    toolbar: [
                       {
                           template: kendo.template($("#headToolbarTemplate").html()),
                       }]
                    
            };



            $scope.vendorCaseGridSearch = function () {
                
                
                    var searchValue = $('#searchBox').val();

                    $("#gridVendorCaseConfig").data("kendoGrid").dataSource.filter({
                        logic: "or",
                        filters: [
                          {
                              field: "VendorId",
                              operator: "eq",
                              value: searchValue
                          },
                          {
                              field: "VendorName",
                              operator: "contains",
                              value: searchValue
                          },
                          {
                              field: "CaseLogin",
                              operator: "contains",
                              value: searchValue
                          },
                          {
                              field: "VendorSupportEmail",
                              operator: "contains",
                              value: searchValue
                          }

                        ]
                    });
               

            }

            //search clear button
            $scope.vendorCaseGridSearchClear = function () {
                $('#searchBox').val('');
                var searchValue = $('#searchBox').val('');
               
                $("#gridVendorCaseConfig").data('kendoGrid').dataSource.filter({
                });
            }
        }
    ])