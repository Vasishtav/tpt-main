﻿'use strict';

app
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('ShippingNoticesController',
        [
            '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
            , '$templateCache', '$sce', '$compile', '$http', '$q', 'PrintService', 'NavbarService',
            function ($scope, $window, HFCService, $routeParams, $location, $rootScope
                , $templateCache, $sce, $compile, $http, $q, PrintService, NavbarService) {
                //for selecting shipment tab
                $scope.NavbarService = NavbarService;
                $scope.NavbarService.SelectOperation();
                $scope.NavbarService.EnableOperartionsShippingNoticeDashboardTab();
                var ctrl = this;
                $scope.Check = true;
                $scope.ShipNoticeId = $routeParams.ShipNoticeId;
                $scope.EditMode = false;
                $scope.PrintService = PrintService;
                $scope.shipStatus = {};
                $scope.ShippingNotice = {
                    ShipNoticeId: '',
                    ShipmentId: '',
                    ShippedDate: '',
                    EstDeliveryDate: '',
                    ShippedVia: '',
                    BOL: '',
                    Weight: '',
                    TotalBoxes: '',
                    PurchaseOrderId: '',
                    VendorName: '',
                    ReceivedComplete: '',
                    ReceivedCompleteDate: '',
                    CheckedinDate: '',
                    RemakeReference: '',
                    TotalQuantityShipped: 0,
                    TotalQuantityMisShipped: 0,
                    TotalQuantityDamaged: 0,
                    CaseID: 0,
                    TotalQuantityShortShipped: 0,
                    TotalQuantityReceived: 0,
                    Status: '',
                    TotalBoxesTP: '',
                    ShiptoAccount: '',
                    MasterPONumber: '',
                };

                $scope.Permission = {};
                var ShipNoticePermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Shipments')

                $scope.Permission.ListShipNotice = ShipNoticePermission.CanRead;
                $scope.Permission.SubmitShipNotice = ShipNoticePermission.CanCreate;
                $scope.Permission.ViewShipNotice = ShipNoticePermission.CanRead;
                $scope.Permission.ShipNoticeEdit = ShipNoticePermission.CanUpdate;

                var checkedIds = {};

                HFCService.setHeaderTitle("Ship Notice #" + $routeParams.ShipNoticeId);
                $scope.MPORelatedIds = {
                    VendorAckIds: [],
                    ShipmentIds: [],
                    VendorInvoiceIds: [],
                    PurchaseOrderId: '',
                };
                $scope.GetMPORelatedIds = function () {
                    $http.get('/api/PurchaseOrders/' + $scope.ShippingNotice.PurchaseOrderId + '/GetMPORelatedIds').then(function (data) {
                        var MPORelatedIds = data.data.data;
                        if (MPORelatedIds.length > 0) {
                            for (var i = 0; i < MPORelatedIds.length; i++) {
                                if (MPORelatedIds[i].vendorReference) {
                                    if (jQuery.isEmptyObject($scope.MPORelatedIds.VendorAckIds))
                                        $scope.MPORelatedIds.VendorAckIds.push({ vendorReference: MPORelatedIds[i].vendorReference, PurchaseOrderId: MPORelatedIds[i].PurchaseOrderId });
                                    else if ($scope.MPORelatedIds.VendorAckIds.length > 0) {
                                        var exists = $scope.MPORelatedIds.VendorAckIds.some(el => el.vendorReference === MPORelatedIds[i].vendorReference);
                                        if (!exists)
                                            $scope.MPORelatedIds.VendorAckIds.push({ vendorReference: MPORelatedIds[i].vendorReference, PurchaseOrderId: MPORelatedIds[i].PurchaseOrderId });
                                    }
                                }
                                if (MPORelatedIds[i].ShipmentId) {
                                    if (jQuery.isEmptyObject($scope.MPORelatedIds.ShipmentIds))
                                        $scope.MPORelatedIds.ShipmentIds.push({ ShipmentId: MPORelatedIds[i].ShipmentId, ShipNoticeId: MPORelatedIds[i].ShipNoticeId });
                                    else if ($scope.MPORelatedIds.ShipmentIds.length > 0) {
                                        var exists = $scope.MPORelatedIds.ShipmentIds.some(el => el.ShipmentId === MPORelatedIds[i].ShipmentId);
                                        if (!exists)
                                            $scope.MPORelatedIds.ShipmentIds.push({ ShipmentId: MPORelatedIds[i].ShipmentId, ShipNoticeId: MPORelatedIds[i].ShipNoticeId });
                                    }
                                }

                                if (MPORelatedIds[i].VendorInvoiceId) {
                                    if (jQuery.isEmptyObject($scope.MPORelatedIds.VendorInvoiceIds))
                                        $scope.MPORelatedIds.VendorInvoiceIds.push({ VendorInvoiceId: MPORelatedIds[i].VendorInvoiceId, InvoiceNumber: MPORelatedIds[i].InvoiceNumber });
                                    else if ($scope.MPORelatedIds.VendorInvoiceIds.length > 0) {
                                        var exists = $scope.MPORelatedIds.VendorInvoiceIds.some(el => el.InvoiceNumber === MPORelatedIds[i].InvoiceNumber);
                                        if (!exists)
                                            $scope.MPORelatedIds.VendorInvoiceIds.push({ VendorInvoiceId: MPORelatedIds[i].VendorInvoiceId, InvoiceNumber: MPORelatedIds[i].InvoiceNumber });
                                    }
                                }
                                //$scope.mporelatedids.vendorackids = MPORelatedIds.find(mpo => mpo.vendorreference);
                            }
                            setTimeout(function () {
                                if ($scope.MPORelatedIds.ShipmentIds) {
                                    $("#submenu-dropdown li").find('a:contains(' + $scope.ShippingNotice.ShipmentId + ')').addClass('activeLi');
                                }
                            }, 100);
                        }
                    });
                }

                //to get value from api call
                $scope.GetShipNotice = function () {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "block";
                    $http.get('/api/ShipNotice/' + $scope.ShipNoticeId + '/GetShipNotice').then(function (data) {
                        $scope.ShippingNotice = data.data;                        
                        $scope.GetMPORelatedIds();
                        if ($scope.ShippingNotice != null && $('#ShippingNotesGrid').data('kendoGrid') != null) {
                            $('#ShippingNotesGrid').data('kendoGrid').dataSource.read($scope.ShippingNotice.ListShipmentDetails);
                        }
                        else {
                            $scope.GetShipNotesValue(); // initial call                            
                        }
                        loadingElement.style.display = "none";
                    }, function (error) {
                        HFC.DisplayAlert(error);
                        loadingElement.style.display = "none";
                    });
                }

                $http.get('/api/ShipNotice/0/GetShipStatus').then(function (data) {
                    $scope.shipStatus = data.data;
                    if ($scope.shipStatus != null) {
                        for (var i = 0; i < $scope.shipStatus.length; i++) {
                            $scope.shipStatus[i].StatusId = $scope.shipStatus[i].ShipmentStatusId;
                            $scope.shipStatus[i].ShipmentDetailId = 0;
                        }
                    }
                });

                $http.get('/api/ShipNotice/' + $scope.ShipNoticeId + '/GetRelatedShipNoticeIds').then(function (data) {
                    $scope.PORelatedShipNoticeIds = data.data;
                });

                if ($scope.ShipNoticeId) {
                    $scope.GetShipNotice();
                }


                //grid code
                $scope.GetShipNotesValue = function () {
                    $scope.GridEditable = false;
                    $scope.ShipLinesOptions = {
                        dataSource: {
                            transport: {
                                read: function (e) {
                                    var _unArray = [];
                                    //$scope.ShippingNotice.ListShipmentDetails.forEach(function (item) {
                                    //    var isPresent = _unArray.filter(function (elem) {
                                    //        return elem.ShipNoticeId === item.ShipNoticeId;
                                    //    })
                                    //    if (isPresent.length == 0) {
                                    //        _unArray.push(item)
                                    //    }
                                    //});
                                    e.success($scope.ShippingNotice.ListShipmentDetails);
                                }

                            },
                            error: function (e) {

                                HFC.DisplayAlert(e.errorThrown);
                            },
                            batch: true,
                            schema: {
                                model: {
                                    id: "ShipmentDetailId",
                                    fields: {
                                        ShipmentDetailId: { editable: false },
                                        LineNumber: { editable: false },
                                        POItem: { editable: false },
                                        ProductNumber: { editable: false },
                                        ItemDescription: { editable: false, },
                                        QuantityShipped: { editable: false },
                                        BoxId: { editable: false },
                                        PICVpo: { editable: false },
                                        PackingSlip: { editable: true },
                                        UomDefinition: { editable: false },
                                        SideMark: { editable: false },
                                        QuantityReceived: {
                                            type: 'number', editable: true,
                                            //validation: {
                                            //    required: true,
                                            //    QuantityReceivedvalidation: function (input) {
                                            //        if (input.is("[name='QuantityReceived']")) {
                                            //            var row = input.closest("tr");
                                            //            var grid = row.closest("[data-role=grid]").data("kendoGrid");
                                            //            var dataItem = grid.dataItem(row);
                                            //            //if (dataItem.Statuses.find(item => item.ShipmentStatusId === 4) && Number(input.val()) > 0) {
                                            //            //    input.attr("data-QuantityReceivedvalidation-msg", "Quantity Received should be 0 when Status is missing.");                                                            
                                            //            //    return false;
                                            //            //}
                                            //            if (Number(input.val()) > dataItem.QuantityShipped) {
                                            //                input.attr("data-QuantityReceivedvalidation-msg", "Quantity Received cannot be more than Quantity Shipped.");
                                            //                return false;
                                            //            }
                                            //        }

                                            //        return true;
                                            //    }
                                            //}
                                        },
                                        QuantityDamaged: {
                                            type: 'number', editable: true,
                                        },
                                        QuantityShortShipped: {
                                            type: 'number', editable: true,
                                        },
                                        QuantityMisShipped: {
                                            type: 'number', editable: true,
                                        },
                                        //TrackingId: { editable: false },
                                        Status: { editable: true, },
                                        Statuses: {},
                                        Notes: { editable: true }
                                    }
                                }
                            }
                        },

                        columns: [
                            {
                                title: '',
                                headerTemplate: "<input type='checkbox' id='header-chb' class='header-checkbox' style='margin-left:18px'><label for='header-chb'> Select All</label>",
                                template: function (dataItem) {
                                    return "<input type='checkbox' id='" + dataItem.ShipmentDetailId + "' class='row-checkbox' style='margin-left:20px'><label for='" + dataItem.ShipmentDetailId + "'></label>";
                                },                                
                                width: "4%"
                            },
                            {
                                field: "PackingSlip",
                                title: "Packing Slip",
                                filterable: { search: true },
                                template: function (dataItem) {
                                    var PackingSlip = "";
                                    if (dataItem.PackingSlip)
                                        PackingSlip = dataItem.PackingSlip;
                                    if ($scope.EditMode) {
                                        return "<input style='width:100%;' class='PackingSlip' ng-blur='PackingBlur(dataItem,$event)' name='PackingSlip' value='" + PackingSlip + "'/>";                                        
                                    }
                                    else
                                        return "<span>" + PackingSlip + "</span>";
                                },                               
                                width: "5%"
                            },
                            {
                                field: "POItem",
                                title: "PO Line #",
                                width: "5%"
                                //template: "#=  (StartDate == null)? '' : kendo.toString(kendo.parseDate(StartDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                            },
                            {
                                field: "ProductNumber",
                                title: "Item",
                                width: "5%"
                                //template: "#=  (StartDate == null)? '' : kendo.toString(kendo.parseDate(StartDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                            },
                            {
                                field: "ItemDescription",
                                title: "Item Description",
                                width: "12%",
                                template: "<div>#=ItemDescription#</div>",
                                filterable: { search: true },
                            },
                            {
                                field: "QuantityShipped",
                                title: "QTY Shipped",
                                filterable: { search: true },
                                width: "5%",
                                template: function (dataItem) {
                                    if (dataItem.QuantityShipped)
                                        return "<span class='Grid_Textalign'>" + dataItem.QuantityShipped + "</span>";
                                    else
                                        return "";
                                }
                            },
                            {
                                field: "UomDefinition",
                                title: "UoM",
                                filterable: { search: true },
                                width: "4%",
                            },
                            {
                                field: "BoxId",
                                title: "BOX ID",
                                filterable: { search: true },
                                width: "5%",
                            },
                            {
                                field: "SideMark",
                                title: "Sales Order-Sidemark",
                                //template: "";
                                filterable: { search: true },
                                width: "5%",
                                template: function (dataItem) {
                                    var sm = dataItem.OrderNumber + " - " + dataItem.SideMark;
                                    return "<span><a href='/#!/Orders/" + dataItem.OrderId + "' style='color: rgb(61,125,139);'>" + sm + "</a></span>";
                                }
                            },
                            {
                                field: "LineNumber",
                                title: "SO Line #",
                                filterable: { search: true },
                                width: "5%",
                                template: function (dataItem) {
                                    if (dataItem.LineNumber)
                                        return "<span class='Grid_Textalign'>" + dataItem.LineNumber + "</span>";
                                    else
                                        return "";
                                }
                            },
                            {
                                field: "QuantityReceived",
                                title: "QTY Recv'd",
                                width: "5%",
                                filterable: { search: true },
                                template: function (dataItem) {
                                    if ($scope.EditMode) {
                                        return "<input style='width:100%;' ng-blur='quantityBlur(dataItem,$event)' class='QuantityReceived' name='Qty' value='" + dataItem.QuantityReceived + "'/>";
                                    }
                                    else if (dataItem.QuantityReceived)
                                        return "<span class='Grid_Textalign'>" + dataItem.QuantityReceived + "</span>";
                                    else
                                        return "";
                                },
                                // editor: quantityEditor
                            },
                            {
                                field: "QuantityDamaged",
                                title: "QTY Damaged",
                                width: "5%",
                                filterable: { search: true },
                                template: function (dataItem) {
                                    if ($scope.EditMode && colEditable(dataItem, 'Damaged')) {
                                        return "<input style='width:100%;' ng-blur='quantityBlur(dataItem,$event)' class='QuantityDamaged' name='Qty' value='" + dataItem.QuantityDamaged + "'/>";                                       
                                    }
                                    else if (dataItem.QuantityDamaged)
                                        return "<span class='Grid_Textalign'>" + dataItem.QuantityDamaged + "</span>";
                                    else return "";
                                },
                                //editor: quantityEditor
                            },
                            {
                                field: "QuantityMisShipped",
                                title: "QTY Mis-Shipped",
                                width: "5%",
                                filterable: { search: true },
                                template: function (dataItem) {
                                    if ($scope.EditMode && colEditable(dataItem, 'Missing')) {
                                        return "<input style='width:100%;' ng-blur='quantityBlur(dataItem,$event)' class='QuantityMisShipped' name='Qty' value='" + dataItem.QuantityMisShipped + "'/>";                                       
                                    }
                                    else if (dataItem.QuantityMisShipped)
                                        return "<span class='Grid_Textalign'>" + dataItem.QuantityMisShipped + "</span>";
                                    else return "";
                                },
                                // editor: quantityEditor
                            },
                            {
                                field: "QuantityShortShipped",
                                title: "QTY Short-Shipped",
                                width: "5%",
                                filterable: { search: true },
                                template: function (dataItem) {
                                    if ($scope.EditMode && colEditable(dataItem, 'Short Shipped')) {
                                        return "<input style='width:100%;' ng-blur='quantityBlur(dataItem,$event)' class='QuantityShortShipped' name='Qty' value='" + dataItem.QuantityShortShipped + "'/>";
                                    }
                                    else if (dataItem.QuantityShortShipped)
                                        return "<span class='Grid_Textalign'>" + dataItem.QuantityShortShipped + "</span>";
                                    else return "";
                                },
                                //editor: quantityEditor
                            },
                            {
                                field: "TrackingId",
                                title: "Tracking Info",
                                width: "5%",
                                template: function (dataItem) {
                                    if (dataItem.TrackingURL)
                                        return "<span><a href='" + dataItem.TrackingURL + "'target='_blank' style='color: rgb(61,125,139);'>" + dataItem.TrackingId + "</a></span>";
                                    else if (dataItem.TrackingId)
                                        return "<span>" + dataItem.TrackingId + "</span>";
                                    else
                                        return "";
                                }
                            },

                            {
                                field: "Statuses",
                                title: "Status",
                                width: "8%",
                                filterable: { search: true },
                                template: function (dataItem) {
                                    if ($scope.EditMode) {
                                        return '<input class="status" name="status" data-bind="value:' + dataItem.Statuses + '"/>';
                                    } else {
                                        if (jQuery.isEmptyObject($scope.shipStatus))
                                            return setTimeout(function () { multiSelectArrayToString(dataItem.Statuses); }, 200);
                                        else
                                            return multiSelectArrayToString(dataItem.Statuses);
                                    }
                                },
                                //editor: multiSelectEditor,


                            },
                            {
                                field: "Notes",
                                title: "Notes",
                                width: "12%",
                                template: function (dataItem) {
                                    var notes = "";
                                    if (dataItem.Notes)
                                        notes = dataItem.Notes;
                                    if ($scope.EditMode) {                                        
                                        return '<textarea class="Notes" ng-blur="NotesBlur(dataItem,$event)" rows="10" style=" width: 100% !important;">' + notes + '</textarea>';                                        
                                    }
                                    else
                                        return notes;
                                },                               
                            },
                        ],

                        editable: "inline",                        
                        dataBound: function (e) {
                            var grid = $("#ShippingNotesGrid").data("kendoGrid");
                            if ($scope.EditMode)
                                grid.showColumn(0);
                            else
                                grid.hideColumn(0);

                            //on dataBound event restore previous selected rows:                        
                            var view = this.dataSource.view();
                            for (var i = 0; i < view.length; i++) {
                                if (checkedIds[view[i].id]) {
                                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                        .addClass("k-state-selected")
                                        .find("input[type='checkbox']")
                                        .attr("checked", "checked");
                                }
                            }
                            InitializeControls(e);
                        },
                        toolbar: [
                            {
                                template: function (headerData) {
                                    return $("#toolbarTemplate").html()
                                }
                            }
                        ],
                        pageable: {
                            refresh: true,
                            pageSize: 25,
                            pageSizes: [25, 50, 100],
                        },
                        filterable: true,
                        resizable: true,
                        noRecords: { template: "No records found" },
                        scrollable: true,

                    };
                };

                var InitializeControls = function (e) {
                    $scope.dataCount = 0;
                    var QuantityTextbox = $("input[name='Qty']");
                    for (var i = 0; i < QuantityTextbox.length; i++) {
                        var idValue = QuantityTextbox[i].className + i;
                        $(QuantityTextbox[i]).prop("id", idValue);
                        $("#" + idValue).kendoNumericTextBox({
                            format: "#",
                            decimals: 0,
                            min: 0,
                            spinners: false
                        });
                    }

                    var Status = $(".status");
                    var data = e.sender.dataSource._data;
                    for (var i = 0; i < Status.length; i++) {
                        var idValue = 'statusDropdown' + data[i].ShipmentDetailId;
                        $(Status[i]).prop("id", idValue);
                    }

                    if (Status && Status.length != 0) {
                        $('.status').kendoMultiSelect({
                            valuePrimitive: true,
                            autoBind: true,
                            //optionLabel: "Select",
                            dataTextField: "Name",
                            dataValueField: "StatusId",
                            change: function (e) {
                                var value = this.value();
                                var ctrlID = e.sender.element[0].id;
                                var ShipmentDetailId = ctrlID.split('down')[1];
                                for (var i = 0; i < $scope.ShippingNotice.ListShipmentDetails.length; i++) {
                                    if ($scope.ShippingNotice.ListShipmentDetails[i].ShipmentDetailId == ShipmentDetailId) {
                                        $scope.ShippingNotice.ListShipmentDetails[i].Statuses = value;
                                    }
                                }
                                $scope.UpdateHeaderStatus();
                            },
                            close: function (e) {
                                if ($scope.StatusPreventClose == true)
                                    e.preventDefault();
                                $scope.StatusPreventClose = false;
                            },
                            dataSource: $scope.shipStatus,
                        });

                        var data = e.sender.dataSource._data;
                        setTimeout(function (e) {
                            var j = 0;
                            for (var i = 0; i < data.length; i++) {
                                if ($scope.EditMode) {
                                    var statusId = '#statusDropdown' + data[i].ShipmentDetailId;
                                    if ($(statusId) && $(statusId).length !== 0 && data[i].Statuses.length > 0) {
                                        var statusDropdown = $(statusId).data("kendoMultiSelect");
                                        var statuses = [];
                                        $.each(data[i].Statuses, function (index, status) {
                                            statuses.push(status);
                                        });
                                        var res = $.merge($.merge([], statusDropdown.value()), statuses); // merging exisiting and new selected ids
                                        statusDropdown.value(res);
                                        statusDropdown.trigger("change");
                                    }
                                }
                            }
                        }, 1000);
                    }
                }

                var colEditable = function (dataItem, field) {
                    //if ((dataItem.Statuses.find(item => item.name === field)).length > 0) {
                    //    return true;
                    //}
                    return true;// false;
                }

                //var quantityEditor = function (container, options) {
                //    // create an input element
                //    var input = $("<input style='width:100%;' data-bind='value: " + options.field + "' name='" + options.field + "' ng-blur='quantityBlur(dataItem,$event);'/>");
                //    // append it to the container
                //    input.appendTo(container).kendoNumericTextBox({
                //        type: "number",
                //        format: "#",
                //        decimals: 0,   // Settings for the editor, e.g. no decimals
                //        step: 0,   // Defines how the up/down arrows change the value
                //        min: 0,// You can also define min/max values
                //        spinners: false
                //    });;
                //}
                //var multiSelectEditor = function (container, options) {
                //    $('<input data-bind="value:' + options.field + '" id="statusDropDown' + options.model.ShipmentDetailId + '""/>')
                //        .appendTo(container)
                //        .kendoMultiSelect({
                //            //suggest: true,
                //            //autoBind:false,                            
                //            optionLabel: "Select",
                //            dataTextField: "Name",
                //            dataValueField: "StatusId",
                //            dataSource: options.values,
                //            change: function (e) {
                //                var event = e;
                //                //var selected = $("#statusDropDown").data("kendoMultiSelect").value();
                //                //var res = $.merge($.merge([], selected), [1, 2]);
                //                $("#statusDropDown").data("kendoMultiSelect").value(res);
                //            },
                //            select: function (e) {
                //                var dataItem = e.dataItem;
                //            }
                //        });
                //};

                var multiSelectArrayToString = function (Statuses) {
                    var strStatus = "";
                    if (jQuery.isEmptyObject($scope.shipStatus) == false) {
                        if (Statuses != null && Statuses.length > 0) {
                            $.each(Statuses, function (index, status) {
                                var name = $scope.shipStatus.find(item => item.StatusId === status).Name;
                                strStatus = strStatus == "" ? name : (strStatus + "," + name);

                            });
                        }
                    }
                    return strStatus;
                };                

                //on click of the checkbox:
                function selectRow() {
                    var checked = this.checked,
                        row = $(this).closest("tr"),
                        grid = $("#ShippingNotesGrid").data("kendoGrid"),
                        dataItem = grid.dataItem(row);

                    checkedIds[dataItem.id] = checked;

                    if (checked) {
                        //-select the row
                        row.addClass("k-state-selected");

                        var checkHeader = true;

                        $.each(grid.items(), function (index, item) {
                            if (!($(item).hasClass("k-state-selected"))) {
                                checkHeader = false;
                            }
                        });

                        $("#header-chb")[0].checked = checkHeader;
                    } else {
                        //-remove selection
                        row.removeClass("k-state-selected");
                        $("#header-chb")[0].checked = false;
                    }
                }

                $scope.ValidateDate = function (e) {
                    var shippedDate = new Date($scope.ShippingNotice.ShippedDate);
                    var CheckedinDate = new Date(e);
                    if (CheckedinDate <= shippedDate) {
                        HFC.DisplayAlert("Checked in Date should be less than Shipped date");
                    }
                }

                $scope.dropdownFunction = function (element) {
                    var dropdowns = document.getElementsByClassName("dropdown-content");
                    for (var i = 0; i < dropdowns.length; i++) {
                        if (dropdowns[i].id != element)
                            dropdowns[i].classList.remove('show');
                    }
                    document.getElementById(element).classList.toggle("show");
                }
                $scope.PackingBlur = function (dataItem, event) {
                    for (var i = 0; i < $scope.ShippingNotice.ListShipmentDetails.length; i++) {
                        if (dataItem.ShipmentDetailId == $scope.ShippingNotice.ListShipmentDetails[i].ShipmentDetailId) {
                            //dataItem.set("Notes", event.target.value);
                            $scope.ShippingNotice.ListShipmentDetails[i].PackingSlip = event.target.value;
                        }
                    }
                }

                $scope.NotesBlur = function (dataItem, event) {
                    for (var i = 0; i < $scope.ShippingNotice.ListShipmentDetails.length; i++) {
                        if (dataItem.ShipmentDetailId == $scope.ShippingNotice.ListShipmentDetails[i].ShipmentDetailId) {
                            //dataItem.set("Notes", event.target.value);
                            $scope.ShippingNotice.ListShipmentDetails[i].Notes = event.target.value;
                        }
                    }
                }

                $scope.UpdateHeaderStatus = function () {
                    //var ReceivedinFull = false;
                    //var Damaged = false;
                    //var MisShipped = false;
                    //var ShortShipped = false;
                    var statuses = $scope.ShippingNotice.ListShipmentDetails.map(list => list.Statuses).join(',');
                    //for (var i = 0; i < $scope.ShippingNotice.ListShipmentDetails.length; i++) {
                    //var statuses = multiSelectArrayToString($scope.ShippingNotice.ListShipmentDetails[i].Statuses);
                    if (statuses) {
                        if (statuses.includes("3"))//("Damaged"))
                            //Damaged = true;
                            $scope.ShippingNotice.Status = $scope.shipStatus.find(item => item.StatusId == 3).Name;
                        else if (statuses.includes("5"))//("Mis-Shipped"))
                            //MisShipped = true;
                            $scope.ShippingNotice.Status = $scope.shipStatus.find(item => item.StatusId == 5).Name;
                        else if (statuses.includes("4"))//("Short Shipped"))
                            //ShortShipped = true;
                            $scope.ShippingNotice.Status = $scope.shipStatus.find(item => item.StatusId == 4).Name;
                        else if ((statuses.includes("7") || statuses.includes("1")) && Number($scope.ShippingNotice.TotalQuantityReceived) === Number($scope.ShippingNotice.TotalQuantityShipped))//=="Receive"
                        {
                            var statusArray = statuses.split(',');
                            if (statusArray.find(x => x === 1 || 7).count === $scope.ShippingNotice.ListShipmentDetails.count)
                                $scope.ShippingNotice.Status = $scope.shipStatus.find(item => item.StatusId == 7).Name;
                            else
                                $scope.ShippingNotice.Status = $scope.shipStatus.find(item => item.StatusId == 8).Name;
                        }
                        else
                            $scope.ShippingNotice.Status = $scope.shipStatus.find(item => item.StatusId == 8).Name;
                    } else $scope.ShippingNotice.Status = $scope.shipStatus.find(item => item.StatusId == 8).Name;

                    //}
                    //$scope.ShippingNotice.Status = Damaged == true ? "Damaged" : MisShipped == true ? "missing" : ShortShipped == true ? "Short Shipped" : ReceivedinFull == true ? "Received in Full" : "Open";
                    setTimeout(function () { $scope.$apply(); }, 100)
                }

                $scope.quantityBlur = function (dataItem, event) {
                    var key = event.target.id;
                    for (var i = 0; i < $scope.ShippingNotice.ListShipmentDetails.length; i++) {
                        if (dataItem.ShipmentDetailId == $scope.ShippingNotice.ListShipmentDetails[i].ShipmentDetailId) {
                            if (key.contains("QuantityReceived")) {
                                if (Number(event.target.value) > Number($scope.ShippingNotice.ListShipmentDetails[i].QuantityShipped)) {
                                    HFC.DisplayAlert("Quantity Received cannot be more than Quantity Shipped");
                                    return false;
                                } else
                                    //dataItem.set("QuantityReceived", event.target.value);
                                    $scope.ShippingNotice.ListShipmentDetails[i].QuantityReceived = event.target.value;
                            } else if (key.contains("QuantityDamaged")) {
                                if (Number(event.target.value) > Number($scope.ShippingNotice.ListShipmentDetails[i].QuantityShipped)) {
                                    HFC.DisplayAlert("Quantity Damaged cannot be more than Quantity Shipped");
                                    return false;
                                }
                                else
                                    //dataItem.set("QuantityDamaged", event.target.value);
                                    $scope.ShippingNotice.ListShipmentDetails[i].QuantityDamaged = event.target.value;
                            } else if (key.contains("QuantityMisShipped")) {
                                if (Number(event.target.value) > Number($scope.ShippingNotice.ListShipmentDetails[i].QuantityShipped)) {
                                    HFC.DisplayAlert("Quantity Mis-Shipped cannot be more than Quantity Shipped");
                                    return false;
                                }
                                else
                                    //dataItem.set("QuantityMisShipped", event.target.value)
                                    $scope.ShippingNotice.ListShipmentDetails[i].QuantityMisShipped = event.target.value;
                            } else if (key.contains("QuantityShortShipped")) {
                                if (Number(event.target.value) > Number($scope.ShippingNotice.ListShipmentDetails[i].QuantityShipped)) {
                                    HFC.DisplayAlert("Quantity Short Shipped cannot be more than Quantity Shipped");
                                    return false;
                                }
                                else
                                    //dataItem.set("QuantityShortShipped", event.target.value)
                                    $scope.ShippingNotice.ListShipmentDetails[i].QuantityShortShipped = event.target.value;
                            }
                        }
                        $scope.UpdateHeaderStatus();
                    }
                    //var grid = $("#ShippingNotesGrid").data("kendoGrid").dataSource.read($scope.ShippingNotice.ListShipmentDetails);
                    $scope.CalculateQuantity();
                }

                $scope.CalculateQuantity = function () {
                    var QuantityReceived = 0, QuantityDamaged = 0, QuantityMisShipped = 0, QuantityShortShipped = 0;
                    for (var i = 0; i < $scope.ShippingNotice.ListShipmentDetails.length; i++) {
                        QuantityReceived = QuantityReceived + Number($scope.ShippingNotice.ListShipmentDetails[i].QuantityReceived);
                        QuantityDamaged = QuantityDamaged + Number($scope.ShippingNotice.ListShipmentDetails[i].QuantityDamaged);
                        QuantityMisShipped = QuantityMisShipped + Number($scope.ShippingNotice.ListShipmentDetails[i].QuantityMisShipped);
                        QuantityShortShipped = QuantityShortShipped + Number($scope.ShippingNotice.ListShipmentDetails[i].QuantityShortShipped);
                    }
                    $scope.ShippingNotice.TotalQuantityDamaged = QuantityDamaged;
                    $scope.ShippingNotice.TotalQuantityMisShipped = QuantityMisShipped;
                    $scope.ShippingNotice.TotalQuantityShortShipped = QuantityShortShipped;
                    $scope.ShippingNotice.TotalQuantityReceived = QuantityReceived;

                    setTimeout(function () {
                        $scope.$apply();
                    }, 100);
                }
                $scope.CreateCaseForSelectedLines = function () {
                    //if ($scope.selectedLines != null && $scope.selectedLines.length > 0) {
                    //    var data = $("#ShippingNotesGrid").data("kendoGrid").dataSource._data;
                    //}                
                }

                $scope.UpdateStatusForSelectedLines = function (status) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "block";
                    var statusObj = {};
                    if (status != "Remove") {
                        if ($scope.shipStatus != null && $scope.shipStatus.length > 0) {
                            statusObj = $scope.shipStatus.find(item => item.Name === status);
                        }
                    }
                    if (checkedIds != null) {
                        var grid = $("#ShippingNotesGrid").data("kendoGrid");//.dataSource._data;  
                        $.each(checkedIds, function (index, value) {
                            if (value) {
                                var dataItem = grid.dataSource.get(index);
                                var ctrlId = "statusDropdown" + dataItem.ShipmentDetailId;
                                var statusDropdown = $("#" + ctrlId).data("kendoMultiSelect");
                                if (jQuery.isEmptyObject(statusObj)) {
                                    //dataItem.set("Statuses", statusObj);                                    
                                    statusDropdown.value([]);
                                    statusDropdown.trigger("change");
                                }
                                else {
                                    //var dtStatus = dataItem.Statuses;
                                    var selected = statusDropdown.value();
                                    var res = $.merge($.merge([], selected), [statusObj.StatusId]);
                                    statusDropdown.value(res);
                                    statusDropdown.trigger("change");
                                }
                            }
                        });
                        loadingElement.style.display = "none";
                    }
                    loadingElement.style.display = "none";
                }

                $scope.EditShipNotice = function () {
                    $scope.EditMode = true;
                    $("#ShippingNotesGrid").toggle($scope.EditMode);
                    checkedIds = {};
                    var grid = $("#ShippingNotesGrid").data("kendoGrid");
                    grid.refresh();                   
                    $("#header-chb")[0].checked = false;
                    //bind click event to the checkbox
                    grid.table.on("click", ".row-checkbox", selectRow);

                    $('#header-chb').change(function (ev) {
                        var checked = ev.target.checked;
                        $('.row-checkbox').each(function (idx, item) {
                            if (checked) {
                                if (!($(item).closest('tr').is('.k-state-selected'))) {
                                    $(item).click();
                                }
                            } else {
                                if ($(item).closest('tr').is('.k-state-selected')) {
                                    $(item).click();
                                }
                            }
                        });
                    });
                }

                $scope.SaveShipNotice = function () {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "block";
                    $http.post('/api/ShipNotice/' + $scope.ShipNoticeId + '/UpdateShipNoticeHeader', JSON.stringify($scope.ShippingNotice)).then(function (data) {
                        if (data.success != "") {
                            $scope.ShippingNotice = data.data;
                            $('#ShippingNotesGrid').data('kendoGrid').dataSource.read($scope.ShippingNotice.ListShipmentDetails);
                        }
                        else {
                            HFC.DisplayAlert(data.error);
                        }
                        $scope.EditMode = false;
                        $("#ShippingNotesGrid").toggle($scope.EditMode);
                        $('#ShippingNotesGrid').data('kendoGrid').refresh();
                        loadingElement.style.display = "none";
                    }).catch(function (e) {
                        $scope.EditMode = false;
                        $("#ShippingNotesGrid").toggle($scope.EditMode);
                        $('#ShippingNotesGrid').data('kendoGrid').refresh();
                        loadingElement.style.display = "none";
                    });
                }
                //}
                $scope.PreventKeypress = function (e) {
                    e.preventDefault();
                }

                //edit button code
                $scope.EditDetails = function (obj) {
                    $scope.Check = false;

                    var validator = $scope.kendoValidator("ShippingNotesGrid");
                    var grid = $("#ShippingNotesGrid").data("kendoGrid");

                    var item = $('#ShippingNotesGrid').find('.k-grid-edit-row');
                    if (!validator.validate()) {
                        return false;
                    }
                    $scope.editedVendorValue = obj;

                    $scope.EditMode = true;
                    $("#ShippingNotesGrid").toggle($scope.EditMode);
                    if (obj) {
                        grid.refresh();
                        grid.editRow(obj);
                    };
                };


                $scope.kendoValidator = function (gridId) {
                    return $("#" + gridId).kendoValidator({
                        validate: function (e) {
                            $("span.k-invalid-msg").hide();
                            var dropDowns = $(".k-dropdown");
                            $.each(dropDowns, function (key, value) {
                                var input = $(value).find("input.k-invalid");
                                var span = $(this).find(".k-widget.k-dropdown.k-header");
                                if (input.size() > 0) {
                                    $(this).addClass("dropdown-validation-error");
                                } else {
                                    $(this).removeClass("dropdown-validation-error");
                                }
                            });
                        }
                    }).getKendoValidator();
                }

                //Cancel button code
                $scope.cancelEditgrid = function () {
                    //$('#ShippingNotesGrid').data("kendoGrid").cancelChanges();
                    $scope.EditMode = false;
                    $("#ShippingNotesGrid").toggle($scope.EditMode);
                    $scope.GetShipNotice();
                    //$scope.Check = true;
                    $('#ShippingNotesGrid').data("kendoGrid").refresh();
                };

                $scope.PrintShipnotice = function () {

                    $scope.PrintService.PrintShipnotice(1);
                }

                $scope.bringTooltipPopup = function () {
                    $(".statusCanvas").show();
                }
            }

        ])
