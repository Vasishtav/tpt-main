﻿


'use strict';
app
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('ShippingNoticeDashboardController',
    ['$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'NavbarService',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, NavbarService) {

            HFCService.setHeaderTitle("ShipmentsList #");
            $scope.SelectVendorId = 0;
            $scope.Procurement = {
                VendorID: '',
            }
            $scope.Permission = {};
            var ShipNoticePermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Shipments')

            $scope.Permission.ListShipNotice = ShipNoticePermission.CanRead;
            $scope.Permission.SubmitShipNotice = ShipNoticePermission.CanCreate;
            $scope.Permission.ViewShipNotice = ShipNoticePermission.CanRead;
            $scope.Permission.ShipNoticeEdit = ShipNoticePermission.CanUpdate;

            $scope.listShipment = {};
            $scope.ValueList = [];
            $scope.ActiveToolTips = false;
            $scope.HFCService = HFCService;
            $scope.NavbarService = NavbarService;
            $scope.NavbarService.SelectOperation();
            $scope.NavbarService.EnableOperartionsShippingNoticeDashboardTab();           
            var offsetvalue = 0;
            $scope.InitialLoad = true;
            //grid code
            $scope.GetShipment = function () {
                $scope.GridEditable = false;
                $scope.ShipmentData = {
                    dataSource: {
                        transport: {
                            read: function (e) {

                                $scope.ValueList = $scope.Valueselected();

                                if ($scope.ValueList != 0 || $scope.ValueList != null || $scope.ValueList != undefined) {
                                    $http.get('/api/ShipNotice/' + $scope.SelectVendorId + '/Get?listStatus=' + $scope.ValueList + '&dateFilter=' + $scope.Procurement.Id).then(function (data) {
                                        $scope.listShipment = data.data;
                                        e.success($scope.listShipment);

                                    });
                                }
                            }

                        },
                        //requestStart: function () {
                        //    if (!$scope.InitialLoad)
                        //    kendo.ui.progress($("#ProcurementboardGrid"), true);
                        //},
                        //requestEnd: function () {
                        //    if (!$scope.InitialLoad)
                        //    kendo.ui.progress($("#ProcurementboardGrid"), false);
                        //else
                        //        $scope.InitialLoad = false;
                        //},
                        error: function (e) {
                           // kendo.ui.progress($("#ProcurementboardGrid"), false);
                            HFC.DisplayAlert(e.errorThrown);
                        },
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    TotalQuantityShipped: { editable: false },
                                    VPO: { editable: false },
                                    OrderNumber: { type: "number", editable: false },
                                    ShipmentId: {editable: false },
                                    POQuantity: { type: "number", editable: false },
                                    POLines: { type: "number", editable: false },
                                    ReceivedComplete: { editable: false },
                                    ReceivedCompleteDate: { type: 'date', editable: false },
                                    SideMark: { editable: false },
                                    MasterPONumber: { type: "number", editable: false },
                                    PurchaseOrderId: { editable: false },
                                    FullVendorName: { editable: false },
                                    OrderID: { editable: false },
                                    ShippedDate: { editable: false, type: "date" },
                                    Status: { editable: false },
                                    ShipNoticeId: { editable: false }

                                }
                            },
                            //parse: function (d) {

                            //    $.each(d, function (idx, elem) {
                            //        elem.ShippedDate = kendo.parseDate(elem.OrderDate, 'yyyy-MM-dd');// kendo.parseDate(elem.OrderDate, "MM/dd/yyyy");
                            //        elem.ReceivedCompleteDate = kendo.parseDate(elem.ReceivedCompleteDate, 'yyyy-MM-dd');// kendo.parseDate(elem.OrderDate, "MM/dd/yyyy");

                            //    });
                            //    return d;
                            //}
                        }
                    },
                    dataBound: function () {
                        if ($scope.InitialLoad) {
                            var heightValue = $("#ProcurementboardGrid .k-grid-content.k-auto-scrollable").height();
                            $("#ProcurementboardGrid .k-grid-content.k-auto-scrollable").height((heightValue - 20) + "px");
                            $scope.InitialLoad = false;
                        }
                    },
                    columns: [
                        {
                            field: "ShipmentId",
                            title: "Shipment Id",
                            width: "200px",
                            hidden: false,
                            template: "<span class='Grid_Textalign'><a href='\\\\#!/shipnotices/#= ShipNoticeId #' style='color: rgb(61,125,139);'><span>#= ShipmentId #</span></a></span> "

                        },
                        {
                            field: "ShippedDate",
                            title: "Shipped Date",
                            width: "150px",
                            hidden: false,
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ShippingShippedDateFilterCalendar", offsetvalue),
                            template: function (dataitem) {
                                if (dataitem.ShippedDate == null)
                                    return "";
                                else
                                    return HFCService.KendoDateFormat(dataitem.ShippedDate);
                            }
                               // "#=  (ShippedDate == null)? '' : kendo.toString(kendo.parseDate(ShippedDate, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                        },
                        {
                            field: "FullVendorName",
                            title: "Vendor Name",
                            width: "150px",
                            hidden: false,
                        },
                        {
                            field: "OrderNumber",
                            title: "Order",
                            width: "100px",
                            hidden: false,
                            template: "<span class='Grid_Textalign'><a href='\\\\#!/Orders/#= OrderID #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a></span> ",
                            //template: "<a href='\\\\#!/Orders/#= OrderNumber #' style='color: rgb(61,125,139);'><span >#= OrderNumber #</span></a>",
                            filterable: {  search: true },
                        },
                        {
                            field: "SideMark",
                            title: "SideMark",
                            width: "150px",
                            hidden: false,
                        },

                         {
                             field: "MasterPONumber",
                             title: "MPO",
                             width: "100px",
                             filterable: {  search: true },
                             hidden: false,
                             template: function (dataItem) {
                                 return "<span class='Grid_Textalign'><a href='\\#!/purchasemasterview/" +
                                     dataItem.PurchaseOrderId +
                                     "' style='color: rgb(61,125,139);'><span>" +
                                     dataItem.MasterPONumber +
                                     "</span></a></span>";
                             },
                         },
                        {
                            field: "VPO",
                            title: "VPO",
                            width: "100px",
                            filterable: { search: true },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'><a href='\\#!/purchasemasterview/" +
                                    dataItem.PurchaseOrderId +
                                    "' style='color: rgb(61,125,139);'><span>" +
                                    dataItem.VPO +
                                    "</span></a></span>";
                            },

                        },




                        {
                            field: "POLines",
                            title: "PO Lines",
                            width: "150px",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem.POLines)
                                    return "<span class='Grid_Textalign'>" + dataItem.POLines + "</span>";
                                else
                                    return "";
                            },
                        },
                        {
                            field: "POQuantity",
                            title: "PO QTY",
                            width: "150px",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem.POQuantity)
                                    return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.POQuantity, 'number') + "</span>";
                                else
                                    return "";
                            },
                        },
                        {
                            field: "TotalQuantityShipped",
                            title: "QTY Shipped",
                            width: "120px",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem.POQuantity)
                                    return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.TotalQuantityShipped, 'number') + "</span>";
                                else
                                    return "";
                            },
                        },
                        {
                            field: "QuantityReceived",
                            title: "QTY Received",
                            width: "120px",
                            hidden: false,
                            template: function (dataItem) {
                                if (dataItem.POQuantity)
                                    return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.QuantityReceived, 'number') + "</span>";
                                else
                                    return "";
                            },
                        },
                        {
                            field: "Status",
                            title: "Status",
                            width: "100px",
                            hidden: false,
                            filterable: {
                                multi: true,
                                search: true
                            },
                        },
                        {
                            field: "ReceivedCompleteDate",
                            title: "Date Received Complete",
                            width: "200px",
                            hidden: false,
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ShippingReceivedCompleteDateFilterCalendar", offsetvalue),
                            template: function (dataitem) {
                                if (dataitem.ReceivedCompleteDate == null)
                                    return "";
                                else
                                    return HFCService.KendoDateFormat(dataitem.ReceivedCompleteDate);
                            }
                              //  "#=  (ReceivedCompleteDate == null)? '' : kendo.toString(kendo.parseDate(ReceivedCompleteDate, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                        }

                    ],

                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],
                        change: function (e) {
                            var myDiv = $('.k-grid-content.k-auto-scrollable');
                            myDiv[0].scrollTop = 0;
                        }
                    },
                    toolbar: kendo.template($('<script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="prodashboardgridSearch()" type="search" id="searchBox" placeholder="Search..." class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="ProcurementSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></div></script>').html()),
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                isempty: "Empty",
                                isnotempty: "Not empty"


                            },
                            number: {
                                eq: "Equal to",
                                neq: "Not equal to",
                                gte: "Greater than or equal to",
                                lte: "Less than or equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    resizable: true,
                    sortable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,

                };
            };



            //search clear
            $scope.ProcurementSearchClear = function () {
                $('#searchBox').val('');
                var searchValue = $('#searchBox').val('');
                $("#ProcurementboardGrid").data('kendoGrid').dataSource.filter({
                });
            }

            //to get active vendor to drop down
            //$http.get('/api/Procurement/0/VendorNameList').then(function (data) {
            //    $scope.VendorListName = data.data;
            //});
            $scope.procurement_vendorname = function () {

                $scope.procurement_vendorname = {
                    optionLabel: {
                        Name: "All",
                        VendorID: ""
                    },
                    dataTextField: "Name",
                    dataValueField: "VendorID",
                    filter: "contains",
                    valuePrimitive: true,
                    change: function (data) {
                        var val = this.value();
                        $scope.VendorChange(val);
                    },
                    autoBind: true,
                    dataSource: {
                        transport: {
                            read: {
                                url: "/api/Procurement/0/VendorNameList",
                            }
                        }
                    },
                };
            }
            $scope.procurement_vendorname();

            //to get Dashboard Filter
            $http.get('/api/Procurement/0/Type_DateTime?Tableid=' + 4).then(function (data) {
                $scope.DateTimeDropdown = data.data;
                for (var i = 0; i < $scope.DateTimeDropdown.length; i++) {
                    if ($scope.DateTimeDropdown[i]['Name'] == 'Last 90 days (rolling)') {
                        $scope.DateRangeId = $scope.DateTimeDropdown[i]['Id'];
                        $scope.Procurement.Id = $scope.DateTimeDropdown[i]['Id'];
                        $scope.FilterChange($scope.DateTimeDropdown[i]['Id']);
                        $scope.GetShipment();
                    }
                }
            })


            //on selecting drop-down value(vendor)
            $scope.VendorChange = function (id) {
                if (id === null || id === '' || id === undefined) {
                    $scope.SelectVendorId = 0;
                } else {
                    $scope.SelectVendorId = id;
                }

                $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
                //$('#ProcurementboardGrid').data('kendoGrid').refresh();
            }

            //on selecting drop-down value(vendor)
            $scope.FilterChange = function (id) {
                if (id === null || id === '' || id === undefined) {
                    $scope.Id = '';
                } else {
                    $scope.Id = id;
                }
                if ($('#ProcurementboardGrid').data('kendoGrid')) {
                    $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
                    //$('#ProcurementboardGrid').data('kendoGrid').refresh();
                }
                return;
            }
            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 315);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 315);
                };
            });
            // End Kendo Resizing


            //on clicking the checkboxes values to be passed
            $scope.Check = function () {
                $scope.ValueList = $scope.Valueselected();
                $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
               // $('#ProcurementboardGrid').data('kendoGrid').refresh();
                var myDiv = $('.k-grid-content.k-auto-scrollable');
                myDiv[0].scrollTop = 0;

                //if ($scope.ValueList != 0 || $scope.ValueList != null || $scope.ValueList != undefined) {
                //    $http.get('/api/Procurement/0/Get?listStatus=' + $scope.ValueList).then(function (data) {

                //    });
                //}
            }
             $scope.bringTooltipPopup = function()
              {
                  $(".statusCanvas").show();
              }

            //getting the check-box value
            $scope.Valueselected = function () {
                var arr = [];
                var result = '';
                if ($('#Check1').is(":checked")) {
                    result = 1;
                    arr.push(result);
                }
                if ($('#Check2').is(":checked")) {
                    result = 2;
                    arr.push(result);
                }
                if ($('#Check3').is(":checked")) {
                    result = 3;
                    arr.push(result);
                }
                return arr;
            }

            //search box code
            $scope.prodashboardgridSearch = function () {
                var searchValue = $('#searchBox').val();

                $("#ProcurementboardGrid").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "MasterPONumber",
                          //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          operator: "eq",
                          value: searchValue
                      },
                      {
                          field: "OrderNumber",
                          //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          operator: "eq",
                          value: searchValue
                      },
                        {
                            field: "VPO",
                            operator: "contains",
                            value: searchValue
                        },
                        {
                            field: "ShipmentId",
                            operator: "contains",
                            value: searchValue
                        },
                      {
                          field: "FullVendorName",
                          operator: "contains",
                          value: searchValue
                      },
                    {
                        field: "SideMark",
                        operator: "contains",
                        value: searchValue
                    },
                        {
                            field: "Status",
                            operator: "contains",
                            value: searchValue
                        }
                    ]
                });

            }

            $scope.ActiveHelp = function () {
                if ($scope.ActiveToolTips == true) {
                    $scope.ActiveToolTips = false;

                }
                else {
                    $scope.ActiveToolTips = true;

                }
            }

        }

    ])
