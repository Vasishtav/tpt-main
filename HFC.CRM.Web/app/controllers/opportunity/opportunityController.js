﻿
'use strict';
app.controller('opportunityController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
                    'HFCService',  'AddressService',
                      'PurchaseOrderFilter',
                     'OpportunityService', 'NewtaskService', 'NavbarService'
                    , 'NoteServiceTP',
                     'AccountService', 'kendoService', 'PrintService',
                    'AdditionalAddressServiceTP', 'DialogYesNoService','calendarModalService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http,
        HFCService, AddressService,
         PurchaseOrderFilter,
          OpportunityService, NewtaskService, NavbarService,
         NoteServiceTP,
         AccountService, kendoService, PrintService,
         AdditionalAddressServiceTP, DialogYesNoService, calendarModalService) {

        //we will use local ctrl var so we don't clutter up $scope that doesn't need to be shared with nested controllers and directives
        var ctrl = this;

        $scope.calendarModalService = calendarModalService;
        // Notes Integration.
        $scope.NoteServiceTP = NoteServiceTP;
        $scope.NoteServiceTP.OpportunityId = $routeParams.OpportunityId;
        $scope.NoteServiceTP.ParentName = "Opportunity";

        // Fix for TP-1588	Attachments - duplicating
        $scope.NoteServiceTP.FranchiseSettings = false;

        var Opportunitypermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity')
        $scope.NoteServiceTP.showAddNotes = Opportunitypermission.CanCreate;
        $scope.NoteServiceTP.showEditNotes = Opportunitypermission.CanUpdate;

        $scope.NavbarService = NavbarService;

        $scope.ReferredAccountId = $routeParams.AccountIdReferred;

        // Opportunity doesn't have its own additional address association.
        // It will associated with coressponding account.
        $scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
        $scope.AdditionalAddressServiceTP.ParentName = "Account";
        $scope.AdditionalAddressServiceTP.AccountId = $routeParams.AccountIdReferred;
        $scope.loadingElement = document.getElementById("loading");

        //Initialize the AdditionalAddressServiceTP, so that it will get the necessary data from the DB.
        $scope.AdditionalAddressServiceTP.Initialize();
        $scope.ShowPopup = true;
        //flag for date validation
        $scope.ValidDate = true;
        $scope.ValidDatelist = [];

        $scope.selectOptions = {
            placeholder: "Select",
            dataTextField: "Name",
            dataValueField: "accountId",
            valuePrimitive: true,
            autoBind: true,
            dataSource: {
                data: $scope.accountlist
            }
        };
        $scope.selectedIds = [];

        // get territories for the current franchise
        $http.get('/api/Accounts/0/getTerritoryForFranchise').then(function (response) {
            if (response.data != null) {

                if (!$scope.Opportunity)
                    $scope.Opportunity = {};

                if (response.data.length > 0) {
                    $scope.DisplayTerritorySource = response.data;
                }
                else {
                    $scope.DisplayTerritorySource = [];
                }


            }
            $scope.DisplayTerritorySource.splice(0, 0, { Id: 0, Name: 'Select' });
        });


        // The callback for successful quick address add from installation
        $scope.handleInstallationAddressAfterQuickAdd = function (newAddressId) {

            resetInstallAfterQuickAdd($scope.AdditionalAddressServiceTP.AccountId, newAddressId)
        }

        var resetInstallAfterQuickAdd = function (accountid, newaddressid) {

            if (!accountid) return;

            $http.get('/api/opportunities/' +accountid + '/InstallationAddresses').then(function (data) {
                var addresses = data.data;
                // update the kendo dropdown data source.
                $scope.InstallationAddresslist = addresses;
                $scope.BillingAddressList = addresses;

                // Select the newly added address as the default one for the user.
                $scope.selectedInstallationAddressId = newaddressid;
            });
        }

        // The callback for successful quick address add from Billing
        $scope.handleBillingAddressAfterQuickAdd = function (newAddressId) {
            resetBillingAfterQuickAdd($scope.AdditionalAddressServiceTP.AccountId, newAddressId)
        }

        var resetBillingAfterQuickAdd = function (accountid, newaddressid) {

            if (!accountid) return;

            $http.get('/api/opportunities/' +accountid + '/InstallationAddresses').then(function (data) {
                var addresses = data.data;

                // update the kendo dropdown data source.
                $scope.InstallationAddresslist = addresses;
                $scope.BillingAddressList = addresses;

                // Select the newly added address as the default one for the user.
                $scope.selectedBillingAddressId = newaddressid;
            });
        }

        // we need this as it needs to be watched by angular
        $scope.selectedBillingAddressId = null;
        $scope.selectedInstallationAddressId = null;

        $scope.$watch('selectedInstallationAddressId', function () {
            handleInstallationAddressChnage();
        });

        var handleInstallationAddressChnage = function () {
            if (!$scope.selectedInstallationAddressId
                || $scope.selectedInstallationAddressId == 0
                || !$scope.InstallationAddresslist) {
                $scope.SelectedInstallationAddress = null;
                return;
            }

            var temp = $.grep($scope.InstallationAddresslist, function (s) {
                return s.InstallAddressId == $scope.selectedInstallationAddressId;
            });
            if (temp) {
                $scope.SelectedInstallationAddress = temp[0];
            }
        }

        $scope.$watch('selectedBillingAddressId', function () {
            handleBillingAddressChnage();
        });

        var handleBillingAddressChnage = function () {
            if (!$scope.selectedBillingAddressId
                || $scope.selectedBillingAddressId == 0
                || !$scope.BillingAddressList) {
                $scope.BillingAddress = null;
                return;
            }

            var temp = $.grep($scope.BillingAddressList, function (s) {
                return s.InstallAddressId == $scope.selectedBillingAddressId;
            });
            if (temp) {
                $scope.BillingAddress = temp[0];
            }

        }

        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesOpportunitiesTab();
        $scope.IsSalesagentChanged = false;

        $scope.AccountId = $routeParams.AccountId;

        $scope.Opportunity = {};

        // getting print service
        $scope.PrintService = PrintService;

        $scope.HFCService = HFCService;
        $scope.FranciseLevelTexting = $scope.HFCService.FranciseLevelTexting;
        $scope.AddressService = AddressService;
        $scope.filter = ctrl.filter = PurchaseOrderFilter;

        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;

        $scope.BrandId = $scope.HFCService.CurrentBrand;

        $scope.Permission = {};
        var Measurementpermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Measurement')
        var Quotepermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Quote')
        var SalesOrderpermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SalesOrder')

        $scope.Permission.AddOpportunity = Opportunitypermission.CanCreate;
        $scope.Permission.EditOpportunity = Opportunitypermission.CanUpdate;
        $scope.Permission.DisplayOpportunity = Opportunitypermission.CanRead;
        $scope.Permission.DisplayMeasurements = Measurementpermission.CanRead;
        $scope.Permission.ListQuotes = Quotepermission.CanRead;
        $scope.Permission.DisplayOrderInfo = SalesOrderpermission.CanRead;
        //$scope.Permission.AddQuotes = Quotepermission.CanRead; --rechanges to canCreate
        $scope.Permission.AddQuotes = Quotepermission.CanCreate;

        // What it does???
        HFCService.GetUrl();

        $scope.calendarModalId = 0;

        var OpportunityId = $routeParams.OpportunityId;

        $scope.GetAccountZillowinfo = function (address, citystatezip) {

            var _url = '/api/zillow/?address=' + address + '&citystatezip=' + citystatezip;
            $http.get(_url).then(function (data) {

                if (data.data.status == true) {
                    $scope.zillowdata = data.data.ZilloResponce;
                    $scope.ShowZillowlink = true;
                }
                else
                    $scope.ShowZillowlink = false;
            },
            function (error) {
                console.log("Error: " + error);
            });
        }

        $http.get('/api/Opportunities/0/checkOpportunityAccess').then(function (data) {

            $scope.OppFullAccess = data.data;
        });



        $scope.CreateNewOpportunity = function (accountid) {
            window.location.href = "#!/Opportunitys/" + accountid;
        }
        $scope.EditOpportunity = function (opportunityId) {
            window.location.href = "#!/opportunityEdit/" + opportunityId;
        }

        $scope.GetSalesAgent = function () {

            $http.get('/api/opportunities/0/GetSalesAgents').then(function (data) {
                $scope.SalesAgentlist = data.data;
            });
        }



        $scope.location = $location.$$path;
        if ($scope.location.match("OpportunityDetail")) {
            $scope.isDisabled = true;
            $scope.ShowPopup = false;
        }
        else if ($scope.location.match("opportunityEdit")) {
            $scope.isDisabled = false;
            $scope.ShowPopup = false;
        }

        $scope.GetInstallers = function () {
            $http.get('/api/opportunities/0/GetInstallers').then(function (data) {
                $scope.Installerslist = data.data;
            });
        }

        $scope.GetDispositions = function () {
            $scope.OpportunityDispositionlist = [
                          { DispositionId: 1, Name: 'Competitor' },
                          { DispositionId: 8, Name: 'Customer Cancelled' },
                          { DispositionId: 2, Name: 'DIY' },
                          { DispositionId: 3, Name: 'Pricing' },
                          { DispositionId: 4, Name: 'Too Small' },
                          { DispositionId: 5, Name: 'Not serviceable' },
                          { DispositionId: 6, Name: 'Not sure at this time' },
                          { DispositionId: 7, Name: 'Project on hold' }
            ];

        }


        $scope.Opportunity = {
            //OpportunityID: 1,
            OpportunityName: '',
            OpportunityStatus: '',
            Account: '',
            Address: '',
            Description: '',
            SalesAgent: '',
            Installer: '',
            SideMark: '',
            //SalesAgentId: 1,
            //InstallerId: 2,
           TerritoryDisplayId: 0,
            InstallAddressId: 1,
            SourcesList: [{
                //SourceId: 0,
                ChannelName: "",
                name: "",
                IsPrimarySource: true,
                SourceName: "",
                AllowFranchisetoReassign: null,
                OriginationName: ""
            }
            ],
            //BillingAddressId: 0,
            Quotes: [{
                QuoteID: '',
                QuoteName: '',
                TotalAmount: '',
            }],
            Order: {
                NetAmount: '',
                TotalSalesAmount: '',
                ServiceCharge: '',
                TaxAmount: '',
                DiscountAmount: '',
                DiscountDescription: '',
                ContractDate: '',
            },
            Accounts: {
                FirstName: '',
                LastName: '',
                Company: '',
                BillingAddress: '',
                Campaign: '',
                Source: '',
                Territory: ''
            },
            NotesAttachments: [
                { Type: 'Note', Title: 'First Note', Category: 'Internal', view: '', Created: '09-05-2017' },
                { Type: 'Note', Title: 'Installation Instructions', Category: 'Internal', view: '', Created: '09-05-2017' },
                { Type: 'Note', Title: 'Directions', Category: 'Internal', view: '', Created: '09-15-2017' },
                { Type: 'Attachment', Title: 'Installer Pic', Category: 'Photos', view: 'DSC_00007.jpg', Created: '09-17-2017' }
            ],
            ProjectNeedsBB: {
                NoOfWindows: '',
                Colors: '',
                RoomTypes: {
                    Dining: '',
                    Living: '',
                    Family: '',
                    BedRooms: '',
                    Kitchen: '',
                    BathRooms: '',
                    Other: ''
                },
                Intrest: {
                    Wood: '',
                    FauxWood: '',
                    Cellular: '',
                    Shades: '',
                    RollarShades: '',
                    Droperies: '',
                    Romans: '',
                    Verticles: '',
                    Shutters: '',
                    WovenWoods: '',
                    SolarShades: ''
                },
                Operational: '',
                Decorative: ''
            },
            ProjectNeedsTL: {

            },
            QuestionsAnswers1: []


        };


        $scope.showterritoryvalue = function () {


            if ($scope.TerritoryDisplayId > 0) {
                $scope.Opportunity.TerritoryDisplayId = $scope.TerritoryDisplayId;
            }
            else {
                $scope.Opportunity.TerritoryDisplayId = 0;

            }

        }


        $scope.OpportunityStatuseslist = null;
        $scope.GetOpportunityStatus = function () {
            $http.get('/api/opportunities/0/OpportunitiesStatus').then(function (data) {
                $scope.OpportunityStatuseslist = data.data;

                // If new mode set the status to New.
                if (!$scope.OpportunityId) {
                    $scope.Opportunity.OpportunityStatusId = 1;
                }
                //$scope.SelectedStatus = 1;
            });
        }
        $scope.accountlist = null;
        $scope.GetAccounts = function () {
            $http.get('/api/opportunities/0/GetAccounts').then(function (data) {
                $scope.accountlist = data.data;
            });
        }

        $http.get('/api/lookup/0/Campaigns').then(function (data) {
            $scope.Campaignslist = data.data;
            if ($scope.Opportunity != null && $scope.Opportunity.Account != null) {
                if ($scope.Opportunity.CampaignId == 0) {
                    $scope.Opportunity.CampaignId = $scope.Opportunity.Account.CampaignId;
                }
            }
        });

        $scope.ChangeCampaign1 = function () {
            var campaignId = $scope.Opportunity.CampaignId;
            if ($scope.Opportunity.CampaignId == undefined || $scope.Opportunity.CampaignId == null) {
                campaignId = 0;
                return;
            }

            $http.get('/api/lookup/' + campaignId + '/SourceTp').then(function (data) {
                $scope.Opportunity.SourcesList = data.data[0];
                $scope.Opportunity.SourcesTPId = data.data[0].SourceId;
            });
        }

        $scope.account_installationAddress_List = function () {
            $scope.account_installationAddress_option = {
                optionLabel: {
                    Name: "Select",
                    InstallAddressId: ""
                },
                dataTextField: "Name",
                dataValueField: "InstallAddressId",
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val = this.value();
                    if (val.length > 0) {
                        removevalidationerror("account_installationAddress");
                    }
                },
                dataSource: {
                    data: $scope.InstallationAddresslist
                }
            };
        }

        $scope.opportunity_BillingAddress_List = function () {
            $scope.opportunity_BillingAddress_option = {
                optionLabel: {
                    Name: "Select",
                    InstallAddressId: ""
                },
                dataTextField: "Name",
                dataValueField: "InstallAddressId",
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val = this.value();
                    if (val.length > 0) {
                        removevalidationerror("opportunity_BillingAddress");
                    }
                },
                dataSource: {
                    data: $scope.BillingAddressList
                }
            };
        }
        $scope.opportunity_BillingAddress_List();
        $scope.account_installationAddress_List();

        $scope.ResetInstallationAddressEdit = function (AccountId, ref) {

            //texting
            if ($scope.ShowPopup) {
                $http.get('/api/Accounts/' + AccountId).then(function (response) {
                    $scope.AccountCellphone = response.data.Account.PrimCustomer.CellPhone;
                    $scope.IsNotifyText = response.data.Account.IsNotifyText;
                    if (response.data.TextStatus == null) {
                        $scope.Opportunity.NotifyTextStatus = 'new';
                    }
                    else if (response.data.TextStatus.Optout == true) {
                        $scope.Opportunity.NotifyTextStatus = 'Optout';
                    }
                    else if (response.data.TextStatus.Optin == true) {
                        $scope.Opportunity.NotifyTextStatus = 'Optin';
                    }
                    else if (response.data.TextStatus.IsOptinmessagesent == true) {
                        $scope.Opportunity.NotifyTextStatus = 'waiting';
                    }
                    else if (response.data.TextStatus.Optout == null || response.data.TextStatus.Optin == null || response.data.TextStatus.IsOptinmessagesent == false) {
                        $scope.Opportunity.NotifyTextStatus = 'error';
                    }
                });
            }

            // We have to ...........
            $scope.AdditionalAddressServiceTP.AccountId = AccountId;

            if (AccountId == null || AccountId == undefined) {
                $scope.InstallationAddresslist = null;
                $scope.BillingAddressList = null;
                if ($("#opportunity_BillingAddress").data("kendoDropDownList")!=undefined)
                    $("#opportunity_BillingAddress").data("kendoDropDownList").dataSource.data($scope.BillingAddressList);
                if ($("#account_installationAddress").data("kendoDropDownList")!=undefined)
                    $("#account_installationAddress").data("kendoDropDownList").dataSource.data($scope.InstallationAddresslist);
                handleBillingAddressChnage();
                return;
            }
            $http.get('/api/opportunities/' + AccountId + '/InstallationAddresses').then(function (data) {

                $scope.BillingAddressList = data.data;
                if ($("#opportunity_BillingAddress").data("kendoDropDownList") != undefined)
                    $("#opportunity_BillingAddress").data("kendoDropDownList").dataSource.data($scope.BillingAddressList);
                handleBillingAddressChnage();

                var installingList = JSLINQ($scope.BillingAddressList)
                         .Where(function (item) { return item.AccountId > 0 });
                $scope.InstallationAddresslist = installingList.items;
                if ($("#account_installationAddress").data("kendoDropDownList") != undefined)
                    $("#account_installationAddress").data("kendoDropDownList").dataSource.data($scope.InstallationAddresslist);
                handleInstallationAddressChnage();


                var ilist = $scope.InstallationAddresslist;
                var blist = $scope.BillingAddressList;
                if (ilist && ilist.length == 1) {
                    $scope.selectedInstallationAddressId = ilist[0].InstallAddressId;
                }

                if (blist && blist.length == 1) {
                    $scope.selectedBillingAddressId = blist[0].InstallAddressId;
                }

                if ($window.location.href.includes("/opportunityEdit/")) {
                    $("#account_installationAddress").data("kendoDropDownList").value($scope.selectedInstallationAddressId);
                    $("#opportunity_BillingAddress").data("kendoDropDownList").value($scope.selectedBillingAddressId);
                }

                //$scope.RelatedAccountName = null;
                //if ($scope.BillingAddressList && $scope.BillingAddressList[0].RelatedAccountId) {
                //    $scope.getRelatedAccountName($scope.BillingAddressList[0].RelatedAccountId)
                //}


                //if (data.data.length == 1) {
                //    if ($scope.Opportunity.OpportunityId != undefined || $scope.ReferredAccountId > 0) {
                //        $scope.selectedInstallationAddressId = data.data[0].InstallAddressId;
                //        $scope.IsValidatedIns = data.data[0].IsValidated;
                //        $scope.selectedBillingAddressId = data.data[0].InstallAddressId;
                //        $scope.IsValidateBil = data.data[0].IsValidated;
                //        if ($scope.ReferredAccountId > 0) {
                //            $scope.SelectedInstallationAddress = data.data[0];
                //            $scope.Opportunity.BillingAddress = data.data[0];
                //        }
                //    }
                //    else {
                //        $scope.SelectedInstallationAddress = data.data[0];
                //        $scope.Opportunity.BillingAddress = data.data[0];
                //    }

                //}
                //else {
                //    if ($scope.selectedInstallationAddressId && $scope.InstallationAddresslist)
                //        $scope.selectedInsAddr = $.grep($scope.InstallationAddresslist, function (s) {
                //            return s.InstallAddressId == $scope.selectedInstallationAddressId
                //        });

                //    if ($scope.selectedInsAddr[0])
                //    $scope.IsValidatedIns = $scope.selectedInsAddr[0].IsValidated;

                //    if ($scope.selectedBillingAddressId && $scope.BillingAddressList)
                //        $scope.selectedBillAddr = $.grep($scope.BillingAddressList, function (s) {
                //            return s.InstallAddressId == $scope.selectedBillingAddressId;
                //        });

                //    if ($scope.selectedBillAddr[0])
                //    $scope.IsValidateBil = $scope.selectedBillAddr[0].IsValidated;
                //}

                // for territory displayid
                if (ref == 2) {

                    $http.get('/api/FranchiseSettings/0/GetDisplayTerritoryId').then(function (data) {
                        if (data.data) {
                            $scope.TerritoryDisData = data.data;
                            $scope.Opportunity.TerritoryDisplayId = data.data.TerritoryId;
                            $scope.TerritoryDisplayId = data.data.TerritoryId;

                        } else {
                            $scope.Opportunity.TerritoryDisplayId = 0;
                            $scope.TerritoryDisplayId = 0;

                        }

                        $http.get('/api/opportunities/' + AccountId + '/getDisplayTerritory').then(function (data) {

                            if (data.data.length > 0) {

                                $scope.Opportunity.TerritoryId = data.data[0].TerritoryId;
                                $scope.Opportunity.TerritoryType = data.data[0].TerritoryType;
                                if (data.data[0].DisplayTerritoryName)
                                    $scope.Opportunity.DisplayTerritoryName = data.data[0].DisplayTerritoryName;
                                if (data.data[0].TerritoryDisplayId && data.data[0].TerritoryDisplayId > 0) {
                                    $scope.Opportunity.TerritoryDisplayId = data.data[0].TerritoryDisplayId;
                                    $scope.TerritoryDisplayId = data.data[0].TerritoryDisplayId;
                                }
                                //if (data.data[0].TerritoryDisplayId && data.data[0].TerritoryDisplayId > 0) {
                                //    $scope.TerritoryDisplayId = data.data[0].TerritoryDisplayId;
                                //    $scope.Opportunity.TerritoryDisplayId = data.data[0].TerritoryDisplayId;

                                //    var includeSelect = true;
                                //    angular.forEach($scope.DisplayTerritorySource, function (value, key) {
                                //        if (value.Name.toUpperCase().includes('GRAY AREA')) {

                                //            includeSelect = false;
                                //        }
                                //    });

                                //    if (includeSelect == true) {
                                //        $scope.DisplayTerritorySource.splice(0, 0, { Id: 0, Name: 'Select' });
                                //    }
                                //}
                                //else {
                                //    $scope.TerritoryDisplayId = 0;
                                //    $scope.Opportunity.TerritoryDisplayId = 0;

                                //    angular.forEach($scope.DisplayTerritorySource, function (value, key) {
                                //        if (value.Name.toUpperCase().includes('GRAY AREA')) {

                                //            $scope.TerritoryDisplayId = value.Id;
                                //            $scope.Opportunity.TerritoryDisplayId = value.Id;
                                //        }
                                //    });
                                //}
                            }
                        });
                    });
                }
            });
        }

        $scope.GetSalesAgent();
        $scope.GetInstallers();

        $scope.GetOpportunityStatus();
        $scope.GetAccounts();
        $scope.GetDispositions();

        $scope.selectedstatus = 1;



        $scope.modalState = {
            loaded: true
        }

        $scope.$watch('modalState.loaded', function () {
            var loadingElement = document.getElementById("loading");

            if (!$scope.modalState.loaded) {
                loadingElement.style.display = "block";
            } else {
                loadingElement.style.display = "none";
            }
        });

        function errorHandler(reseponse) {


            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
        }

        if ($routeParams.OpportunityId > 0) {

            // Inform that the modal is not yet ready
            $scope.modalState.loaded = false;

            $http.get('/api/Opportunities/' + OpportunityId)
                .then(function (opportunityServiceData) {
                    $scope.Opportunity = opportunityServiceData.data.Opportunity;
                    if (document.location.href.toString().toUpperCase().indexOf("/OPPORTUNITYEDIT/") > 0) {
                        if ($scope.Opportunity.CreatedOnUtc != $scope.Opportunity.ReceivedDate) {
                            var receivedDate = ($scope.Opportunity.ReceivedDate).substr(0, (($scope.Opportunity.ReceivedDate).length - 1));
                            $scope.Opportunity.ReceivedDate = $scope.calendarModalService.getDateStringValue(new Date(receivedDate), false, false, true);
                        }
                    }
                        //texting
                    $scope.FranciseLevelTexting = opportunityServiceData.data.FranciseLevelTexting;
                    $scope.Opportunity.TextStatus = opportunityServiceData.data.TextStatus;
                    $scope.AccountCellphone = opportunityServiceData.data.Opportunity.AccountTP.PrimCustomer.CellPhone;
                    $scope.IsNotifyText = opportunityServiceData.data.Opportunity.AccountTP.IsNotifyText;

                        if ($scope.Opportunity.TextStatus == null) {
                            $scope.Opportunity.NotifyTextStatus = 'new';
                        }
                        else if ($scope.Opportunity.TextStatus.Optout == true) {
                            $scope.Opportunity.NotifyTextStatus = 'Optout';
                        }
                        else if ($scope.Opportunity.TextStatus.Optin == true) {
                            $scope.Opportunity.NotifyTextStatus = 'Optin';
                        }
                        else if ($scope.Opportunity.TextStatus.IsOptinmessagesent == true) {
                            $scope.Opportunity.NotifyTextStatus = 'waiting';
                        }
                        else if ($scope.Opportunity.TextStatus.Optout == null || $scope.Opportunity.TextStatus.Optin == null || $scope.Opportunity.TextStatus.IsOptinmessagesent == false) {
                            $scope.Opportunity.NotifyTextStatus = 'error';
                        }

                    $http.get('/api/FranchiseSettings/0/GetDisplayTerritoryId').then(function (data) {

                        if ($scope.Opportunity.TerritoryDisplayId > 0) {
                            // $scope.Account.TerritoryDisplayId = $scope.Account.TerritoryDisplayId;
                            $scope.TerritoryDisplayId = $scope.Opportunity.TerritoryDisplayId;
                        }
                        else {
                            if (data.data) {
                                $scope.TerritoryDisData = data.data;
                                $scope.Opportunity.TerritoryDisplayId = data.data.TerritoryId;
                                $scope.TerritoryDisplayId = data.data.TerritoryId;

                            } else {
                                $scope.Opportunity.TerritoryDisplayId = 0;
                                $scope.TerritoryDisplayId = 0;

                            }
                        }


                    });



                    $scope.Opportunity1 = angular.copy($scope.Opportunity);


                    $scope.selectedInstallationAddressId = $scope.Opportunity.InstallationAddressId;
                    $scope.selectedBillingAddressId = $scope.Opportunity.BillingAddressId;

                    $scope.CurrentSalesAgent = $scope.Opportunity.SalesAgentId;
                    $scope.CurrentInstaller = $scope.Opportunity.InstallerId;



                    $scope.BindOrderData();
                    //Added for Qualification/Question Answers
                    $scope.OpportunityQuestionAns = opportunityServiceData.data.OpportunityQuestionAns;
                    $scope.AllOpportunityQuestions = opportunityServiceData.data.OpportunityQuestionAns;
                    $scope.Opportunity.DispositionId = opportunityServiceData.data.Opportunity.DispositionId;//3;
                    if (document.location.href.toString().indexOf("/OpportunityDetail/") > 0) {
                        $scope.getAccountName($scope.Opportunity.AccountId);
                        //$scope.getAccountName($scope.Opportunity.OpportunityId);
                        $scope.getSalesAgentName($scope.Opportunity.SalesAgentId);
                        $scope.getInstallerName($scope.Opportunity.InstallerId);



                        // TODO: do we really need this?, can't we get it while fetching the data itself?
                        $scope.getInstalltionAddressName($scope.Opportunity.InstallationAddressId);
                        $scope.getBillingAddress($scope.Opportunity.BillingAddressId);

                        $scope.SelectedDisposition = findDispostionValue($scope.OpportunityDispositionlist, $scope.Opportunity.DispositionId);
                        //SelectedDisposition
                    }
                    if ($scope.Opportunity.RelatedAccountId) {
                        $scope.getRelatedAccountName($scope.Opportunity.RelatedAccountId);
                    }
                    //Display Installation and Billing Address on AccountId selection
                    $scope.ResetInstallationAddressEdit($scope.Opportunity.AccountId, 1);
                    $scope.AddressService.MeasurementId = $scope.Opportunity.InstallationAddressId;

                    if ($scope.Opportunity.IsTaxExempt == true) {
                        $scope.Opportunity.TaxExemptID = $scope.Opportunity.TaxExemptID;
                        $scope.Opportunity.IsTaxExempt = $scope.Opportunity.IsTaxExempt;
                    }

                    // Find Brand ID
                    var BrandId = 0;
                    if (opportunityServiceData.data.OpportunityQuestionAns.length > 0)
                        BrandId = opportunityServiceData.data.OpportunityQuestionAns[0].BrandId;

                    $scope.BrandId = BrandId;

                    //Added for Qualification/Question Answers
                    if (opportunityServiceData.data.OpportunityQuestionAns) {
                        var questArray = $.map(opportunityServiceData.data.OpportunityQuestionAns, function (qa) {
                            return qa.Question;
                        });
                        if (questArray)
                            $scope.UniqueOpportunityQuestions = $.grep(questArray, function (el, index) {
                                return index == $.inArray(el, questArray);
                            });
                    }
                    //$scope.isDisabled = true;
                    //Added for Qualification/Question Answers
                    $scope.GetQuestion = function (QAcollection, CurrentQuestion) {
                        //Get each question details based on current
                        //sub question from collection
                        if (QAcollection.length > 0 && CurrentQuestion) {
                            var sourcee = $.grep(QAcollection, function (q) {
                                return (q.Question == CurrentQuestion);
                            });
                            if (sourcee) return sourcee;
                        }
                        return null;
                    };
                    HFCService.setHeaderTitle("Opportunity #" + $scope.Opportunity.OpportunityId + " - " + $scope.Opportunity.OpportunityName);
                    // Inform that the modal is ready to consume:
                    $scope.modalState.loaded = true;



                    ////var instAdd = {};

                    ////for (var i = 0; i < $scope.Opportunity.Addresses.length; i++) {
                    ////    if ($scope.Opportunity.Addresses[i].AddressId === $scope.Opportunity.InstallationAddressId) {
                    ////        instAdd = $scope.Opportunity.Addresses[i];
                    ////    }
                    ////}
                    ////$scope.checkZillowinfo = instAdd.CountryCode2Digits == "US" ? true : false;
                    ////$scope.GetAccountZillowinfo(instAdd.Address1, instAdd.City + ', ' + instAdd.State + ' ' + instAdd.ZipCode);


                }, errorHandler);

            //Initialize the NoteServiceTP, so that it will get the necessary data from the DB.
            $scope.NoteServiceTP.Initialize();

        }
        else {
            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;

            if (document.location.href.toString().indexOf("/Opportunity/") > 0) {
                var s = window.location.hash;
                var z = s.split('/');
                if (z.length == 3) {
                    if (!$scope.Opportunity) $scope.Opportunity = {};
                    $scope.Opportunity.AccountId = parseInt(z[2]);

                    $scope.ResetInstallationAddressEdit($scope.Opportunity.AccountId,1);

                }
            }


            //$http.get('/api/Accounts/0/getTerritoryForFranchise').then(function (response) {
            //    if (response.data != null) {
            //        if (!$scope.Opportunity)
            //            $scope.Opportunity = {};


            //        if (response.data.length > 0) {
            //            $scope.DisplayTerritorySource = response.data;

            //        }
            //        else {
            //            $scope.DisplayTerritorySource = [];
            //        }


            //    }
            //    $scope.DisplayTerritorySource.splice(0, 0, { Id: 0, Name: 'Select' });
            //});
        }


        var isZillowApiCalled = false;
        $scope.handleKendoPanelBarExpand = function (ev) {

            if (isZillowApiCalled) return;

            isZillowApiCalled = true;

            var instAdd = {};

            for (var i = 0; i < $scope.Opportunity.Addresses.length; i++) {
                if ($scope.Opportunity.Addresses[i].AddressId === $scope.Opportunity.InstallationAddressId) {
                    instAdd = $scope.Opportunity.Addresses[i];
                }
            }
            //$scope.checkZillowinfo = instAdd.CountryCode2Digits == "US" ? true : false;
            $scope.GetAccountZillowinfo(instAdd.Address1, instAdd.City + ', ' + instAdd.State + ' ' + instAdd.ZipCode);


        }
        function removevalidationerror(value) {
            var dropdown = $("#" + value).data("kendoDropDownList").value();
            if (dropdown == null || dropdown == "") {
                $("#" + value).addClass("required-error");
            } else {
                $("#" + value).removeClass("required-error");
            }
        }

        function validatedropdowns() {
            var returnvalue = true;
            var accountdropdown = $("#opportunity_account").data("kendoDropDownList").value();
            var saleagentdropdown = $("#opportunity_saleagent").data("kendoDropDownList").value();
            var billing = $("#opportunity_BillingAddress").data("kendoDropDownList").value();
            var installation = $("#account_installationAddress").data("kendoDropDownList").value();
            if (accountdropdown == null || accountdropdown == "") {
                $("#opportunity_account").addClass("required-error");
                returnvalue = false;
            } else {
                $("#opportunity_account").removeClass("required-error");
            }
            if (saleagentdropdown == null || saleagentdropdown == "") {
                $("#opportunity_saleagent").addClass("required-error");
                returnvalue = false;
            } else {
                $("#opportunity_saleagent").removeClass("required-error");
            }
            if (billing == null || billing == "") {
                $("#opportunity_BillingAddress").addClass("required-error");
                returnvalue = false;
            } else {
                $("#opportunity_BillingAddress").removeClass("required-error");
            }
            if (installation == null || installation == "") {
                $("#account_installationAddress").addClass("required-error");
                returnvalue = false;
            } else {
                $("#account_installationAddress").removeClass("required-error");
            }
            return returnvalue;
        }

        $scope.opportunity_campaign = function () {

            $scope.opportunity_campaign = {
                optionLabel: {
                    Name: "Select",
                    CampaignId: ""
                },
                dataTextField: "Name",
                dataValueField: "CampaignId",
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val = this.value();
                    if (val.length > 0) {
                        $scope.ChangeCampaign1();
                    }

                },
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/Campaigns",
                        }
                    }
                },
            };
        }
        $scope.opportunity_campaign();
        $scope.opportunity_installer = function () {

            $scope.opportunity_installer = {
                optionLabel: {
                    Name: "Select",
                    InstallerId: ""
                },
                dataTextField: "Name",
                dataValueField: "InstallerId",
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val = this.value();
                    if (val.length > 0) {
                        $scope.InstallerChange();
                    }

                },
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/opportunities/0/GetInstallers",
                        }
                    }
                },
            };
        }
        $scope.opportunity_installer();
        $scope.opportunity_saleagent = function () {

            $scope.opportunity_saleagent = {
                optionLabel: {
                    Name: "Select",
                    SalesAgentId: ""
                },
                dataTextField: "Name",
                dataValueField: "SalesAgentId",
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val = this.value();
                    if (val.length > 0) {
                        $scope.SalesAgentChange();
                        removevalidationerror("opportunity_saleagent");

                    }

                },
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/opportunities/0/GetSalesAgents",
                        }
                    }
                },
            };
        }
        $scope.opportunity_saleagent();
        $scope.opportunity_account = function () {

            $scope.opportunity_account = {
                optionLabel: {
                    Name: "Select",
                    accountId: ""
                },
                dataTextField: "Name",
                dataValueField: "accountId",
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val = this.value();
                    if (val.length > 0) {
                        $scope.ResetInstallationAddressEdit(val, 2)
                        $scope.GetTaxDetails(val);
                        removevalidationerror("opportunity_account");
                    }
                },
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/opportunities/0/GetAccounts",
                        }
                    }
                },
            };
        }
        $scope.opportunity_account();
        $scope.RelatedSource = function () {

            $scope.RelatedSource = {
                placeholder: "Select",
                dataTextField: "name",
                dataValueField: "SourceId",
                filter: "contains",
                valuePrimitive: true,
                change: function (data) {
                    var val=this.value();
                    if (val.length > 0) {
                        $scope.Opportunity.SourcesTPId = val[0];
                    }

                },
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/SourceTP",
                        }
                    }
                },
            };
        }
        $scope.RelatedSource();
        //$scope.ChangeSource = function (val) {
        //
        //}
        $scope.BindOrderData = function () {

            $http.get('/api/Orders/' + $routeParams.OpportunityId + '/GetOrderByOpportunity').then(function (result) {

                if (result.data.error === '') {
                    $scope.Opportunity.Order = result.data.data;
                } else {
                    HFC.DisplayAlert(result.data.error);
                }


            });

        };


        $scope.CreateNewQuote = function () {

            window.location.href = "#!/quote/" + $routeParams.OpportunityId;
        }

        // TODO: Do we need this??
        $scope.findBySpecField = function (data, reqField, value, resField) {

            if (data == null) {

                return;
            }
            var container = data;
            for (var i = 0; i < container.length; i++) {
                if (container[i][Object.keys(container[i])[0]] == value) {
                    return (container[i]);
                }
            }
            return '';

        }

        if (OpportunityId == undefined || OpportunityId == null || OpportunityId == "") {
            //$scope.Opportunity = null;

            $scope.OpportunityStatusID = 0;
            $scope.SalesAgentId = 0;
            $scope.InstallerId = 0;
            $scope.InstallationAddressId = 0;

            $scope.mode = "Add"
        } else {
            $scope.mode = "Edit"
        }


        $scope.GetOpportunityQuestion = function (QAcollection, CurrentQuestion) {
            //Get each question details based on current
            //sub question from collection
            if (QAcollection.length > 0 && CurrentQuestion) {
                var sourcee = $.grep(QAcollection, function (q) {
                    return (q.Question == CurrentQuestion);
                });
                if (sourcee) return sourcee;
            }
            return null;

        };

        $scope.opportunityqa = [];
        $scope.data = {
            questionId: '',
            answer: ''
        };
        $scope.GetOpportunitiesData = function () {
            for (i in $scope.AllOpportunityQuestions) {
                //if (!this.IsBusy)
                {
                    this.IsBusy = true;
                    var question = $scope.AllOpportunityQuestions[i];
                    //promise = $scope.SaveOpportunityQA(leadId, question);
                    // data = { OpportunityId: $scope.OpportunityId };

                    if (question.Answer == undefined &&
                        (question.SelectionType == 'checkbox' ||
                        question.SelectionType == 'option'))
                    { question.Answer = false; }

                    this.IsBusy = false;
                    $scope.opportunityqa.push(question);
                }
            }
        }
        $scope.getAccountName = function (Accountid) {
            $http.get('/api/opportunities/' + Accountid + '/getAccountName').then(function (data) {
                $scope.SelectedAccount1 = data.data.Name;

            });

        }
        $scope.getRelatedAccountName = function (relatedAccountId) {
            $http.get('/api/opportunities/' + relatedAccountId + '/getAccountName').then(function (data) {
                $scope.RelatedAccountName = null;

                $scope.RelatedAccountName = data.data.Name;
                $scope.RelatedAccId = data.data.accountId;
            });

        }
        $scope.getSalesAgentName = function (SalesAgentId) {
            $http.get('/api/opportunities/' + SalesAgentId + '/getSalesAgentName').then(function (data) {
                $scope.SelectedAgent = data.data.Name;

            });

        }
        $scope.getInstallerName = function (SalesAgentId) {
            if (SalesAgentId != undefined && SalesAgentId != null) {
                $http.get('/api/opportunities/' + SalesAgentId + '/getInstallerName').then(function (data) {
                    if (data.data != null) {
                        $scope.SelectedInstaller = data.data.Name;
                    }
                });
            }
        }
        $scope.getInstalltionAddressName = function (Id) {
            if (Id != undefined && Id != null) {
                $http.get('/api/opportunities/' + Id + '/getInstalltionAddressName').then(function (data) {

                    $scope.SelectedInstallationAddress = data.data;

                    //Thi is only used in Display page. The earlier one was reset, somewhere
                    // and hard to fix now.
                    $scope.SelectedInstallationAddress2 = data.data;
                });
            }
        }

        $scope.setOpportunityAction = function (action) {
            $scope.submitAction = action;
        }

        $scope.AddOpportunity = function () {
            $scope.Opportunity = null;
            window.location = '#!/Opportunitys';
        }
        $scope.cancelOpportunity = function () {
            //$scope.Opportunity = null;
            //window.location = '#!/opportunitySearch//';

            //if ($scope.formopportunity.$setPristine()) {
            //    window.history.back();
            //    $scope.showValidate = false;
            //}
            //else {
            //    if (confirm('You will lose unsaved changes if you reload this page')) {
            //        window.history.back();
            //        $scope.showValidate = false;
            //    }
            //}

            $scope.showValidate = false;

            if (OpportunityId > 0) {
                window.location.href = '#!/OpportunityDetail/' + OpportunityId;
            } else {
                window.location.href = '#!/opportunitySearch';
            }

        }
        $scope.MeasurementEdit = function (OpportunityModel) {
            //window.location.href = '/#!/measurementEdit/' + $routeParams.OpportunityId;
            var AddressId = OpportunityModel.Addresses[0].AddressId;
            $scope.AddressService.MeasurementId = AddressId;
            window.location.href = '/#!/measurementDetail/' + $routeParams.OpportunityId + '/' + AddressId;

        }
        $scope.isSubmitted = false;

        if (document.location.href.toString().indexOf("/Opportunitys/") > 0) {
            //TO-DO
            $http.get('/api/Opportunities/0/GetOpportunityQuestionTypes').then(function (data) {
                //Project Needs in New Opportunity
                $scope.OpportunityQuestionAns = data.data;
                $scope.AllOpportunityQuestions = data.data;

                // Find Brand ID
                var BrandId = 0;
                if (data.data.length > 0)
                    BrandId = data.data[0].BrandId;

                $scope.BrandId = BrandId;

                if (data.data) {
                    var questArray = $.map(data.data, function (qa) {
                        return qa.Question;
                    });
                    if (questArray)
                        $scope.UniqueOpportunityQuestions = $.grep(questArray, function (el, index) {
                            return index == $.inArray(el, questArray);
                        });
                }
                //Added for Qualification/Question Answers
                $scope.GetQuestion = function (QAcollection, CurrentQuestion) {
                    //Get each question details based on current
                    //sub question from collection
                    if (QAcollection.length > 0 && CurrentQuestion) {
                        var sourcee = $.grep(QAcollection, function (q) {
                            return (q.Question == CurrentQuestion);
                        });

                        if (sourcee) return sourcee;
                    }
                    return null;
                };
            });
            $scope.isDisabled = false;

        }

        $scope.SaveOpportunity = function (operation) {

            $scope.CloseTextingPopup();

            if ($scope.IsBusy == true) return;
            $scope.IsBusy = true;

            //
            // TODO: why do we need this??
            $scope.submitAction = operation;

            $scope.validate = 0;
            $scope.opportunitysubmitted = true;
            $scope.SelectAccount = false;
            $scope.IsValidSalesAgent = false;
            $scope.IsValidStatus = false;
            $scope.IsValidInstallationAddress = false;
            $scope.IsValidDisposition = false;
            $scope.opportunityqa = [];

            var modalId = 'error';

            // If there is error in opportunity edit, we need to give
            // control back to the user.
            if (validatedropdowns() == false) {
                $scope.IsBusy = false;
                return;
            }
            if ($scope.formopportunity.$valid == false) {
                $scope.IsBusy = false;
                return;
            }

            var stp = $scope.Opportunity.SourcesTPId;
            if (stp == 0 || stp == undefined) {
                HFC.DisplayAlert("Source is required");
                $scope.SourcesTPId = true;
                //$("#" + modalId).modal("show");
                $scope.IsBusy = false;
                return;
            }

            // TODO: Why do we need this ??? -murugan
            //if ($scope.Opportunity.OpportunityId == 0 || $scope.Opportunity.OpportunityId == undefined) {
            //    $scope.selectedBillingAddressId = $scope.Opportunity.BillingAddress.InstallAddressId;
            //}
            var dto = $scope.Opportunity;
            // $scope.BillingAddressId = dto.BillingAddress.InstallAddressId;
            $scope.GetOpportunitiesData();

            if (dto == null || dto == undefined) {
                $scope.IsBusy = false;
                // $("#" + modalId).modal("show");
                return;
            }
            //if (dto.OpportunityName.length == 0) {
            //    $scope.IsBusy = false;
            //    //$("#" + modalId).modal("show");
            //    return;
            //}



            //if ($routeParams.OpportunityId == 0 || $routeParams.OpportunityId == undefined) {
            //    if ($scope.SelectedInstallationAddress != undefined) {
            //        dto.selectedInstallationAddress = $scope.SelectedInstallationAddress.InstallAddressId;
            //    }
            //    else {
            //        dto.selectedInstallationAddress = null;
            //    }
            //    if ($scope.SelectedInstaller)
            //    dto.SelectedInstaller = $scope.SelectedInstaller.Name;
            //    dto.SelectedStatus = $scope.SelectedStatus;
            //    dto.selectedIds = $scope.selectedIds;
            //    dto.selectedstatus = $scope.selectedstatus;
            //    $scope.SelectedAccount1 = dto.AccountId;

            //    if ($scope.SelectedAccount1 == undefined) {
            //        HFC.DisplayAlert("Accout is required");
            //        $scope.SelectAccount = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }
            //    else {
            //        //dto.AccountId = $scope.SelectedAccount1.accountId;
            //    }
            //    if ($scope.SelectedInstallationAddress == undefined) {

            //        HFC.DisplayAlert("Installation Address is required");
            //        $scope.IsValidInstallationAddress = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }
            //    else {
            //        dto.InstallationAddressId = $scope.SelectedInstallationAddress.InstallAddressId;
            //    }
            //    if ($scope.SelectedStatus == undefined) {
            //        HFC.DisplayAlert("Opportunity Status is required");
            //        $scope.IsValidStatus = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }
            //    else {
            //        dto.OpportunityStatusId = $scope.SelectedStatus;
            //        if (dto.OpportunityStatusId == 6) {

            //            if ($scope.SelectedDisposition == undefined || $scope.SelectedDisposition == null || $scope.SelectedDisposition == '') {
            //                HFC.DisplayAlert("Dispostion is required");
            //                $scope.IsValidDisposition = true;
            //                //$("#" + modalId).modal("show");
            //                $scope.IsBusy = false;
            //                return;
            //            }
            //        }

            //    }

            //    if ($scope.SelectedAgent == undefined) {
            //        HFC.DisplayAlert("Sales Agent is required");
            //        $scope.IsValidSalesAgent = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }
            //    else {
            //        dto.SalesAgentId = $scope.SelectedAgent.SalesAgentId;
            //    }

            //    if ($scope.SelectedInstaller != undefined) {
            //        dto.InstallerId = $scope.SelectedInstaller.InstallerId;

            //    }
            //    else {
            //        dto.InstallerId = 0;
            //    }

            //    if ($scope.SelectedDisposition != undefined) {

            //        dto.DispositionId = $scope.SelectedDisposition.DispositionId;
            //    }
            //    else {
            //        dto.DispositionId = 0;
            //    }

            //    if ($scope.SelectedInstallationAddress != undefined) {
            //        dto.InstallationAddress = $scope.SelectedInstallationAddress.Name;
            //    }
            //    else {
            //        dto.InstallationAddress = null;
            //    }
            //    if (dto.SourcesTPId == 0 || dto.SourcesTPId == undefined) {
            //        HFC.DisplayAlert("Source is required");
            //        $scope.SourcesTPId = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }
            //}
            //else {
            //    if (dto.AccountId == 0 || dto.AccountId == undefined) {
            //        HFC.DisplayAlert("Account is required");
            //        $scope.SelectAccount = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }

            //    if (dto.InstallationAddressId == 0 || dto.InstallationAddressId == undefined) {
            //        HFC.DisplayAlert("Installation Address is required");
            //        $scope.IsValidInstallationAddress = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }
            //    if (dto.BillingAddressId == 0 || dto.BillingAddressId == undefined) {
            //        HFC.DisplayAlert("Billing Address is required");
            //        $scope.IsValidInstallationAddress = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }

            //    if (dto.OpportunityStatusId == 0 || dto.OpportunityStatusId == undefined) {
            //        HFC.DisplayAlert("Opportunity Status is required");
            //        $scope.IsValidStatus = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }
            //    else if (dto.OpportunityStatusId == 6) {
            //        if (dto.DispositionId == 0 || dto.DispositionId == undefined) {
            //            HFC.DisplayAlert("Disposition is required");
            //            $scope.IsValidDisposition = true;
            //            $("#" + modalId).modal("show");
            //            $scope.IsBusy = false;
            //            return;
            //        }
            //    }
            //    if (dto.SalesAgentId == 0 || dto.SalesAgentId == undefined) {
            //        HFC.DisplayAlert("Sales Agent is required");
            //        $scope.IsValidSalesAgent = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }
            //    if (dto.SourcesTPId == 0 || dto.SourcesTPId == undefined) {
            //        HFC.DisplayAlert("Source is required");
            //        $scope.SourcesTPId = true;
            //        //$("#" + modalId).modal("show");
            //        $scope.IsBusy = false;
            //        return;
            //    }

            //}

            dto.InstallationAddressId = $scope.selectedInstallationAddressId;
            dto.BillingAddressId = $scope.selectedBillingAddressId;

            // TODO: why do we need to store installation address as string.
            dto.InstallationAddress = 'TODO: fix this';

            dto.opportunityqa = $scope.opportunityqa;
            dto.OpportunitySources = [];
            dto.OpportunityQuestionAns = $scope.opportunityqa;
            $scope.modalState.loaded = false;
            $http.post('/api/Opportunities', dto).then(function (response) {

                if ($routeParams.OpportunityId > 0) {
                    HFC.DisplaySuccess("Opportunity Modified");
                }
                else {
                    HFC.DisplaySuccess("Opportunity Created");
                }

                $scope.SalesAgentlist = null;
                $scope.Installerslist = null;
                $scope.InstallationAddresslist = null;
                $scope.BillingAddressList = null;

                // This can be removed.
                if ($scope.submitAction == 1) {

                } else if ($scope.submitAction == 2) {

                } else if ($scope.submitAction == 3) {

                    $scope.Opportunity = null;
                    //window.location = '#!/opportunitySearch//';
                    $scope.formopportunity.$setPristine();
                    if ($scope.IsSalesagentChanged)
                        window.location = '#!/opportunitySearch/';
                    else
                    window.location = '#!/OpportunityDetail/' + response.data.OpportunityId;
                }

                $scope.IsBusy = false;
            }, function (response) {
                $scope.modalState.loaded = true;
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });


        }
        function findDispostionValue(data, idToLookFor) {
            var categoryArray = data;
            for (var i = 0; i < categoryArray.length; i++) {
                if (categoryArray[i].DispositionId == idToLookFor) {
                    return (categoryArray[i].Name);
                }
            }
        }
        function addressToString(isSingleLine, model) {
            if (model) {

                var str = "";
                if (model.Address1)
                    str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

                if (model.City)
                    str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
                else
                    str += (model.ZipCode || "");

                return str;
            }
        };
        $scope.GetGoogleAddressHref = function (model) {
            var dest = addressToString(true, model);
            if (model = undefined) { return; }
            if (model && model.Address1) {
                var srcAddr;
                if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                    srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                } else {
                    srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                }

                if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                    return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                } else {
                    return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                }
            } else
                return null;
        }

        $scope.getBillingAddress = function (Id) {
            if (Id != undefined && Id != null) {
                $http.get('/api/opportunities/' + Id + '/getInstalltionAddressName').then(function (data) {

                    $scope.SelectedBillingAddress = data.data;
                });
            }
        }
        $scope.SetEditDispositionStatus = function () {
            if ($scope.Opportunity.OpportunityStatusId == undefined) {
                //$("#cboDisposition").attr('disabled', true);
                $(".disposition_section").hide();

                return;
            }
            var statusId = $scope.Opportunity.OpportunityStatusId;
            if (statusId == 6) {
                //$("#cboDisposition").removeAttr('disabled');
                $(".disposition_section").show();
            }
            else {
                //$("#cboDisposition").attr('disabled', true)
                $(".disposition_section").hide();
            }
        }
        $scope.SetDispositionStatus = function () {
            if ($scope.SelectedStatus == undefined) {
                //$("#cboDisposition").attr('disabled', true);
                $(".disposition_section").hide();
                return;
            }
            var statusId = $scope.SelectedStatus;
            if (statusId == 6) {
                $("#cboDisposition").removeAttr('disabled');
                $(".disposition_section").show();
            }
            else {
                //$("#cboDisposition").attr('disabled', true)
                $(".disposition_section").hide();
            }
        }

        //****************End-Tafsir***************************






            $scope.Quotes = {
                dataSource: {
                    transport: {

                        read: function (e) {
                            $http.get("/api/Quotes/" + $routeParams.OpportunityId + "/GetList").then(function (data) {
                                $scope.InstallationAddressId = data.data.Opportunities.InstallationAddressId;
                                $scope.AccountId = data.data.Opportunities.AccountId;
                                $scope.quoteDataSource = data.data.quoteList;
                                e.success(data.data.quoteList);
                            });
                        }

                    },
                    error: function (e) {

                        HFC.DisplayAlert(e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "QuoteKey",
                            fields: {

                                QuoteID: { editable: false, nullable: true },
                                PrimaryQuote: { validation: { required: true } },
                                QuoteName: { validation: { required: true } },
                                QuoteStatus: { validation: { required: false } },
                                SaleAgent: { validation: { required: false } },
                                QuoteSubTotal: { validation: { required: true } },
                                CreatedOn: { validation: { required: true } },
                                LastUpdatedOn: { validation: { required: true } },
                                OrderExists: { editable: false }

                            }
                        }
                    }
                },
                dataBound: function (e) {
                    $scope.QuoteCount = $("#gridQuoteSearch").data("kendoGrid").dataSource.data().length;
                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                },


                columns: [

                            {
                                field: "QuoteID",
                                title: "Quote ID",
                                width: "100px",
                                filterable: { multi: true, search: true },
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.QuoteID + "</span>";
                                },
                                //width: "100px",
                                //template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteID #</span></a> "

                            }
                            ,
                            {
                                field: "PrimaryQuote",
                                title: "Primary",
                                hidden: false,
                                width: "100px",
                                template:
                                    '<input type="checkbox" #= PrimaryQuote ? "checked=checked" : "" # disabled="disabled" class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched"></input>',
                                filterable: { multi: true, search: true }
                            },
                            {
                                field: "QuoteName",
                                title: "Quote Name",
                                hidden: false,
                                filterable: { multi: true, search: true },
                                template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteName #</span></a> "
                            },

                             {
                                 field: "QuoteStatus",
                                 title: "Status",
                                 //width: "100px",
                                 hidden: false

                             },
                              {
                                  field: "SalesAgent",
                                  title: "Sale Agent",
                                  hidden: false
                              },
                             {
                                 field: "QuoteSubTotal",
                                 title: "Total Amount",
                                 //width: "150px",
                                 hidden: false,
                                 template: function (dataItem) {
                                         return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.QuoteSubTotal) + "</span>";
                                 },
                                // format: "{0:c}"
                             },
                             {
                                 field: "CreatedOn",
                                 title: "Created",
                                 hidden: false,
                                 width: "100px",
                                 template: function (dataitem) {
                                     if (dataitem.CreatedOn == null)
                                         return ''
                                     else
                                         return HFCService.KendoDateFormat(dataitem.CreatedOn);
                                 }
                                     //"#=  (CreatedOn == null)? '' : kendo.toString(kendo.parseDate(CreatedOn, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                             },
                             {
                                 field: "LastUpdatedOn",
                                 title: "Modified",
                                 hidden: false,
                                 width: "100px",
                                 template: function (dataitem) {
                                     if (dataitem.LastUpdatedOn == null)
                                         return ''
                                     else
                                         return HFCService.KendoDateFormat(dataitem.LastUpdatedOn);
                                 }
                                   //  "#=  (LastUpdatedOn == null)? '' : kendo.toString(kendo.parseDate(LastUpdatedOn, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                             },
                             {
                                 field: "",
                                 title: "",
                                 width: "100px",
                                 template: function (QuoteKey) {
                                     return DropdownTemplate(QuoteKey);
                                 }
                             },


                ],
                noRecords: { template: "No records found" },
                editable: false,
                filterable: false,
                resizable: true,
                scrollable: true,
                toolbar: [{ template: kendo.template($('  <script id="toolbarTemplate" type="text/x-kendo-template"> <div class="lead_butns" ng-if="Permission.AddQuotes"> <button type="button"  onclick="return false;" class="plus_but tooltip-bottom tooltip_oportunity" data-tooltip="New Quote" ng-disabled="IsBusy" ng-click="CreateNewQuote()" aria-hidden="true"><i class="far fa-plus-square"></i></button>       </div>   </script>').html()) }]
            };





        function DropdownTemplate(QuoteKey) {
            if ($scope.NoteServiceTP.showEditNotes === true) {
                var Div = '';
                Div +=
                    '<ul > <li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right">';
                if (QuoteKey.QuoteStatusId === 1 || QuoteKey.QuoteStatusId === 2 || !QuoteKey.OrderExists) {
                    Div += ' <li><a href="javascript:void(0)" ng-click="EditquoteFromList( dataItem )">Edit Quote</a></li> ';
                }
                if (!QuoteKey.PrimaryQuote) {
                    Div += '<li><a href="javascript:void(0)" ng-click="SetPrimaryfromList(' +
                        QuoteKey.QuoteKey +
                        ')">Set Primary</a></li> ';
                }

                Div += ' <li><a href="javascript:void(0)" ng-click="CopyQuote(' +
                    QuoteKey.QuoteKey +
                    ',' +
                    QuoteKey.OpportunityId +
                    ')">Copy Quote</a></li> ';

                // move quote button section
                if (!QuoteKey.PrimaryQuote && QuoteKey.QuoteStatusId != 3 && QuoteKey.QuoteStatusId != 5) {
                    Div += '<li><a href="javascript:void(0)" ng-click="moveQuoteToOppurtunity(' +
                        QuoteKey.QuoteKey + ',' + QuoteKey.OpportunityId + ',' + QuoteKey.QuoteStatusId + ', true' +
                        ')">Move Quote</a></li> ';
                }

                //Div += ' <li><a href="javascript:void(0)" ng-click="PrintQuotee(' +
                //QuoteKey.QuoteKey +
                //')">Print Quote</a></li> ';

                Div += ' <li><a href="javascript:void(0)" ng-click="PrintQuotee_condensed(' +
                    QuoteKey.QuoteKey +
                    ')">Print Condensed Version Quote</a></li> ';

                Div += ' <li><a href="javascript:void(0)" ng-click="PrintQuotee(' +
                    QuoteKey.QuoteKey +
                    ')">Print Include Line Item Discounts Quote</a></li> ';
                Div += ' <li><a href="javascript:void(0)" ng-click="PrintQuotee_short(' +
                    QuoteKey.QuoteKey +
                    ')">Print Exclude Line Item Discounts Quote</a></li> ';

                Div += '</ul> </li> </ul>';

                return Div;
            }
            else {
                return '';
            }
        }

        //warning popup

        $scope.DisplayPopup = function () {
            //$scope.selectedQuoteKey = quoteKey;
            //$scope.selectedOpportunityId = oppId;
            $("#Warninginfo").modal("show");
            return;
        }
        //cancel the pop-up
        $scope.CancelPopup = function (modalId) {
            $("#" + modalId).modal("hide");
            $scope.moveQuoteClicked = false;
            return;
           // $scope.Quotation.QuoteStatusId = $scope.QuotationCopy.QuoteStatusId;
        };

        // expire quote move flow
        $scope.changeQuote = function () {
            if ($scope.moveQuoteClicked) {
                $scope.calendarModalService.moveQuote = window.location.href;
                window.location.href = "#!/moveQuote/" + $scope.selectedOpportunityId + "/" + $scope.selectedQuoteKey;
                $scope.moveQuoteClicked = false;
            } else {

                $http.post("/api/Quotes/" + $scope.selectedQuoteKey + "/changeExpiryToDraft").then(function (data) {
                    if (data.data === true) {
                        $scope.calendarModalService.Quote_exp_draft = true;
                        $window.location.href = "#!/Editquote/" + $scope.selectedOpportunityId + "/" + $scope.selectedQuoteKey;
                    }
                });

            }
        }

        // move Quote function
        $scope.moveQuoteToOppurtunity = function (quoteKey, opportunityId, quoteStatusId, flag) {
            // navigate to move quote page
            if (quoteStatusId == 2) {
                // get franchise date call
                $http.get("/api/timezones/0/GetFranchiseDateTime").then(function (data) {
                    var dateString = data.data;
                    $scope.date = $scope.calendarModalService.getDateStringValue(new Date(dateString), true, false, false);
                    var expireDateString;
                    if ($scope.quoteDataSource && $scope.quoteDataSource.length != 0) {
                        for (var i = 0; i < $scope.quoteDataSource.length; i++) {
                            if ($scope.quoteDataSource[i]['QuoteKey'] == quoteKey) {
                                expireDateString = $scope.quoteDataSource[i]['ExpiryDate'];
                            }
                        }
                    }
                    var expdate;

                    if (flag) {
                        expdate = new Date(expireDateString);
                    }

                    if (expdate < $scope.date) {
                        $scope.moveQuoteClicked = true;
                        $scope.DisplayPopup();
                    } else {
                        $scope.calendarModalService.moveQuote = window.location.href;
                        window.location.href = "#!/moveQuote/" + opportunityId + "/" + quoteKey;
                    }
                });
            }
            else {
                $scope.calendarModalService.moveQuote = window.location.href;
                // navigate to move quote page
                window.location.href = "#!/moveQuote/" + opportunityId + "/" + quoteKey;
            }
        }

        // redirect to quote grid page
        $scope.ClickMeToRedirectQuoteDetails = function () {

        }

        $scope.PrintQuotee = function (QuoteKey) {
            $scope.PrintService.PrintQuotee(QuoteKey);
        }

        $scope.PrintQuotee_short = function (QuoteKey) {
            $scope.PrintService.PrintQuotee_short(QuoteKey);
        }

        $scope.PrintQuotee_condensed = function (QuoteKey) {
            $scope.PrintService.PrintQuotee_condensed(QuoteKey);
        }

        $scope.EditquoteFromList = function (quoteKey) {
            //  var url = "http://" + $window.location.host + "/#!/Editquote/" + OpportunityId + "/" + quoteKey;
            $scope.QuoteinEditMode = true;
            $scope.calendarModalService.moveQuoteFlag = false;
            $scope.calendarModalService.editQuoteFlag = undefined;
            if (quoteKey.QuoteStatusId === 2) { // && quoteKey.ExpiryDate < new Date()
                $http.get("/api/timezones/0/GetFranchiseDateTime").then(function (data) {
                    var dateString = data.data;
                    $scope.date = $scope.calendarModalService.getDateStringValue(new Date(dateString), true, false, false);
                    var expdate = $scope.calendarModalService.getDateStringValue(new Date(quoteKey.ExpiryDate), true, false, false);
                    if ($scope.date.setHours(0, 0, 0, 0) > expdate.setHours(0, 0, 0, 0)) {
                        $scope.selectedOpportunityId = quoteKey.OpportunityId;
                        $scope.selectedQuoteKey = quoteKey.QuoteKey;
                        $scope.DisplayPopup();
                    }
                    else {
                        $window.location.href = "#!/Editquote/" + quoteKey.OpportunityId + "/" + quoteKey.QuoteKey;
                    }
                });
                //$scope.selectedOpportunityId = quoteKey.OpportunityId;
                //$scope.selectedQuoteKey = quoteKey.QuoteKey;
                //$scope.DisplayPopup();
            } else {
                $window.location.href = "#!/Editquote/" + quoteKey.OpportunityId + "/" + quoteKey.QuoteKey;
            }
        }

        $scope.CopyQuotePost = function (quoteKey, oppId, value) {
            $scope.loadingElement.style.display = "block";
            $http.get('/api/Quotes/' + oppId + '/CopyQuote?quoteKey=' + quoteKey + "&copyTaxExempt=" + value).then(function (response) {
                if (response.data.error == '') {
                    HFC.DisplaySuccess("New quote line created.");

                    $('#gridQuoteSearch').data('kendoGrid').dataSource.read();
                    $('#gridQuoteSearch').data('kendoGrid').refresh();
                } else {
                    HFC.DisplayAlert(response.error);
                }
                $scope.loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                $scope.loadingElement.style.display = "none";
            });

        }
       $scope.OverwriteQuoteTaxExempt = function () {
            $("#TaxExempt_Warninginfo_Copy").modal("hide");
            $scope.CopyQuotePost($scope.selectedQUoteKeyTaxExempt, $scope.SelectedOpportunityTaxExempt, true);
        }
        $scope.DoNotOverwriteTax = function () {
            $("#TaxExempt_Warninginfo_Copy").modal("hide");
            $scope.CopyQuotePost($scope.selectedQUoteKeyTaxExempt, $scope.SelectedOpportunityTaxExempt, false);
        }
        $scope.TaxExemptMsg = "";
        $scope.selectedQUoteKeyTaxExempt = 0;
        $scope.SelectedOpportunityTaxExempt = 0;
        $scope.CopyQuote = function (quoteKey, oppId) {
            $scope.selectedQUoteKeyTaxExempt = quoteKey;
            $scope.SelectedOpportunityTaxExempt = oppId;
            var oppo = $scope.Opportunity;
            var sQuote = $scope.quoteDataSource;
            var qToCopy = jQuery.grep(sQuote, function (obj) {
                return obj.QuoteKey === quoteKey;
            })[0];

            var oppotaxExempt = false, quoteTaxexempt = false;;

            if ($scope.Opportunity.IsGSTExempt === true
                || $scope.Opportunity.IsHSTExempt === true
                || $scope.Opportunity.IsPSTExempt === true
                || $scope.Opportunity.IsTaxExempt === true
                || $scope.Opportunity.IsVATExempt === true) {
                oppotaxExempt = true;
            }

            if (qToCopy.IsGSTExempt === true
                || qToCopy.IsHSTExempt === true
                || qToCopy.IsPSTExempt === true
                || qToCopy.IsTaxExempt === true
                || qToCopy.IsVATExempt === true) {
                quoteTaxexempt = true;
            }
            //var value = false;
            //var msg = "  The Account/Opportunity is marked as Tax Exempt. Would you like to mark the quote as Tax Exempt?";
            $scope.TaxExemptMsg = "";
            if (oppotaxExempt && !quoteTaxexempt) {
                $scope.TaxExemptMsg = "  The Account/Opportunity is marked as Tax Exempt. Would you like to mark the quote as Tax Exempt?";
            } else if (!oppotaxExempt && quoteTaxexempt) {
                $scope.TaxExemptMsg = "  The Opportunity is not marked as Tax Exempt. Would you like to mark the quote as Taxable?";
            }

            if ($scope.TaxExemptMsg !== '') {
                $("#TaxExempt_Warninginfo_Copy").modal("show");
            } else {
                $scope.CopyQuotePost(quoteKey, oppId, false);
            }
            //$scope.loadingElement.style.display = "block";

            //if (confirm(msg)) {
            //    //value = true;//copyOppoTaxExempt
            //    $scope.CopyQuotePost(quoteKey, oppId, true);
            //} else {
            //    //value = false;//MakeQuoteTaxable
            //    $scope.CopyQuotePost(quoteKey, oppId, false);
            //}
        }

        $scope.SetPrimaryfromList = function (quoteKey) {
            $http.get('/api/Quotes/' + quoteKey + '/ChangePrimaryQuote?opportunityId=' + $scope.Opportunity.OpportunityId).then(
                function (response) {
                    if (response.data.error === "") {
                        $('#gridQuoteSearch').data('kendoGrid').dataSource.read();
                        $('#gridQuoteSearch').data('kendoGrid').refresh();
                        HFC.DisplaySuccess("Primary quote updated")
                    } else {
                        HFC.DisplayAlert(response.data.error);
                    }

                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";

                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });

        }

        if ($scope.ReferredAccountId > 0) {

            $http.get('/api/Accounts/' + $scope.ReferredAccountId).then(function (response) {
                $scope.referredAccount = response.data.Account;
                $scope.ResetInstallationAddressEdit(response.data.Account.AccountId,1)
                $scope.Opportunity = {};
                $scope.Opportunity.AccountId = $scope.referredAccount.AccountId;
                //added when you click "create opportunity" from account opportunity grid.
                $scope.Opportunity.SourcesTPId = response.data.Account.SourcesTPId;
                $scope.SelectedStatus = 1;
                // for territory display name
                $scope.Opportunity.TerritoryType = $scope.referredAccount.TerritoryType;
                $scope.Opportunity.TerritoryDisplayId = $scope.referredAccount.TerritoryDisplayId;
                $scope.Opportunity.DisplayTerritoryName = $scope.referredAccount.DisplayTerritoryName;
                $scope.Opportunity.TerritoryId = $scope.referredAccount.TerritoryId;
                $scope.TerritoryDisplayId = $scope.referredAccount.TerritoryDisplayId;

                //Tax exempt carry from Account
                //US
                ///Make them tax exempt based on flag from opportunity tax flag in account
                if ($scope.referredAccount.IsTaxExempt == true && $scope.referredAccount.AllOpportunityTaxExempt == true) {
                    $scope.Opportunity.TaxExemptID = $scope.referredAccount.TaxExemptID;
                    $scope.Opportunity.IsTaxExempt = $scope.referredAccount.IsTaxExempt;
                }
                else if (($scope.referredAccount.IsPSTExempt == true || $scope.referredAccount.IsGSTExempt == true ||
                    $scope.referredAccount.IsHSTExempt == true || $scope.referredAccount.IsVATExempt == true) && $scope.referredAccount.AllOpportunityTaxExempt == true) {
                    $scope.Opportunity.TaxExemptID = $scope.referredAccount.TaxExemptID;
                    $scope.Opportunity.IsPSTExempt = $scope.referredAccount.IsPSTExempt;
                    $scope.Opportunity.IsGSTExempt = $scope.referredAccount.IsGSTExempt;
                    $scope.Opportunity.IsHSTExempt = $scope.referredAccount.IsHSTExempt;
                    $scope.Opportunity.IsVATExempt = $scope.referredAccount.IsVATExempt;
                }
                else {
                    $scope.Opportunity.TaxExemptID = null;
                    $scope.Opportunity.IsTaxExempt = false;
                    $scope.Opportunity.TaxExemptID = null;
                    $scope.Opportunity.IsPSTExempt = false;
                    $scope.Opportunity.IsGSTExempt = false;
                    $scope.Opportunity.IsHSTExempt = false;
                    $scope.Opportunity.IsVATExempt = false;
                }
                //---End
                $http.get('/api/FranchiseSettings/0/GetDisplayTerritoryId').then(function (data) {

                    if ($scope.Opportunity.TerritoryDisplayId > 0) {
                        $scope.TerritoryDisplayId = $scope.Opportunity.TerritoryDisplayId;
                    }
                    else {
                        if (data.data) {
                            $scope.TerritoryDisData = data.data;
                            $scope.Opportunity.TerritoryDisplayId = data.data.TerritoryId;
                            $scope.TerritoryDisplayId = data.data.TerritoryId;

                        } else {
                            $scope.Opportunity.TerritoryDisplayId = 0;
                            $scope.TerritoryDisplayId = 0;

                        }
                    }
                    //$http.get('/api/Accounts/0/getTerritoryForFranchise').then(function (response) {
                    //    if (response.data != null) {
                    //        if (!$scope.Opportunity)
                    //            $scope.Opportunity = {};

                    //        if (response.data.length > 0) {
                    //            $scope.DisplayTerritorySource = response.data;
                    //        }
                    //        else {
                    //            $scope.DisplayTerritorySource = [];
                    //        }

                    //    }
                    //    $scope.DisplayTerritorySource.splice(0, 0, { Id: 0, Name: 'Select' });
                    //});
                });

            });
        }

        // Clear tax exempt
        $scope.clearTaxExempt = function () {


            // TODO: clear the Tax except id only when all the fiels are empty
            var opt = $scope.Opportunity;
            if (!(opt.IsTaxExempt || opt.IsPSTExempt
                || opt.IsGSTExempt || opt.IsHSTExempt
                || opt.IsVATExempt)) {
                $scope.Opportunity.TaxExemptID = null;
            }
        }

        // print integration -- murugan
        $scope.printControl = {};
        $scope.printSalesPacket = function (modalid) {
            if ($routeParams.OpportunityId) $scope.opportunityIdPrint = $routeParams.OpportunityId;

            // $("#" + modalid).modal("show");
            $scope.printControl.showPrintModal();
        }

        $scope.OpportunityAdditionalAddress = function () {
            $scope.Show('Additional_Address');
        }

        $scope.SalesAgentChange = function () {

            // Running in Add mode, where we no need to check.
            if (!OpportunityId) return;

            if(HFCService.CurrentUser.PersonId != $scope.Opportunity.SalesAgentId)
            {
                DialogYesNoService.confirmDialog('If you re-assign this Opportunity to another Sales/Designer, you will no longer have access to this Opportunity. Do you wish to continue?', 'SA');
            }
        }


        $scope.External = {};

        $scope.refreshNotesGrid = function () {
            var result = $scope.External.Notes;
            $scope.NoteServiceTP.RefreshNotesGrid(result);
            //setTimeout(function () {
            //   var element = $("#checkBoxInput");
            //   if ($("#checkBoxInput").hasClass("active")) {
            //       $("#checkBoxInput").removeClass("active");
            //       $("#checkBoxInput").css({ "background-color": "#fff !important", "border": "1px solid #ffffff !important" });
            //   } else {
            //       $("#checkBoxInput").addClass("active");
            //       $("#checkBoxInput").css({ "background-color": "#3e7d8a !important", "border": "1px solid #ffffff !important" });
            //   }
            //}, 0);

        }

        $scope.refreshAdditionalContactsGrid = function () {

            var result = $scope.External.AdditionalContacts;
            $scope.AdditionalContactServiceTP.RefreshGrid(result);
        }

        $scope.refreshAdditionalAddressGrid = function () {

            var result = $scope.External.AdditionalAddress;
            $scope.AdditionalAddressServiceTP.RefreshGrid(result);
        }


        //calling this as ResetInstallationAddressEdit() loading parent tax details.
        //so made a separate call to get account tax details.
        $scope.GetTaxDetails = function (id) {

            $http.get('/api/Accounts/' + id).then(function (response) {
                $scope.AccountTaxDetails = response.data.Account;
                //Tax exempt carry from Account
                //US
                //added the line to populate source based on account selected.
                $scope.Opportunity.SourcesTPId = $scope.AccountTaxDetails.SourcesTPId;
                //end.
                //texting
                //if ($scope.ShowPopup) {
                //    $scope.AccountCellphone = response.data.Account.PrimCustomer.CellPhone;
                //    $scope.IsNotifyText = response.data.Account.IsNotifyText;
                //    if (response.data.TextStatus == null) {
                //        $scope.Opportunity.NotifyTextStatus = 'new';
                //    }
                //    else if (response.data.TextStatus.Optout == true) {
                //        $scope.Opportunity.NotifyTextStatus = 'Optout';
                //    }
                //    else if (response.data.TextStatus.Optin == true) {
                //        $scope.Opportunity.NotifyTextStatus = 'Optin';
                //    }
                //    else if (response.data.TextStatus.IsOptinmessagesent == true) {
                //        $scope.Opportunity.NotifyTextStatus = 'waiting';
                //    }
                //    else if (response.data.TextStatus.Optout == null || response.data.TextStatus.Optin == null || response.data.TextStatus.IsOptinmessagesent == false) {
                //        $scope.Opportunity.NotifyTextStatus = 'error';
                //    }
                //}
                ///Make them tax exempt based on flag from opportunity tax flag in account
                if ($scope.AccountTaxDetails.IsTaxExempt == true && $scope.AccountTaxDetails.AllOpportunityTaxExempt == true) {
                    $scope.Opportunity.TaxExemptID = $scope.AccountTaxDetails.TaxExemptID;
                    $scope.Opportunity.IsTaxExempt = $scope.AccountTaxDetails.IsTaxExempt;
                }
                else if (($scope.AccountTaxDetails.IsPSTExempt == true || $scope.AccountTaxDetails.IsGSTExempt == true ||
                    $scope.AccountTaxDetails.IsHSTExempt == true || $scope.AccountTaxDetails.IsVATExempt == true) && $scope.AccountTaxDetails.AllOpportunityTaxExempt == true) {
                    $scope.Opportunity.TaxExemptID = $scope.AccountTaxDetails.TaxExemptID;
                    $scope.Opportunity.IsPSTExempt = $scope.AccountTaxDetails.IsPSTExempt;
                    $scope.Opportunity.IsGSTExempt = $scope.AccountTaxDetails.IsGSTExempt;
                    $scope.Opportunity.IsHSTExempt = $scope.AccountTaxDetails.IsHSTExempt;
                    $scope.Opportunity.IsVATExempt = $scope.AccountTaxDetails.IsVATExempt;
                }
                else {
                    $scope.Opportunity.TaxExemptID = null;
                    $scope.Opportunity.IsTaxExempt = false;
                    $scope.Opportunity.IsPSTExempt = false;
                    $scope.Opportunity.IsGSTExempt = false;
                    $scope.Opportunity.IsHSTExempt = false;
                    $scope.Opportunity.IsVATExempt = false;
                }

                //End-----
            });
        }

        $scope.InstallerChange = function()
        {
            // Running in Add mode, where we no need to check.
            if (!OpportunityId) return;

            if (!$scope.OppFullAccess) {
                if (HFCService.CurrentUser.PersonId != $scope.Opportunity.InstallerId) {
                    DialogYesNoService.confirmDialog('If you re-assign this Opportunity to another Installer, you will no longer have access to this Opportunity. Do you wish to continue?', 'IN');

                    //confirm("If you re-assign this Opportunity to another Sales/Designer, you will no longer have access to this Opportunity. Do you wish to continue?")

                }
                else {
                    $scope.IsSalesagentChanged = false;
                }
            }
        }

        //texting
        $scope.ShowTextingPopup = function () {
            if (!$scope.ShowPopup) {
                var value = document.getElementById("ReceivedDate").value;
                //if (!Date.parse(value)) {
                //    HFC.DisplayAlert("Invalid date format");
                //    return;
                //}
                //restrict saving for invalid date
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
                var dateee = new Date(value);
                var oppCre = new Date($scope.Opportunity.CreatedOnUtc.substr(0, 16));
                oppCre.setHours(0, 0, 0, 0);
                if (dateee > oppCre) {
                    HFC.DisplayAlert("Received Date cannot be greater than the Opportunity Created Date");
                    return;
                }
            }
            if ($scope.FranciseLevelTexting && $scope.AccountCellphone != null && $scope.IsNotifyText && ($scope.Opportunity.NotifyTextStatus == 'Optout' || $scope.Opportunity.NotifyTextStatus == 'Optin') && ($scope.ShowPopup)) {
                if ($scope.formopportunity.$valid == false) {
                    $scope.IsBusy = false;
                    return;
                }
                var stp = $scope.Opportunity.SourcesTPId;
                if (stp == 0 || stp == undefined) {
                    HFC.DisplayAlert("Source is required");
                    $scope.SourcesTPId = true;
                    //$("#" + modalId).modal("show");
                    $scope.IsBusy = false;
                    return;
                }
                //restrict saving for invalid date
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
                $("#Textingpopup").modal("show");
            }
            else {
                //restrict saving for invalid date
                for (i = 0; i < $scope.ValidDatelist.length; i++) {
                    if (!$scope.ValidDatelist[i].valid) {
                        HFC.DisplayAlert("Invalid Date!");
                        return;
                    }
                }
                $scope.SaveOpportunity(3);
            }
        }
        $scope.CloseTextingPopup = function () {
            $("#Textingpopup").modal("hide");
        }
        $scope.SendMsg = function () {
            $scope.Opportunity.SendMsg = true;
            $scope.Opportunity.UncheckNotifyviaText = false;
            $scope.SaveOpportunity(3);
        }
        $scope.UncheckNotifyviaText = function () {
            $scope.Opportunity.UncheckNotifyviaText = true;
            $scope.Opportunity.SendMsg = false;
            $scope.SaveOpportunity(3);
        }

        //for date validation
        $scope.ValidateDate = function (date, id) {
            var ValidDateobj = {};
            if (!date)
                date = $("#" + id + "").val();

            if (id != "ReceivedDate" && date == "")
                $scope.ValidDate = true;
            else
                $scope.ValidDate = HFCService.ValidateDate(date, "date");

            if ($scope.ValidDatelist.find(item => item.id === id)) {
                var index = $scope.ValidDatelist.findIndex(item => item.id === id);
                $scope.ValidDatelist[index].valid = $scope.ValidDate;
            }
            else {
                ValidDateobj["id"] = id;
                ValidDateobj["valid"] = $scope.ValidDate;
                $scope.ValidDatelist.push(ValidDateobj);
            }

            if ($scope.ValidDate) {
                $("#" + id + "").removeClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).removeClass("icon_Background");
                if (id == "ReceivedDate")
                    $("#" + id + "").addClass("frm_controllead1");
            }
            else {
                $("#" + id + "").addClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).addClass("icon_Background");
                if (id == "ReceivedDate")
                    $("#" + id + "").removeClass("frm_controllead1");
            }
        }

    }
]);



