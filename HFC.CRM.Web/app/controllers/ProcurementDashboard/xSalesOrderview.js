﻿app.controller('xSalesOrderview', ['$scope', 'NavbarService', '$http', '$routeParams', 'HFCService',
    function ($scope, NavbarService, $http, $routeParams, HFCService) {

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableOperationsProcurementDashboardTab();
        $scope.HFCService = HFCService;

        $scope.PurchaseOrderId = $routeParams.PurchaseOrderId;
        HFCService.setHeaderTitle("Sales Order View #" + $scope.PurchaseOrderId);
        // header block grids configurations
        $scope.summaryDetailList =[
            {
                title: "Total Counts",
                headerList: ["Sales Order Count", "Vendor Name", "Total QTY", "Total Sales $", "Total Cost"],
                dataList: [],
                keyList: ['SalesOrderCount', 'VendorName', 'TotalQty', 'TotalSales', 'TotalCost']
            },
            
        ];

        $scope.VendorArrayList = [];
        $scope.orderIdList = [];

        // data list definition
        //$scope.detailList = [{ 'id': 1 }, { 'id': 2 }, { 'id': 3 }];

        // panel bar grid configurations
        var getGridConfiguration = function (dataList) {
            if (dataList) {
                for (var i = 0; i < dataList.length; i++) {
                    $scope.orderIdList.push(dataList[i]['OrderID']);
                    var gridConfigName = "gridConfig" + i;
                    $scope[gridConfigName] = {
                        dataSource: dataList[i].QuoteLines,
                        resizable: true,
                        columns: [{
                            field: "QuoteLineId",
                            title: "Line ID",
                            width: "150px"
                        },
                         {
                             field: "Quantity",
                             title: "QTY",
                             width: "150px"
                         },
                         {
                             field: "RoomName",
                             title: "Room Name",
                             width: "100px"
                         },
                         {
                             field: "VendorName",
                             title: "Vendor",
                             width: "120px"
                         },
                         {
                             field: "ProductName",
                             title: "Product / Model",
                             width: "120px"
                         },
                         {
                             field: "Width",
                             title: "W",
                             width: "120px",
                             template: "#= Width # #= FranctionalValueWidth #",
                         },
                         {
                             field: "Height",
                             title: "H",
                             width: "150px",
                             template: "#= Height # #= FranctionalValueHeight #"
                         },
                         {
                             field: "MountType",
                             title: "M",
                             width: "100px"
                         },
                         {

                             field: "Color",
                             title: "Color",
                             width: "100px"
                         },
                         {

                             field: "Fabric",
                             title: "Fabric",
                             width: "100px"
                         },
                         {
                             field: "ExtendedPrice",
                             title: "Extended Total",
                             width: "150px",
                             template: function (dataItem) {
                                     return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                             },
                         },
                         {
                             field: "Cost",
                             title: "Cost",
                             width: "100px",
                             template: function (dataItem) {
                                     return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</span>";
                             },
                         }, {
                             field: "QuoteId",
                             title: "Quote ID",
                             width: "100px"
                         }
                        ],
                        noRecords: {
                            template: "No records found"
                        },
                       
                        sortable: ({
                            field: "CreatedOnUtc",
                            dir: "desc"
                        })
                    };
                }
            }
            console.log("*****     ", $scope.gridConfig0);
        }

        var getHeaderDetails = function (dataItem, offset) {
            var lineItemCount = 0;
            var totalSales = 0;
            var totalCost = 0;
            var lineCount = 0;
            var vendorName = ""; var orderFlag;
            //var vendorListArray = [];
            var vendorArray = [];
            if (dataItem && dataItem.length !== 0) {
                for (var i = 0; i < dataItem.length; i++) {
                    var existFlag;
                    orderFlag = true;
                    if (dataItem[i]['QuoteLines'] && dataItem[i]['QuoteLines'].length !== 0) {
                        for (var j = 0; j < dataItem[i]['QuoteLines'].length; j++) {
                            existFlag = false;
                            if ($scope.VendorArrayList && $scope.VendorArrayList.length !== 0) {
                                for (var k = 0; k < $scope.VendorArrayList.length; k++) {
                                    if (dataItem[i]['QuoteLines'][j]['VendorId'] === $scope.VendorArrayList[k].VendorId) {
                                        existFlag = true;
                                        if ($scope.VendorArrayList[k]['OrderFlag']) {
                                            $scope.VendorArrayList[k]['SalesOrderCount'] = $scope.VendorArrayList[k]['SalesOrderCount'] + 1;
                                            $scope.VendorArrayList[k]['OrderFlag'] = false;
                                        }
                                        $scope.VendorArrayList[k].TotalQty += dataItem[i]['QuoteLines'][j].Quantity;
                                        $scope.VendorArrayList[k]['TotalSales']+= dataItem[i]['QuoteLines'][j].ExtendedPrice;
                                        $scope.VendorArrayList[k]['TotalCost']+= dataItem[i]['QuoteLines'][j].Cost;
                                     }
                                }
                            }
                            if (!existFlag) {
                                var obj = {};
                                obj['SalesOrderCount'] = 1;    
                                obj['VendorName'] = dataItem[i]['QuoteLines'][j].VendorName;
                                obj['TotalQty'] = dataItem[i]['QuoteLines'][j].Quantity;
                                obj['TotalSales'] = dataItem[i]['QuoteLines'][j].ExtendedPrice;
                                obj['TotalCost'] = dataItem[i]['QuoteLines'][j].Cost;
                                obj['VendorId'] = dataItem[i]['QuoteLines'][j].VendorId;
                                obj['OrderFlag'] = true;
                                $scope.VendorArrayList.push(obj);
                                //vendorArray.push();
                            }
                        }
                    }
                    if ($scope.VendorArrayList && $scope.VendorArrayList.length != 0) {
                        for (var g = 0; g < $scope.VendorArrayList.length; g++) {
                            $scope.VendorArrayList[g]['OrderFlag'] = false;
                        }
                    }
                  
                }
                console.log($scope.VendorArrayList);
                // setting the header data
                for (var z = 0; z < $scope.VendorArrayList.length; z++) {
                    $scope.VendorArrayList[z].TotalCost = '$' + $scope.VendorArrayList[z].TotalCost.toFixed(2);
                    $scope.VendorArrayList[z].TotalSales = '$' + $scope.VendorArrayList[z].TotalSales.toFixed(2);
                }
                $scope.summaryDetailList[offset]['dataList'] = $scope.VendorArrayList;
                console.log($scope.summaryDetailList);
                
            }
            else if (dataItem && dataItem.length === 0) {
                // setting the header data
                $scope.summaryDetailList[offset]['dataList'] = [];
            }
        }

        var getProcurementHistoryDetails = function () {
            var loading = $("#loading");
            $(loading).css("display", "block");
            $http.get('/api/PurchaseOrders/'+ $scope.PurchaseOrderId + '/GetSalesOrdersByXMpo').then(function (data) {
                var loading = $("#loading");
                $(loading).css("display", "none");
                $scope.orderHistoryDetails = data.data.data;
               
                getGridConfiguration($scope.orderHistoryDetails);
                getHeaderDetails($scope.orderHistoryDetails, 0);
            });
        }

        getProcurementHistoryDetails();

        //getGridConfiguration($scope.detailList);
        
    }]);