﻿'use strict';
app
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('ProcurementDashboardController',
    [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'NavbarService',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, NavbarService) {
            $scope.SelectVendorId = 0;
            $scope.Procurement = {
                VendorID: '',
                Id: 'Today'
            }
            HFCService.setHeaderTitle("Procurement List #");

            $scope.listProcurement = {};
            $scope.ValueList = [];
            $scope.ActiveToolTips = false;
            $scope.HFCService = HFCService;
            $scope.NavbarService = NavbarService;
            $scope.NavbarService.SelectOperation();
            $scope.NavbarService.EnableOperationsProcurementDashboardTab();
            $scope.EstimatedShipDate = "First ship date provided by Vendor";
            $scope.CostToolTips = "Est. Ship Date should match Promise By date. If the vendor sends and update the Est. Ship Date changes, tracking vendor performance";

            $scope.Permission = {};
            var ProcurementDashboardpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'ProcurementDashboard')
            var offsetvalue = 0;
            $scope.Permission.ProcurementList = ProcurementDashboardpermission.CanRead; //$scope.HFCService.GetPermissionTP('Procurement Dashboard List').CanAccess;
            $scope.Permission.ProcurementDetails = ProcurementDashboardpermission.CanRead; //$scope.HFCService.GetPermissionTP('Procurement Dashboard Details').CanAccess;
            $scope.Permission.ProcurementAdd = ProcurementDashboardpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Procurement Dashboard').CanAccess;
            $scope.Permission.Procurementedit = ProcurementDashboardpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Procurement Dashboard').CanAccess;
            function EllipseTemplate(dataitem) {
                var viewShiphtml = "";
                var viewInvoicehtml = "";
                if (dataitem.PICPO <= 0)
                    return '';
                if (dataitem.StatusId != 3 && dataitem.StatusId != 4 && dataitem.StatusId != 5)
                    return '';

                //  appointmenthtml = '<li><a href="javascript:void(0)" ng-click="SetAppointment(\'persongo\', ' + dataitem.LeadId + ')">Appointment</a></li>';
                viewShiphtml = '<li><a href="javascript:void(0)" ng-click="shipmentview(' + dataitem.PICPO + ')">View Shipment</a></li>';

                viewInvoicehtml = '<li><a href="javascript:void(0)"  ng-click="Invoiceview(' + dataitem.PICPO + ')">View Invoice</a></li>';

                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                    '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                    '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                    '<ul class="dropdown-menu pull-right">' + viewShiphtml + viewInvoicehtml +
                    '</ul> </li> </ul>';
            }

            $scope.InitialLoad = true;
            //grid code
            $scope.GetProcurementValue = function () {
                $scope.GridEditable = false;
                $scope.ProcurementlineData = {
                    dataSource: {
                        transport: {
                            read: function (e) {
                                $scope.ValueList = $scope.Valueselected();

                                if ($scope.ValueList != 0 || $scope.ValueList != null || $scope.ValueList != undefined) {
                                    $http.get('/api/Procurement/' + $scope.SelectVendorId + '/Get?listStatus=' + $scope.ValueList + '&dateFilter=' + $scope.Procurement.Id).then(function (data) {
                                        $scope.listProcurement = data.data;
                                        var _unArray = [];
                                        var dupes = [];
                                        data.data.forEach(function (item) {
                                            var isPresent = _unArray.filter(function (elem) {
                                                return (elem.PICPO === item.PICPO &&
                                                    elem.MasterPONumber === item.MasterPONumber && elem.VendorId === item.VendorId);
                                            })
                                            if (isPresent.length === 0) {
                                                _unArray.push(item)
                                            }
                                            else {
                                                dupes.push(item)
                                            }
                                        });
                                        for (var i = 0; i < _unArray.length; i++) {
                                            var obj = [];
                                            obj = dupes.filter(
                                                x => x.PurchaseOrderId === _unArray[i].PurchaseOrderId &&
                                                x.PICPO === _unArray[i].PICPO &&
                                                x.VendorId === _unArray[i].VendorId);
                                            obj.push(_unArray[i]);
                                            if (obj != null && obj != undefined && obj.length > 1) {
                                                var error = false,
                                                    backordered = false,
                                                    partialshipped = false,
                                                    shipped = false;
                                                for (var a = 0; a < obj.length; a++) {
                                                    if (obj[a].StatusId === 10) {
                                                        error = true;
                                                    } else if (obj[a].StatusId === 4) {
                                                        backordered = true;
                                                    }
                                                    else if (obj[a].StatusId === 3) {
                                                        shipped = true;
                                                    }
                                                }
                                                if (error) {
                                                    _unArray[i].VPOStatus = "Error";
                                                    _unArray[i].StatusId = 10;
                                                } else if (partialshipped) {
                                                    _unArray[i].VPOStatus = "Partial Shipped";
                                                    _unArray[i].StatusId = 4;
                                                }
                                                else if (backordered) {
                                                    _unArray[i].VPOStatus = "Back Ordered";
                                                    _unArray[i].StatusId = 4;
                                                }
                                            }
                                        }
                                        e.success(_unArray);
                                    });
                                }
                            }
                        },
                        //requestStart: function () {
                        //    if (!$scope.InitialLoad)
                        //    kendo.ui.progress($("#ProcurementboardGrid"), true);
                        //},
                        //requestEnd: function () {
                        //    if (!$scope.InitialLoad)
                        //        kendo.ui.progress($("#ProcurementboardGrid"), false);
                        //    else
                        //        $scope.InitialLoad = false;
                        //},
                        error: function (e) {
                            //kendo.ui.progress($("#ProcurementboardGrid"), false);
                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    Territory: { editable: false, nullable: true },
                                    MasterPONumber: { editable: false, type: "number" },
                                    PurchaseOrderId: { editable: false },
                                    PICPO: { editable: false, type: "number" },
                                    VendorName: { editable: false },
                                    CompanyName: { editable: false, },
                                    OrderId: { editable: false, type: "number" },
                                    OrderNumber: { editable: false }, // revision 9327
                                    OrderDate: { editable: false, type: "date" },
                                    MPODate: { editable: false, type: "date" },
                                    //Status: { editable: false },//need vpo date
                                    PromiseByDate: { editable: false, type: "date" },
                                    EstShipDate: { editable: false, type: "date" },
                                    ShippedDate: { editable: false, type: "date" },
                                    VPODate: { editable: false, type: "date" },
                                    OrderSubtotal: { type: "number" },
                                    TotalQuantity: { editable: false, type: "string", },
                                    Status: { editable: false },
                                    Cost: {type: "number", editable: false },
                                    SubmittedDate: { editable: false, type: "date" }
                                }
                            },
                            parse: function (d) {
                                $.each(d, function (idx, elem) {
                                    elem.OrderDate = kendo.parseDate(elem.OrderDate, 'yyyy-MM-dd');// kendo.parseDate(elem.OrderDate, "MM/dd/yyyy");
                                    elem.MPODate = kendo.parseDate(elem.MPODate, 'yyyy-MM-dd');
                                    elem.VPODate = kendo.parseDate(elem.VPODate, 'yyyy-MM-dd');
                                    elem.PromiseByDate = kendo.parseDate(elem.PromiseByDate, 'yyyy-MM-dd');
                                    elem.EstShipDate = kendo.parseDate(elem.EstShipDate, 'yyyy-MM-dd');
                                    elem.ShippedDate = kendo.parseDate(elem.ShippedDate, 'yyyy-MM-dd');
                                });
                                return d;
                            }
                        }
                    },
                    //group: [{ field: "MasterPONumber" }, { field: "PICPO" }],
                    //group: { field: "MasterPONumber"},
                    columns: [
                           {
                               field: "Territory",
                               title: "Territory",
                               width: "200px",
                               filterable: {
                                   //multi: true,
                                   search: true
                               }
                           },
                         {
                             field: "MasterPONumber",
                             title: "MPO / xVPO",
                             width: "100px",
                             filterable: { search: true },
                             hidden: false,
                             
                             template: function (dataItem) {
                                 
                                 if (dataItem.IsXMpo === true) {
                                     if (dataItem.VPOStatus.toUpperCase() === "ERROR") {
                                         return "<span class='Grid_Textalign'><a href='\\#!/xPurchaseMasterView/" +
                                             dataItem.PurchaseOrderId +
                                             "' style='color:#ff3400 !important; font-weight:bold;'><span style='color:#ff3400 !important; font-weight:bold;'>" +
                                             'X - ' + dataItem.MasterPONumber +
                                             "</span></a></span>";
                                     }
                                     else if (dataItem.VPOStatus.toUpperCase() === "PARTIAL SHIPPED") {
                                         return "<span class='Grid_Textalign'><a href='\\#!/xPurchaseMasterView/" +
                                           dataItem.PurchaseOrderId +
                                           "' style='color:#00aeef !important; font-weight:bold;'><span style='color:#00aeef !important; font-weight:bold;'>" +
                                           'X - ' + dataItem.MasterPONumber +
                                           "</span></a></span>";
                                     } else if (dataItem.VPOStatus.toUpperCase() === "PROCESSING") {
                                         return "<span class='Grid_Textalign'><a href='\\#!/xPurchaseMasterView/" +
                                             dataItem.PurchaseOrderId +
                                             "' style='color:#C86A00 !important; font-weight:bold;'><span style='color:#C86A00 !important; font-weight:bold;'>" +
                                             'X - ' + dataItem.MasterPONumber +
                                             "</span></a></span>";
                                     }
                                     else {
                                         return "<span class='Grid_Textalign'><a href='\\#!/xPurchaseMasterView/" +
                                             dataItem.PurchaseOrderId +
                                             "' style='color: rgb(61,125,139);'><span>" +
                                             'X - ' + dataItem.MasterPONumber +
                                             "</span></a></span>";
                                     }
                                 } else {
                                     if (dataItem.VPOStatus.toUpperCase() === "ERROR") {
                                         return "<span class='Grid_Textalign'><a href='\\#!/purchasemasterview/" +
                                             dataItem.PurchaseOrderId +
                                             "' style='color:#ff3400 !important; font-weight:bold;'><span style='color:#ff3400 !important; font-weight:bold;'>" +
                                             dataItem.MasterPONumber +
                                             "</span></a></span>";
                                     }
                                     else if (dataItem.VPOStatus.toUpperCase() === "PARTIAL SHIPPED") {
                                         return "<span class='Grid_Textalign'><a href='\\#!/purchasemasterview/" +
                                           dataItem.PurchaseOrderId +
                                           "' style='color:#00aeef !important; font-weight:bold;'><span style='color:#00aeef !important; font-weight:bold;'>" +
                                           dataItem.MasterPONumber +
                                           "</span></a></span>";
                                     } else if (dataItem.VPOStatus.toUpperCase() === "PROCESSING") {
                                         return "<span class='Grid_Textalign'><a href='\\#!/purchasemasterview/" +
                                             dataItem.PurchaseOrderId +
                                             "' style='color:#C86A00 !important; font-weight:bold;'><span style='color:#C86A00 !important; font-weight:bold;'>" +
                                             dataItem.MasterPONumber +
                                             "</span></a></span>";
                                     }
                                     else {
                                         return "<span class='Grid_Textalign'><a href='\\#!/purchasemasterview/" +
                                             dataItem.PurchaseOrderId +
                                             "' style='color: rgb(61,125,139);'><span>" +
                                             dataItem.MasterPONumber +
                                             "</span></a></span>";
                                     }
                                 }
                             },
                             //template: "<a href='\\\\#!/purchasemasterview/#= PurchaseOrderId #' style='color: rgb(61,125,139);'><span>#= MasterPONumber #</span></a> "
                         },
                        {
                            field: "PICPO",
                            //headerTemplate: '<div kendo-tooltip k-content="qwerty" >Vendor Case No</div>',
                            title: "VPO",
                            width: "100px",
                            filterable: { search: true },
                            //template: "<span class='Grid_Textalign'>#if(PICPO==0){# <span></span> #}else{# <a href='\\\\#!/purchasemasterview/#= PurchaseOrderId #' style='color: rgb(61,125,139);'><span>#= PICPO #</span></a>  #}#</span>",
                            template: function (dataItem) {
                                var mpovpo = 'purchasemasterview';
                                if (dataItem.IsXMpo === true) mpovpo = 'xPurchaseMasterView';

                                if (dataItem.PICPO == 0)
                                    return "";

                                // need to render the PICPO as url
                                return "<span class='Grid_Textalign'><a href='\\#!/" + mpovpo + "/" + dataItem.PurchaseOrderId + "' style='color: rgb(61,125,139);'><span>" + dataItem.PICPO + "</span></a></span>";
                            }
                        },
                        {
                            field: "VendorName",
                            title: "Vendor Name",
                            width: "200px",
                            hidden: false,
                            template: function (dataItem) {
                                var template = "";
                                if (dataItem.VendorName) {
                                    if (dataItem.ProductTypeId == 5) {
                                        template = "<span>" + dataItem.VendorName + "</span>";
                                    }
                                    else {
                                        template = "<a href='\\#!/vendorview/ " + dataItem.VendorId + "' style='color: rgb(61,125,139);'><span > "+dataItem.VendorName+" </span></a> ";
                                    }
                                    return template;
                                }
                                else {
                                    return '';
                                }
                            }
                            //template: "#if(ProductTypeId==5){# #: VendorName # #}else{# <a href='\\\\#!/vendorview/#= VendorId #' style='color: rgb(61,125,139);'><span >#= VendorName #</span></a> #}#",
                            //template: "<a href='\\\\#!/vendorview/#= VendorId #' style='color: rgb(61,125,139);'><span >#= VendorName #</span></a>",
                        },
                        {
                            field: "AccountName",
                            title: "Customer Info",
                            width: "200px",
                            hidden: false,
                            filterable: { search: true },
                            template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span >#=AccountName#</span></a>" + " - " + "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span >#=OpportunityName#</span></a>"
                            //template: "<a href='\\\\#!/shipnotices/#= PurchaseOrderId #' style='color: rgb(61,125,139);'><span >#= CompanyName #</span></a>",
                        },
                        {
                            field: "OrderNumber",
                            title: "Order",
                            width: "100px",
                            hidden: false,
                            template: "<span class='Grid_Textalign'><a href='\\\\#!/Orders/#= OrderID #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a></span> ",
                            //template: "<a href='\\\\#!/Orders/#= OrderNumber #' style='color: rgb(61,125,139);'><span >#= OrderNumber #</span></a>",
                            filterable: { search: true },
                        },
                        {
                            field: "OrderDate",
                            title: "Order Date",
                            width: "100px",
                            hidden: false,
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ProcurementOrderFilterCalendar", offsetvalue),
                            template: function (dataitem) {
                                if (dataitem.OrderDate == null)
                                    return ''
                                else
                                    return HFCService.KendoDateFormat(dataitem.OrderDate);
                            }
                                //"#=  (OrderDate == null)? '' : kendo.toString(kendo.parseDate(OrderDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                            //filterable: { multi: true, search: true },
                        },
                        {
                            field: "MPODate",
                            title: "PO Created Date", //"MPO Date",
                            width: "100px",
                            hidden: false,
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ProcurementMPOFilterCalendar", offsetvalue),
                            template: function (dataitem) {
                                if (dataitem.MPODate == null)
                                    return ''
                                else
                                    return HFCService.KendoDateFormat(dataitem.MPODate);
                            }
                                //"#=  (MPODate == null)? '' : kendo.toString(kendo.parseDate(MPODate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                            //filterable: { multi: true, search: true },
                        },
                         {
                             field: "SubmittedDate", //"VPODate",//Vpo Date needed
                             title: "Submitted Date", //"VPO Date",
                             type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ProcurementSubmittedFilterCalendar", offsetvalue),
                             template:function (dataitem) {
                                 if (dataitem.SubmittedDate == null)
                                     return ''
                                 else
                                     return HFCService.KendoDateFormat(dataitem.SubmittedDate);
                             },
                                 //"#=  (VPODate == null)? '' : kendo.toString(kendo.parseDate(VPODate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                             hidden: false,
                             width: "100px",

                             //filterable: { multi: true, search: true },
                         },
                         {
                             field: "PromiseByDate",
                             title: "Promise-By Date",
                             width: "100px",
                             //template: "#= ( (PromiseByDate == null)|| (PromiseByDate == '0001-01-01T00:00:00') )? '' : kendo.toString(kendo.parseDate(PromiseByDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                             hidden: false,
                             type: "date",
                             filterable: HFCService.gridfilterableDate("date", "ProcurementPromiseFilterCalendar", offsetvalue),
                             template: function (dataItem) {
                                 if (dataItem.EstShipDate != null && dataItem.PromiseByDate != null) {
                                     if (new Date(dataItem.EstShipDate) > new Date(dataItem.PromiseByDate)) {
                                         return "<span style='color:#C86A00; font-weight:bold;'>" + HFCService.KendoDateFormat(dataItem.PromiseByDate) + "</span>";
                                     } else if (dataItem.PromiseByDate != null && dataItem.PromiseByDate != undefined) {
                                         return "<span>" + HFCService.KendoDateFormat(dataItem.PromiseByDate) + "</span>";
                                     }
                                     else return "";
                                 } else if (dataItem.PromiseByDate != null && dataItem.ShippedDate != null) {
                                     if (new Date(dataItem.ShippedDate) > new Date(dataItem.PromiseByDate)) {
                                         return "<span style='color:#C86A00; font-weight:bold;'>" + HFCService.KendoDateFormat(dataItem.PromiseByDate) + "</span>";
                                     } else if (dataItem.PromiseByDate != null && dataItem.PromiseByDate != undefined) {
                                         return "<span>" + HFCService.KendoDateFormat(dataItem.PromiseByDate) + "</span>";
                                     }
                                     else return "";
                                 } else if (dataItem.PromiseByDate != null && dataItem.PromiseByDate != undefined) {
                                     return "<span>" + HFCService.KendoDateFormat(dataItem.PromiseByDate) + "</span>";
                                 }
                                 else return "";
                             }
                             //filterable: { multi: true, search: true },
                         },
                         {
                             field: "EstShipDate",
                             //title: "Est Ship Date",
                             headerTemplate: '<label title="First ship date provided by Vendor" class="report_tooltip">Est Ship Date</label>',
                             //headerTemplate: '<div kendo-tooltip k-content="EstimatedShipDate">Est Ship Date</div>',
                             //template: "#= ( (EstShipDate == null)|| (EstShipDate == '0001-01-01T00:00:00') )? '' : kendo.toString(kendo.parseDate(EstShipDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                             hidden: false,
                             width: "120px",
                             type: "date",
                             filterable: HFCService.gridfilterableDate("date", "ProcurementEstShipDateFilterCalendar", offsetvalue),
                             template: function (dataItem) {
                                 if (dataItem.EstShipDate != null && dataItem.PromiseByDate != null) {
                                     if (new Date(dataItem.EstShipDate) > new Date(dataItem.PromiseByDate)) {
                                         return "<span style='color:#C86A00; font-weight:bold;'>" + HFCService.KendoDateFormat(dataItem.EstShipDate) + "</span>";
                                     } else if (dataItem.EstShipDate != null && dataItem.EstShipDate != undefined) {
                                         return "<span>" + HFCService.KendoDateFormat(dataItem.EstShipDate) + "</span>";
                                     }
                                     else return "";
                                 } else if (dataItem.PromiseByDate != null && dataItem.ShippedDate != null) {
                                     if (new Date(dataItem.ShippedDate) > new Date(dataItem.PromiseByDate)) {
                                         if (dataItem.EstShipDate != null)
                                             return "<span style='color:#C86A00; font-weight:bold;'>" + HFCService.KendoDateFormat(dataItem.EstShipDate) + "</span>";
                                         else return "";
                                     } else if (dataItem.EstShipDate != null && dataItem.EstShipDate != undefined) {
                                         return "<span>" + HFCService.KendoDateFormat(dataItem.EstShipDate) + "</span>";
                                     }
                                     else return "";
                                 } else if (dataItem.EstShipDate != null && dataItem.EstShipDate != undefined) {
                                     return "<span>" + HFCService.KendoDateFormat(dataItem.EstShipDate) + "</span>";
                                 }
                                 else return "";
                             }
                             //filterable: { multi: true, search: true },
                         },
                        {
                            field: "ShippedDate",
                            title: "Ship Date",
                            width: "100px",
                            //template: "#= ( (ShippedDate == null)|| (ShippedDate == '0001-01-01T00:00:00') )? '' : kendo.toString(kendo.parseDate(ShippedDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                            hidden: false,
                            type: "date",
                            filterable: HFCService.gridfilterableDate("date", "ProcurementShippedFilterCalendar", offsetvalue),
                            //filterable: { multi: true, search: true }
                            template: function (dataItem) {
                                if (dataItem.EstShipDate != null && dataItem.PromiseByDate != null) {
                                    if (new Date(dataItem.EstShipDate) > new Date(dataItem.PromiseByDate)) {
                                        if (dataItem.ShippedDate != null)
                                            return "<span style='color:#C86A00; font-weight:bold;'>" + HFCService.KendoDateFormat(dataItem.ShippedDate) + "</span>";
                                        else return "";
                                    } else if (dataItem.ShippedDate != null && dataItem.ShippedDate != undefined) {
                                        return "<span>" + HFCService.KendoDateFormat(dataItem.ShippedDate) + "</span>";
                                    }
                                    else return "";
                                } else if (dataItem.PromiseByDate != null && dataItem.ShippedDate != null) {
                                    if (new Date(dataItem.ShippedDate) > new Date(dataItem.PromiseByDate)) {
                                        if (dataItem.ShippedDate != null)
                                            return "<span style='color:#C86A00; font-weight:bold;'>" + HFCService.KendoDateFormat(dataItem.ShippedDate) + "</span>";
                                        else return "";
                                    } else if (dataItem.ShippedDate != null && dataItem.ShippedDate != undefined) {
                                        return "<span>" + HFCService.KendoDateFormat(dataItem.ShippedDate) + "</span>";
                                    }
                                    else return "";
                                } else if (dataItem.ShippedDate != null && dataItem.ShippedDate != undefined) {
                                    return "<span>" + HFCService.KendoDateFormat(dataItem.ShippedDate) + "</span>";
                                }
                                else return "";
                            }
                        },
                        {
                            field: "Cost",
                            type:"number",
                            //title: "Cost",
                            headerTemplate: '<label title="Est. Ship Date should match Promise By date. If the vendor sends and update the Est. Ship Date changes, tracking vendor performance" class="report_tooltip">Cost</label>',
                            //headerTemplate: '<div kendo-tooltip k-content="CostToolTips">Cost</div>',
                            //format: "{0:c2}",
                            template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</span>";
                            },
                            width: "100px",
                            hidden: false,
                        },
                        {
                            field: "TotalQuantity",
                            title: "QTY",
                            
                            width: "100px",
                            template: function (dataItem) {
                                if (dataItem.TotalQuantity && dataItem.TotalQuantity != 0)
                                    return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.TotalQuantity, 'number') + "</span>";
                                else
                                    return "";
                            },
                            hidden: false,
                        },
                         {
                             field: "VPOStatus",
                             title: "VPO Status",
                             width: "100px",
                             hidden: false,
                             template: function (dataItem) {
                                 //if (new Date(dataItem.EstShipDate) > new Date(dataItem.PromiseByDate) || new Date(dataItem.ShippedDate) > new Date(dataItem.PromiseByDate)) {
                                 //    return "<span style='color:#C86A00; font-weight:bold;'>" +
                                 //    dataItem.VPOStatus +
                                 //    "</span>";

                                 //}
                                 //else
                                 if (dataItem.VPOStatus.toUpperCase() === "ERROR") {
                                     return "<span style='color:#ff3400; font-weight:bold;'>" +
                                         dataItem.VPOStatus +
                                         "</span>";
                                 } else if (dataItem.VPOStatus.toUpperCase() === "PROCESSING") {
                                     return "<span style='color:#C86A00; font-weight:bold;'>" +
                                         dataItem.VPOStatus +
                                         "</span>";
                                 }
                                 else if (dataItem.VPOStatus.toUpperCase() === "PARTIAL SHIPPED") {
                                     return "<span style='color:#00aeef; font-weight:bold;'>" +
                                         dataItem.VPOStatus +
                                         "</span>";
                                 }
                                     //else if (dataItem.EstShipDate != null && dataItem.PromiseByDate != null && dataItem.PromiseByDate < dataItem.EstShipDate) {
                                     //    return "<span style='color:#C86A00;'>" +
                                     //        dataItem.VPOStatus +
                                     //        "</span>";
                                     //}
                                 else if (dataItem.VPOStatus === null) {
                                     return '';
                                 } else {
                                     return dataItem.VPOStatus;
                                 }
                             },
                             filterable: { multi: true, search: true },
                         },
                         {
                             field: "",
                             title: "",
                             width: "50px",
                             hidden: false,
                             editable: false,
                             template: function (dataitem) {
                                 return EllipseTemplate(dataitem);
                             },
                         },
                    ],

                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],

                        change: function (e) {
                            var myDiv = $('.k-grid-content.k-auto-scrollable');
                            myDiv[0].scrollTop = 0;
                        }
                    },
                    toolbar: kendo.template($('<script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="prodashboardgridSearch()" type="search" id="searchBox" placeholder="Search..." class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="ProcurementSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></div></script>').html()),
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                isempty: "Empty",
                                isnotempty: "Not empty"


                            },
                            number: {
                                eq: "Equal to",
                                neq: "Not equal to",
                                gte: "Greater than or equal to",
                                lte: "Less than or equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    resizable: true,
                    sortable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    filterMenuInit: function (e) {
                        //$(e.container).find('.k-check-all').click()
                        e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                    },
                };
            };

            //popup code for view shipment
            $scope.shipmentview = function (id) {
                $scope.shipurl = "";
                var url = "";
                var list = $scope.listProcurement;
                var listSN = [];
                for (var i = 0; i < list.length; i++) {
                    if (list[i].PICPO === id) {
                        if (list[i].ListShipment != undefined && list[i].ListShipment.length > 0) {
                            for (var j = 0; j < list[i].ListShipment.length; j++) {
                                listSN.push(list[i].ListShipment[j]);
                            }
                        }
                    }
                }
                var dupes = {};
                var singles = [];
                if (listSN.length > 0) {
                    $.each(listSN, function (i, el) {
                        if (!dupes[el.ShipmentId]) {
                            dupes[el.ShipmentId] = true;
                            singles.push(el);                            
                        }
                    });
                    $scope.shipmentIds = singles;
                    //for (var j = 0; j < singles.length; j++) {
                    //    if (url === '') {                           
                    //        url =
                    //            '<a class="hquote_hover ng-binding" ng-click="navigateShipment(' +
                    //            singles[j].ShipNoticeId +
                    //            ')">' +
                    //            singles[j].ShipmentId +
                    //            '</a>'
                    //    } else {
                    //        url = url + '<span>,</span>' +
                    //            '<a class="hquote_hover ng-binding" href="javascript:void(0)" ng-click="navigateShipment(' +
                    //            singles[j].ShipNoticeId +
                    //            ')">' +
                    //            singles[j].ShipmentId +
                    //            '</a>'
                    //    }
                    //    //<a class="hquote_hover ng-binding" href="/#!/OpportunityDetail/117">Blanston ES</a>
                    //}
                }                
                $scope.shipurl = url;
                //$("#shipPopup").html($scope.shipurl);
               // $compile($scope.shipurl)($scope);
                if (id != 0) {
                    $("#ShipmentViewDetails").modal("show");
                }
            }

            $scope.navigateShipment = function (id) {
                $("#ShipmentViewDetails").modal("hide");
                $("body").removeClass("modal-open");
                window.location.href = "#!/shipnotices/" + id;
            }

            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 315);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 315);
                };
            });
            // End Kendo Resizing
            //popup code for view Invoice
            $scope.Invoiceview = function (id) {
                $scope.shipurl = "";
                var url = "";
                var list = $scope.listProcurement;
                var listSN = [];
                for (var i = 0; i < list.length; i++) {
                    if (list[i].PICPO === id) {
                        if (list[i].ListVendorInvoice != undefined && list[i].ListVendorInvoice.length > 0) {
                            for (var j = 0; j < list[i].ListVendorInvoice.length; j++) {
                                listSN.push(list[i].ListVendorInvoice[j]);
                            }
                        }
                    }
                }
                var dupes = {};
                var singles = [];
                if (listSN.length > 0) {
                    $.each(listSN, function (i, el) {
                        if (!dupes[el.InvoiceNumber]) {
                            dupes[el.InvoiceNumber] = true;
                            singles.push(el);
                        }
                    });

                    for (var j = 0; j < singles.length; j++) {
                        if (url === '') {
                            url =
                                '<a class="hquote_hover ng-binding" href="#!/vendorinvoice/' +
                                singles[j].InvoiceNumber +
                                '">' +
                                singles[j].InvoiceNumber +
                                '</a>'
                        } else {
                            url = url + '<span>,</span>' +
                                '<a class="hquote_hover ng-binding" href="#!/vendorinvoice/' +
                                singles[j].InvoiceNumber +
                                '">' +
                                singles[j].InvoiceNumber +
                                '</a>'
                        }
                        //<a class="hquote_hover ng-binding" href="/#!/OpportunityDetail/117">Blanston ES</a>
                    }
                }

                $scope.shipurl = url;

                if (id != 0) {
                    $("#InvoiceViewDetails").modal("show");
                }
            }

            //Pop up code cancel button code
            $scope.Cancel = function (modalId) {
                $("#" + modalId).modal("hide");
            };

            //search clear
            $scope.ProcurementSearchClear = function () {
                $('#searchBox').val('');
                var searchValue = $('#searchBox').val('');
                $("#ProcurementboardGrid").data('kendoGrid').dataSource.filter({
                });
            }

            //to get active vendor to drop down
            //$http.get('/api/Procurement/0/VendorNameList').then(function (data) {
            //    $scope.VendorListName = data.data;
            //});
            $scope.procurement_vendorname = function () {

                $scope.procurement_vendorname = {
                    optionLabel: {
                        Name: "All",
                        VendorID: ""
                    },
                    dataTextField: "Name",
                    dataValueField: "VendorID",
                    filter: "contains",
                    valuePrimitive: true,
                    change: function (data) {
                        var val = this.value();
                        $scope.VendorChange(val);
                    },
                    autoBind: true,
                    dataSource: {
                        transport: {
                            read: {
                                url: "/api/Procurement/0/VendorNameList",
                            }
                        }
                    },
                };
            }
            $scope.procurement_vendorname();
            //to get Dashboard Filter
            $http.get('/api/Procurement/0/Type_DateTime?Tableid=' + 4).then(function (data) {
                $scope.DateTimeDropdown = data.data;
                for (var i = 0; i < $scope.DateTimeDropdown.length; i++) {
                    if ($scope.DateTimeDropdown[i]['Name'] == 'Last 90 days (rolling)') {
                        $scope.DateRangeId = $scope.DateTimeDropdown[i]['Id'];
                        $scope.Procurement.Id = $scope.DateTimeDropdown[i]['Id'];
                        $scope.FilterChange($scope.DateTimeDropdown[i]['Id']);
                        $scope.GetProcurementValue();
                    }
                }
            })

            //on selecting drop-down value(vendor)
            $scope.VendorChange = function (id) {
                if (id === null || id === '' || id === undefined) {
                    $scope.SelectVendorId = 0;
                } else {
                    $scope.SelectVendorId = id;
                }

                $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
               // $('#ProcurementboardGrid').data('kendoGrid').refresh();                  
            }

            //on selecting drop-down value(vendor)
            $scope.FilterChange = function (id) {
                if (id === null || id === '' || id === undefined) {
                    $scope.Id = '';
                } else {
                    $scope.Id = id;
                }
                if ($('#ProcurementboardGrid').data('kendoGrid')) {
                    $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
                    
                    //$('#ProcurementboardGrid').data('kendoGrid').refresh(); 
                } 
                return;
            }

            //on clicking the checkboxes values to be passed
            $scope.Check = function () {
                $scope.ValueList = $scope.Valueselected();
                $('#ProcurementboardGrid').data('kendoGrid').dataSource.read();
               // $('#ProcurementboardGrid').data('kendoGrid').refresh(); 
               
                var myDiv = $('.k-grid-content.k-auto-scrollable');
                myDiv[0].scrollTop = 0;
                //if ($scope.ValueList != 0 || $scope.ValueList != null || $scope.ValueList != undefined) {
                //    $http.get('/api/Procurement/0/Get?listStatus=' + $scope.ValueList).then(function (data) {
                //    });
                //}
            }
            $scope.bringTooltipPopup = function () {
                $(".statusCanvas").show();
            }
            //getting the check-box value
            $scope.Valueselected = function () {
                var arr = [];
                var result = '';
                if ($('#Check1').is(":checked")) {
                    result = 0;
                    arr.push(result);
                }
                if ($('#Check2').is(":checked")) {
                    result = 2;
                    arr.push(result);
                }
                if ($('#Check3').is(":checked")) {
                    result = 3;
                    arr.push(result);
                }
                if ($('#Check4').is(":checked")) {
                    result = 4;
                    arr.push(result);
                }
                if ($('#Check5').is(":checked")) {
                    result = 10;
                    arr.push(result);
                }
                if ($('#Check6').is(":checked")) {
                    result = 1;
                    arr.push(result);
                }
                return arr;
            }

            //search box code
            $scope.prodashboardgridSearch = function () {
                var searchValue = $('#searchBox').val();

                $("#ProcurementboardGrid").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "MasterPONumber",
                          //operator: (value) => (value + "").indexOf(searchValue) >= 0,
                          operator: "eq",
                          value: searchValue
                      },
                      {
                          field: "PICPO",
                          operator: "eq",
                          value: searchValue
                      },
                        //{
                        //    field: "PurchaseOrderId",
                        //    operator: (value) => (value + "").indexOf(searchValue) >= 0,
                        //    value: searchValue
                        //},
                        {
                            field: "OrderNumber",
                            operator: (value) => (value + "").indexOf(searchValue) >= 0,
                            //operator: "eq",
                            value: searchValue
                        },
                      {
                          field: "VendorName",
                          operator: "contains",
                          value: searchValue
                      },
                    //{
                    //   field: "OpportunityName",
                    //    operator: "contains",
                    //  value: searchValue
                    //},
                        {
                            field: "AccountName",
                            operator: "contains",
                            value: searchValue
                        },
                      //{
                      //    field: "CompanyName",
                      //   operator: "contains",
                      //   value: searchValue
                      //}

                    ]
                });
            }

            $scope.ActiveHelp = function () {
                if ($scope.ActiveToolTips == true) {
                    $scope.ActiveToolTips = false;
                }
                else {
                    $scope.ActiveToolTips = true;
                }
            }
        }

    ])