﻿'use strict';
app.controller('quoteNotesController',
    [
         '$scope', '$http', 'HFCService', 'NavbarService', 'PrintService', '$routeParams','$location','$rootScope',
          function ($scope, $http, HFCService, NavbarService, PrintService, $routeParams, $location, $rootScope) {
              $scope.NavbarService = NavbarService;
              $scope.PrintService = PrintService;
              $scope.NavbarService.SelectSales();

              if ($location.absUrl().split('?')[0].includes('orderEdit')) {
                  $scope.NavbarService.EnableSalesOrdersTab();
                  HFCService.setHeaderTitle("Order #");
                  $scope.quoteKey = $rootScope.HearderNoteQuoteKey;
              }
              else {
                  $scope.NavbarService.EnableSalesOpportunitiesTab();
                  HFCService.setHeaderTitle("Quotes #");
                  $scope.quoteKey = $routeParams.quoteKey;
              }

              $scope.ActiveToolTips = false;
              // $scope.quoteKey = 0;
              // HFCService.setHeaderTitle("Quotes #");

              // $scope.quoteKey = 2154;

              $scope.quoteNoteLinesOptions = {
                  dataSource: {
                      transport: {
                          read: function (e) {
                              $http.get("/api/Quotes/" + $scope.quoteKey + "/GetQuoteLines", e).then(function (data) {
                                  e.success(data.data.quoteLines);
                              })
                          },
                          update: function (e) {
                              var dto = e.data;
                              $http.post("/api/Quotes/0/UpdateQuoteNoteLines", dto).then(function (data) {
                                  $("#QuoteNotelinesGrid").data("kendoGrid").dataSource.read();
                              })
                          },
                          parameterMap: function (options, operation) {
                              if (operation === "update") {
                                  return {
                                      QuoteLineId: options.QuoteLineId,
                                      InternalNotes: options.InternalNotes,
                                      CustomerNotes: options.CustomerNotes,
                                      VendorNotes: options.VendorNotes,
                                      QuoteKey: options.QuoteKey,
                                  };
                              }
                          }
                      },

                      error: function (e) {
                          HFC.DisplayAlert(e.errorThrown);
                      },
                      schema: {
                          model: {
                              id: "QuoteLineId",
                              fields: {
                                  QuoteLineNumber: { editable: false },
                                  QuoteLineId: { editable: false },
                                  QuoteKey: { editable: false },
                                  VendorId: { type: "number", editable: false, nullable: true },
                                  ProductId: { type: "number", editable: false, nullable: true },
                                  Description: { editable: false },
                                  ProductName: { editable: false, nullable: true },
                                  VendorName: { editable: false },
                                  ProductTypeId: { type: "number", editable: false, nullable: true },
                                  RoomName: { editable: false },

                                  InternalNotes: {
                                      editable: true, nullable: true,
                                  },
                                  CustomerNotes: { editable: true, nullable: true },
                                  VendorNotes: { editable: true, nullable: true }
                              }
                          }
                      }
                  },

                  columns: [

                       { field: "QuoteKey", hidden: true },
                       { field: "VendorId", hidden: true },
                       { field: "ProductId", hidden: true },
                       { field: "ProductTypeId", hidden: true },
                       { field: "QuoteLineId", title: "Quote Line Id",hidden:true },
                       {
                           field: "QuoteLineNumber", title: "Line ID", width: "100px",
                           template: function (dataItem) {
                               return "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                           }
                       },
                       { field: "RoomName", title: "Window", width: "100px" },
                       { field: "VendorName", title: "Vendor", width: "100px" },
                       { field: "ProductName", title: "Item", width: "100px" },
                       { field: "Description", title: "Item Description", template: "#=Description#" },
                       { field: "InternalNotes", title: "Internal Notes" },
                       { field: "CustomerNotes", title: "Customer Notes" },
                       { field: "VendorNotes", title: "Vendor Notes" },
                       //{
                       //    field: "",
                       //    title: "",
                       //    width: "60px",
                       //    template:

                       //        ' <ul><li class="dropdown note1 ad_user"><div class="btn-group"><button  class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button><ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="EditRow(dataItem)">Edit</a></li></ul></div></li></ul>'
                       //}
                        { command: ["edit"], title: "&nbsp;", width: "100px" }
                  ],
                  //pageable: {
                  //    refresh: true,
                  //    pageSize: 25,
                  //    pageSizes: [25, 50, 100, 'All'],
                  //    buttonCount: 5
                  //},
                  editable: "inline",
                  edit: function (e) {
                      //e.container.find("input[name=InternalNotes]").attr("maxlength", 200);
                      //e.container.find("input[name=CustomerNotes]").attr("maxlength", 200);
                      //e.container.find("input[name=VendorNotes]").attr("maxlength", 200);
                  },
                  //sortable: ({ field: "CreatedOn", dir: "desc" }),
                  filterable: true,
                  resizable: true,
                  //autoSync: true,
                  noRecords: { template: "No records found" },
                  scrollable: true,
              };

              $scope.EditRow = function (obj) {
                  var grid = $("#QuoteNotelinesGrid").data("kendoGrid");
                  if (obj) {
                      grid.refresh();
                      grid.editRow(obj);
                  }
              };
          }
    ])