﻿app.controller('quoteController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope'
    , 'HFCService', 'NavbarService', 'NoteServiceTP',
    'FileprintService', 'CalendarService', 'AccountService', 'PersonService', 'kendoService', 'PrintService', 'AdditionalAddressServiceTP', '$anchorScroll', '$filter', 'kendotooltipService', 'calendarModalService',
    function ($http,
        $scope,
        $routeParams,
        $window,
        $location,
        $rootScope,
        HFCService,
        NavbarService,
        NoteServiceTP,
        FileprintService,
        CalendarService,
        AccountService,
        PersonService,
        kendoService, PrintService, AdditionalAddressServiceTP, $anchorScroll, $filter, kendotooltipService, calendarModalService) {
        $scope.OpportunityId = $routeParams.OpportunityId;
        $scope.quoteKey = $routeParams.quoteKey;
        $rootScope.quoteKey = $scope.quoteKey;
        $rootScope.OpportunityId = $scope.OpportunityId;
        Opportunity = $rootScope.OpportunityId;
        $scope.HFCService = HFCService;
        $scope.QuotelineMeasurmentWithout = false;
        //TP-3480 -  While page is loading, the red text flashes in the background
        var loadingElement = document.getElementById("loading");
        loadingElement.style.display = "block";
        $scope.Loading = true;
        $scope.QuoteSubmitted = false;
        $scope.SelectedQuoteLine = {};
        $scope.InstallationAddressId = 0;
        $scope.AccountId = 0;
        $scope.CannotChangePrimary = false;
        $scope.EQL = { EmptyQuoteline: "No" };
        $scope.selectedLinesAfterSelection = [];

        $scope.NoteServiceTP = NoteServiceTP;
        $scope.FileprintService = FileprintService;
        $scope.CalendarService = CalendarService;
        $scope.AccountService = AccountService;
        $scope.PersonService = PersonService;
        $scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
        $scope.FranciseLevelTexting = $scope.HFCService.FranciseLevelTexting;
        $scope.moveQuoteClicked = false;
        $scope.calendarModalService = calendarModalService;
        $scope.rowSortFlag = false;
        $scope.rowSortOccurFlag = false;
        var offsetvalue = 0;
        if ($scope.calendarModalService.Quote_exp_draft === true) {
            $scope.QuoteinEditMode = true;
            $scope.calendarModalService.Quote_exp_draft = false;
        }
        else
            $scope.QuoteinEditMode = false;
        //Appointment
        HFCService.GetUrl();

        // getting print service
        $scope.PrintService = PrintService;

        if (!$routeParams.quoteKey) {
            $scope.quoteLineLength = 0;
            $scope.quoteLineQtyTotal = 0;
        }

        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;

        HFCService.setHeaderTitle("Quote #");

        $scope.Permission = {};
        var Quotepermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Quote')
        var ConvertOrderpermission = Quotepermission.SpecialPermission.find(x => x.PermissionCode == 'ConvertOrder').CanAccess;
        var MarginReviewSummarypermission = Quotepermission.SpecialPermission.find(x => x.PermissionCode == 'MarginReviewSummary ').CanAccess;

        $scope.Permission.DisplayQuotes = Quotepermission.CanRead;
        $anchorScroll();
        //$scope.HFCService.GetPermissionTP('Display Quotes').CanAccess;
        $scope.Permission.AddQuotes = Quotepermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Quotes').CanAccess;
        $scope.Permission.EditQuotes = Quotepermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Quotes').CanAccess;
        $scope.Permission.ListQuotes = Quotepermission.CanRead; //$scope.HFCService.GetPermissionTP('List Quotes').CanAccess;
        $scope.Permission.MarginReview = MarginReviewSummarypermission; //$scope.HFCService.GetPermissionTP('Margin Review-Quote').CanAccess;
        $anchorScroll();
        //$scope.Permission.ConvertOrder = $scope.HFCService.GetPermissionTP('Convert Order').CanAccess;
        $scope.Permission.ConvertOrder = ConvertOrderpermission; //$scope.HFCService.GetPermissionTP('Convert Quote to Order-Quote').CanAccess;

        //$scop.Permission.AddNotes = $scope.HFCService.GetPermissionTP('Add Notes&Attachments-Opportunity').CanAccess;

        $scope.Permission.AddQuoteLines = Quotepermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add QuoteLines').CanAccess;
        $scope.Permission.EditQuoteLines = Quotepermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit QuoteLines').CanAccess;
        $scope.Permission.ListQuoteLines = Quotepermission.CanRead; //$scope.HFCService.GetPermissionTP('List QuoteLines').CanAccess;

        $scope.brandid = HFCService.CurrentBrand;
        $scope.changesToPage = false;
        $scope.quoteEdited = false;
        $scope.bringQuotelineEditMode = 0;
        $scope.loadingElement = document.getElementById("loading");
        $scope.TaxTotal = 0;
        $scope.QuoteNoteLineModel = false;
        $scope.BasePriceCurrencyControl = {};
        $scope.CostPriceCurrencyControl = {};
        //$scope.formquote.quote_name.$pristine
        //Scope + FormName + nameofField to check if a field is pristine or not

        $scope.BindCursor = true;

        //edit sale date

        // $scope.ValidSale = false;
        $scope.SetSale = false;
        $scope.show = true;
        $scope.nullpreview = false;
        $scope.ValidDate = true;

        $scope.SetSaleDate = function () {
            $scope.ValidDate = true;
            $scope.SetSale = true;
        }

        $scope.UpdateSaleDate = function () {
            //restrict saving for invalid date
            if (!$scope.ValidDate) {
                HFC.DisplayAlert("Invalid Date!");
                return;
            }
            $scope.showPopUp("SaleDateWarning");
        }

        $scope.CancelSaleDate = function () {
            $scope.Quotation.SaleDate = $scope.Copy;
            $scope.CancelPopup('SaleDateWarning');
        }

        $scope.SaveSaleDate = function () {
            var value = document.getElementById("SaleDate").value;

            if (!Date.parse(value)) {
                HFC.DisplayAlert("Invalid Date!");
            }
            else {
                var dateee = new Date(value);
                var cuDate = new Date();
                var PreviousMonthDate = new Date(cuDate.setMonth(cuDate.getMonth() - 1));
                PreviousMonthDate = PreviousMonthDate.setDate(9);
                var oppCre = new Date($scope.Quotation.OpportunityCreatedOn);
                var rr = $filter('date')($scope.Quotation.OpportunityCreatedOn, HFCService.StaticKendoDateFormat());

                if (dateee > new Date()) {
                    HFC.DisplayAlert("Sale date should not exceed the current date");
                }
                else if (dateee < oppCre.setDate(oppCre.getDate() - 1) || dateee < PreviousMonthDate) {
                    if (new Date(rr) < new Date($filter('date')((new Date(new Date().setMonth(new Date().getMonth() - 1))).setDate(10), HFCService.StaticKendoDateFormat())))
                        HFC.DisplayAlert("Sale date should not be before - " + $filter('date')((new Date(new Date().setMonth(new Date().getMonth() - 1))).setDate(10), HFCService.StaticKendoDateFormat()));
                    else
                        HFC.DisplayAlert("Sale Date should not be before the created date of the opportunity - " + rr);
                }
                //else if (dateee < PreviousMonthDate) {
                //    HFC.DisplayAlert("Sale date should not before the 10th of the previous month");
                //}

                else {
                    var saledate = document.getElementById("SaleDate").value;
                    $scope.Quotation.SaleDate = saledate;
                    $http.post('/api/Quotes/' + $scope.quoteKey + '/SaveSaleDate?SaleDate=' + $scope.Quotation.SaleDate).then(function (response) {
                        if (response.data != null) {
                            $scope.SetSale = false;
                            $scope.show = false;

                            $scope.Quotation.SaleDateCreatedByName = response.data.data.SaleDateCreatedByName;
                            $scope.Quotation.SaleDateCreatedOn = response.data.data.SaleDateCreatedOn;
                            $scope.Quotation.PreviousSaleDate = response.data.data.PreviousSaleDate;
                            $scope.Quotation.SaleDate = response.data.data.SaleDate;
                            $scope.Quotation.SaleDateCreatedBy = response.data.data.SaleDateCreatedBy;
                            $scope.Copy = $scope.Quotation.SaleDate;
                            $scope.SaleDateCreatedByNameCopy = $scope.Quotation.SaleDateCreatedByName;
                            $scope.PreviousSaleDateCopy = $scope.Quotation.PreviousSaleDate;
                            $scope.SaleDateCreatedOnCopy = $scope.Quotation.SaleDateCreatedOn;
                            //$scope.QuotationCopy.SaleDateCreatedByName = $scope.Quotation.SaleDateCreatedByName;
                            //$scope.QuotationCopy.PreviousSaleDate = $scope.Quotation.PreviousSaleDate;
                            //$scope.QuotationCopy.SaleDateCreatedOn = $scope.Quotation.SaleDateCreatedOn;
                        }
                    });
                    $scope.viewCDate = true;
                }
            }
            $scope.CancelPopup('SaleDateWarning');
        }

        //added to get opportunity name and display in quote grid of opportunity
        if ($scope.OpportunityId > 0) {
            $http.get('/api/Opportunities/' + $scope.OpportunityId)
                .then(function (response) {
                    $scope.opportunityvalue = response.data.Opportunity;
                    var panelBar = $("#panelbar1").data("kendoPanelBar");
                    if (panelBar !== undefined && panelBar !== null) {
                        panelBar.expand($("#Panelopportunity"));
                    }
                });
        }

        //if ($scope.OpportunityId > 0) {
        //    $http.get('/api/Opportunities/' + $scope.OpportunityId+'/getPhoneEmailQuote')
        //    .then(function (response) {
        //        $scope.PrimaryEmailPhone = response.data;

        //        //self.Opportunity = opportunity;
        //    });
        //}

        if ($scope.OpportunityId > 0) {
            $http.get('/api/Opportunities/' + $scope.OpportunityId + '/getPhoneEmail')
                .then(function (response) {
                    $scope.emailPhone = response.data;

                    //angular.forEach($scope.emailPhone, function (value, key) {
                    //    if (value != null && value.PrimaryEmail) {
                    //        value.PrimaryEmail = 'Email: ' + value.PrimaryEmail;
                    //    }

                    //    if (value != null && value.AccountPhone) {
                    //        value.AccountPhone = 'Phone: ' + value.AccountPhone;
                    //    }

                    //})

                    //  $scope.opportunityvalue = response.data.Opportunity;

                    //self.Opportunity = opportunity;
                });
        }

        $scope.setAppointment = function (modalid, accountId) {
            var Value = $routeParams.OpportunityId;
            if (accountId == Value) {
                $http.get("/api/Quotes/" + $scope.OpportunityId + "/GetList").then(function (data) {
                    $scope.AccountId = data.data.Opportunities.AccountId;
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";

                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
            }
            var accountIdValu = "";
            if ($scope.AccountId == 0) {
                accountIdValu = accountId;
            } else {
                accountIdValu = $scope.AccountId;
            }

            $scope.calendarModalId = modalid;
            CalendarService.IsOpportunity = true;
            CalendarService.opportunityId = $routeParams.OpportunityId
            var result = AccountService.GetAppointmentData(accountIdValu, $scope.SetAppointmentCallback);
        }
        $scope.SetAppointmentCallback = function (data) {
            $scope.Account = data.Account;
            CalendarService.NewAccountsApt($scope.calendarModalId,
                $scope.Account.AccountId,
                $scope.Account.AccountNumber,
                0,
                0,
                $scope.Account.PrimCustomer,
                $scope.Account.Addresses[0],
                $routeParams.OpportunityId);
        }

        if (window.location.href.includes("Editquote")) {
            if ($scope.calendarModalService.editQuoteFlag || $scope.calendarModalService.editQuoteFlag == undefined) {
                $scope.QuoteinEditMode = true;
                getQuotelines();
            }
            $scope.calendarModalService.editQuoteFlag = undefined;
        }

        $scope.$on('$routeChangeStart', function ($event, next, current) {
            if (current.params.quoteKey === undefined)
                $scope.calendarModalService.editQuoteFlag = true;
            else {
                $scope.calendarModalService.editQuoteFlag = false;
            }
        });



        function DropdownTemplate(dataitem) {
            var Div = '';
            if ($scope.Permission.EditQuotes === true) {
                Div +=
                    '<ul> <li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right">';
                if ($scope.Permission.EditQuotes) {
                    if (dataitem.QuoteStatusId === 1 || dataitem.QuoteStatusId === 2 || !dataitem.OrderExists) {
                        Div += ' <li><a href="javascript:void(0)" ng-click="EditquoteFromList(dataItem)">Edit Quote</a></li> ';
                    }

                    if (!dataitem.PrimaryQuote) {
                        Div += ' <li><a href="javascript:void(0)" ng-click="SetPrimaryfromList(' +
                            dataitem.QuoteKey +
                            ')">Set Primary</a></li> ';
                    }
                    if ($scope.Permission.ConvertOrder && dataitem.QuoteStatusId == 3 && !dataitem.OrderExists) {
                        Div += ' <li><a href="javascript:void(0)" ng-click="ConvertQuoteOrderFromList(' +
                            dataitem.QuoteKey +
                            ')">Create Sales Order</a></li> ';
                    }
                    Div += ' <li><a href="javascript:void(0)" ng-click="CopyQuote(' +
                        dataitem.QuoteKey +
                        ',' +
                        dataitem.OpportunityId +
                        ')">Copy Quote</a></li> ';

                    // move quote button section
                    if (!dataitem.PrimaryQuote && dataitem.QuoteStatusId != 3 && dataitem.QuoteStatusId != 5) {
                        Div += '<li><a href="javascript:void(0)" ng-click="navigateMoveQuote(' +
                            dataitem.QuoteKey + ',' + dataitem.OpportunityId + ',' + dataitem.QuoteStatusId + ', true' +
                            ')">Move Quote</a></li> ';
                    }

                    Div += ' <li><a href="javascript:void(0)" ng-click="PrintQuotee_condensed(' +
                        dataitem.QuoteKey +
                        ')">Print Condensed Version Quote</a></li> ';

                    Div += ' <li><a href="javascript:void(0)" ng-click="PrintQuotee(' +
                        dataitem.QuoteKey +
                        ')">Print Include Line Item Discounts Quote</a></li> ';
                    Div += ' <li><a href="javascript:void(0)" ng-click="PrintQuotee_short(' +
                        dataitem.QuoteKey +
                        ')">Print Exclude Line Item Discounts Quote</a></li> ';

                    //<button ng-show="Permission.ConvertOrder && Quotation.QuoteStatusId == 3 && !Quotation.OrderExist" class="tooltip-bottom cancel_tpbut" type="button" data-tooltip="Create Sales Order" ng-click="ConvertQuoteOrder()"><i class="fa fa-exchange"></i></button>
                }

                Div += '</ul> </li> </ul>';

                return Div;
            }
            else {return ''}
        }

        $scope.PrintQuotee = function (QuoteKey) {
            $scope.PrintService.PrintQuotee(QuoteKey);
            //  $scope.NavbarService.printquotepreview(QuoteKey);
        }

        $scope.PrintQuotee_short = function (QuoteKey) {
            $scope.PrintService.PrintQuotee_short(QuoteKey);
        }

        $scope.PrintQuotee_condensed = function (QuoteKey) {
            $scope.PrintService.PrintQuotee_condensed(QuoteKey);
        }

        // get the data source for quote grid
        $scope.getQuoteDataSource = function (flag) {
            $http.get("/api/Quotes/" + $scope.OpportunityId + "/GetList").then(function (data) {
                $scope.InstallationAddressId = data.data.Opportunities.InstallationAddressId;
                $scope.AccountId = data.data.Opportunities.AccountId;
                // e.success(data.data.quoteList);
                $scope.quoteDataSource = data.data.quoteList;
                if (!flag) {
                    $scope.GetQuotes();
                } else {
                    var grid = $("#gridQuoteSearch").data("kendoGrid");
                    if (grid) {
                        grid.setDataSource($scope.quoteDataSource);
                    }
                }
            });
        }

        $scope.GetQuotes = function () {

            $scope.Quotes = {
                dataSource: {
                    //transport: {
                    //    read: function (e) {
                    //        $http.get("/api/Quotes/" + $scope.OpportunityId + "/GetList").then(function (data) {
                    //            $scope.InstallationAddressId = data.data.Opportunities.InstallationAddressId;
                    //            $scope.AccountId = data.data.Opportunities.AccountId;
                    //            console.log("^&^^   ", data.data);
                    //            e.success(data.data.quoteList);
                    //        }).catch(function (error) {
                    //            var loadingElement = document.getElementById("loading");
                    //            loadingElement.style.display = "none";

                    //            //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                    //        });
                    //    }
                    //},
                    //error: function (e) {
                    //    HFC.DisplayAlert(e.errorThrown);
                    //},
                    data: $scope.quoteDataSource,
                    schema: {
                        model: {
                            id: "QuoteID",
                            fields: {
                                Territory: { editable: false, nullable: true },
                                QuoteID: { type: 'number', editable: false, nullable: true },
                                PrimaryQuote: { type: "boolean", validation: { required: true } },
                                QuoteName: { validation: { required: true } },
                                QuoteStatus: { validation: { required: false } },
                                SalesAgent: { validation: { required: false } },
                                QuoteSubTotal: { type: 'number', validation: { required: true } },
                                CreatedOn: { type: 'date', validation: { required: true } },
                                LastUpdatedOn: { type: 'date', validation: { required: true } },
                                OrderExists: { editable: false }
                            }
                        }
                    }
                },
                dataBound: function (e) {
                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    if (kendotooltipService.columnWidth) {
                        kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                    } else if (window.innerWidth < 1280) {
                        kendotooltipService.restrictTooltip(null);
                    }
                },

                noRecords: { template: "No records found" },
                columns: [

                    {
                        field: "QuoteID",
                        title: "Quote ID",
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.QuoteID) {
                                return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.QuoteID + "</div><span>" + dataItem.QuoteID + "</span></span></span>";
                            } else {
                                return "";
                            }
                        }
                        //template:"<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteID #</span></a> "
                    },
                    {
                        field: "PrimaryQuote",
                        title: "Primary",
                        hidden: false,
                        width: "100px",
                        template:
                            '<input type="checkbox" #= PrimaryQuote ? "checked=checked" : "" # disabled="disabled" class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched"></input>',
                        filterable: { multi: true, search: true }
                    },
                    {
                        field: "QuoteName",
                        title: "Quote Name",
                        hidden: false,
                        template: function (dataItem) {
                            if (dataItem.QuoteName) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.QuoteName + "</div><a href='#!/quote/" + dataItem.OpportunityId + "/" + dataItem.QuoteKey + "' style='color: rgb(61,125,139);'>" + dataItem.QuoteName + "</a></span>";
                            } else {
                                return "";
                            }
                        },
                        filterable: {
                            //multi: true,
                            search: true
                        }
                    },
                    {
                        field: "QuoteStatus",
                        title: "Status",
                        width: "100px",
                        filterable: {
                            multi: true,
                            search: true
                        },
                        hidden: false,
                        template: function (dataItem) {
                            if (dataItem.QuoteID) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.QuoteStatus + "</div><span>" + dataItem.QuoteStatus + "</span></span>";
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: "Territory",
                        title: "Territory",
                        //filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.QuoteID) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Territory + "</div><span>" + dataItem.Territory + "</span></span>";
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: "SalesAgent",
                        title: "Sale Agent",
                        hidden: false,
                        template: function (dataItem) {
                            if (dataItem.QuoteID) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.SalesAgent + "</div><span>" + dataItem.SalesAgent + "</span></span>";
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: "QuoteSubTotal",
                        title: "Total Amount",
                        attributes: { class: 'text-right' },
                        width: "150px",
                        hidden: false,
                        // format: "{0:c}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.CurrencyFormat(dataItem.QuoteSubTotal) + "</div><span>" + HFCService.CurrencyFormat(dataItem.QuoteSubTotal) + "</span></span></span>";
                        }
                    },
                    {
                        field: "CreatedOn",
                        title: "Created",
                        hidden: false,
                        width: "100px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "QuoteCreatedOnFilterCalendar", offsetvalue),
                        // update the selected date in date filter
                        //value is the selected date in the datepicker
                        //navigate if input is empty
                        template: function (dataItem) {
                            if (dataItem.CreatedOn) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + "</div><span>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + "</span></span>";
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: "LastUpdatedOn",
                        title: "Modified",
                        hidden: false,
                        width: "100px",
                        type: "date",
                        filterable: HFCService.gridfilterableDate("date", "QuoteLastUpdatedOnFilterCalendar", offsetvalue),
                        // update the selected date in date filter
                        //value is the selected date in the datepicker
                        //navigate if input is empty
                        template: function (dataItem) {
                            if (dataItem.LastUpdatedOn) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.LastUpdatedOn) + "</div><span>" + HFCService.KendoDateFormat(dataItem.LastUpdatedOn) + "</span></span>";
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: "",
                        title: "",
                        width: "100px",
                        template: function (dataItem) {
                            return DropdownTemplate(dataItem);
                        }
                    }

                    //<li><a href="javascript:void(0)" >Print</a></li> Remove Quote print there is no requirment on print and email
                ],
                editable: false,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                },
                scrollable: true,
                toolbar: [
                    {
                        template: kendo.template(
                            $(
                                ' <script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="QuotegridSearch()" type="search" id="searchBox" placeholder="Search Quote" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="quoteSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>')
                                .html())
                    }
                ],
                columnResize: function (e) {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                    getUpdatedColumnList(e);
                }
            };
            $scope.Quotes = kendotooltipService.setColumnWidth($scope.Quotes);
        };

        //$scope.tooltipOptions = {
        //    filter: "th",
        //    position: "top",
        //    hide: function (e) {
        //        this.content.parent().css('visibility', 'hidden');
        //    },
        //    show: function (e) {
        //        if (this.content.text().length > 1) {
        //            this.content.parent().css('visibility', 'visible');
        //        } else {
        //            this.content.parent().css('visibility', 'hidden');
        //        }
        //    },
        //    content: function (e) {
        //        var target = e.target.data().title; // element for which the tooltip is shown
        //        return target; //$(target).text();
        //    }
        //};

        $scope.enableSortableFlag = false;
        $scope.dragFlag = false;
        $scope.setRowAsSortable = function () {
            // set draggable option to rows
            $scope.dropedQuoteLineList = [];
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if (grid) {
                //grid.hideColumn(9);
                grid.showColumn(0);
                var leftLength = (($("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child()").width()) + 50) + "px";
                if ($(".table-overlay") && $(".table-overlay").length != 0) {
                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "block");
                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("left", leftLength);
                } else {
                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable").append("<div class='table-overlay'></div>");
                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable").css("position", "relative");
                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css({ "position": "absolute", "left": leftLength, "right": "0px", "top": "0", "bottom": "0" });
                }
                $scope.enableSortableFlag = true;
                //$(".k-grid tbody tr").removeClass("disabled-sort");
                grid.table.kendoSortable({
                    filter: ">tbody >tr",
                    hint: function (element) { //customize the hint
                        var table = $('<table style="width: 92%;" class="k-grid k-widget dragged-event-placeholder"></table>'),
                            hint;
                        table.addClass("k-state-hover");
                        table.append(element.clone()); //append the dragged element
                        table.css("opacity", 0.7);

                        return table; //return the hint element
                    },
                    container: $("#QuotelinesGrid .k-grid-content.k-auto-scrollable"),
                    handler: ".draggable-element",
                    cursor: "move",
                    autoScroll: true,
                    placeholder: function (element) {
                        return $('<tr colspan="4" class="placeholder"></tr>');
                    },
                    ignore: "input",
                    start: function (e) {
                        $scope.reorderedQuoteLines = grid.dataSource.data();
                        var draggedElement = e.sender.draggedElement;
                        var subGridFlag = $(draggedElement).hasClass("k-detail-row");
                        if (!$scope.enableSortableFlag || subGridFlag) {
                            e.preventDefault();
                        } else {
                            $scope.dragFlag = true;
                            $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "none");
                            // $(".dragged-event-placeholder .draggable").css("background-color", "green");
                        }
                        var draggedElement = $(".dragged-event-placeholder");
                        if (draggedElement && draggedElement.length > 1) {
                            $(".dragged-event-placeholder").addClass("closing-lagged-element");
                        }

                    },
                    end: function (e) {
                        $scope.dragFlag = false;
                        $(".dragged-event-placeholder").addClass("closing-lagged-element");
                        $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "block");
                        // $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .draggable").css("background-color", "lightgray");
                    },
                    change: function (e) {
                        var oldRecordId = e.oldIndex;
                        var newRecordId = e.newIndex;
                        //var confirmString = "Do you wish to move the Quote line " + (oldRecordId + 1) + " to " + "Quote line " + (newRecordId + 1);
                        //// implementation of confirm box
                        //if (confirm(confirmString)) {
                        // getting the selected row uid
                        var uid = "";
                        var draggedRow = $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr:nth-child(" + (newRecordId + 1) + ")");
                        if (draggedRow && draggedRow.length != 0) {
                            uid = draggedRow[0].dataset.uid;
                            if (uid != "") {
                                $scope.dropedQuoteLineList.push(uid);
                            }
                        }
                        // $(".dragged-event-placeholder").css("display", "none");
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        //var skip = grid.dataSource.skip(),
                        var skip = 0,
                            oldIndex = e.oldIndex + skip,
                            newIndex = e.newIndex + skip,
                            data = grid.dataSource.data(),
                            dataItem = grid.dataSource.getByUid(e.item.data("uid"));

                        grid.dataSource.remove(dataItem);
                        grid.dataSource.insert(newIndex, dataItem);
                        var updatedData = grid.dataSource.data();

                        for (var i = 0; i < updatedData.length; i++) {
                            updatedData[i]['QuoteLineNumber'] = i + 1;
                        }
                        $scope.reorderedQuoteLines = updatedData;
                        //grid.setDataSource(updatedData);
                        // $scope.formquote.$setDirty();
                        $scope.rowSortFlag = true;
                        $scope.rowSortOccurFlag = true;
                        $scope.updateQuoteLineFlag = false;
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        grid.dataSource.read();
                        var tempArray = [];
                        var draggedFlag = false;
                        for (var i = 0; i < $scope.dropedQuoteLineList.length; i++) {
                            if (oldRecordId == $scope.dropedQuoteLineList[i]) {
                                draggedFlag = true;
                            } else {
                                tempArray.push($scope.dropedQuoteLineList[i]);
                            }
                        }
                        $scope.dropedQuoteLineList = tempArray;
                        for (var i = 0; i < $scope.dropedQuoteLineList.length; i++) {
                            $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table").find("[data-uid=" + $scope.dropedQuoteLineList[i] + "]").css({ "background-color": "#3d7d8b", "color": "#fff" });    //  "opacity": "0.6",
                            var record = $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table").find("[data-uid=" + $scope.dropedQuoteLineList[i] + "]");
                            $(record).find("td.draggable button").css({ "color": "#fff" });
                            $(record).find("td .validConfig").css({ "color": "#fff" });
                            $(record).find("td .hquote_hover").css({ "color": "#fff" });
                        }
                        $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "none");
                        // $scope.setRowAsSortable();
                        //} else {
                        //    $scope.rowSortOccurFlag = true;
                        //    $scope.updateQuoteLineFlag = false;
                        //    var grid = $("#QuotelinesGrid").data("kendoGrid");
                        //    grid.dataSource.read();
                        //    for (var i = 0; i < $scope.dropedQuoteLineList.length; i++) {
                        //        $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr:nth-child(" + ($scope.dropedQuoteLineList[i] + 1) + ")").css("background", "lightgreen");
                        //    }
                        //    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "none");
                        //}
                    },
                    cancel: function (e) {
                    }
                });
            }
        }


        $scope.dragAndDropFlag = false;
        // enable / disable the drag & drop behaviour
        $scope.toggleDragAndDrop = function () {
            $scope.dragAndDropFlag = !$scope.dragAndDropFlag;
            if ($scope.dragAndDropFlag) {
                $scope.setRowAsSortable();
                // collapsing all rows
                var grid = $("#QuotelinesGrid").data("kendoGrid");
                if (grid) {
                    $(".k-master-row").each(function (index) {
                        grid.collapseRow(this);
                    });
                    document.getElementById("btnCaret").style.display = "block";
                    document.getElementById("btnCaret1").style.display = "none";
                }
                $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "none");
                // for hiding the grid header options
                $("#QuotelinesGrid .k-grid-header").append("<div class='header-wrapper-overlay'></div>");
                $("#QuotelinesGrid .k-grid-header").css("position", "relative");
                $("#QuotelinesGrid .k-grid-header .header-wrapper-overlay").css({ 'position': 'absolute', 'left': '0', 'right': '0', 'top': '0', 'bottom': '0' });
                // restrict select on long press in ipad
                $("#QuotelinesGrid .k-grid-header .header-wrapper-overlay").addClass("drag-drop-enable");
                $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table").addClass("drag-drop-enable");
                $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").addClass("drag-drop-enable");
            } else {
                if ($scope.rowSortFlag) {
                    if (confirm("You will lose unsaved changes if you leave this page")) {
                        $scope.dragAndDropFlag = false;
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        grid.dataSource.read();
                        $scope.revokeSortableRow();
                        $scope.rowSortFlag = false;
                        $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "block");
                    } else {
                        $scope.dragAndDropFlag = true;
                    }
                } else {
                    var grid = $("#QuotelinesGrid").data("kendoGrid");
                    // grid.dataSource.read();
                    $scope.revokeSortableRow();
                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "block");
                }
            }
        }

        // saving rearrange quote lines
        $scope.saveRearrangeQuoteLines = function () {
            $scope.dragAndDropFlag = false;
            if ($scope.rowSortFlag) {
                var grid = $("#QuotelinesGrid").data("kendoGrid");
                if (grid) {
                    var data = grid.dataSource.data();
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "block";
                    $http.post("/api/Quotes/" + $scope.quoteKey + "/rearrangeQuotelines", data).then(function (data) {
                        $scope.rowSortOccurFlag = false;
                        $scope.rowSortFlag = false;
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        grid.dataSource.read();
                        $scope.revokeSortableRow();
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
                }
            } else {
                $scope.revokeSortableRow();
            }
            $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "block");
        }

        // cancel rearrange quote lines & make it normal mode
        $scope.cancelRearrangeQuoteLines = function () {
            if ($scope.rowSortFlag) {
                if (confirm("You will lose unsaved changes if you leave this page")) {
                    $scope.dragAndDropFlag = false;
                    var grid = $("#QuotelinesGrid").data("kendoGrid");
                    grid.dataSource.read();
                    $scope.revokeSortableRow();
                    $scope.rowSortFlag = false;
                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "block");
                }
            } else {
                $scope.dragAndDropFlag = false;
                $scope.rowSortFlag = false;
                var grid = $("#QuotelinesGrid").data("kendoGrid");
                grid.dataSource.read();
                $scope.revokeSortableRow();
                $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "block");
            }
        }

        // tracking touch devices event
        document.addEventListener('touchend', function (e) {
            setTimeout(function () {
                if ($scope.dragFlag) {
                    if ($(".dragged-event-placeholder")) {
                        $(".dragged-event-placeholder").css("display", "none");
                        $(".dragged-event-placeholder").addClass("closing-lagged-element");
                    }

                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "block");
                    // $scope.rowSortFlag = true;
                    var grid = $("#QuotelinesGrid").data("kendoGrid");
                    if (grid)
                        grid.dataSource.read();
                    $scope.dragFlag = false;
                    $scope.setRowAsSortable();
                    if ($scope.dragAndDropFlag) {
                        for (var i = 0; i < $scope.dropedQuoteLineList.length; i++) {
                            $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table").find("[data-uid=" + $scope.dropedQuoteLineList[i] + "]").css({ "background-color": "#3d7d8b", "opacity": "0.6", "color": "#fff" });
                        }
                        $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table tr td:first-child() a").css("display", "none");
                    }
                }
            }, 100);
        }, false);

        document.addEventListener('touchstart', function (e) {
            //if ($scope.qlEllipsisOpenFlag) {
            //    $($scope.qlselectedLine).removeClass("open");
            //    $scope.qlEllipsisOpenFlag = false;
            //    $scope.destroySortable();
            //    $(document).click();
            //    $($scope.selectedQuoteEllipsisBtn).blur();
            //    //$($scope.selectedQuoteEllipsisBtn).click();
            //    $scope.setRowAsSortable();
            //}
            //if ($scope.dragFlag) {
            //    $(".dragged-event-placeholder").css("display", "none");
            //    $scope.rowSortFlag = true;
            //    var grid = $("#QuotelinesGrid").data("kendoGrid");
            //    grid.dataSource.read();
            //    $scope.dragFlag = false;
            //    $scope.destroySortable();
            //    $scope.setRowAsSortable();
            //}
        }, false);

        $scope.revokeSortableRow = function () {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if (grid) {
                // enable select on long press in ipad
                $("#QuotelinesGrid .k-grid-header .header-wrapper-overlay").removeClass("drag-drop-enable");
                $("#QuotelinesGrid .k-grid-content.k-auto-scrollable table").removeClass("drag-drop-enable");
                $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").removeClass("drag-drop-enable");

                grid.hideColumn(0);
                if ($(".table-overlay") && $(".table-overlay").length != 0) {
                    $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css({ "left": "32px", "right": "60px", "display": "block" });
                }
                $("#QuotelinesGrid .k-grid-header .header-wrapper-overlay").remove();
            }
            $scope.formquote.$dirty = false;
            $scope.enableSortableFlag = false;
            $scope.rowSortFlag = false;
        }

        $scope.destroySortable = function () {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if (grid && grid.table.data("kendoSortable")) {
                grid.table.data("kendoSortable").destroy();
            }
        }

        var getUpdatedColumnList = function (e) {
            kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
        }

        $scope.QuotegridSearch = function () {
            var searchValue = $('#searchBox').val();
            var gridData = $("#gridQuoteSearch").data("kendoGrid");
            var currFilterObj = gridData.dataSource.filter();

            $("#gridQuoteSearch").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                    {
                        field: "QuoteName",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "QuoteStatus",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "SalesAgent",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "QuoteSubTotal",
                        operator: "eq",
                        value: searchValue
                    },
                    //{
                    //    field: "CreatedOn",
                    //    operator: "contains",
                    //    value: searchValue
                    //},
                    //{
                    //    field: "LastUpdatedOn",
                    //    operator: "contains",
                    //    value: searchValue
                    //},
                    {
                        field: "PrimaryQuote",
                        operator: (value) => (value + "").indexOf(searchValue) >= 0,
                        value: searchValue
                    },
                ]
            });
        };

        $scope.quoteSearchClear = function () {
            $('#searchBox').val('');
            $("#gridQuoteSearch").data('kendoGrid').dataSource.filter({
            });
        };

        $scope.NavbarService = NavbarService;

        $scope.NavbarService.SelectSales();

        var ul = $location.absUrl().split('#!')[1];
        if (ul.includes('quoteSearch')) {
            $scope.NavbarService.EnableSalesOpportunitiesTab();
        } else if (ul.includes('quote')) {
            $scope.NavbarService.EnableSalesOpportunitiesTab();
        } else if (ul.includes('moveQuote')) {
            $scope.NavbarService.EnableSalesOpportunitiesTab();
        }

        $scope.EditQuote = function () {
            if ($scope.Quotation.SaleDate != null && $scope.Quotation.QuoteStatusId == 3) {
                $scope.SetSale = false;
                $scope.ValidSale = true;
            }
            var qk = $scope.quoteKey;
            //$scope.EditquoteFromList(qk);
            $scope.PrevExpiryDate = $scope.Quotation.ExpiryDate;
            $scope.PrevStatus = $scope.Quotation.QuoteStatusId;
            if ($scope.Quotation.QuoteStatusId == 2) {
                //  $scope.date = new Date();
                //  var expdate = new Date($scope.Quotation.ExpiryDate);

                $http.get("/api/timezones/0/GetFranchiseDateTime").then(function (data) {
                    var dateString = data.data;
                    $scope.date = $scope.calendarModalService.getDateStringValue(new Date(dateString), true, false, false);
                    var expdate = $scope.calendarModalService.getDateStringValue(new Date($scope.Quotation.ExpiryDate), true, false, false);
                    if ($scope.date.setHours(0, 0, 0, 0) > expdate.setHours(0, 0, 0, 0)) {
                        $scope.DisplayPopup();
                    }
                    else {
                        $scope.QuoteinEditMode = true;
                        getQuotelines();
                    }
                });
            }
            else {
                $scope.QuoteinEditMode = true;
                getQuotelines();
            }

            $anchorScroll();

            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if ($scope.brandid === 1) {
                if (grid != undefined) {
                    $(".k-master-row").each(function (index) {
                        grid.collapseRow(this);
                    });
                    if (document.getElementById("btnCaret"))
                        document.getElementById("btnCaret").style.display = "block";
                    if (document.getElementById("btnCaret1"))
                        document.getElementById("btnCaret1").style.display = "none";
                }
            }
            // enable sortable row
            //  $scope.setRowAsSortable();
        }
        $scope.edit_exip_quote = false;
        $scope.changeQuote = function () {
            if ($scope.moveQuoteClicked) {
                $scope.calendarModalService.moveQuote = window.location.href;
                window.location.href = "#!/moveQuote/" + $scope.Quotation.OpportunityId + "/" + $scope.selectedQuoteKeyBackup;
                $scope.moveQuoteClicked = false;
            } else {

                if (window.location.href.includes('/quote/') || window.location.href.includes('/Editquote/')) {
                    $scope.Quotation.QuoteStatusId = 1;
                    $scope.formquote.$dirty = true;
                    $scope.SaveQuote();
                    $scope.QuoteinEditMode = true;
                    $scope.calendarModalService.Quote_exp_draft = true;
                }
                else {

                    $http.post("/api/Quotes/" + $scope.selectedQuoteKey + "/changeExpiryToDraft").then(function (data) {
                        if (data.data === true) {
                            $scope.calendarModalService.Quote_exp_draft = true;
                            $window.location.href = "#!/Editquote/" + $scope.selectedOpportunityId + "/" + $scope.selectedQuoteKey;
                        }
                    });
                }


            }
        }

        // $scope.GetQuotes();
        var url = window.location.href;
        if (!(url.includes("moveQuote"))) {
            $scope.getQuoteDataSource();
        }
        //$scope.selectMeasurementsForQuoteLine = function () {
        //    selectMeasurements("quotemodal");
        //}

        // for adding new row in the quote line item grid
        $scope.addNewRow = function () {
            $scope.EQL.EmptyQuoteline = 'Yes';
            $scope.SaveMeasurementQuoteLine($scope.Quotation);
        }


        $scope.selectMeasurements = function (modalId) {
            $http.get('/api/Measurement/' + $scope.Quotation.InstallationAddressId + '/Get').then(function (data) {
                //var modalId = "quotemodal";
                var measurment = data.data;
                var arrMes = [];
                var arrqlines = [];
                var grid = $("#QuotelinesGrid").data("kendoGrid");
                if (grid != undefined) {
                    var qlines = grid.dataSource.data()
                    //var qlines = $scope.QuoteLinesList;
                    for (var j = 0; j < measurment.length; j++) {
                        measurment[j].Selected = false;
                        measurment[j].quoteLineCreated = false;
                    }

                    if (qlines !== undefined) {
                        for (var i = 0; i < qlines.length; i++) {
                            for (var j = 0; j < measurment.length; j++) {
                                //measurment[j].Selected = false;

                                if (qlines[i].MeasurementsetId !== 0 && qlines[i].MeasurementsetId !== '') {
                                    arrMes.push(measurment[j].id);
                                }
                                if (qlines[i].MeasurementsetId !== 0 &&
                                    qlines[i].MeasurementsetId !== undefined &&
                                    qlines[i].MeasurementsetId !== '') {
                                    arrqlines.push(qlines[i].MeasurementsetId);
                                }

                                if (qlines[i].MeasurementsetId === measurment[j].id) {
                                    measurment[j].Selected = true;
                                    measurment[j].quoteLineCreated = true;
                                }
                            }
                        }
                        $scope.IsAllChecked = false;
                        $scope.Quotation.MeausurementList = measurment;
                        arrqlines = arrqlines.filter(function (elem, index, self) {
                            return index === self.indexOf(elem);
                        });
                        arrMes = arrMes.filter(function (elem, index, self) {
                            return index === self.indexOf(elem);
                        })
                        if (arrqlines.length > 0) {
                            $scope.IsAllChecked = arraysEqual(arrMes, arrqlines);
                        }
                    }
                }
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });
            $scope.showPopUp(modalId);
        }

        function arraysEqual(arr1, arr2) {
            if (arr1.length !== arr2.length)
                return false;
            for (var i = arr1.length; i--;) {
                if (arr1[i] !== arr2[i])
                    return false;
            }

            return true;
        }

        $scope.showPopUp = function (modalId) {
            $("#" + modalId).modal("show");
        }
        $scope.Cancel = function (modalId) {
            $("#" + modalId).modal("hide");
            var myDiv = $('.margin_summary');
            myDiv[0].scrollTop = 0;
            var myDivq = $('.quotemeasurement_popup');
            myDivq[0].scrollTop = 0;
        };
        $scope.EditquoteFromList = function (quoteKey) {
            //  var url = "http://" + $window.location.host + "/#!/Editquote/" + $scope.OpportunityId + "/" + quoteKey;
            $scope.QuoteinEditMode = true;
            $scope.calendarModalService.moveQuoteFlag = false;
            $scope.calendarModalService.editQuoteFlag = undefined;

            if (quoteKey.QuoteStatusId === 2) { // && quoteKey.ExpiryDate < new Date()
                $http.get("/api/timezones/0/GetFranchiseDateTime").then(function (data) {
                    var dateString = data.data;
                    $scope.date = $scope.calendarModalService.getDateStringValue(new Date(dateString), true, false, false);
                    var expdate = $scope.calendarModalService.getDateStringValue(new Date(quoteKey.ExpiryDate), true, false, false);
                    if ($scope.date.setHours(0, 0, 0, 0) > expdate.setHours(0, 0, 0, 0)) {
                        $scope.selectedOpportunityId = quoteKey.OpportunityId;
                        $scope.selectedQuoteKey = quoteKey.QuoteKey;
                        $scope.DisplayPopup();
                    }
                    else {
                        $window.location.href = "#!/Editquote/" + quoteKey.OpportunityId + "/" + quoteKey.QuoteKey;
                    }
                });
            } else {
                $window.location.href = "#!/Editquote/" + quoteKey.OpportunityId + "/" + quoteKey.QuoteKey;
            }
            // $window.location.href = "#!/Editquote/" + $scope.OpportunityId + "/" + quoteKey;;
        }
        $scope.Newquotepage = function () {
            $http.get('/api/Opportunities/' + $scope.OpportunityId).then(function (data) {
                var opportunity = data.data.Opportunity;
                if (opportunity.InstallationAddressId) {
                    $http.get('/api/opportunities/' + opportunity.InstallationAddressId + '/getInstalltionAddressName')
                        .then(function (data) {
                            $scope.IsValidate = data.data.IsValidated;
                            if ($scope.IsValidate == false) {
                                var errorMessage =
                                    'The Installation Address is not validated, do you still wish to proceed?';
                                if (confirm(errorMessage)) {
                                    $scope.NewQuote();
                                } else {
                                    $scope.IsBusy = false;
                                    $window.location.href = "#!/OpportunityDetail/" + $scope.OpportunityId;;
                                }
                            } else
                                $scope.NewQuote();
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";

                            //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                        });
                }
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });
        };

        $scope.NewQuote = function () {
            if ($scope.Quotation.QuoteKey == 0) {
                // var url = "http://" + $window.location.host + "/#!/quote/" + $scope.OpportunityId;
                $window.location.href = "#!/quote/" + $scope.OpportunityId;;
            } else {
                //var url = "http://" +
                //    $window.location.host +
                //    "/#!/quote/" +
                //    $scope.OpportunityId +
                //    "/" +
                //    $scope.Quotation.QuoteKey;
                $window.location.href = "#!/quote/" + $scope.OpportunityId + "/" + $scope.Quotation.QuoteKey;;
            }
        }
        $scope.ClickMeToRedirectQuoteReview = function (index) {
            // var url = "http://" + $window.location.host + "/#!/quoteReview/" + $scope.quoteKey;
            $window.location.href = "#!/quoteReview/" + $scope.quoteKey;;
        };

        $scope.selectedMeasurementForAccount = [];

        $scope.LinkMeasurementToAccount = function () {
            $http.post('/api/Measurement/' + $scope.quoteKey + '/SaveMeasurementsFromQuoteLineToAccount', $scope.selectedMeasurementForAccount)
                .then(function (data) {
                    $scope.selectedMeasurementForAccount = [];
                    if (data.data != false && data.data) {
                        $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                        $('#QuotelinesGrid').data('kendoGrid').refresh();

                        // HFC.DisplayAlert("Measurements successfully saved to Account: " + $scope.Quotation.AccountName + " Installation Address: " + $scope.Quotation.InstallationAddress.Address1 + " " + $scope.Quotation.InstallationAddress.Address2 + " " + $scope.Quotation.InstallationAddress.City + " " + $scope.Quotation.InstallationAddress.State + " " + $scope.Quotation.InstallationAddress.ZipCode);
                        alert("Measurements successfully saved to Account: " + $scope.Quotation.AccountName + " Installation Address: " + $scope.Quotation.InstallationAddress.Address1 + " " + $scope.Quotation.InstallationAddress.Address2 + " " + $scope.Quotation.InstallationAddress.City + " " + $scope.Quotation.InstallationAddress.State + " " + $scope.Quotation.InstallationAddress.ZipCode);
                    }
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                });
        }
        $scope.selectedquotelinesFortaxexempt = [];
        $scope.hideTaxExemptIcons = true;
        $scope.getSelectClick = function (dataItem, $event) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if ($("#select_" + dataItem.QuoteLineNumber)[0].checked) {
                //if ($scope.selectedquotelinesFortaxexempt.filter(item => item.LineNumber == dataItem.QuoteLineNumber).length != 0) {
                var valExists = false;
                for (var i = $scope.selectedquotelinesFortaxexempt.length - 1; i >= 0; i--) {
                    if ($scope.selectedquotelinesFortaxexempt[i].LineNumber == dataItem.QuoteLineNumber) {
                        valExists = true;
                    }
                }
                if (valExists === false) {
                    $scope.selectedquotelinesFortaxexempt.push(
                        {
                            "LineNumber": dataItem.QuoteLineNumber,
                            "QuoteLineId": dataItem.QuoteLineId
                        });
                }
            }
            else {
                for (var i = $scope.selectedquotelinesFortaxexempt.length - 1; i >= 0; i--) {
                    if ($scope.selectedquotelinesFortaxexempt[i].LineNumber == dataItem.QuoteLineNumber) {
                        $scope.selectedquotelinesFortaxexempt.splice(i, 1);
                    }
                }
            }
            if ($scope.selectedquotelinesFortaxexempt.length > 0) {
                $scope.hideTaxExemptIcons = false;
                //if (grid) {
                //    if ($scope.brandid === 1) {
                //        grid.hideColumn(23);
                //    } else {
                //        grid.hideColumn(15);
                //    }

                //}
            } else {
                $scope.hideTaxExemptIcons = true;
                //if (grid) {
                //    if ($scope.brandid === 1) {
                //        grid.showColumn(23);
                //    }
                //    else {
                //        grid.showColumn(15);
                //    }
                //}
            }

            if ($scope.selectedquotelinesFortaxexempt.length !== 0 && $scope.selectedquotelinesFortaxexempt.length === grid.dataSource.data().length) {
                $('#SelectAll_Chkbox').prop('checked', true);
            } else {
                $('#SelectAll_Chkbox').prop('checked', false);
            }
        }
        $scope.SelectAll = false;
        $scope.SelectAllLineItems = function () {
            var griddata = $("#QuotelinesGrid").data("kendoGrid").dataSource.data();
            var chkbx = $("#SelectAll_Chkbox");
            var res = $scope.SelectAll;
            $scope.selectedquotelinesFortaxexempt = [];
            if (chkbx[0].checked) {
                for (var i = 0; i < griddata.length; i++) {
                    var temp = i + 1;
                    //$scope."select_" + temp=true;
                    $scope.selectedquotelinesFortaxexempt.push(
                        {
                            "LineNumber": griddata[i].QuoteLineNumber,
                            "QuoteLineId": griddata[i].QuoteLineId
                        });
                    $('.Select_Checkbox').prop('checked', true);
                }
                $(".Select_Checkbox").prop('checked', true);
            } else {
                $scope.selectedquotelinesFortaxexempt = [];
                $(".Select_Checkbox").prop('checked', false);
            }
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if ($scope.selectedquotelinesFortaxexempt.length > 0) {
                $scope.hideTaxExemptIcons = false;
                //if (grid) {
                //    if ($scope.brandid == 1) {
                //        grid.hideColumn(23);
                //    } else {
                //        grid.hideColumn(15);
                //    }

                //}
            } else {
                $scope.hideTaxExemptIcons = true;
                //if (grid) {
                //    if ($scope.brandid == 1) {
                //        grid.showColumn(23);
                //    } else {
                //        grid.showColumn(15);
                //    }
                //}
            }
        }
        $scope.getClick = function (dataItem, $event) {
            if ($("#" + dataItem.QuoteLineNumber)[0].checked) {
                $scope.selectedMeasurementForAccount.push(
                    {
                        "id": dataItem.QuoteLineNumber,
                        "RoomLocation": dataItem.RoomLocation,
                        "RoomName": dataItem.RoomName,
                        "WindowLocation": dataItem.WindowLocation,
                        "MountTypeName": dataItem.MountType,
                        "fileName": null,
                        "Stream_id": null,
                        "Width": dataItem.Width,
                        "FranctionalValueWidth": dataItem.FranctionalValueWidth,
                        "Height": dataItem.Height,
                        "FranctionalValueHeight": dataItem.FranctionalValueHeight,
                        "Comments": dataItem.Comments,
                        "QuoteLineId": dataItem.QuoteLineId
                    });
            }
            else {
                for (var i = $scope.selectedMeasurementForAccount.length - 1; i >= 0; i--) {
                    if ($scope.selectedMeasurementForAccount[i].id == dataItem.QuoteLineNumber) {
                        $scope.selectedMeasurementForAccount.splice(i, 1);
                    }
                }
            }
        }

        //Initialize the Quote Object

        $scope.Quotation = {
            QuoteKey: 0,
            QuoteID: 0,
            OpportunityId: 0,
            AccountId: 0,
            InstallationAddressId: 0,
            MeasurementId: '',
            OpportunityName: '',
            Address1: '',
            Address2: '',
            City: '',
            State: '',
            ZipCode: '',
            AccountName: '',
            QuoteName: '',
            PrimaryQuote: false,
            SalesAgent: '',
            QuoteStatus: '',
            QuoteStatusId: 1,
            TotalCharges: 0,
            Discount: 0,
            TotalDiscounts: 0,
            NetCharges: 0,
            CreatedOn: 0,
            CreatedBy: '',
            LastUpdatedOn: 0,
            LastUpdatedBy: '',
            ExpiryDate: 0,
            QuoteLinesList: {},
            MeausurementList: {},
            TotalMargin: 0,
            QuoteSubTotal: 0,
            ProductSubTotal: 0,
            DiscountId: 0,
            Discount: 0,
            DiscountType: '',
            AdditionalCharges: 0,
            BillingAddress: '',
            OrderExist: false,
            PrimaryQuoteName: '',
            PrimaryQuoteId: 0,
            CoreProductMargin: 0,
            CoreProductMarginPercent: 0,
            MyProductMargin: 0,
            MyProductMarginPercent: 0,
            ServiceMargin: 0,
            ServiceMarginPercent: 0,
            ServicesList: {},
            DiscountsList: {},
            TotalMarginPercent: 0,
            NumberOfQuotelines: 0,
            NVR: 0,
            HeaderDiscount: 0,
            TaxDetails: {},
            PreviousExpirydate: '',
            SaleDate: 0,
            SaleDateCreatedBy: '',
            SaleDateCreatedOn: 0,
            SaleDateCreatedByName: '',
            PreviousSaleDate: 0,
            OpportunityCreatedOn: 0,
            Color: '',
            Fabric: '',
            ManualUnitPrice: false,
            QuoteGroupDiscount: {}
        }
        var Quoteline = {
            QuoteLineId: 0,
            QuoteKey: 0,
            MeasurementsetId: 0,
            VendorId: 0,
            ProductId: 0,
            SuggestedResale: 0,
            Discount: 0,
            CreatedOn: 0,
            CreatedBy: 0,
            LastUpdatedOn: 0,
            LastUpdatedBy: 0,
            IsActive: true,
            Width: 0,
            Height: 0,
            UnitPrice: 5,
            Quantity: 0,
            Description: '',
            MountType: '',
            ExtendedPrice: 0,
            Memo: '',
            PICJson: '',
            ProductName: '',
            VendorName: '',
            ProductTypeId: ''
        }
        //$scope.GetQuoteDetails = function () {
        //}
        $scope.SaveQuoteLineMemo = function (data) {
            $http.post('/api/Quotes/' + data.QuoteLineId + '/SaveQuoteLinesMemo', "'" + data.Memo + "'").then(
                function (response) {
                    if (response.data.error == '') {
                        HFC.DisplaySuccess("Memo save was sucessful");
                    } else {
                        HFC.DisplayAlert(response.error);
                    }

                    $("#quoteLineMemomodal").modal("hide");
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";

                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
        }
        $scope.CheckUncheckHeader = function () {
            $scope.IsAllChecked = true;
            for (var i = 0; i < $scope.Quotation.MeausurementList.length; i++) {
                if (!$scope.Quotation.MeausurementList[i].Selected) {
                    $scope.IsAllChecked = false;
                    break;
                }
            };
        };
        $scope.selectMeasurements();

        $scope.CheckUncheckAll = function () {
            var check = $scope.IsAllChecked;
            var selectCount = 0;
            //$scope.selectedMeasurementId=  $scope.Quotation.MeasurementsetId.split(',');
            for (var i = 0; i < $scope.Quotation.MeausurementList.length; i++) {
                //for (var j = 0 ; j < $scope.selectedMeasurementId.length; j++) {
                //if ($scope.MeausurementList[i].id == $scope.selectedMeasurementId[j]) {
                $scope.Quotation.MeausurementList[i].Selected = true;
                selectCount += 1;
                //}

                //}
            }
            if (selectCount == $scope.Quotation.MeausurementList.length) {
                $scope.IsAllChecked = true;
            }
        };

        $scope.itemClicked = function () {
            $scope.selectedAll = $scope.Quotation.MeausurementList.every(function (item) {
                return item.checked;
            });
        }
        $scope.marginSummary = function () {
            $("#marginSummary").modal("show");
        }
        $scope.addEditQuoteLineMemo = function (data) {
            $scope.SelectedQuoteLine = data;
            $("#quoteLineMemomodal").modal("show");
        }
        $scope.selectAll = function () {
            $scope.Quotation.MeausurementList.map(function (item) {
                item.checked = $scope.selectedAll;
            });
        }
        $scope.CopyPrevFlag = false; $scope.CopyFlag = false; $scope.AllowSpin = false;
        $scope.Copyclick = false;
        $scope.CopyQuoteline = function (data) {
            $scope.Copyclick = true;
            $scope.CopyFlag = false;
            $scope.loadingElement.style.display = "block";
            $scope.updateQuoteLineFlag = false;
            $scope.reserveCopyData = data;
            var obj = {};
            var EditRow = true;
            if (!$scope.CopyPrevFlag) {
                var editRow = $("#QuotelinesGrid").find(".k-grid-edit-row");
                if (editRow.length > 0) {	//to check whether we have any row in editable mode
                    var editedrowdata = $("#QuotelinesGrid").data("kendoGrid").dataSource.getByUid(editRow.data("uid"));
                    if (editedrowdata.QuoteLineId == $scope.reserveCopyData.QuoteLineId) {	//to check if current deleting row is in editable mode
                        EditRow = $scope.editUpdateQuoteLines();
                    }
                    else
                        EditRow = true;
                }
                else {	//if there's no editable row
                    EditRow = true;
                }
            }
            if (EditRow) {
                obj.copyOrDelete = "COPY";
                obj.QuoteKey = $scope.reserveCopyData.QuoteKey;
                var postObj = JSON.stringify(obj);
                if (!$scope.CopyFlag) {
                    $scope.loadingElement.style.display = "block";
                    $scope.AllowSpin = true;
                    $http.post('/api/Quotes/' + $scope.reserveCopyData.QuoteLineId + '/CopyDeleteQuoteLine', "'" + postObj + "'").then(
                        function (response) {
                            $scope.Copyclick = false;
                            $scope.loadingElement.style.display = "block";
                            if (response.data.error == '') {

                                //HFC.DisplaySuccess("New quote line created.");
                                //$window.location.reload();

                                //$scope.QuotelineDatasource = new kendo.data.DataSource({
                                //    data: response.data.data,
                                //    //pageSize: 5
                                //});
                                $scope.AllowSpin = false;
                                $scope.QuoteLinesList = response.data.data;
                                $scope.Quotation = response.data.quote;
                                setQuoteHeaderDiscountValue();
                                $scope.QuotationCopy = angular.copy($scope.Quotation);
                                //$scope.apply()

                                $scope.Quotation.TaxDetails = response.data.quote.TaxDetails;
                                $scope.ResetQuoteTax();

                                $('#QuotelinesGrid').data('kendoGrid').dataSource.read(response.data.data);
                                $('#QuotelinesGrid').data('kendoGrid').refresh();
                                //$("#QuotelinesGrid").data('kendoGrid').refresh();
                            } else {
                                HFC.DisplayAlert(response.error);
                            }
                            // console.log(response);
                            $("#quotemodal").modal("hide");
                            $scope.loadingElement.style.display = "none";
                            $scope.CopyPrevFlag = false;
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";

                            //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                        });
                    //$scope.loadingElement.style.display = "block";
                }
            }
            else
                $scope.loadingElement.style.display = "none";
        }


        $scope.DeleteSelectedQuotelines = function () {
            $scope.updateQuoteLineFlag = false;
            //$scope.selectedquotelinesFortaxexempt

            if ($scope.selectedquotelinesFortaxexempt.length > 0) {
                if ($scope.Quotation.PreviousSaleDate == null)
                    if (confirm("Are you sure you want to delete the selected Quote Lines?")) {
                        $scope.loadingElement.style.display = "block";
                        $http.post('/api/Quotes/' + $scope.quoteKey + '/EraseQuoteLine', $scope.selectedquotelinesFortaxexempt).then(
                            function (response) {
                                $scope.selectedLinesAfterSelection = [];
                                if (response.data.error == '') {
                                    if ($scope.selectedquotelinesFortaxexempt.length > 1)
                                        HFC.DisplaySuccess("Quote lines cleared.");
                                    else
                                        HFC.DisplaySuccess("Quote line cleared.");
                                    $scope.QuotelineDatasource = new kendo.data.DataSource({
                                        data: response.data.data,
                                    });
                                    $scope.QuoteLinesList = response.data.data;
                                    $scope.Quotation = response.data.quote;
                                    setQuoteHeaderDiscountValue();
                                    $scope.ResetQuoteTax();
                                    $scope.QuotationCopy = angular.copy($scope.Quotation);
                                    $('#QuotelinesGrid').data('kendoGrid')._selectedIds = {};
                                    $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                                    $('#QuotelinesGrid').data('kendoGrid').refresh();
                                    $scope.hideTaxExemptIcons = true;
                                    $("#SelectAll_Chkbox")[0].checked = false;
                                    $scope.selectedquotelinesFortaxexempt = [];
                                    $scope.SelectAll = false;
                                    var grid = $("#QuotelinesGrid").data("kendoGrid");
                                    grid.refresh();
                                    //if (grid) {
                                    //    if ($scope.brandid === 1) {
                                    //        grid.showColumn(23);
                                    //    } else {
                                    //        grid.showColumn(15);
                                    //    }

                                    //}
                                } else {
                                    HFC.DisplayAlert(response.error);
                                }
                                //  console.log(response);
                                $("#quotemodal").modal("hide");

                                $scope.loadingElement.style.display = "none";
                            }).catch(function (error) {
                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "none";
                            });
                    }
            }


        }
        $scope.MarkLinesTaxexemptOrNot = function (isTaxExempt) {
            $scope.loadingElement.style.display = "block";
            $scope.updateQuoteLineFlag = false;
            if ($scope.selectedquotelinesFortaxexempt.length > 0) {
                if ($scope.Quotation.PreviousSaleDate == null)
                    $http.post('/api/Quotes/' + $scope.quoteKey + '/MarkQuoteLinesTaxExempt?isTaxExempt=' + isTaxExempt, $scope.selectedquotelinesFortaxexempt).then(
                        function (response) {
                            //$("#SelectAll_Chkbox")[0].checked = false;
                            //$scope.SelectAll = false;
                            $scope.selectedLinesAfterSelection = [];
                            if (response.data.error == '') {
                                if ($scope.selectedquotelinesFortaxexempt.length > 1)
                                    HFC.DisplaySuccess("Quote lines cleared.");
                                else
                                    HFC.DisplaySuccess("Quote line cleared.");
                                $scope.QuotelineDatasource = new kendo.data.DataSource({
                                    data: response.data.data,
                                });
                                $scope.QuoteLinesList = response.data.data;
                                $scope.Quotation = response.data.quote;
                                setQuoteHeaderDiscountValue();
                                $scope.ResetQuoteTax();
                                $scope.QuotationCopy = angular.copy($scope.Quotation);
                                $scope.selectedquotelinesFortaxexempt = [];
                                $('#QuotelinesGrid').data('kendoGrid')._selectedIds = {};
                                $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                                $('#QuotelinesGrid').data('kendoGrid').refresh();
                            } else {
                                HFC.DisplayAlert(response.error);
                            }
                            //  console.log(response);
                            $("#quotemodal").modal("hide");
                            $scope.hideTaxExemptIcons = true;
                            $("#SelectAll_Chkbox")[0].checked = false;
                            $scope.SelectAll = false;
                            var grid = $("#QuotelinesGrid").data("kendoGrid");
                            grid.refresh();
                            //if (grid) {
                            //    if ($scope.brandid === 1) {
                            //        grid.showColumn(23);
                            //    } else {
                            //        grid.showColumn(15);
                            //    }

                            //}
                            $scope.loadingElement.style.display = "none";
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                        });
            }

        }


        $scope.deleteQuoteline = function (data) {
            $scope.loadingElement.style.display = "block";
            $scope.updateQuoteLineFlag = false;
            $scope.reserveDeleteData = data;
            var obj = {};
            obj.copyOrDelete = "DELETE";
            obj.QuoteKey = $scope.reserveDeleteData.QuoteKey;
            var postObj = JSON.stringify(obj);
            var deleteRow = false;
            var editRow = $("#QuotelinesGrid").find(".k-grid-edit-row");
            if (editRow.length > 0) {	//to check whether we have any row in editable mode
                var editedrowdata = $("#QuotelinesGrid").data("kendoGrid").dataSource.getByUid(editRow.data("uid"));
                if (editedrowdata.QuoteLineId != $scope.reserveDeleteData.QuoteLineId) {	//to check if current deleting row is in editable mode
                    if ($scope.editUpdateQuoteLines()) {
                        deleteRow = true;
                    }
                }
                else
                    deleteRow = true;
            }
            else {	//if there's no editable row
                deleteRow = true;
            }

            if (deleteRow) {
                $http.post('/api/Quotes/' + $scope.reserveDeleteData.QuoteLineId + '/CopyDeleteQuoteLine', "'" + postObj + "'").then(
                    function (response) {
                        if (response.data.error == '') {
                            HFC.DisplaySuccess("Quote line deleted.");
                            //$window.location.reload();

                            $scope.QuotelineDatasource = new kendo.data.DataSource({
                                data: response.data.data,
                                //pageSize: 5
                            });
                            $scope.QuoteLinesList = response.data.data;
                            $scope.Quotation = response.data.quote;
                            setQuoteHeaderDiscountValue();
                            $scope.ResetQuoteTax();
                            $scope.QuotationCopy = angular.copy($scope.Quotation);

                            $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                            $('#QuotelinesGrid').data('kendoGrid').refresh();
                            //$("#QuotelinesGrid").data('kendoGrid').refresh();
                        } else {
                            HFC.DisplayAlert(response.error);
                        }
                        //  console.log(response);
                        $("#quotemodal").modal("hide");

                        $scope.loadingElement.style.display = "none";
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";

                        //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                    });
            }
        }
        $scope.Viewpageload = false;

        var url = window.location.href;
        if (!(url.includes("moveQuote"))) {
            if ($scope.quoteKey === undefined) {
                $http.get('/api/Quotes/0/GetDetails?oppurtunityId=' + $scope.OpportunityId).then(function (data) {
                    if (data.data.IsTaxExempt && $scope.quoteKey === undefined) {
                        $("#TaxExempt_Warninginfo").modal("show");
                    }
                    $scope.Quotation = data.data;
                    setQuoteHeaderDiscountValue();
                    $scope.Copy = $scope.Quotation.SaleDate;
                    $scope.SaleDateCreatedByNameCopy = $scope.Quotation.SaleDateCreatedByName;
                    $scope.PreviousSaleDateCopy = $scope.Quotation.PreviousSaleDate;
                    $scope.SaleDateCreatedOnCopy = $scope.Quotation.SaleDateCreatedOn;
                    $scope.Viewpageload = true;
                    if ($scope.Quotation.SaleDate != null)
                        $scope.ValidSale = true;

                    if ($scope.Quotation.PreviousSaleDate == null)
                        $scope.nullpreview = true;

                    //if ($scope.Quotation.QuoteStatusId == 5) {
                    //    $scope.formquote.$setPristine();
                    //    $scope.loadingElement.style.display = "none";
                    //    $window.location.href = "#!/quoteSearch/" + $routeParams.OpportunityId;
                    //}


                    $scope.QuotationCopy = angular.copy($scope.Quotation);
                    $scope.QuoteinEditMode = true;
                    $scope.Quotation.DiscountId = 0;
                    //var quotename = "Quote #" + $scope.Quotation.QuoteID;
                    HFCService.setHeaderTitle("Quote #");
                    TaxDetails();
                    $scope.Viewpageload = true;
                    getQuotelines();

                    //TP-1715	CLONE - Quote Name Autofill with Opportunity Name
                    // $scope.Quotation.QuoteName = $scope.Quotation.OpportunityName;
                    $scope.formquote.$dirty = true;
                    var loadingElement = document.getElementById("loading");
                    if ($scope.quoteLineLoaded) {
                        $scope.quoteLineLoaded = false;
                        loadingElement.style.display = "none";
                    }

                    $scope.Loading = false;
                    setTimeout(function () {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    }, 500);
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                    $scope.Loading = false;

                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
            } else {
                $http.get('/api/Quotes/' + $scope.quoteKey + '/GetDetails?oppurtunityId=' + $scope.OpportunityId).then(
                    function (data) {

                        $scope.Quotation = data.data;
                        setQuoteHeaderDiscountValue();
                        $scope.Copy = $scope.Quotation.SaleDate;
                        $scope.SaleDateCreatedByNameCopy = $scope.Quotation.SaleDateCreatedByName;
                        $scope.PreviousSaleDateCopy = $scope.Quotation.PreviousSaleDate;
                        $scope.SaleDateCreatedOnCopy = $scope.Quotation.SaleDateCreatedOn;

                        if ($scope.Quotation.SaleDate != null)
                            $scope.ValidSale = true;

                        //if ($scope.Quotation.QuoteStatusId == 5) {
                        //    $scope.formquote.$setPristine();
                        //    $scope.loadingElement.style.display = "none";
                        //    $window.location.href = "#!/quoteSearch/" + $routeParams.OpportunityId;
                        //}

                        $scope.QuotationCopy = angular.copy($scope.Quotation);
                        $scope.Viewpageload = true;
                        getQuotelines();
                        HFCService.setHeaderTitle("Quote #" + $scope.Quotation.QuoteID + " - " + $scope.Quotation.QuoteName);
                        TaxDetails();

                        var loadingElement = document.getElementById("loading");
                        if ($scope.quoteLineLoaded) {
                            $scope.quoteLineLoaded = false;
                            loadingElement.style.display = "none";
                        }
                        $scope.Loading = false;
                        setTimeout(function () {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                        }, 500);
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                        $scope.Loading = false;

                        //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                    });
            }
        }

        $scope.ResetQuoteTax = function () {
            TaxDetails();
            $scope.TaxTotal = 0;
            var qorg = $scope.QuotationCopy;
            var subTotal = $scope.Quotation.AdditionalCharges + $scope.Quotation.ProductSubTotal - $scope.Quotation.TotalDiscounts;
            if ($scope.Quotation.IsTaxExempt === false) {
                if ($scope.Quotation.TaxDetails != null && $scope.Quotation.TaxDetails.length > 0) {
                    //for (var i = 0; i < $scope.Quotation.TaxDetails.length; i++) {
                    //    if ($scope.Quotation.TaxDetails[i].Amount !== null) {
                    //        $scope.TaxTotal = $scope.TaxTotal + $scope.Quotation.TaxDetails[i].Amount;
                    //    }
                    //}
                    var tax = 0; $.grep($scope.Quotation.TaxDetails, function (item, val) { tax = tax + item.Amount });
                    $scope.TaxTotal = tax;
                }
            }
            else {
                $scope.Quotation.QuoteSubTotal = subTotal;
                $scope.Quotation.TaxDetails = [];
            }
            $scope.Quotation.QuoteSubTotal = subTotal + $scope.TaxTotal;

            $('#taxDetailsGrid').data('kendoGrid').setDataSource($scope.Quotation.TaxDetails);
            $('#taxDetailsGrid').data('kendoGrid').refresh();
        }
        //removing the tax exempt flag
        $scope.RemoveTaxExempt = function () {
            $scope.Quotation.IsTaxExempt = false;
            $scope.Quotation.IsPSTExempt = null;
            $scope.Quotation.IsGSTExempt = false;
            $scope.Quotation.IsHSTExempt = false;
            $scope.Quotation.IsVATExempt = false;
            $("#TaxExempt_Warninginfo").modal("hide");

        }
        function dosometing(dsitemGrid) {
            var categoryList = $scope.list.categories;
            if (dsitemGrid.ProductCategoryId && categoryList)
                var categoryObj = $.grep(categoryList, function (s) {
                    return s.ProductCategoryId == dsitemGrid.ProductCategoryId;
                });

            if (categoryObj) dsitemGrid.set("ProductCategory", categoryObj[0].ProductCategory);
        }
        $scope.oneHeaderDiscountChange = function () {
            var quote = $scope.Quotation;
            var item = $scope.Quotation.DiscountId;
            if (item === null || item == "") {
                $scope.Quotation.Discount = "";
                $scope.Quotation.DiscountType = "";
                setTimeout(function () {
                    $scope.$apply();
                }, 100);
            } else {
                for (var i = 0; i < quote.DiscountsList.length; i++) {
                    if (quote.DiscountsList[i].DiscountId == item) {
                        $scope.Quotation.Discount = Math.abs(quote.DiscountsList[i].DiscountValue);
                        if (Number(quote.DiscountsList[i].DiscountFactor) === 1) {
                            $scope.Quotation.DiscountType = "%";
                        } else {
                            $scope.Quotation.DiscountType = "$";
                        }
                        setTimeout(function () {
                            $scope.$apply();
                        }, 100);
                        break;

                    }
                }
            }

            //  $scope.Quotation.Discount
        }

        $scope.ExpCollapseGrid = function () {
            var expandedRows = $('.k-detail-row:visible');
            if (expandedRows.length != 0) {
                var grid = $("#QuotelinesGrid").data("kendoGrid");
                $(".k-master-row").each(function (index) {
                    grid.collapseRow(this);
                });
            } else {
                var grid = $("#QuotelinesGrid").data("kendoGrid");
                $(".k-master-row").each(function (index) {
                    grid.expandRow(this);
                });
            }
        }

        $scope.gotoMyConfigureProduct = function () {
            $scope.formquote.$setPristine();
        }
        $scope.quoteLineLoaded = true;
        function getQuotelines() {
            var obj;
            if ($scope.QuoteinEditMode) {
                obj = {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        },
                        date: {
                            gt: "After",
                            lt: "Before"
                        },
                    }
                }
            } else {
                obj = false;
            }
            if ($scope.brandid === 1) {
                $scope.quoteLinesOptions = {
                    dataSource: {
                        type: "jsonp",
                        transport: {
                            cache: false,
                            read: function (e) {
                                if ($scope.dragAndDropFlag) {
                                    e.success($scope.reorderedQuoteLines);
                                } else {
                                    if (!$scope.quoteKey) //added since intial page load quote key is undefined and api throws error.
                                        return;
                                    var loadingElement = document.getElementById("loading");
                                    if (!$scope.Viewpageload || $scope.QuoteinEditMode)
                                        loadingElement.style.display = "block";
                                    $http.get("/api/Quotes/" + $scope.quoteKey + "/GetQuoteLines", e).then(function (data) {
                                        //$scope.Quotation = angular.copy($scope.QuotationCopy);
                                        $scope.selectedMeasurementForAccount = [];
                                        $scope.DiscountAndPromo = data.data.discountAndPromo;
                                        $scope.quoteLinesBackup = data.data.quoteLines;
                                        e.success(data.data.quoteLines);
                                        $scope.quoteLineLoaded = true;
                                        if (((!$scope.AllowSpin && !$scope.QuoteSubmitted) || (($scope.saveBasicQuoteDetailFlag || $scope.saveBasicQuoteDetailFlag == undefined) && ($scope.updateLineItemCompleteFlag || $scope.updateLineItemCompleteFlag == undefined) && $scope.Viewpageload) || ($scope.saveBasicQuoteDetailFlag && $scope.updateLineItemCompleteFlag == undefined) || ($scope.saveBasicQuoteDetailFlag == undefined && $scope.updateLineItemCompleteFlag) || ($scope.saveBasicQuoteDetailFlag && $scope.updateLineItemCompleteFlag))) {
                                            var loadingElement = document.getElementById("loading");
                                            loadingElement.style.display = "none";
                                            $scope.Viewpageload = false;

                                        }
                                        // e.success(data.data.quoteLines);
                                        ///make the row editable
                                        if ($scope.bringQuotelineEditMode != 0 &&
                                            $scope.bringQuotelineEditMode != undefined &&
                                            $scope.bringQuotelineEditMode != null) {
                                            $scope.CollapseAllKendoGridDetailRows("QuotelinesGrid");
                                            var grid = $("#QuotelinesGrid").data("kendoGrid");
                                            var dataItem = $("#QuotelinesGrid").data("kendoGrid").dataSource
                                                .get($scope.bringQuotelineEditMode);
                                            //var row = $("#QuotelinesGrid").data("kendoGrid").tbody.find("tr[data-uid='" + dataItem.uid + "']");
                                            var link = grid.tbody.find("tr[data-uid='" + dataItem.uid + "']")
                                                .find("td.k-hierarchy-cell .k-icon");
                                            var row = grid.tbody.find("tr[data-uid='" + dataItem.uid + "']");
                                            grid.expandRow(row);
                                            $scope.childGridUpdate();
                                            //link.click();
                                            var grdId = "#grd" + dataItem.QuoteLineId;
                                            var childgrid = $(grdId).data("kendoGrid");

                                            grid.editRow(dataItem);
                                            childgrid.editRow(dataItem);
                                            //   $('#QuotelinesGrid').data('kendoGrid').refresh();

                                            $scope.bringQuotelineEditMode = 0;
                                        }
                                        $scope.UpdateGroupDiscountLabel();
                                    }).catch(function (error) {
                                        var loadingElement = document.getElementById("loading");
                                        loadingElement.style.display = "none";

                                        //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                                    });
                                }
                            },

                            update: function (e) {
                                $scope.reserveEvent = e;
                                if ($scope.Saveflag) {
                                    setTimeout(function () {
                                        $scope.saveQuoteLines($scope.reserveEvent);
                                        $scope.Saveflag = false;
                                    }, 500);
                                } else {
                                    $scope.saveQuoteLines($scope.reserveEvent);
                                }

                            },

                            parameterMap: function (options, operation) {
                                if (operation === "update") {
                                    return options;
                                }
                            }
                        },

                        schema: {
                            model: {
                                id: "QuoteLineId",
                                fields: {
                                    QuoteLineNumber: { editable: false },
                                    QuoteLineId: { type: "number", editable: false },
                                    QuoteKey: { editable: false },
                                    MeasurementsetId: { editable: false, nullable: true },
                                    MeasurementId: { editable: false },
                                    VendorId: { type: "number", editable: false, nullable: true },
                                    ProductId: { type: "number", editable: true, nullable: true },
                                    SuggestedResale: { type: "number", editable: false, nullable: true },
                                    Discount: { type: "number", editable: true, nullable: true },
                                    Width: { type: "number", editable: true, nullable: true },
                                    Height: { type: "number", editable: true, nullable: true },
                                    UnitPrice: { type: "number", editable: true, nullable: true },
                                    Quantity: {
                                        type: "number",
                                        validation: { decimals: 0 },
                                        editable: true,
                                        nullable: true,
                                        width: "50px"
                                    },
                                    Description: { editable: true, encoded: true },
                                    MountType: { editable: false },
                                    ExtendedPrice: { type: "number", editable: false, nullable: true },
                                    Memo: { editable: false, hidden: true },
                                    PICJson: { editable: false },
                                    ProductName: { editable: true, nullable: true },
                                    VendorName: { editable: false },
                                    ProductTypeId: { type: "number", editable: true, nullable: true },
                                    RoomName: { editable: true },
                                    RoomLocation: { editable: true },
                                    WindowLocation: { editable: true },
                                    FranctionalValueWidth: { editable: true },
                                    FranctionalValueHeight: { editable: true },
                                    Color: { editable: false },
                                    Fabric: { editable: false }, DiscountType: { editable: true },
                                    DiscountorProduct: { editable: true }
                                }
                            }
                        },
                        change: function (e) {
                            if (e.action === "itemchange" && e.field === "UnitPrice") {
                                e.items[0].ManualUnitPrice = true;
                            }
                        },
                    },

                    autoSync: true,
                    batch: true,
                    cache: false,
                    toolbar: [
                        {
                            template: $("#toolbarTemplate").html()
                        }
                    ],
                    sortable: $scope.QuoteinEditMode,
                    //filterable: $scope.QuoteinEditMode,
                    filterable: obj,
                    resizable: true,
                    editable: "inline",
                    detailInit: detailinitParent,
                    dataBound: function (e) {
                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                        $scope.quoteLineQtyTotal = 0;
                        $scope.quoteLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource._data.length;
                        var datasourcee = $("#QuotelinesGrid").data("kendoGrid").dataSource._data;
                        if (datasourcee.length > 0)
                            for (var count = 0; count < $scope.quoteLineLength; count++) {
                                if (datasourcee[count].Quantity) $scope.quoteLineQtyTotal = $scope.quoteLineQtyTotal + datasourcee[count].Quantity;
                            }

                        if ($scope.Permission.EditQuoteLines && $scope.QuoteinEditMode == true && $scope.Quotation.QuoteStatusId != 3 && !$scope.dragAndDropFlag) {
                            var grid = $("#QuotelinesGrid").data("kendoGrid");
                            if (grid) {
                                grid.showColumn(1);
                            }
                        }
                        if ($scope.updateQuoteLineFlag) {
                            $scope.enableSortableFlag = false;
                        }
                    },
                    noRecords: { template: "No records found" },
                    columns: [
                        {
                            field: "", width: "50px",
                            attributes: {
                                "class": "draggable"
                            },
                            template: function () {
                                return "<button type='button' class='tooltip-bottom quote_dragttip ng-scope' data-tooltip='Drag & Drop to re-arrange lines'><i class='far fa-sort draggable-element' aria-hidden='true'></i></button>";
                            },
                            hidden: true
                        }, {
                            field: "", width: "50px",
                            title: 'Select All',
                            headerTemplate: '<input ng-if="QuoteinEditMode==true && Quotation.QuoteStatusId == 1" id="SelectAll_Chkbox" value="SelectAll" type="checkbox" ng-model="SelectAll"  ng-click="SelectAllLineItems()"></input>',
                            template: function (dataItem) {
                                //return '<input id="' + dataItem.QuoteLineNumber.toString() + '" type="checkbox" ng-model="checkvalue' + dataItem.QuoteLineNumber.toString() + '" ng-change="getSelectClick(dataItem)" class="option-input checkbox ng-pristine ng-untouched ng-valid ng-empty" >';
                                return '<span class="Grid_Textalign"><div><div style="float:left;"> <input ng-if="QuoteinEditMode==true && Quotation.QuoteStatusId == 1" class="Select_Checkbox" id="select_' + dataItem.QuoteLineNumber.toString() + '" type="checkbox" ng-model="select_' + dataItem.QuoteLineNumber.toString() + '" ng-click="getSelectClick(dataItem)"></input> </div></div></span>';
                            },
                            hidden: true
                        },
                        { field: "QuoteLineId", hidden: true },
                        { field: "QuoteKey", hidden: true },
                        { field: "MeasurementsetId", hidden: true },
                        { field: "MeasurementId", hidden: true },
                        { field: "VendorId", hidden: true },
                        { field: "ProductId", hidden: true },
                        { field: "Memo", hidden: true },
                        { field: "DiscountType", hidden: true },

                        { field: "PICJson", hidden: true },
                        // { field: "WindowLocation", hidden: true },
                        { field: "ProductTypeId", hidden: true },

                        {
                            field: "QuoteLineId", title: " ", template: function (dataItem) {
                                var template = '';
                                if (dataItem.ProductTypeId == 1 && dataItem.ValidConfiguration !== null) {
                                    if (dataItem.ValidConfiguration) {
                                        template = template + '<i class="fas fa-check-circle validConfig"></i>';
                                    }
                                    else {
                                        template = template + '<i class="fas fa-exclamation-triangle invalidConfig"></i>';
                                    }
                                }
                                return template;
                            }, width: "30px", filterable: false
                        },
                        {
                            field: "QuoteLineNumber", title: "ID",
                            width: "80px",
                            template: function (dataItem) {
                                if ((dataItem.MeasurementsetId == 0 || dataItem.MeasurementsetId == null) && dataItem.ProductTypeId == 1) {
                                    return '<span class="Grid_Textalign"><div><div style="float:left;"> <input id="' + dataItem.QuoteLineNumber.toString() + '" ng-show="QuoteinEditMode"  type="checkbox" ng-model="checkvalue' + dataItem.QuoteLineNumber.toString() + '" ng-change="getClick(dataItem)"></input> </div><div style="float:right;">' + dataItem.QuoteLineNumber.toString() + '</div></div></span>';
                                }
                                else
                                    return '<span class="Grid_Textalign" style="float:right;">' + dataItem.QuoteLineNumber.toString() + '</span>';
                            }
                        },
                        {
                            field: "Quantity", title: "Qty", width: "60px", editor: quantityEditor, template: function (dataItem) {
                                if ((dataItem.ProductTypeId === 1 ||
                                    dataItem.ProductTypeId === 2 ||
                                    dataItem.ProductTypeId === 3 ||
                                    dataItem.ProductTypeId === 5) && $scope.Quotation.QuoteStatusId == 3) {
                                    if (dataItem.Quantity === 0) {
                                        return "Cancelled";
                                    } else {
                                        if (dataItem.Quantity === null || dataItem.Quantity === undefined || dataItem.Quantity == 0) {
                                            return '';
                                        } else {
                                            return '<span class="Grid_Textalign">' + HFCService.NumberFormat(dataItem.Quantity, 'number') + '</span>';
                                        }
                                    }
                                } else {
                                    if (dataItem.Quantity === null || dataItem.Quantity === undefined) {
                                        return '';
                                    } else {
                                        return '<span class="Grid_Textalign">' + HFCService.NumberFormat(dataItem.Quantity, 'number') + '</span>';
                                    }
                                }
                            }
                        },
                        { field: "RoomName", title: "Window Name", width: "120px" },
                        //{ field: "WindowLocation", title: "Window Location" },

                        //{ field: "VendorName", title: "Vendor" },
                        {
                            field: "ProductName",
                            title: "Item",
                            editor: DiscountDropdown,
                            template: "# if(ProductTypeId == null){ #" +
                                "<span></span>" +
                                "#} else if(ProductTypeId == 4){ #" +
                                "<span>#=ProductName#</span>" +
                                "#} else {#" +
                                "<span>#=ProductName#</span>" +
                                "# } #"
                        },
                        { field: "Color", title: "Color" },
                        { field: "Fabric", title: "Fabric" },
                        {
                            field: "Width",
                            title: "Width",
                            editor: FractionDropDownEditorWidth,
                            //template: "#= Width # #= FranctionalValueWidth #",
                            template: function (dataitem) {
                                if (dataitem.FranctionalValueWidth)
                                    return dataitem.Width + " " + dataitem.FranctionalValueWidth;
                                else
                                    return dataitem.Width;
                            },
                            filterable: false
                        },
                        {
                            field: "Height",
                            title: "Height",
                            editor: FractionDropDownEditorHeight,
                            filterable: false,
                            //template: "#= Height # #= FranctionalValueHeight #"
                            template: function (dataitem) {
                                if (dataitem.FranctionalValueHeight)
                                    return dataitem.Height + " " + dataitem.FranctionalValueHeight;
                                else
                                    return dataitem.Height;
                            },
                        },
                        { field: "MountType", title: "Mount Type" },
                        //{ field: "SuggestedResale", title: "Suggested Resale", format: "{0:c}" },
                        //{ field: "UnitPrice", title: "Unit Price", format: "{0:c}" },
                        //{ field: "Discount", title: "Discount", template: '#=kendo.format("{0:p}", Discount / 100)#' },
                        {
                            field: "ExtendedPrice", width:96, title: "Extended Total",
                            //format: "{0:c}"
                            template: function (dataItem) {
                                if (dataItem.TaxExempt) {
                                    return "<span class='Grid_Textalign' style='padding-right:0 !important'><img style='float: left;' src='../../../Content/images/Tax-Exempt.png'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                                } else {
                                    return "<span class='Grid_Textalign' style='float:right; padding-right:0 !important'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                                }
                            },
                        },
                        {
                            field: "",
                            title: " ",
                            width: "60px",
                            template:
                                ' <ul ng-if="hideTaxExemptIcons==true&&Permission.EditQuoteLines && QuoteinEditMode==true && Quotation.QuoteStatusId != 3 && !dragAndDropFlag">' +
                                '<li class="dropdown note1 ad_user"><div class="dropdown"><button  class="btn btn-default dropdown-toggle qlEllipsis" ng-click="qlLineClicked($event)" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button>' +
                                '<ul class="dropdown-menu pull-right">  ' +
                                //'<li ng-if="brandid==1" ng-hide="#=ProductTypeId#!==1&& #=ProductTypeId#!==0 && #=ProductTypeId#!==null"><a href="\\\\\\\\#!/configurator/#=QuoteKey#/#= QuoteLineId #" ng-click="gotoMyConfigureProduct()">Add / Update Configure Core Product</a> </li>' +
                                '<li ng-show="#=ProductTypeId# ===5 || #=ProductTypeId#==2||#=ProductTypeId#==0||#=ProductTypeId#==3"><a href="\\\\\\\\#!/myVendorConfigurator/#= Opportunity #/#= QuoteLineId #" ng-click="gotoMyConfigureProduct()">Add / Update My Product/ Service</a> </li> ' +
                                '<li><a href="javascript:void(0)"  ng-click="EditRowDetail(dataItem)">Edit</a></li>' +
                                '<li><a href="javascript:void(0)" ng-click="CopyQuoteline(dataItem)">Copy</a></li>' +
                                '</ul></div></li></ul>'
                        }
                    ]
                };
            }
            else if ($scope.brandid === 2 || $scope.brandid === 3) {
                $scope.quoteLinesOptions = {
                    dataSource: {
                        type: "jsonp",
                        transport: {
                            cache: false,
                            read: function (e) {
                                if ($scope.dragAndDropFlag) {
                                    e.success($scope.reorderedQuoteLines);
                                } else {
                                    if (!$scope.quoteKey) //added since intial page load quote key is undefined and api throws error.
                                        return;
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "block";
                                    $http.get("/api/Quotes/" + $scope.quoteKey + "/GetQuoteLinesTL", e).then(function (data) {
                                        //$scope.Quotation = angular.copy($scope.QuotationCopy);
                                        $scope.DiscountAndPromo = data.data.discountAndPromo;
                                        if ($scope.saveBasicQuoteDetailFlag && $scope.updateLineItemCompleteFlag && $scope.QuoteSubmitted) {
                                            var loadingElement = document.getElementById("loading");
                                            loadingElement.style.display = "none";
                                        } else if (!$scope.QuoteSubmitted) {
                                            var loadingElement = document.getElementById("loading");
                                            loadingElement.style.display = "none";
                                        }
                                        var loadingElement = document.getElementById("loading");
                                        loadingElement.style.display = "none";
                                        $scope.quoteLinesBackup = data.data.quoteLines;
                                        e.success(data.data.quoteLines);
                                        // e.success(data.data.quoteLines);
                                    }).catch(function (error) {
                                        var loadingElement = document.getElementById("loading");
                                        loadingElement.style.display = "none";

                                        //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                                    });
                                }
                            },

                            update: function (e) {
                                $scope.reserveEvent = e.data;
                                if ($scope.Saveflag) {
                                    setTimeout(function () {
                                        UpdateTLQuoteLine($scope.reserveEvent);
                                        $scope.Saveflag = false;
                                    }, 500);
                                } else {
                                    UpdateTLQuoteLine($scope.reserveEvent);
                                }
                            },

                            parameterMap: function (options, operation) {
                                if (operation === "update") {
                                    return options;
                                }
                            }
                        },
                        schema: {
                            model: {
                                id: "QuoteLineId",
                                fields: {
                                    QuoteLineId: { editable: false, type: "number" },
                                    QuoteLineNumber: { editable: false },
                                    Quantity: { editable: true, type: "number", nullable: true, validation: { required: true } },
                                    //ProductCategoryId: { editable: true, validation: { required: true } },
                                    ProductCategoryId: { editable: true },
                                    ProductSubCategoryId: { editable: true },
                                    Description: { editable: true, encoded: false },
                                    Cost: { editable: false, type: "number", nullable: true, validation: { required: true, min: 1 } },
                                    UnitPrice: { editable: true, type: "number", nullable: true, validation: { required: true, min: 1 } },
                                    Discount: { editable: true, type: "number", nullable: true },
                                    ProductCategory: { editable: true, validation: { required: true } },
                                    //ProductSubCategory: { editable: false },
                                    ProductSubCategory: { editable: true, validation: { required: true } },
                                    SuggestedResale: { editable: true, type: "number" },
                                    ExtendedPrice: { editable: false, type: "number" }, DiscountType: { editable: true }
                                }
                            }
                        },
                        change: function (e) {
                            if (e.action === "itemchange" && e.field === "UnitPrice") {
                                e.items[0].ManualUnitPrice = true;
                            }
                        },
                    },

                    //autoSync: true,
                    batch: true,
                    cache: false,
                    toolbar: [
                        {
                            //template: $("#toolbarTemplate").html()
                            template: function (headerData) {
                                return $("#toolbarTemplateTL").html()
                            }
                        }
                    ],
                    sortable: $scope.QuoteinEditMode,
                    filterable: $scope.QuoteinEditMode,
                    resizable: true,
                    editable: "inline",
                    dataBound: function (e) {
                        // $scope.quoteLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource._data.length;

                        $scope.quoteLineQtyTotal = 0;
                        $scope.quoteLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource._data.length;
                        var datasourcee = $("#QuotelinesGrid").data("kendoGrid").dataSource._data;
                        if (datasourcee.length > 0)
                            for (var count = 0; count < $scope.quoteLineLength; count++) {
                                if (datasourcee[count].Quantity) $scope.quoteLineQtyTotal = $scope.quoteLineQtyTotal + datasourcee[count].Quantity;
                            }
                        if ($scope.Permission.EditQuoteLines && $scope.QuoteinEditMode == true && $scope.Quotation.QuoteStatusId != 3 && !$scope.dragAndDropFlag) {
                            var grid = $("#QuotelinesGrid").data("kendoGrid");
                            if (grid) {
                                grid.showColumn(1);
                            }
                        }
                        if ($scope.updateQuoteLineFlag) {
                            $scope.enableSortableFlag = false;
                        }
                    },
                    noRecords: { template: "No records found" },
                    columns: [
                        {
                            field: "", width: "50px",
                            attributes: {
                                "class": "draggable"
                            },
                            template: function () {
                                return "<button type='button' class='tooltip-bottom cancel_tpbut ng-scope' data-tooltip='Drag & Drop to re-arrange lines'><i class='far fa-sort draggable-element' aria-hidden='true'></i></button>";
                            },
                            hidden: true
                        }, {
                            field: "", width: "50px",
                            title: 'Select All',
                            headerTemplate: '<input id="SelectAll_Chkbox" ng-if="QuoteinEditMode==true && Quotation.QuoteStatusId == 1"  value="SelectAll" type="checkbox" ng-model="SelectAll"  ng-change="SelectAllLineItems()"></input>',
                            template: function (dataItem) {
                                //return '<input id="' + dataItem.QuoteLineNumber.toString() + '" type="checkbox" ng-model="checkvalue' + dataItem.QuoteLineNumber.toString() + '" ng-change="getSelectClick(dataItem)" class="option-input checkbox ng-pristine ng-untouched ng-valid ng-empty" >';
                                return '<span class="Grid_Textalign"><div><div style="float:left;"> <input ng-if="QuoteinEditMode==true && Quotation.QuoteStatusId == 1" class="Select_Checkbox" id="select_' + dataItem.QuoteLineNumber.toString() + '" type="checkbox" ng-model="select_' + dataItem.QuoteLineNumber.toString() + '" ng-click="getSelectClick(dataItem)"></input> </div></div></span>';
                            },
                            hidden: true
                        },
                        { field: "QuoteLineId", hidden: true },
                        { field: "DiscountType", hidden: true },
                        {
                            field: "QuoteLineNumber", title: "Line ID",
                            //template: "#= QuoteLineNumber #",
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                            },
                            width: "90px"
                        },
                        {
                            field: "Quantity", title: "QTY", editor: quantityEditorTL, width: "80px",
                            //template: "#if(Quantity!==0 && Quantity !=null && Quantity !== 'null') {# #=Quantity# #} #"
                            template: function (dataItem) {
                                if ((dataItem.ProductTypeId === 1 ||
                                    dataItem.ProductTypeId === 2 ||
                                    dataItem.ProductTypeId === 3 ||
                                    dataItem.ProductTypeId === 5) && $scope.Quotation.QuoteStatusId == 3) {
                                    if (dataItem.Quantity === 0) {
                                        return "Cancelled";
                                    } else {
                                        if (dataItem.Quantity === null || dataItem.Quantity === undefined) {
                                            return '';
                                        } else {
                                            return '<span class="Grid_Textalign">' + HFCService.NumberFormat(dataItem.Quantity, 'number') + '</span>';
                                        }
                                    }
                                } else {
                                    if (dataItem.Quantity === null || dataItem.Quantity === undefined) {
                                        return '';
                                    } else {
                                        return '<span class="Grid_Textalign">' + HFCService.NumberFormat(dataItem.Quantity, 'number') + '</span>';
                                    }
                                }
                            }
                        },
                        //{ field: "Category", title: "Category" },
                        {
                            field: "ProductCategory",
                            title: "Product Type",
                            editor: dropDownEditorProductType, // dropDownEditorCategory,
                            template: "#=  (ProductCategory == null)? '' : ProductCategory #",
                            width: "200px",
                            filterable: true
                        },
                        //{ field: "Product", title: "Product" },
                        {
                            field: "ProductSubCategory",
                            title: "Category", //"Product",
                            editor: dropDownEditorCategory, //dropDownEditorProduct,
                            template: "#=  (ProductSubCategory == null)? '' : ProductSubCategory #",
                            width: "200px",
                            filterable: true
                        },
                        {
                            field: "ProductName"
                            , title: "Product Name"
                            , editor: dropDownEditorProductName,
                            //, template: "#=  (ProductName == null)? '' : ProductName #",
                        },
                        {
                            field: "Description"
                            , title: "Description"
                            //,template: function(dataItem) {
                            //    return  dataItem.Description;}
                            , editor: textBoxEditorDescription
                        },
                        {
                            field: "Cost", title: "Cost",
                            //format: "{0:c}",
                            //editor: CostTemplate,
                            //template: '  <a type="button" class="linkrule" name="#=QuoteLineId#" value="#=Cost#">#=Cost#</a>'
                            //template:'<a href="javascript: void (0) "  ng-click="TLCost(dataItem) ">#=Cost#</a>'
                            //template: ` <a type="button" class="linkrule" name="Cost" value="#=Cost#">#=kendo.format('{0:c}', Cost)#</a>`,
                            template: function (dataItem) {
                                return CostTemplate(dataItem);
                            }
                        },
                        {
                            field: "SuggestedResale", title: "Suggested Resale",
                            //format: "{0:c}",
                            editor: suggestedresaleEditorTL,
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.SuggestedResale) + "</span>";
                            },
                            // SuggesetedResaleTemplate
                        },
                        {
                            field: "UnitPrice", title: "Unit Price",
                            //format: "{0:c}",
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.UnitPrice) + "</span>";
                            },
                            editor: unitEditorTL
                        },
                        {
                            field: "Discount", title: "Discount",
                            template:

                                function (dataItem) {
                                    if (dataItem.ProductTypeId == 4) {
                                        if (dataItem.DiscountType === "$") {
                                            return "$" + dataItem.Discount;;
                                        } else if (dataItem.DiscountType === "%") {
                                            return dataItem.Discount + "%";
                                        }
                                        else if (dataItem.DiscountType == null) {
                                            if (dataItem.Discount != null && dataItem.Discount != undefined) {
                                                return dataItem.Discount + "%";
                                            }
                                        } else {
                                            return '';
                                        }
                                    }
                                    else {
                                        if (dataItem.Discount !== null && dataItem.Discount !== undefined && dataItem.Discount !== '' && dataItem.Discount > 0) {
                                            return dataItem.Discount + "%";
                                        } else {
                                            return '';
                                        }
                                    }

                                },
                            editor: discountEditorTL
                        },
                        //{ field: "SalePrice", title: "Sale Price", format: "{0:c}" },
                        {
                            field: "ExtendedPrice", title: "Extended Total",
                            //format: "{0:c}"
                            template: function (dataItem) {

                                if (dataItem.TaxExempt) {
                                    return "<span class='Grid_Textalign'><img style='float: left;' src='../../../Content/images/Tax-Exempt.png'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                                } else {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                                }
                                //return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                            },
                        },
                        // TODO: define this later.
                        {
                            field: "",
                            title: "",
                            width: "60px",
                            template: function (dataitem) {
                                var hcontent = `<ul ng-if="hideTaxExemptIcons==true&& Permission.EditQuoteLines && QuoteinEditMode==true && Quotation.QuoteStatusId != 3 && !dragAndDropFlag">
                                                   <li class ="dropdown note1 ad_user">
                                                    <div class ="btn-group">
                                                        <button  class ="btn btn-default dropdown-toggleql Ellipsis" ng-click="qlLineClicked($event)" data-toggle="dropdown">
                                                             <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                        </button>
                                                        <ul class ="dropdown-menu pull-right">
                                                            <li><a href="javascript: void (0) "  ng-click="EditRowDetail(dataItem) ">Edit</a></li>
                                                            <li><a href="javascript: void (0) " ng-click="CopyQuoteline(dataItem) ">Copy</a></li>
                                                        </ul>
                                                    </div>
                                                   </li>
                                                </ul>`;
                                return hcontent;
                            }
                            //    function (dataitem) {
                            //        return DropdownTemplate(dataitem);
                            //    },
                            //' <ul ng-if="Permission.EditQuoteLines && QuoteinEditMode==true && Quotation.QuoteStatusId != 3">                                     <li class="dropdown note1 ad_user">                                         <div class="btn-group">                                             <button  class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button>                                             <ul class="dropdown-menu pull-right">                                                 <li ng-if="brandid==1" ng-hide="#=ProductTypeId#!==1&& #=ProductTypeId#!==0 && #=ProductTypeId#!==null"><a href="\\\\\\\\#!/configurator/#=QuoteKey#/#= QuoteLineId #">Configure Core Product</a> </li>                                                 <li ng-show="#=ProductTypeId# ===5 || #=ProductTypeId#==2||#=ProductTypeId#==0||#=ProductTypeId#==3"><a href="\\\\\\\\#!/myVendorConfigurator/#= QuoteLineId #">Configure My Product/ Service</a> </li><li><a href="javascript:void(0)"  ng-click="EditRowDetail(dataItem)">Edit</a></li>                                                 <li><a href="javascript:void(0)" ng-click="addEditQuoteLineMemo(dataItem)">Add/Edit Memo</a></li>                                                 <li><a href="javascript:void(0)" ng-click="CopyQuoteline(dataItem)">Copy</a></li>                                                 <li><a href="javascript:void(0)" ng-click="deleteQuoteline(dataItem)">Delete</a></li></ul></div></li></ul>'
                        }
                    ]
                };
            }

            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if (grid) {
                grid.setOptions($scope.quoteLinesOptions);
            }
        }

        $scope.qlEllipsisOpenFlag = false;
        $scope.qlselectedLine = "";
        $scope.backupEvent;
        // quote line click event handler
        $scope.qlLineClicked = function (e) {
            //if (e && e.currentTarget) {
            //    $scope.backupEvent = e;
            //    setTimeout(function () {
            //        $scope.selectedQuoteEllipsisBtn = $scope.backupEvent.currentTarget;
            //        $scope.qlselectedLine = $($scope.backupEvent.currentTarget).parent();
            //        if ($scope.qlselectedLine) {
            //            if ($($scope.qlselectedLine).hasClass("open")) {
            //                $scope.qlEllipsisOpenFlag = true;
            //            } else {
            //                $scope.qlEllipsisOpenFlag = false;
            //            }
            //        }
            //    }, 100);
            //}
        }


        // saving quote line items
        $scope.saveQuoteLines = function (e) {
            $scope.loadingElement.style.display = "block";
            $scope.updateLineItemCompleteFlag = false;
            $http.post('/api/Quotes/' + $scope.quoteKey + '/updateQuoteline', e.data).then(

                function (response) {
                    if (response.data.error == "") {
                        $scope.updateLineItemCompleteFlag = true;
                        if ($scope.formquote.$dirty) {
                            var oldObj = $scope.Quotation;

                            $scope.Quotation.DiscountType = response.data.data.quote.DiscountType;
                            $scope.Quotation.DiscountId = response.data.data.quote.DiscountId;
                            $scope.Quotation.Discount = response.data.data.quote.Discount;
                            $scope.Quotation.QuoteStatusId = response.data.data.quote.QuoteStatusId;
                            $scope.Quotation.PrimaryQuote = response.data.data.quote.PrimaryQuote;
                            $scope.Quotation.QuoteName = response.data.data.quote.QuoteName;
                            //  $scope.formquote.$dirty = true;
                        } else {
                            $scope.Quotation = response.data.data.quote;
                            setQuoteHeaderDiscountValue();
                            $scope.QuotationCopy = angular.copy($scope.Quotation);
                        }

                        $scope.QuotelineDatasource = response.data.data.quoteLines;
                        $scope.DiscountAndPromo = response.data.data.discountAndPromo;
                        //getQuotelines();

                        $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                        //$('#QuotelinesGrid').data('kendoGrid').refresh();
                        if (!$scope.CopyFlag)
                            $scope.loadingElement.style.display = "none";

                        $scope.Quotation.TaxDetails = response.data.data.quote.TaxDetails;
                        $scope.ResetQuoteTax();
                        HFC.DisplaySuccess("Quote Modified");
                        //$scope.QuoteinEditMode = false;
                    } else {
                        HFC.DisplayAlert(response.responseJSON.error);
                    }
                    if (!$scope.CopyFlag)
                        $scope.loadingElement.style.display = "none";
                    if ($scope.CopyFlag) {
                        //$scope.loadingElement.style.display = "block";
                        $scope.CopyPrevFlag = $scope.CopyFlag;
                        $scope.CopyQuoteline($scope.reserveCopyData);
                        $scope.CopyFlag = false;
                    }
                    // getQuotelines();
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";

                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
        }

        //warning popup

        $scope.DisplayPopup = function () {
            $("#Warninginfo").modal("show");
            return;
        }
        //cancel the pop-up
        $scope.CancelPopup = function (modalId) {
            $("#" + modalId).modal("hide");
            $scope.Quotation.QuoteStatusId = $scope.QuotationCopy.QuoteStatusId;
        };

        $http.get('/api/lookup/0/GetQuoteStatus').then(function (data) {
            $scope.QuoteStatusList = data.data;
        }).catch(function (error) {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "none";

            //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
        });
        function CostTemplate(dataItem) {
            if (dataItem.Cost === null) {
                return '<span class="Grid_Textalign"><a class="hquote_hover"  ng-click="TLCost(dataItem) " name="#=QuoteLineId#" value="#=Cost#">$0.00</a></span>'
            } else {
                return '<span class="Grid_Textalign"><a class="hquote_hover"  ng-click="TLCost(dataItem) " name="' +
                    dataItem.QuoteLineId +
                    '" value="' +
                    dataItem.Cost +
                    '">$' +
                    Number(dataItem.Cost).toFixed(2) +
                    '</a></span>';
            }

            if (dataItem.ProductTypeId === 5 && $scope.QuoteinEditMode && !$scope.Quotation.OrderExists && $scope.Quotation.QuoteStatusId != 3) {
                return '<span class="Grid_Textalign"><a class="hquote_hover" name="' +
                    dataItem.QuoteLineId +
                    '" value="' +
                    dataItem.Cost +
                    '">$' +
                    Number(dataItem.Cost).toFixed(2) +
                    '</a></span>';
            } else {
                return '<span class="Grid_Textalign">' + '$' +
                    Number(dataItem.Cost).toFixed(2) + '</span>';
            }
        }

        $scope.ConfigureServiceOrDiscount = function () {
            window.location.href =
                '/#!/MyServiceConfigurator/' + $scope.OpportunityId + '/' + $scope.quoteKey;
        }
        $scope.ConfigureMyProducts = function () {
            window.location.href =
                '/#!/myProductConfigurator/' + $scope.OpportunityId + '/' + $scope.quoteKey;
        }

        $scope.ConfigureCoreProducts = function () {
            window.location.href =
                '/#!/CoreProductconfigurator/' + $scope.OpportunityId + '/' + $scope.quoteKey;
        }

        function UpdateTLQuoteLine(TLdata) {
            $scope.loadingElement.style.display = "block";
            if ($scope.list.productList != null && (TLdata.ProductTypeId === 1 || TLdata.ProductTypeId === 2 || TLdata.ProductTypeId === 3 || TLdata.ProductTypeId === 5)) {
                var returnedData = $.grep($scope.list.productList, function (element, index) {
                    return element.ProductKey == TLdata.ProductId;
                });
                //if (returnedData.length>0 && returnedData[0].SalePrice !== TLdata.UnitPrice && Number(TLdata.UnitPrice) > 0) {
                //    TLdata.ManualUnitPrice = true;
                //}
            }
            $scope.updateLineItemCompleteFlag = false;

            //e.data.ProductCategoryId = e.data.CategoryKendo;
            //e.data.ProductSubCategoryId = e.data.SubCategoryKendo;
            $http.post('/api/Quotes/' + $scope.quoteKey + '/updateQuotelineTL', TLdata).then(
                function (response) {
                    $scope.updateLineItemCompleteFlag = true;
                    if (response.data.error == "") {
                        if ($scope.formquote.$dirty) {
                            var oldObj = $scope.Quotation;

                            $scope.Quotation.DiscountType = response.data.data.quote.DiscountType;
                            $scope.Quotation.DiscountId = response.data.data.quote.DiscountId;
                            $scope.Quotation.Discount = response.data.data.quote.Discount;
                            $scope.Quotation.QuoteStatusId = response.data.data.quote.QuoteStatusId;
                            $scope.Quotation.PrimaryQuote = response.data.data.quote.PrimaryQuote;
                            $scope.Quotation.QuoteName = response.data.data.quote.QuoteName;
                            //  $scope.formquote.$dirty = true;
                        } else {
                            $scope.Quotation = response.data.data.quote;
                            setQuoteHeaderDiscountValue();
                            $scope.QuotationCopy = angular.copy($scope.Quotation);
                        }

                        $scope.QuotelineDatasource = response.data.data.quoteLines;
                        $scope.DiscountAndPromo = response.data.data.discountAndPromo;

                        $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                        $('#QuotelinesGrid').data('kendoGrid').refresh();
                        $scope.ResetQuoteTax();
                        //This code is resetting the value of Quote Key as undefined
                        // $scope.Quotation.QuoteKey = response.data.data.quoteKey;
                        // $scope.loadingElement.style.display = "none";
                        if ($scope.bringQuotelineEditMode != 0 &&
                            $scope.bringQuotelineEditMode != undefined &&
                            $scope.bringQuotelineEditMode != null) {
                            $scope.CollapseAllKendoGridDetailRows("QuotelinesGrid");
                            var grid = $("#QuotelinesGrid").data("kendoGrid");
                            var dataItem = $("#QuotelinesGrid").data("kendoGrid").dataSource
                                .get($scope.bringQuotelineEditMode);
                            //var row = $("#QuotelinesGrid").data("kendoGrid").tbody.find("tr[data-uid='" + dataItem.uid + "']");
                            var link = grid.tbody.find("tr[data-uid='" + dataItem.uid + "']").find("td.k-hierarchy-cell .k-icon");
                            var row = grid.tbody.find("tr[data-uid='" + dataItem.uid + "']");
                            grid.expandRow(row);

                            //link.click();
                            //$scope.childGridUpdate(); --child row is not there in tl/cc so commented out
                            var grdId = "#grd" + dataItem.QuoteLineId;
                            //var childgrid = $(grdId).data("kendoGrid");
                            grid.editRow(row);

                            //childgrid.editRow(dataItem);

                            $scope.bringQuotelineEditMode = 0;
                        }

                        HFC.DisplaySuccess("Quote Modified");
                        //$scope.ResetQuoteTax();
                        //$scope.QuoteinEditMode = false;
                    } else {
                        HFC.DisplayAlert(response.responseJSON.error);
                    }
                    if ($scope.saveBasicQuoteDetailFlag) {
                        $scope.loadingElement.style.display = "none";
                    }
                    else if ($scope.saveBasicQuoteDetailFlag == undefined) {
                        $scope.loadingElement.style.display = "none";
                    }
                    getQuotelines();
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";

                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
        }

        $scope.TLCost = function (dataItem) {
            if ($("#QuotelinesGrid").find("tr.k-grid-edit-row").data("uid") === dataItem.uid) {
                //just to display the Amount format
                if (dataItem.Cost == null) {
                    dataItem.Cost = "";
                }
                $scope.SelectedQuoteLine = dataItem;
                $scope.PreviousCost = $scope.SelectedQuoteLine.Cost;
                $scope.PreviousBaseResalePrice = $scope.SelectedQuoteLine.BaseResalePriceWOPromos;
                $scope.BasePriceCurrencyControl.CurrenctFormatControl($scope.SelectedQuoteLine.BaseResalePriceWOPromos);
                $scope.CostPriceCurrencyControl.CurrenctFormatControl($scope.SelectedQuoteLine.Cost);

                if ((dataItem.ProductTypeId === 5) &&
                    $scope.QuoteinEditMode &&
                    //!$scope.Quotation.OrderExists &&
                    $scope.Quotation.QuoteStatusId != 3) {
                    $("#TLQuoteCost").modal("show");
                }
            }
        }


        //added new code for the cost pop-up close.
        $scope.CancelTLCost = function (dataItem, id) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            var grddata = grid.dataSource.data();

            for (var i = 0; i < grddata.length; i++) {
                if (grddata[i].QuoteLineId === dataItem.QuoteLineId) {
                    grddata[i].Cost = $scope.PreviousCost;
                    grddata[i].BaseResalePriceWOPromos = $scope.PreviousBaseResalePrice;
                }
            }
            $("#TLQuoteCost").modal("hide");

        }

        $scope.SaveTLCost = function (dataItem) {
            var va;
            if (isNaN(dataItem.Cost)) {
                HFC.DisplayAlert("Enter Valid Cost");
                return false;
            }
            else if (Number(dataItem.Cost) <= 0) {
                HFC.DisplayAlert("Enter Valid Cost");
                return false;
            }
            if (isNaN(dataItem.BaseResalePriceWOPromos)) {
                HFC.DisplayAlert("Enter Valid Base Resale Price");
                return false;
            } else if (Number(dataItem.BaseResalePriceWOPromos) <= 0) {
                HFC.DisplayAlert("Enter Valid Base Resale Price");
                return false;
            }
            var quotelineId = dataItem.QuoteLineId;// Number($(this).attr('name'));
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            var grddata = grid.dataSource.data();
            //var grid = $scope.QuotelineDatasource;
            for (var i = 0; i < grddata.length; i++) {
                if (grddata[i].QuoteLineId === dataItem.QuoteLineId) {
                    grddata[i].Cost = dataItem.Cost;
                    grddata[i].BaseResalePriceWOPromos = dataItem.BaseResalePriceWOPromos;
                }
            }
            var dataSource = $("#QuotelinesGrid").data("kendoGrid").dataSource;
            var item = dataSource.getByUid(dataItem.uid);
            item.dirty = true;
            //grid.refresh();
            $scope.bringQuotelineEditMode = quotelineId;
            //var grid = $("#QuotelinesGrid").data("kendoGrid");
            //var validator = $("#QuotelinesGrid").kendoValidator().data("kendoValidator");
            var validator = $scope.kendoValidator("QuotelinesGrid");

            if (grid !== undefined && grid !== null)
                if (validator.validate()) {
                    if ($scope.TLValidateGrid()) {
                        grid.saveChanges();
                    }
                }
            //UpdateTLQuoteLine(dataItem);
            $("#TLQuoteCost").modal("hide");
        }
        $("#QuotelinesGrid").on("click", ".linkrule", function () {
            var SelectedgridRow;
            var quotelineId = Number($(this).attr('name'));
            var grid = $scope.QuotelineDatasource;
            //for (var i = 0; i < grid.length; i++) {
            //    if (grid[i].QuoteLineId === quotelineId) {
            //    }
            //}
            $scope.SelectedQuoteLine = data;

            $("#TLQuoteCost").modal("show");
        });

        function detailinitParent(e) {
            var grdId = "grd" + e.data.QuoteLineId;
            $("<div style='margin-right:20px;' id = '" + grdId + "'/>").appendTo(e.detailCell).kendoGrid({
                dataSource: [e.data],
                columns: [
                    { field: "VendorName", title: "Vendor", width: "80px" },
                    { field: "Description", title: "Item Description", editor: descriptionEditor, template: '#=Description#', width: "250px" },
                    { field: "SuggestedResale", title: "Suggested Resale", format: "{0:c}", width: "80px" },
                    { field: "UnitPrice", title: "Unit Price", format: "{0:c}", editor: unitPriceEditor, width: "80px" },
                    {
                        field: "Discount",
                        title: "Discount",
                        editor: DiscountEditor, width: "80px",
                        //editor:
                        //    '<input  data-type="number"  class="k-textbox"  name="Discount"/>',
                        template:

                            function (dataItem) {
                                if (dataItem.ProductTypeId == 4) {
                                    if (dataItem.DiscountType === "$") {
                                        return "$" + dataItem.Discount;;
                                    } else if (dataItem.DiscountType === "%") {
                                        return dataItem.Discount + "%";
                                    }
                                    else if (dataItem.DiscountType == null) {
                                        if (dataItem.Discount != null && dataItem.Discount != undefined) {
                                            return dataItem.Discount + "%";
                                        }
                                    } else {
                                        return '';
                                    }
                                } else {
                                    if (dataItem.Discount !== null && dataItem.Discount !== undefined && dataItem.Discount !== '' && dataItem.Discount > 0) {
                                        return dataItem.Discount + "%";
                                    } else {
                                        return '';
                                    }
                                }

                            },
                    },
                ],
                editable: "inline",
                detailInit: detailInit,
                dataBound: function (e) {
                    var rows = this.tbody.children();
                    var dataItems = this.dataSource.view();
                    for (var i = 0; i < dataItems.length; i++) {
                        var temp = dataItems[i];
                        if (temp["IsGroupDiscountApplied"] == true) {
                            $("#" + this.element[0].id + " th[data-field=Discount]").html("Discount" + '<i class="fas fa-tags pull-right" style="padding-top:5px"></i>');
                        }
                    }
                }
            });
        }

        function detailInit(e) {
            var disc = $scope.DiscountAndPromo;
            var exists = false;

            if (disc != undefined && disc != '' && disc.length > 0) {
                for (var i = 0; disc.length > i; i++) {
                    if (e.data.QuoteLineId == disc[i].QuoteLineId) {
                        exists = true;
                    }
                }
            }
            if (exists) {
                $("<div style='width: 50%;float: right;'id='discount_" + e.data.QuoteLineId + "'/>")
                    .appendTo(e.detailCell).kendoGrid({
                        dataSource: {
                            type: "json",
                            //transport: {
                            //    read: '/api/Quotes/' + $scope.quoteKey + '/GetDiscountDetails/'
                            //},
                            transport: {
                                read: function (ei) {
                                    ei.success($scope.DiscountAndPromo);
                                }
                            },
                            serverPaging: false,
                            serverSorting: false,
                            serverFiltering: false,

                            pageSize: 10,
                            batch: true,
                            filter: { field: "QuoteLineId", operator: "eq", value: e.data.QuoteLineId },
                            schema: {
                                model: {
                                    id: "Id",
                                    fields: {
                                        QuoteLineId: { editable: false },
                                        Description: { editable: false },
                                        Discount: { editable: false },
                                        DiscountPercentPassed: { editable: false },
                                        DiscountAmountPassed: { editable: false }
                                    }
                                }
                            }
                        },
                        //toolbar: [{
                        //    template: $("#toolbarTemplate1").html()
                        //}],
                        scrollable: false,
                        sortable: true,
                        noRecords: { template: "No records found" },
                        pageable: false,
                        editable: true,
                        resizable: true,
                        columns: [
                            { field: "QuoteLineId", title: "Line" },
                            { field: "Description", title: "Description" },
                            { field: "Discount", title: "Discount Value", format: "{0:c}" },
                            {
                                field: "DiscountPercentPassed",
                                title: "Extended Discount",
                                template: '#=kendo.format("{0:p}", DiscountPercentPassed/100)#'
                            },
                            { field: "DiscountAmountPassed", title: "Extended Value", format: "{0:c}" }
                        ]
                    });
            }
        }
        //style=" " name="PricingStrategyTypeId" data-bind="value:PricingStrategyTypeId"

        function descriptionEditor(container, options) {
            var descValue = options.model.Description;
            var description = "";
            var offset = 0;
            var flag = false;
            while (descValue.indexOf("<") != -1) {
                flag = true;
                var startTagIndex = descValue.indexOf("<");
                var endTagIndex = descValue.indexOf(">");
                description += descValue.substr(offset, startTagIndex);
                descValue = descValue.substr(endTagIndex + 1, descValue.length);
            }
            if (flag) {
                options.model.Description = description;
            }
            if (options.model.ProductTypeId === 1) {
                $(
                    '<input data-type="text"  style=" " id="QuoteDescription"    class="k-textbox"  disabled name="Description"/>')
                    .appendTo(container);
            } else {
                $(
                    '<input data-type="text" id="QuoteDescription"  style=" "  class="k-textbox"  name="Description"/>')
                    .appendTo(container);
            }
        }

        function unitPriceEditor(container, options) {
            if (options.model.ProductTypeId === 4) {
                //'<input disabled class="prdDetails currency"  style="width: 25% !important; display: inline !important;" data-bind="value:UnitPrice" disabled name="UnitPrice" data-role="numerictextbox [format]="c2"/>' +
                $('<input min="0" data-type="number" class="currency" culture="en-US" id ="uniitPrice"  data-bind="value:UnitPrice" disabled name="UnitPrice" data-role="numerictextbox" [format]="c2"/>')
                    .appendTo(container);
            } else {
                // TP-1719 : Unable to save blank quote line.
                // Unit price is not a required field - Amelia.
                //$('<input class="currency" id ="uniitPrice"  culture="en-US" data-bind="value:UnitPrice" required name="UnitPrice" data-role="numerictextbox" [format]="c2" data-spinners="false"/>')
                $('<input  min="0" class="currency" id ="uniitPrice"  culture="en-US" data-bind="value:UnitPrice" name="UnitPrice" data-role="numerictextbox" [format]="c2" data-spinners="false"/>')
                    .appendTo(container);
            }
        }

        //check status
        $scope.CheckStatus = function () {

            //$(".hfc_bgbut").hide();
            if ($scope.Quotation.QuoteStatusId == 5) {
                $scope.VoidPopup();
            }
            else {
                $scope.SaveQuote();
            }
            // $scope.revokeSortableRow();
            $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "block");
        }

        $scope.VoidSave = function () {
            // $scope.CancelPopup('VoidWarning');
            $("#VoidWarning").modal("hide");
            $scope.SaveQuote();
        }

        //Void warning popup

        $scope.VoidPopup = function () {
            $("#VoidWarning").modal("show");
        }

        function DiscountEditor(container, options) {
            if (options.model.ProductTypeId === 4) {

                var format = '%';

                if (options.model.DiscountType != null && options.model.DiscountType != undefined) {
                    format = options.model.DiscountType;
                }
                if (format === '%') {

                    format = "p0";

                    options.model.Discount = options.model.Discount / 100.00;
                } else {
                    format = format + "#";
                }

                $('<input id="Discountedit" required name="Discount" disabled data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: format,//"# \\%",
                        decimals: 2,
                        min: 0, spinners: false
                    });

                ////'<input disabled class="prdDetails currency"  style="width: 25% !important; display: inline !important;" data-bind="value:UnitPrice" disabled name="UnitPrice" data-role="numerictextbox [format]="c2"/>' +
                //$(
                //        '<input data-type="number" class="currency" culture="en-US" id ="Discountedit"  required data-bind="value:Discount" disabled name="Discount" data-role="numerictextbox" [format]="c2"/>')
                //    .appendTo(container);
            } else {
                //$(
                //        '<input class="currency" id ="Discountedit"  culture="en-US" data-bind="value:Discount"  name="Discount" data-role="numerictextbox" [format]="c2" data-spinners="false"/>')
                //    .appendTo(container);
                var disabledtext = "";
                //if ((options.model.ProductTypeId == 1 || options.model.ProductTypeId == 2) && options.model.IsGroupDiscountApplied) {
                //    disabledtext = "disabled";
                //}
                $('<input id="Discountedit" name="Discount" ' + disabledtext + '  data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: format,//"# \\%",
                        decimals: 2,
                        min: 0, spinners: false
                    });
            }
        }

        $scope.validateQuantity = function (e) {
            var cId = e.currentTarget.id;
            var value = $("#" + cId).val();
            var parent1 = $("#" + cId).parent()[0];
            var parentElement = $(parent1).parent()[0];
            if (value == "" || value == 0) {
                $(parentElement).addClass("required-error");
                $("#" + cId).val('');
                $("#" + cId).focus();
            }
            else
                $(parentElement).removeClass("required-error");
        }

        function quantityEditor(container, options) {
            //
            if (options.model.ProductTypeId === 4 || options.model.ProductTypeId === 0 || options.model.ProductTypeId === null || options.model.ProductTypeId === undefined || options.model.ProductTypeId === '') {
                $('<input data-type="number"  data-bind="value:Quantity" disabled name="Quantity"  id="qty"/>')
                    .appendTo(container).kendoNumericTextBox({
                        //format: format,//"# \\%",
                        decimals: 0,
                        min: 0, spinners: false,
                    });
            } else {
                $('<input data-type="number" data-bind="value:Quantity" required name="Quantity" id="qty"/>')
                    .appendTo(container).kendoNumericTextBox({
                        format: "#",
                        spinners: false,
                        decimals: 0,
                        min: 0,
                    });
            }
            $("#qty").keyup(function (e) {
                $scope.validateQuantity(e);
            });
        }

        function quantityEditorTL(container, options) {
            if (options.model.ProductTypeId === 4) {
                //$(' <input id="qlQuantity" kendo-numeric-text-box k-min="0"  data-bind="value:Quantity"  disabled name="Quantity" k-spinners= "false"  data-decimals="0"/>')
                $('<input data-type="number"  data-bind="value:Quantity" disabled name="Quantity" id="qlQuantity"/>')
                    .appendTo(container).kendoNumericTextBox({
                        //format: format,//"# \\%",
                        decimals: 0,
                        min: 0, spinners: false
                    });
                options.model.Quantity = null;
            } else {
                //$(' <input id="qlQuantity" kendo-numeric-text-box k-min="0" format= "n0"  data-bind="value:Quantity" name="Quantity" k-spinners= "false"  data-decimals="0"/>')
                $('<input data-type="number" data-bind="value:Quantity" required name="Quantity" id="qlQuantity" />')
                    .appendTo(container).kendoNumericTextBox({
                        format: "#",
                        spinners: false,
                        decimals: 0,
                        min: 0,
                        required: true
                    });
            }

            $("#qlQuantity").keyup(function (e) {
                $scope.validateQuantity(e);
            });
        }



        function costEditorTL(container, options) {
            if (options.model.ProductTypeId === 4) {
                $(' <input id="qlCost" kendo-numeric-text-box k-min="0" data-bind="value:Cost" disabled name="Cost" k-spinners= "false"   />')
                    .appendTo(container);
            } else {
                $(' <input id="qlCost" kendo-numeric-text-box k-min="0" data-bind="value:Cost" name="Cost" k-spinners= "false"   />')
                    .appendTo(container);
            }
        }

        function discountEditorTL(container, options) {
            if (options.model.ProductTypeId === 4) {

                var format = '%';

                if (options.model.DiscountType != null && options.model.DiscountType != undefined) {
                    format = options.model.DiscountType;
                }
                if (format === '%') {

                    format = "p0";
                    options.model.Discount = options.model.Discount / 100;
                } else {
                    format = format + "#";
                }

                $('<input id="qlDiscount" required name="Discount" disabled data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: format,//"# \\%",
                        decimals: 2,
                        min: 0, spinners: false
                    });

                ////'<input disabled class="prdDetails currency"  style="width: 25% !important; display: inline !important;" data-bind="value:UnitPrice" disabled name="UnitPrice" data-role="numerictextbox [format]="c2"/>' +
                //$(
                //        '<input data-type="number" class="currency" culture="en-US" id ="Discountedit"  required data-bind="value:Discount" disabled name="Discount" data-role="numerictextbox" [format]="c2"/>')
                //    .appendTo(container);
            } else {
                //$(
                //        '<input class="currency" id ="Discountedit"  culture="en-US" data-bind="value:Discount"  name="Discount" data-role="numerictextbox" [format]="c2" data-spinners="false"/>')
                //    .appendTo(container);

                $('<input id="qlDiscount" name="Discount"  data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: format,//"# \\%",
                        decimals: 2,
                        min: 0, spinners: false
                    });
            }




            //$('<input id="qlDiscount" name="Discount" data-bind="value:' + options.field + '"/>')
            //    .appendTo(container)
            //    .kendoNumericTextBox({
            //        format: "# \\%",
            //        min: 0, spinners: false
            //    });

            //$(' <input id="qlDiscount" kendo-numeric-text-box k-min="0" data-bind="value:Discount" name="Discount" k-spinners= "false"  />')
            //.appendTo(container);
        }

        function unitEditorTL(container, options) {
            if (options.model.ProductTypeId === 4) {
                $(' <input id="qlUnitPrice" disabled data-bind="value:UnitPrice" name="UnitPrice" />')
                    .appendTo(container).kendoNumericTextBox({
                        format: "$#.00",
                        spinners: false,
                        min: 0
                    });
            } else {
                $(' <input id="qlUnitPrice" data-bind="value:UnitPrice" name="UnitPrice" />')
                    .appendTo(container).kendoNumericTextBox({
                        format: "$#.00",
                        spinners: false,
                        min: 0
                    });
            }
        }

        function suggestedresaleEditorTL(container, options) {
            //if (options.model.ProductTypeId === 4) {
            //    $(' <input id="qlUnitPrice" kendo-numeric-text-box k-min="0" disabled data-bind="value:UnitPrice" name="UnitPrice" k-spinners= "false"  [format]="$#.00"  />')
            //        .appendTo(container);
            //} else {
            $(' <input id="qlSuggestedResale" data-bind="value:SuggestedResale" name="SuggestedResale"/>')
                .appendTo(container).kendoNumericTextBox({
                    format: "$#.00",
                    spinners: false,
                    min: 0
                });
            //   }
        }

        $scope.CollapseAllKendoGridDetailRows = function (prmGridId) {
            //Get a collection of all rows in the grid
            var grid = $('#' + prmGridId).data('kendoGrid');
            var allMasterRows = grid.tbody.find('>tr.k-master-row');

            //Loop through each row, if a row has a detail row following it then collapse that master row
            for (var i = 0; i < allMasterRows.length; i++) {
                if (allMasterRows.eq(i).next('tr.k-detail-row').length > 0) {
                    grid.collapseRow(allMasterRows.eq(i));
                }
            }
        }

        $scope.editUpdateQuoteLines = function () {

            var grid = $("#QuotelinesGrid").data("kendoGrid");
            //var validator = $("#QuotelinesGrid").kendoValidator().data("kendoValidator");
            var validator = $scope.kendoValidator("QuotelinesGrid");

            if (grid !== undefined && grid !== null && grid.dataSource.hasChanges()) {
                if (validator.validate()) {
                    if ($scope.TLValidateGrid()) {
                        if (!$scope.CopyFlag && $scope.Copyclick) {
                            $scope.CopyFlag = true;
                            $scope.Copyclick = false;
                        }
                        grid.saveChanges();
                        return true;
                    } else {
                        var errors = validator.errors();
                        return false;
                    }
                }
                else {
                    var errors = validator.errors();
                    return false;
                }
            }
            else
                return true;
        }

        $scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                rules: {
                    Quantityvalidation: function (input) {
                        if (input.is("[name='Quantity']")) {
                            //input.attr("data-Quantityvalidation-msg", "Product Name should start with capital letter");
                            if (input.val() === "" ||
                                input.val() === 0 ||
                                input.val() === null ||
                                input.val() === undefined) {
                                input.attr("data-Quantityvalidation-msg", "Quantity can not be empty.");
                                input.addClass("required-error");
                                return false;
                            } else {
                                return true
                            }
                        }
                        return true;
                    }
                },
                validate: function (e) {
                    $("span.k-invalid-msg").hide();
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find(".k-widget.k-dropdown.k-header");
                        if (input.size() > 0) {
                            if (input.prop('disabled') != true)
                                $(this).addClass("dropdown-validation-error");
                        } else {
                            $(this).removeClass("dropdown-validation-error");
                        }
                    });
                }
            }).getKendoValidator();
        }

        $scope.TLValidateGrid = function () {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if (grid != undefined && grid !== null) {
                if ($scope.brandid === 2 || $scope.brandid === 3) {
                    for (var i = 0; i < grid.dataItems().length; i++) {
                        if (grid.dataItems()[i].ProductTypeId == 5) {
                            if (grid.dataItems()[i].Cost === 0 ||
                                grid.dataItems()[i].Cost === null ||
                                grid.dataItems()[i].Cost === undefined ||
                                grid.dataItems()[i].Cost === '' || isNaN(grid.dataItems()[i].Cost)) {
                                HFC.DisplayAlert("Please Enter Cost");
                                return false;
                                //break;
                            }
                            if (grid.dataItems()[i].BaseResalePriceWOPromos === 0 ||
                                grid.dataItems()[i].BaseResalePriceWOPromos === null ||
                                grid.dataItems()[i].BaseResalePriceWOPromos === undefined ||
                                grid.dataItems()[i].BaseResalePriceWOPromos === '' || isNaN(grid.dataItems()[i].BaseResalePriceWOPromos)) {
                                HFC.DisplayAlert("Please Enter Base Resale Price");
                                return false;
                                //break;
                            }
                        }
                    }
                    return true;
                } else {
                    return true;
                }
            }
        }

        //savequoteHeader
        $scope.saveQuoteHeader = function (dto) {
            $scope.QuoteSubmitted = true;
            $scope.Quotation.OpportunityId = $scope.OpportunityId;

            if ($scope.quoteKey === undefined) {
                $scope.quoteKey = 0;
            }

            var quoteName = $scope.Quotation.QuoteName;
            var sidemark = $scope.Quotation.SideMark;
            if (quoteName === undefined || quoteName === '' || quoteName == null) {
                HFC.DisplayAlert("Quote Name required.");
                $scope.Saveflag = false;
            }
            if (sidemark === undefined || sidemark === '' || sidemark == null) {
                HFC.DisplayAlert("Sidemark required.");
                $scope.Saveflag = false;
            }
            else if (dto.Discount != null && dto.DiscountType == null && dto.Discount !== '' && dto.Discount !== 0) {
                HFC.DisplayAlert("Discount Type cannot be Empty.");
                $scope.Saveflag = false;
            } else if ((dto.QuoteStatusId === 2 || dto.QuoteStatusId === 3) && dto.NumberOfQuotelines === 0) {
                $scope.Quotation.QuoteStatusId = 1;
                HFC.DisplayAlert("Quote Lines cannot be Empty.");
                $scope.Saveflag = false;

                //if (dto.NumberOfQuotelines > 0) {
                //}
            } else {
                $scope.loadingElement.style.display = "block";
                // $scope.formquote.$dirty = true;
                if ($scope.quoteKey !== null &&
                    $scope.quoteKey != undefined &&
                    $scope.quoteKey !== 0 &&
                    $scope.quoteKey !== '') {
                    dto.QuoteKey = $scope.quoteKey;
                }
                // rearrange quote lines nullify the dirty falg of form
                $scope.formquote.$setDirty();
                $scope.formquote.$setPristine(true);
                var loadingElement = $("#loading");
                if (loadingElement) {
                    loadingElement[0].style.display = "block";
                }
                $http.post('/api/Quotes/' + $scope.quoteKey + '/SaveQuote', dto).then(function (response) {
                    $scope.saveBasicQuoteDetailFlag = true;
                    $scope.Saveflag = false;
                    if (dto.QuoteStatusId == 5) {
                        //$scope.formquote.$setPristine();
                        var loadingElement = $("#loading");
                        if (loadingElement && $scope.updateLineItemCompleteFlag) {
                            loadingElement[0].style.display = "none";
                        }
                        else if (loadingElement && $scope.updateLineItemCompleteFlag == undefined) {
                            loadingElement[0].style.display = "none";
                        }
                        //$scope.loadingElement.style.display = "none";
                        $scope.QuoteinEditMode = false;
                        // $window.location.href = "#!/quoteSearch/" + $routeParams.OpportunityId;
                    } else {
                        if (response.data.error == '') {
                            $scope.Quotation = response.data.data;
                            setQuoteHeaderDiscountValue();
                            $scope.QuotationCopy = angular.copy($scope.Quotation);
                            //for making a copy of sale date
                            $scope.Copy = $scope.Quotation.SaleDate;
                            $scope.Quotation.QuoteKey = response.data.quoteKey;
                            var quoteGrid = $("#QuotelinesGrid").data("kendoGrid");

                            if (quoteGrid != null && quoteGrid !== null) {
                                //  quoteGrid.dataSource.read();
                                //   quoteGrid.refresh();
                            }

                            $scope.Quotation.TaxDetails = response.data.data.TaxDetails;
                            $scope.ResetQuoteTax();

                            $scope.QuoteinEditMode = false;

                            if (response.data.data.QuoteStatusId === 3) {
                                //$scope.formquote.$setPristine();
                                window.location.href = '/#!/quote/' + $scope.OpportunityId + '/' + response.data.quoteKey;
                                //$window.location.reload();
                            } else {
                                //$scope.formquote.$setPristine();
                                window.location.href =
                                    '/#!/Editquote/' + $scope.OpportunityId + '/' + response.data.quoteKey;
                            }
                        } else if (response.data.error === 'Duplicate Primary Quote Check') {
                            $scope.PrimaryQuoteName = response.data.data.QuoteName;
                            $scope.PrimaryQuoteId = response.data.data.PrimaryQuoteId;

                            if ($scope.QuoteKey === $scope.PrimaryQuoteId) {
                                if ($scope.Quotation.PrimaryQuote == false) {
                                    $scope.CannotChangePrimary = true;
                                }
                            }

                            $scope.showPopUp("PrimaryCheckmodal");

                            //$scope.PrimaryQuoteCheck ();
                        }

                        else {
                            HFC.DisplaySuccess(response.data.error);
                        }

                        //$scope.$apply();
                        // $scope.loadingElement.style.display = "none";
                        var loadingElement = $("#loading");
                        if (loadingElement && $scope.updateLineItemCompleteFlag) {
                            //loadingElement[0].style.display = "none";
                            getQuotelines();
                        } else if ($scope.updateLineItemCompleteFlag == undefined && loadingElement) {
                            //loadingElement[0].style.display = "none";
                            getQuotelines();
                        }
                    }
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";

                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
            }
        };
        //Saving Quote
        $scope.Saveflag = false;
        $scope.SaveQuote = function () {
            $scope.Saveflag = true;
            if ($scope.SetSale == true) {
                $scope.Quotation.SaleDate = $scope.Copy;
                //for validating date
                setTimeout(function () { $scope.ValidateDate($scope.Quotation.SaleDate, 'SaleDate'); }, 100);
            }
            var validator = $scope.kendoValidator("QuotelinesGrid");
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if (grid !== undefined && grid !== null) {
                var editRow = $("#QuotelinesGrid").find(".k-grid-edit-row");
                if (editRow.length > 0) {
                    if (!validator.validate()) {
                        return false;
                    }
                }

            }


            if (grid !== undefined && grid !== null && $("#QuotelinesGrid").find(".k-grid-edit-row").length > 0) {
                var editRow = $("#QuotelinesGrid").find(".k-grid-edit-row");
                var editedrowdata = $("#QuotelinesGrid").data("kendoGrid").dataSource.getByUid(editRow.data("uid"));
                if (editedrowdata.ProductTypeId == 1 || editedrowdata.ProductTypeId == 2 || editedrowdata.ProductTypeId == 3 || editedrowdata.ProductTypeId == 5) {
                    if ($('[name="Quantity"]').val() === "" && $('[name="Quantity"]') && $('[name="Quantity"]')[0] && !$('[name="Quantity"]')[0].disabled) {
                        return false;
                    }
                }
            }

            if ($scope.brandid === 1) {
                if (grid != undefined) {
                    $(".k-master-row").each(function (index) {
                        grid.collapseRow(this);
                    });
                    if (document.getElementById("btnCaret"))
                        document.getElementById("btnCaret").style.display = "block";
                    if (document.getElementById("btnCaret1"))
                        document.getElementById("btnCaret1").style.display = "none";
                }
            }

            //
            // $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
            $scope.QuoteSubmitted = true;
            //$scope.Quotation.DiscountType
            //$scope.Quotation.DiscountId
            //$scope.Quotation.Discount
            //$scope.Quotation.QuoteStatusId
            //$scope.Quotation.PrimaryQuote
            //$scope.Quotation.QuoteName

            if ($scope.Quotation.IsTaxExempt === false || ($scope.Quotation.IsTaxExempt === true && $scope.Quotation.TaxExemptID != "" && $scope.Quotation.TaxExemptID != null && $scope.Quotation.TaxExemptID != undefined)) {
                var dto = $scope.Quotation;

                if (!$scope.editUpdateQuoteLines())
                    return false;

                //var grid = $("#QuotelinesGrid").data("kendoGrid");
                //if (grid !== undefined && grid !== null) {
                //    $('#quotelineSave').click();
                //}

                var grid = $("#QuotelinesGrid").data("kendoGrid");
                //if (grid != undefined && grid.dataSource.hasChanges())
                //    $scope.formquote.$dirty = true;
                //var validator = $("#QuotelinesGrid").kendoValidator().data("kendoValidator");
                var validator = $scope.kendoValidator("QuotelinesGrid");

                //added dto.QuoteKey == 0 since initallay create quote from opportunity data is not saved
                //becaz $dirty is allways false initial save.
                if ($scope.formquote.$dirty || dto.QuoteKey == 0) {

                    if (grid !== undefined && grid !== null && grid.dataSource.hasChanges()) {
                        if (validator.validate()) {
                            if ($scope.TLValidateGrid())
                                $scope.saveQuoteHeader(dto);
                        }
                    } else {
                        $scope.saveQuoteHeader(dto);
                    }
                } else {
                    //if ($scope.Quotation.QuoteStatusId == 5) {
                    //    $scope.formquote.$setPristine();
                    //    $scope.loadingElement.style.display = "none";
                    //    $window.location.href = "#!/quoteSearch/" + $routeParams.OpportunityId;
                    //    // return;
                    //}

                    $scope.saveBasicQuoteDetailFlag = true;
                    if ($scope.updateLineItemCompleteFlag) {
                        $scope.loadingElement.style.display = "none";
                    }
                    $scope.QuoteinEditMode = false;

                    //Note:--- Commented out since it call the grid read multiple time and
                    //grid validation code already is there in  $scope.editUpdateQuoteLines();
                    //Which is there above in SaveQuote itself..

                    //---start----
                    //if (validator.validate()) {
                    //    if ($scope.TLValidateGrid()) {
                    //        $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                    //        $('#QuotelinesGrid').data('kendoGrid').refresh();

                    //        $scope.QuoteinEditMode = false;
                    //    }
                    //} else {
                    //    var errors = validator.errors();
                    //}
                    //--End----

                    //this seems to be needed now since when edit icon clicked and edit quote line
                    //dont change any thing in line level click save loader icon get stuck
                    if (!grid.dataSource.hasChanges()) {
                        $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                        $('#QuotelinesGrid').data('kendoGrid').refresh();
                        $scope.QuoteSubmitted = false;
                    }
                }
                //not sure why added?
                //$scope.loadingElement.style.display = "block";
                QuoteSubmitted = false;

            }
        };

        $scope.setAction = function () {
            $scope.SaveQuote();
        }

        $scope.OnModelSelected = function (value) {
            //var previousdate = $scope.Quotation.ExpiryDate;
            var status_val = value;
            if (status_val == 2) {
                if ($scope.Quotation.ExpiryDate == "")
                    $scope.Quotation.ExpiryDate = $scope.Quotation.PreviousExpirydate;
            }
            else {
                if ($scope.Quotation.PreviousExpirydate == undefined) {
                    $scope.Quotation.PreviousExpirydate = $scope.Quotation.ExpiryDate;
                }
                $scope.Quotation.ExpiryDate = "";
            }
        }

        // Create a new quote lines
        $scope.SaveMeasurementQuoteLine = function (data) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            grid.saveChanges();

            var quotelineobj = {
                QuoteID: $scope.quoteKey,
                QuoteLineList: []
            };

            var newqList = [];

            var mList = data.MeausurementList;
            var qList = data.QuoteLinesList;
            if (mList !== undefined && mList.length > 0) {
                for (var i = 0; i < mList.length; i++) {
                    var quoteline = {
                        measurmentSetId: 0,
                        selected: true,
                        RoomLocation: '',
                        RoomName: '',
                        MountTypeName: '',
                        AddemptyLine: 'N',
                        WindowLocation: '',
                        InternalNotes: '',
                    };
                    quoteline.measurmentSetId = mList[i].id;
                    quoteline.selected = mList[i].Selected;
                    quoteline.RoomLocation = mList[i].RoomLocation;
                    quoteline.RoomName = mList[i].RoomName;
                    quoteline.MountTypeName = mList[i].MountTypeName;
                    quoteline.WindowLocation = mList[i].WindowLocation;
                    quoteline.InternalNotes = mList[i].Comments;
                    newqList.push(quoteline);
                }
            }
            if ($scope.EQL.EmptyQuoteline === "Yes") {
                var quoteline = {
                    measurmentSetId: 0,
                    selected: true,
                    RoomLocation: '',
                    RoomName: '',
                    MountTypeName: '',
                    AddemptyLine: 'Y',
                    WindowLocation: '',
                    InternalNotes: ''
                };
                newqList.push(quoteline);
                $scope.EQL.EmptyQuoteline = "No";
            }
            quotelineobj.QuoteLineList = newqList;
            var postobj = JSON.stringify(quotelineobj);
            $scope.loadingElement.style.display = "block";
            $http.post('/api/Quotes/' + $scope.quoteKey + '/SaveQuoteLines', "'" + postobj.replace(/\'/g, '\\\'') + "'")
                .then(function (response) {
                    if (response.data.error == '') {
                        HFC.DisplaySuccess("Success");

                        $scope.QuotelineDatasource = new kendo.data.DataSource({
                            data: response.data.data,
                            //pageSize: 5
                        });
                        $scope.QuoteLinesList = response.data.data;

                        $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                        $('#QuotelinesGrid').data('kendoGrid').refresh();
                        //$("#QuotelinesGrid").data('kendoGrid').refresh();
                        //  $scope.formquote.$setDirty();
                    } else {
                        HFC.DisplayAlert(response.error);
                        $("#quotemodal").modal("hide");
                        $scope.loadingElement.style.display = "none";
                    }

                    // console.log(response);
                    var myDivq = $('.quotemeasurement_popup');
                    myDivq[0].scrollTop = 0;
                    $("#quotemodal").modal("hide");

                    $scope.loadingElement.style.display = "none";
                    //location.reload();

                    // make kendo grid editable to false for confirmation box on redirection
                    //grid.setOptions({ editable: false });
                    //if ($scope.formquote.quote_name.$dirty != true && $scope.formquote.Quote_status.$dirty != true && $scope.formquote.Quote_primaryquote.$dirty != true && $scope.formquote.quote_discount.$dirty != true && $scope.formquote.quote_disprice.$dirty != true && $scope.formquote.Quote_dis_currency.$dirty != true) {
                    //    $scope.formquote.$setPristine();
                    //}
                }).catch(function (error) {
                    var myDivq = $('.quotemeasurement_popup');
                    myDivq[0].scrollTop = 0;
                    $("#quotemodal").modal("hide");
                    $scope.loadingElement.style.display = "none";
                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
        };

        $scope.SaveMeasurementQuoteLineTL = function (data) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            grid.saveChanges();

            var quotelineobj = {
                QuoteID: data.QuoteKey,
                QuoteLineList: []
            };

            var newqList = [];

            if ($scope.EQL.EmptyQuoteline === "Yes") {
                var quoteline = {
                    measurmentSetId: 0,
                    selected: true,
                    RoomLocation: '',
                    RoomName: '',
                    MountTypeName: '',
                    AddemptyLine: 'Y',
                    WindowLocation: ''
                };
                newqList.push(quoteline);
                $scope.EQL.EmptyQuoteline = "No";
            }
            quotelineobj.QuoteLineList = newqList;
            var postobj = JSON.stringify(quotelineobj);
            $scope.loadingElement.style.display = "block";
            $http.post('/api/Quotes/' + $scope.quoteKey + '/SaveQuoteLines', "'" + postobj.replace(/\'/g, '\\\'') + "'")
                .then(function (response) {
                    if (response.data.error == '') {
                        HFC.DisplaySuccess("Success");

                        $scope.QuotelineDatasource = new kendo.data.DataSource({
                            data: response.data.data,
                            //pageSize: 5
                        });
                        $scope.QuoteLinesList = response.data.data;

                        $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                        $('#QuotelinesGrid').data('kendoGrid').refresh();
                        //$("#QuotelinesGrid").data('kendoGrid').refresh();
                    } else {
                        HFC.DisplayAlert(response.error);
                        //$("#quotemodal").modal("hide");
                        $scope.loadingElement.style.display = "none";
                    }
                    // console.log(response);
                    //$("#quotemodal").modal("hide");

                    $scope.loadingElement.style.display = "none";
                    //location.reload();
                }).catch(function (error) {
                    //$("#quotemodal").modal("hide");
                    $scope.loadingElement.style.display = "none";
                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
        }

        //Convert quote to Order
        $scope.ConvertQuoteOrder = function () {
            var installId = $scope.Quotation.InstallationAddressId;
            $scope.loadingElement.style.display = "block";
            $http.get('/api/opportunities/' + installId + '/getInstalltionAddressName').then(function (data) {
                $scope.loadingElement.style.display = "none";
                $scope.IsValidate = data.data.IsValidated;
                if ($scope.IsValidate == false) {
                    var errorMessage = 'The Installation Address is not validated, do you still wish to proceed?';
                    if (confirm(errorMessage)) {
                        $scope.loadingElement.style.display = "block";
                        $http.get('/api/Quotes/' + $scope.quoteKey + '/ConvertQuoteToOrder/').then(function (response) {
                            if (response.data.error === "") {
                                if (response.data.data > 0) {
                                    var orderId = response.data.data;
                                    $window.location.href = '#!/orderEdit/' + orderId;
                                }
                            } else {
                                if (response.data.PICTaxerror === true) {
                                    $scope.showPopUp("overWriteNoTax");
                                } else {
                                    $scope.message = response.data.error;
                                    $scope.showPopUp("QL_Validation");
                                }
                            }
                            $scope.loadingElement.style.display = "none";
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";

                            //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                        });
                    } else {
                        $scope.IsBusy = false;
                        $window.location.href = "#!/quote/" + $scope.OpportunityId + "/" + $scope.Quotation.QuoteKey;
                    }
                }
                else {
                    $scope.loadingElement.style.display = "block";
                    $http.get('/api/Quotes/' + $scope.quoteKey + '/ConvertQuoteToOrder/').then(function (response) {
                        if (response.data.error === "") {
                            if (response.data.data > 0) {
                                var orderId = response.data.data;
                                $window.location.href = '#!/orderEdit/' + orderId;
                            }
                        }
                        else {
                            if (response.data.PICTaxerror === true) {
                                $scope.Cancel("overWriteNoTax");
                            } else {
                                HFC.DisplayAlert(response.data.error);
                            }
                        }

                        $scope.loadingElement.style.display = "none";
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";

                        //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                    });
                }
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });
        }

        $scope.OverWriteConvertQuoteOrder = function () {
            $scope.Cancel("overWriteNoTax");
            //alert("Test complete.");
            $scope.loadingElement.style.display = "block";
            $http.get('/api/Quotes/' + $scope.quoteKey + '/ConvertQuoteToOrderTaxOverride/').then(function (response) {
                if (response.data.error === "") {
                    if (response.data.data > 0) {
                        var orderId = response.data.data;
                        $window.location.href = '#!/orderEdit/' + orderId;
                    }
                }
                else {
                    HFC.DisplayAlert(response.data.error);
                }
                $scope.loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });
        }

        $scope.PrimaryQuoteCheck = function () {
            $scope.PrimaryQuoteName = $scope.Quotation.PrimaryQuoteName;
            var pqId = $scope.Quotation.PrimaryQuoteId;
            var qk = $scope.Quotation.QuoteKey;
            var pq = $scope.Quotation.PrimaryQuote;

            if ($scope.QuoteKey === $scope.PrimaryQuoteId) {
                if ($scope.Quotation.PrimaryQuote == false) {
                    $scope.CannotChangePrimary = true;
                }
            }

            if ($scope.Quotation.PrimaryQuoteId && $scope.Quotation.PrimaryQuoteId > 0)
                $scope.showPopUp("PrimaryCheckmodal");
            else {
                if ($scope.quoteKey === undefined && $scope.CannotChangePrimary == true)
                    $scope.showPopUp("PrimaryCheckmodal");
                else
                    $scope.ChangePrimaryQuote($scope.Quotation, 'Yes');
            }

            // ($scope.quoteKey === undefined)

            //if ($scope.Quotation.PrimaryQuoteId === 0)
            //    $scope.showPopUp("PrimaryCheckmodal");
            //else {
            //    $scope.ChangePrimaryQuote($scope.Quotation, 'Yes');
            //}

            //$("#PrimaryCheckmodal").modal("show");
        }

        //Convert quote to Order
        $scope.ConvertQuoteOrderFromList = function (quoteKey) {
            $http.get('/api/Quotes/' + quoteKey + '/ConvertQuoteToOrder/').then(function (response) {
                if (response.data.error === "") {
                    //var url = "http://" + $window.location.host + "/#!/orderSearch/";
                    //$window.location.href = '#!/quoteSearch/';
                    //window.location.href = '#!/orderSearch'
                    //TP-1396
                    if (response.data.data > 0) {
                        var orderId = response.data.data;
                        $window.location.href = '#!/orderEdit/' + orderId;
                    }
                } else {
                    HFC.DisplayAlert(response.data.error);
                }
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });
        }
        $scope.CopyQuotePost = function (quoteKey, oppId, value) {
            $scope.loadingElement.style.display = "block";
            $http.get('/api/Quotes/' + oppId + '/CopyQuote?quoteKey=' + quoteKey + "&copyTaxExempt=" + value).then(function (response) {
                //

                if (response.data.error == '') {
                    HFC.DisplaySuccess("New quote line created.");

                    $scope.QuotelineDatasource = new kendo.data.DataSource({
                        data: response.data.data,
                        //pageSize: 5
                    });
                    $scope.QuoteLinesList = response.data.data;
                    getQuotelines();
                    $scope.getQuoteDataSource(true);
                    // $('#gridQuoteSearch').data('kendoGrid').dataSource.read();
                    // $('#gridQuoteSearch').data('kendoGrid').refresh();
                    //$("#QuotelinesGrid").data('kendoGrid').refresh();
                } else {
                    HFC.DisplayAlert(response.error);
                }
                $scope.loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";

                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
            });
        }
        $scope.OverwriteQuoteTaxExempt = function () {
            $("#TaxExempt_Warninginfo_Copy").modal("hide");
            $scope.CopyQuotePost($scope.selectedQUoteKeyTaxExempt, $scope.SelectedOpportunityTaxExempt, true);
        }
        $scope.DoNotOverwriteTax = function () {
            $("#TaxExempt_Warninginfo_Copy").modal("hide");
            $scope.CopyQuotePost($scope.selectedQUoteKeyTaxExempt, $scope.SelectedOpportunityTaxExempt, false);
        }
        $scope.TaxExemptMsg = "";
        $scope.selectedQUoteKeyTaxExempt = 0;
        $scope.SelectedOpportunityTaxExempt = 0;
        $scope.CopyQuote = function (quoteKey, oppId) {
            $scope.selectedQUoteKeyTaxExempt = quoteKey;
            $scope.SelectedOpportunityTaxExempt = oppId;
            var oppo = $scope.opportunityvalue;
            var sQuote = $scope.quoteDataSource;
            var qToCopy = jQuery.grep(sQuote, function (obj) {
                return obj.QuoteKey === quoteKey;
            })[0];

            var oppotaxExempt = false, quoteTaxexempt = false;;

            if ($scope.opportunityvalue.IsGSTExempt === true
                || $scope.opportunityvalue.IsHSTExempt === true
                || $scope.opportunityvalue.IsPSTExempt === true
                || $scope.opportunityvalue.IsTaxExempt === true
                || $scope.opportunityvalue.IsVATExempt === true) {
                oppotaxExempt = true;
            }

            if (qToCopy.IsGSTExempt === true
                || qToCopy.IsHSTExempt === true
                || qToCopy.IsPSTExempt === true
                || qToCopy.IsTaxExempt === true
                || qToCopy.IsVATExempt === true) {
                quoteTaxexempt = true;
            }
            //var value = false;
            //var msg = "  The Account/Opportunity is marked as Tax Exempt. Would you like to mark the quote as Tax Exempt?";
            $scope.TaxExemptMsg = "";
            if (oppotaxExempt && !quoteTaxexempt) {
                $scope.TaxExemptMsg = "  The Account/Opportunity is marked as Tax Exempt. Would you like to mark the quote as Tax Exempt?";
            } else if (!oppotaxExempt && quoteTaxexempt) {
                $scope.TaxExemptMsg = "  The Opportunity is not marked as Tax Exempt. Would you like to mark the quote as Taxable?";
            }

            if ($scope.TaxExemptMsg !== '') {
                $("#TaxExempt_Warninginfo_Copy").modal("show");
            } else {
                $scope.CopyQuotePost(quoteKey, oppId, false);
            }
            //$scope.loadingElement.style.display = "block";

            //if (confirm(msg)) {
            //    //value = true;//copyOppoTaxExempt
            //    $scope.CopyQuotePost(quoteKey, oppId, true);
            //} else {
            //    //value = false;//MakeQuoteTaxable
            //    $scope.CopyQuotePost(quoteKey, oppId, false);
            //}
        }
        $scope.SetPrimaryfromList = function (quoteKey) {
            $http.get('/api/Quotes/' + quoteKey + '/ChangePrimaryQuote?opportunityId=' + $scope.OpportunityId).then(
                function (response) {
                    if (response.data.error === "") {
                        $scope.getQuoteDataSource(true);
                        // $('#gridQuoteSearch').data('kendoGrid').dataSource.read();
                        // $('#gridQuoteSearch').data('kendoGrid').refresh();
                        HFC.DisplaySuccess("Primary quote updated")
                    } else {
                        $scope.Quotation.PrimaryQuote = false;
                        HFC.DisplayAlert(response.data.error);
                    }
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";

                    //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
        }

        $scope.ChangePrimaryQuote = function (data, btnType) {
            if (btnType === "Yes") {
                if ($scope.quoteKey === undefined) {
                    $scope.Quotation.PrimaryQuote = true;
                    $scope.Cancel("PrimaryCheckmodal");
                } else {
                    $http.get('/api/Quotes/' +
                        $scope.quoteKey +
                        '/ChangePrimaryQuote?opportunityId=' +
                        $scope.OpportunityId).then(function (response) {
                            if (response.data.error === "") {
                                HFC.DisplaySuccess("Primary quote updated")
                            } else {
                                $scope.Quotation.PrimaryQuote = false;
                                HFC.DisplayAlert(response.data.error);
                            }
                            $scope.Cancel("PrimaryCheckmodal");
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";

                            //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                        });
                }
            } else {
                $scope.Quotation.PrimaryQuote = false;
                $scope.Cancel("PrimaryCheckmodal");
            }
        }

        $scope.CancelChangePrimaryQuote = function () {
            $scope.CannotChangePrimary = false;
            $scope.Quotation.PrimaryQuote = true;
            $scope.Cancel("PrimaryCheckmodal");
        }

        //$scope.setdirtyOrNot = function () {
        //    //
        //    if ($scope.formquote.$dirty != true) {
        //        if ($('#QuotelinesGrid').find('.k-grid-edit-row').length)
        //            $scope.formquote.$dirty = true;
        //        else
        //            $scope.formquote.$setPristine();
        //    }

        //};
        $scope.setdirtyOrNot = function () {
            if ($scope.formquote)
                if ($scope.formquote.$dirty != true) {
                    var grid = $("#QuotelinesGrid").data("kendoGrid");

                    //
                    if (grid != undefined && grid.dataSource.hasChanges())
                        if ($scope.QuoteSubmitted != true)
                            $scope.formquote.$dirty = true;
                        else
                            $scope.formquote.$setPristine();
                    else
                        $scope.formquote.$setPristine();
                }
        };

        $scope.$on('$routeChangeStart', function ($event, next, current) {
            $scope.setdirtyOrNot();
        });
        var initFlag = false;
        // track the route change for detection of change in reorder quote lines
        $scope.$on('$routeChangeStart', function ($event, next, current) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if ($scope.rowSortFlag && grid != undefined && !grid.dataSource.hasChanges() && !initFlag) {
                $scope.formquote.$dirty = false;
                initFlag = true;
                if (confirm('You will lose unsaved changes if you leave this page')) {
                    $scope.rowSortFlag = false;
                    var grid = $("#QuotelinesGrid").data("kendoGrid");
                    if (grid) {
                        grid.dataSource.read();
                    }
                } else {
                    $event.preventDefault();
                    //  $scope.setRowAsSortable();
                }
            } else if (initFlag && current && current.$$route && current.$$route.originalPath == "/quote/:OpportunityId/:quoteKey") {
                $event.preventDefault();
                initFlag = false;
            }
        });

        $scope.onQuoteCancel = function () {
            $scope.ValidDate = true;
            $scope.selectedMeasurementForAccount = [];

            //sale date
            if ($scope.SetSale == true) {
                $scope.SetSale = false;
                $scope.Quotation.SaleDate = $scope.Copy;
                //for validating date
                $scope.ValidateDate($scope.Quotation.SaleDate, 'SaleDate');
            }

            var grid = $("#QuotelinesGrid").data("kendoGrid");
            if ($scope.brandid === 1) {
                if (grid != undefined) {
                    $(".k-master-row").each(function (index) {
                        grid.collapseRow(this);
                    });
                    if (document.getElementById("btnCaret"))
                        document.getElementById("btnCaret").style.display = "block";
                    if (document.getElementById("btnCaret1"))
                        document.getElementById("btnCaret1").style.display = "none";
                }
            }

            var qk = $scope.Quotation.QuoteKey;
            var url;
            if (qk === 0) {
                url = "#!/quoteSearch/" + $scope.OpportunityId;
                $window.location.href = url;
            } else {
                var url = "#!/quote/" + $scope.OpportunityId + "/" + $scope.Quotation.QuoteKey;

                if ($scope.formquote.$dirty === true) {
                    if (confirm('You will lose unsaved changes if you leave this page')) {
                        $scope.QuoteinEditMode = false;
                        getQuotelines();
                        $scope.Quotation = $scope.QuotationCopy;
                        setQuoteHeaderDiscountValue();
                        $scope.Quotation.PreviousSaleDate = $scope.PreviousSaleDateCopy;
                        $scope.Quotation.SaleDateCreatedByName = $scope.SaleDateCreatedByNameCopy;
                        $scope.Quotation.SaleDateCreatedOn = $scope.SaleDateCreatedOnCopy;
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        if (grid !== undefined) {
                            grid.cancelChanges();
                            grid.dataSource.read();
                        }
                        $scope.formquote.$setPristine();
                        $window.location.href = url;
                        // $scope.revokeSortableRow();
                        $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "block");
                        // $scope.destroySortable();
                    } else {
                        // $scope.setRowAsSortable();
                    }
                }
                else {
                    if ($("#QuotelinesGrid").data("kendoGrid").dataSource.hasChanges()) {
                        if (confirm('You will lose unsaved changes if you leave this page')) {
                            $scope.QuoteinEditMode = false;
                            getQuotelines();
                            $scope.Quotation = $scope.QuotationCopy;
                            setQuoteHeaderDiscountValue();
                            $scope.Quotation.PreviousSaleDate = $scope.PreviousSaleDateCopy;
                            $scope.Quotation.SaleDateCreatedByName = $scope.SaleDateCreatedByNameCopy;
                            $scope.Quotation.SaleDateCreatedOn = $scope.SaleDateCreatedOnCopy;
                            var grid = $("#QuotelinesGrid").data("kendoGrid");
                            if (grid !== undefined) {
                                grid.cancelChanges();
                                grid.dataSource.read();
                            }
                            $scope.formquote.$setPristine();
                            $window.location.href = url;
                            // $scope.revokeSortableRow();
                            $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "block");
                            // $scope.destroySortable();
                        } else {
                            // $scope.setRowAsSortable();
                        }
                    }
                    else {
                        $scope.QuoteinEditMode = false;
                        getQuotelines();
                        $scope.Quotation = $scope.QuotationCopy;
                        setQuoteHeaderDiscountValue();
                        $scope.Quotation.PreviousSaleDate = $scope.PreviousSaleDateCopy;
                        $scope.Quotation.SaleDateCreatedByName = $scope.SaleDateCreatedByNameCopy;
                        $scope.Quotation.SaleDateCreatedOn = $scope.SaleDateCreatedOnCopy;
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        if (grid !== undefined) {
                            grid.cancelChanges();
                        }
                        $scope.formquote.$setPristine();
                        $window.location.href = url;
                        // $scope.revokeSortableRow();
                        $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "block");
                    }
                }
            }
        };
        //$scope.onQuoteCancel = function () {
        //    var qk = $scope.Quotation.QuoteKey;
        //    var url; //= "http://" + $window.location.host
        //    if (qk === 0) {
        //        url = "/#!/quoteSearch/" + $scope.OpportunityId;

        //    } else {
        //        $scope.QuoteinEditMode = false;
        //        var url = "/#!/quote/" + $scope.OpportunityId + "/" + $scope.Quotation.QuoteKey;
        //    }
        //    $window.location.href = url;
        //};

        function onDiscountchange(e) {
            var ddlselect = this.dataItem();
            var grid = e.sender.element.closest(".k-grid").data("kendoGrid");
            var row = e.sender.element.closest("tr");
            var dataItem = grid.dataItem(row);

            row.find('[name="Quantity"]').prop("disabled", true);

            var grdId = "#grd" + dataItem.QuoteLineId;
            var childgrid = $(grdId).data("kendoGrid");
            var childRow = childgrid.tbody.find(">tr:first");

            childRow.find('[name="UnitPrice"]').prop("disabled", true);
            childRow.find('[name="Discount"]').prop("disabled", true);
            $scope.childGridUpdate();
            // Setting the discountID
            for (var i = 0; i < grid.dataItems().length; i++) {
                if (grid.dataItems()[i].QuoteLineId === Number(dataItem.QuoteLineId)) {
                    dataItem.ProductName = ddlselect.Name;
                    dataItem.ProductId = Number(ddlselect.DiscountId);
                    dataItem.ProductName = ddlselect.Name;
                    dataItem.Description = ddlselect.Description;
                    dataItem.Discount = ddlselect.DiscountValue;

                    //This is for Discount items
                    dataItem.ProductTypeId = 4;

                    grid.dataItems()[i].ProductId = Number(ddlselect.DiscountId);
                    grid.dataItems()[i].ProductName = ddlselect.Name;
                    grid.dataItems()[i].Description = ddlselect.Description;
                    grid.dataItems()[i].Discount = ddlselect.DiscountValue;
                    if (ddlselect.DiscountFactor === "2") {
                        grid.dataItems()[i].DiscountType = '$';
                    } else {
                        grid.dataItems()[i].DiscountType = '%';
                    }



                    childgrid.refresh();
                    childgrid.editRow(childRow);
                    childRow.find('[name="UnitPrice"]').prop("disabled", true);
                    childRow.find('[name="Discount"]').prop("disabled", true);
                    //This is for Discount items
                    grid.dataItems()[i].ProductTypeId = 4;

                    grid.dataItems()[i].dirty = true;
                    break;
                }
            }
        }

        function UnitPriceTemplate() {
        }

        function DiscountDropdown(container, options) {
            if (options.model.ProductTypeId === 4 || options.model.ProductTypeId === 0) {
                if ($scope.Quotation.DiscountsList)
                    var reg = $scope.Quotation.DiscountsList;

                //    $.grep($scope.Quotation.DiscountsList, function (s) {
                //    return Number(s.DiscountFactor) == 1;
                //});

                $('<input data-bind="value:ProductId" data-quotelineid="' + options.model.QuoteLineId + '" name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: false,
                        optionLabel: "Select Discount",
                        dataTextField: "Name",
                        dataValueField: "DiscountId",
                        value: options.model.ProductId,
                        valuePrimitive: true,
                        filter:"contains",
                        change: onDiscountchange,
                        dataSource: {
                            type: "json",
                            transport: {
                                read: function (ei) {
                                    ei.success(reg);
                                },
                            },
                        },
                        //dataBound: function () {
                        //    this.search(options.model.ProductName);
                        //}
                    });
            } else {
                $(
                    '<span ng-bind="dataItem.ProductName" disabled="true" class="ng-binding" data-bind="value:ProductName">' +
                    options.model.ProductName +
                    '</span>').appendTo(container)
            }
        }
        var globalData;
        $scope.EditRowDetail = function (e) {
            $scope.quoteEdited = true;
            $scope.eveBackup = e;
            $scope.updateQuoteLineFlag = false;
            var grid = $("#QuotelinesGrid").data("kendoGrid");

            //grid.dataSource.read();
            // $('#QuotelinesGrid').data('kendoGrid').refresh();
            //var validator = $("#QuotelinesGrid").kendoValidator().data("kendoValidator");
            var validator = $scope.kendoValidator("QuotelinesGrid");
            if (grid !== undefined && grid !== null) {

                var editRow = $("#QuotelinesGrid").find(".k-grid-edit-row");
                if (editRow.length > 0) {
                    if (!validator.validate()) {
                        return false;
                    }
                }
            }

            if (grid !== undefined && grid !== null && grid.dataSource.hasChanges()) {
                if (validator.validate()) {
                    if ($scope.TLValidateGrid()) {
                        $scope.bringQuotelineEditMode = e.QuoteLineId;
                        $scope.editUpdateQuoteLines();
                        var link = grid.tbody.find("tr[data-uid='" + e.uid + "']").find("td.k-hierarchy-cell .k-icon");
                        var row = grid.tbody.find("tr[data-uid='" + e.uid + "']");
                        grid.expandRow(row);
                        //}

                        //var link = grid.tbody.find("tr[data-uid='" + e.uid + "']").find("td.k-hierarchy-cell .k-icon");
                        //var row = grid.tbody.find("tr[data-uid='" + e.uid + "']");
                        //grid.expandRow(row);
                        //link.click();

                        var grdId = "#grd" + e.QuoteLineId;
                        var childgrid = $(grdId).data("kendoGrid");
                        //var grdId = "#grd" + e.QuoteLineId;
                        //var childgrid = $(grdId).data("kendoGrid");
                        $scope.childGridUpdate();

                        grid.editRow(e);

                        //// need to hide the productname dropdown at very first
                        //var pnamedd = $("#productNameDropDown").data("kendoDropDownList");
                        ////https://stackoverflow.com/questions/23342314/kendo-ui-dropdownlist-set-visible-via-javascript
                        //pnamedd.wrapper.hide();

                        if ($scope.brandid === 2 || $scope.brandid === 3) {
                            onProductTypeChange(e, e.ProductCategoryId);
                        } else {
                            if (childgrid) {
                                childgrid.editRow(e);
                            }
                        }
                        //// updated change for eliminate the error on edit
                        //globalData = e;
                        //setTimeout(function () {
                        //    var grid = $('#QuotelinesGrid').data('kendoGrid');
                        //    // grid.refresh();
                        //    var link = grid.tbody.find("tr[data-uid='" + globalData.uid + "']").find("td.k-hierarchy-cell .k-icon");
                        //    var row = grid.tbody.find("tr[data-uid='" + globalData.uid + "']");
                        //    grid.expandRow(row);
                        //    grid.editRow(globalData);
                        //    if (childgrid) childgrid.editRow(globalData);
                        //}, 100);
                    }
                }
            } else {
                //hide checkbox column for tax exempt and erase
                $scope.CollapseAllKendoGridDetailRows("QuotelinesGrid");
                var link = grid.tbody.find("tr[data-uid='" + e.uid + "']").find("td.k-hierarchy-cell .k-icon");
                var row = grid.tbody.find("tr[data-uid='" + e.uid + "']");
                grid.expandRow(row);
                //link.click();
                $scope.childGridUpdate();

                var grdId = "#grd" + e.QuoteLineId;
                var childgrid = $(grdId).data("kendoGrid");

                // TL does not have Child Grid, only the main Grid, so only when available
                // display that gird in editmode.
                if ($scope.brandid === 2 || $scope.brandid === 3) {
                    //onProductTypeChange(e, e.ProductCategoryId);
                    updateCategoryDropDown(e, e.ProductCategoryId, e.ProductTypeId)
                    onCategoryChange2(e.ProductSubCategoryId);
                } else {
                    if (childgrid) {
                        childgrid.editRow(e);
                    }
                }

                // Set the Quantity to 1 by default.
                if (e.Quantity === null) {
                    //e.Quantity = 1;
                }

                $scope.editUpdateQuoteLines();
                grid.editRow(e);
                //setTimeout(function () {
                //    grid.editRow(e);
                //}, 100)
                var pnamedd = $("#productNameDropDown").data("kendoDropDownList");
                //https://stackoverflow.com/questions/23342314/kendo-ui-dropdownlist-set-visible-via-javascript
                var pnametb = $("#productNameTextbox");
                // TODO : To enable line level discount for TL

                //if (e.ProductId != null && (e.ProductId > 0 || e.ProductCategoryId == 99999)) {
                if (pnamedd != undefined && pnamedd != null) {
                    if (e.ProductId && e.ProductId > 0) {
                        pnamedd.wrapper.show();
                        pnametb.hide();
                    } else {
                        pnamedd.wrapper.hide();
                        pnametb.show();
                    }
                }

                //if (childgrid) childgrid.editRow(e);

                // updated change for eliminate the error on edit
                //globalData = e;
                //setTimeout(function () {
                //    var grid = $('#QuotelinesGrid').data('kendoGrid');
                //   // grid.refresh();
                //    var link = grid.tbody.find("tr[data-uid='" + globalData.uid + "']").find("td.k-hierarchy-cell .k-icon");
                //    var row = grid.tbody.find("tr[data-uid='" + globalData.uid + "']");
                //    grid.expandRow(row);
                //    grid.editRow(globalData);
                //    if (childgrid) childgrid.editRow(globalData);
                //}, 100);

            }
            setTimeout(function () {
                $scope.Changenegativenumber();
            }, 500)
            // $scope.revokeSortableRow();
            $("#QuotelinesGrid .k-grid-content.k-auto-scrollable .table-overlay").css("display", "none");
        };

        $scope.Changenegativenumber = function () {
            $("#Width").change(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $("#Width").keyup(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $("#Height").change(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });

            $("#Height").keyup(function (e) {
                var value = e.target.value;
                if (value < 0) {
                    value = value * (-1);
                }
                $(e.target).val(value);
            });
        }



        $scope.childGridUpdate = function () {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            for (var i = 0; grid.dataItems().length > i; i++) {
                var grd = "#grd" + grid.dataItems()[i].QuoteLineId;
                if ($(grd).data('kendoGrid') !== undefined)
                    $(grd).data('kendoGrid').refresh();
            }
        }
        $scope.Update_width_fraction = function () {
            var item = $('#QuotelinesGrid').find('.k-grid-edit-row');
            item.data().uid
            var dataItem = item.data().$scope.dataItem;

            var radixPos = String(dataItem.Width).indexOf('.');
            var value = parseFloat(String(dataItem.Width).slice(radixPos));

            if ($("#Width").val() === '') {
                $("#Width").val('');
                dataItem.Width = '';
                dataItem.FranctionalValueWidth = null;
                $("#FranctionalValueWidth").data('kendoDropDownList').value(null);
            }
            else {
                dataItem.Width = Math.trunc(dataItem.Width);
                $("#Width").val(dataItem.Width);
                if (radixPos != -1) {
                    var fraction = $scope.getfraction(value);
                    dataItem.FranctionalValueWidth = fraction;
                    $("#FranctionalValueWidth").data('kendoDropDownList').value(fraction);
                }
            }
        };

        $scope.getfraction = function (value) {
            if (value > 0 && value < 0.125) { return "1/16"; }
            else if (value < 0.1875) { return "1/8"; }
            else if (value < 0.25) { return "3/16"; }
            else if (value < 0.3125) { return "1/4"; }
            else if (value < 0.375) { return "5/16"; }
            else if (value < 0.4375) { return "3/8"; }
            else if (value < 0.5) { return "7/16"; }
            else if (value < 0.5625) { return "1/2"; }
            else if (value < 0.625) { return "9/16"; }
            else if (value < 0.6875) { return "5/8"; }
            else if (value < 0.75) { return "11/16"; }
            else if (value < 0.8125) { return "3/4"; }
            else if (value < 0.875) { return "13/16"; }
            else if (value < 0.9375) { return "7/8"; }
            else if (value < 1) { return "15/16"; }
            else { return null; }

            //if (value > 0 && value < 0.1) { return "1/16"; }
            //else if (value < 0.15) { return "1/8"; }
            //else if (value < 0.2) { return "3/16"; }
            //else if (value < 0.3) { return "1/4"; }
            //else if (value < 0.35) { return "5/16"; }
            //else if (value < 0.4) { return "3/8"; }
            //else if (value < 0.45) { return "7/16"; }
            //else if (value < 0.55) { return "1/2"; }
            //else if (value < 0.6) { return "9/16"; }
            //else if (value < 0.65) { return "5/8"; }
            //else if (value < 0.7) { return "11/16"; }
            //else if (value < 0.8) { return "3/4"; }
            //else if (value < 0.85) { return "13/16"; }
            //else if (value < 0.9) { return "7/8"; }
            //else if (value < 1) { return "15/16"; }
            //else { return null; }
        }

        $scope.change_width = function ($event) {
            var item = $('#QuotelinesGrid').find('.k-grid-edit-row');
            item.data().uid
            var dataItem = item.data().$scope.dataItem;

            dataItem.FranctionalValueWidth = null;
            $("#FranctionalValueWidth").data('kendoDropDownList').value(null);
        };

        function FractionDropDownEditorWidth(container, options) {
            //
            if (options.model.ProductTypeId == 2 ||
                options.model.ProductTypeId == 5 ||
                options.model.ProductTypeId == '' ||
                options.model.ProductTypeId == null ||
                options.model.ProductTypeId == undefined) {
                $('<input data-type="number" style="width:49.2%; margin-right:1px;" class="k-textbox" required  ng-keypress="change_width($event)" ng-blur="Update_width_fraction()" name="Width"  id="Width" value="${Width}" />')
                    .appendTo(container);
                $(
                    '<input id="FranctionalValueWidth" style="width:49.2%;" name="FranctionalValueWidth"  data-bind="value:FranctionalValueWidth" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: true,
                        valuePrimitive: true,
                        optionLabel: "Select",
                        dataTextField: "Value",
                        dataValueField: "Key",
                        template: "#=Value #",
                        dataSource: {
                            data:

                                [{ Key: "1/16", Value: "1/16" },
                                { Key: "1/8", Value: "1/8" },
                                { Key: "3/16", Value: "3/16" },
                                { Key: "1/4", Value: "1/4" },
                                { Key: "5/16", Value: "5/16" },
                                { Key: "3/8", Value: "3/8" },
                                { Key: "7/16", Value: "7/16" },
                                { Key: "1/2", Value: "1/2" },
                                { Key: "9/16", Value: "9/16" },
                                { Key: "5/8", Value: "5/8" },
                                { Key: "11/16", Value: "11/16" },
                                { Key: "3/4", Value: "3/4" },
                                { Key: "13/16", Value: "13/16" },
                                { Key: "7/8", Value: "7/8" },
                                { Key: "15/16", Value: "15/16" }]
                        }
                    });
            } else {
                $('<span>' + options.model.Width + ' &nbsp; ' + options.model.FranctionalValueWidth + '</span>')
                    .appendTo(container);
            }
        }

        $scope.Update_height_fraction = function () {
            var item = $('#QuotelinesGrid').find('.k-grid-edit-row');
            item.data().uid
            var dataItem = item.data().$scope.dataItem;
            var radixPos = String(dataItem.Height).indexOf('.');
            var value = parseFloat(String(dataItem.Height).slice(radixPos));

            if ($("#Height").val() === '') {
                $("#Height").val('');
                dataItem.Height = '';

                dataItem.FranctionalValueHeight = null;
                $("#FranctionalValueHeight").data('kendoDropDownList').value(null);
            }
            else {
                dataItem.Height = Math.trunc(dataItem.Height);
                $("#Height").val(dataItem.Height);

                if (radixPos != -1) {
                    var fraction = $scope.getfraction(value);

                    dataItem.FranctionalValueHeight = fraction;
                    $("#FranctionalValueHeight").data('kendoDropDownList').value(fraction);
                }
            }
        };

        $scope.change_height = function ($event) {
            var item = $('#QuotelinesGrid').find('.k-grid-edit-row');
            item.data().uid
            var dataItem = item.data().$scope.dataItem;

            dataItem.FranctionalValueHeight = null;
            $("#FranctionalValueHeight").data('kendoDropDownList').value(null);
        };

        function FractionDropDownEditorHeight(container, options) {
            if (options.model.ProductTypeId == 2 ||
                options.model.ProductTypeId == 5 ||
                options.model.ProductTypeId == '' ||
                options.model.ProductTypeId == null ||
                options.model.ProductTypeId == undefined) {
                $('<input data-type="number" style="width:49.2%; margin-right:1px;" class="k-textbox" required name="Height"  ng-keypress="change_height($event)"  ng-blur="Update_height_fraction()"  id="Height" value="${Height}"/>')
                    .appendTo(container);
                $(
                    '<input  id="FranctionalValueHeight" style="width:49.2%;" name="FranctionalValueHeight"   data-bind="value:FranctionalValueHeight"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: true,
                        valuePrimitive: true,
                        optionLabel: "Select",
                        dataTextField: "Value",
                        dataValueField: "Key",
                        template: "#=Value #",
                        dataSource: {
                            data:

                                [{ Key: "1/16", Value: "1/16" },
                                { Key: "1/8", Value: "1/8" },
                                { Key: "3/16", Value: "3/16" },
                                { Key: "1/4", Value: "1/4" },
                                { Key: "5/16", Value: "5/16" },
                                { Key: "3/8", Value: "3/8" },
                                { Key: "7/16", Value: "7/16" },
                                { Key: "1/2", Value: "1/2" },
                                { Key: "9/16", Value: "9/16" },
                                { Key: "5/8", Value: "5/8" },
                                { Key: "11/16", Value: "11/16" },
                                { Key: "3/4", Value: "3/4" },
                                { Key: "13/16", Value: "13/16" },
                                { Key: "7/8", Value: "7/8" },
                                { Key: "15/16", Value: "15/16" }]
                        }
                    });
            } else {
                $('<span>' + options.model.Height + ' &nbsp; ' + options.model.FranctionalValueHeight + '</span>')
                    .appendTo(container);
            }
        }

        // print integration -- murugan
        $scope.printControl = {};
        $scope.printSalesPacket = function (modalid) {
            if ($routeParams.OpportunityId) $scope.opportunityIdPrint = $routeParams.OpportunityId;
            if ($routeParams.quoteKey) $scope.quoteKeyPrint = $routeParams.quoteKey;

            // $("#" + modalid).modal("show");
            $scope.printControl.showPrintModal();
        }

        $scope.addQuoteLinetitemTL = function () {
            var rowEdit = $('#QuotelinesGrid').find('.k-grid-edit-row');

            if (rowEdit.length) {
                //grid is not in edit mode
                var validator = $scope.kendoValidator("QuotelinesGrid"); //$(rowEdit).kendoValidator().data("kendoValidator");
                if (!validator.validate()) {
                    return false;
                } else {
                    if ($scope.TLValidateGrid()) {
                        $scope.addQuoteLinetitemTLData();
                    }
                }
            } else {
                $scope.addQuoteLinetitemTLData();
            }
        };

        $scope.addQuoteLinetitemTLData = function () {
            //
            var data = {
            };
            data.QuoteKey = $scope.quoteKey;//quoteid,

            //data.MeausurementList;
            //data.QuoteLinesList;
            $scope.EQL.EmptyQuoteline = "Yes"
            $scope.SaveMeasurementQuoteLine(data);

            ////var grid = $("#QuotelinesGridTL").data("kendoGrid");

            ////var dataSource = grid.dataSource;

            ////var index = dataSource._data.length;
            ////if (index != 0) {
            ////    $scope.id = index + 1;
            ////} else {
            ////    $scope.id = 0;
            ////}

            ////var newItem = {
            ////    QuoteLineId: $scope.id
            ////}
            ////var newItem = dataSource.insert(index, newItem); //, {});
            //////newItem.WindowLocation = "";
            ////var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
            ////grid.editRow(newRow);

            ////return true;

            ////// TODO: validate later
            //////var validator = $scope.kendoValidator("gridEditMeasurements");
            //////if (validator.validate()) {
            //////    var newItem = {
            //////        id: $scope.id
            //////    }

            //////    var newItem = dataSource.insert(index, newItem); //, {});
            //////    newItem.WindowLocation = "";
            //////    var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
            //////    grid.editRow(newRow);

            //////    //$("#gridEditMeasurements").data("kendoGrid").addRow(c);

            //////} else {
            //////    return false;
            //////}
        };

        $scope.list = {};
        $http.get("api/lookup/0/GetMyProductCategories")
            .then(function (result) {
                $scope.list.categories = result.data.Categories;
                $scope.list.productList = result.data.Products;
            }).catch(function (error) {
                //var loadingElement = document.getElementById("loading");
                //loadingElement.style.display = "none";
            });

        // function dropDownEditorCategory(container, options) {
        function dropDownEditorProductType(container, options) {
            $('<input id="productTypeDropDown" required style="width: 100%;height:24px;"  name="ProductCategoryId" />')
                .appendTo(container)
                .kendoDropDownList({
                    //autoBind: true,
                    //dataBound: function (e) {
                    //    var grid = $("#QuotelinesGrid").data("kendoGrid");
                    //    var row = e.sender.element.closest("tr");
                    //    var dataItem = grid.dataItem(row);
                    //    var products = $.grep($scope.list.categories, function (s) {
                    //        return s.Parant == dataItem.ProductCategoryId;
                    //    });
                    //    $scope.list.subcategories = products;
                    //    //updateCategoryDropDown(dataItem.ProductCategoryId);
                    //    updateCategoryDropDown(dataItem.ProductCategoryId, dataItem.ProductTypeId);
                    //},
                    valuePrimitive: true,
                    dataTextField: "ProductCategory",
                    dataValueField: "ProductCategoryId",
                    template: "#=ProductCategory #",
                    filter:"contains",
                    change: function (e) {
                        //
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        var row = e.sender.element.closest("tr");
                        var dataItem = grid.dataItem(row);

                        // The Product Type dropdowns current data source.
                        var cds = e.sender.dataItem();

                        // For TL, The Core product's category are displaed as product type
                        // so this sould work based on the dropdown value
                        var categorydd2 = $("#productTypeDropDown").data("kendoDropDownList");
                        var categorydd2Selected = categorydd2.value();
                        if (cds.ProductTypeId == 1) {
                            onProductTypeChange(dataItem, categorydd2Selected);
                        } else {
                            // FOr My products, Service and discounts we need different handling
                            onProductTypeChange(dataItem, categorydd2Selected, cds.ProductTypeId);
                        }
                    }
                });

            ////// Populate the Category from the list.
            ////var categorydd = $("#productTypeDropDown").data("kendoDropDownList");
            ////var categories = $.grep($scope.list.categories, function (s) {
            ////    return s.Parant == 0;
            ////});
            ////categorydd.setDataSource(categories);
            ////categorydd.dataSource.read();

            // Now this is Product Type. Need to display list of Categories
            // from Core Product and the "My Product, Service, Discout"
            var prodctTypedd = $("#productTypeDropDown").data("kendoDropDownList");
            if ($scope.list.categories)
                var productTypes = $.grep($scope.list.categories, function (s) {
                    return (s.Parant == 0 && s.ProductTypeId == 1) ||
                        (s.ProductCategoryId > 99996);
                });

            prodctTypedd.setDataSource(productTypes);
            prodctTypedd.dataSource.read();
        }

        //function onCategoryChange(dataItem, categorydd2Selected, productTypeId) {
        function onProductTypeChange(dataItem, categorydd2Selected, productTypeId) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");

            var quantity = Number($('#qlQuantity').val());

            var qlQuantity = $("#qlQuantity").data("kendoNumericTextBox");
            // var qlCost = $("#qlCost").data("kendoNumericTextBox");
            var qlUnitPrice = $("#qlUnitPrice").data("kendoNumericTextBox");
            var qlDiscount = $("#qlDiscount").data("kendoNumericTextBox");
            if (qlQuantity != undefined) {
                qlQuantity.enable(true);
            }

            //qlCost.enable(true);
            qlUnitPrice.enable(true);
            qlDiscount.enable(true);

            $('#qlQuantity').prop('required', true);
            //  $('#qlCost').prop('required', true);
            $('#qlUnitPrice').prop('required', true);

            $('#qlQuantity').removeClass("k-invalid");
            // $('#qlCost').removeClass("k-invalid");
            $('#qlUnitPrice').removeClass("k-invalid");
            $('#qlDiscount').removeClass("k-invalid");

            for (var i = 0; i < grid.dataItems().length; i++) {
                if (grid.dataItems()[i].QuoteLineId === Number(dataItem.QuoteLineId)) {
                    var row = $('#QuotelinesGrid').data().kendoGrid.dataSource.data()[i];

                    row.set("ExtendedPrice", 0);
                    row.set("Description", '');
                    row.set("ManualUnitPrice", false);
                    row.set("ProductName", '');
                    row.set("SuggestedResale", 0);
                    row.set("UnitPrice", 0);
                    row.set("BaseResalePriceWOPromos", '');

                    row["ExtendedPrice"] = 0;
                    row["Description"] = '';
                    row["ManualUnitPrice"] = false;
                    row["ProductName"] = '';
                    row["SuggestedResale"] = 0;
                    row["UnitPrice"] = 0;

                    $('#descriptionTextbox').removeClass("k-invalid");
                    $('#productNameTextbox').removeClass("k-invalid");

                    grid.dataItems()[i].ExtendedPrice = '';
                    grid.dataItems()[i].ManualUnitPrice = false;
                    grid.dataItems()[i].ProductName = '';
                    grid.dataItems()[i].SalePrice = '';
                    grid.dataItems()[i].SuggestedResale = '';
                    grid.dataItems()[i].UnitPrice = '';
                    grid.dataItems()[i].Description = '';

                    ///Myproduct
                    if (Number(categorydd2Selected) === 99997) {
                        grid.dataItems()[i].ProductTypeId = 2;

                        if (quantity === 0) {
                            if (qlQuantity != undefined)
                                qlQuantity.value(1);
                            grid.dataItems()[i].Quantity = 1;
                        }

                        //qlCost.enable(false);
                        $('#qlUnitPrice').prop('required', false);
                        //  $('#qlCost').prop('required', false);

                        //$('#qlCost').removeClass("k-invalid");
                        $('#qlUnitPrice').removeClass("k-invalid");
                    }
                    else if (Number(categorydd2Selected) === 99998) {///My Service
                        grid.dataItems()[i].ProductTypeId = 3;
                        //qlCost.enable(false);
                        $('#qlUnitPrice').prop('required', false);
                        //  $('#qlCost').prop('required', false);

                        // $('#qlCost').removeClass("k-invalid");
                        $('#qlUnitPrice').removeClass("k-invalid");

                        if (quantity === 0) {
                            if (qlQuantity != undefined)
                                qlQuantity.value(1);
                            grid.dataItems()[i].Quantity = 1;
                        }
                    }
                    else if (Number(categorydd2Selected) === 99999) {///Discount
                        grid.dataItems()[i].ProductTypeId = 4;


                        if (qlQuantity != undefined)
                            qlQuantity.value(null);

                        if (qlQuantity != undefined)
                            qlQuantity.enable(false);

                        // qlCost.enable(false);
                        qlUnitPrice.enable(false);

                        $('#qlQuantity').prop('required', false);
                        // $('#qlCost').prop('required', false);
                        $('#qlUnitPrice').prop('required', false);

                        $('#qlQuantity').removeClass("k-invalid");
                        // $('#qlCost').removeClass("k-invalid");
                        $('#qlUnitPrice').removeClass("k-invalid");

                        if (dataItem.DiscountFactor === "2") {
                            grid.dataItems()[i].DiscountType = '$';
                        } else {
                            grid.dataItems()[i].DiscountType = '%';
                        }

                    } else {
                        grid.dataItems()[i].ProductTypeId = 5;
                        qlUnitPrice.enable(false);
                        $('#qlUnitPrice').prop('required', false);
                        $('#qlUnitPrice').removeClass("k-invalid");
                        if (quantity === 0) {
                            if (qlQuantity != undefined)
                                qlQuantity.value(1);
                            grid.dataItems()[i].Quantity = 1;
                        }
                    }
                    grid.dataItems()[i].dirty = true;
                    break;
                }
            }

            updateCategoryDropDown(dataItem, categorydd2Selected, productTypeId);
            //$('#QuotelinesGrid').data('kendoGrid').refresh();
        }
        function updateDiscountCategory() {
        }
        //function updateProductDropDown(categoryid, productTypeId) {
        function updateCategoryDropDown(dataItem, categoryid, productTypeId) {
            if (categoryid == "99997") productTypeId = 2;
            if (categoryid == "99998") productTypeId = 3;
            if (categoryid == "99999") productTypeId = 4;

            var categorydd = $("#categoryDropDown").data("kendoDropDownList");
            if (categorydd)
                categorydd.enable(true);
            if (productTypeId === 4) {
                if (categorydd !== undefined) {
                    categorydd.enable(false);

                    categorydd.setDataSource({});
                    dataItem.ProductSubCategory = null;
                    dataItem.ProductSubCategoryId = null;
                    $("#categoryDropDown").removeAttr('required');
                }

                var productdd = $("#productNameDropDown").data("kendoDropDownList");
                if ($scope.list.productList)
                    var pnames = $.grep($scope.list.productList, function (s) {
                        return s.ProductType == 4;// Discount Type
                    });

                //$scope.list.products = {};
                $scope.list.products = pnames;

                resetProductName();
                setDescriptionValue("");

                //if (productdd) {
                //    productdd.setDataSource($scope.list.productList);
                //    productdd.dataSource.read();
                //}
                //var grid = $("#QuotelinesGrid").data("kendoGrid");
                //var selected= grid.dataItem(grid.select());
                //var row = e.sender.element.closest("tr");
                //var dataItem = grid.dataItem(row);
                //updateProductType(dataItem)
                // categorydd.enable(false);
                //var productdd = $("#productNameDropDown").data("kendoDropDownList");
                //$scope.list.products = {};
                //if (productdd) {
                //    productdd.setDataSource($scope.list.products);
                //    productdd.dataSource.read();
                //}
            } else {
                if ($scope.list.categories)
                    var categoriesObj = $.grep($scope.list.categories,
                        function (s) {
                            // When need list of categories for My Products, Service and discounts.
                            //if (productTypeId && productTypeId != 1) {
                            //    return s.ProductTypeId == productTypeId
                            //            && s.ProductCategoryId < 99997;
                            //}
                            if (productTypeId && (productTypeId != 1 && productTypeId != 5)) {
                                return s.ProductTypeId == productTypeId && s.ProductCategoryId < 99997;
                            }

                            // Need list of sub categories from Product type1
                            return s.Parant == categoryid;
                        });

                $scope.list.subcategories = categoriesObj;

                if (categorydd) {
                    categorydd.setDataSource($scope.list.subcategories);//products);
                    categorydd.dataSource.read();
                    $("#categoryDropDown").prop('required', true);
                }

                // also we need to remove the product dropdown, as at this
                // moment it contains invalid data.
                var productdd = $("#productNameDropDown").data("kendoDropDownList");
                $scope.list.products = {};
                if (productdd) {
                    productdd.setDataSource($scope.list.products);
                    productdd.dataSource.read();
                }
            }
            $("#productNameDropDown").parent().removeClass('dropdown-validation-error');
            $("#categoryDropDown").parent().removeClass('dropdown-validation-error');
        }

        //function dropDownEditorProduct(container, options) {
        function dropDownEditorCategory(container, options) {
            $('<input id="categoryDropDown" style="width: 100%;height:24px;" name="ProductSubCategoryId"  />')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: true,
                    valuePrimitive: true,
                    dataTextField: "ProductCategory",
                    dataValueField: "ProductCategoryId",
                    filter:"contains",
                    template: "#=ProductCategory #",
                    dataSource: {
                        data: $scope.list.subcategories
                    },
                    change: function (e) {
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        var row = e.sender.element.closest("tr");
                        var dataItem = grid.dataItem(row);

                        // The Product Type dropdowns current data source.
                        var cds = e.sender.dataItem();
                        if (cds) {
                            onCategoryChange2(cds.ProductCategoryId); //dataItem.ProductCategoryId
                            updateProductType(dataItem);
                            $("#categoryDropDown").parent().removeClass('dropdown-validation-error');
                        }

                    }
                });
        }

        function dropDownEditorProductName(container, options) {
            var required = '';
            if (options.model.ProductTypeId === 4) {
                var categorydd = $("#categoryDropDown").data("kendoDropDownList");
                if (categorydd !== undefined)
                    categorydd.enable(false);
                if ($scope.list.productList)
                    var pnames = $.grep($scope.list.productList,
                        function (s) {
                            return s.ProductType == 4; // Discount Type
                        });

                //$scope.list.products = {};
                $scope.list.products = pnames;
                required = 'required';
            }

            //$('<input id="productNameDropDown" style="width: 100%;" name="ProductNamedd"  />')
            $('<input id="productNameDropDown" ' + required + ' style="width: 100%;" name="ProductId" name="' + options.field + '"  />')
                .appendTo(container)
                .kendoDropDownList({
                    optionLabel: "Select",
                    autoBind: true,
                    visible: false,
                    valuePrimitive: true,
                    filter:"contains",
                    dataTextField: "ProductName",
                    dataValueField: "ProductKey", //"ProductID",
                    // The following works, displays with "prodictid - productname"
                    // in the dropdown list (while open only).
                    //template: "#=ProductID #" + " - " + "#=ProductName #",
                    dataSource: {
                        data: $scope.list.products
                    },
                    change: function (e) {
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        var row = e.sender.element.closest("tr");
                        var dataItem = grid.dataItem(row);
                        dataItem.set("ProductName", e.sender.dataItem().ProductName);

                        // The Product Type dropdowns current data source.
                        var cds = e.sender.dataItem();
                        if (cds != null) {
                            $("#productNameDropDown").parent().removeClass('dropdown-validation-error');
                        }
                        onProductNameChange(cds, dataItem, e);

                    }
                });
            $('<input id="productNameTextbox" style="width: 100%;" required name="ProductName"  />')
                .appendTo(container);
            //$('<input id="productNameTextbox" style="width: 100%;" name="ProductNametb"  />')
            //    .appendTo(container);
            resetProductName(); // reset controls based on producttype id
        }

        function textBoxEditorDescription(container, options) {
            $('<input id="descriptionTextbox" required style="width: 100%;" name="Description"  />')
                .appendTo(container);
        }

        function onCategoryChange2(productCategoryId) {
            //var pnamedd = $("#productNameDropDown").data("kendoDropDownList");
            if (productCategoryId && $scope.list.productList)
                var pnames = $.grep($scope.list.productList, function (s) {
                    return s.ProductCategory == productCategoryId;//dataItem.ProductCategoryId;
                });

            $scope.list.products = pnames;

            resetProductName();
            setDescriptionValue("");

            //var pnametb = $("#productNameTextbox");//.data("kendoDropDownList");

            //if (pnamedd && pnametb)
            //{
            //    if ($scope.list.products.length == 0) {
            //        pnamedd.setDataSource($scope.list.products);
            //        pnamedd.dataSource.read();
            //        pnamedd.wrapper.hide();
            //        pnametb.show();
            //    } else {
            //        pnamedd.wrapper.show();
            //        pnametb.val("");
            //        pnametb.hide();
            //    }
            //}

            //if (pnamedd) {
            //    pnamedd.setDataSource($scope.list.products);
            //    pnamedd.dataSource.read();
            //    if ($scope.list.products.length == 1) {
            //        pnamedd.select(0);
            //    }
            //}
        }

        function updateProductType(dataItem) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            for (var i = 0; i < grid.dataItems().length; i++) {
                if (grid.dataItems()[i].QuoteLineId === Number(dataItem.QuoteLineId)) {
                    //var row = $('#QuotelinesGrid').data().kendoGrid.dataSource.data()[i];
                    //var qlCost = $("#qlCost").data("kendoNumericTextBox");
                    if ($scope.list.products != undefined && $scope.list.products.length == 0) {
                        grid.dataItems()[i].DisplayProductTypeId = dataItem.ProductTypeId;
                        grid.dataItems()[i].ProductTypeId = 5;
                        //qlCost.enable(true);
                    } else {
                        //grid.dataItems()[i].DisplayProductTypeId = //dataItem.ProductTypeId;
                        if (grid.dataItems()[i].DisplayProductTypeId) {
                            grid.dataItems()[i].ProductTypeId = grid.dataItems()[i].DisplayProductTypeId;
                        }
                    }
                    break;
                }
            }
        }

        function resetProductName() {
            var pnamedd = $("#productNameDropDown").data("kendoDropDownList");
            var pnametb = $("#productNameTextbox");//.data("kendoDropDownList");

            if (pnamedd && $scope.list.products) {
                pnamedd.setDataSource($scope.list.products);
                pnamedd.dataSource.read();

                if ($scope.list.products && $scope.list.products.length == 0) {
                    pnamedd.wrapper.hide();
                    $('#productNameDropDown').removeAttr('required');
                } else {
                    pnamedd.wrapper.show();
                    $('#productNameDropDown').prop('required', true);
                }
            }

            if (pnametb) {
                pnametb.val("");
                if ($scope.list.products && $scope.list.products.length == 0) {
                    pnametb.show();
                } else {
                    pnametb.hide();
                }
            }

            var dropdownlist = $("#productNameDropDown").data("kendoDropDownList");
            if (dropdownlist) dropdownlist.value("");
        }

        function setdiscountValues(newvalue, discType) {
            var disc = $("#qlDiscount").data("kendoNumericTextBox");

            var format = '%';

            if (discType != null && discType != undefined) {
                if (discType === '%') {

                    format = "p0";
                    newvalue = newvalue / 100;
                } else {
                    format = "c";
                }
            }


            if (disc) {

                $("#qlDiscount").data("kendoNumericTextBox").setOptions({ format: format, decimals: 2 });
                disc.value(newvalue);

                //$("#qlDiscount").data("kendoNumericTextBox").destroy();
                //$("#qlDiscount").kendoNumericTextBox({
                //    format: format,
                //    decimals: 2
                //});
                //var disc1 = $("#qlDiscount").data("kendoNumericTextBox");
                //disc1.value(newvalue);
                //disc.setOptions({ format: format, decimals: 2 });
            }




        }

        function setDescriptionValue(newvalue) {
            var descriptiontb = $('#descriptionTextbox');
            if (descriptiontb) descriptiontb.val(newvalue);
            if (newvalue != '' && newvalue != null)
                $('#descriptionTextbox').removeClass("k-invalid");
        }

        function onProductNameChange(pnCurrentDataItem, gridCurrentRowDataItem, e) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");

            var gridrow = e.sender.element.closest(".k-grid").data("kendoGrid");
            var row = e.sender.element.closest("tr");
            var dataItem = gridrow.dataItem(row);

            for (var i = 0; i < grid.dataItems().length; i++) {
                if (grid.dataItems()[i].QuoteLineId === Number(gridCurrentRowDataItem.QuoteLineId)) {
                    // grid.dataItems()[i].Description = "Asfdsadf";
                    if (grid.dataItems()[i].ProductTypeId === 4) {
                        grid.dataItems()[i].Discount = pnCurrentDataItem.Discount;
                        grid.dataItems()[i].Description = pnCurrentDataItem.Description;
                        grid.dataItems()[i].ProductName = pnCurrentDataItem.ProductName;
                        grid.dataItems()[i].ProductId = pnCurrentDataItem.ProductID;
                        var disctype = "%";
                        if (pnCurrentDataItem.DiscountType != null && pnCurrentDataItem.DiscountType != undefined) {
                            if (pnCurrentDataItem.DiscountType === "2") {
                                disctype = "$";
                            }
                            else {
                                disctype = "%";
                            }
                        }
                        grid.dataItems()[i].DiscountType = disctype;
                        setdiscountValues(pnCurrentDataItem.Discount, disctype);
                        setDescriptionValue(pnCurrentDataItem.Description);
                        if (pnCurrentDataItem.ProductName != '')
                            $("#categoryDropDown").parent().removeClass('dropdown-validation-error');
                    } else {
                        grid.dataItems()[i].SuggestedResale = pnCurrentDataItem.SalePrice;
                        grid.dataItems()[i].Cost = pnCurrentDataItem.Cost;

                        grid.dataItems()[i].Description = pnCurrentDataItem.Description;
                        var row = $('#QuotelinesGrid').data().kendoGrid.dataSource.data()[i];

                        //var descriptiontb = $('#descriptionTextbox');
                        //descriptiontb.val(pnCurrentDataItem.Description);
                        setDescriptionValue(pnCurrentDataItem.Description);

                        if (pnCurrentDataItem.Discount)
                            setdiscountValues(pnCurrentDataItem.Discount, disctype);
                        else
                            setdiscountValues(0, disctype);

                        //var resale = $("#qlresalePrice").data("kendoNumericTextBox");
                        //if (resale) resale.value(pnCurrentDataItem.SalePrice);

                        var disc = $("#qlUnitPrice").data("kendoNumericTextBox");
                        if (disc) disc.value(pnCurrentDataItem.SalePrice);

                        var resale = $("#qlSuggestedResale").data("kendoNumericTextBox");
                        if (resale) resale.value(pnCurrentDataItem.SalePrice);
                    }
                    // return;
                }
            }

            if (pnCurrentDataItem.ProductName)
                $("#productNameTextbox").val(pnCurrentDataItem.ProductName);
            else $("#productNameTextbox").val('');
        }

        $scope.confirmTaxExempt = function () {
            //

            if ($scope.QuoteinEditMode == true) {
                if ($scope.Quotation.IsTaxExempt == true) {
                    if ($scope.Quotation.TaxExemptID != '' && $scope.Quotation.TaxExemptID != null &&
                        $scope.Quotation.TaxExemptID != undefined) {
                    }
                    else {
                        $http.get('/api/Quotes/' + 0 + '/getTaxExempt?OpportunityId=' +
                            $scope.Quotation.OpportunityId).then(function (data) {
                                if (data.data.TaxExemptID != '' && data.data.TaxExemptID != null &&
                                    data.data.TaxExemptID != undefined && data.data.IsTaxExempt != false) {
                                    $scope.Quotation.TaxExemptID = data.data.TaxExemptID;
                                }
                            }).catch(function (error) {
                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "none";
                            });
                    }
                }
                $scope.ResetQuoteTax();
            }
        }

        $scope.ClearQuotelines = function () {
            if (confirm("Are you sure you want to clear all Quote Lines?")) {
                $scope.loadingElement.style.display = "block";
                $http.get('/api/Quotes/' +
                    $scope.Quotation.QuoteStatusId +
                    '/ClearQuoteslines?QuoteKey=' +
                    $scope.quoteKey).then(function (response) {
                        if (response.data.error == '') {
                            $scope.Quotation = response.data.data;
                            setQuoteHeaderDiscountValue();
                            $scope.QuotationCopy = angular.copy($scope.Quotation);
                            $scope.Quotation.QuoteKey = response.data.quoteKey;
                            $scope.ResetQuoteTax();
                            $("#QuotelinesGrid").data('kendoGrid').dataSource.data([]);
                            $scope.UpdateGroupDiscountLabel();
                            $scope.selectedMeasurementForAccount = [];
                        } else {
                            HFC.DisplaySuccess(response.data.error);
                        }
                        //$scope.$apply();
                        $scope.loadingElement.style.display = "none";
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
            }
        };

        $scope.EstimateSalesTaxes = function () {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $http.get('/api/Quotes/' + $scope.quoteKey + '/EstimateSalesTax').then(function (response) {
                if (response.data.error == '') {
                    $scope.Quotation.TaxDetails = response.data.data.Taxinfo;
                    $scope.ResetQuoteTax();
                } else {
                    if (response.data.error.match("Input missing required element 'DestinationAddress1'")) {
                        HFC.DisplayAlert("Valid Address is required for tax calculation");
                    }
                    else if (response.data.error.match("Input missing required element 'Items'")) {
                        HFC.DisplayAlert("Quote Line is required for tax calculation");
                    }
                    else if (response.data.error.match("TaxAddressError: An Address is incomplete or invalid")) {
                        HFC.DisplayAlert("Valid Address is required for tax calculation");
                    }

                    else {
                        HFC.DisplayAlert(response.data.error);
                    }
                }
                $scope.loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }
        //$scope.TaxDetails();

        function TaxDetails() {

            $scope.TaxTotal = 0;
            if ($scope.Quotation.TaxDetails != null && $scope.Quotation.TaxDetails.length > 0) {
                for (var i = 0; i < $scope.Quotation.TaxDetails.length; i++) {
                    if ($scope.Quotation.TaxDetails[i].Amount !== null) {
                        $scope.TaxTotal = $scope.TaxTotal + $scope.Quotation.TaxDetails[i].Amount;
                    }
                }
            }
            $scope.TaxDetailsLines = {
                dataSource: {
                    data: $scope.Quotation.TaxDetails
                },
                resizable: true,
                columns: [
                    //{
                    //    field: "Jurisdiction",
                    //    title: "Jurisdiction",
                    //    filterable: false
                    //},
                    {
                        field: "JurisType",
                        title: "Tax Type",
                        filterable: false
                    },
                    {
                        field: "TaxName",
                        title: "Tax Name",
                        //filterable: false
                        template: function (item) {
                            if (item.TaxName != undefined && item.TaxName != null && item.TaxName != '') {
                                return item.TaxName;
                            } else {
                                return item.Jurisdiction;
                            }
                        }
                    },
                    {
                        field: "Rate",
                        title: "Rate",
                        //width: "100px",
                        template: '#=kendo.format("{0:p}", Rate / 100)#'
                    },
                    {
                        field: "Amount",
                        title: "Amount",
                        //width: "150px",
                        //format: "{0:c}"
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                        },
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: false,
                scrollable: true,
                sortable: true,
                cache: false
            };
        };
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 332);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 332);
            };
            setCaretIconBehaviour();
        });
        // End Kendo Resizing
        $scope.PrevdirtyStatus;
        //Add Notes
        $scope.AddMultipleNotes = function (e, flag) {
            $scope.QuoteNoteLineModel = !$scope.QuoteNoteLineModel;
            $scope.Quotation.InstallerInvoiceNotes = $scope.QuotationCopy.InstallerInvoiceNotes;
            $scope.Quotation.QuoteLevelNotes = $scope.QuotationCopy.QuoteLevelNotes;
            $scope.Quotation.OrderInvoiceLevelNotes = $scope.QuotationCopy.OrderInvoiceLevelNotes;
            $anchorScroll();
            if (flag == true)
                $scope.PrevdirtyStatus = $scope.formquote.$dirty;
            if (e == undefined) {
                //$scope.formquote.$setDirty();
                if (!$scope.PrevdirtyStatus)
                    $scope.formquote.$dirty = false;


            }
        }

        //Save Quote Notes
        $scope.SaveHeaderLevelNote = function () {
            var dto = $scope.Quotation;
            if (!$scope.PrevdirtyStatus)
                $scope.formquote.$dirty = false;
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $http.post('/api/Quotes/' + $scope.quoteKey + '/SaveQuote', dto).then(function (response) {
                if (response.data.error == '') {
                    $scope.Quotation = response.data.data;
                    setQuoteHeaderDiscountValue();
                    $scope.QuotationCopy = angular.copy($scope.Quotation);
                    $scope.Quotation.QuoteKey = response.data.quoteKey;

                    // $scope.QuoteinEditMode = false;

                    $scope.QuoteNoteLineModel = false;
                    $anchorScroll();
                } else {
                    HFC.DisplaySuccess(response.data.error);
                }
                //$scope.$apply();
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }

        var setCaretIconBehaviour = function () {
            if ($scope.brandid === 1) {
                if ($scope.BindCursor) {
                    if (document.getElementById("btnCaret1") && document.getElementById("btnCaret")) {
                        $scope.BindCursor = false;
                        document.getElementById("btnCaret1").style.display = "none";
                        document.getElementById("btnCaret").style.display = "block";

                        $("#btnCaret").click(function (e) {
                            var grid = $("#QuotelinesGrid").data("kendoGrid");
                            $(".k-master-row").each(function (index) {
                                grid.expandRow(this);
                            });
                            document.getElementById("btnCaret").style.display = "none";
                            document.getElementById("btnCaret1").style.display = "block";
                            var panelBody = $("#panelbar .k-item.k-state-default");
                            var panelBar = $("#panelbar").data("kendoPanelBar");
                            panelBar.expand(panelBody);
                            e.preventDefault();
                            e.stopPropagation();
                        });

                        $("#btnCaret1").click(function (e) {
                            //var panelBody = $("#panelbar .k-item.k-state-default");
                            //var panelBar = $("#panelbar").data("kendoPanelBar");
                            //panelBar.collapse(panelBody);

                            var grid = $("#QuotelinesGrid").data("kendoGrid");
                            $(".k-master-row").each(function (index) {
                                grid.collapseRow(this);
                            });
                            document.getElementById("btnCaret").style.display = "block";
                            document.getElementById("btnCaret1").style.display = "none";

                            e.preventDefault();
                            e.stopPropagation();
                        });
                    }
                }
            }
            else {
                if (document.getElementById("btnCaret"))
                    document.getElementById("btnCaret").style.display = "none";
                if (document.getElementById("btnCaret1"))
                    document.getElementById("btnCaret1").style.display = "none";
            }
        };


        // move quote behaviours

        // address pop up

        // The callback for successful quick address add from installation
        $scope.handleInstallationAddressAfterQuickAdd = function (newAddressId) {

            resetInstallAfterQuickAdd($scope.AdditionalAddressServiceTP.AccountId, newAddressId)
        }

        var resetInstallAfterQuickAdd = function (accountid, newaddressid) {

            if (!accountid) return;

            $http.get('/api/opportunities/' + accountid + '/InstallationAddresses').then(function (data) {
                var addresses = data.data;
                // update the kendo dropdown data source.
                $scope.InstallationAddresslist = addresses;
                $scope.BillingAddressList = addresses;

                // Select the newly added address as the default one for the user.
                $scope.selectedInstallationAddressId = newaddressid;
            });
        }

        // The callback for successful quick address add from Billing
        $scope.handleBillingAddressAfterQuickAdd = function (newAddressId) {
            resetBillingAfterQuickAdd($scope.AdditionalAddressServiceTP.AccountId, newAddressId)
        }

        var resetBillingAfterQuickAdd = function (accountid, newaddressid) {

            if (!accountid) return;

            $http.get('/api/opportunities/' + accountid + '/InstallationAddresses').then(function (data) {
                var addresses = data.data;

                // update the kendo dropdown data source.
                $scope.InstallationAddresslist = addresses;
                $scope.BillingAddressList = addresses;

                // Select the newly added address as the default one for the user.
                $scope.selectedBillingAddressId = newaddressid;
            });
        }

        // we need this as it needs to be watched by angular
        $scope.selectedBillingAddressId = null;
        $scope.selectedInstallationAddressId = null;

        // get opportunities status data
        $scope.getOpportunityStatusData = function () {
            $http.get("/api/opportunities/0/OpportunitiesStatus").then(function (data) {
                $scope.opportunityStatusList = data.data;
            });
        }

        // get slaes agent data
        $scope.getSalesAgentData = function () {
            $http.get("/api/opportunities/0/GetSalesAgents").then(function (data) {
                $scope.SalesAgentlist = data.data;
            });
        }

        // get installers data
        $scope.getInstallerData = function () {
            $http.get("/api/opportunities/0/GetInstallers").then(function (data) {
                $scope.InstallationAddresslist = data.data;
                $scope.BillingAddressList = data.data;
            });
        }

        // get account installation
        $scope.getAccountInstallationAddress = function () {
            $http.get("api/opportunities/" + $scope.selectedQuoteAccountId + "/InstallationAddresses").then(function (data) {
                var dataObject = data.data;
                $scope.InstallationAddresslist = data.data;
                $scope.BillingAddressList = data.data;
                if (dataObject && dataObject.length === 1) {
                    $scope.selectedBillingAddressId = dataObject[0]['InstallAddressId'];
                    $scope.selectedInstallationAddressId = dataObject[0]['InstallAddressId'];
                    $scope.selectedInstallationAddress = dataObject[0]['Name'];
                    $scope.installationAddressValidFlag = dataObject[0]['IsValidated'];
                    $scope.billingAddressValidFlag = dataObject[0]['IsValidated'];
                }
            });
        }

        $scope.selectedAddress = function (InstallationId, BillingId) {
            if (InstallationId != 0) {
                for (i = 0; i < $scope.InstallationAddresslist.length; i++) {
                    if ($scope.InstallationAddresslist[i].InstallAddressId == InstallationId)
                        $scope.installationAddressValidFlag = $scope.InstallationAddresslist[i].IsValidated;
                }
            }
            else if (BillingId != 0) {
                for (i = 0; i < $scope.BillingAddressList.length; i++) {
                    if ($scope.BillingAddressList[i].InstallAddressId == BillingId)
                        $scope.billingAddressValidFlag = $scope.BillingAddressList[i].IsValidated;
                }
            }
        }


        // navigate to opportunity page
        $scope.navigateToOpportunityPage = function (id) {
            window.location.href = "/#!/OpportunityDetail/" + id;
        }

        // move quote to opportunity
        $scope.saveQuoteToopportunity = function () {
            if ($scope.IsExistingOpportunity) {
                // $scope.getQuoteDetails(quoteKey, opportunityId);
                $scope.createNewQuoteInExistingOpp();
            } else if ($scope.IsCreateNewOpportunity) {
                if ($scope.FranciseLevelTexting && $scope.AccountCellphone != null && $scope.IsNotifyText && ($scope.NotifyTextStatus == 'Optout' || $scope.NotifyTextStatus == 'Optin')) {
                    $("#Textingpopup").modal("show");
                }
                else {
                    $scope.createNewOpportunity();
                }
            } else {
                var loadingElement = document.getElementById("loading");
                $scope.loadingElement.style.display = "none";
                alert("Please select any one of the opportunities");
            }
        }

        //texting popup
        $scope.SendMsg = function () {
            $scope.SendMsg = true;
            $scope.UncheckNotifyviaText = false;
            $scope.createNewOpportunity();
        }
        $scope.UncheckNotifyviaText = function () {
            $scope.UncheckNotifyviaText = true;
            $scope.SendMsg = false;
            $scope.createNewOpportunity();
        }
        $scope.CloseTextingPopup = function () {
            $("#Textingpopup").modal("hide");
        }


        // save the quote in existing opportunity
        $scope.createNewQuoteInExistingOpp = function () {
            var data = {
                OpportunityId: $scope.selectedOpportunityId,
                QuoteKey: $scope.selectedQuoteKey
            }
            var loadingElement = document.getElementById("loading");
            $scope.loadingElement.style.display = "block";
            $http.post("/api/Quotes/0/MoveQuote", data).then(function (data) {
                var quoteKey = data.data.QuoteKey;
                var opportunityId = data.data.OpportunityId;
                var loadingElement = document.getElementById("loading");
                $scope.loadingElement.style.display = "none";
                window.location.href = "#!/quote/" + opportunityId + "/" + quoteKey;
            });
        }

        // validate the new opportunity block
        $scope.validateNewOpportunityFields = function () {
            if ($scope.newQuoteOpportunityName == '' || $scope.newQuoteOpportunityStatusId == '' || $scope.newQuoteSalesAgentId == '' || $scope.selectedInstallationAddressId == '' || $scope.selectedBillingAddressId == '' || $scope.selectedInstallationAddress == '' || $scope.newQuoteSideMark == '' || $scope.newQuoteSideMark == undefined) {
                return false;
            }
            else {
                return true;
            }
        }

        $scope.createNewOpportunity = function (flag) {
            $scope.submitted = true;
            var validateFlag = $scope.validateNewOpportunityFields();
            if (validateFlag) {
                if (($scope.FranciseLevelTexting && $scope.AccountCellphone != null && $scope.IsNotifyText && ($scope.NotifyTextStatus == 'Optout' || $scope.NotifyTextStatus == 'Optin')) && flag) {
                    $("#Textingpopup").modal("show");
                }
                else {
                    $scope.formopportunity.$setPristine();
                    $scope.submitted = false;
                    var data = {
                        OpportunityId: 0,
                        OpportunityName: $scope.newQuoteOpportunityName,
                        SideMark: $scope.newQuoteSideMark,
                        OpportunityStatusId: $scope.newQuoteOpportunityStatusId,
                        SalesAgentId: $scope.newQuoteSalesAgentId,
                        AccountId: $scope.selectedQuoteAccountId,
                        SourcesTPId: $scope.selectedAccountSource,
                        InstallationAddressId: $scope.selectedInstallationAddressId,
                        BillingAddressId: $scope.selectedBillingAddressId,
                        InstallationAddress: $scope.selectedInstallationAddress,
                        Description: $scope.newOpportunityDescription,
                        SendMsg: $scope.SendMsg,
                        UncheckNotifyviaText: $scope.UncheckNotifyviaText,
                        QuoteKey: $scope.selectedQuoteKey
                    }
                    var loadingElement = document.getElementById("loading");
                    $scope.loadingElement.style.display = "block";
                    $http.post("/api/Quotes/0/MoveQuote", data).then(function (data) {
                        var quoteKey = data.data.QuoteKey;
                        var opportunityId = data.data.OpportunityId;
                        var loadingElement = document.getElementById("loading");
                        $scope.loadingElement.style.display = "none";
                        window.location.href = "#!/quote/" + opportunityId + "/" + quoteKey;
                    });
                }
            } else {
                var loadingElement = document.getElementById("loading");
                $scope.loadingElement.style.display = "none";
            }
        }

        // get account details
        $scope.getAccountDetails = function (accountId) {
            $http.get('/api/Accounts/' + accountId).then(function (response) {
                $scope.AccountCellphone = response.data.Account.PrimCustomer.CellPhone;
                //texting
                $scope.IsNotifyText = response.data.Account.IsNotifyText;
                $scope.selectedAccountSource = response.data.Account.SourcesTPId;
                if (response.data.TextStatus == null) {
                    $scope.NotifyTextStatus = 'new';
                }
                else if (response.data.TextStatus.Optout == true) {
                    $scope.NotifyTextStatus = 'Optout';
                }
                else if (response.data.TextStatus.Optin == true) {
                    $scope.NotifyTextStatus = 'Optin';
                }
                else if (response.data.TextStatus.IsOptinmessagesent == true) {
                    $scope.NotifyTextStatus = 'waiting';
                }
                else if (response.data.TextStatus.Optout == null || response.data.TextStatus.Optin == null || response.data.TextStatus.IsOptinmessagesent == false) {
                    $scope.NotifyTextStatus = 'error';
                }
            });
        }


        // get quote details
        $scope.getQuoteDetails = function (quoteKey, opportunityId) {
            var loadingElement = document.getElementById("loading");
            $scope.loadingElement.style.display = "block";
            $http.get('/api/Quotes/' + quoteKey + '/GetDetails?oppurtunityId=' + opportunityId).then(function (data) {
                var selectedQuoteDetails = data.data;
                $scope.selectedQuoteAccountId = selectedQuoteDetails.AccountId;
                $scope.selectedQuoteId = selectedQuoteDetails.QuoteID;
                $scope.selectedInstallAddressId = selectedQuoteDetails.InstallationAddress.AddressId;
                $scope.AdditionalAddressServiceTP.AccountId = $scope.selectedQuoteAccountId;
                $scope.AdditionalAddressServiceTP.ParentName = 'Account';
                $scope.moveQuoteToOppurtunity($scope.selectedQuoteAccountId, $scope.selectedOpportunityId);

            });
        }

        // cancel move quote
        $scope.cancelMoveQuote = function () {
            window.location.href = $scope.calendarModalService.moveQuote;
        }

        // selected existing opportunity method
        $scope.selectedOpportunity = function (id, index) {
            $scope.selectedOpportunityId = id;
            $scope.IsExistingOpportunity = true;
            $scope.IsCreateNewAccount = false;
            $scope.IsCreateNewOpportunity = false;
            var element = $(".optSelectRadio");
            element.attr("checked", false);
            $(element[index]).prop("checked", true);
            $("#optNewAccount").attr("checked", "false");
        }

        // selected new opportunity
        $scope.newOpportunity = function () {
            // $scope.IsCreateNewAccount = true;
            $scope.IsCreateNewOpportunity = true;
            $scope.IsExistingOpportunity = false;
            $(".optSelectRadio").attr("checked", false);
            if ($scope.apiFlag) {
                $scope.getOpportunityStatusData();
                $scope.getSalesAgentData();
                $scope.getAccountInstallationAddress();
                $scope.getAccountDetails($scope.selectedQuoteAccountId);
                $scope.apiFlag = false;
            }
        }

        // navigate to move quote page
        $scope.navigateMoveQuote = function (quoteKey, opportunityId, quoteStatusId, flag) {
            $scope.selectedQuoteKeyBackup = quoteKey;
            $scope.moveQuoteClicked = true;
            if (quoteStatusId == 2) {
                // get franchise date call
                $http.get("/api/timezones/0/GetFranchiseDateTime").then(function (data) {
                    var dateString = data.data;
                    $scope.date = $scope.calendarModalService.getDateStringValue(new Date(dateString), true, false, false);
                    var expireDateString;
                    if ($scope.quoteDataSource && $scope.quoteDataSource.length != 0) {
                        for (var i = 0; i < $scope.quoteDataSource.length; i++) {
                            if ($scope.quoteDataSource[i]['QuoteKey'] == $scope.selectedQuoteKeyBackup) {
                                expireDateString = $scope.quoteDataSource[i]['ExpiryDate'];
                            }
                        }
                    }
                    var expdate;

                    if (!flag) {
                        expdate = new Date($scope.Quotation.ExpiryDate);
                    } else {
                        expdate = new Date(expireDateString);
                    }
                    if (expdate < $scope.date) {
                        $scope.moveQuoteClicked = true;
                        $scope.DisplayPopup();
                    } else {
                        $scope.calendarModalService.moveQuote = window.location.href;
                        window.location.href = "#!/moveQuote/" + opportunityId + "/" + $scope.selectedQuoteKeyBackup;
                    }
                });
            }
            else {
                $scope.calendarModalService.moveQuote = window.location.href;
                // navigate to move quote page
                window.location.href = "#!/moveQuote/" + opportunityId + "/" + quoteKey;
            }
        }
        // move Quote function
        $scope.moveQuoteToOppurtunity = function (accountId, opportunityId) {
            // api to get the opportunity data related to particular account
            $http.get('/api/search/' + accountId + '/GetSearchOpportunitys?opportunityId=' + opportunityId).then(function (data) {
                $scope.AccountService.matchAccountOpportunity = data.data;
                var loadingElement = document.getElementById("loading");
                $scope.loadingElement.style.display = "none";
            });
        }

        $scope.$on("$locationChangeStart", function (event, next, current) {
            if (current.includes("moveQuote")) {
                $scope.calendarModalService.moveQuoteFlag = true;
            } else {
                $scope.calendarModalService.moveQuoteFlag = false;
            }
        });

        var url = window.location.href;
        if (url.includes("moveQuote")) {
            $scope.selectedOpportunityId = $routeParams.opportunityId;
            $scope.selectedQuoteKey = $routeParams.quoteKey;
            $scope.newQuoteOpportunityName = '';
            $scope.newQuoteSideMark = '';
            $scope.newQuoteOpportunityStatusId = '';
            $scope.newQuoteSalesAgentId = '';
            $scope.newOpportunityDescription = '';
            $scope.IsExistingOpportunity = false;
            $scope.IsCreateNewOpportunity = false;
            $scope.submitted = false;
            // $scope.IsCreateNewAccount = false;
            $scope.apiFlag = true;
            $scope.getQuoteDetails($scope.selectedQuoteKey, $scope.selectedOpportunityId);
        }

        if ($scope.calendarModalService.moveQuoteFlag) {
            $scope.QuoteinEditMode = false;
        }

        //Below method to show the html content of Job Crew Size
        $scope.ShowJobCrewSize = function () {
            $http.get('/api/Quotes/0/GetCrewSize').then(function (data) {
                var htmldata = data.data;
                if (htmldata == null)
                    htmldata = '';
                if ($.trim(htmldata) != '') {
                    $("#JobCrewSizeHTML").html(htmldata);
                }
                else {
                    $("#JobCrewSizeHTML").html('<span style="color:red">Job Crew Size Guidance not available</span>');
                }
                $("#JobCrewSize").modal("show");
            });
        }

        $scope.SurfacingjobEstimator = function () {
            window.location.href = "#!/surfacingJobEstimator/" + $scope.OpportunityId + "/" + $scope.quoteKey;
        }



        $scope.InitializeGroupDiscountGrid = function () {
            $scope.GridGroupDiscountOption = {
                dataSource: {
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    //data: $scope.Quotation.QuoteGroupDiscount,
                    schema: $scope.getGroupDiscountSchema()
                },
                columns: [
                    {
                        field: "ProductGroupDesc",
                        title: "Product Category",
                    },
                    {
                        field: "Discount",
                        title: "Discount (%)",
                        width: "20",
                        template: function (dataItem) {
                            if (dataItem.Discount != null)
                                return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Discount, 'decimal') + "</span>";
                            else
                                return "";
                        },
                        editor: numberEditor
                    },
                    {
                        field: "NoOfLines",
                        title: "Number of Lines",
                        template: function (dataItem) {
                            if (dataItem.NoOfLines != null)
                                return "<span class='Grid_Textalign'>" + dataItem.NoOfLines + "</span>";
                            else
                                return "";
                        },
                    }
                ],
                filterable: false,
                resizable: true,
                sortable: false,
                scrollable: true,
                pageable: false,
                editable: true,
                edit: function (e) {
                    e.container.find(".k-edit-label:last").hide();
                    e.container.find(".k-edit-field:last").hide();
                },
                columnResize: function (e) {
                    getUpdatedColumnList(e);
                }
            };
        }

        function numberEditor(container, options) {

            //if (options.model.Discount != null && options.model.Discount != 0) {
            //    var matched = $.grep($scope.Quotation.QuoteGroupDiscount, function (e) { return e.ProductGroupId == options.model.ProductGroupId && e.ProductTypeId == options.model.ProductTypeId && e.Discount != 0 && e.Discount != null });
            //    if (matched[0] != null) {
            //        $("<span class='Grid_Textalign'>" + options.model.Discount + "</span>").appendTo(container);
            //        container.removeClass('k-edit-cell');
            //    }
            //    else {
            //        $('<input name="' + options.field + '" id="' + options.field + '" data-bind="value:' + options.field + '"/>')
            //        .appendTo(container)
            //        .kendoNumericTextBox({
            //            format: "{0:n2}",
            //            decimals: 2,
            //            min: 0,
            //            spinners: false
            //        });
            //    }
            //}
            //else {
            $('<input name="' + options.field + '" id="' + options.field + '" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoNumericTextBox({
                    format: "{0:n2}",
                    decimals: 2,
                    min: 0,
                    spinners: false
                });
            //}
        }

        $scope.getGroupDiscountSchema = function () {
            return {
                model: {
                    id: "id",
                    fields: {
                        QuoteGroupDiscountId: { editable: false },
                        QuoteKey: { editable: false },
                        ProductGroupId: { editable: false },
                        ProductGroupDesc: { editable: false },
                        Discount: { editable: true, type: "number" },
                        NoOfLines: { editable: false }
                    }
                }
            }
        }

        $scope.InitializeGroupDiscountGrid();
        $scope.ShowGroupDiscountPopup = function () {

            $('[data-toggle="tooltip"]').tooltip('hide');

            var result = $scope.editUpdateQuoteLines();
            if (result == false)
                return false;

            var QuoteLineDataSource = $("#QuotelinesGrid").data("kendoGrid").dataSource;
            var dtQuoteGroupDiscount = JSON.parse(JSON.stringify($scope.Quotation.QuoteGroupDiscount));

            $.each(dtQuoteGroupDiscount, function (key, value) {
                dtQuoteGroupDiscount[key].NoOfLines = 0;
            });

            if (QuoteLineDataSource != null) {
                var QuoteLineDataSourceData = QuoteLineDataSource.data();
                for (var i = 0; i < QuoteLineDataSourceData.length; i++) {
                    if (QuoteLineDataSourceData[i].ProductTypeId == 1 || QuoteLineDataSourceData[i].ProductTypeId == 2) {
                        var ProductTypeId = QuoteLineDataSourceData[i].ProductTypeId;

                        var matched = $.grep(dtQuoteGroupDiscount, function (e) { return e.ProductGroupId == QuoteLineDataSourceData[i].PSProductCategoryId && e.ProductTypeId == ProductTypeId });
                        if (matched[0] == null) {
                            var obj = { QuoteKey: $scope.quoteKey, ProductTypeId: ProductTypeId, ProductGroupId: QuoteLineDataSourceData[i].PSProductCategoryId, ProductGroupDesc: QuoteLineDataSourceData[i].PSProductCategory, NoOfLines: 1 };
                            dtQuoteGroupDiscount.push(obj);
                        }
                        else {
                            matched[0].NoOfLines = matched[0].NoOfLines + 1;
                        }
                    }
                }
            }
            if (dtQuoteGroupDiscount.length > 0) {
                var DataSource = new kendo.data.DataSource({ data: dtQuoteGroupDiscount, schema: $scope.getGroupDiscountSchema() });
                DataSource.sort({ field: "ProductGroupDesc", dir: "asc" });
                var grid = $("#GridGroupDiscount").data("kendoGrid");
                grid.setDataSource(DataSource);

                $("#GroupDiscount").modal("show");

                if ($scope.QuoteinEditMode)
                    grid.dataSource.at(0).fields["Discount"].editable = true;
                else
                    grid.dataSource.at(0).fields["Discount"].editable = false;
            }
            else
                HFC.DisplayAlert("Core Product or My Product is not available!");
        }

        $scope.ApplyGroupDiscount = function () {

            var Data = $("#GridGroupDiscount").data("kendoGrid").dataSource.data();
            var NoOfCategory = 0;
            for (var i = 0; i < Data.length; i++) {
                if (Data[i].Discount && Data[i].Discount != 0)
                    NoOfCategory = NoOfCategory + 1;
            }
            if (NoOfCategory != 0)
                $("#NoOfLinesApplied").text(NoOfCategory);
            else
                $("#NoOfLinesApplied").text('');

            $scope.Quotation.QuoteGroupDiscount = Data;
            loadingElement.style.display = "block";
            $http.post('/api/Quotes/' + $scope.quoteKey + '/ApplyGroupDiscount', $scope.Quotation.QuoteGroupDiscount).then(function (response) {
                if (response.data.error == '') {
                    $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                    $scope.Quotation = response.data.data;
                    setQuoteHeaderDiscountValue();
                    $scope.QuotationCopy = angular.copy($scope.Quotation);
                    loadingElement.style.display = "none";
                    $("#GroupDiscount").modal("hide");
                    $scope.ResetQuoteTax();
                }
                else {
                    loadingElement.style.display = "none";
                    HFC.DisplayAlert("We're Sorry this feature is currently unavailable. Please refresh your screen or try again in a few minutes");
                }
            }).catch(function (error) {
                loadingElement.style.display = "none";

                HFC.DisplayAlert("We're Sorry this feature is currently unavailable. Please refresh your screen or try again in a few minutes");
            });
        }
        $scope.UpdateGroupDiscountLabel = function () {
            var NoOfCategory = 0;

            var dtQuoteGroupDiscount = JSON.parse(JSON.stringify($scope.Quotation.QuoteGroupDiscount));

            for (var i = 0; i < dtQuoteGroupDiscount.length; i++) {
                if (dtQuoteGroupDiscount[i].Discount && dtQuoteGroupDiscount[i].Discount > 0)
                    NoOfCategory = NoOfCategory + 1;
            }

            if (NoOfCategory != 0)
                $("#NoOfLinesApplied").text(NoOfCategory);
            else
                $("#NoOfLinesApplied").text('');
        }

        if ($scope.OpportunityId) {
            $http.get('/api/Quotes/0/GetDetails?oppurtunityId=' + $scope.OpportunityId).then(function (data) {
                $scope.dropdownlist = data.data.DiscountsList;
                quote_discount();
            });
        }


        function quote_discount() {
            var discountlist = $("#quote_discount").data("kendoDropDownList");
            if (discountlist == undefined) return;
            discountlist.setOptions({
                dataTextField: "Name",
                dataValueField: "DiscountId",
                optionLabel: {
                    Name: " ",
                    DiscountId: null
                },
                change: function () {
                    $scope.oneHeaderDiscountChange()
                }
            });
            var dataSource = new kendo.data.DataSource({
                data: $scope.dropdownlist
            });
            discountlist.setDataSource(dataSource);
        }
        function onChange(arg) {
            $scope.selectedLinesAfterSelection = [];
            for (var key in arg.sender._selectedIds) {
                $scope.selectedLinesAfterSelection.push(key);
            }
        }
        $scope.ValidateDate = function (date, id) {
            date = $("#" + id + "").val();
            $scope.ValidDate = HFCService.ValidateDate(date, "date");
            if ($scope.ValidDate) {
                $("#" + id + "").removeClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).removeClass("icon_Background");
                $("#" + id + "").addClass("frm_controllead1");
            }
            else {
                $("#" + id + "").addClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).addClass("icon_Background");
                $("#" + id + "").removeClass("frm_controllead1");
            }
        }

        function setQuoteHeaderDiscountValue() {
            // update scope variable value
            $scope.Quotation.DiscountIdBackup = $scope.Quotation.DiscountId;
            setTimeout(function () {
                var discountvalue = $("#quote_discount").data("kendoDropDownList");
                if (discountvalue != undefined) {
                    discountvalue.value($scope.Quotation.DiscountIdBackup);
                }
            }, 1500);
        }
    }
]);
