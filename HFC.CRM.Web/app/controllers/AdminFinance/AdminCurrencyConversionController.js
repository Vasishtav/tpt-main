﻿
// TODO: rquired refactoring:

'use strict';
appCP
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('AdminCurrencyConversionController',
        [
            '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
            , '$templateCache', '$sce', '$compile', '$http', '$q', 'FranchiseService', '$filter', 'NavbarServiceCP',
            function ($scope, $window, HFCService, $routeParams, $location, $rootScope
                , $templateCache, $sce, $compile, $http, $q, FranchiseService, $filter, NavbarServiceCP) {

                $scope.NavbarServiceCP = NavbarServiceCP;
                $scope.NavbarServiceCP.CurrencyCurrrencyTab();
                $scope.FranchiseService = FranchiseService;
                $scope.HFCService = HFCService;

                $scope.CurrencySubmitted = false;
                $scope.DateValue = '';

                HFCService.setHeaderTitle("Currency Conversion #");
                var offsetvalue = 0;
                $scope.Permission = {};
                var Currencypermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Currency')
                if (Currencypermission) {
                    $scope.Permission.ViewCurrency = Currencypermission.CanRead;
                    $scope.Permission.AddCurrency = Currencypermission.CanCreate;
                }
                //flag for date validation
                $scope.ValidStartDate = true;
                $scope.ValidEndDate = true;

                //Add currency button code
                $scope.AddCurrencyConversion = function () {
                    $scope.EditMode = true;
                    var date = new Date();
                    $scope.DateValue = $filter('date')(new Date(), 'M/d/yyyy');
                    $scope.EndDateValue = "";
                }

                //Save currency code
                $scope.SaveCurrency = function () {
                    var grid = $('#gridCurrencyAdd').data().kendoGrid.dataSource.view();
                    var validator = $scope.kendoValidator("gridCurrencyAdd");

                    $scope.DateValue = $("input[name=endDate]").val();
                    $scope.EndDateValue = $("input[name=endDateValues]").val();

                    var todaydate = new Date();
                    todaydate = new Date(todaydate.setHours(0, 0, 0, 0));
                    var selecteddate = new Date($scope.DateValue);

                    if ($scope.EndDateValue != "") {
                        var selectedenddate = new Date($scope.EndDateValue);
                    }

                    //restrict saving for invalid date
                    if (!$scope.ValidStartDate && !$scope.ValidEndDate) {
                        HFC.DisplayAlert("Invalid Date!");
                        $scope.IsBusy = false;
                        return;
                    }
                    else if (!$scope.ValidStartDate) {
                        HFC.DisplayAlert("Invalid Start Date!");
                        $scope.IsBusy = false;
                        return;
                    }
                    else if (!$scope.ValidEndDate) {
                        HFC.DisplayAlert("Invalid End Date!");
                        $scope.IsBusy = false;
                        return;
                    }

                    if (selecteddate < todaydate) {
                        HFC.DisplayAlert("Date Value Must Be Greater Or Equal To Today's Date");
                        return;
                    }
                    if (selectedenddate != undefined) {
                        //if (selectedenddate < selecteddate)
                        if (!(selectedenddate > selecteddate)) {
                            HFC.DisplayAlert("End Date Value Must Be Greater Than Today's Date");
                            return;
                        }
                    }




                    //dto = $scope.DateValue;
                    var grdData = $('#gridCurrencyAdd').data().kendoGrid._data;
                    for (var i = 0; i < grdData.length; i++) {
                        if (grdData[i].ToRate === null || grdData[i].ToRate === undefined || isNaN(Number(grdData[i].ToRate))) {
                            return;
                        }
                        if (grdData[i].FromRate === null || grdData[i].FromRate === undefined || isNaN(Number(grdData[i].FromRate))) {
                            return;
                        }
                    }
                    if (validator.validate()) {
                        $scope.CurrencySubmitted = true;
                        if (grid.length == undefined || grid.length == 0) {
                            //var dto = grid[grid.length - 1];
                            var dto = $('#gridCurrencyAdd').data().kendoGrid._data;
                            for (i = 0; i < dto.length; i++) {
                                dto[i].DateValue = $scope.DateValue;
                                dto[i].EndDateValue = $scope.EndDateValue;
                            }
                        }

                        else {
                            //var dto = grid[grid.length - 1];
                            var dto = $('#gridCurrencyAdd').data().kendoGrid._data;
                            for (i = 0; i < dto.length; i++) {
                                dto[i].DateValue = $scope.DateValue;
                                dto[i].EndDateValue = $scope.EndDateValue;
                            }

                        }



                        $http.post('/api/AdminCurrency/0/SaveCurrency', dto).then(function (response) {
                            var status = response.data;
                            if (response.data !== "true") {
                                HFC.DisplayAlert(response.data);
                                $scope.EditMode = false;
                                $("#gridCurrency").data("kendoGrid").dataSource.read();
                            }
                            else {
                                HFC.DisplaySuccess("Currency Saved Successfully!");
                                $scope.EditMode = false;
                                $("#gridCurrency").data("kendoGrid").dataSource.read();
                            }
                        });
                    }
                    return;
                }

                $scope.kendoValidator = function (gridId) {
                    return $("#" + gridId).kendoValidator({
                        validate: function (e) {
                            $("span.k-invalid-msg").hide();
                            var dropDowns = $(".k-dropdown");
                            $.each(dropDowns, function (key, value) {
                                var input = $(value).find("input.k-invalid");
                                var span = $(this).find(".k-widget.k-dropdown.k-header");
                                if (input.size() > 0) {
                                    $(this).addClass("dropdown-validation-error");
                                } else {
                                    $(this).removeClass("dropdown-validation-error");
                                }
                            });
                        }
                    }).getKendoValidator();
                }

                //Cancel button code
                $scope.cancelCurrencygrid = function () {
                    $scope.EditMode = false;
                }

                //code for kendo grid view
                $scope.GetCurrencyValue = function () {
                    $scope.GridEditable = false;
                    $scope.CurrencyConversion = {
                        dataSource: {
                            transport: {
                                read: {
                                    url: function (params) {
                                        var url1 = '/api/AdminCurrency/0/Currencydetails';
                                        return url1;
                                    },

                                    dataType: "json"
                                }

                            },
                            error: function (e) {

                                HFC.DisplayAlert(e.errorThrown);
                            },

                            schema: {
                                model: {
                                    id: "id",
                                    fields: {

                                        Currency: { editable: false },
                                        StartDate: { type: 'date', editable: false },
                                        EndDate: { type: 'date', editable: false },
                                        FromRate: { type: 'string', editable: false, },
                                        ToRate: { type: 'string', editable: false },
                                    }
                                }
                            }
                        },

                        columns: [
                            {
                                field: "Currency",
                                title: "Currency",
                                width: "250px",
                                filterable: { multi: true, search: true },
                                hidden: false,
                            },
                            {
                                field: "StartDate",
                                title: "Start Date",
                                type: "date",
                                filterable: HFCService.gridfilterableDate("date", "CurrencyStartDateFilterCalendar", offsetvalue),
                                template: function (dataitem) {
                                    if (dataitem.StartDate == null)
                                        return "";
                                    else
                                        return HFCService.KendoDateFormat(dataitem.StartDate);
                                }
                                //"#=  (StartDate == null)? '' : kendo.toString(kendo.parseDate(StartDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                            },
                            {
                                field: "EndDate",
                                title: "End Date",
                                type: "date",
                                filterable: HFCService.gridfilterableDate("date", "CurrencyEndDateFilterCalendar", offsetvalue),
                                template: function (dataitem) {
                                    if (dataitem.EndDate == null)
                                        return "";
                                    else
                                        return HFCService.KendoDateFormat(dataitem.EndDate);
                                },
                                //"#=  (EndDate == null)? '' : kendo.toString(kendo.parseDate(EndDate, 'yyyy-MM-dd'), 'M/d/yyyy') #",
                                hidden: false,
                            },
                            {
                                field: "FromRate",
                                title: "From Rate",
                                hidden: false,
                                filterable: {
                                    //multi: true,
                                    search: true
                                },
                            },
                            {
                                field: "ToRate",
                                title: "To Rate",
                                hidden: false,

                                filterable: {
                                    //multi: true,
                                    search: true
                                },
                            },
                        ],
                        pageable: {
                            refresh: true,
                            pageSize: 25,
                            pageSizes: [25, 50, 100],
                        },
                        filterable: {
                            extra: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    isempty: "Empty",
                                    isnotempty: "Not empty"


                                },
                                number: {
                                    eq: "Equal to",
                                    neq: "Not equal to",
                                    gte: "Greater than or equal to",
                                    lte: "Less than or equal to"
                                },
                                date: {
                                    gt: "After (Excludes)",
                                    lt: "Before (Includes)"
                                },
                            }
                        },
                        resizable: true,
                        noRecords: { template: "No records found" },
                        scrollable: true,
                        toolbar: [
                            {
                                template: kendo.template($("#headToolbarTemplate").html()),
                            }]
                    };
                };
                $scope.GetCurrencyValue();

                ////code for kendo grid Add new value
                $scope.AddCurrencyValue = function () {
                    $scope.GridEditable = false;
                    $scope.CurrencyConversionAdd = {
                        dataSource: {
                            transport: {
                                read: {
                                    url: function (params) {
                                        //var url1 = '/api/AdminCurrency/0/Currencydetails';
                                        var url1 = '/api/AdminCurrency/0/CountryCurrency';
                                        return url1;
                                    },

                                    dataType: "json"
                                }

                            },
                            error: function (e) {

                                HFC.DisplayAlert(e.errorThrown);
                            },

                            schema: {
                                model: {
                                    id: "id",
                                    fields: {

                                        Country: { editable: false },
                                        FromType: { editable: true, validation: { required: true } },
                                        FromRate: { editable: true, validation: { required: true } },
                                        ToType: { editable: false, },
                                        ToRate: { editable: true, validation: { required: true } },
                                    }
                                }
                            }
                        },

                        columns: [
                            {
                                field: "Country",
                                title: "Currency",
                                width: "250px",
                                filterable: { multi: true, search: true },
                                hidden: false,
                            },
                            {
                                field: "FromType",
                                title: "From Calc Type",
                                //editor: '<input type="text" name="FromType" data-bind="value:FromType"/>',
                                editor: function (container, options) {
                                    $('<input id="FromType"  required style=" "   name="FromType" data-bind="value:FromType"/>')
                                        .appendTo(container)
                                        .kendoDropDownList({
                                            autoBind: false,
                                            optionLabel: "Select",

                                            valuePrimitive: true,
                                            dataTextField: "Value",
                                            dataValueField: "Key",
                                            template: "#=Value #",
                                            change: function (e) {
                                                var item = $('#gridCurrencyAdd').find('.k-grid-edit-row');
                                                item.data().uid
                                                var dataItem = item.data().$scope.dataItem;
                                                DropdownChange(dataItem);
                                            },
                                            dataSource: {
                                                data:
                                                    [{ Key: "*", Value: "*" }, { Key: "/", Value: "/" }]
                                            }
                                            //dataSource: {
                                            //    transport: {
                                            //        read: {
                                            //            //url: '/api/PricingManagement/0/GetPricingStrategyDropdown',
                                            //            url: '/api/AdminCurrency/0/GetVendorDropdownStatus',
                                            //        }
                                            //    },
                                            //}

                                        });
                                }

                            },
                            {
                                field: "FromRate",
                                title: "From Rate",
                                template:
                                    ' #if(FromRate!==null){# #: FromRate # #}else{#<div style="color: red; ">Required</div>#}#',
                                editor: '<input  required=true name=" FromRate " data-bind="value:FromRate" style="width: 230px;border: none "/>',

                            },
                            {
                                field: "ToType",
                                title: "To Calc Type",
                                hidden: false,
                                filterable: {
                                    //multi: true,
                                    search: true
                                },
                                editor: '<input type="text" name="ToType" data-bind="value:ToType"/>',
                            },
                            {
                                field: "ToRate",
                                title: "To Rate",
                                hidden: false,
                                template:
                                    ' #if(ToRate!==null){# #: ToRate # #}else{#<div style="color: red; ">Required</div>#}#',
                                editor: '<input  required=true name=" ToRate " data-bind="value:ToRate" style="width: 230px;border: none "/>',
                                filterable: {
                                    //multi: true,
                                    search: true
                                },
                            },
                        ],
                        pageable: {
                            refresh: true,
                            pageSize: 25,
                            pageSizes: [25, 50, 100],
                        },
                        editable: true,

                        filterable: {
                            extra: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    isempty: "Empty",
                                    isnotempty: "Not empty"


                                },
                                number: {
                                    eq: "Equal to",
                                    neq: "Not equal to",
                                    gte: "Greater than or equal to",
                                    lte: "Less than or equal to"
                                },
                                date: {
                                    gt: "After (Excludes)",
                                    lt: "Before (Includes)"
                                },
                            }
                        },
                        resizable: true,
                        noRecords: { template: "No records found" },
                        scrollable: true,
                        toolbar: [
                            {
                                template: kendo.template($("#headToolbarTemplate").html()),
                            }]

                    };
                };
                $scope.AddCurrencyValue();

                //on change function call-for dropdown
                function DropdownChange(obj) {
                    var value = obj;
                    if (value.FromType == "*") {
                        obj.ToType = "/";
                    }
                    else if (value.FromType == "/") {
                        obj.ToType = "*";
                    }

                }

                //grid search -box code
                $scope.CurrencygridSearch = function () {
                    var searchValue = $('#searchBox').val();
                    $("#gridCurrency").data("kendoGrid").dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Currency",
                                operator: "contains",
                                value: searchValue
                            },
                            //{
                            //    field: "StartDate",
                            //    operator: "contains",
                            //    value: searchValue
                            //},
                            //{
                            //    field: "EndDate",
                            //    operator: "contains",
                            //    value: searchValue
                            //},
                            {
                                field: "FromRate",
                                operator: "contains",
                                value: searchValue
                            },
                            {
                                field: "ToRate",
                                operator: "contains",
                                value: searchValue
                            }

                        ]
                    });

                }

                $scope.CurrencygridClear = function () {
                    $('#searchBox').val('');
                    $("#gridCurrency").data('kendoGrid').dataSource.filter({
                    });
                }

                //for date validation
                $scope.ValidateDate = function (date, id) {
                    if (id == "StartdateValue") {
                        $scope.ValidStartDate = HFCService.ValidateDate(date, "date");

                        if ($scope.ValidStartDate) {
                            $("#StartdateValue").removeClass("gridinvalid_Date");
                            var a = $('#StartdateValue').siblings();
                            $(a[0]).removeClass("icon_Background");
                        }
                        else {
                            $("#StartdateValue").addClass("gridinvalid_Date");
                            var a = $('#StartdateValue').siblings();
                            $(a[0]).addClass("icon_Background");
                        }
                    }
                    else if (id == "EnddateValue") {
                        if (date == "")
                            $scope.ValidEndDate = true;
                        else
                            $scope.ValidEndDate = HFCService.ValidateDate(date, "date");

                        if ($scope.ValidEndDate) {
                            $("#EnddateValue").removeClass("gridinvalid_Date");
                            var a = $('#EnddateValue').siblings();
                            $(a[0]).removeClass("icon_Background");
                        }
                        else {
                            $("#EnddateValue").addClass("gridinvalid_Date");
                            var a = $('#EnddateValue').siblings();
                            $(a[0]).addClass("icon_Background");
                        }
                    }
                }

            }

        ])
