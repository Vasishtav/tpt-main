﻿/***************************************************************************\
Module Name:  leadController.js - leadController AngularJS
Project: HFC
Created on: 15 Oct 2017
Created By:
Copyright:
Description: Adding Editing and save the lead information
Change History:
Date  By  Description

\***************************************************************************/

'use strict';

app.controller('CommunicationListingController', [
    '$scope', '$routeParams', '$rootScope', '$http', '$window',
    '$location', '$modal', 'CalendarService', 'NoteServiceTP', 'PersonService', 'FileprintService', 'LeadService', 'LeadAccountService',
    'AccountService', 'HFCService', 'NavbarService', 'EmailQuoteService',
function (
    $scope, $routeParams, $rootScope, $http, $window,
    $location, $modal, CalendarService, NoteServiceTP, PersonService, FileprintService, leadService, leadAccountService, AccountService,
    HFCService, NavbarService, EmailQuoteService) {

    var self = this;
    //self.Lead = {};
    $scope.HFCService = HFCService;
    $scope.BrandId = $scope.HFCService.CurrentBrand;
    $scope.NoteServiceTP = NoteServiceTP;
    $scope.PersonService = PersonService;
    $scope.FileprintService = FileprintService;
    $scope.EmailQuoteService = EmailQuoteService;
    

    self.leadId = $routeParams.leadId;
    self.accountId = $routeParams.accountId;
    self.opportunityId = $routeParams.opportunityId;
    $scope.EmailSent = false;

    $scope.NavbarService = NavbarService;
    var ul = $location.absUrl().split('#!')[1];
    if (ul.includes('leadCommunication')) {
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesLeadsTab();
    }
    if (ul.includes('accountCommunication')) {
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesAccountsTab();
    }
    if (ul.includes('opportunityCommunication')) {
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesOpportunitiesTab();
    }

    $scope.Permission = {};
    var Leadpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Lead')
    var ConvertLeadpermission = Leadpermission.SpecialPermission.find(x=>x.PermissionCode == 'ConvertLead').CanAccess;
    var Accountpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Account')
    var Opportunitypermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity')

    $scope.Permission.AddLead = Leadpermission.CanCreate; //HFCService.GetPermissionTP('Add Lead').CanAccess
    $scope.Permission.EditLead = Leadpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Lead').CanAccess
    $scope.Permission.ConvertLead = ConvertLeadpermission; //HFCService.GetPermissionTP('Convert Lead').CanAccess
    $scope.Permission.AddEvent = Leadpermission.CanCreate; //HFCService.GetPermissionTP('Add Event').CanAccess

    $scope.Permission.ListCommunication = Leadpermission.CanRead; //HFCService.GetPermissionTP('Communications List-Lead').CanAccess
    $scope.Permission.ListCommunicationAccount = Accountpermission.CanRead; //HFCService.GetPermissionTP('Communications List-Account').CanAccess
    $scope.Permission.ListCommunicationOpportunity = Opportunitypermission.CanRead; //HFCService.GetPermissionTP('Communications List-Opportunity').CanAccess

    $scope.Permission.AddAccount = Accountpermission.CanCreate; //HFCService.GetPermissionTP('Add Account').CanAccess
    $scope.Permission.AccountEdit = Accountpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Account').CanAccess

    $scope.Permission.AddOpportunity = Opportunitypermission.CanCreate; //HFCService.GetPermissionTP('Add Opportunity').CanAccess
    $scope.Permission.OpportunityEdit = Opportunitypermission.CanUpdate; //HFCService.GetPermissionTP('Edit Opportunity').CanAccess


    //For Appointment
    HFCService.GetUrl();

    //https://rclayton.silvrback.com/pubsub-controller-communication
    // Sample publisher to update the grid after editing a calender
    // or newly added calendar. This code should be moved to the new 
    // appointment popup.
    self.updateCommunicationGrid = function () {
        $rootScope.$broadcast("message_Communication_update", new Date());
    }
   
    $scope.modalState = {
        loaded: true
    }

    $scope.$watch('modalState.loaded', function () {
        var loadingElement = document.getElementById("loading");

        if (!$scope.modalState.loaded) {
            loadingElement.style.display = "block";
        } else {
            loadingElement.style.display = "none";
        }
    });

    function errorHandler(reseponse) {
        

        // Inform that the modal is ready to consume:
        $scope.modalState.loaded = true;
        HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
    }

    if (self.leadId > 0) {
        // Inform that the modal is not yet ready
        $scope.modalState.loaded = false;

        leadService.Get(self.leadId, function (response) {
            var lead = response.Lead;
            self.Lead = lead;

            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFCService.setHeaderTitle("Lead Communication #" + self.leadId + " - " + self.Lead.PrimCustomer.FirstName + " " + self.Lead.PrimCustomer.LastName);
        }, errorHandler);
    }
    if (self.accountId > 0) {
        // Inform that the modal is not yet ready
        $scope.modalState.loaded = false;

        leadAccountService.Get(self.accountId, function (response) {
            var account = response.Account;
            self.Account = account;

            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFCService.setHeaderTitle("Account Communication #" + self.accountId + " - " + self.Account.PrimCustomer.FirstName + " " + self.Account.PrimCustomer.LastName);
        }, errorHandler);
    }

    if (self.opportunityId > 0) {

        // Inform that the modal is not yet ready
        $scope.modalState.loaded = false;

        $http.get('/api/Opportunities/' + self.opportunityId)
            .then(function (response) {
                var opportunity = response.data.Opportunity;
                self.Opportunity = opportunity;

                // Inform that the modal is ready to consume:
                $scope.modalState.loaded = true;
                HFCService.setHeaderTitle("Opportunity Communication #" + self.opportunityId + " - " + self.Opportunity.OpportunityName);
            }, errorHandler);
    }

   

    $scope.ClickMeToRedirectEditLead = function () {

        //var url = "http://" + $window.location.host + "/#!/leadsEdit/" + self.leadId;
        //$window.location.href = url;
        $window.location.href = "#!/leadsEdit/" + self.leadId;
    };
    $scope.Newleadpage = function () {

        //var url = "http://" + $window.location.host + "/#!/leads/";
        //$window.location.href = url;
        $window.location.href = "#!/leads";
    };
    $scope.ConvertLead = function () {
        $http.get('/api/search/' + self.leadId + '/GetLeadMatchingAccounts').then(function (data) {
            $scope.MatchLeadAccount = data.data;
        });
        window.location = '#!/leadConvert/' + self.leadId;
    }
    $scope.CreateNewAccount = function () {
        $window.location.href = "#!/account";
    }
    $scope.EditAccount = function (accountId) {
        $window.location.href = "#!/accountEdit/" + accountId;
    }
    $scope.CreateNewOpportunity = function (accountid) {
        window.location = "#!/Opportunitys/" + accountid;
    }
    $scope.EditOpportunity = function (opportunityId) {
        window.location.href = "#!/opportunityEdit/" + opportunityId;
    }

    $scope.EmailQuoteService.EmailSuccessCallback = function () {
        
        self.updateCommunicationGrid();
    }

    $scope.GetEmailstatus = function () {
        
        var leadid = self.leadId;
        if(leadid)
        {
            $http.get('/api/Leads/0/GetEmailStatus?leadid=' + leadid).then(function (response) {
                
                if (response.data == "True") {
                    $scope.EmailSent = true;
                }
                else $scope.EmailSent = false;

            });
        }
    }

    $scope.GetEmailstatus();

    //code for sending email when email button clicked.
    $scope.SendLeadEmail = function () {
        $scope.EmailNotify = false;
        var leadid = self.leadId;
        var isnotifyemail = self.Lead.IsNotifyemails;
        if (isnotifyemail == true) {

        
        $scope.modalState.loaded = false;
        $http.get('/api/Leads/0/SendEmailToLead?leadid=' + leadid).then(function (response) {

            if (response.data == "True") {
                $scope.EmailSent = true;
                $scope.MailSent = true;

                if (self.Lead.PrimCustomer.SecondaryEmail && self.Lead.PrimCustomer.PrimaryEmail)
                    $scope.Mailid = self.Lead.PrimCustomer.PrimaryEmail + ',' + self.Lead.PrimCustomer.SecondaryEmail;
                else if (self.Lead.PrimCustomer.PrimaryEmail)
                    $scope.Mailid = self.Lead.PrimCustomer.PrimaryEmail;
                else if (self.Lead.PrimCustomer.SecondaryEmail)
                    $scope.Mailid = self.Lead.PrimCustomer.SecondaryEmail;

                $scope.modalState.loaded = true;
                self.updateCommunicationGrid();
                $("#AfetrThanks_EmailSend").modal("show");
                return;
            }
            else if(response.data=="Success") 
            {
                $scope.MailSent = false;
                $scope.EmailSent = false;
                $scope.modalState.loaded = true;
                $("#AfetrThanks_EmailSend").modal("show");
                return;
            }
        else {
            $scope.EmailNotify = true;
            $scope.modalState.loaded = true;
            $("#AfetrThanks_EmailSend").modal("show");
            return;
        }

            });
        }
        else {
            $scope.EmailNotify = true;
            $scope.modalState.loaded = true;
            $("#AfetrThanks_EmailSend").modal("show");
            return;
        }
    }

    $scope.EmailMailClose = function () {
        
        $("#AfetrThanks_EmailSend").modal("hide");
        return;
    }

    // print integration -- murugan
    $scope.printControl = {};
    $scope.printSalesPacket = function (modalid) {

        if (self.leadId) {
            $scope.leadIdPrint = $routeParams.leadId;
        }
        else if (self.opportunityId) {
            $scope.opportunityIdPrint = $routeParams.opportunityId;
        }

        //if ($routeParams.leadId) $scope.leadIdPrint = $routeParams.leadId;

        // $("#" + modalid).modal("show");
        $scope.printControl.showPrintModal();
    }

    //$scope.printControl = {};
    //$scope.printSalesPacket = function (modalid) {
    //    if ($routeParams.opportunityId) $scope.opportunityIdPrint = $routeParams.opportunityId;

    //    // $("#" + modalid).modal("show");
    //    $scope.printControl.showPrintModal();
    //}
}
]);




