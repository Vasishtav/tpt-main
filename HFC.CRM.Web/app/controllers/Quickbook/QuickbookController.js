﻿(function (window, document, undefined) {

    'use strict';

    app.controller('QuickbookController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
                        'HFCService', 'NavbarService',
        function ($scope, $routeParams, $rootScope, $window, $location, $http,
            HFCService, NavbarService) {

            if (window.location.href.toLowerCase().endsWith("/qboconnectclose")) {
                $http.get('../api/Quickbooks/0/QBOSessioninfo/').then(function (response) {
                    window.close();
                });
            }

            $scope.IsSalesTaxCodeDisabled = true;
            $scope.IsSalesTaxCodeEditLinkshow = true;
            $scope.IsCOAARDisabled = true;
            $scope.IsCOASalesDisabled = true;
            $scope.IsCOACOGDisabled = true;

            $scope.NavbarService = NavbarService;
            $scope.NavbarService.SelectSettings();
            $scope.NavbarService.EnableSettingsGeneralTab();
            //valid date flag
            $scope.ValidDate = true;

            // This method trigger when QBINTRNL page
            $scope.initQBApp = function () {
                $scope.QBApp = {};
                $scope.GetQBApp();
            };

            $scope.HFCService = HFCService;
            $scope.Permission = {};
            var Quickbookpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode === 'QuickBooksSettings')
            var QuickSyncpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode === 'QuickbooksSync')

            $scope.Permission.Quickbookdisplay = Quickbookpermission.CanRead; //$scope.HFCService.GetPermissionTP('QuickBooks Settings-QBO Display').CanAccess;
            $scope.Permission.Quickbookadd = Quickbookpermission.CanCreate; //$scope.HFCService.GetPermissionTP('QuickBooks Settings-QBO Add').CanAccess;
            $scope.Permission.QuickbookSyncdetails = QuickSyncpermission.CanRead; //$scope.HFCService.GetPermissionTP('Quickbooks Sync Detail').CanAccess;
            $scope.Permission.QuickbookSyncadd = QuickSyncpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Quickbooks Sync Add').CanAccess;
            $scope.Permission.QuickbookEdit = QuickSyncpermission.CanUpdate;

            HFCService.setHeaderTitle("Quick Books #");
            // save qb app info
            $scope.SaveQBApp = function () {
                //restrict saving for invalid date
                if (!$scope.ValidDate) {
                    HFC.DisplayAlert("Invalid Date!");
                    return;
                }

                $http.post('../api/Quickbooks/0/SaveQBApp/', $scope.QBApp).then(function () {
                    $scope.IsCOAARDisabled = true;
                    $scope.IsCOASalesDisabled = true;
                    $scope.IsCOACOGDisabled = true;
                    window.alert("Saved Successfully");
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                });
            };

            // QB online sync 
            var loadingElement = document.getElementById("loading");
            $scope.Sync = function () {
                loadingElement.style.display = "block";                
                $scope.IsSalesTaxCodeDisabled = true;
                $scope.IsSalesTaxCodeEditLinkshow = false;
                function PreSyncCheckSuccess(result) {
                    loadingElement.style.display = "none";
                    if ($scope.preSyncCheck.Correct || window.confirm("There are errors that will prevent syncing some records, would you like to: \"Sync now with errors\" or \"Cancel the Sync and go correct the errors\". The sync will not run unless the user clicks \"OK\"")) {
                        $scope.state.IsLocked = true;
                        $http.post('/api/quickbooks/' + 0 + '/sync/').then(function (response) {
                            HFC.DisplaySuccess("QBO Sync Successfully");
                            $scope.IsSalesTaxCodeEditLinkshow = true;
                            $scope.UpdateState();
                        }, function (response) {
                            HFC.DisplayAlert(response.statusText);
                            $scope.IsSalesTaxCodeEditLinkshow = true;
                            $scope.UpdateState();
                        });
                    } else {
                        $scope.IsSalesTaxCodeEditLinkshow = true;
                    }
                }

                $scope.PreSyncCheck(PreSyncCheckSuccess);

            }

            // Get QB app info
            $scope.GetQBApp = function () {

                $http.get('../api/Quickbooks/0/GetQBApp/').then(function (response) {
                    $scope.QBApp = {};

                    if (response.data == null) {
                        $scope.QBApp = null;
                        return;
                    }

                    $scope.QBApp.FranchiseId = 0;
                    $scope.QBApp.UserName = '';
                    $scope.QBApp.Password = '';
                    $scope.QBApp.SynchInvoices = true;
                    $scope.QBApp.SynchCost = true;
                    $scope.QBApp.SynchPayments = true;
                    $scope.QBApp.IsLastNameFirst = false;
                    $scope.QBApp.SuppressSalesTax = true;

                    $scope.QBApp = angular.copy(response.data);

                }, function (response) {
                    $scope.QBApp = {};
                    //HFC.DisplayAlert(response.statusText);
                });
            };

            // Disconnect from QB online
            $scope.Disconect = function () {
                $http.post('/api/quickbooks/0/disconect/').then(function (response) {
                    alert("Disconnected Successfully");
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                });
                return false;
            }

            $scope.UpdateStateInProgress = false;

            // QBO Pre sync check
            $scope.PreSyncCheck = function (sucessCallback) {
                $http.post('/api/quickbooks/0/PreSyncCheck/').then(function (response) {
                    $scope.preSyncCheck = response.data;

                    if (sucessCallback) {
                        sucessCallback(response.data);
                    }

                }, function (response) {
                    loadingElement.style.display = "none";
                    HFC.DisplayAlert(response.statusText);
                });
            }

            // Update if any info has updated in server end
            $scope.UpdateState = function () {
                if ($scope.UpdateStateInProgress)
                    return;

                $scope.UpdateStateInProgress = true;

                $http.post('/api/quickbooks/' + 0 + '/getState/').then(function (response) {
                    $scope.state = response.data;

                    if (response.data.duplicateTPTCustomer != undefined && response.data.duplicateTPTCustomer != null && response.data.duplicateTPTCustomer != "")
                        $scope.duplicateTPTCustomer = JSON.parse(response.data.duplicateTPTCustomer);
                    else
                        $scope.duplicateTPTCustomer = {}
                    $scope.UpdateStateInProgress = false;
                    $('#qboState').show();
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                });

            }

            // Get QB app info
            $scope.GetSyncLog = function () {

                $http.get('../api/Quickbooks/0/SyncLogs/').then(function (response) {

                    if (response.data == null) {
                        $scope.SyncLogData = null;
                        return;
                    }

                    $scope.SyncLogData = response.data;

                }, function (response) {
                    $scope.SyncLogData = {};
                    //HFC.DisplayAlert(response.statusText);
                });
            };

            // Get Canada Sales Tax Group
            $scope.GetSalesTaxCode = function () {
                $http.get('../api/Quickbooks/0/GetTaxCode').then(function (response) {
                    $scope.SalesTaxCode = response.data;

                    if (response.data == undefined || response.data == null || response.data.length == 0)
                        $scope.QBApp.dispalyTaxCode = false;
                }, function (response) {
                    $scope.SalesTaxCode = {};
                });

                var TaxCode = $scope.QBApp.TaxCode;

                if (TaxCode != undefined && TaxCode != null && TaxCode != "") {
                    $http.get('../api/Quickbooks/0/GetTaxRatebyTaxCode?TaxCode=' + TaxCode + '').then(function (response) {
                        $scope.QBTaxRatebyTaxCode = response.data;
                    }, function (response) {
                        $scope.QBTaxRatebyTaxCode = {};
                    });
                } else {
                    $scope.QBTaxRatebyTaxCode = {};
                }


                $http.get('../api/Quickbooks/0/GetTaxName').then(function (response) {
                    $scope.MapTaxName = response.data;
                }, function (response) {
                    $scope.MapTaxName = {};
                });

            };

            $scope.EnableTaxCodeEdit = function () {
                $scope.IsSalesTaxCodeDisabled = false;
                $scope.IsSalesTaxCodeEditLinkshow = false;
            };

            $scope.UpdateTaxCode = function () {
                var TaxCode = $scope.QBApp.TaxCode;

                $http.get('../api/Quickbooks/0/GetTaxRatebyTaxCode?TaxCode=' + TaxCode + '').then(function (response) {
                    $scope.QBTaxRatebyTaxCode = response.data;
                }, function (response) {
                    $scope.QBTaxRatebyTaxCode = {};
                });

            };

            $scope.UpdateTaxDetails = function () {
                var TaxCode = $scope.QBApp.TaxCode;
                var TaxName = $scope.MapTaxName;

                $http.get('../api/Quickbooks/0/UpdateTaxCode?TaxCode=' + TaxCode + '').then(function (response) {
                    alert("TaxDetails Updated Successfully!!!");
                    $scope.IsSalesTaxCodeDisabled = true;
                    $scope.IsSalesTaxCodeEditLinkshow = true;
                }, function (response) {
                });

                $http.post('../api/Quickbooks/0/UpdateTaxName/', TaxName).then(function () {
                }, function (response) {
                });


            };

            $scope.CancelTaxDetails = function () {
                $scope.IsSalesTaxCodeDisabled = true;
                $scope.IsSalesTaxCodeEditLinkshow = true;
            };

            $scope.GetQBAccount = function () {
                $http.get('../api/Quickbooks/0/GetAccount').then(function (response) {
                    var QBCOA = response.data;
                    $scope.QBCOAAR = QBCOA.AR;
                    $scope.QBCOASales = QBCOA.Sales;
                    $scope.QBCOACOG = QBCOA.COG;
                }, function (response) {
                    $scope.QBCOAAR = {};
                    $scope.QBCOASales = {};
                    $scope.QBCOACOG = {};
                });
            };

            $scope.EnableCOAAR = function () {
                $scope.IsCOAARDisabled = false;
            };
            $scope.EnableCOASales = function () {
                $scope.IsCOASalesDisabled = false;
            };
            $scope.EnableCOACOG = function () {
                $scope.IsCOACOGDisabled = false;
            };

            if (window.location.href.toLowerCase().endsWith("/qbo")) {
                $scope.UpdateState();
                setInterval($scope.UpdateState, 2000);
            }
            if (window.location.href.toLowerCase().endsWith("/qbopresynccheck")) {
                $scope.PreSyncCheck();
            }

            if (window.location.href.toLowerCase().endsWith("/quickbooksetup")) {
                $scope.UpdateState();
                $scope.GetQBAccount();
            }

            var newWindow;
            $scope.ConnectQBO = function (url, title, w, h) {
                // Fixes dual-screen position                         Most browsers      Firefox
                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var systemZoom = width / window.screen.availWidth;
                var left = (width - w) / 2 / systemZoom + dualScreenLeft
                var top = (height - h) / 2 / systemZoom + dualScreenTop
                newWindow = window.open(url, title, 'scrollbars=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);

                // Puts focus on the newWindow
                if (window.focus) newWindow.focus();
            }

            $scope.closewindow = function () {
                if (newWindow != null && newWindow != undefined)
                    newWindow.close();
            }
            $scope.ValidateDate = function () {
                var date = $("#syncinvoicesdate").val();
                var vals = date.split('-');
                date = vals[1] + "/" + vals[2] + "/" + vals[0];
                $scope.ValidDate = HFCService.ValidateDate(date, "date");
                if ($scope.ValidDate) {
                    $("#syncinvoicesdate").addClass("k-valid");
                    $("#syncinvoicesdate").removeClass("k-invalid");
                }
                else {
                    $("#syncinvoicesdate").removeClass("k-valid");
                    $("#syncinvoicesdate").addClass("k-invalid");
                }
            }
        }
    ])

})(window, document);