﻿
// TODO: rquired refactoring:

'use strict';
appCP
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('AdminAlliancevendorController',
    [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q','FranchiseService','NavbarServiceCP',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, FranchiseService, NavbarServiceCP) {

            $scope.NavbarServiceCP = NavbarServiceCP;
            $scope.NavbarServiceCP.VendorsVendorsTab();

            $scope.VendorSubmitted = false;
            $scope.FranchiseService = FranchiseService;

            HFCService.setHeaderTitle("Alliance And Non-Alliance Vendor  #");

            $scope.Permission = {};
            var AdminVendorpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'VendorConfigurator')
            if (AdminVendorpermission){
            $scope.Permission.ViewVendor = AdminVendorpermission.CanRead;
            $scope.Permission.AddVendor = AdminVendorpermission.CanCreate;
            $scope.Permission.EditVendor = AdminVendorpermission.CanUpdate;
            }

            //add new grid row
            $scope.AddRow = function () {
               
                var grid = $("#gridAllianceVendor").data("kendoGrid");
              
                grid.addRow();
                $scope.EditMode = true;
            }

            //cancel or remove new added grid
            $scope.cancelVendorgrid = function () {
                $('#gridAllianceVendor').data("kendoGrid").cancelChanges();
                $scope.EditMode = false;

            };

            //Edit record value 
            $scope.beginEditVendors = function (obj) {
                var validator = $scope.kendoValidator("gridAllianceVendor");
                var grid = $("#gridAllianceVendor").data("kendoGrid");

                var item = $('#gridAllianceVendor').find('.k-grid-edit-row');
                if (!validator.validate()) {
                    return false;
                }
                $scope.editedVendorValue = obj;

                $scope.EditMode = true;
                if (obj) {
                    grid.refresh();
                    grid.editRow(obj);
                };
            };

            $scope.kendoValidator = function (gridId) {
                return $("#" + gridId).kendoValidator({
                    validate: function (e) {
                        $("span.k-invalid-msg").hide();
                        var dropDowns = $(".k-dropdown");
                        $.each(dropDowns, function (key, value) {
                            var input = $(value).find("input.k-invalid");
                            var span = $(this).find(".k-widget.k-dropdown.k-header");
                            
                            if (input.size() > 0) {
                                $(this).addClass("dropdown-validation-error");
                            } else {
                                $(this).removeClass("dropdown-validation-error");
                            }
                        });
                    }
                }).getKendoValidator();
            }

            //save method
            //$scope.saveVendorgrid = function () {
            //    var grid = $('#gridAllianceVendor').data().kendoGrid.dataSource.view();
            //    var validator = $scope.kendoValidator("gridAllianceVendor");

            //    if (validator.validate()) {
            //        $scope.VendorSubmitted = true;
            //        if (grid.length && $scope.editedVendorValue == undefined)
            //            var dto = grid[grid.length - 1];

            //        else if ($scope.editedVendorValue) {
            //            var dto = $scope.editedVendorValue;
            //            $scope.editedVendorValue = undefined;
            //        }
            //        var dropselection = $('#VendorDrop').val();
            //        var value = dropselection;
            //        if (value == "") {
            //            value = 0;
            //        }

            //        //$http.post('/api/AdminVendor/' + value + '/CheckValue', dto).then(function (response) {
            //        //    var value = response;
            //        //    return value;
            //        //})



            //        $http.post('/api/AdminVendor/' + value + '/SaveAdminVendor', dto).then(function (response) {
            //            var status = response.data;
            //            if (response.data !== "Success") {
            //                HFC.DisplayAlert(response.data);
            //                $scope.EditMode = false;

            //                $("#gridAllianceVendor").data("kendoGrid").dataSource.read();
            //            }
            //            else {
            //                HFC.DisplaySuccess("Vendor Saved Successfully!");
            //                $scope.EditMode = false;
            //                $("#gridAllianceVendor").data("kendoGrid").dataSource.read();
            //            }
            //        });
            //    }
            //    return;
            //}



            //code for kendo grid
            $scope.GetAllianceVendorvalue = function () {
                $scope.GridEditable = false;
                $scope.Alliancevendor = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {
                                    var dropselection = $('#VendorDrop').val();
                                    var value = dropselection;
                                    if (value == "") {
                                        value = 0;
                                    }
                                   
                                    //var url1 = '/api/PricingManagement/0/GetAdvancedPricing';
                                    var url1 = '/api/AdminVendor/' + value + '/GetAllianceVendor';
                                    return url1;
                                },
                            },
                                update:{
                                    url: '/api/AdminVendor/0/SaveAdminVendor',
                                    type: "POST",
                                    complete: function (e) {
                                        $("#gridAllianceVendor").data("kendoGrid").dataSource.read();
                                        $("#gridAllianceVendor").data("kendoGrid").refresh();
                                        HFC.DisplaySuccess("Campaign Updated Successfully.");
                                    }
                                },
                                create: {
                                    url: '/api/AdminVendor/0/SaveAdminVendor',
                                    type: "POST",
                                    complete: function (e) {
                                        $("#gridAllianceVendor").data("kendoGrid").dataSource.read();
                                        $("#gridAllianceVendor").data("kendoGrid").refresh();
                                        HFC.DisplaySuccess("New Campaign added sucessfully.");
                                    }
                                },
                                dataType: "json",
                    parameterMap: function (options, operation) {
                    if (operation === "create") {

                        return {
                            VendorIdPk:operation.VendorIdPk,
                            VendorId: options.VendorId,
                            Name: options.Name,
                            VendorTypeName: options.VendorTypeName,
                            IsActive: options.IsActive,
                            BudgetBlind: options.BudgetBlind,
                            TailoredLiving: options.TailoredLiving,
                            ConcreteCraft: options.ConcreteCraft,
                            IsAlliance: options.IsAlliance,
                            PICVendorId: options.PICVendorId,
                            USCurrency: options.USCurrency,
                            CANCurrency: options.CANCurrency,
                            Beta: options.Beta,
                            Status:options.Status
                        };
                    }
                    else if (operation === "update") {
                        
                        return {
                            VendorIdPk:options.VendorIdPk,
                            VendorId: options.VendorId,
                            Name: options.Name,
                            VendorTypeName: options.VendorTypeName,
                            IsActive: options.IsActive,
                            BudgetBlind: options.BudgetBlind,
                            TailoredLiving: options.TailoredLiving,
                            ConcreteCraft: options.ConcreteCraft,
                            IsAlliance: options.IsAlliance,
                            PICVendorId: options.PICVendorId,
                            USCurrency: options.USCurrency,
                            CANCurrency: options.CANCurrency,
                            Beta: options.Beta,
                            Status: options.Status
                           
                        };
                    }
                }


                        },
                        error: function (e) {
                            
                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {
                                id: "VendorIdPk",
                                fields: {
                                    VendorIdPk:{editable:false},
                                    VendorId: { type: "number", editable: false },
                                    Name: { editable: true, validation: { required: true } },
                                    VendorTypeName: { editable: false, 
                                        defaultValue: function (dataItem) {
                                            
                                            var dropselection = $('#VendorDrop').val();
                                            var value = dropselection;
                                            if (value == "") {
                                                value = 0;
                                            }
                                            if (value == 0) {
                                               return "Alliance Vendor";
                                            }
                                            else
                                                return "Non-Alliance Vendor";
                                        }
                                    },
                                    IsActive: { editable: true, validation: { required: true } },
                                    BudgetBlind: { type: "boolean", editable: true, },
                                    TailoredLiving: { type: "boolean", editable: true },
                                    ConcreteCraft: { type: "boolean", editable: true },
                                    IsAlliance: { type: "boolean", editable: true },
                                    PICVendorId: { type: "string", editable: true },
                                    USCurrency: { type: "boolean", editable: true },
                                    CANCurrency: { type: "boolean", editable: true },
                                    Beta: { type: "boolean", editable: true },

                                }
                            }
                        }
                    },
                    dataBound: function (e) {

                        if (this.dataSource._data.length == 0) {
                            $scope.searchBoxEnable = true;
                        } else {
                            $scope.searchBoxEnable = false;
                        }
                        var searchValue = $('#searchBox').val();
                        if (searchValue != "") {
                            $('#btnReset').prop('disabled', false);
                            $('#btnAddrow').prop('disabled', true);
                            
                        }
                        else {
                            $('#btnReset').prop('disabled', true);
                            $('#btnAddrow').prop('disabled', false);
                          
                        }
                      
                        //hide and show column based on vendor
                        var dropselection = $('#VendorDrop').val();
                        var value = dropselection;
                        if (value == "") {
                            value = 0;
                        }
                        if (value === "1") {
                            var grid = $("#gridAllianceVendor").data("kendoGrid");
                            grid.hideColumn("IsAlliance");
                            grid.hideColumn("PICVendorId");
                            grid.hideColumn("Beta");
                        }
                        else {
                            var grid = $("#gridAllianceVendor").data("kendoGrid");
                            grid.showColumn("IsAlliance");
                            grid.showColumn("PICVendorId");
                            grid.showColumn("Beta");
                        }
                        var grid = $("#gridAllianceVendor").data("kendoGrid");
                        grid.autoFitColumn(i);

                        if ($scope.Permission.EditVendor == false) {
                            e.sender.element.find(".k-grid-edit").hide();
                            e.sender.element.find(".k-grid-add").hide();
                        }
                        //--    
                    },
                    columns: [
                        {


                            field: "VendorIdPk",
                            title: "VendorIdPk",
                            width: "100px",
                            filterable: { multi: true, search: true },
                            hidden: true,


                        },
                        {


                            field: "VendorId",
                            title: "Vendor ID",
                            width: "100px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.VendorId + "</span>";
                            },

                        },
                        {
                            field: "Name",
                            title: "Vendor Name",
                            width:"250px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,
                            editor: '<input  required=true name=" Name " data-bind="value:Name" style="width: 230px;"/>',

                        },
                        {
                            field: "VendorTypeName",
                            title: "Vendor Type",
                            filterable: { multi: true, search: true },
                            width:"150px",
                        },
                        {
                            field: "StatusValue",
                            title: "Account Status With HFC",
                            width:"150px",
                            filterable: { multi: true, search: true },
                            hidden: false,
                            editor: function (container, options) {
                                $('<input required=true   name="Status" data-bind="value:Status" style="width: 130px;border: none "  />')
                                    .appendTo(container)
                                    .kendoDropDownList({
                                        autoBind: false,
                                        optionLabel: "Select",
                                       
                                        valuePrimitive: true,
                                        dataTextField: "VendorStatus",
                                        dataValueField: "Id",
                                        template: "#=VendorStatus #",
                                        //change: function (e) {
                                        //    var item = $('#gridAllianceVendor').find('.k-grid-edit-row');
                                        //    item.data().uid
                                        //    var dataItem = item.data().$scope.dataItem;
                                        //    APMDropdownChange(dataItem);
                                        //},
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    //url: '/api/PricingManagement/0/GetPricingStrategyDropdown',
                                                    url: '/api/AdminVendor/0/GetVendorDropdownStatus',
                                                }
                                            },
                                        }

                                    });
                            }


                        },


                                 {
                                     field: "BudgetBlind",
                                     title: "Budget Blinds",
                                     width: "120px",
                                     hidden: false,
                                     template: '<input type="checkbox" #= BudgetBlind ? "checked=checked" : "" # class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched" disabled="true"></input>',
                                     //template: '<input type="checkbox" #= (BudgetBlind == true) ? checked ="checked" : "" # />',
                                     filterable: { messages: { isFalse: "False", isTrue: "True" } },
                                 },
                                {
                                    field: "TailoredLiving",
                                    title: "Tailored Living",
                                    width: "120px",
                                    hidden: false,
                                    template: '<input type="checkbox" #= TailoredLiving ? "checked=checked" : "" # class="option-input checkbox" disabled="true"></input>',
                                    filterable: { messages: { isFalse: "False", isTrue: "True" } },
                                },
                                {
                                    field: "ConcreteCraft",
                                    title: "Concrete Craft",
                                    width: "120px",
                                    hidden: false,
                                    template: '<input type="checkbox" #= ConcreteCraft ? "checked=checked" : "" #  class="option-input checkbox" disabled="true"></input>',
                                    filterable: { messages: { isFalse: "False", isTrue: "True" } },
                                },
                                {
                                    field: "IsAlliance",
                                    title: "PIC Enabled",
                                    width: "120px",
                                    hidden: false,
                                    editor: '<input name="IsAlliance" type="checkbox" #= IsAlliance ? "checked=checked" : "" #  class="option-input checkbox" ng-click="customClick(dataItem)"></input>',
                                    template: '<input type="checkbox" #= IsAlliance ? "checked=checked" : "" #  class="option-input checkbox" disabled="true" onClick="customClick()"></input>', 
                                    filterable: { messages: { isFalse: "False", isTrue: "True" } },
                                },
                                 {
                                     field: "PICVendorId",
                                       title: "PIC Id",
                                       width: "120px",
                                       filterable: {
                                           //multi: true,
                                           search: true
                                       },
                                       editor: '<input name="PICVendorId" data-bind="value:PICVendorId" style="width: 100px;border: none " ng-blur="customClick(dataItem,true)"/>'

                                  },
                                 {
                                     field: "USCurrency",
                                     title: "US Price Grid",
                                     width: "120px",
                                     hidden: false,
                                     template: '<input type="checkbox" #= USCurrency ? "checked=checked" : "" #  class="option-input checkbox" disabled="true"></input>',
                                     filterable: { messages: { isFalse: "False", isTrue: "True" } },
                                 },
                                  {
                                      field: "CANCurrency",
                                      title: "CAN Price Grid",
                                      width: "120px",
                                      hidden: false,
                                      template: '<input type="checkbox" #= CANCurrency ? "checked=checked" : "" #  class="option-input checkbox" disabled="true"></input>',
                                      filterable: { messages: { isFalse: "False", isTrue: "True" } },
                                  },

                                  
                                   {
                                       field: "Beta",
                                       title: "Beta",
                                       width: "100px",
                                       hidden: false,
                                       template: '<input type="checkbox" #= Beta ? "checked=checked" : "" #  class="option-input checkbox" disabled="true"></input>',
                                       filterable: { messages: { isFalse: "False", isTrue: "True" } },
                                   },
                                    { command: ["edit"], title: "&nbsp;", width: "100px" }
                                    //{
                                    //    field: "",
                                    //    title: "",
                                    //    hidden: false,
                                    //    editable: false,
                                    //    width: "100px",
                                    //    template: '<ul ><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="beginEditVendors(dataItem)">Edit</a></li></ul> </li> </ul>'
                                    //},

                    ],

                    editable: {
                        mode: "inline",
                        //createAt: "bottom"
                    },

                    pageable: {
                        refresh: true,
                        pageSize: 25,
                        pageSizes: [25, 50, 100],
                    },

                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                isempty: "Empty",
                                isnotempty: "Not empty"


                            },
                            number: {
                                eq: "Equal to",
                                neq: "Not equal to",
                                gte: "Greater than or equal to",
                                lte: "Less than or equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    resizable: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    toolbar: [
                       {
                           template: kendo.template($("#headToolbarTemplate").html()),
                       }]
                    //{ name: "create", text: "Add New Vendor" }
                    //toolbar: kendo.template($('<script id="template" type="text/x-kendo-template"><div class="vendor_search"><input ng-keyup="VendorgridSearch()" type="search" id="searchBox" placeholder="Search Vendor" class="k-textbox leadsearch_tbox"><input type="button" id="btnReset" ng-click="VendorgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"><div class="productsearch_checkbox"></div></div></div></script>').html()),

                };


            };



            $scope.GetAllianceVendorvalue();

            //dropdown value selection code (fetch allan/non-allan vendor)
            $scope.ChangeVendor = function () {
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
                var dropselection = $('#VendorDrop').val();
                var value = dropselection;
                if (value == "") {
                    value = 0;
                }
                
                $http.get('/api/AdminVendor/' + value + '/AdminVendorOption/').then(function (response) {
                    var data = response.data;
                    var grid = $("#gridAllianceVendor").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    if (dataSource._data.length == 0) {
                        grid.dataSource.data(data);
                    }
                    else if (dataSource._data.length > 25 || dataSource._data.length < 25) {
                        grid.dataSource.pageSize(25);
                    }
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                }).catch(function(e)
                {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                });
            }

            

            //click on pic enable checkbox and validating pic-id field
            $scope.customClick = function (dataitem, flag) {
                var setvalue; var picVal;
                //var setvalue = $('input[name="IsAlliance"]').prop('checked');//not calling the save function properly
                if (flag) {
                    if (dataitem['IsAlliance'])
                        setvalue = "on";
                }
                else {
                    setvalue = $('input[name="IsAlliance"]:checked').val();
                }
                
                if (setvalue == undefined) {
                    dataitem.IsAlliance = "off";
                }
                else {
                    dataitem.IsAlliance = setvalue;
                }
                if (flag) {
                    picVal = dataitem.PICVendorId;
                    if (picVal == null) {
                        picVal = "";
                    }
                }
                else {
                    picVal = $('input[name="PICVendorId"]').val();
                }
                 
                if (dataitem.IsAlliance == "on" && picVal=="") {
                    $('input[name="PICVendorId"]').prop('required', true);
                }
                else {
                    $('input[name="PICVendorId"]').prop('required', false);
                    
                }

                if (flag) {
                    if (dataitem.IsAlliance == "on") {
                        dataitem.IsAlliance = true;
                    }
                    else dataitem.IsAlliance = false;
                }

            }

            //search value in grid(search-textbox)
            $scope.AlliancegridSearch = function () {
                var validator = $scope.kendoValidator("gridAllianceVendor");
                if (validator.validate()) {
                    var searchValue = $('#searchBox').val();

                    $("#gridAllianceVendor").data("kendoGrid").dataSource.filter({
                        logic: "or",
                        filters: [
                          {
                              field: "VendorId",
                              operator: "eq",
                              value: searchValue
                          },
                          {
                              field: "Name",
                              operator: "contains",
                              value: searchValue
                          },
                          {
                              field: "StatusValue",
                              operator: "contains",
                              value: searchValue
                          },
                          {
                              field: "PICVendorId",
                              operator: "eq",
                              value: searchValue
                          }

                        ]
                    });
                }
                else {
                    $('#searchBox').val('');
                }

            }
            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 315);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 315);
                };
            });
            // End Kendo Resizing
            //search clear button
            $scope.AlliancegridSearchClear = function () {
                $('#searchBox').val('');
                var searchValue= $('#searchBox').val('');
                if (searchValue == '') {
                    $('#btnReset').prop('disabled', true);
                    $('#btnAddrow').prop('disabled', false);
                }
                else {
                    $('#btnReset').prop('disabled', false);
                    $('#btnAddrow').prop('disabled', true);
                }
                $("#gridAllianceVendor").data('kendoGrid').dataSource.filter({
                });
            }

        }

    ])
