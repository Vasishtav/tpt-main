﻿/***************************************************************************\
Module Name:  Category Controller .js - AngularJS file
Project: HFC
Created on: 04 FEB 2020
Created By: Vasishta
Copyright:
Description: Core Product Controller
Change History:
Date  By  Description

\***************************************************************************/

'use strict';
appCP.controller('CoreProductController',
    [
        '$scope', '$window', 'HFCService', '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', '$http', '$q', 'FranchiseService', 'NavbarServiceCP','kendotooltipService',
        function ($scope, $window, HFCService, $routeParams, $location, $rootScope
        , $templateCache, $sce, $compile, $http, $q, FranchiseService, NavbarServiceCP, kendotooltipService) {

            $scope.NavbarServiceCP = NavbarServiceCP;
            $scope.NavbarServiceCP.productsCategoryManagementTab();

            $scope.isCatActive = false;
            $scope.allowEdit = false;
            $scope.isSave = true;
            $scope.Permission = {};
            $scope.VendorListName = {};
            //flag for date validation
            $scope.ValidDate = true;

            var offsetvalue = 0;
            var ProductModelpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'ProductModel');

        if (ProductModelpermission) {
                $scope.Permission.ViewProductModel = ProductModelpermission.CanRead;
                $scope.Permission.AddProductModel = ProductModelpermission.CanCreate;
                $scope.Permission.EditProductModel = ProductModelpermission.CanUpdate;
            }
            $scope.FranchiseService = FranchiseService;
            HFCService.setHeaderTitle("Core Product Management #");
            $scope.VendorID = null;
            $scope.BrandId = 1;
            $scope.UpdateFilter = 0;
            $scope.Active = null;
            $scope.CAActive = null;
            $scope.USActive = null;
            $scope.Inactive = null;
            $scope.updatedexactdate = null;


            $("#datepicker").kendoDatePicker({

                change: function (data) {
                    $scope.updatedexactdate = this.value();
                    //for date validation
                    $scope.ValidateDate("datepicker");                     
                    $scope.filterGrid();
                }
            });

            //to get active vendor to drop down
            $http.get('/api/Procurement/0/AdminVendorNameList').then(function (data) {
                $scope.VendorListName = data.data;
            });

            $scope.filterGrid = function () {

                var data = $scope.modelsList;
                var neeData = [];


                //Updated Filter
                if ($scope.UpdateFilter !== null && $scope.UpdateFilter !== 0) {
                    var d = new Date();

                    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

                    var ourDate = new Date(utc - (3600000 * 8));

                    //var ourDate = new Date();
                    var pastDate = null;
                    if ($scope.UpdateFilter === 1) {
                        pastDate = ourDate.getDate();
                        ourDate.setDate(pastDate);
                    }
                    else  if ($scope.UpdateFilter === 2) {
                        pastDate = ourDate.getDate() - 1;
                        ourDate.setDate(pastDate);
                    }
                    else  if ($scope.UpdateFilter === 3) {
                        pastDate = ourDate.getDate() - 7;
                        ourDate.setDate(pastDate);
                    }
                    else
                        if ($scope.UpdateFilter === 4) {
                        pastDate = ourDate.getDate() - 30;
                        ourDate.setDate(pastDate);
                    }
                    else if ($scope.UpdateFilter === 5) {
                        pastDate = ourDate.getDate() - 90;
                        ourDate.setDate(pastDate);
                    }
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].LastUpdatedOn >ourDate) {
                            neeData.push(data[i]);
                        }
                    }
                    data = neeData;
                }
                //Date picker filter
                if ($scope.updatedexactdate !== null && $scope.updatedexactdate !== '') {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].LastUpdatedOn.setHours(0, 0, 0, 0).toDateString() === $scope.updatedexactdate.setHours(0, 0, 0, 0).toDateString()) {
                            neeData.push(data[i]);
                        }
                    }
                    data = neeData;
                }

                neeData = [];
                //Vendor Filter
                if ($scope.VendorID !== null && $scope.VendorID !== '') {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].VendorId === $scope.VendorID) {
                            neeData.push(data[i]);
                        }
                    }
                    data = neeData;
                }

                neeData = [];
                //Filter based on Brand
                if ($scope.BrandId) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].BrandId === $scope.BrandId) {
                            neeData.push(data[i]);
                        }
                    }
                    data = neeData;
                }

                neeData = [];
                //Filter Based on Active
                if ($scope.Active !== null && $scope.Active !== '') {
                    if ($scope.Active) {
                        for (var i = 0; i < data.length; i++) {
                            if ($scope.Active && data[i].Active === true) {
                                neeData.push(data[i]);
                            }
                        }
                        data = neeData;
                    }

                }
                neeData = [];
                //Filter Based on CAActive
                if ($scope.CAActive !== null && $scope.CAActive !== '') {
                    if ($scope.CAActive) {
                        for (var i = 0; i < data.length; i++) {
                            if ($scope.CAActive && data[i].CAActive === $scope.CAActive) {
                                neeData.push(data[i]);
                            }
                        }
                        data = neeData;
                    }

                }
                neeData = [];
                //Filter Based on US Active
                if ($scope.USActive !== null && $scope.USActive !== '') {
                    if ($scope.USActive) {
                        for (var i = 0; i < data.length; i++) {
                            if ($scope.USActive && data[i].USActive === $scope.USActive) {
                                neeData.push(data[i]);
                            }
                        }
                        data = neeData;
                    }

                }
                neeData = [];
                //Filter Based on Inactive
                if ($scope.Inactive !== null && $scope.Inactive !== '') {
                    if ($scope.Inactive) {
                        for (var i = 0; i < data.length; i++) {
                            if ($scope.Inactive && data[i].Active === false) {
                                neeData.push(data[i]);
                            }
                        }
                        data = neeData;
                    }
                }
                $("#gridModelProduct").data("kendoGrid").dataSource.data(data);
                $("#gridModelProduct").data("kendoGrid").refresh();

            };

            $scope.mainGridOptions = {
                dataSource: {
                    transport: {
                        read: function (e) {

                            $http.get("/api/Product/0/GetCoreProductModels").then(function (data) {
                                $scope.modelsList = data.data;
                                e.success(data.data);

                            });
                        },
                        update: function (e) {
                            $http.post('/api/Product/0/UpdateCoreProductModel', e.data).then(function (response) {
                                if (response.data.error == "") {
                                    $scope.modelsList = response.data.modelData;
                                    $scope.filterGrid();
                                }

                            }).catch(function (error) {
                                //var loadingElement = document.getElementById("loading");
                                //loadingElement.style.display = "none";

                                //HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                            });;
                        },
                        parameterMap: function (options, operation) {
                            if (operation === "update") {

                                return {
                                    ModelKey: options.ModelKey,
                                    Model: options.Model,
                                    ProductId: options.ProductId,
                                    PICProductId: options.PICProductId,
                                    VendorId: options.VendorId,
                                    Active: options.Active,
                                    USActive: options.USActive,
                                    CAActive: options.CAActive,
                                    WarningMessage: options.WarningMessage,
                                    Description: options.Description,
                                    IsManualFLag: options.IsManualFLag,
                                    DiscontinuedDate: options.DiscontinuedDate,
                                    PhasedoutDate: options.PhasedoutDate,
                                    CreatedOn: options.CreatedOn,
                                    CreatedBy: options.CreatedBy,
                                    LastUpdatedOn: options.LastUpdatedOn,
                                    LastUpdatedBy: options.LastUpdatedBy,
                                    Vendor: options.Vendor,
                                    ProductName: options.ProductName,
                                    ProductGroup: options.ProductGroup,
                                    ProductGroupDesc: options.ProductGroupDesc,
                                    Prompts: options.Prompts,


                                };
                            }
                        },
                        //update: {
                        //    url: "/api/Product/0/UpdateCoreProductModel",
                        //    type: "POST",
                        //    complete: function (e) {
                        //        $("#gridModelProduct").data("kendoGrid").dataSource.read();
                        //        $("#gridModelProduct").data("kendoGrid").refresh();
                        //        HFC.DisplaySuccess("Core Product Model updated.");
                        //    }
                        //},
                    },
                    schema: {
                        model: {
                            id: "ModelKey",
                            fields: {
                                Model: { type: "string", editable: false },
                                ProductId: { type: "number", editable: false },
                                PICProductId: { type: "number", editable: false },
                                VendorId: { type: "number", editable: false },
                                Active: { type: "boolean", editable: true },
                                USActive: { type: "boolean", editable: true },
                                CAActive: { type: "boolean", editable: true },
                                WarningMessage: { type: "string", editable: false },
                                Description: { type: "string", editable: false },
                                IsManualFLag: { type: "boolean", editable: false },
                                DiscontinuedDate: { type: "date", editable: false },
                                PhasedoutDate: { type: "date", editable: false },
                                CreatedOn: { type: "date", editable: false },
                                CreatedBy: { type: "number", editable: false },
                                LastUpdatedOn: { type: "date", editable: false },
                                LastUpdatedBy: { type: "number", editable: false },
                                Vendor: { type: "string", editable: false },
                                ProductName: { type: "string", editable: false },
                                ProductGroup: { type: "string", editable: false },
                                ProductGroupDesc: { type: "string", editable: false },
                                Prompts: { type: "string", editable: false },
                                AuditTrail: { type: "string", editable: false }
                            }
                        },
                    }
                },

                editable: {
                    mode: "inline"
                },
                dataBound: function (e) {
                    var grid = $("#gridModelProduct").data("kendoGrid");
                    if ($scope.Permission && !$scope.Permission.EditProductModel) {
                        grid.hideColumn(14);
                    }
                },

                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                },


                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                resizable: true,
                sortable:true,
                noRecords: { template: "No records found" },
                scrollable: true,
                toolbar: [
                   {
                       template: kendo.template($("#headToolbarTemplate").html()),
                   }],

                columns: [{
                    field: "Vendor",
                    title: "Vendor",
                    width: "150px",
                }, {
                    field: "ProductGroup",
                    title: "Group ID",
                    width: "100px",
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + dataItem.ProductGroup + "</span>";
                    },

                }, {
                    field: "ProductGroupDesc",
                    width: "150px",
                    title: "Group Description",
                }, {
                    field: "PICProductId",
                    title: "Product ID",
                    width: "100px",
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + dataItem.PICProductId + "</span>";
                    },

                }, {
                    field: "ProductName",
                    title: "Product Name",
                    width: "150px",
                    }
                    , {
                        field: "Model",
                        title: "Model Id",
                        width: "100px",
                    }
                , {
                    field: "Description",
                    title: "Model",
                    width: "200px",
                }
                , {
                    field: "Prompts",
                    width: "150px",
                    title: "Width Height Mount Color Fabric",
                }
                , {
                    field: "PhasedoutDate",
                    title: "Phase Out Date",
                    width: "100px",
                    template: function (dataItem) {
                        if (dataItem.PhasedoutDate) {
                            return "<span class='tooltip-wrapper'><span>" + HFCService.KendoDateFormat(dataItem.PhasedoutDate) + "</span></span>";
                        } else {
                            return "";
                        }
                    },
                    filterable: HFCService.gridfilterableDate("date", "CoreProductPhaseOutDateFilterCalendar", offsetvalue),
                }
                , {
                    field: "DiscontinuedDate",
                    title: "Discontinued Date",
                    width: "100px",
                    template: function (dataItem) {
                        if (dataItem.DiscontinuedDate) {
                            return "<span class='tooltip-wrapper'><span>" + HFCService.KendoDateFormat(dataItem.DiscontinuedDate) + "</span></span>";
                        } else {
                            return "";
                        }
                    },
                    filterable: HFCService.gridfilterableDate("date", "CoreProductDiscontinuedDateFilterCalendar", offsetvalue),
                }
                , {
                    field: "USActive",
                    title: "US",
                    width: "80px",
                    template: '<input type="checkbox" #= USActive ? "checked=checked" : "" # disabled="true" class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched"></input>',
                }
                , {
                    field: "CAActive",
                    title: "CA",
                    width: "80px",
                    template: '<input type="checkbox" #= CAActive ? "checked=checked" : "" # disabled="true" class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched"></input>',
                }
                , {
                    field: "Active",
                    title: "Active",
                    width: "80px",
                    template: '<input type="checkbox" #= Active ? "checked=checked" : "" # disabled="true" class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched"></input>',
                }
                , {
                    field: "LastUpdatedOn",
                    width: "100px",
                    title: "Last Updated",
                    template: function (dataItem) {
                        if (dataItem.LastUpdatedOn) {
                            return "<span class='tooltip-wrapper'><span>" + HFCService.KendoDateFormat(dataItem.LastUpdatedOn) + "</span></span>";
                        } else {
                            return "";
                        }
                    },
                    filterable: HFCService.gridfilterableDate("date", "CoreProductLastUpdatedFilterCalendar", offsetvalue),
                },
                  {
                      field: "AuditTrail",
                      width: "350px",
                      title: "Audit", encoded: true
                 }
                ,
                {
                    command: [{
                        name: "edit", visible: function (dataItem) {

                            return !(dataItem.DiscontinuedDate != null && dataItem.DiscontinuedDate != undefined);
                        }

                }], title: "&nbsp;", width: "100px",
            }
                ],
                //editable: "inline"
                columnResize: function (e) {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 465);
                    getUpdatedColumnList(e);
                }
            };

            var getUpdatedColumnList = function (e) {
                kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
            }

            $scope.gridSearchClear = function () {
                $('#searchBox').val('');
                var searchValue = $('#searchBox').val('');
                if (searchValue == '') {
                    $('#btnReset').prop('disabled', true);
                    $('#btnAddrow').prop('disabled', false);
                }
                else {
                    $('#btnReset').prop('disabled', false);
                    $('#btnAddrow').prop('disabled', true);
                }
                $("#gridModelProduct").data('kendoGrid').dataSource.filter({
                });
            };


            $scope.gridSearch = function () {
                var searchValue = $('#searchBox').val();
                $("#gridModelProduct").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "Vendor",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "ProductName",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "ProductGroupDesc",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Description",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Prompts",
                          operator: "contains",
                          value: searchValue
                      }
                      ,
                      {
                          field: "ProductGroup",
                          operator: "contains",
                          value: searchValue
                      },
                       {
                          field: "Model",
                          operator: "contains",
                          value: searchValue
                      }

                    ]
                });
            }

            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 465);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 465);
                };
            });
            // End Kendo Resizing

            //for date validation
            $scope.ValidateDate = function (id) {
                setTimeout(function () {  
                    var date = $("#datepicker").val();
                    if (date == "")
                        $scope.ValidDate = true;
                    else
                        $scope.ValidDate = HFCService.ValidateDate(date, "date");

                    if ($scope.ValidDate) {
                        $("#" + id + "").removeClass("invalid_Date");
                        var a = $('#' + id + '').siblings();
                        $(a[0]).removeClass("icon_Background");
                        $("#" + id + "").addClass("frm_controllead1");
                    }
                    else {
                        $("#" + id + "").addClass("invalid_Date");
                        var a = $('#' + id + '').siblings();
                        $(a[0]).addClass("icon_Background");
                        $("#" + id + "").removeClass("frm_controllead1");
                    }
                }, 100);
            }
        }]
);