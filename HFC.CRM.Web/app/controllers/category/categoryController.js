﻿/***************************************************************************\
Module Name:  Category Controller .js - AngularJS file
Project: HFC
Created on: 31 May 2018 Thursday
Created By: 
Copyright:
Description: Category Controller
Change History:
Date  By  Description

\***************************************************************************/

appCP.controller('categoryController', ['$http', '$scope', '$route', '$window', '$rootScope', 'HFCService', 'FranchiseService', 'NavbarServiceCP'
    , function ($http, $scope, $route, $window, $rootScope, HFCService, FranchiseService, NavbarServiceCP) {

        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.NavbarServiceCP.productsCategoryManagementTab();

        $scope.isCatActive = false;
        $scope.allowEdit = false;
        $scope.isSave = true;
        $scope.Permission = {};
        var ProductServicepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Products/Services')
        if (ProductServicepermission) {
            $scope.Permission.ViewProductService = ProductServicepermission.CanRead;
            $scope.Permission.AddProductService = ProductServicepermission.CanCreate;
            $scope.Permission.EditProductService = ProductServicepermission.CanUpdate;
        }
        $scope.FranchiseService = FranchiseService;

        HFCService.setHeaderTitle("Product Category Management #");

        var serviceInnerArr = [];
        var myproductArr = [];
        var resultJson = [];
        var productCategories = [];

        $scope.brandList = [{ id: 1, name: "Budget Blind(BB)" }, { id: 3, name: "Concrete Craft(CC)" }, { id: 2, name: "Tailored Living(TL)" }];

        $scope.editCategory = function () {
            if ($scope.allowEdit) {
                $scope.allowEdit = false;
                $route.reload();
            } else {
                $scope.allowEdit = true;
            }
        }

        // Set default brand as 1 and load the tree on load
        $scope.BrandId = 1;
        $scope.showInActiveCategory = false;
        renderTreeData($scope.BrandId);

        // Brand Dropdown Changed event
        $scope.BrandSelect = function (item) {
            $scope.BrandId = item.id;
            renderTreeData(item.id);
        }

        // Toggle show or hide inactive items
        $scope.IsSuspendSelect = function () {
            if ($scope.showInActiveCategory) {
                $scope.showInActiveCategory = false;
            } else {
                $scope.showInActiveCategory = true;
            }
            renderTreeData($scope.BrandId);
        };

        $scope.addNew = function (item) {
            var test = productCategories;
            var validate = $scope.validateProductCategories(test);
            if (!validate) {
                return;
            }
            var treeview = $("#categoryTree").data("kendoTreeView");
            var treeElement = treeview.findByUid(item.uid);
            $scope.onDataBound = function (e) {
                setTimeout(function () {
                    treeview.expand("item");
                }, 100);
            }()

            var temp_id = s4() + s4();

            if (item.isParent == true) {
                var parentID = 0;
                var newItem = makeParentItem(temp_id);
                var ProductCategory = newItem.text;
                if (item.typeId)
                    $scope.Type = item.typeId;
                else
                    $scope.Type = item.ProductTypeId;
            } else {
                if (item.id)
                    var parentID = item.id;
                else
                    var parentID = item.temp_id;
                var newItem = makeItem(temp_id);
                var ProductCategory = newItem.text;
                if ($scope.Type)
                    item.typeId = $scope.Type;
                else
                    item.typeId = item.typeId;
            }
            var temp_prodCatId = generateID(productCategories);
            newItem.id = temp_prodCatId;
            // Add new category to object array
            var newCat = {
                "ProductCategoryId": temp_prodCatId,
                "ProductCategory": ProductCategory,
                "Parant": parentID,
                "Parant_temp": parentID,
                "IsActive": true,
                "IsDeleted": false,
                "PICGroupId": null,//added newly
                "PICProductId": null,//added newly
                "BrandId": $scope.BrandId,
                "SalesTaxCode": null,
                "ProductTypeId": item.typeId,
                "temp_id": temp_id
            };
            productCategories.push(newCat);
            var newNode = treeview.append(newItem, treeElement);
            $(newNode).addClass("k-state-selected"); // Added to highlight newly added element
            // for scroll - code below
            treeview.select(treeview.findByText(item.text));
            // scroll to selected item
            var itemScrollTop = $(treeview.select()[0]).offset();
            itemScrollTop.top = itemScrollTop.top - 200;
            $("#categoryTree").animate({ scrollTop: itemScrollTop.top }, 2000);
        };

        $scope.updateNode = function (item) {

            var tree = this.tree;
            var text = item.editableData;
            var dataItem = tree.dataItem(tree.select());
            if (item.uid == dataItem.uid) {
                dataItem.text = undefined; // force refresh of dataItem
                dataItem.set("text", text);
                item.editorEnabled = false;
            }
            else {
                var dataItem = (item);
                dataItem.text = undefined; // force refresh of dataItem
                dataItem.set("text", text);
                item.editorEnabled = false;
            }

            angular.forEach(productCategories, function (value) {
                if (value.ProductCategoryId != 0 && value.ProductCategoryId == item.id) {
                    value.ProductCategory = text;
                } else if (value.ProductCategoryId == 0 && value.temp_id == item.temp_id) {
                    value.ProductCategory = text;
                }
            });
        };

        $scope.updateTaxCode = function (item) {
            angular.forEach(productCategories, function (value) {

                //rechanged the sales tax to groupid and added newly picproductid
                if (value.ProductCategoryId != 0 && value.ProductCategoryId == item.id) {
                    value.PICGroupId = item.PICGroupId;
                    value.PICProductId = item.PICProductId;
                } else if (value.ProductCategoryId == 0 && value.temp_id == item.temp_id) {
                    value.PICProductId = item.PICProductId;
                    value.PICGroupId = item.PICGroupId;
                }
            });
        };

        $scope.editNode = function (dataItem, focusId) {
            dataItem.editorEnabled = true;
            dataItem.editableData = dataItem.text;
        };

        $scope.disableEditor = function () {
            $scope.editorEnabled = false;
        };

        $scope.onInactive = function (item) {
            angular.forEach(productCategories, function (value) {
                if (value.ProductCategoryId != 0 && value.ProductCategoryId == item.id) {
                    value.IsActive = false;
                } else if (value.ProductCategoryId == 0 && value.temp_id == item.temp_id) {
                    value.IsActive = false;
                }
            });

            item.isCatActive = false;

            var deleteUser = $window.confirm('Are you sure you want to InActivate?');
            if (deleteUser) {
                // $scope.treeData.remove(item);
                item.IsActive = false;
                item.isCatActive = false;
            }
            else if (!deleteUser) {
                item.IsActive = true;
                item.isCatActive = true;
            }

            //  $scope.treeData.remove(item);  // commnted to prevent delete 
        };

        $scope.changeToActive = function (item) {
            angular.forEach(productCategories, function (value) {
                if (value.ProductCategoryId != 0 && value.ProductCategoryId == item.id) {
                    value.IsActive = true;
                } else if (value.ProductCategoryId == 0 && value.temp_id == item.temp_id) {
                    value.IsActive = true;
                }
            });
            item.isCatActive = true;
            item.IsActive = true;

            var activateUser = $window.confirm('Are you sure you want to Activate?');
            if (activateUser) {
                // $scope.treeData.remove(item);
                item.IsActive = true;
                item.isCatActive = true;
            }
            else if (!activateUser) {
                item.IsActive = false;
                item.isCatActive = false;
            }
        }

        $scope.removevalidateclass = function (item) {
            var treeview = $("#categoryTree").data("kendoTreeView");
            var selected = treeview.findByUid(item.uid);
            if (item.PICGroupId != null) {
                if (!$.isNumeric(item.PICGroupId) || item.PICGroupId != Math.floor(item.PICGroupId)) {
                    item.PICGroupId = null;
                    return;
                } else {
                    item.PICGroupId = parseInt(item.PICGroupId);
                    selected.find('li #PICGroupId').removeClass("required-error");
                }
            }

            if (item.PICProductId != null) {
                if (!$.isNumeric(item.PICProductId) || item.PICProductId != Math.floor(item.PICProductId)) {
                    item.PICProductId = null;
                    return;
                } else {
                    item.PICProductId = parseInt(item.PICProductId);
                    selected.find('li #PICProductId').removeClass("required-error");
                }
            }

            if (item.ProductCategory != null) {
                selected.find('li.width120').removeClass("required-error");
            }
            $scope.isSave = true;
        }

        $scope.validateProductCategories = function (test) {
            var returnval = true;
            for (var i = 0; i < productCategories.length; i++) {
                var value = productCategories[i];
                if (value) {
                    if ((value.PICGroupId == null || value.PICProductId == null || value.PICGroupId == "" || value.PICProductId == "" || value.PICGroupId == 0 || value.PICProductId == 0 || value.ProductCategory == "" || value.ProductCategory == null) && value.ProductTypeId != 1) {
                        var treeview = $("#categoryTree").data("kendoTreeView");
                        treeview.expand(".k-item");
                        var getitem = treeview.dataSource.get(value.ProductCategoryId);
                        var selected = treeview.findByUid(getitem.uid)
                        if (value.PICGroupId == null || value.PICGroupId == "" || value.PICGroupId == 0) {
                            selected.find('li #PICGroupId').addClass("required-error");
                        }  if (value.PICProductId == null || value.PICProductId == "" || value.PICProductId == 0) {
                            selected.find('li #PICProductId').addClass("required-error");
                        }  if (value.ProductCategory == "" || value.ProductCategory == null) {
                            selected.find('li.width120').addClass("required-error");
                        }
                        
                        //treeview.select(selected);
                        //treeview.select().find("span.k-state-selected").addClass("required-error");
                        //selected.addClass("required-error");
                        var itemScrollTop = $(selected[0]).offset();
                        itemScrollTop.top = itemScrollTop.top - 200;
                        $('html, body').animate({ scrollTop: itemScrollTop.top }, 2000);
                        returnval = false;
                        $scope.isSave = false;
                        return returnval;
                    }
                }
            }
            return returnval;
        }

        $scope.removetempProId = function () {
            for (var i = 0; i < productCategories.length; i++) {
                if (productCategories[i].temp_id != null) {
                    productCategories[i].ProductCategoryId = 0;
                }
            }
        }

        $scope.saveAll = function () {
            var test = productCategories;
            var validate = $scope.validateProductCategories(test);
            if (!validate) {
                return;
            }
            $scope.removetempProId();

            $http.post('/api/ProductCategoryTP', productCategories).then(function (response) {

                $scope.allowEdit = false;
                $route.reload();
                HFC.DisplaySuccess("Data Saved !!")
            }, function (response) {
                HFC.DisplayAlert("Error while saving data !!");
                //   console.log("response: ", response);
            });
        };

        //$scope.collapseAll = function (item) {
        //    $scope.treeData.collapse(".k-item");
        //}; commented by Ram(not been used)

        $scope.expandAll = function () {
            $("#treeview").data("kendoTreeView").expand(".k-item");
            //$scope.treeData.expand(".k-item");
        };

        function renderTreeData(brandId) {
            serviceInnerArr = [];
            myproductArr = [];
            resultJson = [];
            productCategories = [];
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            //$.ajax({
            //    url: '/api/ProductCategoryTP/0?Inactive=' + $scope.showInActiveCategory + '&brandId=' + brandId,
            //    async: false,
            //    success: function (responseData) {
            //        resultJson = responseData;
            //        productCategories = responseData;
            //    }
            //});
            $http.get('/api/ProductCategoryTP/0?Inactive=' + $scope.showInActiveCategory + '&brandId=' + brandId).then(function (response) {

                var data = response.data;
                resultJson = response.data;
                productCategories = response.data;

                angular.forEach(resultJson, function (value) {
                    if (value.ProductTypeId == 2 && value.Parant == 0) {
                        myproductArr.push(value);
                    } else if (value.ProductTypeId == 3 && value.Parant == 0) {
                        serviceInnerArr.push(value);
                    }
                });

                generateCategoryObject(serviceInnerArr);
                generateCategoryObject(myproductArr);

                var finalProductArray = [];
                var myProductInfo = { text: "Products", text1: "PICGroupId", text2: "PICProductId", allowDelete: false, allowAdd: true, allowEdit: false, isCatActive: true, isCategory: false, isSubCat: false, isService: false, isParent: true, allowTextbox: false, collapsed: false, expanded: true, items: myproductArr, typeId: 2 };
                var serviceInfo = { text: "Service", allowDelete: false, allowAdd: true, allowEdit: false, isCatActive: true, isCategory: false, isSubCat: false, isService: false, isParent: true, allowTextbox: false, collapsed: false, expanded: true, items: serviceInnerArr, typeId: 3 };

                finalProductArray.push(myProductInfo);
                finalProductArray.push(serviceInfo);
                $scope.treeData = new kendo.data.HierarchicalDataSource({
                    data: finalProductArray,
                    dataSource: finalProductArray,  // added dataSource - 1 sep
                    requestEnd: requestEndHandler
                });
                loadingElement.style.display = "none";
            }).catch(function (e) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });


        }

        function generateCategoryObject(rawCatArr) {
            var treeview = $("#categoryTree").data("kendoTreeView");
            angular.forEach(rawCatArr, function (category, index) {
                var checkArr = [];
                angular.forEach(resultJson, function (value) {
                    if (category.ProductCategoryId == value.Parant) {
                        var items = newCategoryObject(value, "subcategory", null);
                        angular.extend(items, value);
                        checkArr.push(items);
                    }

                });
                angular.extend(category, newCategoryObject(category, "category", checkArr));
            });
        }

        function newCategoryObject(category, type, childObj) {
            var data = {
                id: category.ProductCategoryId,
                text: category.ProductCategory,
                PICGroupId: category.PICGroupId,//rechanged salestax to pic groupid
                typeId: category.ProductTypeId,
                PICProductId: category.PICProductId, //added newly
                allowDelete: true,
                allowEdit: true,
                isCatActive: category.IsActive,
                isCategory: true,
                isService: false,
                isSubCat: false,
                isParent: false,
            };

            if (type == "category") {
                data.items = childObj;
                data.allowAdd = true;
                data.allowTextbox = true;
            } else {
                data.allowAdd = false;
                data.allowTextbox = true;//added to make txt box available to sub category.
            }

            return data;
        }

        function makeParentItem(temp_id) {
            var txt = "New Parent Category";
            return { text: txt, allowDelete: true, allowAdd: true, allowEdit: true, IsActive: true, isCatActive: true, allowTextbox: true, collapsed: false, expanded: true, temp_id: temp_id };
        }

        function makeItem(temp_id) {
            var txt = "New Category";
            return { text: txt, allowDelete: true, allowAdd: false, allowEdit: true, IsActive: true, isCatActive: true, allowTextbox: true, collapsed: false, expanded: true, temp_id: temp_id };
        };

        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
        }

        //to expand the tree based on condition(show inactive)
        function requestEndHandler() {

            var treeview = $("#categoryTree").data("kendoTreeView");
            // default expand the item with text Name
            if ($scope.showInActiveCategory == false) {
                treeview.expand(treeview.findByText("Products"));
                treeview.expand(treeview.findByText("Service"));
            }
            if ($scope.showInActiveCategory == true)
                treeview.expand(".k-item");
        }

        function generateID(data) {
            var prodvalue = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].ProductCategoryId > prodvalue) {
                    prodvalue = data[i].ProductCategoryId;
                }
            }
            return prodvalue + 1;
        }
    }]
);