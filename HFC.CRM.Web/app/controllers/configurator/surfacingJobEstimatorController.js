﻿'use strict';
app.controller('surfacingJobEstimatorController', [
    '$http', '$scope', '$window', '$location', '$rootScope', '$sce', '$routeParams', 'configuratorService', 'NavbarService', 'HFCService', function ($http, $scope, $window, $location, $rootScope, $sce, $routeParams, ConfiguratorService, NavbarService, HFCService) {

        $scope.OpportunityId = $routeParams.OpportunityId;
        $scope.quoteKey = $routeParams.quoteKey;
        $rootScope.quoteKey = $scope.quoteKey;
        $rootScope.OpportunityId = $scope.OpportunityId;
        $scope.InstallationAddressId = 0;
        $scope.HFCService = HFCService;
        $scope.BrandId = $scope.HFCService.CurrentBrand;

        $scope.measurementList = [];
        $scope.CalculationGridForMaterials = [];
        $scope.CalculationGridForLabor = [];
        $scope.materialGroupId = 0;
        $scope.laborGroupId = 0;

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesOpportunitiesTab();

        $scope.profitMargin = "";
        $scope.totalQuotePrice = "";
        $scope.totalOrderCost = "";
        $scope.Measurement = '';
        $scope.JobEstimator = {
            OpportunityId: $scope.OpportunityId,
            QuoteKey: $scope.quoteKey,
            InstallationAddressId: $scope.InstallationAddressId,
            MeasurementId: '',
            Area: '',
            SummariziedQuoteLine: false,
            MaterialsList: '',
            LaborsList: ''
        };

        $http.get("/api/Measurement/" + $scope.OpportunityId + "/getMeasurementForJobSurfacing").then(function (data) {
            if (data)
                $scope.measurementList = data.data;
        });

        $http.get("/api/ProductTP/0/GetMaterialForTLorCCJobSurfacing").then(function (data) {
            var rrr = data;
            if (data.data.length > 0)
                $scope.materialListfromDB = data.data;
            else $scope.materialListfromDB = [];
        }).catch(function () {

        });

        $http.get("/api/ProductTP/0/GetLaborForTLorCCJobSurfacing").then(function (data) {
            var rrr = data;
            if (data.data.length > 0)
                $scope.laborListfromDB = data.data;
            else $scope.laborListfromDB = [];

            console.log($scope.laborListfromDB);
        }).catch(function () {

        });

        //    {
        //        "rowGroupId": 1,
        //    "productKey" : 514,
        //        "productName" : "4321-qwerty",
        //"product" : "product-1",
        //    "suggestedQty" : 12,
        //"adjustment" : .30,
        //    "qtyUsed" : 2,
        //    "usedCost" : 23,
        //    "quotePrice" :123,
        //    "suggestedOrderQty" :4,
        //    "orderQty" :3,
        //    "orderCost" : 345.00
        //    }
        //];


        $scope.MaterialsGrid = {
            dataSource: {
                group:       
                 { field: "ProductName" },                
                error: function (e) {

                    HFC.DisplayAlert(e.errorThrown);
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            ProductName: { editable: true },
                            Product: { editable: false },
                            SuggestedQuantity: { editable: false, type: "number" },
                            Adjustment: { editable: true, type: "number" },
                            QuantityUsed: { editable: false, type: "number" },
                            UsedCost: { editable: false, type: "number" },
                            QuotePrice: { editable: false, type: "number" },
                            SuggestedOrderQuantity: { editable: false, type: "number" },
                            OrderQuantity: { editable: true, type: "number" },
                            OrderCost: { editable: false, type: "number" }
                        }
                    }
                }
            },
            columns: [
                {
                    field: "ProductName",
                    title: "MATERIALS",
                    // template: columnTemplateFunction,
                    editor: function (container, options) {
                        $('<input data-bind="value:productKey" id="ProductName"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                valuePrimitive: true,
                                autoBind: true,
                                optionLabel: "Select",
                                dataTextField: "ProductName",
                                dataValueField: "ProductKey",
                                template: "#=ProductName #",
                                change: function (e, options) {
                                    // Do calculations


                                    if (e.sender.selectedIndex > 0 && e.sender.selectedIndex)
                                        $scope.MaterialCalculations(e.sender.selectedIndex, 0);
                                },

                                dataSource: $scope.materialListfromDB


                            });
                    }
                },
                {
                    field: "Product",
                    title: "Product",
                },
                {
                    field: "SuggestedQuantity",
                    title: "Suggested Qty",
                },
                {
                    field: "Adjustment",
                    title: "Adjustment",
                    //template: "<input required class='form-control frm_controllead1' name='Adjustment' data-bind='value:adjustment' style='width: 150px;' />",
                    editor: '<input class="form-control frm_controllead1" ng-blur="AdjustmentBlur(dataItem,$index)" data-bind="value:Adjustment" id="txtAdj" style="width: 150px;"   />'
                    //function (container, options) {        // onkeypress="return validateFloatKeyPress(this,event);"                 
                    //    $('<input class="form-control frm_controllead1" ng-blur="AdjustmentBlur(dataItem,$index)" data-bind="value:Adjustment" id="txtAdj" style="width: 150px;"   />')
                    //    .appendTo(container).kendoNumericTextBox({
                    //        //decimals: 2,   // Settings for the editor, e.g. no decimals
                    //        step: 0,   // Defines how the up/down arrows change the value
                    //        min: 0,// You can also define min/max values
                    //        spinners: false
                    //    });;   
                    //},
                },
                {
                    field: "QuantityUsed",
                    title: "Qty Used",
                },
                {
                    field: "UsedCost",
                    title: "Used Cost",
                    //template: function (dataItem) {
                    //    return "<span class='Grid_Textalign'>" + dataItem.usedCost + "</span>";
                    //},                    
                    footerTemplate: "<span class='Grid_Textalign'><div class='VPOWrapper'>Total</div></span>"
                },
                {
                    field: "QuotePrice",
                    title: "Quote Price",
                    footerTemplate: "<span><div class='VPOWrapper'id='totalQuotePrice'></div></span>"  ///{{orderCost}} // class='Grid_Textalign'
                    //footerTemplate: "<span class='Grid_Textalign'>#=kendo.toString(sum, 'C')#</span>"
                },
                {
                    field: "SuggestedOrderQuantity",
                    title: "Suggested Order Qty",
                    footerTemplate: "<span ><div class='VPOWrapper' id='profitMargin'></div></span>"
                },
                {
                    field: "OrderQuantity",
                    title: "Order Qty",
                    // template :  "<input required class='form-control frm_controllead1' name='Order Qty' data-bind='value:orderqty' style='width: 150px;' />",
                    editor: "<input class='form-control frm_controllead1' ng-blur='OrderQtyBlur()' id='txtOrdQuantity' data-bind='value:orderQty' style='width: 150px;' ng-keyup='materialOrdQuantityKeypress(dataItem)' />"
                    //function (container, options) {

                    //    $("<input class='form-control frm_controllead1' ng-blur='OrderQtyBlur()' id='txtOrdQuantity' data-bind='value:orderQty' style='width: 150px;' ng-keyup='materialOrdQuantityKeypress(dataItem)' />")
                    //    .appendTo(container).kendoNumericTextBox({
                    //        type:"number",
                    //        //format: "{0:n2}",
                    //        //decimals: 2,   // Settings for the editor, e.g. no decimals
                    //        step: 0,   // Defines how the up/down arrows change the value
                    //        min: 0,// You can also define min/max values
                    //        spinners: false
                    //    });
                    //},
                },
                {
                    field: "OrderCost",
                    title: "Order Cost",
                    //template: function (dataItem) {
                    //    return "<span class='Grid_Textalign'>" + dataItem.orderCost + "</span>";
                    //},
                    //aggregates: ["sum"],
                    //footerTemplate: "<span class='Grid_Textalign'>#=kendo.toString(sum, 'C')#</span>"
                    footerTemplate: "<span ><div class='VPOWrapper' id='totalOrderCost'></div></span>" // class='Grid_Textalign'
                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template:
                        ' <ul >' +

                            '<li><a href="javascript:void(0)" ng-click="RemoveMaterialLines(dataItem)">Delete</a></li>' +
                            '</ul>'
                }

            ],

            editable: true,
            filterable: false,
            resizable: true,
            sortable: false,
            scrollable: true,
            pageable: false,
            noRecords: { template: "No records found" },
            selectable: true
            //change: function (e) {
            //    var myDiv = $('.k-grid-content.k-auto-scrollable');
            //    myDiv[0].scrollTop = 0;
            //},
            //edit: function (e) {
            //    //if (e.container.find("#materialsValuedrop").context.innerText == "Select") {
            //    //    var grid = $("#materials").data("kendoGrid");
            //    //    grid.closeCell();
            //    //}

            //}
        };

        $scope.AdjustmentBlur = function (dataItem, Index) {
            $scope.materialAdjKeypress(dataItem);

        }

        $scope.OrderQtyBlur = function () {
            $('#materials').data('kendoGrid').setDataSource($scope.CalculationGridForMaterials);
            $scope.MaterialTotals();
            //if ($("#txtOrdQuantity").val() === '.' || $("#txtOrdQuantity").val() === "") {
            //    $("#txtOrdQuantity").val(0);
            //}
        }

        $scope.materialAdjKeypress = function (dataItem) {

            var grid = $("#materials").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());

            var valuee = $("#txtAdj").val();
            while (valuee.charAt(0) === '0' && valuee != '0')
                valuee = valuee.slice(1);

            $("#txtAdj").val(valuee);

            if ($("#txtAdj").val() === '.' || $("#txtAdj").val() === "") {
                $scope.CalculationGridForMaterials[grid.select().index()].Adjustment = Number('0').toFixed(2);
                selectedItem.Adjustment = Number('0').toFixed(2);
                valuee = 0;
            }
            else if ($("#txtAdj").val().slice(-1) === '.') {
                // $("#txtOrdQuantity").val(0);
                $scope.CalculationGridForMaterials[grid.select().index()].Adjustment = $("#txtAdj").val().slice(0, -1);
                selectedItem.Adjustment = $("#txtAdj").val().slice(0, -1);
                valuee = $("#txtAdj").val().slice(0, -1);
            }
            else {
                if (decimalPlaces($("#txtAdj").val()) > 2)
                    $("#txtAdj").val(Number($("#txtAdj").val()).toFixed(2));

                $scope.CalculationGridForMaterials[grid.select().index()].Adjustment = Number($("#txtAdj").val());
                selectedItem.Adjustment = Number($("#txtAdj").val());
                valuee = Number($("#txtAdj").val());
            }

            //var vall = $("#txtAdj").val();
            //if (vall === '') vall = '0';
            //if (vall.slice(-1) !== '.') {
            //    while (vall.charAt(0) === '0' && vall != '0') {
            //        vall = vall.slice(1);
            //        $("#txtAdj").val(vall)
            //    }


            //    var grid = $("#materials").data("kendoGrid");
            //    var selectedItem = grid.dataItem(grid.select());



            for (var c = 0; c <= $scope.materialListfromDB.length - 1; c++) {
                if ($scope.materialListfromDB[c].ProductKey === $scope.CalculationGridForMaterials[grid.select().index()].ProductKey) {
                    var aaa = $scope.materialListfromDB[c].MultipleSurfaceProductSet;

                    var sumOfMixRatio = 0;
                    for (var q = 0 ; q < aaa.length ; q++) {
                        if (aaa[q].Mixratio > 0 && aaa[q].Mixratio)
                            sumOfMixRatio = sumOfMixRatio + aaa[q].Mixratio;
                    }
                    for (var vv = 0; vv <= aaa.length - 1; vv++) {
                        if (aaa[vv].ProductID === $scope.CalculationGridForMaterials[grid.select().index()].Product) {

                            //var adjustedMixedGallensNeeded = ((aaa[vv].Coverage / $scope.Measurement.Item3) + (aaa[vv].WasteReclaim / ($scope.Measurement.Item3 / 100))) * (1 + parseFloat(valuee));
                            //// (initial gallons needed+waste in gallons)∗(1+adjustment %) // (1+(1/(vall*100)));
                            //var suggestedQuantity_cal = Number(adjustedMixedGallensNeeded * (aaa[vv].Mixratio / sumOfMixRatio)).toFixed(2);
                            //var adjustment_cal = valuee; //
                            ////  Number(vall).toFixed(2);
                            //var qtyUsed_cal = suggestedQuantity_cal;// Number(Number(suggestedQuantity_cal) + adjustment_cal).toFixed(2);
                            //var usedCost_cal = Number(Number(qtyUsed_cal) * (aaa[vv].CostPrice / aaa[vv].ContainerSize)).toFixed(2);  //q_ty used * (cost per container _/ container size)
                            //var quotePrice_cal = Number(Number(qtyUsed_cal) * (aaa[vv].UnitPrice / aaa[vv].ContainerSize)).toFixed(2);   //Qty used * (Price per container / container size)
                            //var suggestedOrderQuantity_cal = Number(Number(qtyUsed_cal) / aaa[vv].ContainerSize).toFixed(2);  // Roundup (qty used / container size)
                            //var orderQuantity_cal = Number(suggestedOrderQuantity_cal).toFixed(2);
                            //var orderCost_cal = Number(Number(orderQuantity_cal) * Number(aaa[vv].CostPrice)).toFixed(2);   // order quantity * cost per container


                            var MixedGallonsNeeded = (aaa[vv].Coverage / $scope.Measurement.Item3).toFixed(2);
                            var WasteInGallons = (aaa[vv].WasteReclaim / ($scope.Measurement.Item3 / 100)).toFixed(2);
                            var AdjustedMixedGallensNeeded = (MixedGallonsNeeded + WasteInGallons) * (1 + parseFloat(valuee));
                            var QuantityUsed = (MixedGallonsNeeded * (aaa[vv].Mixratio / sumOfMixRatio)).toFixed(2);
                            var UsedCost = (QuantityUsed * (aaa[vv].CostPrice / aaa[vv].ContainerSize)).toFixed(2);
                            var QuotePrice = (QuantityUsed * (aaa[vv].UnitPrice / aaa[vv].ContainerSize)).toFixed(2);
                            var SuggestedOrderQuantity = (QuantityUsed / aaa[vv].ContainerSize).toFixed(2);
                            var OrderQuantity = SuggestedOrderQuantity;
                            var OrderCost = (OrderQuantity * aaa[vv].CostPrice).toFixed(2);

                            var adjustment_cal = valuee;
                            var adjustedMixedGallensNeeded = ((aaa[vv].Coverage / $scope.Measurement.Item3) + (aaa[vv].WasteReclaim / ($scope.Measurement.Item3 / 100))) * (1 + parseFloat(valuee));
                            var suggestedQuantity_cal = Number(Number(adjustedMixedGallensNeeded * (aaa[vv].Mixratio / sumOfMixRatio)).toFixed(2));

                            $scope.CalculationGridForMaterials[grid.select().index()].SuggestedQuantity = suggestedQuantity_cal;
                            $scope.CalculationGridForMaterials[grid.select().index()].Adjustment = valuee;
                            $scope.CalculationGridForMaterials[grid.select().index()].QuantityUsed = QuantityUsed;
                            $scope.CalculationGridForMaterials[grid.select().index()].UsedCost = UsedCost;
                            $scope.CalculationGridForMaterials[grid.select().index()].QuotePrice = QuotePrice;
                            $scope.CalculationGridForMaterials[grid.select().index()].SuggestedOrderQuantity = SuggestedOrderQuantity;
                            $scope.CalculationGridForMaterials[grid.select().index()].OrderQuantity = OrderQuantity;
                            $scope.CalculationGridForMaterials[grid.select().index()].OrderCost = OrderCost;

                            selectedItem.SuggestedQuantity = suggestedQuantity_cal;
                            selectedItem.Adjustment = adjustment_cal;
                            selectedItem.QuantityUsed = QuantityUsed;
                            selectedItem.UsedCost = UsedCost;
                            selectedItem.QuotePrice = QuotePrice;
                            selectedItem.SuggestedOrderQuantity = SuggestedOrderQuantity;
                            selectedItem.OrderQuantity = OrderQuantity;
                            selectedItem.OrderCost = OrderCost;

                            //if(vall === '0')
                            //$("#txtAdj").val(0);

                        }
                    }
                }
            }

            $('#materials').data('kendoGrid').setDataSource($scope.CalculationGridForMaterials);
            $scope.MaterialTotals();
            //}
        }

        $scope.materialOrdQuantityKeypress = function (dataItem) {
            //if ($("#txtOrdQuantity").val() === '.') $("#txtOrdQuantity").val(0);
            //if ($("#txtOrdQuantity").val().slice(-1) === '.') $("#txtOrdQuantity").val($("#txtOrdQuantity").val().slice(0, -1)); //laborQuoteHour.slice(-1) !== '.' // .slice(0, -1)


            var grid = $("#materials").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());

            var valuee = $("#txtOrdQuantity").val();
            while (valuee.charAt(0) === '0' && valuee != '0')
                valuee = valuee.slice(1);

            $("#txtOrdQuantity").val(valuee);

            if ($("#txtOrdQuantity").val() === '.' || $("#txtOrdQuantity").val() === "") {
                $scope.CalculationGridForMaterials[grid.select().index()].OrderQuantity = Number(0).toFixed(2);
                selectedItem.OrderQuantity = Number(0).toFixed(2);
                valuee = 0;
            }
            else if ($("#txtOrdQuantity").val().slice(-1) === '.') {
                // $("#txtOrdQuantity").val(0);
                $scope.CalculationGridForMaterials[grid.select().index()].OrderQuantity = $("#txtOrdQuantity").val().slice(0, -1);
                selectedItem.OrderQuantity = $("#txtOrdQuantity").val().slice(0, -1);
                valuee = $("#txtOrdQuantity").val().slice(0, -1);
            }
            else {
                if (decimalPlaces($("#txtOrdQuantity").val()) > 2)
                    $("#txtOrdQuantity").val(Number($("#txtOrdQuantity").val()).toFixed(2));

                $scope.CalculationGridForMaterials[grid.select().index()].OrderQuantity = Number($("#txtOrdQuantity").val());
                selectedItem.OrderQuantity = Number($("#txtOrdQuantity").val());
                valuee = Number($("#txtOrdQuantity").val());
            }
            //  else
            // {


            for (var c = 0; c <= $scope.materialListfromDB.length - 1; c++) {
                if ($scope.materialListfromDB[c].ProductKey === $scope.CalculationGridForMaterials[grid.select().index()].ProductKey) {
                    var aaa = $scope.materialListfromDB[c].MultipleSurfaceProductSet;
                    for (var vv = 0; vv <= aaa.length - 1; vv++) {
                        if (aaa[vv].ProductID === $scope.CalculationGridForMaterials[grid.select().index()].Product) {
                            $scope.CalculationGridForMaterials[grid.select().index()].OrderCost = Number(valuee * aaa[vv].CostPrice).toFixed(2);
                            selectedItem.OrderCost = Number(valuee * aaa[vv].CostPrice).toFixed(2);  //  order quantity * cost per container

                            // $scope.CalculationGridForMaterials[grid.select().index()].orderQty = Number(valuee);
                            //  selectedItem.orderQty = Number($("#txtOrdQuantity").val());  //  order quantity * cost per container
                        }
                    }
                }
            }


            if (valuee === '')
                $("#txtOrdQuantity").val(0);


            $scope.MaterialTotals();
            //  }

            //for (var w = $scope.CalculationGridForMaterials.length - 1; w >= 0 ; w--) {
            //    if ($scope.CalculationGridForMaterials[w].product === dataItem.product) {
            //        $scope.CalculationGridForMaterials[w].orderQty = valuee;
            //    }
            //}


            //  $('#materials').data('kendoGrid').setDataSource($scope.CalculationGridForMaterials);
            //}
        }

        function decimalPlaces(num) {
            var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            if (!match) { return 0; }
            return Math.max(
                 0,
                 // Number of digits right of decimal point.
                 (match[1] ? match[1].length : 0)
                 // Adjust for scientific notation.
                 - (match[2] ? +match[2] : 0));
        }

        $scope.MaterialCalculations = function (selectedIndex, adjusments) {

            $scope.rowFull = $scope.materialListfromDB[selectedIndex - 1];
            $scope.qqq = $scope.materialListfromDB[selectedIndex - 1].MultipleSurfaceProductSet;
            var sumOfMixRatio = 0;
            for (var q = 0 ; q < $scope.qqq.length ; q++) {
                if ($scope.qqq[q].Mixratio > 0 && $scope.qqq[q].Mixratio)
                    sumOfMixRatio = sumOfMixRatio + $scope.qqq[q].Mixratio;
            }

            for (var w = $scope.CalculationGridForMaterials.length - 1; w >= 0 ; w--) {
                if ($scope.CalculationGridForMaterials[w].rowGroupId === $scope.materialGroupId) {
                    $scope.CalculationGridForMaterials.splice(w, 1);
                }
            }

            for (var t = $scope.qqq.length - 1; t >= 0 ; t--) {
                var product_cal = $scope.qqq[t].ProductID;
                // mixed gallons needed = (square feet per mixed gallon)/(square feet of job)
                //waste in gallons = (waste in gallons per 100 square feet)/(((square feet of job)/100))
                //   adjusted mixed gallons needed = (initial gallons needed+waste in gallons)∗(1+adjustment %)

                var MixedGallonsNeeded = ($scope.qqq[t].Coverage / $scope.Measurement.Item3).toFixed(2);
                var WasteInGallons = ($scope.qqq[t].WasteReclaim / ($scope.Measurement.Item3 / 100)).toFixed(2);
                var AdjustedMixedGallensNeeded = (MixedGallonsNeeded + WasteInGallons) * (1);
                var QuantityUsed = (MixedGallonsNeeded * ($scope.qqq[t].Mixratio / sumOfMixRatio)).toFixed(2);
                var UsedCost = (QuantityUsed * ($scope.qqq[t].CostPrice / $scope.qqq[t].ContainerSize)).toFixed(2);
                var QuotePrice = (QuantityUsed * ($scope.qqq[t].UnitPrice / $scope.qqq[t].ContainerSize)).toFixed(2);
                var SuggestedOrderQuantity = (QuantityUsed / $scope.qqq[t].ContainerSize).toFixed(2);
                var OrderQuantity = SuggestedOrderQuantity;
                var OrderCost = (OrderQuantity * $scope.qqq[t].CostPrice).toFixed(2);

                var adjustedMixedGallensNeeded = (($scope.qqq[t].Coverage / $scope.Measurement.Item3) + ($scope.qqq[t].WasteReclaim / ($scope.Measurement.Item3 / 100))) * (1);
                var suggestedQuantity_cal = Number(Number(adjustedMixedGallensNeeded * ($scope.qqq[t].Mixratio / sumOfMixRatio)).toFixed(2));

                // var qtyUsed_cal = Number(($scope.qqq[t].Coverage / $scope.Measurement.Item3) * ($scope.qqq[t].Mixratio / sumOfMixRatio)).toFixed(2);
                var adjustment_cal = Number(Number(adjusments)).toFixed(2);
                //var qtyUsed_cal = Number(Number(Number(suggestedQuantity_cal) + Number(adjustment_cal)).toFixed(2));
                // var usedCost_cal =Number(Number(qtyUsed_cal * ($scope.qqq[t].CostPrice / $scope.qqq[t].ContainerSize)).toFixed(2));  //q_ty used * (cost per container _/ container size)
                // var quotePrice_cal = Number(Number(qtyUsed_cal * ($scope.qqq[t].UnitPrice / $scope.qqq[t].ContainerSize)).toFixed(2));   //Qty used * (Price per container / container size)
                //var suggestedOrderQuantity_cal = Number(Number(qtyUsed_cal / $scope.qqq[t].ContainerSize).toFixed(2));  // Roundup (qty used / container size)
                //var suggestedOrderQuantity_cal = Math.round(qtyUsed_cal / $scope.qqq[t].ContainerSize);  // Roundup (qty used / container size)
                // var orderQuantity_cal = Number(Number(suggestedOrderQuantity_cal).toFixed(2));
                // var orderCost_cal = Number(Number(orderQuantity_cal * $scope.qqq[t].CostPrice).toFixed(2));   // order quantity * cost per container


                //if ($scope.CalculationGridForMaterials.length === 0) {
                //    $scope.CalculationGridForMaterials = [];
                //    $scope.CalculationGridForMaterials.push({
                //        "rowGroupId": $scope.materialGroupId,
                //        "productKey": $scope.rowFull.ProductKey,
                //        "productName": $scope.rowFull.ProductName,
                //        "product": product_cal,
                //        "suggestedQty": suggestedQuantity_cal,
                //        "adjustment": adjustment_cal,
                //        "qtyUsed": qtyUsed_cal,
                //        "usedCost": usedCost_cal,
                //        "quotePrice": quotePrice_cal,
                //        "suggestedOrderQty": suggestedOrderQuantity_cal,
                //        "orderQty": orderQuantity_cal,
                //        "orderCost": orderCost_cal
                //    });
                //}
                //else {
                $scope.CalculationGridForMaterials.splice(0, 0, {
                    "rowGroupId": $scope.materialGroupId,
                    "ProductKey": $scope.rowFull.ProductKey,
                    "ProductName": $scope.rowFull.ProductName,
                    "Product": product_cal,
                    "SuggestedQuantity": suggestedQuantity_cal,
                    "Adjustment": adjustment_cal,
                    "QuantityUsed": QuantityUsed,
                    "UsedCost": UsedCost,
                    "QuotePrice": QuotePrice,
                    "SuggestedOrderQuantity": SuggestedOrderQuantity,
                    "OrderQuantity": OrderQuantity,
                    "OrderCost": OrderCost
                });
                //}
            }

            $('#materials').data('kendoGrid').setDataSource($scope.CalculationGridForMaterials);
            $scope.MaterialTotals();


        }

        $scope.MaterialTotals = function () {
            $scope.quotepriceTotal = 0;
            $scope.orderCostTotal = 0;
            for (var l = 0; l <= $scope.CalculationGridForMaterials.length - 1; l++) {
                $scope.quotepriceTotal = Number(Number($scope.quotepriceTotal) + Number($scope.CalculationGridForMaterials[l].QuotePrice)).toFixed(2);
                $scope.orderCostTotal = Number(Number($scope.orderCostTotal) + Number($scope.CalculationGridForMaterials[l].OrderCost)).toFixed(2);
            }

            $scope.MarginPercentage = Number(($scope.quotepriceTotal - $scope.orderCostTotal) / $scope.quotepriceTotal * 100).toFixed(2);


            $scope.totalQuotePrice = $scope.quotepriceTotal.toString();
            $scope.totalOrderCost = $scope.orderCostTotal.toString();
            $scope.profitMargin = $scope.MarginPercentage.toString();
            if ($scope.profitMargin === "NaN") $scope.profitMargin = "0";
            document.getElementById("totalQuotePrice").innerHTML = $scope.quotepriceTotal.toString();
            document.getElementById("totalOrderCost").innerHTML = $scope.orderCostTotal.toString();
            document.getElementById("profitMargin").innerHTML = "(" + $scope.MarginPercentage.toString() + "%)";


        }

        $scope.materialOnSelection = function (kendoEvent) {
            var grid = kendoEvent.sender;
            var selectedData = grid.dataItem(grid.select());
            $scope.materialGroupId = selectedData.rowGroupId;
        }

        $scope.getMaterialGroupID = function () {
            for (var z = 0; z < $scope.CalculationGridForMaterials.length ; z++) {
                if ($scope.materialGroupId < $scope.CalculationGridForMaterials[z].rowGroupId) {
                    $scope.materialGroupId = $scope.CalculationGridForMaterials[z].rowGroupId;
                }
            }

            if ($scope.materialGroupId === 0) $scope.materialGroupId = 1;
        }

        //add new row to material grid
        $scope.AddRow = function (gridid) {
            var grid = $("#" + gridid).data("kendoGrid");
            var rowEdit = $('#' + gridid).find('.k-grid-edit-row');
            if (rowEdit.length) {
                var validator = $scope.kendoValidator(gridid);
                if (!validator.validate()) {
                    return false;
                }
            }
            else {

                for (var z = 0; z < $scope.CalculationGridForMaterials.length ; z++) {
                    if (z === 0) $scope.materialGroupId = $scope.CalculationGridForMaterials[z].rowGroupId;
                    if ($scope.materialGroupId < $scope.CalculationGridForMaterials[z].rowGroupId) {
                        $scope.materialGroupId = $scope.CalculationGridForMaterials[z].rowGroupId;
                    }
                }

                if ($scope.materialGroupId === 0) $scope.materialGroupId = 1;
                else $scope.materialGroupId = $scope.materialGroupId + 1;


                $scope.CalculationGridForMaterials.splice(0, 0, {
                    "rowGroupId": $scope.materialGroupId,
                    "ProductKey": 0,
                    "ProductName": '',
                    "Product": '',
                    "SuggestedQuantity": 0,
                    "Adjustment": 0,
                    "QuantityUsed": 0,
                    "UsedCost": 0,
                    "QuotePrice": 0,
                    "SuggestedOrderQuantity": 0,
                    "OrderQuantity": 0,
                    "OrderCost": 0
                });


                $('#materials').data('kendoGrid').setDataSource($scope.CalculationGridForMaterials);

                var grid = $("#materials").data("kendoGrid");
                grid.editRow($("#materials tr:eq(1)"));
                //grid.addRow();
                //$scope.EditMode = true;

            }

            $scope.MaterialTotals();
        }

        $scope.validateCurrentRecord = function () {
            var returnval = true;
            var ProductName = $("#ProductName").data("kendoDropDownList");
        }


        //$scope.ADD_New = function () {
        //   var grid = $("#caseGrid01").data("kendoGrid");
        //    var dataSource = grid.dataSource;
        //    if ($scope.selectedRow >= 0) {
        //        if ($scope.ProductNamem)
        //            grid.dataSource._data[$scope.selectedRow].ProductName = $scope.ProductNamem;
        //        if ($scope.ProductIdm)
        //            grid.dataSource._data[$scope.selectedRow].ProductId = $scope.ProductIdm;
        //        if ($scope.ProductDescriptionm)
        //            grid.dataSource._data[$scope.selectedRow].Description = $scope.ProductDescriptionm;
        //        if ($scope.VendorNamem)
        //            grid.dataSource._data[$scope.selectedRow].VendorName = $scope.VendorNamem;
        //        if ($scope.VendorReferencem)
        //            grid.dataSource._data[$scope.selectedRow].VendorReference = $scope.VendorReferencem;
        //        if ($scope.vpom)
        //            grid.dataSource._data[$scope.selectedRow].VPO = $scope.vpom;
        //        if ($scope.LineNumberQuote)
        //            grid.dataSource._data[$scope.selectedRow].QuoteLineNumber = $scope.LineNumberQuote;
        //        if ($scope.TypeValDesc)
        //            grid.dataSource._data[$scope.selectedRow].Typevalue = $scope.TypeValDesc;
        //        if ($scope.ReasonvalDesc)
        //            grid.dataSource._data[$scope.selectedRow].CaseReason = $scope.ReasonvalDesc;
        //        if ($scope.StatusvalDesc)
        //            grid.dataSource._data[$scope.selectedRow].StatusName = $scope.StatusvalDesc;
        //        if ($scope.ResolutionvalDesc)
        //            grid.dataSource._data[$scope.selectedRow].ResolutionName = $scope.ResolutionvalDesc;
        //    }

        //    var validator = $scope.kendoValidator("caseGrid01");
        //    if (validator.validate()) {
        //        var newItem = {
        //            ExpediteReq: false,
        //            ExpediteApproved: false,
        //            ReqTripCharge: false,
        //            TripChgApp: "$",
        //            CaseId: '',
        //        }
        //        var gridd = $("#caseGrid01").data("kendoGrid");
        //        var dataSource = gridd.dataSource;
        //        var newItem = dataSource.insert(0, newItem);
        //        var newRow = gridd.items().filter("[data-uid='" + newItem.uid + "']");
        //        $scope.selectedRow = 0;
        //        gridd.editRow(newRow);
        //        // sub  row expand and bring edit mode
        //        var row = gridd.tbody.find("tr[data-uid='" + gridd.dataSource._data[0].uid + "']");
        //        gridd.expandRow(row);

        //        var grdId = "#grd" + gridd.dataSource._data[0].uid;
        //        var childgrid = $(grdId).data("kendoGrid");
        //        childgrid.editRow(gridd.dataSource._data[0]);

        //        $scope.resetvalues();
        //    }
        //    else {
        //        return false;
        //    }
        //};

        $scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                validate: function (e) {
                    $("span.k-invalid-msg").hide();
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find(".k-widget.k-dropdown.k-header");
                        // 
                        if (input.size() > 0) {
                            $(this).addClass("dropdown-validation-error");
                        } else {
                            $(this).removeClass("dropdown-validation-error");
                        }
                    });
                }
            }).getKendoValidator();
        }

        $scope.MeasurementChange = function (Measurement) {
            console.log($scope.CalculationGridForMaterials);
            $scope.JobEstimator.Area = Measurement.Item3;
            $scope.JobEstimator.MeasurementId = Measurement.Item1;
            if ($scope.CalculationGridForMaterials != null && $scope.CalculationGridForMaterials.length > 0) {
                for (var i = 0; i < $scope.CalculationGridForMaterials.length; i++) {
                    for (var j = 0; j < $scope.materialListfromDB.length; j++) {
                        if ($scope.materialListfromDB[j].ProductKey == $scope.CalculationGridForMaterials[i].ProductKey) {
                            var sumOfMixRatio = 0;
                            var MultisurfaceSet = $scope.materialListfromDB[j].MultipleSurfaceProductSet;
                            for (var k = 0; k < MultisurfaceSet.length; k++) {
                                if (MultisurfaceSet[k].Mixratio > 0 && MultisurfaceSet[k].Mixratio)
                                    sumOfMixRatio = sumOfMixRatio + MultisurfaceSet[k].Mixratio;
                            }
                            for (var vv = 0; vv < MultisurfaceSet.length; vv++) {
                                if ($scope.CalculationGridForMaterials[i].Product == MultisurfaceSet[vv].ProductID) {
                                    var MixedGallonsNeeded = (MultisurfaceSet[vv].Coverage / $scope.Measurement.Item3).toFixed(2);
                                    var WasteInGallons = (MultisurfaceSet[vv].WasteReclaim / ($scope.Measurement.Item3 / 100)).toFixed(2);
                                    var AdjustedMixedGallensNeeded = (MixedGallonsNeeded + WasteInGallons) * (1 + parseFloat($scope.CalculationGridForMaterials[i].Adjustment));
                                    var QuantityUsed = (MixedGallonsNeeded * (MultisurfaceSet[vv].Mixratio / sumOfMixRatio)).toFixed(2);
                                    var UsedCost = (QuantityUsed * (MultisurfaceSet[vv].CostPrice / MultisurfaceSet[vv].ContainerSize)).toFixed(2);
                                    var QuotePrice = (QuantityUsed * (MultisurfaceSet[vv].UnitPrice / MultisurfaceSet[vv].ContainerSize)).toFixed(2);
                                    var SuggestedOrderQuantity = (QuantityUsed / MultisurfaceSet[vv].ContainerSize).toFixed(2);
                                    var OrderQuantity = SuggestedOrderQuantity;
                                    var OrderCost = (OrderQuantity * MultisurfaceSet[vv].CostPrice).toFixed(2);

                                    var adjustment_cal = parseFloat($scope.CalculationGridForMaterials[i].Adjustment);
                                    var adjustedMixedGallensNeeded = ((MultisurfaceSet[vv].Coverage / $scope.Measurement.Item3) + (MultisurfaceSet[vv].WasteReclaim / ($scope.Measurement.Item3 / 100))) * (1 + parseFloat($scope.CalculationGridForMaterials[i].Adjustment));
                                    var suggestedQuantity_cal = Number(Number(adjustedMixedGallensNeeded * (MultisurfaceSet[vv].Mixratio / sumOfMixRatio)).toFixed(2));

                                    $scope.CalculationGridForMaterials[i].SuggestedQuantity = suggestedQuantity_cal;
                                   // $scope.CalculationGridForMaterials[i].Adjustment = valuee;
                                    $scope.CalculationGridForMaterials[i].QuantityUsed = QuantityUsed;
                                    $scope.CalculationGridForMaterials[i].UsedCost = UsedCost;
                                    $scope.CalculationGridForMaterials[i].QuotePrice = QuotePrice;
                                    $scope.CalculationGridForMaterials[i].SuggestedOrderQuantity = SuggestedOrderQuantity;
                                    $scope.CalculationGridForMaterials[i].OrderQuantity = OrderQuantity;
                                    $scope.CalculationGridForMaterials[i].OrderCost = OrderCost;
                                }
                            }
                        }
                    }
                }
                $('#materials').data('kendoGrid').setDataSource($scope.CalculationGridForMaterials);
                $scope.MaterialTotals();
            }
        }

        $scope.RemoveMaterialLines = function (dataItem) {
            for (var w = $scope.CalculationGridForMaterials.length - 1; w >= 0 ; w--) {
                if ($scope.CalculationGridForMaterials[w].rowGroupId === dataItem.rowGroupId) {
                    $scope.CalculationGridForMaterials.splice(w, 1);
                }
            }

            $('#materials').data('kendoGrid').setDataSource($scope.CalculationGridForMaterials);

            $scope.MaterialTotals();
        }

        //// labor code STARTS HERE

        /*
        JobType
        Day
        Process
        SuggestedCrewSize
        SuggestedHours
        QuoteHours
        QuotePrice
        LaborCost

        */


        $scope.laborGrid = {
            dataSource: {
                error: function (e) {

                    HFC.DisplayAlert(e.errorThrown);
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            ProductName: { editable: true },
                            Day: { editable: false },
                            Process: { editable: false },
                            SuggestedCrewSize: { editable: false },
                            SuggestedHours: { editable: false },
                            QuoteHours: { editable: true, type: "number" },
                            QuotePrice: { editable: false, type: "number" },
                            LaborCost: { editable: false, type: "number" }
                        }
                    }
                }
            },

            columns: [
                {
                    field: "ProductName",
                    title: "JobType",
                    // template: columnTemplateFunction,
                    editor: function (container, options) {
                        $('<input data-bind="value:productKey"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                valuePrimitive: true,
                                autoBind: true,
                                optionLabel: "Select",
                                dataTextField: "ProductName",
                                dataValueField: "ProductKey",
                                template: "#=ProductName #",
                                change: function (e, options) {
                                    // Do calculations
                                    if (e.sender.selectedIndex > 0 && e.sender.selectedIndex)
                                        $scope.LaborCalculations(e.sender.selectedIndex, 0);
                                },

                                dataSource: $scope.laborListfromDB


                            });
                    }
                },
                {
                    field: "Day",
                    title: "Day",
                },
                {
                    field: "Process",
                    title: "Process",
                },
                {
                    field: "SuggestedCrewSize",
                    title: "Suggested Crew Size",
                    //editor:
                    //     function (container, options) {        // onkeypress="return validateFloatKeyPress(this,event);"                 
                    //         $('<input class="form-control frm_controllead1"  data-bind="value:adjustment" id="txtAdj" style="width: 150px;" ng-keyup="materialAdjKeypress(dataItem)" />')
                    //         .appendTo(container).kendoNumericTextBox({
                    //             decimals: 2,   // Settings for the editor, e.g. no decimals
                    //             step: 0,   // Defines how the up/down arrows change the value
                    //             min: 0,// You can also define min/max values
                    //             spinners: false
                    //         });;
                    //     },
                },
                {
                    field: "SuggestedHours",
                    title: "Suggested Hours",
                },
                {
                    field: "QuoteHours",
                    title: "Quote Hours",
                    editor:
                       function (container, options) {

                           $("<input class='form-control frm_controllead1' id='txtLaborQuoteHours' data-bind='value:QuoteHours' style='width: 150px;' ng-keyup='LabourQuoteHoursKeyup(dataItem)' />")
                           .appendTo(container).kendoNumericTextBox({
                               type: "number",
                               format: "{0:n0}",
                               decimals: 0,   // Settings for the editor, e.g. no decimals
                               step: 0,   // Defines how the up/down arrows change the value
                               min: 0,// You can also define min/max values
                               spinners: false
                           });
                       },
                    footerTemplate: "<span class='Grid_Textalign'><div class='VPOWrapper'>Total</div></span>"
                },
                {
                    field: "QuotePrice",
                    title: "Quote Price",
                    footerTemplate: "<span><div class='VPOWrapper'id='totalLaborQuotePrice'></div></span>"  ///{{orderCost}} // class='Grid_Textalign'
                    //footerTemplate: "<span class='Grid_Textalign'>#=kendo.toString(sum, 'C')#</span>"
                },
                {
                    field: "LaborCost",
                    title: "LaborCost",
                    footerTemplate: "<span><div class='VPOWrapper'id='totalLaborCost'></div></span>"

                },
                {
                    field: "",
                    title: "",
                    width: "60px",
                    template:
                        ' <ul >' +

                            '<li><a href="javascript:void(0)" ng-click="RemoveLaborLines(dataItem)">Delete</a></li>' +
                            '</ul>'
                }

            ],
            editable: true,
            //edit: function (e) {
            //    e.container.find(".k-edit-label:last").hide();
            //    e.container.find(".k-edit-field:last").hide();
            //},
            filterable: false,
            resizable: true,
            sortable: false,
            scrollable: true,
            pageable: false,
            noRecords: { template: "No records found" },
        };


        //add new row to Labor grid
        $scope.AddLaborRow = function () {
            var grid = $("#labor").data("kendoGrid");
            var rowEdit = $('#labor').find('.k-grid-edit-row');
            if (rowEdit.length) {
                var validator = $scope.kendoValidator('labor');
                if (!validator.validate()) {
                    return false;
                }
            }
            else {

                for (var z = 0; z < $scope.CalculationGridForLabor.length ; z++) {
                    if (z === 0) $scope.laborGroupId = $scope.CalculationGridForLabor[z].rowGroupId;
                    if ($scope.laborGroupId < $scope.CalculationGridForLabor[z].rowGroupId) {
                        $scope.laborGroupId = $scope.CalculationGridForLabor[z].rowGroupId;
                    }
                }

                if ($scope.laborGroupId === 0) $scope.laborGroupId = 1;
                else $scope.laborGroupId = $scope.laborGroupId + 1;


                $scope.CalculationGridForLabor.splice(0, 0, {
                    "rowGroupId": $scope.laborGroupId,
                    "ProductKey": 0,
                    "ProductName": '',
                    "Day": 0,
                    "Process": '',
                    "SuggestedCrewSize": 0,
                    "SuggestedHours": 0,
                    "QuoteHours": 0,
                    "QuotePrice": 0.00,
                    "LaborCost": 0.00
                });


                $('#labor').data('kendoGrid').setDataSource($scope.CalculationGridForLabor);

                var grid = $("#labor").data("kendoGrid");
                grid.editRow($("#labor tr:eq(1)"));
            }

            $scope.LaborTotals();
        }

        $scope.RemoveLaborLines = function (dataItem) {
            for (var w = $scope.CalculationGridForLabor.length - 1; w >= 0 ; w--) {
                if ($scope.CalculationGridForLabor[w].rowGroupId === dataItem.rowGroupId) {
                    $scope.CalculationGridForLabor.splice(w, 1);
                }
            }

            $('#labor').data('kendoGrid').setDataSource($scope.CalculationGridForLabor);

            $scope.LaborTotals();
        }

        $scope.LaborCalculations = function (selectedIndex, adjusments) {

            $scope.rowFull = $scope.laborListfromDB[selectedIndex - 1];
            //  $scope.qqq = $scope.laborListfromDB[selectedIndex - 1].MultipleSurfaceProductSet;

            if ($scope.laborListfromDB[selectedIndex - 1].ProductSurfaceSetup) {
                if ($scope.laborListfromDB[selectedIndex - 1].ProductSurfaceSetup.ProductSurfaceSetupForCC) $scope.qqq = $scope.laborListfromDB[selectedIndex - 1].ProductSurfaceSetup.ProductSurfaceSetupForCC;
                else $scope.qqq = $scope.laborListfromDB[selectedIndex - 1].ProductSurfaceSetup.ProductSurfaceSetupForTL;


                if ($scope.qqq) {
                    for (var w = $scope.CalculationGridForLabor.length - 1; w >= 0 ; w--) {
                        if ($scope.CalculationGridForLabor[w].rowGroupId === $scope.laborGroupId) {
                            $scope.CalculationGridForLabor.splice(w, 1);
                        }
                    }


                    for (var t = $scope.qqq.length - 1; t >= 0 ; t--) {



                        if ($scope.CalculationGridForLabor.length === 0) {
                            $scope.CalculationGridForLabor = [];
                            $scope.CalculationGridForLabor.push({
                                "rowGroupId": $scope.laborGroupId,
                                "ProductKey": $scope.rowFull.ProductKey,
                                "ProductName": $scope.rowFull.ProductName,
                                "Day": $scope.qqq[t].Day,
                                "Process": $scope.qqq[t].Process,
                                "SuggestedCrewSize": $scope.qqq[t].SuggestedCrewSize,
                                "SuggestedHours": $scope.qqq[t].SuggestedHours,
                                "QuoteHours": $scope.qqq[t].SuggestedHours,
                                "QuotePrice": ($scope.qqq[t].SuggestedHours * $scope.rowFull.SalePrice).toFixed(2),
                                "LaborCost": ($scope.qqq[t].SuggestedHours * $scope.rowFull.Cost).toFixed(2),
                                "Cost": $scope.rowFull.Cost,
                                "SalePrice": $scope.rowFull.SalePrice
                            });
                        }
                        else {
                            $scope.CalculationGridForLabor.splice(0, 0, {
                                "rowGroupId": $scope.laborGroupId,
                                "ProductKey": $scope.rowFull.ProductKey,
                                "ProductName": $scope.rowFull.ProductName,
                                "Day": $scope.qqq[t].Day,
                                "Process": $scope.qqq[t].Process,
                                "SuggestedCrewSize": $scope.qqq[t].SuggestedCrewSize,
                                "SuggestedHours": $scope.qqq[t].SuggestedHours,
                                "QuoteHours": $scope.qqq[t].SuggestedHours,
                                "QuotePrice": ($scope.qqq[t].SuggestedHours * $scope.rowFull.SalePrice).toFixed(2),
                                "LaborCost": ($scope.qqq[t].SuggestedHours * $scope.rowFull.Cost).toFixed(2),
                                "Cost": $scope.rowFull.Cost,
                                "SalePrice": $scope.rowFull.SalePrice
                            });
                        }
                    }

                    $('#labor').data('kendoGrid').setDataSource($scope.CalculationGridForLabor)
                    $scope.LaborTotals();
                }
            }

            //var sumOfMixRatio = 0;
            //for (var q = 0 ; q < $scope.qqq.length ; q++) {
            //    if ($scope.qqq[q].Mixratio > 0 && $scope.qqq[q].Mixratio)
            //        sumOfMixRatio = sumOfMixRatio + $scope.qqq[q].Mixratio;
            //}




        }


        $scope.LaborTotals = function () {
            $scope.laborQuotepriceTotal = 0.00;
            $scope.laborCostTotal = 0.00;
            for (var l = 0; l <= $scope.CalculationGridForLabor.length - 1; l++) {
                $scope.laborQuotepriceTotal = $scope.laborQuotepriceTotal + parseFloat($scope.CalculationGridForLabor[l].QuotePrice);
                $scope.laborCostTotal = $scope.laborCostTotal + parseFloat($scope.CalculationGridForLabor[l].LaborCost);
            }

            $scope.laborQuotepriceTotal = $scope.laborQuotepriceTotal;
            //  $scope.laborQuotepriceTotal = parseFloat($scope.laborQuotepriceTotal);
            $scope.laborCostTotal = $scope.laborCostTotal;
            // $scope.laborCostTotal = parseFloat($scope.laborCostTotal);

            $scope.LaborMarginPercentage = ($scope.laborQuotepriceTotal - $scope.laborCostTotal) / $scope.laborQuotepriceTotal * 100;
            $scope.LaborMarginPercentage = parseFloat($scope.LaborMarginPercentage).toFixed(2);

            $scope.labortotalQuotePrice = $scope.laborQuotepriceTotal.toFixed(2);
            $scope.labortotalCost = $scope.laborCostTotal.toFixed(2);
            $scope.laborprofitMargin = $scope.LaborMarginPercentage.toString();
            if ($scope.laborprofitMargin === "NaN") $scope.laborprofitMargin = "0";
            document.getElementById("totalLaborQuotePrice").innerHTML = $scope.labortotalQuotePrice.toString() + " " + "(" + $scope.laborprofitMargin.toString() + "%)";
            document.getElementById("totalLaborCost").innerHTML = $scope.labortotalCost.toString();
            //document.getElementById("profitLaborMargin").innerHTML = "(" + $scope.laborprofitMargin.toString() + "%)";


        }

        $scope.LabourQuoteHoursKeyup = function (dataItem) {

            var laborQuoteHour = $("#txtLaborQuoteHours").val();
            if (laborQuoteHour === '') laborQuoteHour = '0';
            if (laborQuoteHour.slice(-1) !== '.') {
                while (laborQuoteHour.charAt(0) === '0' && laborQuoteHour != '0') {
                    laborQuoteHour = laborQuoteHour.slice(1);
                    $("#txtLaborQuoteHours").val(laborQuoteHour)
                }
            }

            //  if (laborQuoteHour.slice(-1) !== '.') {
            var grid = $("#labor").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());

            // var indexxxx = grid.select().index();
            $scope.CalculationGridForLabor[grid.select().index()].QuoteHours = Number(laborQuoteHour);
            $scope.CalculationGridForLabor[grid.select().index()].QuotePrice = Number($scope.CalculationGridForLabor[grid.select().index()].SalePrice * laborQuoteHour).toFixed(2);
            $scope.CalculationGridForLabor[grid.select().index()].LaborCost = Number($scope.CalculationGridForLabor[grid.select().index()].Cost * laborQuoteHour).toFixed(2);
            if ($scope.CalculationGridForLabor[grid.select().index()].QuoteHours === 0) {
                selectedItem.QuoteHours = 0;
                $("#txtLaborQuoteHours").val(0)
            }
            else {
                selectedItem.QuoteHours = $scope.CalculationGridForLabor[grid.select().index()].QuoteHours;
                $("#txtLaborQuoteHours").val($scope.CalculationGridForLabor[grid.select().index()].QuoteHours)
            }
            selectedItem.QuotePrice = Number($scope.CalculationGridForLabor[grid.select().index()].QuotePrice).toFixed(2);
            selectedItem.LaborCost = Number($scope.CalculationGridForLabor[grid.select().index()].LaborCost).toFixed(2);
            $scope.LaborTotals();
            //  }

        }


        $scope.onCancel = function () {
            $window.location.href = "#!/quote/" + $scope.OpportunityId + "/" + $scope.quoteKey;
        };

        $scope.pushJobEstimator = function () {
            if ($scope.JobEstimator.Area != undefined && $scope.JobEstimator.Area != "") {
                var MaterialList = null;
                if ($('#materials').data()) {
                    MaterialList = $('#materials').data().kendoGrid.dataSource.data();
                    //if ($scope.BrandId == 2) {
                    //var rowEdit = $('#materials').find('.k-grid-edit-row');
                    //if (rowEdit.length) {
                    //    var validator = $scope.kendoValidator("materials");
                    //    if (!validator.validate() || !$scope.validateCurrentRecord()) {
                    //        return false;
                    //    }
                    //}
                    if (MaterialList.length == 0) {
                        HFC.DisplayAlert("Material Grid is empty.");
                        return;
                    }
                    $scope.JobEstimator.MaterialsList = MaterialList;
                    //}
                }
                var LaborList = null;
                if ($('#labor').data()) {
                    LaborList = $('#labor').data().kendoGrid.dataSource.data();
                    //if ($scope.BrandId == 2) {
                    //var rowEdit = $('#materials').find('.k-grid-edit-row');
                    //if (rowEdit.length) {
                    //    var validator = $scope.kendoValidator("materials");
                    //    if (!validator.validate() || !$scope.validateCurrentRecord()) {
                    //        return false;
                    //    }
                    //}
                    if (LaborList.length == 0) {
                        HFC.DisplayAlert("Labor Grid is empty.");
                        return;
                    }
                    $scope.JobEstimator.LaborsList = LaborList;
                    var dto = $scope.JobEstimator;
                    console.log(dto);
                    //}
                }
                $http.post('/api/Quotes/' + $scope.quoteKey + '/SaveJobEstimator', dto).then(function (response) {
                });
            }
        };

    }]);