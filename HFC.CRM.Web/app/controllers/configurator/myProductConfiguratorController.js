﻿'use strict';

app.controller('myProductConfiguratorController', [
    '$http', '$scope', '$window', '$location', '$rootScope', '$sce', '$routeParams', 'myVendorconfiguratorService', 'NavbarService', 'HFCService', 'calendarModalService', function ($http, $scope, $window, $location, $rootScope, $sce, $routeParams, myVendorconfiguratorService, NavbarService, HFCService, calendarModalService) {


        $scope.NavbarService = NavbarService;

        $scope.NavbarService.SelectSales();

        $scope.selectedModel = {};
        $scope.selectedProductGroup = {};
        $scope.selectedProduct = {};
        $scope.selectedVendor = {};
        $scope.calendarModalService = calendarModalService;

        $scope.Preload = false;
        $scope.ProductId = 0;
        $scope.QuoteKey = $routeParams.quoteId
        $scope.Quantity = 1;
        $scope.ProductDetails = {};
        $scope.OpportunityId = $rootScope.OpportunityId;
        $scope.quoteKey = $rootScope.quoteKey;
        $scope.PICGroupId = {};
        $scope.PICProductId = {};
        $scope.OneTimeProduct = false;
        $scope.calendarModalService.editQuoteFlag = undefined;

        $scope.UnitPrice = 0;
        $scope.Cost = 0;

        $scope.Measurements = {};

        $scope.brandid = HFCService.CurrentBrand;


        $http.get('/api/Quotes/' + $scope.QuoteKey + '/GetQuoteAndMeasurementDetails?poId=0').then(function (response) {
            //
            if (response.data != null) {
                $scope.QuoteData = response.data;
                var quotename = "Quote #" + $scope.QuoteData.QuoteID;
                HFCService.setHeaderTitle(quotename);
                if (response.data.Measurements != null) {
                    $scope.Measurements = JSON.parse(response.data.Measurements);
                    measurements(measurementDS(JSON.parse(response.data.Measurements),
                        $scope.QuoteData.QuoteLinesList));
                }

            }

        });

        $http.get('/api/quotes/0/getVendorNames').then(function (data) {

            if (data.data != null)
                $scope.allianceVendorArray = data.data;
            else
                $scope.allianceVendorArray = [];

            $("#autocomplete").kendoAutoComplete({
                dataSource: $scope.allianceVendorArray,
                // dataTextField: "name",
                noDataTemplate: false
            });


        });

        $scope.autocompleteClick = function () {
            var autocomplete = $("#autocomplete").data("kendoAutoComplete");
            autocomplete.search($scope.ProductDetails.Vendor);
        }


        function measurementDS(Measurements, measurementquotelinId) {

            var mds = [];
            if (Measurements !== '' && Measurements.length > 0) {


                for (var q = 0; q < Measurements.length; q++) {
                    var meme = Measurements[q];
                    var mesu = '';
                    meme.quoteLineIds = '';
                    if (measurementquotelinId != null && measurementquotelinId.length > 0) {
                        for (var i = 0; i < measurementquotelinId.length; i++) {
                            if (meme.id === measurementquotelinId[i].MeasurementsetId) {
                                if (mesu !== '') {
                                    mesu = mesu + ', ' + measurementquotelinId[i].QuoteLineNumber;
                                } else {
                                    mesu = measurementquotelinId[i].QuoteLineNumber;
                                }
                            }
                        }
                        meme.quoteLineIds = mesu;

                    }
                    mds.push(meme);
                }


            }
            return mds;
        }

        function measurements(Measurements) {
            $scope.viewMeasurements = {
                dataSource: {
                    data: Measurements,
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                RoomName: { type: "string", editable: false, visable: false },
                                MountTypeName: { type: "string", editable: false, visable: false },
                                Width: { type: "string" },
                                FranctionalValueWidth: { type: "string", editable: false, visable: false },
                                Height: { type: "string" },
                                FranctionalValueHeight: { type: "string", editable: false, visable: false }

                            }
                        }
                    }
                },
                cache: false,
                columns: [
                    {
                        field: "RoomName",
                        title: "Window Name",

                    },
                    {
                        field: "MountTypeName",
                        title: "Mount Type",
                        width: "120px",
                        hidden: false,
                        filterable: { multi: true, search: true },


                    },
                    {
                        field: "Width",
                        title: "Width",
                        template: "#=Width#  #=  (FranctionalValueWidth == null)? '' : FranctionalValueWidth # "

                    },
                    {
                        field: "Height",
                        title: "Height",
                        template: "#=Height#  #=  (FranctionalValueHeight == null)? '' : FranctionalValueHeight # "
                    },
                    {
                        field: "quoteLineIds",
                        title: "Quote Line ID",
                        //template: $scope.GetQuotelineId
                    }
                ],

                //resizable: true,
                noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                autoSync: true,
                //scrollable: {
                //    virtual: true
                //},
                //dataBound: $scope.onMeasurmentDataBound,
                //height: 300,
                toolbar: [
                    {
                        template: kendo.template(
                            $(
                                ' <script id="template" type="text/x-kendo-template"><div style="padding:5px;"><input ng-keyup="MeasurementSearch()" type="search" id="searchBoxmeasu" placeholder="Search any column" class="k-textbox leadsearch_tbox" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="MeasurementClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></script>')
                            .html())
                    }
                ],

            };
        }

        $http.get('/api/quotes/0/GetMyProductsOrServiceDiscount?productType=2').then(function (data) {
            var products = JSON.parse(data.data.data);

            $scope.ProductGroup = products;

            $scope.ConfigureMyProduct = {
                dataSource: {
                    data: products,
                    schema: {
                        model: {

                            id: "ProductKey",
                            fields: {
                                ProductKey: { editable: false },
                                ProductName: { editable: false },
                                Description: { editable: false, type: "string" },
                                ProductCategory: { editable: false },
                                ProductTypeId: { editable: false },
                                Cost: { editable: false },
                                UnitPrice: { editable: false, type: "number" },



                            }
                        }
                    }
                },
                columns: [
                    {
                        headerTemplate: "Select Product",
                        title: "Select Product",
                        selectable: true,                       
                        template: ""
                    },
                    {
                        field: "ProductKey", title: "Product ID",
                        template: function (dataItem) {
                            if (dataItem.ProductKey)
                                return "<span class='Grid_Textalign'>" + dataItem.ProductKey + "</span>";
                            else
                                return "";
                        }
                    },
                    { field: "ProductName", title: "Product Name" },
                    { field: "Description", title: "Description" },
                    { field: "ProductCategory", title: "Category" },
                    { field: "ProductSubCategory", title: "Sub Category" },
                    { field: "Vendor", title: "Vendor" },
                    {
                        field: "Cost", title: "Cost",
                        //format: "{0:c}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</span>";
                        }
                    },
                    {
                        field: "UnitPrice", title: "UnitPrice",
                        //format: "{0:c}",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.UnitPrice) + "</span>";
                        }
                    }
                ],

                //height: 300,
                sortable: true,
                persistSelection: true,
                filterable: true,
                //selectable: "multiple, row",
                scrollable: {
                    virtual: true
                },
                toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="tp-measurementsearch row no-margin"><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="ProductSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="ClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>').html()) }],



            };


        });
        $scope.AddAndConfigureProductList = {
            dataSource: {
                schema: {
                    model: {

                        id: "id",
                        fields: {
                            ProductKey: { editable: false },
                            ProductName: { editable: false },
                            Description: { editable: false, type: "string" },
                            ProductCategory: { editable: false },
                            ProductTypeId: { editable: false },
                            CostEdit: {
                                editable: true,
                                validation: {
                                    required: true
                                }
                            },
                            Quantity: {
                                editable: true, type: "number", defaultValue: 1,
                                validation: {
                                    required: true,
                                    min: 1
                                }
                            },
                            UnitPrice: { editable: false, type: "number" },
                            Measurement: { editable: true },
                            RoomName: { editable: false, type: "string" },
                            Mount: { editable: false, type: "string" },
                            Width: { editable: false, type: "string" },
                            Height: { editable: false, type: "string" }

                        }
                    }
                }
            },

            columns: [

                {
                    field: "ProductKey", title: "Product ID",
                    template: function (dataItem) {
                        if (dataItem.ProductKey)
                            return "<span class='Grid_Textalign'>" + dataItem.ProductKey + "</span>";
                        else
                            return "";
                    }
                },
                { field: "ProductName", title: "Product Name", width: "15%" },
                { field: "Description", title: "Description", width: "25%" },
                //{ field: "ProductCategory", title: "Category" },
                //{ field: "ProductSubCategory", title: "Sub Category" },
                {
                    field: "Quantity",
                    title: "QTY",
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Quantity, 'number') + "</span>";
                    },
                    editor: quantityEditor
                },
                {
                    field: "Measurement", title: "Window Name", editor: MeasurementDropdown, width: "10%", template: function (dataItem) {
                        var ret = ''
                        var mesu = $scope.Measurements;
                        if (dataItem.Measurement === 0) {
                            ret = '';
                        } else {
                            $.each(mesu, function (index, value) {
                                if (value.id === dataItem.Measurement) {
                                    ret = value.RoomName;
                                }
                            });
                        }
                        return ret;

                    }
                },
                {
                    field: "Mount", title: "Mount Type", template: function (dataItem) {
                        var ret = ''
                        var mesu = $scope.Measurements;
                        if (dataItem.Measurement === 0) {
                            ret = '';
                        } else {
                            $.each(mesu, function (index, value) {
                                if (value.id === dataItem.Measurement) {
                                    ret = value.MountTypeName;
                                }
                            });
                        }
                        return ret;

                    }
                },
                {
                    field: "Width", title: "Width", template: function (dataItem) {
                        var ret = ''
                        var mesu = $scope.Measurements;
                        if (dataItem.Measurement === 0) {
                            ret = '';
                        } else {
                            $.each(mesu, function (index, value) {
                                if (value.id === dataItem.Measurement) {
                                    ret = value.Width;
                                    if (value.FranctionalValueWidth != null) {
                                        ret = ret + ' ' + value.FranctionalValueWidth;
                                    }
                                }
                            });
                        }
                        return ret;

                    }
                },
                {
                    field: "Height", title: "Height", template: function (dataItem) {
                        var ret = ''
                        var mesu = $scope.Measurements;
                        if (dataItem.Measurement === 0) {
                            ret = '';
                        } else {
                            $.each(mesu, function (index, value) {
                                if (value.id === dataItem.Measurement) {
                                    ret = value.Height;
                                    if (value.FranctionalValueHeight != null) {
                                        ret = ret + ' ' + value.FranctionalValueHeight;
                                    }
                                }
                            });
                        }
                        return ret;

                    }
                },

                {
                    field: "CostEdit",
                    title: "Cost",
                    //format: "{0:c}",
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.CostEdit) + "</span>";
                    },
                    editor: '<input data-type="number" required id="Cost" class="k-textbox" name="Cost" data-bind="value:CostEdit"/>',
                },
                {
                    field: "UnitPrice", title: "UnitPrice",
                    //format: "{0:c}"
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.UnitPrice) + "</span>";
                    }
                },

            ],


            //sortable: true,
            editable: true,
            //selectable:true,
            //filterable: true,
            scrollable: {
                virtual: true
            }
        };

        //$("#oneTimeProductQuantity").kendoNumericTextBox(
        //    {
        //        format: "#",
        //        min: 1,
        //        max: 100,
        //        spinners: false,
        //        decimals: 0,style:"display:inline-block !important"
        //    });

        function MeasurementDropdown(container, options) {

            $('<input data-text-field="RoomName"  name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: false,
                        dataTextField: "RoomName",
                        dataValueField: "id",
                        valuePrimitive: true,
                        dataSource: {
                            data: $scope.Measurements,

                        },
                    });


        }

        function quantityEditor(container, options) {
            $('<input data-type="number" required id="Quantity" class="k-textbox" ng-keyup="onQuantityChange(dataItem)" name="Quantity"/>')
                .appendTo(container).kendoNumericTextBox({
                    type: "number",
                    format: "#",
                    decimals: 0,   // Settings for the editor, e.g. no decimals
                    step: 0,   // Defines how the up/down arrows change the value
                    min: 1,// You can also define min/max values
                    spinners: false
                });
        }

        $scope.AddProductList = function (e) {
            $scope.ProductList = e;
            var rowEdit = $('#ProductListGrid').find('.k-grid-edit-row');

            if (!rowEdit.length) {
                for (var i = 0; i < $scope.ProductList.length; i++) {
                    e[i].Quantity = 1;
                    e[i].RoomName = "";
                    e[i].Measurement = 0;
                    e[i].CostEdit = e[i].Cost;
                }
            }


            var grid = $("#ProductListGrid").data("kendoGrid");
            grid.dataSource.data([]);
            grid.dataSource.data().push.apply(grid.dataSource.data(), $scope.ProductList)
            if ($scope.brandid !== 1) {


                grid.hideColumn("Measurement");
                grid.hideColumn("Mount");
                grid.hideColumn("Width");
                grid.hideColumn("Height");
            } else {
                grid.hideColumn("ProductCategory");
                grid.hideColumn("ProductSubCategory");
            }


        }
       

        $scope.onQuantityChange = function (dataItem) {

            var quantityVal = $.isNumeric($("#Quantity").val());
            if (!quantityVal) {
                $("#Quantity").val('');
                $("#Quantity").focus();
            }
            else if ($("#Quantity").val() == '0') {
                $("#Quantity").val('');
                $("#Quantity").focus();
            }
        }
        $scope.onCostChange = function (dataItem) {
            var costval = $.isNumeric($("#Cost").val());
            if (!costval) {
                $("#Cost").val('');
            } else {
                var grdData = $('#ProductListGrid').data().kendoGrid.dataSource.data();

                for (var i = 0; i < grdData.length; i++) {
                    if (grdData[i].uid === dataItem.uid) {
                        grdData[i].set("Cost", $("#Cost").val());
                    }
                }

            }
        }
        $scope.tooltipOptions = {
            filter: "td,th",
            position: "top",
            hide: function (e) {
                this.content.parent().css('visibility', 'hidden');
            },
            show: function (e) {
                if (this.content.text().length > 1) {
                    if (this.content.text().trim() === "Edit QualifyAppointment Print") {
                        this.content.parent().css('visibility', 'hidden');
                    } else {
                        this.content.parent().css('visibility', 'visible');
                    }

                }
                else {
                    this.content.parent().css('visibility', 'hidden');
                }
            },
            content: function (e) {

                return e.target.context.textContent;
            }
        };

        $scope.onCancel = function () {

            window.location.href = '/#!/Editquote/' + $scope.OpportunityId + '/' + $scope.QuoteKey;

        };



        $scope.onChange = function (data) {
            if (data !== undefined) {

                $scope.ProductId = data.ProductKey;
                $scope.PICGroupId = data.PICGroupId;
                $scope.PICProductId = data.PICProductId;
                $scope.ProductDetails = data;
                $scope.OneTimeProduct = false;
                $scope.oneTimeProductCheck();
            }
        };


        //Kendo grid search CLear

        $scope.ClearSearch = function () {
            $('#searchBox').val('');
            $("#ProductGrid").data('kendoGrid').dataSource.filter({
            });
        }
        //Kendo Grid Search
        $scope.ProductSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#ProductGrid").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [

                     //{ field: "ProductKey", operator: "eq", value: searchValue },
                     { field: "ProductKey", operator: (value) => (value + "").indexOf(searchValue) >= 0, value: searchValue },
                      { field: "ProductCategory", operator: "contains", value: searchValue },
                       { field: "Description", operator: "contains", value: searchValue },
                        { field: "Vendor", operator: "contains", value: searchValue },


                      { field: "ProductName", operator: "contains", value: searchValue }
                ]
            });

        }



        $scope.OneTimeProductCheck = function () {
            $scope.ProductDetails.Vendor = '';
            $scope.ProductDetails.ProductName = '';
            $scope.ProductDetails.Description = '';

            $scope.ProductDetails.Cost = '';
            $scope.ProductDetails.UnitPrice = '';
            $("#oneTimeProductModel").modal("hide");
        }
        $scope.oneTimeProductCheck = function () {

            var promptsOuter = document.getElementById("promptsOuter");
            promptsOuter.style.display = "block";

            if ($scope.OneTimeProduct) {
                if (!$scope.Preload) {
                    $scope.ProductDetails.Vendor = '';
                    $scope.ProductDetails.ProductName = '';
                    $scope.ProductDetails.Description = '';

                    $scope.ProductId = 0;
                    $scope.PICGroupId = 0;
                    $scope.PICProductId = 0;
                    $scope.ProductDetails.Cost = '';
                    $scope.ProductDetails.UnitPrice = '';
                    if (!$scope.Preload) {
                        $scope.ProductId = 0;
                        $scope.PICGroupId = 0;
                        $scope.PICProductId = 0;
                        $scope.ProductDetails.Cost = '';
                        $scope.ProductDetails.UnitPrice = '';
                        $("#oneTimeProductModel").modal("show");

                    }
                    $scope.Preload = false;
                }
                $(".onetimeProduct").show();
                $(".prdDetails").prop("disabled", false);
                //$(".prdDetails").prop("disabled", false);

                var grid = $("#VendorGrid").data("kendoGrid");
                if (grid !== undefined) {
                    grid.clearSelection();
                }

            }
            else {
                $(".prdDetails").prop("disabled", true);
                $(".onetimeProduct").hide();


                //TP - 1723: CLONE - Don't require quantity for a service
                if (!$scope.Quantity) {
                    $scope.Quantity = 1;
                }

                if ($scope.ProductId === 0 || $scope.ProductId === undefined) {
                    promptsOuter.style.display = "none";
                }

            }
        }

         $('#oneTimeProductQuantity:input[type=number]').on('mousewheel', function (e) { $(this).blur(); });
        $scope.onOTPQuantityChange = function () {

            var quantityVal = $.isNumeric($("#oneTimeProductQuantity").val());
            if (!quantityVal) {
                $("#oneTimeProductQuantity").val('');
                $("#oneTimeProductQuantity").focus();
            }
            else if ($("#oneTimeProductQuantity").val() == "0") {
                $("#oneTimeProductQuantity").val('');
                $("#oneTimeProductQuantity").focus();
            }
        }

        $scope.SaveMyProductConfiguration = function () {
            var loadingElement = document.getElementById("loading");
            var grid = $("#ProductListGrid").data("kendoGrid");
            var grddata = grid.dataSource.data();

            var validator = $("#ProductListGrid").kendoValidator().data("kendoValidator")
            //var result = $('#ProductListGrid').data().kendoGrid.editable.validatable.validate();
            //<form id="OneTimeProductfrm" name="OneTimeProductsection">
            //var test = $scope.OneTimeProductfrm.$valid;
            var formValidate = $scope.OneTimeProductsection.$valid;

            var valid = true;

            if ($scope.OneTimeProduct) {
                if (!formValidate) {
                    valid = false;
                }
            }

            var ProductDetailsList = [];
            if (validator.validate()) {


                // Width
                // Height
                //FranctionalValueWidth
                //FranctionalValueHeight
                //MountType
                //RoomName
                //MeasurementsetId
                var mesu = $scope.Measurements;

                for (var i = 0; i < grddata.length; i++) {
                    var pDetails = {};
                    pDetails.ProductId = grddata[i].ProductKey;
                    pDetails.quantity = grddata[i].Quantity;
                    pDetails.PICGroupId = grddata[i].PICGroupId;
                    pDetails.PICProductId = grddata[i].PICProductId;
                    pDetails.ProductCategory = grddata[i].ProductCategory;
                    pDetails.Cost = grddata[i].CostEdit;

                    pDetails.ProductCategoryId = 99997;
                    pDetails.ProductSubCategoryId = grddata[i].ProductCategoryId;

                    pDetails.Description = grddata[i].Description;
                    pDetails.VendorId = grddata[i].VendorId;
                    pDetails.ProductTypeId = 2;
                    pDetails.Vendor = grddata[i].Vendor;

                    if (grddata[i].Measurement > 0 && mesu != null) {
                        for (var j = 0; j < mesu.length; j++) {
                            if (mesu[j].id == grddata[i].Measurement) {
                                pDetails.Width = mesu[j].Width;
                                pDetails.Height = mesu[j].Height;
                                pDetails.FranctionalValueWidth = mesu[j].FranctionalValueWidth;
                                pDetails.FranctionalValueHeight = mesu[j].FranctionalValueHeight;
                                pDetails.MountType = mesu[j].MountTypeName;
                                pDetails.RoomName = mesu[j].RoomName;
                                pDetails.MeasurementsetId = mesu[j].id;
                            }
                        }
                    }

                    ProductDetailsList.push(pDetails);
                }

                if ($scope.OneTimeProduct && formValidate) {
                    var ProductDetails = $scope.ProductDetails;
                    ProductDetails.ProductTypeId = 5;
                    ProductDetails.ProductCategory = $scope.ProductDetails.ProductName;
                    ProductDetails.VendorID = 0;
                    ProductDetails.PICGroupId = 1000000;
                    ProductDetails.PICProductId = 10000;



                    if ($scope.ProductDetails.Vendor === undefined || $scope.ProductDetails.Vendor === null) {
                        ProductDetails.Vendor = '';
                    } else {
                        ProductDetails.Vendor = $scope.ProductDetails.Vendor;
                    }
                    if ($scope.ProductDetails.Description === undefined || $scope.ProductDetails.Description === null) {
                        ProductDetails.Description = '';
                    } else {
                        ProductDetails.Description = $scope.ProductDetails.Description;
                    }
                    ProductDetailsList.push(ProductDetails);
                }
                if (validator.validate() && valid) {
                    loadingElement.style.display = "block";
                    $http.post('/api/Quotes/' + $scope.QuoteKey + '/SaveMyVendorConfigurationList', ProductDetailsList).then(function (response) {
                        var res = response.data;
                        if (res.error === "") {
                            HFC.DisplaySuccess("Quoteline saved sucessfully.");
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                            var url = '/#!/Editquote/' + $scope.OpportunityId + '/' + $scope.QuoteKey;
                            window.location.href = url;

                        } else {
                            HFC.DisplayAlert("Failed: " + res.error);
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                        }


                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
                }

            }


        }
    }]);