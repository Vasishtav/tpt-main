﻿'use strict';

app.controller('myServiceConfiguratorController', [
    '$http', '$scope', '$window', '$location', '$rootScope', '$sce', '$routeParams', 'myVendorconfiguratorService', 'NavbarService', 'calendarModalService', 'HFCService', function ($http, $scope, $window, $location, $rootScope, $sce, $routeParams, myVendorconfiguratorService, NavbarService, calendarModalService, HFCService) {

        $scope.HFCService = HFCService;
        $scope.NavbarService = NavbarService;

        $scope.NavbarService.SelectSales();

        $scope.selectedModel = {};
        $scope.selectedProductGroup = {};
        $scope.selectedProduct = {};
        $scope.selectedVendor = {};

        $scope.Preload = false;
        $scope.ProductId = 0;
        $scope.QuoteKey = $routeParams.quoteId
        $scope.Quantity = 1;
        $scope.ProductDetails = {};
        $scope.OpportunityId = $rootScope.OpportunityId;
        $scope.quoteKey = $rootScope.quoteKey;
        $scope.PICGroupId = {};
        $scope.PICProductId = {};
        $scope.OneTimeProduct = false;

        $scope.UnitPrice = 0;
        $scope.Cost = 0;

        $scope.calendarModalService = calendarModalService;
        $scope.calendarModalService.editQuoteFlag = undefined;

        HFCService.setHeaderTitle("Quote #" + $scope.QuoteKey);

        var initFlag = true;
        $http.get('/api/quotes/0/GetMyProductsOrServiceDiscount?productType=3').then(function (data) {
            var products = JSON.parse(data.data.data);
            
            $scope.ProductGroup = products;

            $scope. ConfigureMyProduct = {
                dataSource: {
                    data: products,
                    schema: {
                        model: {

                            id: "ProductKey",
                            fields: {
                                ProductKey: { editable: false },
                                ProductName: { editable: false },
                                Description: { editable: false, type: "string" },
                                ProductCategory: { editable: false },
                                ProductTypeId: { editable: false },
                                Cost: { editable: false },

                                UnitPrice: { editable: false, type: "number" },


                            }
                        }
                    }
                },
                dataBound: function (e) {
                    console.log(e);
                    //initial call
                    intializeMethod();
                    if (initFlag) {
                        initializeSelectAll();
                        initFlag = false;
                    }
                    $scope.selectDataList = e.sender._data;
                },
                columns: [
                    {
                        headerTemplate: "Select Service",
                        title: "Select Service",
                        selectable: true
                    },
                    {
                        field: "ProductKey", title: "Product ID",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.ProductKey + "</span>";
                        }
                    },
                    { field: "ProductTypeId", title: "Product Type",template:function(dataItem) {
                        if (dataItem.ProductTypeId == 3) {
                            return 'Service';
                        }
                        else if (dataItem.ProductTypeId == 5) {
                            return 'Discount';
                        }
                    } },
                    { field: "ProductName", title: "Product Name" },
                    { field: "Description", title: "Description" },
                    { field: "ProductCategory", title: "Category" },
                    {
                        field: "Cost", title: "Cost",
                        //format: "{0:c}"
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</span>";
                        }
                    },
                    {
                        field: "UnitPrice", title: "UnitPrice",
                        //format: "{0:c}"
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.UnitPrice) + "</span>";
                        }
                    }
                ],

                height: 300,
                sortable: true,
                persistSelection: true,
                filterable: true,
                //selectable: "multiple, row",
                scrollable: {
                    virtual: true
                },
                toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="tp-measurementsearch row no-margin"><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="ProductSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="ClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>').html()) }],



            };

            setTimeout(function () {
                $(".k-checkbox-label.k-no-text").click(function () {

                });
            }, 2000);

           
        });
        $scope.AddAndConfigureProductList = {
            dataSource: {
                schema: {
                    model: {

                        id: "id",
                        fields: {
                            ProductKey: { editable: false },
                            ProductName: { editable: false },
                            Description: { editable: false, type: "string" },
                            ProductCategory: { editable: false },
                            ProductTypeId: { editable: false },
                            Cost: { editable: false },
                            Quantity: {
                                editable: true,type: "number", defaultValue: 1,
                                validation: {
                                    required: true,
                                    min: 1
                                }
                            },
                            UnitPrice: { editable: false, type: "number" },


                        }
                    }
                }
            },
           
            columns: [
               
                //{ field: "ProductKey", title: "Product ID" },
                //{ field: "ProductTypeId", title: "Product Type",template:function(dataItem) {
                //    if (dataItem.ProductTypeId == 3) {
                //        return 'Service';
                //    }
                //    else if (dataItem.ProductTypeId == 5) {
                //        return 'Discount';
                //    }
                //} },
                //{ field: "ProductName", title: "Product Name" },
                //{ field: "Description", title: "Description" },
                //{ field: "ProductCategory", title: "Category" },
                ////{ field: "ProductSubCategory", title: "Sub Category" },
                ////{ field: "Vendor", title: "Vendor" },
                //{ field: "Cost", title: "Cost", format: "{0:c}" },
                //{ field: "UnitPrice", title: "UnitPrice", format: "{0:c}" }


                {
                    field: "ProductKey", title: "Product ID",
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + dataItem.ProductKey + "</span>";
                    }
                },
                {
                    field: "ProductTypeId", title: "Product Type", template: function (dataItem) {
                    if (dataItem.ProductTypeId == 3) {
                        return 'Service';
                    }
                    else if (dataItem.ProductTypeId == 5) {
                        return 'Discount';
                    }
                } },
                { field: "ProductName", title: "Product Name" },
                { field: "Description", title: "Description" },
                { field: "ProductCategory", title: "Category" },
                {
                    field: "CostEdit", title: "Cost",
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.CostEdit) + "</span>";
                    },
                    editor: '<input data-type="number" id="CostEdit" class="k-textbox" ng-keyup="onCostChange(dataItem)"  name="CostEdit"/>'
                },
                {
                    field: "Quantity",
                    title: "QTY",
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Quantity, 'number') + "</span>";
                    },
                    editor:quantityEditor
                },
                {
                    field: "UnitPrice", title: "UnitPrice",
                    //format: "{0:c}"
                    template: function (dataItem) {
                        return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.UnitPrice) + "</span>";
                    }
                }

            ],


            //sortable: true,
            editable: true,
            //selectable:true,
            //filterable: true,
            scrollable: {
                virtual: true
            },
            //toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="tp-measurementsearch"><input ng-keyup="VendorAndProductSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox leadsearch_tbox"><input type="button" id="btnReset" ng-click="ClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></script>').html()) }],



        };
        function quantityEditor(container, options) {
            if (options.model.ProductTypeId === 3) {
                $('<input data-type="number" required id="Quantity" class="k-textbox" ng-keyup="onQuantityChange(dataItem)" name="Quantity"/>')
                    .appendTo(container).kendoNumericTextBox({
                        type: "number",
                        format: "#",
                        decimals: 0,   // Settings for the editor, e.g. no decimals
                        step: 0,   // Defines how the up/down arrows change the value
                        min: 1,// You can also define min/max values
                        spinners: false
                    });;
            } else if (options.model.ProductTypeId === 4) {
                $('<input data-type="number" disabled id="Quantity" class="k-textbox" ng-keyup="onQuantityChange(dataItem)" name="Quantity"/>')
                    .appendTo(container).kendoNumericTextBox({
                        type: "number",
                        format: "#",
                        decimals: 0,   // Settings for the editor, e.g. no decimals
                        step: 0,   // Defines how the up/down arrows change the value
                        min: 1,// You can also define min/max values
                        spinners: false
                    });;
            }
        }

        $scope.entryFlag = true;
        var intializeMethod = function () {
                $("#ProductGrid .k-grid-content table tr td:first-child label").click(function (e) {
                    e.stopPropagation();
                    if ($scope.entryFlag) {
                        $scope.entryFlag = false;
                        $scope.AddProductList(e);
                       
                    }
                });
                if (!$scope.initialDataList) {
                    $scope.initialDataList = $("#ProductGrid").data('kendoGrid')._data;
                }
        }

        $scope.selectAllFlag = true;
        var initializeSelectAll = function () {
            console.log($("#ProductGrid .k-grid-header-wrap table tr th:first-child label"));
            $("#ProductGrid .k-grid-header-wrap table tr th:first-child label").click(function (e) {
                e.stopPropagation();
                console.log("prdlist", $scope.ProductList);
                console.log("selectDataList", $scope.selectDataList);
                if ($scope.searchfilter)//search and click select all at top
                {
                    var count = 0;
                    for (var i = 0; i < $scope.selectDataList.length; i++) {
                        for (var j = 0; j < $scope.ProductList.length; j++) {
                            if ($scope.ProductList[j].ProductKey == $scope.selectDataList[i].ProductKey) {
                                count++;
                            }
                        }
                    }
                    // deselect scenario
                    if (count == $scope.selectDataList.length) {
                        for (var i = 0; i < $scope.selectDataList.length; i++) {
                            $scope.AddProductList($scope.selectDataList[i], 'selectAllFiltered');
                        }
                    } else {    // select scenario
                        var flag;
                        for (var i = 0; i < $scope.selectDataList.length; i++) {
                            flag = false;
                            for (var j = 0; j < $scope.ProductList.length; j++) {
                                if ($scope.ProductList[j].ProductKey == $scope.selectDataList[i].ProductKey) {
                                    flag = true;
                                }
                            }
                            if (!flag) {
                                $scope.ProductList.push($scope.selectDataList[i]);
                            }
                        }
                        setGridDataValue();
                    }
                }
                else {
                    if ($scope.selectDataList && $scope.ProductList && $scope.ProductList.length == $scope.selectDataList.length) {
                        $scope.selectAllFlag = false;
                    } else {
                        $scope.selectAllFlag = true;
                    }
                    if ($scope.selectAllFlag) {
                        $scope.AddProductList(e, 'select');
                        $scope.selectAllFlag = false;
                    }
                    else {
                        $scope.AddProductList(e, 'deselect');
                        $scope.selectAllFlag = true;
                    }
                }
               
            });
        }
        
        

        $scope.ProductList = [];
        $scope.AddProductList = function (e, flag) {
            var selectedData;

            console.log($scope.ProductList)
            if (flag == 'select') {
                $scope.ProductList = $scope.selectDataList;
            } else if (flag == 'deselect') {
                $scope.ProductList = [];
            } else {
                if (flag != 'selectAllFiltered') {
                    var targetedElement = $($(e.target).parent()[0]).parent()[0];
                    var uid = targetedElement.dataset.uid;

                    for (var i = 0; i < $scope.initialDataList.length; i++) {
                        if (uid == $scope.initialDataList[i].uid) {
                            selectedData = $scope.initialDataList[i];
                        }
                    }
                } else {
                    selectedData = e;
                }

                var flag = false;
                var dataSet = $scope.ProductList;
                $scope.ProductList = [];
                for (var j = 0; j < dataSet.length; j++) {
                    if (selectedData.ProductKey == dataSet[j].ProductKey) {
                        flag = true;
                       
                    } else {
                        $scope.ProductList.push(dataSet[j]);
                    }
                }



               
                if (!flag) {
                    $scope.ProductList.push(selectedData);
                }
                if ($scope.selectDataList && $scope.ProductList && $scope.ProductList.length == $scope.selectDataList.length) {
                    $scope.selectAllFlag = false;
                } else {
                    $scope.selectAllFlag = true;
                }
            }

            
            setGridDataValue();
            
        }

        var setGridDataValue = function () {
            var rowEdit = $('#ProductListGrid').find('.k-grid-edit-row');

            if (!rowEdit.length) {
                for (var i = 0; i < $scope.ProductList.length; i++) {
                    $scope.ProductList[i].Quantity = 1;
                    $scope.ProductList[i].CostEdit = $scope.ProductList[i].Cost;
                }
            }


            var grid = $("#ProductListGrid").data("kendoGrid");
            grid.dataSource.data([]);
            grid.dataSource.data($scope.ProductList);
            $scope.$apply();
            setTimeout(function () {
                $scope.entryFlag = true;
            }, 100);
        }
       
        $scope.onQuantityChange = function (dataItem) {

            var quantityVal = $.isNumeric($("#Quantity").val());
            if (!quantityVal) {
                $("#Quantity").val('');
                $("#Quantity").focus();
            }
            else if($("#Quantity").val()=="0")
            {
                $("#Quantity").val('');
                $("#Quantity").focus();
            }
            
        }
        $scope.onCostChange = function (dataItem) {
            var costval = $.isNumeric($("#Cost").val());
            if (!costval) {
                $("#Cost").val('');
            } else {
                var grdData = $('#ProductListGrid').data().kendoGrid.dataSource.data();

                for (var i = 0; i < grdData.length; i++) {
                    if (grdData[i].uid === dataItem.uid) {
                        grdData[i].set("Cost", $("#Cost").val());
                    }
                }

            }
        }
        $scope.tooltipOptions = {
            filter: "td,th",
            position: "top",
            hide: function (e) {
                this.content.parent().css('visibility', 'hidden');
            },
            show: function (e) {
                if (this.content.text().length > 1) {
                    if (this.content.text().trim() === "Edit QualifyAppointment Print") {
                        this.content.parent().css('visibility', 'hidden');
                    } else {
                        this.content.parent().css('visibility', 'visible');
                    }

                }
                else {
                    this.content.parent().css('visibility', 'hidden');
                }
            },
            content: function (e) {

                return e.target.context.textContent;
            }
        };

        $scope.onCancel = function () {

            window.location.href = '/#!/Editquote/' + $scope.OpportunityId + '/' + $scope.QuoteKey;

        };



        $scope.onChange = function (data) {
            if (data !== undefined) {

                $scope.ProductId = data.ProductKey;
                $scope.PICGroupId = data.PICGroupId;
                $scope.PICProductId = data.PICProductId;
                $scope.ProductDetails = data;
                $scope.OneTimeProduct = false;
                $scope.oneTimeProductCheck();
            }
        };


        //Kendo grid search CLear

        $scope.ClearSearch = function () {
           
            $('#searchBox').val('');
            $("#ProductGrid").data('kendoGrid').dataSource.filter({
            });
            //  intializeMethod();
            $scope.searchfilter = false;

        }
        //Kendo Grid Search
        $scope.ProductSearch = function () {
            var searchValue = $('#searchBox').val();
            if (searchValue)
                $scope.searchfilter = true;
            else
                $scope.searchfilter = false;
            $("#ProductGrid").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [

                     { field: "ProductKey", operator: (value) => (value + "").indexOf(searchValue) >= 0, value: searchValue },
                      { field: "ProductCategory", operator: "contains", value: searchValue },
                       { field: "Description", operator: "contains", value: searchValue },
                        { field: "Vendor", operator: "contains", value: searchValue },
                        
                     
                      { field: "ProductName", operator: "contains", value: searchValue }
                ]
            });
            //setTimeout(function () {
            //    intializeMethod();
            //}, 600);
        }
      


        $scope.OneTimeProductCheck = function () {
            $scope.ProductDetails.Vendor = '';
            $scope.ProductDetails.ProductName = '';
            $scope.ProductDetails.Description = '';

            $scope.ProductDetails.Cost = '';
            $scope.ProductDetails.UnitPrice = '';
            $("#oneTimeProductModel").modal("hide");
        }
        $scope.oneTimeProductCheck = function () {

            var promptsOuter = document.getElementById("promptsOuter");
            promptsOuter.style.display = "block";

            if ($scope.OneTimeProduct) {
                if (!$scope.Preload) {
                    $scope.ProductDetails.Vendor = '';
                    $scope.ProductDetails.ProductName = '';
                    $scope.ProductDetails.Description = '';

                    $scope.ProductId = 0;
                    $scope.PICGroupId = 0;
                    $scope.PICProductId = 0;
                    $scope.ProductDetails.Cost = '';
                    $scope.ProductDetails.UnitPrice = '';
                    if (!$scope.Preload) {
                        $scope.ProductId = 0;
                        $scope.PICGroupId = 0;
                        $scope.PICProductId = 0;
                        $scope.ProductDetails.Cost = '';
                        $scope.ProductDetails.UnitPrice = '';
                        $("#oneTimeProductModel").modal("show");

                    }
                    $scope.Preload = false;
                }
                $(".onetimeProduct").show();
                $(".prdDetails").prop("disabled", false);
                //$(".prdDetails").prop("disabled", false);

                var grid = $("#VendorGrid").data("kendoGrid");
                grid.clearSelection();
            }
            else {
                $(".prdDetails").prop("disabled", true);
                $(".onetimeProduct").hide();


                //TP - 1723: CLONE - Don't require quantity for a service
                if (!$scope.Quantity) {
                    $scope.Quantity = 1;
                }

                if ($scope.ProductId === 0 || $scope.ProductId === undefined) {
                    promptsOuter.style.display = "none";
                }

            }
        }



        $scope.SaveMyProductConfiguration = function () {
            var loadingElement = document.getElementById("loading");
            var grid = $("#ProductListGrid").data("kendoGrid");
            var grddata = grid.dataSource.data();

            var validator = $("#ProductListGrid").kendoValidator().data("kendoValidator")
            //var result = $('#ProductListGrid').data().kendoGrid.editable.validatable.validate();
            //<form id="OneTimeProductfrm" name="OneTimeProductsection">
            //var test = $scope.OneTimeProductfrm.$valid;
           
            var ProductDetailsList = [];
            if (validator.validate()) {
                for (var i = 0; i < grddata.length; i++) {
                    var pDetails = {};                    
                    pDetails.ProductName = grddata[i].ProductName;
                    pDetails.ProductId = grddata[i].ProductKey;
                    pDetails.quantity = grddata[i].Quantity;
                    pDetails.PICGroupId = grddata[i].PICGroupId;
                    pDetails.PICProductId = grddata[i].PICProductId;
                    pDetails.ProductCategory = grddata[i].ProductCategory;
                    pDetails.Cost = grddata[i].CostEdit;
                    pDetails.UnitPrice = grddata[i].UnitPrice;
                    pDetails.ProductSubCategoryId = grddata[i].ProductCategoryId;

                    pDetails.Description = grddata[i].Description;
                    pDetails.VendorId = grddata[i].VendorId;
                    pDetails.ProductTypeId = 3;
                    ProductDetailsList.push(pDetails);
                }

              
                if (validator.validate()) {
                    loadingElement.style.display = "block";
                    $http.post('/api/Quotes/' + $scope.QuoteKey + '/SaveMyVendorConfigurationList', ProductDetailsList).then(function (response) {
                        var res = response.data;
                        if (res.error === "") {
                            HFC.DisplaySuccess("Quoteline saved sucessfully.");
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                            var url = '/#!/Editquote/' + $scope.OpportunityId + '/' + $scope.QuoteKey;
                            window.location.href = url;

                        } else {
                            HFC.DisplayAlert("Failed: " + res.error);
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                        }


                    });
                }
              
            }


        }
    }]);