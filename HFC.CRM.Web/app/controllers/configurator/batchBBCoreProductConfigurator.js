﻿
'use strict';
app.controller('batchBBCoreProductConfigurator',
	[
		'$http', '$scope', '$window', '$location', '$rootScope', '$sce', '$routeParams', 'NavbarService', 'HFCService', 'configuratorService', '$q', 'calendarModalService', 'ConfiguratorFeedbackService',
		function ($http, $scope, $window, $location, $rootScope, $sce, $routeParams, NavbarService, HFCService, configuratorService, $q, calendarModalService, ConfiguratorFeedbackService) {
			$scope.newconfig = true;

			$scope.QuoteLineId = 0;
			$scope.OpportunityId = $routeParams.OpportunityId;
			$scope.quoteKey = $routeParams.quoteKey;
			$scope.purchaseOrderId = $routeParams.purchaseOrderId;

			$scope.Quantity = 1;
			$scope.ReturntoQuote = false;
			$scope.mpoHeader = false;
			$scope.calendarModalService = calendarModalService;
			$scope.calendarModalService.editQuoteFlag = undefined;
			$scope.NavbarService = NavbarService;
			$scope.selectionchangeflag = true;

			$scope.configuratorService = configuratorService;

			$scope.NavbarService.SelectSales();
			$scope.NavbarService.EnableSalesOpportunitiesTab();
			$scope.Measurements = {};
			$scope.ConfiguratorFeedbackService = ConfiguratorFeedbackService;

			$rootScope.OpportunityId = $scope.OpportunityId;
			$rootScope.quoteKey = $scope.quoteKey;
			$scope.heightPrompt = 0;
			$scope.widthPrompt = 0;
			$scope.mountPrompt = 0;
			$scope.roomPrompt = 0;
			$scope.colorPrompt = 0;
			$scope.fabricPrompt = 0;

			$scope.measurementChanged = true;
			$scope.SelectedMeasurements = null;

			$scope.EditconfigFromMPO = false;

			if (window.location.href.indexOf('xVPOCoreProductconfigurator') > 0)
				$scope.returnToMpoOrxVpo = "Return to xVPO";
			else
				$scope.returnToMpoOrxVpo = "Return to Quote Document";

			if (window.location.href.indexOf('MPOCoreProductconfigurator') > 0 || window.location.href.indexOf('xVPOCoreProductconfigurator') > 0) {
				$scope.EditconfigFromMPO = true;

				$http.get('/api/PurchaseOrders/' + $scope.purchaseOrderId + '/GetMPOHeader').then(function (response) {

					if (response.data != null) {
						$scope.mpoHeader = response.data.data;
					}
				});
			}

			$http.get('/api/Quotes/' + $scope.quoteKey + '/GetQuoteAndMeasurementDetails?poId=' + $scope.purchaseOrderId).then(function (response) {
				if (response.data != null) {
					$scope.QuoteData = response.data;
					if ($scope.QuoteData.QuoteLinesVM != null) {
						for (var i = 0; i < $scope.QuoteData.QuoteLinesVM.length; i++) {
							if ($scope.EditconfigFromMPO) {
								$scope.QuoteData.QuoteLinesVM[i].EditconfigFromMPO = true;
							} else {
								$scope.QuoteData.QuoteLinesVM[i].EditconfigFromMPO = false;
							}
						}
					}

					var quotename = "Quote #" + $scope.QuoteData.QuoteID + " - " + $scope.QuoteData.QuoteName

					HFCService.setHeaderTitle(quotename);
					if (response.data.Measurements != null) {
						$scope.Measurements = response.data.Measurements;

						measurements(measurementDS(JSON.parse(response.data.Measurements),
							$scope.QuoteData.QuoteLinesVM));
					}
					getQuotelines();
				}
			});
			//}

			///Quote Grid
			function getQuotelines() {
				$scope.quoteLinesOptions = {
					dataSource: {
						data: $scope.QuoteData.QuoteLinesVM,
						schema: {
							model: {
								id: "QuoteLineId",
								fields: {
									QuoteLineId: { editable: false },
									Quantity: { editable: false },
									RoomName: { editable: false },
									ProductName: { editable: false },
									Width: { editable: false },
									Height: { editable: false },
									MountType: { editable: false },
									Color: { editable: false },
									Fabric: { editable: false },
									ExtendedPrice: { editable: false },
									InternalNotes: { editable: true },
								}
							}
						}
					},

					columns: [
						{ field: "QuoteLineId", hidden: true },
						{ field: "QuoteKey", hidden: true },
						{ field: "MeasurementsetId", hidden: true },
						{ field: "MeasurementId", hidden: true },
						{ field: "VendorId", hidden: true },
						{ field: "ProductId", hidden: true },
						{ field: "Memo", hidden: true },
						{ field: "PICJson", hidden: true },
						{ field: "ProductTypeId", hidden: true },
						{
							field: "QuoteLineId", title: "ID", template: function (dataItem, e) {

								if (dataItem.ConfigErrors !== '' && dataItem.EditconfigFromMPO) {
									var str = '<label class="configure_tooltip" data-tooltip="' + dataItem.ConfigErrors.replace("<ul>", "").replace("</ul>", "").replace("<li>", "").replace("</li>", "") + '" class="ng-binding ldet_label error ng-scope" style="text-align:center;"><i class="fa fa-exclamation-triangle" style="color:red;"></i></label>';
									return str + '<label>' + dataItem.QuoteLineNumber + '</label>';
								}
								else {
									return '<span class="Grid_Textalign"><label>' + dataItem.QuoteLineNumber + '</label></span>';
								}
							},

							width: "50px"
						},
						{
							field: "Quantity", title: "Qty", width: "40px",
							template: function (dataItem) {
								return '<span class="Grid_Textalign">' + HFCService.NumberFormat(dataItem.Quantity, 'number') + '</span>';
							}
						},
						{ field: "RoomName", title: "Window Name", width: "150px" },
						{
							field: "ProductName",
							title: "Product Name/Model",
							filterable: false,
							template: function (dataItem) {
								var str = dataItem.ProductName;

								if (dataItem.ModelDescription != null) {
									str = str + ' - ' + dataItem.ModelDescription;
								}
								return str;
							},
							width: "200px"
						},
						{
							field: "Width",
							title: "Width",
							width: "60px",
							template: "#= Width # #= FranctionalValueWidth #",
							filterable: false
						},
						{
							field: "Height",
							title: "Height",
							filterable: false,
							width: "60px",
							template: "#= Height # #= FranctionalValueHeight #"
						},
						{ field: "MountType", title: "Mount", width: "60px" },
						{ field: "Color", title: "Color", width: "100px" },
						{ field: "Fabric", title: "Fabric", width: "100px" },
						{
							field: "ExtendedPrice", title: "Extended Total", width: "100px",
							//format: "{0:c}"
							template: function (dataItem) {
								return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
							}
						},
						{
							field: "InternalNotes", title: "Internal Notes", width: "150px",
							editor: '<input id="InternalNotesTxt" name="InternalNotesTxt" data-bind="value:InternalNotes" class="k-textbox" />'
						},
						{ command: [{ name: "edit", text: "Edit Comment" }, { name: "fake", text: "Edit Line Config", click: LoadQuoteLine }], title: "&nbsp;", width: "120px", }

					],
					editable: "inline",
					resize: true,
					change: function (e) {
						var parent = $("#prdDownloadlistrequired").children();
						var elementWrapper = $(parent).children();
						var element = $(elementWrapper[0]).children()[0];
						$(element).removeClass("prdrequired");
						$rootScope.selectedquoteLine = null;
						var parent = $("#modelDownloadlistrequired").children();
						var elementWrapper = $(parent).children();
						var element = $(elementWrapper[0]).children()[0];
						$(element).removeClass("modelrequired");
						$("#gridEditMeasurements").data("kendoGrid").refresh();
						var items = e.sender.select();
						var grid = e.sender;
						var mod = grid.dataItem(this.select());

						if ($scope.selectedquoteLinerow != undefined && $scope.selectedquoteLinerow !== null && $scope.selectedquoteLinerow.uid === mod.uid) {
							$scope.selectedquoteLinerow = null;
							$scope.SelectedMeasurements = null;
							$scope.SelectedPICProduct = '';
							$scope.selectedModel = '';

							$("#VendorGrid").data("kendoGrid").refresh();
							$("#gridEditMeasurements").data("kendoGrid").refresh();

							$("#ModelGrpError").hide();
							$("#picProductddl").hide();
							$("#picModelGroup").hide();
							$("#promptsOuter").hide();

							$("#QuotelinesGrid").data("kendoGrid").refresh();
							$scope.Quantity = mod.Quantity;
							e.preventDefault();
							if ($scope.selectedElement) {
								$scope.selectedElement = null;
								$scope.Tempuid = null;
							}
							if ($scope.selectedMeasurementElement) {
								$scope.selectedMeasurementElement = null;
								$scope.measurementUid = null;
							}
							// nullify the filter
							$scope.ClearSearch();
							$scope.MeasurementClearSearch();
						} else {
							//if(mode.)
							if ((mod === null || mod === undefined) || (mod != null && mod !== undefined && mod.ConfigErrors === '' && mod.EditconfigFromMPO)) {
								items.each(function (i, e) {
									//console.log(i);
									//if (mod.QuoteLineId===)
									//if (mod.canSelect === false) {
									$(e).removeClass("k-state-selected");
									//}
								});
								$("#ModelGrpError").hide();
								$("#picProductddl").hide();
								$("#picModelGroup").hide();
								$("#promptsOuter").hide();
								$("#QuotelinesGrid").data("kendoGrid").refresh();
							} else if (mod != null && mod !== undefined) {
								$scope.selectedquoteLinerow = mod;
								$rootScope.selectedquoteLine = { QuoteLineNumber: mod.QuoteLineNumber, RoomName: mod.RoomName };
								if (mod !== null && mod !== undefined) {

									var loadingElement = document.getElementById("loading");
									loadingElement.style.display = "block";
									var re = JSON.parse(mod.PICJson);
									var grdData = $("#VendorGrid").data("kendoGrid").dataSource.data();
									var validProductModel = false;
									for (var i = 0; i < grdData.length; i++) {
										if (grdData[i].VendorId == re.VendorId && grdData[i].PICProductGroupId == re.GGroup) {
											if (grdData[i].PICProductList != null && grdData[i].PICProductList.length > 0) {
												for (var j = 0; j < grdData[i].PICProductList.length; j++) {
													if (grdData[i].PICProductList[j].PICProductId == re.PicProduct) {
														for (var k = 0; k < grdData[i].PICProductList[j].models.length; k++) {
															if (grdData[i].PICProductList[j].models[k].Model == re.ModelId) {
																validProductModel = true;
															}
														}
													}
												}
											}
										}
									}

									if (validProductModel) {
										$scope.QuoteLineId = mod.QuoteLineId;
										$scope.PrdPromptAnswers = JSON.parse(re.PromptAnswers);
										$scope.selectedModel = re.ModelId;
										// nullify the filter
										$scope.ClearSearch();
										$scope.MeasurementClearSearch();
										$scope.OnSelectedProductFromGrid(mod);
										var result = $scope.configuratorService.GetDataSourceAndPromptAnswers(re.PicProduct, re.ModelId, $scope.GetDatsourceCallbackFromGrid);

									} else {
										var msg = "Line " + mod.QuoteLineNumber + " contains an invalid product configuration please re-configure line " + mod.QuoteLineNumber + "  with a valid product-model. "
										alert(msg);
                    $("#ModelGrpError").hide();
										$("#picProductddl").hide();
										$("#picModelGroup").hide();
										$("#promptsOuter").hide();
										loadingElement.style.display = "none";
									}
								}
							} else {
								$scope.selectedquoteLinerow = null;
								$rootScope.selectedquoteLine = null;
								$scope.SelectedMeasurements = null;
								$scope.SelectedPICProduct = '';
								$scope.selectedModel = '';

								$("#VendorGrid").data("kendoGrid").refresh();
								$("#gridEditMeasurements").data("kendoGrid").refresh();

								$("#ModelGrpError").hide();
								$("#picProductddl").hide();
								$("#picModelGroup").hide();
								$("#promptsOuter").hide();
								$("#QuotelinesGrid").data("kendoGrid").refresh();
								e.preventDefault();
							}
							e.preventDefault();
						}

						var grd = $("#gridEditMeasurements").data("kendoGrid");
						if (grd !== null && grd !== undefined) {
							grd.refresh();
						}

					},
					edit: function (e) {
						$scope.EditGridKendo(e);
						$(e.container).parent().find(".k-grid-cancel").after('<a role="button" class="k-button k-button-icontext k-grid-fake" href="#">Edit Line Config</a>');
					},
					noRecords: true,
					resizable: true,
					autoSync: true,
					selectable: "row",
					refresh: true,
					cancel: function (e) {
						$('#QuotelinesGrid').data('kendoGrid').setDataSource($scope.QuoteData.QuoteLinesVM);
					},
					messages: {
						noRecords: "No records found"
					},
				};
				var panelBar = $("#panelbar").data("kendoPanelBar");
				if (panelBar !== undefined && panelBar !== null) {
					panelBar.expand($("#ConfigPanel"));
					panelBar.expand($("#quotelistPanel"), false);
				}
			}

			function LoadQuoteLine(e) {
				e.preventDefault();
				var row = $(e.currentTarget).closest("tr");
				$('#QuotelinesGrid').data('kendoGrid').select(row);
				$('.k-grid-fake').blur();
			}

			/// On select row of Quote Kendo Grid
			$scope.OnSelectedProductFromGrid = function (item) {
				var promptsOuter1 = document.getElementById("promptsOuter");
				promptsOuter1.style.display = "none";
				document.getElementById("response").innerHTML = '';
				$("#response").css('border', 'none');
				var prodlist = $scope.Product;
				var prduid;
				var prodt;

				var data = JSON.parse(item.PICJson);
				var productList;

				var venGrdList = $("#VendorGrid").data("kendoGrid").dataSource.data();
				$("#ModelGrpError").hide();
				for (var i = 0; i < venGrdList.length; i++) {
					if (venGrdList[i].PICProductGroupId === data.GGroup && venGrdList[i].VendorId === data.VendorId) {
						$scope.selectedProduct = venGrdList[i].ProductId;
						prduid = venGrdList[i].uid;
						prodt = venGrdList[i];
						productList = venGrdList[i].PICProductList;
						for (var j = 0; j < productList.length; j++) {
							if (productList[j].PICProductId == data.PicProduct) {
								$scope.heightPrompt = productList[j].HeightPrompt;
								$scope.widthPrompt = productList[j].WidthPrompt;
								$scope.mountPrompt = productList[j].MountPrompt;
								$scope.roomPrompt = productList[j].RoomPrompt;
								$scope.colorPrompt = productList[j].ColorPrompt;
								$scope.fabricPrompt = productList[j].FabricPrompt;

							}
						}

						if (venGrdList[i].WarningPhasingOut !== '' && venGrdList[i].WarningPhasingOut !== undefined && venGrdList[i].WarningPhasingOut !== null) {
							var loadingElement = document.getElementById("modelWarning");
							loadingElement.innerHTML = venGrdList[i].WarningPhasingOut;
							$("#ModelGrpError").show();
						}
						break;
					}
				}
				if (prduid !== undefined && prduid !== null) {
					$("#VendorGrid").data('kendoGrid').refresh();
					$('[data-uid=' + prduid + ']').addClass('k-state-selected');
					//$scope.uid = prduid;
					var selectedRow = $("#VendorGrid .k-state-selected");
					if (selectedRow && selectedRow.length != 0) {
						//console.log($scope);
						//$scope.selectedElement = [];

						$scope.selectedElement = selectedRow;
						$scope.uid = $($scope.selectedElement[0]).attr("data-uid");
						$scope.Tempuid = $($scope.selectedElement[0]).attr("data-uid");
					}
				}

				var mesuList = $("#gridEditMeasurements").data("kendoGrid").dataSource.data();
				var mesuUID;
				for (var i = 0; i < mesuList.length; i++) {
					if (mesuList[i].id === item.MeasurementsetId) {
						mesuUID = mesuList[i].uid;
						$scope.SelectedMeasurements = mesuList[i];
					}
				}
				if (mesuUID !== undefined && mesuUID !== null) {
					$("#gridEditMeasurements").data('kendoGrid').refresh();
					$('[data-uid=' + mesuUID + ']').addClass('k-state-selected');
					$scope.measurementUid = mesuUID;
					$scope.selectedMeasurementElement = $('[data-uid=' + mesuUID + ']');
				}

				//feedbackconfigurator
				$scope.Vendor = prodt.VendorName;
				$scope.ProductCategory = prodt.PICProductGroupName;
				$scope.VendorId = prodt.VendorId;
				$scope.PICProductGroupId = prodt.PICProductGroupId;
				$scope.PICVendor = prodt.PICVendorId;
				$scope.ProductPICJson = productList;
				$scope.PICJson = JSON.parse(item.PICJson);
				$scope.FieldErrors = "";

				$scope.selectedProductGroup = prodt.PICProductGroupId;
				$scope.SelectedPICVendor = prodt.PICVendorId;
				$scope.selectedVendor = prodt.VendorId;

				$scope.SelectedPICProduct = data.PicProduct;
				$scope.Product = null;
				$scope.Product = productList;
				$("#picProductddl").show();
				var prodId = $scope.SelectedPICProduct;

				var dataSource = new kendo.data.DataSource({
					data: productList
				});
				var prddropdownlist = $("#prdDownloadlist").data("kendoDropDownList");
				prddropdownlist.setDataSource(dataSource);
				prddropdownlist.value($scope.SelectedPICProduct);
				if ($scope.EditconfigFromMPO) {
					prddropdownlist.enable(false);
				}

				$scope.selectedModel = data.ModelId;
				//var loadingElement = document.getElementById("loading");
				//loadingElement.style.display = "block";

				if (prodId !== null && prodId != undefined && Number(prodId)) {


					var promptsOuter1 = document.getElementById("promptsOuter");
					promptsOuter1.style.display = "none";
					document.getElementById("response").innerHTML = '';
					$("#response").css('border', 'none');
					var prodlist = $scope.Product;
					//if ($scope.selectedquoteLinerow !== null && $scope.selectedquoteLinerow !== undefined) {
					//    $("#gridEditMeasurements").data('kendoGrid').refresh();
					//    $("#QuotelinesGrid").data('kendoGrid').refresh();
					//}

					$("#ModelGrpError").hide();
					for (var i = 0; i < prodlist.length; i++) {
						if (prodlist[i].PICProductId == prodId) {
							$scope.selectedProduct = prodlist[i].ProductId;
							var dupes = {};
							var singles = [];
							$.each(prodlist[i].models, function (i, el) {
								if (!dupes[el.Model]) {
									dupes[el.Model] = true;
									singles.push(el);
								}
							});
							//productList = singles;
							$scope.ModelGroup = singles;


							$scope.heightPrompt = prodlist[i].HeightPrompt;
							$scope.widthPrompt = prodlist[i].WidthPrompt;
							$scope.mountPrompt = prodlist[i].MountPrompt;
							$scope.roomPrompt = prodlist[i].RoomPrompt;
							$scope.colorPrompt = prodlist[i].ColorPrompt;
							$scope.fabricPrompt = prodlist[i].FabricPrompt;


							if (prodlist[i].WarningPhasingOut !== '') {
								var loadingElement = document.getElementById("modelWarning");
								loadingElement.innerHTML = prodlist[i].WarningPhasingOut;
								$("#ModelGrpError").show();
							}
							break;
						}
					}
					$scope.selectedModel = data.ModelId;;
					//    $("#ModelGrpError").hide();
					//$("#picModelGroup").hide();
					$scope.newconfig = true;
					var prodId = $scope.SelectedPICProduct;
					var loadingElement = document.getElementById("loading");
					loadingElement.style.display = "block";


					$("#picModelGroup").show();
					var dataSource = new kendo.data.DataSource({
						data: $scope.ModelGroup
					});
					var dropdownlist = $("#modelDownloadlist").data("kendoDropDownList");
					dropdownlist.setDataSource(dataSource);
					dropdownlist.value(data.ModelId);

					//$http.get('/api/PIC/0/GetModels?productId=' + prodId).then(function (modRes) {
					//$("#picModelGroup").hide();
					//var PICModels = modRes.data;
					//if (PICModels.error === '') {
					//	$("#picModelGroup").show();

					//	var dataSource = new kendo.data.DataSource({
					//		data: $scope.ModelGroup
					//	});
					//	var dropdownlist = $("#modelDownloadlist").data("kendoDropDownList");
					//	dropdownlist.setDataSource(dataSource);


					//	$scope.ModelGroup = jQuery.grep(PICModels.Data, function (value) {
					//		return value.IsActive === true;
					//	});

					//	var dataSource = new kendo.data.DataSource({
					//		data: $scope.ModelGroup
					//	});
					//	var dropdownlist = $("#modelDownloadlist").data("kendoDropDownList");
					//	dropdownlist.setDataSource(dataSource);
					//	dropdownlist.value($scope.selectedModel);
					//	if ($scope.EditconfigFromMPO) {
					//		dropdownlist.enable(false);
					//	}
					//	//loadingElement.style.display = "none";
					//	//}
					//}
					//else {
					//	HFC.DisplayAlert(PICModels.error);
					//	//loadingElement.style.display = "none";
					//}
					//$scope.selectedModel = data.ModelId;
					//$timeout(function () {
					//$scope.$apply();
					//},3000);
					//$scope.$apply();
					//loadingElement.style.display = "none";
					//}).catch(function (error) {
					//	//loadingElement.style.display = "none";
					//	HFC.DisplayAlert("Unknown error occurred. Please contact site Administrator.253");
					//});
				}
				else {
					//loadingElement.style.display = "none";
				}
			};
			$scope.saveFunction = function (e) {
				angular.forEach($scope.QuoteData.QuoteLinesVM, function (value, key) {
					if (value.QuoteLineId === e.model.QuoteLineId) {
						if (value.InternalNotes !== $("#InternalNotesTxt").val()) {
							value.InternalNotes = $("#InternalNotesTxt").val();

							var valuee = $("#InternalNotesTxt").val();
							$http.post('/api/Measurement/' + value.QuoteLineId + '/saveInternalNotesFromConfigurator?valuee=' + valuee).then(function (res) {
								if (res.data === true) {
									$('#QuotelinesGrid').data('kendoGrid').dataSource.data = $scope.QuoteData.QuoteLinesVM;
								}
							});
						} else {
							$('#QuotelinesGrid').data('kendoGrid').dataSource.data = $scope.QuoteData.QuoteLinesVM;
						}
					}
				});
			}

			$scope.cancelEditGrid = function (e) {
				$('#QuotelinesGrid').data('kendoGrid').dataSource.data = $scope.QuoteLinesVM1;
			}

			$scope.EditGridKendo = function (e) {
				//    $("#InternalNotesTxt").val(e.model.InternalNotes);
				$scope.QuoteLinesVM1 = $scope.QuoteData.QuoteLinesVM;
			}

			function measurementDS(Measurements, measurementquotelinId) {
				var mds = [];
				if (Measurements !== '' && Measurements.length > 0) {
					for (var q = 0; q < Measurements.length; q++) {
						var meme = Measurements[q];
						var mesu = '';
						meme.quoteLineIds = '';
						//if (meme.id !== 0) {
						if (measurementquotelinId != null && measurementquotelinId.length > 0) {
							for (var i = 0; i < measurementquotelinId.length; i++) {
								if (
									Number(meme.Height) === measurementquotelinId[i].Height && meme.FranctionalValueHeight === measurementquotelinId[i].FranctionalValueHeight &&
									Number(meme.Width) === measurementquotelinId[i].Width && meme.FranctionalValueWidth === measurementquotelinId[i].FranctionalValueWidth &&
									meme.MountTypeName === measurementquotelinId[i].MountType &&
									meme.RoomName === measurementquotelinId[i].RoomName
								) {

									if (measurementquotelinId[i].MeasurementsetId != null && measurementquotelinId[i].MeasurementsetId !== 0 && measurementquotelinId[i].MeasurementsetId !== '') {
										if (measurementquotelinId[i].MeasurementsetId == meme.id) {
											if (mesu !== '') {
												mesu = mesu + ', ' + measurementquotelinId[i].QuoteLineNumber;
											} else {
												mesu = measurementquotelinId[i].QuoteLineNumber;
											}
										}
									} else {
										if (mesu !== '') {
											mesu = mesu + ', ' + measurementquotelinId[i].QuoteLineNumber;
										} else {
											mesu = measurementquotelinId[i].QuoteLineNumber;
										}
									}




								}
								//}
								//meme.quoteLineIds = mesu;
							}
							meme.quoteLineIds = mesu;
						}
						//else
						//	meme.quoteLineIds = meme.QuoteLineNumber;

						mds.push(meme);
					}
				}
				return mds;
			}

			function measurements(Measurements) {
				$scope.viewMeasurements = {
					dataSource: {
						data: Measurements,
						schema: {
							model: {
								id: "id",
								fields: {
									RoomName: { type: "string", editable: false, visable: false },
									MountTypeName: { type: "string", editable: false, visable: false },
									Width: { type: "string" },
									FranctionalValueWidth: { type: "string", editable: false, visable: false },
									Height: { type: "string" },
									FranctionalValueHeight: { type: "string", editable: false, visable: false },
									Comments: { type: "string" },
								}
							}
						}
					},
					cache: false,
					//height: 300,
					// scrollable: {
					// virtual: true
					// },
					columns: [
						{
							field: "RoomName",
							title: "Window Name",
							template: function (dataItem) {
								if (dataItem.isAdded == false)
									return dataItem.RoomName + "*";
								else
									return dataItem.RoomName;
							},
							width: "160px",
						},
						{
							field: "MountTypeName",
							title: "Mount",
							width: "50 px",
							hidden: false,
							filterable: { multi: true, search: true },
						},
						{
							field: "Width",
							title: "Width",
							width: "80px",
							template: "#=Width#  #=  (FranctionalValueWidth == null)? '' : FranctionalValueWidth # "
						},
						{
							field: "Height",
							title: "Height",
							width: "80px",
							template: "#=Height#  #=  (FranctionalValueHeight == null)? '' : FranctionalValueHeight # "
						},
						{
							field: "quoteLineIds",
							title: "Line IDs",
							width: "100px",
						},
						{
							field: "Comments",
							title: "Comments",
							// width: "250px",
						},
					],

					//resizable: true,
					noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
					autoSync: true,
					// scrollable: {
					// //virtual: true
					// },
					selectable: "row",
					//change:function(data) {
					//    var selectedRow = $("#gridEditMeasurements .k-state-selected");
					//    if (selectedRow && selectedRow.length != 0) {
					//        $scope.selectedMeasurementElement = selectedRow;
					//        $scope.measurementUid = $($scope.selectedMeasurementElement[0]).attr("data-uid");
					//    }
					//    $scope.onMeasurementChangeGrid(data);
					//},
					change: $scope.onMeasurementChangeGrid,
					dataBound: $scope.onMeasurmentDataBound,
					//height: 300,
					toolbar: [
						{
							template: kendo.template(
								$(
									' <script id="template" type="text/x-kendo-template"><div style="padding:5px;"><input ng-keyup="MeasurementSearch()" type="search" id="searchBoxmeasu" placeholder="Search any column" class="k-textbox leadsearch_tbox" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="MeasurementClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></script>')
									.html())
						}
					],
				};
			}

			$scope.GetQuotelineId = function (data) {
				var mesu = '';
				var measurementquotelinId = $scope.QuoteData.QuoteLinesVM;
				if (measurementquotelinId != null && measurementquotelinId.length > 0) {
					for (var i = 0; i < measurementquotelinId.length; i++) {
						if (data.id === measurementquotelinId[i].MeasurementsetId) {
							if (mesu !== '') {
								mesu = mesu + ', ' + measurementquotelinId[i].QuoteLineNumber;
							} else {
								mesu = measurementquotelinId[i].QuoteLineNumber;
							}
						}
					}
				}
				return mesu;
			}
			$scope.ProductGroup = {
				dataSource: {
					transport: {
						read: {
							url: '/api/PIC/0/GetPICProductGroup',
						},
					},
					error: function (e) {
						HFC.DisplayAlert(e.errorThrown);
					},
					schema: {
						model: {
							id: "PICProductGroupId",
							fields: {
								PICVendorId: { type: "string", editable: false, visable: false },
								VendorId: { type: "number", editable: false, visable: false },
								VendorName: { type: "string" },
								PICProductGroupId: { type: "number", editable: false, visable: false },
								PICProductGroupName: { type: "string" },
								PICProductList: { editable: false, visable: false }
							}
						}
					}
				},
				//height: 300,
				// scrollable: {
				// virtual: true
				// },
				columns: [
					{ field: "VendorName", title: "Vendor" },
					{ field: "PICProductGroupName", title: "Product Category" }
				],
				change: $scope.onChangeGrid,
				noRecords: true,
				messages: {
					noRecords: "No records found"
				},
				//dataBound: $scope.onDataBound,
				//dataBinding: onDataBinding,
				editable: "popup",
				filterable: true,
				selectable: "row",
				// scrollable: {
				// //virtual: true
				// },
				//height: 300,
				toolbar: [
					{
						template: kendo.template(
							$(
								' <script id="template" type="text/x-kendo-template"><div style="padding:5px;"><input ng-keyup="VendorAndProductSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox leadsearch_tbox" style="width:215px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="ClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></script>')
								.html())
					}
				],
			};

			$scope.onDataBound = function (data) {
				var testGrid = data.sender;
				var newdata = testGrid._data;
				$scope.ProductGroup = data.sender.dataSource.data();
			};
			$scope.bringTooltipPopup = function () {
				$(".statusCanvas").show();
			}
			$scope.onMeasurmentDataBound = function (e) {
				var row;
				var selectedMeasurments = $scope.SelectedMeasurements;

				if (selectedMeasurments != undefined && selectedMeasurments.id > 0) {
					var qids = selectedMeasurments.quoteLineIds;
					var grid = $("#gridEditMeasurements").data("kendoGrid");
					for (var i = 0; i < grid.dataSource.data().length; i++) {
						if (grid.dataSource.data()[i].id == selectedMeasurments.id) {
							row = grid.table.find("[data-uid=" + grid.dataSource.data()[i].uid + "]");
						}
					}
					if (row != undefined) {
						$scope.measurementChanged = false;
						grid.select(row);
					}
				}

			}

			// The callback for successful calculatepopupclosed
			$scope.CalculatePopupClosed = function () {
			};

			$scope.receiveDigestCallback = function () {
				//$rootScope.$digest();
			}

			$scope.bringConfiguratorFeedback = function () {
				//$scope.ShowPopup('configuratorFeedbackModal');
				if ($scope.IsProductSelected == undefined || $scope.IsProductSelected == false) return;
				//if ($scope.GetErrorTypes()) {
				$scope.ConfiguratorFeedbackService.Configurator.ErrorTypeId = 15;
				//$scope.ConfiguratorFeedbackService.ErrorTypes = [{ Id: 1, Name: "Missing Product" }, { Id: 2, Name: "Missing product option" }, { Id: 3, Name: "Incorrect price" },
				//{ Id: 4, Name: "Incorrect product detail - other" }, { Id: 5, Name: "Other product data problem" }];
				$scope.ConfiguratorFeedbackService.Configurator.Vendor = $scope.Vendor;
				$scope.ConfiguratorFeedbackService.Configurator.ProductCategory = $scope.ProductCategory;
				$scope.ConfiguratorFeedbackService.Configurator.VendorId = $scope.VendorId;
				$scope.ConfiguratorFeedbackService.Configurator.PICVendor = $scope.PICVendor;
				$scope.ConfiguratorFeedbackService.Configurator.ProductGroupId = $scope.PICProductGroupId;
				$scope.ConfiguratorFeedbackService.Configurator.ProductPICJson = JSON.stringify($scope.ProductPICJson);
				$scope.ConfiguratorFeedbackService.Configurator.PICJson = JSON.stringify($scope.PICJson);
				$scope.ConfiguratorFeedbackService.Configurator.SelectedModel = $scope.selectedModel;
				$scope.ConfiguratorFeedbackService.Configurator.FieldErrors = $scope.FieldErrors;// JSON.stringify($scope.FieldErrors);
				$scope.ConfiguratorFeedbackService.ShowConfigurator($scope.CalculatePopupClosed, $scope.receiveDigestCallback);
				// }
			}

			$scope.onCancel = function () {
				$scope.statusRecursion = false;
				$scope.clearConfigSaveStatuses();

				if (window.location.href.indexOf('xVPOCoreProductconfigurator') > 0)
					$window.location.href = "/#!/xPurchaseMasterView/" + $scope.purchaseOrderId;
				else
					$window.location.href = "#!/quote/" + $scope.OpportunityId + "/" + $scope.quoteKey;
			};

			$("#prdDownloadlist").kendoDropDownList({
				dataSource: $scope.Product,
				dataTextField: "ProductName",
				dataValueField: "PICProductId",
				select: function (e) {
					if (e.dataItem.ProductName === "Select") {
						$scope.PreventprdClose = true;
						$scope.PreventmodelClose = false;
						e.preventDefault();
					}
					else {
						var parent = $("#prdDownloadlistrequired").children();
						var elementWrapper = $(parent).children();
						var element = $(elementWrapper[0]).children()[0];
						$(element).removeClass("prdrequired");
						$scope.PreventprdClose = false;
					}
				},
				filter: "contains",
				filtering: function (e) {
					var filter = e.filter;
				},
				autoBind: false,
				optionLabel: "Select",
				value: $scope.SelectedPICProduct,
				valuePrimitive: true,
				change: $scope.OnSelectedProduct,
				close: function (e) {
					if ($scope.PreventprdClose == true)
						e.preventDefault();

					$scope.PreventprdClose = false;
				}
			});

			$("#modelDownloadlist").kendoDropDownList({
				dataSource: $scope.Product,
				dataTextField: "Description",
				dataValueField: "Model",
				select: function (e) {
					if (e.dataItem.Description === "Select") {
						$scope.PreventmodelClose = true;
						$scope.PreventprdClose = false;
						e.preventDefault();
					}
					else {
						var parent = $("#modelDownloadlistrequired").children();
						var elementWrapper = $(parent).children();
						var element = $(elementWrapper[0]).children()[0];
						$(element).removeClass("modelrequired");
						$scope.PreventmodelClose = false;
					}
				},
				filter: "contains",
				filtering: function (e) {
					var filter = e.filter;
				},
				autoBind: false,
				optionLabel: "Select",
				value: $scope.selectedModel,
				valuePrimitive: true,
				change: $scope.OnModelSelected,
				close: function (e) {
					if ($scope.PreventmodelClose == true)
						e.preventDefault();

					$scope.PreventmodelClose = false;
				}
			});


			$scope.onChangeGrid = function (data) {
				var parent = $("#prdDownloadlistrequired").children();
				var elementWrapper = $(parent).children();
				var element = $(elementWrapper[0]).children()[0];
				$(element).addClass("prdrequired");

				var parent = $("#modelDownloadlistrequired").children();
				var elementWrapper = $(parent).children();
				var element = $(elementWrapper[0]).children()[0];
				$(element).addClass("modelrequired");

				$scope.SelectedPICProduct = '';
				$scope.selectedModel = '';


				// clear the dropdown values on change event
				var modelElement = $("#modelDownloadlist");
				var productElement = $("#prdDownloadlist");
				var modelDropdown;
				var productDropdown;

				if (productElement) {
					productDropdown = $("#prdDownloadlist").data("kendoDropDownList");
					if (productDropdown) {
						productDropdown.text("");
						productDropdown.value("");
					}
				}

				if (modelElement) {
					modelDropdown = $("#prdDownloadlist").data("kendoDropDownList");
					if (modelDropdown) {
						modelDropdown.text("");
						modelDropdown.value("");
					}
				}

				if ($scope.EditconfigFromMPO) {
					var items = data.sender.select();
					var grd = data.sender;

					items.each(function (i, data) {
						var dataItem = grd.dataItem(data);
						$(data).removeClass("k-state-selected");
					});
				} else {
					var grid = data.sender;
					var prodt = grid.dataItem(this.select());

					$scope.selectedProductGroup = prodt.PICProductGroupId;
					$scope.SelectedPICVendor = prodt.PICVendorId;
					$scope.selectedVendor = prodt.VendorId;

					//feedback create
					$scope.IsProductSelected = true;
					$scope.Vendor = prodt.VendorName;
					$scope.ProductCategory = prodt.PICProductGroupName;
					$scope.VendorId = prodt.VendorId;
					$scope.PICProductGroupId = prodt.PICProductGroupId;
					$scope.PICVendor = prodt.PICVendorId;
					$scope.PICJson = "";
					$scope.ProductPICJson = "";
					$scope.FieldErrors = "";

					$("#picProductddl").show();
					$("#ModelGrpError").hide();
					$("#picModelGroup").hide();
					$("#promptsOuter").hide();

					var pGroup = grid.dataSource.data();
					var vendor = prodt.VendorId;
					var productList;
					if (pGroup != undefined)
						for (var i = 0; pGroup.length; i++) {
							if (pGroup[i].PICProductGroupId === prodt.PICProductGroupId &&
								pGroup[i].VendorId === $scope.selectedVendor) {
								//productList = pGroup[i].PICProductList;
								var dupes = {};
								var singles = [];
								$.each(pGroup[i].PICProductList, function (i, el) {
									if (!dupes[el.PICProductId]) {
										dupes[el.PICProductId] = true;
										singles.push(el);
									}
								});
								productList = singles;
								$scope.ProductPICJson = productList;
								break;
							}
						}
					$scope.Product = productList;
					$scope.selectedModel = "";

					var dataSource = new kendo.data.DataSource({
						data: productList
					});
					var dropdownlist = $("#prdDownloadlist").data("kendoDropDownList");
					dropdownlist.setDataSource(dataSource);

					$scope.SelectedPICProduct = '';
					$scope.selectedModel = '';
				}

				var panelBar = $("#panelbar").data("kendoPanelBar");
				panelBar.expand($("#ConfigPanel"));

				//dropdownlist.refresh();

				//$scope.$apply();

				// demoooo

				var selectedRow = $("#VendorGrid .k-state-selected");
				if (selectedRow && selectedRow.length != 0) {
					//console.log($scope);
					//$scope.selectedElement = [];

					$scope.selectedElement = selectedRow;
					$scope.uid = $($scope.selectedElement[0]).attr("data-uid");
					$scope.Tempuid = $($scope.selectedElement[0]).attr("data-uid");
				}
			};


			$scope.validateModel = function () {
				var prodlist = $scope.Product;
				var modelExist = false;
				if (prodlist.length > 0) {
					for (var k = 0; k < prodlist.length; k++) {
						if (prodlist[k].PICProductId == $scope.SelectedPICProduct) {
							if (prodlist[k].models.length > 0) {
								for (var i = 0; i < prodlist[k].models.length; i++) {
									if (prodlist[k].models[i].Model === $scope.selectedModel) {
										modelExist = true;
										// break;
									}
								}
							}
							// break;
						}
					}
				}
				return modelExist;
			}
			$scope.ValidateSaveConfiguratorAndReturn = function () {
				$scope.ReturntoQuote = true;
				$scope.ValidateAndSaveConfigurator();
			}

			$scope.onSaveConfigurator = function () {


				if ($scope.validateModel()) {


					var now = this;
					//var configprese = $scope.configPresentationObj;
					var configprese = now.configPresentationObj;
					var config = $scope.configEngineObj;
					var loadingElement = document.getElementById("loading");
					loadingElement.style.display = "block";
					var myElements = document.querySelectorAll("[class='col-sm-3 text_label']");

					for (var i = 0; i < myElements.length; i++) {
						myElements[i].style.color = "black";
					}
					var myElementsinput = document.querySelectorAll("[class='picInput']");
					for (var i = 0; i < myElementsinput.length; i++) {
						myElementsinput[i].style.borderColor = "#ccc";
					}
					var prodId = $scope.selectedProduct;
					var modelId = $scope.selectedModel;
					if (modelId === null) {
						modelId = $scope.selectedPICModel;
					}

					var gGroup = $scope.selectedProductGroup;
					var Quantity = $scope.Quantity;
					var picVendor = $scope.SelectedPICVendor;
					var picProduct = $scope.SelectedPICProduct;

					var pGroup = $("#VendorGrid").data("kendoGrid").dataSource.data();
					var pCategory, ProductName, VendorName;
					var qk = $scope.quoteKey;
					var opp = $scope.OpportunityId;
					for (var i = 0; i < pGroup.length; i++) {
						if (pGroup[i].PICProductGroupId === $scope.selectedProductGroup &&
							pGroup[i].VendorId === $scope.selectedVendor) {
							pCategory = pGroup[i].PICProductGroupName;
							VendorName = pGroup[i].VendorName;
							var productList = pGroup[i].PICProductList;
							for (var k = 0; k < productList.length; k++) {
								if (productList[k].PICProductId === picProduct) {
									ProductName = productList[k].ProductName;
								}
							}
							break;
						}
					}
					if (configprese !== undefined && Quantity > 0) {
						if (configprese.validatePrompts()) {
							var jsonObj = {};
							jsonObj.VendorName = VendorName;
							jsonObj.ProductName = ProductName;
							jsonObj.PCategory = pCategory;
							jsonObj.VendorId = $scope.selectedVendor;
							jsonObj.PicVendor = picVendor;
							jsonObj.PicProduct = picProduct;
							jsonObj.ProductId = prodId;
							jsonObj.ModelId = modelId;
							jsonObj.ModelDescription = $scope.ModelDescription;
							jsonObj.GGroup = gGroup;
							jsonObj.QuoteLineId = $scope.QuoteLineId;
							jsonObj.Quantity = Quantity;
							jsonObj.PromptAnswers = JSON.stringify(config.getPromptAnswers());
							$scope.pAnswers = config.getPromptAnswers();
							$scope.PrdPromptAnswers = $scope.pAnswers;

							//$scope.ModelGroup
							//$scopee.ModelDescription
							if ($scope.ModelGroup !== null && $scope.ModelGroup !== undefined) {
								var mlist = $scope.ModelGroup;
								for (var i = 0; i < mlist.length; i++) {
									if (mlist[i].Model === modelId) {
										jsonObj.Description = ProductName + ' - ' + mlist[i].Description;
									}
								}
							} else {
								jsonObj.Description = ProductName;
							}

							jsonObj.QuoteKey = qk;
							jsonObj.OpportunityId = opp;

							var promptAnswers = JSON.stringify(jsonObj);

							// set options back to grid
							if ($scope.widthPrompt != null && $scope.widthPrompt !== undefined && $scope.widthPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.widthPrompt] !== undefined) {
									var width = $scope.pAnswers["Prompt" + $scope.widthPrompt].value;

									if (jsonObj.Width !== parseFloat((width + "").split(".")[0])) {
										jsonObj.Width = parseFloat((width + "").split(".")[0]);
										//changed = true;
									}
									if (jsonObj.FranctionalValueWidth !== parseFloat((width + "").split(".")[1])) {
										jsonObj.FranctionalValueWidth = $scope.getFraction((width + "").split(".")[1]);
										//changed = true;
									}
								}
							}

							////HEIGHT PROMPT
							if ($scope.heightPrompt != null && $scope.heightPrompt !== undefined && $scope.heightPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.heightPrompt] !== undefined) {
									var height = $scope.pAnswers["Prompt" + $scope.heightPrompt].value;

									if (jsonObj.Height !== parseFloat((height + "").split(".")[0])) {
										jsonObj.Height = parseFloat((height + "").split(".")[0]);
									}
									if (jsonObj.FranctionalValueHeight !== parseFloat((height + "").split(".")[1])) {
										jsonObj.FranctionalValueHeight = $scope.getFraction((height + "").split(".")[1]);
									}
								}
							}

							//Room Name
							if ($scope.roomPrompt != null && $scope.roomPrompt !== undefined && $scope.roomPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.roomPrompt] !== undefined) {
									var RoomName = $scope.pAnswers["Prompt" + $scope.roomPrompt].value;

									if (jsonObj.RoomName !== RoomName) {
										jsonObj.RoomName = RoomName;
									}
								}
							}

							//////Mount Type
							if ($scope.mountPrompt != null && $scope.mountPrompt !== undefined && $scope.mountPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.mountPrompt] !== undefined) {
									var mountPromptoption = $scope.pAnswers["Prompt" + $scope.mountPrompt].value;
									var mount = '';
									if (mountPromptoption === "I") {
										mount = "IB";
									} else if (mountPromptoption === "O") {
										mount = "OB";
									}
									if (jsonObj.MountType !== mount) {
										jsonObj.MountType = mount;
									}
								}
							}
							$scope.statusRecursion = true;
							// $scope.callforSavingStatus();
							//  setTimeout(function () { $scope.callforSavingStatus(); }, 100);
							$http.post('/api/Quotes/0/validatePromptAnswers', jsonObj).then(function (response) {
								$scope.statusRecursion = false;
								$scope.clearConfigSaveStatuses();
								if (response.data.valid) {
									var res = response.data.data;
									var opId = response.data.OpportunityId;
									var qk = response.data.QuoteKey;
									if (!res.valid) {
										$scope.FieldErrors = "";
										if (res.messages.errors.length > 0) {
											var div = '<ul>';
											for (var i = 0; i < res.messages.errors.length; i++) {
												div = div + "<li>" + res.messages.errors[i] + "</li> ";
												$scope.FieldErrors = $scope.FieldErrors == "" ? res.messages.errors[i] : $scope.FieldErrors + ", " + res.messages.errors[i];
											}
											div = div + "</ul>";
										}
										document.getElementById("response").innerHTML = div;
										var responseElement = document.getElementById("response");
										responseElement.style.display = "block";
										responseElement.style.color = "red";
										$("#response").css('border', '1px solid red');
									} else {
										var responseElement = document.getElementById("response");
										responseElement.style.display = "block";
										responseElement.style.color = "green";
										$("#response").css('border', '1px solid green');

										document.getElementById("response").innerHTML = 'Product validated successfully.';

										var responseElement = document.getElementById("response");
										responseElement.style.display = "block";
									}
								} else {
									//HFC.DisplayAlert("Unknown error occured. Please contact site Administrator 468.");
									HFC.DisplayAlert(response.data.message);
								}
								loadingElement.style.display = "none";
							}).catch(function (e) {
								$scope.statusRecursion = false;
								$scope.clearConfigSaveStatuses();
								var loadingElement = document.getElementById("loading");
								loadingElement.style.display = "none";
								//$scope.loadingElement.style.display = "none";
								HFC.DisplayAlert("Unknown error occurred. Please contact site Administrator 831.");
							});
						} else {
							$scope.statusRecursion = false;
							$scope.clearConfigSaveStatuses();
							loadingElement.style.display = "none";
						}
					}
					else {
						$scope.statusRecursion = false;
						$scope.clearConfigSaveStatuses();
						var loadingElement = document.getElementById("loading");
						loadingElement.style.display = "none";
					}
				}
				else {
					document.getElementById("response").innerHTML = "This product model is inactive, please select a different product model.";
					var responseElement = document.getElementById("response");
					responseElement.style.display = "block";
					responseElement.style.color = "red";
					$("#response").css('border', '1px solid red');
				}


			};
			$scope.ConfigSaveStatus = "";
			//  document.getElementById("ConfigSaveStatuss").innerHTML = $scope.ConfigSaveStatus;
			$scope.callforSavingStatus = function () {
				$http.get('/api/Quotes/0/getConfigSaveStatuses').
					then(function (data) {
						$scope.ConfigSaveStatus = data.data;
						document.getElementById("ConfigSaveStatuss").innerHTML = $scope.ConfigSaveStatus;
						//console.log($scope.ConfigSaveStatus)

						if ($scope.ConfigSaveStatus != "") {

						}
						if ($scope.statusRecursion) {
							//  setTimeout(function () { $scope.callforSavingStatus(); }, 100);
							$scope.callforSavingStatus();
						}
						if (!$scope.statusRecursion) {
							$scope.clearConfigSaveStatuses();
							$scope.ConfigSaveStatus = "";
							document.getElementById("ConfigSaveStatuss").innerHTML = $scope.ConfigSaveStatus;
						}
					}).catch(function (error) {

					});
			}

			$scope.clearConfigSaveStatuses = function () {
				$http.get('/api/Quotes/0/clearConfigSaveStatuses').
					then(function (data) {
						$scope.ConfigSaveStatus = "";
						//  $("ConfigSaveStatuss").text($scope.ConfigSaveStatus);
						document.getElementById("ConfigSaveStatuss").innerHTML = $scope.ConfigSaveStatus;
					});
			}

			$scope.$on('$locationChangeStart', function (event) {
				$scope.clearConfigSaveStatuses();
			});

			$scope.ValidateAndSaveConfigurator = function () {


				if ($scope.validateModel()) {

					var now = this;
					//var configprese = $scope.configPresentationObj;
					var configprese = now.configPresentationObj;
					var config = this.configEngineObj;
					var loadingElement = document.getElementById("loading");
					loadingElement.style.display = "block";
					var myElements = document.querySelectorAll("[class='col-sm-3 text_label']");
					$(".select2-container").parent().addClass("form-submitted");
					// resetting Error Messages
					document.getElementById("response").innerHTML = '';
					$("#response").css('border', 'none');

					for (var i = 0; i < myElements.length; i++) {
						myElements[i].style.color = "black";
					}
					var myElementsinput = document.querySelectorAll("[class='picInput']");
					for (var i = 0; i < myElementsinput.length; i++) {
						myElementsinput[i].style.borderColor = "#ccc";
					}
					var prodId = $scope.selectedProduct;
					var modelId = $scope.selectedModel;
					if (modelId === null) {
						modelId = $scope.selectedPICModel;
					}

					var gGroup = $scope.selectedProductGroup;
					var Quantity = Number($("#kenqty").val());   //$scope.Quantity;
					var picVendor = $scope.SelectedPICVendor;
					var picProduct = $scope.SelectedPICProduct;



					var pGroup = $("#VendorGrid").data("kendoGrid").dataSource.data();
					var pCategory, ProductName, VendorName, ProductCategoryId;
					var qk = $scope.quoteKey;
					var opp = $scope.OpportunityId;
					for (var i = 0; i < pGroup.length; i++) {
						if (pGroup[i].PICProductGroupId === $scope.selectedProductGroup &&
							pGroup[i].VendorId === $scope.selectedVendor) {
							pCategory = pGroup[i].PICProductGroupName;

							VendorName = pGroup[i].VendorName;
							var productList = pGroup[i].PICProductList;
							for (var k = 0; k < productList.length; k++) {
								if (productList[k].PICProductId == picProduct) {
									ProductName = productList[k].ProductName;
									ProductCategoryId = productList[k].ProductCategoryId;
								}
							}
							break;
						}
					}

					if (configprese !== undefined && Quantity > 0) {
						if (configprese.validatePrompts()) {
							var jsonObj = {};
							jsonObj.VendorName = VendorName;
							jsonObj.ProductName = ProductName;
							jsonObj.PCategory = pCategory;
							jsonObj.VendorId = $scope.selectedVendor;
							jsonObj.PicVendor = picVendor;
							jsonObj.PicProduct = picProduct;
							jsonObj.ProductId = prodId;
							jsonObj.ModelId = modelId;
							jsonObj.GGroup = gGroup;
							jsonObj.ProductCategoryId = ProductCategoryId;
							if ($scope.selectedquoteLinerow != null && $scope.selectedquoteLinerow !== undefined) {
								jsonObj.QuoteLineId = $scope.selectedquoteLinerow.QuoteLineId;
							}
							else if ($scope.$parent.selectedquoteLinerow != null && $scope.$parent.selectedquoteLinerow !== undefined) {
								jsonObj.QuoteLineId = $scope.$parent.selectedquoteLinerow.QuoteLineId;
							}
							else {
								jsonObj.QuoteLineId = 0;
							}

							jsonObj.Quantity = Quantity;
							jsonObj.ModelDescription = $scope.ModelDescription;
							jsonObj.PromptAnswers = JSON.stringify(config.getPromptAnswers());
							$scope.pAnswers = config.getPromptAnswers();
							$scope.PrdPromptAnswers = $scope.pAnswers;
							if ($scope.SelectedMeasurements != undefined && $scope.SelectedMeasurements != null) {
								jsonObj.MeasurementSetId = $scope.SelectedMeasurements.id;
								jsonObj.InternalNotes = $scope.SelectedMeasurements.Comments;
							}
							else if ($scope.$parent.SelectedMeasurements != undefined && $scope.$parent.SelectedMeasurements != null) {
								jsonObj.MeasurementSetId = $scope.$parent.SelectedMeasurements.id;
								jsonObj.InternalNotes = $scope.$parent.SelectedMeasurements.Comments;
							}
							else {
								jsonObj.MeasurementSetId = 0;

								if ($scope.selectedquoteLinerow != null && $scope.selectedquoteLinerow !== undefined) {
									jsonObj.InternalNotes = $scope.selectedquoteLinerow.InternalNotes;
								}
								else if ($scope.$parent.selectedquoteLinerow != null && $scope.$parent.selectedquoteLinerow !== undefined) {
									jsonObj.InternalNotes = $scope.$parent.selectedquoteLinerow.InternalNotes;
								} else {
									jsonObj.InternalNotes = '';
								}

							}

							//$scope.ModelGroup
							//$scopee.ModelDescription
							if ($scope.ModelGroup !== null && $scope.ModelGroup !== undefined) {
								var mlist = $scope.ModelGroup;
								for (var i = 0; i < mlist.length; i++) {
									if (mlist[i].Model === modelId) {
										jsonObj.Description = ProductName + ' - ' + mlist[i].Description;
										jsonObj.ModelDescription = mlist[i].Description;
									}
								}
							} else {
								jsonObj.Description = ProductName;
							}

							jsonObj.QuoteKey = qk;
							jsonObj.OpportunityId = opp;

							var promptAnswers = JSON.stringify(jsonObj);





							//Getting the prompt ID's for all width height and
							var prodlist = $scope.Product;
							for (var i = 0; i < prodlist.length; i++) {
								if (prodlist[i].PICProductId == jsonObj.PicProduct) {
									$scope.heightPrompt = prodlist[i].HeightPrompt;
									$scope.widthPrompt = prodlist[i].WidthPrompt;
									$scope.mountPrompt = prodlist[i].MountPrompt;
									$scope.roomPrompt = prodlist[i].RoomPrompt;
									$scope.colorPrompt = prodlist[i].ColorPrompt;
									$scope.fabricPrompt = prodlist[i].FabricPrompt;



									break;
								}
							}





							// set options back to grid
							if ($scope.widthPrompt != null && $scope.widthPrompt !== undefined && $scope.widthPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.widthPrompt] !== undefined) {
									var width = $scope.pAnswers["Prompt" + $scope.widthPrompt].value;

									if (jsonObj.Width !== parseFloat((width + "").split(".")[0])) {
										jsonObj.Width = parseFloat((width + "").split(".")[0]);
									}
									if (jsonObj.FranctionalValueWidth !== parseFloat((width + "").split(".")[1])) {
										jsonObj.FranctionalValueWidth = $scope.getFraction((width + "").split(".")[1]);
									}
								}
							}

							////HEIGHT PROMPT
							if ($scope.heightPrompt != null && $scope.heightPrompt !== undefined && $scope.heightPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.heightPrompt] !== undefined) {
									var height = $scope.pAnswers["Prompt" + $scope.heightPrompt].value;

									if (jsonObj.Height !== parseFloat((height + "").split(".")[0])) {
										jsonObj.Height = parseFloat((height + "").split(".")[0]);
									}
									if (jsonObj.FranctionalValueHeight !== parseFloat((height + "").split(".")[1])) {
										jsonObj.FranctionalValueHeight = $scope.getFraction((height + "").split(".")[1]);
									}
								}
							}

							//Room Name
							if ($scope.roomPrompt != null && $scope.roomPrompt !== undefined && $scope.roomPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.roomPrompt] !== undefined) {
									var RoomName = $scope.pAnswers["Prompt" + $scope.roomPrompt].value;

									if (jsonObj.RoomName !== RoomName) {
										jsonObj.RoomName = RoomName;
									}
								}
							}

							//////Mount Type
							if ($scope.mountPrompt != null && $scope.mountPrompt !== undefined && $scope.mountPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.mountPrompt] !== undefined) {
									var mountPromptoption = $scope.pAnswers["Prompt" + $scope.mountPrompt].value;
									var mount = '';
									if (mountPromptoption === "I") {
										mount = "IB";
									} else if (mountPromptoption === "O") {
										mount = "OB";
									}
									if (jsonObj.MountType !== mount) {
										jsonObj.MountType = mount;
									}
								}
							}

							//color Prompt
							if ($scope.colorPrompt != null && $scope.colorPrompt !== undefined && $scope.colorPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.colorPrompt] !== undefined) {
									var colorPrompt = $scope.pAnswers["Prompt" + $scope.colorPrompt].value + " " + $scope.pAnswers["Prompt" + $scope.colorPrompt].colorValue;

									if (jsonObj.Color !== colorPrompt) {
										jsonObj.Color = colorPrompt;
									}
								}
							}
							//fabric Name
							if ($scope.fabricPrompt != null && $scope.fabricPrompt !== undefined && $scope.fabricPrompt > 0) {
								if ($scope.pAnswers["Prompt" + $scope.fabricPrompt] !== undefined) {
									var fabricPrompt = $scope.pAnswers["Prompt" + $scope.fabricPrompt].value;

									if (jsonObj.Fabric !== fabricPrompt) {
										jsonObj.Fabric = fabricPrompt;
									}
								}
							}




							var url = '/api/Quotes/0/validateAndSaveConfigurator';

							if ($scope.EditconfigFromMPO) {
								url = '/api/PurchaseOrders/' + $scope.mpoHeader.PurchaseOrderId + '/validateAndSaveConfigurator';

								jsonObj.QuoteLinesVM = $scope.selectedquoteLinerow;
							}
							$scope.statusRecursion = true;
							//  setTimeout(function () { $scope.callforSavingStatus(); }, 100);
							document.getElementById("ConfigSaveStatuss").innerHTML = "";
							$scope.callforSavingStatus();
							$http.post(url, jsonObj).then(function (response) {
								$scope.statusRecursion = false;
								$scope.clearConfigSaveStatuses();
								// console.log("statusRecursion ");
								if (response.data.valid) {
									var res = response.data.data;
									var opId = response.data.OpportunityId;
									var qk = response.data.QuoteKey;
									if (!res.valid) {
										$scope.FieldErrors = "";
										if (res.messages.errors.length > 0) {
											var div = '<ul>';
											for (var i = 0; i < res.messages.errors.length; i++) {
												div = div + "<li>" + res.messages.errors[i] + "</li> ";
												$scope.FieldErrors = $scope.FieldErrors == "" ? res.messages.errors[i] : $scope.FieldErrors + ", " + res.messages.errors[i];
											}
											div = div + "</ul>";
										}
										document.getElementById("response").innerHTML = div;
										var responseElement = document.getElementById("response");
										responseElement.style.display = "block";
										responseElement.style.color = "red";
										$("#response").css('border', '1px solid red');
										loadingElement.style.display = "none";
										//document.getElementById('#response').innerText = div;
									} else {
										if ($scope.ReturntoQuote) {
											if ($scope.EditconfigFromMPO) {
												window.location.href = '#!/purchasemasterview/' + $scope.mpoHeader.PurchaseOrderId;
											}
											else {
												$scope.onCancel();
											}
											loadingElement.style.display = "none";
										} else {
											$scope.SelectedMeasurements = null;
											$scope.selectedquoteLinerow = null
											$rootScope.selectedquoteLine = null;
											$scope.QuoteData = response.data.QuoteData;
											if ($scope.QuoteData.QuoteLinesVM != null) {
												for (var i = 0; i < $scope.QuoteData.QuoteLinesVM.length; i++) {
													if ($scope.EditconfigFromMPO) {
														$scope.QuoteData.QuoteLinesVM[i].EditconfigFromMPO = true;
													} else {
														$scope.QuoteData.QuoteLinesVM[i].EditconfigFromMPO = false;
													}
												}
											}

											var quotename = "Quote #" + $scope.QuoteData.QuoteID;

											$http.get('/api/Quotes/' + $scope.quoteKey + '/GetQuoteAndMeasurementDetails?poId=' + $scope.purchaseOrderId).then(function (responseMD) {
												if (responseMD.data != null) {
													$scope.QuoteData = responseMD.data;
													var quotename = "Quote #" + $scope.QuoteData.QuoteID + " - " + $scope.QuoteData.QuoteName
													if ($scope.QuoteData.QuoteLinesVM != null) {
														for (var i = 0; i < $scope.QuoteData.QuoteLinesVM.length; i++) {
															if ($scope.EditconfigFromMPO) {
																$scope.QuoteData.QuoteLinesVM[i].EditconfigFromMPO = true;
															} else {
																$scope.QuoteData.QuoteLinesVM[i].EditconfigFromMPO = false;
															}
														}
													}
													//HFCService.setHeaderTitle(quotename);
													if (responseMD.data.Measurements != null) {
														$scope.Measurements = responseMD.data.Measurements;

														var measure = measurementDS(JSON.parse(responseMD.data.Measurements),
															$scope.QuoteData.QuoteLinesVM);
														//$("#gridEditMeasurements").data("kendoGrid").dataSource.data(measure);
														//$("#gridEditMeasurements").data('kendoGrid').refresh();
														var grid_Mesu = $("#gridEditMeasurements").data("kendoGrid");
														grid_Mesu.setDataSource(measure);
														grid_Mesu.refresh();

														var selectedMeasurments = $scope.SelectedMeasurements;
														if (selectedMeasurments != null && selectedMeasurments != undefined) {
															var mesugrid = $("#gridEditMeasurements").data("kendoGrid");
															for (var i = 0; i < mesugrid.dataSource.data().length; i++) {
																if (mesugrid.dataSource.data()[i].id == selectedMeasurments.id) {
																	$scope.SelectedMeasurements = mesugrid.dataSource.data()[i];
																	// $('[data-uid=' + mesugrid.dataSource.data()[i].uid + ']').addClass('k-state-selected');
																	break;
																}
															}
														}
													}
													$scope.QuoteData.QuoteLinesVM = responseMD.data.QuoteLinesVM;
													var grid_QL = $("#QuotelinesGrid").data("kendoGrid");
													var QuoteLineDS = {
														data: $scope.QuoteData.QuoteLinesVM,
														schema: {
															model: {
																id: "QuoteLineId",
																fields: {
																	QuoteLineId: { editable: false },
																	Quantity: { editable: false },
																	RoomName: { editable: false },
																	ProductName: { editable: false },
																	Width: { editable: false },
																	Height: { editable: false },
																	MountType: { editable: false },
																	Color: { editable: false },
																	Fabric: { editable: false },
																	ExtendedPrice: { editable: false },
																	InternalNotes: { editable: true },
																}
															}
														}
													}
													grid_QL.setDataSource(QuoteLineDS);
													grid_QL.refresh();
													if ($scope.selectedquoteLinerow !== null && $scope.selectedquoteLinerow !== undefined) {
														document.getElementById("response").innerHTML = "Product validated successfully and " + "<b>Quote line#: " + $scope.selectedquoteLinerow.QuoteLineNumber + " with Window Name: " + $scope.selectedquoteLinerow.RoomName + '</b> updated.';
													} else {
														document.getElementById("response").innerHTML = 'Product validated successfully and Quote line created/updated.';
													}
													$scope.selectedquoteLinerow = null;
													$rootScope.selectedquoteLine = null;
													$scope.$parent.selectedquoteLinerow = null;
													//$scope.apply();
												}
												loadingElement.style.display = "none";
											}).catch(function (error) {
												$scope.statusRecursion = false;
												$scope.clearConfigSaveStatuses();
												loadingElement.style.display = "none";
												//$scope.loadingElement.style.display = "none";
												HFC.DisplayAlert("Unknown error occurred. Please contact site Administrator.");
											});

											var responseElement = document.getElementById("response");
											responseElement.style.display = "block";
											responseElement.style.color = "green";
											$("#response").css('border', '1px solid green');
											//var div = JSON.stringify(response.data);
											document.getElementById("response").innerHTML =
												'Product validated successfully and Quote line created/updated.';

											// document.getElementById("response").innerHTML = div;
											var responseElement = document.getElementById("response");
											responseElement.style.display = "block";
										}
									}
								} else {
									//HFC.DisplayAlert("Unknown error occured. Please contact site Administrator 468.");
									HFC.DisplayAlert(response.data.message);
									loadingElement.style.display = "none";
								}

							}).catch(function (error) {
								$scope.statusRecursion = false;
								$scope.clearConfigSaveStatuses();
								var loadingElement = document.getElementById("loading");
								loadingElement.style.display = "none";
								//$scope.loadingElement.style.display = "none";
								//HFC.DisplayAlert("Unknown error occurred. Please contact site Administrator 1094.");
							});
						} else {
							$scope.statusRecursion = false;
							$scope.clearConfigSaveStatuses();
							loadingElement.style.display = "none";
						}
					} else {
						$scope.statusRecursion = false;
						if (Quantity <= 0 || Quantity === null || Quantity === undefined) {
							var div = '<ul><li>Quantity is Required.</li></ul>';

							document.getElementById("response").innerHTML = div;
							var responseElement = document.getElementById("response");
							responseElement.style.display = "block";
							responseElement.style.color = "red";
							$("#response").css('border', '1px solid red');
						}
						$scope.clearConfigSaveStatuses();
						var loadingElement = document.getElementById("loading");
						loadingElement.style.display = "none";
					}
				}
				else {
					document.getElementById("response").innerHTML = "This product model is inactive, please select a different product model.";
					var responseElement = document.getElementById("response");
					responseElement.style.display = "block";
					responseElement.style.color = "red";
					$("#response").css('border', '1px solid red');
				}
			};

			$scope.changegrp = function () {
				var pGroup = $scope.ProductGroup;
				var vendor = $scope.selectedVendor;
				var productList;
				if (pGroup != undefined && $scope.selectedProductGroup != null)
					for (var i = 0; i < pGroup.length; i++) {
						if (pGroup[i].PICProductGroupId === $scope.selectedProductGroup &&
							pGroup[i].VendorId === $scope.selectedVendor) {
							productList = pGroup[i].PICProductList;
							break;
						}
					}
				$scope.newconfig = true;
				$scope.Product = productList;
			}

			$scope.OnSelectedProduct = function (e) {
				var data = this.value();
				var promptsOuter1 = document.getElementById("promptsOuter");
				promptsOuter1.style.display = "none";
				document.getElementById("response").innerHTML = '';
				$("#response").css('border', 'none');
				var prodlist = $scope.Product;
				//if ($scope.selectedquoteLinerow !== null && $scope.selectedquoteLinerow !== undefined) {
				//    $("#gridEditMeasurements").data('kendoGrid').refresh();
				//    $("#QuotelinesGrid").data('kendoGrid').refresh();
				//}
				$scope.ModelGroup = [];
				$("#ModelGrpError").hide();
				$scope.SelectedPICProduct = data;
				for (var i = 0; i < prodlist.length; i++) {
					if (prodlist[i].PICProductId == data) {
						$scope.selectedProduct = prodlist[i].ProductId;
						if (prodlist[i].models != null && prodlist[i].models.length > 0) {
							var dupes = {};
							var singles = [];
							$.each(prodlist[i].models, function (i, el) {

								if (!dupes[el.Model]) {
									dupes[el.Model] = true;
									singles.push(el);
							}
							});
							//productList = singles;
							$scope.ModelGroup = singles;
						}
						$scope.heightPrompt = prodlist[i].HeightPrompt;
						$scope.widthPrompt = prodlist[i].WidthPrompt;
						$scope.mountPrompt = prodlist[i].MountPrompt;
						$scope.roomPrompt = prodlist[i].RoomPrompt;
						$scope.colorPrompt = prodlist[i].ColorPrompt;
						$scope.fabricPrompt = prodlist[i].FabricPrompt;


						if (prodlist[i].WarningPhasingOut !== '') {
							var loadingElement = document.getElementById("modelWarning");
							loadingElement.innerHTML = prodlist[i].WarningPhasingOut;
							$("#ModelGrpError").show();
						}
						//break;
					}
				}
				$scope.selectedModel = "";
				//    $("#ModelGrpError").hide();
				//$("#picModelGroup").hide();
				$scope.newconfig = true;
				var prodId = $scope.SelectedPICProduct;
				var loadingElement = document.getElementById("loading");
				loadingElement.style.display = "block";


				$("#picModelGroup").show();
				var dataSource = new kendo.data.DataSource({
					data: $scope.ModelGroup
				});
				var dropdownlist = $("#modelDownloadlist").data("kendoDropDownList");
				dropdownlist.setDataSource(dataSource);
				dropdownlist.value("");

				loadingElement.style.display = "none";


				//if (prodId !== null && prodId != undefined && Number(prodId)) {
				//    var venGrdList = $("#VendorGrid").data("kendoGrid").dataSource.data();

				//    $scope.ModelGroup = null;

				//    if (venGrdList != undefined)
				//        for (var i = 0; venGrdList.length; i++) {
				//            if (venGrdList[i].PICProductGroupId === prodt.PICProductGroupId &&
				//				venGrdList[i].VendorId === $scope.selectedVendor) {

				//                for (var j = 0; j < venGrdList[i].PICProductList.length; j++) {
				//                    if (venGrdList[i].PICProductList[j].PICProductId === prodId) {
				//                        $scope.ModelGroup =venGrdList[i].PICProductList[j].models;
				//                    }
				//                }
				//                productList = venGrdList[i].PICProductList;
				//                $scope.ProductPICJson = productList;
				//                break;
				//            }
				//        }
				//    //var grdPrdData =

				//    //$scope.ModelGroup =
				//    $("#picModelGroup").show();
				//    var dataSource = new kendo.data.DataSource({
				//        data: $scope.ModelGroup
				//    });
				//    var dropdownlist = $("#modelDownloadlist").data("kendoDropDownList");
				//    dropdownlist.setDataSource(dataSource);
				//    dropdownlist.value("")

				//    loadingElement.style.display = "none";
				//$http.get('/api/PIC/0/GetModels?productId=' + prodId).then(function (modRes) {
				//	var PICModels = modRes.data;
				//	if (PICModels.error === '') {
				//		$("#picModelGroup").show();
				//		$scope.ModelGroup = jQuery.grep(PICModels.Data,
				//			function (value) {
				//				return value.IsActive === true;
				//			});
				//		var dataSource = new kendo.data.DataSource({
				//			data: $scope.ModelGroup
				//		});
				//		var dropdownlist = $("#modelDownloadlist").data("kendoDropDownList");
				//		dropdownlist.setDataSource(dataSource);
				//		dropdownlist.value("")
				//       // $('modelDownloadlist').select2();
				//		loadingElement.style.display = "none";
				//	} else {
				//		HFC.DisplayAlert(PICModels.error);
				//		loadingElement.style.display = "none";
				//	}
				//}).catch(function (error) {
				//	$scope.loadingElement.style.display = "none";
				//	HFC.DisplayAlert("Unknown error occurred. Please contact site Administrator. 536");
				//});
				//} else {
				//	loadingElement.style.display = "none";
				//}
			};
			var configPresentationObj, configEngineObj;

			//srv.GetDatsourcebyproductId = function (productId, success) {
			//$scope.setDatasource(prodId, modelId);
			$scope.setDatasource = function (prodId, modelId) {
				//var ser = $scope.configuratorService.GetProductProductGroup();
				var result = $scope.configuratorService.GetDataSourceAndPromptAnswers(prodId, modelId, $scope.GetDatsourceCallback);
			}
			$scope.GetDatsourceCallback = function (data) {
				$scope.PICJson = data;
				//sp.DataSourceAndPromptAnswers = data;
				//$scope.coredata.data = data;
				$scope.PrdPromptAnswers = JSON.parse(data.data.promptAnswers);
				$scope.PrdDatasource = data.data;

				//$scope.heightPrompt = data.data.productInfo.heightPrompt;
				//$scope.widthPrompt = data.data.productInfo.widthPrompt;

				//$scope.mountPrompt = data.data.productInfo.mountPrompt;
				//$scope.roomPrompt = data.data.productInfo.roomPrompt;

				$scope.newconfig = true;
				var config = new ConfigEngine();
				if ($scope.selectedquoteLinerow === undefined || $scope.selectedquoteLinerow === null) {
					$scope.Quantity = 1;
				}
				$scope.CreateConfiguration($scope.PrdPromptAnswers, $scope.PrdDatasource, $scope.selectedModel, config, false, $scope.Quantity);
			}
			$scope.GetDatsourceCallbackFromGrid = function (data) {
				//sp.DataSourceAndPromptAnswers = data;
				//$scope.coredata.data = data;
				//$scope.PrdPromptAnswers = JSON.parse(data.data.promptAnswers);
				$scope.PrdDatasource = data.data;
				var mod = $scope.selectedquoteLinerow;
				var re = JSON.parse(mod.PICJson);
				var prmpt = JSON.parse(re.PromptAnswers);
				$scope.PrdPromptAnswers = prmpt;

				$scope.Quantity = mod.Quantity;
				var widthPrompt = $scope.widthPrompt;
				var mountPrompt = $scope.mountPrompt;
				var roomPrompt = $scope.roomPrompt;
				var colorPrompt = $scope.colorPrompt;
				var fabricPrompt = $scope.fabricPrompt;


				$scope.widthPrompt = widthPrompt;
				$scope.mountPrompt = mountPrompt;
				$scope.roomPrompt = roomPrompt;
				$scope.colorPrompt = colorPrompt;
				$scope.fabricPrompt = fabricPrompt;


				//$scope.heightPrompt = data.data.productInfo.heightPrompt;
				//$scope.widthPrompt = data.data.productInfo.widthPrompt;

				//$scope.mountPrompt = data.data.productInfo.mountPrompt;
				//$scope.roomPrompt = data.data.productInfo.roomPrompt;
				$scope.newconfig = true;
				var config = new ConfigEngine();
				$scope.CreateConfiguration(prmpt, $scope.PrdDatasource, $scope.selectedModel, config, true, $scope.Quantity);
				var loadingElement = document.getElementById("loading");
				loadingElement.style.display = "none";
			}

			$scope.OnModelSelected = function (e) {
				var loadingElement = document.getElementById("loading");
				loadingElement.style.display = "block";
				var myEl = angular.element(document.querySelector('#Prompts'));
				myEl.empty();
				$scope.ModelDescription = '';
				$("#ModelGrpError").hide();
				$("#response").hide();

				var modelwarning = document.getElementById("modelWarning");

				var modelId = "";
				var prodId = $scope.SelectedPICProduct;
				$scope.PrdPromptAnswers = null;
				//if ($scope.Firstmodel !== null) {
				//    $scope.selectedModel = $scope.Firstmodel;
				//    modelId = $scope.Firstmodel;
				//    $scope.Firstmodel = null;
				//} else {
				//    modelId = this.value();
				//}
				modelId = this.value();
				$scope.selectedModel = modelId;
				document.getElementById("response").innerText = "";
				var responseElement = document.getElementById("response");
				responseElement.style.display = "none";

				promptsOuter.style.display = "none";
				if (modelId !== null && prodId !== null) {
					var PICModels = $scope.ModelGroup;
					if (PICModels !== undefined) {
						for (var i = 0; i < PICModels.length; i++) {
							if (PICModels[i].Model === modelId) {
								$scope.ModelDescription = PICModels[i].Description;
								if (PICModels[i].WarningMessage !== '') {
									var loadingElement = document.getElementById("modelWarning");
									loadingElement.innerHTML = PICModels[i].WarningMessage;
									$("#ModelGrpError").show();
								}
							}
						}
					}
					if (prodId !== null && prodId !== undefined && modelId !== null && modelId !== undefined) {
						$scope.setDatasource(prodId, modelId);
					}

					else {
						var loadingElement = document.getElementById("loading");
						loadingElement.style.display = "none";
					}
				}
			}
			function calcMeasurement(actual, frac) {
				var act = Number(actual);
				if (frac != null && frac !== undefined && frac !== '') {
					act = act + Number(eval(frac));
				}
				return act;
			}
			$scope.CreateConfiguration = function (promptAnswers, PrdDatasource, modelId, configEngine, fromQuote, quantity) {
				$scope.newconfig == false;
				//promptAnswers = $scope.PrdPromptAnswers123;
				var mesu = null;//JSON.parse(localStorage.getItem("SelectedMeasurements"));
				if (!fromQuote) {
					if ($scope.SelectedMeasurements !== null) {
						mesu = $scope.SelectedMeasurements;
					}
					else {
						var grid = $("#gridEditMeasurements").data("kendoGrid");
						var selectedItem = grid.dataItem(grid.select());
						if (selectedItem != null) {
							mesu = selectedItem;
						}
					}
				}
				$scope.Quantity = quantity;
				//var promptAnswers = $scope.PrdPromptAnswers;//JSON.parse(localStorage.getItem("PrdPromptAnswers"));

				var now = this;
				//$scope.configEngineObj = new ConfigEngine();
				$scope.configEngineObj = configEngine;
				//if ($scope.newconfig === true) {
				//    $scope.configEngineObj = new ConfigEngine();
				//} else {
				//    if (now.configEngineObj !== undefined) {
				//        $scope.configEngineObj = now.configEngineObj;
				//    }
				//    else {
				//        $scope.configEngineObj = new ConfigEngine();
				//    }

				//}

				//var PrdDatasource = $scope.PrdDatasource;//JSON.parse(localStorage.getItem("PrdDatasource"));

				//var datasource = PrdDatasource.dataSource;
				var datasource;
				//using the safeparsor only for Ipad and Springs
				if (/iPad|iPhone/.test(navigator.userAgent) && ($scope.selectedVendor === 9968)) {
					var dt = '{"name":"John","age":31,"city":"New York"}';
					var dtj = safeJSONParse(dt);
					datasource = safeJSONParse(PrdDatasource.dataSource);
				}
				else {
					datasource = JSON.parse(PrdDatasource.dataSource);
				}
				//var productInfo = PrdDatasource.productInfo;

				//$scope.heightPrompt = productInfo.heightPrompt;
				//$scope.widthPrompt = productInfo.widthPrompt;

				//$scope.mountPrompt = productInfo.mountPrompt;
				//$scope.roomPrompt = productInfo.roomPrompt;

				$scope.configPresentationObj = new ConfigPresentation('Prompts', $scope.QuotelineData);

				$scope.configEngineObj.autoClampRanges = false;
				// link instances
				$scope.configPresentationObj.configEngine = $scope.configEngineObj;
				$scope.configEngineObj.presentation = $scope.configPresentationObj;

				var promptsOuter = document.getElementById("promptsOuter");
				//var dataSourceAjax = ds.then(function (responseText) {
				var loadingElement = document.getElementById("loading");
				//promptsOuter.style.display = "block";
				//promptsOuter.style.display = "block";
				loadingElement.style.display = "block";
				$scope.configEngineObj.dataSource = datasource;

				$scope.configEngineObj.initPresentation();
				//$scope.configEngineObj.loadPromptAnswers(promptAnswers);
				// loadingElement.style.display = "none";

				angular.forEach($scope.configEngineObj.presentation.prompts, function (value, key) {
					if (value["type"] == "T") {
						$("#" + key).keyup(function (event) {
							$scope.RemoveUnwantedChars(event);
						});
					}
				});
				if (!fromQuote) {
					if (mesu != undefined && mesu != null && mesu !== "") {

						if ($scope.roomPrompt == 0 || $scope.widthPrompt == 0 || $scope.heightPrompt == 0) {
							var prodlist = $scope.Product;
							if (prodlist) {
								for (var i = 0; i < prodlist.length; i++) {
									if (prodlist[i].PICProductId == $scope.SelectedPICProduct) {
										$scope.heightPrompt = prodlist[i].HeightPrompt;
										$scope.widthPrompt = prodlist[i].WidthPrompt;
										$scope.mountPrompt = prodlist[i].MountPrompt;
										$scope.roomPrompt = prodlist[i].RoomPrompt;
										$scope.colorPrompt = prodlist[i].ColorPrompt;
										$scope.fabricPrompt = prodlist[i].FabricPrompt;
									}
								}
							}
						}

						//WIDTH PROMPT
						promptAnswers["Prompt" + $scope.widthPrompt] = {
							"value": calcMeasurement(mesu.Width, mesu.FranctionalValueWidth),
							"colorValue": ""
						};
						//HEIGHT PROMPT
						promptAnswers["Prompt" + $scope.heightPrompt] = {
							"value": calcMeasurement(mesu.Height, mesu.FranctionalValueHeight),
							"colorValue": ""
						};

						//ROOM PROMPT
						if (mesu.RoomName != undefined &&
							mesu.RoomName != '' &&
							mesu.RoomName != null) {
							promptAnswers["Prompt" + $scope.roomPrompt] = {
								"value": mesu.RoomName.replace(/\&/g, "And").replace(/\'/g, "").replace(/\"/g, ""),
								"colorValue": ""
							};
						}
						//MOUNT PROMPT
						if (mesu.MountTypeName != undefined &&
							mesu.MountTypeName != '' &&
							mesu.MountTypeName != null) {
							var mount;
							if (mesu.MountTypeName === "IB") {
								mount = "I";
							} else if (mesu.MountTypeName === "OB") {
								mount = "O";
							}
							promptAnswers["Prompt" + $scope.mountPrompt] =
								{ "value": mount, "colorValue": "" };
						}
					}
				}

				//$scope.PrdPromptAnswers = promptAnswers;
				$scope.configEngineObj.dataSource.prompts.Data10.value = modelId;

				$scope.configEngineObj.loadPromptAnswers(promptAnswers, function () {
					loadingElement.style.display = "block";
					promptsOuter.style.display = "block";
					loadingElement.style.display = "none";
				});
				promptsOuter.style.display = "block";
				var panelBar = $("#panelbar").data("kendoPanelBar");
				panelBar.expand($("#ConfigPanel"));
				var numerictextbox = $("#kenqty").data("kendoNumericTextBox");
				if (numerictextbox !== undefined && numerictextbox != null) {
					numerictextbox.value(quantity);
				}
				$scope.SelectedMeasurements = mesu;
				$('select.kendoiodrpdn').select2();
				// enable change event for input elements as number to eliminate the negative character
				$(':input[type="number"]').change(function (e) {
					var value = e.target.value;
					if (value < 0) {
						value = value * (-1);
					}
					$(e.target).val(value);
				});
				$(':input[type="number"]').keyup(function (e) {
					var value = e.target.value;
					if (value < 0) {
						value = value * (-1);
					}
					$(e.target).val(value);
				});
			}

			$scope.RemoveUnwantedChars = function (controlId) {
				var cId = controlId.currentTarget.id;
				var value = $("#" + cId).val();
				if (value.indexOf('&') != -1) $("#" + cId).val(value.replace(/\&/g, "And"));
				if (value.indexOf('\'') != -1) $("#" + cId).val(value.replace(/\'/g, ""));
				if (value.indexOf('\"') != -1) $("#" + cId).val(value.replace(/\"/g, ""));
			}

			//$(".kendoiodrpdn").kendoDropDownList();
			//Kendo grid search CLear
			$scope.ClearSearch = function () {
				$('#searchBox').val('');
				$("#VendorGrid").data('kendoGrid').dataSource.filter({
				});

				if ($scope.selectedElement && $scope.selectedElement.length != 0) {
					var uid = $($scope.selectedElement[0]).attr("data-uid");
					if ($scope.Tempuid == uid)
						var template = "tr[data-uid=" + uid + "]";
					else var template = "tr[data-uid=" + $scope.Tempuid + "]";
					$(template).addClass("k-state-selected");
					// $("#VendorGrid").data("kendoGrid").select(template);
				}
			}

			//Kendo Grid Search
			$scope.VendorAndProductSearch = function () {
				$(".target").hide();
				var loadingElement = document.getElementById("loading");
				loadingElement.style.display = "block";
				var searchValue = $('#searchBox').val();
				var selectedRow = $("#VendorGrid .k-state-selected");
				if (selectedRow && selectedRow.length != 0) {
					$scope.selectedElement = selectedRow;
					$scope.uid = $($scope.selectedElement[0]).attr("data-uid");
				}
				$("#VendorGrid").data("kendoGrid").dataSource.filter({
					logic: "or",
					filters: [
						{
							field: "VendorName",
							operator: "contains",
							value: searchValue
						},
						{
							field: "PICProductGroupName",
							operator: "contains",
							value: searchValue
						}
					]
				});
				if ($scope.selectedElement && $scope.selectedElement.length != 0) {
					var uid = $($scope.selectedElement[0]).attr("data-uid");
					var template = "tr[data-uid=" + uid + "]";
					$(template).addClass("k-state-selected");
					// $("#VendorGrid").data("kendoGrid").select(template);
				}
				loadingElement.style.display = "none";
			}

			$scope.MeasurementClearSearch = function () {
				$('#searchBoxmeasu').val('');
				$("#gridEditMeasurements").data('kendoGrid').dataSource.filter({
				});
				if ($scope.selectedMeasurementElement && $scope.selectedMeasurementElement.length != 0) {
					var uid = $($scope.selectedMeasurementElement[0]).attr("data-uid");
					if ($scope.measurementUid == uid)
						var template = "tr[data-uid=" + uid + "]";
					else
						var template = "tr[data-uid=" + $scope.measurementUid + "]";
					$(template).addClass("k-state-selected");
					// $("#gridEditMeasurements").data("kendoGrid").select(template);
				}
			}

			//Kendo Grid Search
			$scope.MeasurementSearch = function () {
				$(".target").hide();
				var loadingElement = document.getElementById("loading");
				loadingElement.style.display = "block";
				var searchValue = $('#searchBoxmeasu').val();
				var selectedRow = $("#gridEditMeasurements .k-state-selected");
				if (selectedRow && selectedRow.length != 0) {
					$scope.selectedMeasurementElement = selectedRow;
				}
				$("#gridEditMeasurements").data("kendoGrid").dataSource.filter({
					logic: "or",
					filters: [
						{
							field: "RoomName",
							operator: "contains",
							value: searchValue
						},
						{
							field: "MountTypeName",
							operator: "contains",
							value: searchValue
						},
						{
							field: "Width",
							operator: "contains",
							value: searchValue
						},
						{
							field: "Height",
							operator: "contains",
							value: searchValue
						},
						{
							field: "FranctionalValueWidth",
							operator: "contains",
							value: searchValue
						},
						{
							field: "MountTypeName",
							operator: "contains",
							value: searchValue
						}
					]
				});

				if ($scope.selectedMeasurementElement && $scope.selectedMeasurementElement.length != 0) {
					var uid = $($scope.selectedMeasurementElement[0]).attr("data-uid");
					var template = "tr[data-uid=" + uid + "]";
					$(template).addClass("k-state-selected");
					// $("#gridEditMeasurements").data("kendoGrid").select(template);
				}
				loadingElement.style.display = "none";
			}

			function gcd(a, b) {
				if (b < 0.0000001) return a; // Since there is a limited precision we need to limit the value.

				return gcd(b, Math.floor(a % b)); // Discard any fractions due to limitations in precision.
			};

			$scope.getFraction = function (frac) {
				if (frac) {
					var fraction = "0." + frac;

					var len = fraction.toString().length - 2;

					var denominator = Math.pow(10, len);
					var numerator = fraction * denominator;

					var divisor = gcd(numerator, denominator);
					numerator /= divisor;
					denominator /= divisor;

					return (Math.floor(numerator) + '/' + Math.floor(denominator));
				} else {
					return;
				}
			}

			function ConfigPresentation(containerId, quoteLineData) {
				this.quoteLineData = quoteLineData;
				this.configEngine = null;
				this.container = document.getElementById(containerId);
				this.prompts = {};

				this.stockInquireUrl = null;
				this.stockSelectUrl = null;

				this.combineColor = true;
				this.showCodeValue = false;
				this.showCodeColorValue = false;

				this.initialize = function () {
					this.prompts = {};
					this.container.innerHTML = "";
				}
				this.createPrompt = function (promptId, properties, flag) {
					//

					var self = this;
					this.prompts[promptId] = properties; // properties is expected to contain: {description,type,decPlaces,fraction,fromValue,toValue,manuallySet}
					//var prompptTable = document.createElement("table");
					var promptHeader;
					var promptRow;
					var inputColumn;
					if (flag) {
						promptRow = this.prompts[promptId].row;
						$(promptRow).html("");
					} else {
						promptRow = document.createElement("tr");
					}


					//var promptRow = document.createElement("tr");
					var promptHeader = document.createElement("th");
					promptHeader.style.width = "20%";
					promptHeader.style.textAlign = "right";

					//promptHeader.title = promptId;
					promptHeader.innerHTML = properties.description;
					promptRow.appendChild(promptHeader);

					//var column = document.createElement("td");
					var inputColumn = document.createElement("td");
					//var inputSpan = document.createElement("span");
					//inputSpan.className = "span" + promptId;
					//inputElement.className = promptId;
					//inputColumn.id = promptId;
					inputColumn.className = "col-sm-12 textbox-lmargin " + " div" + promptId;


					switch (properties.type) {
						case "A":
							var inputElement = document.createElement("textArea");
							inputElement.className += "prompt form-control frm_controllead kendoText";
							inputElement.id = promptId;
							inputElement.onchange = function () { self.writeBackPrompt(promptId, this.value); };
							if (properties.fromValue > 0) inputElement.cols = properties.fromValue;
							if (properties.toValue > 0) inputElement.rows = properties.toValue;
							inputColumn.appendChild(inputElement);
							break;
						case "T":
							var inputElement = document.createElement("input");
							inputElement.type = "text";
							inputElement.className += "prompt form-control frm_controllead kendoioTex";
							inputElement.id = promptId;
							inputElement.onchange = function () { self.writeBackPrompt(promptId, this.value); };
							inputColumn.appendChild(inputElement);
							break;
						case "N":
							var inputElement = document.createElement("input");
							inputElement.type = "number";
							inputElement.className = "numberPrompt";
							inputElement.className += "prompt form-control frm_controllead kendoionum";
							inputElement.id = promptId;
							inputColumn.appendChild(inputElement);
							inputColumn.style.display = "inline-flex";

							var fractionOptions = {
								"N": [],
								"S": [{ v: ".0", d: "" }, { v: ".0625", d: "1/16" }, { v: ".125", d: "1/8" }, { v: ".1875", d: "3/16" }, { v: ".25", d: "1/4" }, { v: ".3125", d: "5/16" }, { v: ".375", d: "3/8" }, { v: ".4375", d: "7/16" }, { v: ".5", d: "1/2" }, { v: ".5625", d: "9/16" }, { v: ".625", d: "5/8" }, { v: ".6875", d: "11/16" }, { v: ".75", d: "3/4" }, { v: ".8125", d: "13/16" }, { v: ".875", d: "7/8" }, { v: ".9375", d: "15/16" }],
								"E": [{ v: ".0", d: "" }, { v: ".125", d: "1/8" }, { v: ".25", d: "1/4" }, { v: ".375", d: "3/8" }, { v: ".5", d: "1/2" }, { v: ".625", d: "5/8" }, { v: ".75", d: "3/4" }, { v: ".875", d: "7/8" }],
								"F": [{ v: ".0", d: "" }, { v: ".2", d: "1/5" }, { v: ".4", d: "2/5" }, { v: ".6", d: "3/5" }, { v: ".8", d: "4/5" }],
								"Q": [{ v: ".0", d: "" }, { v: ".25", d: "1/4" }, { v: ".5", d: "1/2" }, { v: ".25", d: "3/4" }],
								"H": [{ v: ".0", d: "" }, { v: ".5", d: "1/2" }],
								"Y": [],
								"": []
							}[properties.fraction];
							if (fractionOptions.length) {
								var fractionElement = document.createElement("select");
								for (var i = 0, n = fractionOptions.length; i < n; i++) {
									var fractionOption = document.createElement("option");
									fractionOption.value = fractionOptions[i].v;
									fractionOption.innerHTML = fractionOptions[i].d;
									fractionElement.style.height = "34px";
									fractionElement.style.width = "30%";
									fractionElement.appendChild(fractionOption);
								}
								inputColumn.appendChild(fractionElement);
								fractionElement.onchange = inputElement.onchange = function () {
									var writeValue;
									//if (inputElement.value.indexOf(".") != -1) {
									writeValue = inputElement.value;
									//}
									//else {
									//	writeValue = inputElement.value + fractionElement.value;
									//}
									var index = inputElement.value.indexOf(".");
									if (index != -1) {
										var fulValueString = writeValue.substr(0, index);
										var decimalValueString = writeValue.substr(index, writeValue.length);
										var decimalValue = parseFloat(decimalValueString);
										var fullValue = parseInt(fulValueString);

										//if (decimalValue > 0 && decimalValue < 0.125) { fullValue = fullValue }
										//else if (decimalValue > 0 && decimalValue < 0.25) { fullValue = fullValue + 0.125 } //1/8
										//else if (decimalValue < 0.375) { fullValue = fullValue + 0.25; }  // 1/4
										//else if (decimalValue < 0.5) { fullValue = fullValue + 0.375; }  // 3/8
										//else if (decimalValue < 0.625) { fullValue = fullValue + 0.5; }   // 1/2
										//else if (decimalValue < 0.75) { fullValue = fullValue + 0.625; } // 5/8
										//else if (decimalValue < 0.875) { fullValue = fullValue + 0.75; }  // 3/4
										//else if (decimalValue < 1) { fullValue = fullValue + 0.875; }  // 7/8
										//else { fullValue = fullValue + 0.00; }

										if (properties.fraction === "S") {
											if (decimalValue > 0 && decimalValue < 0.0624) { fullValue = fullValue }
											else if (decimalValue >= 0.0624 && decimalValue < 0.125) { fullValue = fullValue + 0.0625 }  // 1/16
											else if (decimalValue >= 0.125 && decimalValue < 0.1875) { fullValue = fullValue + 0.125 } //1/8
											else if (decimalValue >= 0.1875 && decimalValue < 0.25) { fullValue = fullValue + 0.1875; } // 1/16

											else if (decimalValue >= 0.25 && decimalValue < 0.3125) { fullValue = fullValue + 0.25; } //1/4
											else if (decimalValue >= 0.3125 && decimalValue < 0.375) { fullValue = fullValue + 0.3125; }  // 5/16
											else if (decimalValue >= 0.375 && decimalValue < 0.4375) { fullValue = fullValue + 0.375; }  // 3/8
											else if (decimalValue >= 0.4375 && decimalValue < 0.5) { fullValue = fullValue + 0.4375; }  // 7/16
											else if (decimalValue >= 0.5 && decimalValue < 0.5625) { fullValue = fullValue + 0.5; }   // 1/2
											else if (decimalValue >= 0.5625 && decimalValue < 0.625) { fullValue = fullValue + 0.5625; }  // 9/16
											else if (decimalValue >= 0.625 && decimalValue < 0.6875) { fullValue = fullValue + 0.625; }  // 5/8
											else if (decimalValue >= 0.6875 && decimalValue < 0.75) { fullValue = fullValue + 0.6875; }   // 11/16
											else if (decimalValue >= 0.75 && decimalValue < 0.8125) { fullValue = fullValue + 0.75; }  // 3/4
											else if (decimalValue >= 0.8125 && decimalValue < 0.875) { fullValue = fullValue + 0.8125; }   //13/16
											else if (decimalValue >= 0.875 && decimalValue < 0.9375) { fullValue = fullValue + 0.875; }  // 7/8
											else if (decimalValue >= 0.9375 && decimalValue < 1) { fullValue = fullValue + 0.9375; }    // 15/16
											else { fullValue = fullValue + 0.00; }
											//else if (decimalValue < 0.375) { fullValue = fullValue + 0.25; }  // 1/4
											//else if (decimalValue < 0.5) { fullValue = fullValue + 0.375; }  // 3/8
											//else if (decimalValue < 0.625) { fullValue = fullValue + 0.5; }   // 1/2
											//else if (decimalValue < 0.75) { fullValue = fullValue + 0.625; } // 5/8
											//else if (decimalValue < 0.875) { fullValue = fullValue + 0.75; }  // 3/4
											//else if (decimalValue < 1) { fullValue = fullValue + 0.875; }  // 7/8
											//else { fullValue = fullValue + 0.00; }
										} else if (properties.fraction === "E") {
											if (decimalValue > 0 && decimalValue < 0.125) { fullValue = fullValue }
											else if (decimalValue >= 0.125 && decimalValue < 0.25) { fullValue = fullValue + 0.125 } //1/8
											else if (decimalValue >= 0.25 && decimalValue < 0.375) { fullValue = fullValue + 0.25; }  // 1/4
											else if (decimalValue >= 0.375 && decimalValue < 0.5) { fullValue = fullValue + 0.375; }  // 3/8
											else if (decimalValue >= 0.5 && decimalValue < 0.625) { fullValue = fullValue + 0.5; }   // 1/2
											else if (decimalValue >= 0.625 && decimalValue < 0.75) { fullValue = fullValue + 0.625; } // 5/8
											else if (decimalValue >= 0.75 && decimalValue < 0.875) { fullValue = fullValue + 0.75; }  // 3/4
											else if (decimalValue >= 0.875 && decimalValue < 1) { fullValue = fullValue + 0.875; }  // 7/8
											else { fullValue = fullValue + 0.00; }
										}
										else if (properties.fraction === "F") {
											if (decimalValue > 0 && decimalValue < 0.2) { fullValue = fullValue }
											else if (decimalValue >= 0.2 && decimalValue < 0.4) { fullValue = fullValue + 0.2 } //1/5
											else if (decimalValue >= 0.4 && decimalValue < 0.6) { fullValue = fullValue + 0.4; }  // 2/5
											else if (decimalValue >= 0.6 && decimalValue < 0.8) { fullValue = fullValue + 0.6; }  // 3/5
											else if (decimalValue >= 0.8 && decimalValue < 1) { fullValue = fullValue + 0.8; }   // 4/5
											else { fullValue = fullValue + 0.00; }
										}
										else if (properties.fraction === "Q") {
											if (decimalValue > 0 && decimalValue < 0.25) { fullValue = fullValue }
											else if (decimalValue >= 0.25 && decimalValue < 0.5) { fullValue = fullValue + 0.25 }
											else if (decimalValue >= 0.5 && decimalValue < 0.75) { fullValue = fullValue + 0.5; }
											else if (decimalValue >= 0.75 && decimalValue < 1) { fullValue = fullValue + 0.75; }
											else { fullValue = fullValue + 0.00; }
										}
										else if (properties.fraction === "H") {
											if (decimalValue > 0 && decimalValue < 0.5) { fullValue = fullValue }
											else if (decimalValue >= 0.5 && decimalValue < 1) { fullValue = fullValue + 0.5 }
											else { fullValue = fullValue + 0.00; }
										}
										else {
											if (this.className === "" || !(this.className.includes("numberPromptprompt")))
												writeValue = inputElement.value + fractionElement.value;
											self.writeBackPrompt(promptId, writeValue);
										}
									}
									else {
										if (this.className === "" || !(this.className.includes("numberPromptprompt")))
											writeValue = inputElement.value + fractionElement.value;
									}
									self.writeBackPrompt(promptId, writeValue);
								};
							}
							else {
								inputElement.onchange = function () { self.writeBackPrompt(promptId, this.value); };
							}

							break;
						case "M":
							if (properties.decPlaces == "0") {
								var inputElement = document.createElement("input");
								inputElement.type = "checkbox";
								inputElement.onchange = function () { self.writeBackPrompt(promptId, this.checked); };
								inputColumn.appendChild(inputElement);
							}
							else if (properties.decPlaces == "1" || properties.decPlaces == "2") {
								var inputElement = document.createElement("select");
								if (properties.decPlaces == "2") inputElement.size = 6;
								inputColumn.appendChild(inputElement);
								var colorInputElement = document.createElement("select");
								if (properties.decPlaces == "2") colorInputElement.size = 6;
								inputColumn.appendChild(colorInputElement);

								colorInputElement.onchange = inputElement.onchange = function () {
									self.writeBackPrompt(promptId, inputElement.value, colorInputElement.value);
								};
							}
							break;
						case "C":
							if (properties.decPlaces == "0") {
								var inputElement = document.createElement("input");
								inputElement.type = "checkbox";
								inputElement.className += " prompt ";
								inputElement.id = promptId;
								inputElement.style.height = "20px !important";
								inputElement.onchange = function () { self.writeBackPrompt(promptId, this.checked); };
								inputColumn.appendChild(inputElement);
							}
							else if (properties.decPlaces == "1" || properties.decPlaces == "2") {
								if (this.combineColor) {
									var inputElement = document.createElement("select");
									if (properties.decPlaces == "2") inputElement.size = 6;
									inputElement.onchange = function () { self.writeBackPrompt(promptId, this.value); };
									inputColumn.appendChild(inputElement);

									inputElement.onchange = inputElement.onchange = function () {
										var combinedValues = this.value.split(/\|/g);
										var color = combinedValues.pop();
										var value = combinedValues.join("|");

										self.writeBackPrompt(promptId, value, color);
									};
								}
								else {
									var inputElement = document.createElement("select");
									if (properties.decPlaces == "2") inputElement.size = 6;
									inputColumn.appendChild(inputElement);
									var colorInputElement = document.createElement("select");
									if (properties.decPlaces == "2") colorInputElement.size = 6;
									inputColumn.appendChild(colorInputElement);

									colorInputElement.onchange = inputElement.onchange = function () {
										self.writeBackPrompt(promptId, inputElement.value, colorInputElement.value);
									};
								}
							}
							break;
						case "L":
						default:
							inputColumn.innerHTML = "Unsupported type: " + properties.type;
					}
					//inputColumn.append(inputspan);

					//inputColumn.appendChild(inputElement);
					//column.appendChild(inputColumn);

					//inputColumn.appendChild(column);



					if (!flag) {
						promptRow.appendChild(inputColumn);
						this.prompts[promptId].row = promptRow;
						this.container.appendChild(promptRow);
					} else {
						promptRow.appendChild(inputColumn);
					}
					//prompptTable.appendChild(this.container);
				}

				// to set possible values of the input on the presentation layer
				this.setPromptOptions = function (promptId, options, colorOptions) {
					var self = this;
					if (this.prompts[promptId].decPlaces == "-1") {
						var radioContainer = this.prompts[promptId].row.childNodes[1];
						radioContainer.innerHTML = "";

						for (var i = 0, n = options.length; i < n; i++) {
							var labelElement = document.createElement("label");

							var inputElement = document.createElement("input");
							inputElement.type = "radio";
							inputElement.name = promptId;
							inputElement.value = options[i].value;
							inputElement.className += " prompt ";

							inputElement.id = promptId;
							inputElement.onchange = function () {
								var combinedValues = this.value.split(/\|/g);
								self.writeBackPrompt(promptId, combinedValues[0], combinedValues[1]);
							};
							labelElement.appendChild(inputElement);

							var spanElement = document.createElement("span");
							spanElement.innerHTML = options[i].description;
							labelElement.appendChild(spanElement);

							radioContainer.appendChild(labelElement);
						}
					}
					else if (this.prompts[promptId].decPlaces == "1" || this.prompts[promptId].decPlaces == "2") {
						if (this.combineColor && this.prompts[promptId].type != 'M') {
							// combined values are assumed
							var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
							var tagName = $(inputElement)[0].tagName;
							if (tagName != "DIV") {
								inputElement.className += " prompt kendoiodrpdn";
								inputElement.id = promptId;
								inputElement.innerHTML = "";

								for (var i = 0, n = options.length; i < n; i++) {
									var optionElement = document.createElement("option");
									optionElement.value = options[i].value;
									optionElement.innerHTML = options[i].description;
									inputElement.appendChild(optionElement);
								}
							} else {

								// reinitialize the select 2
								$("#" + promptId).remove();
								$("#s2id_" + promptId).remove();
								this.createPrompt(promptId, this.prompts[promptId], true);
								var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
								inputElement.className += " prompt kendoiodrpdn select2dropdown";
								inputElement.id = promptId;
								inputElement.innerHTML = "";

								for (var i = 0, n = options.length; i < n; i++) {
									var optionElement = document.createElement("option");
									optionElement.value = options[i].value;
									optionElement.innerHTML = options[i].description;
									if ($scope.selectedValue && $scope.selectedValue == options[i].value) {
										$(optionElement).attr("selected", "true");
										$scope.selectedValue = null;
									}
									inputElement.appendChild(optionElement);
								}
								// $("#" + promptId).select2();
							}
						}
						else {
							var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
							var tagName = $(inputElement)[0].tagName;
							if (tagName != "DIV") {
								inputElement.className += " prompt kendoiodrpdn";
								inputElement.id = promptId;
								inputElement.innerHTML = "";
								for (var i = 0, n = options.length; i < n; i++) {
									var optionElement = document.createElement("option");
									optionElement.value = options[i].value;
									optionElement.innerHTML = options[i].description;
									inputElement.appendChild(optionElement);
								}

								var colorInputElement = this.prompts[promptId].row.childNodes[1].childNodes[1];
								inputElement.className += " prompt kendoiodrpdn";
								inputElement.id = promptId;
								colorInputElement.innerHTML = "";
								for (var i = 0, n = colorOptions.length; i < n; i++) {
									var optionElement = document.createElement("option");
									optionElement.value = colorOptions[i].value;
									optionElement.innerHTML = colorOptions[i].description;
									colorInputElement.appendChild(optionElement);
								}
								if (colorOptions.length) {
									colorInputElement.style.display = "inline";
								}
								else {
									colorInputElement.style.display = "none";
								}
							} else {

								// reinitialize the select 2
								$("#s2id_" + promptId).remove();
								$("#" + promptId).remove();
								this.createPrompt(promptId, this.prompts[promptId], true);
								var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
								inputElement.className += " prompt kendoiodrpdn select2dropdown";
								inputElement.id = promptId;
								inputElement.innerHTML = "";
								for (var i = 0, n = options.length; i < n; i++) {
									var optionElement = document.createElement("option");
									optionElement.value = options[i].value;
									optionElement.innerHTML = options[i].description;
									if ($scope.selectedValue && $scope.selectedValue == options[i].value) {
										$(optionElement).attr("selected", "true");
										$scope.selectedValue = null;
									}
									inputElement.appendChild(optionElement);
								}

								var colorInputElement = this.prompts[promptId].row.childNodes[1].childNodes[1];
								inputElement.className += " prompt kendoiodrpdn select2dropdown";
								inputElement.id = promptId;
								colorInputElement.innerHTML = "";
								for (var i = 0, n = colorOptions.length; i < n; i++) {
									var optionElement = document.createElement("option");
									optionElement.value = colorOptions[i].value;
									optionElement.innerHTML = colorOptions[i].description;
									if ($scope.selectedValue && $scope.selectedValue == options[i].value) {
										$(optionElement).attr("selected", "true");
										$scope.selectedValue = null;
									}
									colorInputElement.appendChild(optionElement);
								}
								if (colorOptions.length) {
									colorInputElement.style.display = "inline";
								}
								else {
									colorInputElement.style.display = "none";
								}
								// $("#" + promptId).select2();
							}
						}
					}
				}

				// to set value of the input on the presentation layer
				this.setPrompt = function (promptId, value, valueDescription, color, manuallySet) { // TODO: accept manuallySet so that we can reasonably display and manage it from the front end
					this.prompts[promptId].manuallySet = !!manuallySet;

					switch (this.prompts[promptId].type) {
						case "A":
							var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
							inputElement.className += " prompt kendoiodrpdn";
							inputElement.id = promptId;
							inputElement.innerText = value;
							break;
						case "T":
							var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
							inputElement.className += " prompt kendoiodrpdn";
							inputElement.id = promptId;
							inputElement.value = value;
							break;
						case "N":
							var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
							inputElement.className += " prompt kendoiodrpdn";
							inputElement.id = promptId;
							var fractionElement = this.prompts[promptId].row.childNodes[1].childNodes[1];

							if (typeof (fractionElement) !== "undefined") {
								inputElement.value = Math.floor(value);

								var fractionValue = ((value - Math.floor(value)) + "").replace(/^0/, "");
								fractionElement.value = fractionValue;
							}
							else {
								inputElement.value = value;
							}
							//self.writeBackPrompt(promptId, value, "");
							break;
						case "M":
							if (this.prompts[promptId].decPlaces == "-1") {
								var radioContainer = this.prompts[promptId].row.childNodes[1];
								for (var i = 0, n = radioContainer.childNodes.length; i < n; i++) {
									var inputElement = radioContainer.childNodes[i].childNodes[0];
									inputElement.checked = inputElement.value == value + "|" + color;
								}
							}
							else if (this.prompts[promptId].decPlaces == "0") {
								var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
								inputElement.checked = value;
							}
							else if (this.prompts[promptId].decPlaces == "1" || this.prompts[promptId].decPlaces == "2") {
								var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
								inputElement.value = value;
								var colorInputElement = this.prompts[promptId].row.childNodes[1].childNodes[1];
								colorInputElement.value = color;
							}
							break;
						case "C":
							if (this.prompts[promptId].decPlaces == "-1") {
								var radioContainer = this.prompts[promptId].row.childNodes[1];
								for (var i = 0, n = radioContainer.childNodes.length; i < n; i++) {
									var inputElement = radioContainer.childNodes[i].childNodes[0];
									inputElement.className += " prompt kendoiodrpdn";
									inputElement.id = promptId;
									inputElement.checked = inputElement.value == value + "|" + color;
								}
							}
							else if (this.prompts[promptId].decPlaces == "0") {
								var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
								inputElement.className += " prompt kendoiodrpdn";
								inputElement.id = promptId;
								inputElement.checked = value;
							}
							else if (this.prompts[promptId].decPlaces == "1" || this.prompts[promptId].decPlaces == "2") {
								if (this.combineColor) {
									var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
									inputElement.className += " prompt kendoiodrpdn";
									inputElement.id = promptId;
									inputElement.value = value + "|" + color;
								}
								else {
									var inputElement = this.prompts[promptId].row.childNodes[1].childNodes[0];
									inputElement.className += " prompt kendoiodrpdn";
									inputElement.id = promptId;
									inputElement.value = value;

									var colorInputElement = this.prompts[promptId].row.childNodes[1].childNodes[1];
									colorInputElement.value = color;
								}
							}
							break;
						default:
							break;
					}
					// validate the select box to apply the select2
					var element = $("#" + promptId);
					if (element && element.hasClass("select2dropdown")) {
						$("#" + promptId).select2();
					}
				}

				// for change events, to trigger engine to set value and evaluate rules
				this.writeBackPrompt = function (promptId, value, color) {
					//
					if (typeof (color) === "undefined") color = "";

					var self = this;
					// this is for capture the selected value from select2 component
					if (value) {
						$scope.selectedValue = value + "|";
					}
					var loadingElement = document.getElementById("loading");
					//loadingElement.style.display = "block";
					this.configEngine.setPromptValue(promptId, value, color, true, false, function () {
						self.configEngine.evaluateRules(function () {
							loadingElement.style.display = "none";
						});
					});
				}

				this.setPromptVisibility = function (promptId, visible) {
					var promptElem = this.prompts[promptId].row;
					if (this.configEngine.dataSource.prompts[promptId].required === true &&
						this.configEngine.dataSource.prompts[promptId].usePresentation === true) {
						promptElem.classList.add("required");
						//promptElem.classList.add(".form-control");
						var input = promptElem.querySelectorAll("input");
						if (input.length > 0) {
							for (var i = 0; i < input.length; i++) {
								if (this.configEngine.dataSource.prompts[promptId].value.trim() == "" || this.configEngine.dataSource.prompts[promptId].value.trim() == "0") {
									//input[i].style.border = " 1px solid red";
									if (input[i].type === "text") {
										input[i].placeholder = "Required";
									}
									input[i].classList.add("errorTP");
									input[i].classList.add("required_field");
								} else {
									//input[i].style.border = " 0px solid red";
									input[i].classList.remove("errorTP");
								}
							}
						}
						var inputSelect = promptElem.querySelectorAll("select");
						if (inputSelect.length > 0) {
							for (var i = 0; i < inputSelect.length; i++) {
								if (this.configEngine.dataSource.prompts[promptId].value.trim() == "" || this.configEngine.dataSource.prompts[promptId].value.trim() == "0") {
									//inputSelect[i].style.border = " 1px solid red";
									inputSelect[i].classList.add("errorTP");
									inputSelect[i].classList.add("required_field");
								}
								else {
									//inputSelect[i].style.border = " 0px solid red";
									inputSelect[i].classList.remove("errorTP");
								}
							}
						}
					}
					if (promptElem) {
						if (visible) {
							promptElem.style.display = "table-row";
						}
						else {
							promptElem.style.display = "none";
						}
					}
				}

				this.message = function (message) {
					// alert(message);
					//var loadingElement = document.getElementById("response");
					//loadingElement.style.display = "block";
					//document.getElementById('#response').innerText = message;
				}

				this.validatePrompts = function () {
					$(".spanPrompt").remove();
					var errorMessages;
					var ele = document.getElementsByClassName('prompt');
					for (var i = 0; i < ele.length; i++) {
						ele[i].style.border = " 1px solid #b2bfca";
					}
					var Messages = this.configEngine.validatePrompts();
					if (Messages.length) {
						var div = " <h4 style='color: red;'>Field Errors</h4><ul>";
						var configerrors = "";
						$scope.FieldErrors = "";
						for (var i = 0; i < Messages.length; i++) {
							if (Messages[i].severity === "E") {
								if (Messages[i].promptId.toString() !== '') {
									var node = $("#" +
										Messages[i]
											.promptId);
									node.css('border', "1px solid red");
									$("<span class='spanPrompt' style='color: red;'>" + Messages[i].message + "</span>").appendTo(".div" + Messages[i].promptId);
									div = div + "<li style='color: red;'>" + Messages[i].message + "</li>";
								} else {
									configerrors = configerrors + "<li style='color: red;'>" + Messages[i].message + "</li>";
								}
								$scope.FieldErrors = $scope.FieldErrors == "" ? Messages[i].message : $scope.FieldErrors + ", " + Messages[i].message;
							}
						}
						if (configerrors !== "") {
							configerrors = " <h4 style='color: red;'>Config Errors</h4><ul>" +
								configerrors + "</ul>";
						}
						div = div + "</ul>";

						errorMessages = $.grep(Messages, function (s) {
							return s.severity == "E";
						});
					}
					if (errorMessages !== undefined && errorMessages.length > 0) {
						document.getElementById("response").innerHTML = div + configerrors;
						var responseElement = document.getElementById("response");
						responseElement.style.display = "block";
						$("#response").css('border', '1px solid red');
						return false;
					} else {
						return true;
					}
				}
			}

			$scope.onMeasurementChangeGrid = function (data) {
				var selectedRow = $("#gridEditMeasurements .k-state-selected");
				if (selectedRow && selectedRow.length != 0) {
					$scope.selectedMeasurementElement = selectedRow;
					$scope.measurementUid = $($scope.selectedMeasurementElement[0]).attr("data-uid");
				}

				var items1 = data.sender.select();
				var grid1 = data.sender;
				var mod1 = grid1.dataItem(this.select());

				if ($scope.SelectedMeasurements != undefined && $scope.SelectedMeasurements != null && mod1 != null && $scope.SelectedMeasurements.uid === mod1.uid) {
					$scope.SelectedMeasurements = null;

					// $(data).removeClass("k-state-selected");
					var grd = $("#gridEditMeasurements").getKendoGrid();
					$scope.measurementChanged = false;
					grd.clearSelection();
					data.preventDefault();
				}
				else {
					if ($scope.measurementChanged) {
						var grid = data.sender;
						var measurement = grid.dataItem(this.select());
						var conf = new ConfigEngine();
						$scope.SelectedMeasurements = measurement;
						$scope.Quantity = $("#kenqty").val();
						var qty = $("#kenqty").val();

						if (qty != undefined && Number(qty) > 0) {
							$scope.Quantity = qty;
						}
						var item = this;
						//Specific to Ipad Not sure why these values are becomming empty here
						//Commented below two lines on 27.11.2019, because its not working in ipad chrome
						if (/iPad|iPhone/.test(navigator.userAgent)) {
							//$scope.SelectedPICProduct=item.$angular_scope.$parent.SelectedPICProduct;
							//$scope.selectedModel=item.$angular_scope.$parent.selectedModel;
						}
						if ($scope.SelectedPICProduct !== '' && $scope.selectedModel !== '') {
							//var loadingElement = document.getElementById("loading");
							//loadingElement.style.display = "block";

							var modelId = item.$angular_scope.selectedModel;
							var prodId = item.$angular_scope.SelectedPICProduct;
							if (item.$angular_scope.configEngineObj !== undefined) {

								$scope.PrdDatasource = item.$angular_scope.PrdDatasource;
								conf = item.$angular_scope.configEngineObj;
								$scope.PrdPromptAnswers = item.$angular_scope.configEngineObj.getPromptAnswers();

								//if (!(/iPad|iPhone/.test(navigator.userAgent))) {
								//	$scope.PrdPromptAnswers = item.$angular_scope.configEngineObj.getPromptAnswers();
								//}
							}
							//if (!(/iPad|iPhone/.test(navigator.userAgent))) {
							//	$scope.PrdDatasource = item.$angular_scope.PrdDatasource;
							//	conf = item.$angular_scope.configEngineObj;
							//}


							if ($scope.PrdPromptAnswers !== null &&
								$scope.PrdPromptAnswers !== undefined &&
								$scope.PrdDatasource !== null &&
								$scope.PrdDatasource !== undefined) {
								$scope.CreateConfiguration($scope.PrdPromptAnswers,
									$scope.PrdDatasource,
									modelId,
									conf, false, $scope.Quantity);
							}
						}

						var responseElement = document.getElementById("response");
						responseElement.innerHTML = '';
						responseElement.style.display = "none";

					} else {
						$scope.measurementChanged = true;
					}
				}

				//var grid = $("#QuotelinesGrid").data("kendoGrid");
				//grid.clearSelection();

				$scope.quotelineflag = false;
				// }
			}

			function safeJSONParse() {
				var typeHints = {
					// object/array/element closing are specially handled
					",": "comma",
					"{": "object",
					"[": "array",
					"\"": "string",
					"-": "number",
					"0": "number",
					"1": "number",
					"2": "number",
					"3": "number",
					"4": "number",
					"5": "number",
					"6": "number",
					"7": "number",
					"8": "number",
					"9": "number",
					// the following "number" characters should never be in the lead of a properly encoded number, but it shouldn't cause a problem so long as the JSON is 100% within spec
					"+": "number",
					".": "number",
					"e": "number",
					"E": "number",
					// assuming your JSON is valid, these should be the only possible tokens starting with these characters
					"t": "true",
					"f": "false",
					"n": "null"
				};
				var simpleEscapes = {
					"\"": "\"",
					"/": "/",
					"b": "\b",
					"f": "\f",
					"n": "\n",
					"r": "\r",
					"t": "\t",
					"\\": "\\"
				};
				var charEscapes = {
					"\"": "\\\"",
					"/": "\\/",
					"\b": "\\b",
					"\f": "\\f",
					"\n": "\\n",
					"\r": "\\r",
					"\t": "\\t",
					"\\": "\\\\"
				};
				function escapeChar(char) { return charEscapes[char]; }
				safeJSONParse = function (objStr) {
					try {
						var topVar = null, encapsulationStack = [], currentString = null, currentNumber = null;
						function set(value) {
							var referencePath = "";
							for (var i = 0, n = encapsulationStack.length; i < n; i++) {
								if (encapsulationStack[i] === "object") {
									referencePath += "[\"" + encapsulationStack[++i].replace(/["\/\b\f\n\r\t\\\\]/g, escapeChar) + "\"]";
								}
								else {
									referencePath += "[" + encapsulationStack[++i] + "]";
								}
							}
							//(new Function("topVar", "value", "topVar"+referencePath+"=value"))(topVar, value);
							eval("topVar" + referencePath + "=value");
						}
						for (var i = 0, n = objStr.length; i < n; i++) {
							var currentToken = objStr.charAt(i);

							if (currentString !== null) {
								if (currentToken !== "\"") { // string hasn't ended yet (escaped quotations will be safely skipped below)
									if (currentToken === "\\") { // escape sequences
										var nextToken = objStr.charAt(i + 1);
										if (simpleEscapes.hasOwnProperty(nextToken)) {
											currentString += simpleEscapes[nextToken];
											i++;
										}
										else if (nextToken === "u") {
											currentString += String.fromCharCode(parseInt(objStr.substring(i + 2, i + 6), 16));
											i += 5; // skip assumed "u0000"
										}
										else {
											currentString += currentToken; // invalid sequence; preserve both slash and character
										}
									}
									else {
										currentString += currentToken;
									}
								}
								else {
									if (!encapsulationStack.length) {
										return currentString; // this is the only possible return path for a top-level encoded string
									}
									else {
										if (encapsulationStack.length % 2 === 1 && encapsulationStack[encapsulationStack.length - 1] === "object") { // note that keys must always be strings, meaning this methodology should always be safe
											encapsulationStack.push(currentString);
											i++; // skip assumed ":"; should be fine even if it somehow had whitespace (not valid JSON) but it also means that you could use any character where there should have been a colon
										}
										else {
											set(currentString);
										}
									}
									currentString = null;
								}
							}
							else {
								if (typeHints.hasOwnProperty(currentToken)) {
									var currentTokenType = typeHints[currentToken];

									if (currentNumber !== null) {
										if (currentTokenType === "number") {
											currentNumber += currentToken;
										}
										else {
											// the number ends when a non-number character is encountered (kinda brittle but should work fine on JSON that is 100% within spec)
											set(parseFloat(currentNumber));
											currentNumber = null;
										}
									}
									switch (currentTokenType) {
										case "comma":
											if (encapsulationStack[encapsulationStack.length - 2] === "object") {
												encapsulationStack.pop();
											}
											else {
												var currentIndex = encapsulationStack.pop();
												encapsulationStack.push(currentIndex + 1);
											}
											break;
										case "object":
											set({});
											encapsulationStack.push("object");
											break;
										case "array":
											set([]);
											encapsulationStack.push("array");
											encapsulationStack.push(0);
											break;
										case "string":
											currentString = "";
											break;
										case "number":
											if (currentNumber === null) {
												currentNumber = currentToken;
											}
											break;
										case "true":
											set(true);
											i += 3;
											break;
										case "false":
											set(false);
											i += 4;
											break;
										case "null":
											set(null);
											i += 3;
											break;
									}
								}
								else if (currentToken == "}" || currentToken == "]") { // outside of whitespace, these should theoretically be the only possible characters not handled elsewhere
									// spit out dangling number before closing the container
									if (currentNumber !== null) {
										set(parseFloat(currentNumber));
										currentNumber = null;
									}
									encapsulationStack.pop();
									encapsulationStack.pop();
								}
							}
						}

						// this is probably the only possible return path for a top-level encoded number
						if (currentNumber !== null) {
							set(parseFloat(currentNumber));
							currentNumber = null;
						}

						return topVar;
					}
					catch (err) {
						//console.log(err);
					}
				}
			};
			$rootScope.$on('$routeChangeStart', function (event, next, prev) {
				$rootScope.selectedquoteLine = null;
			});
		}
	]);