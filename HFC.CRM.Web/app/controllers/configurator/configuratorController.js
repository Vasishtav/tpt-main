﻿'use strict';
app.controller('configuratorController', [
    '$http', '$scope', '$window', '$location', '$rootScope', '$sce', '$routeParams', 'configuratorService', 'NavbarService', function ($http, $scope, $window, $location, $rootScope, $sce, $routeParams, ConfiguratorService, NavbarService) {
        $scope.SelectedPICVendor = {};
        $scope.SelectedPICProduct = {};

        $scope.selectedModel = {};
        $scope.selectedModelDescription = {};
        $scope.selectedProductGroup = {};
        $scope.selectedProduct = {};
        $scope.selectedProductList = {};
        $scope.selectedVendor = {};
        $scope.configPresentationObj = {};
        $scope.configEngineObj = {};

        $scope.QuoteLineId = $routeParams.quoteLineId;
        $scope.OpportunityId = $rootScope.OpportunityId;
        $scope.quoteKey = $routeParams.quoteKey;
        $scope.Quantity = 1;
        $scope.SavedPromptAnswers = null;
        $scope.picJson = null;
        $scope.Firstmodel = null;

        $scope.NavbarService = NavbarService;

        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesOpportunitiesTab();

        $rootScope.OpportunityId = $scope.OpportunityId;
        $rootScope.quoteKey = $scope.quoteKey;
        $scope.QuotelineData = {};
        $scope.heightPrompt = 0;
        $scope.widthPrompt = 0;
        $scope.mountPrompt = 0;
        $scope.roomPrompt = 0;

        $http.get('/api/Accounts/1/GetAccQuoteNames?OpportunityId=' + $scope.quoteKey).then(function (data) {
            if (data.data != null)
            { $scope.AccountName = data.data[0]; $scope.QuoteName = data.data[1]; }
            else
            {
                $scope.AccountName = ''; $scope.QuoteName = '';
            }
        });

        $scope.ProductGroup = {
            dataSource: {
                transport: {
                    read: {
                        url: '/api/PIC/0/GetPICProductGroup',
                    },
                },
                error: function (e) {
                    HFC.DisplayAlert(e.errorThrown);
                },
                schema: {
                    model: {
                        id: "PICProductGroupId",
                        fields: {
                            PICVendorId: { type: "string", editable: false, visable: false },
                            VendorId: { type: "number", editable: false, visable: false },
                            VendorName: { type: "string" },
                            PICProductGroupId: { type: "number", editable: false, visable: false },
                            PICProductGroupName: { type: "string" },
                            PICProductList: { editable: false, visable: false }
                        }
                    }
                }
            },
            columns: [
                                { field: "VendorName", title: "Vendor" },
                                { field: "PICProductGroupName", title: "Product Category" }
            ],
            change: onChangeGrid,
            noRecords: true,
            messages: {
                noRecords: "No records found"
            },
            dataBound: onDataBound,
            //dataBinding: onDataBinding,
            editable: "popup",
            filterable: true,
            selectable: "row",
            scrollable: {
                virtual: true
            },
            height: 300,
            toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="tp-measurementsearch"><div class="leadsearch_topleft col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="VendorAndProductSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search" style="padding:0 !important;"><input type="button" id="btnReset" ng-click="ClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="clearfix"></div></div></script>').html()) }],
        };

        function onDataBound(data) {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var testGrid = data.sender;
            var newdata = testGrid._data;
            $scope.ProductGroup = data.sender.dataSource.data();
            //for now set timer to preload existing prompt answers
            setTimeout(function () {
                newdata.forEach(function (entry) {
                    if (Number($scope.selectedProductGroup) === Number(entry.PICProductGroupId) &&
                        Number(entry.VendorId) === Number($scope.selectedVendor)) {
                        testGrid.select('tr[data-uid="' + entry.uid + '"]');
                        changegrp();
                        $scope.OnSelectedProduct($scope.SelectedPICProduct);
                        $scope.OnModelSelected();
                    } else {
                        loadingElement.style.display = "none";
                    }
                    loadingElement.style.display = "none";
                });
            },
                1000);
        };

        $scope.GetQuotelineData = function () {
            $http.get('/api/quotes/0/GetQuoteLineByQuoteLineId?quoteLineId=' + $scope.QuoteLineId).then(function (data) {
                var quoteline = JSON.parse(data.data.data);
                var picJson = JSON.parse(quoteline[0].PICJson);
                $scope.QuotelineData = quoteline;
                $scope.picJson = picJson;
                if (picJson != null && picJson != undefined) {
                    $scope.Quantity = picJson.Quantity;
                    $scope.selectedModel = picJson.ModelId;
                    $scope.selectedProductGroup = picJson.GGroup;
                    $scope.selectedProduct = picJson.PicProduct;
                    $scope.SelectedPICProduct = picJson.PicProduct;
                    $scope.Firstmodel = picJson.ModelId;

                    $scope.SelectedPICVendor = picJson.PicVendor;
                    $scope.selectedVendor = picJson.VendorId;
                    if (picJson.PromptAnswers != null) {
                        $scope.SavedPromptAnswers = JSON.parse(picJson.PromptAnswers);
                        var input = {};
                        input.PICProductGroupId = $scope.selectedProductGroup;
                        input.PICVendorId = $scope.SelectedPICVendor;
                        input.VendorId = $scope.selectedVendor;

                        //onChangeGrid(input);

                        //$scope.OnSelectedProduct($scope.SelectedPICProduct);
                        //$scope.OnModelSelected();
                    }
                }

                //$scope.$apply();
            });
        }

        $scope.onCancel = function () {
            // var url = "http://" + $window.location.host + "/#!/quote/" + $scope.OpportunityId + "/" + $scope.quoteKey;
            $window.location.href = "#!/quote/" + $scope.OpportunityId + "/" + $scope.quoteKey;
        };

        $scope.GetQuotelineData();

        function onChangeGrid(data) {
            var grid = data.sender;
            var prodt = grid.dataItem(this.select());

            $scope.selectedProductGroup = prodt.PICProductGroupId;
            $scope.SelectedPICVendor = prodt.PICVendorId;
            $scope.selectedVendor = prodt.VendorId;
            $("#picProductddl").show();
            //$("#picModelGroup").hide();

            //var pGroup = grid.dataSource.data();
            var pGroup = $scope.ProductGroup;
            var vendor = prodt.VendorId;
            var productList;
            if (pGroup != undefined)
                for (var i = 0; pGroup.length; i++) {
                    if (pGroup[i].PICProductGroupId === prodt.PICProductGroupId && pGroup[i].VendorId === $scope.selectedVendor) {
                        productList = pGroup[i].PICProductList;
                        break;
                    }
                }
            $scope.Product = productList;
            $scope.selectedModel = "";
            $scope.$apply();
        };

        $scope.onSaveConfigurator = function () {
            var configprese = $scope.configPresentationObj;
            var config = $scope.configEngineObj;
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var myElements = document.querySelectorAll("[class='col-sm-3 text_label']");

            for (var i = 0; i < myElements.length; i++) {
                myElements[i].style.color = "black";
            }
            var myElementsinput = document.querySelectorAll("[class='picInput']");
            for (var i = 0; i < myElementsinput.length; i++) {
                myElementsinput[i].style.borderColor = "#ccc";
            }
            var prodId = $scope.selectedProduct;
            var modelId = $scope.selectedModel;
            if (modelId === null) {
                modelId = $scope.selectedPICModel;
            }

            var gGroup = $scope.selectedProductGroup;
            var Quantity = $scope.Quantity;
            var picVendor = $scope.SelectedPICVendor;
            var picProduct = $scope.SelectedPICProduct;

            var pGroup = $scope.ProductGroup;
            var pCategory, ProductName, VendorName;
            var qk = $scope.quoteKey;
            var opp = $scope.OpportunityId;
            for (var i = 0; i < pGroup.length; i++) {
                if (pGroup[i].PICProductGroupId === $scope.selectedProductGroup && pGroup[i].VendorId === $scope.selectedVendor) {
                    pCategory = pGroup[i].PICProductGroupName;
                    VendorName = pGroup[i].VendorName;
                    var productList = pGroup[i].PICProductList;
                    for (var k = 0; k < productList.length; k++) {
                        if (productList[k].PICProductId === picProduct) {
                            ProductName = productList[k].ProductName;
                        }
                    }
                    break;
                }
            }

            if (configprese.validatePrompts()) {
                var jsonObj = {};

                if ($scope.ModelDescription === '' || $scope.ModelDescription === null || $scope.ModelDescription === undefined) {
                    $scope.ModelDescription = $.grep($scope.ModelGroup, function (item) {
                        if (item.Model === $scope.selectedModel) { return item; }
                    })[0].Description;
                }
                jsonObj.VendorName = VendorName;
                jsonObj.ProductName = ProductName;
                jsonObj.PCategory = pCategory;
                jsonObj.VendorId = $scope.selectedVendor;
                jsonObj.PicVendor = picVendor;
                jsonObj.PicProduct = picProduct;
                jsonObj.ProductId = prodId;
                jsonObj.ModelId = modelId;
                jsonObj.GGroup = gGroup;
                jsonObj.QuoteLineId = $scope.QuoteLineId;
                jsonObj.Quantity = Quantity;
                jsonObj.ModelDescription = $scope.ModelDescription;
                jsonObj.PromptAnswers = JSON.stringify(config.getPromptAnswers());
                $scope.pAnswers = config.getPromptAnswers();

                //$scope.ModelGroup
                //$scopee.ModelDescription
                if ($scope.ModelGroup !== null && $scope.ModelGroup !== undefined) {
                    var mlist = $scope.ModelGroup;
                    for (var i = 0; i < mlist.length; i++) {
                        if (mlist[i].Model === modelId) {
                            jsonObj.Description = ProductName + ' - ' + mlist[i].Description;
                        }
                    }
                } else {
                    jsonObj.Description = ProductName;
                }

                jsonObj.QuoteKey = qk;
                jsonObj.OpportunityId = opp;

                var promptAnswers = JSON.stringify(jsonObj);
                $http.post('/api/PIC/0/validatePromptAnswers', jsonObj).then(function (response) {
                    if (response.data.valid) {
                        var res = response.data.data;
                        var opId = response.data.OpportunityId;
                        var qk = response.data.QuoteKey;
                        if (!res.valid) {
                            if (res.messages.errors.length > 0) {
                                var div = '<ul>';
                                for (var i = 0; i < res.messages.errors.length; i++) {
                                    div = div + "<li>" + res.messages.errors[i] + "</li> ";
                                }
                                div = div + "</ul>";
                            }
                            document.getElementById("response").innerHTML = div;
                            var responseElement = document.getElementById("response");
                            responseElement.style.display = "block";

                            //document.getElementById('#response').innerText = div;
                        } else {
                            var changed = false;
                            var qld = $scope.QuotelineData[0];

                            ///// set options back to grid
                            if ($scope.widthPrompt != null && $scope.widthPrompt !== undefined) {
                                if ($scope.pAnswers["Prompt" + $scope.widthPrompt] !== undefined) {
                                    var width = $scope.pAnswers["Prompt" + $scope.widthPrompt].value;

                                    if (qld.Width !== parseFloat((width + "").split(".")[0])) {
                                        qld.Width = parseFloat((width + "").split(".")[0]);
                                        changed = true;
                                    }
                                    if (qld.FranctionalValueWidth !== parseFloat((width + "").split(".")[1])) {
                                        qld.FranctionalValueWidth = $scope.getFraction((width + "").split(".")[1]);
                                        changed = true;
                                    }
                                }
                            }

                            ////HEIGHT PROMPT
                            if ($scope.heightPrompt != null && $scope.heightPrompt !== undefined) {
                                if ($scope.pAnswers["Prompt" + $scope.heightPrompt] !== undefined) {
                                    var height = $scope.pAnswers["Prompt" + $scope.heightPrompt].value;

                                    if (qld.Height !== parseFloat((height + "").split(".")[0])) {
                                        qld.Height = parseFloat((height + "").split(".")[0]);
                                        changed = true;
                                    }
                                    if (qld.FranctionalValueHeight !== parseFloat((height + "").split(".")[1])) {
                                        qld.FranctionalValueHeight = $scope.getFraction((height + "").split(".")[1]);
                                        changed = true;
                                    }
                                }
                            }

                            //Room Name
                            if ($scope.roomPrompt != null && $scope.roomPrompt !== undefined) {
                                if ($scope.pAnswers["Prompt" + $scope.roomPrompt] !== undefined) {
                                    var RoomName = $scope.pAnswers["Prompt" + $scope.roomPrompt].value;

                                    if (qld.RoomName !== RoomName) {
                                        qld.RoomName = RoomName;
                                        changed = true;
                                    }
                                }
                            }

                            //////Mount Type
                            if ($scope.mountPrompt != null && $scope.mountPrompt !== undefined) {
                                if ($scope.pAnswers["Prompt" + $scope.mountPrompt] !== undefined) {
                                    var mountPromptoption = $scope.pAnswers["Prompt" + $scope.mountPrompt].value;
                                    var mount = '';
                                    if (mountPromptoption === "I") {
                                        mount = "IB";
                                    } else if (mountPromptoption === "O") {
                                        mount = "OB";
                                    }
                                    if (qld.MountType !== mount) {
                                        qld.MountType = mount;
                                        changed = true;
                                    }
                                }
                            }

                            ///////

                            if (changed) {
                                $http.post('/api/Quotes/0/updateQuotelineFromConfig', qld).then(
                                    function (response) {
                                        var opId = response.data.OpportunityId;
                                        var qk = response.data.QuoteKey;

                                        var responseElement = document.getElementById("response");
                                        responseElement.style.display = "none";
                                        var url = '/#!/Editquote/' + opId + '/' + qk;
                                        window.location.href = url;
                                    });
                            } else {
                                var responseElement = document.getElementById("response");
                                responseElement.style.display = "none";
                                var url = '/#!/Editquote/' + opId + '/' + qk;
                                window.location.href = url;
                            }
                        }
                    } else {
                        HFC.DisplayAlert(response.data.message);
                    }
                    loadingElement.style.display = "none";
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                    //$scope.loadingElement.style.display = "none";
                    HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
            }
            else {
                loadingElement.style.display = "none";
            }
        };

        function changegrp() {
            var pGroup = $scope.ProductGroup;
            var vendor = $scope.selectedVendor;
            var productList;
            if (pGroup != undefined && $scope.selectedProductGroup != null)
                for (var i = 0; i < pGroup.length; i++) {
                    if (pGroup[i].PICProductGroupId === $scope.selectedProductGroup && pGroup[i].VendorId === $scope.selectedVendor) {
                        productList = pGroup[i].PICProductList;
                        break;
                    }
                }
            $scope.Product = productList;
        }

        $scope.OnSelectedProduct = function (data) {
            var promptsOuter1 = document.getElementById("promptsOuter");
            promptsOuter1.style.display = "none";
            var prodlist = $scope.Product;
            $("#ModelGrpError").hide();
            for (var i = 0 ; i < prodlist.length; i++) {
                if (prodlist[i].PICProductId === data) {
                    $scope.selectedProduct = prodlist[i].ProductId;
                    if (prodlist[i].WarningPhasingOut !== '') {
                        var loadingElement = document.getElementById("modelWarning");
                        loadingElement.innerHTML = prodlist[i].WarningPhasingOut;
                        $("#ModelGrpError").show();
                    }
                    break;
                }
            }
            $scope.selectedModel = "";

            //$("#picModelGroup").hide();

            var prodId = $scope.SelectedPICProduct;
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            if (prodId !== null && prodId != undefined && Number(prodId)) {
                $http.get('/api/PIC/0/GetModels?productId=' + prodId).then(function (modRes) {
                    var PICModels = modRes.data;
                    if (PICModels.error === '') {
                        $("#picModelGroup").show();
                        //$scope.ModelGroup = PICModels.Data;
                        //if (PICModels.Data.length === 0) {
                        //    $scope.selectedModel = PICModels.Data[0].Model;
                        //    $scope.selectedPICModel = "__ALL__";
                        //    loadingElement.style.display = "none";
                        //    $scope.OnModelSelected();
                        //}
                        //else {
                        //$("#picModelGroup").show();
                        //$scope.selectedModel = ;
                        //for (var i = 0 ; i < PICModels.Data.length; i++) {
                        //    if (PICModels.IsActive === 'true') {
                        //        PICModels.Data[i].Model = values[0];
                        //        PICModels.Data[i].Description = values[1];
                        //    }

                        //}
                        $scope.ModelGroup = jQuery.grep(PICModels.Data, function (value) {
                            return value.IsActive === true;
                        });
                        loadingElement.style.display = "none";
                        //}
                    }
                    else {
                        HFC.DisplayAlert(PICModels.error);
                        loadingElement.style.display = "none";
                    }
                    //loadingElement.style.display = "none";
                }).catch(function (error) {
                    $scope.loadingElement.style.display = "none";
                    HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
            }
            else {
                loadingElement.style.display = "none";
            }
        };

        $scope.RemoveUnwantedChars = function (controlId) {
            var cId = controlId.currentTarget.id;
            var value = $("#" + cId).val();
            if (value.indexOf('&') != -1) $("#" + cId).val(value.replace(/\&/g, ""));
            if (value.indexOf('\'') != -1) $("#" + cId).val(value.replace(/\'/g, ""));
            if (value.indexOf('\"') != -1) $("#" + cId).val(value.replace(/\"/g, ""));
        }

        var configPresentationObj, configEngineObj;
        $scope.OnModelSelected = function () {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var myEl = angular.element(document.querySelector('#Prompts'));
            myEl.empty();

            $("#ModelGrpError").hide();
            var modelwarning = document.getElementById("modelWarning");

            var modelId = "";
            var prodId = $scope.SelectedPICProduct;
            if ($scope.Firstmodel !== null) {
                $scope.selectedModel = $scope.Firstmodel;
                modelId = $scope.Firstmodel;
                $scope.Firstmodel = null;
            } else {
                modelId = $scope.selectedModel;
            }

            document.getElementById("response").innerText = "";
            var promptsOuter1 = document.getElementById("response");
            promptsOuter1.style.display = "none";

            //$scope.configPresentationObj = new ConfigPresentation('Prompts', $scope.QuotelineData, $scope.widthPrompt, $scope.heightPrompt);
            //$scope.configEngineObj = new ConfigEngine();
            //$scope.configEngineObj.autoClampRanges = false;
            //// link instances
            //$scope.configPresentationObj.configEngine = $scope.configEngineObj;
            //$scope.configEngineObj.presentation = $scope.configPresentationObj;
            //var promptsOuter = document.getElementById("promptsOuter");
            promptsOuter.style.display = "none";
            $scope.ModelDescription = '';
            if (modelId !== null && prodId !== null) {
                var PICModels = $scope.ModelGroup;
                if (PICModels !== undefined) {
                    for (var i = 0; i < PICModels.length; i++) {
                        if (PICModels[i].Model === modelId) {
                            $scope.ModelDescription = PICModels[i].Description;
                            if (PICModels[i].WarningPhasingOut !== '') {
                                var loadingElement = document.getElementById("modelWarning");
                                loadingElement.innerHTML = PICModels[i].WarningPhasingOut;
                                $("#ModelGrpError").show();
                            }
                        }
                    }
                }

                // pull product dataSource and initialise
                $http.get("/api/PIC/0/GetDataSourceAndPromptAnswers?productid=" + prodId + "&modelId=" + modelId).then(function (response) {
                    var promptAnswers = JSON.parse(response.data.promptAnswers);
                    $http({
                        type: 'GET',
                        url: response.data.dsurl,
                        headers: {
                            'Content-Type': 'text/plain'
                        }
                    }).then(function (responseText) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "block";
                        $scope.heightPrompt = response.data.productInfo.heightPrompt;
                        $scope.widthPrompt = response.data.productInfo.widthPrompt;

                        $scope.mountPrompt = response.data.productInfo.mountPrompt;
                        $scope.roomPrompt = response.data.productInfo.roomPrompt;

                        $scope.configEngineObj = new ConfigEngine();

                        var datasource = responseText.data;
                        var productInfo = response.data.productInfo;

                        $scope.configPresentationObj = new ConfigPresentation('Prompts', $scope.QuotelineData);

                        $scope.configEngineObj.autoClampRanges = false;
                        // link instances
                        $scope.configPresentationObj.configEngine = $scope.configEngineObj;
                        $scope.configEngineObj.presentation = $scope.configPresentationObj;
                        var promptsOuter = document.getElementById("promptsOuter");
                        //var dataSourceAjax = ds.then(function (responseText) {
                        $scope.configEngineObj.dataSource = datasource;

                        $scope.configEngineObj.initPresentation();

                        angular.forEach($scope.configEngineObj.presentation.prompts, function (value, key) {
                            if (value["type"] == "T") {
                                $("#" + key).keyup(function (event) {
                                    $scope.RemoveUnwantedChars(event);
                                });
                            }

                        });
                        //$scope.configEngineObj.loadPromptAnswers(promptAnswers);
                        //loadingElement.style.display = "none";
                        if ($scope.SavedPromptAnswers != null &&
                            $scope.selectedModel === $scope.picJson.ModelId &&
                            $scope.SelectedPICProduct === $scope.picJson.PicProduct &&
                            $scope.SelectedPICVendor === $scope.picJson.PicVendor) {
                            promptAnswers = $scope.SavedPromptAnswers;
                        } else {
                            //WIDTH PROMPT
                            promptAnswers["Prompt" + $scope.widthPrompt] = { "value": ($scope.QuotelineData[0].Width + eval($scope.QuotelineData[0].FranctionalValueWidth)), "colorValue": "" };
                            //HEIGHT PROMPT
                            promptAnswers["Prompt" + $scope.heightPrompt] = { "value": ($scope.QuotelineData[0].Height + eval($scope.QuotelineData[0].FranctionalValueHeight)), "colorValue": "" };

                            //ROOM PROMPT
                            if ($scope.QuotelineData[0].RoomName != undefined &&
                                $scope.QuotelineData[0].RoomName != '' &&
                                $scope.QuotelineData[0].RoomName != null) {
                                promptAnswers["Prompt" + $scope.roomPrompt] = { "value": $scope.QuotelineData[0].RoomName, "colorValue": "" };
                            }
                            //MOUNT PROMPT
                            if ($scope.QuotelineData[0].MountType != undefined &&
                                $scope.QuotelineData[0].MountType != '' &&
                                $scope.QuotelineData[0].MountType != null) {
                                var mount;
                                if ($scope.QuotelineData[0].MountType === "IB") {
                                    mount = "I";
                                } else if ($scope.QuotelineData[0].MountType === "OB") {
                                    mount = "O";
                                }
                                promptAnswers["Prompt" + $scope.mountPrompt] = { "value": mount, "colorValue": "" };
                            }
                        }
                        loadingElement.style.display = "block";
                        $scope.configEngineObj.loadPromptAnswers(promptAnswers, function () {
                            promptsOuter.style.display = "block";
                            loadingElement.style.display = "none";
                        });
                    });
                });
            }
        }

        //Kendo grid search CLear
        $scope.ClearSearch = function () {
            $('#searchBox').val('');
            $("#VendorGrid").data('kendoGrid').dataSource.filter({
            });
        }

        //Kendo Grid Search
        $scope.VendorAndProductSearch = function () {
            $(".target").hide();
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var searchValue = $('#searchBox').val();
            $("#VendorGrid").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  {
                      field: "VendorName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "PICProductGroupName",
                      operator: "contains",
                      value: searchValue
                  }
                ]
            });

            loadingElement.style.display = "none";
        }

        function gcd(a, b) {
            if (b < 0.0000001) return a;                // Since there is a limited precision we need to limit the value.

            return gcd(b, Math.floor(a % b));           // Discard any fractions due to limitations in precision.
        };

        $scope.getFraction = function (frac) {
            if (frac) {
                var fraction = "0." + frac;

                var len = fraction.toString().length - 2;

                var denominator = Math.pow(10, len);
                var numerator = fraction * denominator;

                var divisor = gcd(numerator, denominator);
                numerator /= divisor;
                denominator /= divisor;

                return (Math.floor(numerator) + '/' + Math.floor(denominator));
            } else {
                return;
            }
        }

        $scope.getFractionValue = function (fraction) {
            //"E": [{ v: ".0", d: "" }, { v: ".125", d: "1/8" }, { v: ".25", d: "1/4" }, { v: ".375", d: "3/8" }, { v: ".5", d: "1/2" }, { v: ".625", d: "5/8" }, { v: ".75", d: "3/4" }, { v: ".875", d: "7/8" }],

            if (fraction === '0') {
                return 0;
            }
            else if (fraction === '1/8') {
                return .125;
            }
            else if (fraction === '2/8' || fraction === '1/4') {
                return .25;
            }
            else if (fraction === '3/8') {
                return .375;
            }
            else if (fraction === '4/8' || fraction === '1/2') {
                return .5;
            }
            else if (fraction === '5/8') {
                return .625;
            }
            else if (fraction === '6/8' || fraction === '3/4') {
                return .75;
            }
            else if (fraction === '7/8') {
                return .875;
            } else {
                return 0;
            }
        }
    }]);