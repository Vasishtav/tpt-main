﻿'use strict';
appPIC.controller('PICconfiguratorController', [
    '$http', '$scope', '$window', '$location', '$rootScope', '$sce', '$routeParams', 'configuratorService', 'NavbarServicePIC', function ($http, $scope, $window, $location, $rootScope, $sce, $routeParams, ConfiguratorService, NavbarServiceCP) {
        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.NavbarServiceCP.PICConfiguratorTab();

        $scope.FranchiseCode = "19001001";
        $scope.SelectedPICVendor = {};
        $scope.SelectedPICProduct = {};
        $scope.ResponseData = {};
        $scope.selectedModel = {};
        $scope.selectedModelDescription = {};
        $scope.selectedProductGroup = {};
        $scope.selectedProduct = {};
        $scope.selectedProductList = {};
        $scope.selectedVendor = {};
        $scope.configPresentationObj = {};
        $scope.configEngineObj = {};
        //$scope.QuoteLineId = $routeParams.quoteLineId;
        //$scope.OpportunityId = $rootScope.OpportunityId;
        //$scope.quoteKey = $rootScope.quoteKey;
        $scope.Quantity = 1;
        $scope.SavedPromptAnswers = null;
        $scope.picJson = null;

        //$rootScope.OpportunityId = $scope.OpportunityId;
        //$rootScope.quoteKey = $scope.quoteKey;
        //$scope.QuotelineData = {};
        $scope.heightPrompt = 0;
        $scope.widthPrompt = 0;

        $scope.ProductGroup = {
            dataSource: {
                transport: {
                    read: {
                        url: '/api/PIC/0/GetPICProductGroupForConfigurator?franchiseCode=' + $scope.FranchiseCode + '&vendorConfig=false',
                    },
                },
                error: function (e) {
                    HFC.DisplayAlert(e.errorThrown);
                },
                schema: {
                    model: {
                        id: "PICProductGroupId",
                        fields: {
                            PICVendorId: { type: "string", editable: false, visable: false },
                            VendorId: { type: "number", editable: false, visable: false },
                            VendorName: { type: "string" },
                            PICProductGroupId: { type: "number", editable: false, visable: false },
                            PICProductGroupName: { type: "string" },
                            PICProductList: { editable: false, visable: false }
                        }
                    }
                }
            },
            columns: [
                                { field: "VendorName", title: "Vendor" },
                                { field: "PICProductGroupName", title: "Product Category" }
            ],
            noRecords: true,
            messages: {
                noRecords: "No records found"
            },
            change: onChangeGrid,
            dataBound: onDataBound,
            //dataBinding: onDataBinding,
            editable: "popup",
            filterable: true,
            selectable: "row",
            scrollable: {
                virtual: true
            },
            height: 300,
            toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div ><input ng-keyup="VendorAndProductSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox leadsearch_tbox" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="ClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></script>').html()) }],
        };

        function onDataBound(data) {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var testGrid = data.sender;
            var newdata = testGrid._data;
            $scope.ProductGroup = data.sender.dataSource.data();
            //for now set timer to preload existing prompt answers
            setTimeout(function () {
                newdata.forEach(function (entry) {
                    if (Number($scope.selectedProductGroup) === Number(entry.PICProductGroupId) && Number(entry.VendorId) === Number($scope.selectedVendor)) {
                        testGrid.select('tr[data-uid="' + entry.uid + '"]');
                        changegrp();
                        //$scope.OnSelectedProduct($scope.SelectedPICProduct);
                        $scope.OnModelSelected();
                    }
                    else {
                        loadingElement.style.display = "none";
                    }
                });
            }, 1000);
        };

        //$scope.onCancel = function () {
        //    //  var url = "http://" + $window.location.host + "/#!/quote/" + $scope.OpportunityId + "/" + $scope.quoteKey;
        //    $window.location.href = "#!/quote/" + $scope.OpportunityId + "/" + $scope.quoteKey;
        //};

        //quotelineData();

        function onChangeGrid(data) {
            //$("#promptanswersiddiv").hide();
            var grid = data.sender;
            var prodt = grid.dataItem(this.select());

            $scope.selectedProductGroup = prodt.PICProductGroupId;
            $scope.SelectedPICVendor = prodt.PICVendorId;
            $scope.selectedVendor = prodt.VendorId;
            $("#picProductddl").show();
            $("#picModelGroup").hide();

            //var pGroup = grid.dataSource.data();
            var pGroup = $scope.ProductGroup;
            var vendor = prodt.VendorId;
            var productList;
            if (pGroup != undefined)
                for (var i = 0; pGroup.length; i++) {
                    if (pGroup[i].PICProductGroupId === prodt.PICProductGroupId && pGroup[i].VendorId === $scope.selectedVendor) {
                        productList = pGroup[i].PICProductList;
                        break;
                    }
                }
            $scope.Product = productList;
            $scope.selectedModel = "";
            $scope.$apply();
        };

        $scope.onSaveConfigurator = function () {
            var configprese = $scope.configPresentationObj;
            var config = $scope.configEngineObj;
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var myElements = document.querySelectorAll("[class='col-sm-3 text_label']");

            for (var i = 0; i < myElements.length; i++) {
                myElements[i].style.color = "black";
            }
            var myElementsinput = document.querySelectorAll("[class='picInput']");
            for (var i = 0; i < myElementsinput.length; i++) {
                myElementsinput[i].style.borderColor = "#ccc";
            }
            var prodId = $scope.selectedProduct;
            var modelId = $scope.selectedModel;
            if (modelId === null) {
                modelId = $scope.selectedPICModel;
            }

            var gGroup = $scope.selectedProductGroup;
            var Quantity = $scope.Quantity;
            var picVendor = $scope.SelectedPICVendor;
            var picProduct = $scope.SelectedPICProduct;

            var pGroup = $scope.ProductGroup;
            var pCategory, ProductName, VendorName;

            for (var i = 0; i < pGroup.length; i++) {
                if (pGroup[i].PICProductGroupId === $scope.selectedProductGroup && pGroup[i].VendorId === $scope.selectedVendor) {
                    pCategory = pGroup[i].PICProductGroupName;
                    VendorName = pGroup[i].VendorName;
                    var productList = pGroup[i].PICProductList;
                    for (var k = 0; k < productList.length; k++) {
                        if (productList[k].PICProductId === picProduct) {
                            ProductName = productList[k].ProductName;
                        }
                    }
                    break;
                }
            }

            if (configprese.validatePrompts()) {
                var jsonObj = {};
                jsonObj.VendorName = VendorName;
                jsonObj.ProductName = ProductName;
                jsonObj.PCategory = pCategory;
                jsonObj.VendorId = $scope.selectedVendor;
                jsonObj.PicVendor = picVendor;
                jsonObj.PicProduct = picProduct;
                jsonObj.ProductId = $scope.selectedProduct;
                jsonObj.ModelId = modelId;
                jsonObj.GGroup = gGroup;
                //jsonObj.QuoteLineId = $scope.QuoteLineId;
                jsonObj.Quantity = Quantity;
                jsonObj.PromptAnswers = JSON.stringify(config.getPromptAnswers());
                $scope.pAnswers = config.getPromptAnswers();
                //$("#promptanswersiddiv").show();

                document.getElementById("dataPromptAnswersPosted").innerHTML = jsonObj.PromptAnswers;
                var re = document.getElementById("dataPromptAnswersPosted");
                re.style.display = "block";

                //var qk = $scope.quoteKey;
                //var opp = $scope.OpportunityId;
                if ($scope.ModelGroup !== null && $scope.ModelGroup !== undefined) {
                    var mlist = $scope.ModelGroup;
                    for (var i = 0; i < mlist.length; i++) {
                        if (mlist[i].Model === modelId) {
                            jsonObj.Description = ProductName + ' - ' + mlist[i].Description;
                        }
                    }
                } else {
                    jsonObj.Description = ProductName;
                }

                jsonObj.QuoteKey = 1;
                jsonObj.OpportunityId = 1;
                var promptAnswers = JSON.stringify(jsonObj);
                $http.post('/api/PIC/0/PICvalidatePromptAnswers?fCode=' + $scope.FranchiseCode, jsonObj).then(function (response) {
                    if (response.data.valid) {
                        var responseElement = document.getElementById("response");
                        responseElement.style.display = "block";
                        responseElement.style.color = "red";
                        responseElement.style.borderColor = "red";
                        var res = response.data.data;
                        if (!res.valid) {
                            if (res.messages.errors.length > 0) {
                                var div = '<ul>';
                                for (var i = 0; i < res.messages.errors.length; i++) {
                                    div = div + "<li>" + res.messages.errors[i] + "</li> ";
                                }
                                div = div + "</ul>";
                            }
                            document.getElementById("response").innerHTML = div;

                            //document.getElementById('#response').innerText = div;
                        } else {
                            //$("#promptanswersiddiv").show();
                            $scope.ResponseData = response.data.data;
                            var responseElement = document.getElementById("response");
                            responseElement.style.display = "block";
                            responseElement.style.color = "green";
                            responseElement.style.borderColor = "green";
                            var div = JSON.stringify(response.data);
                            document.getElementById("response").innerHTML = 'Product Validated Successfully.';

                            //document.getElementById("response").innerHTML = div;
                            var responseElement = document.getElementById("response");
                            responseElement.style.display = "block";

                            ///
                            var responseobj = document.getElementById("response");
                            document.getElementById("responseobj").innerHTML = div;
                            var responseobj = document.getElementById("responseobj");
                            responseobj.style.display = "block";
                        }
                    } else {
                        HFC.DisplayAlert(response.data.messages);
                    }

                    loadingElement.style.display = "none";
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                    //$scope.loadingElement.style.display = "none";
                    HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                });
            }
            else {
                loadingElement.style.display = "none";
            }
        };

        function changegrp() {
            var pGroup = $scope.ProductGroup;
            var vendor = $scope.selectedVendor;
            var productList;
            if (pGroup != undefined && $scope.selectedProductGroup != null)
                for (var i = 0; i < pGroup.length; i++) {
                    if (pGroup[i].PICProductGroupId === $scope.selectedProductGroup && pGroup[i].VendorId === $scope.selectedVendor) {
                        productList = pGroup[i].PICProductList;
                        break;
                    }
                }
            $scope.Product = productList;
        }

        $scope.OnSelectedProduct = function (data) {
            var promptsOuter1 = document.getElementById("promptsOuter");
            promptsOuter1.style.display = "none";
            var prodlist = $scope.Product;
            $("#ModelGrpError").hide();
            $scope.selectedProduct = data;
            //if (prodlist != undefined && prodlist.length > 0) {
            //    for (var i = 0 ; i < prodlist.length; i++) {
            //        if (prodlist[i].PICProductId === data) {
            //            $scope.selectedProduct = prodlist[i].ProductId;
            //            if (prodlist[i].WarningPhasingOut !== '') {
            //                var loadingElement = document.getElementById("modelWarning");
            //                loadingElement.innerHTML = prodlist[i].WarningPhasingOut;
            //                $("#ModelGrpError").show();
            //            }
            //            break;
            //        }
            //    }
            //}

            $scope.selectedModel = "";

            //$("#picModelGroup").hide();

            var prodId = $scope.SelectedPICProduct;
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            if (prodId !== null && prodId != undefined && Number(prodId)) {
                $http.get('/api/PIC/0/GetModels?productId=' + prodId).then(function (modRes) {
                    var PICModels = modRes.data;
                    if (PICModels.error === '') {
                        $("#picModelGroup").show();

                        //$scope.ModelGroup = PICModels.Data;

                        $scope.ModelGroup = jQuery.grep(PICModels.Data, function (value) {
                            return value.IsActive === true;
                        });
                        //for (var i = 0 ; i < PICModels.Data.length; i++) {
                        //    if (PICModels.Data[i].IsActive === true) {
                        //        PICModels.Data[i].Model = values[0];
                        //        PICModels.Data[i].Description = values[1];
                        //    }

                        //}
                        loadingElement.style.display = "none";
                    }
                    else {
                        HFC.DisplayAlert(PICModels.error);
                        loadingElement.style.display = "none";
                    }
                });
            }
            else {
                loadingElement.style.display = "none";
            }
        };
        var configPresentationObj, configEngineObj;
        $scope.OnModelSelected = function () {
            var myEl = angular.element(document.querySelector('#Prompts'));
            myEl.empty();

            $("#ModelGrpError").hide();
            var modelwarning = document.getElementById("modelWarning");

            var prodId = $scope.SelectedPICProduct;
            var modelId = $scope.selectedModel;

            document.getElementById("response").innerText = "";
            var promptsOuter1 = document.getElementById("response");
            promptsOuter1.style.display = "none";
            promptsOuter.style.display = "none";
            if (modelId !== null && prodId !== null) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "block";

                var PICModels = $scope.ModelGroup;
                if (PICModels !== undefined) {
                    for (var i = 0; i < PICModels.length; i++) {
                        if (PICModels[i].Model === modelId) {
                            if (PICModels[i].WarningPhasingOut !== '') {
                                var loadingElement = document.getElementById("modelWarning");
                                loadingElement.innerHTML = PICModels[i].WarningPhasingOut;
                                $("#ModelGrpError").show();
                            }
                        }
                    }
                }
                // pull product dataSource and initialise
                $http.get("/api/PIC/0/GetDataSourceAndPromptAnswersByFranchiseId?productid=" + prodId + "&modelId=" + modelId + "&franchiseCode=" + $scope.FranchiseCode).then(function (response) {
                    var promptAnswers = JSON.parse(response.data.promptAnswers);
                    $http({
                        type: 'GET',
                        url: response.data.dsurl,
                        headers: {
                            'Content-Type': 'text/plain'
                        }
                    }).then(function (responseText) {
                        $scope.heightPrompt = response.data.productInfo.heightPrompt;
                        $scope.widthPrompt = response.data.productInfo.widthPrompt;
                        $scope.configEngineObj = new ConfigEngine();

                        //document.getElementById("dataSource").innerHTML = responseText.data.dataSource;
                        //var responseElement = document.getElementById("dataSource");
                        //responseElement.style.display = "block";

                        var datasource = responseText.data;
                        var productInfo = response.data.productInfo;

                        $scope.configPresentationObj = new ConfigPresentation('Prompts');

                        $scope.configEngineObj.autoClampRanges = false;
                        // link instances
                        $scope.configPresentationObj.configEngine = $scope.configEngineObj;
                        $scope.configEngineObj.presentation = $scope.configPresentationObj;
                        var promptsOuter = document.getElementById("promptsOuter");
                        //var dataSourceAjax = ds.then(function (responseText) {
                        var loadingElement = document.getElementById("loading");
                        //promptsOuter.style.display = "block";
                        //promptsOuter.style.display = "block";
                        loadingElement.style.display = "block";
                        $scope.configEngineObj.dataSource = datasource;

                        $scope.configEngineObj.initPresentation();

                        angular.forEach($scope.configEngineObj.presentation.prompts, function (value, key) {
                            if (value["type"] == "T") {
                                $("#" + key).keyup(function (event) {
                                    $scope.RemoveUnwantedChars(event);
                                });
                            }
                        });

                        //$scope.configEngineObj.loadPromptAnswers(promptAnswers);
                        loadingElement.style.display = "none";

                        $scope.configEngineObj.loadPromptAnswers(promptAnswers, function () {
                            loadingElement.style.display = "block";
                            promptsOuter.style.display = "block";
                            loadingElement.style.display = "none";
                        });
                    });
                });
            }
        }

        $scope.RemoveUnwantedChars = function (controlId) {
            var cId = controlId.currentTarget.id;
            var value = $("#" + cId).val();
            if (value.indexOf('&') != -1) $("#" + cId).val(value.replace(/\&/g, ""));
            if (value.indexOf('\'') != -1) $("#" + cId).val(value.replace(/\'/g, ""));
            if (value.indexOf('\"') != -1) $("#" + cId).val(value.replace(/\"/g, ""));
        }

        //Kendo grid search CLear
        $scope.ClearSearch = function () {
            $('#searchBox').val('');
            $("#VendorGrid").data('kendoGrid').dataSource.filter({
            });
        }

        //Kendo Grid Search
        $scope.VendorAndProductSearch = function () {
            $(".target").hide();
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var searchValue = $('#searchBox').val();
            $("#VendorGrid").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  {
                      field: "VendorName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "PICProductGroupName",
                      operator: "contains",
                      value: searchValue
                  }
                ]
            });
            loadingElement.style.display = "none";
        }

        $scope.getFractionValue = function (fraction) {
            //"E": [{ v: ".0", d: "" }, { v: ".125", d: "1/8" }, { v: ".25", d: "1/4" }, { v: ".375", d: "3/8" }, { v: ".5", d: "1/2" }, { v: ".625", d: "5/8" }, { v: ".75", d: "3/4" }, { v: ".875", d: "7/8" }],

            if (fraction === '0') {
                return 0;
            }
            else if (fraction === '1/8') {
                return .125;
            }
            else if (fraction === '2/8' || fraction === '1/4') {
                return .25;
            }
            else if (fraction === '3/8') {
                return .375;
            }
            else if (fraction === '4/8' || fraction === '1/2') {
                return .5;
            }
            else if (fraction === '5/8') {
                return .625;
            }
            else if (fraction === '6/8' || fraction === '3/4') {
                return .75;
            }
            else if (fraction === '7/8') {
                return .875;
            } else {
                return 0;
            }
        }
    }]);