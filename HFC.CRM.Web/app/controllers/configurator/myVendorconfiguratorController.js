﻿'use strict';

app.controller('myVendorconfiguratorController', [
    '$http', '$scope', '$window', '$location', '$rootScope', '$sce', '$routeParams', 'myVendorconfiguratorService', 'NavbarService',  function ($http, $scope, $window, $location, $rootScope, $sce, $routeParams, myVendorconfiguratorService, NavbarService) {

        
        $scope.NavbarService = NavbarService;

        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesOpportunitiesTab();

        $scope.selectedModel = {};
        $scope.selectedProductGroup = {};
        $scope.selectedProduct = {};
        $scope.selectedVendor = {};

        $scope.Preload = false;
        $scope.ProductId = 0;
        $scope.QuoteLineId = $routeParams.quoteLineId;
        $scope.Quantity = 1;
        $scope.ProductDetails = {};
        $scope.OpportunityId = $routeParams.Opportunity;
        $scope.quoteKey = $rootScope.quoteKey;
        $scope.PICGroupId = {};
        $scope.PICProductId = {};
        $scope.OneTimeProduct = false;

        $scope.UnitPrice = 0;
        $scope.Cost = 0;

        $scope.allianceVendorArray = [];
        //'dsfsdfsdfsdf', 'sdfsdfsdfdfdf', 'dsfsdfsfsf', 'sdfsdfsdfsdf'];

        //$(".currency").kendoNumericTextBox({
        //    culture: "en-US",
        //    format: "c2"
        //});


        $http.get('/api/Accounts/0/GetAccQuoteNames?OpportunityId=' + $scope.OpportunityId).then(function (data) {
            
            if (data.data != null)
            { $scope.AccountName = data.data[0]; $scope.QuoteName = data.data[1]; }
            else
            {
                $scope.AccountName = ''; $scope.QuoteName = '';
            }
        });

      

        $http.get('/api/quotes/0/getVendorNames').then(function (data) {

            if (data.data != null)
                $scope.allianceVendorArray = data.data;
            else
                $scope.allianceVendorArray = [];
         
                $("#autocomplete").kendoAutoComplete({
                    dataSource: $scope.allianceVendorArray,
                    // dataTextField: "name",
                    noDataTemplate: false
                });
         

        });

        $scope.autocompleteClick = function () {
            var autocomplete = $("#autocomplete").data("kendoAutoComplete");
            autocomplete.search($scope.ProductDetails.Vendor);
        }

        $http.get('/api/quotes/0/GetMyProducts').then(function (data) {
            var test = JSON.parse(data.data.data);

            $scope.ProductGroup = test;

            $scope.ProductGroupDatasource1 = {
                dataSource: {
                    data: test
                },
                columns: [
                    { field: "Vendor", title: "Vendor" },
                    { field: "ProductCategory", title: "Product Category" },
                    { field: "VendorProductSKU", title: "Product SKU" },
                    { field: "ProductName", title: "Product Name" },
                    { field: "ProductKey", title: "Vendor", hidden: true }
                ],

                height: 250,
                sortable: true,
                selectable: "row",
                filterable: true,
                scrollable: {
                    virtual: true
                },
                toolbar: [{ template: kendo.template($('<script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="VendorAndProductSearch()" type="search" id="searchBox" placeholder="Search" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search" style="padding:0 !important;"><input type="button" id="btnReset" ng-click="ClearSearch()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>').html()) }],



            };
            //Kendo Grid Data Source
            //$scope.ProductGroupDatasource = new kendo.data.DataSource({
            //    data: test,
            //    //pageSize: 5
            //});
            ////newtest();
            //GetQuoteLineByQuoteLineId
            if ($scope.QuoteLineId !== 0) {
                $http.get('/api/quotes/0/GetQuoteLineByQuoteLineId?quoteLineId=' + $scope.QuoteLineId).then(function (data) {
                    var quoteline = JSON.parse(data.data.data);
                    $scope.Quantity = quoteline[0].Quantity;
                    $scope.quoteKey = quoteline[0].QuoteKey;
                    $scope.OpportunityId = quoteline[0].OpportunityId;
                    var productKey = quoteline[0].ProductId;
                    //$scope.ProductId = data.ProductKey;
                    //$scope.PICGroupId = data.PICGroupId;
                    //$scope.PICProductId = data.PICProductId;


                    if (Number(quoteline[0].ProductTypeId) == 5) {
                        $scope.OneTimeProduct = true;
                        $scope.ProductDetails = quoteline[0];
                        $scope.ProductDetails.Vendor = quoteline[0].VendorName;
                        $scope.ProductDetails.ProductName = quoteline[0].ProductName;
                        $scope.ProductDetails.Description = quoteline[0].Description;
                        $scope.ProductDetails.Cost = quoteline[0].Cost;
                        $scope.ProductDetails.UnitPrice = quoteline[0].UnitPrice;
                        $scope.Preload = true;
                        var picJson = JSON.parse(quoteline[0].PICJson);
                        $scope.ProductId = picJson.productId;
                        $scope.PICGroupId = picJson.gGroup;
                        $scope.PICProductId = picJson.picProduct;

                        $scope.oneTimeProductCheck();
                        //$("#oneTimeProductModel").modal("hide");

                    }
                    else {
                        $scope.OneTimeProduct = false;
                        var grid = $("#VendorGrid").data("kendoGrid");
                        $.each(grid.tbody.find('tr'), function () {
                            var model = grid.dataItem(this);
                            if (model.ProductKey === productKey) {//some condition
                                $('[data-uid=' + model.uid + ']').addClass('k-state-selected');
                                $scope.onChange(model);
                            }
                        });
                        //$scope.onChange(quoteline[0]);
                        //$scope.ProductId = data.ProductKey;
                        //$scope.PICGroupId = data.PICGroupId;
                        //$scope.PICProductId = data.PICProductId;
                        //$scope.ProductDetails = data;
                        //$scope.ProductId = data.ProductKey;
                        //$scope.PICGroupId = data.PICGroupId;
                        //$scope.PICProductId = data.PICProductId;
                    }


                });
            }
        });

        var newtest = function () {
            $http.get('/api/quotes/0/GetQuoteLineByQuoteLineId?quoteLineId=' + $scope.QuoteLineId).then(function (data) {
                var quoteline = JSON.parse(data.data.data);

            });
        }
        
        $scope.tooltipOptions = {
            filter: "td,th",
            position: "top",
            hide: function (e) {
                this.content.parent().css('visibility', 'hidden');
            },
            show: function (e) {
                if (this.content.text().length > 1) {
                    if (this.content.text().trim() === "Edit QualifyAppointment Print") {
                        this.content.parent().css('visibility', 'hidden');
                    } else {
                        this.content.parent().css('visibility', 'visible');
                    }

                }
                else {
                    this.content.parent().css('visibility', 'hidden');
                }
            },
            content: function (e) {

                return e.target.context.textContent;
            }
        };

        $scope.onCancel = function () {

            window.location.href = '/#!/Editquote/' + $scope.OpportunityId + '/' + $scope.quoteKey;
            //var url = "http://" + $window.location.host + "/#!/quote/" + $scope.OpportunityId + "/" + $scope.quoteKey;
            //$window.location.href = url;
        };



        $scope.onChange = function (data) {
            if (data !== undefined) {
                //var numeric = $("#currency").data("kendoNumericTextBox");
                //numeric.value(null);

                //var unitPrice = $("#prdunitPrice").data("kendoNumericTextBox");
                //unitPrice.value(data.unitprice);

                //var cost = $("#prdcost").data("kendoNumericTextBox");

                //cost.value(data.Cost);

                $scope.ProductId = data.ProductKey;
                $scope.PICGroupId = data.PICGroupId;
                $scope.PICProductId = data.PICProductId;
                $scope.ProductDetails = data;
                $scope.OneTimeProduct = false;
                $scope.oneTimeProductCheck();
            }
        };

        //$scope.$watch('ProductId', function (newVal) {
        //    $scope.OneTimeProduct = false;
        //    $scope.oneTimeProductCheck();
        //    var promptsOuter = document.getElementById("promptsOuter");
        //    promptsOuter.style.display = "none";
        //    var productList = $scope.ProductGroup;
        //    var ProductId = $scope.ProductId;
        //    for (var i = 0; i < productList.length; i++) {
        //        if (productList[i].ProductKey === ProductId) {
        //            $scope.ProductDetails = productList[i];
        //        }
        //    }
        //    promptsOuter.style.display = "block";
        //});
        //Kendo grid search CLear

        $scope.ClearSearch = function () {
            $('#searchBox').val('');
            $("#VendorGrid").data('kendoGrid').dataSource.filter({
            });
        }
        //Kendo Grid Search
        $scope.VendorAndProductSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#VendorGrid").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [

                      { field: "Vendor", operator: "contains", value: searchValue },
                      { field: "ProductCategory", operator: "contains", value: searchValue },
                      { field: "VendorProductSKU", operator: "contains", value: searchValue },
                      { field: "ProductName", operator: "contains", value: searchValue }
                ]
            });

        }
        $('#oneTimeProductQuantity:input[type=number]').on('mousewheel', function (e) { $(this).blur(); });
        $scope.onOTPQuantityChange = function () {

            var quantityVal = $.isNumeric($("#oneTimeProductQuantity").val());
            if (!quantityVal) {
                $("#oneTimeProductQuantity").val('');
                $("#oneTimeProductQuantity").focus();
            }
            else if ($("#oneTimeProductQuantity").val() == "0") {
                $("#oneTimeProductQuantity").val('');
                $("#oneTimeProductQuantity").focus();
            }
            else $("#oneTimeProductQuantity").removeClass("required-error");
        }

        $scope.onSaveConfigurator = function () {
            
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            if ($scope.Quantity === 0 || $scope.Quantity === null || $scope.Quantity === undefined) {                
                loadingElement.style.display = "none";
                //HFC.DisplayAlert("Please enter Quantity.");
                $("#oneTimeProductQuantity").addClass("required-error");
                return false;
            }
            
            //loadingElement.style.display = "block";
            var quantity = $scope.Quantity;
            var ProductId = $scope.ProductId;
            var quotelineId = $scope.QuoteLineId;
            var ProductDetails = angular.copy($scope.ProductDetails);

            ProductDetails.ProductId = ProductId;
            ProductDetails.quotelineId = quotelineId;
            ProductDetails.quantity = quantity;
            ProductDetails.PICGroupId = $scope.PICGroupId;
            ProductDetails.PICProductId = $scope.PICProductId;
            ProductDetails.ProductCategory = $scope.ProductDetails.ProductName;//.replace(/\"/g,'&quot;');
            ProductDetails.Description = $scope.ProductDetails.Description;
            //ProductDetails.PICProductId = $scope.Description;
            //ProductDetails.PICProductId = $scope.OneTimeProduct;
            //Cost
            // Unitprice 
            $scope.Cost = 0;
            if ($scope.OneTimeProduct) {
                ProductDetails.ProductTypeId = 5;
                ProductDetails.ProductCategory = $scope.ProductDetails.ProductName;//.replace(/\"/g, '&quot;');
                ProductDetails.VendorID = 0;
                ProductDetails.PICGroupId = 1000000;
                ProductDetails.PICProductId = 10000;



                if ($scope.ProductDetails.Vendor === undefined || $scope.ProductDetails.Vendor === null) {
                    ProductDetails.Vendor = '';
                } else {
                    ProductDetails.Vendor = $scope.ProductDetails.Vendor;//.replace(/\"/g,'&quot;');
                }
                if ($scope.ProductDetails.Description === undefined || $scope.ProductDetails.Description === null) {
                    ProductDetails.Description = '';
                } else {
                    ProductDetails.Description = $scope.ProductDetails.Description;//.replace(/\"/g,'&quot;');
                }

            }
            //$.each(ProductDetails, function (key, value) {
            //    value = value.replace(/\"/g, '\\"');
            //});
            $http.post('/api/Quotes/' + quotelineId + '/SaveMyVendorConfiguration',ProductDetails).then(function (response) {
                var res = response.data;
                if (res.error === "") {
                    HFC.DisplaySuccess("Quoteline saved sucessfully.");
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                    var url = '/#!/Editquote/' + $scope.OpportunityId + '/' + $scope.quoteKey;
                    window.location.href = url;

                } else {
                    HFC.DisplayAlert("Failed: " + res.error);
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                }

              //  console.log(response);
            });

        }
        
        //$("#prdunitPrice").kendoNumericTextBox({
        //    format: "c2",
        //    spinners: false,
        //    culture: "en-US",
            
        //});
        //$("#prdcost").kendoNumericTextBox({
        //    format: "c2",
        //    culture: "en-US",
        //    spinners: false
        //});
        $scope.OneTimeProductCheck = function () {
            $scope.ProductDetails.Vendor = '';
            $scope.ProductDetails.ProductName = '';
            $scope.ProductDetails.Description = '';
            //$scope.Quantity = 1;
            $scope.ProductDetails.Cost = '';
            $scope.ProductDetails.UnitPrice = '';
            $("#oneTimeProductModel").modal("hide");
        }
        $scope.oneTimeProductCheck = function () {

            var promptsOuter = document.getElementById("promptsOuter");
            promptsOuter.style.display = "block";

            if ($scope.OneTimeProduct) {
                if (!$scope.Preload) {
                    $scope.ProductDetails.Vendor = '';
                    $scope.ProductDetails.ProductName = '';
                    $scope.ProductDetails.Description = '';
                    //$scope.Quantity = 1;

                    $scope.ProductId = 0;
                    $scope.PICGroupId = 0;
                    $scope.PICProductId = 0;
                    $scope.ProductDetails.Cost = '';
                    $scope.ProductDetails.UnitPrice = '';
                    if (!$scope.Preload) {
                        $scope.ProductId = 0;
                        $scope.PICGroupId = 0;
                        $scope.PICProductId = 0;
                        $scope.ProductDetails.Cost = '';
                        $scope.ProductDetails.UnitPrice = '';
                        $("#oneTimeProductModel").modal("show");

                    }
                    $scope.Preload = false;
                }
                $(".onetimeProduct").show();
                $(".prdDetails").prop("disabled", false);

                var grid = $("#VendorGrid").data("kendoGrid");
                grid.clearSelection();
            }
            else {
                $(".prdDetails").prop("disabled", true);
                $(".onetimeProduct").hide();

                
                //TP - 1723: CLONE - Don't require quantity for a service
                if (!$scope.Quantity) {
                    $scope.Quantity = 1;
                }

                if ($scope.ProductId === 0 || $scope.ProductId === undefined) {
                    promptsOuter.style.display = "none";
                }

            }
        }
    }]);