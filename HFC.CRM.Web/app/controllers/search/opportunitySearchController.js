﻿// TODO: rquired refactoring:

'use strict';
app

    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('opportunitySearchController',
    [
        '$scope', 'HFCService', 'OpportunitySearchService', 'kendotooltipService'
        , '$routeParams', '$location', '$rootScope'
        , '$templateCache', '$sce', '$compile', 'NavbarService'
        , 'LeadAccountService',  'kendoService',
        function (
            $scope, HFCService, OpportunitySearchService, kendotooltipService
            , $routeParams, $location, $rootScope
            , $templateCache, $sce, $compile, NavbarService
            , LeadAccountService,  kendoService
            ) {

            $rootScope.enableSaveSearch = enableSaveSearch;
            $scope.SelectedAccountId = $routeParams.AccountId;
            $scope.NavbarService = NavbarService;
            //$scope.NoteServiceTP = NoteServiceTP;
            //$scope.PersonService = PersonService;
            //$scope.OrderSearchService = OrderSearchService;

            // for kendo tooltip
            $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
            kendoService.customTooltip = null;

            //
            $scope.NavbarService.SelectSales();
            var ul = $location.absUrl().split('#!')[1];
            if (ul.includes('accountopportunitySearch') || ul.includes('accountordersSearch')) {
                $scope.NavbarService.EnableSalesAccountsTab();
            }else if(ul.includes('opportunitySearch')) {
                $scope.NavbarService.EnableSalesOpportunitiesTab();
            }





            $scope.showOpportunityStatus = function (Opportunity) {
                if (Opportunity.isShowOpportunityStatuses) {
                    return;
                }
                Opportunity.isShowOpportunityStatuses = !Opportunity.isShowOpportunityStatuses;
            };

            $scope.showNotes = function (Opportunity) {
                Opportunity.isShowNotes = !Opportunity.isShowNotes;
            };

           
            $scope.OpportunitySearchService = OpportunitySearchService;
            //$scope.FileprintService = FileprintService;
            $scope.HFCService = HFCService;
            $scope.filter_CounterSelectedSalesAgent = 'Nothing selected';
            $scope.filter_CounterSelectedJobStatus = 'Nothing selected';
            $scope.selectedSales = null;
            $scope.selectedOpportunityStatuses = null;
            $scope.selectedSources = null;
            $scope.SearchFilter = 'Auto_Detect';
            $scope.BrandId = $scope.HFCService.CurrentBrand;

            $scope.Permission = {};
            var Opportunitypermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity')
            var SalesOrderpermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SalesOrder')
            $scope.PermissionOpportunity = Opportunitypermission;
            $scope.Permission.ListOpportunity = Opportunitypermission.CanRead; //$scope.HFCService.GetPermissionTP('List Opportunity').CanAccess;
            $scope.Permission.AccountOrderList = SalesOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('List Order').CanAccess;
            //$scope.Permission.AddNotesAccount = HFCService.GetPermissionTP('Add Notes&Attachments-Account').CanAccess;
            $scope.Permission.AddOpportunity = Opportunitypermission.CanCreate; //HFCService.GetPermissionTP('Add Opportunity').CanAccess;
            //$scope.Permission.AddEvent = $scope.HFCService.GetPermissionTP('Add Event').CanAccess;
            // Appointment
            HFCService.GetUrl();


            var AccountId = $routeParams.AccountId;

            if (AccountId != null) {
                $scope.Account = {
                    AccountId: AccountId
                }
            }
            $scope.ChangeFilterSearch = function (val) {

                $scope.SearchFilter = val;
            }

            $scope.getClassOpportunitySearch = function (val) {

                if (val == $scope.SearchFilter) {
                    return 'active';
                }
                return '';
            }

            if (AccountId > 0) {
                
                LeadAccountService.Get(AccountId, function (response) {
                    
                    $scope.appointAccount = response;
                   
                });
            }

           

            $scope.OpportunitygridSearchClear = function () {
                $('#oppsearchBox').val('');
                $("#gridopportunitySearch").data('kendoGrid').dataSource.filter({
                });
            }
            $scope.NewOpportunitypage = function (accountid) {
                //window.location.href = '/#!/Opportunitys/' + $scope.SelectedAccountId;
                //var s = window.location.hash;
                //var z = s.split("/");
                //if (z[2] != 0 && z[2] != null && z[2] != "") {
                //    //window.location.href = '/#!/Opportunity/' + z[2];
                //    window.location.href = '/#!/Opportunitys/';
                //}
                //else {
                    if ($scope.SelectedAccountId == undefined)
                    {
                        window.location.href = '/#!/Opportunitys/' ;
                    }
                    else
                    {
                        window.location.href = '/#!/Opportunitys/' + accountid;
                    }
                    
                }
            

            $scope.OpportunitygridSearch = function () {
                
                var searchValue = $('#oppsearchBox').val();
                $("#gridopportunitySearch").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                        
                      {
                          field: "OpportunityFullName",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "OpportunityStatus",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "AccountName",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "OpportunityNumber",
                          
                          operator: 'eq',
                          value: searchValue
                      },
                      //{
                      //    field: "ContractDate",
                      //    value: searchValue
                      //},
                      {
                          field: "SalesAgentName",
                          operator: "contains",
                          value: searchValue
                      },
                    ]
                });
            }
            $scope.filter_SelectSalesAgent = function (id) {
                $scope.filter_CounterSelectedSalesAgent = id;
            }

            $scope.filter_SelectJobStatus = function (id) {
                $scope.filter_CounterSelectedJobStatus = id;
            }
            $scope.SortBy = 'createdonutc';
            $scope.SortIsDesc = true;
            $scope.ChangeSortOrder = function (sortBy) {
                if (sortBy) {
                    if (sortBy != $scope.SortBy) {
                        $scope.SortBy = sortBy;
                        $scope.SortIsDesc = true;
                    } else {
                        $scope.SortIsDesc = !$scope.SortIsDesc;

                    }
                    $scope.ApplyFilter();
                }
            }

            //$scope.GoToFirstPage = function () {
            //    $scope.OpportunitySearchService.Pagination.page = 1;
            //    $scope.ApplyFilter();
            //}

            //$scope.GoToPreviousPage = function () {
            //    if ($scope.OpportunitySearchService.Pagination.page > 1) {
            //        $scope.OpportunitySearchService.Pagination.page = +($scope.OpportunitySearchService.Pagination.page) - 1;
            //        $scope.ApplyFilter();
            //    }
            //}

            //$scope.GoToNextPage = function () {

            //    if ($scope.OpportunitySearchService.Pagination.page == $scope.OpportunitySearchService.Pagination.pageTotal) {
            //        return;
            //    }
            //    $scope.OpportunitySearchService.Pagination.page = +($scope.OpportunitySearchService.Pagination.page) + 1;
            //    $scope.ApplyFilter();
            //}

            //$scope.GoToLastPage = function () {
            //    $scope.OpportunitySearchService.Pagination.page = $scope.OpportunitySearchService.Pagination.pageTotal;
            //    $scope.ApplyFilter();
            //}

            $scope.ChangePageSize = function () {
                $scope.ApplyFilter();
            }

            $scope.ExportExcel = function () {
                var data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };

                if ($scope.selectedSales) {
                    data.salesPersonIds = $scope.selectedSales;
                }

                if ($scope.selectedJobsStatuses) {
                    data.jobStatusIds = $scope.selectedJobsStatuses;
                }

                if ($scope.selectedOpportunityStatuses) {
                    data.OpportunityStatusIds = $scope.selectedOpportunityStatuses;
                }

                if ($scope.selectedSources) {
                    data.sourceIds = $scope.selectedSources;
                }

                if ($scope.invoiceStatusesSearch) {
                    data.invoiceStatuses = $scope.invoiceStatusesSearch;
                }

                if ($scope.ForSearchstartDate)
                    data.createdOnUtcStart = $scope.ForSearchstartDate;
                if ($scope.ForSearchendDate)
                    data.createdOnUtcEnd = $scope.ForSearchendDate;

                data.searchTerm = $('#search-box').val();

                data.searchFilter = $('#search-type').find('.active').find('input').val();

                //if (data.searchFilter != 'Auto_Detect') {
                //    data.searchFilter = $scope.SearchFilter;
                //}

                if ($scope.SearchTerm) {
                    data.searchTerm = $scope.SearchTerm;
                }

                data.pageIndex = $scope.OpportunitySearchService.Pagination.page;
                data.pageSize = $scope.OpportunitySearchService.Pagination.size;

                data.ForSearchstartDate = $scope.ForSearchstartDate;
                data.ForSearchendDate = $scope.ForSearchendDate;

                if ($routeParams.commercialType) {
                    data.commercialType = $routeParams.commercialType;
                }

                window.open('/export/Opportunitys?' + $.param(data), '_blank');
            }


            $scope.ApplyFilter = function (isClickedFromUI) {


                var data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };
                //if ($scope.selectedSales) {
                //    data.salesPersonIds = $scope.selectedSales;
                //}

                if (isClickedFromUI) {
                    data.pageIndex = 1;
                    $scope.OpportunitySearchService.Pagination.page = 1;
                }

                if ($scope.selectedSales) {
                    data.salesPersonIds = $scope.selectedSales;
                }

                if ($scope.selectedJobsStatuses) {
                    data.jobStatusIds = $scope.selectedJobsStatuses;
                }

                if ($scope.selectedOpportunityStatuses) {
                    data.OpportunityStatusIds = $scope.selectedOpportunityStatuses;
                }


                if ($scope.selectedSources) {
                    data.sourceIds = $scope.selectedSources;
                }

                if ($scope.invoiceStatusesSearch) {
                    data.invoiceStatuses = $scope.invoiceStatusesSearch;
                }

                if ($scope.ForSearchstartDate)
                    data.createdOnUtcStart = $scope.ForSearchstartDate;
                if ($scope.ForSearchendDate)
                    data.createdOnUtcEnd = $scope.ForSearchendDate;

                data.searchTerm = $('#search-box').val();

                data.searchFilter = $('#search-type').find('.active').find('input').val();

                //if (data.searchFilter != 'Auto_Detect') {
                //    data.searchFilter = $scope.SearchFilter;
                //}

                if ($scope.SearchTerm) {
                    data.searchTerm = $scope.SearchTerm;
                }

                data.pageIndex = $scope.OpportunitySearchService.Pagination.page;
                data.pageSize = $scope.OpportunitySearchService.Pagination.size;

                data.ForSearchstartDate = $scope.ForSearchstartDate;
                data.ForSearchendDate = $scope.ForSearchendDate;


                ///OpportunitysSearch/:searchTerm?/:searchFilter?/:OpportunityStatusIds?/:sortIsDesc?/:orderBy?/:selectedDateText?/:createdOnUtcStart?/:createdOnUtcEnd?
                // var url = '/OpportunitysSearch/' + data.searchTerm + '/' + data.searchFilter + '/' + $scope.ConvertArrayToString(data.OpportunityStatusIds) +
                //      '/' + $scope.selectedDateText + '/' + (data.createdOnUtcStart ? encodeURIComponent(data.createdOnUtcStart) : '') + '/'
                //     + (data.createdOnUtcEnd ? encodeURIComponent(data.createdOnUtcEnd) : '');
                // 
                // var isDiffUrl = $location.path().split('/').length > 5;
                // $location.path(url);
                //// if (!isDiffUrl) {
                // if (isDiffUrl) {
                
                data.selectedDateText = $scope.selectedDateText;

                //if (enableSaveSearch) {
                //    $.cookie('OpportunitysSearchCriteria', JSON.stringify(data));
                //}

                if ($routeParams.commercialType) {
                    data.commercialType = $routeParams.commercialType;
                }

                if ($routeParams.isReportSearch) {
                    data.isReportSearch = $routeParams.isReportSearch;
                }
                data.AccountId = $routeParams.AccountId;
                OpportunitySearchService.Get(data);

                OpportunitySearchService.Get(data);
                $scope.OpportunitySearchService.Opportunitys_options = kendotooltipService.setColumnWidth($scope.OpportunitySearchService.Opportunitys_options);
                //}

                // }
            }
            //window.onresize = function () {
            //    $scope.OpportunitySearchService.Opportunitys_options = kendotooltipService.setColumnWidth($scope.OpportunitySearchService.Opportunitys_options);
            //};
            //$(window).resize(function () {
            //    var grid = $("#gridopportunitySearch").data("kendoGrid");
            //    OpportunitySearchService.Get(data);
            //    if (!kendotooltipService.optimiseTriggerFlag) {
            //        kendotooltipService.optimiseTriggerFlag = true;
            //        OpportunitySearchService.Opportunitys_options = kendotooltipService.setColumnWidth(OpportunitySearchService.Opportunitys_options, true);
            //        grid.refresh();
            //    }
            //});
            $scope.$on('$viewContentLoaded', function () {
                $location.replace(); //clear last history route
            });


            $scope.ConvertArrayToString = function (arr) {
                if (!arr) {
                    return '';
                }
                if ($.isNumeric(arr)) {
                    return arr;
                }
                return arr.join('-');
            }

            $scope.ConvertStringToArray = function (str) {
                var arr = [];
                if (str) {
                    var st = str.split('-');
                    for (var i = 0; i < st.length; i++) {
                        arr.push(st[i]);
                    }
                }
                return arr;
            }
            $scope.DeleteOpportunity = function (Opportunity) {
                if (!confirm("Do you want to continue?")) return;
                HFC.AjaxDelete('/api/Accounts/' + Opportunity.OpportunityId, null, function (result) {
                    HFC.DisplaySuccess("Opportunity successfully deleted");
                    $scope.ApplyFilter();
                });
            }

            $scope.UpdateOpportunityNoteType = function (Opportunity, note, typeenum) {
                if (typeenum != window.NoteTypeEnum.History) {
                    HFC.AjaxPut("/api/Accounts/" + Opportunity.OpportunityId + "/notes", { NoteId: note.NoteId, TypeEnum: typeenum }, function () {
                        $scope.$apply(function () {
                            note.TypeEnum = typeenum;
                        });
                        HFC.DisplaySuccess("Note type successfully updated");
                    });
                }
            }
            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - $("#listdiv").height() - 235);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - $("#listdiv").height() - 235);
                };
            });
            // End Kendo Resizing
            $scope.AddNotes = function (Opportunity, form) {
                var type = Opportunity.NoteType;
                var msg = $.trim(Opportunity.NoteMessage);
                if (msg && msg.length) {
                    HFC.AjaxPost("/api/Opportunitys/" + Opportunity.OpportunityId + "/notes", {
                        TypeEnum: type,
                        Message: HFC.htmlEncode(msg)
                    }, function (result) {
                        if (!Opportunity.OpportunityNotes) {
                            Opportunity.OpportunityNotes = [];
                        }
                        $scope.$apply(function () {
                            Opportunity.OpportunityNotes.splice(0, 0, { NoteId: result.NoteId || 0, AvatarSrc: HFC.CurrentUser.AvatarSrc, TypeEnum: type, Message: HFC.htmlEncode(msg), CreatedByPersonId: HFC.CurrentUser.PersonId, CreatorFullName: HFC.CurrentUser.FullName, CreatedOn: new XDate() });
                            Opportunity.NoteMessage = "";
                        });
                    });
                }
            }

            $scope.AddJobNotes = function (job, form) {
                var type = parseInt(job.NoteType);
                var msg = $.trim(job.NoteMessage);
                if (msg && msg.length) {
                    HFC.AjaxPost("/api/jobs/" + job.JobId + "/notes", {
                        TypeEnum: type,
                        Message: HFC.htmlEncode(msg)
                    }, function (result) {
                        if (!job.JobNotes) {
                            job.JobNotes = [];
                        }

                        $scope.$apply(function () {
                            job.JobNotes.splice(0, 0, { NoteId: result.NoteId || 0, AvatarSrc: HFC.CurrentUser.AvatarSrc, TypeEnum: type, Message: HFC.htmlEncode(msg), CreatedByPersonId: HFC.CurrentUser.PersonId, CreatorFullName: HFC.CurrentUser.FullName, CreatedOn: new XDate() });
                            job.NoteMessage = "";
                        });
                    });
                }
            }

            $scope.UpdateJobNoteType = function (job, note, typeenum) {
                if (typeenum != window.NoteTypeEnum.History) {
                    HFC.AjaxPut("/api/jobs/" + job.JobId + "/notes", { NoteId: note.NoteId, TypeEnum: typeenum }, function () {
                        $scope.$apply(function () {
                            note.TypeEnum = typeenum;
                        });
                        HFC.DisplaySuccess("Note type successfully updated");
                    });
                }
            }


            $scope.GetOpportunityFullName = function (Opportunity) {
                var fullname = "";
                if (Opportunity.PrimPerson) {
                    fullname = $.trim(Opportunity.PrimPerson.FirstName); //trim will convert null to empty string, great for our concatenation
                    if (Opportunity.SecPerson && Opportunity.SecPerson.PersonId) {
                        if (Opportunity.SecPerson.LastName && Opportunity.PrimPerson.LastName && Opportunity.SecPerson.LastName.toLowerCase() == Opportunity.PrimPerson.LastName.toLowerCase())
                            fullname += " & " + $.trim(Opportunity.SecPerson.FirstName) + " " + $.trim(Opportunity.PrimPerson.LastName);
                        else
                            fullname += " " + $.trim(Opportunity.PrimPerson.LastName) + " & " + $.trim(Opportunity.SecPerson.FirstName) + " " + $.trim(Opportunity.SecPerson.LastName);
                    } else
                        fullname += " " + $.trim(Opportunity.PrimPerson.LastName);

                    if (Opportunity.PrimPerson.CompanyName)
                        fullname = "<b>" + Opportunity.PrimPerson.CompanyName + "</b>" + "<br/>" + fullname;
                    else
                        fullname = "<b>" + fullname + "</b>";
                }
                return fullname;
            }

            $scope.GetJobStatuses = function () {
                return LK_JobStatuses;
            };

            $scope.countWatchers = function () {
                var q = [$rootScope], watchers = 0, scope;
                while (q.length > 0) {
                    scope = q.pop();
                    if (scope.$$watchers) {
                        watchers += scope.$$watchers.length;
                    }
                    if (scope.$$childHead) {
                        q.push(scope.$$childHead);
                    }
                    if (scope.$$nextSibling) {
                        q.push(scope.$$nextSibling);
                    }
                }
                window.console.log(watchers);
            };

            $scope.GetOpportunityStatuses = function () {
                return LK_OpportunityStatuses;
            };

            $scope.GetSelectedSalesAgents = function () {
                return sales;
            }

            $scope.GetJobStatusNameById = function (jobStatus) {
                var status = HFC.Lookup.Get(LK_JobStatuses, jobStatus.Id), parent = null;
                if (status.ParentId) {
                    parent = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
                }
                if (parent)
                    return status.Name + "&nbsp;<small class='muted text-amountmuted'>" + parent.Name + "</small>";
                else
                    return status.Name;
            }

            $scope.GetOpportunityStatusColor = function (Opportunity) {
                var status = HFC.Lookup.Get(LK_OpportunityStatuses, Opportunity.OpportunityStatusId);
                if (status.ParentId)
                    status = HFC.Lookup.Get(LK_OpportunityStatuses, status.ParentId);
                return status.ClassName || "btn-default";
            };

            $scope.GetJobStatusColor = function (job) {
                var status = HFC.Lookup.Get(LK_JobStatuses, job.JobStatusId);
                if (!status.ClassName && status.ParentId)
                    status = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
                return (status.ClassName || "btn-default");
            };

            $scope.GetJobStatusName = function (job) {
                var status = HFC.Lookup.Get(LK_JobStatuses, job.JobStatusId),
                    parent = null;
                if (status.ParentId)
                    parent = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
                if (parent)
                    return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
                else
                    return status.Name;
            };

            $scope.GetSelectedSalesAgent = function (job) {
                if (job.SalesPersonId && job.SalesPersonId) {
                    if (HFC.EmployeesJS)
                    var user = $.grep(HFC.EmployeesJS, function (e) {
                        return e.PersonId == job.SalesPersonId;
                    });

                    if (user && user.length)
                        return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
                    else {
                        if (job.SalesPersonId && HFC.DisabledEmployeesJS)
                        var user = $.grep(HFC.DisabledEmployeesJS, function (e) {
                            return e.PersonId == job.SalesPersonId;
                        });

                        if (user && user.length)
                            return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
                        else
                            return "Unassigned";
                    }

                } else
                    return "Unassigned";
            };

            $scope.GetPrimQuote = function (job) {
                return OpportunitySearchService.GetPrimeQuote(job.JobId);
            };

            $scope.FormatPercent = function (num, maxPrecision) {
                return HFC.formatPercent(num, maxPrecision);
            }

            $scope.FormatCurrency = function (num, precision) {
                return HFC.formatCurrency(num, precision);
            };

            $scope.ToFriendlyString = function (date, includeTime, ignoreTZ) {
                var str = "";
                var xdate = false;
                var self = this;


                if (ignoreTZ) {
                    xdate = new XDate(date, true);
                } else {

                    xdate = new XDate(date);
                }

                if (xdate && xdate.valid()) {
                    var today = new XDate();

                    if (xdate.getFullYear() == today.getFullYear()) {
                        if (xdate.getMonth() == today.getMonth() && xdate.getDate() == today.getDate())
                            str = "Today";
                        else if (xdate.getMonth() == today.getMonth() && (today.getDate() - xdate.getDate()) == 1)
                            str = "Yesterday";
                        else
                            str = xdate.toString("ddd, MMM d");
                    } else
                        str = xdate.toString("ddd, MMM d yyyy");

                    if (includeTime) {
                        str += " " + xdate.toString("h:mm tt");
                    }
                }

                return str;
            }

            $scope.GetOpportunityStatusName = function (Opportunity) {
                var status = HFC.Lookup.Get(LK_OpportunityStatuses, Opportunity.OpportunityStatusId), parent = null;
                if (status.ParentId)
                    parent = HFC.Lookup.Get(LK_OpportunityStatuses, status.ParentId);
                if (parent)
                    return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
                else
                    return status.Name;
            }

            $scope.GetOpportunityStatusNameById = function (OpportunityStatus) {
                var status = HFC.Lookup.Get(LK_OpportunityStatuses, OpportunityStatus.Id), parent = null;
                if (status.ParentId) {
                    parent = HFC.Lookup.Get(LK_OpportunityStatuses, status.ParentId);
                }
                if (parent)
                    return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
                else
                    return status.Name;
            }

            function addressToString(isSingleLine, model) {
                if (model) {

                    var str = "";
                    if (model.Address1)
                        str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

                    if (model.City)
                        str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
                    else
                        str += (model.ZipCode || "");

                    return str;
                }
            };



            $scope.GetGoogOpportunitydressHrefOpportunitys = function (dest) {
                //return "maps.google.com?saddr= '1927 n glassell st orange ca'&daddr='21273 beechwood way lake forest ca'";
                var srcAddr = '1927 n glassell st orange ca';
                var dest = '21273 beechwood way lake forest ca';
                return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);

                //if (model.Address1) {

                //    var srcAddr;

                //    if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                //        srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                //    } else {
                //        srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                //    }

                //    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                //        return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                //    } else {
                //        return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                //    }
                //} else
                //    return null;
            }

            $scope.GetGoogOpportunitydressHref = function (model) {
                var dest = addressToString(true, model);
                if (model.Address1) {

                    var srcAddr;

                    if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                        srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                    } else {
                        srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                    }

                    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                        return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                    } else {
                        return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                    }
                } else
                    return null;
            }

            $scope.AddressToString = function (model) { return addressToString(true, model) };

            $scope.GetOpportunityJobs = function (OpportunityId) {
                if (!OpportunityId) {
                    return OpportunitySearchService.Jobs;
                }
                var reults = [];
                for (var i = 0; i < OpportunitySearchService.Jobs.length; i++) {
                    if (OpportunitySearchService.Jobs[i].OpportunityId == OpportunityId) {
                        reults.push(OpportunitySearchService.Jobs[i]);
                    }
                }

                return reults;
            }

            $scope.OpportunityStatusOnChanged = function (Opportunity, val) {
                HFC.AjaxBasic("PUT", HFC.Util.BaseURL() + '/api/Opportunitys/' + Opportunity.OpportunityId + '/Status', JSON.stringify({ "Id": val }), function () {
                    $scope.$apply(function () {
                        Opportunity.OpportunityStatusId = val;
                    });
                    
                    Opportunity.isShowOpportunityStatuses = false;
                    HFC.DisplaySuccess("Status updated for Opportunity #" + Opportunity.OpportunityNumber);
                });
            };

            $scope.GetKeyByValue = function (list, key) {
                return HFC.Lookup.GetKeyByValue(list, key);
            }

            $scope.GetNoteTypeEnum = function () {
                return window.NoteTypeEnum;
            }

            //var statusId = HFCService.GetQueryString("OpportunityStatusId"),
            //    dateRange = HFCService.GetQueryString("dateRange");


            $scope.DateRangeArray = ["All Date & Time", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year", "Last Year"];

            $scope.selectedDateText = "All Date & Time";

            $scope.$watch('$scope.ForSearchReport', function (newVal) {
                if (newVal) {
                    $scope.selectedDateText = "All Date & Time";
                    $scope.ForSearchstartDate = null;
                    $scope.ForSearchendDate = null;
                    $scope.startDate = null;
                    $scope.endDate = null;
                }
            });
            $scope.$watch('startDate', function (newVal) {
                if (newVal != null) {
                    $scope.ForSearchstartDate = newVal;
                    if ($scope.endDate == null) {
                        $scope.selectedDateText = newVal + ' to ';
                    } else {
                        $scope.selectedDateText = newVal + ' to ' + $scope.endDate;
                    }
                }
            });

            $scope.$watch('endDate', function (newVal) {
                if (newVal != null) {
                    $scope.ForSearchendDate = newVal;
                    if ($scope.startDate == null) {
                        $scope.selectedDateText = ' to ' + newVal;
                    } else {
                        $scope.selectedDateText = $scope.startDate + ' to ' + newVal;
                    }
                }
            });

            $scope.endDate = null;
            $scope.startDate = null;
            $scope.ForSearchstartDate = null;
            $scope.ForSearchendDate = null;

            $scope.setDateRange = function (date) {
                var range = $scope.getDateRange(date);
                if (range.StartDate) {
                    $scope.ForSearchstartDate = range.StartDate.toString('MM/dd/yyyy');
                    //$scope.startDate = range.StartDate.toString('MM/dd/yyyy');
                } else {
                    //$scope.startDate = null;
                    $scope.ForSearchstartDate = null;
                }

                if (range.EndDate) {
                    $scope.ForSearchendDate = range.EndDate.toString('MM/dd/yyyy');
                } else {
                    $scope.ForSearchendDate = null;
                }

                $scope.selectedDateText = date;
            }

            $scope.getDateRange = function (range) {
                var endDate = new XDate(),
                    startDate = new XDate();
                switch (range.toLowerCase()) {
                    case "all date & time":
                        startDate = null;
                        endDate = null;
                        break;
                    case "this week":
                        startDate.addDays(-startDate.getDay());
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "this month":
                        startDate.setDate(1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "this year":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addYears(1).addDays(-1);
                        break;
                    case "last week":
                        startDate.addDays(-startDate.getDay() - 7);
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "last month":
                        startDate.setDate(1).addMonths(-1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "last year":
                        startDate.setDate(1).setMonth(0).addYears(-1);
                        endDate.setDate(1).setMonth(0).addDays(-1);
                        break;
                    case "last 7 days":
                        startDate.addDays(-7);
                        break;
                    case "last 30 days":
                        startDate.addDays(-30);
                        break;
                    case "1st quarter":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(3).addDays(-1);
                        break;
                    case "2nd quarter":
                        startDate.setDate(1).setMonth(3);
                        endDate = startDate.clone().addMonths(3).addDays(-1);
                        break;
                    case "3rd quarter":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(3).addDays(-1);
                        break;
                    case "4th quarter":
                        startDate.setDate(1).setMonth(9);
                        endDate = startDate.clone().addMonths(3).addDays(-1);
                        break;
                    case "1st half":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(6).addDays(-1);
                        break;
                    case "2nd half":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(6).addDays(-1);
                        break;
                    default:
                        startDate = null;
                        endDate = null;
                        break;
                }

                return { StartDate: startDate, EndDate: endDate };
            }

            $scope.GetURLParameter = function (sParam) {
                var sPageURL = window.location.search.substring(1);
                var data = $.parseParams(window.location.href);
                if (data) {
                    return data[sParam];
                }
            };

            //if ($routeParams.sortIsDesc) {
            //    $scope.SortIsDesc = $routeParams.sortIsDesc;
            //}

            //if ($routeParams.orderBy) {
            //    $scope.SortBy = $routeParams.orderBy;
            //}


            if ($routeParams.salesPersonIds) {
                if ($.isNumeric($routeParams.salesPersonIds)) {
                    $scope.selectedSales = [];
                    $scope.selectedSales.push($routeParams.salesPersonIds);
                } else {
                    $scope.selectedSales = $scope.ConvertStringToArray($routeParams.salesPersonIds);
                }
            }

            if ($routeParams.jobStatusIds) {
                if ($.isNumeric($routeParams.jobStatusIds)) {
                    $scope.selectedJobsStatuses = [];
                    $scope.selectedJobsStatuses.push(+$routeParams.jobStatusIds);
                } else {
                    $scope.selectedJobsStatuses = $scope.ConvertStringToArray($routeParams.jobStatusIds);

                }
            }
            if ($routeParams.OpportunityStatusIds) {
                if ($.isNumeric($routeParams.OpportunityStatusIds)) {

                    $scope.selectedOpportunityStatuses = [];
                    $scope.selectedOpportunityStatuses.push(+$routeParams.OpportunityStatusIds);
                } else {
                    $scope.selectedOpportunityStatuses = $scope.ConvertStringToArray($routeParams.OpportunityStatusIds);
                }
            }

            if ($routeParams.sourceIds) {
                if ($.isNumeric($routeParams.sourceIds)) {

                    $scope.selectedSources = [];
                    $scope.selectedSources.push(+$routeParams.sourceIds);
                } else {
                    $scope.selectedSources = $scope.ConvertStringToArray($routeParams.sourceIds);
                }
            }

            if ($routeParams.invoiceStatuses) {
                if ($.isNumeric($routeParams.invoiceStatuses)) {
                    $scope.invoiceStatusesSearch = [];
                    $scope.invoiceStatusesSearch.push($routeParams.invoiceStatuses);
                    //$scope.selectedSales = $routeParams.salesPersonIds;
                    //$scope.selectedSales.push(data['salesPersonIds']);
                } else {
                    $scope.invoiceStatusesSearch = $scope.ConvertStringToArray($routeParams.invoiceStatuses);
                }
            }


            if ($routeParams.selectedDateText) {
                $scope.selectedDateText = $routeParams.selectedDateText;
                $scope.ForSearchstartDate = $routeParams.createdOnUtcStart;
                $scope.ForSearchendDate = $routeParams.createdOnUtcEnd;
            } else {
                if ($routeParams.createdOnUtcStart) {
                    $scope.ForSearchstartDate = $routeParams.createdOnUtcStart;
                    $scope.startDate = $routeParams.createdOnUtcStart;
                }
                if ($routeParams.createdOnUtcEnd) {
                    $scope.ForSearchendDate = $routeParams.createdOnUtcEnd;
                    $scope.endDate = $routeParams.createdOnUtcEnd;
                }
            }

            if ($routeParams.searchTerm) {
                $scope.SearchTerm = $routeParams.searchTerm;
                if ($routeParams.searchFilter == "Phone_Number") {
                    var phoneno = /^(?=.*?[1-9])[0-9()-. ]+$/;
                    var inputtxt = $routeParams.searchTerm;
                    if (!inputtxt.match(phoneno)) {
                        HFC.DisplayAlert("Please verify the phone number you entered.");
                        //clear the parameters to show empty result on error
                        var data = {};
                        OpportunitySearchService.Get(data);
                        return false;
                    }
                }
            }


            if ($routeParams.searchFilter != 'undefined') {
                $rootScope.searchMode = $routeParams.searchFilter;
            } else {
                $routeParams.searchFilter = 'Auto_Detect';
                $rootScope.searchMode = $routeParams.searchFilter;
            }

            //Revisit
            //$scope.OpportunityStatusOptions = {
            //    dataTextField: "Id",
            //    dataValueField: "Id",
            //    itemTemplate: '<span>&nbsp; # if (data.IsChiled) { # &nbsp;&nbsp;   # } # #: data.Name # </span> ',
            //    tagTemplate: '<span >#: data.Name #</span> ',
            //    dataSource: OpportunityStatuses,
            //    placeholder: "Select Opportunity Statuses...",
            //};


            $scope.customersDataSource = {
                transport: {
                    read: {
                        dataType: "jsonp",
                        url: "//demos.telerik.com/kendo-ui/service/Customers",
                    }
                }
            };

            $scope.customOptions = {
                dataSource: $scope.customersDataSource,
                dataTextField: "ContactName",
                dataValueField: "CustomerID",

                headerTemplate: '<div class="dropdown-header k-widget k-header">' +
                    '<span>Photo</span>' +
                    '<span>Contact info</span>' +
                    '</div>',

                // using {{angular}} templates:
                valueTemplate: '<span class="selected-value" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span><span>{{dataItem.ContactName}}</span>',

                template: '<span class="k-state-default" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span>' +
                          '<span class="k-state-default"><h3>{{dataItem.ContactName}}</h3><p>{{dataItem.CompanyName}}</p></span>',
            };

            angular.element(document).ready(function () {
                //if ($routeParams.searchTerm)
                $('#search-box').val($routeParams.searchTerm);

                //if ($routeParams.searchFilter) {
                //    $rootScope.searchMode = 'searchFilter';
                //}

                if ($routeParams.searchFilter) {
                    var lis = $('#search-type').find('li');
                    lis.removeClass('active');
                    for (var i = 0; i < lis.length; i++) {
                        if ($(lis[i]).find('input').val() == $routeParams.searchFilter) {
                            $(lis[i]).addClass('active');
                        }
                    }
                    //<li class="active"><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Auto')"><input type="radio" name="searchFilter" value="Auto_Detect" />Auto Detect</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Address')"><input type="radio" name="searchFilter" value="Address" />Address</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Customer Name')"><input type="radio" name="searchFilter" value="Customer_Name" />Customer Name</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Email')"><input type="radio" name="searchFilter" value="Email" />Email</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Job/Invoice Number')"><input type="radio" name="searchFilter" value="Number" />Job/Invoice Number</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Phone Number')"><input type="radio" name="searchFilter" value="Phone_Number" />Phone Number</a></li>

                    if ($routeParams.searchFilter != 'undefined') {
                        switch ($routeParams.searchFilter) {
                            case 'Customer_Name':
                                $rootScope.searchMode = 'Customer Name';
                                break;
                            case 'Number':
                                $rootScope.searchMode = 'Job/Invoice Number';
                                break;
                            case 'Phone_Number':
                                $rootScope.searchMode = 'Phone Number';
                                break;
                            default:
                                $rootScope.searchMode = $routeParams.searchFilter;
                        }
                    }
                    else {
                        $rootScope.searchMode = 'Auto_Detect';
                        $scope.searchFilter = 'Auto_Detect';
                        $routeParams.searchFilter = 'Auto_Detect';
                    }

                }
                
                $scope.ApplyFilter();
            });



            //$scope.tooltipOptions = {
            //    filter: "th,td",
            //    position: "top",
            //    hide: function (e) {
            //        this.content.parent().css('visibility', 'hidden');
            //    },
            //    show: function (e) {
            //        if (this.content.text().length > 1) {
            //            this.content.parent().css('visibility', 'visible');
            //        } else {
            //            this.content.parent().css('visibility', 'hidden');
            //        }
            //    },
            //    content: function (e) {
            //        var target = e.target.data().title; // element for which the tooltip is shown
            //        return target;//$(target).text();
            //    }
            //};


            $scope.calendarModalId = 0;

            //$scope.SetAppointment = function (modalid, accountId, opportunityId) {
               
            //    calendarService.IsOpportunity = true;
            //    calendarService.opportunityId = opportunityId;
            //    calendarService.NewOpportunityApt(modalid, opportunityId);
            //}
         

            //saveCriteriaService.selectedJobsStatuses = $scope.selectedJobsStatuses;
            //saveCriteriaService.selectedOpportunityStatuses = $scope.selectedOpportunityStatuses;
            //saveCriteriaService.selectedSources = $scope.selectedSources;
            //saveCriteriaService.selectedSales = $scope.selectedSales;
            //saveCriteriaService.invoiceStatusesSearch = $scope.invoiceStatusesSearch;

            //saveCriteriaService.selectedDateText = $scope.selectedDateText;
            //saveCriteriaService.ForSearchstartDate = $scope.ForSearchstartDate;
            //saveCriteriaService.ForSearchendDate = $scope.ForSearchendDate;
            //saveCriteriaService.startDate = $scope.startDate;
            //saveCriteriaService.endDate = $scope.endDate;

            $rootScope.$broadcast('applyMainSearchFilter');

            //$('head title').text("" + applicationTitle);
            HFCService.setHeaderTitle("Opportunity #");

            // Helper function to call the print feature.
            $scope.OpportunitySearchService.printControl = {};
            $scope.printSalesPacket2 = function (modalid, opportunityId) {
                $scope.OpportunitySearchService.opportunityIdPrint = opportunityId;

                //$("#" + modalid).modal("show");
                $scope.OpportunitySearchService.printControl.showPrintModal();
            }

            

            $scope.DownloadOpportunity_Excel = function () {
                var grid = $("#gridopportunitySearch").data("kendoGrid");
                grid.saveAsExcel();
            }


        }
    ])
