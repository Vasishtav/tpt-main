﻿'use strict';
app.controller('orderSearchController',
    [
         '$scope', '$http', 'HFCService', 'NavbarService', 'PrintService', 'LeadAccountService', '$location', '$routeParams', 'kendotooltipService',
          function ($scope, $http, HFCService, NavbarService, PrintService, LeadAccountService, $location, $routeParams, kendotooltipService) {
              $scope.NavbarService = NavbarService;

              $scope.NavbarService.SelectSales();

              $scope.Permission = {};

              $scope.SelectedOrderStatus = [];
              $scope.BrandId = HFCService.CurrentBrand;
              $scope.ActiveToolTips = false;
              //Hardcoded the value for Tool Tip
              $scope.OrderLinesToolTip = "Total number of lines on the Sales Order";
              //$scope.TotalQTYToolTip = "Count of all Products on sales order(Core Products and My Products)";
              $scope.TotalAmountToolTip = "Total Sales Amount";
              $scope.SOBalanceToolTip = "Sales Order Open Balance";
              HFCService.GetUrl();
              $scope.AccountId = $routeParams.AccountId;
              var offsetvalue = 0;
              if ($scope.Account == null || $scope.Account == undefined) {
                  $scope.Account = {};
                  $scope.Account.AccountId = $routeParams.AccountId;
                  var AccountId = $scope.Account.AccountId;
                  if (AccountId > 0) {
                      LeadAccountService.Get(AccountId, function (response) {
                          $scope.OrderAccount = response;
                      });
                  }
              }

              var Id = 0;
              if ($scope.AccountId != null) {
                  var ul = $location.absUrl().split('#!')[1];
                  if (ul.includes('accountopportunitySearch') || ul.includes('accountordersSearch')) {
                      $scope.NavbarService.EnableSalesAccountsTab();
                  } else if (ul.includes('opportunitySearch')) {
                      $scope.NavbarService.EnableSalesOpportunitiesTab();
                  }
                  $scope.Permission = {};
                  var Opportunitypermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity')
                  var SalesOrderpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SalesOrder')

                  $scope.Permission.ListOpportunity = Opportunitypermission.CanRead; //HFCService.GetPermissionTP('List Opportunity').CanAccess;
                  $scope.Permission.ListOrderList = SalesOrderpermission.CanRead; //HFCService.GetPermissionTP('List Order').CanAccess;
                  //$scope.Permission.AccountOrderList = HFCService.GetPermissionTP('List Order').CanAccess;
                  $scope.Permission.AddNotesAccount = SalesOrderpermission.CanCreate; //HFCService.GetPermissionTP('Add Notes&Attachments-Account').CanAccess;
                  $scope.Permission.AddOpportunity = Opportunitypermission.CanCreate; //HFCService.GetPermissionTP('Add Opportunity').CanAccess;
                  //$scope.Permission.AddEvent = HFCService.GetPermissionTP('Add Event').CanAccess;
                  Id = $scope.AccountId;
              }
              else {
                  var SalesOrderpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SalesOrder')
                  $scope.Permission.ListOrderList = SalesOrderpermission.CanRead; //HFCService.GetPermissionTP('List Order').CanAccess;
                  var ul = $location.absUrl().split('#!')[1];
                  if (ul.includes('orderSearch')) {
                      $scope.NavbarService.EnableSalesOrdersTab();
                  }
                  Id = 0;
              }

              $scope.PermissionEditOrder = SalesOrderpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Order').CanAccess;

              HFCService.setHeaderTitle("Order #");
              function DropdownTemplate(dataitem) {
                  if (dataitem.Status == "Void" || dataitem.Status == "Cancelled" || dataitem.Status == "Canceled") {
                      return "";
                  }
                  else{
                  var PermissionEditOrder = $scope.PermissionEditOrder;
                  var EditOrder = "";
                  if (PermissionEditOrder == true) {
                      EditOrder = '<li><a href="#!/orderEdit/' + dataitem.OrderId + '">Edit</a></li> ';
                  }
                  return '<ul> <li class="dropdown note1 ad_user"> <div class="dropdown">' +
                      ' <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                      ' <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right">' +
                      EditOrder +
                      //' <li><a href="\\\\#!/orderEdit/#= OrderId #">Edit</a></li>' +
                      //'<li><a  ng-click="ShowInstallationPrint2(' + dataitem.OrderId + ')">Print</a></li>' +
                      '</ul> </li>  </ul>'
                  }
              }
              function OrderNameTemplate(dataitem) {
                  if (dataitem.ReversalAmount && (dataitem.Status == 'Cancelled' || dataitem.Status == 'Void')) {
                      return '<a href="#!/Orders/' + dataitem.OrderId + '"><span style="color:red !important">' + dataitem.OrderName + '</span></a>'
                  }
                  else {
                      return '<a href="#!/Orders/' + dataitem.OrderId + '" style = "color: rgb(61,125,139);"><span >' + dataitem.OrderName + '</span></a>';
                  }
              }
              $scope.GridEditable = false;
              $scope.InitialLoad = true;
              $scope.getGridData = function () {
                  $scope.OrderGridList = {
                      cache: false,
                      excel: {
                          fileName: "SalesOrders_Export.xlsx",
                          allPages: true
                      },
                      dataSource: {
                          transport: {
                              read: {
                                  url: function (params) {
                                      var url1 = '/api/search/' + Id + '/GetSearchOrders?orderStatusListValue=' + $scope.SelectedOrderStatus + '&dateRangeValue=' + $scope.DateRangeId;
                                      return url1;
                                  },
                                  dataType: "json"
                              }
                          },
                          //requestStart: function () {
                          //    if (!$scope.InitialLoad)
                          //    kendo.ui.progress($("#gridOrderSearch"), true);
                          //},
                          //requestEnd: function () {
                          //    if (!$scope.InitialLoad)
                          //        kendo.ui.progress($("#gridOrderSearch"), false);
                          //    else
                          //        $scope.InitialLoad = false;
                          //},
                          error: function (e) {
                              HFC.DisplayAlert(e.errorThrown);
                             // kendo.ui.progress($("#gridOrderSearch"), false);
                          },
                          schema: {
                              model: {
                                  id: "OrderNumber",
                                  fields: {
                                      Territory: {  editable: false, nullable: true },
                                      OrderNumber: { type: "number", editable: false, nullable: true },
                                      QuoteID: { type: "number" ,editable: false, nullable: true },
                                      QuoteKey: { editable: false, nullable: true },
                                      OrderName: { editable: false, nullable: true },
                                      AccountName: { validation: { required: true } },
                                      OpportunityName: { validation: { required: true } },
                                      Status: { validation: { required: false } },
                                      SalesAgentName: { validation: { required: false } },
                                      PrimaryQuote: { validation: { required: true } },
                                      TotalLines: { type: "number", editable: false },
                                      TotalQTY: { type: "number", editable: false },
                                      BalanceDue: { type: "number", editable: false },
                                      TotalAmount: { type: "number", validation: { required: true } },
                                      ContractDate: { type: 'date', validation: { required: true } },
                                      CreatedDate: { type: 'date', validation: { required: true } },
                                      ModifiedDate: { type: 'date', validation: { required: true } },
                                  }
                              }
                          }
                      },
                      dataBound: function (e) {
                          // auto fit column
                          //for (var i = 0; i < this.columns.length; i++) {
                          //    this.autoFitColumn(i);
                          //}
                          // end auto fit column
                          //console.log("dataBound");
                          if (this.dataSource.view().length == 0) {
                              // The Grid contains No recrods so hide the footer.
                              $('.k-pager-nav').hide();
                              $('.k-pager-numbers').hide();
                              $('.k-pager-sizes').hide();
                          } else {
                              // The Grid contains recrods so show the footer.
                              $('.k-pager-nav').show();
                              $('.k-pager-numbers').show();
                              $('.k-pager-sizes').show();
                          }
                          if (kendotooltipService.columnWidth) {
                              kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                          } else if (window.innerWidth < 1280) {
                              kendotooltipService.restrictTooltip(null);
                          }
                          $scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
                      },
                      columns: [
                          {
                              field: "Territory",
                              title: "Territory",
                              template: function (dataItem) {
                                  if (dataItem.Territory) {
                                      return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Territory + "</div><span>" + dataItem.Territory + "</span></span>";
                                  } else {
                                      return "";
                                  }
                              },
                              width: "250px",
                              filterable: {
                                  //multi: true,
                                  search: true
                              }
                          },
                                {
                                    field: "OrderNumber",
                                    title: "Order",
                                    width: "100px",
                                    filterable: { search: true },
                                    template: function (dataItem) {
                                        if (dataItem.OrderNumber) {
                                            return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OrderNumber + "</div><a href='/#!/Orders/" + dataItem.OrderId + "' style='color: rgb(61,125,139);'>" + dataItem.OrderNumber + "</a></span></span>";
                                        } else {
                                            return "";
                                        }
                                    },
                                    //template: "<a href='\\\\#!/Orders/#= OrderId #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a> ",
                                    
                                },
                                {
                                    field: "OrderName",
                                    title: "Order Name",
                                    template: function (dataItem) {
                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OrderName + "</div><span>" + dataItem.OrderName + "</span></span>";
                                    },
                                    width: "150px",
                                    hidden: true,
                                    template: function (dataitem) {
                                        return OrderNameTemplate(dataitem);
                                    },
                                    //template: "<a href='\\\\#!/Orders/#= OrderId #' ><span style='color:red !important'>#= OrderName #</span></a> ",
                                    filterable: { search: true }
                                },
                                {
                                    field: "ContractDate",
                                    title: "Contracted Date",
                                    width: "150px",
                                    type: "date",
                                    filterable: HFCService.gridfilterableDate("date", "OrderFilterCalendar", offsetvalue),
                                    template: function (dataItem) {
                                        if (dataItem.ContractDate) {
                                            return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.ContractDate) + "</div><span>" + HFCService.KendoDateFormat(dataItem.ContractDate) + "</span></span>";
                                        } else {
                                            return "";
                                        }
                                    }
                                },
                                {
                                    field: "QuoteKey",
                                    title: "Quote Key",
                                    template: function (dataItem) {
                                        if (dataItem.QuoteKey) {
                                            return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.QuoteKey + "</div><span>" + dataItem.QuoteKey + "</span></span></span>";
                                        } else {
                                            return "";
                                        }
                                    },
                                    width: "120px",
                                    hidden: true
                                },
                                {
                                    field: "QuoteID",
                                    title: "Quote",
                                    width: "100px",
                                    filterable: { search: true },
                                    template: function (dataItem) {
                                        if (dataItem.QuoteID) {
                                            return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.QuoteID + "</div><a href='/#!/quote/" + dataItem.OpportunityId + "/" + dataItem.QuoteKey + "' style='color: rgb(61,125,139);'>" + dataItem.QuoteID + "</a></span></span>";
                                        } else {
                                            return "";
                                        }
                                    },
                                    //template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteID #</span></a> ",
                                },
                                {
                                    field: "AccountName",
                                    title: "Account Name",
                                    filterable: { search: true },
                                    width: "150px",
                                    template: function (dataItem) {
                                        if (dataItem.AccountName) {
                                            return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountName + "</div><a href='/#!/Accounts/" + dataItem.AccountId + "' style='color: rgb(61,125,139);'>" + dataItem.AccountName + "</a></span>";
                                        } else {
                                            return "";
                                        }
                                    },
                                    //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= AccountName #</span></a> ",
                                    
                                },
                                     {
                                         field: "OpportunityName",
                                         title: "Opportunity",
                                         width: "150px",
                                         filterable: { search: true },
                                         template: function (dataItem) {
                                             if (dataItem.OpportunityName) {
                                                 return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OpportunityName + "</div><a href='/#!/OpportunityDetail/" + dataItem.OpportunityId + "' style='color: rgb(61,125,139);'>" + dataItem.OpportunityName + "</a></span>";
                                             } else {
                                                 return "";
                                             }
                                         },
                                         //template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span>#= OpportunityName #</span></a> ",
                                         
                                     },
                                      {
                                          field: "SideMark",
                                          title: "Sidemark",
                                          filterable: { search: true },
                                          template: function (dataItem) {
                                              if (dataItem.SideMark) {
                                                  return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.SideMark + "</div><span>" + dataItem.SideMark + "</span></span>";
                                              } else {
                                                  return "";
                                              }
                                          },
                                          width: "110px",
                                        
                                      },
                                       {
                                           field: "SalesAgentName",
                                           title: "Sales Agent",
                                           filterable: { search: true },
                                           template: function (dataItem) {
                                               if (dataItem.SalesAgentName) {
                                                   return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.SalesAgentName + "</div><span>" + dataItem.SalesAgentName + "</span></span>";
                                               } else {
                                                   return "";
                                               }
                                           },
                                           width: "120px",
                                          
                                       },

                                       {

                                           field: "TotalLines",
                                           filterable: { search: true },
                                           title: "Order Lines",
                                           template: function (dataItem) {
                                               if (dataItem.TotalLines) {
                                                   return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.TotalLines + "</div><span>" + dataItem.TotalLines + "</span></span></span>";
                                               } else {
                                                   return "";
                                               }
                                           },
                                           width: "100px",
                                           //headerTemplate: '<div kendo-tooltip k-content="OrderLinesToolTip" >Order Lines</div>',
                                          
                                       },

                                        {
                                            field: "TotalQTY",
                                            title: "Total QTY",
                                            
                                            template: function (dataItem) {
                                                if (dataItem.TotalQTY) {
                                                    return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.NumberFormat(dataItem.TotalQTY, 'number') + "</div><span>" + dataItem.TotalQTY + "</span></span><span>";
                                                } else {
                                                    return "";
                                                }
                                            },
                                            width: "100px",
                                            headerTemplate: '<div kendo-tooltip k-content="OrderLinesToolTip" >Total QTY</div>',
                                           
                                        },

                                     {
                                         field: "TotalAmount",
                                         filterable: { search: true },
                                        // format: "{0:c2}",
                                         template: function (dataItem) {
                                                 return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.CurrencyFormat(dataItem.TotalAmount) + "</div><span>" + HFCService.CurrencyFormat(dataItem.TotalAmount) + "</span></span></span>";
                                         },
                                         width: "150px",
                                         attributes: { class: "text-right" },
                                         // title: "Total Amount",
                                         headerTemplate: '<div kendo-tooltip k-content="TotalAmountToolTip" >Total Amount</div>',
                                         
                                     },
                                     {
                                         field: "BalanceDue",
                                        
                                        // format: "{0:c2}",
                                         template: function (dataItem) {
                                                 return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.CurrencyFormat(dataItem.BalanceDue) + "</div><span>" + HFCService.CurrencyFormat(dataItem.BalanceDue) + "</span></span></span>";
                                         },
                                         width: "150px",
                                         attributes: { class: "text-right" },
                                         //title: "SO Balance",
                                         headerTemplate: '<div kendo-tooltip k-content="SOBalanceToolTip" >SO Balance</div>',
                                         
                                     },
                                      {
                                          field: "Status",
                                          title: "Status",
                                          template: function (dataItem) {
                                              if (dataItem.Status) {
                                                  return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Status + "</div><span>" + dataItem.Status + "</span></span>";
                                              } else {
                                                  return "";
                                              }
                                          },
                                          width: "100px",
                                          filterable: { multi: true, search: true }
                                      },
                                      {
                                          field: "CreatedDate",
                                          title: "Created Date",
                                          type: "date",
                                          filterable: HFCService.gridfilterableDate("date", "OrderCreatedFilterCalender", offsetvalue),
                                          template: function (dataItem) {
                                              if (dataItem.CreatedDate) {
                                                  return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CreatedDate + "</div><span>" + dataItem.CreatedDate + "</span></span>";
                                              } else {
                                                  return "";
                                              }
                                          },
                                          width: "50px",
                                          hidden: true
                                      },
                                       {
                                           field: "ModifiedDate",
                                           title: "Modified Date",
                                           type: "date",
                                           filterable: HFCService.gridfilterableDate("date", "OrderModifiedFilterCalender", offsetvalue),
                                           template: function (dataItem) {
                                               if (dataItem.ModifiedDate) {
                                                   return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.ModifiedDate + "</div><span>" + dataItem.ModifiedDate + "</span></span>";
                                               } else {
                                                   return "";
                                               }
                                           },
                                           width: "50px",
                                           hidden: true
                                       },
                                     {
                                         field: "",
                                         title: "",
                                         width: "100px",
                                         template: function (dataitem) {
                                             return DropdownTemplate(dataitem);
                                         },
                                         // attributes: { style: "vertical-align: middle;" },
                                         //template: '<ul ng-if="srv.PermissionEditOrder"> <li class="dropdown note1 ad_user"> <div class="btn-group"> <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">  <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu"> <li><a href="\\\\#!/orderEdit/#= OrderId #">Edit</a></li>   <li><a  ng-click="ShowInstallationPrint2(#= OrderId #)">Print</a></li></ul> </li>  </ul>'
                                     }

                      ],
                      resizable: true,
                      filterable: {
                          extra: true,
                          operators: {
                              string: {
                                  contains: "Contains",
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  isempty: "Empty",
                                  isnotempty: "Not empty"


                              },
                              number: {
                                  eq: "Equal to",
                                  neq: "Not equal to",
                                  gte: "Greater than or equal to",
                                  lte: "Less than or equal to"
                              },
                              date: {
                                  gt: "After (Excludes)",
                                  lt: "Before (Includes)"
                              },
                          }
                      },
                      noRecords: { template: "No records found" },
                      pageable: {
                          refresh: true,
                          pageSize: 25,
                          pageSizes: [25, 50, 100],
                          buttonCount: 5,
                          change: function (e) {
                              var myDiv = $('.k-grid-content.k-auto-scrollable');
                              myDiv[0].scrollTop = 0;
                          }
                      },
                      sortable: ({ field: "CreatedOnUtc", dir: "desc" }),

                      toolbar: kendo.template($("#headToolbarTemplate").html()),
                      columnResize: function (e) {
                          $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                          getUpdatedColumnList(e);
                      },
                      filterMenuInit: function (e) {
                          //$(e.container).find('.k-check-all').click();
                          e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                      },
                  };
                  $scope.OrderGridList = kendotooltipService.setColumnWidth($scope.OrderGridList);
              }

              var getUpdatedColumnList = function (e) {
                  var heightValue = $scope.wrapperHeightValue + "px";
                  kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
                  $(".k-grid-content.k-auto-scrollable").css("height", heightValue);
              }

              $scope.OrdergridSearchClear = function () {
                  $('#searchBox').val('');
                  $("#gridOrderSearch").data('kendoGrid').dataSource.filter({
                  });
              }

              $scope.OrdergridSearch = function () {
                  var searchValue = $('#searchBox').val();
                  $("#gridOrderSearch").data("kendoGrid").dataSource.filter({
                      logic: "or",
                      filters: [

                         {
                             field: "OrderNumber",
                             operator: "eq",
                             value: searchValue
                         },
                          {
                              field: "QuoteID",
                              operator: "eq",
                              value: searchValue
                          },
                          {
                              field: "OrderName",
                              operator: "contains",
                              value: searchValue
                          },
                         {
                             field: "AccountName",
                             operator: "contains",
                             value: searchValue
                         },
                         {
                             field: "OpportunityName",
                             operator: "contains",
                             value: searchValue
                         },
                         {
                             field: "SalesAgentName",
                             operator: "contains",
                             value: searchValue
                         },
                       {
                           field: "Status",
                           operator: "contains",
                           value: searchValue
                       },
                        {
                            field: "TotalAmount",
                            operator: "eq",
                           value: searchValue
                        },
                        {
                            field: "SideMark",
                            operator: "contains",
                            value: searchValue
                        },

                      ]
                  });
              }

              $scope.PrintQuotee = function (QuoteKey) {
                  //
                  $scope.PrintService.PrintQuotee(QuoteKey);
              }

              $scope.EnableDisableToolTips = function () {
                  $scope.ActiveToolTips = !$scope.ActiveToolTips;
              }

              // Excel downloAD
              $scope.DownloadSalesOrder_Excel = function () {
                  var grid = $("#gridOrderSearch").data("kendoGrid");
                  grid.saveAsExcel();
              }

              //to get Dashboard Filter
              $http.get('/api/lookup/0/Type_DateTime?Tableid=' + 4).then(function (data) {
                  $scope.DateRangeValue = data.data;
                  for (var i = 0; i < $scope.DateRangeValue.length; i++) {
                      if ($scope.DateRangeValue[i]['Name'] == 'Last 90 days (rolling)') {
                          $scope.DateRangeId = $scope.DateRangeValue[i]['Id'];
                          $scope.OnChangeDateRangeFilter($scope.DateRangeValue[i]['Id']);
                          $scope.getGridData();
                      }
                  }
              })

              //on selecting dashboard drop-down value(Date/time drop-down)
              $scope.OnChangeDateRangeFilter = function (id) {
                  if (id === null || id === '' || id === undefined) {
                      $scope.DateRangeId = '';
                  } else {
                      $scope.DateRangeId = id;
                  }
                  if ($('#gridOrderSearch').data('kendoGrid')) {
                      $('#gridOrderSearch').data('kendoGrid').dataSource.read();
                      //$('#gridOrderSearch').data('kendoGrid').refresh(); 
                    
                      $scope.OrderGridList = kendotooltipService.setColumnWidth($scope.OrderGridList);
                  }
                  return;
              }

              //on clicking the checkboxes values to be passed
              $scope.FilterBySelectedOrder = function () {
                  $scope.SelectedOrderStatus = $scope.Valueselected();
                  $('#gridOrderSearch').data('kendoGrid').dataSource.read();
                 //$('#gridOrderSearch').data('kendoGrid').refresh();
                 
                  $scope.OrderGridList = kendotooltipService.setColumnWidth($scope.OrderGridList);
                  var myDiv = $('.k-grid-content.k-auto-scrollable');
                  myDiv[0].scrollTop = 0;
              }
              // QuoteList.Get(data);
              //$(window).resize(function () {
              //    var grid = $("#gridOrderSearch").data("kendoGrid");

              //    if (!kendotooltipService.optimiseTriggerFlag) {
              //        kendotooltipService.optimiseTriggerFlag = true;
              //        OrderGridList = kendotooltipService.setColumnWidth(OrderGridList, true);
              //        grid.refresh();
              //    }
              //});
              //window.onresize = function () {
              //    $scope.OrderGridList = kendotooltipService.setColumnWidth($scope.OrderGridList);
              //};
              //getting the check-box value
              $scope.bringTooltipPopup = function () {
                  $(".statusCanvas").show();
              }
              $scope.Valueselected = function () {
                  var arr = [];
                  var result = '';
                  if ($('#Check1').is(":checked")) {
                      result = 7;
                      arr.push(result);
                  }
                  if ($('#Check2').is(":checked")) {
                      result = 8;
                      arr.push(result);
                  }
                  if ($('#Check3').is(":checked")) {
                      result = 1;
                      arr.push(result);
                  }
                  if ($('#Check4').is(":checked")) {
                      result = 2;
                      arr.push(result);
                  }
                  if ($('#Check5').is(":checked")) {
                      result = 3;
                      arr.push(result);
                  }
                  if ($('#Check6').is(":checked")) {
                      result = 4;
                      arr.push(result);
                  }
                  if ($('#Check7').is(":checked")) {
                      result = 5;
                      arr.push(result);
                  }
                  if ($('#Check8').is(":checked")) {
                      result = 9;
                      arr.push(result);
                  }
                  if ($('#Check9').is(":checked")) {
                      result = 6;
                      arr.push(result);
                  }
                  if ($('#receiveFullCheck').is(":checked")) {
                      result = 3;
                      arr.push(result);
                  }

                  return arr;
              }

              function addressToString(isSingleLine, model) {
                  if (model) {
                      var str = "";
                      if (model.Address1)
                          str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

                      if (model.City)
                          str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
                      else
                          str += (model.ZipCode || "");

                      return str;
                  }
              };
              $scope.GetGoogOrderdressHref = function (model) {
                  var dest = addressToString(true, model);
                  if (model.Address1) {
                      var srcAddr;

                      if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                          srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                      } else {
                          srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                      }

                      if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                          return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                      } else {
                          return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                      }
                  } else
                      return null;
              }

              $scope.AddressToString = function (model) { return addressToString(true, model) };

              // Kendo Resizing
              $scope.$on("kendoWidgetCreated", function (e) {
                  $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 405);
                  window.onresize = function () {
                      $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 405);
                  };
              });
              // End Kendo Resizing
          }

    ])