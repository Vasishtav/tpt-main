﻿
// TODO: rquired refactoring:

'use strict';
app
    .directive('dropDown', function () {
        return {
            link: function (scope, element, attr, ctrl) {
                var $el = $(element);

                $el.parent().on({
                    "click": function (e) {
                        if ($(e.currentTarget).hasClass("btn-group")) {
                            var obj = $(this);
                            obj.data("closable", false);
                            setTimeout(function () { obj.data("closable", true); }, 500);
                        } else
                            $(this).data("closable", true);
                    },
                    "hide.bs.dropdown": function (e) {
                        if ($(e.relatedTarget).hasClass("inner-dropdown"))
                            return true;
                        else
                            return $(this).data("closable");
                    }
                });
            }
        }
    })
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('accountSearchController',
    [
        '$scope', 'HFCService', 'AccountSearchService', '$routeParams', 'kendotooltipService'
        , '$location', '$rootScope', 'saveCriteriaService', '$templateCache'
        , '$sce', '$compile', 'FileprintService', 'AccountService'
        , 'CalendarService', 'NavbarService', 'kendoService', '$http',
function ($scope, HFCService, AccountSearchService,  $routeParams, kendotooltipService
    , $location, $rootScope, saveCriteriaService, $templateCache
    , $sce, $compile, FileprintService, accountService
    , calendarService, NavbarService, kendoService, $http) {

    $rootScope.enableSaveSearch = enableSaveSearch;

    $scope.NavbarService = NavbarService;

    $scope.NavbarService.SelectSales();
    //
    $scope.NavbarService.EnableSalesAccountsTab();

    $scope.showAccountStatus = function (Account) {
        if (Account.isShowAccountStatuses) {
            return;
        }
        Account.isShowAccountStatuses = !Account.isShowAccountStatuses;
    };

    $scope.showNotes = function (Account) {
        Account.isShowNotes = !Account.isShowNotes;
    };

    $scope.AccountSearchService = AccountSearchService;
    $scope.HFCService = HFCService;
    $scope.filter_CounterSelectedSalesAgent = 'Nothing selected';
    $scope.filter_CounterSelectedJobStatus = 'Nothing selected';
    $scope.selectedSales = null;
    $scope.selectedAccountStatuses = null;
    $scope.selectedSources = null;
    $scope.SearchFilter = 'Auto_Detect';
    $scope.FileprintService = FileprintService;

    // for kendo tooltip
    $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
    kendoService.customTooltip = null;

    $scope.ChangeFilterSearch = function (val) {

        $scope.SearchFilter = val;
    }


    $scope.Permission = {};
    $scope.AccountPermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Account')
    $scope.Permission.ListAccount = $scope.AccountPermission.CanRead;//$scope.HFCService.GetPermissionTP('List Account').CanAccess;

    $scope.getClassAccountSearch = function (val) {

        if (val == $scope.SearchFilter) {
            return 'active';
        }
        return '';
    }

   
    $scope.AccountgridSearchClear = function () {
        $('#searchBox').val('');
        $("#gridAccountSearch").data('kendoGrid').dataSource.filter({
        });
        AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth(AccountSearchService.Accounts_options);
    }

    // for checkbox to bring Accounts with Inactive/Merged account
    $scope.GetSearchAccounts_forcheck = function () {
        var includeInactive = $('input[name="inactiveMergedAccount_Check"]:checked').val();
        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
        var accountSearchUrl = includeInactive == "on" ? '/api/search/1/GetSearchAccounts' :
            '/api/search/0/GetSearchAccounts';

        $http.get(accountSearchUrl).then(function (response) {

            var data = response.data;
            var grid = $("#gridAccountSearch").data("kendoGrid");
            var dataSource = grid.dataSource;
            dataSource._data = data;
            //grid.dataSource.data(data);
            //for displaying status in kendo grid filter
            grid.setDataSource(data);
            AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth(AccountSearchService.Accounts_options);
            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
        }).catch(function (e) {
            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
        });
    }

    $scope.NewAccountpage = function () {
        window.location.href = '/#!/account';
    }
    $scope.ShowEditMeasurements = function (accountId, MeasurementCount) {
        //if (MeasurementCount == 0) {
        //    HFC.DisplayAlert("No Measurements found for the account");
        //    return;
        //}
        //else {
        //    window.location.href = '/#!/measurementDetails/' + accountId;
        //}
        window.location.href = '/#!/measurementDetails/' + accountId;
    }

    
    $scope.AccountgridSearch = function () {
        var searchValue = $('#searchBox').val();
        $("#gridAccountSearch").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
                {
                    field: "CompanyName",
                    operator: "contains",
                    width: "150px",
                    value: searchValue
                },
              {
                  field: "AccountFullName",
                  operator: "contains",
                  width: "150px",
                  value: searchValue
              },
              {
                  field: "AccountType", //in db it is AccountTypeId
                  operator: "contains",
                  width: "100px",
                  value: searchValue
              },
              {
                  field: "Status",
                  operator: "contains",
                  width: "100px",
                  value: searchValue
              },
              {
                  field: "Address1",
                  operator: "contains",
                  width: "200px",
                  value: searchValue
              },
               {
                   field: "City",
                   operator: "contains",
                   width: "100px",
                   value: searchValue
               },
                {
                    field: "State",
                    operator: "contains",
                    width: "100px",
                    value: searchValue
                },
              {
                  field: "ZipCode",
                  operator: "contains",
                  width: "100px",
                  value: searchValue
              },
              {
                  field: "DisplayPhone",
                  operator: "contains",
                  value: searchValue
              },
               {
                   field: "PrimaryEmail",
                   operator: "contains",
                   value: searchValue
               }
            ]
        });

    }
    $scope.filter_SelectSalesAgent = function (id) {
        $scope.filter_CounterSelectedSalesAgent = id;
    }

    $scope.filter_SelectJobStatus = function (id) {
        $scope.filter_CounterSelectedJobStatus = id;
    }
    $scope.SortBy = 'createdonutc';
    $scope.SortIsDesc = true;
    $scope.ChangeSortOrder = function (sortBy) {
        if (sortBy) {
            if (sortBy != $scope.SortBy) {
                $scope.SortBy = sortBy;
                $scope.SortIsDesc = true;
            } else {
                $scope.SortIsDesc = !$scope.SortIsDesc;

            }
            $scope.ApplyFilter();
        }
    }

    $scope.GoToFirstPage = function () {
        $scope.AccountSearchService.Pagination.page = 1;
        $scope.ApplyFilter();
    }

    $scope.GoToPreviousPage = function () {
        if ($scope.AccountSearchService.Pagination.page > 1) {
            $scope.AccountSearchService.Pagination.page = +($scope.AccountSearchService.Pagination.page) - 1;
            $scope.ApplyFilter();
        }
    }

    $scope.GoToNextPage = function () {

        if ($scope.AccountSearchService.Pagination.page == $scope.AccountSearchService.Pagination.pageTotal) {
            return;
        }
        $scope.AccountSearchService.Pagination.page = +($scope.AccountSearchService.Pagination.page) + 1;
        $scope.ApplyFilter();
    }

    $scope.GoToLastPage = function () {
        $scope.AccountSearchService.Pagination.page = $scope.AccountSearchService.Pagination.pageTotal;
        $scope.ApplyFilter();
    }

    $scope.ChangePageSize = function () {
        $scope.ApplyFilter();
    }

    $scope.ExportExcel = function () {
        var data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };

        if ($scope.selectedSales) {
            data.salesPersonIds = $scope.selectedSales;
        }

        if ($scope.selectedJobsStatuses) {
            data.jobStatusIds = $scope.selectedJobsStatuses;
        }

        if ($scope.selectedAccountStatuses) {
            data.AccountStatusIds = $scope.selectedAccountStatuses;
        }

        if ($scope.selectedSources) {
            data.sourceIds = $scope.selectedSources;
        }

        if ($scope.invoiceStatusesSearch) {
            data.invoiceStatuses = $scope.invoiceStatusesSearch;
        }

        if ($scope.ForSearchstartDate)
            data.createdOnUtcStart = $scope.ForSearchstartDate;
        if ($scope.ForSearchendDate)
            data.createdOnUtcEnd = $scope.ForSearchendDate;

        data.searchTerm = $('#search-box').val();

        data.searchFilter = $('#search-type').find('.active').find('input').val();

        if ($scope.SearchTerm) {
            data.searchTerm = $scope.SearchTerm;
        }

        data.pageIndex = $scope.AccountSearchService.Pagination.page;
        data.pageSize = $scope.AccountSearchService.Pagination.size;

        data.ForSearchstartDate = $scope.ForSearchstartDate;
        data.ForSearchendDate = $scope.ForSearchendDate;

        if ($routeParams.commercialType) {
            data.commercialType = $routeParams.commercialType;
        }

        window.open('/export/Accounts?' + $.param(data), '_blank');
    }

    // Set Appointment for lead integration
    $scope.Account = null;
   

    $scope.SetAppointment = function (modalid, accountId) {
        
        calendarService.IsOpportunity = false;
        calendarService.opportunityId = 0;
        var result = calendarService.NewAccountApt(modalid, accountId);
    }

    $scope.ApplyFilter = function (isClickedFromUI) {

        var data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };

        if (isClickedFromUI) {
            data.pageIndex = 1;
            $scope.AccountSearchService.Pagination.page = 1;
        }

        if ($scope.selectedSales) {
            data.salesPersonIds = $scope.selectedSales;
        }

        if ($scope.selectedJobsStatuses) {
            data.jobStatusIds = $scope.selectedJobsStatuses;
        }

        if ($scope.selectedAccountStatuses) {
            data.AccountStatusIds = $scope.selectedAccountStatuses;
        }

        if ($scope.selectedSources) {
            data.sourceIds = $scope.selectedSources;
        }

        if ($scope.invoiceStatusesSearch) {
            data.invoiceStatuses = $scope.invoiceStatusesSearch;
        }

        if ($scope.ForSearchstartDate)
            data.createdOnUtcStart = $scope.ForSearchstartDate;
        if ($scope.ForSearchendDate)
            data.createdOnUtcEnd = $scope.ForSearchendDate;

        data.searchTerm = $('#search-box').val();

        data.searchFilter = $('#search-type').find('.active').find('input').val();

        if ($scope.SearchTerm) {
            data.searchTerm = $scope.SearchTerm;
        }

        data.pageIndex = $scope.AccountSearchService.Pagination.page;
        data.pageSize = $scope.AccountSearchService.Pagination.size;

        data.ForSearchstartDate = $scope.ForSearchstartDate;
        data.ForSearchendDate = $scope.ForSearchendDate;

        data.selectedDateText = $scope.selectedDateText;

        if ($routeParams.commercialType) {
            data.commercialType = $routeParams.commercialType;
        }

        if ($routeParams.isReportSearch) {
            data.isReportSearch = $routeParams.isReportSearch;
        }
        //kendotooltipService.setColumnWidth("account");
        //$scope.AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth($scope.AccountSearchService.Accounts_options);


        if (!kendotooltipService.optimiseTriggerFlag) {
            AccountSearchService.GetRecord();
            kendotooltipService.optimiseTriggerFlag = true;
            AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth(AccountSearchService.Accounts_options);
            //console.log(AccountSearchService.Accounts_options);
        }
    }

    //window.onresize = function () {
    //    $scope.AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth($scope.AccountSearchService.Accounts_options);
    //};
    //$(window).resize(function () {
    //    var grid = $("#gridAccountSearch").data("kendoGrid");
    //    AccountSearchService.GetRecord();
    //    if (!kendotooltipService.optimiseTriggerFlag) {
    //        kendotooltipService.optimiseTriggerFlag = true;
    //        AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth(AccountSearchService.Accounts_options, true);
    //        grid.refresh();
    //    }
    //});



    //$(window).resize(function () {
    //    var grid = $("#gridAccountSearch").data("kendoGrid");
    //    AccountSearchService.GetRecord();
    //    if (!kendotooltipService.optimiseTriggerFlag) {
    //        kendotooltipService.optimiseTriggerFlag = true;
    //        AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth(AccountSearchService.Accounts_options, true);
    //        grid.refresh();
    //    }
    //});

    $scope.$on('$viewContentLoaded', function () {
        $location.replace(); //clear last history route
    });

    $scope.ConvertArrayToString = function (arr) {
        if (!arr) {
            return '';
        }
        if ($.isNumeric(arr)) {
            return arr;
        }
        return arr.join('-');
    }

    $scope.ConvertStringToArray = function (str) {
        var arr = [];
        if (str) {
            var st = str.split('-');
            for (var i = 0; i < st.length; i++) {
                arr.push(st[i]);
            }
        }
        return arr;
    }
    $scope.DeleteAccount = function (Account) {
        if (!confirm("Do you want to continue?")) return;
        HFC.AjaxDelete('/api/Accounts/' + Account.AccountId, null, function (result) {
            HFC.DisplaySuccess("Account successfully deleted");
            $scope.ApplyFilter();
        });
    }

    $scope.UpdateAccountNoteType = function (Account, note, typeenum) {
        if (typeenum != window.NoteTypeEnum.History) {
            HFC.AjaxPut("/api/Accounts/" + Account.AccountId + "/notes", { NoteId: note.NoteId, TypeEnum: typeenum }, function () {
                $scope.$apply(function () {
                    note.TypeEnum = typeenum;
                });
                HFC.DisplaySuccess("Note type successfully updated");
            });
        }
    }

    $scope.AddNotes = function (Account, form) {
        var type = Account.NoteType;
        var msg = $.trim(Account.NoteMessage);
        if (msg && msg.length) {
            HFC.AjaxPost("/api/Accounts/" + Account.AccountId + "/notes", {
                TypeEnum: type,
                Message: HFC.htmlEncode(msg)
            }, function (result) {
                if (!Account.AccountNotes) {
                    Account.AccountNotes = [];
                }
                $scope.$apply(function () {
                    Account.AccountNotes.splice(0, 0, { NoteId: result.NoteId || 0, AvatarSrc: HFC.CurrentUser.AvatarSrc, TypeEnum: type, Message: HFC.htmlEncode(msg), CreatedByPersonId: HFC.CurrentUser.PersonId, CreatorFullName: HFC.CurrentUser.FullName, CreatedOn: new XDate() });
                    Account.NoteMessage = "";
                });
            });
        }
    }

    $scope.AddJobNotes = function (job, form) {
        var type = parseInt(job.NoteType);
        var msg = $.trim(job.NoteMessage);
        if (msg && msg.length) {
            HFC.AjaxPost("/api/jobs/" + job.JobId + "/notes", {
                TypeEnum: type,
                Message: HFC.htmlEncode(msg)
            }, function (result) {
                if (!job.JobNotes) {
                    job.JobNotes = [];
                }

                $scope.$apply(function () {
                    job.JobNotes.splice(0, 0, { NoteId: result.NoteId || 0, AvatarSrc: HFC.CurrentUser.AvatarSrc, TypeEnum: type, Message: HFC.htmlEncode(msg), CreatedByPersonId: HFC.CurrentUser.PersonId, CreatorFullName: HFC.CurrentUser.FullName, CreatedOn: new XDate() });
                    job.NoteMessage = "";
                });
            });
        }
    }

    $scope.UpdateJobNoteType = function (job, note, typeenum) {
        if (typeenum != window.NoteTypeEnum.History) {
            HFC.AjaxPut("/api/jobs/" + job.JobId + "/notes", { NoteId: note.NoteId, TypeEnum: typeenum }, function () {
                $scope.$apply(function () {
                    note.TypeEnum = typeenum;
                });
                HFC.DisplaySuccess("Note type successfully updated");
            });
        }
    }
    // Kendo Resizing
   
        //$('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 235);
        //window.onresize = function () {
        //    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 235);
        //};
        //ipad kendo pagenation
        function myFunction(x) {
            if (x.matches) { // If media query matches
                $scope.$on("kendoWidgetCreated", function (e) {
                   
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 252);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 252);
                    };
                });
            } else {
                $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 235);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 235);
                    };
                });
            }
        }

    var x = window.matchMedia("(max-width: 991px)");
    myFunction(x);
    x.addListener(myFunction);
        //end ipad kendo pagenation
    // End Kendo Resizing

    $scope.GetAccountFullName = function (Account) {
        var fullname = "";
        if (Account.PrimPerson) {
            fullname = $.trim(Account.PrimPerson.FirstName); //trim will convert null to empty string, great for our concatenation
            if (Account.SecPerson && Account.SecPerson.PersonId) {
                if (Account.SecPerson.LastName && Account.PrimPerson.LastName && Account.SecPerson.LastName.toLowerCase() == Account.PrimPerson.LastName.toLowerCase())
                    fullname += " & " + $.trim(Account.SecPerson.FirstName) + " " + $.trim(Account.PrimPerson.LastName);
                else
                    fullname += " " + $.trim(Account.PrimPerson.LastName) + " & " + $.trim(Account.SecPerson.FirstName) + " " + $.trim(Account.SecPerson.LastName);
            } else
                fullname += " " + $.trim(Account.PrimPerson.LastName);

            if (Account.PrimPerson.CompanyName)
                fullname = "<b>" + Account.PrimPerson.CompanyName + "</b>" + "<br/>" + fullname;
            else
                fullname = "<b>" + fullname + "</b>";
        }
        return fullname;
    }

    $scope.GetJobStatuses = function () {
        return LK_JobStatuses;
    };

    $scope.countWatchers = function () {
        var q = [$rootScope], watchers = 0, scope;
        while (q.length > 0) {
            scope = q.pop();
            if (scope.$$watchers) {
                watchers += scope.$$watchers.length;
            }
            if (scope.$$childHead) {
                q.push(scope.$$childHead);
            }
            if (scope.$$nextSibling) {
                q.push(scope.$$nextSibling);
            }
        }
        window.console.log(watchers);
    };

    $scope.GetAccountStatuses = function () {
        return LK_AccountStatuses;
    };

    $scope.GetSelectedSalesAgents = function () {
        return sales;
    }

    $scope.GetJobStatusNameById = function (jobStatus) {
        var status = HFC.Lookup.Get(LK_JobStatuses, jobStatus.Id), parent = null;
        if (status.ParentId) {
            parent = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
        }
        if (parent)
            return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
        else
            return status.Name;
    }

    $scope.GetAccountStatusColor = function (Account) {
        var status = HFC.Lookup.Get(LK_AccountStatuses, Account.AccountStatusId);
        if (status.ParentId)
            status = HFC.Lookup.Get(LK_AccountStatuses, status.ParentId);
        return status.ClassName || "btn-default";
    };

    $scope.GetJobStatusColor = function (job) {
        var status = HFC.Lookup.Get(LK_JobStatuses, job.JobStatusId);
        if (!status.ClassName && status.ParentId)
            status = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
        return (status.ClassName || "btn-default");
    };

    $scope.GetJobStatusName = function (job) {
        var status = HFC.Lookup.Get(LK_JobStatuses, job.JobStatusId),
            parent = null;
        if (status.ParentId)
            parent = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
        if (parent)
            return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
        else
            return status.Name;
    };

    $scope.GetSelectedSalesAgent = function (job) {
        if (job.SalesPersonId) {
            if (job.SalesPersonId && HFC.EmployeesJS)
            var user = $.grep(HFC.EmployeesJS, function (e) {
                return e.PersonId == job.SalesPersonId;
            });

            if (user && user.length)
                return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
            else {

                if (HFC.DisabledEmployeesJS && job.SalesPersonId) {
                    var user = $.grep(HFC.DisabledEmployeesJS, function (e) {
                        return e.PersonId == job.SalesPersonId;
                    });
                }
             
                if (user && user.length)
                    return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
                else
                    return "Unassigned";
            }

        } else
            return "Unassigned";
    };

    $scope.GetPrimQuote = function (job) {
        return AccountSearchService.GetPrimeQuote(job.JobId);
    };

    $scope.FormatPercent = function (num, maxPrecision) {
        return HFC.formatPercent(num, maxPrecision);
    }

    $scope.FormatCurrency = function (num, precision) {
        return HFC.formatCurrency(num, precision);
    };

    $scope.ToFriendlyString = function (date, includeTime, ignoreTZ) {
        var str = "";
        var xdate = false;
        var self = this;


        if (ignoreTZ) {
            xdate = new XDate(date, true);
        } else {

            xdate = new XDate(date);
        }

        if (xdate && xdate.valid()) {
            var today = new XDate();

            if (xdate.getFullYear() == today.getFullYear()) {
                if (xdate.getMonth() == today.getMonth() && xdate.getDate() == today.getDate())
                    str = "Today";
                else if (xdate.getMonth() == today.getMonth() && (today.getDate() - xdate.getDate()) == 1)
                    str = "Yesterday";
                else
                    str = xdate.toString("ddd, MMM d");
            } else
                str = xdate.toString("ddd, MMM d yyyy");

            if (includeTime) {
                str += " " + xdate.toString("h:mm tt");
            }
        }

        return str;
    }

    $scope.GetAccountStatusName = function (Account) {
        var status = HFC.Lookup.Get(LK_AccountStatuses, Account.AccountStatusId), parent = null;
        if (status.ParentId)
            parent = HFC.Lookup.Get(LK_AccountStatuses, status.ParentId);
        if (parent)
            return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
        else
            return status.Name;
    }

    $scope.GetAccountStatusNameById = function (AccountStatus) {
        var status = HFC.Lookup.Get(LK_AccountStatuses, AccountStatus.Id), parent = null;
        if (status.ParentId) {
            parent = HFC.Lookup.Get(LK_AccountStatuses, status.ParentId);
        }
        if (parent)
            return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
        else
            return status.Name;
    }

    //function addressToString(isSingleLine, model) {
    //    if (model) {

    //        var str = "";
    //        if (model.Address1)
    //            str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

    //        if (model.City)
    //            str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
    //        else
    //            str += (model.ZipCode || "");

    //        return str;
    //    }
    //};

    //$scope.GetGoogAccountdressHrefAccounts = function (dest) {
    //    //return "maps.google.com?saddr= '1927 n glassell st orange ca'&daddr='21273 beechwood way lake forest ca'";
    //    var srcAddr = '1927 n glassell st orange ca';
    //    var dest = '21273 beechwood way lake forest ca';
    //    return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);

    //}

    //$scope.GetGoogAccountdressHref = function (model) {
    //    var dest = addressToString(true, model);
    //    if (model.Address1) {

    //        var srcAddr;

    //        if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
    //            srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
    //        } else {
    //            srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
    //        }

    //        if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
    //            return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
    //        } else {
    //            return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
    //        }
    //    } else
    //        return null;
    //}

    //$scope.AddressToString = function (model) { return addressToString(true, model) };

    $scope.GetAccountJobs = function (AccountId) {
        if (!AccountId) {
            return AccountSearchService.Jobs;
        }
        var reults = [];
        for (var i = 0; i < AccountSearchService.Jobs.length; i++) {
            if (AccountSearchService.Jobs[i].AccountId == AccountId) {
                reults.push(AccountSearchService.Jobs[i]);
            }
        }

        return reults;
    }

    // No more required - murugan march 12, 2019
    //$scope.ToggleJobs = function (model, jsEvent) {
    //    var reults = [];
    //    if (model.JobRowEnabled) {
    //        model.JobRowEnabled = false;
    //        $(jsEvent.currentTarget).find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
    //        $('[name="' + model.AccountId + '"]').remove();
    //        return reults;
    //    }
    //    for (var i = AccountSearchService.Jobs.length - 1; i >= 0; i--) {
    //        if (AccountSearchService.Jobs[i].AccountId == model.AccountId) {
    //            AccountSearchService.Jobs.splice(i, 1);
    //        }
    //    }
    //    if (reults.length != 0) {
    //        model.JobRowEnabled = !model.JobRowEnabled;
    //        $(jsEvent.currentTarget).find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
    //        return reults;
    //    }
    //    AccountSearchService.LoadJobs(model.AccountId).then(function () {
    //        model.JobRowEnabled = !model.JobRowEnabled;
    //        $(jsEvent.currentTarget).find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
    //        var template = $templateCache.get('AccountSearchSecondLevel.html');
    //        template = template.replace(/#AccountId#/g, model.AccountId);
    //        angular.element(document.getElementById('row_' + model.AccountId)).after($compile(template)($scope));
    //    });
    //}
    //$scope.JobStatusOnChanged = function (job, val) {
    //    if (job.JobStatusId != val) {
    //        HFC.AjaxPut('/api/jobs/' + job.JobId + '/status', { Id: val }, function () {
    //            $scope.$apply(function () {
    //                job.JobStatusId = val;
    //            });
    //            HFC.DisplaySuccess("Status updated for Job #" + job.JobNumber);
    //        });
    //    }
    //};
    //$scope.ChangeSalesAgent = function (job, val) {
    //    if (job.SalesPersonId != val) {
    //        HFC.AjaxPut('/api/jobs/' + job.JobId + '/salesagent',
    //            {
    //                PersonId: val
    //            },
    //            function () {
    //                $scope.$apply(function () {
    //                    job.SalesPersonId = val;
    //                });
    //                HFC.DisplaySuccess("Sales agent successfully updated");
    //            });
    //    }
    //};
    //$scope.AccountStatusOnChanged = function (Account, val) {
    //    HFC.AjaxBasic("PUT", HFC.Util.BaseURL() + '/api/Accounts/' + Account.AccountId + '/Status', JSON.stringify({ "Id": val }), function () {
    //        $scope.$apply(function () {
    //            Account.AccountStatusId = val;
    //        });
            
    //        Account.isShowAccountStatuses = false;
    //        HFC.DisplaySuccess("Status updated for Account #" + Account.AccountNumber);
    //    });
    //};
    //$scope.GetKeyByValue = function (list, key) {
    //    return HFC.Lookup.GetKeyByValue(list, key);
    //}
    //$scope.GetNoteTypeEnum = function () {
    //    return window.NoteTypeEnum;
    //}
    //$scope.DateRangeArray = ["All Date & Time", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year", "Last Year"];
    //$scope.selectedDateText = "All Date & Time";
    //$scope.$watch('$scope.ForSearchReport', function (newVal) {
    //    if (newVal) {
    //        $scope.selectedDateText = "All Date & Time";
    //        $scope.ForSearchstartDate = null;
    //        $scope.ForSearchendDate = null;
    //        $scope.startDate = null;
    //        $scope.endDate = null;
    //    }
    //});
    //$scope.$watch('startDate', function (newVal) {
    //    if (newVal != null) {
    //        $scope.ForSearchstartDate = newVal;
    //        if ($scope.endDate == null) {
    //            $scope.selectedDateText = newVal + ' to ';
    //        } else {
    //            $scope.selectedDateText = newVal + ' to ' + $scope.endDate;
    //        }
    //    }
    //});
    //$scope.$watch('endDate', function (newVal) {
    //    if (newVal != null) {
    //        $scope.ForSearchendDate = newVal;
    //        if ($scope.startDate == null) {
    //            $scope.selectedDateText = ' to ' + newVal;
    //        } else {
    //            $scope.selectedDateText = $scope.startDate + ' to ' + newVal;
    //        }
    //    }
    //});
    //$scope.endDate = null;
    //$scope.startDate = null;
    //$scope.ForSearchstartDate = null;
    //$scope.ForSearchendDate = null;
    //$scope.setDateRange = function (date) {
    //    var range = $scope.getDateRange(date);
    //    if (range.StartDate) {
    //        $scope.ForSearchstartDate = range.StartDate.toString('MM/dd/yyyy');
    //        //$scope.startDate = range.StartDate.toString('MM/dd/yyyy');
    //    } else {
    //        //$scope.startDate = null;
    //        $scope.ForSearchstartDate = null;
    //    }

    //    if (range.EndDate) {
    //        $scope.ForSearchendDate = range.EndDate.toString('MM/dd/yyyy');
    //    } else {
    //        $scope.ForSearchendDate = null;
    //    }

    //    $scope.selectedDateText = date;
    //}
    //$scope.getDateRange = function (range) {
    //    var endDate = new XDate(),
    //        startDate = new XDate();
    //    switch (range.toLowerCase()) {
    //        case "all date & time":
    //            startDate = null;
    //            endDate = null;
    //            break;
    //        case "this week":
    //            startDate.addDays(-startDate.getDay());
    //            endDate = startDate.clone().addDays(6);
    //            break;
    //        case "this month":
    //            startDate.setDate(1);
    //            endDate = startDate.clone().addMonths(1).addDays(-1);
    //            break;
    //        case "this year":
    //            startDate.setDate(1).setMonth(0);
    //            endDate = startDate.clone().addYears(1).addDays(-1);
    //            break;
    //        case "last week":
    //            startDate.addDays(-startDate.getDay() - 7);
    //            endDate = startDate.clone().addDays(6);
    //            break;
    //        case "last month":
    //            startDate.setDate(1).addMonths(-1);
    //            endDate = startDate.clone().addMonths(1).addDays(-1);
    //            break;
    //        case "last year":
    //            startDate.setDate(1).setMonth(0).addYears(-1);
    //            endDate.setDate(1).setMonth(0).addDays(-1);
    //            break;
    //        case "last 7 days":
    //            startDate.addDays(-7);
    //            break;
    //        case "last 30 days":
    //            startDate.addDays(-30);
    //            break;
    //        case "1st quarter":
    //            startDate.setDate(1).setMonth(0);
    //            endDate = startDate.clone().addMonths(3).addDays(-1);
    //            break;
    //        case "2nd quarter":
    //            startDate.setDate(1).setMonth(3);
    //            endDate = startDate.clone().addMonths(3).addDays(-1);
    //            break;
    //        case "3rd quarter":
    //            startDate.setDate(1).setMonth(6);
    //            endDate = startDate.clone().addMonths(3).addDays(-1);
    //            break;
    //        case "4th quarter":
    //            startDate.setDate(1).setMonth(9);
    //            endDate = startDate.clone().addMonths(3).addDays(-1);
    //            break;
    //        case "1st half":
    //            startDate.setDate(1).setMonth(0);
    //            endDate = startDate.clone().addMonths(6).addDays(-1);
    //            break;
    //        case "2nd half":
    //            startDate.setDate(1).setMonth(6);
    //            endDate = startDate.clone().addMonths(6).addDays(-1);
    //            break;
    //        default:
    //            startDate = null;
    //            endDate = null;
    //            break;
    //    }

    //    return { StartDate: startDate, EndDate: endDate };
    //}
    //$scope.GetURLParameter = function (sParam) {
    //    var sPageURL = window.location.search.substring(1);
    //    var data = $.parseParams(window.location.href);
    //    if (data) {
    //        return data[sParam];
    //    }
    //};
    //if ($routeParams.salesPersonIds) {
    //    if ($.isNumeric($routeParams.salesPersonIds)) {
    //        $scope.selectedSales = [];
    //        $scope.selectedSales.push($routeParams.salesPersonIds);
    //    } else {
    //        $scope.selectedSales = $scope.ConvertStringToArray($routeParams.salesPersonIds);
    //    }
    //}
    //if ($routeParams.jobStatusIds) {
    //    if ($.isNumeric($routeParams.jobStatusIds)) {
    //        $scope.selectedJobsStatuses = [];
    //        $scope.selectedJobsStatuses.push(+$routeParams.jobStatusIds);
    //    } else {
    //        $scope.selectedJobsStatuses = $scope.ConvertStringToArray($routeParams.jobStatusIds);

    //    }
    //}
    //if ($routeParams.AccountStatusIds) {
    //    if ($.isNumeric($routeParams.AccountStatusIds)) {

    //        $scope.selectedAccountStatuses = [];
    //        $scope.selectedAccountStatuses.push(+$routeParams.AccountStatusIds);
    //    } else {
    //        $scope.selectedAccountStatuses = $scope.ConvertStringToArray($routeParams.AccountStatusIds);
    //    }
    //}
    //if ($routeParams.sourceIds) {
    //    if ($.isNumeric($routeParams.sourceIds)) {

    //        $scope.selectedSources = [];
    //        $scope.selectedSources.push(+$routeParams.sourceIds);
    //    } else {
    //        $scope.selectedSources = $scope.ConvertStringToArray($routeParams.sourceIds);
    //    }
    //}
    //if ($routeParams.invoiceStatuses) {
    //    if ($.isNumeric($routeParams.invoiceStatuses)) {
    //        $scope.invoiceStatusesSearch = [];
    //        $scope.invoiceStatusesSearch.push($routeParams.invoiceStatuses);
    //    } else {
    //        $scope.invoiceStatusesSearch = $scope.ConvertStringToArray($routeParams.invoiceStatuses);
    //    }
    //}
    //if ($routeParams.selectedDateText) {
    //    $scope.selectedDateText = $routeParams.selectedDateText;
    //    $scope.ForSearchstartDate = $routeParams.createdOnUtcStart;
    //    $scope.ForSearchendDate = $routeParams.createdOnUtcEnd;
    //} else {
    //    if ($routeParams.createdOnUtcStart) {
    //        $scope.ForSearchstartDate = $routeParams.createdOnUtcStart;
    //        $scope.startDate = $routeParams.createdOnUtcStart;
    //    }
    //    if ($routeParams.createdOnUtcEnd) {
    //        $scope.ForSearchendDate = $routeParams.createdOnUtcEnd;
    //        $scope.endDate = $routeParams.createdOnUtcEnd;
    //    }
    //}
    //if ($routeParams.searchTerm) {
    //    $scope.SearchTerm = $routeParams.searchTerm;
    //    if ($routeParams.searchFilter == "Phone_Number") {
    //        var phoneno = /^(?=.*?[1-9])[0-9()-. ]+$/;
    //        var inputtxt = $routeParams.searchTerm;
    //        if (!inputtxt.match(phoneno)) {
    //            HFC.DisplayAlert("Please verify the phone number you entered.");
    //            //clear the parameters to show empty result on error
    //            var data = {};
    //            AccountSearchService.Get(data);
    //            return false;
    //        }
    //    }
    //}
    //if ($routeParams.searchFilter != 'undefined') {
    //    $rootScope.searchMode = $routeParams.searchFilter;
    //} else {
    //    $routeParams.searchFilter = 'Auto_Detect';
    //    $rootScope.searchMode = $routeParams.searchFilter;
    //}
    //saveCriteriaService.selectedJobsStatuses = $scope.selectedJobsStatuses;
    //saveCriteriaService.selectedAccountStatuses = $scope.selectedAccountStatuses;
    //saveCriteriaService.selectedSources = $scope.selectedSources;
    //saveCriteriaService.selectedSales = $scope.selectedSales;
    //saveCriteriaService.invoiceStatusesSearch = $scope.invoiceStatusesSearch;
    //saveCriteriaService.selectedDateText = $scope.selectedDateText;
    //saveCriteriaService.ForSearchstartDate = $scope.ForSearchstartDate;
    //saveCriteriaService.ForSearchendDate = $scope.ForSearchendDate;
    //saveCriteriaService.startDate = $scope.startDate;
    //saveCriteriaService.endDate = $scope.endDate;


    //AccountSearchService.Get();

    $scope.customersDataSource = {
        transport: {
            read: {
                dataType: "jsonp",
                url: "//demos.telerik.com/kendo-ui/service/Customers",
            }
        }
    };
    $scope.customOptions = {
        dataSource: $scope.customersDataSource,
        dataTextField: "ContactName",
        dataValueField: "CustomerID",

        headerTemplate: '<div class="dropdown-header k-widget k-header">' +
            '<span>Photo</span>' +
            '<span>Contact info</span>' +
            '</div>',

        // using {{angular}} templates:
        valueTemplate: '<span class="selected-value" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span><span>{{dataItem.ContactName}}</span>',

        template: '<span class="k-state-default" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span>' +
                  '<span class="k-state-default"><h3>{{dataItem.ContactName}}</h3><p>{{dataItem.CompanyName}}</p></span>',
    };


    // do width manipulation for devices that have more than 1280px width
    //var setColumnWidth = function () {
    //    var screenWidth = window.innerWidth;
    //    var grid = $scope.AccountSearchService.Accounts_options;
    //    var columnsList = grid.columns;
    //    if (screenWidth >= 1280) {
    //        var columnWidth = ((screenWidth - 160) / 13);
    //        for (var i = 0; i < columnsList.length; i++) {
    //            columnsList[i].width = columnWidth + "px";
    //        }
    //        columnsList[columnsList.length - 1].width = "80px";
    //        restrictTooltip(columnWidth);
    //    } else {
    //        restrictTooltip(columnWidth, true);
    //    }
    //}

    // bind tooltip restriction functionality
    //var restrictTooltip = function (columnWidth, flag) {
    //    var loadingElement = $("#loading");
    //    if (loadingElement) {
    //        loadingElement[0].style.display = "block";
    //    }
    //    setTimeout(function () {
    //        var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
    //        $(tooltipContainers).css("visibility", "hidden");
    //        var dataWrapperList = $(".k-grid-content tr td .tooltip-wrapper");
    //        for (var i = 0; i < dataWrapperList.length; i++) {
    //            if (flag) {
    //                columnWidth = $(dataWrapperList[i]).parent().width();
    //            }
    //            var dataWidth = $(dataWrapperList[i]).width();
    //            var children = $(dataWrapperList[i]).children()[1];
    //            if (children) {
    //                var data = $(children).text();
    //                if (dataWidth > (columnWidth - 20) && dataWidth != 0) {
    //                    var dataLength = data.length;
    //                    var textWidth = dataWidth / dataLength;
    //                    var limitCount = (columnWidth - 20) / textWidth;
    //                    var updatedText = "<div class='tooltip-content'>" + data + "</div>" + data.substring(0, (limitCount - 3)) + "...";
    //                    $(dataWrapperList[i]).html(updatedText);
    //                } else {
    //                    $(dataWrapperList[i]).text(data);
    //                }
    //            }
    //        }

    //        $(".k-grid-content tr td .tooltip-wrapper").mouseenter(function (e) {
    //            var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
    //            $(tooltipContainers).css("visibility", "hidden");
    //            var tooltip = $(e.target).children();
    //            $(tooltip).css("visibility", "visible");
    //        });
    //        $(".k-grid-content tr td .tooltip-wrapper").mouseleave(function (e) {
    //            var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
    //            $(tooltipContainers).css("visibility", "hidden");
    //        });
    //        if (loadingElement) {
    //            loadingElement[0].style.display = "none";
    //        }
    //    }, 3000);
    //    //var tooltipContainers = $("#gridLeadSearch .k-grid-content tr td .tooltip-wrapper .tooltip-content");
    //    //$(tooltipContainers).css("display", "none");
    //}











    angular.element(document).ready(function () {
         
        $('#search-box').val($routeParams.searchTerm);

        if ($routeParams.searchFilter) {
            var lis = $('#search-type').find('li');
            lis.removeClass('active');
            for (var i = 0; i < lis.length; i++) {
                if ($(lis[i]).find('input').val() == $routeParams.searchFilter) {
                    $(lis[i]).addClass('active');
                }
            }

            if ($routeParams.searchFilter != 'undefined') {
                switch ($routeParams.searchFilter) {
                    case 'Customer_Name':
                        $rootScope.searchMode = 'Customer Name';
                        break;
                    case 'Number':
                        $rootScope.searchMode = 'Job/Invoice Number';
                        break;
                    case 'Phone_Number':
                        $rootScope.searchMode = 'Phone Number';
                        break;
                    default:
                        $rootScope.searchMode = $routeParams.searchFilter;
                }
            }
            else {
                $rootScope.searchMode = 'Auto_Detect';
                $scope.searchFilter = 'Auto_Detect';
                $routeParams.searchFilter = 'Auto_Detect';
            }

        }
        $scope.ApplyFilter();
    });

    // download excel
    $scope.DownloadAccount_Excel = function () {
        var grid = $("#gridAccountSearch").data("kendoGrid");
        grid.saveAsExcel();
    }

   

    $rootScope.$broadcast('applyMainSearchFilter');

    //$('head title').text("" + applicationTitle);
    HFCService.setHeaderTitle("Account #");
}
    ])
