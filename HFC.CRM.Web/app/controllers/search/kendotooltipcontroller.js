﻿// TODO: rquired refactoring:

(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.kendotooltipModule', [])
.service('kendotooltipService', ['$route', function ($route) {

    var srv = this;
    srv.optimiseTriggerFlag = false;
    srv.changedColumnList = [];

    // do width manipulation for devices that have more than 1280px width
    srv.setColumnWidth = function (grid, flag) {
        var screenWidth = window.innerWidth;

        if (grid) {
            var columnsList = grid.columns;
            var columnCount = columnsList.length;
            if (screenWidth >= 1280) {
                var columnWidth = ((screenWidth - 160) / columnCount);
                if (columnsList) {
                    for (var i = 0; i < columnsList.length; i++) {
                        columnsList[i].width = columnWidth + "px";
                    }
                    columnsList[columnsList.length - 1].width = "80px";
                    srv.columnWidth = columnWidth;
                    if (flag) {
                        srv.restrictTooltip(srv.columnWidth);
                    }
                }
            } else {
                $(".k-grid-content tr td").css({ "overflow": "hidden !important", "white-space": "normal" });
            }
            grid.columns = columnsList;
            return grid;
        }
    }

    // bind tooltip restriction functionality
    srv.restrictTooltip = function (columnWidth, flag) {
        var screenWidth = window.innerWidth;
        if (screenWidth >= 1280) {
            var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
            $(tooltipContainers).css("visibility", "hidden");
            var dataWrapperList = $(".k-grid-content tr td .tooltip-wrapper");
            if (dataWrapperList && dataWrapperList.length != 0) {
                for (var i = 0; i < dataWrapperList.length; i++) {
                    var parentTD = $(dataWrapperList[i]).parent()[0];
                    var parentOffset = $(parentTD)[0].cellIndex;
                    var existFlag = false;
                    for (var j = 0; j < srv.changedColumnList.length; j++) {
                        if (srv.changedColumnList[j] == parentOffset) {
                            existFlag = true;
                        }
                    }
                    if (existFlag) {
                        columnWidth = $(dataWrapperList[i]).parent().width();
                    }
                    if (flag) {
                        columnWidth = $(dataWrapperList[i]).parent().width();
                    }
                    var dataWidth = $(dataWrapperList[i]).width();
                    var children = $(dataWrapperList[i]).children()[1];
                    if (children) {
                        var data = $(children).text();
                        var ancherList = $(dataWrapperList[i]).find("a");
                        if (dataWidth > (columnWidth - 20) && dataWidth != 0) {
                            var dataLength = data.length;
                            var textWidth = dataWidth / dataLength;
                            var limitCount = (columnWidth - 20) / textWidth;
                            var updatedText;
                            if (ancherList.length != 0) {
                                $(ancherList[0]).text(data.substring(0, (limitCount - 3)) + "...");
                                var additionalChild = $(ancherList[0])[0].outerHTML;
                                updatedText = "<div class='tooltip-content'>" + data + "</div>" + additionalChild;
                            } else {
                                updatedText = "<div class='tooltip-content'>" + data + "</div>" + data.substring(0, (limitCount - 3)) + "...";
                            }
                            $(dataWrapperList[i]).html(updatedText);
                        } else {
                            if (ancherList.length != 0) {
                                var additionalChild = $(ancherList[0])[0].outerHTML;
                                $(dataWrapperList[i]).html(additionalChild);
                            } else {
                                $(dataWrapperList[i]).text(data);
                            }
                        }
                    }
                    columnWidth = srv.columnWidth;
                }

                $(".k-grid-content tr td .tooltip-wrapper").mouseenter(function (e) {
                    var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
                    $(tooltipContainers).css("visibility", "hidden");
                    var tooltip = $(e.target).children();
                    if (tooltip.length == 0) {
                        var parent = $(e.target).parent();
                        tooltip = $(parent).children()[0];
                    }
                    var screenMaxHeight = window.innerHeight - 80;
                    var screenMinHeight = screenMaxHeight - 70;
                    var flag = $(tooltip).hasClass("tooltip-wrapper");
                    var tooltipTextLength = $(tooltip).text().length;
                    var tooltipWidth = tooltipTextLength * 7.5;
                    var calculatedWidth = e.clientX + tooltipWidth;
                    var remainingWidth = (window.innerWidth - e.clientX) - 20;
                    if (window.innerWidth < calculatedWidth) {
                        var updatedWidthValue = remainingWidth + "px";
                        $(tooltip).css("width", updatedWidthValue);
                        $(tooltip).css("white-space", "pre-wrap");
                    } else {
                        $(tooltip).css("white-space", "nowrap");
                    }
                    var tooltipHeight = $(tooltip).height();
                    if (e.clientY > screenMinHeight && e.clientY < screenMaxHeight && !flag) {
                        $(tooltip).css("bottom", "16px");
                    } else if (!flag) {
                        var heightVal = tooltipHeight + 18;
                        var posValue = "-" + heightVal + "px";
                        $(tooltip).css("bottom", posValue);
                    }
                    $(tooltip).css("visibility", "visible");
                });
                $(".k-grid-content tr td .tooltip-wrapper").mouseleave(function (e) {
                    var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
                    $(tooltipContainers).css("visibility", "hidden");
                });
            }
            // validate the scroll is present or not -- only for ipad
            var compatability = false;
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                compatability = true;
            }
            if (compatability) {
                var contentWrapperHeight = $(".k-grid-content").height();
                var contentHeight = $(".k-grid-content table").height();
                if (contentHeight > contentWrapperHeight) {
                    $(".k-grid .k-grid-header").removeClass("remove-scroll-header");
                } else {
                    $(".k-grid .k-grid-header").addClass("remove-scroll-header");
                }
            }
        } else {
            $(".k-grid-content tr td").css({ "white-space": "normal", "overflow": "hidden !important" });
        }
        srv.optimiseTriggerFlag = false;
        srv.bindresizeEvent();
    }
    // manipulate the behaviour on column resize event

    srv.columnResizeTooltip = function (columnWidth, columnId) {
        var screenWidth = window.innerWidth;
        if (screenWidth >= 1280) {
            var resizedColumnOffset = $("#" + columnId)[0].cellIndex;
            if (resizedColumnOffset >= 0) {
                var existFlag = false;
                for (var j = 0; j < srv.changedColumnList.length; j++) {
                    if (srv.changedColumnList[j] == resizedColumnOffset) {
                        existFlag = true;
                    }
                }
                if (!existFlag) {
                    srv.changedColumnList.push(resizedColumnOffset);
                }
                resizedColumnOffset = resizedColumnOffset + 1;
                var selectedColumn = $(".k-grid-content tr td:nth-child(" + resizedColumnOffset + ")");
                for (var i = 0; i < selectedColumn.length; i++) {
                    var columnText;
                    var columnDatWidth;
                    var tooltipFlag;
                    var ancherList = $(selectedColumn[i]).find(".tooltip-wrapper a");
                    var columnTextElement = $(selectedColumn[i]).find(".tooltip-wrapper .tooltip-content");
                    if (columnTextElement && columnTextElement.length != 0) {
                        columnText = $(columnTextElement).text();
                        var columnTextLength = columnText.length;                        
                        tooltipFlag = true;
                        // columnDatWidth = $(columnTextElement).width();
                        columnDatWidth = columnTextLength * 7;
                    } else {
                        columnTextElement = $(selectedColumn[i]).find(".tooltip-wrapper");
                        columnText = $(columnTextElement).text();
                        var columnTextLength = columnText.length;
                        tooltipFlag = false;
                        // columnDatWidth = $(columnTextElement).width();
                        columnDatWidth = columnTextLength * 7;
                    }
                    if ((columnDatWidth) > (columnWidth - 20)) {
                        var dataLength = columnText.length;
                        var textWidth = (columnDatWidth) / dataLength;
                        var limitCount = (columnWidth - 20) / textWidth;
                        if (tooltipFlag) {
                            var tooltipHTML = $(columnTextElement)[0].outerHTML;
                            var preserveHTML
                            if (ancherList && ancherList.length != 0) {
                                $(ancherList).text(columnText.substring(0, (limitCount - 3)) + "...");
                                preserveHTML = tooltipHTML + $(ancherList)[0].outerHTML;
                            } else {
                                preserveHTML = tooltipHTML + columnText.substring(0, (limitCount - 3)) + "...";
                            }
                            $($(columnTextElement).parent()[0]).html(preserveHTML);
                        } else {
                            if (ancherList && ancherList.length != 0) {
                                $(ancherList).text(columnText.substring(0, (limitCount - 3)) + "...");
                                preserveHTML = "<div class='tooltip-content'>" + columnText + "</div>" + $(ancherList)[0].outerHTML;
                            } else {
                                preserveHTML = "<div class='tooltip-content'>" + columnText + "</div>" + columnText.substring(0, (limitCount - 3)) + "...";
                            }
                            $(columnTextElement).html(preserveHTML);
                        }
                    } else {
                        if (tooltipFlag) {
                            if (ancherList && ancherList.length != 0) {
                                $(ancherList).text(columnText);
                                preserveHTML = $(ancherList)[0].outerHTML;
                            } else {
                                preserveHTML = columnText;
                            }
                            $($(columnTextElement).parent()[0]).html(preserveHTML);
                        } else {
                            if (ancherList && ancherList.length != 0) {
                                $(ancherList).text(columnText);
                                preserveHTML = $(ancherList)[0].outerHTML;
                            } else {
                                preserveHTML = columnText;
                            }
                            $(columnTextElement).html(preserveHTML);
                        }
                    }
                    ancherList = null;
                }
            }
        }
    }

    var compatability = false;

    // window resize method
    srv.bindresizeEvent = function () {
        $(window).resize(function () {
            if (!compatability) {
                compatability = true;
                setTimeout(function () {
                    var url = window.location.href;
                    if (url.includes("leadsSearch") || url.includes("accountSearch") || url.includes("opportunitySearch") || url.includes("quotesSearch") || url.includes("orderSearch") || url.includes("accountPaymentSearch") || url.includes("quoteSearch") || url.includes("franchiseCase") || url.includes("listDocuments") || url.includes("VendorCaseList") || url.includes("homeofficeCase") ) {  //|| url.includes("measurementDetail")
                        $route.reload();
                        compatability = false;
                    }
                }, 300);
            }
        });
    }

}
])
})(window, document);




