﻿// TODO: rquired refactoring:

'use strict';
app
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('leadSearchController',
    [
        '$scope', 'HFCService', 'LeadSearchService', 'kendotooltipService'
        , '$routeParams', '$location', '$rootScope'
        , 'saveCriteriaService', '$templateCache'
        , '$sce', '$compile', 'LeadService', 'CalendarService'
        , 'NavbarService', 'FileprintService', '$http', 'kendoService',
        function ($scope, HFCService, LeadSearchService, kendotooltipService
            , $routeParams, $location, $rootScope
            , saveCriteriaService, $templateCache
            , $sce, $compile, leadService
            , calendarService, NavbarService
            , FileprintService, $http, kendoService
            ) {

            $rootScope.enableSaveSearch = enableSaveSearch;
            $scope.NavbarService = NavbarService;
            $scope.NavbarService.SelectSales();
            $scope.NavbarService.EnableSalesLeadsTab();
            $scope.FileprintService = FileprintService;
            

            HFCService.setHeaderTitle("Lead #");

            $scope.showLeadStatus = function (lead) {
                if (lead.isShowLeadStatuses) {
                    return;
                }
                lead.isShowLeadStatuses = !lead.isShowLeadStatuses;
            };

            $scope.showNotes = function (lead) {
                lead.isShowNotes = !lead.isShowNotes;
            };

            $scope.LeadSearchService = LeadSearchService;
            $scope.HFCService = HFCService;
            $scope.filter_CounterSelectedSalesAgent = 'Nothing selected';
            $scope.filter_CounterSelectedJobStatus = 'Nothing selected';
            $scope.selectedSales = null;
            $scope.selectedleadStatuses = null;
            $scope.selectedSources = null;
            $scope.SearchFilter = 'Auto_Detect';

            // for kendo tooltip
            $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
            kendoService.customTooltip = null;

            $scope.ChangeFilterSearch = function (val) {
                $scope.SearchFilter = val;
            }

            $scope.Permission = {};
            $scope.Permission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Lead')
            $scope.Permission.ListLead = $scope.Permission.CanRead;//$scope.HFCService.GetPermissionTP('List Lead').CanAccess;


            $scope.getClassLeadSearch = function (val) {

                if (val == $scope.SearchFilter) {
                    return 'active';
                }
                return '';
            }

            // Set Appointment for lead integration
            $scope.Lead = null;

            $scope.calendarModalId = 0;
            $scope.SetAppointment = function (modalid, leadId) {
                $scope.calendarModalId = modalid;
                calendarService.NewLeadApt($scope.calendarModalId, leadId);

            }

            $scope.LeadgridSearchClear = function () {
                $('#BoxValue').val('');
                $("#gridLeadSearch").data('kendoGrid').dataSource.filter({
                });
                
            }
            $scope.Newleadpage = function () {
                window.location.href = '/#!/leads';
            }

            // for grid column alignment fix
            $rootScope.$on("$routeChangeStart", function (event, currentRoute, previousRoute) {
                kendotooltipService.optimiseTriggerFlag = false;
            });

            $scope.LeadgridSearch = function () {
                $scope.LeadSearchService.Leads_options = kendotooltipService.setColumnWidth($scope.LeadSearchService.Leads_options);
                var searchValue = $('#BoxValue').val();
                $("#gridLeadSearch").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "LeadFullName",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Status",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Channel",
                          operator: "contains",
                          value: searchValue,
                          
                      },
                      {
                          field: "source",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Campaign",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Address1",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Address2",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "City",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "State",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "ZipCode",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "DisplayPhone",
                          operator: "contains",
                          value: searchValue
                      },
                        {
                            field: "PrimaryEmail",
                            operator: "contains",
                            value: searchValue
                        }
                    ]
                });

            }

            $scope.filter_SelectSalesAgent = function (id) {
                $scope.filter_CounterSelectedSalesAgent = id;
            }

            $scope.filter_SelectJobStatus = function (id) {
                $scope.filter_CounterSelectedJobStatus = id;
            }
            $scope.SortBy = 'createdonutc';
            $scope.SortIsDesc = true;
            $scope.ChangeSortOrder = function (sortBy) {
                if (sortBy) {
                    if (sortBy != $scope.SortBy) {
                        $scope.SortBy = sortBy;
                        $scope.SortIsDesc = true;
                    } else {
                        $scope.SortIsDesc = !$scope.SortIsDesc;

                    }
                    $scope.ApplyFilter();
                }
            }

            $scope.GoToFirstPage = function () {
                $scope.LeadSearchService.Pagination.page = 1;
                $scope.ApplyFilter();
            }

            $scope.GoToPreviousPage = function () {
                if ($scope.LeadSearchService.Pagination.page > 1) {
                    $scope.LeadSearchService.Pagination.page = +($scope.LeadSearchService.Pagination.page) - 1;
                    $scope.ApplyFilter();
                }
            }

            $scope.GoToNextPage = function () {

                if ($scope.LeadSearchService.Pagination.page == $scope.LeadSearchService.Pagination.pageTotal) {
                    return;
                }
                $scope.LeadSearchService.Pagination.page = +($scope.LeadSearchService.Pagination.page) + 1;
                $scope.ApplyFilter();
            }

            $scope.GoToLastPage = function () {
                $scope.LeadSearchService.Pagination.page = $scope.LeadSearchService.Pagination.pageTotal;
                $scope.ApplyFilter();
            }

            $scope.ChangePageSize = function () {
                $scope.ApplyFilter();
            }

            $scope.ExportExcel = function () {
                var data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };

                if ($scope.selectedSales) {
                    data.salesPersonIds = $scope.selectedSales;
                }

                if ($scope.selectedJobsStatuses) {
                    data.jobStatusIds = $scope.selectedJobsStatuses;
                }

                if ($scope.selectedleadStatuses) {
                    data.leadStatusIds = $scope.selectedleadStatuses;
                }

                if ($scope.selectedSources) {
                    data.sourceIds = $scope.selectedSources;
                }

                if ($scope.invoiceStatusesSearch) {
                    data.invoiceStatuses = $scope.invoiceStatusesSearch;
                }

                if ($scope.ForSearchstartDate)
                    data.createdOnUtcStart = $scope.ForSearchstartDate;
                if ($scope.ForSearchendDate)
                    data.createdOnUtcEnd = $scope.ForSearchendDate;

                data.searchTerm = $('#search-box').val();

                data.searchFilter = $('#search-type').find('.active').find('input').val();

                //if (data.searchFilter != 'Auto_Detect') {
                //    data.searchFilter = $scope.SearchFilter;
                //}

                if ($scope.SearchTerm) {
                    data.searchTerm = $scope.SearchTerm;
                }

                data.pageIndex = $scope.LeadSearchService.Pagination.page;
                data.pageSize = $scope.LeadSearchService.Pagination.size;

                data.ForSearchstartDate = $scope.ForSearchstartDate;
                data.ForSearchendDate = $scope.ForSearchendDate;

                if ($routeParams.commercialType) {
                    data.commercialType = $routeParams.commercialType;
                }

                window.open('/export/leads?' + $.param(data), '_blank');
            }

            $scope.ApplyFilter = function (isClickedFromUI) {


                var data = { orderByDirection: $scope.SortIsDesc ? "Desc" : "Asc", orderBy: $scope.SortBy };
                //if ($scope.selectedSales) {
                //    data.salesPersonIds = $scope.selectedSales;
                //}


                if (isClickedFromUI) {
                    data.pageIndex = 1;
                    $scope.LeadSearchService.Pagination.page = 1;
                }

                if ($scope.selectedSales) {
                    data.salesPersonIds = $scope.selectedSales;
                }

                if ($scope.selectedJobsStatuses) {
                    data.jobStatusIds = $scope.selectedJobsStatuses;
                }

                if ($scope.selectedleadStatuses) {
                    data.leadStatusIds = $scope.selectedleadStatuses;
                }


                if ($scope.selectedSources) {
                    data.sourceIds = $scope.selectedSources;
                }

                if ($scope.invoiceStatusesSearch) {
                    data.invoiceStatuses = $scope.invoiceStatusesSearch;
                }

                if ($scope.ForSearchstartDate)
                    data.createdOnUtcStart = $scope.ForSearchstartDate;
                if ($scope.ForSearchendDate)
                    data.createdOnUtcEnd = $scope.ForSearchendDate;

                data.searchTerm = $('#search-box').val();

                data.searchFilter = $('#search-type').find('.active').find('input').val();

                //if (data.searchFilter != 'Auto_Detect') {
                //    data.searchFilter = $scope.SearchFilter;
                //}

                if ($scope.SearchTerm) {
                    data.searchTerm = $scope.SearchTerm;
                }

                data.pageIndex = $scope.LeadSearchService.Pagination.page;
                data.pageSize = $scope.LeadSearchService.Pagination.size;

                data.ForSearchstartDate = $scope.ForSearchstartDate;
                data.ForSearchendDate = $scope.ForSearchendDate;

                data.selectedDateText = $scope.selectedDateText;

                if ($routeParams.commercialType) {
                    data.commercialType = $routeParams.commercialType;
                }

                if ($routeParams.isReportSearch) {
                    data.isReportSearch = $routeParams.isReportSearch;
                }
                if (!kendotooltipService.optimiseTriggerFlag) {
                    LeadSearchService.Get(data);
                    kendotooltipService.optimiseTriggerFlag = true;
                    $scope.LeadSearchService.Leads_options = kendotooltipService.setColumnWidth($scope.LeadSearchService.Leads_options);
                    
                }
                //}

                // }
            }


            $scope.$on('$viewContentLoaded', function () {
                $location.replace(); //clear last history route
            });

            $scope.ConvertArrayToString = function (arr) {
                if (!arr) {
                    return '';
                }
                if ($.isNumeric(arr)) {
                    return arr;
                }
                return arr.join('-');
            }

            $scope.ConvertStringToArray = function (str) {
                var arr = [];
                if (str) {
                    var st = str.split('-');
                    for (var i = 0; i < st.length; i++) {
                        arr.push(st[i]);
                    }
                }
                return arr;
            }
            $scope.DeleteLead = function (lead) {
                if (!confirm("Do you want to continue?")) return;
                HFC.AjaxDelete('/api/leads/' + lead.LeadId, null, function (result) {
                    HFC.DisplaySuccess("Lead successfully deleted");
                    $scope.ApplyFilter();
                });
            }

            $scope.UpdateLeadNoteType = function (lead, note, typeenum) {
                if (typeenum != window.NoteTypeEnum.History) {
                    HFC.AjaxPut("/api/leads/" + lead.LeadId + "/notes", { NoteId: note.NoteId, TypeEnum: typeenum }, function () {
                        $scope.$apply(function () {
                            note.TypeEnum = typeenum;
                        });
                        HFC.DisplaySuccess("Note type successfully updated");
                    });
                }
            }

            $scope.AddNotes = function (lead, form) {
                var type = lead.NoteType;
                var msg = $.trim(lead.NoteMessage);
                if (msg && msg.length) {
                    HFC.AjaxPost("/api/leads/" + lead.LeadId + "/notes", {
                        TypeEnum: type,
                        Message: HFC.htmlEncode(msg)
                    }, function (result) {
                        if (!lead.LeadNotes) {
                            lead.LeadNotes = [];
                        }
                        $scope.$apply(function () {
                            lead.LeadNotes.splice(0, 0, { NoteId: result.NoteId || 0, AvatarSrc: HFC.CurrentUser.AvatarSrc, TypeEnum: type, Message: HFC.htmlEncode(msg), CreatedByPersonId: HFC.CurrentUser.PersonId, CreatorFullName: HFC.CurrentUser.FullName, CreatedOn: new XDate() });
                            lead.NoteMessage = "";
                        });
                    });
                }
            }

            $scope.AddJobNotes = function (job, form) {
                var type = parseInt(job.NoteType);
                var msg = $.trim(job.NoteMessage);
                if (msg && msg.length) {
                    HFC.AjaxPost("/api/jobs/" + job.JobId + "/notes", {
                        TypeEnum: type,
                        Message: HFC.htmlEncode(msg)
                    }, function (result) {
                        if (!job.JobNotes) {
                            job.JobNotes = [];
                        }

                        $scope.$apply(function () {
                            job.JobNotes.splice(0, 0, { NoteId: result.NoteId || 0, AvatarSrc: HFC.CurrentUser.AvatarSrc, TypeEnum: type, Message: HFC.htmlEncode(msg), CreatedByPersonId: HFC.CurrentUser.PersonId, CreatorFullName: HFC.CurrentUser.FullName, CreatedOn: new XDate() });
                            job.NoteMessage = "";
                        });
                    });
                }
            }

            $scope.UpdateJobNoteType = function (job, note, typeenum) {
                if (typeenum != window.NoteTypeEnum.History) {
                    HFC.AjaxPut("/api/jobs/" + job.JobId + "/notes", { NoteId: note.NoteId, TypeEnum: typeenum }, function () {
                        $scope.$apply(function () {
                            note.TypeEnum = typeenum;
                        });
                        HFC.DisplaySuccess("Note type successfully updated");
                    });
                }
            }


            $scope.GetLeadFullName = function (lead) {
                var fullname = "";
                if (lead.PrimPerson) {
                    fullname = $.trim(lead.PrimPerson.FirstName); //trim will convert null to empty string, great for our concatenation
                    if (lead.SecPerson && lead.SecPerson.PersonId) {
                        if (lead.SecPerson.LastName && lead.PrimPerson.LastName && lead.SecPerson.LastName.toLowerCase() == lead.PrimPerson.LastName.toLowerCase())
                            fullname += " & " + $.trim(lead.SecPerson.FirstName) + " " + $.trim(lead.PrimPerson.LastName);
                        else
                            fullname += " " + $.trim(lead.PrimPerson.LastName) + " & " + $.trim(lead.SecPerson.FirstName) + " " + $.trim(lead.SecPerson.LastName);
                    } else
                        fullname += " " + $.trim(lead.PrimPerson.LastName);

                    if (lead.PrimPerson.CompanyName)
                        fullname = "<b>" + lead.PrimPerson.CompanyName + "</b>" + "<br/>" + fullname;
                    else
                        fullname = "<b>" + fullname + "</b>";
                }
                return fullname;
            }

            $scope.GetJobStatuses = function () {
                return LK_JobStatuses;
            };

            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 235);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 235);
                };


                //$(window).resize(function () {
                //    var gridElement = $("#gridLeadSearch"),
                //        newHeight = gridElement.innerHeight(),
                //        otherElements = gridElement.children().not(".k-grid-content"),
                //        otherElementsHeight = 0;

                //    otherElements.each(function () {
                //        otherElementsHeight += $(this).outerHeight();
                //    });

                //    gridElement.children(".k-grid-content").height(newHeight - otherElementsHeight);
                //});
            });
            // End Kendo Resizing

            $scope.countWatchers = function () {
                var q = [$rootScope], watchers = 0, scope;
                while (q.length > 0) {
                    scope = q.pop();
                    if (scope.$$watchers) {
                        watchers += scope.$$watchers.length;
                    }
                    if (scope.$$childHead) {
                        q.push(scope.$$childHead);
                    }
                    if (scope.$$nextSibling) {
                        q.push(scope.$$nextSibling);
                    }
                }
                window.console.log(watchers);
            };

            $scope.GetLeadStatuses = function () {
                return LK_LeadStatuses;
            };

            $scope.GetSelectedSalesAgents = function () {
                return sales;
            }

            $scope.GetJobStatusNameById = function (jobStatus) {
                var status = HFC.Lookup.Get(LK_JobStatuses, jobStatus.Id), parent = null;
                if (status.ParentId) {
                    parent = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
                }
                if (parent)
                    return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
                else
                    return status.Name;
            }

            $scope.GetLeadStatusColor = function (lead) {
                var status = HFC.Lookup.Get(LK_LeadStatuses, lead.LeadStatusId);
                if (status.ParentId)
                    status = HFC.Lookup.Get(LK_LeadStatuses, status.ParentId);
                return status.ClassName || "btn-default";
            };

            $scope.GetJobStatusColor = function (job) {
                var status = HFC.Lookup.Get(LK_JobStatuses, job.JobStatusId);
                if (!status.ClassName && status.ParentId)
                    status = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
                return (status.ClassName || "btn-default");
            };

            $scope.GetJobStatusName = function (job) {
                var status = HFC.Lookup.Get(LK_JobStatuses, job.JobStatusId),
                    parent = null;
                if (status.ParentId)
                    parent = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
                if (parent)
                    return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
                else
                    return status.Name;
            };

            $scope.GetSelectedSalesAgent = function (job) {
                if (job.SalesPersonId && job.SalesPersonId) {
                    if (HFC.EmployeesJS && job.SalesPersonId)
                        var user = $.grep(HFC.EmployeesJS, function (e) {
                            return e.PersonId == job.SalesPersonId;
                        });

                    if (user && user.length)
                        return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
                    else {
                        if (HFC.DisabledEmployeesJS && job.SalesPersonId)
                        var user = $.grep(HFC.DisabledEmployeesJS, function (e) {
                            return e.PersonId == job.SalesPersonId;
                        });

                        if (user && user.length)
                            return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
                        else
                            return "Unassigned";
                    }

                } else
                    return "Unassigned";
            };

            $scope.GetPrimQuote = function (job) {
                return LeadSearchService.GetPrimeQuote(job.JobId);
            };

            $scope.FormatPercent = function (num, maxPrecision) {
                return HFC.formatPercent(num, maxPrecision);
            }

            $scope.FormatCurrency = function (num, precision) {
                return HFC.formatCurrency(num, precision);
            };

            $scope.ToFriendlyString = function (date, includeTime, ignoreTZ) {
                var str = "";
                var xdate = false;
                var self = this;


                if (ignoreTZ) {
                    xdate = new XDate(date, true);
                } else {

                    xdate = new XDate(date);
                }

                if (xdate && xdate.valid()) {
                    var today = new XDate();

                    if (xdate.getFullYear() == today.getFullYear()) {
                        if (xdate.getMonth() == today.getMonth() && xdate.getDate() == today.getDate())
                            str = "Today";
                        else if (xdate.getMonth() == today.getMonth() && (today.getDate() - xdate.getDate()) == 1)
                            str = "Yesterday";
                        else
                            str = xdate.toString("ddd, MMM d");
                    } else
                        str = xdate.toString("ddd, MMM d yyyy");

                    if (includeTime) {
                        str += " " + xdate.toString("h:mm tt");
                    }
                }

                return str;
            }

            $scope.GetLeadStatusName = function (lead) {
                var status = HFC.Lookup.Get(LK_LeadStatuses, lead.LeadStatusId), parent = null;
                if (status.ParentId)
                    parent = HFC.Lookup.Get(LK_LeadStatuses, status.ParentId);
                if (parent)
                    return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
                else
                    return status.Name;
            }

            $scope.GetLeadStatusNameById = function (leadStatus) {
                var status = HFC.Lookup.Get(LK_LeadStatuses, leadStatus.Id), parent = null;
                if (status.ParentId) {
                    parent = HFC.Lookup.Get(LK_LeadStatuses, status.ParentId);
                }
                if (parent)
                    return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
                else
                    return status.Name;
            }

            //function addressToString(isSingleLine, model) {
            //    if (model) {

            //        var str = "";
            //        if (model.Address1)
            //            str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

            //        return str;
            //    }
            //};

            //function MapDestinationaddress(model) {
            //    if (model) {

            //        var str = "";
            //        if (model.Address1)
            //            str = model.Address1 + (model.Address2 ? (", " + model.Address2 + " ") : "");

            //        if (model.City)
            //            str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
            //        else
            //            str += (model.ZipCode || "");

            //        return str;
            //    }
            //};


            //$scope.GetGoogleAddressHref = function (model) {
            //    var dest = MapDestinationaddress(model);
            //    if (model.Address1) {

            //        var srcAddr;

            //        if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
            //            srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
            //        } else {
            //            srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
            //        }

            //        if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
            //            return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
            //        } else {
            //            return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
            //        }
            //    } else
            //        return null;
            //}

            //$scope.AddressToString = function (model) { return addressToString(true, model) };

            $scope.LeadStatusOnChanged = function (lead, val) {
                HFC.AjaxBasic("PUT", HFC.Util.BaseURL() + '/api/leads/' + lead.LeadId + '/Status', JSON.stringify({ "Id": val }), function () {
                    $scope.$apply(function () {
                        lead.LeadStatusId = val;
                    });
                    
                    lead.isShowLeadStatuses = false;
                    HFC.DisplaySuccess("Status updated for Lead #" + lead.LeadNumber);
                });
            };

            $scope.GetKeyByValue = function (list, key) {
                return HFC.Lookup.GetKeyByValue(list, key);
            }

            $scope.GetNoteTypeEnum = function () {
                return window.NoteTypeEnum;
            }

            //var statusId = HFCService.GetQueryString("leadStatusId"),
            //    dateRange = HFCService.GetQueryString("dateRange");


            $scope.DateRangeArray = ["All Date & Time", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year", "Last Year"];

            $scope.selectedDateText = "All Date & Time";

            $scope.$watch('$scope.ForSearchReport', function (newVal) {
                if (newVal) {
                    $scope.selectedDateText = "All Date & Time";
                    $scope.ForSearchstartDate = null;
                    $scope.ForSearchendDate = null;
                    $scope.startDate = null;
                    $scope.endDate = null;
                }
            });
            $scope.$watch('startDate', function (newVal) {
                if (newVal != null) {
                    $scope.ForSearchstartDate = newVal;
                    if ($scope.endDate == null) {
                        $scope.selectedDateText = newVal + ' to ';
                    } else {
                        $scope.selectedDateText = newVal + ' to ' + $scope.endDate;
                    }
                }
            });

            $scope.$watch('endDate', function (newVal) {
                if (newVal != null) {
                    $scope.ForSearchendDate = newVal;
                    if ($scope.startDate == null) {
                        $scope.selectedDateText = ' to ' + newVal;
                    } else {
                        $scope.selectedDateText = $scope.startDate + ' to ' + newVal;
                    }
                }
            });

            $scope.endDate = null;
            $scope.startDate = null;
            $scope.ForSearchstartDate = null;
            $scope.ForSearchendDate = null;

            $scope.setDateRange = function (date) {
                var range = $scope.getDateRange(date);
                if (range.StartDate) {
                    $scope.ForSearchstartDate = range.StartDate.toString('MM/dd/yyyy');
                    //$scope.startDate = range.StartDate.toString('MM/dd/yyyy');
                } else {
                    //$scope.startDate = null;
                    $scope.ForSearchstartDate = null;
                }

                if (range.EndDate) {
                    $scope.ForSearchendDate = range.EndDate.toString('MM/dd/yyyy');
                } else {
                    $scope.ForSearchendDate = null;
                }

                $scope.selectedDateText = date;
            }

            $scope.getDateRange = function (range) {
                var endDate = new XDate(),
                    startDate = new XDate();
                switch (range.toLowerCase()) {
                    case "all date & time":
                        startDate = null;
                        endDate = null;
                        break;
                    case "this week":
                        startDate.addDays(-startDate.getDay());
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "this month":
                        startDate.setDate(1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "this year":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addYears(1).addDays(-1);
                        break;
                    case "last week":
                        startDate.addDays(-startDate.getDay() - 7);
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "last month":
                        startDate.setDate(1).addMonths(-1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "last year":
                        startDate.setDate(1).setMonth(0).addYears(-1);
                        endDate.setDate(1).setMonth(0).addDays(-1);
                        break;
                    case "last 7 days":
                        startDate.addDays(-7);
                        break;
                    case "last 30 days":
                        startDate.addDays(-30);
                        break;
                    case "1st quarter":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(3).addDays(-1);
                        break;
                    case "2nd quarter":
                        startDate.setDate(1).setMonth(3);
                        endDate = startDate.clone().addMonths(3).addDays(-1);
                        break;
                    case "3rd quarter":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(3).addDays(-1);
                        break;
                    case "4th quarter":
                        startDate.setDate(1).setMonth(9);
                        endDate = startDate.clone().addMonths(3).addDays(-1);
                        break;
                    case "1st half":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(6).addDays(-1);
                        break;
                    case "2nd half":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(6).addDays(-1);
                        break;
                    default:
                        startDate = null;
                        endDate = null;
                        break;
                }

                return { StartDate: startDate, EndDate: endDate };
            }

            $scope.GetURLParameter = function (sParam) {
                var sPageURL = window.location.search.substring(1);
                var data = $.parseParams(window.location.href);
                if (data) {
                    return data[sParam];
                }
            };

            //if ($routeParams.sortIsDesc) {
            //    $scope.SortIsDesc = $routeParams.sortIsDesc;
            //}

            //if ($routeParams.orderBy) {
            //    $scope.SortBy = $routeParams.orderBy;
            //}


            if ($routeParams.salesPersonIds) {
                if ($.isNumeric($routeParams.salesPersonIds)) {
                    $scope.selectedSales = [];
                    $scope.selectedSales.push($routeParams.salesPersonIds);
                } else {
                    $scope.selectedSales = $scope.ConvertStringToArray($routeParams.salesPersonIds);
                }
            }

            if ($routeParams.jobStatusIds) {
                if ($.isNumeric($routeParams.jobStatusIds)) {
                    $scope.selectedJobsStatuses = [];
                    $scope.selectedJobsStatuses.push(+$routeParams.jobStatusIds);
                } else {
                    $scope.selectedJobsStatuses = $scope.ConvertStringToArray($routeParams.jobStatusIds);

                }
            }
            if ($routeParams.leadStatusIds) {
                if ($.isNumeric($routeParams.leadStatusIds)) {

                    $scope.selectedleadStatuses = [];
                    $scope.selectedleadStatuses.push(+$routeParams.leadStatusIds);
                } else {
                    $scope.selectedleadStatuses = $scope.ConvertStringToArray($routeParams.leadStatusIds);
                }
            }

            if ($routeParams.sourceIds) {
                if ($.isNumeric($routeParams.sourceIds)) {

                    $scope.selectedSources = [];
                    $scope.selectedSources.push(+$routeParams.sourceIds);
                } else {
                    $scope.selectedSources = $scope.ConvertStringToArray($routeParams.sourceIds);
                }
            }

            if ($routeParams.invoiceStatuses) {
                if ($.isNumeric($routeParams.invoiceStatuses)) {
                    $scope.invoiceStatusesSearch = [];
                    $scope.invoiceStatusesSearch.push($routeParams.invoiceStatuses);
                    //$scope.selectedSales = $routeParams.salesPersonIds;
                    //$scope.selectedSales.push(data['salesPersonIds']);
                } else {
                    $scope.invoiceStatusesSearch = $scope.ConvertStringToArray($routeParams.invoiceStatuses);
                }
            }


            if ($routeParams.selectedDateText) {
                $scope.selectedDateText = $routeParams.selectedDateText;
                $scope.ForSearchstartDate = $routeParams.createdOnUtcStart;
                $scope.ForSearchendDate = $routeParams.createdOnUtcEnd;
            } else {
                if ($routeParams.createdOnUtcStart) {
                    $scope.ForSearchstartDate = $routeParams.createdOnUtcStart;
                    $scope.startDate = $routeParams.createdOnUtcStart;
                }
                if ($routeParams.createdOnUtcEnd) {
                    $scope.ForSearchendDate = $routeParams.createdOnUtcEnd;
                    $scope.endDate = $routeParams.createdOnUtcEnd;
                }
            }

            if ($routeParams.searchTerm) {
                $scope.SearchTerm = $routeParams.searchTerm;
                if ($routeParams.searchFilter == "Phone_Number") {
                    var phoneno = /^(?=.*?[1-9])[0-9()-. ]+$/;
                    var inputtxt = $routeParams.searchTerm;
                    if (!inputtxt.match(phoneno)) {
                        HFC.DisplayAlert("Please verify the phone number you entered.");
                        //clear the parameters to show empty result on error
                        var data = {};
                        LeadSearchService.Get(data);
                        return false;
                    }
                }
            }


            if ($routeParams.searchFilter != 'undefined') {
                $rootScope.searchMode = $routeParams.searchFilter;
            } else {
                $routeParams.searchFilter = 'Auto_Detect';
                $rootScope.searchMode = $routeParams.searchFilter;
            }

            $scope.LeadStatusOptions = {
                dataTextField: "Id",
                dataValueField: "Id",
                itemTemplate: '<span>&nbsp; # if (data.IsChiled) { # &nbsp;&nbsp;   # } # #: data.Name # </span> ',
                tagTemplate: '<span >#: data.Name #</span> ',
                dataSource: leadStatuses,
                placeholder: "Select Lead Statuses...",
            };


            $scope.customersDataSource = {
                transport: {
                    read: {
                        dataType: "jsonp",
                        url: "//demos.telerik.com/kendo-ui/service/Customers",
                    }
                }
            };

            window.onresize = function (event) {
                if (window.innerWidth >= 1280)
                    var dataWrapperList = $(".k-grid-content tr td .tooltip-wrapper");
                else
                    var dataWrapperList = $(".k-grid-content tr td .tooltip-wrapper1");

            };

            $scope.customOptions = {
                dataSource: $scope.customersDataSource,
                dataTextField: "ContactName",
                dataValueField: "CustomerID",

                headerTemplate: '<div class="dropdown-header k-widget k-header">' +
                    '<span>Photo</span>' +
                    '<span>Contact info</span>' +
                    '</div>',

                // using {{angular}} templates:
                valueTemplate: '<span class="selected-value" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span><span>{{dataItem.ContactName}}</span>',

                template: '<span class="k-state-default" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span>' +
                          '<span class="k-state-default"><h3>{{dataItem.ContactName}}</h3><p>{{dataItem.CompanyName}}</p></span>',
            };

            // do width manipulation for devices that have more than 1280px width
            //var setColumnWidth = function () {
            //    var screenWidth = window.innerWidth;
            //    var grid = $scope.LeadSearchService.Leads_options;
            //    var columnsList = grid.columns;
            //    if (screenWidth >= 1280) {
            //        var columnWidth = ((screenWidth - 160) / 13);
            //        for (var i = 0; i < columnsList.length; i++) {
            //            columnsList[i].width = columnWidth + "px";
            //        }
            //        columnsList[columnsList.length - 1].width = "80px";
            //        restrictTooltip(columnWidth);
            //    } else {
            //        // restrictTooltip(columnWidth, true);
            //        $(".k-grid-content tr td").css({ "overflow": "hidden !important", "white-space": "normal" });
            //    }

            //    //if (screenWidth < 1280) {
            //    //    if (dataWidth > (columnWidth - 20) && dataWidth != 0) {
            //    //        var dataLength = data.length;
            //    //        var textWidth = dataWidth / dataLength;
            //    //        var limitCount = (columnWidth - 20) / textWidth;
            //    //        var updatedText = "<div class='tooltip-content11'>" + data + "</div>" + data.substring(0, (limitCount - 3)) + "...";
            //    //        $(dataWrapperList[i]).html(updatedText);
            //    //    } else {
            //    //        $(dataWrapperList[i]).text(data);
            //    //    }
            //    //}

            //    // $(".k-grid-content tr td .tooltip-wrapper").mouseenter(function (e) {
            //    //        var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper1 .tooltip-content");
            //    //        $(tooltipContainers).css("visibility", "hidden");
            //    //        var tooltip = $(e.target).children();
            //    //        $(tooltip).css("visibility", "visible");
            //    //    });
            //}

            // bind tooltip restriction functionality
            //var restrictTooltip = function (columnWidth, flag) {
            //    var loadingElement = $("#loading");
            //    if (loadingElement) {
            //        loadingElement[0].style.display = "block";
            //    }
            //    setTimeout(function () {
            //        var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
            //        $(tooltipContainers).css("visibility", "hidden");
            //        if ( window.innerWidth >= 1280) 
            //            var dataWrapperList = $(".k-grid-content tr td .tooltip-wrapper");
            //        else
            //            var dataWrapperList = $(".k-grid-content tr td .tooltip-wrapper1");

            //        for (var i = 0; i < dataWrapperList.length; i++) {
            //            if (flag) {
            //                columnWidth = $(dataWrapperList[i]).parent().width();
            //            }
            //            var dataWidth = $(dataWrapperList[i]).width();
            //            var children = $(dataWrapperList[i]).children()[1];
            //            if (children) {
            //                var data = $(children).text();
            //                if (dataWidth > (columnWidth - 20) && dataWidth != 0) {
            //                    var dataLength = data.length;
            //                    var textWidth = dataWidth / dataLength;
            //                    var limitCount = (columnWidth - 20) / textWidth;
            //                    var updatedText = "<div class='tooltip-content'>" + data + "</div>" + data.substring(0, (limitCount - 3)) + "...";
            //                    $(dataWrapperList[i]).html(updatedText);
            //                } else {
            //                    $(dataWrapperList[i]).text(data);
            //                }
            //            }
            //        }
                    
            //        $(".k-grid-content tr td .tooltip-wrapper").mouseenter(function (e) {
            //            var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
            //            $(tooltipContainers).css("visibility", "hidden");
            //            var tooltip = $(e.target).children();
            //            $(tooltip).css("visibility", "visible");
            //        });
            //        $(".k-grid-content tr td .tooltip-wrapper").mouseleave(function (e) {
            //            var tooltipContainers = $(".k-grid-content tr td .tooltip-wrapper .tooltip-content");
            //            $(tooltipContainers).css("visibility", "hidden");
            //        });
            //        if (loadingElement) {
            //            loadingElement[0].style.display = "none";
            //        }
            //    }, 3000);
            //        //var tooltipContainers = $("#gridLeadSearch .k-grid-content tr td .tooltip-wrapper .tooltip-content");
            //        //$(tooltipContainers).css("display", "none");
            //}


            angular.element(document).ready(function () {
                //if ($routeParams.searchTerm)
                $('#search-box').val($routeParams.searchTerm);

                //if ($routeParams.searchFilter) {
                //    $rootScope.searchMode = 'searchFilter';
                //}

                if ($routeParams.searchFilter) {
                    var lis = $('#search-type').find('li');
                    lis.removeClass('active');
                    for (var i = 0; i < lis.length; i++) {
                        if ($(lis[i]).find('input').val() == $routeParams.searchFilter) {
                            $(lis[i]).addClass('active');
                        }
                    }
                    //<li class="active"><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Auto')"><input type="radio" name="searchFilter" value="Auto_Detect" />Auto Detect</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Address')"><input type="radio" name="searchFilter" value="Address" />Address</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Customer Name')"><input type="radio" name="searchFilter" value="Customer_Name" />Customer Name</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Email')"><input type="radio" name="searchFilter" value="Email" />Email</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Job/Invoice Number')"><input type="radio" name="searchFilter" value="Number" />Job/Invoice Number</a></li>
                    //                <li><a href="" onclick="return HFC.ToggleSearchFilter(this)" ng-click="setSearchMode('Phone Number')"><input type="radio" name="searchFilter" value="Phone_Number" />Phone Number</a></li>

                    if ($routeParams.searchFilter != 'undefined') {
                        switch ($routeParams.searchFilter) {
                            case 'Customer_Name':
                                $rootScope.searchMode = 'Customer Name';
                                break;
                            case 'Number':
                                $rootScope.searchMode = 'Job/Invoice Number';
                                break;
                            case 'Phone_Number':
                                $rootScope.searchMode = 'Phone Number';
                                break;
                            default:
                                $rootScope.searchMode = $routeParams.searchFilter;
                        }
                    }
                    else {
                        $rootScope.searchMode = 'Auto_Detect';
                        $scope.searchFilter = 'Auto_Detect';
                        $routeParams.searchFilter = 'Auto_Detect';
                    }
                }
                
                $scope.ApplyFilter();
            });

            //$scope.tooltipOptions = {
            //    filter: "td,th",
            //    position: "top",
            //    hide: function (e) {
            //        this.content.parent().css('visibility', 'hidden');
            //    },
            //    show: function (e) {
            //        if (this.content.text().length > 1) {
            //            if (this.content.text().trim() === "Edit QualifyAppointment Print") {
            //                this.content.parent().css('visibility', 'hidden');
            //            } else {
            //                this.content.parent().css('visibility', 'visible');
            //            }

            //        }
            //        else {
            //            this.content.parent().css('visibility', 'hidden');
            //        }
            //    },
            //    content: function (e) {

            //        return e.target.context.textContent;
            //    }
            //};

            saveCriteriaService.selectedJobsStatuses = $scope.selectedJobsStatuses;
            saveCriteriaService.selectedleadStatuses = $scope.selectedleadStatuses;
            saveCriteriaService.selectedSources = $scope.selectedSources;
            saveCriteriaService.selectedSales = $scope.selectedSales;
            saveCriteriaService.invoiceStatusesSearch = $scope.invoiceStatusesSearch;

            saveCriteriaService.selectedDateText = $scope.selectedDateText;
            saveCriteriaService.ForSearchstartDate = $scope.ForSearchstartDate;
            saveCriteriaService.ForSearchendDate = $scope.ForSearchendDate;
            saveCriteriaService.startDate = $scope.startDate;
            saveCriteriaService.endDate = $scope.endDate;

            $rootScope.$broadcast('applyMainSearchFilter');

            //$('head title').text("" + applicationTitle);
           

            // Helper function to call the print feature.
            $scope.LeadSearchService.printControl = {};
            $scope.printSalesPacket2 = function (modalid, leadId) {
                //old
                //$scope.LeadSearchService.leadIdPrint = leadId;
                //$scope.LeadSearchService.printControl.showPrintModal();
                $scope.NavbarService.printLead(leadId);
            }

            // for checkbox to bring leads with lost lead
            $scope.GetSearchLeads_forcheck = function () {
                var setvalue = $('input[name="lostlead_Check"]:checked').val();
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
                if (setvalue == "on") {                    
                    $http.get('/api/search/0/GetSearchLeads_forcheck').then(function (response) {
                        
                        var data = response.data;
                        var grid = $("#gridLeadSearch").data("kendoGrid");
                        var dataSource = grid.dataSource;
                        dataSource._data = data;
                        //grid.dataSource.data(data);
                        //for displaying status in kendo grid filter
                        grid.setDataSource(data);
                        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                        //if (!kendotooltipService.optimiseTriggerFlag) {
                        //    LeadSearchService.Get(data);
                        //    kendotooltipService.optimiseTriggerFlag = true;
                        //   $scope.LeadSearchService.Leads_options = kendotooltipService.setColumnWidth($scope.LeadSearchService.Leads_options);
                        //}
                      
                        $scope.LeadSearchService.Leads_options = kendotooltipService.setColumnWidth($scope.LeadSearchService.Leads_options);

                    }).catch(function (e) {
                        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                    })                    
                }
                else {                    
                    $http.get('/api/search/0/GetSearchLeads').then(function (response) {
                        
                        var data = response.data;
                        var grid = $("#gridLeadSearch").data("kendoGrid");
                        var dataSource = grid.dataSource;
                        dataSource._data = data;
                        //grid.dataSource.data(data);
                        //for displaying status in kendo grid filter
                        grid.setDataSource(data);
                        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                    }).catch(function(e)
                    {
                        kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                    });
                }
            }

            $scope.DownloadLead_Excel = function()
            {
                var grid = $("#gridLeadSearch").data("kendoGrid");
                grid.saveAsExcel();
            }

            // $scope.SetAccounts = function () {
            //   $scope.CalendarService.SetAccountsTest();

            //  }
            // $scope.SetAccounts();

            //$("#gridLeadSearch").kendoGrid({
            //    dataSource: {
            //        type: "odata",
            //        transport: {
            //            read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
            //        },
            //        schema: {
            //            model: {
            //                fields: {
            //                    OrderID: { type: "number" },
            //                    ShipName: { type: "string" },
            //                    ShipCity: { type: "string" }
            //                }
            //            }
            //        },
            //        pageSize: 25,
            //        serverPaging: true,
            //        serverFiltering: true,
            //        serverSorting: true
            //    },
            //    filterable: true,
            //    sortable: true,
            //    resizable: true,
            //    pageable: true,
            //    pageable: {
            //        refresh: true,
            //        pageSize: 20,
            //        pageSizes: [10, 25, 50, 100, 'All'],
            //        buttonCount: 5
            //    },
            //    columns: [{
            //        field: "OrderID",
            //        filterable: false,
            //        width: 200
            //    },
            //     {
            //         field: "ShipName",
            //         title: "ShipName---"
            //     },
            //        "ShipCity"
            //    ]
            //});

            // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                };
            });
            // End Kendo Resizing

            $scope.leadOption2 = {
                dataSource: {
                    type: "odata",
                    //resizable: true,
                    //groupable: true,
                    //scrollable: true,
                    //sortable: true,
                    //pageable: true,
                    //height: 300,
                    transport: {
                        read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
                    },
                    schema: {
                        model: {
                            fields: {
                                OrderID: { type: "number" },
                                ShipName: { type: "string" },
                                ShipCity: { type: "string" }
                            }
                        }
                    },
                    pageSize: 25,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                filterable: true,
                sortable: true,
                resizable: true,
                pageable: true,
                pageable: {
                    refresh: true,
                    pageSize: 20,
                    resizable: true,
                    pageSizes: [10, 25, 50, 100, 'All'],
                    buttonCount: 5
                },
                columns: [{
                    field: "OrderID",
                    filterable: false,
                    width: 200
                },
                 {
                     field: "ShipName",
                     title: "---ShipName"
                 },
                 {
                     field: "ShipCity",
                     title: "ShipCity++"
                 }
                    
                ]
            };

            
        }
    ])
