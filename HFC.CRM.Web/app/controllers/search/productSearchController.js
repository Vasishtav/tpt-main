﻿/***************************************************************************\
Module Name:  Product Search Controller .js - AngularJS file
Project: HFC
Created on: 02 November 2017 Thursday
Created By:
Copyright:
Description: Product Search Controller
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app
    .directive('dropDown', function () {
        return {
            link: function (scope, element, attr, ctrl) {
                var $el = $(element);

                $el.parent().on({
                    "click": function (e) {
                        if ($(e.currentTarget).hasClass("btn-group")) {
                            var obj = $(this);
                            obj.data("closable", false);
                            setTimeout(function () { obj.data("closable", true); }, 500);
                        } else
                            $(this).data("closable", true);
                    },
                    "hide.bs.dropdown": function (e) {
                        if ($(e.relatedTarget).hasClass("inner-dropdown"))
                            return true;
                        else
                            return $(this).data("closable");
                    }
                });
            }
        }
    })
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('productSearchController',
    [
        '$scope', 'HFCService', '$routeParams', '$location', '$rootScope', 'saveCriteriaService', '$templateCache', '$sce', '$compile', 'kendoService',
        function ($scope, HFCService, $routeParams, $location, $rootScope, saveCriteriaService, $templateCache, $sce, $compile, kendoService) {

            $rootScope.enableSaveSearch = enableSaveSearch;

            $scope.showProductStatus = function (Product) {
                if (Product.isShowProductStatuses) {
                    return;
                }
                Product.isShowProductStatuses = !Product.isShowProductStatuses;
            };

            $scope.showNotes = function (Product) {
                Product.isShowNotes = !Product.isShowNotes;
            };

            
            $scope.HFCService = HFCService;
            $scope.filter_CounterSelectedSalesAgent = 'Nothing selected';
            $scope.filter_CounterSelectedJobStatus = 'Nothing selected';
            $scope.selectedSales = null;
            $scope.selectedProductStatuses = null;
            $scope.selectedSources = null;
            $scope.SearchFilter = 'Auto_Detect';


            // for kendo tooltip
            $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
            kendoService.customTooltip = null;


            $scope.Permission = {};
            var MyProductspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'MyProducts')

            $scope.Permission.ListProducts = MyProductspermission.CanRead; //$scope.HFCService.GetPermissionTP('List Products').CanAccess;

            $scope.ChangeFilterSearch = function (val) {

                $scope.SearchFilter = val;
            }

            $scope.getClassProductSearch = function (val) {

                if (val == $scope.SearchFilter) {
                    return 'active';
                }
                return '';
            }

           
           

           
            $scope.filter_SelectSalesAgent = function (id) {
                $scope.filter_CounterSelectedSalesAgent = id;
            }

            $scope.filter_SelectJobStatus = function (id) {
                $scope.filter_CounterSelectedJobStatus = id;
            }
            $scope.SortBy = 'createdonutc';
            $scope.SortIsDesc = true;
            $scope.ChangeSortOrder = function (sortBy) {
                if (sortBy) {
                    if (sortBy != $scope.SortBy) {
                        $scope.SortBy = sortBy;
                        $scope.SortIsDesc = true;
                    } else {
                        $scope.SortIsDesc = !$scope.SortIsDesc;

                    }
                    $scope.ApplyFilter();
                }
            }

           

           

            

           
            

            

            
            function addressToString(isSingleLine, model) {
                if (model) {

                    var str = "";
                    if (model.Address1)
                        str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

                    if (model.City)
                        str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
                    else
                        str += (model.ZipCode || "");

                    return str;
                }
            };

            //$scope.tooltipOptions = {
            //    filter: "td,th",
            //    position: "top",
            //    hide: function (e) {
            //        this.content.parent().css('visibility', 'hidden');
            //    },
            //    show: function (e) {
            //        if (this.content.text().length > 1) {
            //            if (this.content.text().trim() === "Edit QualifyAppointment Print") {
            //                this.content.parent().css('visibility', 'hidden');
            //            } else {
            //                this.content.parent().css('visibility', 'visible');
            //            }

            //        }
            //        else {
            //            this.content.parent().css('visibility', 'hidden');
            //        }
            //    },
            //    content: function (e) {

            //        return e.target.context.textContent;
            //    }
            //};

            $scope.GetGoogProductdressHrefProducts = function (dest) {
                //return "maps.google.com?saddr= '1927 n glassell st orange ca'&daddr='21273 beechwood way lake forest ca'";
                var srcAddr = '1927 n glassell st orange ca';
                var dest1 = '21273 beechwood way lake forest ca';
                return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                
                //if (model.Address1) {

                //    var srcAddr;

                //    if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                //        srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                //    } else {
                //        srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                //    }

                //    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                //        return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                //    } else {
                //        return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                //    }
                //} else
                //    return null;
            }

            $scope.GetGoogProductdressHref = function (model) {
                var dest = addressToString(true, model);
                if (model.Address1) {

                    var srcAddr;

                    if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                        srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                    } else {
                        srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                    }

                    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                        return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                    } else {
                        return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                    }
                } else
                    return null;
            }

            $scope.AddressToString = function (model) { return addressToString(true, model) };

            $scope.customersDataSource = {
                transport: {
                    read: {
                        dataType: "jsonp",
                        url: "//demos.telerik.com/kendo-ui/service/Customers",
                    }
                }
            };

            $scope.customOptions = {
                dataSource: $scope.customersDataSource,
                dataTextField: "ContactName",
                dataValueField: "CustomerID",

                headerTemplate: '<div class="dropdown-header k-widget k-header">' +
                    '<span>Photo</span>' +
                    '<span>Contact info</span>' +
                    '</div>',

                valueTemplate: '<span class="selected-value" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span><span>{{dataItem.ContactName}}</span>',

                template: '<span class="k-state-default" style="background-image: url(\'//demos.telerik.com/kendo-ui/content/web/Customers/{{dataItem.CustomerID}}.jpg\')"></span>' +
                          '<span class="k-state-default"><h3>{{dataItem.ContactName}}</h3><p>{{dataItem.CompanyName}}</p></span>',
            };

            angular.element(document).ready(function () {
                //if ($routeParams.searchTerm)
                $('#search-box').val($routeParams.searchTerm);

                if ($routeParams.searchFilter) {
                    var lis = $('#search-type').find('li');
                    lis.removeClass('active');
                    for (var i = 0; i < lis.length; i++) {
                        if ($(lis[i]).find('input').val() == $routeParams.searchFilter) {
                            $(lis[i]).addClass('active');
                        }
                    }
                      if ($routeParams.searchFilter != 'undefined') {
                        switch ($routeParams.searchFilter) {
                            case 'Customer_Name':
                                $rootScope.searchMode = 'Customer Name';
                                break;
                            case 'Number':
                                $rootScope.searchMode = 'Job/Invoice Number';
                                break;
                            case 'Phone_Number':
                                $rootScope.searchMode = 'Phone Number';
                                break;
                            default:
                                $rootScope.searchMode = $routeParams.searchFilter;
                        }
                    }
                    else {
                        $rootScope.searchMode = 'Auto_Detect';
                        $scope.searchFilter = 'Auto_Detect';
                        $routeParams.searchFilter = 'Auto_Detect';
                    }

                }
                
                $scope.ApplyFilter();
            });

            saveCriteriaService.selectedJobsStatuses = $scope.selectedJobsStatuses;
            saveCriteriaService.selectedProductStatuses = $scope.selectedProductStatuses;
            saveCriteriaService.selectedSources = $scope.selectedSources;
            saveCriteriaService.selectedSales = $scope.selectedSales;
            saveCriteriaService.invoiceStatusesSearch = $scope.invoiceStatusesSearch;

            saveCriteriaService.selectedDateText = $scope.selectedDateText;
            saveCriteriaService.ForSearchstartDate = $scope.ForSearchstartDate;
            saveCriteriaService.ForSearchendDate = $scope.ForSearchendDate;
            saveCriteriaService.startDate = $scope.startDate;
            saveCriteriaService.endDate = $scope.endDate;

            $rootScope.$broadcast('applyMainSearchFilter');

            //$('head title').text("" + applicationTitle);
            HFCService.setHeaderTitle("Product");
        }
    ])
