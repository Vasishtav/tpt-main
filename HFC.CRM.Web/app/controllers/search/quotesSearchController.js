﻿'use strict';
app.controller('quotesSearchController',
    [
         '$scope', '$http', 'HFCService', 'NavbarService', 'PrintService', '$anchorScroll', 'kendotooltipService' ,
function ($scope, $http, HFCService, NavbarService, PrintService, $anchorScroll, kendotooltipService) {

              $scope.NavbarService = NavbarService;
              $scope.PrintService = PrintService;
              $scope.NavbarService.SelectSales();
              //Hardcoded the value for Tool Tip
              $scope.OrderLinesToolTip = "Total number of lines on the Sales Order";
              $scope.TotalQTYToolTip = "Count of all Products on sales order (Core Products and My Products)";
              $scope.TotalAmountToolTip = "Total Sales Amount";
              $scope.NavbarService.EnableSalesQutoesTab();
              $scope.ActiveToolTips = false;
              $scope.SelectedQuotesStatus = [];
              $scope.AccountId = 0;
              $scope.SelectedPrimaryQuote = true;

              $scope.Permission = {};
              var Quotepermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Quote')
              var offsetvalue = 0;
              $scope.Permission.QuoteList = Quotepermission.CanRead;
             //HFCService.GetPermissionTP('List Quotes').CanAccess;


              HFCService.setHeaderTitle("Quotes #");
              function DropdownTemplate(dataItem) {

                  var Div = '';
                  Div += '<ul> <li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"> ';

                  Div += ' <li><a href="javascript:void(0)" ng-click="PrintQuotee(' + dataItem.QuoteKey + ')">Print Quote</a></li> ';


                  return Div;
              }
              
    //to get Dashboard Filter

              $http.get('/api/lookup/0/Type_DateTime?Tableid=' + 4).then(function (data) {
                  $scope.DateRangeValue = data.data;
                  for (var i = 0; i < $scope.DateRangeValue.length; i++) {
                      if ($scope.DateRangeValue[i]['Name'] == 'Last 90 days (rolling)') {
                          $scope.DateRangeId = $scope.DateRangeValue[i]['Id'];
                          $scope.OnChangeDateRangeFilter($scope.DateRangeValue[i]['Id']);
                          $scope.getGrid();
                      }
                  }
              });

              $scope.InitialLoad = true;

              $scope.GridEditable = false;
              $scope.getGrid = function () {
                  $scope.QuoteList = {
                      excel: {
                          fileName: "Quotes_Export.xlsx",
                          allPages: true
                      },
                      dataSource: {
                          transport: {
                              read: {
                                  url: function (params) {
                                      var url1 = '/api/Quotes/0/GetAllQuote?quoteStatusListValue=' + $scope.SelectedQuotesStatus + '&dateRangeValue=' + $scope.DateRangeId + '&PrimaryQuote=' + $scope.SelectedPrimaryQuote;
                                      return url1;
                                  },
                                  dataType: "json"
                              }

                          },
                          //requestStart: function () {
                          //    if(!$scope.InitialLoad)
                          //    kendo.ui.progress($("#gridQuoteSearch"), true);                              
                          //},
                          //requestEnd: function () {
                          //    if (!$scope.InitialLoad)
                          //        kendo.ui.progress($("#gridQuoteSearch"), false);
                          //    else
                          //        $scope.InitialLoad = false;
                          //},
                          error: function (e) {
                              HFC.DisplayAlert(e.errorThrown);
                              //kendo.ui.progress($("#gridQuoteSearch"), false);
                          },
                          schema: {
                              model: {
                                  id: "QuoteID",
                                  fields: {
                                      Territory: { editable: false, nullable: true },
                                      QuoteID: { type: 'number', editable: false, nullable: true },
                                      QuoteKey: { editable: false, nullable: true },
                                      OpportunityId: { type: 'number', editable: false, nullable: true },
                                      QuoteName: { validation: { required: true } },
                                      OpportunityName: { validation: { required: true } },
                                      AccountName: { validation: { required: true } },
                                      PrimaryQuote: { type: "boolean", validation: { required: true } },
                                      QuoteStatus: { validation: { required: false } },
                                      AccountId: { type: 'number', editable: false, nullable: true },
                                      SalesAgent: { validation: { required: false } },
                                      NetCharges: { validation: { required: true } },
                                      CreatedOn: { type: 'date', validation: { required: true } },
                                      LastUpdatedOn: { validation: { required: true } },
                                      OrderExists: { editable: false },
                                      QuoteSubTotal: { type: 'number', editable: false },
                                      Quantity: { type: 'number', editable: false },
                                      NumberOfQuotelines: { type: 'number', editable: false }
                                  }
                              }
                          }
                      },
                      dataBound: function (e) {
                          // auto fit column
                          //for (var i = 0; i < this.columns.length; i++) {
                          //    this.autoFitColumn(i);
                          //}
                          // end auto fit column
                          //console.log("dataBound");
                          if (this.dataSource.view().length == 0) {
                              // The Grid contains No recrods so hide the footer.
                              $('.k-pager-nav').hide();
                              $('.k-pager-numbers').hide();
                              $('.k-pager-sizes').hide();
                          } else {
                              // The Grid contains recrods so show the footer.
                              $('.k-pager-nav').show();
                              $('.k-pager-numbers').show();
                              $('.k-pager-sizes').show();
                          }
                          if (kendotooltipService.columnWidth) {
                              kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                          } else if (window.innerWidth < 1280) {
                              kendotooltipService.restrictTooltip(null);
                          }
                          $scope.wrapperHeightValue = $(".k-grid-content.k-auto-scrollable").height();
                      },
                      columns: [
                    {
                        field: "QuoteID",
                        title: "Quote ID",
                        filterable: {
                            //multi: true,
                            search: true
                        },
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.QuoteID) {
                                return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.QuoteID + "</div><a href='/#!/quote/" + dataItem.OpportunityId + "/" + dataItem.QuoteKey + "' style='color: rgb(61,125,139);'>" + dataItem.QuoteID + "</a></span></span>";
                            } else {
                                return "";
                            }
                        }
                        //template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteID #</span></a>"

                    },

                    {
                        field: "QuoteName",
                        title: "Quote Name",
                        hidden: false,
                        width: "150px",
                        template: function (dataItem) {
                            if (dataItem.QuoteName) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.QuoteName + "</div><a href='/#!/quote/" + dataItem.OpportunityId + "/" + dataItem.QuoteKey + "' style='color: rgb(61,125,139);'>" + dataItem.QuoteName + "</a></span>";
                            } else {
                                return "";
                            }
                        },
                        //template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteName #</span></a>",
                        filterable: {
                            //multi: true,
                            search: true
                        }
                    },
                     {
                         field: "PrimaryQuote",
                         title: "Primary Quote",
                         hidden: false,
                         width: "140px",
                         template:
                             '<input type="checkbox" #= PrimaryQuote ? "checked=checked" : "" # disabled="disabled" class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched"></input>',
                         filterable: { multi: true, search: true }
                     },
                      {
                          field: "LastUpdatedOn",
                          title: "Last Update",
                          hidden: false,
                          width: "150px",
                          type: "date",
                          filterable: HFCService.gridfilterableDate("date", "QuoteFilterCalendar", offsetvalue),
                          template: function(dataItem) {
                              if (dataItem.LastUpdatedOn) {
                                  return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.LastUpdatedOn) + "</div>" + HFCService.KendoDateFormat(dataItem.LastUpdatedOn) + "</span>";
                              } else {
                                  return "";
                              }
                          }
                      },
                       {
                           field: "AccountName",
                           title: "Account Name",
                           filterable: {
                               //multi: true,
                               search: true
                           },
                           width: "150px",
                           template: function (dataItem) {
                               if (dataItem.AccountName) {
                                   return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountName + "</div><a href='/#!/Accounts/" + dataItem.AccountId + "' style='color: rgb(61,125,139);'>" + dataItem.AccountName + "</a></span>";
                               } else {
                                   return "";
                               }
                           }
                           //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= AccountName #</span></a>"

                       },
                       {
                           field: "OpportunityName",
                           title: "Opportunity",
                           width: "200px",
                           filterable: {
                               //multi: true,
                               search: true
                           },
                           template: function (dataItem) {
                               if (dataItem.OpportunityName) {
                                   return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OpportunityName + "</div><a href='/#!/OpportunityDetail/" + dataItem.OpportunityId + "' style='color: rgb(61,125,139);'>" + dataItem.OpportunityName + "</a></span>";
                               } else {
                                   return "";
                               }
                           }
                           //template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span>#= OpportunityName #</span></a>"

                       },
                          {
                              field: "Territory",
                              title: "Territory",
                              template: function (dataItem) {
                                  if (dataItem.Territory) {
                                      return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Territory + "</div><span>" + dataItem.Territory + "</span></span>";
                                  } else {
                                      return "";
                                  }
                              },
                              filterable: { multi: true, search: true },
                              width: "200px",
                              //  template: "<a href='\\\\#!/quote/#= OpportunityId #/#= QuoteKey #' style='color: rgb(61,125,139);'><span>#= QuoteID #</span></a> "

                          },
                       {
                           field: "SideMark",
                           title: "Sidemark",
                           template: function (dataItem) {
                               if (dataItem.SideMark) {
                                   return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.SideMark + "</div><span>" + dataItem.SideMark + "</span></span>";
                               } else {
                                   return "";
                               }
                           },
                           width: "120px",
                           hidden: false
                       },
                       {
                           field: "SalesAgent",
                           title: "Sale Agent",
                           template: function (dataItem) {
                               if (dataItem.SalesAgent) {
                                   return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.SalesAgent + "</div><span>" + dataItem.SalesAgent + "</span></span>";
                               } else {
                                   return "";
                               }
                           },
                           width: "150px",
                           hidden: false
                       },
                       {

                           field: "NumberOfQuotelines",
                           //title: "Order Lines",
                           template: function (dataItem) {
                               if (dataItem.NumberOfQuotelines) {
                                   return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.NumberOfQuotelines + "</div><span>" + dataItem.NumberOfQuotelines + "</span></span></span>";
                               } else {
                                   return "";
                               }
                           },
                           headerTemplate: '<div kendo-tooltip k-content="OrderLinesToolTip" >Order Lines</div>',
                           hidden: false,
                           width: "150px",

                       },
                       {
                           field: "Quantity",
                           //title: "Total QTY",
                           template: function (dataItem) {
                               if (dataItem.Quantity) {
                                   return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.NumberFormat(dataItem.Quantity, 'number') + "</div><span>" + HFCService.NumberFormat(dataItem.Quantity, 'number') + "</span></span></span>";
                               } else {
                                   return "";
                               }
                           },
                           headerTemplate: '<div kendo-tooltip k-content="TotalQTYToolTip" >Total QTY</div>',
                           width: "150px",
                           hidden: false
                       },
                       {
                           field: "QuoteSubTotal",
                           //title: "Total Amount",
                           template: function (dataItem) {
                                   return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.CurrencyFormat(dataItem.QuoteSubTotal) + "</div><span>" + HFCService.CurrencyFormat(dataItem.QuoteSubTotal) + "</span></span></span>";
                           },
                           headerTemplate: '<div kendo-tooltip k-content="TotalAmountToolTip" >Total Amount</div>',
                           attributes: { class: 'text-right' },
                           width: "150px",
                           hidden: false,
                          // format: "{0:c}"
                       },

                    {
                        field: "QuoteStatus",
                        title: "Status",
                        filterable: { multi: true, search: true },
                        template: function (dataItem) {
                            if (dataItem.QuoteStatus) {
                                return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.QuoteStatus + "</div><span>" + dataItem.QuoteStatus + "</span></span>";
                            } else {
                                return "";
                            }
                        },
                        width: "100px",
                        hidden: false

                    },
                    //{
                    //    field: "LastUpdatedOn",
                    //    title: "Modified",
                    //    hidden: true,
                    //    width: "100px",
                    //    template: "#= kendo.toString(kendo.parseDate(LastUpdatedOn, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                    //},
                    //{
                    //    field: "",
                    //    title: "",
                    //    width: "100px",
                    //    template: function (dataItem) {
                    //        return DropdownTemplate(dataItem);
                    //    }
                    //}

                    //<li><a href="javascript:void(0)" >Print</a></li> Remove Quote print there is no requirment on print and email
                      ],
                      pageable: {
                          refresh: true,
                          pageSize: 25,
                          resizable: true,
                          pageSizes: [25, 50, 100],
                          buttonCount: 5,
                          change: function (e) {
                              var myDiv = $('.k-grid-content.k-auto-scrollable');
                              myDiv[0].scrollTop = 0;
                          }
                      },
                      sortable: ({ field: "CreatedOn", dir: "desc" }),
                      filterable: {
                          extra: true,
                          operators: {
                              string: {
                                  contains: "Contains",
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  isempty: "Empty",
                                  isnotempty: "Not empty"


                              },
                              number: {
                                  eq: "Equal to",
                                  neq: "Not equal to",
                                  gte: "Greater than or equal to",
                                  lte: "Less than or equal to"
                              },
                              date: {
                                  gt: "After (Excludes)",
                                  lt: "Before (Includes)"
                              },
                          }
                      },
                      resizable: true,
                      //autoSync: true,
                      noRecords: { template: "No records found" },
                      scrollable: true,
                      toolbar: [
                         {
                             template: kendo.template($("#headToolbarTemplate").html()),
                         }],
                      columnResize: function (e) {
                          $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                          getUpdatedColumnList(e);
                      },
                      filterMenuInit: function (e) {
                         // $(e.container).find('.k-check-all').click();
                          e.container.find(".k-multicheck-wrap").css("max-height", ($('.k-grid-content.k-auto-scrollable').height() - 100));
                      },
                  };
                  if (!kendotooltipService.optimiseTriggerFlag) {
                      kendotooltipService.optimiseTriggerFlag = true;
                      $scope.QuoteList = kendotooltipService.setColumnWidth($scope.QuoteList);
                  }
              }

              var getUpdatedColumnList = function (e) {
                  var heightValue = $scope.wrapperHeightValue + "px";
                  kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
                  $(".k-grid-content.k-auto-scrollable").css("height", heightValue);
              }

              $scope.QuotegridSearchClear = function () {
                  $('#searchBox').val('');
                  $("#gridQuoteSearch").data('kendoGrid').dataSource.filter({
                  });
              }

              $scope.QuotegridSearch = function () {

                  var searchValue = $('#searchBox').val();
                  $("#gridQuoteSearch").data("kendoGrid").dataSource.filter({
                      logic: "or",
                      filters: [
                          {
                              field: "QuoteName",
                              operator: "contains",
                              value: searchValue
                          },
                        {
                            field: "OpportunityName",
                            operator: "contains",
                            value: searchValue
                        },
                        {
                            field: "AccountName",
                            operator: "contains",
                            value: searchValue
                        },
                        {
                            field: "QuoteStatus",
                            operator: "contains",
                            value: searchValue
                        },
                        {
                            field: "PrimaryQuote",
                            operator: (value) => (value + "").indexOf(searchValue) >= 0,
                            value: searchValue
                        },
                        {
                            field: "SalesAgent",
                            operator: "contains",
                            value: searchValue
                        },

                          {
                              field: "QuoteID",
                              operator: "eq",
                              value: searchValue
                          },
                          {
                              field: "NumberOfQuotelines",
                              operator: "eq",
                              value: searchValue
                          },
                          {
                              field: "QuoteSubTotal",
                              operator: "eq",
                              value: searchValue
                          },
                          {
                              field: "Quantity",
                              operator: "eq",
                              value: searchValue
                          },
                      ]
                  });

              }



              $scope.PrintQuotee = function (QuoteKey) {
                  //
                  $scope.PrintService.PrintQuotee(QuoteKey);
              }

              $scope.ActiveHelp = function () {
                  if ($scope.ActiveToolTips == true) {
                      $scope.ActiveToolTips = false;

                  }
                  else {
                      $scope.ActiveToolTips = true;

                  }
              }



              //on selecting dashboard drop-down value(Date/time drop-down)
              $scope.OnChangeDateRangeFilter = function (id) {
                  if (id === null || id === '' || id === undefined) {
                      $scope.DateRangeId = '';
                  } else {
                      $scope.DateRangeId = id;
                  }
                  if ($('#gridQuoteSearch').data('kendoGrid')) {
                      $('#gridQuoteSearch').data('kendoGrid').dataSource.read();
                      //$('#gridQuoteSearch').data('kendoGrid').refresh();                     
                      $scope.QuoteList = kendotooltipService.setColumnWidth($scope.QuoteList);
                  }
                  return;
              }

              //on clicking the checkboxes values to be passed
              $scope.FilterBySelectedQuote = function () {
                  
                  $scope.SelectedQuotesStatus = $scope.Valueselected();
                  $('#gridQuoteSearch').data('kendoGrid').dataSource.read();
                 // $('#gridQuoteSearch').data('kendoGrid').refresh();
                  $scope.QuoteList = kendotooltipService.setColumnWidth($scope.QuoteList);

                  //$anchorScroll();
                  var myDiv = $('.k-grid-content.k-auto-scrollable');
                  
                  myDiv[0].scrollTop = 0;

              }

    //on selecting dashboard checkbox primary Quote value
              $scope.FilterBySelectedPrimaryQuote = function () {
                  if ($('#PrimaryQuoteSelected').is(":checked")) 
                      $scope.SelectedPrimaryQuote = true;
                   else $scope.SelectedPrimaryQuote = false;

                  if ($('#gridQuoteSearch').data('kendoGrid')) {
                      $('#gridQuoteSearch').data('kendoGrid').dataSource.read();
                      $scope.QuoteList = kendotooltipService.setColumnWidth($scope.QuoteList);
                  }
                  return;
              }
    // QuoteList.Get(data);

              //$(window).resize(function () {
              //    var grid = $("#gridQuoteSearch").data("kendoGrid");
                
              //    if (!kendotooltipService.optimiseTriggerFlag) {
              //        kendotooltipService.optimiseTriggerFlag = true;
              //        QuoteList = kendotooltipService.setColumnWidth(QuoteList, true);
              //        grid.refresh();
              //    }
              //});
              //$scope.QuoteList = kendotooltipService.setColumnWidth($scope.QuoteList);
              //window.onresize = function () {
              //    $scope.QuoteList = kendotooltipService.setColumnWidth($scope.QuoteList);
              //};


              $scope.bringTooltipPopup = function()
              {
                  $(".statusCanvas").show();
              }
              //getting the check-box value
              $scope.Valueselected = function () {
                  var arr = [];
                  var result = '';
                  if ($('#Check1').is(":checked")) {
                      result = 1;
                      arr.push(result);
                  }
                  if ($('#Check2').is(":checked")) {
                      result = 2;
                      arr.push(result);
                  }
                  if ($('#Check3').is(":checked")) {
                      result = 3;
                      arr.push(result);
                  }
                  if ($('#Check4').is(":checked")) {
                      result = 4;
                      arr.push(result);
                  }
                  if ($('#Check5').is(":checked")) {
                      result = 5;
                      arr.push(result);
                  }

                  return arr;
              }
              //QuoteList.Get(data);
              //$scope.QuoteList = kendotooltipService.setColumnWidth($scope.QuoteList);
              //window.onresize = function () {
              //    $scope.QuoteList = kendotooltipService.setColumnWidth($scope.QuoteList);
              //};
              // Kendo Resizing
              $scope.$on("kendoWidgetCreated", function (e) {
                  $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 393);
                  window.onresize = function () {
                      $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 393);
                  };
              });
              // End Kendo Resizing

              // Excel downloAD
              $scope.DownloadQuote_Excel = function () {
                  var grid = $("#gridQuoteSearch").data("kendoGrid");
                  grid.saveAsExcel();
              }

          }
    ])