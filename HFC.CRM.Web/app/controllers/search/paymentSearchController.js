﻿
// TODO: rquired refactoring:

'use strict';
app
    .directive('dropDown', function () {
        return {
            link: function (scope, element, attr, ctrl) {
                var $el = $(element);

                $el.parent().on({
                    "click": function (e) {
                        if ($(e.currentTarget).hasClass("btn-group")) {
                            var obj = $(this);
                            obj.data("closable", false);
                            setTimeout(function () { obj.data("closable", true); }, 500);
                        } else
                            $(this).data("closable", true);
                    },
                    "hide.bs.dropdown": function (e) {
                        if ($(e.relatedTarget).hasClass("inner-dropdown"))
                            return true;
                        else
                            return $(this).data("closable");
                    }
                });
            }
        }
    })
    .filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
    .controller('paymentSearchController',
    [
        '$scope', 'HFCService', 'PaymentSearchService', '$routeParams', '$location', '$rootScope', 'saveCriteriaService',
        '$templateCache', '$sce', '$compile', 'FileprintService', 'PaymentService', 'CalendarService', 'NavbarService',
        'NoteServiceTP', 'PersonService', 'LeadAccountService', 'kendoService',
function ($scope, HFCService, PaymentSearchService, $routeParams, $location, $rootScope,
            saveCriteriaService, $templateCache, $sce, $compile, FileprintService, accountService,
            calendarService, NavbarService, NoteServiceTP, PersonService, LeadAccountService, kendoService) {

    $rootScope.enableSaveSearch = enableSaveSearch;

    $scope.NavbarService = NavbarService;


    var ul = $location.absUrl().split('#!')[1];
    if (ul.includes('accountPaymentSearch')) {
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesAccountsTab();
    }

    $scope.PaymentSearchService = PaymentSearchService;
    $scope.HFCService = HFCService;
    $scope.filter_CounterSelectedSalesAgent = 'Nothing selected';
    $scope.filter_CounterSelectedJobStatus = 'Nothing selected';
    $scope.selectedSales = null;
    $scope.selectedPaymentStatuses = null;
    $scope.selectedSources = null;
    $scope.SearchFilter = 'Auto_Detect';
    $scope.FileprintService = FileprintService;
    $scope.NoteServiceTP = NoteServiceTP;
    $scope.PersonService = PersonService;
    $scope.LeadAccountService = LeadAccountService;
    $scope.BrandId = $scope.HFCService.CurrentBrand;
    // Appointment
    HFCService.GetUrl();
    // for kendo tooltip
    $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
    kendoService.customTooltip = null;

    //for displaying payment popup
    $scope.ReversePaymentControl = {};
    $scope.bringReversePaymentModal = function (PaymentID, OrderID) {
        $scope.ReversePaymentControl.showPaymentModal(PaymentID, OrderID);
    }


    $scope.ChangeFilterSearch = function (val) {

        $scope.SearchFilter = val;
    }

    if ($scope.Account == null || $scope.Account == undefined) {
        $scope.Account = {};
        $scope.Account.AccountId = $routeParams.AccountId;
        var AccountId = $scope.Account.AccountId;
        if (AccountId > 0) {

            LeadAccountService.Get(AccountId, function (response) {
                $scope.appointAccount = response;

            });
        }
    }

    $scope.PaymentgridSearchClear = function () {
        $('#searchBox').val('');
        $("#gridpaymentSearch").data('kendoGrid').dataSource.filter({
        });
    }

    $scope.Permission = {};
    var Paymentpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Payment')

    $scope.Permission.AccountPayment = Paymentpermission.CanRead; //$scope.HFCService.GetPermissionTP('List Payments').CanAccess;
    //$scope.Permission.AddNotesAccount = HFCService.GetPermissionTP('Add Notes&Attachments-Account').CanAccess
    //$scope.Permission.AddEvent = $scope.HFCService.GetPermissionTP('Add Event').CanAccess;


    $scope.NewPaymentpage = function () {
        window.location.href = '/#!/Payment';
    }

    $scope.PaymentgridSearch = function () {
        var searchValue = $('#searchBox').val();
        $("#gridpaymentSearch").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
              //{
              //    field: "PaymentFullName",
              //    operator: "contains",
              //    value: searchValue
              //},
              //{
              //    field: "Status",
              //    operator: "contains",
              //    value: searchValue
              //},
              //{
              //    field: "Address1",
              //    operator: "contains",
              //    value: searchValue
              //}, 
            {
                field: "PaymentID",
                operator: "eq",
                value: searchValue
            },
            {
                field: "OpportunityName",
                operator: "contains",
                value: searchValue
            },
            {
                field: "Method",
                operator: "contains",
                value: searchValue
            },
            {
                field: "VerificationCheck",
                operator: "contains",
                value: searchValue
            },
            {
                field: "Memo",
                operator: "contains",
                value: searchValue
            },
            ]
        });

    }
    $scope.filter_SelectSalesAgent = function (id) {
        $scope.filter_CounterSelectedSalesAgent = id;
    }

    $scope.filter_SelectJobStatus = function (id) {
        $scope.filter_CounterSelectedJobStatus = id;
    }
    $scope.SortBy = 'createdonutc';
    $scope.SortIsDesc = true;
    $scope.ChangeSortPayment = function (sortBy) {
        if (sortBy) {
            if (sortBy != $scope.SortBy) {
                $scope.SortBy = sortBy;
                $scope.SortIsDesc = true;
            } else {
                $scope.SortIsDesc = !$scope.SortIsDesc;

            }
            $scope.ApplyFilter();
        }
    }

    $scope.GoToFirstPage = function () {
        $scope.PaymentSearchService.Pagination.page = 1;
        $scope.ApplyFilter();
    }

    $scope.GoToPreviousPage = function () {
        if ($scope.PaymentSearchService.Pagination.page > 1) {
            $scope.PaymentSearchService.Pagination.page = +($scope.PaymentSearchService.Pagination.page) - 1;
            $scope.ApplyFilter();
        }
    }

    $scope.GoToNextPage = function () {

        if ($scope.PaymentSearchService.Pagination.page == $scope.PaymentSearchService.Pagination.pageTotal) {
            return;
        }
        $scope.PaymentSearchService.Pagination.page = +($scope.PaymentSearchService.Pagination.page) + 1;
        $scope.ApplyFilter();
    }

    $scope.GoToLastPage = function () {
        $scope.PaymentSearchService.Pagination.page = $scope.PaymentSearchService.Pagination.pageTotal;
        $scope.ApplyFilter();
    }

    $scope.ChangePageSize = function () {
        $scope.ApplyFilter();
    }

    $scope.test = function () {
        return 'test';
    }

    $scope.Payment = null;
    $scope.ApplyFilter = function (isClickedFromUI) {


        var data = { PaymentByDirection: $scope.SortIsDesc ? "Desc" : "Asc", PaymentBy: $scope.SortBy };
        //if ($scope.selectedSales) {
        //    data.salesPersonIds = $scope.selectedSales;
        //}

        if (isClickedFromUI) {
            data.pageIndex = 1;
            $scope.PaymentSearchService.Pagination.page = 1;
        }

        if ($scope.selectedSales) {
            data.salesPersonIds = $scope.selectedSales;
        }

        if ($scope.selectedJobsStatuses) {
            data.jobStatusIds = $scope.selectedJobsStatuses;
        }

        if ($scope.selectedPaymentStatuses) {
            data.PaymentStatusIds = $scope.selectedPaymentStatuses;
        }


        if ($scope.selectedSources) {
            data.sourceIds = $scope.selectedSources;
        }

        if ($scope.invoiceStatusesSearch) {
            data.invoiceStatuses = $scope.invoiceStatusesSearch;
        }

        if ($scope.ForSearchstartDate)
            data.createdOnUtcStart = $scope.ForSearchstartDate;
        if ($scope.ForSearchendDate)
            data.createdOnUtcEnd = $scope.ForSearchendDate;

        data.searchTerm = $('#search-box').val();

        data.searchFilter = $('#search-type').find('.active').find('input').val();

        //if (data.searchFilter != 'Auto_Detect') {
        //    data.searchFilter = $scope.SearchFilter;
        //}

        if ($scope.SearchTerm) {
            data.searchTerm = $scope.SearchTerm;
        }

        data.pageIndex = $scope.PaymentSearchService.Pagination.page;
        data.pageSize = $scope.PaymentSearchService.Pagination.size;

        data.ForSearchstartDate = $scope.ForSearchstartDate;
        data.ForSearchendDate = $scope.ForSearchendDate;



        data.selectedDateText = $scope.selectedDateText;


        if ($routeParams.commercialType) {
            data.commercialType = $routeParams.commercialType;
        }

        if ($routeParams.isReportSearch) {
            data.isReportSearch = $routeParams.isReportSearch;
        }
        data.AccountId = $routeParams.AccountId;
        PaymentSearchService.Get(data);

    }

    $scope.Get = function (data) {

    }

    $scope.$on('$viewContentLoaded', function () {
        $location.replace(); //clear last history route
    });

    $scope.ConvertArrayToString = function (arr) {
        if (!arr) {
            return '';
        }
        if ($.isNumeric(arr)) {
            return arr;
        }
        return arr.join('-');
    }

    $scope.ConvertStringToArray = function (str) {
        var arr = [];
        if (str) {
            var st = str.split('-');
            for (var i = 0; i < st.length; i++) {
                arr.push(st[i]);
            }
        }
        return arr;
    }


    $scope.GetPaymentFullName = function (Payment) {
        var fullname = "";
        if (Payment.PrimPerson) {
            fullname = $.trim(Payment.PrimPerson.FirstName); //trim will convert null to empty string, great for our concatenation
            if (Payment.SecPerson && Payment.SecPerson.PersonId) {
                if (Payment.SecPerson.LastName && Payment.PrimPerson.LastName && Payment.SecPerson.LastName.toLowerCase() == Payment.PrimPerson.LastName.toLowerCase())
                    fullname += " & " + $.trim(Payment.SecPerson.FirstName) + " " + $.trim(Payment.PrimPerson.LastName);
                else
                    fullname += " " + $.trim(Payment.PrimPerson.LastName) + " & " + $.trim(Payment.SecPerson.FirstName) + " " + $.trim(Payment.SecPerson.LastName);
            } else
                fullname += " " + $.trim(Payment.PrimPerson.LastName);

            if (Payment.PrimPerson.CompanyName)
                fullname = "<b>" + Payment.PrimPerson.CompanyName + "</b>" + "<br/>" + fullname;
            else
                fullname = "<b>" + fullname + "</b>";
        }
        return fullname;
    }


    $scope.GetPaymentStatuses = function () {
        return LK_PaymentStatuses;
    };


    $scope.FormatPercent = function (num, maxPrecision) {
        return HFC.formatPercent(num, maxPrecision);
    }

    $scope.FormatCurrency = function (num, precision) {
        return HFC.formatCurrency(num, precision);
    };


    $scope.GetPaymentStatusName = function (Payment) {
        var status = HFC.Lookup.Get(LK_PaymentStatuses, Payment.PaymentStatusId), parent = null;
        if (status.ParentId)
            parent = HFC.Lookup.Get(LK_PaymentStatuses, status.ParentId);
        if (parent)
            return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
        else
            return status.Name;
    }

    $scope.GetPaymentStatusNameById = function (PaymentStatus) {
        var status = HFC.Lookup.Get(LK_PaymentStatuses, PaymentStatus.Id), parent = null;
        if (status.ParentId) {
            parent = HFC.Lookup.Get(LK_PaymentStatuses, status.ParentId);
        }
        if (parent)
            return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
        else
            return status.Name;
    }


    $scope.GetPaymentJobs = function (PaymentId) {
        if (!PaymentId) {
            return PaymentSearchService.Jobs;
        }
        var reults = [];
        for (var i = 0; i < PaymentSearchService.Jobs.length; i++) {
            if (PaymentSearchService.Jobs[i].PaymentId == PaymentId) {
                reults.push(PaymentSearchService.Jobs[i]);
            }
        }

        return reults;
    }

    $scope.ToggleJobs = function (model, jsEvent) {
        var reults = [];
        if (model.JobRowEnabled) {
            model.JobRowEnabled = false;
            $(jsEvent.currentTarget).find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
            $('[name="' + model.PaymentId + '"]').remove();
            return reults;
        }

        for (var i = PaymentSearchService.Jobs.length - 1; i >= 0; i--) {
            if (PaymentSearchService.Jobs[i].PaymentId == model.PaymentId) {
                PaymentSearchService.Jobs.splice(i, 1);
            }
        }

        if (reults.length != 0) {
            model.JobRowEnabled = !model.JobRowEnabled;
            $(jsEvent.currentTarget).find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
            //$(".row_" + model.PaymentId).toggleClass("active");
            //$(".row_job_" + model.PaymentId).toggle();
            return reults;
        }


        //PaymentSearchService.LoadJobs(model.PaymentId).then(function () {
        //    model.JobRowEnabled = !model.JobRowEnabled;
        //    $(jsEvent.currentTarget).find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
        //    var template = $templateCache.get('PaymentSearchSecondLevel.html');
        //    template = template.replace(/#PaymentId#/g, model.PaymentId);
        //    angular.element(document.getElementById('row_' + model.PaymentId)).after($compile(template)($scope));
        //});
    }

    $scope.DateRangeArray = ["All Date & Time", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year", "Last Year"];

    $scope.selectedDateText = "All Date & Time";

    $scope.$watch('$scope.ForSearchReport', function (newVal) {
        if (newVal) {
            $scope.selectedDateText = "All Date & Time";
            $scope.ForSearchstartDate = null;
            $scope.ForSearchendDate = null;
            $scope.startDate = null;
            $scope.endDate = null;
        }
    });
    $scope.$watch('startDate', function (newVal) {
        if (newVal != null) {
            $scope.ForSearchstartDate = newVal;
            if ($scope.endDate == null) {
                $scope.selectedDateText = newVal + ' to ';
            } else {
                $scope.selectedDateText = newVal + ' to ' + $scope.endDate;
            }
        }
    });

    $scope.$watch('endDate', function (newVal) {
        if (newVal != null) {
            $scope.ForSearchendDate = newVal;
            if ($scope.startDate == null) {
                $scope.selectedDateText = ' to ' + newVal;
            } else {
                $scope.selectedDateText = $scope.startDate + ' to ' + newVal;
            }
        }
    });

    $scope.endDate = null;
    $scope.startDate = null;
    $scope.ForSearchstartDate = null;
    $scope.ForSearchendDate = null;

    $scope.setDateRange = function (date) {
        var range = $scope.getDateRange(date);
        if (range.StartDate) {
            $scope.ForSearchstartDate = range.StartDate.toString('MM/dd/yyyy');
            //$scope.startDate = range.StartDate.toString('MM/dd/yyyy');
        } else {
            //$scope.startDate = null;
            $scope.ForSearchstartDate = null;
        }

        if (range.EndDate) {
            $scope.ForSearchendDate = range.EndDate.toString('MM/dd/yyyy');
        } else {
            $scope.ForSearchendDate = null;
        }

        $scope.selectedDateText = date;
    }

    $scope.getDateRange = function (range) {
        var endDate = new XDate(),
            startDate = new XDate();
        switch (range.toLowerCase()) {
            case "all date & time":
                startDate = null;
                endDate = null;
                break;
            case "this week":
                startDate.addDays(-startDate.getDay());
                endDate = startDate.clone().addDays(6);
                break;
            case "this month":
                startDate.setDate(1);
                endDate = startDate.clone().addMonths(1).addDays(-1);
                break;
            case "this year":
                startDate.setDate(1).setMonth(0);
                endDate = startDate.clone().addYears(1).addDays(-1);
                break;
            case "last week":
                startDate.addDays(-startDate.getDay() - 7);
                endDate = startDate.clone().addDays(6);
                break;
            case "last month":
                startDate.setDate(1).addMonths(-1);
                endDate = startDate.clone().addMonths(1).addDays(-1);
                break;
            case "last year":
                startDate.setDate(1).setMonth(0).addYears(-1);
                endDate.setDate(1).setMonth(0).addDays(-1);
                break;
            case "last 7 days":
                startDate.addDays(-7);
                break;
            case "last 30 days":
                startDate.addDays(-30);
                break;
            case "1st quarter":
                startDate.setDate(1).setMonth(0);
                endDate = startDate.clone().addMonths(3).addDays(-1);
                break;
            case "2nd quarter":
                startDate.setDate(1).setMonth(3);
                endDate = startDate.clone().addMonths(3).addDays(-1);
                break;
            case "3rd quarter":
                startDate.setDate(1).setMonth(6);
                endDate = startDate.clone().addMonths(3).addDays(-1);
                break;
            case "4th quarter":
                startDate.setDate(1).setMonth(9);
                endDate = startDate.clone().addMonths(3).addDays(-1);
                break;
            case "1st half":
                startDate.setDate(1).setMonth(0);
                endDate = startDate.clone().addMonths(6).addDays(-1);
                break;
            case "2nd half":
                startDate.setDate(1).setMonth(6);
                endDate = startDate.clone().addMonths(6).addDays(-1);
                break;
            default:
                startDate = null;
                endDate = null;
                break;
        }

        return { StartDate: startDate, EndDate: endDate };
    }

    $scope.GetURLParameter = function (sParam) {
        var sPageURL = window.location.search.substring(1);
        var data = $.parseParams(window.location.href);
        if (data) {
            return data[sParam];
        }
    };

    if ($routeParams.PaymentStatusIds) {
        if ($.isNumeric($routeParams.PaymentStatusIds)) {

            $scope.selectedPaymentStatuses = [];
            $scope.selectedPaymentStatuses.push(+$routeParams.PaymentStatusIds);
        } else {
            $scope.selectedPaymentStatuses = $scope.ConvertStringToArray($routeParams.PaymentStatusIds);
        }
    }



    if ($routeParams.selectedDateText) {
        $scope.selectedDateText = $routeParams.selectedDateText;
        $scope.ForSearchstartDate = $routeParams.createdOnUtcStart;
        $scope.ForSearchendDate = $routeParams.createdOnUtcEnd;
    } else {
        if ($routeParams.createdOnUtcStart) {
            $scope.ForSearchstartDate = $routeParams.createdOnUtcStart;
            $scope.startDate = $routeParams.createdOnUtcStart;
        }
        if ($routeParams.createdOnUtcEnd) {
            $scope.ForSearchendDate = $routeParams.createdOnUtcEnd;
            $scope.endDate = $routeParams.createdOnUtcEnd;
        }
    }

    if ($routeParams.searchTerm) {
        $scope.SearchTerm = $routeParams.searchTerm;
        if ($routeParams.searchFilter == "Phone_Number") {
            var phoneno = /^(?=.*?[1-9])[0-9()-. ]+$/;
            var inputtxt = $routeParams.searchTerm;
            if (!inputtxt.match(phoneno)) {
                HFC.DisplayAlert("Please verify the phone number you entered.");
                //clear the parameters to show empty result on error
                var data = {};
                PaymentSearchService.Get(data);
                return false;
            }
        }
    }


    if ($routeParams.searchFilter != 'undefined') {
        $rootScope.searchMode = $routeParams.searchFilter;
    } else {
        $routeParams.searchFilter = 'Auto_Detect';
        $rootScope.searchMode = $routeParams.searchFilter;
    }




    $scope.customersDataSource = {
        transport: {
            read: {
                dataType: "jsonp",
                url: "//demos.telerik.com/kendo-ui/service/Customers",
            }
        }
    };

    $scope.tooltipOptions = {
        filter: "td,th",
        position: "top",
        hide: function (e) {
            this.content.parent().css('visibility', 'hidden');
        },
        show: function (e) {
            if (this.content.text().length > 1) {
                if (this.content.text().trim() === "Edit QualifyAppointment Print") {
                    this.content.parent().css('visibility', 'hidden');
                } else {
                    this.content.parent().css('visibility', 'visible');
                }

            }
            else {
                this.content.parent().css('visibility', 'hidden');
            }
        },
        content: function (e) {

            return e.target.context.textContent;
        }
    };

    angular.element(document).ready(function () {
        $('#search-box').val($routeParams.searchTerm);

        if ($routeParams.searchFilter) {
            var lis = $('#search-type').find('li');
            lis.removeClass('active');
            for (var i = 0; i < lis.length; i++) {
                if ($(lis[i]).find('input').val() == $routeParams.searchFilter) {
                    $(lis[i]).addClass('active');
                }
            }

            if ($routeParams.searchFilter != 'undefined') {
                switch ($routeParams.searchFilter) {
                    case 'Customer_Name':
                        $rootScope.searchMode = 'Customer Name';
                        break;
                    case 'Number':
                        $rootScope.searchMode = 'Job/Invoice Number';
                        break;
                    case 'Phone_Number':
                        $rootScope.searchMode = 'Phone Number';
                        break;
                    default:
                        $rootScope.searchMode = $routeParams.searchFilter;
                }
            }
            else {
                $rootScope.searchMode = 'Auto_Detect';
                $scope.searchFilter = 'Auto_Detect';
                $routeParams.searchFilter = 'Auto_Detect';
            }

        }

        $scope.ApplyFilter();
    });

    saveCriteriaService.selectedJobsStatuses = $scope.selectedJobsStatuses;
    saveCriteriaService.selectedPaymentStatuses = $scope.selectedPaymentStatuses;
    saveCriteriaService.selectedSources = $scope.selectedSources;
    saveCriteriaService.selectedSales = $scope.selectedSales;
    saveCriteriaService.invoiceStatusesSearch = $scope.invoiceStatusesSearch;

    saveCriteriaService.selectedDateText = $scope.selectedDateText;
    saveCriteriaService.ForSearchstartDate = $scope.ForSearchstartDate;
    saveCriteriaService.ForSearchendDate = $scope.ForSearchendDate;
    saveCriteriaService.startDate = $scope.startDate;
    saveCriteriaService.endDate = $scope.endDate;

    $rootScope.$broadcast('applyMainSearchFilter');

    //$('head title').text("" + applicationTitle);
    HFCService.setHeaderTitle("Payment #");
}
    ])
