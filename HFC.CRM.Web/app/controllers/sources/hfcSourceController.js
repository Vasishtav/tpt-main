﻿/***************************************************************************\
Module Name:  sourceController .js - AngularJS file
Project: HFC
Created on: 21 June 2018 Thursday
Created By: Ramesh
Copyright:
Description: Source Controller
Change History:
Date  By  Description

\***************************************************************************/

appCP.controller('hfcSourceController', [
    '$http', '$scope', '$window', '$location', '$rootScope', 'HFCService', 'FranchiseService','NavbarServiceCP','AdminSourceChannelServiceTP',
    function ($http, $scope, $window, $location, $rootScope, HFCService, FranchiseService, NavbarServiceCP, AdminSourceChannelServiceTP) {

        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.NavbarServiceCP.SetupMarketInfoTab();
        $scope.AdminSourceChannelServiceTP = AdminSourceChannelServiceTP;

        $scope.FranchiseService = FranchiseService;
        $scope.HFCService = HFCService;
        HFCService.setHeaderTitle("HFC Source #");
        // for kendo tooltip
        //$scope.kendotooltipOptions = kendoService.kendoToolTipOptions;

        $scope.Permission = {};
        var Sourcepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Sources')
        if (Sourcepermission){
        $scope.Permission.Viewsource = Sourcepermission.CanRead;
        $scope.Permission.Addsource = Sourcepermission.CanCreate;
        $scope.Permission.Editsource = Sourcepermission.CanUpdate;
        }
        $scope.AddRow = function () {

            var grid = $("#gridSources").data("kendoGrid");

            grid.addRow();
            $scope.EditMode = true;
        }

        //click on add source button
        $scope.AddNewSource = function () {
             
            $scope.HFCSourcemap_form.$setPristine();
            var grids = $("#gridSources").data("kendoGrid");
            var rowInEditMode = $("#gridSources").find("tr.k-grid-edit-row");
            var itemBeingEdited = $("#gridSources").data("kendoGrid").dataSource.getByUid(rowInEditMode.data("uid"));
            $scope.AdminSourceChannelServiceTP.setEditableRow(itemBeingEdited);
            $scope.AdminSourceChannelServiceTP.setPageNumber(grids.dataSource.page());

            $scope.AdminSourceChannelServiceTP.setIcon(false);
            $window.location.href = "#!/editsourcelist/0";
        }

        //click on add channel button
        $scope.AddNewChannel = function () {
            $scope.HFCSourcemap_form.$setPristine();
            var grids = $("#gridSources").data("kendoGrid");

            var rowInEditMode = $("#gridSources").find("tr.k-grid-edit-row");
            var itemBeingEdited = $("#gridSources").data("kendoGrid").dataSource.getByUid(rowInEditMode.data("uid"));
            $scope.AdminSourceChannelServiceTP.setPageNumber(grids.dataSource.page());

            $scope.AdminSourceChannelServiceTP.setEditableRow(itemBeingEdited);
            $scope.AdminSourceChannelServiceTP.setIcon(false);
            $window.location.href = "#!/editchannellist/0";
        }

        $scope.RedirecttoTPTSource = function () {
             
            $window.location.href = "#!/edittpsources";
            $scope.AdminSourceChannelServiceTP.clearValue();
        }

        $scope.RedirecttoUTMCampaign = function () {
             
            $window.location.href = "#!/hfcleadsourcemap";
            $scope.AdminSourceChannelServiceTP.clearValue();
        }


        //grid row editable.
        $scope.setValueToGrid = function () {
             

            var sourcegrids = $("#gridSources").data("kendoGrid");

            var pagenumber = $scope.AdminSourceChannelServiceTP.getPageNumberValue();
            if (pagenumber) {
                sourcegrids.dataSource.page(pagenumber);
            }
            var editedrows = $scope.AdminSourceChannelServiceTP.getEditableRow();

            if (editedrows){
                var listrows = sourcegrids.dataSource._data;
                if (editedrows.id == null) {
                    var items = sourcegrids.dataSource.insert(0, editedrows);
                    offset = 0;
                }

                var val = sourcegrids.dataSource._data;

                var offset; var uid = "";

                for (var i = 0; i < listrows.length; i++) {
                        if (listrows[i]['SourcesHFCId'] == editedrows.SourcesHFCId)
                            offset = i + 1;
                            uid = listrows[i].uid;
                }

                var recordCount = sourcegrids.dataSource._pageSize;

                while (offset > recordCount) {
                    offset = offset - recordCount;
                }
           
                sourcegrids.editRow($("#gridSources tr:eq(" + offset + ")"));
                if (editedrows.id == null) {
                    var channellistDropdown = $("#Channeldropdown").data("kendoDropDownList");
                    var cd = channellistDropdown.dataSource.read();
                    var SourceListDropdown = $("#Sourcedropdown").data("kendoDropDownList");
                    var sd = SourceListDropdown.dataSource.read();
                }
            }
        }

        //populate the channel value
        $scope.ChannelListGet = function () {
             
            var channellistgrids = $("#Channeldropdown").data("kendoDropDownList");
            var Channellistvalue = channellistgrids.dataSource._data;
            var editchannel = $scope.AdminSourceChannelServiceTP.getChannelValue();
            var channel_id = ""; var offset = "";
            if (editchannel) {
                for (var i = 0; i < Channellistvalue.length; i++) {
                    if (Channellistvalue[i]['ChannelHFC'] == editchannel.Name) {
                        offset = i + 1;
                        channel_id = Channellistvalue[i].ChannelIdHFC;
                    }
                }
            }

            var Sourcehfc_Id = ""; var channelhfc_Id = ""; var channelhfc_val = "";
            if (editchannel){
                channellistgrids.select(offset);
                var erow = $scope.AdminSourceChannelServiceTP.getEditableRow();
                erow.ChannelIdHFC = channel_id;
                erow.ChannelHFC = editchannel.Name;
                var grids = $("#gridSources").data("kendoGrid");
                var listrow = grids.dataSource._data;
                for (i = 0; i < listrow.length; i++) {
                    if (listrow[i]['SourcesHFCId'] == erow.SourcesHFCId) {
                        channelhfc_Id = erow.ChannelIdHFC;
                        channelhfc_val = erow.ChannelHFC;
                        Sourcehfc_Id = erow.SourcesHFCId;
                    }
                }

                var data_Row = grids.dataSource.get(Sourcehfc_Id);
                data_Row.set("ChannelIdHFC", channelhfc_Id)
                data_Row.set("ChannelHFC", channelhfc_val)

                $scope.AdminSourceChannelServiceTP.clearValue();
            }
        }

        //populate the source value
        $scope.SourceListGet = function () {
             
            var sourcelistgrids = $("#Sourcedropdown").data("kendoDropDownList");
            var sourcelistvalue = sourcelistgrids.dataSource._data;
            
            var editedsource = $scope.AdminSourceChannelServiceTP.getSourceValue();
            var source_id = ""; var offset = "";
            if (editedsource) {
                for (var i = 0; i < sourcelistvalue.length; i++) {
                    if (sourcelistvalue[i]['SourceHFC'] == editedsource.Name) {
                        offset = i + 1;
                        source_id = sourcelistvalue[i].SourceIdHFC;
                    }
                        
                }
            }
            
            var Sourcehfc_Id = ""; var sources_Id = ""; var sources_val = "";
            if (editedsource) {
                sourcelistgrids.select(offset);
                var row = $scope.AdminSourceChannelServiceTP.getEditableRow();
                row.SourceIdHFC = source_id;
                row.SourceHFC = editedsource.Name;
                var grids = $("#gridSources").data("kendoGrid");
                var listrow = grids.dataSource._data;
                for (i = 0; i < listrow.length; i++) {
                    if (listrow[i]['SourcesHFCId'] == row.SourcesHFCId) {
                        sources_Id = row.SourceIdHFC;
                        sources_val = row.SourceHFC;
                        Sourcehfc_Id = row.SourcesHFCId;
                    }
                }

                var data_Row = grids.dataSource.get(Sourcehfc_Id);
                data_Row.set("SourceIdHFC", sources_Id)
                data_Row.set("SourceHFC", sources_val)
                
                $scope.AdminSourceChannelServiceTP.clearValue();
            }
                
        }

        $scope.$on('$routeChangeStart', function ($event, next, current) {
            // ... you could trigger something here ...
             
            if (next.$$route.originalPath.toUpperCase().includes('EDITCHANNELLIST') || next.$$route.originalPath.toUpperCase().includes('EDITSOURCELIST')) {

            }
            else {
                var value_edit = $("#gridSources").find("tr.k-grid-edit-row");
                if (value_edit.length > 0) {
                    $scope.HFCSourcemap_form.$setDirty(true);
                }

            }

        });

        $scope.campaignsByFranchise = function () {
            $scope.EditSourcesList = {
                dataSource: {
                    transport: {
                        read: {

                            url: '/api/Source/0/GetSourcesHFCmapTP',
                            complete: function () {
                                $scope.setValueToGrid();
                            }
                        },
                        update: {
                            url: '/api/Source/0/PostSourcesHFC',
                            type: "POST",
                            complete: function (e) {
                                $("#gridSources").data("kendoGrid").dataSource.read();
                                $("#gridSources").data("kendoGrid").refresh();
                                HFC.DisplaySuccess("Campaign Updated Successfully.");
                            }

                        },
                        create: {
                            url: '/api/Source/0/PostSourcesHFC',
                            type: "POST",
                            complete: function (e) {
                                 
                                $("#gridSources").data("kendoGrid").dataSource.read();
                                $("#gridSources").data("kendoGrid").refresh();
                                HFC.DisplaySuccess("New Campaign added sucessfully.");
                            }

                        },
                        dataType: "json",
                        parameterMap: function (options, operation) {
                            if (operation === "create") {
                                $scope.setpagepristine();
                                $scope.AdminSourceChannelServiceTP.clearValue();
                                return {
                                    SourcesHFCId: options.SourcesHFCId,
                                    SourcesTPId: options.SourcesTPId,
                                    OwnerId: options.OwnerIdHFC,
                                    ChannelId: options.ChannelIdHFC,
                                    SourceId: options.SourceIdHFC,
                                    IsActive: options.IsActive,
                                    AllowFranchisetoReassign: options.AllowFranchisetoReassign,
                                    IsDeleted: options.IsDeleted,
                                    LeadType: options.LeadType,
                                    LMDBSourceID: options.LMDBSourceID,
                                };
                            }
                            else if (operation === "update") {
                                $scope.setpagepristine();
                                $scope.AdminSourceChannelServiceTP.clearValue();
                                return {
                                    SourcesHFCId: options.SourcesHFCId,
                                    SourcesTPId: options.SourcesTPId,
                                    OwnerId: options.OwnerIdHFC,
                                    ChannelId: options.ChannelIdHFC,
                                    SourceId: options.SourceIdHFC,
                                    IsActive: options.IsActive,
                                    AllowFranchisetoReassign: options.AllowFranchisetoReassign,
                                    IsDeleted: options.IsDeleted,
                                    LeadType: options.LeadType,
                                    LMDBSourceID: options.LMDBSourceID,
                                };
                            }
                        }
                    },
                    error: function (e) {
                         
                        HFC.DisplayAlert(e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "SourcesHFCId",
                            fields: {
                                SourcesHFCId: { type: "number", editable: false, nullable: true },
                                
                                TPTSource: { editable: true, validation: { required: true } },
                                OwnerHFC: { editable: true },
                                ChannelHFC: { editable: true },
                                SourceHFC: { editable: true },
                                AllowFranchisetoReassign: { type: "boolean" },
                                IsActive: { type: "boolean" },
                                LeadType: { editable: true, validation: { required: false } },
                                LMDBSourceID:{type: "number",editable:true}
                            }
                        }
                    }


                },
                // https://www.telerik.com/forums/remove-page-if-no-records
                dataBound: function (e) {

                    $('.k-grid-add').click(function () {
                        $scope.setpagedirty();
                    })

                    $('.k-grid-edit').click(function () {
                        $scope.setpagedirty();
                    })

                    $("tr", "#gridSources").on("click", ".k-grid-cancel", function (e) {
                        $scope.setpagepristine();
                    });

                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    if (this.dataSource._data.length == 0) {
                        $scope.searchBoxEnable = true;
                    } else {
                        $scope.searchBoxEnable = false;
                    }
                    var searchValue = $('#searchBox').val();
                    if (searchValue != "") {
                        $('#btnReset').prop('disabled', false);
                        $('#btnAddrow').prop('disabled', true);
                        $('#btnAddSource').prop('disabled', true);
                        $('#btnAddChannel').prop('disabled', true);
                    }
                    else {
                        $('#btnReset').prop('disabled', true);
                        $('#btnAddrow').prop('disabled', false);
                        $('#btnAddSource').prop('disabled', false);
                        $('#btnAddChannel').prop('disabled', false);
                    }
                    if ($scope.Permission.Editsource == false) {
                        e.sender.element.find(".k-grid-edit").hide();
                        e.sender.element.find(".k-grid-add").hide();
                    }

                },
                resizable: true,
                columns: [
                    {
                        field: "SourcesHFCId",
                        title: "HFC SourceID",
                        hidden: true,
                        
                     },
                     {
                         field: "LMDBSourceID",
                         title: "HFC Source ID",
                         hidden: false,
                         width: "100px",
                         template: function (dataitem) {
                             if (dataitem.LMDBSourceID)
                                 return '<span class= "Grid_Textalign">' + dataitem.LMDBSourceID + '</span>'
                             else
                                 return "";
                         }
                     },

                            {
                                field: "TPTSource",
                                title: "TPTSource",
                                width: "250px",
                                hidden: false,
                                filterable: { multi: true, search: true },
                                editor: function (container, options) {
                                    $('<input required=true   name="TPTSource" data-bind="value:SourcesTPId" style="border: none "  />')
                                            .appendTo(container)
                                            .kendoDropDownList({
                                                filter: "contains",
                                                autoBind: false,
                                                optionLabel: "Select",
                                                valuePrimitive: true,
                                                dataTextField: "TPTSource",
                                                dataValueField: "SourcesTPId",
                                                template: "#=TPTSource #",
                                                dataSource: {
                                                    transport: {
                                                        read: {
                                                            url: '/api/Source/0/GetTPTSource',
                                                        }
                                                    },
                                                    sort: {
                                                        field: 'TPTSource',
                                                        dir: 'asc'
                                                    }
                                                }
                                            });
                                }
                            },

                            {
                                field: "OwnerHFC",
                                title: "OwnerHFC",
                                width: "180px",
                                hidden: false,
                                filterable: { multi: true, search: true },
                                editor: function (container, options) {
                                    $('<input required=true   name="OwnerHFC" data-bind="value:OwnerIdHFC" style="width: 100%;border: none "  />')
                                            .appendTo(container)
                                            .kendoDropDownList({
                                                autoBind: false,
                                                filter: "contains",
                                                optionLabel: "Select",
                                                valuePrimitive: true,
                                                dataTextField: "OwnerHFC",
                                                dataValueField: "OwnerIdHFC",
                                                template: "#=OwnerHFC #",
                                                dataSource: {
                                                    transport: {
                                                        read: {
                                                            url: '/api/Source/0/GetTPTOwnerSources',
                                                        }
                                                    },
                                                    sort: {
                                                        field: 'OwnerHFC',
                                                        dir: 'asc'
                                                    }
                                                }
                                            });
                                }
                            },
                              {
                                  field: "ChannelHFC",
                                  title: "ChannelHFC",
                                  width: "200px",
                                  hidden: false,
                                  filterable: {
                                      //multi: true,
                                      search: true
                                  },
                                  editor: function (container, options) {
                                      $('<input required=true   name="ChannelHFC" id="Channeldropdown" data-bind="value:ChannelIdHFC" data-filter="contains" style="width: 80%;border: none "  />')
                                              .appendTo(container)
                                              .kendoDropDownList({
                                                  filter: "contains",
                                                  autoBind: false,
                                                  optionLabel: "Select",
                                                  valuePrimitive: true,
                                                  dataTextField: "ChannelHFC",
                                                  dataValueField: "ChannelIdHFC",
                                                  template: "#=ChannelHFC #",
                                                  dataSource: {
                                                      transport: {
                                                          read: {

                                                              url: '/api/Source/0/GetTPTChannelSources',
                                                              complete: function () {
                                                                  $scope.ChannelListGet();
                                                              }
                                                          }
                                                      },
                                                      sort: {
                                                          field: 'ChannelHFC',
                                                          dir: 'asc'
                                                      }
                                                  }
                                              });
                                      $('<button type="button" ng-click="AddNewChannel()" class="plus_but tooltip-bottom" value="Add" ><i class="far fa-plus-square" style="vertical-align:sub;margin-left:10px;"></i></button>').appendTo(container);
                                  }
                              },
                              {
                                  field: "SourceHFC",
                                  title: "SourceHFC",
                                  width: "200px",
                                  hidden: false,
                                  filterable: {
                                      //multi: true,
                                      search: true
                                  },
                                  editor: function (container, options) {
                                      $('<input required=true   name="SourceHFC" id="Sourcedropdown" data-bind="value:SourceIdHFC" data-filter="contains" style="width:80%;border: none "  />')
                                              .appendTo(container)
                                              .kendoDropDownList({
                                                  autoBind: false,
                                                  optionLabel: "Select",
                                                  filter: "contains",
                                                  valuePrimitive: true,
                                                  dataTextField: "SourceHFC",
                                                  dataValueField: "SourceIdHFC",
                                                  template: "#=SourceHFC #",
                                                  dataSource: {
                                                      transport: {
                                                          read: {
                                                              url: '/api/Source/0/GetTPTSourceList',
                                                              complete: function () {
                                                                  $scope.SourceListGet();
                                                              }
                                                          }
                                                      },
                                                      sort: {
                                                          field: 'SourceHFC',
                                                          dir: 'asc'
                                                      }
                                                  }
                                              });
                                      $('<button type="button" ng-click="AddNewSource()" value="Add" class="plus_but tooltip-bottom"><i class="far fa-plus-square" style="vertical-align:sub;margin-left:10px;"></i></button>').appendTo(container);
                                  }
                              },
                               {
                                   field: "AllowFranchisetoReassign",
                                   title: "AllowFranchisetoReassign",
                                   filterable: { messages: { isTrue: "True", isFalse: "False" } },
                                   template: '<input type="checkbox" #= AllowFranchisetoReassign ? "checked=checked" : "" # class="option-input checkbox ng-pristine ng-valid ng-empty ng-touched" disabled="true"></input>',
                                   width: "100px",
                                   //template: " #if(AllowFranchisetoReassign){# AllowFranchisetoReassign #}else{# Not-AllowFranchisetoReassign#}# "
                               },
                              {
                                  field: "IsActive",
                                  title: "Status",
                                  template: " #if(IsActive){# Active #}else{# Inactive#}# ",
                                  width: "100px",
                                  filterable: { messages: { isTrue: "Active", isFalse: "Inactive" } }
                              },
                              //{
                              //    field: "LeadType",
                              //    title: "Lead Type",
                              //    editor: '<input name=" LeadType " data-bind="value:LeadType" style="width: 135px;"/>',
                              //},
                               {
                                   field: "LeadType",
                                   title: "Lead Type",
                                   width: "200px",
                                   hidden: false,
                                   filterable: {
                                       //multi: true,
                                       search: true
                                   },
                                   editor: function (container, options) {
                                       $('<input name="LeadType" data-bind="value:LeadType" style="width: 100%;border: none "  />')
                                               .appendTo(container)
                                               .kendoDropDownList({
                                                   autoBind: false,
                                                   optionLabel: "Select",
                                                   filter: "contains",
                                                   valuePrimitive: true,
                                                   dataTextField: "Value",
                                                   dataValueField: "Key",
                                                   template: "#=Key #",
                                                   dataSource: {
                                                       transport: {
                                                           read: {
                                                               url: '/api/Source/0/SourceLeadType',
                                                           }
                                                       },
                                                   }
                                               });
                                   }
                               },
                    { command: ["edit"], title: "&nbsp;", width: "100px" }

                ],
                editable: "inline",
                width: "100px",

                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                noRecords: { template: "No records found" },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                        $scope.AdminSourceChannelServiceTP.clearValue();
                        $scope.HFCSourcemap_form.$setPristine();
                        if ($scope.setvalue == "on") {

                        }
                        else {
                            $("#gridSources").data("kendoGrid").dataSource.read();
                            $("#gridSources").data("kendoGrid").refresh();
                        }
                        
                    }
                },
                toolbar: [
                      {
                          template: kendo.template($("#headToolbarTemplate").html()),
                      }]

            };

        };

        $scope.campaignsByFranchise();

        //added the code for the showinactive checkbox.
        $scope.IsSuspendSelect = function () {
            kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
            $scope.setvalue = $('input[name="SourceCheck"]:checked').val();
            if ($scope.setvalue == "on") {

                $http.get('/api/Source/0/GetInactiveHFCSources').then(function (response) {
                    var data = response.data;
                    var grid = $("#gridSources").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                }).catch(function (e) {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                });
            }
            else if ($scope.setvalue == undefined) {
                $http.get('/api/Source/0/GetSourcesHFCmapTP').then(function (response) {
                    var data = response.data;
                    var grid = $("#gridSources").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                }).catch(function (e) {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                });
            }
            else
            {
                kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
            }
        }

        $scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                validate: function (e) {
                    $("span.k-invalid-msg").hide();
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find(".k-widget.k-dropdown.k-header");
                        
                        if (input.size() > 0) {
                            $(this).addClass("dropdown-validation-error");
                        } else {
                            $(this).removeClass("dropdown-validation-error");
                        }
                    });
                }
            }).getKendoValidator();
        }

        $scope.HfcSource = function () {
            var validator = $scope.kendoValidator("gridSources");
            if (validator.validate()) {
                var searchValue = $('#searchBox').val();

                $("#gridSources").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [

                      {
                          field: "TPTSource",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "OwnerHFC",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "ChannelHFC",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "SourceHFC",
                          operator: "contains",
                          value: searchValue
                      }

                    ]
                });
            }
            else {
                $('#searchBox').val('');
            }

        }
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
            };
        });
        // End Kendo Resizing
        $scope.HfcSourceSearchClear = function () {
            $('#searchBox').val('');
            var searchValue = $('#searchBox').val('');
            if (searchValue == '') {
                $('#btnReset').prop('disabled', true);
                $('#btnAddrow').prop('disabled', false);
                $('#btnAddSource').prop('disabled', false);
                $('#btnAddChannel').prop('disabled', false);
            }
            else {
                $('#btnReset').prop('disabled', false);
                $('#btnAddrow').prop('disabled', true);
                $('#btnAddSource').prop('disabled', true);
                $('#btnAddChannel').prop('disabled', true);
            }
            $("#gridSources").data('kendoGrid').dataSource.filter({
            });
        }

        $scope.setpagedirty = function () {
            
            $scope.HFCSourcemap_form.$setDirty();
        }

        $scope.setpagepristine = function () {
            
            $scope.HFCSourcemap_form.$setPristine();
        }
    }
]);