﻿

appCP.controller('hfcLeadsourcemapController', [
    '$http', '$scope', '$window', '$location', '$rootScope','HFCService','FranchiseService','NavbarServiceCP',
    function ($http, $scope, $window, $location, $rootScope, HFCService, FranchiseService, NavbarServiceCP) {

        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.NavbarServiceCP.SetupMarketInfoTab();
       
        $scope.isLoadData = false;
        $scope.treeData = [];
        $scope.editableCampaign = {};
        $scope.editableSource = {};
        $scope.permissions = {};

        $scope.sources = [];
        $scope.rootSources = [];
        $scope.newSource = {};
       
        HFCService.setHeaderTitle("UTM-Campaign #");
        $scope.FranchiseService = FranchiseService;
        $scope.HFCService = HFCService;
        var offsetvalue = 0;
        $scope.Permission = {};
        var Campaignpermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Campaigns')
        if (Campaignpermission){
        $scope.Permission.ViewCampaign = Campaignpermission.CanRead;
        $scope.Permission.AddCampaign = Campaignpermission.CanCreate;
        $scope.Permission.EditCampaign = Campaignpermission.CanUpdate;
        $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;        }

        //for date validation
        $scope.InvalidMessage = "";
        $scope.ValidStartDate = true;
        $scope.ValidEndDate = true;

        // for kendo tooltip
        //$scope.kendotooltipOptions = kendoService.kendoToolTipOptions;


        $scope.AddRow = function () {            
            var grid = $("#gridleadsourcemap").data("kendoGrid");
            var editedRow = $("#gridleadsourcemap").find("tr.k-grid-edit-row");
            grid.addRow();
            $scope.EditMode = true;
            if (!editedRow || editedRow.length == 0) {
                setTimeout(function () {
                    $(".k-grid-update").click(function () {
                        $scope.enableRequiredFieldAlert();
                        $scope.invalidDateMessage();
                    });
                    $(".source-dropdown .k-input").addClass("required-text");
                }, 300);
            } else {
                $scope.enableRequiredFieldAlert();
            }
        }
       

        $scope.Id = null;
       
        $scope.IsActive = true;
        $scope.CampaignIsDeleted = false;
        $scope.EndDate = null;
        $scope.StartDate = null;
        

    $scope.campaignsByFranchise = function () {
            $scope.LeadSourcemap = {
                dataSource: {
                    transport: {
                        read: {

                            url: '/api/Source/0/GetLeadSourceMapping',

                        },
                        update: {
                            url: '/api/Source/0/PostUTMCampaign',
                            type: "POST",
                            complete: function (e) {
                                $("#gridleadsourcemap").data("kendoGrid").dataSource.read();
                                HFC.DisplaySuccess("Campaign Updated Successfully.");
                            }

                        },
                        create: {
                            url: '/api/Source/0/PostUTMCampaign',
                            type: "POST",
                            complete: function (e) {
                                $("#gridleadsourcemap").data("kendoGrid").dataSource.read();
                                HFC.DisplaySuccess("New Campaign added sucessfully.");
                            }

                        },
                        parameterMap: function (options, operation) {
                            if (operation === "create") {
                                $scope.setpagepristine();
                                return {
                                    Id: options.Id,
                                    UTMCode:options.UTMCode,
                                    Name: options.Campaign,
                                    StartDate: options.StartDate != null ? options.StartDate.toLocaleString() : "",
                                    EndDate: options.EndDate != null ? options.EndDate.toLocaleString() : "",
                                    Amount: options.Amount,
                                    Memo: options.Memo,
                                    IsActive: options.IsActive,
                                    SourcesHFCId: options.SourcesHFCId
                                    //SourcesHFCId: options.SourcesHFCId.SourcesHFCId
                                };
                            }
                            else if (operation === "update") {
                                $scope.setpagepristine();
                                return {
                                    Id: options.Id,
                                    Name: options.Campaign,
                                    StartDate: options.StartDate != null ? options.StartDate.toLocaleString() : "",
                                    EndDate: options.EndDate != null ? options.EndDate.toLocaleString() : "",
                                    Amount: options.Amount,
                                    Memo: options.Memo,
                                    IsActive: options.IsActive,
                                    SourcesHFCId: options.SourcesHFCId,
                                    UTMCode:options.UTMCode
                                };
                            }
                        }
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false, nullable: true },
                                UTMCode: { editable: true, validation: { required: true } },
                                StartDate: {
                                    type: "date", nullable: true, validation: {
                                        "class": "kendo-date-picker-date", "id": "AdminLeadSourceStartDate",
                                        "ng-model": "StartDate",
                                        "ng-change": "validateStart(StartDate)",
                                        "maxlength": 10,
                                        "ng-keypress": "restrictYear($event,'AdminLeadSourceStartDate')",
                                    }
                                },
                                SourcesHFCId: { editable: true ,required:true},
                                CampaignIsDeleted: { type: "boolean" },
                                IsActive: { type: "boolean" },
                                Memo: { type: "string" },
                                Amount: { type: "number" },
                                HFCSourceName: { editable: true, visable: true },
                                EndDate: {

                                    type: "date", nullable: true, validation: {
                                        "class": "kendo-date-picker-date","id":"AdminLeadSourceEndDate",
                                        validationMessage: "End date cannot be before start date.",
                                        "ng-model": "EndDate",
                                        "ng-change": "validateEnd(EndDate)",
                                        "maxlength": 10,
                                        "ng-keypress": "restrictYear($event,'AdminLeadSourceEndDate')",
                                        min: 1,
                                        campaignValidation: function (input) {
                                            var row = input.closest("tr");
                                            var grid = row.closest("[data-role=grid]").data("kendoGrid");
                                            var dataItem = grid.dataItem(row);

                                            if (input.is("[name='StartDate']")) {

                                                if (Date.parse(dataItem.EndDate) < Date.parse(input.val())) {
                                                    if (HFCService.ValidateDate(input.val(), "date") && !HFCService.ValidateDate($("#AdminLeadSourceEndDate").val(), "date")) {
                                                        $("#AdminLeadSourceStartDate").removeClass("gridinvalid_Date");
                                                        return true;
                                                    }

                                                    var a = $('#AdminLeadSourceStartDate').siblings();
                                                    $(a[0]).addClass("icon_Background");
                                                    return false;
                                                }
                                                else if (!HFCService.ValidateDate(input.val(), "date") && input.val() != "")
                                                    return false;
                                            }
                                            if (input.is("[name='EndDate']")) {

                                                if (Date.parse(input.val()) < Date.parse(dataItem.StartDate)) {
                                                    if (HFCService.ValidateDate(input.val(), "date") && !HFCService.ValidateDate($("#AdminLeadSourceStartDate").val(), "date")) {
                                                        $("#AdminLeadSourceEndDate").removeClass("gridinvalid_Date");
                                                        return true;
                                                    }

                                                    var a = $('#AdminLeadSourceEndDate').siblings();
                                                    $(a[0]).addClass("icon_Background");
                                                    return false;
                                                }
                                                else if (!HFCService.ValidateDate(input.val(), "date") && input.val() != "")
                                                    return false;
                                            }
                                            return true;
                                        }
                                    }
                                },


                            }
                        }
                    }


                },
                // https://www.telerik.com/forums/remove-page-if-no-records
                dataBound: function (e) {

                    $('.k-grid-add').click(function () {
                        $scope.setpagedirty();
                    })

                    $('.k-grid-edit').click(function () {                        
                        $scope.ValidStartDate = true;
                        $scope.ValidEndDate = true;
                        $scope.setpagedirty();
                        setTimeout(function () {
                            var StartBeforeEdit = date = $("#AdminLeadSourceStartDate").val();
                            var EndBeforeEdit = date = $("#AdminLeadSourceEndDate").val();
                            $(".k-grid-update").click(function () {
                                var StartAfterEdit = date = $("#AdminLeadSourceStartDate").val();
                                var EndAfterEdit = date = $("#AdminLeadSourceEndDate").val();
                                if (StartBeforeEdit != StartAfterEdit || EndBeforeEdit != EndAfterEdit) {
                                    var editedRow = $("#gridleadsourcemap").find("tr.k-grid-edit-row");
                                    var dataItem = $("#gridleadsourcemap").data("kendoGrid").dataItem(editedRow);
                                    if (StartAfterEdit == "") dataItem.StartDate = null;
                                    if (EndAfterEdit == "") dataItem.EndDate = null;
                                    dataItem.dirty = true;
                                }
                                $scope.enableRequiredFieldAlert();
                                $(".source-dropdown .k-input").removeClass("required-text");
                                $scope.invalidDateMessage();
                            });
                        }, 300);
                    })

                    $("tr", "#gridleadsourcemap").on("click", ".k-grid-cancel", function (e) {
                        $scope.setpagepristine();
                        $scope.ValidStartDate = true;
                        $scope.ValidEndDate = true;
                    });

                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    var searchValue = $('#searchBox').val();
                    
                    if (searchValue != "") {
                        $('#btnReset').prop('disabled', false);
                        $('#btnAddrow').prop('disabled', true);

                    }
                    else {
                        $('#btnReset').prop('disabled', true);
                        $('#btnAddrow').prop('disabled', false);

                    }
                    if ($scope.Permission.EditCampaign == false) {
                        e.sender.element.find(".k-grid-edit").hide();
                        e.sender.element.find(".k-grid-add").hide();
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "UTMCode",
                        title: "UTM Code",
                        width: "150px",
                        hidden: false,
                    },
                    
                            {
                                field: "HFCSourceName",
                                title: "HFC SourceName",
                                width: "200px",
                                //editor: SourceDropDownEditor,
                                hidden: false,
                                filterable: { multi: true, search: true },
                                editor: function (container, options) {
                                    $('<input required=true   name="HFCSourceName" class="source-dropdown" id="HFCSourceName" data-bind="value:SourcesHFCId" style="border: none "  />')
                                            .appendTo(container)
                                            .kendoDropDownList({
                                                autoBind: true,
                                                optionLabel: "Select",
                                                filter: "contains",
                                                valuePrimitive: true,
                                                dataTextField: "HFCSourceName",
                                                dataValueField: "SourcesHFCId",
                                                template: "#=HFCSourceName #",
                                                change: function (e) {
                                                    $(".k-widget.k-dropdown.k-header").css("border", "1px solid #ceced2 !important");
                                                    $(".source-dropdown .k-input").removeClass("required-text");
                                                },
                                                select: function (e) {
                                                    if (e.dataItem.HFCSourceName === "Select") {
                                                        $scope.preventClose = true;
                                                        e.preventDefault();
                                                    }
                                                },
                                                close: function (e) {
                                                    if ($scope.preventClose == true)
                                                        e.preventDefault();

                                                    $scope.preventClose = false;
                                                },
                                                dataSource: {
                                                    transport: {
                                                        read: {
                                                            
                                                            url: '/api/lookup/0/GetAllDropdownValues',
                                                        }
                                                    },
                                                    sort: {
                                                        field: 'HFCSourceName',
                                                        dir: 'asc'
                                                    }
                                                }

                                            });
                                        }


                            },
                             
                            {
                                field: "StartDate",
                                title: "Start Date",
                                width: "150px",
                                type: "date",
                                
                                filterable: HFCService.gridfilterableDate("date", "AdminSourceStartDateFilterCalendar", offsetvalue),
                                template: function (dataitem) {
                                    if (dataitem.StartDate == null)
                                        return "";
                                    else
                                        return HFCService.KendoDateFormat(dataitem.StartDate);
                                }
                                    //"#=  (StartDate == null)? '' : kendo.toString(kendo.parseDate(StartDate, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                                


                            },
                              {
                                  field: "EndDate",
                                  title: "End Date",
                                  width: "150px",
                                  type: "date",
                                  filterable: HFCService.gridfilterableDate("date", "AdminSourceEndDateFilterCalendar", offsetvalue),
                                  template: function (dataitem) {
                                      if (dataitem.EndDate == null)
                                          return "";
                                      else
                                        return HFCService.KendoDateFormat(dataitem.EndDate);
                                  }
                                      //"#=  (EndDate == null)? '' : kendo.toString(kendo.parseDate(EndDate, 'yyyy-MM-dd'), 'M/d/yyyy') #"
                                  //hidden: true


                              },
                              {
                                  field: "Amount",
                                  title: "Amount",
                                  width: "100px",
                                  attributes: { class: 'text-right' },
                                  template: function (dataItem) {
                                          return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                                  },
                              },
                              {
                                  field: "Memo",
                                  title: "Memo",
                                  width: "100px",
                              },
                              {
                                  field: "IsActive",
                                  title: "Status",
                                  width: "100px",
                                  template: " #if(IsActive){# Active #}else{# Inactive#}# ",
                                  filterable: { messages: { isTrue: "Active", isFalse: "Inactive" } }
                              },
                    { command: ["edit"], title: "&nbsp;", width: "100px" }

                ],
                editable: "inline",
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                noRecords: { template: "No records found" },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                },
                sortable: ({ field: "StartDate", dir: "desc" }),
                toolbar: [
                      {
                          template: kendo.template($("#headToolbarTemplate").html()),
                      }]
                //toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search" style="padding:0 5px !important;"><input ng-keyup="UTMgridSearch()" type="search" id="searchBox" placeholder="Search Campaigns" class="k-textbox leadsearch_tbox"><input type="button" id="btnReset" ng-click="UTMgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"><div class="productsearch_checkbox"><label><input class="option-input" name="SourceCheck" style="margin-right:5px;" type="checkbox" ng-click="GetallSource()">Show Inactive</label></div></script>').html()) }, { name: "create", text: "Add New UTM-Campaign" }],

            };

        };

        $scope.campaignsByFranchise();

        //added the code for the showinactive checkbox.
        $scope.GetallSource = function () {
            var setvalue = $('input[name="SourceCheck"]:checked').val();
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            if (setvalue == "on") {
             $http.get('/api/Source/0/GetUTMMapping').then(function (response) {
                    var data = response.data;
                    var grid = $("#gridleadsourcemap").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                });
                
            }
            else if (setvalue == undefined) {
                $http.get('/api/Source/0/GetLeadSourceMapping').then(function (response) {
                    var data = response.data;
                    var grid = $("#gridleadsourcemap").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                });
            }
        }


      
        $scope.UTMgridSearchClear = function () {
            $('#searchBox').val('');
            var searchValue = $('#searchBox').val('');
            if (searchValue == '') {
                $('#btnReset').prop('disabled', true);
                $('#btnAddrow').prop('disabled', false);
            }
            else {
                $('#btnReset').prop('disabled', false);
                $('#btnAddrow').prop('disabled', true);
            }
            $("#gridleadsourcemap").data('kendoGrid').dataSource.filter({
            });
        }

        $scope.UTMgridSearch = function () {
            var validator = $scope.kendoValidator("gridleadsourcemap");
            if (validator.validate()) {
                var searchValue = $('#searchBox').val();
                $("#gridleadsourcemap").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                      {
                          field: "UTMCode",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "HFCSourceName",
                          operator: "contains",
                          value: searchValue
                      },
                      {
                          field: "Amount",
                          operator: "eq",
                          value: searchValue
                      },
                       {
                           field: "Memo",
                           operator: "contains",
                           value: searchValue
                       }

                    ]
                });
            }
            else {
                $('#searchBox').val('');
            }
            

        }
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
            window.onresize = function () {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
            };
        });
        // End Kendo Resizing
        $scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                validate: function (e) {
                    $("span.k-invalid-msg").hide();
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find(".k-widget.k-dropdown.k-header");
                        
                        if (input.size() > 0) {
                            $(this).addClass("dropdown-validation-error");
                        } else {
                            $(this).removeClass("dropdown-validation-error");
                        }
                    });
                }
            }).getKendoValidator();
        }
        

        $scope.setpagedirty = function () {
            
            $scope.LeadSourcemap_form.$setDirty();
        }

        $scope.setpagepristine = function () {
            
            $scope.LeadSourcemap_form.$setPristine();
        }

        

        $scope.EditSource = function (obj) {

            angular.copy(obj, $scope.editableSource);

            obj.inEdit = true;
        }

        $scope.CancelCampaign = function () {

            $scope.editableCampaign = {};
        }

        $scope.CancelSource = function (obj) {

            obj.Name = $scope.editableSource.Name;
            obj.inEdit = false;
            $scope.editableSource = {};
        }
        

       

        

        function customBoolEditor(container, options) {
            var guid = kendo.guid();
            $('<input class="option-input" id="' + guid + '" type="checkbox" name="Discontinued" data-type="boolean" data-bind="checked:Discontinued">').appendTo(container);
            $('<label class="k-checkbox-label" for="' + guid + '">​</label>').appendTo(container);
        };

        //for validating start date
        $scope.validateStart = function (value) {
            if (value == "") {
                $scope.ValidStartDate = true;
                $("#AdminLeadSourceStartDate").removeClass("k-invalid");
                $("#AdminLeadSourceStartDate").addClass("k-valid");
            }
            else
                $scope.ValidStartDate = HFCService.ValidateDate(value, "date");

            $scope.StartDate = $("#AdminLeadSourceStartDate").val();
            $scope.EndDate = $("#AdminLeadSourceEndDate").val();
            var start_validDate = HFCService.ValidateDate($scope.EndDate, "date");
            if ($scope.ValidStartDate) {
                if (start_validDate) {
                    if (Date.parse($scope.EndDate) < Date.parse($scope.StartDate)) {
                        $scope.ValidStartDate = false;
                        $scope.InvalidMessage = "Start date cannot be before End date.";
                    }
                    else
                        $scope.ValidEndDate = true;
                }
                else
                    $scope.InvalidMessage = "Invalid End date!";
            }
            else {
                if ($scope.EndDate == "")
                    $scope.InvalidMessage = "Invalid Start date!";
                else if (start_validDate) {
                    $("#AdminLeadSourceEndDate").removeClass("gridinvalid_Date");
                    $("#AdminLeadSourceEndDate").removeClass("k-invalid");
                    $("#AdminLeadSourceEndDate").addClass("k-valid");
                    var a = $('#AdminLeadSourceEndDate').siblings();
                    $(a[0]).removeClass("icon_Background");
                    $scope.InvalidMessage = "Invalid Start date!";
                }
                else
                    $scope.InvalidMessage = "Invalid date!";

            }

            if ($scope.ValidStartDate) {
                $("#AdminLeadSourceStartDate").removeClass("gridinvalid_Date");
                $("#AdminLeadSourceStartDate").removeClass("k-invalid");
                $("#AdminLeadSourceStartDate").addClass("k-valid");
                var a = $("#AdminLeadSourceStartDate").siblings();
                $(a[0]).removeClass("icon_Background");
            }
            else {
                $("#AdminLeadSourceStartDate").addClass("gridinvalid_Date");
                var a = $("#AdminLeadSourceStartDate").siblings();
                $(a[0]).addClass("icon_Background");
            }
        }
        //for validating end date
        $scope.validateEnd = function (value) {
            if (value == "") {
                $scope.ValidEndDate = true;
                $("#AdminLeadSourceEndDate").removeClass("k-invalid");
                $("#AdminLeadSourceEndDate").addClass("k-valid");
            }
            else
                $scope.ValidEndDate = HFCService.ValidateDate(value, "date");

            $scope.StartDate = $("#AdminLeadSourceStartDate").val();
            $scope.EndDate = $("#AdminLeadSourceEndDate").val();
            var end_validDate = HFCService.ValidateDate($scope.StartDate, "date");
            if ($scope.ValidEndDate) {
                if (end_validDate) {
                    if (Date.parse($scope.EndDate) < Date.parse($scope.StartDate)) {
                        $scope.ValidEndDate = false;
                        $scope.InvalidMessage = "End date cannot be before start date.";
                    }
                    else
                        $scope.ValidStartDate = true;
                }
                else
                    $scope.InvalidMessage = "Invalid Start date!";
            }
            else {
                if ($scope.StartDate == "")
                    $scope.InvalidMessage = "Invalid End date!";
                else if (end_validDate) {
                    $("#AdminLeadSourceStartDate").removeClass("gridinvalid_Date");
                    $("#AdminLeadSourceStartDate").removeClass("k-invalid");
                    $("#AdminLeadSourceStartDate").addClass("k-valid");
                    var a = $('#AdminLeadSourceStartDate').siblings();
                    $(a[0]).removeClass("icon_Background");
                    $scope.InvalidMessage = "Invalid End date!";
                }
                else
                    $scope.InvalidMessage = "Invalid date!";
            }

            if ($scope.ValidEndDate) {
                $("#AdminLeadSourceEndDate").removeClass("gridinvalid_Date");
                $("#AdminLeadSourceEndDate").removeClass("k-invalid");
                $("#AdminLeadSourceEndDate").addClass("k-valid");
                var a = $("#AdminLeadSourceEndDate").siblings();
                $(a[0]).removeClass("icon_Background");
            }
            else {
                $("#AdminLeadSourceEndDate").addClass("gridinvalid_Date");
                var a = $("#AdminLeadSourceEndDate").siblings();
                $(a[0]).addClass("icon_Background");
            }
        }
        $scope.invalidDateMessage = function () {
            if (!$scope.ValidStartDate || !$scope.ValidEndDate) {
                HFC.DisplayAlert($scope.InvalidMessage);
            }
        }
        $scope.restrictYear = function (keyEvent, id) {
            var date = $("#" + id + "").val();
            var vals = date.split('/');
            var year = vals[2];
            if (year && year.length == 4 && Date.parse(date))
                keyEvent.preventDefault();
        }
        // enable the red color border for required field
        $scope.enableRequiredFieldAlert = function () {
            var element = $("#HFCSourceName");
            var selectedValue = $("#HFCSourceName").data("kendoDropDownList").select();
            if ((element && element.length != 0) && selectedValue == 0) {
                var parentElement = $("#HFCSourceName").parent();
                $(parentElement[0]).addClass("required-field");
            } else if ((element && element.length != 0) && selectedValue != 0) {
                var parentElement = $("#HFCSourceName").parent();
                $(parentElement[0]).removeClass("required-field");
            }
        }
    }
]);









