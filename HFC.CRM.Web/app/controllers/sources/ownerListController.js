﻿/***************************************************************************\
Module Name:  ownerListController.js - AngularJS file
Project: HFC
Created on: 28 June 2018 Thursday
Created By: Ramesh
Copyright:
Description: Owner Controller
Change History:
Date  By  Description

\***************************************************************************/

appCP.controller('ownerListController', [
    '$scope', '$route', '$routeParams', '$rootScope', '$http', '$window',
    '$location', '$modal', 'HFCService','FranchiseService','NavbarServiceCP',
function (
    $scope, $route, $routeParams, $rootScope, $http, $window,
    $location, $modal, HFCService, FranchiseService, NavbarServiceCP) {
    $scope.NavbarServiceCP = NavbarServiceCP;
    $scope.NavbarServiceCP.SetupMarketInfoTab();
     

    $scope.FranchiseService = FranchiseService;

    var unOrgList = [];
    var list = [];
    $scope.ShowInActive = false;

    $scope.EditOwnerList = {
        dataSource: {
            transport: {
                read: function (e) {
                    list = [];
                    $http.get("/api/lookup/0/GetSourcePickList").then(function (data) {
                         
                        angular.forEach(data.data.Owner, function (value, key) {
                            if ($scope.ShowInActive || value.IsActive)
                                list.push(value);
                        });
                         
                        e.success(list);
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
                }
            },
            error: function (e) {
                //HFC.DisplayAlert(e.errorThrown);
            },
        },
        schema: {
            model: {
                id: "OwnerID",
                fields: {
                    Name: { editable: true, validation: { required: true } },
                    IsActive: { type: "boolean", editable: false },
                }
            }
        },
        columns: [
            {
                field: "Name",
                title: "Owner",
                editor: '<input  required=true name=" Name " data-bind="value:Name" style="width: 230px;border: none "/>',
                filterable: { multi: true, search: true },
            },
            {
                field: "IsActive",
                title: "Active",
                hidden: false,
                /*template: function (dataItem) {
                    return statusTemplate(dataItem);
                },*/
                editor: '<span class="index_cbox"><input name="IsActive" type="checkbox" #= IsActive ? "checked=checked" : "" #  class="option-input checkbox"></input></span>',
            }
        ],
        save: function (e) {
             
            var ownerId = e.model.OwnerId;
            var oldValue = e.model.Name;
            var newValue = e.values.Name;
            if (ownerId && ownerId != '') {
                list.forEach(function (v) {
                    if (v.OwnerId == ownerId) {
                        if (newValue)
                            v.Name = newValue;
                        else
                            v.IsActive = e.values.IsActive;
                    }
                });
            } else {
                if (oldValue) {
                    list.forEach(function (v) {
                        if (v.Name == oldValue) {
                            if (newValue)
                                v.Name = newValue;
                            else
                                v.IsActive = e.values.IsActive;
                        }
                    });
                } else {
                    list.push({ "ownerId": "", "Name": newValue, "IsActive": true });
                }
            }
             
        },
        filterable: true,
        dataBound: function (e) {
            // get the index of the UnitsInStock cell
            var columns = e.sender.columns;
            var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "IsActive" + "]").index();

            // iterate the data items and apply row styles where necessary
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var discontinued = dataItems[j].get("IsActive");

                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (!discontinued) {
                    row.addClass("LightGray");
                }
            }
        },
        resizable: true,
        autoSync: true,
        sortable: true,
        noRecords: { template: "No records found" },
        scrollable: true,
        editable: true,
        toolbar: ["create"],
    };

    $scope.OwnerList = {
        dataSource: {
            transport: {
                read: function (e) {
                    list = [];
                    $http.get("/api/lookup/0/GetSourcePickList").then(function (data) {
                         
                        angular.forEach(data.data.Owner, function (value, key) {
                            if ($scope.ShowInActive || value.IsActive)
                                list.push(value);
                        });
                         
                        e.success(list);
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
                }
            },
            error: function (e) {
                //HFC.DisplayAlert(e.errorThrown);
            },
        },
        columns: [
            {
                field: "Name",
                title: "Owner",
                filterable: { multi: true, search: true },
            },
            {
                field: "",
                title: "Active",
                filterable: { multi: true, search: true },
                template: function (dataItem) {
                    return MainGridStatusTemplate(dataItem);
                }
            },
        ],
        dataBound: function (e) {
            // get the index of the UnitsInStock cell
            var columns = e.sender.columns;
            var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "IsActive" + "]").index();

            // iterate the data items and apply row styles where necessary
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var discontinued = dataItems[j].get("IsActive");

                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (!discontinued) {
                    row.addClass("LightGray");
                }
            }
        },
        filterable: true,
        resizable: true,
        autoSync: true,
        sortable: true,
        noRecords: { template: "No records found" },
        scrollable: true,
        editable: false,
    };
     

    function statusTemplate(dataItem) {
         
        var div = '';
        div += '<span class="index_cbox"><input type="checkbox"';
        if (dataItem.IsActive) div += 'checked=checked';
        div += 'class="option-input checkbox"></input></span>';
        return div;
    }

    $scope.kendoValidator = function (gridId) {
        return $("#" + gridId).kendoValidator({
            validate: function (e) {
                $("span.k-invalid-msg").hide();
                var dropDowns = $(".k-dropdown");
                $.each(dropDowns, function (key, value) {
                    var input = $(value).find("input.k-invalid");
                    var span = $(this).find(".k-widget.k-dropdown.k-header");
                    
                    if (input.size() > 0) {
                        $(this).addClass("dropdown-validation-error");
                    } else {
                        $(this).removeClass("dropdown-validation-error");
                    }
                });
            }
        }).getKendoValidator();
    }

    $scope.saveGrid = function () {
         
        var validator = $scope.kendoValidator("gridOwner");
        if (validator.validate()) {
            $http.post('/api/Source/0/PostTypeOwner', list).then(function (response) {
                 
                //$scope.allowEdit = false;
                //$route.reload();
                $window.location.href = '/controlpanel#!/ownerlist';
            }, function (response) {
                 
                console.log("response: ", response);
            });
        }
        
    }

    //filter with show inactive
    $scope.IsSuspendSelect = function () {
         
        var test = $scope.ShowInActive;
        $("#gridOwner").data("kendoGrid").dataSource.read();
        //$("#gridOwner").data("kendoGrid").dataSource.reload();
        $("#gridOwner").data("kendoGrid").refresh();
         
    }

    function MainGridStatusTemplate(dataItem) {

        var Div = '';
        Div += '<span ng-hide="' + dataItem.IsActive + '" class="fieldInActive index_cbox">';
        Div += '<input type="checkbox" value="suspended Owner" id="lstchksuspendedOwner" checked="' + dataItem.IsActive + '" style="border:0px solid #b2bfca">';
        Div += '<label class="checkbox" for="lstchksuspendedOwner" style="border:0px solid #b2bfca"></label>';
        Div += '</span>';

        Div += '<span class="field index_cbox" ng-if="' + dataItem.IsActive + '" >';
        Div += '<input type="checkbox" value="suspended Owner" id="lstchksuspendedOwner" checked="' + dataItem.IsActive + '" style="border:0px solid #b2bfca">';
        Div += '<label class="checkbox" for="lstchksuspendedOwner" style="border:0px solid #b2bfca"></label>';
        Div += '</span>';
        return Div;
    }
}]);