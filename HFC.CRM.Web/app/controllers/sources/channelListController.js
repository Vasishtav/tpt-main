﻿/***************************************************************************\
Module Name:  channelListController.js - AngularJS file
Project: HFC
Created on: 28 June 2018 Thursday
Created By: Ramesh
Copyright:
Description: Channel Controller
Change History:
Date  By  Description

\***************************************************************************/

appCP.controller('channelListController', [
    '$scope', '$route', '$routeParams', '$rootScope', '$http', '$window',
    '$location', '$modal', 'HFCService', 'FranchiseService', 'NavbarServiceCP',
    'AdminSourceChannelServiceTP',
function (
    $scope, $route, $routeParams, $rootScope, $http, $window,
    $location, $modal, HFCService, FranchiseService, NavbarServiceCP, AdminSourceChannelServiceTP) {
    $scope.NavbarServiceCP = NavbarServiceCP;
    $scope.AdminSourceChannelServiceTP = AdminSourceChannelServiceTP;
    $scope.NavbarServiceCP.SetupMarketInfoTab();
   
    $scope.id = $routeParams.Id;
    if ($scope.id == "1")
        $scope.ShowIcon = true;
    else
        $scope.ShowIcon = false;
    //$scope.ShowIcon = $scope.AdminSourceChannelServiceTP.getIcon();
    
    
    $scope.Page_dirty = false;

    $scope.FranchiseService = FranchiseService;
    var list = [];
    var unOrgList = [];
    $scope.ShowInActive = false;

    $scope.Permission = {};
    var Sourcepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Sources')

    $scope.Permission.Viewsource = Sourcepermission.CanRead;
    $scope.Permission.Addsource = Sourcepermission.CanCreate;
    $scope.Permission.Editsource = Sourcepermission.CanUpdate;

    $scope.EditChannelList = {
        dataSource: {
            transport: {
                read: function (e) {
                    list = [];
                    $http.get("/api/lookup/0/GetSourcePickList").then(function (data) {
                         
                        angular.forEach(data.data.Channel, function (value, key) {
                            if ($scope.ShowInActive || value.IsActive)
                                list.push(value);
                        });
                         
                        e.success(list);
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
                }
            },
            error: function (e) {
                //HFC.DisplayAlert(e.errorThrown);
            },
        },
        schema: {
            model: {
                id: "ChannelID",
                fields: {
                    Name: { editable: true, validation: { required: true } },
                    IsActive: { type: "boolean", editable: true },
                }
            }
        },
        columns: [
            {
                field: "Name",
                title: "Channel",
                editor: '<input  required=true name=" Name " data-bind="value:Name" style="width: 230px;border: none "/>',
                filterable: { multi: true, search: true },
            },
            {
                field: "IsActive",
                title: "Active",
                hidden: false,
                editor: '<span class="index_cbox"><input name="IsActive" type="checkbox" #= IsActive ? "checked=checked" : "" #  class="option-input checkbox"></input></span>',
                /*template: function (dataItem) {
                    return statusTemplate(dataItem);
                }*/
            }
        ],
        save: function (e) {
             
            $scope.Channelmap_form.$setDirty(true);
            var channelId = e.model.ChannelId;
            var oldValue = e.model.Name;
            var newValue = e.values.Name;
            if (channelId && channelId != '') {
                list.forEach(function (v) {
                    if (v.ChannelId == channelId) {
                        if (newValue)
                            v.Name = newValue;
                        else
                            v.IsActive = e.values.IsActive;
                    }
                });
            } else {
                if (oldValue) {
                    list.forEach(function (v) {
                        if (v.Name == oldValue) {
                            if (newValue)
                                v.Name = newValue;
                            else
                                v.IsActive = e.values.IsActive;
                        }
                    });
                } else {
                    list.push({ "channelId": "", "Name": newValue, "IsActive": true });
                }
            }
             
        },
        filterable: true,
        dataBound: function (e) {
            // get the index of the UnitsInStock cell
            var columns = e.sender.columns;
            var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "IsActive" + "]").index();

            // iterate the data items and apply row styles where necessary
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var discontinued = dataItems[j].get("IsActive");

                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (!discontinued) {
                    row.addClass("LightGray");
                }
            }
            if ($scope.Permission.Addsource == false || $scope.Permission.Editsource == false) {
                //e.sender.element.find(".k-grid-edit").hide();
                e.sender.element.find(".k-grid-add").hide();
            }
        },
        resizable: true,
        autoSync: true,
        sortable: true,
        noRecords: { template: "No records found" },
        scrollable: true,
        editable: true,
        toolbar: ["create"],
    };

    

    $scope.ChannelList = {
        dataSource: {
            transport: {
                read: function (e) {
                    list = [];
                    $http.get("/api/lookup/0/GetSourcePickList").then(function (data) {
                         
                        angular.forEach(data.data.Channel, function (value, key) {
                            if ($scope.ShowInActive || value.IsActive)
                                list.push(value);
                        });
                         
                        e.success(list);
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
                }
            },
            error: function (e) {
                //HFC.DisplayAlert(e.errorThrown);
            },
        },
        columns: [
            {
                field: "Name",
                title: "Channel",
                filterable: { multi: true, search: true },
            },
            {
                field: "",
                title: "Active",
                filterable: { multi: true, search: true },
                template: function (dataItem) {
                    return MainGridStatusTemplate(dataItem);
                }
            },
        ],
        dataBound: function (e) {
            // get the index of the UnitsInStock cell
            var columns = e.sender.columns;
            var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "IsActive" + "]").index();

            // iterate the data items and apply row styles where necessary
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var discontinued = dataItems[j].get("IsActive");

                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (!discontinued) {
                    row.addClass("LightGray");
                }
            }
        },
        filterable: true,
        resizable: true,
        autoSync: true,
        sortable: true,
        noRecords: { template: "No records found" },
        scrollable: true,
        editable: false,
    };
    

    function statusTemplate(dataItem) {
        var div = '';
        div += '<span class="index_cbox"><input type="checkbox" checked = "' + dataItem.IsActive + '" class="option-input checkbox"></input></span>';
        //div += '<input type="checkbox" #= IsActive ? "checked=checked" : "" #  class="option-input checkbox"></input>';
        return div;
    }

    $scope.kendoValidator = function (gridId) {
        return $("#" + gridId).kendoValidator({
            validate: function (e) {
                $("span.k-invalid-msg").hide();
                var dropDowns = $(".k-dropdown");
                $.each(dropDowns, function (key, value) {
                    var input = $(value).find("input.k-invalid");
                    var span = $(this).find(".k-widget.k-dropdown.k-header");
                    
                    if (input.size() > 0) {
                        $(this).addClass("dropdown-validation-error");
                    } else {
                        $(this).removeClass("dropdown-validation-error");
                    }
                });
            }
        }).getKendoValidator();
    }

    $scope.saveGrid = function () {
         
        $scope.Channelmap_form.$setPristine();
        var validator = $scope.kendoValidator("gridChannel");
        if (validator.validate()) {
            
            $http.post('/api/Source/0/PostTypeChannel', list).then(function (response) {
                 
                if (response.data == "Success") {
                    history.go(-1);
                    if (response.config.data[response.config.data.length - 1].channelId == "" && response.config.data[response.config.data.length - 1].IsActive == true)
                    $scope.AdminSourceChannelServiceTP.setValuetorow(response.config.data[response.config.data.length - 1]);
                }
                else {
                    HFC.DisplayAlert(response.data);
                    return;
                }
                
            }
            //function (response) {
            //     
            //    console.log("response: ", response);
            //}
            );
        }
       
    }

    $scope.CancelGrid = function () {
        
        history.go(-1);
    }
    
    function backToList() {
        history.go(-1);
    }

    //filter with show inactive
    $scope.IsSuspendSelect = function () {
         
        var test = $scope.ShowInActive;
        $("#gridChannel").data("kendoGrid").dataSource.read();
        //$("#gridChannel").data("kendoGrid").dataSource.reload();
        $("#gridChannel").data("kendoGrid").refresh();
         
    }

    function MainGridStatusTemplate(dataItem) {

        var Div = '';
        Div += '<span ng-hide="' + dataItem.IsActive + '" class="fieldInActive index_cbox">';
        Div += '<input type="checkbox" value="suspended Channel" id="lstchksuspendedChannel" checked="' + dataItem.IsActive + '" style="border:0px solid #b2bfca">';
        Div += '<label class="checkbox" for="lstchksuspendedChannel" style="border:0px solid #b2bfca"></label>';
        Div += '</span>';

        Div += '<span class="field index_cbox" ng-if="' + dataItem.IsActive + '" >';
        Div += '<input type="checkbox" value="suspended Channel" id="lstchksuspendedChannel" checked="' + dataItem.IsActive + '" style="border:0px solid #b2bfca">';
        Div += '<label class="checkbox" for="lstchksuspendedChannel" style="border:0px solid #b2bfca"></label>';
        Div += '</span>';
        return Div;
    }
}]);