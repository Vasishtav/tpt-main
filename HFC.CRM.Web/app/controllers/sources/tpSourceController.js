﻿/***************************************************************************\
Module Name:  sourceController .js - AngularJS file
Project: HFC
Created on: 21 June 2018 Thursday
Created By: Ramesh
Copyright:
Description: Source Controller
Change History:
Date  By  Description

\***************************************************************************/

appCP.controller('tpSourceController', [
    '$http', '$scope', '$window', '$location', '$rootScope', 'HFCService', 'FranchiseService','NavbarServiceCP','AdminSourceChannelServiceTP',
    function ($http, $scope, $window, $location, $rootScope, HFCService, FranchiseService, NavbarServiceCP, AdminSourceChannelServiceTP) {

        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.AdminSourceChannelServiceTP = AdminSourceChannelServiceTP;
        $scope.NavbarServiceCP.SetupMarketInfoTab();

        $scope.FranchiseService = FranchiseService;
        $scope.HFCService = HFCService;

        // for kendo tooltip
        //$scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        HFCService.setHeaderTitle("TPT Source #");
        $scope.Permission = {};
        var Sourcepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Sources')
        if (Sourcepermission){
        $scope.Permission.Viewsource = Sourcepermission.CanRead;
        $scope.Permission.Addsource = Sourcepermission.CanCreate;
        $scope.Permission.Editsource = Sourcepermission.CanUpdate;
        }
        $scope.AddRow = function () {
            var grid = $("#tpGrid").data("kendoGrid");
            grid.addRow();
            $scope.EditMode = true;
        }

        //click add source button.
        $scope.AddNewSource = function () {
             
            $scope.setpagepristine();
            var grids = $("#tpGrid").data("kendoGrid");

            //which row is edited
            var rowInEditMode = $("#tpGrid").find("tr.k-grid-edit-row");
            var itemBeingEdited = $("#tpGrid").data("kendoGrid").dataSource.getByUid(rowInEditMode.data("uid"));
            $scope.AdminSourceChannelServiceTP.setPageNumber(grids.dataSource.page());

            //assign or set to service
            $scope.AdminSourceChannelServiceTP.setEditableRow(itemBeingEdited);
            $scope.AdminSourceChannelServiceTP.setIcon(true);
            $window.location.href = "#!/editsourcelist/1";
        }
        
        //click add channel button.
        $scope.AddNewChannel = function () {
             
            //$scope.setpagepristine();
            
            //which row is edited
            var grids = $("#tpGrid").data("kendoGrid");
            var rowInEditMode = $("#tpGrid").find("tr.k-grid-edit-row");
            var itemBeingEdited = $("#tpGrid").data("kendoGrid").dataSource.getByUid(rowInEditMode.data("uid"));
            $scope.AdminSourceChannelServiceTP.setPageNumber(grids.dataSource.page());
            //assign or set to service
            $scope.AdminSourceChannelServiceTP.setEditableRow(itemBeingEdited);
            //var show_val = true;
            $scope.AdminSourceChannelServiceTP.setIcon(true);
            $scope.HFCSourcemap_form.$setPristine(true);
            $window.location.href = "#!/editchannellist/1";
        }

        //redirect clear out else grid will be in edit-mode
        $scope.RedirecttoHFCSource = function () {
             
            $window.location.href = "#!/edithfcsources";
            $scope.AdminSourceChannelServiceTP.clearValue();
        }

        //redirect clear out else grid will be in edit-mode
        $scope.RedirecttoUTMCampaign = function () {
             
            $window.location.href = "#!/hfcleadsourcemap";
            $scope.AdminSourceChannelServiceTP.clearValue();
        }
        
        $scope.$on('$routeChangeStart', function ($event, next, current) {
            // ... you could trigger something here ...
             
            if (next.$$route.originalPath.toUpperCase().includes('EDITCHANNELLIST') || next.$$route.originalPath.toUpperCase().includes('EDITSOURCELIST')) {

            }
            else {
                var value_edit=$("#tpGrid").find("tr.k-grid-edit-row");
                if (value_edit.length > 0) {
                    $scope.HFCSourcemap_form.$setDirty(true);
                }
               
            }
            
        });


        //make the grid row editable.
        $scope.setDefaultValue = function () {
             
            var grids = $("#tpGrid").data("kendoGrid");
            var pagenumber = $scope.AdminSourceChannelServiceTP.getPageNumberValue();
            if (pagenumber)
            {
                grids.dataSource.page(pagenumber);
            }
           
            //get the which row was in edit mode 
            var editedrow = $scope.AdminSourceChannelServiceTP.getEditableRow();
            if (editedrow) {
            //get the kendo grid list
            var listrow = grids.dataSource._data;
            if (editedrow.id == null) {
                var items = grids.dataSource.insert(0, editedrow);
                offset = 0;
            }
            var val = grids.dataSource._data;

            var offset; var uid = ""; 
            
            for (var i = 0; i < listrow.length; i++) {
                if (listrow[i]['SourcesTPId'] == editedrow.SourcesTPId) {
                    offset = i + 1;
                    uid = listrow[i].uid;
                }
            }

            var recordCount = grids.dataSource._pageSize;

            while (offset > recordCount) {
                offset = offset - recordCount;
            }
                                
            grids.editRow($("#tpGrid tr:eq(" + offset + ")"));
            if (editedrow.id == null) {
                var channelDropdown = $("#Channeldropdown").data("kendoDropDownList");
                var cd = channelDropdown.dataSource.read();
                var sourceDropdown = $("#Sourcedropdown").data("kendoDropDownList");
                var sd = sourceDropdown.dataSource.read();
            }
        }

                //make the grid row in edit mode
                    
            }

        //assign channelvalue after added.
        $scope.GetChannellist = function () {
             
            //get value from dropdown
            var channelgrids = $("#Channeldropdown").data("kendoDropDownList");
            var listvalue = channelgrids.dataSource._data;
            
            if (listvalue.length == 0) {
                listvalue = $scope.AdminSourceChannelServiceTP.getdropdownChannnel();
            }
            //look for newly added channel
            var editedchannel = $scope.AdminSourceChannelServiceTP.getChannelValue();
            var channel_id = ""; var offset = "";
            if (editedchannel) {
                for (var i = 0; i < listvalue.length; i++) {
                    if (listvalue[i]['Channel'] == editedchannel.Name) {
                        offset = i + 1;
                        channel_id = listvalue[i].ChannelId;
                    }
                }
            }
            var Sourcetp_Id = ""; var channel_Id = ""; var channel_val = "";
            
            //assign value to row
            if (editedchannel) {
                channelgrids.select(offset);
                var rrow = $scope.AdminSourceChannelServiceTP.getEditableRow();
                rrow.ChannelId = channel_id;
                rrow.Channel = editedchannel.Name
                var grids = $("#tpGrid").data("kendoGrid");
                var listrow = grids.dataSource._data;
                for (i = 0;i<listrow.length;i++){
                    if (listrow[i]['SourcesTPId'] == rrow.SourcesTPId) {
                        channel_Id = rrow.ChannelId;
                        channel_val = rrow.Channel;
                        Sourcetp_Id = rrow.SourcesTPId;
                        
                    }
                    
                }
                
                //update to grid else save will not work
                var data_Row = grids.dataSource.get(Sourcetp_Id);
                data_Row.set("ChannelId", channel_Id);
                data_Row.set("Channel", channel_val);
                //clear the value after set
                $scope.AdminSourceChannelServiceTP.clearValue();
            }
                    
        }

        //assign source value after added.
        $scope.GetSourceListVal = function () {
             
            //get value from dropdown
            var sourcegrids = $("#Sourcedropdown").data("kendoDropDownList");
            var sourcelistvalue = sourcegrids.dataSource._data;
            //look for newly added source.
            var editedsource = $scope.AdminSourceChannelServiceTP.getSourceValue();
            var source_id = ""; var offset = "";
            if (editedsource) {
                for (var i = 0; i < sourcelistvalue.length; i++) {
                    if (sourcelistvalue[i]['Source'] == editedsource.Name) {
                        offset = i + 1;
                        source_id = sourcelistvalue[i].SourceId;
                    }
                       
                }
            }
            var Sourcetp_Id = ""; var source_Id = ""; var source_val = "";
            //assign value to row
            if (editedsource) {
                sourcegrids.select(offset);
                var rrow = $scope.AdminSourceChannelServiceTP.getEditableRow();
                rrow.SourceId = source_id;
                rrow.Source = editedsource.Name
                var grids = $("#tpGrid").data("kendoGrid");
                var listrow = grids.dataSource._data;
                for (i = 0; i < listrow.length; i++) {
                    if (listrow[i]['SourcesTPId'] == rrow.SourcesTPId) {
                        source_Id = rrow.SourceId;
                        source_val = rrow.Source;
                        Sourcetp_Id = rrow.SourcesTPId;
                    }
                }
                //update to grid else save will not work
                var data_Row = grids.dataSource.get(Sourcetp_Id);
                data_Row.set("SourceId", source_Id)
                data_Row.set("Source", source_val)
                //clear the value after set
                $scope.AdminSourceChannelServiceTP.clearValue();
            }
        }

$scope.campaignsByFranchise = function () {
            $scope.EditSourcesList = {
                dataSource: {
                    transport: {
                        read: {

                            url: '/api/Source/0/GetSourcesTP',
                            complete: function () {
                                $scope.setDefaultValue();
                            }

                        },
                        update: {
                            url: '/api/Source/0/PostSourcesTP',
                            type: "POST",
                            complete: function (e) {
                                $("#tpGrid").data("kendoGrid").dataSource.read();
                                HFC.DisplaySuccess("Campaign Updated Successfully.");
                            }

                        },
                        create: {
                            url: '/api/Source/0/PostSourcesTP',
                            type: "POST",
                            complete: function (e) {
                                $("#tpGrid").data("kendoGrid").dataSource.read();
                                HFC.DisplaySuccess("New Campaign added sucessfully.");
                            }

                        },
                        parameterMap: function (options, operation) {
                             
                            //console.log('test');
                            if (operation === "create") {
                                $scope.setpagepristine();
                                $scope.AdminSourceChannelServiceTP.clearValue();
                                return {
                                    SourcesTPId: options.SourcesTPId,
                                    Owner: options.Owner,
                                    OwnerId: options.OwnerId,
                                    Channel: options.Channel,
                                    ChannelId: options.ChannelId,
                                    Source: options.Source,
                                    SourceId: options.SourceId,
                                    IsActive: options.IsActive,
                                    IsDeleted: options.IsDeleted,
                                   
                                };
                            }
                            else if (operation === "update") {
                                $scope.setpagepristine();
                                $scope.AdminSourceChannelServiceTP.clearValue();
                                return {
                                    SourcesTPId: options.SourcesTPId,
                                    Owner:options.Owner,
                                    OwnerId: options.OwnerId,
                                    Channel:options.Channel,
                                    ChannelId: options.ChannelId,
                                    Source:options.Source,
                                    SourceId: options.SourceId ,
                                    IsActive: options.IsActive,
                                    IsDeleted: options.IsDeleted,
                                    
                                };
                            }
                            
                        }
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "SourcesTPId",
                            fields: {
                                SourcesTPId: { editable: false, nullable: true },
                                Owner: { editable: true, validation: { required: true } },
                                Channel: { editable: true },
                                Source: { editable: true },
                                IsActive: { type: "boolean" },
                            }
                        }
                    }


                },
                // https://www.telerik.com/forums/remove-page-if-no-records
                dataBound: function (e) {

                    $('.k-grid-add').click(function () {
                        $scope.setpagedirty();
                    })

                    $('.k-grid-edit').click(function () {
                        $scope.setpagedirty();
                    })

                    $("tr", "#tpGrid").on("click", ".k-grid-cancel", function (e) {
                        $scope.setpagepristine();
                    });

                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }

                    if (this.dataSource._data.length == 0) {
                        $scope.searchBoxEnable = true;
                    } else {
                        $scope.searchBoxEnable = false;
                    }
                    var searchValue = $('#searchBox').val();
                    if (searchValue != "") {
                        $('#btnReset').prop('disabled', false);
                        $('#btnAddrow').prop('disabled', true);
                        $('#btnAddSource').prop('disabled', true);
                        $('#btnAddChannel').prop('disabled', true);
                    }
                    else {
                        $('#btnReset').prop('disabled', true);
                        $('#btnAddrow').prop('disabled', false);
                        $('#btnAddSource').prop('disabled', false);
                        $('#btnAddChannel').prop('disabled', false);
                    }
                    if ($scope.Permission.Editsource == false) {
                        e.sender.element.find(".k-grid-edit").hide();
                        e.sender.element.find(".k-grid-add").hide();
                    }
                },
                resizable: true,
                columns: [
                    {
                        field: "SourcesTPId",
                        title: "SourcesTPId",
                        hidden: true,
                    },

                            {
                                field: "Owner",
                                title: "Owner",
                               
                                hidden: false,
                                filterable: { multi: true, search: true },
                                editor: function (container, options) {
                                    $('<input required=true   name="Owner" data-bind="value:OwnerId" style="width: 130px;border: none "  />')
                                            .appendTo(container)
                                            .kendoDropDownList({
                                                autoBind: false,
                                                optionLabel: "Select",

                                                valuePrimitive: true,
                                                dataTextField: "Owner",
                                                dataValueField: "OwnerId",
                                                template: "#=Owner #",

                                                dataSource: {
                                                    transport: {
                                                        read: {

                                                            url: '/api/Source/0/GetOwnerSources',
                                                        }
                                                    },
                                                }

                                            });
                                }


                            },

                            {
                                field: "Channel",
                                title: "Channel",
                                hidden: false,
                                filterable: {
                                    //multi: true,
                                    search: true
                                },
                                editor: function (container, options) {
                                    $('<input required=true id="Channeldropdown"  name="Channel" data-bind="value:ChannelId" data-filter="contains" style="width: 130px;border: none "  />')
                                            .appendTo(container)
                                            .kendoDropDownList({
                                                //autoBind: true,

                                                autoBind: false,
                                                optionLabel: "Select",
                                                valuePrimitive: true,
                                                dataTextField: "Channel",
                                                dataValueField: "ChannelId",
                                                template: "#=Channel #",
                                                dataSource: {
                                                    transport: {
                                                        read: {

                                                            url: '/api/Source/0/GetChannelSources',
                                                            complete: function () {
                                                                $scope.GetChannellist()
                                                            }
                                                        }
                                                    },
                                                }

                                            });
                                    
                                    $('<button type="button" ng-click="AddNewChannel()" class="plus_but tooltip-bottom"  value="Add"><i class="far fa-plus-square" style="vertical-align:sub;margin-left:10px;"></i></button>').appendTo(container);
                                }



                            },
                              {
                                  field: "Source",
                                  title: "Source",
                                  hidden: false,
                                  filterable: {
                                      //multi: true,
                                      search: true
                                  },
                                  editor: function (container, options) {
                                      $('<input required=true   name="Source" id="Sourcedropdown" data-bind="value:SourceId" data-filter="contains" style="width: 130px;border: none "  />')
                                              .appendTo(container)
                                              .kendoDropDownList({
                                                  autoBind: true,
                                                  //autoBind: false,
                                                  optionLabel: "Select",
                                                  valuePrimitive: true,
                                                  dataTextField: "Source",
                                                  dataValueField: "SourceId",
                                                  template: "#=Source #",
                                                  dataSource: {
                                                      transport: {
                                                          read: {

                                                              url: '/api/Source/0/GetSource',
                                                              complete: function () {
                                                                  $scope.GetSourceListVal();
                                                              }
                                                          }
                                                      },
                                                  }

                                              });
                                      $('<button type="button" ng-click="AddNewSource()" class="plus_but tooltip-bottom"  value="Add" ><i class="far fa-plus-square" style="vertical-align:sub;margin-left:10px;"></i></button>').appendTo(container);
                                  }


                              },
                             
                              {
                                  field: "IsActive",
                                  title: "Status",
                                  template: " #if(IsActive){# Active #}else{# Inactive#}# ",
                                  filterable: { messages: { isTrue: "Active", isFalse: "Inactive" } }
                              },
                    { command: ["edit"], title: "&nbsp;", width: "100px" }

                ],
                editable: "inline",

                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                noRecords: { template: "No records found" },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                        $scope.AdminSourceChannelServiceTP.clearValue();
                        $scope.HFCSourcemap_form.$setPristine();
                        if ($scope.setvalue == "on") {
                            
                        }
                        else {
                            $("#tpGrid").data("kendoGrid").dataSource.read();
                            $("#tpGrid").data("kendoGrid").refresh();
                        }
                        
                    }
                },
                sortable: ({ field: "StartDate", dir: "desc" }),
                toolbar: [
                      {
                          template: kendo.template($("#headToolbarTemplate").html()),
                      }]
              
            };

            //$scope.setDefaultValue();
        };

$scope.campaignsByFranchise();


        // Kendo Resizing
$scope.$on("kendoWidgetCreated", function (e) {
    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
    window.onresize = function () {
        $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
    };
});

       
        

        //added the code for the showinactive checkbox.
$scope.IsSuspendSelect = function () {
    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), true);
            $scope.setvalue = $('input[name="SourceCheck"]:checked').val();
            if ($scope.setvalue == "on") {

                $http.get('/api/Source/0/GetInactiveSourcesTP').then(function (response) {
                    var data = response.data;
                    var grid = $("#tpGrid").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                }).catch(function(e)
                {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                });
            }
            else if ($scope.setvalue == undefined) {
                $http.get('/api/Source/0/GetSourcesTP').then(function (response) {
                    var data = response.data;
                    var grid = $("#tpGrid").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                }).catch(function(e)
                {
                    kendo.ui.progress($('.k-grid-content.k-auto-scrollable'), false);
                });
            }
        }

    $scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                validate: function (e) {
                    $("span.k-invalid-msg").hide();
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find(".k-widget.k-dropdown.k-header");
                        
                        if (input.size() > 0) {
                            $(this).addClass("dropdown-validation-error");
                        } else {
                            $(this).removeClass("dropdown-validation-error");
                        }
                    });
                }
            }).getKendoValidator();
        }

    $scope.TptSource = function () {
        var validator = $scope.kendoValidator("tpGrid");
        if (validator.validate()) {
            var searchValue = $('#searchBox').val();

            $("#tpGrid").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  
                  {
                      field: "Owner",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "Channel",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "Source",
                      operator: "contains",
                      value: searchValue
                  }

                ]
            });
        }
        else {
            $('#searchBox').val('');
        }

    }

    $scope.TptSourceSearchClear = function () {
        $('#searchBox').val('');
        var searchValue = $('#searchBox').val('');
        if (searchValue == '') {
            $('#btnReset').prop('disabled', true);
            $('#btnAddrow').prop('disabled', false);
            $('#btnAddSource').prop('disabled', false);
            $('#btnAddChannel').prop('disabled', false);
        }
        else {
            $('#btnReset').prop('disabled', false);
            $('#btnAddrow').prop('disabled', true);
            $('#btnAddSource').prop('disabled', true);
            $('#btnAddChannel').prop('disabled', true);
        }
        $("#tpGrid").data('kendoGrid').dataSource.filter({
        });
    }
        // Kendo Resizing
    $scope.$on("kendoWidgetCreated", function (e) {
        $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
        window.onresize = function () {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 283);
        };
    });
        // End Kendo Resizing
        $scope.setpagedirty = function () {
            
            $scope.HFCSourcemap_form.$setDirty();
        }

        $scope.setpagepristine = function () {
            
            $scope.HFCSourcemap_form.$setPristine();
        }
    }
]);









