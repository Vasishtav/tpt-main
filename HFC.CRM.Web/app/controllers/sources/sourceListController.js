﻿/***************************************************************************\
Module Name:  sourceListController.js - AngularJS file
Project: HFC
Created on: 28 June 2018 Thursday
Created By: Ramesh
Copyright:
Description: Source Controller
Change History:
Date  By  Description

\***************************************************************************/

appCP.controller('sourceListController', [
    '$scope', '$route', '$routeParams', '$rootScope', '$http', '$window',
    '$location', '$modal', 'HFCService', 'FranchiseService', 'NavbarServiceCP',
    'AdminSourceChannelServiceTP',
function (
    $scope, $route, $routeParams, $rootScope, $http, $window,
    $location, $modal, HFCService, FranchiseService, NavbarServiceCP, AdminSourceChannelServiceTP) {
    $scope.NavbarServiceCP = NavbarServiceCP;
    $scope.NavbarServiceCP.SetupMarketInfoTab();
     
    $scope.id = $routeParams.Id;
    $scope.FranchiseService = FranchiseService;
    $scope.AdminSourceChannelServiceTP = AdminSourceChannelServiceTP;
    if ($scope.id == "1")
        $scope.ShowIcon = true;
    else
        $scope.ShowIcon = false;

    $scope.Permission = {};
    var Sourcepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Sources')

    $scope.Permission.Viewsource = Sourcepermission.CanRead;
    $scope.Permission.Addsource = Sourcepermission.CanCreate;
    $scope.Permission.Editsource = Sourcepermission.CanUpdate;

    var unOrgList = [];
    var list = [];
    $scope.ShowInActive = false;

    $scope.EditSourceList = {
        dataSource: {
            transport: {
                read: function (e) {
                    $http.get("/api/lookup/0/GetSourcePickList").then(function (data) {
                        list = [];
                         
                        angular.forEach(data.data.Source, function (value, key) {
                            if ($scope.ShowInActive || value.IsActive)
                                list.push(value);
                        });
                         
                        e.success(list);
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
                }
            },
            error: function (e) {
                //HFC.DisplayAlert(e.errorThrown);
            },
        },
        schema: {
            model: {
                id: "SourceID",
                fields: {
                    Name: { editable: true, validation: { required: true } },
                    IsActive: { type: "boolean", editable: true, },
                }
            }
        },
        columns: [
            {
                field: "Name",
                title: "Source Name",
                filterable: { multi: true, search: true },
                editor: '<input  required=true name=" Name " data-bind="value:Name" style="width: 230px;border: none "/>',
            },
            {
                field: "IsActive",
                title: "Source Status",
                editor: '<input name="IsActive" type="checkbox" #= IsActive ? "checked=checked" : "" #  class="option-input checkbox"></input>',
                /*template: function (dataItem) {
                    return statusTemplate(dataItem);
                },*/
                filterable: { multi: true, search: true },
            },
        ],
        save: function (e) {
             
            $scope.Sourcemap_form.$setDirty(true);
            var sourceId = e.model.SourceId;
            var oldValue = e.model.Name;
            var newValue = e.values.Name;
            if (sourceId && sourceId != '') {
                list.forEach(function (v) {
                    if (v.SourceId == sourceId) {
                        if (newValue)
                            v.Name = newValue;
                        else
                            v.IsActive = e.values.IsActive;
                    }
                });
            } else {
                if (oldValue) {
                    list.forEach(function (v) {
                        if (v.Name == oldValue) {
                            if (newValue)
                                v.Name = newValue;
                            else
                                v.IsActive = e.values.IsActive;
                        }
                    });
                } else {
                    list.push({ "sourceId": "", "Name": newValue, "IsActive": true });
                }
            }
             
        },
        filterable: true,
        dataBound: function (e) {

            e.sender.items().each(function () {
                var dataItem = e.sender.dataItem(this);
                kendo.bind(this, dataItem);
                if (dataItem.Discontinued) {
                    $(this).addClass("k-state-selected");
                }
            })

            // get the index of the UnitsInStock cell
            var columns = e.sender.columns;
            var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "IsActive" + "]").index();

            // iterate the data items and apply row styles where necessary
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var discontinued = dataItems[j].get("IsActive");

                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (!discontinued) {
                    row.addClass("LightGray");
                }
            }
            if ($scope.Permission.Addsource == false || $scope.Permission.Editsource == false) {
                //e.sender.element.find(".k-grid-edit").hide();
                e.sender.element.find(".k-grid-add").hide();
            }
        },
        resizable: true,
        autoSync: true,
        sortable: true,
        noRecords: { template: "No records found" },
        scrollable: true,
        editable: true,
        toolbar: ["create"],
    };

    $scope.SourceList = {
        dataSource: {
            transport: {
                read: function (e) {
                    $http.get("/api/lookup/0/GetSourcePickList").then(function (data) {
                        list = [];
                         
                        angular.forEach(data.data.Source, function (value, key) {
                            if ($scope.ShowInActive || value.IsActive)
                                list.push(value);
                        });
                         
                        e.success(list);
                    }).catch(function (error) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                    });
                }
            },
            error: function (e) {
                //HFC.DisplayAlert(e.errorThrown);
            },
        },
        columns: [
            {
                field: "Name",
                title: "Source",
                filterable: { multi: true, search: true },
            },
            {
                field: "",
                title: "Active",
                filterable: { multi: true, search: true },
                template: function (dataItem) {
                    return MainGridStatusTemplate(dataItem);
                }
            },
        ],
        dataBound: function (e) {
            // get the index of the UnitsInStock cell
            var columns = e.sender.columns;
            var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "IsActive" + "]").index();

            // iterate the data items and apply row styles where necessary
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var discontinued = dataItems[j].get("IsActive");

                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (!discontinued) {
                    row.addClass("LightGray");
                }
            }
        },
        filterable: true,
        resizable: true,
        autoSync: true,
        sortable: true,
        noRecords: { template: "No records found" },
        scrollable: true,
        editable: false,
    };
     

    //click on pic enable checkbox and validating pic-id field
    $scope.customClick = function (dataitem) {
        
        var setvalue = $('input[name="IsActive"]').prop('checked');
        dataitem.IsActive = setvalue;
    }

    //filter with show inactive
    $scope.IsSuspendSelect = function () {
         
        var test = $scope.ShowInActive;
        //$scope.SourceList.dataSource.read();
        $("#gridSources").data("kendoGrid").dataSource.read();
        $("#gridSources").data("kendoGrid").refresh();
         
    }

    $scope.kendoValidator = function (gridId) {
        return $("#" + gridId).kendoValidator({
            validate: function (e) {
                $("span.k-invalid-msg").hide();
                var dropDowns = $(".k-dropdown");
                $.each(dropDowns, function (key, value) {
                    var input = $(value).find("input.k-invalid");
                    var span = $(this).find(".k-widget.k-dropdown.k-header");
                    
                    if (input.size() > 0) {
                        $(this).addClass("dropdown-validation-error");
                    } else {
                        $(this).removeClass("dropdown-validation-error");
                    }
                });
            }
        }).getKendoValidator();
    }

    $scope.saveGrid = function () {
        $scope.Sourcemap_form.$setPristine();
        var validator = $scope.kendoValidator("gridSources");
        if (validator.validate()) {
            var newList = list;
             
            $http.post('/api/Source/0/PostSources', list).then(function (response) {
                 
                if (response.data == "Success") {
                    if (response.config.data[response.config.data.length - 1].sourceId == "")
                        $scope.AdminSourceChannelServiceTP.setSourceValtorow(response.config.data[response.config.data.length - 1]);
                    backToList();
                }
                else {
                    HFC.DisplayAlert(response.data);
                    return;
                }
                } 
                //function (response) {
            //     
            //    console.log("response: ", response);
            //}
            );
        }
        
    }

    $scope.CancelGrid = function () {
        
        backToList();
    }

    function backToList() {
        history.go(-1);
    }

    function statusTemplate(dataItem) {
        var div = '';
        div += '<input type="checkbox" checked = "' + dataItem.IsActive + '" class="option-input checkbox"></input>';
        //div += '<input type="checkbox" #= IsActive ? "checked=checked" : "" #  class="option-input checkbox"></input>';
        return div;
    }

    function MainGridStatusTemplate(dataItem) {

        var Div = '';
        Div += '<span ng-hide="' + dataItem.IsActive + '" class="fieldInActive index_cbox">';
        Div += '<input type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + dataItem.IsActive + '" style="border:0px solid #b2bfca">';
        Div += '<label class="checkbox" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
        Div += '</span>';

        Div += '<span class="field index_cbox" ng-if="' + dataItem.IsActive + '" >';
        Div += '<input type="checkbox" value="suspended Source" id="lstchksuspendedSource" checked="' + dataItem.IsActive + '" style="border:0px solid #b2bfca">';
        Div += '<label class="checkbox" for="lstchksuspendedSource" style="border:0px solid #b2bfca"></label>';
        Div += '</span>';
        return Div;
    }

    function customBoolEditor(dataItem) {
        var div = '';
        div += '<input class="k-checkbox" id="' + dataItem.SourceId + '" type="checkbox" name="Discontinued" data-type="boolean" data-bind="checked:Discontinued">';
        div += '<label class="k-checkbox-label" for="' + dataItem.SourceId + '">&#8203;</label>';
        return div;
    }

    function statusTemplate(dataItem) {
        var div = '';
        div += '<input type="checkbox" checked = "' + dataItem.IsActive + '" class="option-input checkbox"></input>';
        //div += '<input type="checkbox" #= IsActive ? "checked=checked" : "" #  class="option-input checkbox"></input>';
        return div;
    }
}]);