'use strict';

app.controller('accountEditorController', [
        '$scope', '$http', 'AccountSourceService', 'PersonService', 'AccountService', 'JobService', 'AddressService', 'AccountNoteService', 'HFCService', , 'AttendeeService', 'OpportunityService', 'FileprintService',
function ($scope, $http, AccountSourceService, PersonService, AccountService, JobService, AddressService, AccountNoteService, HFCService,  AttendeeService, OpportunityService, FileprintService) {
            //AccountSourceService.Get(0);

            // reset
            AccountSourceService.AccountSources = [];
            $scope.AddressService = AddressService;
            $scope.PersonService = PersonService;
            $scope.AccountNoteService = AccountNoteService;
            $scope.AccountDispositionlist = [];
            $scope.AccountSalesSteplist = [];
            $scope.AccountStatuseslist = [];
            PersonService.People = [];
            $scope.AccountService = AccountService;
            $scope.AttendeeService = AttendeeService;
            $scope.OpportunityService = OpportunityService;
            $scope.FileprintService = FileprintService;
            $scope.OpportunityService = OpportunityService;

            $http.get('/api/lookup/0/AccountStatus').then(function (data) {
                $scope.AccountStatuseslist = data.data;

            });
            $http.get('/api/lookup/0/Campaigns').then(function (data) {
                $scope.AccountCampaignslist = data.data;
            });
            $http.get('/api/lookup/0/SourceTP').then(function (data) {
                $scope.AccountSourcesList = data.data;
            });
            // $scope.GetSubSourcesTP = AccountSourceService.GetSubSourcesTP();
            
            $scope.ChangeSourceStatus = function (source) {
                var sourcestatusid = source.AccountSourcesList.Value;
                $scope.AccountchildSourceStatus(sourcestatusid);
            }   
            $scope.AccountchildSourceStatus = function (sourcestatusid) {
                $http.get('/api/lookup/' + sourcestatusid + '/Type_Source').then(function (data) {
                    $scope.Accountchildsourcelist = data.data;
                });
            }

            //Added for Qualification/Question Answers
            $http.get('/api/Accounts/0/GetQuestionTypes').then(function (data) {
                //To get List Question Types and Unique question
                $scope.AllQuestions = data.data;

                if (data.data) {
                    var questArray = $.map(data.data, function (qa) {
                        return qa.Question;
                    });
                    $scope.UniqueQuestions = $.grep(questArray, function (el, index) {
                        return index == $.inArray(el, questArray);
                    });
                }
            });

            //Added for Qualification/Question Answers
            $scope.GetQuestion = function (QAcollection, CurrentQuestion) {
                //Get each question details based on current 
                //sub question from collection
                if (QAcollection) {
                    return $.grep(QAcollection, function (q) {
                        return (q.Question == CurrentQuestion);
                    });
                } else
                    return null;
            };

            //Added for Qualification/Question Answers
            $scope.SaveQAlists = function () {
                //To save the all question and answer selected
                //Loop through all the controls dynamically
                var promise = null,
                    AccountId = 1;
                $scope.isDisabled = false;
                //Loop through controls and save
                for (i in $scope.AllQuestions) {
                    if (!this.IsBusy) {
                        this.IsBusy = true;
                        var question = $scope.AllQuestions[i];
                        promise = $scope.SaveQA(AccountId, question);
                        this.IsBusy = false;
                    }
                }
                $scope.isDisabled = true;
                //After successful save
                if (promise) (
                    function (res) {
                        if (!question.AnswerId && res.data && res.data.AnswerId)
                            question.AnswerId = res.data.AnswerId;
                        question.IsChecked = !question.IsChecked;
                        HFC.DisplaySuccess(question.SubQuestion + " saved successfully");
                    }
                );
            }

            //Added for Qualification/Question Answers
            $scope.SaveQA = function (AccountId, question) {
                //Save the Question and Answer
                if (AccountId && question) {

                    var promise = null,
                    data = { AccountId: AccountId };

                    if (question.Answer == undefined)
                    { question.Answer = false; }

                    data.AccountId = AccountId;
                    data.questionId = question.QuestionId;
                    data.answer = question.Answer;
                    data.JobId = question.JobId;

                    if (question.AnswerId) {
                        promise = $http.put('/api/Accounts/' + AccountId + '/qanda/', data);
                    } else
                        promise = $http.post('/api/Accounts/' + AccountId + '/qanda/', data);
                    return promise;
                }
            };
            
            $scope.ChangeAccountStatus=function ()
            {
                
                $scope.AccountService.AccountSalesSteplist = null;
                $scope.AccountService.AccountDispositionlist = null
                $scope.AccountService.AccountDispositionByAccountStatus($scope.Account.AccountStatusId);

                
            }
            
            $scope.ChangeCampaign = function () {
                var campaignId = $scope.Account.CampaignId;
                //if (campaign == null) {
                //    campaignId = 0;
                //}
                //else {
                //    campaignId = campaign.CampaignId;
                //}
                $http.get('/api/lookup/' + campaignId + '/SourceTp').then(function (data) {
                    $scope.AccountSingleSourceList = [];
                    $scope.AccountChannelList = {};
                    $scope.AccountSingleSourceList = data.data;
                    if (campaignId == 0) {
                        $scope.AccountChannelList = null;

                        $scope.Account.SourcesTPId = 0;
                        $scope.Account.SourceNameFromCampaign = "";
                        $scope.Account.SourcesTPIdFromCampaign = 0;
                    }
                    else {
                        $scope.AccountChannelList = data.data;

                        // Set the source value based on the selected campaign
                        // A Campain can't exist and it always return one source combination
                        // so we can hardcode that the first element in the list is the one.
                        $scope.Account.SourcesTPId = $scope.AccountSingleSourceList[0].SourceId;
                        $scope.Account.SourceNameFromCampaign = $scope.AccountSingleSourceList[0].name;
                        $scope.Account.SourcesTPIdFromCampaign = $scope.AccountSingleSourceList[0].SourceId;
                    }

                    
                });
            }

            $scope.AccountSalesStepByAccountStatus = function (AccountstatusId) {
                
                $scope.AccountSalesSteplist = [
                { SalesStepId: 1, Name: 'Call back set' },
                { SalesStepId: 2, Name: 'Appointment scheduled' },
                { SalesStepId: 3, Name: 'In Qualification' },
                { SalesStepId: 4, Name: 'Queued to call' }
                ];
               
                //$http.get('/api/lookup/' + AccountstatusId + '/SalesStep').then(function (data) {
                //    $scope.AccountDispositionlist = data.data;
                //});
            }
            $scope.AccountService = AccountService;

                    $http({ method: 'GET', url: '/api/address/', params: { AccountIds: 0, includeStates: true } })
                    .then(function (response) {
                        $scope.AddressService.States = response.data.States || [];
                        $scope.AddressService.Countries = response.data.Countries || [];
                        $scope.AddressService.Addresses = response.data.Addresses || [];
                        $scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
                    }, function (response) {
                    });
            

            $scope.setAccountAction = function (action) {
                $scope.submitAction = action;
            }

            $scope.isSubmitted = false;

            $scope.onMouseDownAccount = function (action) {

                $scope.submitAction = action;
                $scope.SaveAccount();
            }



            $scope.DisplayMore = false;

            $scope.IsBusy = false;

            $scope.Account = {
                SourcesTPId: 0,
                AccountStatusId: 0,
                DispositionId:0,
                CampaignId:0,
                PrimaryNotes: '',
                PrimCustomer: {
                    FirstName: '',
                    LastName: '',
                    CompanyName: '',
                    WorkTitle: '',
                    HomePhone: '',
                    CellPhone: '',
                    FaxPhone: '',
                    WorkPhone: '',
                    WorkPhoneExt: '',
                    PrimaryEmail: '',
                    SecondaryEmail: '',
                    PreferredTFN: 'C',
                    Unsubscribed: true,
                    UnsubscribedOn: '',
                    Origination: '',
                    ActionTaken: '',
                    CrossStreet: '',
                    
                    SalesStep: '',
                    LocationInfo: ''

                },
                SecCustomer: {
                    FirstName: '',
                    LastName: '',
                    CompanyName: '',
                    WorkTitle: '',
                    HomePhone: '',
                    CellPhone: '',
                    FaxPhone: '',
                    WorkPhone: '',
                    WorkPhoneExt: '',
                    PrimaryEmail: '',
                    SecondaryEmail: '',
                    PreferredTFN: 'C',
                    Unsubscribed: true,
                    UnsubscribedOn: ''
                },
            };

            $scope.Address =
            {
                IsResidential: false,
                IsCommercial: false,
                AddressType: 'false',
                AttentionText: '',
                Address1: '',
                Address2: '',
                City: '',
                Country:'',
                State: '',
                ZipCode: '',
                CountryCode2Digits: HFCService.FranchiseCountryCode,
                CrossStreet: '',
                Location: ''
            };

            $scope.$watch('Address.IsResidential', function (v) {
                if ($scope.Address.IsResidential) {
                    $scope.Address.IsCommercial = false;
                }
            });

            $scope.$watch('Address.IsCommercial', function (v) {
                if ($scope.Address.IsCommercial) {
                    $scope.Address.IsResidential = false;
                }
            });

            $scope.$watch('Address.CountryCode2Digits', function (v) {
                $scope.Address.City = '';
                $scope.Address.State = '';
                $scope.Address.ZipCode = '';
                $scope.Address.CrossStreet = '';
            });

            $scope.$watch('Address.ZipCode', function () {
                if ($scope.Address.ZipCode) {
                    
                    $http.get('/api/lookup/0/zipcodes?q=' + $scope.Address.ZipCode + "&country=" + $scope.Address.CountryCode2Digits).then(function (response) {
                        $scope.IsBusy = false;
                        if (response.data.zipcodes.length > 0) {
                            $scope.Address.City = response.data.zipcodes[0].City;
                            $scope.Address.State = response.data.zipcodes[0].State;
                            $scope.Address.Country = response.data.zipcodes[0].CountryCode;
                            $scope.AddressService.Addresses.State = response.data.zipcodes[0].State;
                        };
                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                        $scope.IsBusy = false;
                    });
                }
            });

            $scope.addressTypeIsNotValid = false;
            $scope.AccountSourceIsNotValid = false;
            $scope.zipCodeIsNotValid = false;
            $scope.firstNameIsNotValid = false;
            $scope.lastNameIsNotValid = false;

            $scope.SaveAccount = function (operation) {
                
                var dto = $scope.Account;
                if (dto.AccountId == 0 || dto.AccountId== undefined) {
                    dto.Addresses = [];
                    dto.Addresses.push($scope.Address);
                }
                if (dto.Addresses[0].IsResidential === false && dto.Addresses[0].IsCommercial === false) {
                    if (dto.selectedAddressType = 'Residential')
                    {
                        dto.Addresses[0].IsResidential=true;
                    }
                    else if (dto.selectedAddressType = 'Commercial') {
                        dto.Addresses[0].IsCommercial = true;
                    }
                    else {
                        $scope.addressTypeIsNotValid = true;
                        HFC.DisplayAlert("Required Address Type");
                        return;
                    }
                } else {
                    $scope.addressTypeIsNotValid = false;
                }

                if (dto.selectedAddressType = 'Residential')
                {
                        dto.Addresses[0].IsResidential=true;
                        }
                else if (dto.selectedAddressType = 'Commercial') {
                    dto.Addresses[0].IsCommercial = true;
                }
                else {
                    $scope.addressTypeIsNotValid = true;
                    HFC.DisplayAlert("Required Address Type");
                    return;
                }
                if (dto.Addresses[0].IsResidential == false && dto.Addresses[0].IsCommercial)
                {
                    HFC.DisplayAlert("Primary Person Address Type Required");
                }
                if (dto.PrimCustomer.FirstName == '' || dto.PrimCustomer.FirstName== undefined) {
                    $scope.firstNameIsNotValid = true;
                    HFC.DisplayAlert("Required Primary Person First Name");
                    return;
                }
                else {
                    $scope.firstNameIsNotValid = false
                }

                if (dto.PrimCustomer.LastName == '' || dto.PrimCustomer.LastName == undefined) {
                    $scope.lastNameIsNotValid = true;
                    HFC.DisplayAlert("Required Primary Person Last Name");
                    return;
                }
                else {
                    $scope.lastNameIsNotValid = false
                }

                if (dto.SourcesTPId == 0) {
                    HFC.DisplayAlert("Source is required");
                    return;
                }
                
                if (dto.Addresses[0].ZipCode == '' || dto.Addresses[0].ZipCode == undefined) {
                    $scope.zipCodeIsNotValid = true;
                    HFC.DisplayAlert("Required Primary Person Addresses or Zip Code");
                    return;
                }
                else {
                    if (dto.Addresses[0].ZipCode.length > 6)
                    {
                        HFC.DisplayAlert("Primary Person Zip Code invalid");
                        return;
                    }
                    $scope.zipCodeIsNotValid = false;
                }


                //murugan...
                if ((dto.PrimCustomer.CellPhone != null && dto.PrimCustomer.CellPhone != '') && dto.PrimCustomer.CellPhone.length != 10) {
                    HFC.DisplayAlert("Cell Phone invalid");
                    return;
                }

                if ((dto.PrimCustomer.HomePhone != null && dto.PrimCustomer.HomePhone != '') && dto.PrimCustomer.HomePhone.length != 10) {
                    HFC.DisplayAlert("Home Phone invalid");
                    return;
                }

                if ((dto.PrimCustomer.FaxPhone != null && dto.PrimCustomer.FaxPhone != '') && dto.PrimCustomer.FaxPhone.length != 10) {
                    HFC.DisplayAlert("Fax Phone invalid");
                    return;
                }

                if ((dto.PrimCustomer.WorkPhone != null && dto.PrimCustomer.WorkPhone != '') && dto.PrimCustomer.WorkPhone.length != 10) {
                    HFC.DisplayAlert("Work Phone invalid");
                    return;
                }

                dto.Addresses[0].AddressType = dto.Addresses[0].IsResidential;

                

                //murugan, this is not required as this is already done by the Angular.
                //dto.CampaignId = $scope.AccountCampaignslist.CampaignId.CampaignId;
                //dto.SourcesTPId = $scope.AccountSingleSourceList.SourceId.SourceId;
                dto.AccountSources = AccountSourceService.AccountSources;
                

                //if (AccountSourceService.AccountSources.length == 0) {
                //    $scope.AccountSourceIsNotValid = true;
                //    return;
                //}
                //else {
                //    $scope.AccountSourceIsNotValid = false;
                //}
                //dto.AccountSourceId = this.AccountSourcesList.Value.Value;
                if (!$scope.Account.PrimCustomer.Unsubscribed) {
                    dto.PrimCustomer.UnsubscribedOn = new Date().toUTCString();
                }

                dto.AdditionalContacts = PersonService.People;

                //if (PersonService.Person == null) {
                //    dto.SecCustomer = PersonService.People;
                //}
                //else {
                //    dto.SecCustomer = PersonService.Person;
                //}
                dto.SecCustomer = PersonService.People;
                //dto.AccountNotes = [{ Message: $scope.Account.PrimaryNotes, TypeEnum: 4 }];

                dto.AdditionalAddresses = AddressService.Addresses;
                dto.NotesAttachmentslist = PersonService.NotesAttachmentslist;

                dto.SecCustomer = PersonService.People;
                //dto.Addresses = AddressService.Addresses;
                if ($scope.AccountStatuseslist.Id)
                {
                    dto.AccountStatusId = $scope.AccountStatuseslist.Id.Id;//Account status
                    dto.DispositionId = $scope.AccountDispositionlist.Name.DispositionId;//Account status
                }
                

                //if (AccountServiceData.Addresses[0].IsResidential) {
                //    $scope.Account.selectedAddressType = "Residential";

                //} else {
                //    $scope.Account.selectedAddressType = "Commercial";
                //}

                for (i = 0; i < AddressService.Addresses.length; i++) {
                    dto.Addresses.push(AddressService.Addresses[i]);
                }
                

                dto.AccountNotes = PersonService.NotesAttachmentslist;


                if ($scope.isClosedDuplicateSave) {
                    dto.SkipDuplicateCheck = true;
                }
                
                $scope.IsBusy = true;
                $http.post('/api/Accounts', dto).then(function (response) {
                    $scope.IsBusy = false;
                    if (Account.AccountId > 0) {
                        HFC.DisplaySuccess("Account Modified");
                    }
                    else {
                        HFC.DisplaySuccess("Account created");
                    }
                    $scope.CheckDuplicatesSave = true;
                    if ($scope.isClosedDuplicateSave == false || $scope.isClosedDuplicateSave == undefined) {
                        if (response.data.lstDuplicates) {
                            $scope.lstDuplicates = response.data.lstDuplicates;
                            if ($scope.lstDuplicates.length > 0) {
                                $("#Account-duplicates").modal("show");
                                return;
                            }
                        }
                    }
                    
                    
                    $scope.AccountDispositionlist = null;
                    $scope.AccountCampaignslist = null;
                    $scope.AccountStatuseslist = null;
                    $scope.AccountSingleSourceList = null;
                    $scope.AccountChannelList = null;
                    
                    if ($scope.submitAction == 1) {
                        JobService.AddJob(response.data.AccountId, function () {
                            window.location = '#/Accounts/' + response.data.AccountId;
                        });
                    } else if ($scope.submitAction == 2) {
                        JobService.AddJob(response.data.AccountId, function (job) {
                            window.location = '/calendar/?aptType=1&jobId=' + job.JobId;
                        }, true);
                    }

                    else if ($scope.submitAction == 3) {
                        $scope.Account = {
                            PrimaryNotes: '',
                            PrimCustomer: {
                                FirstName: '',
                                LastName: '',
                                CompanyName: '',
                                WorkTitle: '',
                                HomePhone: '',
                                CellPhone: '',
                                FaxPhone: '',
                                WorkPhone: '',
                                WorkPhoneExt: '',
                                PrimaryEmail: '',
                                SecondaryEmail: '',
                                PreferredTFN: 'C',
                                Unsubscribed: true,
                                UnsubscribedOn: '',
                                Origination: '',
                                ActionTaken: '',
                                CrossStreet: '',
                                Disposition: '',
                                SalesStep: '',
                                LocationInfo: ''
                            },
                            SecCustomer: {
                                FirstName: '',
                                LastName: '',
                                CompanyName: '',
                                WorkTitle: '',
                                HomePhone: '',
                                CellPhone: '',
                                FaxPhone: '',
                                WorkPhone: '',
                                WorkPhoneExt: '',
                                PrimaryEmail: '',
                                SecondaryEmail: '',
                                PreferredTFN: 'C',
                                Unsubscribed: true,
                                UnsubscribedOn: ''
                            },
                        };

                        $scope.Address =
                        {
                            IsResidential: false,
                            IsCommercial: false,
                            AddressType: 'false',
                            AttentionText: '',
                            Address1: '',
                            Address2: '',
                            City: '',
                            State: '',
                            ZipCode: '',
                            CountryCode2Digits: HFCService.FranchiseCountryCode,
                            CrossStreet: '',
                            Location: ''
                        };
                        AccountSourceService.AccountSources = [];
                        window.location.href = '#!/Accounts/' + response.data.AccountId;
                    }
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
            };


            $scope.cancel = function () {
                window.close();
            }

        $scope.CheckDuplicates = function(type, e) {
            
            if ($("#Account-duplicates").css("display") == 'block') {
                    return;
                }
                var creteria = '';
                var dto = $scope.Account;
                switch (type) {
                    case 'CellPhone':
                        if (dto.PrimCustomer.CellPhone != "" && dto.PrimCustomer.CellPhone.length == 10) {
                            creteria = dto.PrimCustomer.CellPhone;
                        }
                        else {
                            return
                        }
                        
                        break;
                    case 'HomePhone':
                        if (dto.PrimCustomer.HomePhone != "" && dto.PrimCustomer.HomePhone.length == 10) {
                            creteria = dto.PrimCustomer.HomePhone;
                        }
                        else {
                            return
                        }
                        break;
                    case 'PrimaryEmail':
                        if (!dto.PrimCustomer.PrimaryEmail) {
                            return;
                        }
                        if (dto.PrimCustomer.PrimaryEmail.indexOf('@') == -1 || dto.PrimCustomer.PrimaryEmail.indexOf('.') == -1) {
                            return;
                        }
                        creteria = dto.PrimCustomer.PrimaryEmail;
                        break;
                    case 'FaxPhone':
                        if (dto.PrimCustomer.FaxPhone != "" && dto.PrimCustomer.FaxPhone.length == 10) {
                            creteria = dto.PrimCustomer.FaxPhone;
                        }
                        else {
                            return
                        }
                        break;
                    case 'WorkPhone':
                        if (dto.PrimCustomer.WorkPhone != "" && dto.PrimCustomer.WorkPhone.length == 10) {
                            creteria = dto.PrimCustomer.WorkPhone;
                        }
                        else {
                            return
                        }
                        break;
                    case 'SecondaryEmail':
                        if (!dto.PrimCustomer.SecondaryEmail) {
                            return;
                        }
                        if (dto.PrimCustomer.SecondaryEmail.indexOf('@') == -1 || dto.PrimCustomer.SecondaryEmail.indexOf('.') == -1) {
                            return;
                        }
                        creteria = dto.PrimCustomer.SecondaryEmail;
                        break;
                    default:

                }
                
                $http.post('/api/Accounts/0/CheckDuplicates/', { SearchTerm: creteria, SearchType: type }).then(function (response) {

                    $scope.lstDuplicates = response.data.lstDuplicates;
                    if ($scope.lstDuplicates.length > 0) {
                        $("#Account-duplicates").modal("show");
                    }
                },
                function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });

                return true;
            }

            $scope.DuplicatesModalClose = function () {
                if ($scope.CheckDuplicatesSave) {
                    $scope.isClosedDuplicateSave = true;
                }
                
                $("#Account-duplicates").modal("hide");
            }
        }
    ]).directive('hfcAccountDuplicateModal', ['cacheBustSuffix', function ( cacheBustSuffix) {
        return {
            restrict: 'E',
            replace: true,
            controller: 'accountEditorController',
            templateUrl: '/templates/NG/Account/account-duplicates-modal.html?cache-bust=' + cacheBustSuffix
        }
    }]);




