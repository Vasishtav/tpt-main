﻿'use strict';

// TODO: required refacatoring.

/**
* Scrolls the container to the target position minus the offset
*
* @param target    - the destination to scroll to, can be a jQuery object
*                    jQuery selector, or numeric position
* @param offset    - the offset in pixels from the target position, e.g.
*                    pass -80 to scroll to 80 pixels above the target
* @param speed     - the scroll speed in milliseconds, or one of the
*                    strings "fast" or "slow". default: 500
* @param container - a jQuery object or selector for the container to
*                    be scrolled. default: "html, body"
*/
jQuery.scrollTo = function (target, offset, speed, container) {

    if (isNaN(target)) {

        if (!(target instanceof jQuery))
            target = $(target);

        target = parseInt(target.offset().top);
    }

    container = container || "html, body";
    if (!(container instanceof jQuery))
        container = $(container);

    speed = speed || 500;
    offset = offset || 0;

    container.animate({
        scrollTop: target + offset
    }, speed);
};

app.controller('accountDetailsController', ['$scope', '$routeParams', '$rootScope', '$window'
    , '$location', '$http', 'HFCService', 'LeadSourceService', 'JobService', 'AddressService'
    , 'PersonService', 'JobNoteService', 'InvoiceService', 'PurchaseOrderFilter'
    , 'PaymentService', 'AdvancedEmailService', 'LeadAccountService', 'AttendeeService'
    , 'OpportunityService', 'NewtaskService', 'NavbarService', 'NoteServiceTP'
    , 'AdditionalContactServiceTP', 'AdditionalAddressServiceTP', 'FileprintService'
    , 'AddressValidationService', '$q', 'alertModalService'
    , function ($scope, $routeParams, $rootScope, $window
        , $location, $http, HFCService, LeadSourceService, JobService, AddressService
        , PersonService, JobNoteService, InvoiceService, PurchaseOrderFilter
        , PaymentService, AdvancedEmailService, LeadAccountService, AttendeeService
        , OpportunityService, NewtaskService, NavbarService, NoteServiceTP
        , AdditionalContactServiceTP, AdditionalAddressServiceTP, FileprintService
        , AddressValidationService, $q, alertModalService) {

        //we will use local ctrl var so we don't clutter up $scope that doesn't need to be shared with nested controllers and directives
        var ctrl = this;

        // Notes Integration.
        $scope.NoteServiceTP = NoteServiceTP;
        $scope.NoteServiceTP.AccountId = $routeParams.accountId;
        $scope.NoteServiceTP.ParentName = "Account";

        $scope.HFCService = HFCService;
        //texting
        $scope.BrandId = $scope.HFCService.CurrentBrand;
        $scope.FranciseLevelTexting = $scope.HFCService.FranciseLevelTexting;
        $scope.disablenotifytext = true;
        $scope.IsNotifyemails = true;
        $scope.Required = true;

        // Fix for TP-1588	Attachments - duplicating
        $scope.NoteServiceTP.FranchiseSettings = false;

        var Accountpermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Account');
        $scope.AccountPermission = Accountpermission;

        //$scope.NoteServiceTP.showAddNotes = Accountpermission.CanRead;//HFCService.GetPermissionTP('Add Notes&Attachments-Account').CanAccess;
        //$scope.NoteServiceTP.showEditNotes = Accountpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Notes&Attachments-Account').CanAccess;

        // Additional Contact Integration:
        $scope.AdditionalContactServiceTP = AdditionalContactServiceTP;
        $scope.AdditionalContactServiceTP.ParentName = "Account";
        //$scope.AdditionalContactServiceTP.showAddoption = Accountpermission.CanRead;//HFCService.GetPermissionTP('Add Additional Contacts-Account').CanAccess;
        //$scope.AdditionalContactServiceTP.showEditoption = Accountpermission.CanRead;//HFCService.GetPermissionTP('Edit Additional Contact-Account').CanAccess;
        $scope.AdditionalContactServiceTP.AccountId = $routeParams.accountId;
        // Appointment
        HFCService.GetUrl();

        // Additional Address
        $scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
        $scope.AdditionalAddressServiceTP.ParentName = "Account";
        //$scope.AdditionalAddressServiceTP.showAddAddress = Accountpermission.CanRead;//HFCService.GetPermissionTP('Add Additional Address-Account').CanAccess;
        //$scope.AdditionalAddressServiceTP.showEditAddress = Accountpermission.CanRead;//HFCService.GetPermissionTP('Edit Additional Address-Account').CanAccess
        $scope.AdditionalAddressServiceTP.AccountId = $routeParams.accountId;
        $scope.Account = {};

        // display Territory
        $scope.TerritoryDisplayId = 0;
        $http.get('/api/Accounts/0/getTerritoryForFranchise').then(function (response) {
            if (response.data != null) {

                if (!$scope.Account)
                    $scope.Account = {};

                if (response.data.length > 0) {
                    $scope.DisplayTerritorySource = response.data;
                }
                else {
                    $scope.DisplayTerritorySource = [];
                }

            }

            $scope.DisplayTerritorySource.splice(0, 0, { Id: 0, Name: 'Select' });
        });




        $scope.NavbarService = NavbarService;

        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesAccountsTab();

        // This is very much required for Angular to track otherwise the current implementation won't work.
        $scope.StateList = [];

        $scope.AdvancedEmailService = AdvancedEmailService;
        $scope.JobNoteService = JobNoteService;
        //$scope.AccountNoteService = AccountNoteService;
        $scope.LeadAccountService = LeadAccountService;
        $scope.JobService = JobService;
        HFCService.GetUrl();


        $scope.franchiseAddressObject = null;
        $scope.LeadSourceService = LeadSourceService;
        $scope.AttendeeService = AttendeeService;
        $scope.AccountStuseslist = [];

        $scope.AccountCampaignslist = [];
        $scope.HFCService = HFCService;
        $scope.PersonService = PersonService;
        $scope.AddressService = AddressService;
        $scope.InvoiceService = InvoiceService;
        $scope.PaymentService = PaymentService;
        $scope.OpportunityService = OpportunityService;
        $scope.NewtaskService = NewtaskService;
        $scope.FileprintService = FileprintService;
        $scope.accountPopAddress = [];

        $scope.minDate = new Date();
        $scope.maxDate = new Date('2099/12/31');
        $scope.BrandId = $scope.HFCService.CurrentBrand;

        $scope.accountURL = window.location.href.split('#!');
        $scope.accountURL = $scope.accountURL[1].split('/');
        $scope.accountPathType = $scope.accountURL[1];
        $scope.accountPathId = $scope.accountURL[2];
        var accountId = $routeParams.accountId;

        $scope.Address =
                {
                    IsResidential: false,
                    IsCommercial: false,
                    AddressType: 'false',
                    AttentionText: '',
                    Address1: '',
                    Address2: '',
                    City: '',
                    Country: '',
                    State: '',
                    ZipCode: '',
                    CountryCode2Digits: HFCService.FranchiseCountryCode,
                    CrossStreet: '',
                    Location: ''
                };


        $http.get('/api/lookup/0/AccountStatus').then(function (data) {
            $scope.AccountStatuseslist = data.data;
            if (accountId == undefined) {

                $scope.Account.AccountStatusId =0
                $scope.Account.AccountStatusId = 1;

            }
        });
        $http.get('/api/lookup/0/Campaigns').then(function (data) {
            $scope.AccountCampaignslist = data.data;
            if (accountId == undefined) {
                $scope.Account.CampaignId = 0;
            }
        });


        $http.get('/api/lookup/0/ReferralType').then(function (data) {
            $scope.referralTypeList = data.data;
        });
        //Get the Industries

        $http.get('/api/lookup/1/Type_LookUpValues').then(function (data) {
            $scope.AccountIndustries = data.data;
        });
        //Get the Commercial Type
        $http.get('/api/lookup/2/Type_LookUpValues').then(function (data) {
            $scope.AccountCommercialType = data.data;

        });

        $scope.Permission = {};
        var Opportunitypermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity')
        var Invoicepermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Invoice')
        var SalesOrderpermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SalesOrder')
        var Paymentpermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Payment')

        $scope.Permission.AddAccount = Accountpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Account').CanAccess;
        $scope.Permission.ListOpportunity = Opportunitypermission.CanRead; //$scope.HFCService.GetPermissionTP('List Opportunity').CanAccess;
        $scope.Permission.AccountInvoice = Invoicepermission.CanRead; //$scope.HFCService.GetPermissionTP('List Invoice').CanAccess;
        $scope.Permission.AccountOrderList = SalesOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('List Order').CanAccess;
        $scope.Permission.AccountPayment = Paymentpermission.CanRead; //$scope.HFCService.GetPermissionTP('List Payments').CanAccess;
        //$scope.Permission.AccountOrderList = $scope.HFCService.GetPermissionTP('List Order').CanAccess;
        $scope.Permission.AccountView = Accountpermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Account').CanAccess;
        $scope.Permission.AccountEdit = Accountpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Account').CanAccess;
        //$scope.Permission.AddEvent = $scope.HFCService.GetPermissionTP('Add Event').CanAccess;
        //$scope.Permission.AccountQualify = $scope.HFCService.GetPermissionTP('Qaulify Account').CanAccess;
        $scope.Permission.AddNotesAccount = Accountpermission.CanCreate;//HFCService.GetPermissionTP('Add Notes&Attachments-Account').CanAccess

        $scope.CreateNewAccount = function () {

            $window.location.href = "#!/account";
        }
        $scope.EditAccount = function (accountId) {
            $window.location.href = "#!/accountEdit/" + accountId;
        }

        $scope.cancel = function () {

          //  var url = "http://" + $window.location.host + "/#!/accountsQualify/" + $scope.Account.AccountId;
           // $window.location.href = url;
            $window.location.href = "#!/accountsQualify/" + $scope.Account.AccountId;
        }
        $scope.cancelAccountEdit = function () {
            //window.history.back();
            $scope.showValidate = false;

            if (accountId > 0) {
                window.location.href = '#!/Accounts/' + accountId;
            } else {
                window.location.href = '#!/accountSearch';
            }

        }
        //For editing the Account Qualify
        $scope.AccountEditQualify = function () {

            //var url = "http://" + $window.location.host + "/#!/accountsEditQualify/" + $scope.Account.AccountId;
            //$window.location.href = url;
            $window.location.href = "#!/accountsEditQualify/" + $scope.Account.AccountId;
        };

        $scope.CountryChanged = function () {
            if ($scope.Account.Addresses != null) {
                $scope.Account.Addresses[0].City = '';
                $scope.Account.Addresses[0].State = '';
                $scope.Account.Addresses[0].ZipCode = '';
                $scope.Account.Addresses[0].CrossStreet = '';
            }
        }

        $scope.ResetStateCountry = function (mode) {
            if (mode == 'new') {
                if ($scope.Address.ZipCode == '' || $scope.Address.ZipCode == undefined) {
                    $scope.Address.City = '';
                    $scope.Address.State = '';
                }
            }
            if (mode == 'edit') {
                if ($scope.Lead.Addresses[0].ZipCode == '' || $scope.Lead.Addresses[0].ZipCode == undefined) {
                    $scope.Lead.Addresses[0].City = '';
                    $scope.Lead.Addresses[0].State = '';
                }
            }
        }
        $scope.EditAccountAddressChanges = function () {
            if ($scope.Account.Addresses[0].ZipCode) {

                $http.get('/api/lookup/0/zipcodes?q=' + $scope.Account.Addresses[0].ZipCode + "&country=" + $scope.Account.Addresses[0].CountryCode2Digits).then(function (response) {
                    $scope.IsBusy = false;
                    if (response.data.zipcodes.length > 0) {
                        $scope.Account.Addresses[0].City = response.data.zipcodes[0].City;
                        $scope.Account.Addresses[0].State = response.data.zipcodes[0].State;
                        $scope.Account.Addresses[0].Country = response.data.zipcodes[0].CountryCode;
                        //$scope.AddressService.Addresses.State = response.data.zipcodes[0].State;
                    };
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
            }

        }
        $scope.$watch('Address.ZipCode', function () {

            if ($scope.Address.ZipCode) {

                $http.get('/api/lookup/0/zipcodes?q=' + $scope.Address.ZipCode + "&country=" + $scope.Address.CountryCode2Digits).then(function (response) {
                    $scope.IsBusy = false;
                    if (response.data.zipcodes.length > 0) {
                        $scope.Address.City = response.data.zipcodes[0].City;
                        $scope.Address.State = response.data.zipcodes[0].State;
                        $scope.Address.Country = response.data.zipcodes[0].CountryCode;
                        $scope.AddressService.Addresses.State = response.data.zipcodes[0].State;
                    };
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
            }
        });



        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        $scope.RelatedAccounts = function () {

            $scope.RelatedAccounts = {
                placeholder: "Select",
                dataTextField: "FullName",
                dataValueField: "AccountId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: function (params) {
                                if (accountId == undefined) {

                                    var url1 = "/api/lookup/0/GetRelatedAccounts";
                                    dataType: "json";
                                    return url1;
                                }
                                else
                                {
                                    var url1 = "/api/lookup/" + accountId + "/GetRelatedAccounts";
                                    dataType: "json";
                                    return url1;
                                }
                            } ,
                        }
                    }
                },
            };

        }
        $scope.SelectedRelatedAccountIds = [];
        $scope.EditAccountAddressChange = function () {

            if ($scope.Account.Addresses[0].ZipCode) {

                $http.get('/api/lookup/0/zipcodes?q=' + $scope.Account.Addresses[0].ZipCode + "&country=" + $scope.Account.Addresses[0].ZipCode.CountryCode2Digits).then(function (response) {
                    $scope.IsBusy = false;
                    if (response.data.zipcodes.length > 0) {

                        $scope.Account.Addresses[0].City = response.data.zipcodes[0].City;
                        $scope.Account.Addresses[0].State = response.data.zipcodes[0].State;
                        $scope.Account.Addresses[0].Country = response.data.zipcodes[0].CountryCode;
                        //$scope.AddressService.Addresses.State = response.data.zipcodes[0].State;
                    };
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
            }

        }

        $scope.ShowAccountQualify = function (accountId) {

            window.location.href = '#!/accountsQualify/' + accountId;
        }

        $scope.viewPage = function (url) {
            window.location = url;
        }
        $http.get('/api/lookup/0/QuickDispositions').then(function (data) {
            $scope.QuickDispositionList = data.data;
        });


        //accountDetailsController

        $scope.External = {};

        $scope.refreshNotesGrid = function () {

            var result = $scope.External.Notes;
            $scope.NoteServiceTP.RefreshNotesGrid(result);
        }

        $scope.refreshAdditionalContactsGrid = function () {

            var result = $scope.External.AdditionalContacts;
            $scope.AdditionalContactServiceTP.RefreshGrid(result);
        }

        $scope.refreshAdditionalAddressGrid = function () {

            var result = $scope.External.AdditionalAddress;
            $scope.AdditionalAddressServiceTP.RefreshGrid(result);
        }


        //To save Qualify
        $scope.SaveQualify = function () {
            //Added for Qualification/Question Answers
            //To save Qualify - Question
            $scope.AccountId = $scope.Account.AccountId;
            $scope.SaveQAlists($scope.AccountId);
            //Save alert message
            HFC.DisplaySuccess("Qualify saved");
            // Move the url the Lead listing page.
            //window.location.href = '#!/accountsQualify/' + $scope.Account.AccountId;
            // Move the url the Lead listing page.
            var goLeadQualify = '#!/accountsQualify/' + $scope.Account.AccountId;
            //
            $location.path(goLeadQualify, false);
            $window.open(goLeadQualify, '_self', '', true);
            $window.location.reload(true);

        }
        $scope.RelatedAccounts();


        //***************************************
        $scope.GetAccountZillowinfo = function (address, citystatezip) {
            //var address = "15018 Rixeyville Rd";
            //var citystatezip = "Culpeper, VA 22701";

            $http.get('/api/zillow/?address=' + address + '&citystatezip=' + citystatezip).then(function (data) {

                if (data.data.status == true) {
                    $scope.zillowdata = data.data.ZilloResponce;
                    $scope.ShowZillowlink = true;
                }
                else
                    $scope.ShowZillowlink = false;
            },
            function (error) {
                console.log("Error: " + error);
            });

        }
        //****************Start-Tafsir***************************
        $scope.onMouseDownAccount = function (action) {

            $scope.submitAction = action;
            $scope.SaveAccount(2, 0);
        }
        $scope.returnToAccountList = function () {
            $scope.showValidate = false;
            window.location.href = '#!/accountSearch/';
        }

        $scope.NavigateToLeadListPage = function () {
            var listPage = '#!/leadsSearch/';
            $window.open(listPage, '_self');
            location.reload(true);
        }
        $scope.NavigateToAccoutsOpportunitiesListPage = function (AccountId) {
            var listPage = '#!/accountopportunitySearch/' + AccountId;
            $window.open(listPage, '_self');
            location.reload(true);
        }
        $scope.SaveAccount = function (operation, newacccount) {
           // $scope.CloseNotifyTextpopup();
            $scope.Accntsubmit = true;

            $scope.submitAction = operation;

            // This is a hack to fix "Canada zipcode not allowing to save without editing" issue.
            // http://plnkr.co/edit/wCWTk9My0LFqEpKa4r3a?p=preview
            // https://github.com/angular/angular.js/issues/8527
            $scope.frm_accnt.formAddressValidation.zipCode.$commitViewValue(true);

            if ($scope.Account.AccountStatusId <= 0) {
                return;
            }
            if ($scope.Account.SourcesTPId == undefined || !($scope.Account.SourcesTPId>0)) {
                return;
            }
            if ($scope.frm_accnt.$invalid) {
                return;
            }

            var dto = $scope.Account;
            //warning for duplicate email
            if (!(dto.PrimCustomer.PrimaryEmail == "" && dto.PrimCustomer.SecondaryEmail == "") && !(dto.PrimCustomer.PrimaryEmail == null && dto.PrimCustomer.SecondaryEmail == null) && !(dto.PrimCustomer.PrimaryEmail == undefined && dto.PrimCustomer.SecondaryEmail == undefined) && (dto.PrimCustomer.PrimaryEmail === dto.PrimCustomer.SecondaryEmail)) {
                HFC.DisplayAlert("Secondary Email should not be same as Primary Email");
                return;
            }

            dto.AllowFranchisetoReassign = true;
            if (dto.AccountId == 0 || dto.AccountId == undefined) {
                dto.Addresses = [];
                dto.Addresses.push($scope.Address);
            }

            if (dto.IsBusiness == true) {
                dto.Addresses[0].IsResidential = false;
                dto.Addresses[0].AddressType = "false"
            } else if (dto.IsBusiness == false) {
                dto.Addresses[0].IsResidential = true;
                dto.Addresses[0].AddressType = "true"
            }
            //if (!$routeParams.accountId) {
            //    //$scope.Account.AccountType = 'Key/Commercial';
            //    //$scope.Account.AccountTypeId = 2;

            //}

            //if(!$scope.Account.AccountTypeId) {
            //  $scope.Account.AccountTypeId = 10008;
            //}

            if ($scope.Account.IsCommercial == false || $scope.Account.IsCommercial == undefined)
            {
                $scope.Account.RelationshipTypeId = null;
            }
            if ($scope.Account.PrimCustomer.UnsubscribedOn != null) {
                if (!$scope.Account.PrimCustomer.UnsubscribedOn) {
                    dto.PrimCustomer.UnsubscribedOn = new Date().toUTCString();
                }
            }
            var PreferredPhone = " ";//default cell
            if ($("input[name='preferredTFNC']:checked").val() != undefined) {
                PreferredPhone = "C"
            }
            if ($("input[name='preferredTFNH']:checked").val() != undefined) {
                PreferredPhone = "H"
            }
            if ($("input[name='preferredTFNW']:checked").val() != undefined) {
                PreferredPhone = "W"
            }
            dto.PrimCustomer.PreferredTFN = PreferredPhone;

            dto.AdditionalContacts = PersonService.People;

            dto.SecCustomer = PersonService.People;

            dto.AdditionalAddresses = AddressService.Addresses;
            dto.NotesAttachmentslist = PersonService.NotesAttachmentslist;

            dto.SecCustomer = PersonService.People;

            dto.AccountNotes = PersonService.NotesAttachmentslist;

            if ($scope.isClosedDuplicateSave) {
                dto.SkipDuplicateCheck = true;
            }

            $scope.IsBusy = true;



            if (!angular.isNumber(dto.SourcesTPId)) {
                dto.SourcesTPId = dto.SourcesTPId[0];
            }




            if ($scope.SelectedRelatedAccountIds) {
                if ($scope.SelectedRelatedAccountIds.length != undefined) {
                    dto.RelatedAccountId = $scope.SelectedRelatedAccountIds[0];
                }
                else if ($scope.SelectedRelatedAccountIds != null) {
                    dto.RelatedAccountId = $scope.SelectedRelatedAccountIds;
                }
            }
            validateAddress(dto)
            .then(function (response) {
                var objaddress = response.data;

                if (objaddress.Status == "error") {
                    //TP-1793: CLONE - The request failed with HTTP status 503: Service Temporarily Unavailable.
                    AfterValidateAddress(dto);
                    //HFC.DisplayAlert(objaddress.Message);
                    //$scope.IsBusy = false;
                } else if (objaddress.Status == "true") {
                    var validAddress = objaddress.AddressResults[0];
                    dto.Addresses[0].Address1 = validAddress.address1;
                    $scope.Addressvalues = dto.Addresses[0].Address1;
                    dto.Addresses[0].address2 = validAddress.address2;
                    dto.Addresses[0].City = validAddress.City;
                    dto.Addresses[0].State = validAddress.StateOrProvinceCode;

                    // Update that the address validated to true.
                    dto.Addresses[0].IsValidated = true;

                    // assign the zipcode back to the model before saving.
                    if (dto.Addresses[0].CountryCode2Digits == "US") {
                        dto.Addresses[0].ZipCode = validAddress.PostalCode.substring(0, 5);
                    }
                    else {
                        dto.Addresses[0].ZipCode = validAddress.PostalCode;
                    }
                    $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                    AfterValidateAddress(dto);
                } else if (objaddress.Status == "false") {

                    dto.Addresses[0].IsValidated = false;

                    alertModalService.setPopUpDetails(true, "Confirmation", "This address is marked as invalid. An invalid address can result in incorrect sales tax values. Click cancel to provide a new address or click ok to continue.", AfterValidateAddress, dto);
                  /*  if (confirm(errorMessage)) {
                        AfterValidateAddress(dto);
                    }
                    else {
                        $scope.IsBusy = false;
                    } */
                    $scope.Addressvalues = dto.Addresses[0].Address1;
                    //address get empty so assiging the value here.
                    $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                } else { //SkipValidation
                    AfterValidateAddress(dto);
                }
            },
            function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });

        };

        function AfterValidateAddress(dto) {

            //code for spacing in Canada Postal/ZipCode
            if (!dto.Addresses[0].IsValidated) {
                if (dto.Addresses[0].CountryCode2Digits == "CA") {
                    if (dto.Addresses[0].ZipCode) {
                        var Ca_Zipcode = dto.Addresses[0].ZipCode;
                        Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                        dto.Addresses[0].ZipCode = Ca_Zipcode;
                    }
                }
            }
            //for postal code display
            if (dto.Addresses[0].CountryCode2Digits == "CA") {
                if (dto.Addresses[0].ZipCode) {
                    if (dto.Addresses[0].ZipCode.length == 6) {
                        var Ca_Zipcode = dto.Addresses[0].ZipCode;
                        Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                        dto.Addresses[0].ZipCode = Ca_Zipcode;
                    }
                }
            }

            $http.post('/api/Accounts', dto).then(function (response) {
                $scope.IsBusy = false;

                if (response.config.data.AccountId > 0) {
                    HFC.DisplaySuccess("Account Modified");
                }
                if (response.config.data.AccountId > 0) {
                    HFC.DisplaySuccess("Account created");
                }

                $scope.CheckDuplicatesSave = true;
                if ($scope.isClosedDuplicateSave == false || $scope.isClosedDuplicateSave == undefined) {
                    if (response.data.lstDuplicates) {
                        $scope.lstDuplicates = response.data.lstDuplicates;
                        if ($scope.lstDuplicates.length > 0) {
                            $("#Account-duplicates").modal("show");
                            return;
                        }
                    }
                }

                $scope.AccountDispositionlist = null;
                $scope.AccountCampaignslist = null;
                $scope.AccountStatuseslist = null;
                $scope.AccountSingleSourceList = null;
                $scope.AccountChannelList = null;

                if ($scope.submitAction == 1) {
                    $scope.frm_accnt.$setPristine();
                    window.location = '#!/Accounts/' + response.data.accountId;
                } else if ($scope.submitAction == 2) {
                    $scope.frm_accnt.$setPristine();
                    window.location = '#!/Accounts/' + response.data.accountId;
                }

                else if ($scope.submitAction == 3) {

                    $scope.Account = {
                        //PrimaryNotes: '',
                        PrimCustomer: {
                            FirstName: '',
                            LastName: '',
                            CompanyName: '',
                            WorkTitle: '',
                            HomePhone: '',
                            CellPhone: '',
                            FaxPhone: '',
                            WorkPhone: '',
                            WorkPhoneExt: '',
                            PrimaryEmail: '',
                            SecondaryEmail: '',
                            PreferredTFN: 'C',
                            Unsubscribed: true,
                            UnsubscribedOn: '',
                            Origination: '',
                            ActionTaken: '',
                            CrossStreet: '',
                            Disposition: '',
                            SalesStep: '',
                            LocationInfo: '',
                            TerritoryDisplayId : 0
                        },
                        SecCustomer: {
                            FirstName: '',
                            LastName: '',
                            CompanyName: '',
                            WorkTitle: '',
                            HomePhone: '',
                            CellPhone: '',
                            FaxPhone: '',
                            WorkPhone: '',
                            WorkPhoneExt: '',
                            PrimaryEmail: '',
                            SecondaryEmail: '',
                            PreferredTFN: 'C',
                            Unsubscribed: true,
                            UnsubscribedOn: ''
                        },
                    };

                    $scope.Address =
                    {
                        IsResidential: false,
                        IsCommercial: false,

                        AddressType: 'false',
                        AttentionText: '',
                        Address1: '',
                        Address2: '',
                        City: '',
                        State: '',
                        ZipCode: '',
                        CountryCode2Digits: HFCService.FranchiseCountryCode,
                        CrossStreet: '',
                        Location: ''
                    };

                    $scope.frm_accnt.$setPristine();
                    window.location.href = '#!/accountSearch/';
                }
                else {
                    window.location.href = '#!/accountSearch/';
                }
            }, function (response) {

                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            })
        }

        function validateAddress(dto) {
            var addressModeltp = {
                address1: dto.Addresses[0].Address1,
                address2: dto.Addresses[0].Address2,
                City: dto.Addresses[0].City,
                StateOrProvinceCode: dto.Addresses[0].State,
                PostalCode: dto.Addresses[0].ZipCode,
                CountryCode: dto.Addresses[0].CountryCode2Digits
            };

            if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                var result = {
                    data: {
                        Status: "SkipValidation",
                        Message: "No Address validation required."
                    },
                    status: 200,
                    statusText: "OK"
                }

                return $q.resolve(result);;
            }

            // Call the api to Return the result.
            return AddressValidationService.validateAddress(addressModeltp);
        }


        $scope.ChangeCampaign1 = function () {

            var campaignId = $scope.Account.CampaignId;
            if ($scope.Account.CampaignId == undefined || $scope.Account.CampaignId == null) {
                campaignId = 0;
                return;
            }

            $http.get('/api/lookup/' + campaignId + '/SourceTp').then(function (data) {
                $scope.AccountSingleSourceList = [];

                $scope.AccountSingleSourceList = data.data;
                if ($scope.Account.AccountId != undefined) {
                    $scope.franchiseFlag = $scope.Account.AllowFranchisetoReassign;
                }
                else
                    $scope.franchiseFlag = true;
                if ($scope.franchiseFlag == true) {


                    //if ($scope.Account.SourcesTPId == 57) {
                        $scope.Account.SourcesTPId = $scope.AccountSingleSourceList[0].SourceId;
                        $scope.selectedSourceName = $scope.AccountSingleSourceList[0].name;
                        $scope.ChangeSource();
                    //}
                }
                else if ($scope.franchiseFlag == false) {
                        $scope.Account.SourcesTPId = $scope.AccountSingleSourceList[0].SourceId;
                        $scope.selectedSourceName = $scope.AccountSingleSourceList[0].name;
                        $scope.ChangeSource();

                }
            });
        }
        $scope.ChangeSource = function () {

               // $scope.Account.SourcesTPId = $scope.Account.SourcesTPId[0];

            if ($scope.Account.SourcesTPId != undefined) {
                $scope.AccountChannelList = {};
                var id = $scope.Account.SourcesTPId;
                $http.get('/api/lookup/' + id + '/ChannelBySource').then(function (data) {

                    $scope.AccountChannelList = data.data;
                });
            }
        }

        $scope.account_campaign = function () {

            $scope.account_campaign = {
                optionLabel: {
                    Name: "Select",
                    CampaignId: ""
                },
                dataTextField: "Name",
                dataValueField: "CampaignId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                change: function (data) {
                    var val = this.value();
                    if (val.length > 0) {
                        $scope.ChangeCampaign1()
                    }
                },
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/Campaigns",
                        }
                    }
                },
            };
        }
        $scope.account_campaign();
        $scope.RelatedSource = function () {

            $scope.RelatedSource = {
                placeholder: "Select",
                dataTextField: "name",
                dataValueField: "SourceId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/SourceTP",
                        }
                    }
                },
            };
        }
        $scope.RelatedSource();
        //****************End-Tafsir***************************

        $scope.modalState = {
            loaded: true
        }

        $scope.$watch('modalState.loaded', function () {
            var loadingElement = document.getElementById("loading");

            if (!$scope.modalState.loaded) {
                loadingElement.style.display = "block";
            } else {
                loadingElement.style.display = "none";
            }
        });

        function errorHandler(reseponse) {
            //

            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
        }



        if (accountId > 0) {
            // Inform that the modal is not yet ready
            $scope.modalState.loaded = false;

            $scope.LeadAccountService.Get(accountId, function (accountServiceData) {

              //  $scope.Account = accountServiceData.Account;


                var promise1 = new Promise(function (resolve, reject) {
                    resolve( $scope.Account = accountServiceData.Account);
                });

                promise1.then(function (value) {
                   var AccPromise= value;

                    $http.get('/api/FranchiseSettings/0/GetDisplayTerritoryId').then(function (data) {

                        if (AccPromise.TerritoryDisplayId > 0) {
                            // $scope.Account.TerritoryDisplayId = $scope.Account.TerritoryDisplayId;
                            $scope.TerritoryDisplayId = AccPromise.TerritoryDisplayId;
                        }
                        else {
                            if (data.data) {
                                $scope.TerritoryDisData = data.data;
                                $scope.Account.TerritoryDisplayId = data.data.TerritoryId;
                                $scope.TerritoryDisplayId = data.data.TerritoryId;

                            } else {
                                $scope.Account.TerritoryDisplayId = 0;
                                $scope.TerritoryDisplayId = 0;

                            }
                        }



                    });


                });


                //texting
                $scope.FranciseLevelTexting = accountServiceData.FranciseLevelTexting;
                $scope.Account.TextStatus = accountServiceData.TextStatus;
                $scope.IsNotifyTextCopy = $scope.Account.IsNotifyText;
                $scope.IsNotifyemails = $scope.Account.IsNotifyemails;
                $scope.CellPhoneCopy = $scope.Account.PrimCustomer.CellPhone;

                if (!$scope.Account.IsNotifyemails && document.getElementsByName('accnt_email')[0] != undefined) {
                    document.getElementsByName('accnt_email')[0].placeholder = '';
                    $scope.Required = false;
                }

                if ($scope.Account.PrimCustomer.CellPhone == null) {
                    $scope.disablenotifytext = true;
                }
                else {
                    if ($scope.Account.TextStatus == null) {
                        $scope.Account.NotifyTextStatus = 'new';
                        $scope.disablenotifytext = false;
                    }
                    else if ($scope.Account.TextStatus.Optout == true) {
                        $scope.Account.NotifyTextStatus = 'Optout';
                        $scope.disablenotifytext = false;
                    }
                    else if ($scope.Account.TextStatus.Optin == true) {
                        $scope.Account.NotifyTextStatus = 'Optin';
                        $scope.disablenotifytext = false;
                    }
                    else if ($scope.Account.TextStatus.IsOptinmessagesent == true) {
                        $scope.Account.NotifyTextStatus = 'waiting';
                        $scope.disablenotifytext = false;
                    }
                    else if ($scope.Account.TextStatus.Optout == null && $scope.Account.TextStatus.Optin == null && $scope.Account.TextStatus.IsOptinmessagesent == false) {
                        $scope.Account.NotifyTextStatus = 'error';
                        $scope.disablenotifytext = false;
                    }
                }



                //if ($scope.Account.TerritoryDisplayId && $scope.Account.TerritoryDisplayId > 0)
                //    $scope.TerritoryDisplayId = $scope.Account.TerritoryDisplayId;
                //else {
                //    $scope.TerritoryDisplayId = 0;
                //    $scope.Account.TerritoryDisplayId = 0;

                //    angular.forEach($scope.DisplayTerritorySource, function (value, key) {
                //        if (value.Name.toUpperCase().includes('GRAY AREA')) {

                //            $scope.TerritoryDisplayId = value.Id;
                //            $scope.Account.TerritoryDisplayId = value.Id;
                //        }
                //    });
                //}




                if ($scope.Account && $scope.Account.SourcesList.length>0) $scope.selectedSourceName = $scope.Account.SourcesList[0].name;

                $scope.Address = $scope.Account.Addresses[0];
                var franchiseAddressObject = accountServiceData.FranchiseAddressObject;
                //this is temporary until we change accounts to use an array of customer so we can have more than 2 people per account
                PersonService.People = [];
                $scope.PersonSecondaryPeople = [];
                $scope.SelectedAccountStatus = accountServiceData.SelectedAccountStatus;
                $scope.accounttypeId = accountServiceData.AccountTypeId;
                //if ($scope.Account) {
                //    $http.get('/api/Accounts/' + $scope.Account.RelatedAccountId+'/GetChildAccount).then(function (response) {
                //        $scope.childAccountL = response.data.Account;

                //    });
                //}

                $scope.LeadAccountService.AccountDispositionByAccountStatus(
                    accountServiceData.Account.AccountStatusId); //, errorHandler);

                if (accountServiceData.Account.SecCustomer)
                    for (i = 0; i < accountServiceData.Account.SecCustomer.length; i++) {
                        $scope.PersonSecondaryPeople.push(accountServiceData.Account.SecCustomer[i]);
                    }
                PersonService.People = $scope.PersonSecondaryPeople;

                $scope.AddressService.Set(accountServiceData.States, accountServiceData.Countries, accountServiceData.Addresses, accountServiceData.FranchiseAddress);

                // TODO: how can we bring that with better control.
                //$scope.accountAddress = accountServiceData.Addresses[0];

                $scope.AccountAddresses = accountServiceData.Addresses;

                $scope.AddressService.Addresses = [];

                $scope.SelectedRelatedAccountIds = accountServiceData.Account.RelatedAccountId;
                //Added for Qualification/Question Answers
                $scope.AccountQuestionAns = accountServiceData.AccountQuestionAns;
                $scope.AllQuestions = accountServiceData.AccountQuestionAns;
                //Added for Qualification/Question Answers
                if (accountServiceData.AccountQuestionAns) {
                    var questArray = $.map(accountServiceData.AccountQuestionAns, function (qa) {
                        return qa.Question;
                    });
                    if ($scope.questArray === undefined ||
                        $scope.questArray === "" ||
                        $scope.questArray === null) { console.log("questArray ="); console.log(questArray); }
                    if (questArray) {
                        $scope.UniqueQuestions = $.grep(questArray, function (el, index) {
                            return index == $.inArray(el, questArray);
                        });
                    }

                }
                $scope.isDisabled = true;
                //Added for Qualification/Question Answers
                $scope.GetQuestion = function (QAcollection, CurrentQuestion) {
                    //Get each question details based on current
                    //sub question from collection
                    if (QAcollection.length > 0 && CurrentQuestion) {
                        var sourcee = $.grep(QAcollection, function (q) {
                            return (q.Question == CurrentQuestion);
                        });

                        if (sourcee) return sourcee;
                    }
                    return null;
                };

                //Added for Qualification/Question Answers
                $scope.SaveQAlists = function (accountId) {
                    //To save the all question and answer selected
                    //Loop through all the controls dynamically
                    var promise = null;
                    $scope.isDisabled = false;
                    //
                    promise = $http.post('/api/accounts/' + accountId + '/SaveQuestionAnswers/', $scope.AllQuestions);
                    //
                    this.IsBusy = false;
                    $scope.isDisabled = true;
                    return promise;
                    //
                }
                for (i = 1; i < accountServiceData.Account.Addresses.length; i++) {

                    //$scope.accountPopAddress.push(accountServiceData.Addresses[i]);
                    $scope.AddressService.Addresses.push(accountServiceData.Account.Addresses[i]);
                }

                //Culpeper, VA 22701
                ////var addressZillow = accountServiceData.Account.Addresses[0];
                ////var citystatezip = addressZillow.City + ', ' + addressZillow.State + ' ' + addressZillow.ZipCode;
                ////var address = addressZillow.Address1;

                ////// Zillow only display US regions
                ////$scope.checkZillowinfo = addressZillow.CountryCode2Digits == "US" ? true : false;
                ////if (addressZillow.CountryCode2Digits == "US") {
                ////    $scope.GetAccountZillowinfo(address, citystatezip);
                ////}

                $scope.ChangeLeadStatus = function () {
                    $scope.LeadService.LeadSalesSteplist = null;
                    $scope.LeadService.LeadDispositionlist = null
                    $scope.LeadAccountService.AccountDispositionByAccountStatus($scope.Account.AccountStatusId);
                }
                if (accountServiceData.Account.IsPerpetual) {
                    $scope.Account.selectedRelationShipType = "IsPerpetual";

                } else if (!accountServiceData.Account.IsPerpetual) {
                    $scope.Account.selectedRelationShipType = "Onetime";
                }
                else {
                    $scope.Account.selectedRelationShipType = null;
                }

                $scope.AccountChannelList = accountServiceData.Account.SourcesList;

                $scope.AddressService.Addresses = [];
                for (i = 1; i < accountServiceData.Account.Addresses.length; i++) {

                    $scope.AddressService.Addresses.push(accountServiceData.Account.Addresses[i]);
                }
                if ($scope.Account.Addresses[0].IsResidential) {
                    $scope.Account.IsBusiness = false;
                }
                else if (!$scope.Account.Addresses[0].IsResidential) {
                    $scope.Account.IsBusiness = true;
                }
                //
                $scope.Account.DispositionId = accountServiceData.Account.DispositionId;

                $scope.FranchiseAddressObject = franchiseAddressObject;
                function _get() {

                    $scope.filter.page_info.size = 99999;

                    return $scope.filter.get(accountId).then(function (response) {
                        angular.forEach(response.data.PurchaseOrders, function (inv) {
                            inv.CreatedDate = new XDate(inv.CreatedDate, true);
                        });

                        $scope.purchaseOrders = response.data.PurchaseOrders;
                        $scope.orderStatuses = response.data.OrderStatus;
                        $scope.filter.total_items = response.data.TotalRecords;
                    },
                        function (response) {
                            HFC.DisplayAlert(response.statusText);
                        });
                };


                //$scope.$watch(function() { return $scope.filter.predicate; }, _get);

                var activeTabQueryParameter = getParameterByName("tab");
                if (activeTabQueryParameter) {
                    $('a[href="#' + activeTabQueryParameter + '"]').tab("show");
                }

                //set window title
                $('head title').text("Account #" + $scope.Account.AccountId + " - " + $scope.Account.PrimCustomer.FullName);

                // Need to set the initial value for the address one, because it is an
                // autocomplete component needs a spcial handling.
                $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_',
                    $scope.Account.Addresses[0].Address1);

                // Inform that the modal is ready to consume:
                $scope.modalState.loaded = true;
            }, errorHandler);

            //Initialize the NoteServiceTP, so that it will get the necessary data from the DB.
            $scope.NoteServiceTP.Initialize();

            //Initialize the AdditionalContactServiceTP, so that it will get the necessary data from the DB.
            $scope.AdditionalContactServiceTP.Initialize();

            //Initialize the AdditionalAddressServiceTP
            $scope.AdditionalAddressServiceTP.Initialize();
            $scope.AdditionalAddressServiceTP.PopulateAddressList();



        }
        else {

            $http.get('/api/FranchiseSettings/0/GetDisplayTerritoryId').then(function (data) {
                if (data.data) {
                    $scope.TerritoryDisData = data.data;
                    $scope.Account.TerritoryDisplayId = data.data.TerritoryId;
                    $scope.TerritoryDisplayId = data.data.TerritoryId;
                } else {
                    $scope.Account.TerritoryDisplayId = 0;
                    $scope.TerritoryDisplayId = 0;

                }

                //$http.get('/api/Accounts/0/getTerritoryForFranchise').then(function (response) {
                //    if (response.data != null) {
                //        if (!$scope.Account)
                //            $scope.Account = {};

                //        if (response.data.length > 0) {
                //            $scope.DisplayTerritorySource = response.data; }
                //        else {
                //            $scope.DisplayTerritorySource = [];
                //        }

                //    }
                //    $scope.DisplayTerritorySource.splice(0, 0, { Id: 0, Name: 'Select' });
                //});
            });

        }


        //Get the RelationshipType
        $http.get('/api/lookup/3/Type_LookUpValues').then(function (data) {
            $scope.AccountRelationshipType = data.data;

        });


        var isZillowApiCalled = false;
        $scope.handleKendoPanelBarExpand = function (ev) {

            //var element = ev.srcElement ? ev.srcElement : ev.target;
            //console.log(element, angular.element(element));

            if (isZillowApiCalled) return;

            isZillowApiCalled = true;

            //Culpeper, VA 22701
            var addressZillow = $scope.Account.Addresses[0];
            var citystatezip = addressZillow.City + ', ' + addressZillow.State + ' ' + addressZillow.ZipCode;
            var address = addressZillow.Address1;

            // Zillow only display US regions
            $scope.checkZillowinfo = addressZillow.CountryCode2Digits == "US" ? true : false;
            if (addressZillow.CountryCode2Digits == "US") {
                $scope.GetAccountZillowinfo(address, citystatezip);
            }
        }


        // Source
        $scope.RelatedSource = function () {

            $scope.RelatedSource = {
                placeholder: "Select",
                dataTextField: "name",
                dataValueField: "SourceId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                dataSource: {
                    transport: {
                        read: {
                            url: "/api/lookup/0/SourceTP",
                        }
                    }
                },
            };
        }
        $scope.RelatedSource();
        $http({ method: 'GET', url: '/api/address/', params: { leadIds: 0, includeStates: true } })
           .then(function (response) {
               $scope.AddressService.States = response.data.States || [];
               $scope.AddressService.Countries = response.data.Countries || [];
               $scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
           }, function (response) {
           });

        //Added for Address - State
        //TODO: Can we move this common service??
        $scope.getStates = function (countryISO) {

            if ($scope.StateList.length > 0 && countryISO) {
               var sourcee = $.grep($scope.StateList, function (s) {
                    return s.CountryISO2 == countryISO;
               });

               if (sourcee) return sourcee;
            }
            return null;
        };


        // Clear tax exempt
        $scope.clearTaxExempt = function () {


            // TODO: clear the Tax except id only when all the fiels are empty
            var act = $scope.Account;
            if (!(act.IsTaxExempt || act.IsPSTExempt
                || act.IsGSTExempt || act.IsHSTExempt
                || act.IsVATExempt )) {
                $scope.Account.TaxExemptID = null;
                $scope.Account.AllOpportunityTaxExempt = null;
                }

        }

        $scope.EnableorDesableTaxExempt = function () {
            // TODO: do we need this??? murugan
            //if ($scope.Account.TaxExemptID === '' || $scope.Account.TaxExemptID === null || $scope.Account.TaxExemptID === undefined) $scope.Account.IsTaxExempt = false; else $scope.Account.IsTaxExempt = true;

        }

        if (accountId < 0 || accountId== undefined) {

            $scope.Account = {};
            $scope.Account.TerritoryDisplayId = 0;
            //$scope.Account.AccountType = '';
            //$scope.Account.AccountType = "Key/Commercial";
            $scope.Account.AccountTypeId = 10008;

        }
        $scope.UpdateCommercialDependField = function () {
            if($scope.Account.IsCommercial==false)
            {
                $scope.Account.CommercialTypeId = null;
                $scope.Account.RelationshipTypeId = null;
                $scope.Account.IndustryId = null;
                $scope.Account.OtherIndustry = null;
            }
            else if ($scope.Account.IsCommercial == true) {
                $scope.Account.RelationshipTypeId = 3001;
            }
        }
        $scope.UpdateOtherIndustries = function () {
            $scope.Account.OtherIndustry = null;
        }

        //Update the RelationShip type on Commercial Type Change
        $scope.UpdateRelationShipType = function () {
            if ($scope.Account.CommercialTypeId != 2000) {
                $scope.Account.RelationshipTypeId = 3001;
            }
            else
                $scope.Account.RelationshipTypeId = null;
        }

        // Populate the list of State
        // TODO: Can we move this to commonservice.
        var geturl = '/api/AdditionalAddress/0/GetState/';
        $http.get(geturl).then(function (responseSuccess) {
            $scope.StateList = responseSuccess.data;
            //$scope.StateListForCountry = $scope.getStates($scope.Address.CountryCode2Digits);
        }, function (responseError) {
            HFC.DisplayAlert(responseError.statusText);
        });

        // print integration -- murugan
        $scope.printControl = {};
        $scope.printSalesPacket = function (modalid) {
            if ($routeParams.accountId) $scope.accountIdPrint = $routeParams.accountId;

            // $("#" + modalid).modal("show");
            $scope.printControl.showPrintModal();
        }


    //    $scope.DisplayTerritorySource = [
    //          { id: 0, name: 'Select' },
    //        { id: 1, name: 'dharaniya' },
    //    { id: 2, name: 'muthuraj' },
    //{ id: 3, name: 'sirikki' }
        //    ]

        //to get account type
         $http.get('/api/Accounts/0/GetAccountTypeStatus').then(function (response) {
                $scope.GetAccountTypeList = response.data;
            });


        $scope.showterritoryvalue = function () {


            if ($scope.TerritoryDisplayId > 0) {
                $scope.Account.TerritoryDisplayId = $scope.TerritoryDisplayId;
            }
            else $scope.Account.TerritoryDisplayId = 0;

        }

        $scope.$watch('Address.Territory', function () {


            //if ($scope.Account.TerritoryType != $scope.Address.Territory) {
            //    $scope.TerritoryDisplayId = 0;
            //    $scope.Account.TerritoryDisplayId = 0;
            //}

          //    if ($scope.Address.Territory)
            $scope.Account.TerritoryType = $scope.Address.Territory;

            //if ($scope.Account)
            //    if ($scope.Account.TerritoryType)
            //        if ($scope.TerritoryDisData) {
            //            //angular.forEach($scope.DisplayTerritorySource, function (value, key) {
            //            //    if (value.Name.toUpperCase().includes('GRAY AREA')) {
            //            //        $scope.TerritoryDisplayId = value.Id;
            //            //        $scope.Lead.TerritoryDisplayId = value.Id;
            //            //    }
            //            //    else {
            //            //        $scope.TerritoryDisplayId = 0;
            //            //        $scope.Lead.TerritoryDisplayId = 0;
            //            //    }
            //            //});

            //        }
            //        else {
            //            $scope.Account.TerritoryDisplayId = 0;
            //            $scope.TerritoryDisplayId = 0;
            //            //if ($scope.DisplayTerritorySource.length > 0)
            //            //    angular.forEach($scope.DisplayTerritorySource, function (value, key) {
            //            //        if (value.Name.toUpperCase().includes('GRAY AREA')) {

            //            //            $scope.TerritoryDisplayId = value.Id;
            //            //            $scope.Account.TerritoryDisplayId = value.Id;
            //            //        }
            //            //    });
            //            // $scope.Lead.TerritoryDisplayId = 0;
            //        }
        });




        //$scope.TerritoryDisplayId = 0;

        //$http.get('/api/Accounts/0/getTerritoryForFranchise').then(function (response) {
        //    if (response.data != null) {
        //
        //        if (!$scope.Account)
        //            $scope.Account = {};
        //        $scope.Account.TerritoryDisplayId = 0;
        //        $scope.TerritoryDisplayId = 0;

        //        if (response.data.length > 0) {
        //            $scope.DisplayTerritorySource = response.data;
        //            angular.forEach($scope.DisplayTerritorySource, function (value, key) {
        //                if (value.Name.toUpperCase().includes('GRAY AREA')) {

        //                    $scope.TerritoryDisplayId = value.Id;
        //                    $scope.Account.TerritoryDisplayId = value.Id;
        //                }
        //            });

        //            if (!($scope.TerritoryDisplayId > 0) || !($scope.TerritoryDisplayId)) {
        //                $scope.DisplayTerritorySource.splice(0, 0, { Id: 0, Name: 'Select' });
        //                $scope.Account.TerritoryDisplayId = 0;
        //            }

        //        }
        //        else {
        //            $scope.DisplayTerritorySource = [];
        //        }

        //    }
        //});

        //$http.get('/api/Accounts/0/getTerritoryForFranchise').then(function (response) {
        //    if (response.data != null) {
        //
        //        if (response.data.length > 0) {
        //            $scope.DisplayTerritorySource = response.data;
        //            $scope.DisplayTerritorySource.splice(0, 0, { Id: 0, Name: 'Select' });
        //            if (!($scope.TerritoryDisplayId > 0) || !($scope.TerritoryDisplayId)) {

        //                $scope.Account.TerritoryDisplayId = 0;
        //            }

        //        }
        //        else {
        //            $scope.DisplayTerritorySource = [];
        //        }

        //    }
        //});

        //texting
        $scope.EnableNotifyText = function (CellPhone) {

            if (CellPhone != undefined) {
                //if (CellPhone == $scope.CellPhoneCopy)
                //    $scope.Account.IsNotifyText = $scope.IsNotifyTextCopy;

                if (CellPhone.length == 10) {
                    $http.get("/api/Communication/0/GetTextingStatus?cellPhone=" + CellPhone).then(function (data) {

                        if (data.data == null) {
                            $scope.Account.NotifyTextStatus = 'new';
                            $scope.disablenotifytext = false;
                        }
                        else if (data.data.optOut == true) {
                            $scope.Account.NotifyTextStatus = 'Optout';
                            $scope.disablenotifytext = false;
                        }
                        else if (data.data.optIn == true) {
                            $scope.Account.NotifyTextStatus = 'Optin';
                            $scope.disablenotifytext = false;
                        }
                        else if (data.data.msgSent == true) {
                            $scope.Account.NotifyTextStatus = 'waiting';
                            $scope.disablenotifytext = false;
                        }
                        else if (data.data.optOut == null && data.data.optIn == null && data.data.msgSent == false) {
                            $scope.Account.NotifyTextStatus = 'error';
                            $scope.disablenotifytext = false;
                        }

                    })
                }
                else {
                    $scope.Account.NotifyTextStatus = 'new';
                    $scope.disablenotifytext = true;
                    $scope.Account.IsNotifyText = false;
                }
            }
            else {
                $scope.Account.NotifyTextStatus = 'new';
                $scope.disablenotifytext = true;
                $scope.Account.IsNotifyText = false;
            }
        }

        $scope.NotifyText = function () {
            $scope.Account.IsNotifyemails = $scope.IsNotifyemails;

            if ($scope.FranciseLevelTexting && $scope.Account.IsNotifyText && ($scope.Account.NotifyTextStatus == 'new' || $scope.Account.NotifyTextStatus == 'error')) {
                if ($scope.CellPhoneCopy == $scope.Account.PrimCustomer.CellPhone && $scope.Account.IsNotifyText == $scope.IsNotifyTextCopy) {
                    $scope.Account.SendText = false;
                    $scope.SaveAccount(1, 1);
                }
                else {
                    //if ($scope.frm_accnt.$invalid) {
                    //    $scope.Accntsubmit = true;
                    //    return;
                    //}
                    //if ($scope.Account.SourcesTPId == undefined || !($scope.Account.SourcesTPId > 0)) {
                    //    return;
                    //}
                    $scope.Account.SendText = true;
                    // $("#Textnotification").modal("show");
                    $scope.SaveAccount(1, 1);
                }
            }
            else {
                $scope.Account.SendText = false;
                $scope.SaveAccount(1, 1);
            }
        }
        $scope.CloseNotifyTextpopup = function () {
                $("#Textnotification").modal("hide");
        }

        $scope.EmailCheck = function (value) {
            if (value == false) {
                document.getElementsByName('accnt_email')[0].placeholder = '';
                $scope.Required = false;
            }
            else {
                document.getElementsByName('accnt_email')[0].placeholder = 'Required';
                $scope.Required = true;
            }

        }

    }
]).directive('hfcDuplicateAccountModal', ['cacheBustSuffix', function (cacheBustSuffix) {
    return {
        restrict: 'E',
        replace: true,
        controller: 'accountDetailsController',
        templateUrl: '/templates/NG/Account/account-duplicates-modal.html?cache-bust=' + cacheBustSuffix
    }
}]);
app.filter('dateFormat1', function ($filter) {
    return function (input) {
        if (input == null) { return ""; }

        var _date = $filter('date')(new Date(input), 'M/d/yyyy');

        return _date.toUpperCase();

    };
});
//app.filter('ignoreTimeZone', function () {
//    return function (val) {
//        var newDate = new Date(val.replace('T', ' ').slice(0, -6));
//        return newDate;
//    };
//});