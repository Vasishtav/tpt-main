﻿var appVendor = angular.module('HFC_NGAppVendor', [
    'unsavedChanges', 'ngRoute', 'ui.mask', 'ngMessages', 'ui.format'
    , 'hfc.address', 'ui.select2', 'summernote', 'ui.mask'
    , 'ui.helpers', 'treeControl', 'angular-cache', 'ui.dropzone'
    , 'mgcrea.ngStrap.modal', 'mgcrea.ngStrap.select', 'ngSanitize'
    , 'ui.sortable', 'globalMod', 'hfc.note.service', 'mgcrea.ngStrap.aside'
    , 'kendo.directives', 'ui.bootstrap.pagination', 'mgcrea.ngStrap'
    , 'ui.bootstrap', 'ngMask', 'hfc.core.service'
    , "kendo.directives", "angucomplete-alt", 'hfc.newsandupdates'
    , 'hfc.Notemodule', 'hfc.Cachemodule', 'ui.tinymce', 'HFC_NGApp_Auth'   // , 'hfc.accountModule', 'hfc.leadModule', 'hfc.leadaccountModule'
    , 'hfc.personModule', 'hfc.fileUploadModule', 'hfc.fileUploadModuleVg', 'hfc.commentsModule', 'hfc.caseCommentsModule', 'jqwidgets', 'hfc.calendarModule', 'commonConfiguratorService', 'hfc.kendotooltipModule', 'changeRequestModule', 'feedbackModule', 'overviewModule', 'HFC_NGAppCP', 'HFC_NGAppPIC']);   // , 'hfc.calendarModule'
appVendor.config(function ($routeProvider, unsavedWarningsConfigProvider) {
    $routeProvider.when("/VendorCaseList", {
        controller: "VendorcaseManagementListController",
        templateUrl: "/app/views/VendorCaseManagement/VendorCaseList.html",
        title: " Vendor Case List"
    });

    $routeProvider.when("/VendorcaseView/:CaseId/:VendorCaseId", {
        controller: "VendorcaseManagementViewController",
        templateUrl: "/app/views/VendorCaseManagement/vendorCaseView.html",
        title: " Details"
    });

    $routeProvider.when("/VendorcaseEdit/:CaseId/:VendorCaseId", {
        controller: "VendorcaseManagementController",
        templateUrl: "/app/views/VendorCaseManagement/vendorEdit.html",
        title: " Details"
    });

    $routeProvider.when("/VendorConfigurator", {
        controller: "VendorconfiguratorController",
        templateUrl: "/app/views/configurator/Vendorconfigurator.html",
        //resolve: resolve,
    });
    //vendor governance
    $routeProvider.when("/ChangeRequestCreate", {
        controller: "ChangeRequestController",
        templateUrl: "/app/views/VendorGovernance/ChangeRequestAdd.html",
    });

    $routeProvider.when("/ChangeRequestEdit/:CaseId", {
        controller: "ChangeRequestController",
        templateUrl: "/app/views/VendorGovernance/ChangeRequestEdit.html",
    });

    $routeProvider.when("/ChangeRequestView/:CaseId", {
        controller: "ChangeRequestController",
        templateUrl: "/app/views/VendorGovernance/ChangeRequestView.html",
    });

    $routeProvider.when("/ChangeRequest", {
        controller: "ChangeRequestController",
        templateUrl: "/app/views/VendorGovernance/ChangeRequest.html",
    });

    $routeProvider.when("/ChangeRequestKanbanView", {
        controller: "ChangeRequestController",
        templateUrl: "/app/views/VendorGovernance/ChangeRequestKanbanView.html",
    });

    $routeProvider.when("/Feedback", {
        controller: "FeedbackController",
        templateUrl: "/app/views/VendorGovernance/Feedback.html",
    });

    $routeProvider.when("/FeedbackKanbanView", {
        controller: "FeedbackController",
        templateUrl: "/app/views/VendorGovernance/FeedbackKanbanView.html",
    });

    $routeProvider.when("/Overview", {
        controller: "OverviewController",
        templateUrl: "/app/views/VendorGovernance/Overview.html",
    });

    $routeProvider.when("/OverviewKanbanView", {
        controller: "OverviewController",
        templateUrl: "/app/views/VendorGovernance/OverviewKanbanView.html",
    });

    $routeProvider.when("/FeedbackUpdate/:CaseId", {
        controller: "FeedbackController",
        templateUrl: "/app/views/VendorGovernance/FeedbackUpdate.html",
    });

    $routeProvider.when("/FeedbackView/:CaseId", {
        controller: "FeedbackController",
        templateUrl: "/app/views/VendorGovernance/FeedbackView.html",
    });
    //--End---

    $routeProvider.otherwise({ redirectTo: "/ChangeRequest" });

    unsavedWarningsConfigProvider.useTranslateService = false;
});

appVendor.constant('CONFIG', {
    apiServiceBaseUri: HFCNamespaceUrls.baseEndPoint,
});

//appCP.run(function ($rootScope) {
//    $rootScope.$on("$locationChangeStart", function (event, next, current) {
//        // TODO: do we really need this???
//        //////if (next.indexOf('search') > 0) {
//        //////    $rootScope.enableSaveSearch = enableSaveSearch;
//        //////} else {
//        //////    $rootScope.enableSaveSearch = false;
//        //////}
//    });
//});

var cacheBustSuffixFESettings = HFC.Version;

appVendor.constant("cacheBustSuffixFESettings", cacheBustSuffixFESettings);

appVendor.config(function ($httpProvider) {
    $httpProvider.interceptors.push('preventTemplateCache');
    $httpProvider.interceptors.push('authInterceptorService');
});

appVendor.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
    var original = $location.path;
    $location.path = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };

    $rootScope.$on('$viewContentLoaded', function () {
        var interval = setInterval(function () {
            if (document.readyState == "complete") {
                window.scrollTo(0, 0);
                clearInterval(interval);
            }
        }, 200);
    });
    $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {
        window.scrollTo(0, 0);
        setTimeout(function () {
            $(".tooltip-bottom").click(function (e) {
                var parent = $(e.target).parent()[0];
                $(parent).blur();
            });
        }, 5000);
    });
}]);

appVendor.run(function (HFCService) {
    HFCService.CurrentUserPermissions = permissionsList;
    HFCService.Roles = roles;
});

var permissionsList = [];
var roles = [];

angular.element(document).ready(function () {

    if (!angular.element(document).scope()) {
        $.ajax({
            url: '/api/Permission/0/GetUserRolesPermission',
            async: false,
            success: function (data) {
                //console.log("manual bootstraping");
                permissionsList = data;
                $.ajax({
                    url: '/api/users/',
                    async: false,
                    success: function (data) {
                        if (data != "") {
                            roles = data;
                        }
                        var path = window.location.href;
                        if (path.includes("VendorCP"))
                            angular.bootstrap(document, ['HFC_NGAppVendor']);
                    },
                    error: function (data) {
                        angular.bootstrap(document, ['HFC_NGAppVendor']);
                    }
                });
                              
            },
            error: function (data) {
                angular.bootstrap(document, ['HFC_NGAppVendor']);
            }
        });
    }

});

//appCP.run(function ($rootScope, $templateCache) {
//    $templateCache.removeAll();
//});

appVendor.controller('NavbarControllerVendorTP', [
        '$http', '$scope', '$window', '$location', '$rootScope', 'NavbarServiceVendor', 'FranchiseService'
        , function ($http, $scope, $window, $location, $rootScope, NavbarServiceVendor, FranchiseService) {
            $("#navvcp").css({ "display": "block" });
            $("#navvcp_side").css({ "display": "block" });

            $scope.NavbarServiceVendor = NavbarServiceVendor;
            $scope.FranchiseService = FranchiseService;
        }
])
  .service('NavbarServiceVendor', [
        '$http', '$rootScope', function ($http, $rootScope) {
            var srv = this;

            srv.hidemenusVendor = true;

            function DeslectAll() {
                srv.HomeSelected = "";
            }
            $.ajax({
                url: '/api/Color/0/Headercolor',
                async: false,
                success: function (data) {
                    srv.headercolor = data;
                }
            });

            srv.SelectHome = function () {
                DeslectAll();
                srv.HomeSelected = "selected";
            }

            function DeselectAllTop() {
                srv.HomeTab = "";
                srv.configuratorTab = "";
                srv.hidemenusVendor = false;
                // srv.hidemenusCP = false;
            }

            srv.SelectHomeTab = function () {
                srv.SelectHome();
                DeselectAllTop();
                srv.HomeTab = "active";
                srv.configuratorTab = "";
                // srv.Vendors = "active";
            }
            srv.PICConfiguratorTab = function () {
                srv.SelectHome();
                DeselectAllTop();
                srv.HomeTab = "";
                srv.configuratorTab = "active";
            }
            //srv.VendorsVendorsTab = function () {
            //    srv.Operations();
            //    DeselectAllTop();
            //    srv.vendorsTab = "active";
            //    srv.VendorsVendors = "active";
            //}
        }
  ]);
appVendor.factory('$exceptionHandler', ['$log', function ($log, $location) {
    return function myExceptionHandler(exception, cause) {
        $log.error(exception, cause);
        var loadingElement = document.getElementById("loading");
        if (loadingElement)
            loadingElement.style.display = "none";

        if (window.location.href.includes('localhost'))
            var loadingElement1 = document.getElementById("errorLoadingg");
        if (loadingElement1)
            loadingElement1.style.display = "block";
    };
}]);
// For global unhandled exception handling
appVendor.run(function ($rootScope) {
    $rootScope.$on("$locationChangeStart", function (event, next, current) {
        var loadingElement1 = document.getElementById("errorLoadingg");
        if (loadingElement1)
            loadingElement1.style.display = "none";
    });
});
//Prevent Template Cache by adding Version to html
appVendor.factory('preventTemplateCache',
    function ($injector) {
        var ENV = HFC.Version;
        var Version = ENV.substring(0, ENV.indexOf(' '));
        return {
            'request': function (config) {
                if (config.url.indexOf('views') !== -1) {
                    config.url = config.url + '?t=' + Version;
                }
                else if (config.url.indexOf('/templates/') !== -1) {
                    config.url = config.url + '?t=' + Version;
                }

                return config;
            }
        }
    });

appVendor.filter('ignoreTimeZone', function ($filter, calendarModalService) {
    return function (val, flag) {
        if (val) {
            let finalDate = '';
            if (typeof (val) == 'string') {
                finalDate = val.substr(0, 16);
                var newDate = calendarModalService.getDateStringValue(new Date(finalDate), true, false, false);
            }
            else
                var newDate = val;

            if (flag == "short")
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy hh:mm a');
            else
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy');

            return formattedDate;
        } else {
            return "";
        }
    };
});

//display time without using ignore timezone
appVendor.filter('GlobalTime', function ($filter, calendarModalService) {
    return function (val, flag) {
        if (val) {
            let finalDate = '';
            if (typeof (val) == 'string') {
                finalDate = val.substr(0, 16);
                var newDate = calendarModalService.getDateStringValue(new Date(finalDate), true, false, false);
            }
            else
                var newDate = val;

            if (flag == "short")
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy hh:mm a');
            else
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy');

            return formattedDate;
        } else {
            return "";
        }
    };
});

appVendor.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('errorHttpInterceptor');
}]);

appVendor.factory('errorHttpInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
    return {
        responseError: function responseError(rejection) {

            if (rejection.status === 401) {
                $rootScope.hideConfirm = true;
                location.reload();
                return;
            }
            $rootScope.hideConfirm = false;
            return $q.reject(rejection);
        }
    };
}])