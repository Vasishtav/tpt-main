﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Http.Filters;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Web.Filters
{
    public class LogExceptionsFilter : ExceptionFilterAttribute
    {
        
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var exception = actionExecutedContext.Exception;
            try
            {
                EventLogger.LogEvent(exception);
            }
            catch (Exception ex)
            {
                EventLogger.WriteToSystemEventLog(ex, ex.Message, LogSeverity.Error, ex.Source, ex.StackTrace, ex.GetType().Name);
            }


        }
    }
}