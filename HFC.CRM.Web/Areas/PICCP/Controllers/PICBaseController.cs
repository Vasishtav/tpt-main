﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HFC.CRM.Core.Common;

namespace HFC.CRM.Web.Areas.PICCP.Controllers
{
    public class PICBaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!SessionManager.PicPanelPermission.CanRead)
            {
                filterContext.Result = this.Redirect("/");
            }
        }
    }
}