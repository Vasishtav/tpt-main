﻿using System.Web.Mvc;

namespace HFC.CRM.Web.Areas.PICCP
{
    public class PICCPAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PICCP";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PICCP_detail",
                "PICCP/{controller}/{id}",
                new { action = "Detail", id = UrlParameter.Optional },
                new { id = @"\d+" },
                new[] { "HFC.CRM.Web.Areas.PICCP.Controllers" }
            );


            context.MapRoute(
                "PICCP_default",
                "PICCP/{controller}/{action}/{id}",

                new { controller = "PICHome", action = "Index", id = UrlParameter.Optional }, new[] { "HFC.CRM.Web.Areas.PICCP.Controllers" }
            );


        }
    }
}