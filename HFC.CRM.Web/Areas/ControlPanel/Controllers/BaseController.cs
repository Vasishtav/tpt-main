﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HFC.CRM.Core.Common;
using HFC.CRM.Core;

namespace HFC.CRM.Web.Areas.ControlPanel.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!SessionManager.SecurityUser.IsAdminInRole() && !SessionManager.ProductStrgMangPermission.CanAccess)
            {
                filterContext.Result = this.Redirect("/");
            }
        }
    }
}