﻿using System.Web.Mvc;

/// <summary>
/// The ControlPanel namespace.
/// </summary>
namespace HFC.CRM.Web.Areas.ControlPanel
{
    /// <summary>
    /// Class ControlPanelAreaRegistration.
    /// </summary>
    public class ControlPanelAreaRegistration : AreaRegistration 
    {
        /// <summary>
        /// Gets the name of the area to register.
        /// </summary>
        /// <value>The name of the area.</value>
        /// <returns>The name of the area to register.</returns>
        public override string AreaName 
        {
            get 
            {
                return "ControlPanel";
            }
        }

        /// <summary>
        /// Registers an area in an ASP.NET MVC application using the specified area's context information.
        /// </summary>
        /// <param name="context">Encapsulates the information that is required in order to register the area.</param>
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ControlPanel_detail",
                "ControlPanel/{controller}/{id}",
                new { action = "Detail", id = UrlParameter.Optional },
                new { id = @"\d+" },
                new[] { "HFC.CRM.Web.Areas.ControlPanel.Controllers" }
            );

           
            context.MapRoute(
                "ControlPanel_default",
                "ControlPanel/{controller}/{action}/{id}",

                new { controller = "AdminHome", action = "Index", id = UrlParameter.Optional }, new[] { "HFC.CRM.Web.Areas.ControlPanel.Controllers" }
            );

          
        }
    }
}