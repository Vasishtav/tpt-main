﻿using System.Web.Mvc;

namespace HFC.CRM.Web.Areas.VendorCP
{
    public class VendorCPAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "VendorCP";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "VendorCP_detail",
                "VendorCP/{controller}/{id}",
                new { action = "Detail", id = UrlParameter.Optional },
                new { id = @"\d+" },
                new[] { "HFC.CRM.Web.Areas.VendorCP.Controllers" }
            );


            context.MapRoute(
                "VendorCP_default",
                "VendorCP/{controller}/{action}/{id}",

                new { controller = "VendorHome", action = "Index", id = UrlParameter.Optional }, new[] { "HFC.CRM.Web.Areas.VendorCP.Controllers" }
            );


        }
    }
}