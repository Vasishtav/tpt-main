﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HFC.CRM.Core.Common;

namespace HFC.CRM.Web.Areas.VendorCP.Controllers
{
    public class VendorBaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!SessionManager.VendorPanelPermission.CanRead)
            {
                filterContext.Result = this.Redirect("/");
            }
        }
    }
}