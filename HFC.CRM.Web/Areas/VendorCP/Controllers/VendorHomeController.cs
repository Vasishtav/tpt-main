﻿using System.Web.Mvc;
using HFC.CRM.Core.Common;
using WebMatrix.WebData;
using HFC.CRM.Managers.QBSync.Helpers;
using HFC.CRM.Core;
using HFC.CRM.Core.Membership;
using HFC.CRM.Managers;
using HFC.CRM.Web.Models;

/// <summary>
/// The Controllers namespace.
/// </summary>
namespace HFC.CRM.Web.Areas.VendorCP.Controllers
{
    /// <summary>
    /// Class ControlPanelController.
    /// </summary>
    [Authorize]
    public class VendorHomeController : VendorBaseController
    {
        public ActionResult Index()
        {
            SessionManager.CurrentFranchise = null;
            CookieManager.ImpersonateUserId = null;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            SessionManager.Clear();
            WebSecurity.Logout();

            SessionManager.CurrentFranchise = null;
            CookieManager.ImpersonateUserId = null;

            return RedirectToAction("Login", "Account",new {Area="" });
        }

       
       

       
    }
}