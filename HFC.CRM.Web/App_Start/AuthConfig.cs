﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using HFC.CRM.Web.Models;
using HFC.CRM.Managers;
using HFC.CRM.Core;

/// <summary>
/// The Web namespace.
/// </summary>
namespace HFC.CRM.Web
{
    /// <summary>
    /// Class AuthConfig.
    /// </summary>
    public static class AuthConfig
    {
        /// <summary>
        /// Registers the authentication.
        /// </summary>
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            //OAuthWebSecurity.RegisterFacebookClient(
            //    appId: "",
            //    appSecret: "");

            //OAuthWebSecurity.RegisterGoogleClient();
            OAuthWebSecurity.RegisterClient(new GoogleOAuthClient(
                AppConfigManager.GetConfig<string>("ClientId", "GoogleCalendar", "Authentication"),
                AppConfigManager.GetConfig<string>("ClientSecret", "GoogleCalendar", "Authentication")), 
                "Google", null);
            
            /*OAuthWebSecurity.RegisterClient(new GoogleOAuthClient(
                "468379546083-ielhnfje8v9bm83r83b0p7mku3sk9ilo.apps.googleusercontent.com",
                "Lyi8Ehe8GHhCgXnAPLZGiSGX"), 
                "Google", null);*/
        }
    }
}
