using Microsoft.Practices.Unity;
using System.Web.Http;
using HFC.CRM.Cache.Cache;
using HFC.CRM.Cache.IoC;
using HFC.CRM.Cache.Push;
using HFC.CRM.Web.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Unity.WebApi;

namespace HFC.CRM.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            ServiceLocator.Init(container);

            container.RegisterType<IHubConnectionContext<dynamic>>(new InjectionFactory(f => GlobalHost.ConnectionManager.GetHubContext<PushHub>().Clients));

            container.RegisterType<IPushService, PushService>();
            container.RegisterType<IChangeTracker, ChangeTracker>();
            

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}