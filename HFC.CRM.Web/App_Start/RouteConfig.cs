﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

/// <summary>
/// The Web namespace.
/// </summary>
namespace HFC.CRM.Web
{
    /// <summary>
    /// Class RouteConfig.
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Registers the routes.
        /// </summary>
        /// <param name="routes">The routes.</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "DefaultIndexWithSecondId",
                url: "{controller}/{leadid}/{jobid}",
                defaults: new { action = "Details" },
                constraints: new { leadid = @"\d+", jobid = @"\d+" },
                namespaces: new[] { "HFC.CRM.Web.Controllers" }
            );

            routes.MapRoute(
                name: "DefaultIndex",
                url: "{controller}/{leadid}",
                defaults: new { action = "Details" },
                constraints: new { leadid = @"\d+" },
                namespaces: new[] { "HFC.CRM.Web.Controllers" }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "HFC.CRM.Web.Controllers" }
            );
        
        }
    }
}