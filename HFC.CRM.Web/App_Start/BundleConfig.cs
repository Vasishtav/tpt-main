﻿using System.Web.Optimization;

using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;

using HFC.CRM.Core;

/// <summary>
/// The Web namespace.
/// </summary>
namespace HFC.CRM.Web
{
    /// <summary>
    /// Class BundleConfig.
    /// </summary>
    public class BundleConfig
    {
        public static string BrandedPath { get; set; }

        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        /// <summary>
        /// Registers the bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            BrandedPath = AppConfigManager.BrandedPath;
            RegisterStyles(bundles);
            RegisterScripts(bundles);
#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }

        /// <summary>
        /// Registers the scripts.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        private static void RegisterScripts(BundleCollection bundles)
        {
#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif

            #region Script Bundles

            bundles.Add(new ScriptBundle("~/Scripts/jquery").Include(
               "~/Scripts/jquery-{version}.js"
           ));

            bundles.Add(new ScriptBundle("~/Scripts/base").Include(
                "~/Scripts/jquery-{version}.js",
                 "~/Scripts/jquery.signalR-2.2.0.min.js",
                  "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/knockout-{version}.js",
                "~/Scripts/knockstrap.js",
                "~/Scripts/moment.js",
                "~/Scripts/moment-new.js",
                "~/Scripts/xdate.dev.js",
                "~/Scripts/date.js",
                "~/Scripts/cycle.js",
                "~/Scripts/select2.js",
                "~/Scripts/jquery.hoverIntent.js",
                "~/Scripts/CRM/xdate.extended.js",
                "~/Scripts/CRM/core.js",
                "~/Scripts/JSLINQ-vsdoc.js",
                "~/Scripts/JSLINQ.js",
                "~/Scripts/json2.js"

            ));
            //SOON TO BE OBSOLETE
            bundles.Add(new ScriptBundle("~/Scripts/knockout").Include(
                //"~/Scripts/knockout-{version}.js"
                "~/Scripts/knockout-3.1.0.js",
                "~/Scripts/knockstrap.js"
                ));

            var bootstrapBundle = new ScriptBundle("~/Scripts/bootstrap").Include(
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/angular-messages.js",
                "~/Scripts/bootstrap-datepicker.js",
                "~/Scripts/bootstrap-multiselect.js",
                "~/Scripts/bootstrap-timepicker.js",
                "~/Scripts/bootstrap.colorpickersliders.js",
                "~/Scripts/tinycolor.js",
                "~/Scripts/bootstrap-select.js",
                "~/Scripts/CRM/KO.CustomBindings/datepicker.js",
                "~/Scripts/CRM/KO.CustomBindings/select2.js",
                "~/Scripts/CRM/KO.CustomBindings/selectpicker.js"
                );
            bootstrapBundle.Transforms.Clear();
            bundles.Add(bootstrapBundle);

            var angularBundle = new ScriptBundle("~/Scripts/angular").Include(
                //"~/Scripts/angular.js",
                "~/Scripts/jquery.signalR-2.2.0.min.js",
                "~/Scripts/angular-ui/ui-utils.js",
                "~/Scripts/angular-strap.js",
                "~/Scripts/angular-strap.tpl.js",
                "~/Scripts/angular-loading-bar/loading-bar.js",
                "~/Scripts/angular-tree-control.js",
                "~/Scripts/angular-cache.js",
                "~/Scripts/CRM/NG.Core/ngHFCServiceold.js",
                "~/Scripts/jszip.min.js",
                  "~/Scripts/kendo.all.min.js",
                   "~/Scripts/jqxcore.js",
                   "~/Scripts/jqxbuttons.js",
                   "~/Scripts/jqxangular.js",
                    "~/Scripts/jqxsortable.js",
                   "~/Scripts/jqxkanban.js",
                   "~/Scripts/jqxdata.js",
                   "~/Content/signpad/jquery.signaturepad.min.js",
                   "~/Content/signpad/json2.min.js"

                );

            angularBundle.Transforms.Clear();
            bundles.Add(angularBundle);

            var spaBundle = new ScriptBundle("~/Scripts/spa").Include(

              //"~/Scripts/angular.js",
              "~/app/services/common/ngHFCService.js",
              "~/Scripts/ko.editables.js",
   "~/Scripts/bootstrap3-typeahead.js",
   "~/Scripts/angular-ui/ui-bootstrap.min.js",
   "~/Scripts/angular-ui/ui-bootstrap-tpls.js",

    "~/Scripts/angular-ui/mask.js",

   "~/Scripts/CRM/KO.CustomBindings/typeahead.js",
   "~/Scripts/CRM/KO.CustomBindings/timepicker.js",
   "~/Scripts/CRM/KO.Models/event.js",
   "~/Scripts/CRM/KO.Models/task.js",
   "~/Scripts/CRM/KO.Models/recurring.js",
   "~/Scripts/CRM/KO.Models/calendar.js",
   "~/Scripts/CRM/KO.ViewModels/calendar.js",
   "~/Scripts/CRM/KO.ViewModels/tasks.js",
   "~/Scripts/CRM/KO.ViewModels/calendar_modal.js",
   "~/Scripts/CRM/calevents_popover.js",
   "~/Scripts/CRM/KO.CustomBindings/googlemaps.js",
   "~/Scripts/CRM/KO.Models/address.js",
   "~/Scripts/CRM/KO.Models/user.js",
   "~/Scripts/CRM/KO.Models/person.js",
   "~/Scripts/CRM/KO.Models/quote.js",
   "~/Scripts/CRM/KO.Models/saleadjustment.js",
   "~/Scripts/CRM/KO.Models/job.js",
   "~/Scripts/CRM/KO.Models/jobitem.js",
   "~/Scripts/CRM/KO.ViewModels/notes.js",
   "~/Scripts/CRM/KO.ViewModels/filter_extension.js",
   "~/Scripts/CRM/KO.ViewModels/job_list.js",
   "~/Scripts/CRM/charts.js",
   "~/Scripts/bootbox.min.js",
   "~/signalr/hubs",
   "~/Scripts/angular-sanitize.min.js",
   "~/Scripts/angular-strap.js",
   "~/Scripts/angular-strap.tpl.js",
   "~/Scripts/parseParams.js",
   "~/Scripts/angular-route.min.js",
   "~/Scripts/angular-cache.js",
   "~/Scripts/ng-sortable.js",
   "~/Scripts/ng-numbersOnly.js",
   "~/Scripts/CRM/NG.Directives/ngPagination.js",
   "~/Scripts/select2.js",
   "~/Scripts/angular-ui/ui-select2.js",
   "~/Scripts/summernote_0.6/summernote.js",
   "~/Scripts/summernote_0.6/summernote-ext-fields.js",
   "~/Scripts/summernote_0.6/summernote-ext-fontstyle.js",
   "~/Scripts/angular-summernote.js",
   "~/Scripts/tinymce/tinymce.js",
   "~/Scripts/angular-ui/ui-tinymce.js",
   "~/Scripts/dropzone/dropzone.min.js",
   "~/app/configUrls.js",

   // cusom modules.
   "~/app/modules/ngMask.js",
   "~/app/modules/kendoMod.js",
      // "~/app/services/common/ngHFCService.js",
      "~/app/services/common/ngNoteService.js",
         "~/app/services/common/CacheService.js",
            "~/app/services/common/unsavedChanges.js",
             "~/app/modal.js",
                "~/app/appVendor.js",
                "~/app/appPIC.js",
              "~/app/app-CP.js",
   "~/app/app.js",
    "~/app/controllers/admin/FranchiseController.js",
    "~/app/controllers/admin/franchiseListingController.js",
    "~/app/controllers/batchPurchasingController.js",
    "~/app/services/FranchiseServiceTP.js",
   "~/app/services/interceptors/authInterceptorService.js",
   "~/app/services/LeadService.js",
   "~/app/services/LeadAccountService.js",
   "~/app/services/LeadSourceService.js",
   "~/app/services/AddressService.js",
   "~/app/services/AdvancedEmailService.js",
   "~/app/services/CalendarService.js",
   "~/app/directives/alertModalDirective.js",
   //"~/app/directives/custom-tooltip.js",
   "~/app/services/settings/alertModalService.js",
   "~/app/services/FileUploadService.js",
   "~/app/services/FileUploadServiceVg.js",
   "~/app/services/DropzoneService.js",
   "~/app/services/EmailService.js",
   "~/app/services/InvoiceService.js",
   "~/app/services/JobNoteService.js",
   "~/app/services/JobService.js",
   //"~/app/services/LeadCampaignService.js",
   "~/app/services/LeadNoteService.js",
   "~/app/services/LeadSourceService.js",
   "~/app/services/OrderService.js",
   "~/app/services/PaymentService.js",
   "~/app/services/PersonService.js",
   "~/app/services/AccountService.js",
   "~/app/services/EmailQuoteService.js",
   "~/app/services/FileprintService.js",
   "~/app/services/Payment/PaymentInvoiceService.js",
   "~/app/services/AttendeesService.js",
   "~/app/services/OpportunityService.js",
   "~/app/services/PrintService.js",
   "~/app/services/ErrorService.js",
    "~/app/services/NewtaskService.js",
   "~/app/services/PurchaseOrderFilter.js",
   "~/app/services/PurchaseOrderService.js",
   "~/app/services/search/saveCriteriaService.js",
   "~/app/services/settings/sourceService.js",
   "~/app/services/settings/vendorService.js",
   "~/app/services/PurchaseSearchService.js",
   // the following is no more required.
   // "~/app/services/noteService.js",
   "~/app/services/QuestionService.js",
   "~/app/controllers/search/kendotooltipcontroller.js",
   "~/app/services/LinkedList/NoteServiceTP.js",
   "~/app/services/LinkedList/AdditionalContactServiceTP.js",
   "~/app/services/LinkedList/AdditionalAddressServiceTP.js",
    // This is no more required as it is replaced by noteservicetp itself.
    //"~/app/services/LinkedList/AttachmentServiceTP.js",

    // Print coponent
    "~/app/services/PrintServiceTP.js",
   "~/app/directives/PrintModalTP.js",
    //payment
    "~/app/directives/ReversePayment.js",

   // Autogrow text area
   "~/app/directives/Autogrow.js",

    // Appointment Component:
    "~/app/services/CalendarServiceTP.js",
     "~/app/services/CalenderModalService.js",
    "~/app/directives/AppointmentGridTP.js",
    "~/app/directives/SquareFeetCalculator.js",
    "~/app/controllers/lead/appointmentsListingController.js",
    "~/app/directives/configuratorFeedback.js",
    // Communication Log
    "~/app/services/CommunicationServiceTP.js",
    "~/app/directives/CommunicationGridTP.js",
    "~/app/controllers/CommunicationListingController.js",

    //new communicationlog
    "~/app/controllers/AccountCommunicationListingController.js",
    "~/app/controllers/LeadCommunicationListingController.js",
    "~/app/controllers/OpportunityCommunicationListingController.js",

    //Source migration
    "~/app/controllers/settings/myCrmDataConversionController.js",

   // Address validation:
   "~/app/services/AddressValidationService.js",
   "~/app/directives/AddressTP.js",
   "~/app/directives/angucomplete-alt.js",

    // Currency Validation
    "~/app/directives/CurrencyTP.js",

    // common dialog service
    "~/app/services/DialogService.js",

    // Tpt custom filters.
    "~/app/filters/maskTP.js",
     "~/app/filters/htmlToPlaintext.js",

   string.Format("~/app/services{0}/quoteItemService.js", BrandedPath),
   string.Format("~/app/services{0}/quoteSaleAdjService.js", BrandedPath),
   string.Format("~/app/services{0}/quoteService.js", BrandedPath),

   "~/app/services/search/JobSearchService.js",
   "~/app/services/InvoiceService.js",
   "~/app/services/JobNoteService.js",
   "~/app/services/PaymentService.js",
   "~/app/services/AddressService.js",
   "~/app/services/PersonService.js",
   "~/app/services/AccountService.js",

   //"~/app/services/AccountCampaignService.js",
   //"~/app/services/AccountNoteService.js",
   // "~/app/services/AccountSourceService.js",
   "~/app/services/FileprintService.js",
   "~/app/services/Payment/PaymentInvoiceService.js",
    "~/app/services/NewtaskService.js",
   "~/app/services/search/LeadSearchService.js",
    "~/app/services/search/AccountSearchService.js",
    "~/app/services/search/OrderSearchService.js",
    "~/app/services/search/PaymentSearchService.js",
    "~/app/services/search/InvoiceSearchService.js",
    "~/app/services/search/OpportunitySearchService.js",
   "~/app/services/dashboard/dashboardService.js",
   "~/app/services/kendo/kendoService.js",
   "~/app/services/categoryService.js",
   "~/app/controllers/common/navBarController.js",
   "~/app/controllers/common/NavbarControllerTP.js",
    "~/app/controllers/common/EmailController.js",
   "~/app/controllers/common/AdvancedEmailController.js",
   "~/app/controllers/common/CalendarController.js",
   "~/app/controllers/common/CalendarControllerTP.js",
   "~/app/controllers/common/SaveCalendarControllerTP.js",
  //"~/app/controllers/common/OperationControllerTP.js",
  "~/app/controllers/PurchaseOrder/purchaseOrderController.js",
   "~/app/controllers/PurchaseOrder/xVendorPurchaseOrderController.js",
   "~/app/controllers/common/ReportsControllerTP.js",
   "~/app/controllers/common/MarketingControllerTP.js",
   "~/app/controllers/search/leadSearchController.js",
   "~/app/controllers/search/orderSearchController.js",
   "~/app/controllers/search/quotesSearchController.js",
   "~/app/controllers/search/paymentSearchController.js",
   "~/app/controllers/search/invoiceSearchController.js",
    "~/app/controllers/search/accountSearchController.js",
    "~/app/controllers/search/opportunitySearchController.js",
     "~/app/controllers/Measurement/MeasurementController.js",
     "~/app/controllers/Quote/quoteController.js",
      "~/app/controllers/Quote/quoteNotesController.js",
     "~/app/controllers/Margin/marginController.js",
   //"~/app/controllers/invoice/invoiceDetailsController.js",
   //"~/app/controllers/search/invoiceSearchController.js",
   "~/app/controllers/dashboard/dashboardController.js",
"~/app/controllers/FranchiseCaseMgmt/FranchiseCaseListController.js",
"~/app/controllers/FranchiseCaseMgmt/FranchiseCaseViewController.js",
"~/app/controllers/FranchiseCaseMgmt/FranchiseCaseVendorViewController.js",
    //"~/app/controllers/dashboard/ngLeadListController.js",
    //"~/app/controllers/dashboard/ngJobListController.js",

    // News and updates
    "~/app/services/NewsUpdatesService.js",
   "~/app/controllers/news/newsListingController.js",
   "~/app/controllers/news/NewsController.js",
    // HFC Level Permission Set
    "~/app/controllers/HFCRoles/HFCRoleSearchController.js",
    "~/app/controllers/HFCRoles/HFCRoleController.js",

    "~/app/controllers/HFCPermission/HFCPermissionSetController.js",
    "~/app/controllers/HFCPermission/HFCPermissionSetSearchController.js",

   "~/app/controllers/ShippingNotices/ShippingNoticesController.js",
   "~/app/controllers/ShippingNotices/ShippingNoticeDashboardController.js",

   "~/app/controllers/ProcurementDashboard/ProcurementDashboardController.js",

   "~/app/controllers/ProcurementDashboard/xSalesOrderview.js",

    "~/app/controllers/MasterProcurementDashboard/MasterProcurementDashboardController.js",

   "~/app/controllers/vendorInvoice/VendorInvoiceController.js",
   "~/app/controllers/VendorAcknowledgement/VendorAcknowledgementController.js",

   "~/app/controllers/settings/buyerremorseController.js",

   "~/app/controllers/SampleCrud/SampleCrudController.js",

   "~/app/controllers/lead/leadConvertController.js",
   "~/app/controllers/lead/leadController.js",
   "~/app/controllers/account/accountDetailsController.js",
   "~/app/controllers/opportunity/opportunityController.js",
   "~/app/controllers/order/orderController.js",
   "~/app/controllers/order/paymentController.js",
   "~/app/controllers/account/accountController.js",
   //"~/app/controllers/job/jobController.js",
   "~/app/controllers/person/personController.js",
   "~/app/controllers/settings/sourceController.js",
   "~/app/controllers/settings/mergeToolController.js",
   "~/app/controllers/settings/vendorController.js",

   // the following is no moore required.
   //"~/app/controllers/Note/noteController.js",

   "~/app/controllers/Disclaimer/disclaimerController.js",
   "~/app/controllers/EmailSettings/emailsettingsController.js",
   //"~/app/controllers/Roles/rolesController.js",
   "~/app/controllers/question/questionController.js",
   //procurement settings
   "~/app/controllers/ProcurementSettings/ProcurementController.js",

   //"~/app/controllers/ProcurementSettings/FeAlliancevendorController
   //Fe-Vendors
   "~/app/controllers/AdminOperations/AdminAlliancevendorController.js",
   "~/app/controllers/AdminOperations/CoreProductController.js",

   "~/app/controllers/AdminFinance/AdminCurrencyConversionController.js",

   "~/app/controllers/CaseManageAddEdit/CaseManagementController.js",
   "~/app/controllers/VendorCasemanage/VendorcaseManagementController.js",
    "~/app/controllers/VendorCasemanage/VendorcaseManagementListController.js",
    "~/app/controllers/VendorCasemanage/VendorcaseManagementViewController.js",
    
   "~/app/directives/CommentsComponent.js",
   "~/app/directives/CaseCommentsComponent.js",
   //Vendor Case Config
   "~/app/controllers/VendorCaseConfig/VendorCaseController.js",

    "~/app/controllers/question/questionController.js",
    "~/app/controllers/configurator/PICconfiguratorController.js",
    "~/app/controllers/configurator/VendorconfiguratorController.js",
    "~/app/controllers/configurator/HomeOfficeconfiguratorController.js",
    "~/app/controllers/configurator/configuratorController.js",
    "~/app/controllers/configurator/myVendorconfiguratorController.js",
    "~/app/controllers/configurator/myProductConfiguratorController.js",
    "~/app/controllers/configurator/batchBBCoreProductConfigurator.js",
    "~/app/controllers/configurator/myServiceConfiguratorController.js",
    "~/app/services/configurator/configuratorService.js",
    "~/app/services/configurator/myVendorconfiguratorService.js",
    "~/app/controllers/configurator/surfacingJobEstimatorController.js",

     "~/app/controllers/category/categoryController.js",
     "~/app/controllers/sources/tpSourceController.js",
     "~/app/controllers/sources/hfcSourceController.js",
     "~/app/controllers/sources/channelListController.js",
     "~/app/controllers/sources/sourceListController.js",
     "~/app/controllers/sources/ownerListController.js",

     "~/app/services/AdminSourceChannelServiceTP.js",

   //Kanban
   "~/app/controllers/Kanban/KanbanController.js",

     "~/app/controllers/sources/hfcLeadsourcemapController.js",

   "~/app/services/ProductService.js",
   "~/app/controllers/Product/ProductController.js",
   "~/app/services/search/ProductSearchService.js",
   "~/app/controllers/search/ProductSearchController.js",
   "~/app/directives/ResurfacingProduct.js",
   "~/app/controllers/settings/pricingController.js",
   "~/app/controllers/settings/advancedpricingController.js",
   "~/app/controllers/settings/manageDocuments.js",
   "~/app/controllers/settings/userController.js",
   "~/app/controllers/settings/RolesController.js",
   "~/app/controllers/settings/RoleSearchController.js",
    "~/app/controllers/settings/PermissionSetSearchController.js",
   "~/app/controllers/settings/PermissionSetsController.js",
   "~/app/controllers/settings/appTypeController.js",
   "~/app/controllers/settings/permissionController.js",
   "~/app/controllers/attachments/globalSystemWideAttachmentsController.js",
   "~/app/controllers/News/newsControllerCP.js",
   "~/app/controllers/settings/TaxSettingsController.js",
   "~/app/controllers/settings/JobCrewSizeController.js",
   "~/app/controllers/settings/SurfacingProductController.js",
   "~/app/controllers/settings/SurfacingProductSearchController.js",
   "~/app/controllers/settings/SettingsMsrReportController.js",

   //global search
   "~/app/controllers/GlobalSearch/GlobalSearchController.js",

   //vendor governance
   "~/app/controllers/VendorGovernance/ChangeRequestController.js",
   "~/app/controllers/VendorGovernance/FeedbackController.js",
   "~/app/controllers/VendorGovernance/OverviewController.js",
   
   string.Format("~/app/controllers{0}/QuoteLineController.js", BrandedPath),

   "~/app/directives/Address.js",
   "~/app/directives/HoverIntent.js",
   // This is no more required, as we are not using any of this---murugan
   //"~/app/directives/Dropzone.js",
   "~/app/directives/Enter.js",
   "~/app/directives/JobPanel.js",
   "~/app/directives/Payment.js",
   "~/app/directives/Person.js",
     "~/app/directives/CalendarModalDirective.js",
   "~/app/directives/Account.js",
   "~/app/directives/Quote.js",
   "~/app/directives/Measurement.js",
    "~/app/directives/Fileprint.js",
    "~/app/directives/PaymentInvoice.js",
   "~/app/directives/Attendees.js",
   "~/app/directives/Opportunity.js",
   "~/app/directives/Error.js",
    "~/app/directives/Newtask.js",
   "~/app/directives/hfcLeadSource.js",
   "~/app/directives/hfcLeadSourceModal.js",
   "~/Scripts/CRM/NG.Directives/ngDropzone.js",

   string.Format("~/app/directives{0}/QuotePanel.js", BrandedPath),

   "~/app/directives/hfcEmailModal.js",
   "~/app/directives/hfcAdvancedEmailModal.js",
    "~/app/directives/hfcCalendarModal.js",

    "~/app/controllers/Reports/ReportsController.js",
     "~/app/services/Reports/ReportsService.js",
     "~/app/controllers/Quickbook/QuickbookController.js",
     "~/app/directives/ReportHeader.js",
    //Touchpoint specific javascript files.
    // No more used, replaced by additionalcontact
    //"~/app/services/PersonServiceTP.js",
    //"~/app/directives/PersonDirectiveTP.js"


    //Home Office Case Management
    "~/app/controllers/HomeOfficeCaseManagement/HomeOfficeCaseManagementController.js",
    "~/app/controllers/HomeOfficeCaseManagement/HomeOfficeCaseListController.js",
    "~/app/controllers/HomeOfficeCaseManagement/HomeOfficeCaseViewController.js",
    "~/app/controllers/HomeOfficeCaseManagement/HomeOfficeCaseVendorViewController.js",
    "~/app/controllers/HomeOfficeCaseManagement/HomeOfficePurchaseOrderController.js",
    "~/app/controllers/HomeOfficeCaseManagement/HomeOfficeVendorController.js"

    );
            spaBundle.Transforms.Clear();
            bundles.Add(spaBundle);

            //this is primarily for calendar, since calendar requires jqueryui for dragging script and has to be loaded before bootstrap or it bugs out
            bundles.Add(new ScriptBundle("~/Scripts/jqueryui").Include(
                "~/Scripts/jquery-ui-{version}.js"
                ));

            var reminderBundle = (new ScriptBundle("~/Scripts/ngReminder").Include(
                "~/Scripts/CRM/NG.Directives/ngReminder.js"
                ));
            reminderBundle.Transforms.Clear();
            bundles.Add(reminderBundle);

            var pushListenerBundle = (new ScriptBundle("~/Scripts/ngPushListener").Include(
               "~/Scripts/CRM/NG.Directives/ngPushListener.js"
               ));
            pushListenerBundle.Transforms.Clear();
            bundles.Add(pushListenerBundle);

            //SOON TO BE OBSOLETE
            bundles.Add(new ScriptBundle("~/Scripts/reminder").Include(
                "~/Scripts/knockout.mapping-latest.js",
                "~/Scripts/CRM/reminder.js"
            ));

            bundles.Add(new ScriptBundle("~/Scripts/fullcalendar").Include(
                "~/Scripts/fullcalendar.js"
            ));

            bundles.Add(new ScriptBundle("~/Scripts/dashboard").Include(
                "~/Scripts/ko.editables.js",
                "~/Scripts/bootstrap3-typeahead.js",
                "~/Scripts/CRM/KO.CustomBindings/typeahead.js",
                "~/Scripts/CRM/KO.CustomBindings/timepicker.js",
                "~/Scripts/CRM/KO.Models/event.js",
                "~/Scripts/CRM/KO.Models/task.js",
                "~/Scripts/CRM/KO.Models/recurring.js",
                "~/Scripts/CRM/KO.Models/calendar.js",
                "~/Scripts/CRM/KO.ViewModels/calendar.js",
                "~/Scripts/CRM/KO.ViewModels/tasks.js",
                "~/Scripts/CRM/KO.ViewModels/calendar_modal.js",
                "~/Scripts/CRM/calevents_popover.js",
                "~/Scripts/CRM/charts.js",
                "~/Scripts/bootbox.min.js"
            ));
            bundles.Add(new ScriptBundle("~/Scripts/calendar").Include(
                "~/Scripts/jquery.hoverIntent.js",
                "~/Scripts/CRM/KO.CustomBindings/googlemaps.js",
                "~/Scripts/ko.editables.js",
                "~/Scripts/bootstrap3-typeahead.js",
                "~/Scripts/CRM/KO.CustomBindings/typeahead.js",
                "~/Scripts/CRM/KO.Models/event.js",
                "~/Scripts/CRM/KO.Models/task.js",
                "~/Scripts/CRM/KO.Models/recurring.js",
                "~/Scripts/CRM/KO.Models/calendar.js",
                "~/Scripts/CRM/KO.Models/user.js",
                "~/Scripts/CRM/KO.ViewModels/users.js",
                "~/Scripts/CRM/KO.ViewModels/calendar.js",
                "~/Scripts/CRM/KO.ViewModels/tasks.js",
                "~/Scripts/CRM/KO.ViewModels/calendar_modal.js",
                "~/Scripts/CRM/calevents_popover.js",
                "~/Scripts/CRM/hfc.calendar.js",
                "~/Scripts/bootbox.min.js"
           ));

            bundles.Add(new ScriptBundle("~/Scripts/user-list").Include(
                "~/Scripts/CRM/KO.Models/user.js",
                "~/Scripts/CRM/KO.ViewModels/users.js",
                "~/Scripts/CRM/KO.ViewModels/user-detail.js",
                "~/Scripts/jquery.maskedinput-1.3.1.js",
                "~/Scripts/CRM/KO.Models/address.js",
                "~/Scripts/CRM/KO.Models/person.js",
                "~/Scripts/tinycolor.js",
                "~/Scripts/bootstrap.colorpickersliders.js",
                "~/Scripts/bootstrap-datepicker.js",
                "~/Scripts/CRM/KO.CustomBindings/masked.js",
                "~/Scripts/CRM/KO.CustomBindings/colorpicker.js",
                "~/Scripts/bootbox.min.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/user-detail").Include(
                    "~/Scripts/CRM/KO.ViewModels/user-detail.js",
                    "~/Scripts/jquery.maskedinput-1.3.1.js",
                    "~/Scripts/CRM/KO.Models/address.js",
                    "~/Scripts/CRM/KO.Models/person.js",
                    "~/Scripts/CRM/KO.Models/user.js",

                    "~/Scripts/tinycolor.js",
                    "~/Scripts/bootstrap.colorpickersliders.js",
                    "~/Scripts/bootstrap-datepicker.js",
                    "~/Scripts/CRM/KO.CustomBindings/masked.js",
                    "~/Scripts/CRM/KO.CustomBindings/colorpicker.js"

                ));

            // This is no more required in Touchpoint --murugan march 12, 2019
            //bundles.Add(new ScriptBundle("~/Scripts/leads_list").Include(
            //    "~/Scripts/jquery.hoverIntent.js",
            //    "~/Scripts/CRM/KO.CustomBindings/googlemaps.js",
            //    "~/Scripts/CRM/KO.CustomBindings/notesdropdown.js",
            //    "~/Scripts/CRM/KO.Models/address.js",
            //    "~/Scripts/CRM/KO.Models/user.js",
            //    "~/Scripts/CRM/KO.Models/person.js",
            //    "~/Scripts/CRM/KO.Models/quote.js",
            //    "~/Scripts/CRM/KO.Models/leadsource.js",
            //    "~/Scripts/CRM/KO.Models/lead.js",
            //    "~/Scripts/CRM/KO.Models/job.js",
            //    "~/Scripts/CRM/KO.ViewModels/addresses.js",
            //    "~/Scripts/CRM/KO.ViewModels/notes.js",
            //    "~/Scripts/CRM/KO.ViewModels/filter_extension.js",
            //    "~/Scripts/CRM/KO.ViewModels/lead_list.js",
            //    "~/Scripts/native.history.js",
            //    "~/Scripts/parseParams.js"
            //));

            var leadSearchBundle = new ScriptBundle("~/Scripts/ngleadsSearch_list").Include(
             "~/Scripts/jquery.hoverIntent.js",
               "~/Scripts/CRM/NG.Core/ngCache.js",
               "~/Scripts/native.history.js",
               "~/Scripts/parseParams.js"//,
                                         //  "~/Scripts/CRM/NG.Controllers/ngLeadSearchController.js"

           );
            leadSearchBundle.Transforms.Clear();
            bundles.Add(leadSearchBundle);

            // Not used anywhere--murugan

            //var leadListBundle = new ScriptBundle("~/Scripts/ngleads_list").Include(
            //    "~/Scripts/CRM/NG.Core/ngCache.js",
            //    "~/Scripts/CRM/NG.Directives/ngLeadSource.js",
            //    "~/Scripts/CRM/NG.Directives/ngPagination.js",
            //    "~/Scripts/CRM/NG.Directives/ngHoverIntent.js",
            //    "~/Scripts/CRM/NG.Directives/ngAddress.js",
            //    "~/Scripts/CRM/NG.Directives/ngPerson.js",
            //    "~/Scripts/CRM/NG.Core/ngNoteService.js",
            //   // "~/Scripts/CRM/NG.Core/ngLeadService.js",
            //    "~/Scripts/CRM/NG.Core/ngJobService.js"
            //    //"~/Scripts/CRM/NG.Controllers/ngLeadListController.js"
            //    );
            //leadListBundle.Transforms.Clear();
            //bundles.Add(leadListBundle);

            bundles.Add(new ScriptBundle("~/Scripts/jobs_list").Include(
                "~/Scripts/jquery.hoverIntent.js",
                "~/Scripts/CRM/KO.CustomBindings/googlemaps.js",
                "~/Scripts/CRM/KO.Models/address.js",
                "~/Scripts/CRM/KO.Models/user.js",
                "~/Scripts/CRM/KO.Models/person.js",
                "~/Scripts/CRM/KO.Models/quote.js",
                "~/Scripts/CRM/KO.Models/saleadjustment.js",
                "~/Scripts/CRM/KO.Models/job.js",
                "~/Scripts/CRM/KO.Models/jobitem.js",
                "~/Scripts/CRM/KO.ViewModels/notes.js",
                "~/Scripts/CRM/KO.ViewModels/filter_extension.js",
                "~/Scripts/native.history.js",
                "~/Scripts/parseParams.js"
            ));

            var jobListBundler = new ScriptBundle("~/Scripts/ngjobs_list").Include(
                "~/Scripts/jquery.hoverIntent.js",
                "~/Scripts/CRM/NG.Core/ngCache.js",
                "~/Scripts/CRM/KO.CustomBindings/googlemaps.js",
                "~/Scripts/CRM/KO.Models/address.js",
                "~/Scripts/CRM/KO.Models/user.js",
                "~/Scripts/CRM/KO.Models/person.js",
                "~/Scripts/CRM/KO.Models/quote.js",
                "~/Scripts/CRM/KO.Models/saleadjustment.js",
                "~/Scripts/CRM/KO.Models/job.js",
                "~/Scripts/CRM/KO.Models/jobitem.js",
                "~/Scripts/CRM/KO.ViewModels/notes.js",
                "~/Scripts/CRM/KO.ViewModels/filter_extension.js",
                "~/Scripts/CRM/KO.ViewModels/job_list.js",
                "~/Scripts/native.history.js"
                //"~/Scripts/CRM/NG.Controllers/ngJobListController.js"
                );
            jobListBundler.Transforms.Clear();
            bundles.Add(jobListBundler);

            var jobListNewBundler = new ScriptBundle("~/Scripts/ngjobs_listNew").Include(
                "~/Scripts/jquery.hoverIntent.js",
                "~/Scripts/CRM/NG.Core/ngCache.js",
                "~/Scripts/native.history.js",
                "~/Scripts/parseParams.js"
                //"~/Scripts/CRM/NG.Controllers/ngJobSearchController.js"

                );
            jobListNewBundler.Transforms.Clear();
            bundles.Add(jobListNewBundler);

            bundles.Add(new ScriptBundle("~/Scripts/new-lead-form").Include(
                "~/Scripts/jquery.maskedinput-1.3.1.js",
                "~/Scripts/ko.editables.js",
                "~/Scripts/CRM/KO.CustomBindings/masked.js",
                "~/Scripts/CRM/KO.Models/address.js",
                "~/Scripts/CRM/KO.Models/user.js",
                "~/Scripts/CRM/KO.Models/person.js",
                "~/Scripts/CRM/KO.ViewModels/addresses.js",
                "~/Scripts/CRM/KO.Models/leadsource.js",
                "~/Scripts/CRM/KO.Models/lead.js",
                //"~/Scripts/CRM/KO.ViewModels/lead_detail.js"
                string.Format("~/Scripts/CRM/KO.ViewModels{0}/lead_detail.js", BrandedPath)
            ));

            bundles.Add(new ScriptBundle("~/Scripts/lead-detail").Include(
                "~/Scripts/jquery.hoverIntent.js",
                "~/Scripts/jquery.maskedinput-1.3.1.js",
                "~/Scripts/bootbox.min.js",
                "~/Scripts/ko.editables.js",
                "~/Scripts/CRM/KO.CustomBindings/masked.js",
                "~/Scripts/CRM/KO.CustomBindings/googlemaps.js",
                "~/Scripts/CRM/KO.Models/address.js",
                "~/Scripts/CRM/KO.Models/user.js",
                "~/Scripts/CRM/KO.Models/person.js",
                "~/Scripts/CRM/KO.Models/product_option.js",
                "~/Scripts/CRM/KO.Models/jobitem.js",
                "~/Scripts/CRM/KO.Models/saleadjustment.js",
                "~/Scripts/CRM/KO.Models/quote.js",
                "~/Scripts/CRM/KO.Models/question.js",
                "~/Scripts/CRM/hfc.dropzone.js",
                "~/Scripts/CRM/KO.ViewModels/question_answers.js",
                "~/Scripts/CRM/KO.ViewModels/notes.js",
                "~/Scripts/CRM/KO.ViewModels/addresses.js",
                "~/Scripts/CRM/KO.ViewModels/edit_modal.js",
                "~/Scripts/CRM/KO.Models/leadsource.js",
                "~/Scripts/CRM/KO.Models/lead.js",
                "~/Scripts/CRM/KO.Models/job.js",
                //"~/Scripts/CRM/KO.ViewModels/lead_detail.js"
                string.Format("~/Scripts/CRM/KO.ViewModels{0}/lead_detail.js", BrandedPath),
                "~/Scripts/CRM/printable.js"
            ));
            bundles.Add(new ScriptBundle("~/Scripts/lead-detail-calendar").Include(
                "~/Scripts/bootstrap3-typeahead.js",
                "~/Scripts/CRM/KO.CustomBindings/typeahead.js",
                "~/Scripts/CRM/KO.Models/event.js",
                "~/Scripts/CRM/KO.Models/task.js",
                "~/Scripts/CRM/KO.Models/recurring.js",
                "~/Scripts/CRM/KO.Models/calendar.js",
                "~/Scripts/CRM/KO.ViewModels/calendar.js",
                "~/Scripts/CRM/KO.ViewModels/tasks.js",
                "~/Scripts/CRM/KO.ViewModels/calendar_modal.js",
                "~/Scripts/CRM/calevents_popover.js"
            ));

            bundles.Add(new ScriptBundle("~/Scripts/reports").Include(
                "~/Scripts/bootstrap-datepicker.js"));

            var settingsBundle = new ScriptBundle("~/Scripts/settings").Include("~/Scripts/CRM/hfc.settings.js");
            settingsBundle.Transforms.Clear();
            bundles.Add(settingsBundle);

            var dropZoneBundle = new ScriptBundle("~/Scripts/dropzonejs").Include(
                "~/Scripts/dropzone/dropzone.js",
                "~/Scripts/CRM/NG.Directives/ngDropzone.js"
                );
            dropZoneBundle.Transforms.Clear();
            bundles.Add(dropZoneBundle);

            bundles.Add(new ScriptBundle("~/Scripts/settings/account").Include(
                "~/Scripts/ko.editables.js",
                "~/Scripts/select2.js",
                "~/Scripts/jquery.maskedinput-1.3.1.js",
                "~/Scripts/CRM/KO.CustomBindings/masked.js",
                "~/Scripts/CRM/KO.CustomBindings/numeric.js",
                "~/Scripts/CRM/KO.Models/question.js",
                "~/Scripts/CRM/KO.Models/product.js",
                "~/Scripts/CRM/KO.Models/tax.js",
                "~/Scripts/CRM/KO.Models/generic.js",
                 "~/Scripts/tinycolor.js",
                "~/Scripts/bootstrap.colorpickersliders.js",
                string.Format("~/Scripts/CRM/KO.ViewModels/Settings{0}/account.js", BrandedPath)));

            bundles.Add(new ScriptBundle("~/Scripts/settings/sources").Include(
                "~/Scripts/ko.editables.js",
                "~/Scripts/CRM/KO.CustomBindings/datepicker.js",
                "~/Scripts/CRM/KO.CustomBindings/tooltip.js",
                "~/Scripts/CRM/KO.Models/campaign.js",
                "~/Scripts/CRM/KO.Models/generic.js",
                "~/Scripts/CRM/KO.ViewModels/Settings/sources.js"));

            bundles.Add(new ScriptBundle("~/Scripts/email").Include(
                "~/Scripts/select2.js",
                "~/Scripts/Summernote/summernote.js",
                "~/Scripts/CRM/email_modal.js"
                ));

            var advanceEmailBundle = new ScriptBundle("~/Scripts/advancedEmail").Include(
                "~/Scripts/select2.js",
                "~/Scripts/summernote_0.6/summernote.js",
                "~/Scripts/summernote_0.6/summernote-ext-fields.js",
                "~/Scripts/summernote_0.6/summernote-ext-fontstyle.js",
                "~/Scripts/angular-summernote.js",
                "~/Scripts/CRM/NG.Core/ngAdvancedEmailService.js"
                );
            advanceEmailBundle.Transforms.Clear();
            bundles.Add(advanceEmailBundle);

            var invoicesBundle = new ScriptBundle("~/Scripts/invoices").Include(
                "~/Scripts/angular-animate.js",
                "~/Scripts/bootbox.min.js",
                "~/Scripts/CRM/NG.Directives/ngPagination.js",
                //"~/Scripts/CRM/NG.Core/ng-hfc-app.js",
                "~/Scripts/CRM/NG.Core/ngInvoiceService.js",
                "~/Scripts/CRM/NG.Core/ngJobService.js",
                "~/Scripts/CRM/NG.Controllers/ng-invoice-ctrl.js",
                "~/Scripts/native.history.js",
                "~/Scripts/parseParams.js"
                );
            invoicesBundle.Transforms.Clear();
            bundles.Add(invoicesBundle);

            var purchaseOrdersBundle = new ScriptBundle("~/Scripts/purchaseOrders").Include(
                "~/Scripts/bootbox.min.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/CRM/NG.Directives/ngPagination.js",
                "~/Scripts/CRM/NG.Core/ngPurchaseOrderService.js",
                "~/Scripts/CRM/NG.Controllers/ng-purchaseOrder-ctrl.js"
                );
            purchaseOrdersBundle.Transforms.Clear();
            bundles.Add(purchaseOrdersBundle);

            var orderReviewsBundle = new ScriptBundle("~/Scripts/OrderReviews").Include(
                "~/Scripts/CRM/NG.Core/ngOrderService.js",
                "~/Scripts/CRM/NG.Controllers/ngOrderReviewController.js"
                );
            orderReviewsBundle.Transforms.Clear();
            bundles.Add(orderReviewsBundle);

            // not used anywhere--murugan

            //var invoiceDetailsBundle = (new ScriptBundle("~/Scripts/invoice_detail").Include(
            //    "~/Scripts/select2.js",
            //    "~/Scripts/angular-ui/ui-select2.js",
            //    "~/Scripts/summernote_0.6/summernote.js",
            //    "~/Scripts/summernote_0.6/summernote-ext-fields.js",
            //    "~/Scripts/summernote_0.6/summernote-ext-fontstyle.js",
            //    "~/Scripts/angular-summernote.js",
            //    "~/Scripts/CRM/NG.Directives/ngHoverIntent.js",
            //    "~/Scripts/CRM/NG.Directives/ngAddress.js",
            //    "~/Scripts/CRM/NG.Directives/ngPerson.js",
            //    "~/Scripts/CRM/NG.Core/ngNoteService.js",
            //    "~/Scripts/CRM/NG.Core/ngInvoiceService.js",
            //    "~/Scripts/CRM/NG.Core/ngJobService.js",
            //    //"~/Scripts/CRM/NG.Core/ngLeadService.js",
            //    "~/Scripts/CRM/NG.Core/ngEmailService.js",
            //    "~/Scripts/CRM/NG.Core/ngPrintService.js",
            //    "~/Scripts/CRM/NG.Core/ngESignatureService.js",
            //    "~/Scripts/CRM/NG.Directives/ngPayment.js",
            //    "~/Scripts/CRM/NG.Core/ng-hfc-app.js",
            //    "~/Scripts/CRM/NG.Controllers/ngInvoiceDetailController.js"
            //    ));
            //invoiceDetailsBundle.Transforms.Clear();
            //bundles.Add(invoiceDetailsBundle);

            var tinyMceBundle = new ScriptBundle("~/Scripts/tinymce/bundle").Include(
                "~/Scripts/angular-sanitize.js"
                );
            tinyMceBundle.Transforms.Clear();

            bundles.Add(tinyMceBundle);

            bundles.Add(new ScriptBundle("~/Scripts/ng-email").Include(
                "~/Scripts/select2.js",
                "~/Scripts/angular-ui/ui-select2.js",
                "~/Scripts/CRM/NG.Core/ngPrintService.js",
                "~/Scripts/CRM/NG.Core/ngPrintService.js",
                "~/Scripts/CRM/NG.Core/ngESignatureService.js"
                ));

            // Not used anywhere--murugan

            //var leadDetailBundle = new ScriptBundle("~/Scripts/ng-lead-detail").Include(
            //    "~/Scripts/ng-sortable.js",
            //    "~/Scripts/ng-numbersOnly.js",
            //    "~/Scripts/bootbox.min.js",
            //    string.Format("~/Scripts/CRM/NG.Core{0}/ngQuoteService.js", BrandedPath),
            //    string.Format("~/Scripts/CRM/NG.Core{0}/ngQuoteItemService.js", BrandedPath),
            //    string.Format("~/Scripts/CRM/NG.Core{0}/ngQuoteSaleAdjService.js", BrandedPath),
            //    "~/Scripts/CRM/NG.Core/ngCache.js",
            //    //"~/Scripts/CRM/NG.Core/ngLeadService.js",
            //    "~/Scripts/CRM/NG.Core/ngEmailService.js",
            //    "~/Scripts/CRM/NG.Core/ngPrintService.js",
            //    "~/Scripts/CRM/NG.Core/ngJobService.js",
            //    "~/Scripts/CRM/NG.Core/ngNoteService.js",
            //    "~/Scripts/CRM/NG.Core/ngInvoiceService.js",
            //    "~/Scripts/CRM/NG.Core/ngOrderService.js",
            //    "~/Scripts/CRM/NG.Core/ngPrintService.js",
            //    "~/Scripts/CRM/NG.Directives/ngHoverIntent.js",
            //    "~/Scripts/CRM/NG.Directives/ngAddress.js",
            //    "~/Scripts/CRM/NG.Directives/ngPerson.js",
            //    "~/Scripts/CRM/NG.Directives/ngLeadSource.js",
            //    //"~/Scripts/CRM/NG.Directives/ngLeadCampaign.js",
            //    "~/Scripts/CRM/NG.Directives/ngJobPanel.js",
            //    "~/Scripts/CRM/NG.Directives/ngPayment.js",
            //    string.Format("~/Scripts/CRM/NG.Directives{0}/ngQuotePanel.js", BrandedPath),
            //    "~/Scripts/CRM/NG.Directives/ngCalendarModal.js"
            //    );
            //leadDetailBundle.Transforms.Clear();
            //bundles.Add(leadDetailBundle);

            //var newLeadBundle = new ScriptBundle("~/Scripts/ng-new-lead").Include("~/Scripts/select2.js",
            //    "~/Scripts/angular-ui/ui-select2.js",
            //    "~/Scripts/CRM/NG.Core/ngCache.js",
            //    "~/Scripts/CRM/NG.Directives/ngDropzone.js",
            //    "~/Scripts/summernote_0.6/summernote.js",
            //    "~/Scripts/summernote_0.6/summernote-ext-fields.js",
            //    "~/Scripts/summernote_0.6/summernote-ext-fontstyle.js",
            //    "~/Scripts/angular-summernote.js",
            //    "~/Scripts/angular-tree-control.js",
            //    "~/Scripts/CRM/NG.Core/ngHFCServiceold.js",
            //    "~/Scripts/CRM/NG.Directives/ngLeadSource.js",
            //    "~/Scripts/CRM/NG.Directives/ngPerson.js",
            //    //"~/Scripts/CRM/NG.Core/ngLeadService.js",
            //    "~/Scripts/CRM/NG.Core/ngEmailService.js",
            //    "~/Scripts/CRM/NG.Core/ngPrintService.js",
            //    "~/Scripts/CRM/NG.Core/ngJobService.js",
            //    //"~/Scripts/CRM/NG.Controllers/ngLeadController.js",
            //    "~/Scripts/bootbox.min.js");
            //newLeadBundle.Transforms.Clear();
            //bundles.Add(newLeadBundle);

            bundles.Add(new ScriptBundle("~/Scripts/QuickBooks").Include(
                "~/Scripts/CRM/NG.Core/ngQuickBooksService.js",
                "~/Scripts/CRM/NG.Controllers/ngQuickBooksController.js"
            // "~/app/services/common/ngHFCService.js"
            ));

            var accountBundle = new ScriptBundle("~/Scripts/Account").Include(
                "~/Scripts/ControlPanel/NG.Controllers/ngInvoiceOptionsController.js",
                "~/Scripts/ControlPanel/NG.Controllers/ngAppointmentTypeColorController.js",
                "~/Scripts/ControlPanel/NG.Controllers/ngUserWidgetsController.js"
                );
            accountBundle.Transforms.Clear();
            bundles.Add(accountBundle);

            #endregion Script Bundles

            #region Reports Script Bundles

            var bundle = new ScriptBundle("~/Scripts/reports/summary").Include(
                "~/Scripts/CRM/KO.CustomBindings/selectpicker.js",
                "~/Scripts/CRM/KO.ViewModels/reports/summary.js");
            bundle.Transforms.Clear();
            bundles.Add(bundle);

            var salesBundle = new ScriptBundle("~/Scripts/reports/sales").Include(
                "~/Scripts/CRM/KO.CustomBindings/datepicker.js",
                "~/Scripts/CRM/KO.ViewModels/filter_extension.js",
                "~/Scripts/CRM/KO.ViewModels/reports/sales.js");
            bundle.Transforms.Clear();
            bundles.Add(salesBundle);

            bundles.Add(new ScriptBundle("~/Scripts/reports/salesBeta").Include(
              "~/Scripts/angular-animate.js",
              "~/Scripts/bootbox.min.js",
              "~/Scripts/CRM/NG.Directives/ngPagination.js",
              "~/Scripts/CRM/NG.Controllers/Report/salesBeta.js",
              "~/Scripts/jquery.jqGrid.min.js"));

            bundles.Add(new ScriptBundle("~/Scripts/reports/sentEmails").Include(
              "~/Scripts/angular-animate.js",
              "~/Scripts/bootbox.min.js",
              "~/Scripts/CRM/NG.Directives/ngPagination.js",
              "~/Scripts/CRM/NG.Controllers/Report/sentEmails.js",
              "~/Scripts/jquery.jqGrid.min.js"));

            bundles.Add(new ScriptBundle("~/Scripts/reports/marketing").Include(
                "~/Scripts/CRM/KO.CustomBindings/datepicker.js",
                "~/Scripts/CRM/KO.ViewModels/filter_extension.js",
                "~/Scripts/CRM/KO.ViewModels/reports/marketing.js"));

            bundles.Add(new ScriptBundle("~/Scripts/reports/mls").Include(
                "~/Scripts/CRM/KO.CustomBindings/datepicker.js",
                "~/Scripts/CRM/KO.ViewModels/filter_extension.js",
                "~/Scripts/CRM/KO.ViewModels/reports/mls.js"));

            bundles.Add(new ScriptBundle("~/Scripts/reports/msr").Include(
                "~/Scripts/CRM/printable.js",
                "~/Scripts/CRM/KO.ViewModels/reports/msr.js"));

            #endregion Reports Script Bundles

            #region Control Panel Script Bundles

            bundles.Add(new ScriptBundle("~/Scripts/CP/base").Include(
                "~/Scripts/ControlPanel/cp.core.js"
            ));

            bundles.Add(new ScriptBundle("~/Scripts/CP/franchise").Include(
                "~/Scripts/jquery.maskedinput-1.3.1.js",
                "~/Scripts/CRM/KO.Models/address.js",
                "~/Scripts/CRM/KO.Models/franchise.js",
                "~/Scripts/CRM/KO.Models/territories.js",
                "~/Scripts/CRM/KO.Models/franchiseRoyalties.js",
                "~/Scripts/CRM/KO.Models/FranchiseOwner.js",
                "~/Scripts/CRM/KO.Models/user.js",
                "~/Scripts/CRM/KO.Models/person.js",

                "~/Scripts/CRM/KO.ViewModels/franchise.js",
                "~/Scripts/CRM/KO.ViewModels/franchise_royalties.js",
                "~/Scripts/CRM/KO.ViewModels/user.js",
                "~/Scripts/ControlPanel/cp.franchise.js",
                "~/Scripts/CRM/KO.CustomBindings/masked.js",
                "~/Scripts/bootbox.min.js"

            ));

            bundles.Add(new ScriptBundle("~/Scripts/CP/dashboard").Include(
                "~/Scripts/ControlPanel/cp.charts.js"
            ));

            var vendorsBundle = new ScriptBundle("~/Scripts/CP/Vendors").Include(
                "~/Scripts/ControlPanel/NG.Controllers/ngVendorController.js"
                );
            vendorsBundle.Transforms.Clear();
            bundles.Add(vendorsBundle);

            #endregion Control Panel Script Bundles
        }

        /// <summary>
        /// Registers the styles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        private static void RegisterStyles(BundleCollection bundles)
        {
#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
            var angularCss = new Bundle("~/Content/bootstrapbundle")
                            .Include(
                            //"~/Content/bootstrap.css",
                            "~/Content/bootstrap-multiselect.css",
                            "~/Content/bootstrap-timepicker.css",
                            //"~/Content/angular-strap/bootstrap-additions.min.css",
                            "~/Content/angular-strap-datepicker.css",
                            "~/Content/angular-strap/animation.css",
                            "~/Content/css/tree-control.css",
                            "~/Content/bootstrap.colorpickersliders.css",
                            "~/Content/bootstrap-select.css",
                            "~/Content/css/angucomplete-alt.css",
                            "~/Content/css/jqx.base.css",
                            "~/Content/signpad/jquery.signaturepad.css");
            //angularCss.Transforms.Add(new CssTransformer());
            angularCss.Orderer = new NullOrderer();

            bundles.Add(angularCss);

            //bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
            //    "~/Content/bootstrap.css",
            //    //"~/Content/font-awesome.css", //if we start using more of their icons then we'll add it to base
            //    //"~/Content/bootstrap-datepicker3.css",
            //    "~/Content/bootstrap-select.css"
            //));

            bundles.Add(new StyleBundle("~/Content/css/email").Include(
                "~/Content/font-awesome.css",
                "~/Scripts/summernote/summernote.css",
                "~/Scripts/summernote/summernote.extension.css",
                "~/Content/css/select2.css",
                "~/Content/select2-bootstrap.css", //use this css with the above select2.css since it doesnt support bootstrap 3 styles
                "~/Content/email.css"
            ));

            bundles.Add(new StyleBundle("~/Content/font-awesome").Include(
                "~/Content/font-awesome.css",
                "~/Scripts/summernote/summernote.css",
                "~/Scripts/summernote/summernote.extension.css"
            ));
            bundles.Add(new StyleBundle("~/Content/themes/base/jqueryui")
               .Include("~/Content/themes/base/jquery-ui.css",
                   "~/Content/themes/base/jquery.ui.dialog.css"
                   ));

            bundles.Add(new StyleBundle("~/Content/base").Include(
                "~/Content/base.css",
                "~/Content/reminder.css",
                 "~/Scripts/angular-loading-bar/loading-bar.css"
                ));

            bundles.Add(new StyleBundle("~/Content/invoices").Include(
                "~/Content/bootstrap-datepicker3.css",
                "~/Content/ng-animation.css"
                ));

            bundles.Add(new StyleBundle("~/Content/dashboard").Include(
                "~/Content/evt_popover.css",
                "~/Content/crm-accordion.css",
                "~/Content/calendar.css",
                "~/Content/evt_edit_modal.css"
            ));

            bundles.Add(new StyleBundle("~/Content/calendar").Include(
                "~/Content/fullcalendar.css",
                "~/Content/calendar.css",
                "~/Content/evt_popover.css",
                "~/Content/crm-accordion.css",
                "~/Content/evt_edit_modal.css",
                "~/Content/bootstrap-datepicker.css",
                "~/Content/onetofour.css"

            ));

            bundles.Add(new StyleBundle("~/Content/leads_jobs_list").Include(
                "~/Content/leads_jobs_list.css"
            ));

            bundles.Add(new StyleBundle("~/Content/lead-detail").Include(
                "~/Content/lead-detail.css",
                "~/Content/evt_popover.css",
                "~/Content/crm-accordion.css",
                "~/Content/calendar.css",
                "~/Content/evt_edit_modal.css"/*,
                 "~/Content/bootstrap-datepicker3.css"*/
            ));

            bundles.Add(new StyleBundle("~/Content/ng-lead-detail").Include(
                "~/Content/ng-lead-detail.css"/*,
                "~/Content/bootstrap-datepicker3.css"*/
                ));

            bundles.Add(new StyleBundle("~/Scripts/dropzone/css/dropzone").Include(
                "~/Scripts/dropzone/css/dropzone.css"
                ));

            bundles.Add(new StyleBundle("~/Content/css/settings").Include(
                "~/Content/css/select2.css",
                "~/Content/select2-bootstrap.css",
                "~/Content/bootstrap-datepicker3.css",
                "~/Content/settings.css"));

            #region Control Panel Style Bundles

            bundles.Add(new StyleBundle("~/Content/CP/base").Include(
                "~/Content/ControlPanel/cp.base.css"
            ));

            #endregion Control Panel Style Bundles
        }
    }
}