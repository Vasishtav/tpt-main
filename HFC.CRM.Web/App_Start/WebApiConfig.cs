﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Web.Http.Routing;
using Elmah;
using HFC.CRM.Web.Filters;
using Newtonsoft.Json;

/// <summary>
/// The Web namespace.
/// </summary>
namespace HFC.CRM.Web
{
    /// <summary>
    /// Class WebApiConfig.
    /// </summary>
    /// 
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {

        public override void OnException(HttpActionExecutedContext context)
        {
            Exception ex = context.Exception;
            ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    public static class WebApiConfig
    {

        /// <summary>
        /// Gets the URL prefix.
        /// </summary>
        /// <value>The URL prefix.</value>
        public static string UrlPrefix { get { return "api"; } }
        /// <summary>
        /// Gets the URL prefix relative.
        /// </summary>
        /// <value>The URL prefix relative.</value>
        public static string UrlPrefixRelative { get { return "~/api"; } }

        /// <summary>
        /// Registers the specified configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public static void Register(HttpConfiguration config)
        {
            // Allow CORS for all origins. (Caution!)
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            config.Filters.Add(new CustomExceptionFilterAttribute());

            config.Filters.Add(new CompressFilter());

            // Convert all dates to UTC
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            
            config.Routes.MapHttpRoute(
                name: "GetActions",
                routeTemplate: UrlPrefix + "/{controller}/{id}",
                defaults: new { action = "Get" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
            );

            /*config.Routes.MapHttpRoute(
                name: "GetRecentLeads",
                routeTemplate: WebApiConfig.UrlPrefix + "/{controller}/{action}",
                defaults: new { action = "GetRecentLeads", cotroller = "LeadsController" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
            );

            config.Routes.MapHttpRoute(
                name: "GetRecentJobs",
                routeTemplate: WebApiConfig.UrlPrefix + "/{controller}/{action}",
                defaults: new { action = "recent", cotroller = "JobsController" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
            );*/

            config.Routes.MapHttpRoute(
                name: "PutActions",
                routeTemplate: UrlPrefix + "/{controller}/{id}",
                defaults: new { action = "Put" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) }
            );

            config.Routes.MapHttpRoute(
                name: "PostActions",
                routeTemplate: UrlPrefix + "/{controller}/{id}",
                defaults: new { action = "Post" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
            );

            config.Routes.MapHttpRoute(
                name: "DeleteActions",
                routeTemplate: UrlPrefix + "/{controller}/{id}",
                defaults: new { action = "Delete" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Delete) }
            );

            config.Routes.MapHttpRoute(
                name: "AdditionalActions",
                routeTemplate: UrlPrefix + "/{controller}/{id}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: UrlPrefix + "/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi2",
                routeTemplate: UrlPrefix + "/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();
        }
    }
}