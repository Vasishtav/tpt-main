(function ($) {
  // template, editor
  var tmpl = $.summernote.renderer.getTemplate();
  var editor = $.summernote.eventHandler.getEditor();

  // add plugin
  $.summernote.addPlugin({
    name: 'fields', // name of plugin
    buttons: { // buttons
      hello: function () {

        return tmpl.iconButton('fa fa-header', {
          event : 'hello',
          title: 'hello',
          hide: true
        });
      },
      fieldsDropdown: function () {
        var list = '<li><a data-event="insertField" href="#" data-value="{FirstName}">FirstName</a></li>';
             list += '<li><a data-event="insertField" data-value="{LastName}" href="#">LastName</a></li>';
        var dropdown = '<ul class="dropdown-menu" id="email-fields">' + list + '</ul>';

        return tmpl.iconButton('note-current-fontname', {
          title: 'Personalisation',
          hide: true,
          dropdown: dropdown,
          text: "Select Fields ",
          style: "width: 108px;"
        });
      }

    },

    events: { // events
      hello: function (layoutInfo) {
        // Get current editable node
        var $editable = layoutInfo.editable();

        // Call insertText with 'hello'
        editor.insertText($editable, 'hello ');
      },

      insertField: function (layoutInfo, value) {
        var $editable = layoutInfo.editable();

        editor.insertText($editable, value);
      }
    }
  });
})(jQuery);
