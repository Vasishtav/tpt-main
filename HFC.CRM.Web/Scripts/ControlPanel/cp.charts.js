﻿HFC.CP.Charts = {
    ViewModel: function () {
        var self = this;

        self.StartDate = ko.observable(new XDate().setDate(1)).extend({date: true});
        self.EndDate = ko.observable(new XDate()).extend({date: true});

        self.IsInstantiated = false;

        self.CurrentYear = null;
        //cache the result for redraw if window resizes
        self.CacheData = [];

        //self.BaseColor = ["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477", "#66aa00", "#b82e2e", "#316395"];
        //self.YearColor = [];

        function drawChart() {
            if (self.CacheData) {
                $.each(self.CacheData, function (i, c) {
                    c.Chart.draw(c.Data, c.Options);
                });
            }
        }

        function getCache(key) {
            var item = $.grep(self.CacheData, function (c) {
                return c.Key == key;
            });
            if(item && item.length)
                return item[0];
            else
                return null;
        }

        function fetchChartData(start, end, container_selector, callback) {            
            //var yearstart = new XDate(start.getFullYear(), 0, 1);
            //yearend = yearstart.clone().addYears(1).addDays(-1); //get end of the year
            var container = $(container_selector),
                args = Array.prototype.slice.call(arguments, 4);
            HFC.AjaxGetJson('/api/charts/0/controlpanel', { startDate: start.toString("MM/dd/yyyy"), endDate: end.toString("MM/dd/yyyy") },
                function (result) {
                    var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true });
                    
                    //#region Top 5 Franchise sales                   
                    var top5FranArray = [['Franchise', 'Sales']],
                        top5FranKey = container_selector + " .top5fran";
                    if (result.Top5FranSales) {
                        $.each(result.Top5FranSales, function (i, s) {
                            if (s.Value > 0)
                                top5FranArray.push([s.Key, s.Value]);
                        });
                    }
                    //since column chart doesnt handle empty data message, we will use piechart so it can display the message
                    //TEMPORARY HACK UNTIL GOOGLE ADD NO DATA HANDLING FOR COLUMN CHART
                    var top5FranChart = result.Top5FranSales && result.Top5FranSales.length ? new google.visualization.ColumnChart(container.find(".top5fran").get(0)) : new google.visualization.PieChart(container.find(".top5fran").get(0)),
                        top5FranData = google.visualization.arrayToDataTable(top5FranArray),
                        item = getCache(top5FranKey);
                        
                    formatter.format(top5FranData, 1);

                    if (item) {
                        item.Chart = top5FranChart;
                        item.Data = top5FranData;
                    } else {
                        var top5Franoptions = { title: 'Top 5 Franchise Sales', legend: { position: 'none' } };
                    
                        self.CacheData.push({ Key: top5FranKey, Chart: top5FranChart, Data: top5FranData, Options: top5Franoptions });
                    }

                    //#region top 5 source sales                  
                    var srcArray = [],
                        srcKey = container_selector + " .top5source";
                    if (result.Top5SourceSales) {
                        srcArray.push(['Source', 'Sales']);
                        $.each(result.Top5SourceSales, function (i, s) {
                            if(s.Value > 0)
                                srcArray.push([s.Key, s.Value]);
                        });
                    }
                    
                    //since column chart doesnt handle empty data message, we will use piechart so it can display the message
                    //TEMPORARY HACK UNTIL GOOGLE ADD NO DATA HANDLING FOR COLUMN CHART
                    var top5SrcChart = result.Top5SourceSales && result.Top5SourceSales.length ? new google.visualization.ColumnChart(container.find(".top5source").get(0)) : new google.visualization.PieChart(container.find(".top5source").get(0)),
                        top5SrcData = google.visualization.arrayToDataTable(srcArray),
                        item = getCache(srcKey);

                    formatter.format(top5SrcData, 1);

                    if (item) {
                        item.Chart = top5SrcChart;
                        item.Data = top5SrcData;
                    } else {
                        var top5Srcoptions = { title: 'Top 5 Source Sales', legend: { position: 'none' } };

                        self.CacheData.push({ Key: srcKey, Chart: top5SrcChart, Data: top5SrcData, Options: top5Srcoptions });
                    }

                    //#region top 4 lead status, and all other count                    
                    var leadArray = [['Lead Status', 'Count']],
                        leadKey = container_selector + " .leadstatus",
                        leadColors = [];
                    if (result.LeadStatus) {
                        $.each(result.LeadStatus, function (i, s) {
                            if (s.Value > 0) {
                                leadArray.push([s.Key, s.Value]);
                                leadColors.push(s.Color);
                            }
                        });
                    }
                    var leadData = google.visualization.arrayToDataTable(leadArray),
                        item = getCache(leadKey);
                        
                    if (item) {
                        item.Data = leadData;
                        item.Options.colors = leadColors;
                    } else {
                        var leadChart = new google.visualization.PieChart(container.find(".leadstatus").get(0)),
                            leadOptions = { title: 'Leads By Status', colors: leadColors };

                        self.CacheData.push({ Key: leadKey, Chart: leadChart, Data: leadData, Options: leadOptions });
                    }
                    

                    //#region top 4 job status and all other count
                    var jobArray = [['Job Status', 'Count']],
                        jobKey = container_selector + ". jobstatus",
                        jobColors = [];
                    if (result.JobStatus) {
                        $.each(result.JobStatus, function (i, s) {
                            if (s.Value > 0) {
                                jobArray.push([s.Key, s.Value]);
                                jobColors.push(s.Color);
                            }
                        });
                    }
                    var jobData = google.visualization.arrayToDataTable(jobArray),
                        item = getCache(jobKey);

                    if (item) {
                        item.Data = jobData;
                        item.Options.colors = jobColors;
                    } else {
                        var jobChart = new google.visualization.PieChart(container.find(".jobstatus").get(0)),
                            jobOptions = { title: 'Jobs By Status', colors: jobColors };

                        self.CacheData.push({ Key: jobKey, Chart: jobChart, Data: jobData, Options: jobOptions });
                    }                    

                    if($.isFunction(callback))
                        callback.apply(null, args);
                    else
                        drawChart();
                }
            );
        }

        self.fetchCharts = function (form) {            
            var start = self.StartDate.Value,
                end = self.EndDate.Value,
                sameYear = start.getFullYear() == self.CurrentYear;

            if (!sameYear) {
                self.CurrentYear = start.getFullYear();
                var yearstart = start.clone().setMonth(0).setDate(1),
                    yearend = yearstart.clone().addYears(1).addDays(-1);
                if (yearend > new XDate())
                    yearend = new XDate();
                fetchChartData(yearstart, yearend, ".cp-ytd", fetchChartData, start, end, ".cp-mtd");
                $("#year-span").text(start.getFullYear());
                $("#year-small").text("Jan 1 to " + yearend.toString("MMM d"))
            } else
                fetchChartData(start, end, ".cp-mtd");

            if (self.IsInstantiated) {
                $("#month-span").text("Custom Date");
                $("#month-small").text(start.toFriendlyString() + " to " + end.toFriendlyString());
            } else
                $("#month-span").text(start.toString('MMMM'));
            
            self.IsInstantiated = true;
        }

        //we add a resizeTO timeout so it only needs to resize when user stops resizing, not as user resize so it doesnt consume too much resource
        function redrawChart() {            
            if (this.resizeTimeOut) clearTimeout(this.resizeTimeOut);
            this.resizeTimeOut = setTimeout(function () {
                if (Math.abs(window.innerWidth - window.currentWidth) > 50) { //only resize if width changes, height will change often with bootstrap drop down menu                    
                    drawChart();
                    window.currentWidth = window.innerWidth;
                }
            }, 500);            
        };

        //bind to resize so we can refresh the chart and let google resize the chart if user resizes window
        $(window).unbind('resize.cpdashboard');
        $(window).bind('resize.cpdashboard', redrawChart);
        window.addEventListener("orientationchange", redrawChart);
        window.currentWidth = window.innerWidth;
        window.currentHeight = window.innerHeight;

    }
};