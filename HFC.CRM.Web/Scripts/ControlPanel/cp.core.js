﻿HFC.CP = {
    Impersonate: function (userId) {
        if (userId) {
            HFC.AjaxPost('/api/users/' + userId + '/Impersonate', null, function () {
                window.location = '/'; //go to home page, user should have been already switched
            });
        } else
            HFC.DisplayAlert("Invalid user");

        return false;
    },

    ChangeBrand: function (brandId) {
        if (brandId) {
            HFC.AjaxPost('/api/users/' + brandId + '/PostBrandChange', null, function () {
                //window.location = '/'; //go to home page, user should have been already switched
            });
        } else
            HFC.DisplayAlert("Invalid Brand");

        return false;
    }
};