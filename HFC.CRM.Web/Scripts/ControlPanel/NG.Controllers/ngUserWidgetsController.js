﻿
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.cp.UserWidgets', []).
        service('UserWidgetsService', [
            '$http', function($http) {
                var srv = this;
                srv.IsBusy = false;
                srv.Data = null;

                srv.Users = [];
                srv.Roles = [];
                srv.Widgets = [];

                srv.IsLoaded = false;


                srv.Save = function(data, roleId, userId) {
                    if (srv.IsBusy)
                        return;
                    srv.IsBusy = true;
                    $http.put('/api/users/0/UpdateWidgets?rId=' + roleId + '&uId=' + userId, data).then(function() {
                        srv.IsBusy = false;
                        HFC.DisplaySuccess("Widgets settings successfully saved");
                    }, function(response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }

                srv.GetUsers = function() {
                    if (srv.IsBusy)
                        return;
                    srv.IsBusy = true;
                    $http.get('/api/Users').then(function(response) {
                        srv.IsBusy = false;
                        srv.Users = response.data.Users;
                        return response.data;

                    }, function(response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }

                srv.GetRoles = function() {
                    $http.get('/api/users/0/getRoles').then(function(response) {
                        srv.Roles = response.data;

                    }, function(response) {
                        HFC.DisplayAlert(response.statusText);
                    });
                }

                srv.GetWidgets = function (roleId, userId) {
                    srv.Widgets = [];

                    $http.get('/api/users/0/GetWidgets?roleId=' + roleId + '&userId=' + userId).then(function (response) {
                        srv.Widgets = response.data;
                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }

            }
        ])
        .controller('UserWidgetsController', [
            '$http', '$scope', 'UserWidgetsService', function($http, $scope, UserWidgetsService) {

                $scope.data = null;

                $scope.User = null;
                $scope.Role = null;

                $scope.UserWidgetsService = UserWidgetsService;

                $scope.$watch('User', function(newVal, oldVal) {
                    if (newVal) {
                        $scope.Role = null;
                        $scope.UserWidgetsService.GetWidgets('', $scope.User.UserId);
                    }
                });

                $scope.$watch('Role', function (newVal, oldVal) {
                    if (newVal) {
                        $scope.User = null;
                        $scope.UserWidgetsService.GetWidgets($scope.Role.RoleId, '');
                    }
                });

                $scope.SaveSettings = function() {
                   // console.log($scope.UserWidgetsService.Widgets);

                    $scope.UserWidgetsService.Save($scope.UserWidgetsService.Widgets, $scope.Role ? $scope.Role.RoleId : '', $scope.User ? $scope.User.UserId : '');
                }

                /*$scope.$watch('AppointmentTypeColorService.Data.Installation', function(newVal, oldVal) {
                    if (newVal) {
                        $scope.AppointmentTypeColorService.IsLoaded = true;

                        $("#appSample").attr('style', 'background-color: ' + newVal + ' !important; width: 203.725px; height: 82px; margin-top: 250px; border-radius: 3px;line-height: 80px;text-align: center;');
                    }
                });

                $scope.Save = function() {
                    $scope.AppointmentTypeColorService.Save($scope.data);
                }

                $scope.Get = function() {
                    $scope.data = $scope.AppointmentTypeColorService.Get();
                }

                $scope.Get();*/

                $scope.UserWidgetsService.GetUsers();
                $scope.UserWidgetsService.GetRoles();
            }
        ]);

})(window, document);