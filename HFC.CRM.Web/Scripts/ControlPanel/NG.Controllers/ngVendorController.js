﻿
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.cp.vendor', [])
        .service('VendorService', ['$http', function ($http) {
            var srv = this;
            srv.IsBusy = false;
            srv.Rows = [];
            srv.Update = function (model) {
                if (srv.IsBusy)
                    return;

                srv.IsBusy = true;
                $http.put('/api/vendor/0/update', model).then(function () {
                    srv.IsBusy = false;
                    HFC.DisplaySuccess("Successfully saved");
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }

            srv.Get = function () {
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;
                $http.get('/api/vendor/0/get').then(function (response) {
                    srv.IsBusy = false;
                    srv.Rows = response.data;

                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }

            srv.Get();
            srv.Delete = function (model) {
                model.editMode = false;
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;

                //var index = srv.Rows.indexOf(model);
                //if (index > -1) {
                //    srv.Rows.splice(index, 1);
                //}
                if (model.VendorId != null) {
                    $http.delete('/api/vendor/' + model.VendorId + '/delete').then(function (response) {
                        srv.IsBusy = false;
                        srv.Get();
                        HFC.DisplaySuccess("Successfully deleted");
                        return response.data;

                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }
                else {
                    srv.IsBusy = false;
                }

            }

            srv.AddNewRow = function () {
                srv.Rows.push({ VendorId: null, Name: null, Description: null, Phone: null, AltPhone: null, Fax: null, Email: null, Website: null, ReferenceNum: null, IsActive: true, editMode: true });
            }

            srv.Insert = function (model) {
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;
                $http.post('/api/vendor/0/Create', model).then(function () {
                    srv.IsBusy = false;
                    HFC.DisplaySuccess("Successfully saved");
                    srv.Get();
                }, function (response) {
                    model.editMode = true;
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
            srv.EditingItem = {};
            srv.edit = function (model) {
                for (var i = 0; i < srv.Rows.length; i++) {
                    if (srv.Rows[i].editMode == true) {
                        return;
                    }
                }
                srv.EditingItem = angular.copy(model);
                model.editMode = true;
            }

            srv.save = function (model) {
                if (!model.Name) {
                    model.NameRequired = true;
                    return;
                }
                model.NameRequired = false;
                
                var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
                if (model.Email) {
                    if (EMAIL_REGEXP.test(model.Email) == false) {
                        model.validEmail = true;
                        return;
                    }
                }
                model.editMode = false;
                model.validEmail = false;
                if (model.VendorId == null) {
                    srv.Insert(model);
                }
                else {
                    srv.Update(model);
                }
            };

            srv.cancel = function (model) {
                model.editMode = false;
                if (model.VendorId == null) {
                    var index = srv.Rows.indexOf(model);
                    if (index > -1) {
                        srv.Rows.splice(index, 1);
                    }
                }
                else {
                    model.AltPhone = srv.EditingItem.AltPhone;
                    model.Description = srv.EditingItem.Description;
                    model.Email = srv.EditingItem.Email;
                    model.Fax = srv.EditingItem.Fax;
                    model.IsActive = srv.EditingItem.IsActive;
                    model.Name = srv.EditingItem.Name;
                    model.NameRequired = srv.EditingItem.NameRequired;
                    model.Phone = srv.EditingItem.Phone;
                    model.ReferenceId = srv.EditingItem.ReferenceId;
                    model.ReferenceNum = srv.EditingItem.ReferenceNum;
                    model.VendorGuid = srv.EditingItem.VendorGuid;
                    model.VendorId = srv.EditingItem.VendorId;
                    model.Website = srv.EditingItem.Website;
                }
            };

        }])
        .controller('VendorController', ['$http', '$scope', 'VendorService', function ($http, $scope, VendorService) {
            $scope.name = "Vendor Details";
            $scope.VendorService = VendorService;


        }]);

})(window, document);