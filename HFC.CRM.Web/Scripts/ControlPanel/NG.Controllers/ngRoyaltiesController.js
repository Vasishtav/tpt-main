﻿
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.cp.royalty', [])
        .service('RoyaltyrService', ['$http', function ($http) {
            var srv = this;
            srv.IsBusy = false;
            srv.Rows = [];
            srv.Update = function (model) {
                if (srv.IsBusy)
                    return;

                srv.IsBusy = true;
                $http.put('/api/royalty/0/update', model).then(function () {
                    srv.IsBusy = false;
                    HFC.DisplaySuccess("Successfully saved");
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }

            srv.Get = function () {
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;
                //$http.get('/api/royalty/0/get').then(function (response) {
                //    srv.IsBusy = false;
                //    srv.Rows = response.data;

                //}, function (response) {
                //    HFC.DisplayAlert(response.statusText);
                //    srv.IsBusy = false;
                //});
                srv.Rows = { RoyaltyId: 1, Name: "Royalty1", Amount: "100", Percent: "2", StartsOn: "16-9-2017", EndsOn: "16-9-2017", editMode:false };
            }

            srv.Get();
            srv.Delete = function (model) {
                model.editMode = false;
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;

                
                if (model.RoyaltyId != null) {
                    $http.delete('/api/royalty/' + model.RoyaltyId + '/delete').then(function (response) {
                        srv.IsBusy = false;
                        srv.Get();
                        HFC.DisplaySuccess("Successfully deleted");
                        return response.data;

                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }
                else {
                    srv.IsBusy = false;
                }

            }

            srv.AddNewRow = function () {
                srv.Rows.push({
                    RoyaltyId: null, Name: null, Amount: null, Percent: null,
                    StartsOn: null, EndsOn: null, editMode: true
                });
            }

            srv.Insert = function (model) {
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;
                $http.post('/api/royalty/0/Create', model).then(function () {
                    srv.IsBusy = false;
                    HFC.DisplaySuccess("Successfully saved");
                    srv.Get();
                }, function (response) {
                    model.editMode = true;
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
            srv.EditingItem = {};
            srv.edit = function (model) {
                for (var i = 0; i < srv.Rows.length; i++) {
                    if (srv.Rows[i].editMode == true) {
                        return;
                    }
                }
                srv.EditingItem = angular.copy(model);
                model.editMode = true;
            }

            srv.save = function (model) {
                if (!model.Name) {
                    model.NameRequired = true;
                    return;
                }
                model.NameRequired = false;
                
                 model.editMode = false;
               
                if (model.RoyaltyId == null) {
                    srv.Insert(model);
                }
                else {
                    srv.Update(model);
                }
            };

            srv.cancel = function (model) {
                model.editMode = false;
                if (model.RoyaltyId == null) {
                    var index = srv.Rows.indexOf(model);
                    if (index > -1) {
                        srv.Rows.splice(index, 1);
                    }
                }
                else {

                    model.RoyaltyId = srv.EditingItem.RoyaltyId;
                    model.Name = srv.EditingItem.Name;
                    model.Amount = srv.EditingItem.Amount;
                    model.Percent = srv.EditingItem.Percent;
                    model.StartsOn = srv.EditingItem.StartsOn;
                    model.EndsOn = srv.EditingItem.EndsOn;
                }
            };

        }])
        .controller('RoyaltyController', ['$http', '$scope', 'RoyaltyService', function ($http, $scope, RoyaltyService) {
            $scope.name = "Royalty Details";
            $scope.RoyaltyService = RoyaltyService;


        }]);

})(window, document);