﻿
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.cp.invoiceOptions', [])
        .service('InvoiceOptionsService', ['$http', function ($http) {
            var srv = this;
            srv.IsBusy = false;
            srv.Data = null;
            srv.Save = function (data) {
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;
                $http.post('/api/Franchise/0/InvoiceOption', data).then(function () {
                    srv.IsBusy = false;
                    if (success) {
                        success();
                    }
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
            srv.Get = function () {
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;
                $http.get('/api/Franchise/0/InvoiceOption').then(function (response) {
                    srv.IsBusy = false;
                    return response.data.entity;
                    
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
        }])
        .controller('InvoiceOptionsController', ['$http', '$scope', 'InvoiceOptionsService', function ($http, $scope, InvoiceOptionsService) {
            $scope.data = null;
            $scope.InvoiceOptionsService = InvoiceOptionsService;
            $scope.Save = function () {
                $scope.InvoiceOptionsService.Save($scope.data);
            }
            $scope.Get = function () {
                $http.get('/api/Franchise/0/InvoiceOption').then(function (response) {
                    $scope.data = response.data.entity;

                });
            }
            $scope.Get();
        }])

})(window, document);