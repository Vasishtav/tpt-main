﻿
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.cp.AppointmentTypeColors', []).
        directive('colorPicker', function ($parse) {
            return {
                restrict: 'A',
            require: 'ngModel',
            link: function (scope, elm, attrs, ngModel) {
                var getter = $parse(attrs.ngModel);
                var value = getter(scope);

                var options = angular.extend({
                    swatches: window.Swatches,
                    //flat: true,
                    color: value,
                    onchange: function (container, color) {
                        scope.$apply(function() {
                            ngModel.$setViewValue(color.tiny.toRgbString());
                        });
                    }
                }, scope.$eval(attrs.colorPicker));
           
                ngModel.$render = function () {

//                    elm.ColorPickerSliders(attrs.colorPicker);
                };

                elm.ColorPickerSliders(options);
            }
        };
    }).service('AppointmentTypeColorService', ['$http', function ($http) {
            var srv = this;
            srv.IsBusy = false;
            srv.Data = null;
            srv.IsLoaded = false;
            srv.Save = function (data) {
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;
                $http.post('/api/AppointmentTypeColor/0/UpdateColors', srv.Data).then(function () {
                    srv.IsBusy = false;
                    HFC.DisplaySuccess("Colors successfully saved");
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }

            srv.Get = function () {
                if (srv.IsBusy)
                    return;
                srv.IsBusy = true;
                $http.get('/api/AppointmentTypeColor/0/GetColors').then(function (response) {
                    srv.IsBusy = false;
                    srv.Data = response.data;
                    return response.data;
                    
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
           
        }])
        .controller('AppointmentTypeColorController', ['$http', '$scope', 'AppointmentTypeColorService', function ($http, $scope, AppointmentTypeColorService) {
            
            $scope.data = null;
            $scope.AppointmentTypeColorService = AppointmentTypeColorService;

            $scope.$watch('AppointmentTypeColorService.Data.Installation', function (newVal, oldVal) {
                if (newVal) {
                    $scope.AppointmentTypeColorService.IsLoaded = true;

                    $("#appSample").attr('style', 'background-color: ' + newVal + ' !important; width: 203.725px; height: 82px; margin-top: 250px; border-radius: 3px;line-height: 80px;text-align: center;');
                }
            });

            $scope.Save = function () {
                $scope.AppointmentTypeColorService.Save($scope.data);
            }

            $scope.Get = function () {
                $scope.data = $scope.AppointmentTypeColorService.Get();
            }

            $scope.Get();
        }])

})(window, document);