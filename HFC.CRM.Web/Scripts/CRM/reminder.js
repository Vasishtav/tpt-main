﻿HFC.Reminder = {
    ModalId: "crm-reminder-modal",
    BellId: "crm-reminder-bell",
    Model: function(rawModel) {
        var self = this;
        ko.mapping.fromJS(rawModel, {
            StartDate: {
                create: function(options) { return ko.observable(options.data ? new XDate(options.data) : null); }
            },
            EndDate: {
                create: function(options) { return ko.observable(options.data ? new XDate(options.data) : null); }
            }
        }, self);

        self.EventDateTxt = ko.computed(function() {
            var rangeText = "";
            if (self.StartDate()) {
                if (self.EndDate()) {
                    var minDiff = self.StartDate().diffMinutes(self.EndDate()),
                        start = "",
                        end = "";

                    if (minDiff <= 1440) { //both are happening within same day
                        rangeText = self.StartDate().toFriendlyString();
                        if (!self.IsAllDay())
                            rangeText += ", " + self.StartDate().toString("h:mm tt") + " to " + self.EndDate().toString("h:mm tt");
                    } else {
                        start = self.StartDate().toFriendlyString(!self.IsAllDay());
                        end = self.EndDate().toFriendlyString(!self.IsAllDay());
                        if (start == end)
                            rangeText = start;
                        else
                            rangeText = start + " to " + end;
                    }
                } else
                    rangeText = self.StartDate().toFriendlyString(true);
            }

            return rangeText;
        });
        //we're doing two bool since we dont want KO to switch the active flag for user when they hit button instead of selecting
        self.IsActive = ko.observable(false);
        self.IsSelected = ko.observable(false);
        self.ToggleActive = function() {
            self.IsActive(!self.IsActive());
            self.IsSelected(self.IsActive());
        }

        self.GlyphIcon = ko.computed({
            read: function() {
                var _icon = "";
                switch (self.AptTypeEnum()) {
                case 13:
                    _icon = "glyphicon glyphicon-earphone";
                    break;
                case 12:
                    _icon = "glyphicon glyphicon-comment";
                    break;
                case 11:
                    _icon = "glyphicon glyphicon-home";
                    break;
                case 10:
                    _icon = "glyphicon glyphicon-glass";
                    break;
                case 9:
                    _icon = "glyphicon glyphicon-plane";
                    break;
                case 8:
                    _icon = "glyphicon glyphicon-eye-close";
                    break;
                case 7:
                    _icon = "glyphicon glyphicon-question-sign";
                    break;
                case 6:
                    _icon = "glyphicon glyphicon-wrench";
                    break;
                case 5:
                    _icon = "glyphicon glyphicon-wrench";
                    break;
                case 4:
                    _icon = "glyphicon glyphicon-Four";
                    break;
                case 3:
                    _icon = "glyphicon glyphicon-Three";
                    break;
                case 2:
                    _icon = "glyphicon glyphicon-Two";
                    break;
                case 1:
                default:
                    _icon = "glyphicon glyphicon-One";
                }
                return _icon;
            },
            deferEvaluation: true
        })
    },
    Collection: function() {
        var self = this;
        self.Collection = ko.observableArray();
        self.SnoozeMinutes = ko.observable(15);
        //Only need this if we are using input to check for valid number input
        //self.SnoozeMinutes.subscribe(function () {
        //    var pattern = /^(\d{1,})$/;
        //    if (!pattern.test(self.SnoozeMinutes()))
        //        self.SnoozeMinutes(10);
        //});
        self.Selected = function() {
            return $.map(self.Collection(), function(obj) {
                return obj.IsSelected() ? obj.EventPersonId() : null;
            });
        };
        self.SelectAll = function() {
            $(self.Collection()).each(function(i, itm) {
                itm.IsSelected(true);
            });
        };

        function _checkBell() {
            if (self.Collection().length)
                $("#" + HFC.Reminder.BellId).addClass("active");
            else
                $("#" + HFC.Reminder.BellId).removeClass("active");
        }

        function _post(data) {
            HFC.AjaxPost('/api/reminders', data,
                function(result) {
                    self.Collection.remove(function(m) {
                        return m.IsSelected();
                    });
                    if (self.Collection().length == 0)
                        $('#' + HFC.Reminder.ModalId).modal('hide');
                    _checkBell();
                }
            );
        }

        self.Dismiss = function() {
            if (self.Collection().length == 1)
                self.Collection()[0].IsSelected(true); //auto select first one if there is only 1

            var idArr = self.Selected();
            if (idArr.length > 0)
                _post({ ids: idArr });
        }
        self.DismissAll = function() {
            self.SelectAll();
            self.Dismiss();
        }
        self.Snooze = function() {
            if (self.Collection().length == 1)
                self.Collection()[0].IsSelected(true); //auto select first one if there is only 1

            var idArr = self.Selected();
            if (idArr.length > 0)
                _post({ ids: idArr, minutes: self.SnoozeMinutes() });
        }

        //we will be using localstorage to keep track of reminders so we don't have it pop up every time new page is loaded
        //and easier to remove dismissed events from two separate pages
        self.Merge = function(activeReminders) {
            var storageKey = "CRMReminders" + HFC.CurrentUser.PersonId;

            if (activeReminders)
                localStorage.setItem(storageKey, JSON.stringify(activeReminders)); //save to localstorage for all pages to use                
            else //no active reminders so clear it from storage
                localStorage.removeItem(storageKey);

            self.RefreshReminderCollection();
        }
        self.RefreshCount = 0;

        self.RefreshReminderCollection = function() {
            var storageKey = "CRMReminders" + HFC.CurrentUser.PersonId,
                storageVal = localStorage.getItem(storageKey),
                activeReminders = null;
            if (storageVal)
                activeReminders = JSON.parse(storageVal);

            if (activeReminders) {
                var displayReminder = false;
                $.each(activeReminders, function(i, rem) {
                    var existInCol = $.grep(self.Collection(), function(obj) {
                        return obj.EventPersonId() == rem.EventPersonId;
                    });
                    if (existInCol == null || existInCol.length == 0) {
                        self.Collection.push(new HFC.Reminder.Model(rem));
                        //if (self.RefreshCount > 0)
                            displayReminder = true;
                    }
                });
                self.Collection.removeAll(function(rem) {
                    var exist = $.grep(activeReminders, function(obj) {
                        return obj.EventPersonId == rem.EventPersonId();
                    });
                    return exist == null || exist.length == 0;
                });

                //finally display any new reminders that is not already in storage
                if (displayReminder)
                    $('#' + HFC.Reminder.ModalId).modal('show');
            } else {
                self.Collection.removeAll();
                $('#' + HFC.Reminder.ModalId).modal('hide'); //hide modal in case it is opened
            }

            self.RefreshCount++;
            _checkBell();
        }
        var streamErrorCount = 0;
        self.InitEventStream = function(excl_email) {

            var url = "/eventstream.ashx",
                qs = [];
            //if (excl_counters && excl_reminders)
            //    url += "?excl_counters=true&excl_reminders=true";
            if (excl_email)
                qs.push("excl_email=true");
            if (HFC.Browser.isIOS())
                qs.push("isSingleConnect=true");
            if (qs.length)
                url += "?" + qs.join("&");
            //else if (excl_reminders)
            //    url += "?excl_reminders=true";
            if (window.opener != null) {
                self.RefreshReminderCollection();
                //set timer to check localstorage every min
                setInterval(self.RefreshReminderCollection, 60000);
            } else {
                //timeout is for safari, so it doesnt keep thinking the page is in loading state
                var source = new EventSource(url);
                try {
                    source.addEventListener("reminder",
                        function(e) {
                            streamErrorCount = 0;
                            if (e.origin.toLowerCase() == window.location.origin.toLowerCase()) {
                                this.retry = e.retry;
                                var json = JSON.parse(e.data);
                                if (json) {
                                    HFC.Reminder.KO.Merge(json);
                                }
                            }
                        }, false);
                    source.onerror = function(e) {
                        if (streamErrorCount > 5)
                            this.close();
                        else {
                            this.retry = 60000;
                            streamErrorCount++;
                        }
                        //self.RefreshReminderCollection();
                        //setInterval(self.RefreshReminderCollection, 60000);
                    };
                    source.onmessage = function(e) {
                        streamErrorCount = 0;
                        if (e.origin.toLowerCase() == window.location.origin.toLowerCase()) {
                            this.retry = e.retry;
                            var json = JSON.parse(e.data);
                            if (json && json.TaskCount != null)
                                $("#tasksCount").text(json.TaskCount);
                            else
                                $("#tasksCount").text("~");
                            if (json && json.EmailCount != null)
                                $("#emailCount").text(json.EmailCount);
                            else
                                $("#emailCount").text("~");
                            if (json && json.NewLeadCount != null)
                                $("#newLeadCount").text(json.NewLeadCount);
                            else
                                $("#newLeadCount").text("~");
                        }
                    };
                } catch (err) {
                    source.close();
                }
            }
        }

        self.InitSocketEventStream = function() {
            $(function () {
                
                var reminderHub = $.connection.reminderHub;

                reminderHub.client.updateCounters = function(message) {
                    //console.log('updateCounters push message received');

                    if (message && message.TaskCount != null)
                        $("#tasksCount").text(message.TaskCount);
                    else
                        $("#tasksCount").text("~");
                    /* if (message && message.EmailCount != null)
                        $("#emailCount").text(message.EmailCount);
                    else
                        $("#emailCount").text("~");*/
                    if (message && message.NewLeadCount != null)
                        $("#newLeadCount").text(message.NewLeadCount);
                    else
                        $("#newLeadCount").text("~");
                };

                reminderHub.client.updateReminders = function(message) {
                    //console.log('updateReminders push message received');
                    var json = JSON.parse(message);
                    if (json) {
                        HFC.Reminder.KO.Merge(json);
                    }
                };

                // Start the connection.
                $.connection.hub.start().done(function() {
                    console.log('websocket connection established');
                });
            });
        }
    }
}

$(document).ready(function() {
    HFC.Reminder.KO = new HFC.Reminder.Collection();
    ko.applyBindings(HFC.Reminder.KO, document.getElementById(HFC.Reminder.ModalId));
    HFC.Reminder.KO.InitSocketEventStream(); //connect to the eventstream for updating reminders

    if ($("#emailCount").length > 0) {
        $.get('/api/email/0/GetUnreadMessageCount').then(function(response) {
            $("#emailCount").text(response.unreadMessageCount);
        });
    }

});