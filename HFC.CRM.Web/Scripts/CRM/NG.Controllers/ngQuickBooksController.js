﻿(function (window, document, undefined) {
    'use strict';


    angular.module('hfc.quickbooks', ['hfc.quickbooks.service', 'hfc.core.service'])

        .directive('datepicker', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {
                    $(function () {
                        element.datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            dateFormat: 'dd/mm/yy',
                            onSelect: function (date) {
                                scope.$apply(function () {
                                    ngModelCtrl.$setViewValue(date);
                                });
                            }
                        });
                    });
                }
            }
        })

        .controller('QuickBooksController', ['$http', '$scope', 'QuickBooksService', '$sce', 'HFCService', function ($http, $scope, QuickBooksService, $sce, HFCService) {

           

            // This method trigger when QBINTRNL page
            $scope.initQBApp = function (feId) {
                $scope.QBApp = {};
                $scope.GetQBApp(feId);
            };

            $scope.HFCService = HFCService;
            $scope.Permission = {};
            var Quickbookpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'QuickBooksSettings')
            var QuickSyncpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'QuickbooksSync')

            $scope.Permission.Quickbookdisplay = Quickbookpermission.CanRead; //$scope.HFCService.GetPermissionTP('QuickBooks Settings-QBO Display').CanAccess;
            $scope.Permission.Quickbookadd = Quickbookpermission.CanCreate; //$scope.HFCService.GetPermissionTP('QuickBooks Settings-QBO Add').CanAccess;
            $scope.Permission.QuickbookSyncdetails = QuickSyncpermission.CanRead; //$scope.HFCService.GetPermissionTP('Quickbooks Sync Detail').CanAccess;
            $scope.Permission.QuickbookSyncadd = QuickSyncpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Quickbooks Sync Add').CanAccess;
            

            // save qb app info
            $scope.SaveQBApp = function (feId) {
                $scope.QBApp.FranchiseId = feId;
                //if (!window.syncApi)
                //    return;

                $http.post('../api/Quickbooks/0/SaveQBApp/', $scope.QBApp).then(function () {
                    //HFC.DisplaySuccess("Saved Successfully");
                    window.alert("Saved Successfully");
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                });
            };

            // QB online sync 
            $scope.Sync = function (feId) {
                if (!window.syncApi)
                    return;

                function PreSyncCheckSuccess(result) {
                    if ($scope.preSyncCheck.Correct || window.confirm("There are errors that will prevent syncing some records, would you like to: \"Sync now with errors\" or \"Cancel the Sync and go correct the errors\". The sync will not run unless the user clicks \"OK\"")) {
                        $scope.state.IsLocked = true;
                        $http.post(syncApi + '/api/quickbooks/' + feId + '/sync/').then(function (response) {
                            HFC.DisplaySuccess("QBO Sync Successfully");
                            $scope.UpdateState();
                        }, function (response) {
                            HFC.DisplayAlert(response.statusText);
                            $scope.UpdateState();
                        });
                    }
                }

                $scope.PreSyncCheck(PreSyncCheckSuccess);

            }

            // Get QB app info
            $scope.GetQBApp = function (feId) {
                //if (!window.syncApi)
                //    return;

                $http.get('../api/Quickbooks/' + feId + '/GetQBApp/').then(function (response) {
                    $scope.QBApp = {};

                    $scope.QBApp.FranchiseId = feId;
                    $scope.QBApp.UserName = '';
                    $scope.QBApp.Password = '';
                    $scope.QBApp.SynchInvoices = true;
                    $scope.QBApp.SynchCost = true;
                    $scope.QBApp.SynchPayments = true;
                    $scope.QBApp.IsLastNameFirst = false;
                    $scope.QBApp.SuppressSalesTax = true;

                    if (response != "null") {
                        $scope.QBApp = angular.copy(response.data);
                    }

                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                });
            };

            // Disconnect from QB online
            $scope.Disconect = function () {
                if (!window.syncApi)
                    return;

                $http.post(syncApi + '/api/quickbooks/' + CurrentFranchiseId + '/disconect/').then(function (response) {
                    var old = window.location.href;
                    var el = document.createElement("iframe");
                    document.body.appendChild(el);
                    el.id = 'iframe';
                    el.style.display = "none";
                    el.src = logOffUrl;
                    setCookie("QBToken", "");
                    HFC.DisplaySuccess("Disconnected Successfully");
                    setTimeout($scope.UpdateState, 500);
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                });
            }

            $scope.UpdateStateInProgress = false;

            // QBO Pre sync check
            $scope.PreSyncCheck = function (sucessCallback) {
                $("#loaderDiv").show();
                $http.post(syncApi + '/api/quickbooks/' + CurrentFranchiseId + '/PreSyncCheck/').then(function (response) {
                    $("#loaderDiv").hide();
                    $scope.preSyncCheck = response.data;

                    if (sucessCallback) {
                        sucessCallback(response.data);
                    }

                }, function (response) {
                    $("#loaderDiv").hide();
                    HFC.DisplayAlert(response.statusText);
                });
            }

            // Update if any info has updated in server end
            $scope.UpdateState = function () {
                if ($scope.UpdateStateInProgress)
                    return;

                $scope.UpdateStateInProgress = true;

                if (!window.syncApi)
                    return;

                $http.post(syncApi + '/api/quickbooks/' + CurrentFranchiseId + '/getState/').then(function (response) {
                    $scope.state = response.data;
                    $scope.UpdateStateInProgress = false;
                    $('#qboState').show();
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                });

            }

            if (window.location.href.toLowerCase().endsWith("/qbo")) {
                $scope.UpdateState();
                setInterval($scope.UpdateState, 2000);
            } else {
                $scope.PreSyncCheck();
            }
        }
        ])

        .factory('sessionInjector', [function () {
            var sessionInjector = {
                request: function (config) {
                    if (window.qbToken) {
                        config.headers['qbToken'] = qbToken;
                    }
                    return config;
                }
            };
            return sessionInjector;
        }]).config(['$httpProvider', function ($httpProvider) {
            $httpProvider.interceptors.push('sessionInjector');
        }]);

})(window, document);


