﻿/**
 * hfc.invoice module  
 * a simple module for handling invoice
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.invoice.detail.controller', ['hfc.note.service', 'hfc.invoice.service', 'hfc.address', 'hfc.person', 'hfc.payment', 'hfc.email.service', 'hfc.print.service'])

    .controller('InvoiceDetailController', ['$scope', '$rootScope', '$http', 'InvoiceService', 'AddressService', 'PaymentService', 'JobNoteService', 'EmailService', 'PrintService',
        function ($scope, $rootScope, $http, InvoiceService, AddressService, PaymentService, JobNoteService, EmailService, PrintService) {
        $scope.Invoice = null;

        $scope.SaleAdjTypes = window.SaleAdjTypeEnum;
        $scope.NoteTypes = window.NoteTypeEnum;
             
        $scope.JobNoteService = JobNoteService;
        $scope.PaymentService = PaymentService;

        $scope.EmailService = EmailService;
        $scope.PrintService = PrintService;

               
        var loc = window.location.pathname.lastIndexOf('/') + 1 == window.location.pathname.length ? window.location.pathname.substr(0, window.location.pathname.length - 1) : window.location.pathname,
                split = loc.split('/'),
                invoiceId = parseInt(split[split.length - 1]);

        $scope.Void = function (invoice) {
            if ($scope.PaymentService.Payments.length > 0)
                alert('You need to delete payments first.'); //HFC.DisplayAlert('Please delete payments first'); 
            else {
                var toDel = confirm('Are you sure?');
                if (toDel == true) {
                    InvoiceService.Void(invoice);
                }
            }

        }

        InvoiceService.Get(invoiceId, function (response) {
            function error(res) {
                HFC.DisplayAlert(res.statusText);
            }

            $scope.balance = response.data.Job.Balance;
            $scope.Job = response.data.Job;

            $rootScope.$on('invoiceUpdated', function (event, data) {
                $http.get('/api/jobs/' + $scope.Job.JobId + '/invoices').then(function (response) {
                    $scope.balance = response.data.Job.Balance;
                }, error);
            });

            $scope.Invoice = response.data;
            $scope.Customers = [$scope.Invoice.Job.Customer]; // thus primary only can be billed
            if ($scope.Invoice.PaidInFullOn)
                $scope.Invoice.PaidInFullOn = new XDate($scope.Invoice.PaidInFullOn, true);
            $scope.Invoice.CreatedOn = new XDate($scope.Invoice.CreatedOn, true);           

            $scope.JobNoteService.Get([$scope.Invoice.Job.JobId]);
            $scope.PaymentService.Get($scope.Invoice.InvoiceId);
            if ($scope.Invoice.Job.BillingAddress)
                AddressService.Addresses.push($scope.Invoice.Job.BillingAddress);
            if ($scope.Invoice.Job.InstallAddress && $scope.Invoice.Job.BillingAddressId != $scope.Invoice.Job.InstallAddressId)
                AddressService.Addresses.push($scope.Invoice.Job.InstallAddress);
            
            //set window title
            $('head title').text("Invoice #" + $scope.Invoice.InvoiceNumber + " - " + $scope.Invoice.Job.Customer.FullName);
        });
    }]);

})(window, document);