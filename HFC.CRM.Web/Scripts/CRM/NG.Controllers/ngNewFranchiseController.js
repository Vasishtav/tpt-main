﻿
//Kendo Grid implementation
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.newfranchise.controller', ['hfc.core.service', "kendo.directives", 'ui.bootstrap', 'ui.mask'])


        .service('AddressService', ['$http', '$sce', '$rootScope', function ($http, $sce, $rootScope) {
            var srv = this;

            srv.Address = null;
            srv.Addresses = [];
            srv.AddressIndex = -1;

            srv.Countries = [];
            srv.States = [];
            srv.IsBusy = false;
            srv.IsNew = false;

            srv.ZipCodeChanged = function () {
                if (srv.Address == null) { return; }
                if (srv.Address.ZipCode) {
                    srv.IsBusy = true;
                    $http.get('/api/lookup/0/ZipCodesforFranchise?q=' + srv.Address.ZipCode + "&country=" + srv.Address.CountryCode2Digits).then(function (response) {
                        srv.IsBusy = false;
                        if (response.data.zipcodes.length > 0) {
                            srv.Address.City = response.data.zipcodes[0].City;
                            srv.Address.State = response.data.zipcodes[0].State;
                        } else {
                            srv.Address.City = null;
                            srv.Address.State = null;
                        }
                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }

            };

            srv.CountryChanged = function () {
                if (srv.Address != null) {
                    srv.Address.City = '';
                    srv.Address.State = '';
                    srv.Address.ZipCode = '';
                    srv.Address.CrossStreet = '';
                }
            }

            srv.GetAddress = function (addrId) {
                var addr = $.grep(srv.Addresses, function (a) {
                    return a.AddressId == addrId;
                });
                if (addr)
                    return addr[0];
                else
                    return null;
            };

            srv.GetLabel = function (addrId, singleline) {
                var addr = srv.GetAddress(addrId)
                if (addr) {
                    var str = (addr.Address1 || "") + " " + (addr.Address2 || "");
                    if (!singleline && (addr.City || addr.State))
                        str += "<br/>";
                    else if (singleline || addr.ZipCode)
                        str += ", ";
                    str += (addr.City || "");
                    if (addr.State)
                        str += ", " + addr.State + " ";
                    else if (addr.City)
                        str += " ";
                    str += (addr.ZipCode || "");

                    if (singleline)
                        return str;
                    else
                        return $sce.trustAsHtml(str);
                }
            };

            srv.GetStates = function (countryISO) {
                return $.grep(srv.States, function (s) {
                    return s.CountryISO2 == countryISO;
                });
            };

            srv.GetZipCodeMask = function (countryISO) {
                var cc = $.grep(srv.Countries, function (c) {
                    return c.ISOCode2Digits == (countryISO || "US");
                });
                if (cc && cc.length)
                    return cc[0].ZipCodeMask;
                else
                    return "";
            };

            srv.GetGoogleHref = function (addrId) {
                var addr = srv.GetAddress(addrId);
                if (addr && addr.Address1) {

                    var srcAddr;
                    if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                        srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                    } else {
                        srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                    }

                    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                        return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(srv.GetLabel(addrId, true));
                    } else {
                        return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(srv.GetLabel(addrId, true));
                    }
                } else
                    return null;
            };

            srv.Get = function (leadIds, includeStates) {

                if (!srv.IsBusy) {
                    srv.IsBusy = true;

                    $http({ method: 'GET', url: '/api/address/', params: { leadIds: leadIds, includeStates: includeStates } })

                        .then(function (response) {
                             
                            srv.States = response.data.States || [];
                            srv.Countries = response.data.Countries || [];
                            srv.Addresses = response.data.Addresses || [];
                            srv.FranchiseAddress = response.data.FranchiseAddress;

                            srv.IsBusy = false;
                        }, function (response) {
                            srv.IsBusy = false;
                        });
                }
            }

            srv.Set = function (states, countries, addresses, franchiseAddress) {
                srv.States = states || [];
                srv.Countries = countries || [];
                srv.Addresses = addresses || [];
                srv.FranchiseAddress = franchiseAddress;

            }


            function remove(item) {
                var index = srv.Addresses.indexOf(item);
                if (index > 0) {
                    srv.Addresses.splice(index, 1);
                } else if (index === 0) {
                    srv.Addresses.shift();
                }
            }
        }
        ])

        .service('HFCService', ['$http', function ($http) {
            var srv = this;

            srv.Users = [];
            srv.DisabledUsers = [];
            srv.AllUsers = [];
            srv.CurrentUser = null;
            srv.CurrentUserPermissions = [];
            srv.FranchiseCountryCode = [];
            srv.FranchiseIsSolaTechEnabled = false;

            srv.getUsers = function (id) {

                var result = srv.Users.slice();

                var found = $.grep(srv.Users, function (emp, i) {
                    return emp.UserId == id || emp.PersonId == id || emp.Email == id;
                });


                if (found && found.length > 0) {
                    return result;
                }

                found = $.grep(srv.DisabledUsers, function (emp, i) {
                    return emp.UserId == id || emp.PersonId == id || emp.Email == id;
                });

                if (found && found.length > 0) {
                    result.push(found[0]);
                }

                return result;
            }

            //for now we will only check user agent, but we can also add a check for the media break points as well
            var browser = {
                isAndroid: function () {
                    return navigator.userAgent.match(/Android/i);
                },
                isBlackBerry: function () {
                    return navigator.userAgent.match(/BlackBerry/i);
                },
                isIOS: function () {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                isOpera: function () {
                    return navigator.userAgent.match(/Opera Mini/i);
                },
                isWindows: function () {
                    return navigator.userAgent.match(/IEMobile/i);
                },
                isMobile: function () {
                    return (browser.isAndroid() || browser.isBlackBerry() || browser.isIOS() || browser.isOpera() || browser.isWindows());
                }
            };
            srv.IsMobile = browser.isMobile;

            srv.GetUser = function (personId) {
                var user = $.grep(srv.Users, function (u) {
                    return u.PersonId == personId;
                });

                if (user)
                    return user[0];
                else
                    return null;
            };

            /*srv.SalesUsers = function () {
                return $.grep(srv.Users, function (u) {
                    return u.InSales;
                });
            };*/

            srv.SalesUsers = function (id) {
                var users = srv.Users.slice();

                var result = $.grep(users, function (u) {
                    return u.InSales;
                });

                var found = $.grep(result, function (emp, i) { // if result contains selected element - nothing to do
                    return (emp.UserId == id || emp.PersonId == id || emp.Email == id);
                });

                if (found && found.length > 0) {
                    return result;
                }

                found = $.grep(srv.DisabledUsers, function (emp, i) { // try to find missed selected element in disabled users
                    return (emp.UserId == id || emp.PersonId == id || emp.Email == id) && emp.InSales;
                });

                if (found && found.length > 0) {
                    result.push(found[0]);
                }

                return result;
            };

            srv.InstallerUsers = function (id) {
                var users = srv.Users.slice();

                var result = $.grep(users, function (u) {
                    return u.InInstall;
                });

                var found = $.grep(result, function (emp, i) { // if result contains selected element - nothing to do
                    return (emp.UserId == id || emp.PersonId == id || emp.Email == id);
                });

                if (found && found.length > 0) {
                    return result;
                }

                found = $.grep(srv.DisabledUsers, function (emp, i) { // try to find missed selected element in disabled users
                    return (emp.UserId == id || emp.PersonId == id || emp.Email == id) && emp.InInstall;
                });

                if (found && found.length > 0) {
                    result.push(found[0]);
                }

                return result;
            };

            srv.GetUserLabel = function (user) {
                return '<span class="color-box color-' + (user.ColorId || "none") + '"></span> ' + (user.FullName || "");
            };

            srv.GetUserAvatar = function (personId) {
                var user = srv.GetUser(personId);
                if (user)
                    return user.AvatarSrc;
                else
                    return "/images/avatar/generic.png";
            }

            srv.GetPermission = function (name, subsection) {
                var permission = $.grep(srv.CurrentUserPermissions, function (p) {
                    if (!subsection) {
                        return p.Name == name;
                    } else {
                        return p.Name == name && p.Subsection == subsection;
                    }
                });

                if (permission) {
                    return permission[0];
                }
            };

            srv.GetQueryString = function (key) {
                var qstring = location.search.replace('?', '').split('&');
                //var QS = {}
                for (var i = 0; i < qstring.length; i++) {
                    var keyval = qstring[i].split('=');
                    if (keyval[0].toLowerCase() == key.toLowerCase())
                        return decodeURIComponent(keyval[1]);
                    //QS[keyval[0]] = decodeURIComponent(keyval[1]);
                }
                return null;
            }

            srv.GetDateRange = function (range) {
                var endDate = new XDate(),
                    startDate = new XDate();
                switch (range.toLowerCase()) {
                    case "this week":
                        startDate.addDays(-startDate.getDay());
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "this month":
                        startDate.setDate(1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "this year":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addYears(1).addDays(-1);
                        break;
                    case "last week":
                        startDate.addDays(-startDate.getDay() - 7);
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "last month":
                        startDate.setDate(1).addMonths(-1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "last year":
                        startDate.setDate(1).setMonth(0).addYears(-1);
                        endDate.setDate(1).setMonth(0).addDays(-1);
                        break;
                    case "last 7 days":
                        startDate.addDays(-7); break;
                    case "last 30 days":
                        startDate.addDays(-30); break;
                    case "1st quarter":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "2nd quarter":
                        startDate.setDate(1).setMonth(3);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "3rd quarter":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "4th quarter":
                        startDate.setDate(1).setMonth(9);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "1st half":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(6).addDays(-1); break;
                    case "2nd half":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(6).addDays(-1); break;
                    default:
                        startDate = null; endDate = null; break;
                }
                return { StartDate: startDate, EndDate: endDate };
            }



            ////turn off async, we want to make sure we have list of users first, as other angular services will reference users list
            //$.ajax({
            //    url: '/api/users/',
            //    async: false,
            //    success: function (data) {
            //        srv.Users = data.Users || [];
            //        srv.DisabledUsers = data.DisabledUsers || [];
            //        srv.AllUsers = srv.Users.concat(srv.DisabledUsers);
            //        //srv.CurrentUser = srv.GetUser(data.CurrentUser.PersonId);
            //        if (srv.CurrentUser == null) {
            //            srv.CurrentUser = data.CurrentUser.user;
            //        }

            //        srv.FranchiseCountryCode = data.FranchiseCountryCode;
            //        srv.CurrentBrand = data.BrandId;
            //        srv.FranchiseIsSolaTechEnabled = data.FranchiseIsSolaTechEnabled;
            //    }
            //});

            //$.ajax({
            //    url: '/api/permissions/',
            //    async: true,
            //    success: function (data) {
            //        srv.CurrentUserPermissions = data;
            //    }
            //});

        }])

    .service('NewFranchiseService', ['$http', '$location', 'HFCService', function ($http, $location, HFCService) {
        var srv = this;
        srv.FranchiseID = 0;
        srv.ZipCodeModel = {
            Id: 0,
            TerritoryId: 0,
            ZipCode: '',
            City: '',
            State: '',
            ContractStatus: '',
            CreatedOnUtc: new Date(),
            AcquiredOnUtc: null,
            EffectiveStartUtc: null,
            EffectiveEndUtc: null

        };
        srv.Countries = [];
        srv.FranchiseRoyaltiesModel = {
            RoyaltyId: 0,
            Name: '',
            EndDate: '',
            Amount: null,
            Percent: null,
            TerritoryId: 0,
            StartOnUtc: null,
            EndOnUtc: null
        };

        srv.RoyaltyLevelModel = {
            Amount: 0,
            Month: 0,
            Royalty: '',
            TerritoryId: 0
        };

        srv.RoyaltyTemplateModel = {
            RoyaltyProfileId: '1',
            TerritoryId: 0,
            StartDate: new Date(),
        };

        srv.AddressModel = {
            AddressId: 0,
            Address1: '',
            Address2: '',
            City: '',
            State: '',
            ZipCode: '',
            CreatedOnUtc: new Date(),
            IsDeleted: false,
            IsResidential: false,
            CrossStreet: '',
            CountryCode2Digits: 'US',
            AttentionText: ''
        };
        srv.MailingAddressModel = {
            AddressId: 0,
            Address1: '',
            Address2: '',
            City: '',
            State: '',
            ZipCode: '',
            CreatedOnUtc: new Date(),
            IsDeleted: false,
            IsResidential: false,
            CrossStreet: '',
            CountryCode2Digits: 'US',
            AttentionText: ''
        };

        srv.PersonModel = {
            PersonId: 0,
            FirstName: '',
            LastName: '',
            MI: '',
            HomePhone: '',
            CellPhone: '',
            CompanyName: '',
            WorkTitle: '',
            WorkPhone: '',
            WorkPhoneExt: '',
            PrimaryEmail: '',
            SecondaryEmail: '',
            Notes: '',
            IsDeleted: false,
            CreatedOnUtc: new Date(),
            FaxPhone: '',

            UnsubscribedOnUtc: '',
            PreferredTFN: '',
            PersonGuid: '',
            FranchiseID: '',

            OrderItems: '',
            Orders: '',
            Orders1: '',
            Calendars2: '',
            Tasks2: '',
            EventToPeople_History: '',
            SentEmails: '',
            PhysicalFiles: ''
        }
        srv.UserModelBasic = {
            UserId: '',
            UserName: '',
            PersonId: 0,
            CreationDateUtc: new Date(),
            Password: '',
            Domain: '',
            DomainPath: '',
            LastActivityDateUtc: null,
            IsDeleted: false,
            IsApproved: false,
            IsLockedOut: false,
            LastLoginDateUtc: null,
            LastPasswordChangedDateUtc: null,
            LastLockoutDateUtc: null,
            FailedPasswordAttemptCount: 0,
            FailedPasswordAttemptStartDateUtc: null,
            Comment: '',
            LastKnownIPAddress: '',
            FranchiseId: null,
            Email: '',
            ColorId: '',
            SyncStartsOnUtc: null,
            CalendarFirstHour: 0,
            AddressId: null,
            Person: srv.PersonModel
        };


        srv.TimeZoneCode = [
                { id: 1, name: "UTC" },
                { id: 2, name: "PST" },
                { id: 3, name: "EST" },
                { id: 4, name: "CST" },
                { id: 5, name: "MST" },
                { id: 6, name: "HAST" },
                { id: 7, name: "ATZ" }
        ];

        srv.IsSuspendedModel = [
                { id: true, name: "Yes" },
                { id: false, name: "No" }
        ];

        srv.Territories = {
            TerritoryId: 0,
            Minion: '',
            BrandId: '',
            FranchiseId: '',
            Name: '',
            Code: '',
            CreatedOnUtc: new Date(),
            isArrears: false,
            IsInUsed: false,
            ZipCodes: [srv.ZipCodeModel],
            FranchiseRoyalties: [srv.FranchiseRoyaltiesModel]
        };

        srv.FranchiseModel = {
            FranchiseId: 0,
            FranchiseGuid: '',
            Name: '',
            Code: null,
            BrandId: null,
            Website: '',
            OwnerEmail: '',
            CreatedOnUtc: new Date(),
            IsDeleted: false,
            TimezoneCode: '',
            LogoImgUrlRelativePath: '',
            AddressId: null,
            MailingAddressId: null,
            TollFreeNumber: '',
            LocalPhoneNumber: '',
            FaxNumber: '',
            SupportEmail: '',
            AdminEmail: '',
            OwnerEmail: '',
            LastUpdatedUtc: null,
            LastUpdatedByPersonId: null,
            IsSuspended: false,
            IsSolaTechEnabled: false,
            IsQBEnabled: false,
            Address: srv.AddressModel,
            MailingAddress: srv.MailingAddressModel,
            Territories: srv.Territories,

        };

        srv.GetFranchiseDetails = function (franchiseId, success) {
            srv.FranchiseID = franchiseId;
            $http.get('/api/franchise/' + franchiseId).then(function (response) {

                var FranchiseServiceData = response.data;
                srv.FranchiseModel = FranchiseServiceData;
                if (success)
                    success(FranchiseServiceData);
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });
        };

        //srv.GethasNextLevel = function () {
        //    $http.get('/api/FranchiseRoyalties/' + srv.FranchiseID).then(function (response) {
        //        return response.data.NextLevels;

        //    });
        //};

    }])
.directive("datepicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function () {
                scope.$apply(function () {
                    ngModelCtrl.$modelValue = elem.val();
                });
            };
            if (attrs.value) {
                elem.datepicker('setDate', new Date(attrs.value));
            }
            elem.datepicker({
                //useCurrent: false,
                format: 'mm/dd/yyyy',
            });
            elem.on("change", function (e) {
                updateModel();
            });
        }
    }
})
.controller('NewFranchiseController',
    ['$scope', 'HFCService', 'NewFranchiseService', '$filter', '$window', '$location', '$http', 'AddressService',
    function ($scope, HFCService, NewFranchiseService, $filter, $window, $location, $http, AddressService) {
        var ctrl = this;
        

        $scope.NewFranchiseService = NewFranchiseService;
        $scope.HFCService = HFCService;
        $scope.AddressService = AddressService;
        var franchiseId = $location.absUrl().split('/').pop();
        NewFranchiseService.FranchiseID = franchiseId;

        $scope.TerritoryErr = '';
        $scope.addterritory = false;
        $scope.FranchiseSubmitted = false;
        $scope.RoyaltyLevelSubmitted = false;
        $scope.RoyaltyTemplateSubmitted = false;
        $scope.addterritoryCount = [];
        $scope.editRoyalty = 0;
        $scope.hasNextLevel = false;
        $scope.dtWithout = new Date('2027-11-04');
        $scope.setAction = function () {
            $scope.FranchiseSubmitted = true;
            //var dto = $scope.Franchise;
            $scope.SaveFranchise();
        }

        $scope.GethasNextLevel = function () {
            $http.get('/api/FranchiseRoyalties/' + NewFranchiseService.FranchiseID).then(function (response) {
                $scope.hasNextLevel = response.data.NextLevels;

            });
        };

        $scope.GethasNextLevel();
        if (parseInt(franchiseId) > 0) {
            NewFranchiseService.GetFranchiseDetails(franchiseId, function (FranchiseServiceData) {
                var Franchise = FranchiseServiceData;
                NewFranchiseService.FranchiseID = Franchise.Franchise.FranchiseId;
                $scope.Franchise = Franchise.Franchise;
                $scope.QBText();
            });
        }
        else {

            $scope.Franchise = NewFranchiseService.FranchiseModel;
        }
        $scope.qbbtntext = 'Enable QB Sync';
        $scope.QBText = function () {
            if ($scope.Franchise.IsQBEnabled == null || $scope.Franchise.IsQBEnabled == false) {
                $scope.qbbtntext = 'Enable QB Sync';
            }
            if ($scope.Franchise.IsQBEnabled == true) {
                $scope.qbbtntext = 'QB added';
            }
        };

        $scope.addToQB = function () {
            $http.post('/api/franchise/' + franchiseId + '/AddToQB').then(function (response) {
                $scope.Franchise.IsQBEnabled = true;
                $scope.qbbtntext = 'QB added';
                HFC.DisplaySuccess("QB Added Successfully");
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });
        }

        $scope.SaveFranchise = function () {

            var dto = $scope.Franchise;

            if (dto.Name == '' || dto.Name == null) {
                //$("#errorModal").modal("show");
                return;
            }

            if (dto.Code == '' || dto.Code == null) {
                //$("#errorModal").modal("show");
                return;
            }

            if (dto.TimezoneCode == '' || dto.TimezoneCode < 0) {
                //$("#errorModal").modal("show");
                return;
            }

            if (dto.Address.ZipCode == '' || dto.Address.ZipCode == null) {
                //$("#errorModal").modal("show");
                return;
            }

            //API call to save or update the lead
            $http.post('/controlpanel/franchises/saveFranchise', dto).then(function (response) {
                
                var saveStatus = response.data.Result;
                if (saveStatus != "Success") {
                    HFC.DisplayAlert(saveStatus);
                }
                else {
                    HFC.DisplaySuccess("Franchise Saved Successfully!");
                    var url = "http://" + $window.location.host + "/controlpanel/franchises";
                    $window.location.href = url;
                }
            });
        }

        $scope.addNewterritory = function () {
            $scope.addterritory = true;
            var newItemNo = $scope.addterritoryCount.length + 1;
            $scope.addterritoryCount.push(newItemNo);
        };

        $scope.removeNewterritory = function () {
            var newItemNo = $scope.addterritoryCount.length - 1;

            $scope.addterritoryCount.splice(newItemNo);
        };

        //$scope.checkTerritory = function (territory) {
        //    $http.post('/controlpanel/franchises/checkTerritory', territory).then(function (response) {
        //        
        //        var saveStatus = response.data.confirmStr;
        //        if (saveStatus != "Success") {
        //            HFC.DisplayAlert(saveStatus);
        //            return false;
        //        }
        //        else {
        //            return true;
        //        }
        //    });
        //};

        $scope.checkTerritory = function (territory, success) {
            $http.post('/controlpanel/franchises/checkTerritory', territory).then(function (response) {
                
                var saveStatus = response.data.confirmStr;

                if (success)
                    success(saveStatus);

            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });
        };

        $scope.saveNewTerritory = function (territory) {
             

            territory.FranchiseId = NewFranchiseService.FranchiseID;

            $scope.checkTerritory(territory, function (Status) {
                if (Status == "Success") {
                    $http.post('/controlpanel/franchises/saveTerritory', territory).then(function (response) {
                        
                        var saveStatus = response.data.Result;
                        if (saveStatus != "Success") {
                            HFC.DisplayAlert(saveStatus);
                            return false;
                        }
                        else {
                            HFC.DisplaySuccess("Territory Saved Successfully");
                            $scope.addterritory = false;
                            $scope.addterritoryCount = [];
                            $http.get('/controlpanel/franchises/getTerritory/' + NewFranchiseService.FranchiseID).then(function (response) {

                                
                                var territories = response.data;
                                // $scope.Franchise.Territories = territories;
                                $scope.GetSavedRoyalties();
                                $scope.ResetNewTerritoryModel();
                            });
                        }

                    });
                }
                else {
                    $scope.TerritoryErr = Status;
                    $("#terrErrorModal").modal("show");
                    return;
                }
            });

        }

        $scope.AssignTerritoryToCurrentFranchise = function () {
            var territory = NewFranchiseService.Territories;
            territory.FranchiseId = NewFranchiseService.FranchiseID;
            $http.post('/controlpanel/franchises/saveTerritory', territory).then(function (response) {
                
                var saveStatus = response.data.Result;
                if (saveStatus != "Success") {
                    HFC.DisplayAlert(saveStatus);
                    return false;
                }
                else {
                    $scope.addterritory = false;
                    $scope.addterritoryCount = [];
                    $http.get('/controlpanel/franchises/getTerritory/' + NewFranchiseService.FranchiseID).then(function (response) {

                        
                        var territories = response.data;
                        $scope.Franchise.Territories = territories;
                        $scope.GetSavedRoyalties();
                    });
                }

            });
        }

        $scope.saveTerritory = function (territory) {
            

            $scope.checkTerritory(territory, function (Status) {
                if (Status == "Success") {
                    $http.post('/controlpanel/franchises/saveTerritory', territory).then(function (response) {
                        
                        var saveStatus = response.data.Result;
                        if (saveStatus != "Success") {
                            HFC.DisplayAlert(saveStatus);
                            return false;
                        }
                        else {
                            HFC.DisplaySuccess("Territory Saved Successfully");
                            $scope.addterritory = false;
                            $scope.addterritoryCount = [];
                            $http.get('/controlpanel/franchises/getTerritory/' + NewFranchiseService.FranchiseID).then(function (response) {

                                
                                var territories = response.data;
                                //$scope.Franchise.Territories = territories;
                                $scope.GetSavedRoyalties();
                            });
                        }

                    });
                }
                else {
                    $scope.TerritoryErr = Status;
                    $("#terrErrorModal").modal("show");
                    return;
                }
            });
        }

        $scope.deleteTerritory = function (territory) {
            
            $http.post('/controlpanel/franchises/deleteTerritory', territory).then(function (response) {
                
                var saveStatus = response.data.Result;
                if (saveStatus != "Success") {
                    HFC.DisplayAlert(saveStatus);
                    return false;
                }
                else {
                    HFC.DisplaySuccess("Territory Deleted Successfully");
                    $scope.addterritory = false;
                    $scope.addterritoryCount = [];
                    $http.get('/controlpanel/franchises/getTerritory/' + NewFranchiseService.FranchiseID).then(function (response) {

                        
                        var territories = response.data;
                        $scope.Franchise.Territories = territories;
                        $scope.GetSavedRoyalties();
                    });
                }

            });
        }



        // gets the royalty template to ng-include for a table row / item
        $scope.getRoyaltyTemplate = function (royalty) {
            if ($scope.editRoyalty > 0) {
                if ($scope.editRoyalty == royalty.RoyaltyId) {
                    return 'editRoyalty';
                }
                else {
                    return 'displayRoyalty';
                }
            }
            else {
                return 'displayRoyalty';
            }
        };

        $scope.beginEditRoyalty = function (royalty) {
            $scope.editRoyalty = royalty.RoyaltyId;

        };

        $scope.cancelEditRoyalty = function () {
            $scope.editRoyalty = 0;
        };

        $scope.AddRoyaltyLevel = function () {
            $http.get('/api/FranchiseRoyalties/' + NewFranchiseService.FranchiseID).then(function (response) {

                
                var territories = response.data;
                $scope.Franchise.Territories = territories;
            });
        };

        $scope.GetNextRoyaltyLevel = function (territoryId) {
            NewFranchiseService.RoyaltyLevelModel.TerritoryId = territoryId;
            $http.get('/api/FranchiseRoyalties/' + territoryId + '/GetNextRoyaltyLevel').then(function (response) {

                var Royalty = response.data;
                NewFranchiseService.RoyaltyLevelModel.Royalty = Royalty.nextRoyaltyName;
            });
        };


        $scope.SetTerritoryDropdownValue = function (territoryId, CountryCode) {
            
            NewFranchiseService.RoyaltyTemplateModel.TerritoryId = territoryId;
            if (CountryCode == 'US') {
                NewFranchiseService.RoyaltyTemplateModel.RoyaltyProfileId = "1";
            }
            else {
                NewFranchiseService.RoyaltyTemplateModel.RoyaltyProfileId = "2";
            }
        };

        $scope.GetSavedRoyalties = function () {
            $http.get('/api/FranchiseRoyalties/' + NewFranchiseService.FranchiseID + '/GetRoyalties').then(function (response) {

                
                var territories = response.data;
                $scope.Franchise.Territories = territories.Royalties;

            });
            $scope.GethasNextLevel();
        }

        $scope.SaveRoyaltyTemplete = function (data, model) {

            $scope.RoyaltyTemplateSubmitted = true;

            if (model.form_RoyaltyTemplate.$invalid) {
                return;
            }
            else {

                HFC.AjaxBasic('POST', '/api/FranchiseRoyalties/' + data.TerritoryId + '/ApplyTemplate', JSON.stringify({ TerritoryId: data.TerritoryId, RoyaltyProfileId: data.RoyaltyProfileId, StartDate: new Date(data.StartDate) }), function (response) {
                    
                    NewFranchiseService.RoyaltyTemplateModel.RoyaltyProfileId = '1';
                    NewFranchiseService.RoyaltyTemplateModel.TerritoryId = 0;
                    NewFranchiseService.RoyaltyTemplateModel.StartDate = new Date();
                    $("#RoyaltyTemplateModal").modal("hide");
                    $scope.GetSavedRoyalties();
                    HFC.DisplaySuccess("Royalty Added Successfully");
                });
            }
        }

        $scope.CancelRoyaltyTemplete = function () {
            NewFranchiseService.RoyaltyTemplateModel.RoyaltyProfileId = '1';
            NewFranchiseService.RoyaltyTemplateModel.TerritoryId = 0;
            NewFranchiseService.RoyaltyTemplateModel.StartDate = $filter('date')(new Date(), 'MM/dd/yyyy');

            $scope.RoyaltyTemplateSubmitted = false;
        }


        $scope.SaveRoyaltyLevel = function (data, model) {

            $scope.RoyaltyLevelSubmitted = true;

            if (model.form_RoyaltyLevel.$invalid) {
                return;
            }
            else {

                if (data.Month <= 0 || data.Amount <= 0) {
                    HFC.DisplayAlert("Amount and Month values should be greater than zero!");
                    return
                }



                HFC.AjaxBasic('POST', '/api/FranchiseRoyalties/' + data.TerritoryId + '/AddNextRoyaltyLevel', JSON.stringify({
                    monthes: data.Month,
                    amount: data.Amount
                }), function (response) {

                    $("#addRoyaltyLevelModal").modal("hide");
                    NewFranchiseService.RoyaltyLevelModel.Amount = 0;
                    NewFranchiseService.RoyaltyLevelModel.Month = 0;
                    NewFranchiseService.RoyaltyLevelModel.Royalty = '';
                    NewFranchiseService.RoyaltyLevelModel.TerritoryId = 0;

                    $scope.GetSavedRoyalties();
                    HFC.DisplaySuccess("Royalty Added Successfully");
                });
            }
        }

        $scope.CancelRoyaltyLevel = function () {
            NewFranchiseService.RoyaltyLevelModel.Amount = 0;
            NewFranchiseService.RoyaltyLevelModel.Month = 0;
            NewFranchiseService.RoyaltyLevelModel.Royalty = '';
            NewFranchiseService.RoyaltyLevelModel.TerritoryId = 0;

            $scope.RoyaltyLevelSubmitted = false;
        }

        $scope.UpdateRoyalty = function (model) {

            var startdate = new Date(model.StartOnUtc);
            var enddate = new Date(model.EndOnUtc);

            if (startdate > enddate) {
                HFC.DisplayAlert("Action cancelled. End date greater start date");
                return;
            }

            if (model.StartOnUtc) {
                model.StartOnUtc = moment(model.StartOnUtc).format('YYYY-MM-DD');
            }
            if (model.EndOnUtc) {
                model.EndOnUtc = moment(model.EndOnUtc).format('YYYY-MM-DD');
            }

            HFC.AjaxBasic('PUT', '/api/FranchiseRoyalties', JSON.stringify(model), function (response) {
                $scope.GetSavedRoyalties();
                $scope.cancelEditRoyalty();
                HFC.DisplaySuccess("Royalty Saved Successfully");
            });
        }

        $scope.deleteRoyalty = function (Id) {
            if (confirm("Are You sure?")) {
                HFC.AjaxBasic('DELETE', '/api/FranchiseRoyalties/' + Id, null, function (response) {
                    $scope.GetSavedRoyalties();
                    HFC.DisplaySuccess("Royalty deleted");
                });
            }
        }

        $scope.ZipCodeChanged = function () {
            $scope.Franchise.Address.City = '';
            $scope.Franchise.Address.State = '';
            if ($scope.Franchise.Address.ZipCode) {
                $scope.IsBusy = true;
                $http.get('/api/lookup/0/ZipCodesforFranchise?q=' + $scope.Franchise.Address.ZipCode + "&country=" + $scope.Franchise.Address.CountryCode2Digits).then(function (response) {
                    $scope.IsBusy = false;
                    if (response.data.zipcodes.length > 0) {
                        $scope.Franchise.Address.City = response.data.zipcodes[0].City;
                        $scope.Franchise.Address.State = response.data.zipcodes[0].State;
                    } else {
                        $scope.Franchise.Address.City = '';
                        $scope.Franchise.Address.State = '';
                    }
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
            }

        };

        $scope.GetZipCodeMask = function (countryISO) {
            var cc = $.grep($scope.AddressService.Countries, function (c) {
                return c.ISOCode2Digits == (countryISO || "US");
            });
            if (cc && cc.length)
                return cc[0].ZipCodeMask;
            else
                return "";
        };

        $scope.CountryChanged = function () {
            if ($scope.Franchise.Address != null) {
                $scope.Franchise.Address.City = '';
                $scope.Franchise.Address.State = '';
                $scope.Franchise.Address.ZipCode = '';
                $scope.Franchise.Address.CrossStreet = '';
            }
        }


        $scope.MailingZipCodeChanged = function () {
            $scope.Franchise.MailingAddress.City = '';
            $scope.Franchise.MailingAddress.State = '';
            if ($scope.Franchise.MailingAddress.ZipCode) {
                $scope.IsBusy = true;
                $http.get('/api/lookup/0/ZipCodesforFranchise?q=' + $scope.Franchise.MailingAddress.ZipCode + "&country=" + $scope.Franchise.MailingAddress.CountryCode2Digits).then(function (response) {
                    $scope.IsBusy = false;
                    if (response.data.zipcodes.length > 0) {
                        $scope.Franchise.MailingAddress.City = response.data.zipcodes[0].City;
                        $scope.Franchise.MailingAddress.State = response.data.zipcodes[0].State;
                    } else {
                        $scope.Franchise.MailingAddress.City = '';
                        $scope.Franchise.MailingAddress.State = '';
                    }
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
            }

        };

        $scope.MailingGetZipCodeMask = function (countryISO) {
            var cc = $.grep($scope.AddressService.Countries, function (c) {
                return c.ISOCode2Digits == (countryISO || "US");
            });
            if (cc && cc.length)
                return cc[0].ZipCodeMask;
            else
                return "";
        };

        $scope.MailingCountryChanged = function () {
            if ($scope.Franchise.MailingAddress != null) {
                $scope.Franchise.MailingAddress.City = '';
                $scope.Franchise.MailingAddress.State = '';
                $scope.Franchise.MailingAddress.ZipCode = '';
                $scope.Franchise.MailingAddress.CrossStreet = '';
            }
        }

        $scope.ResetNewTerritoryModel = function () {
            NewFranchiseService.Territories = {
                TerritoryId: 0,
                Minion: '',
                BrandId: '',
                FranchiseId: '',
                Name: '',
                Code: '',
                CreatedOnUtc: new Date(),
                isArrears: false,
                IsInUsed: false,
                ZipCodes: [NewFranchiseService.ZipCodeModel],
                FranchiseRoyalties: [NewFranchiseService.FranchiseRoyaltiesModel]
            };
        }



    }
    ]);

})(window, document);