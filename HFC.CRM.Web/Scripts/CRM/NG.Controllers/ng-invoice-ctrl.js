﻿/**
 * hfc.invoice.list module  
 * a simple module for handling invoice list
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.invoice.list', ['hfc.invoice.service'])
        .directive('datepicker', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {
                    
                    $(function () {
                        element.datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            dateFormat: 'dd/mm/yy',
                            onSelect: function (date) {
                                scope.$apply(function () {
                                    ngModelCtrl.$setViewValue(date);
                                });
                            }
                        });
                    });
                }
            }
        })
        .directive('selectpicker', ['$parse', function($parse) {
            return {
                restrict: 'A',
                require: '?ngModel',
                priority: 10,
                compile: function(tElement, tAttrs, transclude) {
                    tElement.selectpicker($parse(tAttrs.selectpicker)());
                    tElement.selectpicker('refresh');
                    return function(scope, element, attrs, ngModel) {
                        if (!ngModel) return;

                        scope.$watch(attrs.ngModel, function(newVal, oldVal) {
                            scope.$evalAsync(function() {
                                if (!attrs.ngOptions || /track by/.test(attrs.ngOptions)) element.val(newVal);
                                element.selectpicker('refresh');
                            });
                        });

                        ngModel.$render = function() {
                            scope.$evalAsync(function() {
                                element.selectpicker('refresh');
                            });
                        }
                    };
                }

            };
        }]).filter('unsafe', function ($sce) { return $sce.trustAsHtml; })
        .service('InvoiceFilter', ['$http', function ($http) {
        
        this.page_info = { page: 1, size: 25 };
        this.total_items = 0;
        
        this.predicate = '-CreatedOn';
        this.startDate = null;
        this.endDate = null;
        this.statusEnum = null; // null = all, 0 = open, 2 = paid, 1 = partial,  ref: HFC.CRM.Data.InvoiceStatusEnum
                
        this.orderBy = function () {
            return this.predicate.substr(1);
        };

        this.orderByDesc = function () {
            return this.predicate.substr(0, 1) == '-';
        };

        this.get = function() {
                var filters = {
                    pageNum: this.page_info.page,
                    pageSize: this.page_info.size,
                    statusEnum: this.statusEnum,
                    orderBy: this.predicate.substr(1),
                    orderDir: this.predicate.substr(0, 1) == '-' ? 1 : 0, //0 | + == ascending, 1 | - == descending
                    startDate: this.startDate != null ? this.startDate : null,
                    endDate: this.endDate != null ? this.endDate : null,
                    searchTerm: this.searchTerm,
                    searchFilter: this.searchFilter,
                    invoiceStatuses: this.invoiceStatuses
                }

            return $http.get('/api/search/0/GetInvoices', { params: filters });
        }

            this.exportExcel = function () {
                
                var filters = {
                    pageNum: this.page_info.page,
                    pageSize: this.page_info.size,
                    statusEnum: this.statusEnum,
                    orderBy: this.predicate.substr(1),
                    orderDir: this.predicate.substr(0, 1) == '-' ? 1 : 0, //0 | + == ascending, 1 | - == descending
                    startDate: this.startDate != null ? this.startDate : null,
                    endDate: this.endDate != null ? this.endDate : null,
                    searchTerm: this.searchTerm,
                    searchFilter: this.searchFilter,
                    invoiceStatuses: this.invoiceStatuses

                }

                window.open('/export/invoices?' + $.param(filters), '_blank');
                
            }

        }])
        
    .controller('InvoiceController', ['$http', '$scope', '$window', 'InvoiceFilter', function ($http, $scope, $window, InvoiceFilter) {
        var ctrl = this;
        ctrl.countLoad = 0;

        ctrl.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
        ctrl.invoices = [];
        ctrl.filter = InvoiceFilter;
        ctrl.void = function (Item) {
            var doDelete = function() {
                var invoice = ctrl.invoices.indexOf(Item);
                $http.delete('/api/invoices/' + Item.InvoiceId).then(
                    function(response) {
                        remove(Item);
                        HFC.DisplaySuccess("Inovice #" + Item.InvoiceId + " deleted");
                    }, function(response) {
                        HFC.DisplayAlert(response.statusText);
                    });
            };
            
            var ask = function (message) {
                bootbox.dialog({
                    message: message,
                    title: "Warning",
                    buttons: {
                        danger: {
                            label: "Yes, delete",
                            className: "btn-danger",
                            callback: function () {
                                doDelete();
                            }
                        },
                        main: {
                            label: "Cancel",
                            className: "btn-default",
                            callback: function () {

                            }
                        }
                    }
                });
            };

            if (Item.IsOrdered) {
                ask(window.localization.POSentDeleteConfirmation);
            } else {
                ask(window.localization.POOrderedDeleteConfirmation);
            }
        };

        function remove(item) {
            var index = ctrl.invoices.indexOf(item);
            if (index > 0) {
                ctrl.invoices.splice(index, 1);
            }
            else if (index === 0) {
                ctrl.invoices.shift();
            }
        };
        ctrl.GetURLParameter = function (sParam) {
            var sPageURL = window.location.search.substring(1);
            var data = $.parseParams(window.location.href);
            if (data) {
                return data[sParam];
            }
        };

        ctrl.selectedDateText = ctrl.GetURLParameter("selectedDateText") ? ctrl.GetURLParameter("selectedDateText") :  "All Date & Time";

        ctrl.setDateRange = function (date) {
            var range = ctrl.getDateRange(date);
            if (range.StartDate) {
                ctrl.startDate = range.StartDate.toString('MM/dd/yyyy');
            } else {
                ctrl.startDate = null;
            }

            if (range.EndDate) {
                ctrl.endDate = range.EndDate.toString('MM/dd/yyyy');
            } else {
                ctrl.endDate = null;
            }
            
            ctrl.selectedDateText = date;
        }

        ctrl.getDateRange = function (range) {
            var endDate = new XDate(),
                startDate = new XDate();
            switch (range.toLowerCase()) {
                case "all date & time":
                    startDate = null;
                    endDate = null;
                    break;
                case "this week":
                    startDate.addDays(-startDate.getDay());
                    endDate = startDate.clone().addDays(6);
                    break;
                case "this month":
                    startDate.setDate(1);
                    endDate = startDate.clone().addMonths(1).addDays(-1);
                    break;
                case "this year":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addYears(1).addDays(-1);
                    break;
                case "last week":
                    startDate.addDays(-startDate.getDay() - 7);
                    endDate = startDate.clone().addDays(6);
                    break;
                case "last month":
                    startDate.setDate(1).addMonths(-1);
                    endDate = startDate.clone().addMonths(1).addDays(-1);
                    break;
                case "last year":
                    startDate.setDate(1).setMonth(0).addYears(-1);
                    endDate.setDate(1).setMonth(0).addDays(-1);
                    break;
                case "last 7 days":
                    startDate.addDays(-7); break;
                case "last 30 days":
                    startDate.addDays(-30); break;
                case "1st quarter":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "2nd quarter":
                    startDate.setDate(1).setMonth(3);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "3rd quarter":
                    startDate.setDate(1).setMonth(6);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "4th quarter":
                    startDate.setDate(1).setMonth(9);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "1st half":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addMonths(6).addDays(-1); break;
                case "2nd half":
                    startDate.setDate(1).setMonth(6);
                    endDate = startDate.clone().addMonths(6).addDays(-1); break;
                default:
                    startDate = null; endDate = null; break;
            }
            
            return { StartDate: startDate, EndDate: endDate };
        }

        //these properties are also here and in InvoiceFilter because we want the changes here to only affect filter when user hits "Filter" button
        //Otherwise, when a user clicks on different pages or sort, it will use the InvoiceFilter's originally stored values to retrieve the correct data for the filter

      

        ctrl.startDate = ctrl.GetURLParameter("startDate");
        ctrl.endDate = ctrl.GetURLParameter("endDate");
        ctrl.statusEnum = ctrl.GetURLParameter("invoiceStatuses");
        ctrl.invoiceStatuses = ctrl.GetURLParameter("invoiceStatuses") ? ctrl.GetURLParameter("invoiceStatuses") : [];
        ctrl.searchTerm = ctrl.GetURLParameter("searchTerm");
        ctrl.searchFilter = ctrl.GetURLParameter("searchFilter");

        ctrl.pageChanged = _get;

        $scope.$watch(function () {
              return ctrl.filter.predicate; }, _get);             
        
        //get will reset page num to 1 for fresh records
        ctrl.applyFilter = function ($event) {

            ctrl.filter.page_info.page = 1;
            ctrl.filter.startDate = ctrl.startDate;
            ctrl.filter.endDate = ctrl.endDate;
            ctrl.filter.statusEnum = ctrl.invoiceStatuses;
            ctrl.filter.searchTerm = ctrl.searchTerm;
            ctrl.filter.searchFilter = ctrl.searchFilter;
            if (ctrl.invoiceStatuses.length != 0 ||
                      ctrl.startDate || ctrl.endDate ||
                      ctrl.searchTerm || ctrl.searchFilter) {
                var myParams = {
                    invoiceStatuses: ctrl.invoiceStatuses, startDate: ctrl.startDate,
                    endDate: ctrl.endDate, searchTerm: ctrl.searchTerm,
                    searchFilter: ctrl.searchFilter, selectedDateText : ctrl.selectedDateText
                };
                var pr = jQuery.param(myParams, false);
                //History.replaceState(myParams, "invoiceSearch", "?" + pr);
            }
            else {
                //History.replaceState(null, "invoiceSearch", "/accounting/invoices");
            }

          
            _get();
        };

        ctrl.getPrefTFN = function (customer) {
            if (customer.PreferredTFN == "C" && customer.CellPhone)
                return HFC.formatPhone(customer.CellPhone);
            else if (customer.PreferredTFN == "W" && customer.WorkPhone)
                return HFC.formatPhone(customer.WorkPhone) + (customer.WorkPhoneExt ? (' x' + customer.WorkPhoneExt) : '');
            else if (customer.PreferredTFN == "F" && customer.FaxPhone)
                return HFC.formatPhone(customer.FaxPhone);
            else if (customer.HomePhone)
                return HFC.formatPhone(customer.HomePhone);
            else
                return "";
        };

        ctrl.getBalance = function (invoice) {
            var paymentSum = 0;
            if (invoice.Payments) {
                angular.forEach(invoice.Payments, function (payment) {
                    paymentSum += parseFloat(payment.Amount);
                });
            }

            return invoice.JobQuote.NetTotal - paymentSum;
        };

        function _get() {
            ctrl.countLoad ++;
            if (ctrl.countLoad == 2 || ctrl.countLoad == 3) {
                return;
            }

            ctrl.filter.get().then(function (response) {
                angular.forEach(response.data.Invoices, function (inv) {
                    inv.CreatedOn = new XDate(inv.CreatedOn);
                    inv.LastUpdated = new XDate(inv.LastUpdated);
                });
                ctrl.invoices = response.data.Invoices;
                ctrl.filter.total_items = response.data.TotalRecords;
            },
            function (response) {
                HFC.DisplayAlert(response.statusText);
            });            
        };
        
        ctrl.exportExcel = function() {
            ctrl.filter.exportExcel();
        }

        //first initial get
        ctrl.filter.statusEnum = ctrl.invoiceStatuses;
        
        ctrl.applyFilter();
    }]);


})(window, document);
