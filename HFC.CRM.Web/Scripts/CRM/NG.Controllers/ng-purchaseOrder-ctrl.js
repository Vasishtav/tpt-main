﻿/**
 * hfc.invoice.list module  
 * a simple module for handling invoice list
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.purchaseOrder.list', ['hfc.core.service'])

    .service('PurchaseOrderFilter', ['$http', 'HFCService', function ($http, HFCService) {
        
        this.page_info = { page: 1, size: 25 };
        this.total_items = 0;
        
        this.predicate = '-CreatedOn';
        this.startDate = null;
        this.endDate = null;
        this.statusEnum = null; // null = all, 0 = open, 2 = paid, 1 = partial,  ref: HFC.CRM.Data.InvoiceStatusEnum


        this.orderBy = function () {
            return this.predicate.substr(1);
        };
        this.orderByDesc = function () {
            return this.predicate.substr(0, 1) == '-';
        };

        this.get = function() {
            var filters = {
                pageNum: this.page_info.page,
                pageSize: this.page_info.size,
                statusEnum: this.statusEnum,
                orderBy: this.predicate.substr(1),
                orderDir: this.predicate.substr(0, 1) == '-' ? 1 : 0, //0 | + == ascending, 1 | - == descending
                startDate: this.startDate != null ? this.startDate : null,
                endDate: this.endDate != null ? this.endDate : null
            }
            return $http.get('/api/purchaseOrders/', { params: filters });
        }

    }])
        
    .controller('PurchaseOrderController', ['$http', '$scope', '$window', 'PurchaseOrderFilter', 'HFCService', function ($http, $scope, $window, PurchaseOrderFilter, HFCService) {
        var ctrl = this;
        ctrl.invoices = [];
        ctrl.filter = PurchaseOrderFilter;
        ctrl.IsBusy = false;
        ctrl.void = function (Item) {

            var doDelete = function() {
                var invoice = ctrl.purchaseOrders.indexOf(Item);

                $http.delete('/api/purchaseOrders/' + Item.OrderId).then(
                    function(response) {
                        remove(Item);
                        HFC.DisplaySuccess("PurchaseOrder #" + Item.OrderNumber + " canceled");
                    }, function(response) {
                        HFC.DisplayAlert(response.statusText);
                    });
            };

            var ask = function (message) {
                bootbox.dialog({
                    message: message,
                    title: "Warning",
                    buttons: {
                        danger: {
                            label: "Yes, delete",
                            className: "btn-danger",
                            callback: function () {
                                doDelete();
                            }
                        },
                        main: {
                            label: "Cancel",
                            className: "btn-default",
                            callback: function () {

                            }
                        }
                    }
                });
            };

            if (Item.IsOrdered) {
                ask(window.localization.POSentDeleteConfirmation);
            } else {
                ask(window.localization.POOrderedDeleteConfirmation);
            }
        };

        ctrl.order = function(Item) {

            if (!ctrl.IsBusy)
            {
                ctrl.IsBusy = true;
                $http.post('/api/purchaseOrders/'+ Item.OrderId + '/ordernow/').then(function (res) {
                    HFC.DisplaySuccess("Order sent to solatech");
                    _get();
                }, function (res) {
                    HFC.DisplayAlert(res.statusText);
                });
                ctrl.IsBusy = false;
            }
        }
        ctrl.updateOrderStatus = function (Id, status) {

            if(!ctrl.IsBusy) {
                ctrl.IsBusy = true;
                $http.put('/api/purchaseOrders/' + Id + '/orderstatus/', { Id : status }).then(function (res) {
                    HFC.DisplaySuccess('Order Status updated for PO # ' +Id);
                }, function (res) {
                    HFC.DisplayAlert(res.statusText);
                });
                ctrl.IsBusy = false;
            }
        }

        function remove(item) {
            var index = ctrl.purchaseOrders.indexOf(item);
            if (index > 0) {
                ctrl.purchaseOrders.splice(index, 1);
            }
            else if (index === 0) {
                ctrl.purchaseOrders.shift();
            }
        };

        //these properties are also here and in InvoiceFilter because we want the changes here to only affect filter when user hits "Filter" button
        //Otherwise, when a user clicks on different pages or sort, it will use the InvoiceFilter's originally stored values to retrieve the correct data for the filter

        
        ctrl.startDate = null;
        ctrl.endDate = null;
        ctrl.statusEnum = null;
        
        ctrl.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
        ctrl.DateRangeText = "All Date & Time";
        ctrl.SelectDateRange = function (dateRange) {
            ctrl.DateRangeText = dateRange;
            var dates = HFCService.GetDateRange(ctrl.DateRangeText);
            ctrl.startDate = dates.StartDate;
            ctrl.endDate = dates.EndDate;
        }

        //if ($.cookie('PurchaseOrderSearchCriteria')) {
            
        //    var data = JSON.parse($.cookie('PurchaseOrderSearchCriteria'));
        //    //ctrl.startDate = data.startDate;
        //    //ctrl.endDate = data.endDate;
        //    ctrl.statusEnum = data.statusEnum;
        //    ctrl.DateRangeText = data.DateRangeText;
        //    ctrl.SelectDateRange(data.DateRangeText);
        //}

        ctrl.CalendarClick = function($event) {
            $event.stopPropagation();
        };

        $scope.clearFilters = function() {
            ctrl.statusEnum = null;
            ctrl.DateRangeText = "All Date & Time";
            ctrl.SelectDateRange(ctrl.DateRangeText);
        }

        ctrl.pageChanged = _get;

        $scope.$watch(function () { return ctrl.filter.predicate; }, _get);             
        
        //get will reset page num to 1 for fresh records
        ctrl.applyFilter = function ($event) {
            ctrl.filter.page_info.page = 1;
            ctrl.filter.startDate = ctrl.startDate;
            ctrl.filter.endDate = ctrl.endDate;
            ctrl.filter.statusEnum = ctrl.statusEnum;
            ctrl.filter.DateRangeText = ctrl.DateRangeText;
           
            //if (enableSaveSearch) {
            //    $.cookie('PurchaseOrderSearchCriteria', JSON.stringify(ctrl.filter), { path: '/' });
            //}

            _get();
        };

        ctrl.getBalance = function (invoice) {
            var paymentSum = 0;
            if (invoice.Payments) {
                angular.forEach(invoice.Payments, function (payment) {
                    paymentSum += parseFloat(payment.Amount);
                });
            }

            return invoice.JobQuote.NetTotal - paymentSum;
        };
        
        function _get() {
            ctrl.filter.get().then(function (response) {
                angular.forEach(response.data.PurchaseOrders, function (inv) {
                    inv.CreatedDate = new XDate(inv.CreatedDate, true);
                });
                
                ctrl.purchaseOrders = response.data.PurchaseOrders;
                ctrl.orderStatuses = response.data.OrderStatus;
                ctrl.filter.total_items = response.data.TotalRecords;
            },
            function (response) {
                HFC.DisplayAlert(response.statusText);
            });            
        };

        //first initial get
        
        _get();
    }]);


})(window, document);
