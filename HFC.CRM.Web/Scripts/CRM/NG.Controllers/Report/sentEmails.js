﻿
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.report.SentEmails', ['kendo.directives']).service('SentEmailsService', ['$http', 'HFCService', function ($http, HFCService) {
        var srv = this;
        srv.Report = 'MonthlyStatistics';
        srv.startDate = null;
        srv.endDate = null;

        srv.ReportData = [];

        srv.MonthlyStatisticsData = [];
        srv.SalesAgentData = [];
        srv.exportToExcelData = [];

        srv.FirstLevel = [];
        srv.SelecondLevel = [];

        srv.Commercial = 0;

        srv.GetReport = function () {

            var startDateReport = srv.startDate, endDateReport = srv.endDate;

            $http.post('/api/charts/0/GetSentEmails', { startDate: startDateReport, endDate: endDateReport }).then(function (response) {
                srv.IsBusy = false;


                srv.ReportData = new kendo.data.DataSource({
                    data: response.data.data,
                    schema: {
                        model: {
                            fields: {
                                Subject: { type: "string" },
                                Body: { type: "string", encoded: false },
                                SentByPerson: { type: "string" },
                                SentAt: { type: "date", format: "{0:MM/dd/yyyy}" },
                                Recipients: { type: "string" },
                                LeadId: { type: "number" },
                                JobId: { type: "number" },
                                LeadFullName: { type: "string" },
                                TemplateName: { type: "string" },
                            }
                        }
                    },
                });


            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        };

        srv.exportToExcel = function () {
            var exportData = srv.exportToExcelData.options.data;
            var totalCells = [];
            var grid = null
            if (srv.Report == 'MarketingPerformance') {

                grid = $("#gridMarketingPerformance1").data("kendoGrid");
                var dataInGrid = $("#gridMarketingPerformance1").data("kendoGrid").dataSource.view();
                exportData = []
                for (var i = 0; i < dataInGrid.length; i++) {
                    for (var s = 0; s < srv.exportToExcelData.options.data.length; s++) {
                        if (dataInGrid[i].Source == srv.exportToExcelData.options.data[s].Source) {
                            exportData.push(srv.exportToExcelData.options.data[s]);
                        }
                    }
                }
                var orderByColumn = 'Sales';
                var orderByDirection = 'desc';
                var srtAttr = $("#gridMarketingPerformance1").data("kendoGrid").dataSource.sort();
                if (srtAttr) {
                    if (srtAttr.length != 0) {
                        orderByDirection = srtAttr[0].dir;
                        orderByColumn = srtAttr[0].field;

                    }
                }
                exportData = _.orderBy(exportData, orderByColumn, orderByDirection);

                var totalsHtml = $($("#gridMarketingPerformance1").data("kendoGrid").footer).find('td');
                for (var i = 0; i < grid.columns.length; i++) {
                    var txt = $(totalsHtml[i + 1]).text().replace(/,/g, "");;

                    if (txt.indexOf('$') != -1) {
                        totalCells.push({ value: +txt.replace('$', ''), format: '$ #,##0.00', color: "#000000", bold: true });
                    }
                    else if (txt.indexOf('%') != -1) {
                        totalCells.push({ value: txt.replace('%', '') / 100, format: '#,##0.00%', color: "#000000", bold: true });
                    }
                    else if (txt.indexOf('.') != -1) {
                        totalCells.push({ value: +txt, format: '#,##0.00', color: "#000000", bold: true });
                    }
                    else {
                        totalCells.push({ value: txt, color: "#000000", bold: true });
                    }
                }
            }

            if (srv.Report == 'MonthlyStatistics') {
                grid = $("#gridMonthlyStatistics").data("kendoGrid");
            }

            if (srv.Report == 'SalesAgent') {
                grid = $("#gridSalesAgent").data("kendoGrid");
            }

            if (srv.Report == 'City') {
                grid = $("#gridCity").data("kendoGrid");
            }

            if (srv.Report == 'Zip') {
                grid = $("#gridZip").data("kendoGrid");
            }

            if (srv.Report == 'Territory') {
                grid = $("#gridTerritory").data("kendoGrid");
            }

            if (srv.Report == 'JobStatus') {
                grid = $("#gridJobStatus").data("kendoGrid");
            }

            if (srv.Report == 'LeadReport') {
                grid = $("#gridLeadReport").data("kendoGrid");
            }

            if (srv.Report == 'Category') {
                grid = $("#gridCategory").data("kendoGrid");
            }

            var cellsTitle = []

            for (var i = 0; i < grid.columns.length; i++) {
                cellsTitle.push({ value: grid.columns[i].title, background: "#428bca", color: "#ffffff" })
            }

            var rows = [{
                cells: cellsTitle
            }];

            for (var i = 0; i < exportData.length; i++) {
                var cellsData = [];
                for (var f = 0; f < grid.columns.length; f++) {
                    if (grid.columns[f].format) {

                        var formatArr = grid.columns[f].format.split(':');
                        var format = formatArr[1].substring(0, formatArr[1].length - 1);
                        if (format.indexOf('c') != -1 || format.indexOf('C') != -1) {
                            cellsData.push({ value: exportData[i][grid.columns[f].field] == 0 ? 0.00 : exportData[i][grid.columns[f].field], format: '$ #,##0.00' });
                        }
                        if (format.indexOf('n') != -1 || format.indexOf('N') != -1) {
                            cellsData.push({ value: exportData[i][grid.columns[f].field] == 0 ? 0.00 : exportData[i][grid.columns[f].field], format: '#,##0.00' });
                        }
                        if (format.indexOf('p') != -1 || format.indexOf('P') != -1) {
                            cellsData.push({ value: exportData[i][grid.columns[f].field] == 0 ? 0.00 : exportData[i][grid.columns[f].field] / 100, format: '#,##0.00%' });
                        }
                    }
                    else {
                        if (grid.columns[f].title.indexOf('Source') != -1) {
                            cellsData.push({ value: exportData[i]['Campaign'] });
                        }
                        else {
                            cellsData.push({ value: exportData[i][grid.columns[f].field] });
                        }
                    }
                }
                rows.push({
                    cells: cellsData
                });

            }

            if (totalCells.length > 0) {
                rows.push({
                    cells: totalCells
                });
            }
            var columns = [];
            for (var i = 0; i < grid.columns.length; i++) {
                columns.push({ autoWidth: true })
            }

            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                  {
                      columns: [
                        // Column settings (width)
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                      ],
                      // Title of the sheet
                      title: srv.Report,
                      // Rows of the sheet
                      rows: rows
                  }
                ]
            });


            //save the file as Excel file with extension xlsx
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: srv.Report });

        }




        function filterableDate() {
            return {
                ui: function (element) {
                    element.kendoDatePicker({
                        format: HFCService.StaticKendoDateFormat(),
                        change: function (e) {
                            var value = this.value();
                            // update the selected date in date filter
                            var divElement = e.sender.dateView.div[0];
                            var divChildren = $(divElement).children()[0];
                            var dateInput = $(divChildren).children()[2];
                            value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                            $(dateInput).text(value);
                            //value is the selected date in the datepicker
                        },
                        open: function (e) {
                            var calendar = this.dateView.calendar;
                            if (!this.element.val()) {
                                //navigate if input is empty
                                calendar.navigate(new Date());
                                var value = new Date();
                                var divElement = e.sender.dateView.div[0];
                                var divChildren = $(divElement).children()[0];
                                var dateInput = $(divChildren).children()[2];
                                value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                                $(dateInput).text(value);
                            }
                        }
                    });
                    //for disabling entering of date
                    $(element).attr("disabled", "disabled");
                }
            }
        }
        

        srv.ReportOptions = {
            columns: [
                { field: "LeadFullName", title: "Lead", width: "200px" /*, template: "<a href='/leads/#=LeadId#'>#=LeadFullName#</a> "*/ },

                { field: "Recipients", title: "Recipients", width: "200px" },
                { field: "Subject", title: "Subject", width: "150px" },
                { field: "Body", title: "Body", encoded: false, width: "150px", type: "string" },
                { field: "TemplateName", title: "Template", width: "150px" },
                { field: "SentByPerson", title: "Sent by Person", width: "150px" },
                {
                    field: "SentAt", title: "Sent At", width: "150px", type: "date",
                    filterable: HFCService.gridfilterableDate("date", "", ""),
                    template: function (dataItem) {
                        if (dataItem.SentAt) {
                            return HFCService.KendoDateFormat(dataItem.SentAt);
                        }
                        else {
                            return "";
                        }
                    },
                   // format: "{0:MM/dd/yyyy}"
                }


            ],

            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true,

        };


    }])
        .controller('SentEmailsController', ['$http', '$scope', 'SentEmailsService', function ($http, $scope, SentEmailsService) {

            $scope.data = null;
            $scope.SentEmailsService = SentEmailsService;
            $scope.startDate = null;
            $scope.endDate = null;


            $scope.DateRangeArraySA = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
            $scope.DateRangeArrayMS = ["All Date & Time", "This Year", "Last Year"];
            $scope.DateRangeArrayMP = ["All Date & Time", "Last 90 days", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year", "Last Year"];

            //self.DateRangeArray = ["All Date & Time", "This Month", "Last Month", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year"];

            $scope.selectedDateText = "All Date & Time";




            $scope.$watch('startDate', function (newVal) {
                if (newVal != null) {
                    SentEmailsService.startDate = newVal;
                    if ($scope.endDate == null) {
                        $scope.selectedDateText = newVal + ' to ';
                    }
                    else {
                        $scope.selectedDateText = newVal + ' to ' + $scope.endDate;
                    }
                }
            });

            $scope.$watch('endDate', function (newVal) {
                if (newVal != null) {
                    SentEmailsService.endDate = newVal;
                    if ($scope.startDate == null) {
                        $scope.selectedDateText = ' to ' + newVal
                    }
                    else {
                        $scope.selectedDateText = $scope.startDate + ' to ' + newVal;
                    }
                }
            });

            $scope.setDateRange = function (date) {
                var range = $scope.getDateRange(date);
                if (range.StartDate) {
                    SentEmailsService.startDate = range.StartDate.toString('MM/dd/yyyy');
                    //$scope.startDate = range.StartDate.toString('MM/dd/yyyy');
                } else {
                    //$scope.startDate = null;
                    SentEmailsService.startDate = null;
                }

                if (range.EndDate) {
                    SentEmailsService.endDate = range.EndDate.toString('MM/dd/yyyy');
                    //$scope.EndDate = range.EndDate.toString('MM/dd/yyyy');
                } else {
                    SentEmailsService.endDate = null;
                    //$scope.startDate = null;
                }

                $scope.selectedDateText = date;
            }

            $scope.getDateRange = function (range) {
                var endDate = new XDate(),
                    startDate = new XDate();
                switch (range.toLowerCase()) {
                    case "last 90 days":
                        startDate.addDays(-90);
                        break;
                    case "all date & time":
                        startDate = null;
                        endDate = null;
                        break;
                    case "this week":
                        startDate.addDays(-startDate.getDay());
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "this month":
                        startDate.setDate(1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "this year":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addYears(1).addDays(-1);
                        break;
                    case "last week":
                        startDate.addDays(-startDate.getDay() - 7);
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "last month":
                        startDate.setDate(1).addMonths(-1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "last year":
                        startDate.setDate(1).setMonth(0).addYears(-1);
                        endDate.setDate(1).setMonth(0).addDays(-1);
                        break;
                    case "last 7 days":
                        startDate.addDays(-7); break;
                    case "last 30 days":
                        startDate.addDays(-30); break;
                    case "1st quarter":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "2nd quarter":
                        startDate.setDate(1).setMonth(3);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "3rd quarter":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "4th quarter":
                        startDate.setDate(1).setMonth(9);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "1st half":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(6).addDays(-1); break;
                    case "2nd half":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(6).addDays(-1); break;
                    default:
                        startDate = null; endDate = null; break;
                }

                return { StartDate: startDate, EndDate: endDate };
            }


            //$scope.SalesBetaService.GetReport();
        }]).directive('datepicker', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {

                    $(function () {
                        element.datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            dateFormat: 'dd/mm/yy',
                            onSelect: function (date) {
                                scope.$apply(function () {
                                    ngModelCtrl.$setViewValue(date);
                                });
                            }
                        });
                    });
                }
            }
        })
        .directive('selectpicker', ['$parse', function ($parse) {
            return {
                restrict: 'A',
                require: '?ngModel',
                priority: 10,
                compile: function (tElement, tAttrs, transclude) {
                    tElement.selectpicker($parse(tAttrs.selectpicker)());
                    tElement.selectpicker('refresh');
                    return function (scope, element, attrs, ngModel) {
                        if (!ngModel) return;

                        scope.$watch(attrs.ngModel, function (newVal, oldVal) {
                            scope.$evalAsync(function () {
                                if (!attrs.ngOptions || /track by/.test(attrs.ngOptions)) element.val(newVal);
                                element.selectpicker('refresh');
                            });
                        });

                        ngModel.$render = function () {
                            scope.$evalAsync(function () {
                                element.selectpicker('refresh');
                            });
                        }
                    };
                }

            };
        }])

})(window, document);