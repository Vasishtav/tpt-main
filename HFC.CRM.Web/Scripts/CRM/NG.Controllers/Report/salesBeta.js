﻿
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.report.SalesBeta', ['kendo.directives']).service('SalesBetaService', ['$http', 'HFCService', function ($http, HFCService) {
        var srv = this;
        srv.Report = 'MonthlyStatistics';
        srv.startDate = null;
        srv.endDate = null;
        srv.MarketingPerformanceData = [];
        srv.CityData = [];
        srv.ZipData = [];
        srv.TerritoryData = [];
        srv.JobStatusData = [];
        srv.LeadReportData = [];
        srv.CategoryData = [];

        srv.MonthlyStatisticsData = [];
        srv.SalesAgentData = [];
        srv.exportToExcelData = [];

        srv.FirstLevel = [];
        srv.SelecondLevel = [];

        srv.Commercial = 0;

        srv.toolTipOptions = {
            filter: "th",
            position: "left",
            showAfter: 1000,
            //position: "bottom",
            content: function (e) {

                //var grid = e.target.closest(".k-grid").getKendoGrid();
                //var dataItem = grid.dataItem(e.target.closest("tr"));
                //var cellIndex = $(e.target).parent().children().index($(e.target));
                //var $th = $($(e.target).closest('[kendo-grid]')).find('th').eq(e.target.index());
                var colName = e.target.find(".k-link").text();
                if (!colName) {
                    return e.target.text().replace('Filter', '');
                }
                else if (colName == '# of Leads' || colName == '# Leads') {
                    return tooltips['numLeads'];
                } else if (colName == 'Valid Leads' || colName == '# Valid Leads') {
                    return tooltips['numValidLeads'];
                }
                else if (colName == 'Lost Sales') {
                    return tooltips['lostSalesCount'];
                }
                else if (colName == '# of Sales' || colName == '# Sales') {
                    return tooltips['salesCount'];
                }
                else if (colName == 'Total Sales') {
                    return tooltips['totalSalesAmount'];
                }
                else if (colName == 'Conversion Rate') {
                    return tooltips['conversionRate'];
                }
                else if (colName == 'Closing Rate') {
                    return tooltips['closingRate'];
                }
                else if (colName == 'Avg Sale' || colName == 'Avg Sales') {
                    return tooltips['avgSale'];
                }
                else if (colName == 'Revenue Per Lead') {
                    return tooltips['revenuePerLead'];
                }
                else if (colName == '# of Quotes') {
                    return tooltips['totalNumOfQuotes'];
                }
                else if (colName == 'Total Quotes') {
                    return tooltips['totalQuotes'];
                }
                else if (colName == 'Avg Quote') {
                    return tooltips['avgQuote'];
                }
                else if (colName == 'Quote to Close %') {
                    return tooltips['quoteToClosePer'];
                }
                else if (colName == '# of Jobs') {
                    return tooltips['jobsCount'];
                }
                else if (colName == '# Lost jobs') {
                    return tooltips['lostSalesCount'];
                }
                else if (colName == 'Line Item Totals') {
                    return tooltips['lineItemsTotal'];
                }
                else if (colName == 'Line Items Sold') {
                    return tooltips['lineItemsSold'];
                }
                else if (colName == '% Sales per Category') {
                    return tooltips['perSalesPerCategory'];
                }
                else if (colName == 'Avg Sales Per Line Item') {
                    return tooltips['avgSalesPerLineItem'];
                }
                else if (colName == '# Open Jobs') {
                    return tooltips['openJobsCount'];
                }
                else if (colName == 'Campaign Cost') {
                    return tooltips['campaignCost'];
                }
                else if (colName == 'Cost Per Lead') {
                    return tooltips['costPerLead'];
                }
                else if (colName == 'Cost Per Sale') {
                    return tooltips['costPerSale'];
                }
                else if (colName == 'ROI') {
                    return tooltips['rOI'];
                }
                else if (colName == 'Sales per Category') {
                    return tooltips['salesPerCategory'];
                }
                else if (colName == 'Line Itmes Quoted') {
                    return tooltips['lineItmesQuoted'];
                }
                else if (colName == 'Closing Rate per Line Item') {
                    return tooltips['closingRatePerLineItem'];
                }
               return colName;

            },
            show: function (e) {
                this.popup.element[0].style.width = "300px";
            }
        }

        srv.GetReport = function () {

            var startDateReport = srv.startDate, endDateReport = srv.endDate;

            $http.post('/api/charts/0/GetSalesBeta', { ReportName: srv.Report, startDate: startDateReport, endDate: endDateReport, Commercial: srv.Commercial }).then(function (response) {
                srv.IsBusy = false;
                switch (srv.Report) {
                    case "MonthlyStatistics":
                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        Month: { type: "string" },
                                        NumberOfLeads: { type: "number" },
                                        ValidLeads: { type: "number" },
                                        LostSale: { type: "number" },
                                        NumberOfSales: { type: "number" },
                                        TotalSales: { type: "number" },
                                        ConversionRate: { type: "number" },
                                        ClosingRate: { type: "number" },
                                        AvgSale: { type: "number" },
                                        RevenuePerLead: { type: "number" },
                                        TotalNumQuotes: { type: "number" },
                                        TotalQuotes: { type: "number" },
                                        AvgQuote: { type: "number" },
                                        QuoteCloseRate: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.MonthlyStatisticsData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        Month: { type: "string" },
                                        NumberOfLeads: { type: "number" },
                                        ValidLeads: { type: "number" },
                                        LostSale: { type: "number" },
                                        NumberOfSales: { type: "number" },
                                        TotalSales: { type: "number" },
                                        ConversionRate: { type: "number" },
                                        ClosingRate: { type: "number" },
                                        AvgSale: { type: "number" },
                                        RevenuePerLead: { type: "number" },
                                        TotalNumQuotes: { type: "number" },
                                        TotalQuotes: { type: "number" },
                                        AvgQuote: { type: "number" },
                                        QuoteCloseRate: { type: "number" }
                                    }
                                }
                            },
                            aggregate: [
                                    { field: "NumberOfLeads", aggregate: "sum" },
                                    { field: "ValidLeads", aggregate: "sum" },
                                    { field: "LostSale", aggregate: "sum" },
                                    { field: "NumberOfSales", aggregate: "sum" },
                                    { field: "TotalSales", aggregate: "sum" },
                                    { field: "TotalNumQuotes", aggregate: "sum" },
                                    { field: "TotalQuotes", aggregate: "sum" }
                            ]
                        });

                        //srv.LoadMonthlyStatisticsReport(response.data.data);
                        break;
                    case "MarketingPerformance":
                        srv.FirstLevel = _.chain(response.data.data)
    .groupBy("Source")
    .map(function (value, key) {
        return [key, _.reduce(value, function (result, currentObject) {
            return {
                Source: currentObject.Source,
                TotalLeads: result.TotalLeads + currentObject.TotalLeads,
                TotalValidLeads: result.TotalValidLeads + currentObject.TotalValidLeads,
                TotalValidLeadsNoFactor: result.TotalValidLeadsNoFactor + currentObject.TotalValidLeadsNoFactor,                
                NumberOfSales: result.NumberOfSales + currentObject.NumberOfSales,
                Sales: result.Sales + currentObject.Sales,
                TotalNumQuotes: result.TotalNumQuotes + currentObject.TotalNumQuotes,
                TotalQuotes: result.TotalQuotes + currentObject.TotalQuotes,
                CampaignCost: result.CampaignCost + currentObject.CampaignCost,
                //ClosingRate:  currentObject.ClosingRate,
                //ConversionRate: (currentObject.TotalLeads == 0 ? 0 :  currentObject.TotalValidLeads / currentObject.TotalLeads),
                ClosingRate: (currentObject.TotalValidLeadsNoFactor == 0 ? 0 : currentObject.NumberOfSales / currentObject.TotalValidLeadsNoFactor),
                //AveSale: (currentObject.NumberOfSales == 0 ? 0 : currentObject.Sales / currentObject.NumberOfSales) ,
                //RevenuePerLead: (currentObject.TotalValidLeads == 0 ? 0 : currentObject.Sales / currentObject.TotalValidLeads) ,
                AvgQuote: (currentObject.TotalNumQuotes == 0 ? 0 : currentObject.TotalQuotes / currentObject.TotalNumQuotes),
                //QuoteToClose: (currentObject.TotalQuotes == 0 ? 0 :currentObject.Sales / currentObject.TotalQuotes) ,
                //CostPerLead: (currentObject.TotalValidLeads == 0 ? 0 : currentObject.CampaignCost / currentObject.TotalValidLeads) ,
                //CostPerSale: (currentObject.NumberOfSales == 0 ? 0 : currentObject.CampaignCost / currentObject.NumberOfSales),
                //ROI: (currentObject.CampaignCost == 0 ? 0 : currentObject.Sales / currentObject.CampaignCost),
                StartDate: currentObject.StartDate,
                EndDate: currentObject.EndDate,
                SourceId: currentObject.SourceId,
                ParentSourceId: currentObject.ParentSourceId
            }
        }, {
            Source: '',
            TotalLeads: 0,
            TotalValidLeads: 0,
            TotalValidLeadsNoFactor:0,
            NumberOfSales: 0,
            Sales: 0,
            TotalNumQuotes: 0,
            TotalQuotes: 0,
            CampaignCost: 0,
            ConversionRate: 0,
            ClosingRate: 0,
            AveSale: 0,
            RevenuePerLead: 0,
            AvgQuote: 0,
            QuoteToClose: 0,
            CostPerLead: 0,
            CostPerSale: 0,
            ROI: 0,
            StartDate: '',
            EndDate: '',
            SourceId: -1,
            ParentSourceId: -1
        })];
    })
    .value();
                        for (var i = 0; i < srv.FirstLevel.length; i++) {
                            var currentObject = srv.FirstLevel[i][1];
                            currentObject.ConversionRate = (currentObject.TotalLeads == 0 ? 0 : currentObject.TotalValidLeads / currentObject.TotalLeads);
                            currentObject.ClosingRate = (currentObject.TotalValidLeadsNoFactor == 0 ? 0 : currentObject.NumberOfSales / currentObject.TotalValidLeadsNoFactor);
                            currentObject.AveSale = (currentObject.NumberOfSales == 0 ? 0 : currentObject.Sales / currentObject.NumberOfSales);
                            currentObject.RevenuePerLead = (currentObject.TotalValidLeads == 0 ? 0 : currentObject.Sales / currentObject.TotalValidLeads);
                            currentObject.AvgQuote = (currentObject.TotalNumQuotes == 0 ? 0 : currentObject.TotalQuotes / currentObject.TotalNumQuotes);
                            currentObject.QuoteToClose = (currentObject.TotalNumQuotes == 0 ? 0 : currentObject.NumberOfSales / currentObject.TotalNumQuotes);
                            currentObject.CostPerLead = (currentObject.TotalValidLeadsNoFactor == 0 ? 0 : currentObject.CampaignCost / currentObject.TotalValidLeadsNoFactor);
                            currentObject.CostPerSale = (currentObject.NumberOfSales == 0 ? 0 : currentObject.CampaignCost / currentObject.NumberOfSales);
                            currentObject.ROI = (currentObject.CampaignCost == 0 ? 0 : currentObject.Sales - currentObject.CampaignCost);

                        }

                        //srv.FirstLevel[0][1]

                        srv.SelecondLevel = _.groupBy(response.data.data, 'Source');

                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        Source: { type: "string" },
                                        Campaign: { type: "string" },
                                        TotalLeads: { type: "number" },
                                        TotalValidLeads: { type: "number" },                                      
                                        NumberOfSales: { type: "number" },
                                        Sales: { type: "number" },
                                        ConversionRate: { type: "number" },
                                        ClosingRate: { type: "number" },
                                        AveSale: { type: "number" },
                                        RevenuePerLead: { type: "number" },
                                        TotalNumQuotes: { type: "number" },
                                        TotalQuotes: { type: "number" },
                                        AvgQuote: { type: "number" },
                                        QuoteToClose: { type: "number" },
                                        CampaignCost: { type: "number" },
                                        CostPerLead: { type: "number" },
                                        CostPerSale: { type: "number" },
                                        ROI: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.MarketingPerformanceData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        Source: { type: "string" },
                                        Campaign: { type: "string" },
                                        TotalLeads: { type: "number" },
                                        TotalValidLeads: { type: "number" },
                                        TotalValidLeadsNoFactor: { type: "number" },                                        
                                        NumberOfSales: { type: "number" },
                                        Sales: { type: "number" },
                                        ConversionRate: { type: "number" },
                                        ClosingRate: { type: "number" },
                                        AveSale: { type: "number" },
                                        RevenuePerLead: { type: "number" },
                                        TotalNumQuotes: { type: "number" },
                                        TotalQuotes: { type: "number" },
                                        AvgQuote: { type: "number" },
                                        QuoteToClose: { type: "number" },
                                        CampaignCost: { type: "number" },
                                        CostPerLead: { type: "number" },
                                        CostPerSale: { type: "number" },
                                        ROI: { type: "number" }
                                    }
                                }
                            },

                            group: {
                                field: "Source",
                                aggregates: [
                                        { field: "TotalLeads", aggregate: "sum" },
                                        { field: "TotalValidLeads", aggregate: "sum" },
                                        { field: "TotalValidLeadsNoFactor", aggregate: "sum" },                                        
                                        { field: "NumberOfSales", aggregate: "sum" },
                                        { field: "Sales", aggregate: "sum" },
                                        { field: "TotalNumQuotes", aggregate: "sum" },
                                        { field: "TotalQuotes", aggregate: "sum" },
                                        { field: "CampaignCost", aggregate: "sum" },
                                        { field: "ClosingRate", aggregate: "average" }
                                ]
                            },
                            aggregate: [
                                 { field: "TotalLeads", aggregate: "sum" },
                                 { field: "TotalValidLeads", aggregate: "sum" },
                                 { field: "TotalValidLeadsNoFactor", aggregate: "sum" }, 
                                 { field: "NumberOfSales", aggregate: "sum" },
                                 { field: "Sales", aggregate: "sum" },
                                 { field: "TotalNumQuotes", aggregate: "sum" },
                                 { field: "TotalQuotes", aggregate: "sum" },
                                 { field: "CampaignCost", aggregate: "sum" },
                                 { field: "ClosingRate", aggregate: "average" }
                            ]
                        });

                        if ($('#gridMarketingPerformance1').data('kendoGrid')) {
                            $('#gridMarketingPerformance1').data('kendoGrid').dataSource.read();
                            $('#gridMarketingPerformance1').data('kendoGrid').refresh();
                        }

                        //srv.LoadMarketingPerformanceReport(response.data.data)
                        break;
                    case "SalesAgent":
                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        SalesAgent: { type: "string" },
                                        NumValidLeads: { type: "number" },
                                        NumOpenJobs: { type: "number" },
                                        NumLostJobs: { type: "number" },
                                        NumSales: { type: "number" },
                                        Sales: { type: "number" },
                                        ClosingRate: { type: "number" },
                                        AvgSale: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.SalesAgentData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        SalesAgent: { type: "string" },
                                        SalesPersonId: { type: "string" },
                                        NumValidLeads: { type: "number" },
                                        NumOpenJobs: { type: "number" },
                                        NumLostJobs: { type: "number" },
                                        NumSales: { type: "number" },
                                        Sales: { type: "number" },
                                        ClosingRate: { type: "number" },
                                        AvgSale: { type: "number" },
                                        StartDate: { type: "string" },
                                        EndDate: { type: "string" }
                                    }
                                }
                            },
                            aggregate: [
                                    { field: "NumValidLeads", aggregate: "sum" },
                                    { field: "NumOpenJobs", aggregate: "sum" },
                                    { field: "NumLostJobs", aggregate: "sum" },
                                    { field: "NumSales", aggregate: "sum" },
                                    { field: "Sales", aggregate: "sum" },
                            ]
                        });
                        //srv.LoadSalesAgentReport(response.data.data)
                        break;
                    case "City":
                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        City: { type: "string" },
                                        GTotalLeads: { type: "number" },
                                        GValidLeads: { type: "number" },
                                        GLostSales: { type: "number" },
                                        GNumOfSales: { type: "number" },
                                        GTotalSales: { type: "number" },
                                        GConversionRate: { type: "number" },
                                        GClosingRate: { type: "number" },
                                        GAvgSales: { type: "number" },
                                        GRevenuePerLead: { type: "number" },
                                        GNumberQuotes: { type: "number" },
                                        GTotalQuotes: { type: "number" },
                                        GAvgQuote: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.CityData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        City: { type: "string" },
                                        GTotalLeads: { type: "number" },
                                        GValidLeads: { type: "number" },
                                        GLostSales: { type: "number" },
                                        GNumOfSales: { type: "number" },
                                        GTotalSales: { type: "number" },
                                        GConversionRate: { type: "number" },
                                        GClosingRate: { type: "number" },
                                        GAvgSales: { type: "number" },
                                        GRevenuePerLead: { type: "number" },
                                        GNumberQuotes: { type: "number" },
                                        GTotalQuotes: { type: "number" },
                                        GAvgQuote: { type: "number" },
                                        StartDate: { type: "string" },
                                        EndDate: { type: "string" }
                                    }
                                }
                            },
                            aggregate: [
                                        { field: "GTotalLeads", aggregate: "sum" },
                                        { field: "GValidLeads", aggregate: "sum" },
                                        { field: "GLostSales", aggregate: "sum" },
                                        { field: "GNumOfSales", aggregate: "sum" },
                                        { field: "GTotalSales", aggregate: "sum" },
                                        { field: "GAvgSales", aggregate: "sum" },
                                        { field: "GRevenuePerLead", aggregate: "sum" },
                                        { field: "GNumberQuotes", aggregate: "sum" },
                                        { field: "GTotalQuotes", aggregate: "sum" },
                                        { field: "GAvgQuote", aggregate: "sum" }
                            ]
                        });

                        //srv.LoadCityReport(response.data.data)
                        break;
                    case "Zip":
                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        ZipCode: { type: "string" },
                                        GTotalLeads: { type: "number" },
                                        GValidLeads: { type: "number" },
                                        GLostSales: { type: "number" },
                                        GNumOfSales: { type: "number" },
                                        GTotalSales: { type: "number" },
                                        GConversionRate: { type: "number" },
                                        GClosingRate: { type: "number" },
                                        GAvgSales: { type: "number" },
                                        GRevenuePerLead: { type: "number" },
                                        GNumberQuotes: { type: "number" },
                                        GTotalQuotes: { type: "number" },
                                        GAvgQuote: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.ZipData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        ZipCode: { type: "string" },
                                        GTotalLeads: { type: "number" },
                                        GValidLeads: { type: "number" },
                                        GLostSales: { type: "number" },
                                        GNumOfSales: { type: "number" },
                                        GTotalSales: { type: "number" },
                                        GConversionRate: { type: "number" },
                                        GClosingRate: { type: "number" },
                                        GAvgSales: { type: "number" },
                                        GRevenuePerLead: { type: "number" },
                                        GNumberQuotes: { type: "number" },
                                        GTotalQuotes: { type: "number" },
                                        GAvgQuote: { type: "number" },
                                        StartDate: { type: "string" },
                                        EndDate: { type: "string" }
                                    }
                                }
                            },
                            aggregate: [
                                        { field: "GTotalLeads", aggregate: "sum" },
                                        { field: "GValidLeads", aggregate: "sum" },
                                        { field: "GLostSales", aggregate: "sum" },
                                        { field: "GNumOfSales", aggregate: "sum" },
                                        { field: "GTotalSales", aggregate: "sum" },
                                        { field: "GAvgSales", aggregate: "sum" },
                                        { field: "GRevenuePerLead", aggregate: "sum" },
                                        { field: "GNumberQuotes", aggregate: "sum" },
                                        { field: "GTotalQuotes", aggregate: "sum" },
                                        { field: "GAvgQuote", aggregate: "sum" }
                            ]
                        });
                        //srv.LoadZipReport(response.data.data)
                        break;
                    case "Territory":
                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        Territory: { type: "string" },
                                        GTotalLeads: { type: "number" },
                                        GValidLeads: { type: "number" },
                                        GLostSales: { type: "number" },
                                        GNumOfSales: { type: "number" },
                                        GTotalSales: { type: "number" },
                                        GConversionRate: { type: "number" },
                                        GClosingRate: { type: "number" },
                                        GAvgSales: { type: "number" },
                                        GRevenuePerLead: { type: "number" },
                                        GNumberQuotes: { type: "number" },
                                        GTotalQuotes: { type: "number" },
                                        GAvgQuote: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.TerritoryData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        Territory: { type: "string" },
                                        GTotalLeads: { type: "number" },
                                        GValidLeads: { type: "number" },
                                        GLostSales: { type: "number" },
                                        GNumOfSales: { type: "number" },
                                        GTotalSales: { type: "number" },
                                        GConversionRate: { type: "number" },
                                        GClosingRate: { type: "number" },
                                        GAvgSales: { type: "number" },
                                        GRevenuePerLead: { type: "number" },
                                        GNumberQuotes: { type: "number" },
                                        GTotalQuotes: { type: "number" },
                                        GAvgQuote: { type: "number" }
                                    }
                                }
                            },
                            aggregate: [
                                        { field: "GTotalLeads", aggregate: "sum" },
                                        { field: "GValidLeads", aggregate: "sum" },
                                        { field: "GLostSales", aggregate: "sum" },
                                        { field: "GNumOfSales", aggregate: "sum" },
                                        { field: "GTotalSales", aggregate: "sum" },
                                        { field: "GAvgSales", aggregate: "sum" },
                                        { field: "GRevenuePerLead", aggregate: "sum" },
                                        { field: "GNumberQuotes", aggregate: "sum" },
                                        { field: "GTotalQuotes", aggregate: "sum" },
                                        { field: "GAvgQuote", aggregate: "sum" }
                            ]
                        });
                        //srv.LoadTerritoryReport(response.data.data)
                        break;
                    case "JobStatus":
                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        Status: { type: "string" },
                                        SubStatus: { type: "string" },
                                        NumOfJobs: { type: "number" },
                                        Totals: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.JobStatusData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        Status: { type: "string" },
                                        SubStatus: { type: "string" },
                                        SubStatusId: { type: "number" },
                                        ParentStatusIds: { type: "string" },
                                        NumOfJobs: { type: "number" },
                                        Totals: { type: "number" },
                                        StartDate: { type: "string" },
                                        EndDate: { type: "string" }
                                    }
                                }
                            },

                            group: {
                                field: "Status",
                                aggregates: [
                                        { field: "NumOfJobs", aggregate: "sum" },
                                        { field: "SubStatusId", aggregate: "min" },
                                        { field: "ParentStatusIds", aggregate: "min" },
                                        { field: "Totals", aggregate: "sum" },
                                        { field: "StartDate", aggregate: "min" },
                                        { field: "EndDate", aggregate: "min" }
                                ]
                            },
                            aggregate: [
                                 { field: "NumOfJobs", aggregate: "sum" },
                                 { field: "Totals", aggregate: "sum" }
                            ]
                        });
                        //srv.LoadJobStatusReport(response.data.data)
                        break;
                    case "LeadReport":
                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        LeadStatus: { type: "string" },
                                        SubStatus: { type: "string" },
                                        NumOfLeads: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.LeadReportData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        LeadStatus: { type: "string" },
                                        SubStatus: { type: "string" },
                                        SubStatusId: { type: "number" },
                                        ParentStatusIds: { type: "string" },
                                        NumOfLeads: { type: "number" },
                                        StartDate: { type: "string" },
                                        EndDate: { type: "string" }
                                    }
                                }
                            },

                            group: {
                                field: "LeadStatus",
                                aggregates: [
                                        { field: "NumOfLeads", aggregate: "sum" },
                                        { field: "SubStatusId", aggregate: "min" },
                                        { field: "ParentStatusIds", aggregate: "min" },
                                        { field: "StartDate", aggregate: "min" },
                                        { field: "EndDate", aggregate: "min" }
                                ]
                            },
                            aggregate: [
                                 { field: "NumOfLeads", aggregate: "sum" }
                            ]
                        });
                        //srv.LoadLeadStatusReport(response.data.data)
                        break;
                    case "Category":
                        srv.exportToExcelData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "Id",
                                    fields: {
                                        Category: { type: "string" },
                                        SubCategory: { type: "string" },
                                        TotalSales: { type: "number" },
                                        PercentOf_TotalSalesPerCategory: { type: "number" },
                                        TotalQuotes: { type: "number" },
                                        LineItmesQuoted: { type: "number" },
                                        LinesItemsSold: { type: "number" },
                                        ClosingRate: { type: "number" },
                                        AvgSalesPerLineItem: { type: "number" }
                                    }
                                }
                            },
                        });

                        srv.CategoryData = new kendo.data.DataSource({
                            data: response.data.data,
                            schema: {
                                model: {
                                    fields: {
                                        Category: { type: "string" },
                                        SubCategory: { type: "string" },
                                        TotalSales: { type: "number" },
                                        PercentOf_TotalSalesPerCategory: { type: "number" },
                                        TotalQuotes: { type: "number" },
                                        LineItmesQuoted: { type: "number" },
                                        LinesItemsSold: { type: "number" },
                                        ClosingRate: { type: "number" },
                                        AvgSalesPerLineItem: { type: "number" }
                                    }
                                }
                            },
                            group: {
                                field: "Category",
                                aggregates: [
                                        { field: "TotalSales", aggregate: "sum" },
                                        { field: "PercentOf_TotalSalesPerCategory", aggregate: "sum" },
                                        { field: "TotalQuotes", aggregate: "sum" },
                                        { field: "LineItmesQuoted", aggregate: "sum" },
                                        { field: "LinesItemsSold", aggregate: "sum" },
                                        { field: "ClosingRate", aggregate: "sum" },
                                        { field: "AvgSalesPerLineItem", aggregate: "sum" },
                                ]
                            },
                            aggregate: [
                                 { field: "TotalSales", aggregate: "sum" },
                                 { field: "TotalQuotes", aggregate: "sum" }
                            ]
                        });
                        //srv.LoadCategoryReport(response.data.data)
                        break;
                    default:

                }

            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        }

        srv.exportToExcel = function () {
            var exportData = srv.exportToExcelData.options.data;
            var totalCells = [];
            var grid = null
            if (srv.Report == 'MarketingPerformance') {

                grid = $("#gridMarketingPerformance1").data("kendoGrid");
                var dataInGrid = $("#gridMarketingPerformance1").data("kendoGrid").dataSource.view();
                exportData = []
                for (var i = 0; i < dataInGrid.length; i++) {
                    for (var s = 0; s < srv.exportToExcelData.options.data.length; s++) {
                        if (dataInGrid[i].Source == srv.exportToExcelData.options.data[s].Source) {
                            exportData.push(srv.exportToExcelData.options.data[s]);
                        }
                    }
                }
                var orderByColumn = 'Sales';
                var orderByDirection = 'desc';
                var srtAttr = $("#gridMarketingPerformance1").data("kendoGrid").dataSource.sort();
                if (srtAttr) {
                    if (srtAttr.length != 0) {
                        orderByDirection = srtAttr[0].dir;
                        orderByColumn = srtAttr[0].field;

                    }
                }
                exportData = _.orderBy(exportData, orderByColumn, orderByDirection);

                var totalsHtml = $($("#gridMarketingPerformance1").data("kendoGrid").footer).find('td');
                for (var i = 0; i < grid.columns.length; i++) {
                    var txt = $(totalsHtml[i + 1]).text().replace(/,/g, "");;

                    if (txt.indexOf('$') != -1) {
                        totalCells.push({ value: +txt.replace('$', ''), format: '$ #,##0.00', color: "#000000", bold: true });
                    }
                    else if (txt.indexOf('%') != -1) {
                        totalCells.push({ value: txt.replace('%', '') / 100, format: '#,##0.00%', color: "#000000", bold: true });
                    }
                    else if (txt.indexOf('.') != -1) {
                        totalCells.push({ value: +txt, format: '#,##0.00', color: "#000000", bold: true });
                    }
                    else {
                        totalCells.push({ value: txt, color: "#000000", bold: true });
                    }
                }
            }

            if (srv.Report == 'MonthlyStatistics') {
                grid = $("#gridMonthlyStatistics").data("kendoGrid");
            }

            if (srv.Report == 'SalesAgent') {
                grid = $("#gridSalesAgent").data("kendoGrid");
            }

            if (srv.Report == 'City') {
                grid = $("#gridCity").data("kendoGrid");
            }

            if (srv.Report == 'Zip') {
                grid = $("#gridZip").data("kendoGrid");
            }

            if (srv.Report == 'Territory') {
                grid = $("#gridTerritory").data("kendoGrid");
            }

            if (srv.Report == 'JobStatus') {
                grid = $("#gridJobStatus").data("kendoGrid");
            }

            if (srv.Report == 'LeadReport') {
                grid = $("#gridLeadReport").data("kendoGrid");
            }

            if (srv.Report == 'Category') {
                grid = $("#gridCategory").data("kendoGrid");
            }

            var cellsTitle = []

            for (var i = 0; i < grid.columns.length; i++) {
                cellsTitle.push({ value: grid.columns[i].title, background: "#428bca", color: "#ffffff" })
            }

            var rows = [{
                cells: cellsTitle
            }];

            for (var i = 0; i < exportData.length; i++) {
                var cellsData = [];
                for (var f = 0; f < grid.columns.length; f++) {
                    if (grid.columns[f].format) {

                        var formatArr = grid.columns[f].format.split(':');
                        var format = formatArr[1].substring(0, formatArr[1].length - 1);
                        if (format.indexOf('c') != -1 || format.indexOf('C') != -1) {
                            cellsData.push({ value: exportData[i][grid.columns[f].field] == 0 ? 0.00 : exportData[i][grid.columns[f].field], format: '$ #,##0.00' });
                        }
                        if (format.indexOf('n') != -1 || format.indexOf('N') != -1) {
                            cellsData.push({ value: exportData[i][grid.columns[f].field] == 0 ? 0.00 : exportData[i][grid.columns[f].field], format: '#,##0.00' });
                        }
                        if (format.indexOf('p') != -1 || format.indexOf('P') != -1) {
                            cellsData.push({ value: exportData[i][grid.columns[f].field] == 0 ? 0.00 : exportData[i][grid.columns[f].field], format: '#,##0.00%' });
                        }
                    }
                    else {
                        if (grid.columns[f].title.indexOf('Source') != -1) {
                            cellsData.push({ value: exportData[i]['Campaign'] });
                        }
                        else {
                            cellsData.push({ value: exportData[i][grid.columns[f].field] });
                        }
                    }
                }
                rows.push({
                    cells: cellsData
                });

            }

            if (totalCells.length > 0) {
                rows.push({
                    cells: totalCells
                });
            }
            var columns = [];
            for (var i = 0; i < grid.columns.length; i++) {
                columns.push({ autoWidth: true })
            }

            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                  {
                      columns: [
                        // Column settings (width)
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                        { autoWidth: true },
                      ],
                      // Title of the sheet
                      title: srv.Report,
                      // Rows of the sheet
                      rows: rows
                  }
                ]
            });


            //save the file as Excel file with extension xlsx
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: srv.Report });

        }

        function onDataBound(gridName, fideField) {
            //find("td:first").html('')
            setTimeout(function () {
                var firstElements = $("#" + gridName).find('[ng-bind="dataItem.' + fideField + '"]');
                firstElements.removeAttr("ng-bind")
                firstElements.removeAttr("class")
                firstElements.html('');
            }, 10);
        }

        //function filterablePercent() {
        //    return {
        //        ui: function (e) {
        //            e.kendoNumericTextBox({
        //                //format: "{0:p4}",
        //                //format: "p4",
        //                format: '{0:p2}',
        //                step: 0.01,
        //                decimals: 2,
        //                change: function () {
        //                    var val = this.value();
        //                    if (val > 1) {
        //                        this.value(val / 100);
        //                        this.trigger("change");
        //                    }
        //                }
        //            });
        //        }
        //    }
        //}
        function filterableDate() {
            return {
                ui: function (element) {
                    element.kendoDatePicker({
                        format: HFCService.StaticKendoDateFormat(),
                        change: function (e) {
                            var value = this.value();
                            // update the selected date in date filter
                            var divElement = e.sender.dateView.div[0];
                            var divChildren = $(divElement).children()[0];
                            var dateInput = $(divChildren).children()[2];
                            value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                            $(dateInput).text(value);
                            //value is the selected date in the datepicker
                        },
                        open: function (e) {
                            var calendar = this.dateView.calendar;
                            if (!this.element.val()) {
                                //navigate if input is empty
                                calendar.navigate(new Date());
                                var value = new Date();
                                var divElement = e.sender.dateView.div[0];
                                var divChildren = $(divElement).children()[0];
                                var dateInput = $(divChildren).children()[2];
                                value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                                $(dateInput).text(value);
                            }
                        }
                    });
                    //for disabling entering of date
                    $(element).attr("disabled", "disabled");
                }
            }
        }
        srv.CategoryOptions = {

            columns: [
                { field: 'Category', title: 'Category', width: "250px", sortable: false, groupHeaderTemplate: "#= generateHeaderGroup(data, value, 'gridCategory', 2) # ", footerTemplate: "Totals:" },
                { field: 'SubCategory', title: 'SubCategory', width: "250px" },
                { field: 'TotalSales', title: 'Sales per Category', format: "{0:c2}", width: "200px", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: 'PercentOf_TotalSalesPerCategory', title: '% Sales per Category', format: "{0:n2} %", width: "200px", },
                { field: 'TotalQuotes', title: 'Total Quotes', format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: 'LineItmesQuoted', title: 'Line Itmes Quoted', width: "170px" },
                { field: 'LinesItemsSold', title: 'Line Items Sold', width: "200px" },
                { field: 'ClosingRate', title: 'Closing Rate per Line Item', format: "{0:n2} %", width: "220px" },
                { field: 'AvgSalesPerLineItem', title: 'Avg Sales Per Line Item', format: "{0:c2}", width: "200px" },
            ],
            dataBound: function () {
                setTimeout(function () {
                    var firstElements = $("#gridCategory").find('[ng-bind="dataItem.Category"]');
                    firstElements.removeAttr("ng-bind")
                    firstElements.removeAttr("class")
                    firstElements.html('');
                }, 10);

            },

            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true,
        };

        srv.MonthlyStatisticsOptions = {

            columns: [
                { field: "Month", title: "Month", width: "150px", footerTemplate: "Totals:" },
                { field: "NumberOfLeads", title: "# of Leads", width: "120px", footerTemplate: "#=sum#" },
                { field: "ValidLeads", title: "Valid Leads", width: "120px", footerTemplate: "#=sum#" },
                { field: "LostSale", title: "Lost Sales", width: "120px", footerTemplate: "#=sum#" },
                { field: "NumberOfSales", title: "# of Sales", width: "120px", footerTemplate: "#=sum#" },
                { field: "TotalSales", title: "Total Sales", format: "{0:c2}", width: "120px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "ConversionRate", title: "Conversion Rate", format: "{0:n2} %", width: "160px", footerTemplate: "#=calculateTotals('gridMonthlyStatistics','ValidLeads/NumberOfLeads', 'P')#" },
                { field: "ClosingRate", title: "Closing Rate", format: "{0:n2} %", width: "130px", footerTemplate: "#=calculateTotals('gridMonthlyStatistics','NumberOfSales/ValidLeads', 'P')#" },
                { field: "AvgSale", title: "Avg Sale", width: "120px", format: "{0:c2}", footerTemplate: "#=calculateTotals('gridMonthlyStatistics','TotalSales/NumberOfSales', 'C')#" },
                { field: "RevenuePerLead", title: "Revenue Per Lead", format: "{0:c2}", width: "170px", footerTemplate: "#=calculateTotals('gridMonthlyStatistics','TotalSales/ValidLeads', 'C')#" },
                { field: "TotalNumQuotes", title: "# of Quotes", width: "160px", footerTemplate: "#=sum#" },
                { field: "TotalQuotes", title: "Total Quotes", format: "{0:c2}", width: "130px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "AvgQuote", title: "Avg Quote", format: "{0:c2}", width: "130px", footerTemplate: "#=calculateTotals('gridMonthlyStatistics','TotalQuotes/TotalNumQuotes', 'C')#", },
                { field: "QuoteCloseRate", title: "Quote to Close %", format: "{0:n2} %", width: "160px", footerTemplate: "#=calculateTotals('gridMonthlyStatistics','TotalSales/TotalQuotes', 'P')#", },
            ],

            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            //columnMenu: true,
            sortable: true,
            scrollable: true,
            resizable: true

        };

        srv.MarketingPerformanceOptions1 = {

            columns: [
                { field: "Source", title: "Category", sortable: false, width: "200px", footerTemplate: "Totals:" },
                { title: "Source/Campaign", width: "200px" },
                { field: "TotalLeads", title: "# Leads", format: "{0:n2}", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','TotalLeads:sum', '0.00') #", width: "100px" },
                { field: "TotalValidLeads", title: "# Valid Leads", format: "{0:n2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','TotalValidLeads:sum', '0.00') #", },                
                { field: "NumberOfSales", title: "# of Sales", format: "{0:n2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','NumberOfSales:sum', '0.00') #" },
                { field: "Sales", title: "Total Sales", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','Sales:sum', 'C') #" },
                { field: "ConversionRate", title: "Conversion Rate", format: "{0:P} ", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','TotalValidLeads/TotalLeads', 'P2') #" },
                { field: "ClosingRate", title: "Closing Rate", format: "{0:P2} ", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','NumberOfSales/TotalValidLeadsNoFactor', 'P2') #" },
                { field: "AveSale", title: "Avg Sale", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','Sales/NumberOfSales', 'C') #" },
                { field: "RevenuePerLead", title: "Revenue Per Lead", format: "{0:c2}", width: "200px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','Sales/TotalValidLeads', 'C') #" },
                { field: "TotalNumQuotes", title: "# of Quotes", format: "{0:n2}", width: "200px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','TotalNumQuotes:sum', '0.00') #" },
                { field: "TotalQuotes", title: "Total Quotes", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','TotalQuotes:sum', 'C') #" },
                { field: "AvgQuote", title: "Avg Quote", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','TotalQuotes/TotalValidLeads', 'C') #" },
                { field: "QuoteToClose", title: "Quote to Close %", format: "{0:P2} ", width: "160px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','Sales/TotalQuotes', 'P') #" },
                { field: "CampaignCost", title: "Campaign Cost", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','CampaignCost:sum', 'C') #" },
                { field: "CostPerLead", title: "Cost Per Lead", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','CampaignCost/TotalValidLeadsNoFactor', 'C') #" },
                { field: "CostPerSale", title: "Cost Per Sale", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','CampaignCost/NumberOfSales', 'C') #" },
                { field: "ROI", title: "ROI", format: "{0:c2}", width: "100px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','Sales/CampaignCost', 'C') #" },
                { hidden: true, field: "TotalValidLeadsNoFactor", title: "XXXXXXXX", format: "{0:n2}", width: "150px", footerTemplate: "#= calculateTotalsH('gridMarketingPerformance1','TotalValidLeads:sum', '0.00') #", },
                
            ],

            dataBound: function (e) {

                $('[data-field]').attr("style", "background-color:#f3f3f4");
                var srtAttr = e.sender.dataSource.sort();
                if (srtAttr) {
                    if (srtAttr.length == 0) {
                        $('[data-field="Sales"]').attr("style", "background-color:lightblue");
                    }
                    else {
                        $('[data-field="' + srtAttr[0].field + '"]').attr("style", "background-color:lightblue");
                    }
                }
                else {
                    $('[data-field="Sales"]').attr("style", "background-color:lightblue");

                }

            },
            detailInit: detailInit23,
            dataSource: {
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "id",
                        fields: {
                            Source: { type: "string" },
                            Campaign: { type: "string" },
                            TotalLeads: { type: "number" },
                            TotalValidLeads: { type: "number" },
                            NumberOfSales: { type: "number" },
                            Sales: { type: "number" },
                            ConversionRate: { type: "number" },
                            ClosingRate: { type: "number" },
                            AveSale: { type: "number" },
                            RevenuePerLead: { type: "number" },
                            TotalNumQuotes: { type: "number" },
                            TotalQuotes: { type: "number" },
                            AvgQuote: { type: "number" },
                            QuoteToClose: { type: "number" },
                            CampaignCost: { type: "number" },
                            CostPerLead: { type: "number" },
                            CostPerSale: { type: "number" },
                            ROI: { type: "number" },
                            StartDate: { type: "string" },
                            EndDate: { type: "string" },
                            SourceId: { type: "number" },
                            ParentSourceId: { type: "number" },
                        }
                    }
                },
                error: function (e) {
                    var msg = e.xhr.responseText;
                    $scope.savedSuccessfully = false;
                    $scope.message = msg;
                },
                transport: {
                    read: function (options) {
                        var ds = {
                            data: null,
                            total: null
                        }

                        var temp = [];
                        if (srv.FirstLevel) {
                            for (var i = 0; i < srv.FirstLevel.length; i++) {
                                temp.push(srv.FirstLevel[i][1]);
                            }

                            ds.data = _.orderBy(temp, ['Sales'], ['desc']);
                            ds.total = temp.length;
                        }
                        options.success(ds);
                    },

                    parameterMap: function (data, operation) {
                        return JSON.stringify(data);
                    }
                }, serverPaging: false,
                serverSorting: false
            },
            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true
        };

        function detailInit23(e) {
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                columns: [
                    {
                        field: "", title: "Category", headerAttributes: {
                            "class": "GridNoHeader"

                        }, sortable: false, width: "190px"
                    },
                { field: "Campaign", title: "Source/Campaign", width: "200px" },
                { field: "TotalLeads", title: "# Leads", format: "{0:n2}", width: "100px", },
                { field: "TotalValidLeads", title: "# Valid Leads", format: "{0:n2}", width: "150px" },
                { hidden: true, field: "TotalValidLeadsNoFactor", title: "Whole leads", format: "{0:n2}", width: "150px" },
                { field: "NumberOfSales", title: "# of Sales", format: "{0:n2}", width: "150px" },
                { field: "Sales", title: "Total Sales", format: "{0:c2}", width: "150px" },
                { field: "ConversionRate", title: "Conversion Rate", format: "{0:P2}", width: "150px" },
                { field: "ClosingRate", title: "Closing Rate", format: "{0:P2}", width: "150px" },
                { field: "AveSale", title: "Avg Sale", format: "{0:c2}", width: "150px" },
                { field: "RevenuePerLead", title: "Revenue Per Lead", format: "{0:c2}", width: "200px" },
                { field: "TotalNumQuotes", title: "# of Quotes", format: "{0:n2}", width: "200px" },
                { field: "TotalQuotes", title: "Total Quotes", format: "{0:c2}", width: "150px" },
                { field: "AvgQuote", title: "Avg Quote", format: "{0:c2}", width: "150px" },
                { field: "QuoteToClose", title: "Quote to Close %", format: "{0:P2}", width: "160px" },
                { field: "CampaignCost", title: "Campaign Cost", format: "{0:c2}", width: "150px" },
                { field: "CostPerLead", title: "Cost Per Lead", format: "{0:c2}", width: "150px" },
                { field: "CostPerSale", title: "Cost Per Sale", format: "{0:c2}", width: "150px" },
                { field: "ROI", title: "ROI", format: "{0:c2}", width: "100px" }
                ],
                //filterable: true,
                //{
                //    mode: "row"
                //},
                dataBound: function (e) {
                    e.sender.thead.hide();
                    $(e.sender.table).parent().attr("style", "overflow:hidden");
                },
                dataSource: {
                    dataType: 'JSON',
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                    autoSync: true,
                    pageSize: 10,
                    schema: {
                        data: "data",
                        model: {
                            id: "id",
                            fields: {
                                Source: { type: "string" },
                                Campaign: { type: "string" },
                                TotalLeads: { type: "number" },
                                TotalValidLeads: { type: "number" },
                                NumberOfSales: { type: "number" },
                                Sales: { type: "number" },
                                ConversionRate: { type: "number" },
                                ClosingRate: { type: "number" },
                                AveSale: { type: "number" },
                                RevenuePerLead: { type: "number" },
                                TotalNumQuotes: { type: "number" },
                                TotalQuotes: { type: "number" },
                                AvgQuote: { type: "number" },
                                QuoteToClose: { type: "number" },
                                CampaignCost: { type: "number" },
                                CostPerLead: { type: "number" },
                                CostPerSale: { type: "number" },
                                ROI: { type: "number" },
                                StartDate: { type: "string" },
                                EndDate: { type: "string" },
                                SourceId: { type: "number" },
                                ParentSourceId: { type: "number" },
                            }
                        }
                    },
                    transport: {
                        read: function (options) {


                            var data = srv.SelecondLevel[e.data.Source];
                            var ds = {
                                data: null,
                                total: null
                            }
                            if (data) {

                                ds.data = _.orderBy(data, ['Sales'], ['desc']);
                                //                            var ff = _(data).chain()
                                //.sort("date")
                                //.reverse()
                                // .result();

                                ds.total = data.length;
                            }
                            options.success(ds);
                        },
                        parameterMap: function (data, operation) {
                            return JSON.stringify(data);
                        }
                    }
                },
                filterable: true,
                sortable: false,
                scrollable: false
            });
        }


        srv.MarketingPerformanceOptions = {

            columns: [
                { field: "Source", title: "Category", sortable: false, groupHeaderTemplate: "#= generateHeaderGroup(data, value, 'gridMarketingPerformance', 2) # ", width: "200px", footerTemplate: "Totals:" },
                { field: "Campaign", title: "Source/Campaign", width: "200px" },
                {
                    field: "TotalLeads", title: "# Leads", format: "{0:n2}", footerTemplate: "#=sum#", width: "100px", sortable: {
                        compare: function (a, b) {
                            var grid = $("#gridMarketingPerformance").data("kendoGrid");
                            //set asc direction to the group (for example grouped by FirstName)
                            grid.dataSource.group({ field: "TotalLeads", dir: "asc" });

                            // $('#gridMarketingPerformance').data('kendoGrid').dataSource.sort({ field: 'TotalLeads', dir: 'asc' });
                            return 1;
                        }
                    }
                },
                { field: "TotalValidLeads", title: "# Valid Leads", format: "{0:n2}", width: "150px", footerTemplate: "#=kendo.toString(sum, '0.00') #", },
                { field: "NumberOfSales", title: "# of Sales", format: "{0:n2}", width: "150px", footerTemplate: "#=kendo.toString(sum, '0.00') #" },
                { field: "Sales", title: "Total Sales", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "ConversionRate", title: "Conversion Rate", format: "{0:n2} %", width: "150px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','TotalValidLeads/TotalLeads', 'P') #" },
                { field: "ClosingRate", title: "Closing Rate", format: "{0:n2} %", width: "150px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','NumberOfSales/TotalLeads', 'P') #" },
                { field: "AveSale", title: "Avg Sale", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','Sales/NumberOfSales', 'C') #" },
                { field: "RevenuePerLead", title: "Revenue Per Lead", format: "{0:c2}", width: "200px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','Sales/TotalValidLeads', 'C') #" },
                { field: "TotalNumQuotes", title: "# of Quotes", format: "{0:n2}", width: "200px", footerTemplate: "#=kendo.toString(sum, '0.00') #" },
                { field: "TotalQuotes", title: "Total Quotes", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "AvgQuote", title: "Avg Quote", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','TotalQuotes/TotalValidLeads', 'C') #" },
                { field: "QuoteToClose", title: "Quote to Close %", format: "{0:n2} %", width: "160px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','Sales/TotalQuotes', 'P') #" },
                { field: "CampaignCost", title: "Campaign Cost", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "CostPerLead", title: "Cost Per Lead", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','CampaignCost/TotalValidLeads', 'C') #" },
                { field: "CostPerSale", title: "Cost Per Sale", format: "{0:c2}", width: "150px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','CampaignCost/NumberOfSales', 'C') #" },
                { field: "ROI", title: "ROI", format: "{0:c2}", width: "100px", footerTemplate: "#= calculateTotals('gridMarketingPerformance','Sales/CampaignCost', 'C') #" }
            ],
            dataBound: function () {
                if (this.dataSource.group().length > 0) {
                    var groups = $(".k-grouping-row");
                    for (var i = 0; i < groups.length; i++) {
                        this.collapseGroup(groups[i]);
                    }
                }

                setTimeout(function () {
                    var firstElements = $("#gridMarketingPerformance").find('[ng-bind="dataItem.Source"]');
                    firstElements.removeAttr("ng-bind")
                    firstElements.removeAttr("class")
                    firstElements.html('');
                }, 10);

            },

            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true,
            columnReorder: function () {

                var sort = [];
                $.each(this.columns, function (idx, elem) {
                    if (elem.field.lastIndexOf("Ship", 0) === 0) {
                        sort.push({ field: elem.field, dir: "asc" });
                    }
                });
                this.dataSource.sort(sort);
            },
        };

        srv.SalesAgentOptions = {

            columns: [
                { field: "SalesAgent", title: "Sales Agent", width: "200px", footerTemplate: "Totals:" },
                { field: "NumValidLeads", title: "# Valid Leads", width: "100px", footerTemplate: "#=sum#" },
                { field: "NumOpenJobs", title: "# Open Jobs", width: "100px", footerTemplate: "#=sum#" },
                { field: "NumLostJobs", title: "# Lost jobs", width: "100px", footerTemplate: "#=sum#" },
                { field: "NumSales", title: "# Sales", width: "100px", footerTemplate: "#=sum#" },
                { field: "Sales", title: "Total Sales", format: "{0:c2}", width: "100px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "ClosingRate", title: "Closing Rate", format: "{0:n2} %", width: "100px", footerTemplate: "#=calculateTotals('gridSalesAgent','NumSales/NumValidLeads', 'P')#" },
                { field: "AvgSale", title: "Avg Sales", format: "{0:c2}", width: "100px", footerTemplate: "#=calculateTotals('gridSalesAgent','Sales/NumSales', 'C')#" },
            ],

            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true,
        };

        srv.ZipOptions = {
            columns: [
                { field: "ZipCode", title: "Zip/Postal", width: "200px", footerTemplate: "Totals:" },
              { field: "GTotalLeads", title: "# of Leads", width: "150px", footerTemplate: "#=sum#" },
                { field: "GValidLeads", title: "Valid Leads", width: "150px", footerTemplate: "#=sum#" },
                { field: "GLostSales", title: "Lost Sales", width: "150px", footerTemplate: "#=sum#" },
                { field: "GNumOfSales", title: "# of Sales", width: "150px", footerTemplate: "#=sum#" },
                { field: "GTotalSales", title: "Total Sales", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "GConversionRate", title: "Conversion Rate", format: "{0:n2} %", width: "150px", footerTemplate: "#=calculateTotals('gridZip','GValidLeads/GTotalLeads', 'P')#" },
                { field: "GClosingRate", title: "Closing Rate", format: "{0:n2} %", width: "150px", footerTemplate: "#=calculateTotals('gridZip','GNumOfSales/GValidLeads', 'P')#" },
                { field: "GAvgSales", title: "Avg Sale", width: "100px", format: "{0:c2}", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "GRevenuePerLead", title: "Revenue Per Lead", format: "{0:c2}", width: "170px", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "GNumberQuotes", title: "# of Quotes", width: "170px", footerTemplate: "#=sum#", },
                { field: "GTotalQuotes", title: "Total Quotes", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "GAvgQuote", title: "Avg Quote", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },

            ],
            detailInit: CityTerZipDetailInit,
            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true,
        };

        srv.CityOptions = {
            columns: [
                { field: "City", title: "City", width: "200px", footerTemplate: "Totals:" },
                { field: "GTotalLeads", title: "# of Leads", width: "150px", footerTemplate: "#=sum#" },
                { field: "GValidLeads", title: "Valid Leads", width: "150px", footerTemplate: "#=sum#" },
                { field: "GLostSales", title: "Lost Sales", width: "150px", footerTemplate: "#=sum#" },
                { field: "GNumOfSales", title: "# of Sales", width: "150px", footerTemplate: "#=sum#" },
                { field: "GTotalSales", title: "Total Sales", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "GConversionRate", title: "Conversion Rate", format: "{0:n2} %", width: "150px", footerTemplate: "#=calculateTotals('gridCity','GValidLeads/GTotalLeads', 'P')#" },
                { field: "GClosingRate", title: "Closing Rate", format: "{0:n2} %", width: "150px", footerTemplate: "#=calculateTotals('gridCity','GNumOfSales/GValidLeads', 'P')#" },
                { field: "GAvgSales", title: "Avg Sale", width: "100px", format: "{0:c2}", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "GRevenuePerLead", title: "Revenue Per Lead", format: "{0:c2}", width: "170px", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "GNumberQuotes", title: "# of Quotes", width: "170px", footerTemplate: "#=sum#", },
                { field: "GTotalQuotes", title: "Total Quotes", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "GAvgQuote", title: "Avg Quote", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },

            ],
            detailInit: CityTerZipDetailInit,
            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true
        };

        srv.TerritoryOptions = {
            columns: [
                { field: "TerrCode", title: "Territory", width: "200px", footerTemplate: "Totals:" },
                { field: "GTotalLeads", title: "# of Leads", width: "150px", footerTemplate: "#=sum#", },
                { field: "GValidLeads", title: "Valid Leads", width: "150px", footerTemplate: "#=sum#", },
                { field: "GLostSales", title: "Lost Sales", width: "150px", footerTemplate: "#=sum#", },
                { field: "GNumOfSales", title: "# of Sales", width: "150px", footerTemplate: "#=sum#", },
                { field: "GTotalSales", title: "Total Sales", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "GConversionRate", title: "Conversion Rate", format: "{0:n2} %", width: "150px", footerTemplate: "#=calculateTotals('gridTerritory','GValidLeads/GTotalLeads', 'P')#" },
                { field: "GClosingRate", title: "Closing Rate", format: "{0:n2} %", width: "150px", footerTemplate: "#=calculateTotals('gridTerritory','GNumOfSales/GValidLeads', 'P')#" },
                { field: "GAvgSales", title: "Avg Sale", width: "100px", format: "{0:c2}", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "GRevenuePerLead", title: "Revenue Per Lead", format: "{0:c2}", width: "170px", footerTemplate: "#=kendo.toString(sum, 'C') #" },
                { field: "GNumberQuotes", title: "# of Quotes", width: "170px", footerTemplate: "#=sum#", },
                { field: "GTotalQuotes", title: "Total Quotes", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },
                { field: "GAvgQuote", title: "Avg Quote", format: "{0:c2}", width: "150px", footerTemplate: "#=kendo.toString(sum, 'C') #", },

            ],
            detailInit: CityTerZipDetailInit,
            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true,
        };

        function CityTerZipDetailInit(e) {

            $("<div/>").appendTo(e.detailCell).kendoGrid({
                columns: [
                { field: "JobId", title: "Job Id", width: "100px" },
                { field: "LeadId", title: "Lead Id", width: "100px" },
                { field: "JobNumber", title: "Job Number", width: "100px" },
                    { field: "ContractedOnUtc", title: "Contracted", width: "100px", type: "date", filterable: HFCService.gridfilterableDate("date", "", ""), format: "{0:MM/dd/yyyy}", parseFormats: "{0:MM/dd/yyyy}", },
                { field: "Subtotal", title: "Sub Total", width: "100px", format: "{0:c2}" },
                { field: "SurchargeTotal", title: "Surcharge Total", width: "100px", format: "{0:c2}" },
                { field: "DiscountTotal", title: "Discount Total", width: "100px", format: "{0:c2}" },
                { field: "TotalSales", title: "Total Sales", width: "100px", format: "{0:c2}" },
                ]
                ,
                filterable: false,
                sortable: true,
                //{
                //    mode: "row"
                //},
                dataBound: function () {
                    //this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                dataSource: {
                    dataType: 'JSON',

                    pageSize: 10,
                    schema: {
                        data: "data",
                        model: {
                            id: "id",
                            fields: {
                                JobId: { type: "number" },
                                LeadId: { type: "number" },
                                JobNumber: { type: "number" },
                                ContractedOnUtc: { type: "date" },
                                Subtotal: { type: "number" },
                                SurchargeTotal: { type: "number" },
                                DiscountTotal: { type: "number" },
                                TotalSales: { type: "number" }
                            }
                        }
                    },
                    transport: {
                        read: function (options) {
                            //$(".k-loading-mask").remove();
                            var data = e.data.SubModels;
                            var ds = {
                                data: null,
                                total: null
                            }

                            if (data) {
                                ds.data = data;
                                ds.total = data.length;
                            }
                            options.success(ds);
                        },
                        parameterMap: function (data, operation) {
                            return JSON.stringify(data);
                        }
                    }
                },
            });
        }


        srv.JobStatusOptions = {

            columns: [
                { field: "Status", title: "Job Status", sortable: false, groupHeaderTemplate: "#= generateHeaderGroup(data, value, 'gridJobStatus', 2) # ", width: "260px", footerTemplate: "Totals:" },
                { field: "SubStatus", title: "Sub Status", width: "260px", footerTemplate: "" },
                { field: "NumOfJobs", title: "# of Jobs", footerTemplate: "#=sum#", width: "120px", },
                { field: "Totals", title: "Line Item Totals", format: "{0:c2}", width: "165px", footerTemplate: "#=kendo.toString(sum, 'C') #", }
            ],
            dataBound: function () {
                setTimeout(function () {
                    var firstElements = $("#gridJobStatus").find('[ng-bind="dataItem.Status"]');
                    firstElements.removeAttr("ng-bind")
                    firstElements.removeAttr("class")
                    firstElements.html('');
                }, 10);

            },

            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true,
        };

        srv.LeadReportOptions = {

            columns: [
                { field: "LeadStatus", title: "Lead Status", sortable: false, groupHeaderTemplate: "#= generateHeaderGroup(data, value, 'gridLeadReport', 2) # ", width: "322px", footerTemplate: "Totals:" },
                { field: "SubStatus", title: "Sub Status", width: "322px" },
                { field: "NumOfLeads", title: "# of Leads", footerTemplate: "#=sum#", width: "160px" },
            ],
            dataBound: function () {

                setTimeout(function () {
                    var firstElements = $("#gridLeadReport").find('[ng-bind="dataItem.LeadStatus"]');
                    firstElements.removeAttr("ng-bind")
                    firstElements.removeAttr("class")
                    firstElements.html('');
                }, 10);
            },
            autoBind: true,
            height: window.innerHeight - 200,
            filterable: true,
            sortable: true,
            scrollable: true,
        };


    }])
        .controller('SalesBetaController', ['$http', '$scope', 'SalesBetaService', function ($http, $scope, SalesBetaService) {

            $scope.data = null;
            $scope.SalesBetaService = SalesBetaService;
            $scope.startDate = null;
            $scope.endDate = null;
            $scope.Links = function (id) {
                var row = id.split("=");
                var row_ID = row[1];
                var sitename = $("#users_grid").getCell(row_ID, 'Site_Name');
                var url = "http://" + sitename; // sitename will be like google.com or yahoo.com

                window.open(url);

            }

            // $scope.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];

            $scope.DateRangeArraySA = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
            $scope.DateRangeArrayMS = ["All Date & Time", "This Year", "Last Year"];
            $scope.DateRangeArrayMP = ["All Date & Time", "Last 90 days", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year", "Last Year"];

            //self.DateRangeArray = ["All Date & Time", "This Month", "Last Month", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year"];

            $scope.selectedDateText = "All Date & Time";

            if ($scope.SalesBetaService.Report == 'MarketingPerformance') {
                $scope.selectedDateText = 'Last 90 days';
            }

            $scope.$watch('SalesBetaService.Report', function (newVal) {
                if (newVal) {
                    $scope.selectedDateText = "All Date & Time";

                    SalesBetaService.startDate = null;
                    SalesBetaService.endDate = null;
                    $scope.startDate = null;
                    $scope.endDate = null;

                    if ($scope.SalesBetaService.Report == 'MarketingPerformance') {
                        $scope.selectedDateText = 'Last 90 days';
                        var endDate = new XDate();
                        var startDate = new XDate().addDays(-90);

                        SalesBetaService.startDate = startDate.toString('MM/dd/yyyy');
                        SalesBetaService.endDate = endDate.toString('MM/dd/yyyy');
                    }
                }
            });

            $scope.$watch('startDate', function (newVal) {
                if (newVal != null) {

                    SalesBetaService.startDate = newVal;
                    if ($scope.endDate == null) {
                        $scope.selectedDateText = newVal + ' to ';
                    }
                    else {
                        $scope.selectedDateText = newVal + ' to ' + $scope.endDate;
                    }
                }
            });

            $scope.$watch('endDate', function (newVal) {
                if (newVal != null) {

                    SalesBetaService.endDate = newVal;
                    if ($scope.startDate == null) {
                        $scope.selectedDateText = ' to ' + newVal
                    }
                    else {
                        $scope.selectedDateText = $scope.startDate + ' to ' + newVal;
                    }
                }
            });

            $scope.setDateRange = function (date) {
                var range = $scope.getDateRange(date);
                if (range.StartDate) {
                    SalesBetaService.startDate = range.StartDate.toString('MM/dd/yyyy');
                    //$scope.startDate = range.StartDate.toString('MM/dd/yyyy');
                } else {
                    //$scope.startDate = null;
                    SalesBetaService.startDate = null;
                }

                if (range.EndDate) {
                    SalesBetaService.endDate = range.EndDate.toString('MM/dd/yyyy');
                    //$scope.EndDate = range.EndDate.toString('MM/dd/yyyy');
                } else {
                    SalesBetaService.endDate = null;
                    //$scope.startDate = null;
                }
                //if (SalesBetaService.Report == 'MonthlyStatistics') {

                //  SalesBetaService.startDate = range.StartDate.getFullYear() ;
                //SalesBetaService.endDate = range.EndDate.getFullYear() + 1;
                // $scope.startDate = SalesBetaService.startDate;
                // $scope.endDate = SalesBetaService.endDate
                //}

                $scope.selectedDateText = date;
            }

            $scope.getDateRange = function (range) {
                var endDate = new XDate(),
                    startDate = new XDate();
                switch (range.toLowerCase()) {
                    case "last 90 days":
                        startDate.addDays(-90);
                        break;
                    case "all date & time":
                        startDate = null;
                        endDate = null;
                        break;
                    case "this week":
                        startDate.addDays(-startDate.getDay());
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "this month":
                        startDate.setDate(1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "this year":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addYears(1).addDays(-1);
                        break;
                    case "last week":
                        startDate.addDays(-startDate.getDay() - 7);
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "last month":
                        startDate.setDate(1).addMonths(-1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "last year":
                        startDate.setDate(1).setMonth(0).addYears(-1);
                        endDate.setDate(1).setMonth(0).addDays(-1);
                        break;
                    case "last 7 days":
                        startDate.addDays(-7); break;
                    case "last 30 days":
                        startDate.addDays(-30); break;
                    case "1st quarter":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "2nd quarter":
                        startDate.setDate(1).setMonth(3);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "3rd quarter":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "4th quarter":
                        startDate.setDate(1).setMonth(9);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "1st half":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(6).addDays(-1); break;
                    case "2nd half":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(6).addDays(-1); break;
                    default:
                        startDate = null; endDate = null; break;
                }

                return { StartDate: startDate, EndDate: endDate };
            }


            //$scope.SalesBetaService.GetReport();
        }]).directive('datepicker', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {

                    $(function () {
                        element.datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            dateFormat: 'dd/mm/yy',
                            onSelect: function (date) {
                                scope.$apply(function () {
                                    ngModelCtrl.$setViewValue(date);
                                });
                            }
                        });
                    });
                }
            }
        })
        .directive('selectpicker', ['$parse', function ($parse) {
            return {
                restrict: 'A',
                require: '?ngModel',
                priority: 10,
                compile: function (tElement, tAttrs, transclude) {
                    tElement.selectpicker($parse(tAttrs.selectpicker)());
                    tElement.selectpicker('refresh');
                    return function (scope, element, attrs, ngModel) {
                        if (!ngModel) return;

                        scope.$watch(attrs.ngModel, function (newVal, oldVal) {
                            scope.$evalAsync(function () {
                                if (!attrs.ngOptions || /track by/.test(attrs.ngOptions)) element.val(newVal);
                                element.selectpicker('refresh');
                            });
                        });

                        ngModel.$render = function () {
                            scope.$evalAsync(function () {
                                element.selectpicker('refresh');
                            });
                        }
                    };
                }

            };
        }])

})(window, document);