﻿
//Kendo Grid implementation
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.franchise.list.controller', ['hfc.core.service', "kendo.directives"])

    .service('FranchiseListService', ['$http', '$location', 'HFCService', function ($http, $location, HFCService) {
        var srv = this;

        srv.RoyaltiesCollection1 = {};
        srv.FranchiseID = null;

        srv.Pagination = { page: 0, size: 25, pageTotal: 5, getPage: function () { return srv.Pagination.page + 1 } };
        srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
        srv.PageTotal = srv.Pagination.pageTotal;
        srv.PageSize = srv.Pagination.size;
        srv.PageNumber = srv.Pagination.page;
        srv.TotalRecords = 0;
        srv.ChangePageSize = function () {

        }

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            var result = decodeURIComponent(results[2].replace(/\+/g, " "));
            return result;
        }

        function MainGridDropdownTemplate(dataItem) {
           
            var Div = '';
            Div += '<ul>';
            Div += '<li class="dropdown note1 ad_user">';
            Div += '<div class="dropdown">';
            Div += '<button class="btn btn-default dropbtn">';
            Div += '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>';
            Div += '</button>';
            Div += ' <ul class="dropdown-content">';
            if (dataItem.IsSuspended) {
                Div += '<li>';
                Div += '<a href="javascript:void(0)" ng-if="' + dataItem.IsSuspended + '" class="removeSuspended" data-toggle="modal" data-target="#myModalRenew" ng-click="FranchiseListService.RenewFranchise(' + dataItem.FranchiseId + ')">Renew Franchise</a>';
                Div += '</li>';
            }
            else {
                Div += '<li>';
                Div += '<a href="#" class="deactivateLink" data-toggle="modal" data-target="#myModal" ng-click="FranchiseListService.editFranchise(' + dataItem.FranchiseId + ')">Deactivate</a>';
                Div += '</li>';
            }


            if (dataItem.Owner != null && !dataItem.IsSuspended) {
                Div += '<li class="dropdown-submenu">';

                Div += '<a href="#">Impersonate as:</a>';
                Div += '<ul class="dropdown-menu" style="height: auto; top:3px; margin-left:-130px !important; overflow-x: hidden;" role="menu">';
                Div += '<li>';
                Div += '<a href="#" ng-click="impersonate(\'' + dataItem.Owner.User.UserId + '\')">' + dataItem.Owner.User.UserName + '</a>';
                Div += '</li>';
                for (var i = 0; i < dataItem.Users.length; i++) {
                    Div += '<li>';
                    Div += '<a href="#" ng-click="impersonate(\'' + dataItem.Users[i].UserId + '\')" >' + dataItem.Users[i].UserName + '</a>';
                    Div += '</li>';
                }
                Div += '</ul>';
                Div += '</li>';
            }
            Div += '</ul>';
            Div += '</div>';
            Div += '</li>';

            Div += '</ul>';
            return Div;
        };

        function MainGridStatusTemplate(dataItem) {
            var Div = '';
            Div += '<span ng-if="' + dataItem.IsSuspended + '" class="fieldInActive index_cbox">';
            Div += '<input type="checkbox" value="suspended Franchise" id="lstchksuspendedFranchise" disabled="disabled" checked="' + dataItem.IsSuspended + '" style="border:0px solid #b2bfca">';
            Div += '<label class="checkbox" for="lstchksuspendedFranchise" style="border:0px solid #b2bfca"></label>';
            Div += '</span>';

            Div += '<span class="field index_cbox" ng-hide="' + dataItem.IsSuspended + '">';
            Div += '<input type="checkbox" value="suspended Franchise" id="lstchksuspendedFranchise" disabled="disabled" checked="' + dataItem.IsSuspended + '" style="border:0px solid #b2bfca">';
            Div += '<label class="checkbox" for="lstchksuspendedFranchise" style="border:0px solid #b2bfca"></label>';
            Div += '</span>';
            return Div;
        }

        srv.GetMainGridOptions = function (data) {
            var filters = {
                pageIndex: srv.Pagination.page,
                pageSize: srv.Pagination.size
            };
            if (getParameterByName('searchTerm'))
                filters.searchTerm = getParameterByName('searchTerm');
            if (getParameterByName('searchFilter'))
                filters.searchFilter = getParameterByName('searchFilter');
            if (getParameterByName('searchBrand'))
                filters.searchBrand = getParameterByName('searchBrand');
            if (getParameterByName('ShowInActive'))
                filters.ShowInActive = getParameterByName('ShowInActive');



            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '/api/franchise/0/list',
                        dataType: "json",
                        data: filters
                    }

                },
                error: function (e) {

                    HFC.DisplayAlert(e.errorThrown);
                },
                pageSize: 25
            });

            srv.mainGridOptions = {
                cache: false,
                dataSource: ds,
                // https://www.telerik.com/forums/remove-page-if-no-records
                dataBound: function (e) {
                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                },
                columns: [
                          {
                              field: "Name",
                              title: "Franchise",
                              template: "<a href='\\\\controlpanel/franchises/#= FranchiseId #' style='color: rgb(61,125,139);'><span >#= Name #</span></a> ",
                              filterable: { multi: true, search: true, dataSource: ds }
                          },
                          {
                              field: "Code",
                              title: "Account Code",
                              filterable: { multi: true, search: true, dataSource: ds }
                          },
                          {
                              field: "Address",
                              title: "Location",
                              filterable: { multi: true, search: true, dataSource: ds }
                          },
                          {
                              field: "IsSuspended",
                              title: "Active",
                              template: function (dataItem) {
                                  return MainGridStatusTemplate(dataItem);
                              }
                          },
                          {
                              field: "",
                              title: "",
                              template: function (dataItem) {
                                  return MainGridDropdownTemplate(dataItem);
                              }
                          }
                ],
                noRecords: { template: "No records found" },
                dataBound: function (e) {
                    // get the index of the UnitsInStock cell
                    var columns = e.sender.columns;
                    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "IsSuspended" + "]").index();

                    // iterate the data items and apply row styles where necessary
                    var dataItems = e.sender.dataSource.view();
                    for (var j = 0; j < dataItems.length; j++) {
                        var discontinued = dataItems[j].get("IsSuspended");

                        var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                        if (discontinued) {
                            row.addClass("LightGray");
                        }
                    }
                },
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        },
                        date: {
                         gt: "After (Excludes)",
                         lt: "Before (Includes)"
                     },
                    }
                },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100, 'All'],
                    buttonCount: 5
                },
                sortable: ({ field: "Name", dir: "desc" }),
                //toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><input ng-keyup="FranchisegridSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox form-control frm_controllead" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="FranchisegridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></script>').html()) }],

            };
        };

        srv.GetDeactivateReasons = function () {
            $http.get('/controlpanel/franchises/DeactivationReasons').then(function (response) {
                $scope.DeactivateReasons = response.data;
            });
        }

        srv.RenewFranchise = function (FranchiseId) {

            var Franchiseroyaltiesurl = '/controlpanel/franchises/getUnexpiredRoyalties/' + FranchiseId;
            $http.get(Franchiseroyaltiesurl).then(function (response) {
                srv.RoyaltiesCollection1 = response.data
            });
            srv.FranchiseID = FranchiseId;
        }

        srv.editFranchise = function (FranchiseId) {
            srv.FranchiseID = FranchiseId;
            $http.get('/controlpanel/franchises/DeactivationReasons').then(function (response) {
                srv.DeactivateReasons = response.data;
            });
        }



        srv.RemoveSuspended = function (franchiseId) {
            var reasonId = $('#cboRenewReason').val();
            var comments = $('#txtRenewMessage').val();

            $http.post('/controlpanel/franchises/removeSuspended/' + franchiseId + '/' + reasonId + '/' + comments).then(function () {
                srv.Get();
            });
        };

        srv.RemoveSuspended1 = function (franchiseId) {

            $("tr.clsRenew").each(function (i, tr) {
                $this = $(this);
                var value = $this.find("span.value").html();
                var quantity = $this.find("input.quantity").val();
            });

        };

    }])

.directive("datepicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function () {
                scope.$apply(function () {
                    ngModelCtrl.$modelValue = elem.val();
                });
            };
            elem.datepicker('setDate', new Date(attrs.value));
            elem.datepicker({
                //useCurrent: false,
                format: 'mm/dd/yyyy',
            });
            elem.on("change", function (e) {
                updateModel();
            });
        }
    }
})

.controller('FranchiseListController',
    ['$scope', 'HFCService', 'FranchiseListService', '$http',
    function ($scope, HFCService, FranchiseListService, $http) {

        var ctrl = this;
        $scope.FranchiseListService = FranchiseListService;

        $scope.HFCService = HFCService;

        $scope.impersonate = function (UserId) {

            HFC.CP.Impersonate(UserId);
        }

        $scope.Deactivatefran = {
            cbodeactivate: '',
            Message: '',
        }
        $scope.Deacsubmit = false;
        $scope.RenewSubmitted = false;

        FranchiseListService.GetMainGridOptions();

        $scope.FranchisegridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#grdkendoFranchises").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  {
                      field: "Name",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "Code",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "Address",
                      operator: "contains",
                      value: searchValue
                  }
                ]
            });

        }

        $scope.FranchisegridSearchClear = function () {
            $('#searchBox').val('');
            $("#grdkendoFranchises").data('kendoGrid').dataSource.filter({
            });
        }

        $scope.DeactivateFranchise = function (franchiseId, modal) {
            
            $scope.Deacsubmit = true;

            var dto = $scope.Deactivatefran;
            if (modal.DeacReson.$invalid) {
                // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
                return;
            }

            var DeactivationReasonId = dto.cbodeactivate.DeactivationReasonId;
            var DeactivationAdditionalReason = dto.Message;
            var model = {};
            model.DeactivationReasonId = DeactivationReasonId;
            model.FranchiseId = franchiseId;
            model.DeactivationAdditionalReason = DeactivationAdditionalReason;

            if (model.DeactivationReasonId != "" && model.DeactivationReasonId != "0" && model.DeactivationAdditionalReason != "" && model.DeactivationAdditionalReason != "null") {
                $http.post('/controlpanel/franchises/deactivateTP/', model).then(
                      function () {
                          HFC.DisplaySuccess("Deactivated Sucessfully");
                          $('#txtDeactivateMessage').val('');
                          $('#myModal').modal('hide');
                          $('#grdkendoFranchises').data('kendoGrid').dataSource.read();
                          $('#grdkendoFranchises').data('kendoGrid').refresh();

                      });

            }
            //else if ((model.DeactivationReasonId == "" || model.DeactivationReasonId == "0") && (model.DeactivationAdditionalReason == "" || model.DeactivationAdditionalReason == "null")) {
            //    HFC.DisplayAlert("Reason & Comment is required");
            //}
            //else if (model.DeactivationReasonId == "" || model.DeactivationReasonId == "0") {
            //    HFC.DisplayAlert("Reason is required");
            //}
            //else if (model.DeactivationAdditionalReason == "" || model.DeactivationAdditionalReason == "null") {
            //    HFC.DisplayAlert("Comment is required");
            //}
        }

        $scope.ReactivateFranchise = function (Royalties, modal) {

            $scope.RenewSubmitted = true;

            if (modal.form_RenewFranchisee.$invalid) {
                return;
            }

            if (Royalties.length == 0) {
                var id = FranchiseListService.FranchiseID;
                //$http.post('/controlpanel/franchises/0/ReActivateFranchise')

                $http.post('/controlpanel/franchises/ReActivateFranchise/' + id).then(function () {

                    $('#myModalRenew').modal('hide');
                    $('#grdkendoFranchises').data('kendoGrid').dataSource.read();
                    $('#grdkendoFranchises').data('kendoGrid').refresh();
                });
            }
            else {
                var FranchiseroyaltieRenewurl = '/controlpanel/franchises/removeSuspended';
                var data = Royalties;
                $http.post(FranchiseroyaltieRenewurl, data).then(function (response) {
                    $('#myModalRenew').modal('hide');
                    $('#grdkendoFranchises').data('kendoGrid').dataSource.read();
                    $('#grdkendoFranchises').data('kendoGrid').refresh();
                });
            }

        };
        $scope.tooltipOptions = {
            filter: "th",
            position: "top",
            hide: function (e) {
                this.content.parent().css('visibility', 'hidden');
            },
            show: function (e) {
                if (this.content.text().length > 1) {
                    this.content.parent().css('visibility', 'visible');
                } else {
                    this.content.parent().css('visibility', 'hidden');
                }
            },
            content: function (e) {
                var target = e.target.data().title; // element for which the tooltip is shown
                return target;//$(target).text();
            }
        };
        $scope.ClearDeactiveValues = function () {
            $('#cbodeactivate').val("");
            $('#txtDeactivateMessage').val("");
            $scope.Deacsubmit = false;
        }

        $scope.CancelRenew = function () {
            $scope.RenewSubmitted = false;
        }

    }
    ]);

})(window, document);