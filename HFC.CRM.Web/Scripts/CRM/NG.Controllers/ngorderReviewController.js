﻿
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.orderreview.list', ['hfc.order.service'])
        .controller('OrderReviewController', [
            '$http', '$scope', 'OrderService', function ($http, $scope, OrderService) {

                $scope.order = {};

                var pattern = /\/(\d+)(\/(\d+)\/?)?/g;
                var match = pattern.exec(window.location.pathname);
                var orderId = match && match.length > 1 ? parseInt(match[1]) : 0;

                if (orderId > 0) {
                    OrderService.getOrderReview(orderId, function (orderReview) {
                        $scope.order = orderReview;
                    });
                }

                $scope.toggle = function (item) {
                    item.visible = !item.visible;
                };

            }
        ]);

})(window, document);
