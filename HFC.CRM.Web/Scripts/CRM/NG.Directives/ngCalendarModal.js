﻿/**
 * hfc.calendar.service module  
 * a simple module for handling calendar
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.calendar.service', ['hfc.core.service'])

    .service('CalendarService', ['$http', 'HFCService', 'AddressService', function ($http, HFCService, AddressService) {
        var srv = this,
            numbers = [];

        for (var i = 1; i <= 31; i++)
            numbers.push(i);

        srv.IsBusy = false;
        srv.NumbersArray = numbers;
        srv.RelativeGroup = 'Day';
        srv.EndSelection = 'Never';
        srv.IncludeJobLead = false;
        srv.RecurrenceIsEnabled = false;
        srv.RecurrenceIsDisabled = false; //we may want to disable recurrence for modified split occurrence in series

        srv.CachedJobIds = [];
        srv.CalendarEvents = [];
        srv.Tasks = [];
        //srv.RecurringMasterCollection = [];
        srv.EditableEvent = null;
        srv.EventIndex = -1;
        
        srv.AppointmentTypeEnum = null;
        srv.RecurringPatternEnum = null;
        srv.DayOfWeekEnum = null; //this is a special day of week enum, since it can be used as flag the values are not sequential
        srv.DayOfWeekIndexEnum = null;
        srv.EventTypeEnum = null;
        function error(res) {
            srv.IsBusy = false;
            HFC.DisplayAlert(res.statusText);
        };

        function remove(arr, item) {
            var index = arr.indexOf(item);
            if (index > 0) {
                arr.splice(index, 1);
            }
            else if (index === 0) {
                arr.shift();
            }
        }


        srv.GetNextAppointment = function(jobId) {
            var filtered = [];
            for (var j = 0; j < srv.CalendarEvents.length; j++) {
                if (srv.CalendarEvents[j].JobId == jobId) {
                    filtered.push(srv.CalendarEvents[j]);
                }
            }

            if (filtered.length == 0) return "";

            var sorted = filtered.sort(function(a, b) {
                return Date.parse(a.start) - Date.parse(b.start);
            });

            var nextAppointment = false;
            for (var k = 0; k < sorted.length; k++) {
                if (Date.parse(sorted[k].start) > new Date()) {
                    nextAppointment = sorted[k];
                    break;
                }
            }

            return nextAppointment;
        }

        srv.Get = function (jobId) {
            if ($.inArray(jobId, srv.CachedJobIds) >= 0)
                return;

            srv.CachedJobIds.push(jobId);

            var data = {
                includeLookup: srv.AppointmentTypeEnum == null
            };
            if (jobId)
                data.JobIds = [jobId];
            
            $http.get('/api/calendar/', { params: data }).then(function (res) {
                srv.IsBusy = false;
                srv.AppointmentTypeEnum = res.data.AppointmentTypeEnum;
                //HFC.Lookup.ToJSFromJsn(window.lk_AppointmentTypeEnum,'Name','AppointmentTypeId')
                srv.RecurringPatternEnum = res.data.RecurringPatternEnum;
                srv.DayOfWeekEnum = res.data.DayOfWeekEnum;
                srv.DayOfWeekIndexEnum = res.data.DayOfWeekIndexEnum;
                srv.EventTypeEnum = res.data.EventTypeEnum;

                //srv.RecurMasterCollection = res.data.RecurringMaster;
                angular.forEach(res.data.Events, function (evt) {
                    var exist = $.grep(srv.CalendarEvents, function (e) {
                        return e.id == evt.id;
                    });
                    if (exist && exist.length == 0)
                        srv.CalendarEvents.push(evt);
                });
                angular.forEach(res.data.Tasks, function (task) {
                    var exist = $.grep(srv.Tasks, function (t) {
                        return t.id == task.id;
                    });
                    if (exist && exist.length == 0)
                        srv.Tasks.push(task);
                });

            }, error);
        };

        srv.NewApt = function (leadId, leadNumber, jobId, jobNumber, person, installAddressId) {

            var address = AddressService.GetAddress(installAddressId);
            var location = address.Address1 + ' ' + address.City + ', ' +  address.State + ' ' + address.ZipCode;
            
            var startDate = XDate.getRoundedDate(),
                endDate = startDate.clone().addMinutes(60),
                title = person ? ("Job #"+jobNumber+" - "+person.FullName) : "";
            var evt = {
                title: title,
                OrganizerPersonId: HFCService.CurrentUser.PersonId,
                LeadId: leadId,
                LeadNumber: leadNumber,
                JobId: jobId,
                JobNumber: jobNumber,
                ReminderMinute: 30,
                AptTypeEnum: srv.AppointmentTypeEnum.Appointment,
                EventTypeEnum: srv.EventTypeEnum.Single,
                start: startDate[0],
                end: endDate[0],
                editable: true,
                IsDeletable: true,
                Location: location
            }

            _setDefaults(evt, -1);
        };

        srv.NewTask = function (leadId, leadNumber, jobId, jobNumber, person) {
            //
            var title = person ? ("Job #" + jobNumber + " - " + person.FullName) : "";
            var evt = {
                title: title,
                OrganizerPersonId: HFCService.CurrentUser.PersonId,
                LeadId: leadId,
                LeadNumber: leadNumber,
                JobId: jobId,
                JobNumber: jobNumber,
                ReminderMinute: 30,
                editable: true,
                IsDeletable: true
            }

            _setDefaults(evt, -1);
        };

        function _setDefaults(clone, index) {
            if (typeof (clone.start) == 'string') {
                var xdate = new XDate(clone.start, true)[0];
                clone.start = new XDate(xdate.getUTCFullYear(), xdate.getUTCMonth(), xdate.getUTCDate(), xdate.getUTCHours(), xdate.getUTCMinutes(), xdate.getUTCSeconds());
            }

            if (typeof (clone.end) == 'string') {
                var xdate = new XDate(clone.end, true)[0];
                clone.end = new XDate(xdate.getUTCFullYear(), xdate.getUTCMonth(), xdate.getUTCDate(), xdate.getUTCHours(), xdate.getUTCMinutes(), xdate.getUTCSeconds());
            }

            srv.EndSelection = 'Never';
            srv.RelativeGroup = 'Day';
            srv.RecurrenceIsEnabled = false;
            srv.RecurrenceIsDisabled = false;
            if (clone.JobId || clone.LeadId)
                srv.IncludeJobLead = true;

            if (clone.EventRecurring) {
                srv.RecurrenceIsEnabled = true;
                if (clone.EventTypeEnum == srv.EventTypeEnum.Occurrence)
                    srv.RecurrenceIsDisabled = true;
                if (srv.EventRecurring.EndDate)
                    srv.EndSelection = 'EndsOn';
                else if (srv.EventRecurring.EndsAfterXOccurrences)
                    srv.EndSelection = 'After';

                if (clone.EventRecurring.DayOfWeekIndex)
                    srv.RelativeGroup = 'Relative';
            }

            srv.EditableEvent = clone;
            srv.EventIndex = index;
            $("#crm-evt-container").modal("show");
            setTimeout(function () {
                $("#subjectIpt").focus();
            }, 300)
        }

        srv.Edit = function (calEvent, index) {
            var clone = angular.copy(calEvent);
            _setDefaults(clone, index);
        };


        srv.ApptCancelled = function () {
            srv.IsBusy = true;
            $http.post('/api/calendar/' + srv.EditableEvent.id + '/ApptCancelled').then(function (res) {
                srv.IsBusy = false;
                $("#crm-evt-container").modal("hide");
                for (var i = 0; i < srv.CalendarEvents.length; i++) {
                    if (srv.EditableEvent.id == srv.CalendarEvents[i].id) {
                        srv.CalendarEvents[i].IsCancelled = true;
                    }
                }
            }, error);
        };
        
        srv.Save = function() {
            if (!srv.IsBusy) {
                    srv.IsBusy = true;
                    if (srv.EditableEvent.start > srv.EditableEvent.end) {
                        srv.DateRangeError = true;
                        return;
                    }
                
                    //HFCService.Users
                    //srv.EditableEvent.AdditionalPeople;
                    var notMatched = [];
                    for (var a = 0; a < srv.EditableEvent.AdditionalPeople.length; a++) {
                        var isExists = false;
                        for (var i = 0; i < HFCService.AllUsers.length; i++) {
                            if (HFCService.AllUsers[i].Email == srv.EditableEvent.AdditionalPeople[a]) {
                                isExists = true;
                                break;
                            }
                        }

                        if (isExists == false) {
                            notMatched.push(srv.EditableEvent.AdditionalPeople[a]);
                        }
                    }

                    var isAllExists = true;
                    for (var i = 0; i < notMatched.length; i++) {
                        var index = srv.EditableEvent.AdditionalPeople.indexOf(notMatched[i]);
                        srv.EditableEvent.AdditionalPeople.splice(index, 1);
                        isAllExists = false;
                    }

                    var payload = angular.copy(srv.EditableEvent);
                    payload.start = moment(payload.start.length > 0 ? payload.start[0] : payload.start).format('YYYY-MM-DD HH:mm:ss');
                    payload.end = moment(payload.end && payload.end.length > 0 ? payload.end[0] : payload.end).format('YYYY-MM-DD HH:mm:ss');

                    var promise = null,
                            url = '/api/tasks/';
                    if (payload.AptTypeEnum)
                        url = '/api/calendar/';
                    if (payload.id)
                        promise = $http.put(url + payload.id, payload);
                    else
                        promise = $http.post(url, payload);

                    if (isAllExists == false) {
                        $("#crm-evt-container").modal("hide");
                        bootbox.dialog({
                            closeButton: false,
                            message: HFC.AttendeeWarning,
                            title: "Warning",
                            buttons: {
                                main: {
                                    label: "Ok",
                                    className: "btn-default",
                                    callback: function () {
                                        srv.SaveSendToServer(promise, payload);
                                    }
                                }
                            }
                        });
                    }
                    else {
                        $("#crm-evt-container").modal("hide");
                        srv.SaveSendToServer(promise, payload);
                    }
                }
        };
        
        srv.SaveSendToServer = function (promise, payload) {
            promise.then(function (res) {
                srv.IsBusy = false;

                // lookup AssignedName
                var assignedPerson = $.grep(HFCService.Users, function (u) {
                    return u.PersonId == srv.EditableEvent.AssignedPersonId;
                });

                if (assignedPerson && assignedPerson.length > 0) {
                    srv.EditableEvent.AssignedName = assignedPerson[0].FullName;
                }

                if (!srv.EditableEvent.id && res.data.id != srv.EditableEvent.id)
                    srv.EditableEvent.id = res.data.id;

                if (res.data.calendar) {
                    srv.EditableEvent.AdditionalPeople = [];
                    for (var l = 0; l < res.data.calendar.Attendees.length; l++) {
                        srv.EditableEvent.AdditionalPeople.push(res.data.calendar.Attendees[l].PersonEmail);
                    }

                    srv.EditableEvent.RevisionSequence = res.data.calendar.RevisionSequence;

                } else if (res.data.task) {
                    srv.EditableEvent.AdditionalPeople = [];
                    for (var l = 0; l < res.data.task.Attendees.length; l++) {
                        srv.EditableEvent.AdditionalPeople.push(res.data.task.Attendees[l].PersonEmail);
                    }

                    srv.EditableEvent.RevisionSequence = res.data.task.RevisionSequence;
                }

                if (srv.EditableEvent.AptTypeEnum) {
                    if (srv.EventIndex >= 0) {
                        for (var j = 0; j < srv.CalendarEvents.length; j++) {
                            if (srv.CalendarEvents[j].id == srv.EditableEvent.id) {
                                srv.CalendarEvents[j] = srv.EditableEvent;
                            }
                        }
                    } else
                        srv.CalendarEvents.push(srv.EditableEvent);
                } else {
                    if (srv.EventIndex >= 0) {
                        for (var k = 0; k < srv.Tasks.length; k++) {
                            if (srv.Tasks[k].id == srv.EditableEvent.id) {
                                srv.Tasks[k] = srv.EditableEvent;
                            }
                        }
                    } else
                        srv.Tasks.push(srv.EditableEvent);
                }
            }, error);
        }

        srv.DeleteApt = function (apt) {
            if (!srv.IsBusy && apt.id && confirm("Do you want to continue?")) {
                $http.delete('/api/calendar/' + apt.id).then(function(res) {
                    srv.IsBusy = false;

                    remove(srv.CalendarEvents, apt);
                }, error);
            }
        };

        srv.DeleteTask = function (task) {
            if (!srv.IsBusy && task.id && confirm("Do you want to continue?")) {
                $http.delete('/api/tasks/' + task.id).then(function(res) {
                    srv.IsBusy = false;

                    remove(srv.Tasks, task);
                }, error);
            }
        };

        function lookupFullnameByEmail(email) {
            var users = $.grep(HFCService.getUsers(email), function(u) {
                return u.Email == email;
            });

            if (users && users.length > 0) {
                return users[0].FullName;
            }

            //console.log('not found user with email: ' + email);
            return "";
        };

        function lookupColorIdById(id) {
            var users = $.grep(HFCService.Users, function (u) {
                return u.PersonId == id;
            });

            if (users && users.length > 0) {
                return users[0].ColorId;
            }

            return "";
        };

        srv.GetTaskAttendees = function(task) {
                var result = '';

                for (var j = 0; j < task.AdditionalPeople.length; j++) {
                    var fullname = lookupFullnameByEmail(task.AdditionalPeople[j]);

                    if (result.length > 2 && result[result.length - 2] != ',') {
                        result = result + ', ';
                    }

                    if (fullname == task.AssignedName) {
                        result = '<strong>' + fullname + '</strong>, ' + result;
                    } else {
                        result = result + (lookupFullnameByEmail(task.AdditionalPeople[j]));
                    }
                }

                if (result.length > 2 && result[result.length - 2] == ',') {
                   result =  result.substring(0, result.length - 2);
                }

                return result;
        };

        srv.GetClassName = function(task) {
            var color = lookupColorIdById(task.AssignedPersonId);
            return "color-" + color;
        }

        srv.SetCompletedDate = function () {
            srv.EditableEvent.CompletedDate = srv.EditableEvent.CompletedDate ? null : new Date();
        }

        srv.GetEventTypeGlyph = function (evt) {
            if (evt.EventTypeEnum == srv.EventTypeEnum.Series)
                return "glyphicon glyphicon-repeat";
            else if (evt.EventTypeEnum == srv.EventTypeEnum.Occurrence)
                return "glyphicon glyphicon-record";
            else
                return "";
        };

        srv.GetAptTypeGlyph = function(evt) {
                if (!evt) {
                    return "";
                }

                return HFC.getAptGlyphIcon(evt.AptTypeEnum);


            };

        srv.GetDateRange = function (evt) {
            var startDate = new XDate(evt.start, true),
                endDate = new XDate(evt.end, true),
                rangeText = "";

            if (startDate.valid()) {
                if (typeof evt.start !== 'string') {
                    startDate.setHours(evt.start.getHours());
                }
                if (endDate.valid()) {
                    if (typeof evt.end !== 'string') {
                        endDate.setHours(evt.end.getHours());
                    }
                    var minDiff = startDate.diffMinutes(endDate),
                        start = "",
                        end = "";

                    if (minDiff <= 1440) {//both are happening within same day
                        rangeText = startDate.toFriendlyString();
                        if (!evt.allDay)
                            rangeText += ", " + startDate.toString("h:mm tt") + " to " + endDate.toString("h:mm tt");
                    } else {
                        start = startDate.toFriendlyString(!evt.allDay);
                        end = endDate.toFriendlyString(!evt.allDay);
                        if (start == end)
                            rangeText = start;
                        else
                            rangeText = start + " to " + end;
                    }
                } else
                    rangeText = startDate.toFriendlyString(!evt.allDay);
            }

            return rangeText;
        }
    }])

    .controller('CalendarController', ['$scope', 'HFCService', 'CalendarService', function ($scope, HFCService, CalendarService) {

        $scope.CalendarService = CalendarService;
        $scope.HFCService = HFCService;
        $scope.$watch('CalendarService.EditableEvent.start', function (newVal, oldVal) {
            if (newVal) {
                if ($scope.CalendarService.EditableEvent.end < newVal) {
                    $scope.CalendarService.EditableEvent.end = newVal;
                }
            }
        });

        $scope.$watch('CalendarService.EditableEvent.end', function (newVal, oldVal) {
            if (newVal) {
                if ($scope.CalendarService.EditableEvent.start > newVal) {
                    $scope.CalendarService.EditableEvent.start = newVal;
                }
            }
        });

        $scope.$watch('CalendarService.EditableEvent.AssignedPersonId', function (newVal, oldVal, $scope) {
            if (oldVal != undefined) {
                for (var i = 0; i < $scope.HFCService.Users.length; i++) {
                    if ($scope.HFCService.Users[i].PersonId == oldVal) {
                        var index = $scope.CalendarService.EditableEvent.AdditionalPeople.indexOf($scope.HFCService.Users[i].Email);
                        if (index != -1) {
                            $scope.CalendarService.EditableEvent.AdditionalPeople.splice(index, 1);
                        }
                    }
                }

                for (var i = 0; i < $scope.HFCService.Users.length; i++) {
                    if ($scope.HFCService.Users[i].PersonId == newVal) {
                        var index = $scope.CalendarService.EditableEvent.AdditionalPeople.indexOf($scope.HFCService.Users[i].Email);
                        if (index == -1) {
                            $scope.CalendarService.EditableEvent.AdditionalPeople.push($scope.HFCService.Users[i].Email);
                        }
                    }
                }
            }
        });

        $scope.$watch('CalendarService.EditableEvent.AdditionalPeople', function (newVal, oldVal, $scope) {
            if (oldVal != undefined) {
                for (var i = 0; i < $scope.HFCService.Users.length; i++) {
                    if ($scope.HFCService.Users[i].PersonId == $scope.CalendarService.EditableEvent.AssignedPersonId) {
                        var index = $scope.CalendarService.EditableEvent.AdditionalPeople.indexOf($scope.HFCService.Users[i].Email);
                        if (index == -1) {
                            $scope.CalendarService.EditableEvent.AdditionalPeople.push($scope.HFCService.Users[i].Email);
                        }
                    }
                }
            }
        });
        
        var tags = $.map(HFCService.Users, function (usr) {
            var text = usr.FullName || usr.Email;
            if (usr.FullName)
                text += " <" + usr.Email + ">";
            return { id: usr.Email, text: text, FullName: usr.FullName }
        });
        $scope.SelectOption = {
            multiple: true,
            simple_tags: true,
            width: '100%',
            placeholder: "Attendees",
            tokenSeparators: [",", " ", ";"],
            maximumSelectionSize: 10,
            maximumInputLength: 256,
            minimumInputLength: 1,
            tags: tags,
            formatSelection: function (email) {
                return "<span title='" + email.id + "'>" + (email.FullName || email.id) + "</span>";
            },
            createSearchChoice: function (term) {
                if (HFC.Regex.EmailPattern.test(term))
                    return { id: term, text: term };
                else
                    return null;
            },
            query: function (query) {
                var data = { results: [] };
                $.each(tags, function () {
                    if (query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0) {
                        data.results.push(this);
                    }
                });
                return query.callback(data);
            }
        };

    }])

    .directive('hfcCalendarModal', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            controller: 'CalendarController',
            templateUrl: '/templates/NG/Calendar/calendar-modal.html?cache-bust=' + cacheBustSuffix
        }
    }]);

})(window, document);

