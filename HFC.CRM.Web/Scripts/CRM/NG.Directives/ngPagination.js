﻿/**
 * angular-strap ui.bootstrap.pagination module 
 * with some modifications
 * added select for page size and displaying record numbers
 * @version v2.0.3 - 2014-05-30
 * @link http://mgcrea.github.io/angular-strap
 * @author Olivier Louvignes (olivier@mg-crea.com)
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function(window, document, undefined) {
    'use strict';

    angular.module('ui.bootstrap.pagination', [])

    .run([
        '$templateCache',
        function ($templateCache) {
            //pagination tpl
            $templateCache.put('template/pagination/pagination.html', '<div class="row"><div class="col-xs-12"><ul class="pagination pull-left"><li ng-if="boundaryLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(1)">{{getText(\'first\')}}</a></li><li ng-if="directionLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}"><a href ng-click="selectPage(page.number)">{{page.text}}</a></li><li ng-if="directionLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li><li ng-if="boundaryLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(totalPages)">{{getText(\'last\')}}</a></li></ul> &nbsp; <select class="form-control form-control-select pagination" ng-change="onChange" ng-disabled="disablePageSize" ng-model="itemsPerPage" ng-init="itemsPerPage = 25" ng-options="num for num in [25,50,75,100]"></select> <span ng-show="showRecordCount">{{startRecordCount()}} - {{endRecordCount()}} out of {{totalItems}}</span></div></div>');
            //pager tpl
            $templateCache.put('template/pagination/pager.html', '<ul class="pager"><li ng-class="{disabled: noPrevious(), previous: align}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li><li ng-class="{disabled: noNext(), next: align}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li></ul>');
        }
    ])

    .controller('PaginationController', ['$scope', '$attrs', '$parse', function ($scope, $attrs, $parse) {
        var self = this,
            ngModelCtrl = { $setViewValue: angular.noop }, // nullModelCtrl
            setNumPages = $attrs.numPages ? $parse($attrs.numPages).assign : angular.noop;

        self.init = function (ngModelCtrl_, config) {
            ngModelCtrl = ngModelCtrl_;
            this.config = config;

            ngModelCtrl.$render = function () {
                self.render();
            };
            if ($attrs.showRecordCount)
                $scope.showRecordCount = $attrs.showRecordCount;
            else
                $scope.showRecordCount = config.showRecordCount;
            if ($attrs.disablePageSize)
                $scope.disablePageSize = $attrs.disablePageSize;
            else
                $scope.disablePageSize = config.disablePageSize;
            //if ($attrs.itemsPerPage) {
            //    //$scope.$parent.$watch($parse($attrs.itemsPerPage), function (value) {
            //    //    $scope.itemsPerPage = parseInt(value, 10);
            //    //    $scope.totalPages = self.calculateTotalPages();
            //    //});
            //    $scope.itemsPerPage = $attrs.itemsPerPage;
            //} else {
            //    $scope.itemsPerPage = config.itemsPerPage;
            //}
        };

        self.calculateTotalPages = function () {
            var totalPages = $scope.itemsPerPage < 1 ? 1 : Math.ceil($scope.totalItems / $scope.itemsPerPage);
            return Math.max(totalPages || 0, 1);
        };

        self.render = function () {
            $scope.page = parseInt(ngModelCtrl.$viewValue.page, 10) || 1;
        };

        $scope.startRecordCount = function () {
            if ($scope.totalItems)
                return (($scope.page - 1) * $scope.itemsPerPage) + 1;
            else
                return 0;
        };

        $scope.endRecordCount = function () {
            if ($scope.totalItems) {
                var num = $scope.page * $scope.itemsPerPage;
                return num > $scope.totalItems ? $scope.totalItems : num;
            } else
                return 0;
        };

        $scope.selectPage = function (page) {
            if (($scope.page !== page || self.itemsPerPage !== $scope.itemsPerPage) && page > 0 && page <= $scope.totalPages) {
                ngModelCtrl.$setViewValue({ page: page, size: $scope.itemsPerPage });
                ngModelCtrl.$render();
                self.itemsPerPage = $scope.itemsPerPage;
            }
        };

        $scope.getText = function (key) {
            return $scope[key + 'Text'] || self.config[key + 'Text'];
        };
        $scope.noPrevious = function () {
            return $scope.page === 1;
        };
        $scope.noNext = function () {
            return $scope.page === $scope.totalPages;
        };

        $scope.$watchCollection('[itemsPerPage, totalItems]', function () {
            $scope.totalPages = self.calculateTotalPages();
        });
        
        $scope.$watch('totalPages', function (value) {
            setNumPages($scope.$parent, value); // Readonly variable

            if ($scope.page > value) {
                $scope.selectPage(value);
            } else {
                ngModelCtrl.$render();
                $scope.selectPage($scope.page);
            }
        });
    }])

    .constant('paginationConfig', {
        itemsPerPage: 25,
        maxSize: 10,
        showRecordCount: true,
        disablePageSize: false,
        boundaryLinks: false,
        directionLinks: true,
        firstText: 'First',
        previousText: 'Prev',
        nextText: 'Next',
        lastText: 'Last',
        rotate: true
    })

    .directive('pagination', ['$parse', 'paginationConfig', function ($parse, paginationConfig) {
        return {
            restrict: 'EA',
            scope: {
                totalItems: '=',
                firstText: '@',
                previousText: '@',
                nextText: '@',
                lastText: '@'
            },
            require: ['pagination', '?ngModel'],
            controller: 'PaginationController',
            templateUrl: 'template/pagination/pagination.html',
            replace: true,
            link: function (scope, element, attrs, ctrls) {
                var paginationCtrl = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                var isMobile = /(ip(a|o)d|iphone|android)/gi.test(window.navigator.userAgent);

                // Setup configuration parameters
                var maxSize = angular.isDefined(attrs.maxSize) ? scope.$parent.$eval(attrs.maxSize) : paginationConfig.maxSize,
                    rotate = angular.isDefined(attrs.rotate) ? scope.$parent.$eval(attrs.rotate) : paginationConfig.rotate;
                scope.boundaryLinks = angular.isDefined(attrs.boundaryLinks) ? scope.$parent.$eval(attrs.boundaryLinks) : paginationConfig.boundaryLinks;
                scope.directionLinks = angular.isDefined(attrs.directionLinks) ? scope.$parent.$eval(attrs.directionLinks) : paginationConfig.directionLinks;
                if (isMobile) {
                    maxSize = 4;
                    scope.boundaryLinks = false;
                }

                paginationCtrl.init(ngModelCtrl, paginationConfig);

                //if (attrs.maxSize) {
                //    scope.$parent.$watch($parse(attrs.maxSize), function (value) {
                //        maxSize = parseInt(value, 10);
                //        paginationCtrl.render();
                //    });
                //}

                // Create page object used in template
                function makePage(number, text, isActive) {
                    return {
                        number: number,
                        text: text,
                        active: isActive
                    };
                }

                function getPages(currentPage, totalPages) {
                    var pages = [];

                    // Default page limits
                    var startPage = 1, endPage = totalPages;
                    var isMaxSized = (angular.isDefined(maxSize) && maxSize < totalPages);

                    // recompute if maxSize
                    if (isMaxSized) {
                        if (rotate) {
                            // Current page is displayed in the middle of the visible ones
                            startPage = Math.max(currentPage - Math.floor(maxSize / 2), 1);
                            endPage = startPage + maxSize - 1;

                            // Adjust if limit is exceeded
                            if (endPage > totalPages) {
                                endPage = totalPages;
                                startPage = endPage - maxSize + 1;
                            }
                        } else {
                            // Visible pages are paginated with maxSize
                            startPage = ((Math.ceil(currentPage / maxSize) - 1) * maxSize) + 1;

                            // Adjust last page if limit is exceeded
                            endPage = Math.min(startPage + maxSize - 1, totalPages);
                        }
                    }

                    // Add page number links
                    for (var number = startPage; number <= endPage; number++) {
                        var page = makePage(number, number, number === currentPage);
                        pages.push(page);
                    }

                    // Add links to move between page sets
                    if (isMaxSized && !rotate) {
                        if (startPage > 1) {
                            var previousPageSet = makePage(startPage - 1, '...', false);
                            pages.unshift(previousPageSet);
                        }

                        if (endPage < totalPages) {
                            var nextPageSet = makePage(endPage + 1, '...', false);
                            pages.push(nextPageSet);
                        }
                    }

                    return pages;
                }

                var originalRender = paginationCtrl.render;
                paginationCtrl.render = function () {
                    originalRender();
                    if (scope.page > 0 && scope.page <= scope.totalPages) {
                        scope.pages = getPages(scope.page, scope.totalPages);
                    }
                };
            }
        };
    }])

    .constant('pagerConfig', {
        itemsPerPage: 25,
        previousText: '« Previous',
        nextText: 'Next »',
        align: true
    })

    .directive('pager', ['pagerConfig', 'cacheBustSuffix', function (pagerConfig, cacheBustSuffix) {
        return {
            restrict: 'EA',
            scope: {
                totalItems: '=',
                previousText: '@',
                nextText: '@'
            },
            require: ['pager', '?ngModel'],
            controller: 'PaginationController',
            templateUrl: 'template/pagination/pager.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function (scope, element, attrs, ctrls) {
                var paginationCtrl = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                scope.align = angular.isDefined(attrs.align) ? scope.$parent.$eval(attrs.align) : pagerConfig.align;
                paginationCtrl.init(ngModelCtrl, pagerConfig);
            }
        };
    }]);

})(window, document);