﻿/**
 * hfc.hoverintent module  
 * a simple directive for hover intent, pulled from https://github.com/angular-ui/ui-utils/issues/106
 * @author marklagendijk
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.hoverintent', [])
    .directive('hoverIntent', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                var hoverIntentPromise;
                element.bind('mouseenter', function (event) {
                    var delay = scope.$eval(attributes.hoverIntentDelay);
                    if (delay === undefined) {
                        delay = 500;
                    }

                    hoverIntentPromise = $timeout(function () {
                        scope.$eval(attributes.hoverIntent, { $event: event });
                    }, delay);
                });
                element.bind('mouseleave', function () {
                    $timeout.cancel(hoverIntentPromise);
                });
            }
        };
    }]);

})(window, document);