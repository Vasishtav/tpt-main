﻿/**
 * An AngularJS directive for Dropzone.js, http://www.dropzonejs.com/
 * 
 * Usage:
 * 
 * <div ng-app="app" ng-controller="SomeCtrl">
 *   <button dropzone="dropzoneConfig">
 *     Drag and drop files here or click to upload
 *   </button>
 * </div>
 */

angular.module('ui.dropzone', [])

    .service('DropzoneService', function () {
        var srv = this;

        srv.Dropzones = [];
    })

    .directive('dropzone', ['DropzoneService', function (DropzoneService) {
        return function (scope, element, attrs) {
            var config;

            config = scope[attrs.dropzone];

            // Touchpoint: When consumed from the popupup "scope" is not containing the attribute.
            if (!config) {
                config = scope.$parent[attrs.dropzone];
            }

            // create a Dropzone for the element with the given options
            var dropzone = new Dropzone(element[0], config.options);
            
            // bind the given event handlers
            for(var event in config.eventHandlers) {
                dropzone.on(event, config.eventHandlers[event]);
            };

            //we will require an id for each dropzone so we can retrieve it later to insert existing files
            DropzoneService.Dropzones.push({ id: attrs.id || "", dropzone: dropzone });
        };
    }]);
