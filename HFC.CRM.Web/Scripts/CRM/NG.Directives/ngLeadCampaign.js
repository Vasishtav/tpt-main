﻿/**lead-campaign
 * hfc.person module  
 * a simple directive for creating a person DOM
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.leadcampaign', [])

    .service('LeadCampaignService', ['$http', function ($http) {
        var srv = this;

        srv.IsBusy = false;
        srv.Sources = [];
        srv.Campaigns = [];
        
        srv.LeadCampaigns = [];

        srv.GetSources = function () {
            var arr = [];
            if (srv.Sources) {
                var parents = $.grep(srv.Sources, function (stat) {
                    return !stat.ParentId;
                });

                angular.forEach(parents, function (stat) {
                    arr.push(stat);
                    var child = $.grep(srv.Sources, function (child) {
                        return child.ParentId == stat.Id && child.MarketingCampaigns && child.MarketingCampaigns.length > 0;
                    });
                    if (child)
                        arr = arr.concat(child);
                });
            }
            return arr;
        };

        srv.GetCampaigns = function (sourceId) {
            var arr = [];
            if (srv.Campaigns) {
                arr = $.grep(srv.Campaigns, function (stat) {
                    return stat.SourceId == sourceId;
                });
            }
            return arr;
        };

        srv.GetSource = function (srcId) {
            var src = $.grep(srv.Sources, function (stat) {
                return stat.Id == srcId;
            });
            if (src)
                return src[0];
            else
                return null;
        };

        srv.GetLabel = function (srcId, excludeMargin) {
            var src = srv.GetSource(srcId);
            if (!src)
                return "";

            var parent = null;
            if (src.ParentId)
                parent = srv.GetSource(src.ParentId);
            if (parent) 
                return '<span' + (excludeMargin ? '' : ' class="left-margin"') +'>' + src.Name + ' <span class="text-muted">' + parent.Name + '</span></span>';
            else
                return src.Name;
        };

        srv.Add = function (leadId) {
            srv.LeadCampaigns.push({                
                LeadId: leadId,
                SourceId: null,
                CampaignId: null,
                IsManuallyAdded: true,
                
            });
        };

        srv.Delete = function (src) {
            if (srv.IsBusy)
                return false;

            if (src.LeadSourceId) {
                srv.IsBusy = true;
                $http.delete('/api/leads/' + src.LeadSourceId + '/campaign/').then(function (response) {
                    remove(src);
                    srv.IsBusy = false;
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            } else
                remove(src);
        };

        srv.Update = function (src) {

            if (!src.SourceId || !src.CampaignId) return;

            if (srv.IsBusy)
                return false;

            if (src.LeadSourceId && src.OriginalSourceId == src.SourceId.id) {
                return false;
            }

            srv.IsBusy = true;
            var promise = null;
            
            src.OriginalSourceId = src.SourceId.id;
            if (src.LeadSourceId) {
                promise = $http.put('/api/leads/' + src.LeadSourceId + '/campaign/', { SourceId: src.SourceId.id });
            } else {
                promise = $http.post('/api/leads/' + src.LeadId + '/campaign/', { LeadId: src.LeadId, SourceId: src.SourceId.id });
            }

            promise.then(function (response) {
                if (!src.LeadSourceId && src.LeadSourceId != response.data.LeadSourceId)
                    src.LeadSourceId = response.data.LeadSourceId;
                HFC.DisplaySuccess("Lead Source saved");
                srv.IsBusy = false;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        };

        srv.Get = function (leadId) {
            if (leadId) {
                $http.get('/api/leads/' + leadId+'/source/').then(function (response) {
                    srv.Sources = response.data.Sources;
                    srv.Campaigns = response.data.Campaigns;
                    srv.LeadSources = response.data.LeadSources;

                    angular.forEach(srv.LeadSources, function (stat) {
                        stat.OriginalSourceId = stat.SourceId;
                    });

                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
        }

        srv.ChangeSource = function (source) {
            if (!source || !source.SourceId) return;
            source.CampaignId = null;
        }

        function remove(item) {
            var index = srv.LeadSources.indexOf(item);
            if (index > 0) {
                srv.LeadSources.splice(index, 1);
            }
            else if (index === 0) {
                srv.LeadSources.shift();
            }
        }

    }])

    .directive('hfcLeadCampaign', ['LeadCampaignService', function (LeadCampaignService) {
        return {
            restrict: 'EA',
            scope: {
                leadId: '@',
                heading: '@'
            },
            templateUrl: '/templates/NG/Lead/lead-campaign.html',
            replace: true,
            link: function (scope, element, attrs) {
                scope.LeadCampaignService = LeadCampaignService;


                /*scope.GetSourceSelectOption = function(leadSource) {
                    return {
                        leadSource: leadSource,
                        width: '150px',
                        placeholder: 'Select a source',
                        formatResult: function(item) {
                            var label = scope.LeadCampaignService.GetLabel(item.id);
                            return label;
                        },

                        data: function () {
                            var arr = scope.LeadCampaignService.GetSources();
                            var data = [];
                            $.each(arr, function() {
                                data.push({ id: this.Id, text: this.Name });
                            });
                            return {
                                results: data
                            }
                        },
                    }
                };*/

                scope.GetCampaignSelectOption = function(leadSource) {
                    return {
                        leadSource: leadSource,
                        width: '120px',
                        placeholder: 'Select campaign',
                        formatResult: function(item) {
                            var label =item.text;
                            return label;
                        },


                        data: function() {
                            var arr = scope.LeadCampaignService.GetCampaigns(leadSource.SourceId.id);
                            var data = [];
                            $.each(arr, function() {
                                data.push({ id: this.MktCampaignId, text: this.Label });
                            });
                            return {
                                results: data
                            }
                        },
                    };
                }
                
                // Setup configuration parameters
                scope.heading = scope.heading || "Lead Campaigns";
                if (attrs.canUpdate)
                    scope.canUpdate = attrs.canUpdate === 'true';
                if (attrs.canDelete)
                    scope.canDelete = attrs.canDelete === 'true';
                if (attrs.canCreate)
                    scope.canCreate = attrs.canCreate === 'true';
            }
        };
    }]);

})(window, document);