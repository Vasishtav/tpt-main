﻿/**
 * hfc.person module  
 * a simple directive for creating a person DOM
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.leadsource', ['hfc.core.service', 'hfc.cache'])

    .service('LeadSourceService', ['$http', 'CacheService', function ($http, CacheService) {
        var srv = this;

        srv.IsBusy = false;

        srv.SelectedSources = [];

        srv.Sources = [];

        srv.LeadSources = [];

        srv.EditorSelection = [];

        srv.LeadId = false;

        srv.GetLeadSources = function (jobCount) {
            
            var result = [];
            result.push({ SourceId: 9999 });

            if (jobCount > 1) {
                var RepeatID = 0;
                RepeatID = (HFC.Util.BrandedPath != "/brand/tl") ? 22 : 21; /// The Repeat SourceID on TL is 21, on BB is 22;
                if (srv.LeadSources.map(function (e) { return e.SourceId; }).indexOf(RepeatID) == -1) {
                    result.push({ SourceId: RepeatID });
                }
            }
            for (var i = 0; i < srv.LeadSources.length; i++) {
                result.push(srv.LeadSources[i]);
            }
            return result;
        };

        srv.GetSource = function (sourceId, sources, path) {
            if (!sources || sources.length == 0) {
                return false;
            }
            
            for (var i = 0; i < sources.length; i++) {
                if (sources[i].SourceId == sourceId) {
                    sources[i].path = path;
                    return sources[i];
                } else {

                    if (sources[i].Children && sources[i].Children.length > 0) {
                        var result = srv.GetSource(sourceId, sources[i].Children, path + sources[i].Name + " > ");
                        if (result) {
                            return result;
                        }
                    }
                }
            }

            return false;
        };

        srv.GetLabel = function (srcId, excludeMargin) {
            var path = "";
            var src = srv.GetSource(srcId, srv.Sources, path);
            return src;
        };

      srv.AddSource = function (leadId) {
            srv.LeadSources.push({                
                LeadId: leadId,
                SourceId: null,
                IsManuallyAdded: true
            });
        };

        srv.Delete = function (src) {
            if (!srv.LeadId) {
                remove(src);
                return;
            };

            if (srv.IsBusy)
                return false;
             
            var exists = srv.CheckIfSourceUsed(src.SourceId);
            if (exists) {
                HFC.DisplayAlert("Source selected to remove is already used in a job for this lead");
                return false;
            }

            if (src.LeadSourceId) {
                srv.IsBusy = true;
                $http.delete('/api/leads/' + src.LeadSourceId + '/source/').then(function (response) {
                    remove(src);
                    srv.IsBusy = false;
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            } else
                remove(src);
        };

        srv.Update = function (sources) {
            if (!srv.LeadId) return;

            if (srv.IsBusy)
                return false;

            srv.IsBusy = true;
            
            $http.post('/api/leads/' + srv.LeadId + '/source/', sources).then(function (response) {
                HFC.DisplaySuccess("Lead Source(s) saved");
                srv.IsBusy = false;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        }
        
        srv.SelectSource = function(node) {
            if (node.Selected) { // select
                    srv.EditorSelection.push(node.SourceId);
                } else {

                    for (var i = 0; i < srv.EditorSelection.length; i++) {
                        if (srv.EditorSelection[i] == node.SourceId) {
                            srv.EditorSelection.splice(i, 1);
                        }
                    }
                }

                console.log(srv.EditorSelection);
            }
        
       srv.GetEditorData = function(data) {
           var filter = function(nodes) {
               var result = [];
               if (!nodes || nodes.length == 0) return [];
               for (var i = 0; i < nodes.length; i++) {
                   if (!nodes[i].isDeleted) {
                       var n = nodes[i];
                       if (n.Children) {
                           n.Children = filter(n.Children);
                       }

                       result.push(n);
                   }
               }

               return result;
           }

           return filter(data);
       }

       srv.Get = function (leadId) {
           if (/*leadId && */!srv.IsBusy && !srv.LeadId) {
                    srv.LeadId = leadId;
                    srv.IsBusy = true;
                    $http.get('/api/leads/' + leadId + '/source/').then(function (response) {

                        srv.Sources = [];

                        CacheService.Get('Source', function (items) {
                            srv.Sources = items;
                        });

                        srv.LeadSources = response.data.LeadSources;
                        srv.GetSelectedSources(leadId);
                        srv.IsBusy = false;
                    }, function(response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }
       }

       srv.GetSelectedSources = function (leadId) {
           //Selected Sources 
           srv.IsBusy = true;
           $http.get('/api/leads/' + leadId + '/SelectedSources/').then(function (response) {
               
               srv.SelectedSources = [];
               srv.EditorData = false;
               CacheService.Get('SelectedSources', function (items) {
                   srv.SelectedSources = items;
               });

               srv.SelectedSources = response.data.SelectedSources;
           }, function (response) {
               HFC.DisplayAlert(response.statusText);
           });
           srv.IsBusy = false;
       }


       srv.CheckIfSourceUsed = function (sourceId) {

           if (srv.SelectedSources.length > 0) {
               var found = false;
               for (var i = 0; i <= srv.SelectedSources.length - 1; i++) {
                   if (srv.SelectedSources[i] == sourceId) {
                       found = true;
                   }
               }
               return found;
           }
           else {
               return false;
           }
       }

       srv.ShowEditor = function (modalId, index, $event) {
           if ($event) $event.preventDefault();
           
           srv.EditorSelection = [];

           for (var i = 0; i < srv.LeadSources.length; i++) {
               srv.EditorSelection.push(srv.LeadSources[i].SourceId);
           }

           if (!srv.EditorData) {
               srv.EditorData = srv.GetEditorData(srv.Sources);
           }

           srv.EditorDataExpandedNodes = [];

           restoreSelection(srv.EditorData);

           $("#" + modalId).modal("show");
        };

       srv.CancelEditor = function (modalId) {
            $("#" + modalId).modal("hide");
        }

       var restoreSelection = function (nodes) {
            if (!nodes || nodes.length == 0) {
                return;
            }
           for (var i = 0; i < nodes.length; i++) {
               restoreSelection(nodes[i].Children);
               if (srv.EditorSelection.indexOf(nodes[i].SourceId) > -1) {
                   nodes[i].Selected = true;
                   expandPath(nodes[i]);
               } else {
                   nodes[i].Selected = false;
               }
           }
        };

       var expandPath = function(node) {
            srv.EditorDataExpandedNodes.push(node);

            if (node.ParentId) {
                var parent = srv.GetSource(node.ParentId, srv.Sources);
                expandPath(parent);
            }
       }

       var resetSelection = function(node) {
                node.Selected = false;
                if (!node.Children) {
                    return;
                }

                for (var i = 0; i < node.Children.length; i++) {
                    resetSelection(node.Children[i]);
                }
            }

            srv.SaveEditor = function (modalId) {
            

            for (var i = 0; i < srv.EditorData.length; i++) {
                resetSelection(srv.EditorData[i]);
            };

            srv.LeadSources = [];

            var selectedSources = [];

           for (var j = 0; j < srv.EditorSelection.length; j++) {
               selectedSources.push(srv.GetSource(srv.EditorSelection[j], srv.Sources));
           }

           var sourcesToUnselect = [];

            for (var j = 0; j < selectedSources.length; j++) {
                if (selectedSources[j].ParentId > 0) {
                    var selectedParent = $.grep(selectedSources, function (em) {
                        return em.SourceId == selectedSources[j].ParentId;
                    });

                    if (selectedParent && selectedParent.length > 0) {
                        sourcesToUnselect.push(selectedParent[0].SourceId);
                    }
                }
            }

           if (sourcesToUnselect.length > 0) {
               for (var k = 0; k < sourcesToUnselect.length; k++) {
                   var index = srv.EditorSelection.indexOf(sourcesToUnselect[k]);
                   if (index >= 0) srv.EditorSelection.splice(index, 1);
               }
           }

           srv.Update(srv.EditorSelection);

           for (var j = 0; j < srv.EditorSelection.length; j++) {
               srv.LeadSources.push({ LeadId: srv.LeadId, SourceId: srv.EditorSelection[j] });
           }

           $("#" + modalId).modal("hide");
        }

       function remove(item) {
            var index = srv.LeadSources.indexOf(item);
            if (index > 0) {
                srv.LeadSources.splice(index, 1);
            }
            else if (index === 0) {
                srv.LeadSources.shift();
            }
        }

    }])

    .directive('hfcLeadSourceModal', ['LeadSourceService', 'cacheBustSuffix', function (LeadSourceService, cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {
                modalId: '@',
                leadId: '@'
            },
            templateUrl: '/templates/NG/Lead/leadSource-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function (scope) {
                scope.LeadSourceService = LeadSourceService;
            }
        }
    }])

    .directive('hfcLeadSource', ['LeadSourceService', 'cacheBustSuffix', function (LeadSourceService, cacheBustSuffix) {
        return {
            restrict: 'EA',
            scope: {
                leadId: '@',
                heading: '@'
            },
            templateUrl: '/templates/NG/Lead/lead-source.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function (scope, element, attrs) {
                scope.LeadSourceService = LeadSourceService;


                // Setup configuration parameters
                scope.heading = scope.heading || "Lead Sources";
                if (attrs.canUpdate)
                    scope.canUpdate = attrs.canUpdate === 'true';
                if (attrs.canDelete)
                    scope.canDelete = attrs.canDelete === 'true';
                if (attrs.canCreate)
                    scope.canCreate = attrs.canCreate === 'true';
            }
        };
    }]);

})(window, document);