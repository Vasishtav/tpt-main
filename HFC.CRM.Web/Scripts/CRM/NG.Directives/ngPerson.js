﻿/**
 * hfc.person module  
 * a simple directive for creating a person DOM
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.person', ['ui.mask'])

    .service('PersonService', ['$http', function ($http) {
        var srv = this;

        srv.People = [];
        srv.Person = null;
        srv.PersonIndex = -1;
        srv.LeadId = null;
        srv.IsBusy = false;

        srv.GetPerson = function (personId) {
            var per = $.grep(srv.People, function (p) {
                return p.PersonId == personId;
            });
            if (per)
                return per[0];
            else
                return null;
        };

        srv.Edit = function (personId, modalId, leadId, index) {
            var person = srv.GetPerson(personId);
            var copy = angular.copy(person);            
            copy.IsSubscribed = copy.UnsubscribedOnUtc == null;
            srv.Person = copy;
            srv.PersonIndex = parseInt(index);
            srv.LeadId = leadId;
            $("#" + modalId).modal("show");
        };

        srv.AddOrEdit = function (modalId) {
            if (!srv.Person) srv.Person = { PreferredTFN: 'C' };
            srv.LeadId = 0;
            srv.PersonIndex = 1;
            $("#" + modalId).modal("show");
        };
        srv.Addnote = function (modalId) {
            if (!srv.Person) srv.Person = { PreferredTFN: 'C' };
            srv.LeadId = 0;
            srv.PersonIndex = 1;
            $("#" + modalId).modal("show");
        };
        srv.Save = function (modalId) {
             if (srv.Person && srv.LeadId >= 0 && !srv.IsBusy) {
                 srv.IsBusy = true;
                 if (srv.Person.HomePhone!= "" && srv.Person.HomePhone.length < 10) {
                    HFC.DisplayAlert("Home Phone invalid");
                    srv.IsBusy = false;
                    return;
                }
                 if (srv.Person.CellPhone != "" && srv.Person.CellPhone.length < 10) {
                    HFC.DisplayAlert("Cell Phone invalid");
                    srv.IsBusy = false;
                    return;
                }
                 if (srv.Person.FaxPhone != "" && srv.Person.FaxPhone.length < 10) {
                    HFC.DisplayAlert("Fax Phone invalid");
                    srv.IsBusy = false;
                    return;
                }
                 if (srv.Person.WorkPhone != "" && srv.Person.WorkPhone.length < 10) {
                    HFC.DisplayAlert("Work Phone invalid");
                    srv.IsBusy = false;
                    return;
                }

                if (!srv.Person.IsSubscribed)
                    srv.Person.UnsubscribedOnUtc = new Date().toISOString();
                else
                    srv.Person.UnsubscribedOnUtc = null;

                if (srv.LeadId == 0) {
                    srv.IsBusy = false;
                    $("#" + modalId).modal("hide");
                    return;
                }

                var promise = null;
                if (!srv.Person.PersonId)
                    promise = $http.post('/api/leads/' + srv.LeadId + '/person/', srv.Person);
                else
                    promise = $http.put('/api/leads/' + srv.LeadId + '/person/', srv.Person);

                promise.then(function (response) {
                    if (!srv.Person.PersonId && response.data.PersonId != srv.Person.PersonId) {
                        srv.Person.PersonId = response.data.PersonId;
                    }

                    srv.Person.FullName = $.trim($.trim(srv.Person.FirstName) + " " + $.trim(srv.Person.LastName));
                    srv.People[srv.PersonIndex] = srv.Person;

                    $("#" + modalId).modal("hide");
                    HFC.DisplaySuccess("Person saved");
                    srv.Person = null;
                    srv.IsBusy = false;
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });                
            }
        };

        srv.Cancel = function (modalId) {
            $("#" + modalId).modal("hide");
            srv.Person = null;
            srv.PersonIndex = -1;
        };

        srv.SaveChangePerson = function (personId, jobId, success) {
            if (!srv.IsBusy && jobId && personId) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/customer/', { CustomerPersonId: personId })
                .then(function (response) {
                    if (success)
                        success();

                    HFC.DisplaySuccess("Customer updated");
                    srv.IsBusy = false;
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }                            
        };

        srv.parsePhone = function (phone) {
            return phone.match(/(\d{3})(\d{3})(\d{4})/);
        };

    }])

    .controller('PersonController', ['$scope','PersonService', function ($scope, PersonService) {
        $scope.inChangeMode = false;

        $scope.PersonService = PersonService;
        $scope.ChangePerson = function () {
            if($scope.jobId)
                PersonService.SaveChangePerson($scope.ngModel, $scope.jobId, function () {
                    $scope.inChangeMode = false;
                });
        }

        $scope.$watch('ngModel', function (newval, oldval) {
            if (newval != oldval || !$scope.person) {
                $scope.person = PersonService.GetPerson(newval);
            }
        });        
    }])

    .directive('hfcPerson', [function () {
        return {
            restrict: 'E',
            scope: {
                ngModel: '=',
                heading: '@',
                modalId: '@',
                index: '@',
                leadId: '@',
                jobId: '@',
                secondary: '@'
            },
            controller: 'PersonController',
            templateUrl: '/templates/NG/person-view.html',
            replace: true,
            link: function (scope, element, attrs) {                
                // Setup configuration parameters
                scope.heading = scope.heading || personConfig.heading;                
                if (attrs.editable)
                    scope.editable = attrs.editable === 'true';
                if (attrs.changeable)
                    scope.changeable = attrs.changeable === 'true';
                
                if (attrs.secondary) {
                    scope.secondary = attrs.secondary === 'true';
                }
            }
        };
    }])
    .directive('hfcPersonBase', ['PersonService', 'cacheBustSuffix', function (PersonService, cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/person-base.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function(scope){
                scope.PersonService = PersonService;
            }
        }
    }])
    //.directive('hfcPersonModal', ['PersonService', function (PersonService) {
    //    return {
    //        restrict: 'E',
    //        scope: {
    //            modalId: '@'
    //        },
    //        templateUrl: '/templates/NG/person-modal.html',
    //        replace: true,
    //        link: function(scope){
    //            scope.PersonService = PersonService;
    //        }
    //    }
    //}])
        

    //.directive('hfcPersonForm', [function () {
    //    return {
    //        restrict: 'E',
    //        scope: true,
    //        templateUrl: '/templates/NG/person-edit-form.html',
    //        replace: true
    //    }
    //}])
    .directive('hfcAddnoteModal', ['PersonService', function (PersonService) {
        return {
            restrict: 'E',
            scope: {
                modalId: '@'
            },
            templateUrl: '/templates/NG/addnote-modal.html',
            replace: true,
            link: function (scope) {
                scope.PersonService = PersonService;
            }
        }
    }])
        .directive('hfcAddnoteForm', [function () {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: '/templates/NG/addnote-edit-form.html',
                replace: true
            }
        }]);

})(window, document);