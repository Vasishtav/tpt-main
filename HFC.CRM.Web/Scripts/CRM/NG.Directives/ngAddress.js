﻿/**
 * hfc.address module  
 * a simple directive for creating an address hover over with google maps 
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

// TODO: NOTE: can be deleted, not used anywhere--murugan
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.address', ['hfc.hoverintent', 'hfc.core.service'])

    .service('AddressService', ['$http', '$sce', '$rootScope', function ($http, $sce, $rootScope) {
        var srv = this;

        srv.Address = null;
        srv.Addresses = [];
        srv.AddressIndex = -1;
        
        srv.Countries = [];
        srv.States = [];
        srv.IsBusy = false;

        srv.ZipCodeChanged = function() {

                if (srv.Address.ZipCode) {
                    srv.IsBusy = true;
                    $http.get('/api/lookup/0/zipcodes?q=' + srv.Address.ZipCode + "&country=" + srv.Address.CountryCode2Digits).then(function(response) {
                        srv.IsBusy = false;
                        if (response.data.zipcodes.length > 0) {
                            srv.Address.City = response.data.zipcodes[0].City;
                            srv.Address.State = response.data.zipcodes[0].State;
                        } else {
                            srv.Address.City = null;
                            srv.Address.State = null;
                        }
                    }, function(response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }

            };

        srv.CountryChanged = function() {
            srv.Address.City = '';
            srv.Address.State = '';
            srv.Address.ZipCode = '';
            srv.Address.CrossStreet = '';
        }

        srv.GetAddress = function (addrId) {
            var addr = $.grep(srv.Addresses, function (a) {
                return a.AddressId == addrId;
            });
            if (addr)
                return addr[0];
            else
                return null;
        };

        srv.GetLabel = function (addrId, singleline) {
            var addr = srv.GetAddress(addrId)
            if(addr) {
                var str = (addr.Address1 || "") + " " + (addr.Address2 || "");
                if (!singleline && (addr.City || addr.State))
                    str += "<br/>";
                else if (singleline || addr.ZipCode)
                    str += ", ";
                str += (addr.City || "");
                if (addr.State)
                    str += ", " + addr.State + " ";
                else if(addr.City)
                    str += " ";
                str += (addr.ZipCode || "");

                if (singleline)
                    return str;
                else
                    return $sce.trustAsHtml(str);
            }
        };
        
        srv.GetStates = function (countryISO) {
            return $.grep(srv.States, function (s) {
                return s.CountryISO2 == countryISO;
            });
        };

        srv.GetZipCodeMask = function (countryISO) {
            var cc = $.grep(srv.Countries, function (c) {
                return c.ISOCode2Digits == (countryISO || "US");
            });
            if (cc && cc.length)
                return cc[0].ZipCodeMask;
            else
                return "";
        };

            srv.GetGoogleHref = function(addrId) {
                var addr = srv.GetAddress(addrId);
                if (addr && addr.Address1) {

                    var srcAddr;
                    if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                        srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                    } else {
                        srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                    }

                    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                        return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(srv.GetLabel(addrId, true));
                    } else {
                        return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(srv.GetLabel(addrId, true));
                    }
                } else
                    return null;
            };

        srv.Get = function (leadIds, includeStates) {

            if (!srv.IsBusy) {
                srv.IsBusy = true;

                $http({ method: 'GET', url: '/api/address/', params: { leadIds: leadIds, includeStates: includeStates } })
                .then(function (response) {
                    srv.States = response.data.States || [];
                    srv.Countries = response.data.Countries || [];
                    srv.Addresses = response.data.Addresses || [];
                    srv.FranchiseAddress = response.data.FranchiseAddress;

                    srv.IsBusy = false;
                }, function (response) {
                    srv.IsBusy = false;
                });
            }
        }

        srv.Set = function(states, countries, addresses, franchiseAddress) {
                srv.States = states || [];
                srv.Countries = countries || [];
                srv.Addresses = addresses || [];
                srv.FranchiseAddress = franchiseAddress;
            
        }

       srv.Add = function (modalId, country) {            
            srv.Address = {
                IsResidential: true,
                CountryCode2Digits: country || 'US'
            };
            srv.AddressIndex = -1;
            $("#" + modalId).modal("show");
        }

        srv.Delete = function (leadId, addressId) {
            if (!srv.IsBusy && leadId && confirm("Do you wish to continue?")) {
                srv.IsBusy = true;

                $http({ method: 'DELETE', url: '/api/leads/' + leadId + '/address/', headers: {'Content-Type': 'application/json'}, data: { AddressId: addressId } })
                .then(function (response) {
                    var address = srv.GetAddress(addressId);
                    var label = srv.GetLabel(addressId, true);
                    remove(address);
                    HFC.DisplaySuccess("Address: " + label + " deleted");
                    srv.IsBusy = false;
                }, function(response){
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
        };
        srv.zipCodeIsNotValid = false;

        srv.Save = function (leadId, modalId) {
            if (!srv.IsBusy && srv.Address && leadId) {
                srv.IsBusy = true;
                var promise = null;

                if (srv.Address.ZipCode == '' || srv.Address.ZipCode == undefined) {
                    srv.zipCodeIsNotValid = true;
                    srv.IsBusy = false;
                    return;
                }
                else {
                    srv.zipCodeIsNotValid = false;
                }

                if (srv.Address.AddressId)
                    promise = $http.put('/api/leads/' + leadId + '/address/', srv.Address);
                else
                    promise = $http.post('/api/leads/' + leadId + '/address/', srv.Address);

                promise.then(function (res) {
                    if (!srv.Address.AddressId && res.data.AddressId != srv.Address.AddressId)
                        srv.Address.AddressId = res.data.AddressId;

                    if (srv.AddressIndex >= 0)
                        srv.Addresses[srv.AddressIndex] = srv.Address;
                    else
                        srv.Addresses.push(srv.Address);

                    $rootScope.$broadcast('refresh.territory', { zipCode: srv.Address.ZipCode, jobId: null });
                    srv.Address = null;
                    srv.AddressIndex = -1;
                    
                    HFC.DisplaySuccess("Address saved");
                    srv.IsBusy = false;

                    $("#" + modalId).modal("hide");
                }, function (res) {
                    HFC.DisplayAlert(res.statusText);
                    srv.IsBusy = false;
                });
            }
        };

        srv.Edit = function (addressId, modalId, index) {
            var address = srv.GetAddress(addressId);

            srv.Address = angular.copy(address);
            srv.AddressIndex = index;
            
            $("#" + modalId).modal("show");
        };

        srv.Cancel = function (modalId) {
            srv.Address = null;
            srv.AddressIndex = -1;
            $("#" + modalId).modal("hide");
        }

        srv.SaveAddressChange = function (jobId, newAddressId, isBilling, success) {
            if (srv.IsBusy)
                return;

            srv.IsBusy = true;
            var data = { BillingAddressId: newAddressId };
            if (!isBilling)
                data = { InstallAddressId: newAddressId };
            $http.put('/api/jobs/' + jobId + '/address', data).then(function () {
                srv.IsBusy = false;
                if (success) {
                    success();
                }
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        }

        function remove(item) {
            var index = srv.Addresses.indexOf(item);
            if (index > 0) {
                srv.Addresses.splice(index, 1);
            }
            else if (index === 0) {
                srv.Addresses.shift();
            }
        }
    }])

    .controller('AddressController', ['$scope', '$http', 'AddressService', function ($scope, $http, AddressService) {

        $scope.AddressService = AddressService;
        $scope.inChangeMode = false;

            $scope.OnMouseEnter = function ($event) {
            var $element = $($event.currentTarget),
                canvas = $element.find(".mapCanvas"),
                address = AddressService.GetAddress($scope.ngModel);
            if(address && address.Address1) {
                var dest = AddressService.GetLabel(address.AddressId, true);
                //if ($element.data("maploaded") == null) { //TODO: detect if address has changed then reload google map as needed instead of on every mouse over
                    var request = {
                        origin: AddressService.FranchiseAddress,
                        destination: dest,
                        travelMode: google.maps.TravelMode.DRIVING
                    };
                    var directionsService = new google.maps.DirectionsService(),
                        directionsDisplay = new google.maps.DirectionsRenderer();
                    directionsService.route(request, function (result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setDirections(result);
                            var mapOptions = {
                                zoom: 7,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            }
                            directionsDisplay.setMap(new google.maps.Map(canvas.get()[0], mapOptions));
                            $element.data("maploaded", true);
                        }
                    });
                //}
                var viewHeight = window.innerHeight,
                    pos = $element.offset();
                if (pos.top + canvas.height() - window.scrollY > viewHeight)
                    canvas.css("top", -canvas.height());
                else
                    canvas.css("top", "");
                if (pos.left < canvas.width() + 20)
                    canvas.css({ right: "", left: 0 });
                else
                    canvas.css({ right: 0, left: "" });
                canvas.show();
            }
        }

        $scope.OnMouseLeave = function ($event) {            
            var canvas = $($event.currentTarget).find(".mapCanvas");
            if (canvas)
                canvas.hide();
        }

        $scope.ChangeAddress = function ($event) {
            if ($scope.jobId) {
                var isBilling = $scope.heading.toLowerCase().indexOf('billing') >= 0;
                $scope.AddressService.SaveAddressChange($scope.jobId, $scope.ngModel, isBilling, function () {
                    $scope.inChangeMode = false;
                    if (isBilling === false) {
                        var adr = $scope.AddressService.GetAddress($scope.ngModel);
                        $scope.$root.$broadcast('refresh.territory', { zipCode: adr.ZipCode, jobId : $scope.jobId });
                        return;
                    }
                    //$scope.$root.$broadcast('reload.jobs');
                });
            } 
        };
    }])

    .directive('hfcAddress', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',            
            scope: {
                ngModel: '=', //addressId                
                heading: '@',
                leadId: '@',
                jobId: '@',
                modalId: '@'
            },
            controller: 'AddressController',
            controllerAs: 'addrCtrl',
            templateUrl: '/templates/NG/address-view.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function (scope, element, attrs) {                
                // Setup configuration parameters                
                if(attrs.editable)
                    scope.editable = attrs.editable === 'true';
                if(attrs.deletable)
                    scope.deletable = attrs.deletable === 'true';
                if(attrs.changeable)
                    scope.changeable = attrs.changeable === 'true';
                if (attrs.index)
                    scope.index = parseInt(attrs.index);
            }
        };
    }])

    .directive('hfcAddressModal', ['AddressService', 'cacheBustSuffix', function (AddressService, cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {
                modalId: '@',
                leadId: '@'
            },
            templateUrl: '/templates/NG/address-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function (scope) {
                scope.AddressService = AddressService;
            }
        }
    }])

    .directive('hfcAddressForm', ['cacheBustSuffix',  function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/address-edit-form.html?cache-bust=' + cacheBustSuffix,
            replace: true
        }
    }]);

})(window, document);