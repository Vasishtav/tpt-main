﻿(function(window, document, undefined) {
    'use strict';

    angular.module('hfc.push', ['hfc.core.service'])
        .service('PushListenerService', [
            '$http', 'CacheService', function ($http, CacheService) {
                var srv = this;

                srv.Init = function() {
                    $(function() {
                        var pushHub = $.connection.pushHub;

                        pushHub.client.entryUpdated = function(entryType) {
                            console.log('Entry updated: ' + entryType);

                            CacheService.Invalidate(entryType);
                            CacheService.Load(entryType);
                        }

                        // Start the connection.
                        $.connection.hub.start().done(function() {
                            console.log('websocket connection to pushHub established');
                        });
                    });
                };


            }
        ]).directive('hfcPush', [
            'PushListenerService', 'cacheBustSuffix', function (PushListenerService, cacheBustSuffix) {
                return {
                    restrict: 'E',
                    scope: false,
                    replace: true,
                    link: function(scope, element, attrs) {
                        scope.PushListenerService = PushListenerService;
                       
                        scope.PushListenerService.Init();

                       
                    }
                }
            }
        ]);


})(window, document);