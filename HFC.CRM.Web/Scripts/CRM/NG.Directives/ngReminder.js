﻿/**
 * hfc.reminder module  
 * a simple directive for handling reminders
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.reminder', ['hfc.core.service'])

    .service('ReminderService', ['$http',  function ($http, CacheService) {
        var srv = this,
            streamErrorCount = 0,
            pollCount = 0,
            interval = 60000,
            modalId = "reminderModal";

        srv.Reminders = [];
        srv.IsBusy = false;
        srv.SnoozeMinutes = "5";
        srv.TaskCount = null;
        srv.EmailCount = null;
        srv.NewLeadCount = null;

        function PollLocalStorage() {
            var storageKey = "CRMReminders";
            var reminders = JSON.parse(localStorage.getItem(storageKey));
            if (pollCount > 0) {
                var notfound = true;
                $.each(reminders, function (i, rem) {
                    var existInCol = $.grep(srv.Reminders, function (obj) {
                        return obj.EventPersonId == rem.EventPersonId;
                    });
                    //doesn't already exist in collection so we can flag visible
                    if (existInCol == null || existInCol.length == 0) {
                        notfound = false;
                        return;
                    }
                });
                if (!notfound)
                    $("#" + modalId).modal("show");
                else
                    $("#" + modalId).modal("hide");
            }
            srv.Reminders = reminders;
            srv.TaskCount = parseInt(localStorage.getItem("TaskCount"));
            //srv.EmailCount = parseInt(localStorage.getItem("EmailCount"));
            srv.NewLeadCount = parseInt(localStorage.getItem("NewLeadCount"));
        }

        //sets up EventSource to do a push notification of reminders and counts
        srv.Init = function (excludeCounter) {
            localStorage.clear();

            var url = "/eventstream.ashx",
                qs = [],
                storageKey = "CRMReminders",
                channelKey = "CRMReminderEventSource";

            qs.push("interval=" + interval);
            if (HFC.Browser.isIOS())
                qs.push("isSingleConnect=true");
            if (excludeCounter)
                qs.push("excl_counter=true");
            if (qs.length)
                url += "?" + qs.join("&");

            var existingChannel = localStorage.getItem(channelKey);

            //If there is already a push notification channel then we only need to check localStorage instead of opening a new push channel
            if (existingChannel) {
                PollLocalStorage();
                //set timer to check localstorage every min
                setInterval(PollLocalStorage, interval);
            } else {
                //IE doesn't support EventSource
                var source = new EventSource(url);
                try {
                    localStorage.setItem(channelKey, "connected");
                    $(window).unload(function () {
                        localStorage.removeItem(channelKey);
                    });

                    source.addEventListener("reminder",
                        function (e) {
                            streamErrorCount = 0;
                            if (e.origin.toLowerCase() == window.location.origin.toLowerCase()) {
                                this.retry = e.retry;
                                var json = JSON.parse(e.data);
                                if (json)
                                    localStorage.setItem(storageKey, e.data); //save to localstorage for all pages to use                
                                else  //no active reminders so clear it from storage
                                    localStorage.removeItem(storageKey);
                            }
                            PollLocalStorage();
                        }, false);
                    source.onerror = function (e) {
                        if (streamErrorCount > 5)
                            this.close();
                        else {
                            this.retry = interval;
                            streamErrorCount++;
                        }
                    };
                    source.onmessage = function (e) {
                        streamErrorCount = 0;
                        if (e.origin.toLowerCase() == window.location.origin.toLowerCase()) {
                            this.retry = e.retry;
                            var json = JSON.parse(e.data);
                            if (json) {
                                localStorage.setItem("TaskCount", json.TaskCount);
                                localStorage.setItem("EmailCount", json.EmailCount);
                                localStorage.setItem("NewLeadCount", json.NewLeadCount);
                            }
                        }
                    };
                } catch (err) { source.close(); }
            }
        };

        srv.InitWebSocket = function (excludeCounter) {
            $(function () {
                var reminderHub = $.connection.reminderHub;
                reminderHub.client.updateCounters = function (message) {
                    srv.TaskCount = parseInt(message.TaskCount);
                    srv.EmailCount = parseInt(message.EmailCount);
                    srv.NewLeadCount = parseInt(message.NewLeadCount);
                };

                reminderHub.client.updateReminders = function (message) {

                    var reminders = JSON.parse(message);
                    if (reminders) {
                        var notfound = true;
                        $.each(reminders, function (i, rem) {
                            var existInCol = $.grep(srv.Reminders, function (obj) {
                                return obj.EventPersonId == rem.EventPersonId;
                            });

                            //doesn't already exist in collection so we can flag visible
                            if (existInCol == null || existInCol.length == 0) {
                                notfound = false;
                                return;
                            }
                        });
                        if (!notfound)
                            $("#" + modalId).modal("show");
                        else
                            $("#" + modalId).modal("hide");

                        srv.Reminders = reminders;
                    }
                };
                
                reminderHub.client.entryUpdated = function (entryType) {
                    console.log('Entry updated: ' + entryType);
                    
                    CacheService.Invalidate(entryType);
                    CacheService.Load(entryType);
                }

                // Start the connection.
                $.connection.hub.start().done(function () {
                    console.log('websocket connection established');
                });
            });
        };

        srv.GetIcon = function (reminder) {
            var _icon = "";
            switch (reminder.AptTypeEnum) {
                case 13:
                    _icon = "glyphicon glyphicon-earphone";
                    break;
                case 12:
                    _icon = "glyphicon glyphicon-comment";
                    break;
                case 11:
                    _icon = "glyphicon glyphicon-home";
                    break;
                case 10:
                    _icon = "glyphicon glyphicon-glass";
                    break;
                case 9:
                    _icon = "glyphicon glyphicon-plane";
                    break;
                case 8:
                    _icon = "glyphicon glyphicon-eye-close";
                    break;
                case 7:
                    _icon = "glyphicon glyphicon-question-sign";
                    break;
                case 6:
                    _icon = "glyphicon glyphicon-wrench";
                    break;
                case 5:
                    _icon = "glyphicon glyphicon-ok";
                    break;
                case 4:
                    _icon = "glyphicon glyphicon-Four";
                    break;
                case 3:
                    _icon = "glyphicon glyphicon-Three";
                    break;
                case 2:
                    _icon = "glyphicon glyphicon-Two";
                    break;
                case 1:
                default:
                    _icon = "glyphicon glyphicon-One";
            }
            return _icon;
        }

        srv.EventDateTxt = function (reminder) {
            var startDate = new XDate(reminder.StartDate),
                endDate = new XDate(reminder.EndDate),
                rangeText = "";

            if (startDate.valid()) {
                if (endDate.valid()) {
                    var minDiff = startDate.diffMinutes(endDate),
                        start = "",
                        end = "";

                    if (minDiff <= 1440) {//both are happening within same day
                        rangeText = startDate.toFriendlyString();
                        if (!reminder.IsAllDay)
                            rangeText += ", " + startDate.toString("h:mm tt") + " to " + endDate.toString("h:mm tt");
                    } else {
                        start = startDate.toFriendlyString(!reminder.IsAllDay);
                        end = endDate.toFriendlyString(!reminder.IsAllDay);
                        if (start == end)
                            rangeText = start;
                        else
                            rangeText = start + " to " + end;
                    }
                } else
                    rangeText = startDate.toFriendlyString(!reminder.IsAllDay);
            }

            return rangeText;
        }

        srv.DismissAll = function () {
            angular.forEach(srv.Reminders, function (r) {
                r.IsActive = true;
            });
            srv.Dismiss();
        }

        srv.Dismiss = function () {
            var idArr = getIds();
            if (idArr.length > 0)
                post({ ids: idArr });
            //else
            //    HFC.DisplayAlert("Please select at least one reminder");
        }

        srv.Snooze = function () {
            var idArr = getIds();
            if (idArr.length > 0)
                post({ ids: idArr, minutes: parseInt(srv.SnoozeMinutes) });
            //else
            //    HFC.DisplayAlert("Please select at least one reminder");
        }

        function getIds() {
            var idArr = [];
            if (srv.Reminders.length == 1)
                idArr = [srv.Reminders[0].EventPersonId];
            else if (srv.Reminders.length > 1)
                idArr = $.map(srv.Reminders, function (r) {
                    return r.IsActive ? r.EventPersonId : null;
                });

            return idArr;
        }

        function post(data) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.post('/api/reminders/', data).then(function () {
                    if (srv.Reminders.length == 1)
                        srv.Reminders = [];
                    else if(srv.Reminders.length > 1) {
                        angular.forEach(srv.Reminders, function (r) {
                            if (r && r.IsActive)
                                remove(r);
                        });
                    }
                    if (srv.Reminders.length == 0)
                        $('#' + modalId).modal('hide');
                    
                    srv.IsBusy = false;
                }, function (res) {
                    HFC.DisplayAlert(res.statusText);
                    srv.IsBusy = false;
                });
            }
        }

        function remove(item) {
            var index = srv.Reminders.indexOf(item);
            if (index > 0) {
                srv.Reminders.splice(index, 1);
            }
            else if (index === 0) {
                srv.Reminders.shift();
            }
        }
    }])

    .directive('hfcReminder', ['ReminderService', 'cacheBustSuffix', function (ReminderService, cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: '/templates/NG/reminder-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function (scope, element, attrs) {
                scope.ReminderService = ReminderService;
                if (attrs.excludeCounter)
                    scope.excludeCounter = attrs.excludeCounter === 'true';

                scope.ReminderService.InitWebSocket(scope.excludeCounter);

                if ($("#emailCount").length > 0) {
                    $.get('/api/email/0/GetUnreadMessageCount').then(function (response) {
                        $("#emailCount").text(response.unreadMessageCount);
                    });
                }
            }
        };
    }]);

})(window, document);