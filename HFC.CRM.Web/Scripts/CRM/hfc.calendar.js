﻿HFC.Calendar = {
    Id: "fullcalendar-container"
    , Id_small: "smallcalender"
    , ResizeTimeout: null
    , QuickUpdate: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
        //
        var url = '/api/tasks/' + event.id + '/quick';
        if (event.AptTypeEnum > 0)
            url = '/api/calendar/' + event.id + '/quick';
        HFC.AjaxPut(url, {
            dayDelta: dayDelta,
            minuteDelta: minuteDelta,
            allDay: allDay === "undefined" || allDay == null ? null : allDay
        }).error(revertFunc);
    }
    , QuickUpdateResize: function (event, dayDelta, minuteDelta, revertFunc) {
        if (event.AptTypeEnum > 0)
            HFC.Calendar.QuickUpdate(event, dayDelta, minuteDelta, null, revertFunc);
        else {
            HFC.DisplayAlert("Task due date can not span more than 1 day.");
            revertFunc();
        }
    }
    , GetEvents: function (start, end, callback) {
        //t.fullcalendar.loading(true);
        var xstart = new XDate(start),
            xend = new XDate(end);

        var ids = HFC.KO.Users.SelectedUserIds();
        if (ids && ids.length > 0) {
            HFC.AjaxGetJson('/api/calendar',
                {
                    start: xstart.toString('u'),
                    end: xend.toString('u'),
                    personIds: ids
                }, function (result) {
                    //save result to look up recurring master
                    HFC.KO.CalModal.RecurMasterCollection(result.RecurringMaster);
                    if (result.Tasks && result.Events)
                        callback(result.Events.concat(result.Tasks));
                    else if (result.Events)
                        callback(result.Events);
                    else
                        callback(result.Tasks);
                });
        }
    }
    , GetViewableSize: function () {
        var topHeight = 60; //60 for the main top nav
        if (window.innerWidth < 768) //small devices
            topHeight += 65; //65 for the calendar nav with date on 2nd row
        else
            topHeight += 35; //35 for calendar nav
        var minCalHeight = window.innerHeight - topHeight - 80 - 25; //90 for the task header at bottom, 25 for the footer
        if (minCalHeight < 575)
            minCalHeight = 575;

        return minCalHeight;
    }
    , Resize: function () {
        //we add a resizeTO timeout so it only needs to resize when user stops resizing, not as user resize so it doesnt consume too much resource        
        if (HFC.Calendar.ResizeTimeout)
            clearTimeout(HFC.Calendar.ResizeTimeout); //removes it before it gets a chance to run

        HFC.Calendar.ResizeTimeout = setTimeout(function () {
            $("#" + HFC.Calendar.Id).fullCalendar("option", "height", HFC.Calendar.GetViewableSize());
        }, 1000);
    }

}


$(document).ready(function () {
    HFC.Popover.KO = new HFC.Popover.Model();
    //HFC.Tasks.KO = new HFC.Tasks.Collection();
    HFC.KO = {
        Users: new HFC.VM.Users(),
        CalModal: new HFC.VM.CalendarModal()
    }

    ko.applyBindings(HFC.Popover.KO, document.getElementById(HFC.Popover.ContainerId));
    //ko.applyBindings(HFC.Tasks.KO, document.getElementById(HFC.Tasks.ContainerId));
    ko.applyBindings(HFC.KO.CalModal, document.getElementById(HFC.KO.CalModal.ContainerId));
    ko.applyBindings(HFC.KO.Users, document.getElementById("crm-user-list"));
    //HFC.Tasks.KO.fetchTasks(); //get tasks
    ko.applyBindings({ RouteJob: HFC.QS.JobId != 0 }, document.getElementById("crm-cal-job"));

    $("#" + HFC.Calendar.Id).fullCalendar({
        header: false,
        defaultView: "agendaWeek",
        //minTime: options.minTime,
        //maxTime: options.maxTime,
        ignoreTimezone: true,
        slotMinutes: 15,
        firstHour: HFC.CurrentUser.CalendarFirstHour,
        defaultEventMinutes: 60,
        selectable: true,
        selectHelper: true,
        editable: true,
        height: HFC.Calendar.GetViewableSize(),
        events: HFC.Calendar.GetEvents,
        eventClick: function (calEvent, jsevent) {
            var clone = $.extend({}, calEvent);
            if (clone.AptTypeEnum)
                HFC.Popover.KO.Load(HFC.Models.Calendar(clone), jsevent.currentTarget, jsevent.clientX, jsevent.clientY);
            else
                HFC.Popover.KO.Load(HFC.Models.Task(clone), jsevent.currentTarget, jsevent.clientX, jsevent.clientY);
        },
        unselectCancel: "body",
        //save event once they finish dragging
        eventDrop: HFC.Calendar.QuickUpdate,
        eventResize: HFC.Calendar.QuickUpdateResize,
        eventDragStart: HFC.Popover.KO.Close,
        viewDisplay: HFC.Popover.KO.Close,
        mousedown: HFC.Popover.KO.Close,

        select: function (start, end, isAllDay, jsEvent, view) {
            //we're not limiting user
            //disable selection of past dates                    
            //if ((isAllDay && Globalize.compareDate(start, new Date(), true) >= 0) ||
            //    Globalize.compareDate(start, new Date()) >= 0)
            //    Calendar.Event.Show({ start: start, end: end, allDay: isAllDay });
            //else {
            //    HFC.Core.DisplayAlert("Unable to create events for past dates");
            //    this.unselect();
            //}
            HFC.KO.CalModal.NewEvent(null, { start: start, end: end, allDay: isAllDay });
        },
        eventRender: function (event, element, view) {
            var $timeEl = element.find(".fc-event-time");

            var diff = moment.utc(moment(event.end, "DD/MM/YYYY HH:mm:ss").diff(moment(event.start, "DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
            var dur = moment.duration(diff);

            $timeEl.prepend(assignCalendarStyle(dur.asHours(), event));

            //if (event.AptTypeEnum == 5)
            //    dur.asHours() >= 1 ? $timeEl.prepend("<span title='Appointment' class='calIcon glyphicon glyphicon-magnet'></span>")
            //        : $timeEl.prepend("<span title='Appointment' class='glyphicon glyphicon-calendar'></span>");
            //if (event.AptTypeEnum == 1)
            //    dur.asHours() >= 1 ? $timeEl.prepend("<span title='Appointment' class='calIcon glyphicon glyphicon-home'></span>")
            //        : $timeEl.prepend("<span title='Appointment' class='glyphicon glyphicon-calendar'></span>");
            //else if (event.AptTypeEnum == AppointmentTypeEnum.Installation)
            //    dur.asHours() >= 1 ? $timeEl.prepend("<span title='Installation' class='btn-lg glyphicon glyphicon-wrench'></span>") : $timeEl.prepend("<span title='Installation' class='glyphicon glyphicon-wrench'></span>");

            if (event.AptTypeEnum == null) {
                var span = $("<span title='Task' class='glyphicon " + (event.CompletedDateUtc ? "glyphicon-ok" : "glyphicon-unchecked") + "'></span>");
                span.click(function (e) {
                    e.stopPropagation();
                    var clone = $.extend({}, event);
                    var t = HFC.Models.Task(clone);
                    t.MarkComplete(t, e);
                    setTimeout(function () {
                        if (t.IsCompletedChecked())
                            event.CompletedDateUtc = new XDate();
                        else
                            event.CompletedDateUtc = null;
                    }, 500);
                });
                element.find(".fc-event-title").prepend(span);
            }
        },
        eventAfterAllRender: function (view) {
            $(".crm-cal-daterange").html(view.title);
        }
    });

    assignCalendarStyle: function assignCalendarStyle(asHours, eventType) {
        var _icon = "";
        var _span = "";
        var _jobNumber = eventType.JobNumber != null ? eventType.JobNumber : "NA";

        var _eventInfo = "<span class='newLine'>" + eventType.AssignedName + "</span><span class='newLine'>Job# " + _jobNumber + "</span>";

        if (eventType.EventType == EventTypeEnum.Series) {
            _span = asHours >= 1 ? "<span title='Recurring Event' class='btn calIcon-lg glyphicon glyphicon-repeat'></span><span>" + eventType.Location + " </span>" + _eventInfo : "<span title='Recurring Event' class='btn calIcon glyphicon glyphicon-repeat'>" + eventType.Location + "</span>";
        }
        else if (eventType.EventType == EventTypeEnum.Occurrence) {
            _span = asHours >= 1 ? "<span title='Single Occurrence' class='btn calIcon-lg glyphicon glyphicon-record'></span><span>" + eventType.Location + " </span>" + _eventInfo : "<span title='Single Occurrence' class='btn calIcon glyphicon glyphicon-record'>" + eventType.Location + "</span>";
        }

        if (eventType.AptTypeEnum != null) {
            switch (eventType.AptTypeEnum) {
                case 13:
                    _icon = "glyphicon glyphicon-earphone";
                    break;
                case 12:
                    _icon = "glyphicon glyphicon-comment";
                    break;
                case 11:
                    _icon = "glyphicon glyphicon-home";
                    break;
                case 10:
                    _icon = "glyphicon glyphicon-glass";
                    break;
                case 9:
                    _icon = "glyphicon glyphicon-plane";
                    break;
                case 8:
                    _icon = "glyphicon glyphicon-eye-close";
                    break;
                case 7:
                    _icon = "glyphicon glyphicon-question-sign";
                    break;
                case 6:
                    _icon = "glyphicon glyphicon-wrench";
                    break;
                case 5:
                    _icon = "glyphicon glyphicon-compressed";
                    break;
                case 4:
                    _icon = "glyphicon glyphicon-Four";
                    break;
                case 3:
                    _icon = "glyphicon glyphicon-Three";
                    break;
                case 2:
                    _icon = "glyphicon glyphicon-Two";
                    break;
                case 1:
                    _icon = "glyphicon glyphicon-One";
                    break;
                default:
                    _icon = "glyphicon glyphicon-calendar";
            }

            _span = (asHours >= 1) ? "<span title='Appointment' class='btn calIcon-lg " + _icon + "'></span><span>" + eventType.Location + " </span>" + _eventInfo : "<span title='Appointment' class='btn calIcon " + _icon + "'>" + eventType.Location + "</span";
        }

        return _span;
    };

    $("#" + HFC.Calendar.Id_small).datepicker();

    $("#view-month").on("click", function () {
        $('#' + HFC.Calendar.Id).fullCalendar('changeView', 'month');
    });
    $("#view-week").on("click", function () {
        $('#' + HFC.Calendar.Id).fullCalendar('changeView', 'agendaWeek');
    })
    $("#view-day").on("click", function () {
        $('#' + HFC.Calendar.Id).fullCalendar('changeView', 'agendaDay');
    });

    $("#moveto-prev").click(function (event) {
        moveTofullCalender('prev')
    });

    $("#moveto-today").click(function (event) {
        moveTofullCalender('today')
    });

    $("#moveto-next").click(function (event) {
        moveTofullCalender('next')
    });

    function moveTofullCalender(move) {
        $('#' + HFC.Calendar.Id).fullCalendar(move);
        var moveDate = $('#' + HFC.Calendar.Id).fullCalendar('getDate');
        $('#' + HFC.Calendar.Id_small).datepicker('setDate', moveDate)
    }

    $('#' + HFC.Calendar.Id_small).datepicker().on('changeDate', function (event) {
        var curDate = event.date;
        $('#' + HFC.Calendar.Id).fullCalendar('gotoDate', curDate);
    });

    $('#' + HFC.Calendar.Id_small).datepicker().on('changeMonth', function (event) {
        var curDate = event.date;
        $('#' + HFC.Calendar.Id).fullCalendar('gotoDate', curDate);
    });

    //bind to resize so we can refresh the chart and let google resize the chart if user resizes window
    $(window).unbind('resize.calendar');
    $(window).bind('resize.calendar', HFC.Calendar.Resize);
    window.addEventListener("orientationchange", HFC.Calendar.Resize);

    if (HFC.QS.LeadId || HFC.QS.JobId) {
        if (HFC.QS.AptTypeEnum) {
            HFC.KO.CalModal.NewEvent();
            $('#crm-evt-container').modal('hide');
        }
        else {
            HFC.KO.CalModal.NewTask();
        }
    }

});