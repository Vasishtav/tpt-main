﻿/*
 * XDate Extended methods
 * For Home Franchise Concepts
 * Copyright (c) 2013 Quan Tran
 * Last Modified: 11/22/2013
 */

;
function XTimeSpan(hour, minute, second, millisecond) {

    this.Day = 0;
    this.Hour = hour || 0;
    this.Minute = minute || 0;
    this.Second = second || 0;
    this.Millisecond = millisecond || 0;
    this.TotalHours = function () {
        return this.Hour + (this.Minute / 60) + (this.Second / 3600) + (this.Millisecond / 3600000);
    };
    this.TotalMinutes = function () {
        return (this.Hour * 60) + this.Minute + (this.Second / 60) + (this.Millisecond / 3600);
    };
    this.TotalSeconds = function () {
        return (this.Hour * 3600) + (this.Minute * 60) + this.Second + (this.Millisecond / 1000);
    };
    this.TotalMilliseconds = function () {
        return (this.Hour * 3600000) + (this.Minute * 60000) + (this.Second * 1000) + this.Millisecond;
    };       

    this.SetTimeFrom = function (totalMillisecond) {        
        totalMillisecond = parseInt(totalMillisecond);
        if (!isNaN(totalMillisecond)) {            
            this.Millisecond = totalMillisecond % 1000;
            this.Second = (totalMillisecond - this.Millisecond) % 60;
            this.Minute = parseInt(((totalMillisecond / 60000) % (24 * 60)) % 60);
            this.Hour = parseInt(((totalMillisecond / 60000) % (24 * 60)) / 60);
            this.Day = parseInt(totalMillisecond / (24 * 3600000));            
        }
    }

    //TODO
    //this.toString = function (format) {

    //};
    this.toText = function () {
        var text = "";
        if (this.Day > 0)
            text = this.Day + " day" + (this.Day > 1 ? "s " : " ");
        if (this.Hour > 0)
            text += this.Hour + " hr ";
        if (this.Minute > 0)
            text += this.Minute + " min";
        if (this.Second > 0)
            text += this.Second + " sec";
        if (this.Millisecond > 0)
            text += this.Millisecond + " ms";

        return text;
    }

};

XTimeSpan.FromHours = function (hour) {
    var xtime = new XTimeSpan();
    xtime.SetTimeFrom(hour * 3600000);
    return xtime;
};

XTimeSpan.FromMinutes = function (min) {
    var xtime = new XTimeSpan();
    xtime.SetTimeFrom(min * 60000);
    return xtime;
};

XTimeSpan.FromSeconds = function (second) {
    var xtime = new XTimeSpan();
    xtime.SetTimeFrom(second * 1000);
    return xtime;
};

XTimeSpan.FromMilliseconds = function (millisecond) {
    var xtime = new XTimeSpan();
    xtime.SetTimeFrom(millisecond);
    return xtime;
};

if (typeof XDate !== "undefined") {

    XDate.getRoundedDate = function (minIncrement) {
        var now = new XDate();
        minIncrement = minIncrement || 15; //default to 15 if developer doesnt supply 
        minIncrement -= (now.getMinutes() % minIncrement);
        now.addMinutes(minIncrement);
        return now; 
    }

    XDate.parsers.push(function (str) {
        //matches time hh:mm tt
        var timeParts = str.match(/^(?:(\d{1,2}):(\d{1,2})(?:\:(\d{1,2})(?:.(\d{0,}))?)?(?:\s*([a|p]m)?)?)?$/i);
        if (timeParts && timeParts.length > 5) {
            var hour = parseInt(timeParts[1]);

            if (timeParts[5] && timeParts[5].toLowerCase() == "pm" && hour != 12) {
                hour += 12;
            } else {
                if (timeParts[5] && timeParts[5].toLowerCase() == "am" && hour == 12)
                    hour = 0;
            }

            if (hour >= 24)
                hour = 0;
            var now = new Date();
            return new XDate(
                now.getFullYear(), // year
                now.getMonth(), // month
                now.getDate(),// date
                hour, //hour
                parseInt(timeParts[2] || 0)//min
            );
        } else {
            // this example parses dates like "yearh-month-date"
            var parts = str.match(/^(\d{2}|\d{4})[/|-]{1}(\d{1,2})[\|-]{1}(\d{1,2})\s+(?:(\d{1,2}):(\d{1,2})(?:\:(\d{1,2})(?:.(\d{0,}))?)?(?:\s+([a|p]m)?)?)?$/i);
            if (parts && parts.length >= 4) {
                var hour = parseInt(parts[4]);
                if (parts[8] && parts[8].toLowerCase() == "pm")
                    hour += 12;
                if (hour >= 24)
                    hour = 0;
                return new XDate(
                    parseInt(parts[1]), // year
                    parseInt(parts[2]) - 1, // month
                    parseInt(parts[3]),// date
                    hour, //hour
                    parseInt(parts[5] || 0)//min
                );
            }
        }
    });

    XDate.prototype.toFriendlyString = function (includeTime, ignoreTZ) {
        var str = "";
        var xdate = false;
        var self = this;

        /*if (self.getTimezoneOffset) {
            var offset = self.getTimezoneOffset();
            if (offset !== 0) {
                xdate = new XDate(this, true);
            } else {
                xdate = new XDate(this, false);
            }
        } else {
            xdate = new XDate(this);
        }*/

        if (ignoreTZ) {
            xdate = new XDate(this, true);
        } else {

            xdate = new XDate(this);
        }

        if (xdate && xdate.valid()) {
            var today = new XDate();                
                //minDiff = xdate.diffMinutes(today),
                //minFromMidnightToday = today.diffMinutes(new XDate().clearTime());
            
            if (xdate.getFullYear() == today.getFullYear()) {
                if (xdate.getMonth() == today.getMonth() && xdate.getDate() == today.getDate())
                    str = "Today";
                else if(xdate.getMonth() == today.getMonth() && (today.getDate() - xdate.getDate()) == 1)
                    str = "Yesterday";
                else
                    str = xdate.toString("ddd, MMM d");
            } else
                str = xdate.toString("ddd, MMM d yyyy");

            if (includeTime) {
                str += " " + xdate.toString("h:mm tt");
            }
        } 

        return str;
    }
};