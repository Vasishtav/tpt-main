﻿HFC.EditPermission = function (link, inEditMode) {
    var $tr = $(link).parent().parent().parent(),
                    $createCell = $tr.find("td:eq(3)"),
                    $readCell = $tr.find("td:eq(4)"),
                    $updateCell = $tr.find("td:eq(5)"),
                    $deleteCell = $tr.find("td:eq(6)"),
                    $createChk = $createCell.find("input"),
                    $readChk = $readCell.find("input"),
                    $updateChk = $updateCell.find("input"),
                    $deleteChk = $deleteCell.find("input"),
                    $createSpan = $createCell.find("span"),
                    $readSpan = $readCell.find("span"),
                    $updateSpan = $updateCell.find("span"),
                    $deleteSpan = $deleteCell.find("span");

    if (inEditMode) {
        if ($createChk.length == 0) {
            $createChk = $("<input type='checkbox'>");
            $readChk = $createChk.clone();
            $updateChk = $createChk.clone();
            $deleteChk = $createChk.clone();
            $createCell.append($createChk);
            $readCell.append($readChk);
            $updateCell.append($updateChk);
            $deleteCell.append($deleteChk);
        }
        $createChk.prop("checked", $createSpan.text() == "Allow").show();
        $readChk.prop("checked", $readSpan.text() == "Allow").show();
        $updateChk.prop("checked", $updateSpan.text() == "Allow").show();
        $deleteChk.prop("checked", $deleteSpan.text() == "Allow").show();
        $createSpan.hide();
        $readSpan.hide();
        $updateSpan.hide();
        $deleteSpan.hide();
        $tr.find(".glyphicon-pencil").parent().hide();
        $tr.find(".glyphicon-trash").parent().hide();
        $tr.find(".glyphicon-save").parent().show();
        $tr.find(".glyphicon-remove").parent().show();
    } else {
        $createChk.hide();
        $readChk.hide();
        $updateChk.hide();
        $deleteChk.hide();
        $createSpan.show();
        $readSpan.show();
        $updateSpan.show();
        $deleteSpan.show();
        $tr.find(".glyphicon-pencil").parent().show();
        $tr.find(".glyphicon-trash").parent().show();
        $tr.find(".glyphicon-save").parent().hide();
        $tr.find(".glyphicon-remove").parent().hide();
    }
}
HFC.SavePermission = function (link, permId) {
    var $tr = $(link).parent().parent().parent(),                    
                    $createChk = $tr.find("td:eq(3) input"),
                    $readChk = $tr.find("td:eq(4) input"),
                    $updateChk = $tr.find("td:eq(5) input"),
                    $deleteChk = $tr.find("td:eq(6) input"),
                    $createSpan = $tr.find("td:eq(3) span"),
                    $readSpan = $tr.find("td:eq(4) span"),
                    $updateSpan = $tr.find("td:eq(5) span"),
                    $deleteSpan = $tr.find("td:eq(6) span"),
                    data = 
                    {
                        CanCreate: $createChk.is(":checked"),
                        CanRead: $readChk.is(":checked"),
                        CanUpdate: $updateChk.is(":checked"),
                        CanDelete: $deleteChk.is(":checked")
                    };

    HFC.AjaxPut('/api/permissions/' + permId, data,
    function () {        
        if (data.CanCreate)
            $createSpan.text("Allow");
        else
            $createSpan.text("");
        if (data.CanRead)
            $readSpan.text("Allow");
        else
            $readSpan.text("");
        if (data.CanUpdate)
            $updateSpan.text("Allow");
        else
            $updateSpan.text("");
        if (data.CanDelete)
            $deleteSpan.text("Allow");
        else
            $deleteSpan.text("");

        $createChk.hide();
        $readChk.hide();
        $updateChk.hide();
        $deleteChk.hide();
        $createSpan.show();
        $readSpan.show();
        $updateSpan.show();
        $deleteSpan.show();
        $tr.find(".glyphicon-pencil").parent().show();
        $tr.find(".glyphicon-trash").parent().show();
        $tr.find(".glyphicon-save").parent().hide();
        $tr.find(".glyphicon-remove").parent().hide();        
    });
}
HFC.DeletePermission = function (link, permId, typeId) {
    if (confirm("Do you wish to continue?")) {
        HFC.AjaxDelete('/api/permissions/' + permId, null, function () {
            $(link).parent().parent().parent().remove();
            if ($(".perm-" + typeId, $(link).parentUtil('table')).length == 0)
                $(".perm-main-" + typeId, $(link).parentUtil('table')).remove();
            HFC.DisplaySuccess("Permission deleted");
        });
    }
}
HFC.NewPermission = function (form) {
    var userIdDDL = $("#perm-userIds"),
        roleIdDDL = $("#perm-roleIds"),
        userVal = userIdDDL.val(),
        roleVal = roleIdDDL.val();

    if (!userIdDDL.prop("disabled") && (userVal == null || userVal.length == 0) && (roleVal == null || roleVal.length == 0)) {
        HFC.DisplayAlert("Please select a role");
        return false;
    }
    
    $(form).parent().parent().find("button").prop("disabled", true);
    var data = {};
    $.each(form.elements, function (i, e) {
        if (e.name && e.value && (e.type != "checkbox" || (e.type == "checkbox" && e.checked))) {
            var val = $(e).val();
            if (typeof val == "object") {
                $.each(val, function (i, v) {
                    data[e.name + '[' + i + ']'] = v;
                });
            } else
                data[e.name] = e.value;
        }
    });
    HFC.AjaxPost(form.action, data, function (success) {
        window.location.reload();       
    }).complete(function () {
        $(form).parent().parent().find("button").prop("disabled", false);
    });
    return false;
}
HFC.PermChanged = function (ddl) {
    var opt = $(ddl).find("option:selected");
    if (opt.data("special") == "True")
        $("#perm-roleIds, #perm-userIds").prop("disabled", true);
    else
        $("#perm-roleIds, #perm-userIds").prop("disabled", false);
    $("#perm-roleIds, #perm-userIds").selectpicker('refresh');
}
HFC.TogglePermission = function(link, ptypeId) {
    $(".perm-" + ptypeId, $(link).parent().parent().parent()).toggle();
    var $span = $(link).find("span");
    if ($span.hasClass("glyphicon-plus"))
        $span.removeClass("glyphicon-plus").addClass("glyphicon-minus");
    else
        $span.removeClass("glyphicon-minus").addClass("glyphicon-plus");

    return false;
}