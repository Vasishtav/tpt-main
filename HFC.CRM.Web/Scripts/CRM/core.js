﻿/*******************IMPORTANT***********************
* This will set up traditional post/get serialization of arrays
* for example: var arr = [1,2,3,4] without traditional it will generate arr[]=1&arr[]=2&arr[]=3&arr[]=4
* This will not bind to MVC model binder, it needs to be traditional arr=1&arr=2&arr=3&arr=4
*/
$.ajaxSetup({
    traditional: true,
    cache: true,
    statusCode: {
        401: function () {
            location.href = "/Account/Login?SessionExpired=True";
        }
    }
});

/************************************************/

/*!
 * CRM Core JS
 */
var HFC = {   


    Util: {
        BaseURL: function () {
            var _url = window.location.href.split('/');
            return _url[0] + '//' + _url[2];
        }
    },
    Defaults: { MarginPercent: 60, JobberDiscountPercent: 60, Country: "US" },
    Models: {},
    VM: {},
    DisplayAlert: function (msg, type) {

        if ((msg != "Event Saved" && $("#crm-alertbox").length == 0) || (msg != "Task Saved" && $("#crm-alertbox").length == 0)) {

            if (type == null) {
                if (msg.length > 0) {
                    DisplayPopup(msg);
                }
               
            }
            else {
                $(".wrapper").append('<div id="crm-alertbox" class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><span></span></div>');
                $("#crm-alertbox").css('class', 'alert alert-dismissable alert-danger');
                $("#crm-alertbox > span").html(msg);
                $("#crm-alertbox").removeClass("alert-success alert-danger alert-info alert-warning").addClass(type).show().delay(delaytime).fadeOut();
                $("#crm-alertbox").css('display', 'block');
            }
           

           
        }
        else if (msg && msg.length > 0) {

            if (type == null) {
                DisplayPopup(msg);
            }
            else {
                var delaytime = 1000; //default of 10k millisecond or 10 seconds
                if (msg.length - 25 > 0)
                    delaytime += (msg.length - 25) * 200; //for every additional character add 100 millisecond, so a very long message will stay visible longer
                if (delaytime > 1000)
                    delaytime = 1000; //cap it at 1 minute;
                $("#crm-alertbox > span").html(msg);
                $("#crm-alertbox").removeClass("alert-success alert-danger alert-info alert-warning").addClass(type).show().delay(delaytime).fadeOut(); //delayed for 10 sec before dissapearing  
            }
            //if (type == null)
            //    type = "alert-danger"; //HFC.DataTypes.AlertTypes.Error;
            //var delaytime = 1000; //default of 10k millisecond or 10 seconds
            //if (msg.length - 25 > 0)
            //    delaytime += (msg.length - 25) * 200; //for every additional character add 100 millisecond, so a very long message will stay visible longer
            //if (delaytime > 1000)
            //    delaytime = 1000; //cap it at 1 minute;
            //$("#crm-alertbox > span").html(msg);
            //$("#crm-alertbox").removeClass("alert-success alert-danger alert-info alert-warning").addClass(type).show().delay(delaytime).fadeOut(); //delayed for 10 sec before dissapearing       
        }
    },
    DisplaySuccess: function (msg) {
        //HFC.DisplayAlert(msg, "alert-success");
    },
    DisplayWarning: function (msg) {
        HFC.DisplayAlert(msg, "alert-warning");
    },
    GetQueryString: function (name) {
        var qstring = location.search.replace('?', '').split('&');
        //var QS = {}
        for (var i = 0; i < qstring.length; i++) {
            var keyval = qstring[i].split('=');
            if (keyval[0].toLowerCase() == name.toLowerCase())
                return decodeURIComponent(keyval[1]);
            //QS[keyval[0]] = decodeURIComponent(keyval[1]);
        }
        return null;
    },
    htmlEncode: function (html) {
        if (html)
            return document.createElement('a').appendChild(
                document.createTextNode(html)).parentNode.innerHTML;
        else
            return null;
    },
    htmlDecode: function (input) {
        if (input) {
            var e = document.createElement('div');
            e.innerHTML = input;
            return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
        } else return null;
    },
    formatPhone: function (phone) {
        if (phone && phone.length == 10) {
            var reg = /(\d{3})(\d{3})(\d{4})/;
            return phone.replace(reg, "($1) $2-$3");
        } else if (phone)
            return phone;
        else
            return "";
    },
    evenRound: function (num, decimalPlaces) {
        var d = decimalPlaces || 0;
        var m = Math.pow(10, d);
        var n = +(d ? num * m : num).toFixed(8); // Avoid rounding errors
        var i = Math.floor(n), f = n - i;
        var e = 1e-8; // Allow for rounding errors in f
        var r = (f > 0.5 - e && f < 0.5 + e) ?
        ((i % 2 == 0) ? i : i + 1) : Math.round(n);
        return d ? r / m : r;
    },
    formatCurrency: function (num, precision) {
        if (precision == null)
            precision = 4;
        if (typeof num == "string")
            num = parseFloat(num.replace(/\$|\,/g, ''));
        num = isNaN(num) || num === '' || num === null ? 0.00 : num;
        num = HFC.evenRound(num, precision); //Math.floor(num * 100 + 0.50000000001);

        num = num.toFixed(precision).toString();
        var sign = num < 0,
            split = num.split('.'),
            dollar = split[0],
            cents = "",
            rgx = /(\d+)(\d{3})/;
        if (split.length > 1) {
            cents = '.';
            var centsStr = split[1];
            if (centsStr.length > 2) {
                var loopcount = centsStr.length - 2;
                for (var j = centsStr.length - 1; j >= 2; j--) {
                    if (centsStr[j] == 0)
                        centsStr = centsStr.substr(0, centsStr.length - 1);
                    else
                        break;
                }
            }
            cents += centsStr;
        }
        while (rgx.test(dollar)) {
            dollar = dollar.replace(rgx, '$1' + ',' + '$2');
        }

        //for (var i = 0; i < Math.floor((length - (1 + i)) / 3); i++)
        //    num = num.substring(0, length - (4 * i + 3)) + ',' + num.substring(length - (4 * i + 3));

        return ('$' + (sign ? '(' : '') + dollar.replace('-', '') + cents + (sign ? ')' : ''));
    },
    formatPercent: function (num, maxPrecision) {
        num = isNaN(num) || num === '' || num === null ? 0.00 : parseFloat(num);
        num = Math.round(num * 1000) / 1000.0;
        var precision = maxPrecision || 2,
            split = num.toString().split('.');
        if (split.length > 1 && split[1].length < precision) {
            precision = split[1].length;
        } else if (split.length == 1)
            precision = 0;
        return (100 % num ? num.toFixed(precision) : num) + "%";
    },
    Regex: {
        TimePattern: /^(\d{1,2}):(\d{1,2})(?:\:(\d{1,2})(?:.(\d*))?)?(?:\s+([a|p]m)?)?$/i,
        EmailPattern: /^[a-z0-9!#$%&"'*+/=?^_`{|}~.-]+@[a-z0-9!#$%&"'*+/=?^_`{|}~.-]+(\.[a-z0-9-]+)*$/i, ///^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i,
        IsValidTime: function (input) {
            var matches = HFC.Regex.TimePattern.exec(input),
                valid = false;
            if (matches && matches.length > 1) { //need at least first 2 matches
                var hr = parseInt(matches[0]),
                    min = parseInt(matches[1]);
                valid = hr <= 24 && hr >= 0 && min < 60 && min >= 0;
            }
            return valid;
        },
        IsValidAddress: function (addr) {
            var valid = false;
            if (addr && addr.length > 15) { //at least 15 char
                var zip_pattern = /([a-zA-Z]\d[a-zA-Z][\s-]?(\d[a-zA-Z]\d)?$)|([0-9]{5}[\s-]?([0-9]{4})?$)/;
                valid = zip_pattern.test(addr.substr(addr.length - 10, addr.length)); //tests the zip code
            }
            return valid;
        }
    },
    Browser: {
        //mobile or desktop compatible event name, to be used with '.on' function
        TOUCH_DOWN_EVENT_NAME: 'mousedown touchstart',
        TOUCH_UP_EVENT_NAME: 'mouseup touchend',
        TOUCH_MOVE_EVENT_NAME: 'mousemove touchmove',
        TOUCH_DOUBLE_TAB_EVENT_NAME: 'dblclick dbltap',

        isAndroid: function () {
            return navigator.userAgent.match(/Android/i);
        },
        isBlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        isIOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        isOpera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        isWindows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        isMobile: function () {
            return (HFC.Browser.isAndroid() || HFC.Browser.isBlackBerry() || HFC.Browser.isIOS() || HFC.Browser.isOpera() || HFC.Browser.isWindows());
        }
    },
    AjaxGetJson: function (url, data, success, complete) {
        return HFC.Ajax("GET", url, data, success, complete, "json");
    },
    AjaxGet: function (url, data, success, complete) {
        return HFC.Ajax("GET", url, data, success, complete);
    },
    AjaxPut: function (url, data, success, complete) {
        return HFC.Ajax("PUT", url, data, success, complete, "json");
    },
    AjaxPost: function (url, data, success, complete) {
        return HFC.Ajax("POST", url, data, success, complete, "json");
    },
    AjaxDelete: function (url, data, success, complete) {
        return HFC.Ajax("DELETE", url, data, success, complete, "json");
    },
    Ajax: function (type, url, data, success, complete, datatype) {
        $("#preloader").show();
        return $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: datatype,
            headers: type != "GET" ? HFC.getVerificationToken() : {},
            success: success,
            //textStatus possible values: "timeout", "error", "abort", and "parsererror"
            //reasonPhrase is the error message passed back by server
            error: function (xhr, textStatus, reasonPhrase) {
                if (xhr.responseJSON && xhr.responseJSON.Message) {
                    HFC.DisplayAlert(xhr.responseJSON.Message, "alert-warning");
                } else {
                    if (reasonPhrase.length > 0) {
                        //reasonPhrase = xhr.statusCode == 404 ? "404 - Page not found" : "An error occurred, unable to complete request";
                        HFC.DisplayAlert(reasonPhrase, textStatus == "timeout" ? "alert-warning" : "alert-danger");
                    }
                }
            },
            complete: function () {
                $("#preloader").hide();
                if (complete)
                    complete();
            }
        });
    }
    //, AjaxBasic: function (type, url, data) {
    //    $("#preloader").show();
    //    return $.ajax({
    //        type: type,
    //        url: url,
    //        data: data,
    //        contentType: 'application/json; charset=utf-8',
    //        error: function (xhr, textStatus, reasonPhrase) {
    //            if (reasonPhrase.length > 0) {
    //                HFC.DisplayAlert(reasonPhrase, textStatus == "timeout" ? "alert-warning" : "alert-danger");
    //            }
    //        }             
    //    });
    //    $("#preloader").hide();
    //}
    ,
    AjaxBasic: function (type, url, data, success, complete) {
        $("#preloader").show();
        return $.ajax({
            type: type,
            url: url,
            data: data,
            contentType: 'application/json; charset=utf-8',
            headers: type != "GET" ? HFC.getVerificationToken() : {},
            error: function (xhr, textStatus, reasonPhrase) {
                if (reasonPhrase.length > 0) {
                    HFC.DisplayAlert(reasonPhrase, textStatus == "timeout" ? "alert-warning" : "alert-danger");
                }
            },
            success: success,
            complete: function () {
                $("#preloader").hide();
                if (complete)
                    complete();
            }
        });
    },
    Lookup: {
        Get: function (array, lookup_id) {
            var stat = $.grep(array, function (s) {
                return s.Id == lookup_id;
            });
            if (stat && stat.length > 0)
                return stat[0];
            else
                return null;
        },
        GetChildren: function (array, parentId) {
            return $.grep(array, function (s) {
                return s.ParentId == parentId;
            });
        },
        ToArray: function (key_value_obj) {
            var result = new Array();
            for (var key in key_value_obj) {
                result.push({ Key: key.replace("_", " "), Value: key_value_obj[key] });
            }
            return result;
        },
        ToJSFromJsn: function (list, KeyProp, ValueProp) {
            var result = new Array();
            //$.each(list, function (i, key) { result.push(key[KeyProp], key[ValueProp]) });
            $.each(list, function (i, key) { result.push({ Key: key[KeyProp], Value: key[ValueProp] }) });
            return result;
        },
        GetKeyByValue: function (obj_to_check, value) {
            for (var prop in obj_to_check) {
                if (obj_to_check[prop] === value)
                    return prop;
            }
            return null; //return null if it can't find it
        }
    },

    // No more used in TPT.--murugan
    //LoadUser: function (personId, href) {
    //    // is it used in TP???
    //    var user = null;
    //    if (HFC.EmployeesJS) {
    //        user = $.grep(HFC.EmployeesJS, function (u) {
    //            return u.PersonId == personId;
    //        });
    //        if (user && user.length)
    //            user = user[0];
    //        else
    //            user = null;
    //    }
    //    var $modal = $("#user-profile-modal"),
    //        $modal_body = null,
    //        $modal_head = null;
    //    if ($modal.length == 0) {
    //        $modal = $('<div id="user-profile-modal" class="modal fade" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">');
    //        $modal_body = $('<div class="modal-body">');
    //        $modal_header = $('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title"></h4></div>');
    //        var $modal_dialog = $('<div class="modal-dialog">'),
    //            $modal_content = $('<div class="modal-content">');

    //        $modal_content.append($modal_header).append($modal_body);
    //        $modal_dialog.append($modal_content);
    //        $modal.append($modal_dialog);
    //        $(document.body).append($modal);
    //    } else {
    //        $modal_body = $modal.find(".modal-body");
    //        $modal_head = $modal.find(".modal-header");
    //    }

    //    $modal_header.removeAttr('style').css("border-radius", "4px 4px 0 0").removeAttr("class").addClass("modal-header");
    //    if (user && user.ColorId) {
    //        $modal_header.addClass("color-" + user.ColorId); //remove class attr to clear previous colors
    //        $modal_header.find('.modal-title').text(user.FullName + "'s Profile");
    //    } else {
    //        $modal_header.find('.modal-title').text('New User');
    //    }
    //    $("#preloader").show();
    //    href = href || '/settings/users';
    //    $modal_body.load(href + '?personId=' + personId, function () {
    //        $modal.modal("show");
    //        $("#preloader").hide();
    //    });

    //},
    GetStatuses: function (statusArray) {
        return $.map(statusArray, function (stat) {
            if (stat.ParentId == null || stat.ParentId == 0)
                return { Id: stat.Id, Name: stat.Name, Children: HFC.Lookup.GetChildren(statusArray, stat.Id) };
            else
                return null;
        });
    },
    getVerificationToken: function (returnAsString) {
        var val = $("input[name='__RequestVerificationToken']:first").val();
        if (returnAsString)
            return val;
        else
            return { __RequestVerificationToken: val };
    },

    getAptGlyphIcon: function (type) {
        var _icon = "";
        switch (type) {
            case 13:
                _icon = "glyphicon glyphicon-earphone";
                break;
            case 12:
                _icon = "glyphicon glyphicon-comment";
                break;
            case 11:
                _icon = "glyphicon glyphicon-home";
                break;
            case 10:
                _icon = "glyphicon glyphicon-glass";
                break;
            case 9:
                _icon = "glyphicon glyphicon-plane";
                break;
            case 8:
                _icon = "glyphicon glyphicon-eye-close";
                break;
            case 7:
                _icon = "glyphicon glyphicon-question-sign";
                break;
            case 6:
                _icon = "glyphicon glyphicon-wrench";
                break;
            case 5:
                _icon = "glyphicon glyphicon-compressed";
                break;
            case 4:
                _icon = "glyphicon glyphicon-Four";
                break;
            case 3:
                _icon = "glyphicon glyphicon-Three";
                break;
            case 2:
                _icon = "glyphicon glyphicon-Two";
                break;
            case 1:
            default:
                _icon = "glyphicon glyphicon-One";
        }
        return _icon;
    },

    currentGeoLocation: null,

    geoLocationError: null,

    getGeoLocation: function (fnCallback) {
        function getError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    return "User denied the request for Geolocation.";
                    break;
                case error.POSITION_UNAVAILABLE:
                    return "Location information is unavailable.";
                    break;
                case error.TIMEOUT:
                    return "The request to get user location timed out.";
                    break;
                case error.UNKNOWN_ERROR:
                    return "An unknown error occurred.";
                    break;
            }
        };


        if (navigator.geolocation) {

            var watchID = navigator.geolocation.watchPosition(function (location) {
                HFC.currentGeoLocation = location;
            }, function (errorCode) {
                HFC.geoLocationError = getError(errorCode);
            });



        } else {
            HFC.geoLocationError = 'geolocation is not available';
        }
    },

    getEmployeesJS: function (id) {
        var result = HFC.EmployeesJS;

        var found = $.grep(HFC.DisabledEmployeesJS, function (emp, i) {
            return emp.UserId == id;
        });

        if (found && found.length > 0) {
            result.push(found[0]);
        }

        return result;
    }   
};


if (HFC.Browser.isMobile()) {
    HFC.getGeoLocation();
}

//$(document).ready(function () {

    $('.mySelect').change(function () {
        //

        if ($(this).val() === null)
            $(this).css('color', 'red');
        else
            $(this).css('color', 'black');


    }).trigger('change');
//});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function DisplayPopup(msg) {
     var $dialog = $('<div style="background: #ebccd1;color: #a94442;" class="alert-danger"></div>')
.html(msg)
.dialog({
    dialogClass: 'ui-state-error',
    modal: true,
    title: "Error",
    width: 500,
    position: { my: 'top', at: 'top+150' },
    buttons: [
{
    text: "Ok",
    "class": 'k-button btn btn-primary cancel_but',
    click: function () {
        $(this).dialog("close");
    }
}
    ],
    open: function (event, ui) {
        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
    }
});

    $dialog.dialog('open');

    $dialog.html(msg);
}
function onExistingInfo() {
    var dom = $('select');
    //$('select')[2].style.color='black'
    for (var i = 0; dom.length > i; i++) {
        if (dom[i].value != "") {
            dom[i].style.color = 'black';
        }
    }
   
}
function RequiredDropdown() {

    $("select").click(function() {
        if ($(this).val() === null)
            $(this).css("color", "red");
        else
            $(this).css("color", "black");
    }).trigger("click");
    
}

////REMOVE FOR NOW AS WE ARE MOVING THE CONTEXT MENU TO LEFT TOP
//function elipseMenu() {
//    $(document.body).on('click', '[data-toggle=dropdown]', function () {
//        var dropmenu = $(this).next('.dropdown-menu');

//        dropmenu.css({
//            visibility: "hidden",
//            display: "block"
//        });

//        // Necessary to remove class each time so we don't unwantedly use dropup's offset top
//        dropmenu.parent().removeClass("dropup");
//        dropmenu.parent().removeClass("pull-right");


//        // Determine whether bottom of menu will be below window at current scroll position
//        if (dropmenu.offset().top + dropmenu.outerHeight() > $(window).innerHeight() + $(window).scrollTop()) {
//            dropmenu.parent().addClass("dropup");
//            dropmenu.parent().addClass("pull-right");
//        }

//        // Return dropdown menu to fully hidden state
//        dropmenu.removeAttr("style");
//    });
//}   

