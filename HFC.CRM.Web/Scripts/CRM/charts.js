﻿HFC.Charts = {
    Collection: function () {
        var self = this;

        self.CurrentYear = null;
        self.CurrentMonth = null;

        self.CacheData = [];

        self.fetchCharts = function () {
            fetchUserChartOptions()
              .done(function (result) {
                  if (HFC.KO && HFC.KO.AptList) {
                      var start = HFC.KO.AptList.StartDate().clone().setDate(1),
                          end = start.clone().addMonths(1).addDays(-1),
                          sameMonth = start.getMonth() == self.CurrentMonth,
                          sameYear = start.getFullYear() == self.CurrentYear;
                      //if (result.DisplayOption.indexOf("ShowAppts") != -1) { $('#ko-aptlist').css("display", "block"); }
                      //if (result.DisplayOption.indexOf("ShowTasks") != -1) { $('#ko-tasks').css("display", "block"); }
                      if (result.DisplayOption.indexOf("ShowTasks") != -1 && result.DisplayOption.indexOf("ShowAppts") == -1 ||
                      result.DisplayOption.indexOf("ShowTasks") == -1 && result.DisplayOption.indexOf("ShowAppts") != -1) {
                          $('#ko-tasks').removeClass("col-sm-5");
                          $('#ko-aptlist').removeClass("col-sm-7");
                      }
                     
                      if (!sameMonth) {
                          self.CurrentMonth = start.getMonth();
                        
                          if (result.DisplayOption.indexOf("ShowMonthlyTop5SalesAgents") != -1) { SalesAgentChart(start, end); }
                          if (result.DisplayOption.indexOf("ShowMonthlyJobStatus") != -1) { JobStatusChart(start, end); }
                          if (result.DisplayOption.indexOf("ShowMonthlySalesbySource") != -1) { SalesBySourceChart(start, end); }
                          if (result.DisplayOption.indexOf("ShowMonthlyLeadsbySource") != -1) { LeadsBySourceChart(start, end); }
                          if (result.DisplayOption.indexOf("ShowMonthlyJobsbySource") != -1) { JobsBySourceChart(start, end); }
                          if (result.DisplayOption.indexOf("ShowMonthlySalesbyVendor") != -1) { SalesByVendorChart(start, end); }
                          if (result.DisplayOption.indexOf("ShowMonthlySalesbyProductType") != -1) { SalesByProductChart(start, end); }
                      }

                      if (!sameYear) {
                          self.CurrentYear = start.getFullYear();
                          self.CurrentMonth = start.getMonth();
                          var yearstart = start.clone().setMonth(0).setDate(1),
                              yearend = yearstart.clone().addYears(1).addDays(-1),
                              isYearly = true;
                        
                          if (result.DisplayOption.indexOf("ShowYearlyTop5SalesAgents") != -1) { SalesAgentChart(yearstart, yearend, isYearly); }
                          if (result.DisplayOption.indexOf("ShowYearlyJobStatus") != -1) { JobStatusChart(yearstart, yearend, isYearly); }
                          if (result.DisplayOption.indexOf("ShowYearlySalesbySource") != -1) { SalesBySourceChart(yearstart, yearend, isYearly); }
                          if (result.DisplayOption.indexOf("ShowYearlyLeadsbySource") != -1) { LeadsBySourceChart(yearstart, yearend, isYearly); }
                          if (result.DisplayOption.indexOf("ShowYearlyJobsbySource") != -1) { JobsBySourceChart(yearstart, yearend, isYearly); }
                          if (result.DisplayOption.indexOf("ShowYearlySalesbyVendor") != -1) { SalesByVendorChart(yearstart, yearend, isYearly); }
                          if (result.DisplayOption.indexOf("ShowYearlySalesbyProductType") != -1) { SalesByProductChart(yearstart, yearend, isYearly); }
                      }

                  }
              })
              .fail(function (data) {
                  //alert("Unable to get data" + data.error);
              })
        };

        function drawChart() {
            if (self.CacheData) {
                $.each(self.CacheData, function (i, c) {
                    c.Chart.draw(c.Data, c.Options);
                });
            }
        }

        function getCache(key) {
            var item = $.grep(self.CacheData, function (c) {
                return c.Key == key;
            });
            if (item && item.length)
                return item[0];
            else
                return null;
        }

        //Ajax Call - maybe move to different object
        function fetchUserChartOptions() { return $.getJSON('/api/charts/0/LeadsSummaryDisplayOptions'); }
        function fetchChartData(url, start, end) { return $.getJSON(url, { startDate: start.toString("MM/dd/yyyy"), endDate: end.toString("MM/dd/yyyy") }); }
        //---End of Ajax Call

        //Sales Agent Chart
        function SalesAgentChart(start, end, isyearly) {
            var selector = isyearly == null ? "#crm-mtdtop5repLoader" : "#crm-ytdtop5repLoader";
            $(selector).show();
            fetchChartData('/api/charts/0/salesagentdashboardsummary', start, end)
            .done(function (result) { displaySalesAgentChart(result, start, isyearly); $(selector).hide(); })
            .fail(function (data) {  $(selector).hide(); })
        }
        function displaySalesAgentChart(result, start, isyearly) {

            var container_selector = isyearly == null ? ".crm-mtd" : ".crm-ytd",
             container = $(container_selector),
             args = Array.prototype.slice.call(arguments, 3);

            var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true }),
                       titlePrefix = (container_selector == ".crm-mtd" ? start.toString("MMMM") : ("Year " + start.getFullYear()));

            //#region top 5 sales rep
            var dataArray = [['Sales Agent', 'Sales', { role: 'style' }]],
                cacheKey = container_selector + " .top5rep";
          
            if (result.SalesRep) {
                $.each(result.SalesRep, function (i, s) {
                    if (s.Value > 0 || result.SalesRep.length == 1)
                        dataArray.push([s.Key, s.Value, s.Color]);
                });
            }
            //since column chart doesnt handle empty data message, we will use piechart so it can display the message
            //TEMPORARY HACK UNTIL GOOGLE ADD NO DATA HANDLING FOR COLUMN CHART
            var thisChart = container.filter(".top5rep").get(0);
            //thisChart.parentNode.setAttribute("style", "display: block;");


            var chart = result.SalesRep && result.SalesRep.length ? new google.visualization.ColumnChart(container.filter(".top5rep").get(0)) : new google.visualization.PieChart(container.filter(".top5rep").get(0)),
                datatable = google.visualization.arrayToDataTable(dataArray),
                item = getCache(cacheKey),
                title = titlePrefix + " Top 5 Sales Agents";

            formatter.format(datatable, 1);

            if (item) {
                item.Data = datatable;
                item.Chart = chart;
                item.Options.title = title;
            } else {
                var cOptions = { title: title, legend: { position: 'none' } };

                self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
            }
            drawChart();

        }
        //---End of Sales Agent Chart

        //Job Status Chart
        function JobStatusChart(start, end, isyearly) {
            var selector = isyearly == null ? "#crm-mtdjobstatusLoader" : "#crm-ytdjobstatusLoader";
            $(selector).show();
            fetchChartData('/api/charts/0/jobstatusdashboardsummary', start, end)
            .done(function (result) { displayJobStatusChart(result, start, isyearly); $(selector).hide(); })
            .fail(function (data) {  $(selector).hide(); })
        }
        function displayJobStatusChart(result, start, isyearly) {
            var container_selector = isyearly == null ? ".crm-mtd" : ".crm-ytd",
                  container = $(container_selector),
                  args = Array.prototype.slice.call(arguments, 3);
            var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true }),
                titlePrefix = (container_selector == ".crm-mtd" ? start.toString("MMMM") : ("Year " + start.getFullYear()));
            dataArray = [['Lead Status', 'Count']];
            cacheKey = container_selector + " .jobstatus";
            colors = [];
            if (result.JobStatus) {
                $.each(result.JobStatus, function (i, s) {
                    if (s.Value > 0 || result.JobStatus.length == 1) {
                        dataArray.push([s.Key, s.Value]);
                        colors.push(s.Color);
                    }
                });
            }

            datatable = google.visualization.arrayToDataTable(dataArray);
            item = getCache(cacheKey);
            title = titlePrefix + " Job Status";
            if (item) {
                item.Data = datatable;
                item.Options.title = title;
                item.Options.colors = colors;
            } else {
                chart = new google.visualization.PieChart(container.filter(".jobstatus").get(0));
                cOptions = { title: title, colors: colors };

                self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
            }

            var thisChart = container.filter(".jobstatus").get(0);
            thisChart.parentNode.setAttribute("style", "display: block;");
            drawChart();
        }
        //---End of Job Status Chart

        //Sales by Source
        function SalesBySourceChart(start, end, isyearly) {
           
            var selector = isyearly == null ? "#crm-mtdsourcesalesLoader" : "#crm-ytdsourcesalesLoader";
            $(selector).show();

            fetchChartData('/api/charts/0/salesbysourcedashboardsummary', start, end)
            .done(function (result) { displaySalesBySourceChart(result, start, isyearly); $(selector).hide(); })
            .fail(function (data) {  $(selector).hide(); })
        }
        function displaySalesBySourceChart(result, start, isyearly) {
            var container_selector = isyearly == null ? ".crm-mtd" : ".crm-ytd",
                 container = $(container_selector);
            var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true }),
                titlePrefix = (container_selector == ".crm-mtd" ? start.toString("MMMM") : ("Year " + start.getFullYear()));

            //#region source sales
            dataArray = [['Source', 'Sales']];
            cacheKey = container_selector + ".sourcesales";
            if (result.SourceSales) {
                $.each(result.SourceSales, function (i, s) {
                    if (s.Value > 0 || result.SourceSales.length == 1) {
                        dataArray.push([s.Key, s.Value]);
                    }
                });
            }

            var thisChart = container.filter(".sourcesales").get(0);
            //thisChart.parentNode.setAttribute("style", "display: block;");
            chart = result.SourceSales && result.SourceSales.length ? new google.visualization.ColumnChart(container.filter(".sourcesales").get(0)) : new google.visualization.PieChart(container.filter(".sourcesales").get(0)),
            datatable = google.visualization.arrayToDataTable(dataArray),

        item = getCache(cacheKey),
            title = titlePrefix + " Sales by Source";

            formatter.format(datatable, 1);

            if (item) {
                item.Data = datatable;
                item.Chart = chart;
                item.Options.title = title;
            } else {
                var cOptions = { title: title, legend: { position: 'none' } };

                self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
            }
            drawChart();

        }
        //---End of Sales by Source

        //Leads by Source
        function LeadsBySourceChart(start, end, isyearly) {
            $('#sourcesalesLoader').show();
            fetchChartData('/api/charts/0/leadsbysourcedashboardsummary', start, end)
            .done(function (result) { displayLeadsBySourceChart(result, start, isyearly); $('#sourcesalesLoader').hide(); })
            .fail(function (data) {  $('#sourcesalesLoader').hide(); })
        }
        function displayLeadsBySourceChart(result, start, isyearly) {
            var container_selector = isyearly == null ? ".crm-mtd" : ".crm-ytd",
            container = $(container_selector);
            var formatter = new google.visualization.NumberFormat({ fractionDigits: 2 }),
                    titlePrefix = (container_selector == ".crm-mtd" ? start.toString("MMMM") : ("Year " + start.getFullYear()));
            dataArray = [['Source', 'Leads']];
            cacheKey = container_selector + ".leadsbysource";
            if (result.Leadsbysource) {
                $.each(result.Leadsbysource, function (i, s) {
                    if (s.Value > 0 || result.Leadsbysource.length == 1) {
                        dataArray.push([s.Key, s.Value]);
                    }
                });
            }
            var thisChart = container.filter(".leadsbysource").get(0);
            thisChart.parentNode.setAttribute("style", "display: block;");
            chart = result.Leadsbysource && result.Leadsbysource.length ? new google.visualization.ColumnChart(container.filter(".leadsbysource").get(0)) : new google.visualization.PieChart(container.filter(".leadsbysource").get(0)),
            datatable = google.visualization.arrayToDataTable(dataArray),

            item = getCache(cacheKey),
            title = titlePrefix + " Leads by Source";

            formatter.format(datatable, 1);

            if (item) {
                item.Data = datatable;
                item.Chart = chart;
                item.Options.title = title;
            } else {
                var cOptions = { title: title, legend: { position: 'none' } };

                self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
            }

            drawChart();
        }
        //---End of Leads by Source

        //Jobs by Source
        function JobsBySourceChart(start, end, isyearly) {

           
            var selector = isyearly == null ? "#crm-mtdjobsourceLoader" : "#crm-ytdjobsourceLoader";
            $(selector).show();

            fetchChartData('/api/charts/0/jobsourcedashboardsummary', start, end)
            .done(function (result) { displayJobsBySourceChart(result, start, isyearly); $(selector).hide(); })
            .fail(function (data) { 
            $(selector).hide();
            })
        }
        function displayJobsBySourceChart(result, start, isyearly) {
            var container_selector = isyearly == null ? ".crm-mtd" : ".crm-ytd",
                container = $(container_selector),
                args = Array.prototype.slice.call(arguments, 3);
                  var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true }),
                        titlePrefix = (container_selector == ".crm-mtd" ? start.toString("MMMM") : ("Year " + start.getFullYear()));

                    //#region job count by source                    
                    dataArray = [['Source', 'Job Count']];
                    cacheKey = container_selector + " .jobsource";
                    if (result.JobSourceCount) {
                        $.each(result.JobSourceCount, function (i, s) {
                            if (s.Value > 0 || result.JobSourceCount.length == 1) {
                                dataArray.push([s.Key, s.Value]);
                            }
                        });
                    }
                    datatable = google.visualization.arrayToDataTable(dataArray);
                    item = getCache(cacheKey);
                    title = titlePrefix + " Jobs by Source";

                    if (item) {
                        item.Data = datatable;
                        item.Options.title = title;
                    } else {
                        chart = new google.visualization.PieChart(container.filter(".jobsource").get(0));
                        cOptions = { title: title };

                        self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
                    }
                    var thisChart = container.filter(".jobsource").get(0);
                    thisChart.parentNode.setAttribute("style", "display: block;");

                    drawChart();
                
        }
        //---End of Jobs by Source

        //Sales by Vendor
        function SalesByVendorChart(start, end, isyearly) {
            fetchChartData('/api/charts/0/salesbyvendordashboardsummary', start, end)
            .done(function (result) { displaySalesByVendorChart(result, start, isyearly); })
            .fail(function (data) {  })
        }
        function displaySalesByVendorChart(result, start, isyearly) {
            var container_selector = isyearly == null ? ".crm-mtd" : ".crm-ytd",
                container = $(container_selector),
                args = Array.prototype.slice.call(arguments, 3);           
                    var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true }),
                        titlePrefix = (container_selector == ".crm-mtd" ? start.toString("MMMM") : ("Year " + start.getFullYear()));
                 
                    dataArray = [['Source', 'Sales']];
                    cacheKey = container_selector + ".salesbyvendor";
                    if (result.Salesbyvendor) {
                        $.each(result.Salesbyvendor, function (i, s) {
                            if (s.Value > 0 || result.Salesbyvendor.length == 1) {
                                dataArray.push([s.Key, s.Value]);
                            }
                        });
                    }
                    var thisChart = container.filter(".salesbyvendor").get(0);
                    thisChart.parentNode.setAttribute("style", "display: block;");
                    chart = result.Salesbyvendor && result.Salesbyvendor.length ? new google.visualization.ColumnChart(container.filter(".salesbyvendor").get(0)) : new google.visualization.PieChart(container.filter(".salesbyvendor").get(0)),
                    datatable = google.visualization.arrayToDataTable(dataArray),
                item = getCache(cacheKey),
                    title = titlePrefix + " Sales by Vendor";

                    formatter.format(datatable, 1);

                    if (item) {
                        item.Data = datatable;
                        item.Chart = chart;
                        item.Options.title = title;
                    } else {
                        var cOptions = { title: title, legend: { position: 'none' } };

                        self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
                    }
                    drawChart();                
        }
        //---End of Sales by Vendor
    
        //Sales by Product Type
        function SalesByProductChart(start, end, isyearly) {
            fetchChartData('/api/charts/0/salesbyproducttypedashboardsummary', start, end)
            .done(function (result) { displaySalesByProductChart(result, start, isyearly); })
            .fail(function (data) {  })
        }
        function displaySalesByProductChart(result, start, isyearly) {
            var container_selector = isyearly == null ? ".crm-mtd" : ".crm-ytd",
                 container = $(container_selector),
                 args = Array.prototype.slice.call(arguments, 3);        
                    var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true }),
                        titlePrefix = (container_selector == ".crm-mtd" ? start.toString("MMMM") : ("Year " + start.getFullYear()));

                    //#region source sales
                    dataArray = [['Source', 'Sales']];
                    cacheKey = container_selector + ".salesbyproducttype";
                    if (result.SalesbyProduct) {
                        $.each(result.SalesbyProduct, function (i, s) {
                            if (s.Value > 0 || result.SalesbyProduct.length == 1) {
                                dataArray.push([s.Key, s.Value]);
                            }
                        });
                    }

                    var thisChart = container.filter(".salesbyproducttype").get(0);
                    thisChart.parentNode.setAttribute("style", "display: block;");
                    chart = result.SalesbyProduct && result.SalesbyProduct.length ? new google.visualization.ColumnChart(container.filter(".salesbyproducttype").get(0)) : new google.visualization.PieChart(container.filter(".salesbyproducttype").get(0)),
                    datatable = google.visualization.arrayToDataTable(dataArray),


                item = getCache(cacheKey),
                    title = titlePrefix + " Sales by Product";

                    formatter.format(datatable, 1);

                    if (item) {
                        item.Data = datatable;
                        item.Chart = chart;
                        item.Options.title = title;
                    } else {
                        var cOptions = { title: title, legend: { position: 'none' } };

                        self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
                    }
                    drawChart();               
        }
        //---End of Product Type


        function fetchSalesByProducttypeChartData(start, end, callback) {

            var container_selector = callback == null ? ".crm-mtd" : ".crm-ytd",
                container = $(container_selector),
                args = Array.prototype.slice.call(arguments, 3);

            HFC.AjaxGetJson('/api/charts/0/salesbyproducttypedashboardsummary', { startDate: start.toString("MM/dd/yyyy"), endDate: end.toString("MM/dd/yyyy") },
                function (result) {
                    var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true }),
                        titlePrefix = (container_selector == ".crm-mtd" ? start.toString("MMMM") : ("Year " + start.getFullYear()));

                    //#region source sales
                    dataArray = [['Source', 'Sales']];
                    cacheKey = container_selector + ".salesbyproducttype";
                    if (result.SalesbyProduct) {
                        $.each(result.SalesbyProduct, function (i, s) {
                            if (s.Value > 0 || result.SalesbyProduct.length == 1) {
                                dataArray.push([s.Key, s.Value]);
                            }
                        });
                    }

                    var thisChart = container.filter(".salesbyproducttype").get(0);
                    thisChart.parentNode.setAttribute("style", "display: block;");
                    chart = result.SalesbyProduct && result.SalesbyProduct.length ? new google.visualization.ColumnChart(container.filter(".salesbyproducttype").get(0)) : new google.visualization.PieChart(container.filter(".salesbyproducttype").get(0)),
                    datatable = google.visualization.arrayToDataTable(dataArray),


                item = getCache(cacheKey),
                    title = titlePrefix + " Sales by Product";

                    formatter.format(datatable, 1);

                    if (item) {
                        item.Data = datatable;
                        item.Chart = chart;
                        item.Options.title = title;
                    } else {
                        var cOptions = { title: title, legend: { position: 'none' } };

                        self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
                    }
                    drawChart();
                }
            );

        }


        //we add a resizeTO timeout so it only needs to resize when user stops resizing, not as user resize so it doesnt consume too much resource
        function redrawChart() {
            if (HFC.KO && HFC.KO.AptList && document.getElementById('leadSourceYTD') != null) {
                if (this.resizeTimeOut) clearTimeout(this.resizeTimeOut);
                this.resizeTimeOut = setTimeout(function () {
                    if (Math.abs(window.innerWidth - window.currentWidth) > 50) { //only resize if width changes, height will change often with bootstrap drop down menu                                            
                        drawChart();
                        window.currentWidth = window.innerWidth;
                    }
                }, 500);
            }
        };
        //bind to resize so we can refresh the chart and let google resize the chart if user resizes window
        $(window).unbind('resize.dashboard');
        $(window).bind('resize.dashboard', redrawChart);
        window.addEventListener("orientationchange", redrawChart);
        window.currentWidth = window.innerWidth;
        window.currentHeight = window.innerHeight;
    }
};