﻿(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.quickbooks.service', [])

    .service('QuickBooksService', ['$http', '$rootScope', function ($http, $rootScope) {
        var srv = this;

        srv.IsBusy = false;

        srv.SaveQBApp = function (qbapp) {
            
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.post('/api/Quickbooks/0' + '/SaveQBApp/', qbapp);
                srv.IsBusy = false;
            }
        };

        srv.GetQBApp = function (feId, callback) {
            var qbApp = {};
            if (feId && !srv.IsBusy) {
                srv.IsBusy = true;
                $http.get('/api/Quickbooks/' + feId + '/GetQBApp/').success(function (res) {
                    qbApp = res;
                    if (callback) {
                        callback(qbApp);
                    };
                });
                srv.IsBusy = false;
            }
        };

        srv.GetQbToken = function(callback) {
            $http.get('/api/Quickbooks/0/qbToken/', { ignoreLoadingBar: true }).success(function (res) {
                if (callback) {
                    callback(res);
                };
            });
        };

        srv.Sync = function (feId, callback) {
            if (!window.syncApi)
                return;
            if (feId && !srv.IsBusy) {
                srv.IsBusy = true;
                $http.post(syncApi + '/api/quickbooks/' + feId + '/sync/').success(function (res) {
                    if (callback) {
                        callback(res);
                    };
                });
                srv.IsBusy = false;
            }
        };

        srv.GetState = function (feId, callback) {
            if (!window.syncApi)
                return;
            if (feId && !srv.IsBusy) {
                $http.post(syncApi + '/api/quickbooks/' + feId + '/getState/', {}, { ignoreLoadingBar: true }).success(function (res) {
                    if (callback) {
                        callback(res);
                    };
                });
            }
        };

        srv.PreSyncCheck = function (feId, callback) {
            if (!window.syncApi)
                return;
            if (feId && !srv.IsBusy) {
                $http.post(syncApi + '/api/quickbooks/' + feId + '/PreSyncCheck/', {}, { ignoreLoadingBar: true }).success(function (res) {
                    if (callback) {
                        callback(res);
                    };
                });
            }
        };

        srv.Disconect = function (feId, callback) {
            if (!window.syncApi)
                return;
            if (feId && !srv.IsBusy) {
                srv.IsBusy = true;
                $http.post(syncApi + '/api/quickbooks/' + feId + '/disconect/').success(function (res) {
                    if (callback) {
                        callback(res);
                    };
                });
                srv.IsBusy = false;
            }
        };
    }]);
})(window, document);
