﻿(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.esignature.service', [])
        .service('ESignatureService', [
            '$http', function($http) {
                var srv = this;

                srv.IsBusy = false;

                srv.RequestSignatureOnDocument = function (quoteId) {
                    if (!srv.IsBusy) {
                        srv.IsBusy = true;
                        $http.get('/api/esignature/' + quoteId + '/quote').then(function () {
                            alert('');
                        });
                    }
                }
            }
        ]);

})(window, document);

