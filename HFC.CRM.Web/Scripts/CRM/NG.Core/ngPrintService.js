﻿/**
 * hfc.print.service module  
 * a simple module for handling printing
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.print.service', [])

    .service('PrintService', ['$http', function ($http) {
        var srv = this;

        srv.IsBusy = false;

        function getActiveXObject(name) {
            try { return new ActiveXObject(name); } catch(e) {}
        };

        function getNavigatorPlugin(name) {
            for(var key in navigator.plugins) {
                var plugin = navigator.plugins[key];
                if(plugin.name == name) return plugin;
            }
        };

        function getPDFPlugin() {            
            var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";

            if (userAgent.indexOf("msie") > -1) {
                //
                // load the activeX control
                // AcroPDF.PDF is used by version 7 and later
                // PDF.PdfCtrl is used by version 6 and earlier
                return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
            }
            else {
                return getNavigatorPlugin('Adobe Acrobat') || getNavigatorPlugin('Chrome PDF Viewer') || getNavigatorPlugin('WebKit built-in PDF');
            }            
        }; 

        function _injectIframe() {
            if (document.getElementById("print_frame")) {
                document.getElementById("print_frame").remove();
            }

            if (document.getElementById("print_frame") == null) {
                var frame = document.createElement("iframe");
                frame.id = "print_frame";
                frame.width = 0;
                frame.height = 0;
                frame.frameBorder = 0;
                $(".wrapper").append(frame);
            }
        }

        function _Print(src) {            
            var pdfPlugin = getPDFPlugin();
            if (pdfPlugin) {
                _injectIframe();

                var frame = document.getElementById("print_frame");
                $(frame).load(function () {
                    frame.contentWindow.print();
                    srv.IsBusy = false;
                });
                frame.src = src;
            } else {
                window.open(src, "_blank"); // if there is no pdf viewer plugin then we will let the user download it in a new window
                srv.IsBusy = false;
            }
        };

        srv.PrintQuote = function (quoteId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                var ticks = (new Date()).getTime();
                var url = '/export/quote/' + quoteId + '/?type=PDF&inline=true&t=' + ticks;

                _Print(url);
            }
        },

        srv.PrintInvoice = function (invoiceId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                var ticks = (new Date()).getTime();
                var url = '/export/invoice/' + invoiceId + '/?type=PDF&inline=true&t=' + ticks;

                _Print(url);
            }
        }


        srv.PrintLeadSheet = function (jobId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                var ticks = (new Date()).getTime();
                var url = '/export/LeadSheet/' + jobId + '/?type=PDF&inline=true&t=' + ticks;

                _Print(url);
            }
        }

    }]);

})(window, document);

