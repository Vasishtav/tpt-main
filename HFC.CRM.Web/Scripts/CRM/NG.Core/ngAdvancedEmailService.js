﻿/**
 * hfc.email.service module  
 * a simple module for handling email
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.advanced.email.service', ['hfc.core.service', 'ui.select2', 'summernote'])

    .service('AdvancedEmailService', ['$http', 'HFCService', 'LeadService', 'PersonService', function ($http, HFCService, LeadService, PersonService) {
        var srv = this;

        srv.IsBusy = false;
        srv.SendUrl = "";

        srv.HFCService = HFCService;
        srv.LeadService = LeadService;
        srv.PersonService = PersonService;
        srv.IsValidTo = true;
        srv.ToAddresses = [];
        srv.BccAddresses = [];
        srv.Subject = "";
        srv.Body = "";
        srv.Attachments = [];
        srv.Files = [];
        srv.TemplateId = null;
        srv.EmailTemplates = null;
        srv.EmailTemplatesLoaded = false;
        srv.Job = false;
        srv.PersonId = 0;
        srv.Appointment = 0;
        srv.PersonPrimaryEmail = null;
        angular.element(document).ready(function () {
            srv.LoadEmailTemplatesAdmin();
        });

        srv.AddNewBlank = function () {
            //var f = srv.LoadEmailTemplatesAdmin();
            srv.EmailTemplates.push({
                Description: '',
                EmailTemplateId: null,
                EmailTemplateTypeId: -1,
                EmailTemplateTypeName: '',
                Franchise: null,
                FranchiseId: 2439,
                TemplateBody: '',
                TemplateLayout: '',
                TemplateSubject: "Blank",
                isDeleted: false
            })
           
            //var error = function (res) {
            //    srv.IsBusy = false;
            //    HFC.DisplayAlert(res.statusText);
            //};
            //$http.post("/api/email/0/AddBlank").then(function (res) {
            //    srv.LoadEmailTemplatesAdmin();
            //}, error);

        }

        srv.TypeChange = function (emailTemplate) {
            var remainderCounter = 0;
            if (emailTemplate.EmailTemplateTypeId == 8) {
                for (var i = 0; i < srv.EmailTemplates.length; i++) {
                    if (srv.EmailTemplates[i].EmailTemplateTypeId == 8) {
                        remainderCounter++;
                    }
                }
            }

            if (remainderCounter >= 2) {
                alert("Remainder already exists");
                for (var i = 0; i < srv.EmailTemplates.length; i++) {
                    if (srv.EmailTemplates[i].EmailTemplateTypeId == 8 && srv.EmailTemplates[i].EmailTemplateId == emailTemplate.EmailTemplateId) {
                        srv.EmailTemplates[i].EmailTemplateTypeId = 7;
                    }
                }
            }

            else {
                var error = function (res) {
                    srv.IsBusy = false;
                    HFC.DisplayAlert(res.statusText);
                };

                $http.post("/api/email/" + emailTemplate.EmailTemplateId + "/UpdateTemplateType", { emailTemplateTypeId: emailTemplate.EmailTemplateTypeId }).then(function (res) {
                    srv.LoadEmailTemplatesAdmin();
                }, error);
            }

        }

        srv.LoadEmailTemplatesAdmin = function () {
            var error = function (res) {
                srv.IsBusy = false;
                HFC.DisplayAlert(res.statusText);
            };
            $http.get("/api/email/0/getTemplateAdmin").then(function (res) {
                srv.EmailTemplates = res.data;

                for (var i = 0; i < srv.EmailTemplates.length; i++) {
                    switch (srv.EmailTemplates[i].EmailTemplateTypeId) {
                        case 1:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Thank You Letter";
                            break;
                        case 2:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Appointment Confirmation";
                            break;
                        case 3:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Final Thank You";
                            break;
                        case 4:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Installation Appointment Confirmation";
                            break;
                        case 5:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Lead Details";
                            break;
                        case 6:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Sale Thank You";
                            break;
                        case 7:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Job";
                            break;
                        case 8:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Reminder";
                            break;
                        case 9:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Appointment";
                            break;
                        default:
                            srv.EmailTemplates[i].EmailTemplateTypeName = "Not Supported";
                            break;
                    }
                }

            }, error);
            srv.EmailTemplatesLoaded = true;
        }

        srv.Edit = function (templateId, job, appointment) {
            var error = function (res) {
                srv.IsBusy = false;
                HFC.DisplayAlert(res.statusText);
            };
            srv.Job = job;
            srv.Appointment = appointment;

            srv.TemplateId = templateId;
            if (job) {
                var leadId = job.LeadId;

                srv.LeadService.Get(leadId, function (leadServiceData) {
                    var lead = leadServiceData.Lead;
                    if (lead) {
                        var person = srv.PersonService.GetPerson(lead.PersonId);
                        if (person) {
                            var email = person.PrimaryEmail
                            srv.PersonId = lead.PersonId;
                            if (email != '') {
                                srv.ToAddresses = [];
                                srv.ToAddresses.push(email);
                            }
                        }
                    }
                });

            }

            $http.get('/api/users/' + srv.HFCService.CurrentUser.PersonId + "/getPerson/").then(function (response) {
                var person = response.data.Person;
                if (person) {
                    srv.PersonPrimaryEmail = person.PrimaryEmail;
                }
                else {
                    if (HFCService.CurrentUser) {
                        srv.PersonPrimaryEmail = HFCService.CurrentUser.Email;
                    }
                }
            });

            $http.get("/api/email/" + templateId + "/templateById").then(function (res) {
                srv.Subject = res.data.emailTemplate.TemplateSubject;
                srv.Body = res.data.emailTemplate.TemplateBody;
                if (res.data.emailTemplate.EmailTemplateTypeId == 9) {
                    if (!srv.Appointment) {

                        for (var i = 0; i < res.data.fieldsJob.length; i++) {
                            if (res.data.fieldsJob[i] != "Franchise Name") {
                                var index = res.data.customFields.indexOf(res.data.fieldsJob[i]);
                                if (index != -1) {
                                    res.data.customFields.splice(index, 1);
                                }
                            }
                        };


                    }
                    else {
                        if (srv.Appointment.JobId == null) {
                            for (var i = 0; i < res.data.fieldsJob.length; i++) {
                                if (res.data.fieldsJob[i] != "Franchise Name") {
                                    var index = res.data.customFields.indexOf(res.data.fieldsJob[i]);
                                    if (index != -1) {
                                        res.data.customFields.splice(index, 1);
                                    }
                                }
                            };
                        }
                    }
                };
                srv.CustomFields = res.data.customFields;
                // set custom fileds to editor
                var elem = $("#email-fields");
                elem.empty();
                srv.CustomFields.forEach(function (item) {
                    elem.append('<li><a data-event="insertField" href="#" data-value="{' + item + '}">' + item + '</a></li>');
                });

                $http.get("/api/jobs/" + job.JobId + "/files").then(function (res) {
                    srv.Files = res.data;

                    $("#advancedEmailModal").modal("show");

                }, error);

            }, error);
        }

        srv.AdminEdit = function (templateId) {
            var error = function (res) {
                srv.IsBusy = false;
                HFC.DisplayAlert(res.statusText);
            };
            srv.TemplateId = templateId;
            $http.get("/api/email/" + templateId + "/templateById").then(function (res) {
                srv.Subject = res.data.emailTemplate.TemplateSubject;
                srv.Body = res.data.emailTemplate.TemplateBody;
                if (res.data.emailTemplate.EmailTemplateTypeId == 9) {
                    for (var i = 0; i < res.data.fieldsJob.length; i++) {
                        if (res.data.fieldsJob[i] != "Franchise Name") {
                            var index = res.data.customFields.indexOf(res.data.fieldsJob[i]);
                            if (index != -1) {
                                res.data.customFields.splice(index, 1);
                            }
                        }
                    };
                }

                srv.CustomFields = res.data.customFields
                
                srv.TemplateId = res.data.emailTemplate.EmailTemplateId
                // set custom fileds to editor
                var elem = $("#email-fields");
                elem.empty();
                srv.CustomFields.forEach(function (item) {
                    elem.append('<li><a data-event="insertField" href="#" data-value="{' + item + '}">' + item + '</a></li>');
                });
                $("#advancedEmailModal").modal("show");

            }, error);
        }

        srv.AdminDelete = function (templateId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                $http.delete("/api/email/" + templateId + "/delete").then(function (res) {
                    HFC.DisplaySuccess("Email successfully deleted");
                    srv.IsBusy = false;
                    srv.LoadEmailTemplatesAdmin();
                }, function (res) {
                    srv.IsBusy = false;
                    HFC.DisplayAlert(res.statusText);
                });
            }
        };

        srv.AdminAdd = function (emailTemplate) {
            var remainderCounter = 0;
            if (emailTemplate.EmailTemplateTypeId == 8) {
                for (var i = 0; i < srv.EmailTemplates.length; i++) {
                    if (srv.EmailTemplates[i].EmailTemplateTypeId == 8) {
                        remainderCounter++;
                    }
                }
            }

            if (remainderCounter >= 2) {
                alert("Remainder already exists");
                for (var i = 0; i < srv.EmailTemplates.length; i++) {
                    if (srv.EmailTemplates[i].EmailTemplateTypeId == 8 && srv.EmailTemplates[i].EmailTemplateId == emailTemplate.EmailTemplateId) {
                        srv.EmailTemplates[i].EmailTemplateTypeId = -1;
                    }
                }
                return; 
            }
           
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                $http.post("/api/email/0/Add", emailTemplate).then(function (res) {
                    HFC.DisplaySuccess("Email successfully saved");
                    srv.IsBusy = false;
                    srv.LoadEmailTemplatesAdmin();
                }, function (res) {
                    srv.IsBusy = false;
                    HFC.DisplayAlert(res.statusText);
                });
            }
        };

        srv.AdminSave = function (templateId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                var data = {
                    toAddresses: srv.ToAddresses,
                    bccAddresses: srv.BccAddresses,
                    subject: HFC.htmlEncode(srv.Subject),
                    body: HFC.htmlEncode(srv.Body),
                    attachments: srv.Attachments,
                    emailTemplateId: srv.TemplateId
                };

                $http.post("/api/email/0/save", data).then(function (res) {
                    HFC.DisplaySuccess("Email successfully saved");
                    srv.IsBusy = false;
                    $("#advancedEmailModal").modal("hide");
                    srv.LoadEmailTemplatesAdmin();
                }, function (res) {
                    srv.IsBusy = false;
                    HFC.DisplayAlert(res.statusText);
                });
            }
        }


        srv.Send = function (save) {
            if (!srv.IsBusy) {
                //var to = document.getElementById('s2id_email-editor-to');
                if (srv.ToAddresses.length == 0) {
                    srv.IsValidTo = false;
                    return;
                }
                else
                {
                    srv.IsValidTo = true;
                }

                srv.IsBusy = true;
                
                var data = {
                    toAddresses: srv.ToAddresses,
                    bccAddresses: srv.BccAddresses,
                    subject: HFC.htmlEncode(srv.Subject),
                    body: HFC.htmlEncode(srv.Body),
                    attachments: srv.Attachments,
                    saveBody: save,
                    emailTemplateId: srv.TemplateId,
                    personId: srv.PersonId,
                    jobId: srv.Job.JobId,
                    appointmentId: null
                };

                if (srv.Appointment) {
                    data.appointmentId = srv.Appointment.id;
                }

                $http.post("/api/email/0/send", data).then(function (res) {
                    HFC.DisplaySuccess("Email successfully sent");
                    srv.IsBusy = false;
                    $("#advancedEmailModal").modal("hide");
                }, function (res) {
                    srv.IsBusy = false;
                    HFC.DisplayAlert(res.statusText);
                });
            }
        }
    }])

    .controller('AdvancedEmailController', ['$scope', 'HFCService', 'AdvancedEmailService', function ($scope, HFCService, AdvancedEmailService) {
        $scope.HFCService = HFCService;
        $scope.AdvancedEmailService = AdvancedEmailService;
        var tags = $.map(HFCService.Users, function (usr) {
            var text = usr.FullName || usr.Email;
            if (usr.FullName)
                text += " <" + usr.Email + ">";
            return { id: usr.Email, text: text, FullName: usr.FullName }
        });

        $scope.AddressesSelectOption = {
            multiple: true,
            simple_tags: true,
            width: '100%',
            placeholder: "",
            tokenSeparators: [",", " ", ";"],
            maximumSelectionSize: 10,
            maximumInputLength: 256,
            minimumInputLength: 1,
            tags: tags,
            formatSelection: function (email) {
                return "<span title='" + email.id + "'>" + (email.FullName || email.id) + "</span>";
            },
            createSearchChoice: function (term) {
                if (HFC.Regex.EmailPattern.test(term))
                    return { id: term, text: term };
                else
                    return null;
            },

            query: function (query) {
                var data = { results: [] };
                $.each(tags, function () {
                    if (query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0) {
                        data.results.push(this);
                    }
                });
                return query.callback(data);
            }
        };

        $scope.GetSelectOptions = function () {
            return {
                multiple: true,
                width: '100%',
                placeholder: "Files",
                maximumSelectionSize: 10,
                maximumInputLength: 256,

                id: function (file) {
                    return file.PhysicalFileId;
                },

                formatResult: function (file) {
                    return file.FileName;
                },

                formatSelection: function (file) {
                    return file.FileName;
                },

                query: function (query) {
                    var data = { results: [] };
                    $.each($scope.AdvancedEmailService.Files, function () {
                        data.results.push(this);
                    });
                    return query.callback(data);
                }
            }
        };
    }])

    .directive('hfcAdvancedEmailModal', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/templates/NG/advanced-email-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            controller: 'AdvancedEmailController'
        }
    }])

    .directive('hfcAdvancedEmailAdminModal', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/templates/NG/advanced-email-admin-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            controller: 'AdvancedEmailController'
        }
    }])
    .directive('hfcEmailTemplateAdminModal', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/templates/NG/email-template-admin-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            controller: 'AdvancedEmailController'
        }
    }])
    .directive('myMaxlength', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                attrs.$set("ngTrim", "false");
                var maxlength = parseInt(attrs.myMaxlength, 10);
                ctrl.$parsers.push(function (value) {
                    if (value.length > maxlength) {
                        value = value.substr(0, maxlength);
                        ctrl.$setViewValue(value);
                        ctrl.$render();
                    }
                    return value;
                });
            }
        };
    }]);
  
})(window, document);

