﻿/**
 * hfc.job.service module  
 * a simple module for handling job and quote
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.job.service', ['hfc.email.service', 'hfc.print.service', 'hfc.cache'])

    .service('JobService', ['$timeout', '$http', '$window', '$rootScope', 'PrintService', 'EmailService', 'CacheService', function ($timeout, $http, $window, $rootScope, PrintService, EmailService, CacheService) {
        var srv = this;

        srv.Jobs = [];
        srv.Concepts = [];
        srv.Territories = [];
        srv.JobStatuses = [];
        srv.IsBusy = false;
        srv.UniqueQuestions = [];
        srv.CurrentJob = false;
        srv.PrintService = PrintService;
        srv.EmailService = EmailService;
        srv.CacheService = CacheService;


        srv.GetJobIndex = function(job) {
            if (srv.Jobs.length == 0) {
                return -1;
            }

            // clone
            var newObjArray = srv.Jobs.slice(0);

            // sort
            function SortById(a, b) {
                var aName = a.JobId;
                var bName = b.JobId;
                return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
            }

            newObjArray.sort(SortById);
            
            for (var i = 0; i < newObjArray.length; i++) {
                if (newObjArray[i].JobId == job.JobId) {
                    return i;
                }
            }

            return -1;
        }

        $rootScope.$on('invoiceUpdated', function(event, data) {
            angular.forEach(srv.Jobs, function (j) {
                if (j.JobId == data.JobId) {
                    srv.UpdateInvoices(j);
                }
                
            });
        });
      
        function error(res) {
            srv.IsBusy = false;
            HFC.DisplayAlert(res.statusText);
        }

        $rootScope.$on('refresh.territory', function (event, data) {
            $http.post('/api/Franchise/' + data.zipCode + '/assignedTerritory').then(function (response) {
                if (response.data.territoryId === -1) {
                    return;
                }
                var territoryId = response.data.territoryId;
                if (territoryId === 0) {
                    territoryId = null;
                }
                if (data.jobId != null) {
                        srv.IsBusy = true;
                        $http.put('/api/jobs/' + data.jobId + '/territory', { TerritoryId: territoryId }).then(function () {
                            srv.IsBusy = false;
                            //HFC.DisplaySuccess("Territory successfully changed");
                            $rootScope.$broadcast('reload.jobs');
                        });
                    return;
                }
                var reloadJob = false;
                for (var i = 0; i < srv.Jobs.length; i++) {
                    var j = srv.Jobs[i];
                    if (j.TerritoryId == null && territoryId != null) {
                        reloadJob = true;
                        j.TerritoryId = territoryId;
                        srv.SaveTerritory(j.JobId, territoryId);
                    }
                }
                if (reloadJob) {
                    $rootScope.$broadcast('reload.jobs');
                }
            }, error);
        });

        function remove(item) {
            var index = srv.Jobs.indexOf(item);
            if (index > 0) {
                srv.Jobs.splice(index, 1);
            }
            else if (index === 0) {
                srv.Jobs.shift();
            }
        };

        srv.GetNetTotal = function (job) {
            if (!job.IsLoaded) {
                return "";
            }

            for (var i = 0; i < job.JobQuotes.length; i++) {
                if (job.JobQuotes[i].IsPrimary) {
                    return job.JobQuotes[i].NetTotal;
                }
            }

            return "";
        };

        srv.GetBalance = function (job) {
            if (!job.IsLoaded) {
                return 0;
            }

            var netTotal = 0;
            for (var i = 0; i < job.JobQuotes.length; i++) {
                if (job.JobQuotes[i].IsPrimary) {
                    netTotal = job.JobQuotes[i].NetTotal;
                }
            }
            var paymentSum = 0;
            if (job.Invoices && job.Invoices.length > 0) {
                for (var i = 0; i < job.Invoices[0].Payments.length; i++) {
                    paymentSum += parseFloat(job.Invoices[0].Payments[i].Amount);
                }
            }

            return netTotal - paymentSum;
        };

        srv.GetCostSubtotal = function (job) {
            if (!job.IsLoaded) {
                return 0;
            }

            for (var i = 0; i < job.JobQuotes.length; i++) {
                if (job.JobQuotes[i].IsPrimary) {
                    return job.JobQuotes[i].CostSubtotal;
                }
            }

            return "";
        };

        srv.UpdateInvoices = function(job) {
            $http.get('/api/jobs/' + job.JobId + '/invoices').then(function (response) {
                job.Invoices = response.data.Invoices;
                job.Balance = response.data.Job.Balance;
                
                if (job.SaleMade && job.Invoices.length > 0) {
                    job.InvoiceButtonText = "View Invoice";
                } else if (job.SaleMade && job.Invoices.length == 0) {
                    job.InvoiceButtonText = "Create Invoice";
                }
                
                srv.IsBusy = false;
            }, error);
        };

        srv.GetQuestion = function (qaCollection, question) {
            if (qaCollection) {
                return $.grep(qaCollection, function (q) {
                    return q.Question == question;
                });                
            } else
                return null;
        };

        srv.SaveQA = function (jobId, question) {
            if (!srv.IsBusy && jobId && question) {
                srv.IsBusy = true;

                var promise = null,
                    data = { QuestionId: question.QuestionId };

                if (question.SelectionType == 'checkbox')
                    data.answer = question.IsChecked ? "" : question.SubQuestion; //if question is already checked, then we want to save the opposite, which is an empty answer for unchecking this question otherwise use the subquestion
                else
                    data.answer = question.Answer;

                if (question.AnswerId) {
                    data.AnswerId = question.AnswerId;
                    promise = $http.put('/api/jobs/' + jobId + '/qanda/', data);
                } else
                    promise = $http.post('/api/jobs/' + jobId + '/qanda/', data);

                promise.then(
                    function (res) {
                        if (!question.AnswerId && res.data && res.data.AnswerId)
                            question.AnswerId = res.data.AnswerId;

                        question.IsChecked = !question.IsChecked;

                        srv.IsBusy = false;
                        HFC.DisplaySuccess(question.SubQuestion + " saved successfully");                        
                    }, error
                );
            }
        };

        srv.GetStatus = function (statId) {
            if (!srv.JobStatuses)
                return null;
            
            var stat = $.grep(srv.JobStatuses, function (stat) {
                return stat.Id == statId;
            });
            if (stat)
                return stat[0];
            else
                return null;
        };

        //this will sort it by parent > child then return the list
        srv.GetJobStatuses = function (job) {
            var arr = [];
            if (srv.JobStatuses) {
                var parents = $.grep(srv.JobStatuses, function (stat) {
                    return stat.ParentId == null &&
                    (job.SaleMade && srv.JobStatusesAfterSaleMade.indexOf(stat.Id) >= 0 || !job.SaleMade && srv.JobStatusesAfterSaleMade.indexOf(stat.Id) < 0);
                });

                angular.forEach(parents, function (stat) {
                    arr.push(stat);
                    var child = $.grep(srv.JobStatuses, function (child) {
                        return child.ParentId == stat.Id;
                    });
                    if (child)
                        arr = arr.concat(child);
                });

                var selectedStatus = $.grep(arr, function(e) {
                    return e.Id == job.JobStatusId;
                });

                if (!selectedStatus || selectedStatus == 0) {
                    var missedStatus = $.grep(srv.JobStatuses, function(e) {
                        return e.Id == job.JobStatusId;
                    });

                    if (missedStatus && missedStatus.length > 0) {
                        arr = arr.concat(missedStatus[0]);
                    }
                }
            }
            return arr;
        };

        srv.GetStatusLabel = function (stat) {
            if (!srv.JobStatuses)
                return "";

            var parent = null;
            if (stat.ParentId)
                parent = srv.GetStatus(stat.ParentId);
            if (parent)
                return '<span class="left-margin">' + stat.Name + ' <span class="text-muted">' + parent.Name + '</span></span>';
            else
                return stat.Name;
        };

        srv.GetTerritoryLabel = function (terr) {
            return terr.Code + ' - ' + (terr.Name || "");
        };

        //utility to return a class name linked to a status id
        srv.JobStatusCss = function (statusId) {
            var css = null;
            if (statusId && srv.JobStatuses) {
                var stat = srv.GetStatus(statusId);
                if (stat.ParentId)
                    stat = srv.GetStatus(stat.ParentId);

                css = stat.ClassName;                
            }
            return css || "btn-default";
        };

        srv.ReloadOrders = function (leadIds) {
            if (leadIds) {
                $http.get('/api/jobs/', { params: { leadIds: leadIds, includeAddlLookup: true } }).then(function(response) {
                    srv.SentJobItemIds = response.data.SentJobItemIds || [];
                    srv.OrderedJobItemIds = response.data.OrderedJobItemIds || [];
                });
            }
        }

        //gets a list of jobs and store it in this service as well as any pass in reference
        srv.Get = function (leadIds, success) {
            if (leadIds) {
                $http.get ('/api/jobs/', { params: { leadIds: leadIds, includeAddlLookup: true } }).then(function (response) {


                    srv.JobStatuses = [];

                    srv.CacheService.Get('JobStatus', function (items) {
                        srv.JobStatuses = items;
                    });

                    srv.Concepts = [];

                        srv.CacheService.Get('Concept', function(items) {
                            srv.Concepts = items;
                        });

                    srv.Territories = response.data.Territories || [];
                    
                    srv.JobStatusesAfterSaleMade = response.data.JobStatusesAfterSaleMade || [];
                    srv.JobStatusesAfterQuoteGiven = response.data.JobStatusesAfterQuoteGiven || [];
                    srv.SentJobItemIds = response.data.SentJobItemIds || [];
                    srv.OrderedJobItemIds = response.data.OrderedJobItemIds || [];
                    srv.QandA = response.data.QandA;

                    if (response.data.QandA) {
                        var questArray = $.map(response.data.QandA, function (qa) {
                            return qa.Question;
                        });
                        srv.UniqueQuestions = $.grep(questArray, function (el, index) {
                            return index == $.inArray(el, questArray);
                        });
                    }
                    
                    //change dates to XDate for easier manipulation
                    angular.forEach(response.data.JobCollection, function (j) {
                        
                        j.CreatedOnUtc = new XDate(j.CreatedOnUtc, true);
                        var qandaclone = angular.copy(response.data.QandA);

                        j.SaleMade = srv.JobStatusesAfterSaleMade.indexOf(j.JobStatusId) >= 0;
                        j.QuoteGiven = srv.JobStatusesAfterQuoteGiven.indexOf(j.JobStatusId) >= 0;

                        if (!j.SaleMade) {
                            j.InvoiceButtonText = "Convert to Sale";
                        } else {
                            j.InvoiceButtonText = (!j.Invoices || !j.Invoices.length > 0) ? 'Create Invoice' : 'View Invoice';
                        }
                        
                        //merge jobquestionanswers to QandA so we can loop through to render
                        if (j.JobQuestionAnswers) {
                            angular.forEach(j.JobQuestionAnswers, function (ans) {
                                var exist = $.grep(qandaclone, function (question) {
                                    return ans.QuestionId == question.QuestionId;
                                });
                                if (exist) {
                                    exist[0].Answer = ans.Answer;
                                    exist[0].AnswerId = ans.AnswerId;
                                    exist[0].IsChecked = (ans.Answer || "") != "";
                                }
                            });
                        }
                        j.JobQuestionAnswers = qandaclone;
                    });
                    srv.Jobs = response.data.JobCollection || [];

                    angular.forEach(srv.Jobs, function (j) {
                        j.CurrentJobStatusId = j.JobStatusId;
                        j.ContractedOnUtcRequired = false;
                    });

                    if (success)
                        success();
                }, error);
            }
        };



        srv.Load = function(job, fnNext) {
            if (job.IsLoaded) {
                return;
            }

            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.get('/api/jobs/' + job.JobId + '/details').then(function (response) {
                    job = response.data;
                    
                    job.CreatedOnUtc = new XDate(job.CreatedOnUtc, true);
                    var qandaclone = angular.copy(srv.QandA);

                    job.SaleMade = srv.JobStatusesAfterSaleMade.indexOf(job.JobStatusId) >= 0;
                    job.QuoteGiven = srv.JobStatusesAfterQuoteGiven.indexOf(job.JobStatusId) >= 0;

                    if (!job.SaleMade) {
                        job.InvoiceButtonText = "Convert to Sale";
                    } else {
                        job.InvoiceButtonText = (!job.Invoices.length > 0) ? 'Create Invoice' : 'View Invoice';
                    }

                    //merge jobquestionanswers to QandA so we can loop through to render
                    if (job.JobQuestionAnswers) {
                        angular.forEach(job.JobQuestionAnswers, function (ans) {
                            var exist = $.grep(qandaclone, function (question) {
                                return ans.QuestionId == question.QuestionId;
                            });
                            if (exist) {
                                exist[0].Answer = ans.Answer;
                                exist[0].AnswerId = ans.AnswerId;
                                exist[0].IsChecked = (ans.Answer || "") != "";
                            }
                        });
                    }
                    job.JobQuestionAnswers = qandaclone;


                    fnNext(response.data);

                    srv.IsBusy = false;
                }, error);
            }
        }
        
        srv.SaveSource = function (jobId, sourceId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/source', { SourceId: sourceId }).then(function () {
                    HFC.DisplaySuccess("Job source successfully changed");
                    srv.IsBusy = false;
                }, error);
            }
        };

        srv.SaveContractDate = function (jobId, date) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                var dateStr = date != null && typeof (date) == 'object' ? date.toISOString() : date;
                $http.put('/api/jobs/' + jobId + '/contractdate', { ContractedOnUtc: dateStr }).then(function () {
                    HFC.DisplaySuccess("Contract date successfully changed");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.SaveQuoteDate = function (jobId, date) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                var dateStr = date != null && typeof (date) == 'object' ? date.toISOString() : date;
                $http.put('/api/jobs/' + jobId + '/quotedate', { QuoteDateUtc: dateStr }).then(function () {
                    HFC.DisplaySuccess("Quote date successfully changed");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.LoadJobFiles = function(job) {
                $http.get('/api/jobs/' + job.JobId + '/getjobfiles').then(function(response) {
                    job.PhysicalFiles = response.data.jobFiles;

                    job.FilesCount = job.PhysicalFiles.length;

                    /*$timeout(function() {
                        var dropzone = null;
                        $.each(DropzoneService.Dropzones, function(i, dz) {
                            if (dz.id == "dz" + job.JobId) {
                                dropzone = dz.dropzone;
                                return;
                            }
                        });

                        if (dropzone.options.addedFiles > 0) return;

                        $.each(job.PhysicalFiles, function(i, file) {
                            var mockFile = {
                                fileId: file.PhysicalFileId,
                                name: file.FileName,
                                size: file.FileSize || 0,
                                ThumbUrl: file.ThumbUrl,
                                Url: file.UrlPath,
                                description: file.FileDescription,
                                category: file.FileCategory,
                                creatorFullName: file.AddedByFullName,
                                createdOn: new XDate(file.CreatedOnUtc, true)
                            };

                            dropzone.options.addedfile.call(dropzone, mockFile);
                            dropzone.options.addedFiles = dropzone.options.addedFiles + 1;
                            $(mockFile.previewElement).popover({
                                trigger: 'hover',
                                title: mockFile.creatorFullName + ' - ' + new XDate(mockFile.createdOn, true).toString("M/d/yy h:mm tt"),
                                content: (mockFile.description || "No description") + " (" + (mockFile.category || "No Category") + ")",
                                placement: 'auto',
                                animation: !HFC.Browser.isMobile()
                            });

                            $(".dz-details", mockFile.previewElement).attr("href", mockFile.Url);
                            if (file.MimeType && file.MimeType.indexOf("image") >= 0)
                                dropzone.options.thumbnail.call(dropzone, mockFile, mockFile.ThumbUrl);
                        });
                    }, 1000);*/


                }, error);

            }

            srv.SaveDescription = function(jobId, description) {
                if (!srv.IsBusy) {
                    srv.IsBusy = true;
                    $http.put('/api/jobs/' + jobId + '/description', { Description: description }).then(function() {
                        HFC.DisplaySuccess("Description successfully changed");
                        srv.IsBusy = false;
                    }, error);
                }
            }

            srv.SaveHint = function (jobId, hint) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/hint', { Hint: hint }).then(function () {
                    HFC.DisplaySuccess("Hint successfully changed");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.SaveSidemark = function (jobId, sidemark) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/sidemark', { SideMark: sidemark }).then(function () {
                    HFC.DisplaySuccess("Sidemark successfully changed");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.ShowTerritorySelectDialog = function() {
            $("#territoryModal").modal("show");
        };

        srv.ConvertToSale = function (job) {
            if (!job.TerritoryId) {
                srv.CurrentJob = job;
                srv.ShowTerritorySelectDialog();

                // we need to revert back status changes
                $http.get('/api/jobs/' + job.JobId + '/jobStatus', {}).then(function (response) {
                    job.JobStatusId = response.data.JobStatusId;
                }, error);

                return;
            }

            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + job.JobId + '/convertToSale', {}).then(function (response) {
                    HFC.DisplaySuccess("Job successfully converted to sale");
                    srv.IsBusy = false;
                    job.SaleMade = true;
                    job.ContractedOnUtc = response.data.ContractedOnUtc;
                    job.JobStatusId = response.data.JobStatusId;
                    job.InvoiceButtonText = "Create Invoice";

                }, error);
            }
        }

        srv.SaveTerritoryAndConvertToSale = function (job, territoryId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + job.JobId + '/territory', { TerritoryId: territoryId }).then(function () {
                    HFC.DisplaySuccess("Territory successfully changed");
                    srv.IsBusy = false;

                    srv.ConvertToSale(job);

                    $("#territoryModal").modal("hide");
                    
                }, error);
            }
        }

        srv.SaveConcept = function (jobId, conceptId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/concept', { Id: conceptId }).then(function () {
                    HFC.DisplaySuccess("Concept successfully changed");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.SaveTerritory = function (jobId, territoryId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/territory', { TerritoryId: territoryId }).then(function () {
                    HFC.DisplaySuccess("Territory successfully changed");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.SaveSalesAgent = function (jobId, personId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/salesagent', { PersonId: personId }).then(function (response) {
                    var salesagent = response.data;
                    HFC.DisplaySuccess("Sales agent successfully changed");
                    angular.forEach(srv.Jobs, function (j) {
                        if (j.JobId == jobId) {
                            j.SalesPerson = salesagent;
                        }
                    });            
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.SaveInstaller = function (jobId, personId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/installer', { PersonId: personId }).then(function () {
                    HFC.DisplaySuccess("Installer successfully changed");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.SaveStatus = function (jobId, statId, job) {
            if (!srv.IsBusy) {
                if (srv.JobStatusesAfterSaleMade.indexOf(statId) >= 0 && !job.SaleMade) {
                    return srv.ConvertToSale(job);
                }

                if (srv.JobStatusesAfterQuoteGiven.indexOf(statId) >= 0) {
                    job.QuoteGiven = true;
                    //job.QuoteDateUTC = new Date();
                    //srv.SaveQuoteDate(jobId, job.QuoteDateUTC);
                }

                srv.IsBusy = true;
                $http.put('/api/jobs/' + jobId + '/status', { Id: statId }).then(function (response) {
                    HFC.DisplaySuccess("Job status successfully changed");
                    srv.IsBusy = false;

                    job.CurrentJobStatusId = job.JobStatusId;
                    job.ContractedOnUtcRequired = false;
                }, function (res) {
                    job.ContractedOnUtcRequired = true;
                    job.JobStatusId = job.CurrentJobStatusId;
                    error(res);
                });
            }
        }
        
        srv.AddJob = function (leadId, callback) {
            if (!srv.IsBusy && leadId) {
                srv.IsBusy = true;
                $http.get('/api/leads/' + leadId + '/jobs/').then(
                function (res) {
                    
                    res.data.job.CreatedOnUtc = new XDate(res.data.job.CreatedOnUtc, true);

                    if (callback) {
                        callback(res.data.job);
                    }

                    if (!res.data.job.SaleMade) {
                        res.data.job.InvoiceButtonText = "Convert to Sale";
                    } else {
                        res.data.job.InvoiceButtonText = (!res.data.job.Invoices.length > 0) ? 'Create Invoice' : 'View Invoice';
                    }

                    var qandaclone = angular.copy(res.data.QandA);

                    //merge jobquestionanswers to QandA so we can loop through to render
                    if (res.data.job.JobQuestionAnswers) {
                        angular.forEach(res.data.job.JobQuestionAnswers, function (ans) {
                            var exist = $.grep(qandaclone, function (question) {
                                return ans.QuestionId == question.QuestionId;
                            });
                            if (exist) {
                                exist[0].Answer = ans.Answer;
                                exist[0].AnswerId = ans.AnswerId;
                                exist[0].IsChecked = (ans.Answer || "") != "";
                            }
                        });
                    }

                    res.data.job.JobQuestionAnswers = qandaclone;

                    srv.Jobs.unshift(res.data.job);
                    setTimeout(function () {
                        var top = $('#job_'+res.data.job.JobId).parent().parent().offset().top;
                        window.scrollTo(0, top);
                    }, 1000);
                    srv.IsBusy = false;
                }, error);
            }
        };

        srv.Delete = function (job) {
            var doDelete = function() {
                srv.IsBusy = true;
                $http.delete('/api/jobs/' + job.JobId).then(function (res) {
                    remove(job);
                    $rootScope.$broadcast('reload.purchaseOrders');
                    HFC.DisplaySuccess("Job #" + job.JobNumber + " deleted");
                    srv.IsBusy = false;
                }, error);
            }

            var ask = function (message) {
                bootbox.dialog({
                    message: message,
                    title: "Warning",
                    buttons: {
                        danger: {
                            label: "Yes, delete",
                            className: "btn-danger",
                            callback: function () {
                                doDelete();
                            }
                        },
                        main: {
                            label: "Cancel",
                            className: "btn-default",
                            callback: function () {

                            }
                        }
                    }
                });
            };

            $http.get('/api/jobs/' + job.JobId + '/orderstatus').then(function(res) {
                if (res.data.sent) {
                    ask(window.localization.ConfirmationJobWithSentItemsDeletion);
                } else if (res.data.ordered) {
                    ask(window.localization.ConfirmationJobWithOrderedItemsDeletion);
                } else {
                    ask("Do you want to continue?");
                }
            });
         };

        srv.SendThankYouLetter = function (jobId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.post('/api/jobs/' + jobId + '/sendThankYouLetter').then(function (response) {
                    HFC.DisplaySuccess("Email message successfully sent");
                    srv.IsBusy = false;
                }, function (res) {
                    error(res);
                });
            }
        }

        srv.SendEmailTemplate2 = function (jobId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.post('/api/jobs/' + jobId + '/SendEmailTemplate2').then(function (response) {
                    HFC.DisplaySuccess("Email message successfully sent");
                    srv.IsBusy = false;
                }, function (res) {
                    error(res);
                });
            }
        }

        srv.SendEmailTemplate3 = function (jobId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.post('/api/jobs/' + jobId + '/SendEmailTemplate3').then(function (response) {
                    HFC.DisplaySuccess("Email message successfully sent");
                    srv.IsBusy = false;
                }, function (res) {
                    error(res);
                });
            }
        }

        srv.PrintLeadSheet = function (jobId) {
            console.log("print service");
        }

    }]).controller('TerritoryController', ['$scope', 'JobService', function ($scope, JobService) {
        $scope.JobService = JobService;
    }])

    .directive('hfcTerritoryModal', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/templates/NG/territory-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            controller: 'TerritoryController'
        }
    }]);

})(window, document);

