﻿/**
 * hfc.quote.item.service module  
 * a simple module for handling quote item
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.quote.item.service', [])

    .service('QuoteItemService', ['$http', 'QuoteSaleAdjService', '$filter', function ($http, QuoteSaleAdjService, $filter) {
        var srv = this;

        srv.IsBusy = false;
        
        srv.ProductTypes = [];
        srv.ProductManufacturers = [];
        srv.ProductItems = [];
        srv.ProductList = [];
        srv.ProductStyles = [];

        srv.ProductCategories = [];
        srv.ProductOptions = {};
        srv.sessionId = '';
        srv.styleId = '';
        srv.AllToggled = true;
        srv.CheckedCategoryIds = [];
        srv.ProductOptions.Options = [];

        srv.CustomProductTypes = [];
        srv.CustomManufacturers = [];
        srv.ProductAdditInfo = '';
        srv.InEditMode = false;

        srv.OptionItem = function (val) {
            var opt = this;
            opt.OptionId = val.OptionId || 0;
            opt.Name = val.Name;
            opt.DefaultValue = val.DefaultValue || val.Value;
            opt.Value = val.Value;
            opt.IsList = val.IsList;
            opt.IsReadOnly = val.IsReadOnly;
            opt.MinValue = val.MinValue;
            opt.MaxValue = val.MaxValue;
            opt.AffectsPrice = val.AffectsPrice;
            opt.AvailableOptions = val.AvailableOptions;
            opt.IsVisible = val.IsVisible;
            opt.IsHidden = !val.IsVisible && val.ProcessIfHidden;

            opt.HasError = function () {
                var hasError = false;
                //This is a text input and has a min or max value so we know its a number
                if (!val.IsList && (opt.MinValue > 0 || opt.MaxValue > 0)) {
                    var fltValue = parseFloat(opt.Value);
                    if (isNaN(fltValue))
                        hasError = true;
                    else if ((opt.MinValue > 0 && fltValue < opt.MinValue) || (opt.MaxValue > 0 && fltValue > opt.MaxValue))
                        hasError = true;
                }
                return hasError;
            };

            opt.IsChanged = function () {
                return $.trim(val.DefaultValue) != $.trim(val.Value);
            }

        }

        function error(res) {
            srv.IsBusy = false;
            HFC.DisplayAlert(res.statusText);
        }

        srv.initService = function (prodTypeId, mfgId, sucess) {
            var promise;
            promise = $http.get('/api/lookup/0/products/');
            promise.then(function (res) {
                if (sucess)
                {
                    srv.ProductList = res.data;
                    srv.GetProductTypes();
                    if (prodTypeId != 0 && mfgId != 0) {
                        srv.GetManufacturers(prodTypeId);
                        srv.GetProducts(mfgId, prodTypeId);
                    }
                    sucess();
                }
            });
        };

        srv.initCustomData = function (success) {
            var promise;
            promise = $http.get('/api/lookup/0/customproducts/');
            promise.then(function (res) {
                if (success) {
                    srv.CustomProductTypes = res.data.ProductTypes;
                    srv.CustomManufacturers = res.data.Manufacturers;
                    success();
                }
            });
        }

        srv.GetProductTypes = function () {
            var typeList = [];
            var typeValid = {};

            $.each(srv.ProductList, function (index, product) {
                if (!typeValid[product.ProductType.ProductTypeId]) {
                    typeValid[product.ProductType.ProductTypeId] = true;
                    typeList.push(product.ProductType);
                }
            });

            srv.clearOptions();
            typeList.sort(srv.SortProductTypes);
            srv.ProductTypes = typeList;
        };
        
        srv.SortProductTypes = function (a, b) {

            if (a.TypeName < b.TypeName)
                return -1;
            else if (a.TypeName > b.TypeName)
                return 1;
            else
                return 0;
        }
        srv.GetManufacturers = function (productTypeId) {
            var mfgList = [];
            var mfgValid = {};

            $.each(srv.ProductList, function (index, product) {
                if (product.ProductType.ProductTypeId == productTypeId && !mfgValid[product.Manufacturer.ManufacturerId]) {
                    mfgValid[product.Manufacturer.ManufacturerId] = true;
                    mfgList.push(product.Manufacturer);
                }
            });
            srv.ProductAdditInfo = '';
            srv.clearOptions();
            mfgList.sort(srv.SortManufacturers);
            srv.ProductManufacturers = mfgList;
        };

        
        srv.SortManufacturers = function (a, b) {

            if (a.Name < b.Name)
                return -1;
            else if (a.Name > b.Name)
                return 1;
            else
                return 0;
        }

        srv.GetProducts = function (mfgId, productTypeId) {
            srv.clearOptions();
            srv.ProductAdditInfo = '';
            srv.ProductItems = $.grep(srv.ProductList, function (product) {
                return product.ProductType.ProductTypeId == productTypeId && product.Manufacturer.ManufacturerId == mfgId;
            });
        };

        srv.SortManufacturers = function (a, b) {

            if (a.Name < b.Name)
                return -1;
            else if (a.Name > b.Name)
                return 1;
            else
                return 0;
        }

        srv.GetProductStyles = function (productGuid) {
            srv.GetProductAdditInfo(productGuid)
            $http.get('/api/lookup/' + productGuid + '/productstyles/').success(function (res) {
                srv.ProductStyles = res;
            });
        };

        srv.GetProductAdditInfo = function (productGuid) {
            var product = srv.ProductItems.filter(function (prod) {
                return prod.ProductGuid == productGuid;
            })
            if (product.length>0) {
                srv.ProductAdditInfo = 'Rev:' + product[0].ProductRev + '-' + $filter('date')(product[0].ProductLastUpdated, "MM/dd/yyyy");
            }
        };

        srv.GetCategories = function () {
            $http.get('/api/lookup/0/category/').success(function (res) {
                srv.ProductCategories = res;
            });
        };

        srv.GetProductOptions = function (jobItem, productGuid, styleId, jobItemId, sessionId, clone) {
            srv.IsBusy = true;
            //this can't use async extend since we need to update the options with the saved option values
            $http.get('/api/pesquotes/' + productGuid + '/productoptions/', { params: { jobitemid: jobItemId, styleid: styleId, sessionid: sessionId } }).success(function (res) {

                srv.ProductOptions.Messages = res.Messages;
                srv.ProductOptions.Attachments = res.Attachments;
                srv.ProductOptions.Images = res.Images;
                srv.ProductOptions.JobberPrice = res.JobberPrice;
                srv.ProductOptions.JobberDiscountPercent = res.JobberDiscountPercent;
                srv.ProductOptions.MarginPercent = res.MarginPercent;
                //update quote job item
                srv.sessionId = res.SessionId;
                srv.ProductOptions.Options = [];

                 angular.forEach(res.Options, function (val, index) {
                    srv.ProductOptions.Options.push(new srv.OptionItem(val));
                });

                if (jobItem.Options.length ==  srv.ProductOptions.Options.length) {
                    $.each(jobItem.Options, function (index, val) {
                        srv.ProductOptions.Options[index].Value = val.Value;
                    });
                }
                else {
                    jobItem.JobberPrice = res.JobberPrice;
                    jobItem.MarginPercent = res.MarginPercent;
                    jobItem.UnitCost = HFC.evenRound(res.JobberPrice * (1 - (res.JobberDiscountPercent / 100.0)), 4);
                    jobItem.CostSubtotal = jobItem.UnitCost * jobItem.Quantity;
                    jobItem.SalePrice = HFC.evenRound(jobItem.UnitCost / (1 - (res.MarginPercent / 100.0)), 4);
                    jobItem.Subtotal = jobItem.SalePrice * jobItem.Quantity;
                }


                srv.IsBusy = false;
                if (clone) {
                    srv.cleanClone();
                }
            });
        };

        srv.GetCustomOptions = function (jobItem, productGuid, styleId, jobItemId, clone) {
            srv.IsBusy = true;

            //this can't use async extend since we need to update the options with the saved option values
            var id = jobItemId || 0;
            $http.get('/api/pesquotes/' + id +'/customoptions/', { params: { guid: productGuid, jobitemid: jobItemId, styleid: styleId } }).success(function (res) {

                srv.ProductOptions.JobberPrice = res.JobberPrice;
                srv.ProductOptions.JobberDiscountPercent = res.JobberDiscountPercent;
                srv.ProductOptions.MarginPercent = res.MarginPercent;
                //update quote job item
                srv.ProductOptions.Options = [];

                angular.forEach(res.Options, function (val, index) {
                    srv.ProductOptions.Options.push(new srv.OptionItem(val));
                });

                /*
                if (jobItem != undefined) {
                    jobItem.JobberPrice = res.JobberPrice;
                    jobItem.MarginPercent = res.MarginPercent;
                    jobItem.UnitCost = HFC.evenRound(res.JobberPrice * (1 - (res.JobberDiscountPercent / 100.0)), 4);
                    jobItem.CostSubtotal = jobItem.UnitCost * jobItem.Quantity;
                    jobItem.SalePrice = HFC.evenRound(jobItem.UnitCost / (1 - (res.MarginPercent / 100.0)), 4);
                    jobItem.Subtotal = jobItem.SalePrice * jobItem.Quantity;
                }
                */

                srv.IsBusy = false;

                if (clone) {
                    srv.cleanClone();
                }

            });
        };

        srv.UpdateOptions = function (jobItem, opt, productGuid, styleId) {
            if (!srv.IsBusy) {

                if (opt.Name != 'Room Location' && opt.Name != 'Remarks') {

                    srv.IsBusy = true;

                    $http.put('/api/pesquotes/' + productGuid + '/jobitemoption', {
                        optionId: opt.OptionId,
                        name: opt.Name,
                        value: opt.Value,
                        styleId: styleId,
                        sessionId: srv.sessionId
                    }).success(function (res) {

                        srv.IsBusy = false;

                        var tempOptions = srv.ProductOptions.Options;
                        srv.ProductOptions.Options = [];

                        angular.forEach(res.Options, function (val, index) {

                            if (val.Name == 'Room Location' || val.Name == 'Remarks'){
                                var inject = $.grep(tempOptions, function (ele, idx) {
                                    return ele.Name == val.Name;
                                })[0];
                                val.Value = inject.Value;
                            }

                            srv.ProductOptions.Options.push(new srv.OptionItem(val));
                        });
                       
                        var messages = [];
                        for (var i = 0; i < srv.ProductOptions.Options.length; i++) {
                            var option = srv.ProductOptions.Options[i];

                            if (option.HasError()) {
                                messages.push("The " + option.Name + " for this product must be between " + option.MinValue + " and " + option.MaxValue + ".");
                            }
                        }

                        if (srv.ProductOptions.Messages.length == 0 && messages.length > 0) {
                            srv.ProductOptions.Messages = messages;
                        }

                        srv.ProductOptions.Messages = messages;
                        //srv.ProductOptions.Images = res.Images;
                        //srv.ProductOptions.Attachments = res.Attachments;
                        srv.ProductOptions.JobberPrice = jobItem.JobberPrice = res.JobberPrice;
                        srv.ProductOptions.JobberDiscountPercent = res.JobberDiscountPercent;
                        srv.ProductOptions.MarginPercent = jobItem.MarginPercent = res.MarginPercent;
                        srv.sessionId = res.SessionId;
                        jobItem.UnitCost = HFC.evenRound(res.JobberPrice * (1 - (res.JobberDiscountPercent / 100.0)), 4);
                        jobItem.CostSubtotal = jobItem.UnitCost * jobItem.Quantity;
                        jobItem.SalePrice = HFC.evenRound(jobItem.UnitCost / (1 - (res.MarginPercent / 100.0)), 4);
                        jobItem.Subtotal = jobItem.SalePrice * jobItem.Quantity;
                        
                    });
                }
                if (opt.Name == "Width") {
                    window.OptionWidth = opt.Value || 0;
                } else if (opt.Name == "Height")
                    window.OptionHeight = opt.Value || 0;

                
            }

            srv.IsBusy = false;
        };

        srv.GetAttachmentName = function (fileName, index) {
            var str = fileName.split('.');
            if (str.length < 1) {
                return str
            }
            else {
                var extention = str[str.length - 1];
                return extention.toUpperCase() + ' ' + (index + 1);
            }
        }

        srv.ToggleOptions = function (productOption) {
            angular.forEach(productOption.Options, function (val, index) {
                if (!val.AffectsPrice && val.Name != "Room Location")
                    val.IsVisible = !srv.AllToggled;
            });

            srv.AllToggled = !srv.AllToggled;
        };

        srv.clearOptions = function () {

            if (srv.ProductOptions.Options.length > 0) {
                srv.ProductOptions.Options = [];
                srv.ProductOptions.Messages = [];
            }
        }

        srv.cleanClone = function () {

            angular.forEach(srv.ProductOptions.Options, function (val, index) {
                if (val.Name == 'Room Location' || val.Name == 'Width' || val.Name == 'Height') {
                    val.Value = '';
                }
            });
        }

        srv.Delete = function (item, quote, success) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                item.QuoteLastUpdated = quote.LastUpdated;

                $http({ method: 'DELETE', url: '/api/pesquotes/' + item.QuoteId + '/jobitems/', data: item, headers: { 'Content-Type': 'application/json' } })
                .then(function (res) {
                    if (success)
                        success(res.data);
                    HFC.DisplaySuccess("Quote item deleted");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.Save = function (item, quote, errors, success) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                var productType, manufName, productName, styleName;

                item.QuoteLastUpdated = quote.LastUpdated;
                item.Errors = errors;

                if (!item.isCustom) {
                    //retrieve the product and category names from Ids
                    productType = $.grep(srv.ProductTypes, function(t) {
                        return t.ProductTypeId == item.ProductTypeId;
                    });

                    manufName = $.grep(srv.ProductManufacturers, function(m) {
                        return m.ManufacturerId == item.ManufacturerId;
                    });

                    productName = $.grep(srv.ProductList, function(p) {
                        return p.ProductGuid == item.ProductGuid;
                    });

                    styleName = $.grep(srv.ProductStyles, function(s) {
                        return s.Id == item.StyleId;
                    });

                    item.ProductType = productType[0].TypeName;
                    item.Manufacturer = manufName[0].Name;
                    item.ProductName = productName[0].Name;
                    item.Style = styleName[0].Name;
                    item.Options = [];

                    item.SessionId = srv.sessionId;
                } else {
                    //retrieve the product and category names from Ids
                    productType = $.grep(srv.CustomProductTypes, function(t) {
                        return t.ProductTypeId == item.ProductTypeId;
                    });

                    if (item.ManufacturerId) {
                        manufName = $.grep(srv.CustomManufacturers, function(m) {
                            return m.ManufacturerId == item.ManufacturerId;
                        });
                    }

                    if (productType && productType.length > 0) {
                        item.ProductType = productType[0].TypeName;
                    }
                    
                    if (manufName && manufName.length > 0) {
                        item.Manufacturer = manufName[0].Name;
                    }

                }

                if (srv.ProductOptions.Options && srv.ProductOptions.Options.length) {
                    item.Options.length = 0;
                    for (var i = 0; i < srv.ProductOptions.Options.length; i++) {
                        item.Options.push({
                            OptionId: srv.ProductOptions.Options[i].OptionId,
                            Name: srv.ProductOptions.Options[i].Name,
                            Value: srv.ProductOptions.Options[i].Value
                        });
                    }
                }

                var promise;
                if (item.JobItemId)
                    promise = $http.put('/api/pesquotes/' + item.JobItemId + '/jobitems/', item);
                else
                    promise = $http.post('/api/pesquotes/' + item.QuoteId + '/jobitems/', item);

                promise.then(function (res) {
                    if (success) {
                        if (item.isCustom) {
                            item.ProductGuid = res.data.ProductGuid;
                            item.StyleId = res.data.StyleId;
                        }
                        success(res.data);
                    }
                    HFC.DisplaySuccess("Quote item saved");
                    srv.IsBusy = false;
                    item.InEditMode = false;
                }, error);
            }
        }

        srv.Edit = function (jobItem, clone) {

            srv.clearOptions();

            srv.ProductOptions.Messages = [];
            srv.ProductOptions.Attachments = [];
            srv.ProductOptions.Images = [];

            if (jobItem.isCustom) {
                srv.initCustomData(function () {
                    srv.GetCustomOptions(jobItem, jobItem.ProductGuid, jobItem.StyleId, jobItem.JobItemId, clone);
                });
            } else {

                if (srv.ProductList.length == 0) {
                    srv.initService(jobItem.ProductTypeId, jobItem.ManufacturerId, function () {

                        srv.GetProductStyles(jobItem.ProductGuid);
                        srv.GetProductOptions(jobItem, jobItem.ProductGuid, jobItem.StyleId, jobItem.JobItemId, '', clone);
                    });
                }
                else {

                    srv.GetProductTypes();
                    srv.GetManufacturers(jobItem.ProductTypeId);
                    srv.GetProducts(jobItem.ManufacturerId, jobItem.ProductTypeId);

                    srv.GetProductStyles(jobItem.ProductGuid);
                    srv.GetProductOptions(jobItem, jobItem.ProductGuid, jobItem.StyleId, jobItem.JobItemId, '', clone);
                }
            }
        };
      
        srv.AddCustom = function (quote) {
            if (!srv.InEditMode) {

                srv.InEditMode = true;

                srv.ProductOptions.Options = [];
                srv.ProductOptions.Messages = [];
                srv.ProductOptions.Attachments = [];
                srv.ProductOptions.Images = [];

                srv.GetCustomOptions(undefined, 0, 0, 0);

                quote.JobItems.push({
                    QuoteId: quote.QuoteId,
                    InEditMode: true,
                    isCustom: true,
                    Quantity: 1,
                    IsTaxable: true,
                    SalePrice: 0,
                    JobberPrice: 0,
                    Subtotal: 0,
                    DiscountPercent: 0,
                    DiscountAmount: 0,
                    MarginPercent: 0,
                    CostSubtotal: 0,
                    UnitCost: 0,
                    Options: [],
                });
            } else {
                alert('Can only edit 1 line item.');
            }
        }

        srv.Add = function (quote) {

            if (!srv.InEditMode) {


                srv.InEditMode = true;
                srv.ProductOptions.Options = [];
                srv.ProductOptions.Messages = [];
                srv.ProductOptions.Attachments = [];
                srv.ProductOptions.Images = [];

                quote.JobItems.push({
                    QuoteId: quote.QuoteId,
                    InEditMode: true,
                    isCustom: false,
                    Quantity: 1,
                    IsTaxable: true,
                    SalePrice: 0,
                    JobberPrice: 0,
                    Subtotal: 0,
                    DiscountPercent: 0,
                    DiscountAmount: 0,
                    MarginPercent: 70,
                    CostSubtotal: 0,
                    UnitCost: 0,
                    Options: [],
                });
            } else {
                alert('Can only edit 1 line item.');
            }
        }


        srv.UpdateSortOrder = function(quote) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                var jobItems = [];

                for (var i = 0; i < quote.JobItems.length; i++) {
                    quote.JobItems[i].InEditMode = false;
                    jobItems.push(quote.JobItems[i].JobItemId);
                }

                $http({ method: 'POST', url: '/api/quotes/' + quote.QuoteId + '/sortorder/', data: jobItems, headers: { 'Content-Type': 'application/json' } })
                .then(function (res) {
                    srv.IsBusy = false;
                }, error);
            }
        } 

    }]);

})(window, document);

