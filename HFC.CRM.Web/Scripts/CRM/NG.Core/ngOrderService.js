﻿/**
 * hfc.order.service module  
 * a simple module for handling quote item
 * @author Babu Goli
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.order.service', [])

    .service('OrderService', ['$http', '$rootScope', 'HFCService', function ($http, $rootScope, HFCService) {
        var srv = this;

        srv.IsBusy = false;
        srv.OrderReview = {};

        srv.AddOrder = function (quoteId) {
            if (quoteId && !srv.IsBusy && HFCService.FranchiseIsSolaTechEnabled) {

                srv.IsBusy = true;
                $http.get('/api/purchaseOrders/' + quoteId + '/addorder/').success(function (res) {
                    HFC.DisplaySuccess("quote is added to order");
                    $rootScope.$broadcast('reload.purchaseOrders');
                    $rootScope.$broadcast('reload.orderedJobItems');
                });
                srv.IsBusy = false;
            }
        };

        srv.getOrderReview = function (orderId, success) {
            if (orderId && !srv.IsBusy)
            {
                srv.IsBusy = true;
                $http.get('/api/purchaseOrders/' + orderId + '/orderreview/').success(function (res) {
                    srv.OrderReview = res;

                    angular.forEach(srv.OrderReview.OrderItems, function (val, index) {
                        val.visible = false;
                    });

                    if (success)
                        success(srv.OrderReview);
                });
                srv.IsBusy = false;
            }
        };

    }]);
 
})(window, document);

