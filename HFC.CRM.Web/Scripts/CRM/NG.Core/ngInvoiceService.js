﻿/**
 * hfc.invoice module  
 * a simple module for handling invoice
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.invoice.service', [])
        .service('InvoiceService', [
            '$http', '$window', '$location', 'LeadNoteService', function ($http, $window, $location, LeadNoteService) {

                var srv = this;


                srv.Get = function (invoiceId, success) {
                    if (!isNaN(invoiceId) && invoiceId > 0 && success != null) {
                        $http.get('/api/invoices/' + invoiceId).then(success, function (response) {
                            HFC.DisplayAlert(response.statusText);
                        });
                    }
                }

                srv.Delete = function (invoiceId, success) {
                    if (!isNaN(invoiceId) && invoiceId > 0 && success != null) {
                        $http.delete('/api/invoices/' + invoiceId).then(success, function (response) {
                            if (success) {
                                success(response);
                            }
                        });
                    }
                }

                srv.InvoiceId = 0;

                srv.Payments = function (quote, job, jobService) {
                    if (!job.SaleMade) {
                        return jobService.ConvertToSale(job);
                    }

                    if (job.Invoices.length == 0) {
                        if (!isNaN(quote.QuoteId) && quote.QuoteId > 0 && !isNaN(job.JobId) && job.JobId > 0) {
                            $http.post('/api/invoices/Post', JSON.stringify({ 'quoteId': quote.QuoteId, 'jobId': job.JobId }))
                                .then(function (response) {
                                    srv.InvoiceId = response.data.InvoiceId;
                                    //  job.Invoices.push(response.data);
                                    HFC.DisplaySuccess(response.statusText);
                                    job.InvoiceButtonText = 'View Invoice';
                                });

                        }
                    }
                    if (job.Invoices.length > 0 || job.InvoiceButtonText == 'View Invoice') {
                        var _InvoiceId = (typeof job.Invoices[0] !== 'undefined' && typeof job.Invoices[0].InvoiceId !== 'undefined')
                            ? job.Invoices[0].InvoiceId : srv.InvoiceId;
                        window.location.href = HFC.Util.BaseURL() + '#/invoices/detail/' + _InvoiceId + '/';

                    }
                }
                srv.Void = function (invoice) {
                    if (!isNaN(invoice.InvoiceId) && invoice.InvoiceId > 0) {
                        $http.delete('/api/invoices/' + invoice.InvoiceId).then(
                          function (response) {
                              var oldURL = document.referrer;
                              if (window.history.length > 1) {
                                  window.history.back();
                              }
                              else {
                                  window.close();
                              }
                          }
                          , function (response) {
                              HFC.DisplayAlert(response.statusText);
                          });
                    }
                }
            }
        ]);


})(window, document);

