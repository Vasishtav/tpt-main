﻿
/**
 * hfc.email.service module  
 * a simple module for handling email
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.email.service', ['hfc.core.service', 'ui.select2', 'ui.tinymce', 'summernote', 'hfc.lead.service'])

    .service('EmailService', ['$http', 'HFCService', 'LeadService', 'PersonService', function ($http, HFCService, LeadService, PersonService) {
        var srv = this;

        srv.IsBusy = false;
        srv.SendUrl = "";
        srv.ToAddresses = [];
        srv.BccAddresses = [];
        srv.Subject = "";
        srv.Body = "";
        srv.LeadService = LeadService;
        srv.PersonService = PersonService;
        srv.AttachmentLink = "";
        srv.AttachmentText = "";
        srv.PersonPrimaryEmail = "";
        srv.GetPersonPrimaryEmail = function (personId) {
            $http.get('/api/users/' + personId + "/getPerson/").then(function (response) {
                var person = response.data.Person;
                if (person) {
                    srv.PersonPrimaryEmail = person.PrimaryEmail;
                }
                else {
                    if (HFCService.CurrentUser) {
                        srv.PersonPrimaryEmail = HFCService.CurrentUser.Email;
                    }
                }
            });
        }

        srv.EmailQuote = function (quote, job, customer) {
            if (customer) {
                if (job) {
                    var leadId = job.LeadId;

                    srv.LeadService.Get(leadId, function (leadServiceData) {
                        var lead = leadServiceData.Lead;
                        if (lead) {
                            var person = srv.PersonService.GetPerson(lead.PersonId);
                            if (person) {
                                var email = person.PrimaryEmail
                                srv.PersonId = lead.PersonId;
                                if (email != '') {
                                    srv.ToAddresses = [];
                                    srv.ToAddresses.push(email);
                                }
                            }
                        }
                    });

                }

                if (HFCService.CurrentUser) {
                    srv.GetPersonPrimaryEmail(HFCService.CurrentUser.PersonId);
                }

                srv.SendUrl = "/email/quote/" + quote.QuoteId;
                //if (customer.PrimaryEmail) {
                //    srv.ToAddresses = [customer.PrimaryEmail];
                //}
                srv.BccAddresses = [];
                srv.Subject = "Quote for " + customer.FullName;
                srv.AttachmentLink = "/export/quote/" + quote.QuoteId;
                srv.AttachmentText = "Quote " + job.JobNumber + "-" + quote.QuoteNumber + ".pdf";
                if (HFCService.CurrentUser) {
                    if (HFCService.CurrentUser.EmailSignature)
                        srv.Body = "<br/>" + HFCService.CurrentUser.EmailSignature;
                }
                $("#emailModal").modal("show");
            }
        }

        srv.EmailLeadSheet = function (job) {
            if (job.SalesPersonId > 0) {
                if (HFCService.CurrentUser) {
                    srv.GetPersonPrimaryEmail(HFCService.CurrentUser.PersonId);
                }
                
                console.log(job.SalesPerson.PrimaryEmail);
                srv.SendUrl = "/email/leadsheet/" + job.JobId;
                if (job) {
                    var leadId = job.LeadId;

                    srv.LeadService.Get(leadId, function (leadServiceData) {
                        var lead = leadServiceData.Lead;
                        if (lead) {
                            var person = srv.PersonService.GetPerson(lead.PersonId);
                            if (person) {
                                var email = person.PrimaryEmail
                                srv.PersonId = lead.PersonId;
                                if (email != '') {
                                    srv.ToAddresses = [];
                                    //srv.ToAddresses.push(email);
                                }
                            }
                        }
                    });

                }
                //if (job.SalesPerson.PrimaryEmail) {
                //    srv.ToAddresses = [job.SalesPerson.PrimaryEmail];
                //}
                srv.BccAddresses = [];
                srv.Subject = "Lead Sheet for Job Number #" + job.JobNumber;
                srv.AttachmentLink = "/export/LeadSheet/" + job.JobId;
                srv.AttachmentText = "LeadSheet for Job Number #" + job.JobNumber + ".pdf";            
                $("#emailModal").modal("show");
            }
        }

        srv.EmailInvoice = function (invoice, customer) {
            if (customer) {
                if (HFCService.CurrentUser) {
                    srv.GetPersonPrimaryEmail(HFCService.CurrentUser.PersonId);
                }

                srv.SendUrl = "/email/invoice/" + invoice.InvoiceId;
                var job = invoice.Job;
                if (job) {
                    var leadId = job.LeadId;

                    srv.LeadService.Get(leadId, function (leadServiceData) {
                        var lead = leadServiceData.Lead;
                        if (lead) {
                            var person = srv.PersonService.GetPerson(lead.PersonId);
                            if (person) {
                                var email = person.PrimaryEmail
                                srv.PersonId = lead.PersonId;
                                if (email != '') {
                                    srv.ToAddresses = [];
                                    srv.ToAddresses.push(email);
                                }
                            }
                        }
                    });

                }
                //if (customer.PrimaryEmail) {
                //    srv.ToAddresses = [customer.PrimaryEmail];
                //}
                srv.BccAddresses = [];
                srv.Subject = "Invoice for " + customer.FullName;
                srv.AttachmentLink = "/export/invoice/" + invoice.InvoiceId;
                srv.AttachmentText = "Invoice " + invoice.InvoiceNumber + ".pdf";
                if (HFCService.CurrentUser) {
                    if (HFCService.CurrentUser.EmailSignature)
                        srv.Body = "<br/>" + HFCService.CurrentUser.EmailSignature;
                }
                $("#emailModal").modal("show");
            }
        }

        srv.Send = function () {
            if (!srv.IsBusy && srv.SendUrl) {
                srv.IsBusy = true;

                var data = {
                    toAddresses: srv.ToAddresses,
                    bccAddresses: srv.BccAddresses,
                    subject: HFC.htmlEncode(srv.Subject),
                    body: HFC.htmlEncode(srv.Body),
                    attachment: srv.AttachmentLink
                };
                $http.post(srv.SendUrl, data).then(function (res) {
                    HFC.DisplaySuccess("Email successfully sent");
                    srv.IsBusy = false;
                    $("#emailModal").modal("hide");
                }, function (res) {
                    srv.IsBusy = false;
                    HFC.DisplayAlert(res.statusText);
                });
            }
        }
    }])

    .controller('EmailController', ['$scope', 'HFCService', 'EmailService', function ($scope, HFCService, EmailService) {
        $scope.HFCService = HFCService;
        $scope.EmailService = EmailService;

        var tags = $.map(HFCService.Users, function (usr) {
            var text = usr.FullName || usr.Email;
            if (usr.FullName)
                text += " <" + usr.Email + ">";
            return { id: usr.Email, text: text, FullName: usr.FullName }
        });
        $scope.SelectOption = {
            multiple: true,
            simple_tags: true,
            width: '100%',
            placeholder: "Recipients",
            tokenSeparators: [",", " ", ";"],
            maximumSelectionSize: 10,
            maximumInputLength: 256,
            minimumInputLength: 1,
            tags: tags,
            formatSelection: function (email) {
                return "<span title='" + email.id + "'>" + (email.FullName || email.id) + "</span>";
            },
            createSearchChoice: function (term) {
                if (HFC.Regex.EmailPattern.test(term))
                    return { id: term, text: term };
                else
                    return null;
            },
            query: function (query) {
                var data = { results: [] };
                $.each(tags, function () {
                    if (query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0) {
                        data.results.push(this);
                    }
                });
                return query.callback(data);
            }
        };
        $scope.TinyMCEOpt = {
            height: 350,
            resize: true,
            browser_spellcheck: true,
            forced_root_block: "div",
            plugins: [
                "advlist autolink lists link image hr charmap print preview anchor textcolor",
                "searchreplace visualblocks code",
                "insertdatetime table contextmenu paste"
            ],
            menu: {
                file: { title: 'File', items: 'print' },
                edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall searchreplace' },
                insert: { title: 'Insert', items: 'link image | hr' },
                view: { title: 'View', items: 'visualblocks visualaid preview' },
                format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | removeformat' },
                table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' }
            },
            toolbar: "styleselect | fontselect | fontsizeselect | forecolor backcolor bullist numlist"
        }
    }])

    .directive('hfcEmailModal', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/templates/NG/email-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            controller: 'EmailController'
        }
    }]);

})(window, document);

