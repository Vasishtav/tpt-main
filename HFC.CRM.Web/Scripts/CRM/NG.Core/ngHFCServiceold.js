﻿/**
 * hfc.core.service module  
 * a simple module for handling common core utility services
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';


    var cacheBustSuffix = HFC.Version;// Date.now();

    angular.module('hfc.core.service', ['hfc.core.service'])
    .constant("cacheBustSuffix", cacheBustSuffix)
    .service('HFCService', ['$http', function ($http) {
        var srv = this;

        srv.Users = [];
        srv.DisabledUsers = [];
        srv.AllUsers = [];
        srv.CurrentUser = null;
        srv.CurrentUserPermissions = [];
        srv.FranchiseCountryCode = [];
        srv.FranchiseIsSolaTechEnabled = false;

        srv.getUsers = function (id) {

            var result = srv.Users.slice();

            var found = $.grep(srv.Users, function (emp, i) {
                return emp.UserId == id || emp.PersonId == id || emp.Email == id;
            });


            if (found && found.length > 0) {
                return result;
            }

            found = $.grep(srv.DisabledUsers, function (emp, i) {
                return emp.UserId == id || emp.PersonId == id || emp.Email == id;
            });

            if (found && found.length > 0) {
                result.push(found[0]);
            }

            return result;
        }

        //for now we will only check user agent, but we can also add a check for the media break points as well
        var browser = {
            isAndroid: function () {
                return navigator.userAgent.match(/Android/i);
            },
            isBlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            isIOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            isOpera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            isWindows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            isMobile: function () {
                return (browser.isAndroid() || browser.isBlackBerry() || browser.isIOS() || browser.isOpera() || browser.isWindows());
            }
        };
        srv.IsMobile = browser.isMobile;

        srv.GetUser = function (personId) {
            var user = $.grep(srv.Users, function (u) {
                return u.PersonId == personId;
            });

            if (user)
                return user[0];
            else
                return null;
        };

        /*srv.SalesUsers = function () {
            return $.grep(srv.Users, function (u) {
                return u.InSales;
            });
        };*/

        srv.SalesUsers = function (id) {
            var users = srv.Users.slice();

            var result = $.grep(users, function (u) {
                return u.InSales;
            });

            var found = $.grep(result, function (emp, i) { // if result contains selected element - nothing to do
                return (emp.UserId == id || emp.PersonId == id || emp.Email == id);
            });

            if (found && found.length > 0) {
                return result;
            }

            found = $.grep(srv.DisabledUsers, function (emp, i) { // try to find missed selected element in disabled users
                return (emp.UserId == id || emp.PersonId == id || emp.Email == id) && emp.InSales;
            });

            if (found && found.length > 0) {
                result.push(found[0]);
            }

            return result;
        };

        srv.InstallerUsers = function (id) {
            var users = srv.Users.slice();

            var result = $.grep(users, function (u) {
                return u.InInstall;
            });

            var found = $.grep(result, function (emp, i) { // if result contains selected element - nothing to do
                return (emp.UserId == id || emp.PersonId == id || emp.Email == id);
            });

            if (found && found.length > 0) {
                return result;
            }

            found = $.grep(srv.DisabledUsers, function (emp, i) { // try to find missed selected element in disabled users
                return (emp.UserId == id || emp.PersonId == id || emp.Email == id) && emp.InInstall;
            });

            if (found && found.length > 0) {
                result.push(found[0]);
            }

            return result;
        };

        srv.GetUserLabel = function (user) {
            return '<span class="color-box color-' + (user.ColorId || "none") + '"></span> ' + (user.FullName || "");
        };

        srv.GetUserAvatar = function (personId) {
            var user = srv.GetUser(personId);
            if (user)
                return user.AvatarSrc;
            else
                return "/images/avatar/generic.png";
        }

        srv.GetPermission = function (name, subsection) {
            var permission = $.grep(srv.CurrentUserPermissions, function (p) {
                if (!subsection) {
                    return p.Name == name;
                } else {
                    return p.Name == name && p.Subsection == subsection;
                }
            });

            if (permission) {
                return permission[0];
            }
        };

        srv.GetQueryString = function (key) {
            var qstring = location.search.replace('?', '').split('&');
            //var QS = {}
            for (var i = 0; i < qstring.length; i++) {
                var keyval = qstring[i].split('=');
                if (keyval[0].toLowerCase() == key.toLowerCase())
                    return decodeURIComponent(keyval[1]);
                //QS[keyval[0]] = decodeURIComponent(keyval[1]);
            }
            return null;
        }

        srv.GetDateRange = function (range) {
            var endDate = new XDate(),
                startDate = new XDate();
            switch (range.toLowerCase()) {
                case "this week":
                    startDate.addDays(-startDate.getDay());
                    endDate = startDate.clone().addDays(6);
                    break;
                case "this month":
                    startDate.setDate(1);
                    endDate = startDate.clone().addMonths(1).addDays(-1);
                    break;
                case "this year":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addYears(1).addDays(-1);
                    break;
                case "last week":
                    startDate.addDays(-startDate.getDay() - 7);
                    endDate = startDate.clone().addDays(6);
                    break;
                case "last month":
                    startDate.setDate(1).addMonths(-1);
                    endDate = startDate.clone().addMonths(1).addDays(-1);
                    break;
                case "last year":
                    startDate.setDate(1).setMonth(0).addYears(-1);
                    endDate.setDate(1).setMonth(0).addDays(-1);
                    break;
                case "last 7 days":
                    startDate.addDays(-7); break;
                case "last 30 days":
                    startDate.addDays(-30); break;
                case "1st quarter":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "2nd quarter":
                    startDate.setDate(1).setMonth(3);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "3rd quarter":
                    startDate.setDate(1).setMonth(6);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "4th quarter":
                    startDate.setDate(1).setMonth(9);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "1st half":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addMonths(6).addDays(-1); break;
                case "2nd half":
                    startDate.setDate(1).setMonth(6);
                    endDate = startDate.clone().addMonths(6).addDays(-1); break;
                default:
                    startDate = null; endDate = null; break;
            }
            return { StartDate: startDate, EndDate: endDate };
        }

        //turn off async, we want to make sure we have list of users first, as other angular services will reference users list
        $.ajax({
            url: '/api/users/',
            async: true,
            success: function (data) {
                if (data != "") {
                    srv.Users = data.Users || [];
                    srv.DisabledUsers = data.DisabledUsers || [];
                    srv.AllUsers = srv.Users.concat(srv.DisabledUsers);
                    srv.CurrentUser = srv.GetUser(data.CurrentUser.PersonId);
                    if (srv.CurrentUser == null) {
                        srv.CurrentUser = data.CurrentUser.user;
                    }

                    srv.FranchiseCountryCode = data.FranchiseCountryCode;
                    srv.CurrentBrand = data.BrandId;
                    srv.FranchiseIsSolaTechEnabled = data.FranchiseIsSolaTechEnabled;
                }

            }
        });

        $.ajax({
            url: '/api/permissions/',
            async: true,
            success: function (data) {
                srv.CurrentUserPermissions = data;
            }
        });
        $.ajax({
            url: '/api/Timezones/0/GetTimezones',
            async: false,
            success: function (data) {
                srv.CurrentUserTimezone = data;
            }
        });
    }])
    .directive('charLimit', function () {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attributes) {
                var limit = $attributes.maxlength;

                $element.bind('keyup', function (event) {
                    var span = $element.parent().children('.text-muted');
                    span.html(limit - $element.val().length + ' left');
                });
            }
        };
    });

})(window, document);

