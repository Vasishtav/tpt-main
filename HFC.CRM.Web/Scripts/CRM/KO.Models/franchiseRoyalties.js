﻿HFC.Models.FranchiseRoyalties = function (model) {
    model = model || {};
    model.RoyaltyId = ko.observable(model.RoyaltyId);
    model.Name = ko.observable(model.Name);
    model.Amount = ko.observable(model.Amount);
    model.Percent = ko.observable(model.Percent);
    model.StartOnUtc = ko.observable(model.StartOnUtc);
    model.EndOnUtc = ko.observable(model.EndOnUtc);
    model.TerritoryId = ko.observable(model.TerritoryId);

    model.Template = ko.observable("FranchiseRoyalties-view");

model.toJS = function (data, prefix) {
    data = data || {};
    data[(prefix || "") + RoyaltyId] = model.RoyaltyId;
    data[(prefix || "") + Name] = model.Name;
    data[(prefix || "") + Amount] = model.Amount;
    data[(prefix || "") + Percent] = model.Percent;
    data[(prefix || "") + StartOnUtc] = model.StartOnUtc;
    data[(prefix || "") + EndOnUtc] = model.EndOnUtc;
    data[(prefix || "") + TerritoryId] = model.TerritoryId;

    return data;
}
model.toBaseJS = function (model) {
    var data = {
        RoyaltyId: model.RoyaltyId(),
        Name: model.Name(),
        Amount: model.Amount(),
        Percent: model.Percent(),
        StartOnUtc: model.StartOnUtc(),
        EndOnUtc: model.EndOnUtc(),
        TerritoryId: model.TerritoryId(),
    }
    return data;
}
return model;
}