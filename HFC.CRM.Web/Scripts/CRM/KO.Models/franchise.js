﻿
ko.bindingHandlers.disableClick = {
    init: function (element, valueAccessor) {

        $(element).click(function (evt) {
            if (valueAccessor())
                evt.preventDefault();
        });

    },

    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        ko.bindingHandlers.css.update(element, function () { return { disabled_anchor: value }; });
    }
};

HFC.Models.Franchise = function (model) {
    model = model || {};
    model.vmFranchise = new HFC.VM.Franchise();

    model.FranchiseId = ko.observable(model.FranchiseId);
    model.FranchiseGuid = ko.observable(model.FranchiseGuid);
    model.Name = ko.observable(model.Name);
    model.Code = ko.observable(model.Code);
    model.Website = ko.observable(model.Website);
    model.CreatedOnUtc = ko.observable(model.CreatedOnUtc);
    model.AddressId = ko.observable(model.AddressId);
    model.MailingAddressId = ko.observable(model.MailingAddressId);
    model.IsDeleted = ko.observable(model.IsDeleted);
    model.IsSuspended = ko.observable(model.IsSuspended);
    model.IsSolaTechEnabled = ko.observable(model.IsSolaTechEnabled);
    model.IsQBEnabled = ko.observable(model.IsQBEnabled);
    model.TimezoneCode = ko.observable(model.TimezoneCode);
    model.LogoImgUrlRelativePath = ko.observable(model.LogoImgUrlRelativePath);
    model.TollFreeNumber = ko.observable(model.TollFreeNumber);
    model.LocalPhoneNumber = ko.observable(model.LocalPhoneNumber);
    model.FaxNumber = ko.observable(model.FaxNumber);
    model.SupportEmail = ko.observable(model.SupportEmail);
    model.AdminEmail = ko.observable(model.AdminEmail);
    model.OwnerEmail = ko.observable(model.OwnerEmail);
    model.LastUpdatedUtc = ko.observable(model.LastUpdatedUtc);
    model.LastUpdatedByPersonId = ko.observable(model.LastUpdatedByPersonId);
    model.Address = new HFC.Models.Address(model.Address);
    model.MailingAddress = new HFC.Models.MailingAddress(model.MailingAddress);
    model.FranchiseOwners = ko.observableArray();
    model.Territories = ko.observableArray();
    model.RoyaltyProfiles = ko.observableArray();

    model.vmFranchiseRoyalties = new HFC.VM.FranchiseRoyalties(model.FranchiseId, model.Territories, model.RoyaltyProfiles);

    model.isRequiredAllFilled = ko.computed({
        read: function () {
             
            var hasEmail = true;
            var hasName, hasZipCode, hasMailingZipCode;
            $.each(model.FranchiseOwners(), function (i, owner) {
                if (owner.User.Person.PrimaryEmail() != null) {
                    if (owner.User.Person.PrimaryEmail().length == 0) {
                        hasEmail = false;
                    }
                }
                else
                {
                    hasEmail = false;
                }
            });

            var id = model.FranchiseId();

            if (model.Name() != null) {
                if (model.Name().length > 0) {
                    hasName = true;
                }
            }

            if (model.Address != null) {
                if (model.Address.ZipCode() != null && model.Address.ZipCode().length > 0) {
                    hasZipCode = true;
                }
            }

            if (model.MailingAddress.Address1() != null && model.MailingAddress.Address1().length>0) {
                if (model.MailingAddress.ZipCode() != null && model.MailingAddress.ZipCode().length > 0) {
                    hasMailingZipCode = true;
                }

            }
            else {
                hasMailingZipCode = true;
            }


            //if (model.MailingAddress != null) {
            //    if (model.MailingAddress.ZipCode() != null && model.MailingAddress.ZipCode().length > 0) {
            //        hasMailingZipCode = true;
            //    }
            //}
            return (hasName && (hasEmail || id == 0) && hasZipCode && hasMailingZipCode);

        }, deferEvaluation: true
    });

    model.hasAnyPrimaryEmail = ko.computed({
        read: function () {
            var hasEmail;

            $.each(model.FranchiseOwners(), function (i, owner) {
                if (owner.User.Person.PrimaryEmail() != null) {
                    if (owner.User.Person.PrimaryEmail().length > 0) {
                        hasEmail = true;
                    }
                }
                
            });
            return hasEmail;

        }, deferEvaluation: true

        });

    model.ShouldShow = function () {
        if (model.FranchiseId() != 0) {
            return true;
        }
        else {
            return false;
        }

    };

    model.SolatechText = function () {
        if (model.IsSolaTechEnabled() == null || model.IsSolaTechEnabled() == undefined) {
            return 'Setup Solatech';
        }
        if (model.IsSolaTechEnabled() == true) {
            return 'Disable Solatech';
        }
        else {
            return 'Enable Solatech';
        }
    };

    model.QBText = function () {
        if (model.IsQBEnabled() == null || model.IsQBEnabled() == undefined || model.IsQBEnabled() == false) {
            return 'Setup QB';
        }
        if (model.IsQBEnabled() == true) {
            return 'QB added';
        }
    };

    model.toJS = function (data, prefix) {
        data = data || {};
        data[(prefix || "") + FranchiseId] = model.FranchiseId;
        data[(prefix || "") + FranchiseGuid] = model.FranchiseGuid;
        data[(prefix || "") + Name] = model.Name;
        data[(prefix || "") + Code] = model.Code;
        data[(prefix || "") + Website] = model.Website;
        data[(prefix || "") + CreatedOnUtc] = model.CreatedOnUtc;
        data[(prefix || "") + AddressId] = model.AddressId;
        data[(prefix || "") + MailingAddressId] = model.MailingAddressId;
        data[(prefix || "") + IsDeleted] = model.IsDeleted;
        data[(prefix || "") + TimezoneCode] = model.TimezoneCode;
        data[(prefix || "") + LogoImgUrlRelativePath] = model.LogoImgUrlRelativePath;
        data[(prefix || "") + TollFreeNumber] = model.TollFreeNumber;
        data[(prefix || "") + LocalPhoneNumber] = model.LocalPhoneNumber;
        data[(prefix || "") + FaxNumber] = model.FaxNumber;
        data[(prefix || "") + SupportEmail] = model.SupportEmail;
        data[(prefix || "") + AdminEmail] = model.AdminEmail;
        data[(prefix || "") + OwnerEmail] = model.OwnerEmail;
        data[(prefix || "") + LastUpdatedUtc] = model.LastUpdatedUtc;
        data[(prefix || "") + LastUpdatedByPersonId] = model.LastUpdatedByPersonId;
        data[(prefix || "") + IsSuspended] = model.IsSuspended;
        data[(prefix || "") + IsSolaTechEnabled] = model.IsSolaTechEnabled;
        return data;
    }
    model.toBaseJS = function (model) {
        var data = {
         
            FranchiseId: ko.toJS(model.FranchiseId()),
            FranchiseGuid: ko.toJS(model.FranchiseGuid()),
            Name: ko.toJS(model.Name()),
            Code: ko.toJS(model.Code()),
            Website: ko.toJS(model.Website()),
            CreatedOnUtc: ko.toJS(model.CreatedOnUtc()),
            AddressId: ko.toJS(model.AddressId()),
            MailingAddressId:ko.toJS(model.MailingAddressId()),
            IsDeleted: ko.toJS(model.IsDeleted()),
            TimezoneCode: ko.toJS(model.TimezoneCode()),
            LogoImgUrlRelativePath: ko.toJS(model.LogoImgUrlRelativePath()),
            TollFreeNumber: ko.toJS(model.TollFreeNumber()),
            LocalPhoneNumber: ko.toJS(model.LocalPhoneNumber()),
            FaxNumber: ko.toJS(model.FaxNumber()),
            SupportEmail: ko.toJS(model.SupportEmail()),
            AdminEmail: ko.toJS(model.AdminEmail()),
            OwnerEmail: ko.toJS(model.OwnerEmail()),
            LastUpdatedUtc: ko.toJS(model.LastUpdatedUtc()),
            LastUpdatedByPersonId: ko.toJS(model.LastUpdatedByPersonId()),           
            FranchiseOwners: ko.toJS(model.FranchiseOwners),
            Territories: ko.toJS(model.Territories),
            Address: model.Address.toBaseJS(),
            MailingAddress: model.MailingAddress.toBaseJS(),
            IsSuspended: model.IsSuspended(),
            IsSolaTechEnabled: model.IsSolaTechEnabled(),
            //Address: ko.toJS(model.Address)          
        }
        return data;
    }
    model.Template = ko.observable("franchise-view");

    model.addOwner = function (model, e) {       
        var one = new HFC.Models.FranchiseOwner();
        one.FranchiseId = model.FranchiseId();
        model.FranchiseOwners.push(one);
    }
  
    model.addTerritory = function (model, e) {
        var _Territory = new HFC.Models.Territory();
        _Territory.FranchiseId = this.FranchiseId();
        franchiseModel.Territories.push(_Territory);
    }

    //model.saveTerritories = function (model, e) {
    //    $.each(franchiseModel.Territories, function (i, Territory) { Territory.FranchiseId = this.FranchiseId });
    //    model.vmFranchise.saveTerritories(ko.toJS(franchiseModel.Territories));
    //}

    model.saveAddress = function (model, e) {
        var _address = ko.toJS(model.Address)
        model.vmFranchise.saveAddress(_address, function (result) {
            if (result.Result == "Success") {
                HFC.DisplaySuccess("Address Saved successfully");
            }
            else {
                HFC.DisplayAlert(result.Result);
            }
        });
    }

    model.deactivate = function (franchiseId) {
        model.vmFranchise.deactivate(franchiseId, function (result) {
            if (result.Result == "Success") {
                HFC.DisplaySuccess("Franchise deactivated successfully");
            }
            else {
                HFC.DisplayAlert(result.Result);
            }
        });
    }

    model.addToSolatech = function (model, e) {
        var _franchise = model.toBaseJS(model);
        var franchise = new HFC.VM.Franchise(model.FranchiseId());
        franchise.saveToSolatech(_franchise, function () {
            if (model.IsSolaTechEnabled() == null || model.IsSolaTechEnabled() == undefined) {
                model.IsSolaTechEnabled(true);
            }
            else if (model.IsSolaTechEnabled() == true) {
                model.IsSolaTechEnabled(false);
            }
            else {
                model.IsSolaTechEnabled(true);
            }
        });
    }

    model.addToQB = function (model, e) {
        var _franchise = model.toBaseJS(model);
        var franchise = new HFC.VM.Franchise(model.FranchiseId());
        franchise.saveToQB(_franchise, function () {
            if (model.IsQBEnabled() == null || model.IsQBEnabled() == undefined || model.IsQBEnabled() == false) {
                model.IsQBEnabled(true);
            }
        });
    }

    model.saveFranchise = function (model, e) {
         
        try {
            
            var isFormValid = model.isRequiredAllFilled();
            
            if (!isFormValid) {
                return;
            }

            var _franchise = model.toBaseJS(model);
            var franchise = new HFC.VM.Franchise(model.FranchiseId());
            franchise.saveFranchise(_franchise, function (result) {
                if (result.Result == "Success") {
                    window.location.href = "/controlpanel/franchises";
                }
                else {
                    HFC.DisplayAlert(result.Result);
                }
            });
        }
        catch (e) {
        }
    }

    model.YesNoModels = [{
        Value: true,
        Key: 'Yes'
    },
{
    Value: false,
    Key: 'No'
}];
    

    return model;
}