﻿
HFC.Models.Event = function (model) {
    model = model || { };

    //******************************** base model properties
    model.id = ko.observable(model.id || 0);
    model.Message = ko.observable(model.Message);
    model.title = ko.observable(model.title);
    model.IsPrivate = ko.observable(model.IsPrivate);
    model.RevisionSequence = ko.observable(model.RevisionSequence);
    model.allDay = ko.observable(model.allDay);
    //model.CreatedOnUtc = new XDate(model.CreatedOnUtc);        
    model.JobId = model.JobId || 0;
    model.JobNumber = model.JobNumber || 0;
    model.LeadId = model.LeadId || 0;
    model.LeadNumber = model.LeadNumber || 0;
    model.startdate = ko.observable(model.start).extend({ date: true });
    model.starttime = ko.observable(model.start).extend({ date: HFC.Browser.isMobile() ? "H:mm" : "hh:mm tt" });

    model.enddate = ko.observable(model.end).extend({ date: true });
    model.endtime = ko.observable(model.end).extend({ date: HFC.Browser.isMobile() ? "H:mm" : "hh:mm tt" });
    model.Location = ko.observable(model.Location);
    model.AdditionalPeople = ko.observableArray(model.AdditionalPeople);    
    model.OrganizerName = ko.observable(model.OrganizerName);
    model.OriginalStartDate = ko.observable(model.start);
    model.OrganizerPersonId = ko.observable(model.OrganizerPersonId || HFC.CurrentUser.PersonId);
    model.OrganizerEmail = ko.observable(model.OrganizerEmail);
    model.AssignedPersonId = ko.observable(model.AssignedPersonId || HFC.CurrentUser.PersonId);
    model.AssignedPersonIdOldValue = ko.observable(model.AssignedPersonId());
    model.AssignedName = ko.observable(model.AssignedName);
    model.ReminderMinute = ko.observable(model.ReminderMinute || 30);
    model.ReminderMethods = ko.observable(model.ReminderMethods || 7); //7 is all 3 reminder methods 
    model.className = ko.observable(model.className ? (model.className instanceof Array ? model.className[0] : model.className) : ("color-" + HFC.CurrentUser.ColorId));
    model.IsDeletable = ko.observable(model.IsDeletable || true);
    model.editable = ko.observable(model.editable || true);
    //Pavel: className can be different, show one in the calendar but other in popover and modal
    model.classNameAssignedPerson = ko.computed(function () {
        var emp = $.grep(HFC.EmployeesJS, function (em) {
            return em.PersonId == model.AssignedPersonId();
        });
        if (emp && emp.length)
            return "color-" + emp[0].ColorId;
    });

    model.ApptCancelledBtn = ko.computed(function () {
        
        return model.id() != 0 && model.AptTypeEnum;
    });

    model.start = ko.computed({
        read: function () {
            if (model.startdate())
                return new XDate(model.startdate() + " " + model.starttime());
            else
                return null;
        }, deferEvaluation: true
    });

    model.end = ko.computed({
        read: function () {
            if (model.enddate())
                return new XDate(model.enddate() + " " + model.endtime());
            else
                return null;
        }, deferEvaluation: true
    });
    model.startdate.subscribe(function () {
        if (model.AptTypeEnum) {
            var start = new XDate(model.startdate() + " " + model.starttime());
            if (start.valid() && start.diffMinutes(model.end()) <= 0) { //start date is past end date so increment end by one hour
                start.addHours(1);
                model.enddate(start);
                model.endtime(start);
            }
        }
    });

    model.starttime.subscribe(function () {
        if (model.AptTypeEnum) {
            var start = new XDate(model.startdate() + " " + model.starttime());
            if (start.valid() && start.diffMinutes(model.end()) <= 0) { //start date is past end date so increment end by one hour
                start.addHours(1);
                model.enddate(start);
                model.endtime(start);
            }
        }
    });

    model.enddate.subscribe(function () {
        var end = new XDate(model.enddate() + " " + model.endtime());
        if (end.valid() && model.start().diffMinutes(end) <= 0) { //start date is past end date so decrement start by one hour
            end.addHours(-1);
            model.startdate(end);
            model.starttime(end);
        }
    });

    model.endtime.subscribe(function () {
        var end = new XDate(model.enddate() + " " + model.endtime());
        if (end.valid() && model.start().diffMinutes(end) <= 0) { //start date is past end date so decrement start by one hour
            end.addHours(-1);
            model.startdate(end);
            model.starttime(end);
        }
    });
    //*************************************************************************************************************************************************

    //*********************** additional custom properties just for client side UI
    model.IncludeLeadJob = ko.observable(model.JobId || model.LeadId);

    model.ToggleLeadJob = function () {

        model.IncludeLeadJob(!model.IncludeLeadJob());
        if (model.IncludeLeadJob()) {
            model.title(HFC.QS.EventTitle);
            model.Location(HFC.QS.Location);
            model.RepeatIsChecked(false);
        } else {
            model.title('');
            model.Location('')
           
        }
    }
    model.TogglePrivate = function () {
        model.IsPrivate(!model.IsPrivate());
    }
    model.ToggleAllDay = function () {
        model.allDay(!model.allDay());
    }
    
    //*********************** computed properties for client side UI
    model.AssignedFullName = ko.computed(function () {
        if (model.OrganizerPersonId()) {
            var emp = $.grep(HFC.EmployeesJS, function (e) {
                return e.PersonId == model.AssignedPersonId();
            });
            if (emp && emp.length)

                return emp[0].FullName;
            else
                return "";
        } else
            return model.OrganizerName() || "Unknown organizer";
    });
 
    model.GetAssignedUserEmail = function () {
        for (var i = 0; i < HFC.EmployeesJS.length; i++) {
            if (HFC.EmployeesJS[i].PersonId === model.AssignedPersonId()) {
                return HFC.EmployeesJS[i].Email;
            }
        };
    };

    model.AttendeeRW = ko.computed({
        read: function () {
            if (model.id() == 0 && model.AdditionalPeople().length == 0) {
                var emp = $.grep(HFC.EmployeesJS, function (em) {
                    if (em.PersonId === HFC.CurrentUser.PersonId) {
                        model.AdditionalPeople().push(em.Email);
                        model.AssignedPersonIdOldValue(em.PersonId);
                    }
                });
            }
            return model.AdditionalPeople() ? model.AdditionalPeople().join(", ") : "";
        },
        write: function (val) {
            val = val.replace(';', ',');
            var validEmailArr = [];
            if (!val) {
                val = model.GetAssignedUserEmail();
            }
            if (val.indexOf(model.GetAssignedUserEmail()) == -1) {
                val = val + ', ' + model.GetAssignedUserEmail()
            }

            if (val) {
                var split = val.split(',');
                split = unique(split);
                if (split && split.length) {
                    for (var i = 0; i < split.length; i++) {
                        var email = $.trim(split[i]);
                        if (HFC.Regex.EmailPattern.test(email)){
                            validEmailArr.push(email);
                        }
                    }
                }
            }
          
            if (validEmailArr.length) {
                model.AdditionalPeople(validEmailArr);
                $('#attendee').val(model.AdditionalPeople().join(", "));
            }
            else
            {
                model.AdditionalPeople.removeAll();
            }
   
        }
    });
 
    function unique(list) {
        var result = [];
        $.each(list, function (i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
        });
        return result;
    }

    model.OrganizerPersonId.subscribe(function () {
        var emp = $.grep(HFC.EmployeesJS, function (em) {
            return em.PersonId == model.OrganizerPersonId();
        });
        if (emp && emp.length)
            model.className("color-" + emp[0].ColorId);
    });

    model.AssignedPersonId.subscribe(function (newValue) {
        for (var i = 0; i < HFC.EmployeesJS.length; i++) {
            if (HFC.EmployeesJS[i].PersonId === model.AssignedPersonIdOldValue()) {
                var index = model.AdditionalPeople().indexOf(HFC.EmployeesJS[i].Email);
                if (index != -1) {
                    model.AdditionalPeople().splice(index, 1);
                }
                model.AssignedPersonIdOldValue(newValue);
            }
        };
        var emp = $.grep(HFC.EmployeesJS, function (em) {
            return em.PersonId == model.AssignedPersonId();
        });
        if (emp && emp.length) {
            var index = model.AdditionalPeople().indexOf(emp[0].Email);
            if (index === -1) {
                model.AdditionalPeople().push(emp[0].Email);
            }
            $('#attendee').val(model.AdditionalPeople().join(", "));
            model.className("color-" + emp[0].ColorId);
        }
    });

    //model.starttime = ko.computed({
    //    read: function () {
    //        if (model.start())
    //            return HFC.Browser.isMobile() ? model.start().toString("HH:mm") : model.start().toString("hh:mm TT");
    //        else
    //            return "";
    //    },
    //    write: function (input, jsevent) {
    //        var val = typeof input == "string" ? input : (jsevent ? jsevent.currentTarget.value : "");
    //        if (val) {
    //            if (HFC.Regex.IsValidTime(val)) {
    //                var date = model.startdate();
    //                if (date == "")
    //                    date = new XDate().toString("MM/dd/yyyy");
    //                var newdate = new XDate(date + ' ' + val);
    //                if (newdate.valid()) {
    //                    model.start(newdate);
    //                    if (newdate.diffMinutes(model.end()) <= 0) //start date is past end date so increment end by one hour
    //                        model.end(newdate.clone().addHours(1))
    //                }
    //            } else
    //                HFC.DisplayAlert("Invalid start date, reverting to previous date.");
    //        } else if (model.start())
    //            model.start(model.start().clearTime()); //reset it to midnight                
    //    }
    //});
    //model.startdate = ko.computed({
    //    read: function () {
    //        if (model.start())
    //            return model.start().toString(HFC.Browser.isMobile() ? "yyyy-MM-dd" : "MM/dd/yyyy");
    //        else
    //            return "";
    //    },
    //    write: function (input, jsevent) {
    //        var val = typeof input == "string" ? input : (jsevent ? jsevent.currentTarget.value : "");
    //        if (val) {
    //            if (val != model.startdate()) {//only update if different date
    //                var time = "";
    //                if (!model.allDay() || !model.AptTypeEnum) {
    //                    time = model.starttime();
    //                    if (!(time))
    //                        time = XDate.getRoundedDate().toString("hh:mm TT");
    //                }
    //                var newdate = new XDate(val + ' ' + time);
    //                if (newdate.valid()) {
    //                    model.start(newdate);
    //                    if (newdate.diffMinutes(model.end()) <= 0) //start date is past end date so increment end by one hour
    //                        model.end(newdate.clone().addHours(1))
    //                } else
    //                    HFC.DisplayAlert("Invalid date, reverting to previous date.");
    //            }
    //        } else if (model.start())
    //            model.start(null);
    //    }
    //});

    //model.endtime = ko.computed({
    //    read: function () {
    //        if (model.end())
    //            return HFC.Browser.isMobile() ? model.end().toString('HH:mm') : model.end().toString("hh:mm TT");
    //        else
    //            return "";
    //    },
    //    write: function (input, jsevent) {
    //        var val = typeof input == "string" ? input : (jsevent ? jsevent.currentTarget.value : "");
    //        if (val) {
    //            if (HFC.Regex.IsValidTime(val)) {
    //                var date = model.enddate(),
    //                    newdate = new XDate(date + ' ' + val);
    //                if (newdate.valid()) {
    //                    model.end(newdate);
    //                    if (model.start().diffMinutes(newdate) <= 0) //END date is past start date so decrement start by one hour
    //                        model.start(newdate.clone().addHours(-1))
    //                }
    //            } else
    //                HFC.DisplayAlert("Invalid end date, reverting to previous date.");
    //        } else if (model.end())
    //            model.end(model.end().clearTime());
    //    }
    //});
    //model.enddate = ko.computed({
    //    read: function () {
    //        if (model.end())
    //            return model.end().toString(HFC.Browser.isMobile() ? "yyyy-MM-dd" : "MM/dd/yyyy");
    //        else
    //            return "";
    //    },
    //    write: function (input, jsevent) {
    //        var val = typeof input == "string" ? input : (jsevent ? jsevent.currentTarget.value : "");
    //        if (val) {
    //            if (val != model.enddate()) {
    //                var time = "";
    //                if (!model.allDay()) {
    //                    time = model.endtime();
    //                    if (!(time))
    //                        time = XDate.getRoundedDate().addHours(1).toString("hh:mm TT");
    //                }
    //                var newdate = new XDate(val + ' ' + time);
    //                if (newdate.valid()) {
    //                    model.end(newdate);
    //                    if (model.start().diffMinutes(newdate) <= 0) //END date is past start date so decrement start by one hour
    //                        model.start(newdate.clone().addHours(-1))
    //                } else
    //                    HFC.DisplayAlert("Invalid start date, reverting to previous date.");
    //            }
    //        } else if (model.end())
    //            model.end(null);
    //    }
    //});

    model.DateRangeText = ko.computed(function () {
        var rangeText = "";
        if (model.start()) {
            if (model.end()) {
                var minDiff = model.start().diffMinutes(model.end()),
                    start = "",
                    end = "";

                if (minDiff <= 1440) {//both are happening within same day
                    rangeText = model.start().toFriendlyString();
                    if (!model.allDay())
                        rangeText += ", " + model.start().toString("h:mm tt") + " to " + model.end().toString("h:mm tt");
                } else {
                    start = model.start().toFriendlyString(!model.allDay());
                    end = model.end().toFriendlyString(!model.allDay());
                    if (start == end)
                        rangeText = start;
                    else
                        rangeText = start + " to " + end;
                }
            } else
                rangeText = model.start().toFriendlyString(true);
        }

        return rangeText;
    });

    //this is optional if we want to do google map on the event
    //model.LocationHref = ko.computed(function () {
    //    if (model.Location()) { //do some basic validation for address instead of location being like "office"?
    //        return '//maps.google.com/maps?saddr=' + encodeURIComponent(HFC.FranchiseAddress) + '&daddr=' + encodeURIComponent(model.Location());
    //    } else
    //        return "";
    //});

    //function to set time when user use the drop down to select a time
    //model.SelectStartTime = function (time) {
    //    model.starttime(time);
    //};
    //model.SelectEndTime = function (time) {
    //    model.endtime(time)
    //};

    model.toBaseJS = function () {
        var data = {
            id: model.id(),
            Message: model.Message(),
            title: model.title(),
            IsPrivate: model.IsPrivate(),
            RevisionSequence: model.RevisionSequence(),                        
            Location: model.Location(),            
            allDay: model.allDay(),
            OrganizerName: model.OrganizerName(),
            OrganizerPersonId: model.OrganizerPersonId(),
            OrganizerEmail: model.OrganizerEmail(),
            AssignedPersonId: model.AssignedPersonId(),
            AssignedName: model.AssignedName(),
            ReminderMinute: model.ReminderMinute(),
            ReminderMethods: model.ReminderMethods(),
            //model.OriginalStartDate = ko.observable(model.start)
            //model.OriginalStartDate = ko.observable(model.start);
            //model.OriginalStartTime = ko.observable(model.start).extend({ date: HFC.Browser.isMobile() ? "H:mm" : "hh:mm tt" });
            OriginalStartDate: model.OriginalStartDate()
        }
        if (model.start()) {
            model.start().setSeconds(0);
            model.start().setMilliseconds(0);
            data.start = model.start().toString("u"); //ISO8601 format, with a timezone indicator 
        }
        if (model.end()) {
            model.end().setSeconds(0);
            model.end().setMilliseconds(0);
            data.end = model.end().toString("u");
        }

        var notMatched = [];
        for (var a = 0; a < model.AdditionalPeople().length; a++) {
            var isExists = false;
            for (var i = 0; i < HFC.AllEmployeesJS.length; i++) {
                if (HFC.AllEmployeesJS[i].Email == model.AdditionalPeople()[a]) {
                    isExists = true;
                    break;
                } 
            }

            if (isExists == false) {
                notMatched.push(model.AdditionalPeople()[a]);
            }
        }
        var isAllExists = true;
        for (var i = 0; i < notMatched.length; i++) {
            var index = model.AdditionalPeople().indexOf(notMatched[i]);
            model.AdditionalPeople().splice(index, 1);
            isAllExists = false;
        }
      

        data.isAllExists = isAllExists;
        if (model.AdditionalPeople()) {
            $(model.AdditionalPeople()).each(function (i, p) {
                data['AdditionalPeople[' + i + ']'] = p;
            });
        }
        if (model.IncludeLeadJob()) {
            data.JobId = model.JobId;
            data.JobNumber = model.JobNumber;
            data.PhoneNumber = model.PhoneNumber;
            data.LeadId = model.LeadId;
            data.LeadNumber = model.LeadNumber;
        }
        return data;
    }

    return model;
}