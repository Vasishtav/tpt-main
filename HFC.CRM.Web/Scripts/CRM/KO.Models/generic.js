﻿HFC.Models.Generic = function (model) {
    model = model || {};

    model.Id = model.Id;
    model.Name = ko.observable(model.Name);
    //model.Description = ko.observable(model.Description);
    model.ParentId = model.ParentId;
    model.FranchiseId = model.FranchiseId;
    model.InEdit = ko.observable();

    return model;
}