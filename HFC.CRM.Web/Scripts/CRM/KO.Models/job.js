﻿/*
* This model is dependent on Notes, Address, Person, JobItems, and SaleAdjustment
*/
HFC.Models.Job = function (jsModel, isCollapsed) {
    jsModel = jsModel || { };

    jsModel.LeadId = jsModel.LeadId || 0;
    jsModel.LeadNumber = jsModel.LeadNumber || 0; //this is not part of jobs table so make sure to pass it when you create the job object from lead
    jsModel.JobId = jsModel.JobId || 0;
    jsModel.JobNumber = jsModel.JobNumber || 0;
    jsModel.CustomerPersonId = ko.observable(jsModel.CustomerPersonId);
    jsModel.IsCollapsed = ko.observable(isCollapsed);
    jsModel.CreatedOnUtc = jsModel.CreatedOnUtc ? new XDate(jsModel.CreatedOnUtc) : null;
    jsModel.LastUpdated = jsModel.LastUpdated ? new XDate(jsModel.LastUpdated) : new XDate(); // use this to determine ATOMIC state of data
    jsModel.InstallAddressId = ko.observable(jsModel.InstallAddressId);
    jsModel.BillingAddressId = ko.observable(jsModel.BillingAddressId);
    jsModel.InstallAddress = HFC.Models.Address(jsModel.InstallAddress);
    jsModel.BillingAddress = HFC.Models.Address(jsModel.BillingAddress);
    jsModel.SalesPersonId = ko.observable(jsModel.SalesPersonId);
    jsModel.InstallerPersonId = ko.observable(jsModel.InstallerPersonId);
    jsModel.Permission = jsModel.Permission || { CanUpdate: false, CanDelete: false };
    jsModel.CanUpdateStatus = jsModel.CanUpdateStatus || false;
    jsModel.CanDistribute = jsModel.CanDistribute || false;
    jsModel.ContractedOnUtc = ko.observable(jsModel.ContractedOnUtc).extend({date: true});
    jsModel.Customer = HFC.Models.Person(jsModel.Customer);    
    jsModel.JobConceptId = ko.observable(jsModel.JobConceptId || 1); //1 for default BB or TL concept
    jsModel.JobNotes = new HFC.VM.Notes(jsModel.JobNotes, jsModel.JobId ? ("/api/jobs/" + jsModel.JobId + "/notes") : null);
    jsModel.SourceId = ko.observable(jsModel.SourceId);
    jsModel.TerritoryId = ko.observable(jsModel.TerritoryId);
    jsModel.CostPermission = jsModel.CostPermission || {CanUpdate: false, CanRead: false};

    jsModel.DisplayCost = ko.observable(jsModel.CostPermission.CanRead);

    jsModel.OriginalContractedOnUtc = jsModel.ContractedOnUtc();
    jsModel.IsBusy = ko.observable();
    setTimeout(function () {
        jsModel.ContractedOnUtc.subscribe(function () {
            if (!jsModel.IsBusy() && jsModel.OriginalContractedOnUtc != jsModel.ContractedOnUtc()) {
                jsModel.IsBusy(true);
                var text = "";
                if (jsModel.ContractedOnUtc()) {
                    var time = jsModel.ContractedOnUtc.Value,
                        now = new Date();
                    time.addHours(now.getHours());
                    time.addMinutes(now.getMinutes());
                    time.addSeconds(now.getSeconds());
                    text = time.toISOString();
                }
                HFC.AjaxPut('/api/jobs/' + jsModel.JobId + '/contractdate', { ContractedOnUtc: text }, function () {
                    jsModel.OriginalContractedOnUtc = jsModel.ContractedOnUtc();
                    HFC.DisplaySuccess("Contract date successfully updated");
                }).always(function () {
                    jsModel.IsBusy(false);
                });
            }
        });
    }, 1000);

    if(HFC.VM.Tasks)
        jsModel.Tasks = new HFC.VM.Tasks(jsModel.Tasks, jsModel.JobId);
    if(HFC.VM.Calendar)
        jsModel.Calendar = new HFC.VM.Calendar(jsModel.Calendar, jsModel.JobId);
    if (HFC.VM.QA)
        jsModel.JobQuestionAnswers = new HFC.VM.QA(jsModel.JobQuestionAnswers, jsModel.JobId);

    jsModel.PhysicalFiles = ko.observableArray(jsModel.PhysicalFiles);
    
    jsModel.JobQuotes = ko.observableArray(jsModel.JobQuotes ? $.map(jsModel.JobQuotes, function (i) {
        return HFC.Models.Quote(i);
    }) : null);
    
    jsModel.PrimQuote = ko.computed({
        read: function () {
            var quote = $.grep(jsModel.JobQuotes(), function (q) {
                return q.IsPrimary();
            });
            if (quote && quote.length)
                return quote[0];
            else
                return null;
        }, deferEvaluation: true
    });

    jsModel.SelectedSrcHtml = ko.computed({
        read: function () {
            var src = HFC.Models.LeadSource({ SourceId: jsModel.SourceId() });
            return src.SrcHtml();
        }, deferEvaluation: true
    });    

    //use this bool to toggle job item row
    jsModel.ItemRowEnabled = ko.observable();

    jsModel.JobStatusId = ko.observable(jsModel.JobStatusId || 1);
    jsModel.JobStatusColor = ko.computed({
        read: function () {
            var status = HFC.Lookup.Get(LK_JobStatuses, jsModel.JobStatusId());
            if (!status.ClassName && status.ParentId)
                status = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
            return (status.ClassName || "btn-default");
        }, deferEvaluation: true
    });    
    jsModel.JobStatusName = ko.computed({
        read: function () {
            var status = HFC.Lookup.Get(LK_JobStatuses, jsModel.JobStatusId()),
                parent = null;
            if (status.ParentId) 
                parent = HFC.Lookup.Get(LK_JobStatuses, status.ParentId);
            if (parent)
                return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
            else
                return status.Name;
        }, deferEvaluation: true
    });

    jsModel.JobStatusParentId = ko.computed({
        read: function () {
            var status = HFC.Lookup.Get(LK_JobStatuses, jsModel.JobStatusId());
            return status.ParentId;
        }, deferEvaluation: true
    });
    jsModel.ChangeSalesAgent = function (model, jsEvent) {
        var newId = $(jsEvent.currentTarget).data("value");
        if (jsModel.SalesPersonId() != newId) {
            HFC.AjaxPut('/api/jobs/' + jsModel.JobId + '/salesagent',
            {
                PersonId: newId
            },
            function () {
                jsModel.SalesPersonId(newId);
                HFC.DisplaySuccess("Sales agent successfully updated");
            });
        }
    };
    jsModel.SelectedSalesAgent = ko.computed({
        read: function () {
            if (jsModel.SalesPersonId()) {
                var user = $.grep(HFC.EmployeesJS, function (e) {
                    return e.PersonId == jsModel.SalesPersonId();
                });
                if (user && user.length)
                    return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
                else {
                    var user = $.grep(HFC.DisabledEmployeesJS, function(e) {
                        return e.PersonId == jsModel.SalesPersonId();
                    });
                    if (user && user.length)
                        return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
                    else
                        return "Unassigned";
                }
                
            } else
                return "Unassigned";
        }, deferEvaluation: true
    });

    jsModel.SelectedInstaller = ko.computed({
        read: function () {
            if (jsModel.InstallerPersonId()) {
                var user = $.grep(HFC.EmployeesJS, function (e) {
                    return e.PersonId == jsModel.InstallerPersonId();
                });
                if (user && user.length)
                    return "<span class='color-box color-" + user[0].ColorId + "'></span> " + user[0].FullName;
                else
                    return "Unassigned";
            } else
                return "Unassigned";
        }, deferEvaluation: true
    })

    jsModel.BalanceCalc = ko.computed({
        read: function () {
            var paymentSum = 0;
            if (jsModel.Invoices.length > 0) {
                for (var i = 0; i < jsModel.Invoices[0].Payments.length; i++) {
                    paymentSum += parseFloat(jsModel.Invoices[0].Payments[i].Amount);
                }
            }

            return jsModel.PrimQuote().NetTotal() - paymentSum;
        }, deferEvaluation: true
    })

    jsModel.ChangeInstaller = function (model, jsEvent) {
        var newId = $(jsEvent.currentTarget).data("value");
        if (jsModel.InstallerPersonId() != newId) {
            HFC.AjaxPut('/api/jobs/' + jsModel.JobId + '/installer',
                {
                    InstallerPersonId: newId
                },
                function () {
                    jsModel.InstallerPersonId(newId);
                    HFC.DisplaySuccess("Installer successfully updated");
                });
        }
    };

    jsModel.JobStatusOnChanged = function (model, jsEvent) {
        var element = jsEvent.currentTarget,
            val = $(element).data("value");
        if (model.JobStatusId() != val) {
            HFC.AjaxPut('/api/jobs/' + model.JobId + '/status', { Id: val }, function () {
                model.JobStatusId(val);
                HFC.DisplaySuccess("Status updated for Job #" + model.JobNumber);
            });
        }
    };

    jsModel.SourceOnChanged = function (src, jsEvent) {
        var srcId = $(jsEvent.currentTarget).data("value");
        if (srcId != jsModel.SourceId() && jsModel.JobId > 0) {            
            HFC.AjaxPut('/api/jobs/' + jsModel.JobId + '/source', { Id: srcId },
                function () {
                    jsModel.SourceId(srcId);
                    HFC.DisplaySuccess("Job Source saved");
                });            
        } 
    };
        
    setTimeout(function () {
        jsModel.JobConceptId.subscribe(function () {
            HFC.AjaxPut('/api/jobs/' + jsModel.JobId + '/concept', { Id: jsModel.JobConceptId() }, function (result) {
                HFC.DisplaySuccess("Concept updated");
            });
        });
        jsModel.TerritoryId.subscribe(function () {
            HFC.AjaxPut('/api/jobs/' + jsModel.JobId + '/territory', { TerritoryId: jsModel.TerritoryId() }, function (result) {
                HFC.DisplaySuccess("Territory updated");
            });
        })
    }, 0);

    return jsModel;
}