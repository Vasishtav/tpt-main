﻿HFC.Models.Campaign = function (model) {
    model = model || {};
    
    model.InEdit = ko.observable();
    model.ParentId = model.ParentId;
    model.SourceId = model.SourceId;
    model.StartDate = ko.observable(model.StartDate).extend({ date: true });
    model.EndDate = ko.observable(model.EndDate).extend({ date: true });
    model.Amount = ko.observable(model.Amount || 0);
    model.Name = ko.observable(model.Name);
    model.IsActive = ko.observable(model.IsActive || true);
    model.Memo = ko.observable(model.Memo);

    model.DateRange = ko.computed({
        read: function() {
            var date = model.StartDate.Value.toFriendlyString();
            if (model.EndDate())
                date += " to " + model.EndDate.Value.toFriendlyString();
            else
                date += " to Infinity";
            return date;
        },
        deferEvaluation: true
    });

    return model;
}