﻿
HFC.Models.Territory = function (model) {
    model = model || {};

    model.TerritoryId = ko.observable(model.TerritoryId);
    model.FranchiseId = model.FranchiseId;
    model.Name = ko.observable(model.Name);
    model.Minion = ko.observable(model.Minion);
    model.Code = ko.observable(model.Code);
    model.isArrears = ko.observable(model.isArrears);
    model.CreatedOnUtc = ko.observable(model.CreatedOnUtc);
    model.FranchiseRoyalties = ko.observable(model.FranchiseRoyalties);
    
    model.toJS = function (data, prefix) {
        data = data || {};
        data[(prefix || "") + TerritoryId] = model.TerritoryId;
        data[(prefix || "") + FranchiseId] = model.FranchiseId;
        data[(prefix || "") + Name] = model.Name;
        data[(prefix || "") + Minion] = model.Minion;
        data[(prefix || "") + Name] = model.isArrears;
        data[(prefix || "") + Code] = model.Code;
        data[(prefix || "") + CreatedOnUtc] = model.CreatedOnUtc;
        return data;
    }

    model.toBaseJS = function () {
        var data = {
            TerritoryId: 0,
            FranchiseId: model.FranchiseId,
            Name: model.Name(),
            Code: model.Code(),
            Minion: model.Minion(),
            isArrears: model.isArrears(),
            CreatedOnUtc: model.CreatedOnUtc(),
        }
        if ($.isFunction(model.TerritoryId)) {
            data.TerritoryId = model.TerritoryId();
        }
        else {
            data.TerritoryId = model.TerritoryId;
        }
        return data;
    }

    model.deleteTerritory = function (model, e) {
        try {
            e.preventDefault();
            //franchiseModel.Territories.remove(this);
            if (!model.Code()) {
                franchiseModel.Territories.remove(this);
            }
            else {
                var _territory = model.toBaseJS(model);
                var delTerr = new HFC.VM.Franchise(model.FranchiseId);
                var result = delTerr.deleteTerritory(_territory, function (result) {
                    if (result.Result == "Success") {
                     
                        HFC.DisplaySuccess("Territory deleted successfully");
                    }
                    else {
                        HFC.DisplayAlert(result.Result);
                    }
                });
                franchiseModel.Territories.remove(this);
            }
        }
        catch (e) {
        }
    }

    model.saveTerritory = function (model, e) {
        try {
            e.preventDefault();
            if (model.Code() != '' && model.Code() != undefined) {
                var _territory = model.toBaseJS(model);
                var saveTerr = new HFC.VM.Franchise(model.FranchiseId);
                saveTerr.checkTerritory(_territory, function (result) {
                    _territory.isInUsed = result.isInUsed;
                    if (result.confirmStr === '') {
                        saveTerr.saveTerritory(_territory, function (result) {
                            if (result.Result == "Success") {
                                model.TerritoryId = result.TerritoryId;
                                HFC.DisplaySuccess("Territory saved successfully");
                            }
                            else {
                                HFC.DisplayAlert(result.Result);
                            }
                        });
                    }
                    else {
                        bootbox.dialog({
                            message: result.confirmStr,
                            title: "Warning",
                            buttons: {
                                danger: {
                                    label: "Yes",
                                    className: "btn-danger",
                                    callback: function () {
                                        saveTerr.saveTerritory(_territory, function (result) {
                                            if (result.Result == "Success") {
                                                model.TerritoryId = result.TerritoryId;
                                                HFC.DisplaySuccess("Territory saved successfully");
                                            }
                                            else {
                                                HFC.DisplayAlert(result.Result);
                                            }
                                        });
                                    }
                                },
                                main: {
                                    label: "Cancel",
                                    className: "btn-default",
                                    callback: function () {
                                        //Example.show("Primary button");
                                    }
                                }
                            }
                        });
                    }
                });


            }

        }
        catch (e) {
        }
    }

    return model;
}