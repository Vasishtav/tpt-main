﻿HFC.Models.Product = function (model) {
    var self = {};

    self.ProductId = model.ProductId || 0;
    self.CategoryId = model.CategoryId;
    self.CategoryName = model.CategoryName;
    self.ProductTypeId = model.ProductTypeId;
    self.ProductType = model.ProductType;
    self.InEdit = ko.observable(false);
    self.FranchiseId = ko.observable(model.FranchiseId || 0);

    return self;
}