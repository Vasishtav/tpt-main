﻿//Depends on HFC.Models.Person
//           HFC.Models.Address
if (HFC.Models.User == null) {
    HFC.Models.User = function (model) {
        model = model || {};

        if (model.Domain == "bbi.corp") {   
            var idx = model.UserName.indexOf("@");
            if (idx!= -1) {
                model.UserName = model.UserName.substring(0, idx);
                }
        }



        model.UserId = model.UserId;
        model.PersonId = model.PersonId;
        model.AddressId = model.AddressId;
       // model.UserName = model.UserName;
        model.IsUserProfile = model.IsUserProfile;
        model.Domain = model.Domain;
        model.InSales = model.InSales || false;
        model.InInstall = model.InInstall || false;
        model.ColorId = model.ColorId;
        model.FullName = model.FullName;
        model.ColorCss = model.ColorId ? "color-" + model.ColorId : "color-none";
        model.DataContent = "<span class='color-box " + model.ColorCss + "'></span> " + (model.FullName || "");
        model.CreationDateUtc = model.CreationDateUtc ? new XDate(model.CreationDateUtc) : new XDate();
        //model.LastLoginDateUtc = model.LastLoginDateUtc ? new XDate(model.LastLoginDateUtc) : null;
        model.LastLoginDateUtc = model.LastLoginDateUtc!=null ? new XDate(model.LastLoginDateUtc) : new XDate();//Ram

        model.UserName = ko.observable(model.UserName);
        model.Password = ko.observable();
        model.ConfirmPassword = ko.observable();
        model.IsApproved = ko.observable(model.IsApproved);
        model.IsLockedOut = ko.observable(model.IsLockedOut);
        model.IsDisabled = ko.observable(model.IsDisabled);
        model.Comment = ko.observable(model.Comment);
        model.SyncStartsOnUtc = ko.observable(model.SyncStartsOnUtc).extend({ date: true });
        model.CalendarFirstHour = ko.observable(model.CalendarFirstHour);
        model.BGColor = ko.observable(model.BGColor);
        model.FGColor = ko.observable(model.FGColor);
        model.IsADUser = ko.observable(true);//ko.observable((model.Domain == "bbi.corp") ? true : false);
        model.CalSync = ko.observable(model.CalSync);

        //model.CheckVal.subscribe(function (value) {
        //    self.CheckValChanged(value);
        //});

        model.refreshUserName = function () {
            if (model.IsADUser()) {
                var myusername = model.UserName();
                if (model.UserName() != null && myusername.indexOf("@") != -1) {
                    var aa = myusername.substring(0, myusername.indexOf("@"));
                    model.UserName(aa);
                }
            }
        };

        model.refreshUserName();

        model.refreshUserNameForCreate = function () {
            if (model.IsADUser()) {
                var myprimaryemail = model.Person.PrimaryEmail();
                if (myprimaryemail != null && myprimaryemail.indexOf("@") != -1) {
                    var aa = myprimaryemail.substring(0, myprimaryemail.indexOf("@"));
                    model.Person.PrimaryEmail(aa);
                }
            }
        };

        model.CheckPassword = ko.computed({
            read: function () {
                if (model.IsADUser()) {
                    model.Password("");
                    model.ConfirmPassword("");
                }
            }, deferEvaluation: false
        });
       
        model.OAuthAccounts = ko.observableArray(model.OAuthAccounts ? $.map(model.OAuthAccounts, function (o) {
            o.IsActive = ko.observable(o.IsActive);
            o.SyncStartsOnUtc = ko.observable(o.SyncStartsOnUtc).extend({ date: true });
            return o;
        }) : null);
        if (HFC.Models.Person)
            model.Person = new HFC.Models.Person(model.Person);
        model.RoleIds = ko.observableArray(model.RoleIds);
        if (HFC.Models.Address)
            model.Address = HFC.Models.Address(model.Address);

        model.IsSelected = ko.observable(false);
        //model.isUserADUser= function (Domain) {if (Domain == "bbi.corp") return true; else return false;}
        model.toJS = function () {
            var data = {
                UserId: model.UserId,
                PersonId: model.PersonId,
                AddressId: model.AddressId,
                Password: model.Password(),
                ConfirmPassword: model.ConfirmPassword(),
                IsApproved: model.IsApproved(),
                IsLockedOut: model.IsLockedOut(),
                IsDisabled: model.IsDisabled(),
                IsADUser: model.IsADUser(),
                CalSync: model.CalSync(),
                Comment: model.Comment(),
                SyncStartsOnUtc: model.SyncStartsOnUtc(),
                CalendarFirstHour: model.CalendarFirstHour(),
                BGColor: model.BGColor(),
                FGColor: model.FGColor(),
                RoleIds: model.RoleIds()
            };
            if (model.OAuthAccounts().length) {
                $.each(model.OAuthAccounts(), function (a, i) {
                    data['OAuthAccounts[' + i + '].ProviderLinkId'] = a.ProviderLinkId;
                    data['OAuthAccounts[' + i + '].Provider'] = a.Provider;
                    data['OAuthAccounts[' + i + '].IsActive'] = a.IsActive();
                    data['OAuthAccounts[' + i + '].SyncStartsOnUtc'] = a.SyncStartsOnUtc();
                });
            }

            model.Person.toBaseJS();
            model.Address.toBaseJS();

            return data;
        }
        model.toBaseJS = function () {
            var data = {
                UserId: model.UserId,
                UserName: model.UserName(),
                PersonId: model.PersonId,
                AddressId: model.AddressId,
                CreationDateUtc: model.CreationDateUtc,
                BGColor: model.BGColor(),
                FGColor: model.FGColor(),
                RoleIds: model.RoleIds(),
                Password: model.Password(),
                ConfirmPassword: model.ConfirmPassword(),
                Domain: model.Domain,
                DomainPath: model.DomainPath,
                LastActivityDateUtc: model.LastActivityDateUtc,
                IsDeleted: model.IsDeleted,
                IsADUser: model.IsADUser(),
                CalSync: model.CalSync(),
                IsApproved: model.IsApproved(),
                IsLockedOut: model.IsLockedOut(),
                IsDisabled: model.IsDisabled(),
                LastLoginDateUtc: model.LastLoginDateUtc,
                LastPasswordChangedDateUtc: model.LastPasswordChangedDateUtc,
                LastLockoutDateUtc: model.LastLockoutDateUtc,
                FailedPasswordAttemptCount: model.FailedPasswordAttemptCount,
                FailedPasswordAttemptStartDateUtc: model.FailedPasswordAttemptStartDateUtc,
                Comment: model.Comment(),
                LastKnownIPAddress: model.LastKnownIPAddress,
                FranchiseId: model.FranchiseId,
                Email: model.Email,
                ColorId: model.ColorId,
                SyncStartsOnUtc: model.SyncStartsOnUtc(),
                CalendarFirstHour: model.CalendarFirstHour(),
                Person: model.Person.toBaseJS(),
                Address: model.Address.toBaseJS()

                //Person: ko.toJS(model.Person),
                //Address: ko.toJS(model.Address),
            }
            return data;
        }

        return model;
    }
}

