﻿HFC.Models.Question = function (model) {
    var self = {};

    self.QuestionId = model.QuestionId || 0;
    self.Question = model.Question;
    self.SubQuestion = model.SubQuestion;
    self.SelectionType = model.SelectionType || 'textbox';
    self.Answer = ko.observable(model.Answer);
    self.IsChecked = ko.computed({ read: function () { return !!(self.Answer()) }, deferEvaluation: true });
    self.AnswerId = model.AnswerId || 0;
    self.InEdit = ko.observable(false);
    self.FranchiseId = ko.observable(model.FranchiseId);

    return self;
}