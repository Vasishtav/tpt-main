﻿//This task model is dependent on HFC.Models.Event
HFC.Models.Task = function (model) {
    model = model || { id: 0 };
    
    HFC.Models.Event(model);
    model.CompletedDateUtc = ko.observable(model.CompletedDateUtc ? new XDate(model.CompletedDateUtc) : null);
    model.IsCompletedChecked = ko.observable(!!(model.CompletedDateUtc()));
    
    //this is for the "Edit modal" to toggle completed checkbox, to save user will hit the Save button
    model.ToggleCompleted = function () {
        model.IsCompletedChecked(!model.IsCompletedChecked());
    }

    model.AptTypeGlyph = ko.computed(function () {
        if (model.IsCompletedChecked())
            return "glyphicon glyphicon-ok";
        else
            return "";//"glyphicon glyphicon-unchecked";
    });

    model.DueDateWarning = ko.computed({
        read: function () {
            var clsName = "label-default";
            if (model.start() != null && model.CompletedDateUtc() == null) { //not completed
                if ((new XDate()).diffDays(model.start()) < 0) //due date past today
                    clsName = "label-danger";
                else if ((model.start().getTime() - new Date().getTime()) / 60000 <= 1440) //if due date is within 24 hours then its yellow for warning
                    clsName = "label-warning";
            }
            return clsName;
        }, deferEvaluation: true
    });
    //this is for the little checkbox that user can quickly toggle and we save immediately
    model.MarkComplete = function (task, jsevent) {
        var currentState = task.IsCompletedChecked();
        var icon = $(jsevent.currentTarget);
        HFC.AjaxGet('/api/tasks/' + task.id() + '/mark', { IsComplete: !currentState },
            function () {
                task.IsCompletedChecked(!currentState); //only mark it as completed if ajax was successful
                if (icon && icon.length) {
                    if (task.IsCompletedChecked())
                        icon.attr("class", "glyphicon glyphicon-ok");
                    else
                        icon.attr("class", "glyphicon glyphicon-unchecked");
                }
            }
        );
        return false; //do not bubble up, this will prevent the checkbox from checking with "checked: IsCompletedChecked" binding
    };

    model.toJS = function () {
        var data = model.toBaseJS();
        if(model.IsCompletedChecked())
            data.CompletedDateUtc = new Date().toISOString();

        return data;
    }

    return model;
}