﻿//ko.extenders.unitCost = function (target, model) {
//    return ko.computed({
//        read: target,
//        write: function (val) {
//            val = parseFloat(val);
//            if (!isNaN(val)) {
//                target(val);
//                var margin = HFC.evenRound((model.SalePrice() - val) / val, 4);
//                model.MarginPercent(margin * 100.0);
//                //var list = HFC.evenRound((val * (model.MarginPercent() / 100.0)) + val, 4);
//                //if(list != model.SalePrice())
//                //    model.SalePrice(list);
//            }
//        }
//    });
//}
ko.extenders.discountPercent = function (target, model) {
    return ko.computed({
        read: function () {
            var subtotal = model.Quantity() * model.SalePrice();
            if (subtotal > 0)
                return HFC.evenRound((model.DiscountAmount() / subtotal) * 100.0); //rounding to 4 decimal places
            else
                return 0;
        },
        write: function (val) {
            val = parseFloat(val);
            if (!isNaN(val)) {
                var subtotal = model.Quantity() * model.SalePrice(),
                    amount = HFC.evenRound((val / 100.0) * subtotal, 4);
                model.DiscountAmount(amount);
            }
        }
    });
}

window.ProductList = null;

HFC.Models.JobItem = function (jsModel, quote) {
    jsModel = jsModel || {};

    jsModel.AlertMessage = ko.observable();
    jsModel.JobItemId = jsModel.JobItemId || 0;
    jsModel.JobId = jsModel.JobId;
    jsModel.QuoteId = jsModel.QuoteId;
    jsModel.LastUpdated = jsModel.LastUpdated ? new XDate(jsModel.LastUpdated) : new XDate(); // use this to determine ATOMIC state of data
    jsModel.ProductTypeId = ko.observable(jsModel.ProductTypeId);
    jsModel.ProductType = ko.observable(jsModel.ProductType);
    jsModel.CategoryId = ko.observable(jsModel.CategoryId);
    jsModel.CategoryName = ko.observable(jsModel.CategoryName);
    jsModel.Quantity = ko.observable(jsModel.Quantity || 1);
    jsModel.Description = ko.observable(jsModel.Description);    
    jsModel.IsTaxable = ko.observable(jsModel.IsTaxable || true);           
    jsModel.ListPrice = ko.observable(jsModel.ListPrice || 0); 
    jsModel.SalePrice = ko.observable(jsModel.SalePrice || 0); //price customer pays     
    jsModel.DiscountRemark = ko.observable(jsModel.DiscountRemark);
    jsModel.DiscountAmount = ko.observable(jsModel.DiscountAmount || 0);
    jsModel.DiscountPercent = ko.observable(jsModel.DiscountPercent || 0).extend({ discountPercent: jsModel });   
    jsModel.JobberPrice = ko.observable(jsModel.JobberPrice || 0);     //this is also List price for TL    
    //jsModel.MarginPercent = ko.observable(jsModel.MarginPercent || HFC.Defaults.MarginPercent); //default to whatever we set for franchise
    jsModel.UnitCost = ko.observable(jsModel.UnitCost || 0);//.extend({ unitCost: jsModel }); //cost franchise pays to vendor
    jsModel.JobberDiscountPercent = jsModel.JobberDiscountPercent || HFC.Defaults.JobberDiscountPercent; //multiplier discount from vendor    
    //this sets initial isEditable for existing imported record that may not have a categoryId but does have a Category Name
    jsModel.IsEditable = jsModel.JobItemId == 0 || (jsModel.JobItemId > 0 && jsModel.CategoryId());
    jsModel.Options = ko.observableArray(jsModel.Options ? $.map(jsModel.Options, function (o) {
        return HFC.Models.ProductOption(o);
    }) : null);

    jsModel.Color = ko.computed({
        read: function () {
            var color = $.grep(jsModel.Options(), function (c) {
                return c.Name == "Color";
            });
            if (color && color.length)
                return color[0].Value();
            else
                return "";
        },
        write: function (val) {
            var color = $.grep(jsModel.Options(), function (c) {
                return c.Name == "Color";
            });
            if (color && color.length)
                color[0].Value(val);
            else {
                color = HFC.Models.ProductOption({ Name: "Color", Value: val });
                jsModel.Options.push(color);
            }
        }, deferEvaluation: true
    });

    jsModel.Template = ko.observable("item-view");

    jsModel.ProductList = ko.computed({
        read: function () {
            var list = [];            
            if (jsModel.CategoryId() > 0 && window.ProductList != null) {
                list = $.grep(window.ProductList, function (p) {
                    return p.ProductTypeId == jsModel.ProductTypeId() && p.CategoryId == jsModel.CategoryId()
                });
                if (list.length == 0) {
                    $.ajax({
                        type: 'GET',
                        async: false,
                        dataType: 'json',
                        url: '/api/lookup/' + jsModel.CategoryId() + '/products',
                        success: function (data) {
                            list = data;
                            window.ProductList = window.ProductList.concat(data);
                        },
                        error: function (xhr, textStatus, reasonPhrase) {
                            if (reasonPhrase.length == 0)
                                reasonPhrase = xhr.statusCode == 404 ? "404 - Page not found" : "An error occurred, unable to complete request";
                            HFC.DisplayAlert(reasonPhrase, textStatus == "timeout" ? "alert-warning" : "alert-danger");
                        }
                    });
                }
            } else
                window.ProductList = [];
            return list;
        }, deferEvaluation: true
    });
    jsModel.ProductTypeId.subscribe(function () {
        var prod = $.grep(window.ProductList, function (p) {
            return p.ProductTypeId == jsModel.ProductTypeId() && p.CategoryId == jsModel.CategoryId()
        });
        if (prod && prod.length) {
            jsModel.ProductType(prod[0].ProductType);
            jsModel.CategoryName(prod[0].CategoryName);
            jsModel.Description(prod[0].Description);
            jsModel.IsTaxable(prod[0].IsTaxable);
            jsModel.MSRP(prod[0].MSRP);
            jsModel.ListPrice(prod[0].ListPrice);
            jsModel.JobberPrice(prod[0].JobberPrice);
            jsModel.UnitCost(prod[0].UnitCost);
            jsModel.JobberDiscountPercent = prod[0].JobberDiscountPercent;
        }
    });
    jsModel.ChangeCategory = function (model, jsEvent) {
        if (model.CategoryId()) {
            var opt = jsEvent.currentTarget;
            model.CategoryName($(opt).find(":selected").text());
        } else model.CategoryName("");
    };    

    jsModel.ChangeColor = function (model, jsevent) {
        jsModel.Color($(jsevent.currentTarget).text());
        return false;
    }    

    jsModel.CostSubtotal = ko.computed({
        read: function () {
            return HFC.evenRound(jsModel.Quantity() * jsModel.UnitCost(), 4);
        }, deferEvaluation: true
    })
    jsModel.Subtotal = ko.computed({
        read: function() {
            var subtotal = HFC.evenRound((jsModel.Quantity() * jsModel.SalePrice()) - jsModel.DiscountAmount(), 4);
            //if (subtotal < jsModel.CostSubtotal())
            //    jsModel.AlertMessage("Warning: Subtotal is less than cost");
            //else
            //    jsModel.AlertMessage("");
            return subtotal;
        }, deferEvaluation: true
    });

    jsModel.JobberPrice.subscribe(function () {
        if (jsModel.UnitCost() <= 0) {
            var cost = HFC.evenRound(jsModel.JobberPrice() * (1 - (jsModel.JobberDiscountPercent) / 100.0), 4);
            if (cost != jsModel.UnitCost())
                jsModel.UnitCost(cost);
        }
    });
    
    jsModel.MarginPercent = ko.computed({
        read: function () {
            var margin = 0;
            if(jsModel.UnitCost() > 0 && jsModel.SalePrice() > 0)
                margin = HFC.evenRound((jsModel.SalePrice() - jsModel.UnitCost()) / jsModel.SalePrice(), 4);
            return margin * 100.0;            
        }, deferEvaluation: true
    });

    jsModel.DisplayDiscount = ko.observable(jsModel.DiscountAmount() > 0);

    if (quote) {        
        jsModel.JobberPrice.subscribe(quote.CalculateTotals);
        jsModel.UnitCost.subscribe(quote.CalculateTotals);
        jsModel.ListPrice.subscribe(quote.CalculateTotals);
        jsModel.SalePrice.subscribe(quote.CalculateTotals);
        jsModel.DiscountAmount.subscribe(quote.CalculateTotals);
        jsModel.IsTaxable.subscribe(quote.CalculateTotals);
    }
    jsModel.toJS = function (includeOptAsObject) {
        var data = {
            JobItemId: jsModel.JobItemId,
            QuoteId: jsModel.QuoteId,
            ProductTypeId: jsModel.ProductTypeId(),            
            ProductType: jsModel.ProductType(),
            CategoryId: jsModel.CategoryId(),
            CategoryName: jsModel.CategoryName(),            
            Quantity: jsModel.Quantity(),
            Description: HFC.htmlEncode(jsModel.Description() || ""),
            LastUpdated: jsModel.LastUpdated.toISOString(),            
            IsTaxable: jsModel.IsTaxable(),
            DiscountRemark: jsModel.DiscountRemark(),
            DiscountAmount: jsModel.DiscountAmount(),
            DiscountPercent: jsModel.DiscountPercent(),            
            ListPrice: jsModel.ListPrice(), 
            SalePrice: jsModel.SalePrice(), //price customer pays           
            JobberPrice: jsModel.JobberPrice(),     //this is also List price for TL    
            MarginPercent: jsModel.MarginPercent(),
            UnitCost: jsModel.UnitCost(), //cost franchise pays to vendor
            JobberDiscountPercent: jsModel.JobberDiscountPercent,
            Subtotal: jsModel.Subtotal(),
            CostSubtotal: jsModel.CostSubtotal()
        };
        if (includeOptAsObject)
            data.Options = jsModel.Options();
        else {
            if (jsModel.Options() && jsModel.Options().length) {
                for (var i = 0; i < jsModel.Options().length; i++) {
                    data['Options[' + i + '].OptionId'] = jsModel.Options()[i].OptionId;
                    data['Options[' + i + '].Name'] = jsModel.Options()[i].Name;
                    data['Options[' + i + '].Value'] = jsModel.Options()[i].Value();
                }
            }
        }
        return data;
    }

    jsModel.Clone = function () {
        var obj = jsModel.toJS(true);
        obj.JobItemId = 0;
        obj.Description = HFC.htmlDecode(obj.Description);
        obj.ProductName = HFC.htmlDecode(obj.ProductName);
        return obj;
    }

    return jsModel;
}
