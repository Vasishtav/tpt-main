﻿HFC.Models.LeadSource = function (jsModel) {
    jsModel = jsModel || {};

    jsModel.SourceId = ko.observable(jsModel.SourceId);
    jsModel.LeadId = jsModel.LeadId || 0;
    jsModel.IsManuallyAdded = jsModel.IsManuallyAdded || true;
    jsModel.Source = ko.computed({
        read: function () {
            if (jsModel.SourceId()) {
                return HFC.Lookup.Get(LK_LeadSources, jsModel.SourceId());
            } else return null;
        }, deferEvaluation: true
    })
    jsModel.SrcHtml = ko.computed({
        read: function () {
            var src = jsModel.Source();
            if (src) {
                if (src.Parent && src.Parent.Id)
                    return src.Name + "&nbsp;<small class='muted text-muted'>" + src.Parent.Name + "</small>";
                else
                    return src.Name;
            } else
                return "";
        }, deferEvaluation: true
    });

    return jsModel;
}