﻿// NOTES: REFER TO PERSON.JS
HFC.Models.Address = function (model) {
    model = model || { };

    model.AddressId = model.AddressId;
    model.AttentionText = ko.observable(model.AttentionText);
    model.Address1 = ko.observable(model.Address1);
    model.Address2 = ko.observable(model.Address2);
    model.City = ko.observable(model.City);
    model.State = ko.observable(model.State);
    model.ZipCode = ko.observable(model.ZipCode);
    model.CountryCode2Digits = ko.observable(model.CountryCode2Digits || HFC.Defaults.Country);
    model.CreatedOnUtc = ko.observable(model.CreatedOnUtc);
    model.IsDeleted = ko.observable(model.IsDeleted);
    model.IsResidential = ko.observable(model.IsResidential || true);
    model.CrossStreet = ko.observable(model.CrossStreet);
    model.AddressToString = ko.observable(addressToString(true, model));
    model.selectionChanged = function (event) {
        model.ZipCode('');
    } 
    model.StatesList = ko.computed({
        read: function () {
            if (window.LK_States) {
                return $.grep(window.LK_States, function (s) {
                    return s.CountryISO2 == (model.CountryCode2Digits() || HFC.Defaults.Country)
                });
            } else return null;
        }, deferEvaluation: true
    });

    function addressToString (isSingleLine,model) {
        var str = "";
        if (model.Address1())
            str = model.Address1() + (model.Address2() ? (", " + model.Address2()) : "") + (isSingleLine ? ", " : "<br/>");

        if (model.City())
            str += model.City() + ", " + (model.State() || "") + " " + (model.ZipCode() || "");
        else
            str += (model.ZipCode() || "");

        return str;
    };

    model.GoogleAddressHref = ko.computed({
        read: function () {
            var dest = addressToString(true, model);
            if (model.Address1()) {

                var srcAddr;

                if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                    srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                } else {
                    srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                }

                if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                    return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                } else {
                    return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                }
            } else
                return null;
        }, deferEvaluation: true
    });
    model.Template = ko.observable("address-view");


model.toJS = function (data, prefix) {
        data = data || {};
        data[(prefix || "") + 'AddressId'] = model.AddressId;
        data[(prefix || "") + 'AttentionText'] = model.AttentionText();
        data[(prefix || "") + 'Address1'] = model.Address1();
        data[(prefix || "") + 'Address2'] = model.Address2();
        data[(prefix || "") + 'City'] = model.City();
        data[(prefix || "") + 'State'] = model.State();
        data[(prefix || "") + 'ZipCode'] = model.ZipCode();
        data[(prefix || "") + 'CountryCode2Digits'] = model.CountryCode2Digits();
        data[(prefix || "") + 'CrossStreet'] = model.CrossStreet();
        data[(prefix || "") + 'IsResidential'] = model.IsResidential();
        return data;
    }
model.toBaseJS = function () {
        var data = {
            AddressId: model.AddressId,
            AttentionText: model.AttentionText(),
            Address1: model.Address1(),
            Address2: model.Address2(),
            City: model.City(),
            State: model.State(),
            ZipCode: model.ZipCode(),
            CountryCode2Digits: model.CountryCode2Digits(),
            CreatedOnUtc: model.CreatedOnUtc(),
            IsDeleted: model.IsDeleted(),
            IsResidential: model.IsResidential(),
            CrossStreet: model.CrossStreet()
        }
        return data;
       }

    
return model;
}
// mailing address
HFC.Models.MailingAddress = function (model) {
    model = model || {};

    model.MailingAddressId = model.MailingAddressId;
    model.AttentionText = ko.observable(model.AttentionText);
    model.Address1 = ko.observable(model.Address1);
    model.Address2 = ko.observable(model.Address2);
    model.City = ko.observable(model.City);
    model.State = ko.observable(model.State);
    model.ZipCode = ko.observable(model.ZipCode);
    model.CountryCode2Digits = ko.observable(model.CountryCode2Digits || HFC.Defaults.Country);
    model.CreatedOnUtc = ko.observable(model.CreatedOnUtc);
    model.IsDeleted = ko.observable(model.IsDeleted);
    model.IsResidential = ko.observable(model.IsResidential || true);
    model.CrossStreet = ko.observable(model.CrossStreet);
    model.AddressToString = ko.observable(addressToString(true, model));
    model.selectionChanged = function (event) {
        model.ZipCode('');
    }
    model.StatesList = ko.computed({
        read: function () {
            if (window.LK_States) {
                return $.grep(window.LK_States, function (s) {
                    return s.CountryISO2 == (model.CountryCode2Digits() || HFC.Defaults.Country)
                });
            } else return null;
        }, deferEvaluation: true
    });

    function addressToString(isSingleLine, model) {
        var str = "";
        if (model.Address1())
            str = model.Address1() + (model.Address2() ? (", " + model.Address2()) : "") + (isSingleLine ? ", " : "<br/>");

        if (model.City())
            str += model.City() + ", " + (model.State() || "") + " " + (model.ZipCode() || "");
        else
            str += (model.ZipCode() || "");

        return str;
    };

    model.GoogleAddressHref = ko.computed({
        read: function () {
            var dest = addressToString(true, model);
            if (model.Address1()) {

                var srcAddr;

                if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                    srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                } else {
                    srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                }

                if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                    return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                } else {
                    return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                }
            } else
                return null;
        }, deferEvaluation: true
    });
    model.Template = ko.observable("address-view");


    model.toJS = function (data, prefix) {
        data = data || {};
        data[(prefix || "") + 'MailingAddressId'] = model.MailingAddressId;
        data[(prefix || "") + 'AttentionText'] = model.AttentionText();
        data[(prefix || "") + 'Address1'] = model.Address1();
        data[(prefix || "") + 'Address2'] = model.Address2();
        data[(prefix || "") + 'City'] = model.City();
        data[(prefix || "") + 'State'] = model.State();
        data[(prefix || "") + 'ZipCode'] = model.ZipCode();
        data[(prefix || "") + 'CountryCode2Digits'] = model.CountryCode2Digits();
        data[(prefix || "") + 'CrossStreet'] = model.CrossStreet();
        data[(prefix || "") + 'IsResidential'] = model.IsResidential();
        return data;
    }
    model.toBaseJS = function () {
        var data = {
            MailingAddressId: model.MailingAddressId,
            AttentionText: model.AttentionText(),
            Address1: model.Address1(),
            Address2: model.Address2(),
            City: model.City(),
            State: model.State(),
            ZipCode: model.ZipCode(),
            CountryCode2Digits: model.CountryCode2Digits(),
            CreatedOnUtc: model.CreatedOnUtc(),
            IsDeleted: model.IsDeleted(),
            IsResidential: model.IsResidential(),
            CrossStreet: model.CrossStreet()
        }
        return data;
    }


    return model;
}


