﻿ko.extenders.SAPercent = function (target, model) {
    return ko.computed({
        read: function () {
            if (model.TypeEnum() == SaleAdjTypeEnum.Tax)
                return target();
            else {
                if (model.JobSubtotal() > 0) {
                    var val = HFC.evenRound(model.Amount() / model.JobSubtotal(), 4) * 100.0;
                    return val.toFixed(2);
                } else
                    return 0;
            }
        },
        write: function (val) {
            val = parseFloat(val);
            if (!isNaN(val)) {
                target(val);
                if (model.TypeEnum() != SaleAdjTypeEnum.Tax) {
                    var amt = model.JobSubtotal() * (val / 100.0);
                    model.Amount(HFC.evenRound(amt, 4));
                }
            }
        }, deferEvaluation: true
    });
}
HFC.Models.SaleAdjustment = function (jsModel, quote) {
    jsModel = jsModel || {};

    jsModel.SaleAdjustmentId = jsModel.SaleAdjustmentId || 0;
    jsModel.JobId = jsModel.JobId || 0;
    jsModel.QuoteId = jsModel.QuoteId || 0;

    jsModel.JobSubtotal = quote.Subtotal;
    jsModel.Category = ko.observable(jsModel.Category);
    jsModel.Amount = ko.observable(jsModel.Amount || 0);    
    jsModel.Memo = ko.observable(jsModel.Memo);
    jsModel.TypeEnum = ko.observable(jsModel.TypeEnum);
    jsModel.IsTaxable = ko.observable(jsModel.IsTaxable);
    jsModel.LastUpdated = jsModel.LastUpdated ? new XDate(jsModel.LastUpdated) : new XDate();
    jsModel.Template = ko.observable("sales-adj-view");

    jsModel.Percent = ko.observable(jsModel.Percent || 0).extend({ SAPercent: jsModel });

    jsModel.ChangeCategory = function (model, jsEvent) {
        var ddl = $(jsEvent.currentTarget);
        jsModel.TypeEnum(parseInt(ddl.find(":selected").parent().data("type")));
    }
    if (quote) {
        if(jsModel.TypeEnum() == SaleAdjTypeEnum.Tax)
            jsModel.Percent.subscribe(quote.CalculateTotals);
        else
            jsModel.Amount.subscribe(quote.CalculateTotals);        
    }
    jsModel.toJS = function () {
        return {
            SaleAdjustmentId: jsModel.SaleAdjustmentId,
            QuoteId: jsModel.QuoteId,
            JobId: jsModel.JobId,
            Category: jsModel.Category(),
            Amount: jsModel.Amount(),
            Memo: HFC.htmlEncode(jsModel.Memo()),
            TypeEnum: jsModel.TypeEnum(),
            IsTaxable: jsModel.IsTaxable(),
            LastUpdated: jsModel.LastUpdated.toISOString(),
            Percent: jsModel.Percent()
        };
    }

    return jsModel;
};