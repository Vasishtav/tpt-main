﻿HFC.Models.ProductOption = function (jsModel) {
    jsModel = jsModel || {};

    jsModel.OptionId = jsModel.OptionId || 0;
    jsModel.Name = jsModel.Name;
    jsModel.DefaultValue = jsModel.Value;
    jsModel.Value = ko.observable(jsModel.Value);
    
    return jsModel;
}