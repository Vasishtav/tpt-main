﻿HFC.Models.Quote = function (jsModel) {
    jsModel = jsModel || {};

    jsModel.QuoteId = jsModel.QuoteId || 0;
    jsModel.JobId = jsModel.JobId || 0;
    jsModel.QuoteNumber = jsModel.QuoteNumber || 1;
    jsModel.IsPrimary = ko.observable(jsModel.IsPrimary);
    jsModel.LastUpdated = jsModel.LastUpdated ? new XDate(jsModel.LastUpdated) : new XDate();
        
    jsModel.CostSubtotal = ko.observable(jsModel.CostSubtotal || 0);
    jsModel.Subtotal = ko.observable(jsModel.Subtotal || 0);
    jsModel.TaxPercent = ko.observable(jsModel.TaxPercent || 0);
    jsModel.DiscountTotal = ko.observable(jsModel.DiscountTotal || 0);
    jsModel.SurchargeTotal = ko.observable(jsModel.SurchargeTotal || 0);
    jsModel.TaxTotal = ko.observable(jsModel.TaxTotal || 0);
    jsModel.NetTotal = ko.observable(jsModel.NetTotal || 0);
    jsModel.NetProfit = ko.observable(jsModel.NetProfit || 0);
    jsModel.Margin = ko.observable(jsModel.Margin || 0);
    
    jsModel.CalculateTotals = function () {
        setTimeout(function () {
            var subtotal = 0, costsubtotal = 0, discount = 0, surcharge = 0, taxpercent = 0, taxable_subtotal = 0;
            $.each(jsModel.JobItems(), function (i, s) {
                subtotal += parseFloat(s.Subtotal() || 0);
                costsubtotal += parseFloat(s.CostSubtotal() || 0);
                if (s.IsTaxable())
                    taxable_subtotal += parseFloat(s.Subtotal());
            });
            $.each(jsModel.SaleAdjustments(), function (i, s) {
                var amt = parseFloat(s.Amount() || 0);
                if (s.TypeEnum() == SaleAdjTypeEnum.Surcharge) {
                    surcharge += amt;
                    if (s.IsTaxable())
                        taxable_subtotal += amt;
                } else if (s.TypeEnum() == SaleAdjTypeEnum.Discount)
                    discount += amt;
                else if (s.TypeEnum() == SaleAdjTypeEnum.Tax)
                    taxpercent += parseFloat(s.Percent() || 0);
            });
            var taxtotal = taxable_subtotal * (taxpercent / 100.0);
            jsModel.Subtotal(HFC.evenRound(subtotal, 4));
            jsModel.CostSubtotal(HFC.evenRound(costsubtotal, 4));
            jsModel.TaxPercent(HFC.evenRound(taxpercent, 4));
            jsModel.DiscountTotal(HFC.evenRound(discount, 4));
            jsModel.SurchargeTotal(HFC.evenRound(surcharge, 4));
            jsModel.TaxTotal(HFC.evenRound(taxtotal, 4));
            jsModel.NetTotal(HFC.evenRound(subtotal + taxtotal + surcharge - discount, 2));
            jsModel.NetProfit(HFC.evenRound(subtotal + surcharge - costsubtotal - discount, 2));
            jsModel.Margin(HFC.evenRound((subtotal + surcharge - costsubtotal - discount) / (subtotal + surcharge - discount) * 100.0, 2));
        }, 0);
    }

    //This is important to add this peroperty after Subtotal since the calculation will need it
    jsModel.JobItems = ko.observableArray(jsModel.JobItems ? $.map(jsModel.JobItems, function (i) {
        return HFC.Models.JobItem(i, jsModel);
    }) : null);

    jsModel.SaleAdjustments = ko.observableArray(jsModel.SaleAdjustments ? $.map(jsModel.SaleAdjustments, function (s) {
        return HFC.Models.SaleAdjustment(s, jsModel);
    }) : null);

    return jsModel;
}