﻿HFC.Models.FranchiseOwner = function (model) {
    model = model || {};
    model.OwnerId = ko.observable(model.OwnerId);
    model.UserId = ko.observable(model.UserId);
    model.FranchiseId = model.FranchiseId;
    model.OwnershipPercent = ko.observable(model.OwnershipPercent);
    model.CreatedOnUtc = ko.observable(model.CreatedOnUtc);
    model.User = HFC.Models.User(model.User);    
    model.Template = ko.observable("FranchiseOwners-view");


model.toJS = function (data, prefix) {
        data = data || {};
        data[(prefix || "") + OwnerId] = model.OwnerId;
        data[(prefix || "") + UserId] = model.UserId;
        data[(prefix || "") + FranchiseId] = model.FranchiseId;
        data[(prefix || "") + OwnershipPercent] = model.OwnershipPercent;
        data[(prefix || "") + CreatedOnUtc] = model.CreatedOnUtc;
        data[(prefix || "") + User] = model.User;
        return data;
    }
    model.toBaseJS = function () {
        var data = {
            OwnerId: ko.toJS(model.OwnerId()),
            UserId:  ko.toJS(model.UserId()),
            FranchiseId: model.FranchiseId,
            OwnershipPercent:  ko.toJS(model.OwnershipPercent()),
            CreatedOnUtc:  ko.toJS(model.CreatedOnUtc()),
            User:ko.toJS(model.User)
        }
        return data;
    }
    model.deleteOwner = function (model,e) {
        try {
            e.preventDefault();
            franchiseModel.FranchiseOwners.remove(this);
        }
        catch (e)
        {
        }
    }

    model.saveOwner = function (model, e) {
        e.preventDefault();
        var _owner = model.toBaseJS(model);   
        var franchise = new HFC.VM.Franchise(model.FranchiseId);
         franchise.saveOwner(_owner);
    }
   
    return model;
}