﻿//This model is dependent on HFC.Models.Event and HFC.Models.Recurring
HFC.Models.Calendar = function (model) {
    model = model || { id: 0 };
    HFC.Models.Event(model);
    //model.AptTypeEnum = ko.observable(model.AptTypeEnum || 1); //1 is appointment    
    model.AppointmentTypeEnum = ko.observable(lk_AppointmentTypeEnum); //1 is appointment    
    model.AptTypeEnum = ko.observable(model.AptTypeEnum || 1); //1 is appointment    
    model.EventRecurring = HFC.Models.Recurring(model.EventRecurring);
    model.RecurringEventId = ko.observable(model.RecurringEventId);
    model.EventType = ko.observable(model.EventType || 0); //0 is same as EventTypeEnum.Single
    model.RepeatIsChecked = ko.observable(!!(model.RecurringEventId() > 0));
    model.RepeatIsDisabled = ko.computed({ read: function () { return model.EventType() == EventTypeEnum.Occurrence; }, deferEvaluation: true });

    //fullCalendar will null end date if it is equal to start date, so we undo that here
    if (model.end() == null)
        model.end(model.start().clone());
    
    model.ToggleRepeat = function () {
        model.RepeatIsChecked(!model.RepeatIsChecked());
    }
    model.EventTypeGlyph = ko.computed(function () {
        if (model.EventType() == EventTypeEnum.Series)
            return "glyphicon glyphicon-repeat";
        else if (model.EventType() == EventTypeEnum.Occurrence)
            return "glyphicon glyphicon-record";
        else
            return "";
    });

    model.AptTypeGlyph = ko.computed(function () {
        return HFC.getAptGlyphIcon(model.AptTypeEnum());
    });

    model.toJS = function () {
        var data = model.toBaseJS();
        //data.AptTypeEnum = model.AppointmentTypeEnum();
        data.AptTypeEnum = model.AptTypeEnum();
        data.RecurringEventId = model.RecurringEventId();
        data.EventType = model.EventType();

        if (model._start) {
            var cl = XDate(model._start);
            data.OriginalStartDate = cl.toString("u");
        }
        if (model.RepeatIsChecked() && !model.RepeatIsDisabled()) {
            var StartDate = model.start();
            //StartDate.addDays(parseInt(model.start().diffDays(model.end())))
            //StartDate.setHours(model.allDay() ? 0 : model.start().getHours());
            //StartDate.setMinutes(model.allDay() ? 0 : model.start().getMinutes());
            //StartDate.setSeconds(0);
            //StartDate.setMilliseconds(0);

            var EndDate = StartDate.clone();
            //fix MYC-615
            if (model.end().getDate() > model.start().getDate()) {
                EndDate.setDate(model.end().getDate());
            }

            EndDate.addDays(parseInt(model.start().diffDays(model.end()))); //parseInt because diffDays will return decimal that can be added to end date
            EndDate.setHours(model.allDay() ? 0 : model.end().getHours());
            EndDate.setMinutes(model.allDay() ? 0 : model.end().getMinutes());
            data['EventRecurring.StartDate'] = StartDate.toString("u");

            var repeatedEndDate = EndDate.clone();
            data['EventRecurring.EndDate'] = repeatedEndDate.addDays(-parseInt(model.start().diffDays(model.end()))).toString("u");
            data["EventType"] = EventTypeEnum.Series;

            data['start'] = StartDate.toString("u");
            data['end'] = EndDate.toString("u");
            if (StartDate.toDateString() != EndDate.toDateString()) {
                //fix MYC-615
                //data['EventRecurring.EndsOn'] = EndDate.toString("u");
            }
            if (model.EventRecurring.EndsOn()) {
                var endsOn = model.EventRecurring.EndsOn();
                endsOn.setHours(model.end().getHours());
                endsOn.setMinutes(model.end().getMinutes());
                endsOn.setSeconds(0);
                endsOn.setMilliseconds(0);
                data['EventRecurring.EndsOn'] = endsOn.toString("u");
            }
            data['EventRecurring.PatternEnum'] = model.EventRecurring.PatternEnum();
            data['EventRecurring.RecursEvery'] = model.EventRecurring.RecursEvery();
            if (model.EventRecurring.EndsAfterXOccurrences() > 0)
                data['EventRecurring.EndsAfterXOccurrences'] = model.EventRecurring.EndsAfterXOccurrences();
            if (model.EventRecurring.PatternEnum() == RecurringPatternEnum.Monthly || model.EventRecurring.PatternEnum() == RecurringPatternEnum.Yearly) {
                if (model.EventRecurring.PatternEnum() == RecurringPatternEnum.Yearly)
                    data['EventRecurring.MonthEnum'] = model.EventRecurring.MonthEnum();
                if (model.EventRecurring.RelativeGroup() == "Day") {
                    data['EventRecurring.DayOfMonth'] = model.EventRecurring.DayOfMonth();
                } else {
                    data['EventRecurring.DayOfWeekEnum'] = model.EventRecurring.DayOfWeekRelativeEnum();
                    data['EventRecurring.DayOfWeekIndex'] = model.EventRecurring.DayOfWeekIndex();
                }
            } else if (model.EventRecurring.PatternEnum() == RecurringPatternEnum.Weekly && model.EventRecurring.DayOfWeekEnum() > 0)
                data['EventRecurring.DayOfWeekEnum'] = model.EventRecurring.DayOfWeekEnum();

        } else if (!model.RepeatIsChecked() && model.EventType() == EventTypeEnum.Series)
            data.RecurringEventId = null;

        return data;
    }

    return model;
}