﻿/*
* This will simply turn a person model into knockout models, it doesn't create a new js object, it just converts properties into knockout observable.
* This is also necessary instead of using ko.mapping because this allows us to pass nullable properties and still have proper observable properties to bind for editing
* Sample usage: var koModel = HFC.Models.Person(rawModel); //for single model
*               OR 
*               viewModel.Collection = observableArray(rawModelCollection ? $.map(rawModelCollection, function(m) { return HFC.Models.Person(m); }) : null); //for observable collection of models
*/
HFC.Models.Person = function (model) {
    model = model || { };

    model.PersonId = model.PersonId || 0;
    model.FirstName = ko.observable(model.FirstName);
    model.LastName = ko.observable(model.LastName);

    model.FullName = ko.computed(function () {
        return $.trim(model.FirstName()) + " " + $.trim(model.LastName());
    });

    model.HomePhone = ko.observable(HFC.formatPhone(model.HomePhone));
    model.CellPhone = ko.observable(HFC.formatPhone(model.CellPhone));
    model.WorkPhone = ko.observable(HFC.formatPhone(model.WorkPhone));
    model.FaxPhone = ko.observable(HFC.formatPhone(model.FaxPhone));
    model.WorkPhoneExt = ko.observable(model.WorkPhoneExt);

    model.PrimaryEmail = ko.observable(model.PrimaryEmail);

    model.Editable = function () {
        if (model.PersonId != 0) {
            return false;
        } else {
            return true;
        }
    };
    
    model.SecondaryEmail = ko.observable(model.SecondaryEmail);

    model.IsSubscribed = ko.observable(model.UnsubscribedOnUtc == null);
    model.UnsubscribedOnUtc = model.UnsubscribedOnUtc ? new XDate(model.UnsubscribedOnUtc) : null;
    
    model.WorkTitle = ko.observable(model.WorkTitle);
    model.CompanyName = ko.observable(model.CompanyName);

    model.PreferredTFN = ko.observable(model.PreferredTFN || "H");    

    model.PreferredTFNHtml = ko.computed({
        read: function () {
            if (model.PreferredTFN() == "C" && model.CellPhone())
                return "<abbr title='Cell Phone'>C:</abbr><a href='tel:" + HFC.formatPhone(model.CellPhone()) + "'><span>" + HFC.formatPhone(model.CellPhone()) + "</span></a>";
            else if (model.PreferredTFN() == "W" && model.WorkPhone())
                return "<abbr title='Work Phone'>W:</abbr><a href='tel:" + HFC.formatPhone(model.WorkPhone()) + "'><span>" + HFC.formatPhone(model.WorkPhone()) + (model.WorkPhoneExt() ? (" x" + model.WorkPhoneExt()) : "") + "</span></a>";
            else if (model.HomePhone())
                return "<abbr title='Home Phone'>H:</abbr> <a href='tel:" + HFC.formatPhone(model.HomePhone()) + "'><span>" + HFC.formatPhone(model.HomePhone()) + "</span></a>";
            else
                return "";

        }, deferEvaluation: true
    });

   

    model.Template = ko.observable("person-view");

model.toJS = function (data, prefix) {
        data = data || {};
        data[(prefix || "") + 'PersonId'] = model.PersonId;
        data[(prefix || "") + 'FirstName'] = model.FirstName();
        data[(prefix || "") + 'LastName'] = model.LastName();
        data[(prefix || "") + 'HomePhone'] = model.HomePhone();
        data[(prefix || "") + 'CellPhone'] = model.CellPhone();
        data[(prefix || "") + 'WorkPhone'] = model.WorkPhone();
        data[(prefix || "") + 'FaxPhone'] = model.FaxPhone();
        data[(prefix || "") + 'WorkPhoneExt'] = model.WorkPhoneExt();
        data[(prefix || "") + 'PrimaryEmail'] = model.PrimaryEmail();
        data[(prefix || "") + 'SecondaryEmail'] = model.SecondaryEmail();
        data[(prefix || "") + 'UnsubscribedOnUtc'] = model.IsSubscribed() ? null : new Date().toISOString();
        data[(prefix || "") + 'WorkTitle'] = model.WorkTitle();
        data[(prefix || "") + 'CompanyName'] = model.CompanyName();
        data[(prefix || "") + 'PreferredTFN'] = model.PreferredTFN();
        return data;
    }
model.toBaseJS = function () {
    var test = model;
        var data = {
            PersonId: model.PersonId,
            FirstName: model.FirstName(),
            LastName: model.LastName(),
            //MI: model.MI,
            HomePhone: model.HomePhone(),
            CellPhone: model.CellPhone(),
            CompanyName: model.CompanyName(),
            WorkTitle: model.WorkTitle(),
            WorkPhone: model.WorkPhone(),
            WorkPhoneExt: model.WorkPhoneExt(),
            FaxPhone: model.FaxPhone(),
            PrimaryEmail: model.PrimaryEmail(),
            SecondaryEmail: model.SecondaryEmail(),
            //Notes: model.Notes(),
            //IsDeleted: model.IsDeleted(),
            //CreatedOnUtc: model.CreatedOnUtc(),
            //UnsubscribedOnUtc: model.UnsubscribedOnUtc(),
            PreferredTFN: model.PreferredTFN(),
        }
        return data;
    }

return model;
}