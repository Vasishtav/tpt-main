﻿HFC.Models.Tax = function (t) {
    t = t || {};

    t.TaxRuleId = t.TaxRuleId;
    t.County = ko.observable(t.County);
    t.ZipCode = ko.observable(t.ZipCode);
    t.City = ko.observable(t.City);
    t.State = ko.observable(t.State);
    t.Percent = ko.observable(t.Percent);
    t.Default = ko.observable(t.Default);
    t.RuleName = ko.observable(t.RuleName);
    t.InEdit = ko.observable();
    t.Label = ko.computed({
        read: function () {
            if (t.ZipCode() || t.City()) {
                var arr = [];
                if (t.ZipCode())
                    arr.push(t.ZipCode());
                if (t.City())
                    arr.push(t.City());
                if (t.State())
                    arr.push(t.State());
                return arr.join(", ");
                //return t.RuleName();
            } else if (t.County())
                return t.County();
            else if (t.Default() || t.IsBaseTax)
                return "Base Tax";
            else
                return "Available For Selection";
        }, deferEvaluation: true
    });

    return t;
}