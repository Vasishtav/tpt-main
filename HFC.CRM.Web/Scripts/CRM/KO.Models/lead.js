﻿/*
* This model is dependent on Notes, Address, LeadSource and Person
*/
HFC.Models.Lead = function (jsModel) {
    jsModel = jsModel || { };

    jsModel.LeadId = jsModel.LeadId || 0;
    jsModel.LeadNumber = jsModel.LeadNumber || 0;
    jsModel.CreatedOnUtc = jsModel.CreatedOnUtc ? new XDate(jsModel.CreatedOnUtc) : null;
    jsModel.Addresses = new HFC.VM.Addresses(jsModel.Addresses, jsModel.LeadId);

    jsModel.PrimPerson = HFC.Models.Person(jsModel.PrimPerson);
    jsModel.SecPerson = HFC.Models.Person(jsModel.SecPerson);

    if(HFC.VM.Notes)
        jsModel.LeadNotes = new HFC.VM.Notes(jsModel.LeadNotes, jsModel.LeadId ? ("/api/leads/" + jsModel.LeadId + "/notes") : null);

    jsModel.LeadSources = ko.observableArray(jsModel.LeadSources ? $.map(jsModel.LeadSources, function (s) {        
        return HFC.Models.LeadSource(s);
    }) : null);

    jsModel.Jobs = ko.observableArray(jsModel.Jobs ? $.map(jsModel.Jobs, function (j, i) {
        var isCollapsed = true;
        if (HFC.QS && HFC.QS.JobId)
            isCollapsed = j.JobId != HFC.QS.JobId;
        else
            isCollapsed = i > 0;
        if (HFC.Models.Job) {
            j.LeadNumber = jsModel.LeadNumber;
            HFC.Models.Job(j, isCollapsed);
            //retrieve the person from Lead.PrimPerson or SecPerson so they can share same object for updates
            j.Customer = jsModel.PrimPerson.PersonId == j.CustomerPersonId() ? jsModel.PrimPerson : jsModel.SecPerson;
            //retrieve the addresses from Lead.Addresses so they can share the same object for updates
            j.BillingAddress = jsModel.Addresses.Get(j.BillingAddressId());
            j.InstallAddress = jsModel.Addresses.Get(j.InstallAddressId());
        }
        return j;
    }) : null);

    jsModel.JobRowEnabled = ko.observable();
    jsModel.SearchTerm = ko.observable();
    jsModel.LeadStatusId = ko.observable(jsModel.LeadStatusId || 1);
    jsModel.LeadStatusColor = ko.computed({
        read: function () {
            var status = HFC.Lookup.Get(LK_LeadStatuses, jsModel.LeadStatusId());
            if (status.ParentId)
                status = HFC.Lookup.Get(LK_LeadStatuses, status.ParentId);
            return status.ClassName || "btn-default";
        }, deferEvaluation: true
    });
    jsModel.LeadStatusName = ko.computed({
        read: function () {
            var status = HFC.Lookup.Get(LK_LeadStatuses, jsModel.LeadStatusId()),
                parent = null;
            //if (status.ParentId)
            //    parent = HFC.Lookup.Get(LK_LeadStatuses, status.ParentId);
            if (parent)
                return status.Name + "&nbsp;<small class='muted text-muted'>" + parent.Name + "</small>";
            else
                return status.Name;
        }, deferEvaluation: true
    });

    jsModel.LeadFullName = ko.computed(function () {
        var fullname = "";
        if (jsModel.PrimPerson) {
            fullname = $.trim(jsModel.PrimPerson.FirstName()); //trim will convert null to empty string, great for our concatenation
            if (jsModel.SecPerson && jsModel.SecPerson.PersonId) {
                if (jsModel.SecPerson.LastName() && jsModel.PrimPerson.LastName() && jsModel.SecPerson.LastName().toLowerCase() == jsModel.PrimPerson.LastName().toLowerCase())
                    fullname += " & " + $.trim(jsModel.SecPerson.FirstName()) + " " + $.trim(jsModel.PrimPerson.LastName());
                else
                    fullname += " " + $.trim(jsModel.PrimPerson.LastName()) + " & " + $.trim(jsModel.SecPerson.FirstName()) + " " + $.trim(jsModel.SecPerson.LastName());
            } else
                fullname += " " + $.trim(jsModel.PrimPerson.LastName());

            if (jsModel.PrimPerson.CompanyName())
                fullname = "<b>" + jsModel.PrimPerson.CompanyName() + "</b>" + "<br/>" + fullname;
            else
                fullname = "<b>" + fullname + "</b>";
        }
        return fullname;
    });
        
    //jsModel.SourceToHtml = ko.computed({
    //    read: function () {
    //        var source = "";
    //        if (jsModel.LeadSources().length) {
    //            var srcArray = $.map(jsModel.LeadSources(), function (src) {
    //                if (src.SourceId()) {
    //                    var str = "",
    //                        srcType = HFC.Lookup.Get(LK_LeadSources, src.SourceId());
    //                    if (!srcType)
    //                        return null;
    //                    if (srcType.ParentId) {
    //                        var parentSrc = HFC.Lookup.Get(LK_LeadSources, srcType.ParentId);
    //                        if (parentSrc)
    //                            str = parentSrc.Name + " - " + srcType.Name;
    //                        else
    //                            str = srcType.Name;
    //                    } else
    //                        str = srcType.Name;

    //                    if (src.IsManuallyAdded)
    //                        return str;
    //                    else
    //                        return str + " (A)";
    //                } else return null;
    //            });
    //            return srcArray.join("<br/>");
    //        }
    //        return source;
    //    }, deferEvaluation: true
    //});        

    jsModel.LeadStatusOnChanged = function (model, jsEvent) {
        var element = jsEvent.currentTarget,
            val = $(element).data("value");
        if (model.LeadStatusId() != val) {
            //var _url = HFC.Util.BaseURL() + '/api/leads/Status';
            //var data = JSON.stringify({ "id": model.LeadId, "LeadStatusId": val });
            //HFC.AjaxBasic("POST", HFC.Util.BaseURL() + '/api/leads/Status', JSON.stringify({ "id": model.LeadId, "LeadStatusId": val }), function () {
            HFC.AjaxBasic("PUT", HFC.Util.BaseURL() + '/api/leads/' + model.LeadId + '/Status', JSON.stringify({ "Id": val }), function () {

                model.LeadStatusId(val);
                HFC.DisplaySuccess("Status updated for Lead #" + model.LeadNumber);
            });
        }
    };

    return jsModel;
}