﻿HFC.Models.Recurring = function (model) {
    model = model || {};
    var today = new XDate();

    model.RecurringEventId = model.RecurringEventId || 0;
    model.StartDate = ko.observable(model.StartDate ? new XDate(model.StartDate) : new XDate());
    model.EndDate = ko.observable(model.EndDate ? new XDate(model.EndDate) : new XDate());
    model.EndsOn = ko.observable(model.EndsOn ? new XDate(model.EndsOn) : null);
    model.EndsAfterXOccurrences = ko.observable(model.EndsAfterXOccurrences);
    model.RecursEvery = ko.observable(model.RecursEvery || 1);
    model.DayOfWeekEnum = ko.observable(model.DayOfWeekEnum);
    model.MonthEnum = ko.observable(model.MonthEnum || (today.getMonth() + 1));
    model.DayOfMonth = ko.observable(model.DayOfMonth || today.getDate());
    model.PatternEnum = ko.observable(model.PatternEnum || 1); //1 is same as RecurringPatternEnum.Daily but instead of using enum we will use number just in case the RecurringPatternEnum JS object is not loaded
    model.DayOfWeekIndex = ko.observable(model.DayOfWeekIndex);

    var selection = "", relativeGroup = "Day";
    if (model.EndsAfterXOccurrences() > 0)
        selection = "After";
    else if (model.EndsOn())
        selection = "EndsOn";
    else
        selection = "Never";
    if (model.DayOfWeekIndex())
        relativeGroup = "Relative";

    model.EndSelection = ko.observable(selection);
    model.RelativeGroup = ko.observable(relativeGroup);
    model.DayOfWeekRelativeEnum = ko.observable(model.DayOfWeekEnum() || 1);

    model.StartsOnText = ko.computed({
        read: function () {
            return model.StartDate().toString("MM/dd/yyyy");
        }, write: function (val) {
            model.StartDate(new XDate(val));
        }
    });
    model.RepeatEveryText = ko.computed(
        function () {
            var text = "days";
            if (model.PatternEnum() == RecurringPatternEnum.Weekly)
                text = "weeks";
            else if (model.PatternEnum() == RecurringPatternEnum.Monthly)
                text = "months";
            else if (model.PatternEnum() == RecurringPatternEnum.Yearly)
                text = "years";
            return text;
        }
    );

    model.PatternEnum.subscribe(function () {
        setTimeout(function () { $("#month_warning").tooltip({ container: "#evt_recurring" }) }, 100);
    });

    model.EndSelection.subscribe(function () {
        if (model.EndSelection() == "After") {
            model.EndsAfterXOccurrences(10);
            model.EndsOn(null);
        } else if (model.EndSelection() == "EndsOn") {
            model.EndsOn(new XDate());//Globalize.getDateAdd(new Date(), 43200)); //30 days
            model.EndsAfterXOccurrences(null);
        } else {
            model.EndsOn(null);
            model.EndsAfterXOccurrences(null);
        }
    });

    model.EndsOnText = ko.computed({
        read: function () {
            if (model.EndsOn())
                return model.EndsOn().toString("MM/dd/yyyy");
            else
                return "";
        }, write: function (val) {
            var date = new XDate(new Date(val));
            if (date.valid())
                model.EndsOn(date);
        }
    });
    model.DayOfWeekFlag = function (flagValue) {
        return ko.computed({
            read: function () {
                return !!(model.DayOfWeekEnum() & flagValue);
            },
            write: function (checked) {
                if (checked)
                    model.DayOfWeekEnum(model.DayOfWeekEnum() | flagValue);
                else
                    model.DayOfWeekEnum(model.DayOfWeekEnum() & ~flagValue);
            }
        });
    }
    model.Summary = ko.computed(
        function () {
            var text = "";
            if (model.PatternEnum() == RecurringPatternEnum.Daily) {
                if (model.RecursEvery() == 1)
                    text = "Daily";
                else
                    text = "Every " + model.RecursEvery() + " days";
            } else if (model.PatternEnum() == RecurringPatternEnum.Weekly) {
                var daysArr = [];
                for (var name in DayOfWeekEnum) {
                    if ((model.DayOfWeekEnum() & DayOfWeekEnum[name]) && $.inArray(name, daysArr) < 0)
                        daysArr.push(name);
                }

                if (daysArr.length == 0)
                    daysArr.push(model.StartDate().toString("dddd"));
                if (model.RecursEvery() == 1)
                    text = "Weekly on " + (daysArr.length == 7 ? "all days" : daysArr.join(", "));
                else
                    text = "Every " + model.RecursEvery() + " weeks on " + (daysArr.length == 7 ? "all days" : daysArr.join(", "));

            } else if (model.PatternEnum() == RecurringPatternEnum.Monthly) {
                if (model.RecursEvery() == 1)
                    text = "Monthly";
                else
                    text = "Every " + model.RecursEvery() + " months";
                if (model.RelativeGroup() == "Day")
                    text += " on day " + model.DayOfMonth();
                else {
                    var dowIndexName = HFC.Lookup.GetKeyByValue(window.DayOfWeekIndexEnum, model.DayOfWeekIndex()),
                        dow = HFC.Lookup.GetKeyByValue(window.DayOfWeekEnum, model.DayOfWeekRelativeEnum());
                    text += " on the " + dowIndexName + " " + dow;
                }
            } else if (model.PatternEnum() == RecurringPatternEnum.Yearly) {
                if (model.RecursEvery() == 1)
                    text = "Yearly";
                else
                    text = "Every " + model.RecursEvery() + " years";
                var monthName = HFC.Lookup.GetKeyByValue(window.MonthEnum, model.MonthEnum());
                if (model.RelativeGroup() == "Day")
                    text += " on " + monthName + " " + model.DayOfMonth();
                else {
                    var dowIndexName = HFC.Lookup.GetKeyByValue(window.DayOfWeekIndexEnum, model.DayOfWeekIndex()),
                        dow = HFC.Lookup.GetKeyByValue(window.DayOfWeekEnum, model.DayOfWeekRelativeEnum());
                    text += " on the " + dowIndexName + " " + dow + " of " + monthName;
                }
            }
            if (model.EndSelection() == "After") {
                if (model.EndsAfterXOccurrences() == 1)
                    text = "Once";
                else
                    text += ", " + model.EndsAfterXOccurrences() + " times";
            } else if (model.EndSelection() == "EndsOn" && model.EndsOn()) {
                text += ", until " + model.EndsOn().toString("MMM d, yyyy");
            }
            return text;
        }
    );

    return model;
}