﻿HFC.FormatSelection = function (email) {
    return "<span title='" + email.id + "'>" + (email.FullName || email.id) + "</span>";
}

$(document).ready(function () {
    $("#emailEditor").summernote({
       
        height: 200,
        toolbar: [
            //['style', ['style']], // no style button
            ['font', ['bold', 'italic', 'underline']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
            ['insert', ['link']], // no 'picture' insert buttons since, images are submitted as base64 string 
            //['table', ['table']], // no table button
            ['view', ['fullscreen']], //, 'codeview'
            ['help', ['help']] //no help button
        ]
    });

    $(".select2").select2({
        multiple: true,
        width: '100%',
        tags: "",
        placeholder: "Recipients",
        tokenSeparators: [",", " ", ";"],
        maximumSelectionSize: 10,
        maximumInputLength: 256,
        minimumInputLength: 1,        
        formatSelection: HFC.FormatSelection,
        createSearchChoice: function(term){
            if (HFC.Regex.EmailPattern.test(term))
                return { id: term, text: term };
            else
                return null;
        },
        tags: $.map(HFC.EmployeesJS, function (usr) {
            var text = usr.FullName || usr.Email;
            if (usr.FullName)
                text += " <" + usr.Email + ">";
            return { id: usr.Email, text: text, FullName: usr.FullName }
        })
        //// We are not using ajax contact lookup, for now its good enough to just use contacts from the employee list
        //// uncomment and update accordingly if we want to sync with google and exchange contact for a longer list
        //, ajax: { 
        //    url: "/api/lookup/contacts",            
        //    data: function (term, page) {
        //        return {
        //            q: term, // search term
        //            page_limit: 10                    
        //        };
        //    },
        //    results: function (data, page) { 
        //        return { results: data.contacts };
        //    }
        //}
    }).on("select2-blur", function (e) {
        if ($(e.currentTarget).attr("required")) {
            var visibleInput = $(e.currentTarget).prev().find("input");
            if (e.currentTarget.value.length) {
                visibleInput.tooltip('destroy');
            } else {
                var id = visibleInput.attr("id");
                var num = id.substr(id.length - 1, 1);
                id = id.substr(0, id.length - 1) + (parseInt(num) - 1);
                visibleInput.tooltip({
                    title: 'Please fill out this field',
                    trigger: 'manual',
                    container: "#" + id
                });
                visibleInput.tooltip("show");
            }
        }
    }); 
       
});
