﻿HFC.Popover = {
    ContainerId: "evt_popover",
    Model: function () {
        var self = this,
            $Popover = $("#" + HFC.Popover.ContainerId),
            $Prompter = $("#evt_prompter");

        self.Event = ko.observable();
        self.ArrowLeft = ko.observable();
        self.Placement = ko.observable("bottom");
        self.ArrowClassName = ko.computed(function () {
            if (self.Placement() == "bottom" && self.Event()) 
                return self.Event().className() + "-n-arrow";
            else 
                return "";
        });
        self.GoogleAddressHref = function (dest) {
            if (HFC.Regex.IsValidAddress(dest))
                return '//maps.google.com/maps?saddr=' + encodeURIComponent(HFC.FranchiseAddress) + '&daddr=' + encodeURIComponent(dest);
            else
                return "#";
        }
        self.Load = function (evt_model, target, clientX, clientY) {
            self.Event(evt_model);
            
            //target is the datarow (tr), container is the panel (ie, tasks)
            var target_pos = $(target).position(),
                target_height = $(target).height(),
                target_width = $(target).width(),
                //container_width = $(container || target).width(),
                //container_parent_pos = $(container || target).parent().position(),
                left = 0,
                top = 0,
                popover_height = $Popover.height(),
                popover_width = $Popover.width();
            
            if (clientX + popover_width > window.innerWidth + 15) {                
                left = clientX - popover_width + 27;
                self.ArrowLeft('90%');
                if (left < target_width) {
                    left = 35;
                    self.ArrowLeft("10%");
                }
            } else {
                left = clientX - 27;
                self.ArrowLeft('10%');
            }

            top = clientY + window.scrollY;//container_parent_pos.top + target_pos.top + target_height - 5; //7 is the arrow, so the arrow is inside the target and the top of the header touches target_height
            if (top + popover_height + 15 > window.innerHeight + window.scrollY) { //15 pixel padding
                top -= popover_height;
                self.Placement("top");
                if (popover_height > clientY) {
                    top = clientY + 5;
                    self.Placement("bottom");
                }
            } else
                self.Placement("bottom");
            
            $Popover.css({ left: left, top: top }).show();
        }

        self.Close = function () {
            $Popover.hide();
        }

        function _delete(data){
            var url = (self.Event().AptTypeEnum ? "/api/calendar/" : "/api/tasks/") + self.Event().id();
                    
            HFC.AjaxDelete(url, data, function (result) {
                if (HFC.Calendar && $("#" + HFC.Calendar.Id).data("fullCalendar")) {                    
                    $("#" + HFC.Calendar.Id).fullCalendar('refetchEvents');
                }
                if (self.Event().AptTypeEnum && HFC.KO && HFC.KO.AptList)
                    HFC.KO.AptList.fetchData();
                if (self.Event().AptTypeEnum == null && HFC.KO.Tasks && HFC.KO.Tasks)
                    HFC.KO.Tasks.Remove(self.Event());
            });
        }

        self.Delete = function () {
            var hasRecur = self.Event().AptTypeEnum && self.Event().EventType() == EventTypeEnum.Series;

            if (hasRecur) { //prompt for series or occurrence selection
                var cont_btn = $Prompter.find(".btn-primary");
                cont_btn.unbind("click.delete").unbind("click.edit");                         
            
                cont_btn.bind("click.delete", function () {
                    var occur_selection = $Prompter.find("input[name='occurrenceType']:checked").val(),
                        data = null;
                    if(occur_selection == EventTypeEnum.Occurrence)
                        data = {
                            start: self.Event().start().toString("u"), /*Occurrence start date to delete from series*/
                            end: self.Event().end().toString("u")
                        };

                    _delete(data)
                });

                $Prompter.modal("show");
            } else {
                _delete();
            }
            $Popover.hide();

        } //end Delete

        self.Send = function () {

            var data = {
                saveBody: false,
                appointmentId: self.Event().id(),
                emailTemplateTypeId: 8
            };

            var url = "/api/email/0/send";

            HFC.AjaxPost(url, data, function (result) {
                HFC.DisplaySuccess("Email successfully sent");
            });

        } //end

        self.Edit = function () {
            //prompt for single occurrence or series if this is recurring
            var hasRecur = self.Event().AptTypeEnum && self.Event().EventType() == EventTypeEnum.Series;

            if (hasRecur) { //prompt for series or occurrence selection
                var cont_btn = $Prompter.find(".btn-primary"); 
                cont_btn.unbind("click.edit").unbind("click.delete");

                cont_btn.bind("click.edit", function () {
                    var occur_selection = $Prompter.find("input[name='occurrenceType']:checked").val();
                    if (occur_selection == EventTypeEnum.Occurrence) { 
                        HFC.KO.CalModal.Edit(self.Event(), true);
                    } else {
                        var eventArr = $.grep(HFC.KO.CalModal.RecurMasterCollection(), function (evt) {
                            return evt.RecurringEventId == self.Event().RecurringEventId() && evt.EventType == EventTypeEnum.Series && evt.EventRecurring != null;
                        });
                        if (eventArr && eventArr.length)
                            HFC.KO.CalModal.Edit(HFC.Models.Calendar(eventArr[0]));
                        else
                            HFC.DisplayAlert("Sorry, we were unable to locate the series template for editing.");
                    }
                });

                $Prompter.modal("show");
            } else if(HFC.KO && HFC.KO.CalModal){
                HFC.KO.CalModal.Edit(self.Event());
            }            
            $Popover.hide();
        } //end Edit
    }
}