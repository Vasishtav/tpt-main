﻿if (document.getElementById("print_frame") == null) {
    frame = document.createElement("iframe");
    frame.id = "print_frame";
    frame.width = 0;
    frame.height = 0;
    frame.frameBorder = 0;
    $(".wrapper").append(frame);
}

HFC.Print = function(src) {
    var frame = document.getElementById("print_frame");
    $(frame).load(function () {
        frame.contentWindow.print();
    });
    frame.src = src;    
}
