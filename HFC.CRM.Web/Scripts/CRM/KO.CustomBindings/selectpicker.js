﻿ko.bindingHandlers.selectPicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        if ($(element).is('select')) {            
            if (ko.isObservable(valueAccessor())) {
                if ($(element).prop('multiple') && $.isArray(ko.utils.unwrapObservable(valueAccessor()))) {                    
                    // in the case of a multiple select where the valueAccessor() is an observableArray, call the default Knockout selectedOptions binding
                    ko.bindingHandlers.selectedOptions.init(element, valueAccessor, allBindingsAccessor);
                }
                else {                    
                    // regular select and observable so call the default value binding
                    ko.bindingHandlers.value.init(element, valueAccessor, allBindingsAccessor);
                }
            }
            $(element).selectpicker();
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        if ($(element).is('select')) {
            //var selectPickerOptions = allBindingsAccessor().selectPickerOptions;
            //Not using this so uncomment to save some file size
            //if (typeof selectPickerOptions !== 'undefined' && selectPickerOptions !== null) {
            //    var options = selectPickerOptions.optionsArray,
            //        optionsText = selectPickerOptions.optionsText,
            //        optionsValue = selectPickerOptions.optionsValue,
            //        optionsCaption = selectPickerOptions.optionsCaption,
            //        isDisabled = selectPickerOptions.disabledCondition || false,
            //        resetOnDisabled = selectPickerOptions.resetOnDisabled || false;
            //    if (ko.utils.unwrapObservable(options).length > 0) {
            //        // call the default Knockout options binding
            //        ko.bindingHandlers.options.update(element, options, allBindingsAccessor);
            //    }
            //    if (isDisabled && resetOnDisabled) {
            //        // the dropdown is disabled and we need to reset it to its first option
            //        $(element).selectpicker('val', $(element).children('option:first').val());
            //    }
            //    $(element).prop('disabled', isDisabled);
            //}
            if (ko.isObservable(valueAccessor())) {
                if ($(element).prop('multiple') && $.isArray(ko.utils.unwrapObservable(valueAccessor()))) {
                    // in the case of a multiple select where the valueAccessor() is an observableArray, call the default Knockout selectedOptions binding
                    ko.bindingHandlers.selectedOptions.update(element, valueAccessor);
                }
                else {
                    // call the default Knockout value binding
                    ko.bindingHandlers.value.update(element, valueAccessor);
                }
            }

            $(element).selectpicker('refresh');
        }
    }
};
//OBSOLETE BINDING BELOW
//ko.bindingHandlers.selectpicker = {
//    init: function (element, valueAccessor, allBindings, viewModel) {
//        //we use timeout so ko has time to generate all the options before we convert it to select picker
//        setTimeout(function () {
//            $(element).selectpicker({
//                size: 10,
//                showSubtext: true
//            });
//            $(element).selectpicker('val', ko.unwrap(valueAccessor()) || "");
//            //sample usage <select data-bind="selectpicker: LeadStatusId, OnChanged: StatusOnChanged">
//            //register the change so it can update the model
//            ko.utils.registerEventHandler(element, 'change', function () {
//                if (allBindings && allBindings().OnChanged)
//                    allBindings().OnChanged(viewModel, element);
//                else {
//                    var changed = valueAccessor();
//                    changed($(element).val());
//                }
//            });
//        }, 0);

//    }
//    //To Review if necessary: UPDATE IS NOT NEEDED, CHANCES ARE WE DONT SET STATUS PROGRAMMATICALLY TO UPDATE THE UI.  If this is enabled, ko will do two on change trigger
//    //, update: function (element, valueAccessor) {
//    //    if ($(element).data("selectpicker"))
//    //        $(element).selectpicker('val', ko.unwrap(valueAccessor()));
//    //    else
//    //        $(element).val(ko.unwrap(valueAccessor()));
//    //}
//};