﻿ko.bindingHandlers.timepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {

        $(element).timepicker();

        //register the change so it can update the model
        /*ko.utils.registerEventHandler(element, 'change', function () {
            var observable = valueAccessor();
            observable(element.value);
        });*/
    }
    , update: function (element, valueAccessor) {
        element.value = ko.unwrap(valueAccessor());
    }
};