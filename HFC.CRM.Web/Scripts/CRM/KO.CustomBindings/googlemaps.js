﻿ko.bindingHandlers.googlemaps = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        $(element).hoverIntent({
            over: function () {
                var addr = ko.unwrap(valueAccessor());
                var dest = "";
                if (typeof (addr) == "string")
                    dest = addr;
                else if (addr && addr.Address1())
                    dest = addr.Address1() + ", " + addr.City() + " " + addr.State() + " " + addr.ZipCode();
                else if (addr)
                    dest = addr.City() + " " + addr.State() + " " + addr.ZipCode();
                if (HFC.CurrentUserAddress.length > 0 && dest != null && dest.length > 0) {
                    var canvas = $(element).find(".mapCanvas");                    
                    if ($(element).data("maploaded") == null) {
                        var request = {
                            origin: HFC.FranchiseAddress,
                            destination: dest,
                            travelMode: google.maps.TravelMode.DRIVING
                        };
                        var directionsService = new google.maps.DirectionsService(),
                            directionsDisplay = new google.maps.DirectionsRenderer();
                        directionsService.route(request, function (result, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                directionsDisplay.setDirections(result);
                                var mapOptions = {
                                    zoom: 7,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                }
                                directionsDisplay.setMap(new google.maps.Map(canvas.get()[0], mapOptions));
                                $(element).data("maploaded", true);
                            }
                        });
                    }
                    var viewHeight = window.innerHeight,
                        pos = $(element).offset();
                    if (pos.top + canvas.height() - window.scrollY > viewHeight)
                        canvas.css("top", -canvas.height());
                    else
                        canvas.css("top", "");
                    if (pos.left < canvas.width() + 20)
                        canvas.css({ right: "", left: 0 });
                    else
                        canvas.css({ right: 0, left: "" });
                    canvas.show();                    
                }
            },
            out: function () {
                $(element).find(".mapCanvas").hide();
            },
            interval: 300
        });
    }
};