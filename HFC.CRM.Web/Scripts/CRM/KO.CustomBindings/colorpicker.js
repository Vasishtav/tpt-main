﻿ko.bindingHandlers.colorpicker = {
    init: function (element, valueAccessor, allBindings) {
        var option = allBindings.get('cpOptions') || {};
        $.extend(option, {
            onchange: function (container, color) {
                var observable = valueAccessor();
                observable(color.tiny.toRgbString());
            }
        });
        $(element).val(ko.unwrap(valueAccessor())).ColorPickerSliders(option);
    }
    , update: function (element, valueAccessor) {
        element.value = ko.unwrap(valueAccessor());
    }
};

//@ sourceURL=ko.custombinding.colorpicker.js