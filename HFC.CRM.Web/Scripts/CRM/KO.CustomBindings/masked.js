﻿
ko.bindingHandlers.masked = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var $el = $(element),
            mask = allBindingsAccessor().mask || {},
            maskIndex = allBindingsAccessor().maskIndex || 0;
        var maskList = mask.split(';');

        $el.mask(maskList[maskIndex]);
        //we need to prevent enter key from submitting forms, input masked gets bypassed so the values will not save if user hits enter while in a masked input field
        $el.bind("keydown", function (e) {
            var code = e.keyCode || e.which;
            if (code == 13)
                return false;
            else
                return true;
        });
        ko.utils.registerEventHandler(element, 'blur', function () {
            var observable = valueAccessor();
            if (ko.isObservable(observable)) {
                //we have to use timeout because maskedinput will do a final check on blur to update and remove "optional" masks
                //setTimeout(function () { observable(element.value) }, 0);                                
                observable(element.value);
            }
        });
    },
    update: function (element, valueAccessor, allBindings, viewModel) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        
        $(element).val(value);
    }
};