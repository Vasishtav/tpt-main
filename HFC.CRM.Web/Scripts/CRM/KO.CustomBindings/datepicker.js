﻿//we extend this so it can handle date to string and vice versa
//Sample Usage: 
//1. self.StartDate = ko.observable("2012-10-30T14:00:05Z").extend({ date: true }) // or { date: "MM/dd/yyyy HH:mm" } bool or string parameter, bool will use default date format
//2. self.StartDate = ko.observable(new XDate()).extend({ date: true }) //instantiate new XDate or native Date instead of using a string for date
// All dates, regardless if the default value is string, date, or XDate will be converted to XDate object
// KO Binding example:
// <input type="text" data-bind="value: StartDate" /> //this will output from the computed read function, and save using the write
// JS example:
// var dateString = ViewModel.StartDate(), this will return the string formated that is same as the value binding from KO Binding example, ie: "10/30/2012"
// var xdate = ViewModel.StartDate.Value, use this if you want the underlining XDate object instead of the formated string
// var formated = ViewModel.StartDate.Value.toString(format), use this if you want to return some other format instead of the default format, notice no parenthesis
ko.extenders.date = function (target, format) {
    var result = ko.computed({
        read: function () {
            var value = target();

            if (value) {

                if (typeof value !== "string") {
                    var offset = value.getTimezoneOffset();
                    if (offset !== 0) {
                        value = new XDate(value, false);
                    } else {
                        value = new XDate(value, true);
                    }
                } else {
                    value = new XDate(value, true);
                }
            } else
                return "";

            if (value.valid()) {
                if (typeof format !== "string")
                    format = HFC.Browser.isMobile() ? "yyyy-MM-dd" : "MM/dd/yyyy";

                var v = value.toString(format);
                return v;
            } else return "";
        },
        write: function (val) {
    
          
            if (val && (typeof val === "string" || val instanceof Date)) {
                val = new XDate(val, false);
                
                if (!val.valid())
                    val = null;
            }
            if (result)
                result.Value = val;
            target(val);
        }
    }).extend({ notify: 'always' });



    result.Value = new XDate(target());

    return result;
}

ko.bindingHandlers.date = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = valueAccessor();
        var allBindings = allBindingsAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);

        // Date formats: http://momentjs.com/docs/#/displaying/format/
        var pattern = allBindings.format || 'DD/MM/YYYY';
        var isUTC = allBindings.utc || false;

        var output = "-";
        if (valueUnwrapped !== null && valueUnwrapped !== undefined && valueUnwrapped.length > 0) {
            output = isUTC ? moment(valueUnwrapped).utc().format(pattern) : moment(valueUnwrapped).format(pattern);
        }

        if ($(element).is("input") === true) {
            $(element).val(output);
        } else {
            $(element).text(output);
        }
    }
};

ko.bindingHandlers.currency = {
    symbol: ko.observable('$'),
    update: function (element, valueAccessor, allBindingsAccessor) {
        return ko.bindingHandlers.text.update(element, function () {
            var value = +(ko.utils.unwrapObservable(valueAccessor()) || 0),
                symbol = ko.utils.unwrapObservable(allBindingsAccessor().symbol === undefined
                            ? allBindingsAccessor().symbol
                            : ko.bindingHandlers.currency.symbol);
            return symbol + value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
        });
    }
};


ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor, viewmodel) {
        var clearBtn = allBindingsAccessor().clearBtn || false;
        $(element).datepicker({
            autoclose: true,
            clearBtn: clearBtn,
            todayHighlight: true
        });
        if (element.tagName == "INPUT") {
            ko.utils.registerEventHandler(element, "changeDate", function (event) {
                var value = valueAccessor();
                var val = element.value;
                if (element.value != "") {
                    if (moment(val, 'MM/DD/YYYY').isValid()) {
                        var date = moment(val, 'MM/DD/YYYY').toDate();
                        
                        if (date.getTime() === event.date.getTime() && event.date.getFullYear() > 1950) {
                            if (ko.isObservable(value)) {
                                value(event.date);
                            }
                        }
                    }
                }
            });
        }
    }
    , update: function (element, valueAccessor) {
        if (element.tagName == "INPUT") {
            var widget = $(element).data("datepicker");
            //when the view model is updated, update the widget
            if (widget) {
                var value = ko.utils.unwrapObservable(valueAccessor());

                if (!value) {
                    $(element).val("").change();
                    return;
                }

                widget.setDate(typeof (value) === 'string' ? new Date(value) : value);
            }
        }
    }
};

ko.bindingHandlers.utcdatepicker = {
    init: function (element, valueAccessor, allBindingsAccessor, viewmodel) {
        var clearBtn = allBindingsAccessor().clearBtn || false;
        $(element).datepicker({
            autoclose: true,
            clearBtn: clearBtn,
            todayHighlight: true
        });

        if (element.tagName == "INPUT") {
            ko.utils.registerEventHandler(element, "changeDate", function (event) {
                var value = valueAccessor();
                var val = element.value;
                if (element.value != "") {
                    if (moment(val, 'MM/DD/YYYY').isValid()) {
                        var date = moment(val, 'MM/DD/YYYY').toDate();

                        if (date.getTime() === event.date.getTime() && event.date.getFullYear() > 1950) {
                            if (ko.isObservable(value)) {
                                value(event.date);
                            }
                        }
                    }
                }
            });
        }
    }
    , update: function (element, valueAccessor) {
        if (element.tagName == "INPUT") {
            var widget = $(element).data("datepicker");
            //when the view model is updated, update the widget
            if (widget) {
                var value = ko.utils.unwrapObservable(valueAccessor());

                if (!value) {
                    $(element).val("").change();
                    return;
                }

                widget.setDate(typeof (value) === 'string' ? new Date(moment(value).utc().year(), moment(value).utc().month(), moment(value).utc().date()) : value);
            }
        }
    }
};