﻿ko.bindingHandlers.typeahead = {
    init: function (element, valueAccessor, allBindingsAccessor) {

        var emailArr = $.map(HFC.EmployeesJS, function (emp) {
            return emp.Email;
        });
        function extractor(query) {
            var result = /([^,;]+)$/.exec(query);
            if (result && result[1])
                return result[1].trim();
            return '';
        }
        //options below is for multiple values with comma seperator
        $(element).typeahead({
            mode: "multiple"
            , source: emailArr,
            updater: function (item) {
                return this.$element.val().replace(/[^,;]*$/, '') + item;
            },
            matcher: function (item) {
                var tquery = extractor(this.query);
                if (!tquery) return false;
                return ~item.toLowerCase().indexOf(tquery.toLowerCase())
            },
            highlighter: function (item) {

                var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
                return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                    return '<strong>' + match + '</strong>'
                })
            }
        });

        //register the change so it can update the model
        ko.utils.registerEventHandler(element, 'change', function () {
            var observable = valueAccessor();
            observable(element.value);
        });
    }
    , update: function (element, valueAccessor) {
        element.value = ko.unwrap(valueAccessor());
    }
};