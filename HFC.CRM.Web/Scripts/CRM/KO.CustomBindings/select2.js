﻿ko.bindingHandlers.select2 = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var obj = valueAccessor(), allBindings = allBindingsAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(allBindingsAccessor());
        var selectedOptions = valueUnwrapped.selectedOptions;

        setTimeout(function () {
            $(element).select2(obj);
        }, 0);

        selectedOptions.subscribe(function (newValue) {
            
            setTimeout(function () {
                $(element).select2('destroy');
                $(element).select2(obj);
            }, 0);
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).select2('destroy');
        });
    },

    update: function (element) {
        $(element).trigger('change');
    }
};