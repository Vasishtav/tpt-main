﻿
ko.bindingHandlers.notesDropdown = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var $el = $(element);
        
        $el.parent().on({
            "click": function (e) {
                if ($(e.currentTarget).hasClass("btn-group")) {
                    var obj = $(this);
                    obj.data("closable", false);
                    setTimeout(function () { obj.data("closable", true); }, 500);
                } else
                    $(this).data("closable", true);
            },
            "hide.bs.dropdown": function (e) {
                if ($(e.relatedTarget).hasClass("inner-dropdown"))
                    return true;
                else
                    return $(this).data("closable");
            }
        });
    }    
};