﻿var NumbersArray = [];
for (var i = 1; i <= 31; i++) {
    NumbersArray.push(i);
}
HFC.VM.CalendarModal = function () {
    var self = this;

    self.ContainerId = "crm-evt-container";
    var $container = $("#"+ self.ContainerId),
        timeArr = [];    

    self.FullTimeArray = function () {
        if (timeArr.length == 0) {
            var date = new XDate(2013, 0, 1).clearTime();
            while (date.getDate() == 1) {
                timeArr.push(date.toString("hh:mm tt"));
                date.addMinutes(15);
            }
        }
        
        return timeArr;
    };

    self.RecurMasterCollection = ko.observableArray();
    self.Event = ko.observable();
    self.IsBusy = ko.observable(false);
    self.CurrentJob = null;

    self.Cancel = function () {
        self.Event().rollback();
        if (HFC.Calendar)
            $("#" + HFC.Calendar.Id).fullCalendar("unselect");
    };

    self.NewTask = function (job, event) {
        var data = { AptTypeEnum: null };
        if (event) {
            data.start = event.start;
            data.allDay = true;
        }
        else {
            data.start = XDate.getRoundedDate();
        }
        if (job && job.Customer) {
            HFC.QS.EventTitle = 'Job #' + job.JobNumber + ', ' + job.Customer.FullName();
        }
        data.title = HFC.QS.EventTitle;
        data.LeadId = job ? job.LeadId : HFC.QS.LeadId;
        data.LeadNumber = job ? job.LeadNumber : HFC.QS.LeadNumber;
        data.JobId = job ? job.JobId : HFC.QS.JobId;
        data.JobNumber = job ? job.JobNumber : HFC.QS.JobNumber;
        data.PhoneNumber = job ? job.PhoneNumber : HFC.QS.PhoneNumber;
        self.CurrentJob = job;

        self.Edit(HFC.Models.Task(data));
    };
  
    self.NewEvent = function (job, event) {
        var data = { AptTypeEnum: HFC.QS.AptTypeEnum || 1 };
        data.start = event ? event.start : XDate.getRoundedDate();
        data.end = event ? event.end : XDate.getRoundedDate().addHours(1);
        data.allDay = false;
        if (job && job.Customer) {
            HFC.QS.EventTitle = 'Job #' + job.JobNumber + ', ' + job.Customer.FullName();
            HFC.QS.Location = job.InstallAddress.ToSingleLineString();
        }
        data.title = HFC.QS.EventTitle;
        data.Location = HFC.QS.Location;
        data.LeadId = job ? job.LeadId : HFC.QS.LeadId;
        data.LeadNumber = job ? job.LeadNumber : HFC.QS.LeadNumber;
        data.JobId = job ? job.JobId : HFC.QS.JobId;
        data.JobNumber = job ? job.JobNumber : HFC.QS.JobNumber;
        data.PhoneNumber = job ? job.PhoneNumber : HFC.QS.PhoneNumber;
        self.CurrentJob = job;
        self.Edit(HFC.Models.Calendar(data));
    }

    self.Edit = function (koModelToEdit, isNewOccurrenceEdit) {
        if (koModelToEdit == null) {
            HFC.DisplayAlert("Invalid model for editing");
        } else {
            //if (koModelToEdit && !(ko.isObservable(koModelToEdit.title))) {                
            //    if (koModelToEdit.AptTypeEnum)
            //        koModelToEdit = HFC.Models.Calendar(koModelToEdit);
            //    else
            //        koModelToEdit = HFC.Models.Task(koModelToEdit);
            //}
            self.Event(koModelToEdit);             
            ko.editable(self.Event());            
            self.Event().beginEdit();
            if (isNewOccurrenceEdit) {
                self.Event().id(0); //this will create a new occurrence
                self.Event().EventType(EventTypeEnum.Occurrence);
            }
            $container.modal("show");
            setTimeout(function () { $("#subjectIpt").focus(); }, 300)
            
            if (self.Event().RepeatIsDisabled && self.Event().RepeatIsDisabled())
                $("#evt_recurring_panel").find("input,select").prop("disabled", true);

            //scroll to the default time for the time drop down
            $(".time-dropdown").parent().on('shown.bs.dropdown', function () {
                var list = $(this).find("ul").get(0);
                var pos = $(this).find("li.active").position();
                list.scrollTop = list.scrollTop + pos.top - 10;
            });
        }
    }

    self.Save = function (){
        self.BaseSave(false);
    }

    self.SaveGoToJob = function () {
        self.BaseSave(true);
    }

    self.ApptCancelled = function () {
   
        self.IsBusy(true);
        var url = '/api/calendar/' + self.Event().id() + '/ApptCancelled';
        HFC.Ajax('POST', url, null, function (response) {
            self.IsBusy(false);
            if (!response.warning) {
                $container.modal("hide");
                HFC.DisplaySuccess("Event Cancelled");
                if (HFC.Calendar && $("#" + HFC.Calendar.Id).data("fullCalendar")) {
                    $("#" + HFC.Calendar.Id).fullCalendar('refetchEvents');
                }
                if (self.Event().AptTypeEnum && HFC.KO && HFC.KO.AptList)
                    HFC.KO.AptList.fetchData();
            }
        }, function (success) {
            $container.modal("hide");
            self.IsBusy(false);
        }, "json"
        );
    };

    function goToJob () {
        if (HFC.QS.JobId && HFC.QS.JobId != 0) {
            window.location.href = HFC.Util.BaseURL() + '#/leads/' + HFC.QS.LeadId + '/' + HFC.QS.JobId;
        }
    }

    self.BaseSave = function (navToJob) {
        var CalidEdit = self.Event().id();
        
        if (!self.Event().title() || self.Event().title().length == 0) {
            HFC.DisplayAlert("A subject is required");
            return false;
        }
        var CalEndDate = self.Event().enddate();
        var CalStartDate = self.Event().startdate();
        
        if (CalidEdit == 0) {
            var currentDate = new Date();
           

            if (CalStartDate != null && CalStartDate != "") {
                var Startmonth = CalStartDate.substring(0, 2);
                var Startdate = CalStartDate.substring(3, 5);
                var Startyear = CalStartDate.substring(6, 10);
                var StartdateToCompare = new Date(Startyear, Startmonth - 1, Startdate);
                if (StartdateToCompare < currentDate.setHours(0, 0, 0, 0)) {
                    HFC.DisplayAlert("Start Date Entered is lesser than Current Date");
                    return false;
                }
            }
        }
        var repeatEndsOn = self.Event().EventRecurring.EndsOn();

        if (CalEndDate != null && CalEndDate != "" && repeatEndsOn != null && repeatEndsOn != "") {
            if (new Date(repeatEndsOn) < new Date(CalEndDate)) {
                HFC.DisplayAlert("Event/Task to date cannot be after Repeat Ends On Date.");
                return false;
            }
        }
        var isRepeat = self.Event().RepeatIsChecked();
        if (isRepeat != null && isRepeat) {
            var one_day = 1000 * 60 * 60 * 24;   
            var start = self.Event().startdate();
            var end = self.Event().enddate();   
            var difference_ms = new Date(end).getTime() - new Date(start).getTime();     
            var days = Math.round(difference_ms / one_day);
            if (days > 1) {
                HFC.DisplayAlert("You cannot select repeat for multi day events. Please turn off repeat or change to same day event.");
                return false;
            }
        }

        if (CalEndDate != null && CalEndDate != "") {                
                var Endmonth = CalEndDate.substring(0, 2);
                var Enddate = CalEndDate.substring(3, 5);
                var Endyear = CalEndDate.substring(6, 10);
                var EnddateToCompare = new Date(Endyear, Endmonth - 1, Enddate);
                if (EnddateToCompare < currentDate.setHours(0, 0, 0, 0)) {
                    HFC.DisplayAlert("End Date Entered is lesser than Current Date");
                    return false;
                }
                else if (EnddateToCompare < StartdateToCompare) {
                    HFC.DisplayAlert("End Date Entered is lesser than Start Date");
                    return false;
                }
            }
            if ($('#evt_recurring_panel').css('display') == 'block') {

                var CalStartsOnDate = $("#StartsOnTest").val();
                if (CalStartsOnDate != null) {
                    var StartsOnmonth = CalStartsOnDate.substring(0, 2);
                    var StartsOndate = CalStartsOnDate.substring(3, 5);
                    var StartsOnyear = CalStartsOnDate.substring(6, 10);
                    var StartsOndateToCompare = new Date(StartsOnyear, StartsOnmonth - 1, StartsOndate);

                    if (StartsOndateToCompare < StartdateToCompare) {

                        HFC.DisplayAlert("Repeat Start Date Entered is lesser than Start Date");
                        return false;
                    }

                    else if (StartsOndateToCompare > EnddateToCompare) {

                        HFC.DisplayAlert("Repeat Start Date Entered is Greater than End Date");
                        return false;
                    }
                }

                var CalEndsOnDate = $("#EndsOnTest").val()
                if (CalEndsOnDate != null && CalEndsOnDate != "") {
                    //alert(CalEndsOnDate);
                    var EndsOnmonth = CalEndsOnDate.substring(0, 2);
                    var EndsOndate = CalEndsOnDate.substring(3, 5);
                    var EndsOnyear = CalEndsOnDate.substring(6, 10);
                    var EndsOndateToCompare = new Date(EndsOnyear, EndsOnmonth - 1, EndsOndate);
                    if (EndsOndateToCompare < StartdateToCompare) {

                        HFC.DisplayAlert("Repeat End Date Entered is lesser than Start Date");
                        return false;
                    }
                    else if (EndsOndateToCompare > EnddateToCompare) {

                        HFC.DisplayAlert("Repeat End Date Entered is Greater than End Date");
                        return false;
                    }

                    else if (EndsOndateToCompare < StartsOndateToCompare) {

                        HFC.DisplayAlert("Repeat End Date Entered is lesser than Repeat Start Date");
                        return false;
                    }
                }

            }
        
        self.IsBusy(true);


        var url = '/api/tasks/', method = "POST"; //POST for update
        if (self.Event().AptTypeEnum != null)
            url = '/api/calendar/';

        if (self.Event().id()) {
            url += self.Event().id();
            method = "PUT"; //PUT for update
        }

        var data = self.Event().toJS();

        var goToJob = function () {
            if (HFC.QS.JobId && HFC.QS.JobId != 0 && navToJob) {
                window.location.href = HFC.Util.BaseURL() + '#/leads/' + HFC.QS.LeadId + '/' + HFC.QS.JobId;
            }
        }

        if (data.isAllExists == false) {
            $container.modal("hide");
            bootbox.dialog({
                closeButton: false,
                message: HFC.AttendeeWarning,
                title: "Warning",
                buttons: {
                    main: {
                        label: "Ok",
                        className: "btn-default",
                        callback: function () {
                            self.SaveSendToServer(method, url, data, goToJob);
                        }
                    }
                }
            });
        }
        else {
            $container.modal("hide");
            self.SaveSendToServer(method, url, data, goToJob);
        }
    };

    self.SaveSendToServer = function (method, url, data, goToJob) {
        HFC.Ajax(method, url, data,
    function (response) {
        if (response.warning) {
            HFC.DisplayWarning(response.warning);
        }
        if (self.Event().AptTypeEnum != null) {
            if (self.CurrentJob && self.CurrentJob.Calendar)
                self.CurrentJob.Calendar.fetchData();
            if (HFC.KO && HFC.KO.AptList)
                HFC.KO.AptList.fetchData();
            if (HFC.Calendar && $("#" + HFC.Calendar.Id).data("fullCalendar")) {
                $("#" + HFC.Calendar.Id).fullCalendar('unselect');
                $("#" + HFC.Calendar.Id).fullCalendar('refetchEvents');
            }
            if (!response.warning) {
                HFC.DisplaySuccess("Event Saved");
            }

        } else {
            if (self.CurrentJob && self.CurrentJob.Tasks)
                self.CurrentJob.Tasks.fetchData();
            if (HFC.KO && HFC.KO.Tasks)
                HFC.KO.Tasks.fetchData();
            if (HFC.Calendar && $("#" + HFC.Calendar.Id).data("fullCalendar")) {
                $("#" + HFC.Calendar.Id).fullCalendar('unselect');
                $("#" + HFC.Calendar.Id).fullCalendar('refetchEvents');
            }
            if (!response.warning) {
                HFC.DisplaySuccess("Task Saved");
            }
        }

        //$container.modal("hide");
    }
    , function (success) { //on complete
        self.IsBusy(false);
        goToJob();
    }, "json"
);
    }
}