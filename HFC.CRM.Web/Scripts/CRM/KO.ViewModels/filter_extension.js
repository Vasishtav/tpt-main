﻿HFC.Ext = {
    Filter: function (self, dateRange, searchTerm, searchFilter, isReportFilter, start, end) {
        if (!self)
            return self;
        self.decodeHtmlEntity = function (str) {
            if (str) {
                return str.replace(/&#(\d+);/g, function (match, dec) {
                    return String.fromCharCode(dec);
                });
            }
            return null;
        };
        self.SearchTerm = ko.observable(self.decodeHtmlEntity(searchTerm));
        self.SearchFilter = ko.observable(searchFilter || "Auto_Detect");
        self.PageIndex = ko.observable(0);
        self.PageSize = ko.observable(HFC.Browser.isMobile() ? 10 : 100);
        self.TotalRecords = ko.observable(0);
        self.PageTotal = ko.computed(function () {
            return self.TotalRecords() ? Math.ceil(self.TotalRecords() / self.PageSize()) : 1;
        });
        self.PageNumber = ko.computed({
            read: function () {
                return self.PageIndex() + 1;
            },
            write: function (num) {
                var numInt = parseInt(num);
                if (numInt > 0) {
                    self.PageIndex(num - 1);
                    self._applyFilter();
                }
            }
        });
        self.SortBy = ko.observable("createdonutc");
        self.SortIsDesc = ko.observable(true);
        self.SortDirectionIcon = ko.computed(function () {
            return self.SortIsDesc() ? 'glyphicon-arrow-down' : 'glyphicon-arrow-up';
        });
        self.ChangeSortOrder = function (sortBy) {
            if (sortBy) {
                if (sortBy != self.SortBy()) {
                    self.SortBy(sortBy);
                    self.SortIsDesc(true);
                } else {
                    self.SortIsDesc(!self.SortIsDesc());
                }
                self.PageIndex(0);
                self._applyFilter();
            }
        }

        self.PageSummary = ko.computed(function () {
            var summary = "";
            if (self.TotalRecords()) {
                var end = self.PageSize() * (self.PageIndex() + 1);
                if (end > self.TotalRecords())
                    end = self.TotalRecords();
                if (self.PageTotal() > 1)
                    summary = ((self.PageIndex() * self.PageSize()) + 1) + " - " + end + " out of " + self.TotalRecords();
                else
                    summary = self.TotalRecords() + " total records";
            }
            return summary;
        });
        self.NextPage = function () {
            if (self.PageNumber() < self.PageTotal()) {
                self.PageIndex(self.PageIndex() + 1);
                self._applyFilter();
            }
        }
        self.PrevPage = function () {
            if (self.PageNumber() > 1) {
                self.PageIndex(self.PageIndex() - 1);
                self._applyFilter();
            }
        }
        self.LastPage = function () {
            if (self.PageTotal() > 1) {
                self.PageIndex(self.PageTotal() - 1);
                self._applyFilter();
            }
        }
        self.FirstPage = function () {
            if (self.PageIndex() > 0) {
                self.PageIndex(0);
                self._applyFilter();
            }
        }
        //self.PageSize.subscribe(_applyFilter);
        // end paging methods

        self.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
        if (isReportFilter)
            self.DateRangeArray = ["All Date & Time", "This Month", "Last Month", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half", "This Year"];
        if (dateRange && $.inArray(dateRange, self.DateRangeArray) < 0)
            dateRange = ""; //make sure it is valid date range

        self.SelectedDate = ko.observable(dateRange || (start || end ? null : "All Date & Time"));
        var dateranges = getDateRange(self.SelectedDate(), start, end);

        self.StartDate = ko.observable(dateranges ? dateranges.StartDate : null).extend({ date: true });
        self.EndDate = ko.observable(dateranges ? dateranges.EndDate : null).extend({ date: true });

        self.SelectedDateText = ko.computed({
            read: function () {
                if (self.SelectedDate())
                    return self.SelectedDate();
                else if (self.StartDate() && self.EndDate()) {
                    var rangeText = "",
                        minDiff = self.StartDate.Value.diffMinutes(self.EndDate.Value),
                        start = "",
                        end = "";

                    if (minDiff <= 1440) {//both are happening within same day
                        rangeText = self.StartDate.Value.toFriendlyString();
                    } else {
                        start = self.StartDate.Value.toFriendlyString();
                        end = self.EndDate.Value.toFriendlyString();
                        if (start == end)
                            rangeText = start;
                        else
                            rangeText = start + " to " + end;
                    }
                    return rangeText;
                } else if (self.StartDate()) {
                    var date = self.StartDate.Value.toFriendlyString();
                    if (date != "Today")
                        date += " to Today";
                    return date;
                } else
                    return "Please pick a date";
            },
            write: function (val) {
                if (val != self.SelectedDate()) {
                    self.SelectedDate(val);
                    if (val != null) {
                        var dateranges = getDateRange(val);
                        self.StartDate(dateranges.StartDate);
                        self.EndDate(dateranges.EndDate);
                    }
                }
            }
        });

        function getDateRange(range, start, end) {
            var endDate = new XDate()
            startDate = new XDate();
            if (start) {
                startDate = new XDate(start);
            }
            if (end) {
                endDate = new XDate(end);
            }
            if (!range) {
                return { StartDate: startDate, EndDate: endDate };
            }
            switch (range.toLowerCase()) {
                case "this week":
                    startDate.addDays(-startDate.getDay());
                    endDate = startDate.clone().addDays(6);
                    break;
                case "this month":
                    startDate.setDate(1);
                    endDate = startDate.clone().addMonths(1).addDays(-1);
                    break;
                case "this year":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addYears(1).addDays(-1);
                    break;
                case "last week":
                    startDate.addDays(-startDate.getDay() - 7);
                    endDate = startDate.clone().addDays(6);
                    break;
                case "last month":
                    startDate.setDate(1).addMonths(-1);
                    endDate = startDate.clone().addMonths(1).addDays(-1);
                    break;
                case "last year":
                    startDate.setDate(1).setMonth(0).addYears(-1);
                    endDate.setDate(1).setMonth(0).addDays(-1);
                    break;
                case "last 7 days":
                    startDate.addDays(-7); break;
                case "last 30 days":
                    startDate.addDays(-30); break;
                case "1st quarter":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "2nd quarter":
                    startDate.setDate(1).setMonth(3);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "3rd quarter":
                    startDate.setDate(1).setMonth(6);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "4th quarter":
                    startDate.setDate(1).setMonth(9);
                    endDate = startDate.clone().addMonths(3).addDays(-1); break;
                case "1st half":
                    startDate.setDate(1).setMonth(0);
                    endDate = startDate.clone().addMonths(6).addDays(-1); break;
                case "2nd half":
                    startDate.setDate(1).setMonth(6);
                    endDate = startDate.clone().addMonths(6).addDays(-1); break;
                default:
                    startDate = null; endDate = null;
                    break;
            }
            return { StartDate: startDate, EndDate: endDate };
        }


        return self;
    }
};