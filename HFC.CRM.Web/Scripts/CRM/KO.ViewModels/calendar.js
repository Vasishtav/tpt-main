﻿HFC.VM.Calendar = function (collection, jobId) {
    var self = this,
        today = new Date();

    self.JobId = jobId;
    self.Collection = ko.observableArray(collection ? $.map(collection, function (c) { return HFC.Models.Calendar(c); }) : null);

    self.StartDate = ko.observable(new XDate().addDays(-today.getDay()));
    self.StartToday = ko.observable(new XDate());
    self.EndDate = ko.observable(self.StartDate().clone().addDays(6));

    self.IsBusy = ko.observable();
    function _fetchCharts() {
        if (HFC.Charts && HFC.Charts.KO) {
            HFC.Charts.KO.fetchCharts();
        }
    }

    self.PrevWeek = function () {
        if (!self.IsBusy()) {
            self.EndDate(self.EndDate().addDays(-7)); //can't call self.EndDate().addDays() //this will not trigger knockout
            self.StartDate(self.StartDate().addDays(-7));
            self.fetchData();
        }
    };
    self.NextWeek = function () {
        if (!self.IsBusy()) {
            self.StartDate(self.StartDate().addDays(7));
            self.EndDate(self.EndDate().addDays(7));
            self.fetchData();
        }
    };
    self.CurrentWeek = function () {
        if (!self.IsBusy()) {
            self.StartDate(new XDate().addDays(-today.getDay()));
            self.EndDate(self.StartDate().clone().addDays(6));
            self.fetchData();
        }
    };

    self.TotalRecords = ko.computed(function () {
        return self.Collection().length ? self.Collection().length : null;
    });
    self.DateRange = ko.computed(function () {
        var range = "";
        if (self.StartDate().getMonth() === self.EndDate().getMonth())
            range = self.StartDate().toString("MMM d") + " - " + self.EndDate().getDate();
        else
            range = self.StartDate().toString("MMM d") + " - " + self.EndDate().toString("MMM d");
        return range;
    });
    self.QuickSummary = ko.computed(function () {
        var text = "";
        if (self.Collection().length) {
            var apt = $.map(self.Collection(), function (a) {
                return a.AptTypeEnum() == AppointmentTypeEnum.Appointment ? true : null;
            });
            var install = $.map(self.Collection(), function (a) {
                return a.AptTypeEnum() == AppointmentTypeEnum.Installation ? true : null;
            });
            if (apt.length)
                text = apt.length + " appointments";
            if (install.length)
                text += (text.length ? " & " : "") + install.length + " installation";
            var other = self.Collection().length - install.length - apt.length;
            if (other > 0)
                text += (text.length ? " & " : "") + other + " others";
        } else
            text = "&nbsp;";

        return text;
    });

    self.LoadPopover = function (model, jsevent) {
        HFC.Popover.KO.Load(model, jsevent.currentTarget, jsevent.clientX, jsevent.clientY);
    }

    self.fetchData = function () {
        if (!self.IsBusy()) {
            self.IsBusy(true);
            var ids = [HFC.CurrentUser.PersonId],
                data = {
                    start: (self.StartDate().getMonth() + 1) + '/' + self.StartDate().getDate() + '/' + self.StartDate().getFullYear(),
                    end: (self.EndDate().getMonth() + 1) + '/' + self.EndDate().getDate() + '/' + self.EndDate().getFullYear()
                };
            if (HFC.KO && HFC.KO.Users)
                ids = HFC.KO.Users.SelectedUserIds();
            if (self.JobId > 0)
                data.JobIds = self.JobId;
            else {
                if (ids && ids.length)
                    data.personIds = ids;
                else {
                    self.Collection([]);
                    return;
                }
            }
            HFC.AjaxGetJson('/api/calendar', data,
                function (result) {
                    if (result != null) {
                        //save the collection to collection so we can retrieve master series for changes if we need to                            
                        if (HFC.KO && HFC.KO.CalModal)
                            HFC.KO.CalModal.RecurMasterCollection(result.RecurringMaster);

                        //only render non-series event, since the master series is only for reference if user decides to edit series
                        var apts = $.map(result.Events, function (a) {
                            return HFC.Models.Calendar(a);
                        });
                        self.Collection(apts);
                    } else {
                        self.Collection([]);
                        if (HFC.KO && HFC.KO.CalModal)
                            HFC.KO.CalModal.RecurMasterCollection([]);
                    }
                }
            ).always(function () { self.IsBusy(false); });

            _fetchCharts();
        }
    }

    //set up hide popover on scroll in panel-body
    $(".aptlist-panel .panel-body").on("scroll", function () {
        $("#" + HFC.Popover.ContainerId).hide();
    });

    function resize() {
        if (window.innerWidth >= 768 && $(".chart-container").length == 0) {
            if (window.AptListTimeout) clearTimeout(window.AptListTimeout);
            window.AptListTimeout = setTimeout(function () {
                $(".tasks-panel .panel-body, .aptlist-panel .panel-body").height(window.innerHeight - 260)
            }, 200);
        }
    }

    //bind to resize so we can refresh the chart and let google resize the chart if user resizes window
    $(window).unbind('resize.aptlist');
    $(window).bind('resize.aptlist', resize);
    window.addEventListener("orientationchange", resize);
    resize();

}