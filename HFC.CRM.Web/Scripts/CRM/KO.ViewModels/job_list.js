﻿/*
* This will handle the interaction on the jobs list, such as changing status
* Sample usage: viewModel.Leads = new HFC.VM.JobList();  or new HFC.VM.JobList(rawCollection);
*/
HFC.VM.JobList = function (dateRange, job_statusid, sales_personid, collection) {
    var self = this;

    self.IsBusy = ko.observable();

    self.Collection = ko.observableArray(collection ? $.map(collection, function (l) {
        return HFC.Models.Job(l);
    }) : null);        
    
    self.ToggleJobItems = function (model, jsEvent) {
       
        if (!model.ItemRowEnabled())
            model.ItemRowEnabled(true); //one time enable so it doesnt have to re-render every time you toggle
        $(jsEvent.currentTarget).find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
        $(".row_" + model.JobId).toggleClass("active");
        $(".row_item_" + model.JobId).toggle();
    }

    self.decodeHtmlEntity = function (str) {
        if (str) {
            return str.replace(/&#(\d+);/g, function (match, dec) {
                return String.fromCharCode(dec);
            });
        }
        return null;
    };

    self.RestoreFilters = function () {
        var data = History.getState();
        if (data) {
            data = data.data;
            return data.salesPersonIds;
        }
     };

    self._applyFilter = function (searchTerm, searchFilter) {
        ;
        if (self.SelectedDate() == null && (self.StartDate.Value == null || !self.StartDate.Value.valid()))
            HFC.DisplayAlert("Please pick a date to filter");
        else if (!self.IsBusy()) {
            self.IsBusy(true);
            var data = { orderByDirection: self.SortIsDesc() ? "Desc" : "Asc", orderBy: self.SortBy() };
            if (self.SalesPersonIds() && self.SalesPersonIds().length) {
                $.each(self.SalesPersonIds(), function (i, s) {
                    data['salesPersonIds[' + i + ']'] = s;
                });
            }
            if (self.JobStatusIds() && self.JobStatusIds().length) {
                $.each(self.JobStatusIds(), function (i, s) {
                    data['jobStatusIds[' + i + ']'] = s;
                });
            }
            
            if (self.StartDate())
                data.createdOnUtcStart = moment(self.StartDate.Value[0]).format('YYYY-MM-DD')
            if (self.EndDate())
                data.createdOnUtcEnd = moment(self.EndDate.Value[0]).format('YYYY-MM-DD')
            
            data.searchTerm = self.decodeHtmlEntity(searchTerm);

            data.searchFilter = searchFilter;

            data.pageIndex = self.PageIndex();
            data.pageSize = self.PageSize();
            
            if (self.SalesPersonIds().length != 0 || self.JobStatusIds().length != 0 ||
                        data.createdOnUtcStart || data.createdOnUtcEnd ||
                        data.searchTerm || data.searchFilter || self.SelectedDate()) {
                var myParams = {
                    salesPersonIds: self.SalesPersonIds(), jobStatusIds: self.JobStatusIds(), createdOnUtcStart: data.createdOnUtcStart,
                    createdOnUtcEnd: data.createdOnUtcEnd, searchTerm: data.searchTerm,
                    searchFilter: data.searchFilter, selectedDate: self.SelectedDate()
                };
                var pr = jQuery.param(myParams, false);
                History.replaceState(myParams, "JobSearch", "?" + pr);
            }
            else {
                History.replaceState(null, "JobSearch", "/jobs/");
            }

            //'/api/search/0/getJobs'
            //HFC.AjaxGetJson('/api/search/0/getJobs', data,
            //    function (result) {
            //        self.Collection([]);
            //        if (result) {
            //            self.TotalRecords(result.TotalRecords);
            //            if (result.TotalRecords > 0) {
            //                if (result.JobCollection) {
            //                    var jobs = $.map(result.JobCollection, function (item) {
            //                        return HFC.Models.Job(item);
            //                    });
            //                    self.Collection(jobs);
            //                }
            //            }
            //        } else
            //            self.TotalRecords(0);

            //        //this will allow dropdowns to be dropup if it is too far down at bottom that the menu would require scrolling
            //        $("#crm-job-table .dropdown-toggle").on("click", function(e) {
            //            if (e.clientY > window.innerHeight / 2)
            //                $(this).parent().addClass("dropup");
            //            else
            //                $(this).parent().removeClass("dropup");
            //        });
            //    }
            //).complete(function() { self.IsBusy(false); });
        }
    }

   // self = HFC.Ext.Filter(self, dateRange);

    self.GetURLParameter = function (sParam) {
        var sPageURL = window.location.search.substring(1);
        var data = $.parseParams(window.location.href);
        if (data) {
            return data[sParam];
        }
    };

    HFC.Ext.Filter(self, self.GetURLParameter("selectedDate"), null, null, false, self.GetURLParameter("createdOnUtcStart"), self.GetURLParameter("createdOnUtcEnd"));

    self.SalesPersonIds = ko.observableArray(sales_personid ? [sales_personid.toString()] : self.GetURLParameter("salesPersonIds"));
    self.JobStatusIds = ko.observableArray(job_statusid ? [job_statusid.toString()] : self.GetURLParameter("jobStatusIds"));
        
    self.ExportExcel = function () {
        if (self.SelectedDate() == null && (self.StartDate.Value == null || !self.StartDate.Value.valid()))
            HFC.DisplayAlert("Please pick a date to filter");
        else {
            var data = {};
            if (self.SalesPersonIds().length)
                data.salesPersonIds = self.SalesPersonIds();
            if (self.JobStatusIds())
                data.jobStatusIds = self.JobStatusIds();            
            if (self.StartDate())
                data.createdOnUtcStart = self.StartDate();
            if (self.EndDate())
                data.createdOnUtcEnd = self.EndDate();
            window.open('/export/jobs?' + $.param(data), '_blank');
        }
    }

    self.ApplyFilter = function () {
        var searchTerm = $('#search-box').val();
        var searchFilter = $("[name='searchFilter']:checked").val();

        self.PageIndex(0);
        self._applyFilter(searchTerm || '', searchFilter);
    };
}