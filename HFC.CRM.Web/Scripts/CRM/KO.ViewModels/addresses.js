﻿HFC.VM.Addresses = function (addrArray, leadId) {
    var self = this;

    self.LeadId = leadId;

    self.Collection = ko.observableArray(addrArray ? $.map(addrArray, function (addr) {        
        return HFC.Models.Address(addr);
    }) : null);

    self.Get = function (addrId) {
        var addr = $.grep(self.Collection(), function (a) {
            return a.AddressId == addrId;
        });
        if (addr && addr.length)
            return addr[0];
        else
            return null;
    }

    self.Reset = function (addr) {
        addr.rollback();
        if (!addr.AddressId)
            self.Collection.remove(addr);
        HFC.KO.EditModal.IsBusy(false);
        HFC.KO.EditModal.Hide();
    }

    //addr can be the address object or a form object
    self.Save = function (addr) {
        addr = addr.tagName ? this : addr;
        HFC.KO.EditModal.IsBusy(true);

        var data = addr.toJS(),
            method = 'POST',
            url = '/api/leads/' + self.LeadId + '/address';
        if (addr.AddressId)
            method = 'PUT';
        HFC.Ajax(method, url, data, function (result) {
            if (result && result.AddressId && addr.AddressId != result.AddressId) {
                addr.AddressId = result.AddressId;
            }
            HFC.DisplaySuccess("Address saved");
            addr.Template('address-view');
            addr.commit();
            //if successful then close modal
            HFC.KO.EditModal.Hide();
        }).complete(function () {
            HFC.KO.EditModal.IsBusy(false);
        });
    }

    self.Edit = function (model) {
        ko.editable(model);
        model.beginEdit();
        model.Template("address-edit");
        
        if (HFC.KO && HFC.KO.EditModal) {
            HFC.KO.EditModal.Title("Edit Address");
            HFC.KO.EditModal.EditableModel(model);
            HFC.KO.EditModal.Save = self.Save;
            HFC.KO.EditModal.Reset = self.Reset;
            HFC.KO.EditModal.Show();
        }
    }

    self.Add = function () {
        var addr = HFC.Models.Address();
        self.Collection.push(addr);
        self.Edit(addr);
    }

    self.Delete = function (model) {
        if (model.AddressId && self.LeadId && confirm("Do you wish to continue?")) {
            HFC.AjaxDelete('/api/leads/' + self.LeadId + '/address', { AddressId: model.AddressId }, function () {
                self.Collection.remove(model);
                HFC.DisplaySuccess("Address: " + model.ToSingleLineString() + " deleted");
            });
        }
    }
}