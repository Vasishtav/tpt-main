﻿HFC.VM.MLS = function () {
    var self = this,
        dateRange = "1st Half";

    HFC.Ext.Filter(self, dateRange, null, null, true);

    self.FilteredData = null;
    self.TotalRow = null;
    self.OrderIndex = null;
    self.MlsSummaryData = null;
    self.MlsSummaryData_vendor = null;
    self.MlsSummaryData_tax = null;
    self.MlsSummaryData_surcharge = null;
    self.OrderDesc = false;
    self.TerritoryId = ko.observable();
    self.TerritoryText = ko.computed(function () {
        return $("#territoryDDL option:selected").text();
    });
    self.Sort = function () {
        var index = parseInt($(this).data("index"));
        if (self.OrderIndex == index)
            self.OrderDesc = !self.OrderDesc;
        else {
            self.OrderIndex = index;
            self.OrderDesc = false;
        }
        self.FilteredData.sort([{ column: self.OrderIndex, desc: self.OrderDesc }]);
        drawTable();

        return false;
    }
    self.fetch = function () {

        HFC.AjaxGetJson('/api/charts/0/Mls', { territoryId: self.TerritoryId(), startDate: self.StartDate(), endDate: self.EndDate() }, function (result) {
            self.TotalRow = result.TotalRow;
            self.FilteredData = new google.visualization.DataTable(result.DataTable);
            drawTable();
        });
    }


    self.fetchMlsSummary = function () {
        HFC.AjaxGetJson('/api/charts/0/MLSViewSummary', { territoryId: self.TerritoryId(), startDate: self.StartDate(), endDate: self.EndDate() }, function (result) {
            createSurchargeTable(result.AllSummaryData.MLSSurchargeSummaryItems);
            createTaxTable(result.AllSummaryData.MLSTaxSummaryItems);
            createVendorTable(result.AllSummaryData.MLSVendorSummaryItems);
            createProductTypeTable(result.AllSummaryData.MLSTypeSummaryItems);
        });
    }

    function currencyFormat(num) {
        return "$" + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    function createTaxTable(result) {
        if (result != null && result.length >= 0) {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Tax Name/Rate');
            data.addColumn('number', 'Tax Total');
            $.each(result, function (index, value) {
                var items = [];
                items.push(value.ItemName);
                items.push(value.Retail);
                data.addRow(items);
            });

            self.MlsSummaryData_tax = data;
        }
        drawMlsSummaryTable_Tax();
    }
    function createSurchargeTable(result) {
        if (result != null && result.length >= 0) {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Additional Charges');
            data.addColumn('number', 'Sale Price');
            $.each(result, function (index, value) {
                var items = [];
                items.push(value.ItemName);
                items.push(value.Retail);
                data.addRow(items);
            });

            self.MlsSummaryData_surcharge = data;
        }
        drawMlsSummaryTable_Surcharge();
    }

    function createVendorTable(result) {
        var isBB = window.location.host.indexOf("budgetblinds.com") != -1? true: false;       
        var totalHeader = (isBB) ? 'Totals By Vendor' : 'Totals By Category';

        if (result != null && result.length >= 0) {
            var data = new google.visualization.DataTable();
            data.addColumn('string', totalHeader);
            data.addColumn('number', 'Cost');
            data.addColumn('number', 'Sale Price');
            data.addColumn('number', 'GP%');
            $.each(result, function (index, value) {
                var items = [];
                items.push(value.ItemName);
                items.push(value.Cost);
                items.push(value.Retail);
                items.push(value.GP);
                data.addRow(items);
            });

            self.MlsSummaryData_vendor = data;
        }
        drawMlsSummaryTable_Vendor();
    }

    function createProductTypeTable(result) {
        if (result != null && result.length >= 0) {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Totals By Product Type');
            data.addColumn('number', 'Cost');
            data.addColumn('number', 'Sale Price');
            data.addColumn('number', 'GP%');
            $.each(result, function (index, value) {
                var items = [];
                items.push(value.ItemName);
                items.push(value.Cost);
                items.push(value.Retail);
                items.push(value.GP);
                data.addRow(items);
            });
            self.MlsSummaryData = data;
        }
        drawMlsSummaryTable_Type();
    }

    function drawTable() {

        $("#mlsTable").html('');

        $("#mls_vendor_table").html('');
        $("#mls_type_table").html('');
        $("#mls_tax_table").html('');
        $("#mls_surcharge_table").html('');


        var thead = $("<thead>"),
            tbody = $("<tbody>"),
            tfoot = $("<tfoot>"),
            theadRow = $("<tr>"),
            tfootRow = $("<tr>"),
            maxColCount = self.FilteredData.getNumberOfColumns(),
            maxRowCount = self.FilteredData.getNumberOfRows();

        for (var i = 0; i < maxColCount ; i++) {
            var label = self.FilteredData.getColumnLabel(i) || self.ReportText,
                td = $("<td class='text-truncate'>"),
                arrow = $("<span class='glyphicon'>"),
                link = $("<a href='#'>").attr("title", label).data("index", i);
            if (self.OrderDesc)
                arrow.addClass("glyphicon-arrow-down");
            else
                arrow.addClass("glyphicon-arrow-up");
            if (self.OrderIndex == i)
                link.append(arrow);
            link.append(label);
            link.on("click", self.Sort);
            theadRow.append(td.append(link));
        }
        thead.append(theadRow);

        if (maxRowCount) {
            for (var j = 0; j < maxRowCount; j++) {
                var keyname = self.FilteredData.getValue(j, 0);

                var tr = $("<tr>");
                for (var k = 0; k < maxColCount; k++) {
                    var val = self.FilteredData.getFormattedValue(j, k);

                    tr.append("<td class='text-truncate'>" + (val || "") + "</td>");
                }
                tbody.append(tr);
            };
        }
        else
            tbody.append("<tr><td colspan='20' class='text-center'>No data</td></tr>");

        $("#mlsTable").append(thead).append(tbody).append(tfoot);

    }

    function drawMlsSummaryTable_Type() {
        $("#mlsTable").html('');
        $("#mls_type_table").html('');



        var thead = $("<thead>"),
            tbody = $("<tbody>"),
            tfoot = $("<tfoot>"),
            theadRow = $("<tr>"),
            tfootRow = $("<tr>"),
            totalCost = 0,
            totalSaleprice = 0,
            maxColCount = self.MlsSummaryData.getNumberOfColumns(),
            maxRowCount = self.MlsSummaryData.getNumberOfRows();

        for (var i = 0; i < maxColCount ; i++) {
            var label = self.MlsSummaryData.getColumnLabel(i) || self.ReportText,
                td = $("<td class='text-truncate'>");
            theadRow.append(td.append(label));
        }
        thead.append(theadRow);

        if (maxRowCount) {
            for (var j = 0; j < maxRowCount; j++) {
                var keyname = self.MlsSummaryData.getValue(j, 0);
                var tr = $("<tr>");
                for (var k = 0; k < maxColCount; k++) {
                    var val = self.MlsSummaryData.getValue(j, k);
                    if (k == 1) {

                        totalCost += Number(val);
                        val = currencyFormat(val); // "$" + val;
                    }
                    if (k == 2) {

                        totalSaleprice += Number(val);
                        val = currencyFormat(val); // "$" + val;
                    }

                    if (k == 3) {
                        val = val + "%";
                    }

                    tr.append("<td class='text-truncate'>" + (val || "") + "</td>");
                }
                tbody.append(tr);
            };
        }
        else
            tbody.append("<tr><td colspan='20' class='text-center'>No data</td></tr>");
        tfoot.append("<tr><td>Total</td><td>" + currencyFormat(totalCost) + "</td><td>" + currencyFormat(totalSaleprice) + "</td><td>" + (((totalSaleprice - totalCost) / totalSaleprice) * 100).toFixed(2) + "%</td></tr>");
        $("#mls_type_table").append(thead).append(tbody).append(tfoot);



    }

    function drawMlsSummaryTable_Vendor() {
        $("#mlsTable").html('');
        $("#mls_vendor_table").html('');




        var thead = $("<thead>"),
            tbody = $("<tbody>"),
            tfoot = $("<tfoot>"),
            theadRow = $("<tr>"),
            tfootRow = $("<tr>"),
            totalCost = 0,
            totalSaleprice = 0,
            maxColCount = self.MlsSummaryData_vendor.getNumberOfColumns(),
            maxRowCount = self.MlsSummaryData_vendor.getNumberOfRows();

        for (var i = 0; i < maxColCount ; i++) {
            var label = self.MlsSummaryData_vendor.getColumnLabel(i) || self.ReportText,
                td = $("<td class='text-truncate'>");


            theadRow.append(td.append(label));
        }
        thead.append(theadRow);

        if (maxRowCount) {
            for (var j = 0; j < maxRowCount; j++) {
                var keyname = self.MlsSummaryData_vendor.getValue(j, 0);

                var tr = $("<tr>");
                for (var k = 0; k < maxColCount; k++) {
                    // var val = self.MlsSummaryData_vendor.getFormattedValue(j, k);
                    var val = self.MlsSummaryData_vendor.getValue(j, k);
                    if (k == 1) {
                        totalCost += Number(val);
                        val = currencyFormat(val); // "$" + val;
                    }
                    if (k == 2) {
                        totalSaleprice += Number(val);
                        val = currencyFormat(val); // "$" + val;
                    }

                    if (k == 3) {
                        val = val + "%";
                    }
                    tr.append("<td class='text-truncate'>" + (val || "") + "</td>");
                }
                tbody.append(tr);
            };
        }
        else
            tbody.append("<tr><td colspan='20' class='text-center'>No data</td></tr>");
        tfoot.append("<tr><td>Total</td><td>" + currencyFormat(totalCost) + "</td><td>" + currencyFormat(totalSaleprice) + "</td><td>" + (((totalSaleprice - totalCost) / totalSaleprice) * 100).toFixed(2) + "%</td></tr>");

        $("#mls_vendor_table").append(thead).append(tbody).append(tfoot);
    }

    function drawMlsSummaryTable_Tax() {
        $("#mlsTable").html('');
        $("#mls_tax_table").html('');




        var thead = $("<thead>"),
            tbody = $("<tbody>"),
            tfoot = $("<tfoot>"),
            theadRow = $("<tr>"),
            tfootRow = $("<tr>");
        var myTotal = 0.0;
            maxColCount = self.MlsSummaryData_tax.getNumberOfColumns(),
          maxRowCount = self.MlsSummaryData_tax.getNumberOfRows();

        for (var i = 0; i < maxColCount ; i++) {
            var label = self.MlsSummaryData_tax.getColumnLabel(i) || self.ReportText,
                td = $("<td class='text-truncate'>");

            theadRow.append(td.append(label));
        }
        thead.append(theadRow);

        if (maxRowCount) {
            for (var j = 0; j < maxRowCount; j++) {
                var keyname = self.MlsSummaryData_tax.getValue(j, 0);

                var tr = $("<tr>");
                for (var k = 0; k < maxColCount; k++) {
                    var val = self.MlsSummaryData_tax.getValue(j, k);


                    if (k == 1) {
                        myTotal = myTotal + Number(val);
                        val = currencyFormat(val); // "$" + val;
                    }
                    tr.append("<td class='text-truncate'>" + (val || "") + "</td>");
                }
                tbody.append(tr);
            };
        }
        else
            tbody.append("<tr><td colspan='20' class='text-center'>No data</td></tr>");
        tfoot.append("<tr><td>Total</td><td>" + currencyFormat(myTotal)+ "</td></tr>");
        $("#mls_tax_table").append(thead).append(tbody).append(tfoot);
    }

    function drawMlsSummaryTable_Surcharge() {


        $("#mlsTable").html('');
        $("#mls_surcharge_table").html('');


        var thead = $("<thead>"),
            tbody = $("<tbody>"),
            tfoot = $("<tfoot>"),
            theadRow = $("<tr>"),
            tfootRow = $("<tr>"),
            total = 0,
            maxColCount = self.MlsSummaryData_surcharge.getNumberOfColumns(),
            maxRowCount = self.MlsSummaryData_surcharge.getNumberOfRows();

        for (var i = 0; i < maxColCount ; i++) {
            var label = self.MlsSummaryData_surcharge.getColumnLabel(i) || self.ReportText,
                td = $("<td class='text-truncate'>");

            theadRow.append(td.append(label));
        }
        thead.append(theadRow);

        if (maxRowCount) {
            for (var j = 0; j < maxRowCount; j++) {
                var keyname = self.MlsSummaryData_surcharge.getValue(j, 0);

                var tr = $("<tr>");
                for (var k = 0; k < maxColCount; k++) {
                    var val = self.MlsSummaryData_surcharge.getValue(j, k);
                    if (k == 1) {
                        total += Number(val);
                        val = currencyFormat(val); //  "$" + val;
                    }
                    tr.append("<td class='text-truncate'>" + (val || "") + "</td>");
                }
                tbody.append(tr);
            };
        }
        else
            tbody.append("<tr><td colspan='20' class='text-center'>No data</td></tr>");
        tfoot.append("<tr><td>Total</td><td>" + currencyFormat(total) + "</td></tr>");
        $("#mls_surcharge_table").append(thead).append(tbody).append(tfoot);
    }

    self.ExportExcel = function () {
        if (self.SelectedDate() == null && (self.StartDate.Value == null || !self.StartDate.Value.valid()))
            HFC.DisplayAlert("Please pick a date to filter");
        else {
            window.open('/export/mls?territoryId=' + self.TerritoryId() + "&startDate=" + self.StartDate() + "&endDate=" + self.EndDate(), '_blank');
        }
    }

    self.ExportSummeryExcel = function () {
        if (self.SelectedDate() == null && (self.StartDate.Value == null || !self.StartDate.Value.valid()))
            HFC.DisplayAlert("Please pick a date to filter");
        else {
            window.open('/export/mlssummary?territoryId=' + self.TerritoryId() + "&startDate=" + self.StartDate() + "&endDate=" + self.EndDate(), '_blank');
        }
    }

}