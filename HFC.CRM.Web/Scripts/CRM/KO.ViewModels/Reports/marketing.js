﻿HFC.VM.Marketing = function () {
    var self = this,
        dateRange = "1st Half";

    HFC.Ext.Filter(self, dateRange, null, null, true);
        
    self.FilteredData = null;
    self.TotalRow = null;
    self.OrderIndex = null;
    self.OrderDesc = false;
    self.Sort = function () {
        var index = parseInt($(this).data("index"));
        if (self.OrderIndex == index)
            self.OrderDesc = !self.OrderDesc;
        else {
            self.OrderIndex = index;
            self.OrderDesc = false;
        }
        self.FilteredData.sort([{ column: self.OrderIndex, desc: self.OrderDesc }]);
        drawTable();

        return false;
    }

    function drawTable() {

        $("#marketingTable").html('');

        var thead = $("<thead>"),
            tbody = $("<tbody>"),
            tfoot = $("<tfoot>"),
            theadRow = $("<tr>"),
            tfootRow = $("<tr>"),
            maxColCount = self.FilteredData.getNumberOfColumns(),
            maxRowCount = self.FilteredData.getNumberOfRows();
            
        for (var i = 0; i < maxColCount ; i++) {
            var label = self.FilteredData.getColumnLabel(i) || self.ReportText,
                td = $("<td class='text-truncate'>"),
                arrow = $("<span class='glyphicon'>"),
                //div = $("<div class='text-truncate'>"),
                link = $("<a href='#'>").attr("title", label).data("index", i);
            if (self.OrderDesc)
                arrow.addClass("glyphicon-arrow-down");
            else
                arrow.addClass("glyphicon-arrow-up");
            if (self.OrderIndex == i)
                link.append(arrow);
            link.append(label);
            link.on("click", self.Sort);
            theadRow.append(td.append(link));
        }
        thead.append(theadRow);

        if (maxRowCount) {
            for (var j = 0; j < maxRowCount; j++) {
                var keyname = self.FilteredData.getValue(j, 0);
                
                var tr = $("<tr>"),
                    sourceId = self.FilteredData.getRowProperty(j, "id");
                tr.append("<td class='text-truncate'><a href='/reports/sales?report=source&reportId=" + sourceId + "&startDate=" + encodeURIComponent(self.StartDate()) + "&endDate=" + encodeURIComponent(self.EndDate()) + "' target='_blank'>" + self.FilteredData.getValue(j, 0) + "</a></td>");
                for (var k = 1; k < maxColCount; k++) {   
                    var val = self.FilteredData.getFormattedValue(j, k);
                    if (k == maxColCount - 2 && val)
                        tr.append("<td class='text-truncate'><a href='/settings/sources?sourceId=" + sourceId + "' target='_blank'><span class='glyphicon glyphicon-new-window'></span></a> " + val + "</td>");
                    else-
                        tr.append("<td class='text-truncate'>" + (val || "") + "</td>");
                }
                tbody.append(tr);                
            };
        }
        else
            tbody.append("<tr><td colspan='20' class='text-center'>No data</td></tr>");

        if (self.TotalRow && self.TotalRow.c.length) {            
            $.each(self.TotalRow.c, function (i, cell) {                
                tfootRow.append("<td class='text-truncate'>" + (cell.f || cell.v || "") + "</td>");                
            });
            tfoot.append(tfootRow);
        }
        $("#marketingTable").append(thead).append(tbody).append(tfoot);

    }

    self.fetch = function () {        
        self.OrderDesc = false;
        self.OrderIndex = 1;

        HFC.AjaxGetJson('/api/charts/0/marketing', { startDate: self.StartDate(), endDate: self.EndDate() }, function (result) {
            var title = "Lead Effectiveness & Cost Analysis <small>" + (self.StartDate() ? (self.StartDate.Value.toFriendlyString() + " - " + self.EndDate.Value.toFriendlyString()) : "All Date & Time") + "</small>";
            $("#titleHeading").html(title);
            self.TotalRow = result.TotalRow;
            self.FilteredData = new google.visualization.DataTable(result.DataTable);
            self.FilteredData.sort([{ column: self.OrderIndex, desc: self.OrderDesc }]);
            drawTable();
        });

    }



    self.ExportExcel = function () {
        if (self.SelectedDate() == null && (self.StartDate.Value == null || !self.StartDate.Value.valid()))
            HFC.DisplayAlert("Please pick a date to filter");
        else {
            var data = {};
            if (self.StartDate())
                data.StartDate = self.StartDate();
            if (self.EndDate())
                data.EndDate = self.EndDate();
            window.open('/export/marketing?' + $.param(data), '_blank');
        }
    }

}