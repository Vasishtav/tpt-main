﻿HFC.VM.RepSummary = function () {
    var self = this;

    self.LeftCompareValue = ko.observable();
    self.RightCompareValue = ko.observable();
    self.LeftCompareYear = ko.observable();
    self.RightCompareYear = ko.observable();
    self.CompareOptions = ko.observableArray();

    self.DataTable = null;
    self.LeftView = null;
    self.ChartOptions = {};
    
    function drawChart() {
        if (self.DataTable) {
            $(".chart-summary").html('');
            var chart = new google.visualization.AnnotationChart($(".chart-summary").get(0));
            var lastDate = self.LeftView.getValue(self.LeftView.getNumberOfRows() - 1, 0);
            self.ChartOptions.zoomStartTime = new XDate(lastDate).addMonths(-3)[0];            
            chart.draw(self.LeftView || self.DataTable, self.ChartOptions);

            var colChart = new google.visualization.ColumnChart($(".chart-totals").get(0));
            colChart.draw(self.RightView, { legend: {position: 'bottom'}});
        }
    }
    
    function aggregateData() {
        
        var view = new google.visualization.DataView(self.DataTable),
                uniqueYears = [],
                uniqueMonths = [],
                columns = [],
                distinctDates = self.DataTable.getDistinctValues(0);
        $(distinctDates).each(function (i, d) {
            if ($.inArray(d.getFullYear(), uniqueYears) < 0)
                uniqueYears.push(d.getFullYear());
            if ($.inArray(d.getMonth(), uniqueMonths) < 0)
                uniqueMonths.push(d.getMonth());
        });                
        
        //var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true });
        self.FilteredData = new google.visualization.DataTable();
        for (var i = 0; i < view.getNumberOfColumns() ; i++) {
            columns.push(i);
            self.FilteredData.addColumn(view.getColumnType(i), view.getColumnLabel(i), view.getColumnId(i));
            //if(i > 0)
            //formatter.format(self.CacheData.FilteredData, i);
        }

        $(uniqueYears).each(function (i, year) {
            $(uniqueMonths).each(function (j, month) {
                var startDate = new XDate(year, month, 1),
                    endDate = startDate.clone().addMonths(1).addDays(-1),
                    row = [startDate[0]];

                var filterRows = view.getFilteredRows([{ column: 0, minValue: startDate[0], maxValue: endDate[0] }]);
                for (var ci = 1; ci < view.getNumberOfColumns() ; ci++) {
                    var sum = 0;
                    $(filterRows).each(function (k, ri) {
                        sum += view.getValue(ri, ci) || 0;
                    });
                    row[ci] = sum;
                }
                self.FilteredData.addRow(row);
            });
        });

        //var FilteredView = new google.visualization.DataView(self.FilteredData);
        //columns.push({
        //    calc: function (dt, row) {
        //        var total = 0;
        //        for (var col = 1; col < dt.getNumberOfColumns() ; col++) {
        //            total += dt.getValue(row, col) || 0;
        //        }
        //        return { v: total, f: HFC.formatCurrency(total, 2) };
        //    },
        //    label: "Totals",
        //    type: "number"
        //});

        //FilteredView.setColumns(columns);
        //self.FilteredData = FilteredView.toDataTable();
    }
    
    self.OrderIndex = null;
    self.OrderDesc = false;
    self.Sort = function () {
        var index = parseInt($(this).data("index"));
        if (self.OrderIndex == index)
            self.OrderDesc = !self.OrderDesc;
        else {
            self.OrderIndex = index;
            self.OrderDesc = false;
        }
        self.FilteredData.sort([{ column: self.OrderIndex, desc: self.OrderDesc }]);
        drawTable();

        return false;
    }

    function drawTable() {
        
        $(".table-summary").html('');

        var thead = $("<thead>"),
            tbody = $("<tbody>"),
            tfoot = $("<tfoot>"),
            theadRow = $("<tr>"),
            tfootRow = $("<tr>"),
            totalsArray = ["Totals"];
        var maxColCount = self.FilteredData.getNumberOfColumns(),
            maxRowCount = self.FilteredData.getNumberOfRows();

        for (var i = 0; i < maxColCount ; i++) {
            var label = self.FilteredData.getColumnLabel(i),
                td = $("<td class='text-truncate'>"),
                arrow = $("<span class='glyphicon'>"),
                //div = $("<div class='text-truncate'>"),
                link = $("<a href='#'>").attr("title", label).data("index", i);
            if (self.OrderDesc)
                arrow.addClass("glyphicon-arrow-down");
            else
                arrow.addClass("glyphicon-arrow-up");
            if (self.OrderIndex == i)
                link.append(arrow);
            link.append(label);
            link.on("click", self.Sort);
            theadRow.append(td.append(link));
        }
        thead.append(theadRow);

        if (maxRowCount) {
            for (var j = 0; j < maxRowCount; j++) {
                var tr = $("<tr>"),
                    date = new XDate(self.FilteredData.getValue(j, 0));
                var querystring = "report=" + encodeURIComponent(self.Report()) + "&startDate=" + (date.getMonth() + 1) + "-1-" + (date.getFullYear());
                tr.append("<td class='text-truncate'><a target='_blank' href='/reports/sales?" + querystring + "'>" + date.toString("MMM 'yy") + " Totals</a></td>");
                for (var k = 1; k < maxColCount; k++) {                        
                    tr.append("<td class='text-truncate'>" + (k == maxColCount - 1 ? "" : "<a target='_blank' href='/reports/sales?" + querystring + "&id=" + (self.FilteredData.getColumnId(k) || "") + "'>") + HFC.formatCurrency(self.FilteredData.getValue(j, k), 2) + "</a></td>");
                    totalsArray[k] = (totalsArray[k] || 0) + self.FilteredData.getValue(j, k);
                }
                tbody.append(tr);
            };
        }
        else
            tbody.append("<tr colspan='20'><td class='text-center'>No data</td></tr>");

        //var footSubtotal = 0;
        $.each(totalsArray, function (i, v) {                
            tfootRow.append("<td class='text-truncate'>" + (i == 0 ? v : HFC.formatCurrency(v, 2)) + "</td>");
        });
        tfoot.append(tfootRow);
        $(".table-summary").append(thead).append(tbody).append(tfoot);            
        
    }

    self.Compare = function () {
        
        var leftView = new google.visualization.DataView(self.DataTable),
            rightView = new google.visualization.DataView(self.DataTable);

        var leftColIndex = 0,
            rightColIndex = 0;
        for (var i = 1; i < leftView.getNumberOfColumns() ; i++) {
            if (leftView.getColumnLabel(i) == self.LeftCompareValue()) {
                leftColIndex = i;
            }
            if (leftView.getColumnLabel(i) == self.RightCompareValue()) {
                rightColIndex = i;
            }
        }
        if (leftColIndex > 0) 
            leftView.setColumns([0, leftColIndex]);
        else
            leftView.setColumns([0, 1]);
        if (rightColIndex > 0)
            rightView.setColumns([0, rightColIndex]);
        else
            rightView.setColumns([0, 1]);
        var lfstartDate = new XDate(self.LeftCompareYear(), 0, 1),
            lfendDate = lfstartDate.clone().addYears(1).addDays(-1),
            rtstartDate = new XDate(self.RightCompareYear(), 0, 1),
            rtendDate = rtstartDate.clone().addYears(1).addDays(-1);

        leftView.setRows(leftView.getFilteredRows([{ column: 0, minValue: lfstartDate[0], maxValue: lfendDate[0] }]));
        rightView.setRows(rightView.getFilteredRows([{ column: 0, minValue: rtstartDate[0], maxValue: rtendDate[0] }]));

        self.LeftView = new google.visualization.DataTable();
        self.LeftView.addColumn("date", "Date");
        self.LeftView.addColumn("number", self.LeftCompareValue() + " " + self.LeftCompareYear());
        self.LeftView.addColumn("number", self.RightCompareValue() + " " + self.RightCompareYear());

        self.RightView = new google.visualization.DataTable();
        self.RightView.addColumn("string", "Date");
        self.RightView.addColumn("number", self.LeftCompareValue() + " " + self.LeftCompareYear() + " Totals");
        self.RightView.addColumn("number", self.RightCompareValue() + " " + self.RightCompareYear() + " Totals");

        var distinctMonthYear = [];

        var distinctDates = leftView.getDistinctValues(0);
        $(rightView.getDistinctValues(0)).each(function (i, d) {
            var newDate = new Date(self.LeftCompareYear(), d.getMonth(), d.getDate());
            if ($.inArray(newDate, distinctDates) < 0)
                distinctDates.push(newDate);
        });
        $(distinctDates).each(function (i, d) {
            var leftVal = 0,                
                rightVal = 0,
                rightDate = new Date(self.RightCompareYear(), d.getMonth(), d.getDate());

            var left_filtered = leftView.getFilteredRows([{ column: 0, value: d }]);
            var right_filtered = rightView.getFilteredRows([{ column: 0, value: rightDate }]);

            if (left_filtered && left_filtered.length)
                leftVal = leftView.getValue(left_filtered[0], 1);
            if (right_filtered && right_filtered.length)
                rightVal = rightView.getValue(right_filtered[0], 1);

            if($.inArray(d.getMonth(), distinctMonthYear) < 0)            
                distinctMonthYear.push(d.getMonth());

            self.LeftView.addRow([d, leftVal, rightVal]);
        });

        $(distinctMonthYear).each(function (i, month) {
            var leftVal = 0,
                rightVal = 0,
                lftstartDate = new XDate(self.LeftCompareYear(), month, 1),
                lftendDate = lftstartDate.clone().addMonths(1).addDays(-1),
                rtstartDate = new XDate(self.RightCompareYear(), month, 1),
                rtendDate = rtstartDate.clone().addMonths(1).addDays(-1);

            var left_filtered = self.FilteredData.getFilteredRows([{ column: 0, minValue: lftstartDate[0], maxValue: lftendDate[0] }]);
            var right_filtered = self.FilteredData.getFilteredRows([{ column: 0, minValue: rtstartDate[0], maxValue: rtendDate[0] }]);

            if (left_filtered && left_filtered.length)
                leftVal = self.FilteredData.getValue(left_filtered[0], leftColIndex);
            if (right_filtered && right_filtered.length)
                rightVal = self.FilteredData.getValue(right_filtered[0], rightColIndex);
            
            self.RightView.addRow([lftstartDate.toString("MMM"), leftVal, rightVal]);
        });
        var formatter = new google.visualization.NumberFormat({ prefix: '$', negativeColor: 'red', negativeParens: true });
        formatter.format(self.RightView, 1);
        formatter.format(self.RightView, 2);

        drawChart();
        
    }

    function getUnique(inputArray) {
        var outputArray = [];

        for (var i = 0; i < inputArray.length; i++) {
            if ((jQuery.inArray(inputArray[i], outputArray)) == -1) {
                outputArray.push(inputArray[i]);
            }
        }

        return outputArray;
    }

    function processJson(result, title) {
        if (!result.DataTable.cols.length) {
            $('.chart-summary').append('<h3>No data presents!</h3>');
            $('.chart-summary').css('text-align', 'center');
            return;
        }
        $("#titleHeader").text(title);
        self.DataTable = new google.visualization.DataTable(result.DataTable);
        
        self.CompareOptions(result.UniqueKeys);

        if (result.UniqueKeys.length) {
            self.LeftCompareValue(result.UniqueKeys[0]);
            //set default right compare value
            if (self.Years() && self.Years().length > 1)
                self.RightCompareYear(self.Years()[1]);
            else if (result.UniqueKeys.length > 1)
                self.RightCompareValue(result.UniqueKeys[1]);
        }
        
        //chart.setAction({
        //    id: 'detail',
        //    text: 'View Detail',
        //    action: function () {
        //        var selection = chart.getSelection(),                    
        //            month = datatable.xf[selection[0].row].c[0].v,
        //            id = datatable.yf[selection[0].column].id;
        //        if (id)
        //            window.location.href = '/reports/sales/?report=' + encodeURIComponent(self.Report) + '&month=' + encodeURIComponent(month) + '&id=' + encodeURIComponent(id);
        //    }
        //}); 
                
        self.ChartOptions = {            
            displayAnnotations: false,
            fill: 20,
            thickness: 2,
            //displayZoomButtons: false,
            numberFormats: "$#,##0.00"
            //hAxis: { baselineColor: "transparent" },
            //vAxis: { title: "Sales" },
            //legend: { position: 'top' }
        };
        
        aggregateData();
        drawTable();
        self.Compare();
    }

    self.ReportText = "";
    self.Report = ko.observable();
    self.Years = ko.observableArray([new Date().getFullYear().toString()]);
    self.fetchCharts = function () {        
        self.ReportText = $("#reportDDL option:selected").text();
        self.OrderIndex = null;
        self.OrderDesc = false;

        //have to use ajax type of "text" and manually eval the date object        
        HFC.Ajax('GET', '/api/charts/0/' + self.Report(), { years: self.Years() }, function (result) {
            processJson(eval("(" + result + ")"), self.Years().join(", ") + " Yearly Sales Summary - by " + self.ReportText);
        }, null, 'text');          
    }

    self.exportCharts = function () {
        window.open('/export/report/0/?ReportType=' + self.Report() + "&years=" + self.Years().join("&years="), "_blank");
    }
    //we add a resizeTO timeout so it only needs to resize when user stops resizing, not as user resize so it doesnt consume too much resource
    function redrawChart() {        
        if (this.resizeTimeOut) clearTimeout(this.resizeTimeOut);
        this.resizeTimeOut = setTimeout(function () {
            if (Math.abs(window.innerWidth - window.currentWidth) > 50) { //only resize if width changes, height will change often with bootstrap drop down menu                                            
                drawChart();
                window.currentWidth = window.innerWidth;
            }
        }, 500);        
    };

    //bind to resize so we can refresh the chart and let google resize the chart if user resizes window
    $(window).unbind('resize.reports');
    $(window).bind('resize.reports', redrawChart);
    window.addEventListener("orientationchange", redrawChart);
    window.currentWidth = window.innerWidth;
    window.currentHeight = window.innerHeight;

}