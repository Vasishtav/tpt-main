﻿HFC.VM.MSR = function () {
    var self = this;

    self.Year = ko.observable(new Date().getFullYear());
    self.Month = ko.observable(new Date().getMonth() + 1);
    self.TerritoryId = ko.observable();
    self.TerritoryText = ko.computed(function () {
        return $("#territoryDDL option:selected").text();
    });
    
    self.ViewReport = function () {
        $("#reportPaper .paper-body").load('/reports/msr/' + self.TerritoryId(), { month: self.Month(), year: self.Year() });
    }

    self.PrintReport = function () {        
        if (HFC.Print) 
            HFC.Print("/export/msr/" + self.TerritoryId() + '?type=PDF&inline=true&month=' + self.Month() + '&year=' + self.Year());
        else
            HFC.DisplayAlert("Print frame not found");
    }

    self.EmailReport = function () {
        var attachmentHref = "/export/msr/" + self.TerritoryId() + '?type=PDF&inline=true&month=' + self.Month() + '&year=' + self.Year();
        $("#email_attachment").attr('href', attachmentHref).text($("#territoryDDL option:selected").data("code") + "_" + new XDate(self.Year(), self.Month() - 1, 1).toString("MMMyyyy") + "_MSR.pdf");

        try {
            $("#email_toAddresses").select2("val", $("#email_toAddresses").data("default") || "");
            $("#email_bccAddresses").select2("val", $("#email_bccAddresses").data("default") || "");
        } catch (err) { }
        $("#email_subject").val($("#email_subject").data("default") || "");
        $('#emailEditor').code("<br/><br/>" + (HFC.CurrentUser.EmailSignature || ""));

        $("#emailForm").unbind('submit').submit(function (e) {
            e.preventDefault();
            $("#emailForm").find("button").prop("disabled", true);
            
            //convert all css style into inline style for emailing
            $(".note-editable").find('*').each(function (i, elem) {
                elem.style.cssText = document.defaultView.getComputedStyle(elem, "").cssText;
            });
            var bodyHtml = $('#emailEditor').code();
            var data = {
                startDate: self.Month() + '/1/' + self.Year(),
                toAddresses: $("#email_toAddresses").select2("val"),
                bccAddresses: $("#email_bccAddresses").select2("val"),
                subject: HFC.htmlEncode($("#email_subject").val()),
                body: HFC.htmlEncode(bodyHtml),
                attachment: attachmentHref
            };
            HFC.AjaxPost('/email/msr/'+self.TerritoryId(), data, function (result) {
                HFC.DisplaySuccess("Report sent");
                $("#emailModal").modal("hide");
            }).always(function () {
                $("#emailForm").find("button").prop("disabled", false);
            });

            return false;
        });
        $("#emailModal").modal("show");
    }
}