﻿HFC.VM.RepSales = function (report, startDate, endDate) {
    var self = this,
        dateRange = "This Month";

    HFC.Ext.Filter(self, dateRange, null, null, true);
    if (startDate) {
        self.SelectedDate(null);
        self.StartDate(startDate);
        self.EndDate(endDate);
    }

    self.Report = ko.observable(report);

    //self.CacheData = [];
    //function drawChart() {
    //    if (self.CacheData) {
    //        $.each(self.CacheData, function (i, c) {
    //            c.Chart.draw(c.Data, c.Options);
    //        });
    //    }
    //}

    self.FilteredData = null;
    self.SalesData = null;
    self.OrderIndex = null;
    self.OrderDesc = false;
    self.Sort = function () {
        var index = parseInt($(this).data("index"));
        if (self.OrderIndex == index)
            self.OrderDesc = !self.OrderDesc;
        else {
            self.OrderIndex = index;
            self.OrderDesc = false;
        }
        self.FilteredData.sort([{ column: self.OrderIndex, desc: self.OrderDesc }]);
        drawTable();

        return false;
    }

    function drawTable() {

        $("#reportTable").html('');

        var thead = $("<thead>"),
            tbody = $("<tbody>"),
            tfoot = $("<tfoot>"),
            theadRow = $("<tr>"),
            tfootRow = $("<tr>"),
            maxColCount = self.FilteredData.getNumberOfColumns(),
            maxRowCount = self.FilteredData.getNumberOfRows(),
            totalsArray = new Array(maxColCount);
        totalsArray[0] = "Totals";

        theadRow.append('<td style="width:30px"></td>');
        for (var i = 0; i < maxColCount ; i++) {
            var label = self.FilteredData.getColumnLabel(i) || self.ReportText,
                td = $("<td class='text-truncate'>"),
                arrow = $("<span class='glyphicon'>"),
                //div = $("<div class='text-truncate'>"),
                link = $("<a href='#'>").attr("title", label).data("index", i);
            if (self.OrderDesc)
                arrow.addClass("glyphicon-arrow-down");
            else
                arrow.addClass("glyphicon-arrow-up");
            if (self.OrderIndex == i)
                link.append(arrow);
            link.append(label);
            link.on("click", self.Sort);
            theadRow.append(td.append(link));
        }
        thead.append(theadRow);

        if (maxRowCount) {
            for (var j = 0; j < maxRowCount; j++) {
                var keyname = self.FilteredData.getValue(j, 0);
                var jobs = $.grep(self.SalesData, function (s) {
                    if (self.Report() == "city")
                        return s.City == keyname;
                    else if (self.Report() == "zipcode")
                        return s.ZipCode == keyname;
                    else
                        return s.KeyName == keyname;
                });
                var tr = $("<tr>"),
                    toggle = $('<a href="#"><span class="glyphicon glyphicon-plus"></span></a>'),
                    td = $("<td>");
                toggle.on("click", function () {
                    $(this).parent().parent().next().toggle();
                    $(this).find("span").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
                    return false;
                });
                tr.append(td.append(toggle));
                tr.append("<td class='text-truncate'>" + self.FilteredData.getValue(j, 0) + "</td>");
                for (var k = 1; k < maxColCount; k++) {
                    tr.append("<td class='text-truncate'>" + self.FilteredData.getFormattedValue(j, k) + "</td>");
                    if (self.FilteredData.getColumnType(k) == "number")
                        totalsArray[k] = (totalsArray[k] || 0) + self.FilteredData.getValue(j, k);
                }
                tbody.append(tr);

                tr = $("<tr class='jobrow'>");
                if (jobs && jobs.length) {
                    var jobTable = $('<table>');
                    jobTable.addClass("table");
                    if (self.Report() == "product" || self.Report() == "type" || self.Report() == "vendor") {

                        jobTable.append("<thead><tr><th>Job #</th><th>1st Appt</th><th>Contract Date</th><th>Customer Name</th><th>Total</th></tr></thead>");
                        $.each(jobs, function (jidx, job) {
                            var aptDate = '',
                                contractDate = '';
                            if (job.FirstAppointment) {
                                var date = new XDate(job.FirstAppointment);
                                aptDate = date.toFriendlyString();
                            }
                            if (job.ContractedOnUtc) {
                                var date = new XDate(job.ContractedOnUtc);
                                contractDate = date.toFriendlyString(true);
                            }
                            jobTable.append("<tr><td><a href='/#/leads/" + job.LeadId + "/" + job.JobId + "' target='lead" + job.LeadId + "'>" + job.JobNumber +
                                "</a></td><td>" + aptDate + "</td><td>" + contractDate + "</td><td>" + job.FirstName + " " + (job.LastName || '') +
                                "</td>><td>" + HFC.formatCurrency(job.Subtotal, 2) + "</td>" + "</tr>");
                        });


                    } else {
                        jobTable.append("<thead><tr><th>Job #</th><th>1st Appt</th><th>Contract Date</th><th>Customer Name</th><th>Subtotal</th>" + (self.Report() == "category" ? "" : "<th>Surcharge</th><th>Discount</th><th>Total</th>") + "</tr></thead>");
                        $.each(jobs, function (jidx, job) {
                            var aptDate = '',
                                contractDate = '';
                            if (job.FirstAppointment) {
                                var date = new XDate(job.FirstAppointment);
                                aptDate = date.toFriendlyString();
                            }
                            if (job.ContractedOnUtc) {
                                var date = new XDate(job.ContractedOnUtc);
                                contractDate = date.toFriendlyString(true);
                            }
                            jobTable.append("<tr><td><a href='/leads/" + job.LeadId + "/" + job.JobId + "' target='lead" + job.LeadId + "'>" + job.JobNumber +
                                "</a></td><td>" + aptDate + "</td><td>" + contractDate + "</td><td>" + job.FirstName + " " + (job.LastName || '') +
                                "</td><td>" + HFC.formatCurrency(job.Subtotal, 2) + "</td>" +
                                (self.Report() == "category" ? "" : "<td>" + HFC.formatCurrency(job.SurchargeTotal, 2) + "</td><td>" + HFC.formatCurrency(job.DiscountTotal, 2) + "</td><td>" + HFC.formatCurrency(job.Subtotal + job.SurchargeTotal - job.DiscountTotal, 2) + "</td>") + "</tr>");
                        });
                    }
                    td = $("<td colspan=20>");
                    tr.append(td.append(jobTable));
                } else  //This shouldn't happen but just in case                    
                    tr.append("<td colspan='20' class='text-center'>No data</td>");

                tbody.append(tr);
            };
        }
        else
            tbody.append("<tr><td colspan='20' class='text-center'>No data</td></tr>");

        if (totalsArray.length > 1) {
            tfootRow.append("<td></td>");
            if (self.Report() == "product" || self.Report() == "type" || self.Report() == "vendor") {
                tfootRow.append("<td>Totals</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[1] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[2] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + HFC.formatCurrency(totalsArray[3]) + "</td>");
                var AvgSales = HFC.formatCurrency(totalsArray[3] / totalsArray[2], 2);
                var ClosingPercent = HFC.formatPercent((totalsArray[2] / totalsArray[1]) * 100, 2);
                tfootRow.append("<td class='text-truncate'>" + ClosingPercent + "</td>");
                tfootRow.append("<td class='text-truncate'>" + AvgSales + "</td>");
                tfootRow.append("<td></td>");
            } else if (self.Report() == "category") {
                tfootRow.append("<td>Totals</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[1] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[2] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[3] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + HFC.formatCurrency(totalsArray[4]) + "</td>");
                var AvgSales = HFC.formatCurrency(totalsArray[4] / totalsArray[3], 2);
                var ClosingPercent = HFC.formatPercent((totalsArray[3] / totalsArray[1]) * 100, 2);
                tfootRow.append("<td class='text-truncate'>" + ClosingPercent + "</td>");
                tfootRow.append("<td class='text-truncate'>" + AvgSales + "</td>");
                tfootRow.append("<td></td>");
            }
            else if (self.Report() == "source" || self.Report() == "territory")
            {
                tfootRow.append("<td>Totals</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[1] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[2] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[3] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[4] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[5] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + HFC.formatCurrency(totalsArray[6], 2) + "</td>");
             
                var AvgSales = HFC.formatCurrency(totalsArray[6] / totalsArray[5], 2);
                var ClosingPercent = HFC.formatPercent((totalsArray[5] / totalsArray[2]) * 100, 2);
                tfootRow.append("<td class='text-truncate'>" + ClosingPercent + "</td>");
                tfootRow.append("<td class='text-truncate'>" + AvgSales + "</td>");
                tfootRow.append("<td></td>");
            }
            else if (self.Report() == "zipcode") {
                tfootRow.append("<td>Totals</td>");
                tfootRow.append("<td class='text-truncate'></td>");
                tfootRow.append("<td class='text-truncate'></td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[3] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[4] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[5] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[6] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[7] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + HFC.formatCurrency(totalsArray[8], 2) + "</td>");
                var AvgSales = HFC.formatCurrency(totalsArray[8] / totalsArray[7], 2);
                var ClosingPercent = HFC.formatPercent((totalsArray[7] / totalsArray[4]) * 100, 2);
                tfootRow.append("<td class='text-truncate'>" + ClosingPercent + "</td>");
                tfootRow.append("<td class='text-truncate'>" + AvgSales + "</td>");
                tfootRow.append("<td></td>");
            } else {
                tfootRow.append("<td>Totals</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[1] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[2] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[3] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[4] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + totalsArray[5] + "</td>");
                tfootRow.append("<td class='text-truncate'>" + HFC.formatCurrency(totalsArray[6], 2) + "</td>");

                var AvgSales = HFC.formatCurrency(totalsArray[6] / totalsArray[5], 2);
                var ClosingPercent = HFC.formatPercent((totalsArray[5] / totalsArray[2]) * 100, 2);
                tfootRow.append("<td class='text-truncate'>" + ClosingPercent + "</td>");
                tfootRow.append("<td class='text-truncate'>" + AvgSales + "</td>");
                tfootRow.append("<td></td>");

                //$.each(totalsArray, function (i, v) {
                //    if (v) {
                //        var col = self.FilteredData.getColumnLabel(i);
                //        if (col.indexOf('%') > 0)
                //            tfootRow.append("<td class='text-truncate'>" + HFC.formatPercent((v / maxRowCount) * 100.0, 2) + "</td>");
                //        else if (col == "Total Sales")
                //            tfootRow.append("<td class='text-truncate'>" + HFC.formatCurrency(v, 2) + "</td>");
                //        else if (col == "Total Alloc Sales")
                //            tfootRow.append("<td class='text-truncate'>" + HFC.formatCurrency(v, 2) + "</td>");
                //        else if (col == "Avg Sales")
                //            tfootRow.append("<td class='text-truncate'>" + HFC.formatCurrency(v / maxRowCount, 2) + "</td>");
                //        else
                //            tfootRow.append("<td class='text-truncate'>" + v + "</td>");
                //    } else
                //        tfootRow.append("<td></td>");
                //});

            }

            tfoot.append(tfootRow);
        }
        $("#reportTable").append(thead).append(tbody).append(tfoot);

    }

    //function processChart(result, cacheKey, title) {       

    //    var chart = null,
    //        datatable = google.visualization.arrayToDataTable([['Empty', 'Column']]),
    //        item = getCache(cacheKey);

    //    if(result.DataTable && result.DataTable.cols.length) {
    //        chart = new google.visualization.ColumnChart($(".chart-summary").get(0));
    //        datatable = new google.visualization.DataTable(result.DataTable);
    //    } else 
    //        chart = new google.visualization.PieChart($(".chart-summary").get(0));

    //    drawTable(result.RawData);

    //    if (item) {
    //        item.Data = datatable;
    //        item.Chart = chart;
    //        item.Options.title = title;
    //    } else {
    //        var cOptions = {
    //            title: title,
    //            vAxis: { title: "Sales" }
    //            //hAxis: { title: "" }
    //        };

    //        self.CacheData.push({ Key: cacheKey, Chart: chart, Data: datatable, Options: cOptions });
    //    }
    //    drawChart();
    //}
    self.ReportText = "";
    self.FilterText = "";
    self.fetchCharts = function () {
        self.ReportText = $("#reportDDL option:selected").text();
        //self.FilterText = $("#filterDDL option:selected").text();;
        self.OrderDesc = false;
        self.OrderIndex = null;

        HFC.AjaxGetJson('/api/charts/1/' + self.Report(), { startDate: self.StartDate(), endDate: self.EndDate() }, function (result) {
            $("#titleHeading").text(self.StartDate() + " - " + self.EndDate() + " Sales - by " + self.ReportText + (self.FilterText ? (" - " + self.FilterText) : ""));
            self.SalesData = result.SalesData;
            self.FilteredData = new google.visualization.DataTable(result.DataTable);
            drawTable();
        });

    }

    self.ExportExcel = function () {
        var test = $("#reportDDL option:selected");
        self.ReportText = $("#reportDDL option:selected").text();
        if (self.SelectedDate() == null && (self.StartDate.Value == null || !self.StartDate.Value.valid()))
            HFC.DisplayAlert("Please pick a date to filter");
        else {
            var data = {};
            if (self.ReportText)
                data.ReportType = self.ReportText;
            if (self.StartDate())
                data.StartDate = self.StartDate();
            if (self.EndDate())
                data.EndDate = self.EndDate();
            window.open('/export/sales?' + $.param(data), '_blank');
        }
    }

    ////we add a resizeTO timeout so it only needs to resize when user stops resizing, not as user resize so it doesnt consume too much resource
    //function redrawChart() {
    //    if (this.resizeTimeOut) clearTimeout(this.resizeTimeOut);
    //    this.resizeTimeOut = setTimeout(function () {
    //        if (Math.abs(window.innerWidth - window.currentWidth) > 50) { //only resize if width changes, height will change often with bootstrap drop down menu                                            
    //            drawChart();
    //            window.currentWidth = window.innerWidth;
    //        }
    //    }, 500);
    //};

    ////bind to resize so we can refresh the chart and let google resize the chart if user resizes window
    //$(window).unbind('resize.reports');
    //$(window).bind('resize.reports', redrawChart);
    //window.addEventListener("orientationchange", redrawChart);
    //window.currentWidth = window.innerWidth;
    //window.currentHeight = window.innerHeight;

}