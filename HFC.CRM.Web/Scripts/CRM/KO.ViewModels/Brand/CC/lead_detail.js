﻿HFC.VM.LeadDetail = function (lead) {
    var self = this;    

    self.IsBusy = ko.observable();
    self.InJobAddressEdit = ko.observable();
    self.Lead = HFC.Models.Lead(lead);

    self.DeleteSource = function (src) {        
        if (src.SourceId() && self.Lead.LeadId > 0 && confirm("Do you wish to continue?")) {
            HFC.AjaxDelete('/api/leads/' + self.Lead.LeadId + '/source', { SourceId: src.SourceId() }, function () {
                var srcOb = src.Source,
                    text = "";
                if (srcOb != null)
                    text = "Source " + src.Name + " deleted";
                else
                    text = "Source deleted";
                HFC.DisplaySuccess(text);
                self.Lead.LeadSources.remove(src);
            });
        } else if (!src.SourceId()) {
            //we can just remove since the source is empty
            self.Lead.LeadSources.remove(src);
        }
    }
    self.AddSource = function () {
        self.Lead.LeadSources.push(HFC.Models.LeadSource({LeadId: self.Lead.LeadId}));
    }
    self.SourceOnChanged = function (src, jsEvent) {
        var srcId = $(jsEvent.currentTarget).data("value");
        if (srcId != src.SourceId() && self.Lead.LeadId > 0) {
            if (src.SourceId()) {
                //put for updating record
                HFC.AjaxPut('/api/leads/' + self.Lead.LeadId + '/source', { OriginalSourceId: src.SourceId(), SourceId: srcId },
                    function () {
                        src.SourceId(srcId);
                        HFC.DisplaySuccess("Source saved");
                    });
            } else {
                //post for adding new record
                HFC.AjaxPost('/api/leads/' + self.Lead.LeadId + '/source', { SourceId: srcId },
                    function (result) {
                        src.SourceId(srcId);
                        HFC.DisplaySuccess("Source saved");
                    });
            }
        } else
            src.SourceId(srcId);
    }
    self.AddSecondPerson = function () {
        if (self.Lead.SecPerson == null)
            self.Lead.SecPerson = HFC.Models.Person({});

        self.EditPerson(self.Lead.SecPerson);
    }
    self.AddJob = function (btn) {
        $(btn).prop("disabled", true);
        HFC.AjaxGetJson('/api/leads/' + self.Lead.LeadId + '/jobs', null,
            function (result) {
                $.each(self.Lead.Jobs(), function (i, j) {
                    j.IsCollapsed(true);
                });
                self.Lead.Jobs.push(HFC.Models.Job(result));
            }).done(function () {
                $(btn).prop("disabled", false);
            });
    }
    self.CancelLead = function () {
        window.close();
        $("#crm-lead-detail .panel-body").html(""); //wipe it out in case we can't close the window/tab
    }
    self.SaveLead = function () {  
        var data = {
            'PrimPerson.FirstName': self.Lead.PrimPerson.FirstName(),
            'PrimPerson.LastName': self.Lead.PrimPerson.LastName(),
            'PrimPerson.CompanyName': self.Lead.PrimPerson.CompanyName(),
            'PrimPerson.WorkTitle': self.Lead.PrimPerson.WorkTitle(),
            'PrimPerson.HomePhone': self.Lead.PrimPerson.HomePhone(),
            'PrimPerson.CellPhone': self.Lead.PrimPerson.CellPhone(),
            'PrimPerson.FaxPhone': self.Lead.PrimPerson.FaxPhone(),
            'PrimPerson.WorkPhone': self.Lead.PrimPerson.WorkPhone(),
            'PrimPerson.WorkPhoneExt': self.Lead.PrimPerson.WorkPhoneExt(),
            'PrimPerson.PrimaryEmail': self.Lead.PrimPerson.PrimaryEmail(),
            'PrimPerson.SecondaryEmail': self.Lead.PrimPerson.SecondaryEmail(),
            'PrimPerson.PreferredTFN': self.Lead.PrimPerson.PreferredTFN()
        };        
        if (!self.Lead.PrimPerson.IsSubscribed())
            data['PrimPerson.UnsubscribedOnUtc'] = new Date().toISOString();
        
        if (self.Lead.Addresses.Collection().length) {
            $.each(self.Lead.Addresses.Collection(), function (i, addr) {
                data['Addresses[' + i + '].IsResidential'] = addr.IsResidential() ? "true" : "false";
                data['Addresses[' + i + '].AttentionText'] = addr.AttentionText();
                data['Addresses[' + i + '].Address1'] = addr.Address1();
                data['Addresses[' + i + '].Address2'] = addr.Address2();
                data['Addresses[' + i + '].City'] = addr.City();
                data['Addresses[' + i + '].State'] = addr.State();
                data['Addresses[' + i + '].ZipCode'] = addr.ZipCode();
                data['Addresses[' + i + '].CountryCode2Digits'] = addr.CountryCode2Digits();
                data['Addresses[' + i + '].CrossStreet'] = addr.CrossStreet();
            });
        }
        if (self.Lead.LeadSources().length) {
            $.each(self.Lead.LeadSources(), function (i, src) {
                data['LeadSources[' + i + '].SourceId'] = src.SourceId();
            });
        }
        self.IsBusy(true);
        HFC.AjaxPost('/api/leads', data, function (result) {
            HFC.DisplaySuccess("Lead Created")
            window.location = '/leads/' + result.LeadId;
        }).complete(function () {
            self.IsBusy(false);
        });

    }
    self.EditPerson = function (model) {
        ko.editable(model);
        model.beginEdit();
        model.Template("person-edit");

        if (HFC.KO && HFC.KO.EditModal) {            
            HFC.KO.EditModal.Title(model.PersonId ? "Edit Person" : "New Person");
            HFC.KO.EditModal.EditableModel(model);            
            HFC.KO.EditModal.Save = self.SavePerson;
            HFC.KO.EditModal.Reset = self.ResetPerson;
            HFC.KO.EditModal.Show();
        }
    }
    self.SavePerson = function () {
        var model = HFC.KO.EditModal.EditableModel();//model.tagName ? this : model;

        var data = model.toJS(),
			uri = '/api/leads/' + self.Lead.LeadId + '/person',
			method = 'POST';
        
        if (data.PersonId)
            method = 'PUT'; //update
        if (HFC.KO && HFC.KO.EditModal)
            HFC.KO.EditModal.IsBusy(true);

        HFC.Ajax(method, uri, data, function (result) {
            if (!model.PersonId && result && model.PersonId != result.PersonId) {
                model.PersonId = result.PersonId;                
            }
            model.Template("person-view");
            model.commit();
            //if successful then close modal
            if(HFC.KO && HFC.KO.EditModal)
                HFC.KO.EditModal.Hide();
            HFC.DisplaySuccess("Person saved");
        }).complete(function () {
            if (HFC.KO && HFC.KO.EditModal)
                HFC.KO.EditModal.IsBusy(false);
        });
    }
    self.ResetPerson = function (model) {
        model.rollback();
        if (!model.PersonId)
            self.Lead.SecPerson = null;
        if (HFC.KO && HFC.KO.EditModal) {
            HFC.KO.EditModal.IsBusy(false);
            HFC.KO.EditModal.Hide();
        }
    }
        
    self.OriginalBillAddressId = null;
    self.OriginalInstallAddressId = null;
    self.EditJobAddr = function (job) {
        self.OriginalBillAddressId = job.BillingAddress.AddressId;
        self.OriginalInstallAddressId = job.InstallAddress.AddressId;
        self.InJobAddressEdit(true);
    }
    self.CancelJobAddr = function (job) {
        job.BillingAddressId(self.OriginalBillAddressId);
        job.InstallAddressId(self.OriginalInstallAddressId);
        self.InJobAddressEdit(false);
    }
    self.SaveJobAddr = function (job) {
        HFC.AjaxPut('/api/jobs/' + job.JobId + '/address', { BillingAddressId: job.BillingAddressId(), InstallAddressId: job.InstallAddressId() },
            function () {
                job.BillingAddress = self.Lead.Addresses.Get(job.BillingAddressId());
                job.InstallAddress = self.Lead.Addresses.Get(job.InstallAddressId());
                HFC.DisplaySuccess("Job addresses updated");
                self.InJobAddressEdit(false);
            });        
    }
    
    self.AddQuote = function (job) {
        if (confirm("Do you want to continue?")) {
            HFC.AjaxPost('/api/quotes/', { JobId: job.JobId }, function (result) {
                job.JobQuotes.push(HFC.Models.Quote(result));
            });
        }
    }

    self.CloneQuote = function (quoteToClone, job) {
        if (quoteToClone) {
            HFC.AjaxPost('/api/quotes/' + quoteToClone.QuoteId + '/clone', null, function (result) {
                job.JobQuotes.push(HFC.Models.Quote(result));
            });
        }
    }

    self.SetPrimaryQuote = function (quoteToSet, job) {
        if (quoteToSet) {
            HFC.AjaxPut('/api/quotes/' + quoteToSet.QuoteId + '/primary', null, function (result) {
                $.each(job.JobQuotes(), function (i, q) {
                    $.each(q.JobItems(), function (j, ji) {
                        ji.LastUpdated = new XDate(result.LastUpdated, true);
                    });
                    if (q.QuoteId == quoteToSet.QuoteId)
                        q.IsPrimary(true);
                    else
                        q.IsPrimary(false);
                });
                quoteToSet.LastUpdated = new XDate(result.LastUpdated, true);
            });
        }
    }

    self.DeleteQuote = function (quote, job, primquote) {
        if (quote) {
            HFC.AjaxDelete('/api/quotes/' + quote.QuoteId, null, function (result) {
                job.JobQuotes.remove(quote);
                //show primary quote
                $('a[href="#quote'+job.JobId+'_'+primquote.QuoteId+'"]').tab('show');
                HFC.DisplaySuccess("Quote deleted");
            });
        }
    }

    self.DeleteJob = function (job) {
        if (confirm("All quotes will also be deleted, do you want to continue?")) {
            HFC.AjaxDelete('/api/jobs/' + job.JobId, null, function () {
                self.Lead.Jobs.remove(job);
                HFC.DisplaySuccess("Job #" + job.JobNumber + " deleted");
            });
        }
    };
    
    self.Print = function (model, jsEvent, type, id) {
        if (type) {
            var url = '/export/' + type + '/';
            
            HFC.Print(url + (id || model.JobId) + '/?type=PDF&inline=true');            
        } else
            HFC.DisplayAlert("Action cancelled, this type is not printable");
    }
    self.Email = function (model, jsEvent, type, id, label, subject, toAddress) {
        if (!HFC.EmailTemplateIsLoaded) {
            var loadingElement = document.getElementById("loading");
            var emailDiv = $("<div>");
            $(".wrapper").append(emailDiv);
            $(emailDiv).load('/partial/email/', function () {
                HFC.EmailTemplateIsLoaded = true;
                email(type, id || model.JobId, label, subject, toAddress);
                loadingElement.style.display = "none";
            });
        } else
            email(type, id || model.JobId, label, subject, toAddress);
    }

    function email(type, id, label, subject, toAddress) {        
        var attachmentHref = "/export/"+type+"/" + id + '?type=PDF&inline=true';
        $("#email_attachment").attr('href', attachmentHref).text((label || "Untitled")+".pdf");

        try {
            if(toAddress)
                $("#email_toAddresses").select2("data", [{ id: toAddress, text: toAddress }]);            
            $("#email_bccAddresses").select2("val", "");
        } catch (err) { }
        $("#email_subject").val(subject || "");
        $('#emailEditor').code("<br/><br/>" + (HFC.CurrentUser.EmailSignature || ""));

        $("#emailForm").unbind('submit').submit(function (e) {
            e.preventDefault();
            $("#emailForm").find("button").prop("disabled", true);

            //convert all css style into inline style for emailing
            $(".note-editable").find('*').each(function (i, elem) {
                elem.style.cssText = document.defaultView.getComputedStyle(elem, "").cssText;
            });
            var bodyHtml = $('#emailEditor').code();
            var data = {
                toAddresses: $("#email_toAddresses").select2("val"),
                bccAddresses: $("#email_bccAddresses").select2("val"),
                subject: HFC.htmlEncode($("#email_subject").val()),
                body: HFC.htmlEncode(bodyHtml),
                attachment: attachmentHref
            };
            HFC.AjaxPost('/email/'+type+'/' + id, data, function (result) {
                HFC.DisplaySuccess("Email successfully sent");
                $("#emailModal").modal("hide");
            }).always(function () {
                $("#emailForm").find("button").prop("disabled", false);
            });

            return false;
        });
        $("#emailModal").modal("show");
    }

    //##################################################################### region Sales Adjustment Methods
    self.AddSurcharge = function (quote) {
        var saleAdj = HFC.Models.SaleAdjustment({ TypeEnum: SaleAdjTypeEnum.Surcharge, Category: "Installation", JobId: quote.JobId, QuoteId: quote.QuoteId, Template: "sales-adj-edit" }, quote);
        quote.SaleAdjustments.push(saleAdj);
        self.EditSaleAdjustment(saleAdj);
    };

    self.AddDiscount = function (quote) {
        var saleAdj = HFC.Models.SaleAdjustment({ TypeEnum: SaleAdjTypeEnum.Discount, Category: "Discount", JobId: quote.JobId, QuoteId: quote.QuoteId, Template: "sales-adj-edit" }, quote);
        quote.SaleAdjustments.push(saleAdj);
        self.EditSaleAdjustment(saleAdj);
    };
    self.AddTaxRate = function (quote) {
        var saleAdj = HFC.Models.SaleAdjustment({ TypeEnum: SaleAdjTypeEnum.Tax, Category: "Base Tax", JobId: quote.JobId, QuoteId: quote.QuoteId, Template: "sales-adj-edit" }, quote);
        quote.SaleAdjustments.push(saleAdj);
        self.EditSaleAdjustment(saleAdj);
    }
    self.EditSaleAdjustment = function (sa_model) {
        ko.editable(sa_model);
        sa_model.beginEdit();
        sa_model.Template("sales-adj-edit");
    }

    self.ResetSaleAdjustment = function (sa_model, quote) {
        if (sa_model.SaleAdjustmentId <= 0 && quote)
            quote.SaleAdjustments.remove(sa_model);

        sa_model.rollback();
        sa_model.Template("sales-adj-view")
    }
    self.DeleteSaleAdjustment = function (sa_model, quote) {
        if (quote) {
            if (sa_model.SaleAdjustmentId) {
                HFC.AjaxDelete('/api/quotes/' + quote.QuoteId + '/saleadjustment', { SaleAdjustmentId: sa_model.SaleAdjustmentId, QuoteLastUpdated: quote.LastUpdated.toISOString() },
                    function (result) {
                        quote.LastUpdated = new XDate(result.LastUpdated, true);
                        quote.SaleAdjustments.remove(sa_model);
                        HFC.DisplaySuccess(sa_model.Category() + " deleted");
                    });
            } else {
                quote.SaleAdjustments.remove(sa_model);
            }
        }
    }

    self.SaveSaleAdjustment = function (jitem, quote) {
        if (self.IsBusy())
            return;

        var method = 'PUT',
            data = jitem.toJS();
        
        if (quote)
            data.QuoteLastUpdated = quote.LastUpdated.toISOString();
        else {
            HFC.DisplayAlert("Unable to verify job date");
            return;
        }

        self.IsBusy(true);
        if (jitem.SaleAdjustmentId <= 0)
            method = "POST";
        
        HFC.Ajax(method, '/api/quotes/' + jitem.QuoteId + '/saleadjustment', data, function (result) {
            if (result && result.SaleAdjustmentId > 0 && jitem.SaleAdjustmentId <= 0)
                jitem.SaleAdjustmentId = result.SaleAdjustmentId;
            quote.LastUpdated = new XDate(result.LastUpdated, true);
            jitem.LastUpdated = new XDate(result.LastUpdated, true);
            jitem.commit();
            jitem.Template("sales-adj-view");
            HFC.DisplaySuccess(jitem.Category() + " saved");
        }).always(function () {
            self.IsBusy(false);
        });
    }

    //##################################################################### END REGION

    //##################################################################### region Line Item Methods 

    self.AddLineItem = function (quote) {
        var ji = HFC.Models.JobItem(null, quote);
        ji.JobId = quote.JobId;
        ji.QuoteId = quote.QuoteId;
        quote.JobItems.push(ji);
        self.EditLineItem(ji);
    }
    self.EditLineItem = function (jitem) {
        ko.editable(jitem);
        jitem.beginEdit();
        jitem.Template('item-edit');
    }
    self.CopyLineItem = function (jitem, quote) {
        if (quote) {
            var clone = jitem.Clone();
            clone.JobItemId = 0;
            clone = HFC.Models.JobItem(clone, quote);

            quote.JobItems.push(clone);
            self.EditLineItem(clone);
        } else
            HFC.DisplayAlert("Sorry, we're unable to copy line item.  Please try adding a new line item instead.");
    }
    self.DeleteLineItem = function (jitem, quote) {
        if (quote) {
            if (jitem.JobItemId > 0 && confirm("Do you wish to continue?")) {
                HFC.AjaxDelete('/api/quotes/' + jitem.QuoteId + '/jobitems', { QuoteLastUpdated: quote.LastUpdated.toISOString(), JobItemId: jitem.JobItemId, LastUpdated: jitem.LastUpdated.toISOString() },
                    function (result) {
                        quote.LastUpdated = new XDate(result.LastUpdated, true);
                        quote.JobItems.remove(jitem);
                        HFC.DisplaySuccess("Job item deleted");
                    });
            } else if (jitem.JobItemId <= 0) {
                //its a new line item that for some reason failed to save, we can simply just remove it from the collection
                //or if we decide to include a delete/remove button while in edit mode where the line item doesn't have a JobItemId
                quote.JobItems.remove(jitem);
            }
        }
    }

    self.SaveLineItem = function(jitem, quote, alsoAddNew) {
        if (self.IsBusy())
            return;

        var method = 'PUT',
            data = jitem.toJS();

        if (quote)
            data.QuoteLastUpdated = quote.LastUpdated.toISOString();
        else {
            HFC.DisplayAlert("Unable to verify quote's last updated date");
            return;
        }

        self.IsBusy(true);
        if (jitem.JobItemId <= 0)
            method = "POST";

        HFC.Ajax(method, '/api/quotes/' + jitem.QuoteId + '/jobitems', data, function (result) {
            if (result && result.JobItemId > 0 && jitem.JobItemId <= 0)
                jitem.JobItemId = result.JobItemId;
            quote.LastUpdated = new XDate(result.LastUpdated, true);
            jitem.LastUpdated = new XDate(result.LastUpdated, true);
            jitem.commit();
            jitem.Template("item-view");
            HFC.DisplaySuccess("Job item saved");
            if (alsoAddNew) 
                self.AddLineItem(quote);            
        }).always(function () {
            self.IsBusy(false);
        });
    };
    
    self.ResetLineItem = function (jitem, quote) {
        if (jitem.JobItemId <= 0 && quote) {
            quote.JobItems.remove(jitem);
        }
        jitem.rollback();
    }

    //######################################################## END REGION
}