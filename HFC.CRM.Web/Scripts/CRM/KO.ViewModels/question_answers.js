﻿HFC.VM.QA = function (collection, jobid) {
    var self = this;

    self.JobId = jobid;
    var ansArr = $.map(LK_Questions, function (q) {        
        if (collection) {
            var ansarr = $.grep(collection, function (a) {
                return a.QuestionId == q.QuestionId;
            });
            if (ansarr && ansarr.length) {                
                q.AnswerId = ansarr[0].AnswerId;
                q.Answer = ansarr[0].Answer;
            }
        }        
        return new HFC.Models.Question(q);
    });

    self.Collection = ko.observableArray(ansArr);

    self.UniqueQuestions = ko.computed({
        read: function () {
            var questArray = $.map(LK_Questions, function (qa) {
                return qa.Question;
            });
            return $.grep(questArray, function (el, index) {
                return index == $.inArray(el, questArray);
            });
        }, deferEvaluation: true
    });   

    self.GetQuestions = function (question) {
        var q = $.grep(self.Collection(), function (qa) {
            return qa.Question == question;
        });
        return q;
    }
    function _save(model, answer) {
        var method = "POST",            
            data = {
                QuestionId: model.QuestionId,
                Answer: answer
            };
        if (model.AnswerId) {
            method = "PUT";
            data.AnswerId = model.AnswerId;
        }
        HFC.Ajax(method, '/api/jobs/' + self.JobId + '/qanda', data,
            function (result) {
                if (!model.AnswerId && result && result.AnswerId)
                    model.AnswerId = result.AnswerId;
                if (data.Answer != model.Answer())
                    model.Answer(data.Answer);
                HFC.DisplaySuccess(model.SubQuestion + " saved successfully");
            });
    }
    self.ToggleCheckbox = function (model) {
        //send subquestion as answer if it is not checked, we will only check it if the answer saved successfully
        _save(model, model.IsChecked() ? null : model.SubQuestion);
    }
    self.Save = function (model, jsEvent) {
        _save(model, model.Answer());
    }
}