﻿HFC.VM.FranchiseRoyalties = function (id, territories, royaltyProfiles) {
    var self = this;
    self.FranchiseId = id;
    
    self.Territories = territories;
    self.RoyaltyProfiles = royaltyProfiles;

    self.Royalties = ko.observableArray();
    self.NextLevels = ko.observableArray();

    self.TableItems = ko.computed(function() {
        var result = [];
        ko.utils.arrayForEach(self.Royalties(), function (territory) {
            result.push({ territoryTitle: territory.Name + ' - ' + territory.Code, territoryId: territory.TerritoryId, root: true, hasNextLevel: self.NextLevels()[territory.TerritoryId] || false });
            ko.utils.arrayForEach(territory.FranchiseRoyalties, function(royalty) {
                result.push({
                    territoryName: territory.Name,
                    territoryId: territory.TerritoryId,
                    royalty: royalty,
                    canDelete: royalty.Type_Royalty.isDependant,
                    root: false,
                });
            });
        });

        return result;
    });

    self.SelectedTerritory = ko.observable();
    self.SelectedRoyaltyProfile = ko.observable();
    self.SelectedDate = ko.observable();

    self.EditingItem = ko.observable(null);
    
    self.deleteRoyalty = function(item) {
        if (confirm("Are You sure?")) {
            HFC.AjaxBasic('DELETE', '/api/FranchiseRoyalties/' + item.royalty.RoyaltyId, null, function (response) {
                getTerritories();
                HFC.DisplaySuccess("Royalty deleted");
            });
        }
    }

    self.editRoyalty = function (item) {

        self.EditingItem({
            RoyaltyId: item.royalty.RoyaltyId,
            Name: ko.observable(item.royalty.Name),
            Amount: ko.observable(item.royalty.Amount),
            StartOnUtc: ko.observable(item.royalty.StartOnUtc),
            EndOnUtc: ko.observable(item.royalty.EndOnUtc)
        });
    }

    self.saveRoyalty = function (item) {
        var it = ko.toJS(self.EditingItem());
        if (it.StartOnUtc > it.EndOnUtc) {
            HFC.DisplayAlert("Action cancelled. End date greater start date");
            return;
        }

        if (it.StartOnUtc) {
            it.StartOnUtc = moment(it.StartOnUtc).format('YYYY-MM-DD');
        }
        if (it.EndOnUtc) {
            it.EndOnUtc = moment(it.EndOnUtc).format('YYYY-MM-DD');
        }
    
        HFC.AjaxBasic('PUT', '/api/FranchiseRoyalties', JSON.stringify(it), function (response) {
            getTerritories();
        });
        self.EditingItem(null);
    }
    
    self.applyRoyaltyTemplate = function() {
        if (!confirm("Are You sure")) {
            return;
        }

        HFC.AjaxBasic('POST', '/api/FranchiseRoyalties/' + self.modalData.SelectedTerritory() + '/ApplyTemplate', JSON.stringify({ TerritoryId: self.modalData.SelectedTerritory(), RoyaltyProfileId: self.modalData.SelectedRoyaltyProfile(), StartDate: moment(self.modalData.SelectedDate()).format('YYYY-MM-DD') }), function (response) {
            getTerritories();
            self.modalVisible(false);
        });
    }

    self.addRoyaltyLevel = function (item) {
        self.addRoyaltyLevelShowModal(item);
    }

    self.cancelEdit = function (item) {
        self.EditingItem(null);
    }

    /* Apply Royalty Template Modal */

    self.modalVisible = ko.observable(false);
    
    self.showModal = function () {
        self.modalVisible(true);
    };

    self.headerLabel = ko.observable('Apply Royalty Template');
    self.bodyTemplate = ko.observable('modalTemplate');

    self.modalData = {
        SelectedTerritory: self.SelectedTerritory,
        SelectedRoyaltyProfile: self.SelectedRoyaltyProfile,
        SelectedDate: self.SelectedDate,

        Territories: self.Territories,
        RoyaltyProfiles: self.RoyaltyProfiles
    };
    
    self.modalApply = function () {
        self.applyRoyaltyTemplate();
    };


    /* Add Royalty Level Modal */

    self.addRoyaltyLevelModalVisible = ko.observable(false);

    self.addRoyaltyLevelShowModal = function (item) {
        HFC.AjaxBasic('GET', '/api/FranchiseRoyalties/' + item.territoryId + '/GetNextRoyaltyLevel', {}, function (response) {
            self.addRoyaltyLevelModalData.territoryId = item.territoryId;
            self.addRoyaltyLevelModalData.NextRolyaltyLevelName(response.nextRoyaltyName);
            self.addRoyaltyLevelModalVisible(true);
        });
    };

    self.addRoyaltyLevelModalData = {
        territoryId: 0,
        SelectedMonthes: ko.observable(1),
        SelectedAmount: ko.observable(1),
        NextRolyaltyLevelName: ko.observable()
    };

    self.addRoyaltyLevelHeaderLabel = ko.observable('Add Next Royalty Level');
    self.addRoyaltyLevelBodyTemplate = ko.observable('addRoyaltyLevelModalTemplate');
    
    self.addRoyaltyLevelModalAction = function () {
        HFC.AjaxBasic('POST', '/api/FranchiseRoyalties/' + self.addRoyaltyLevelModalData.territoryId + '/AddNextRoyaltyLevel', JSON.stringify({
            monthes: self.addRoyaltyLevelModalData.SelectedMonthes(),
            amount: self.addRoyaltyLevelModalData.SelectedAmount()
        }), function (response) {
            getTerritories();
            self.addRoyaltyLevelModalVisible(false);
        });
    };

    var getTerritories = function() {
        HFC.AjaxBasic('GET', '/api/FranchiseRoyalties/' + self.FranchiseId(), null, function(response) {
            self.Royalties(response.Royalties);
            self.NextLevels(response.NextLevels);
        });
    }

    getTerritories();
}