﻿HFC.VM.EditModal = function () {
    var self = this;

    self.IsBusy = ko.observable();
    self.ContainerId = "edit-modal";
    self.Show = function () {
        self.IsBusy(false);
        $("#"+self.ContainerId).modal("show");
    }
    self.Hide = function () {
        $("#" + self.ContainerId).modal("hide");
    }
    self.Title = ko.observable();
    self.EditableModel = ko.observable();    

    //placeholder, must override by the calling VM if you want to add logic
    self.Save = function () {

    }
    //placeholder, must override by the calling VM if you want to add logic
    self.Reset = function () {

    }
}