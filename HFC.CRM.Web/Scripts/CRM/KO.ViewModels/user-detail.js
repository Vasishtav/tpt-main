﻿//depends on HFC.Models.User
HFC.VM.UserDetail = function (user) {
    var self = this,
        checkPasswordValidity = null,
        password1 = document.getElementById('password1'),
        password2 = document.getElementById('password2');

    self.IsBusy = ko.observable();
    self.User = HFC.Models.User(user);

    if (password1) {
        checkPasswordValidity = function () {
            if (password1.value != password2.value) {
                password1.setCustomValidity('Passwords must match.');
            } else {
                password1.setCustomValidity('');
            }
            return password1.value == password2.value;
        };

        password1.addEventListener('change', checkPasswordValidity, false);
        password2.addEventListener('change', checkPasswordValidity, false);
    }

    self.PreviousRolesId = ko.observableArray();
    self.Permission_Click = function (item, name) {
        if (name == "Installer" || name == "Sales") {
            if (self.PreviousRolesId.indexOf(item) != -1) {
                if (self.User.RoleIds.indexOf(item) == -1) {
                    HFC.AjaxGetJson('/api/users/' + self.User.UserId + '/CheckUserPermissionUsed/', { permissionName: name }, function (result) {
                        if (result.str != '') {
                            if (!confirm(result.str)) {
                                self.User.RoleIds.push(item);
                                self.PreviousRolesId(self.User.RoleIds.slice(0));
                            }

                            //bootbox.dialog({
                            //    message: result.str,
                            //    title: "Warning",
                            //    buttons: {
                            //        danger: {
                            //            label: "Yes",
                            //            className: "btn-danger",
                            //            callback: function () {

                            //            }
                            //        },
                            //        main: {
                            //            label: "No",
                            //            className: "btn-primary",
                            //            callback: function () {
                            //                self.User.RoleIds.push(item);
                            //                //Example.show("Primary button");
                            //            }
                            //        }
                            //    }
                            //});
                        }
                    });
                }
            }
        }
        self.PreviousRolesId(self.User.RoleIds.slice(0));
    }

    self.SaveProfile = function (form) {
        var validPass = true,
            method = "PUT",
            url = '/api/users',
            data = self.User.toBaseJS();

        if (password1 && checkPasswordValidity)
            validPass = checkPasswordValidity();

        var whichusername = self.User.UserName();
        var whichusername1 = self.User.Person.PrimaryEmail;

        if (self.User.UserName() != null) {
            if (!self.User.IsADUser() && !isValidEmail(self.User.UserName())) {
                HFC.DisplayAlert("Provide email address as UserName.");
                $('#TestEmail').focus();
                return false;
            }
        } else
        {
            if (!self.User.IsADUser() && !isValidEmail(self.User.Person.PrimaryEmail())) {
                HFC.DisplayAlert("Provide email address as User ID.");
                $('#TestEmail').focus();
                return false;
            }
        }

        if (!self.User.IsADUser && !validPass)
            return false;

        self.IsBusy(true);
        if (self.User.PersonId == null || self.User.PersonId <= 0)
            method = "POST";
        else
            url += "/" + self.User.UserId;
        var jsonString = JSON.stringify(data);
        console.log(jsonString);

        HFC.AjaxBasic(method, url, jsonString, function (success) {
            if (location.href.toLowerCase().indexOf("/settings/users") >= 0)
                window.location.reload();
            else {
                HFC.DisplaySuccess("User profile saved");
                if (success && success.UserId && self.User.UserId != success.UserId)
                    self.User.UserId = success.UserId;
                $(form).parentsUntil(".modal").parent().modal("hide");
            }
        }).complete(function () {
            self.IsBusy(false);
        });
    }

    self.ResetProfile = function () {
        //nothing special here, using data-dismiss for modal dismiss/reset
    };


    function isValidEmail(email) {
        var pattern = /^[a-z0-9!#$%&"'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;;
        return pattern.test(email);
    };
}

//General code for retrieving elements in a form submit
//var data = {},
//    keyval = [];
//$.each(form.elements, function (i, e) {
//    if (e.name && e.value && (e.type != "checkbox" || (e.type == "checkbox" && e.checked))) {
//        var kv = $.grep(keyval, function (keyvalel) {
//            return keyvalel.name == e.name;
//        });
//        if (kv && kv.length) {
//            if (typeof kv[0].value != "object")
//                kv[0].value = [kv[0].value];
//            kv[0].value.push(e.value)
//        } else
//            keyval.push({ name: e.name, value: e.value });
//    }
//});
//$.each(keyval, function (i, e) {
//    if (typeof e.value == "object")
//        $.each(e.value, function (ix, v) { data[e.name + '[' + ix + ']'] = v });
//    else
//        data[e.name] = e.value;
//});

//@ sourceURL=user-detail.js



$(document).ready(function () {
    $("#TestEmail").keyup(function () {
        if ($('#TestCheckBox').is(":checked")) {
            var test = $("#TestEmail").val();
            if (test.indexOf("@") != -1) {
                $("#TestEmail").val(test.substring(0, test.indexOf("@")));
            }
        }
    });
    $("#TestEmail").focusout(function () {
        if ($('#TestCheckBox').is(":checked")) {
            var test = $("#TestEmail").val();
            if (test.indexOf("@") != -1) {
                $("#TestEmail").val(test.substring(0, test.indexOf("@")));
            }
        }
    });
})
