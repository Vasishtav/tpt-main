﻿HFC.VM.Users = function(users) {
    var self = this;
        
    users = users || HFC.EmployeesJS;
    
    self.Collection = ko.observableArray(users && HFC.Models.User ? $.map(users, function (e) {
        return HFC.Models.User(e);
    }) : null);

    self.SalesUsers = ko.computed(function () {
        return $.grep(self.Collection(), function (usr) {
            return usr.InSales == true;
        });
    });

    self.InstallUsers = ko.computed(function () {
        return $.grep(self.Collection(), function (usr) {
            return usr.InInstall == true;
        });
    });

    self.OtherUsers = ko.computed(function () {
        return $.grep(self.Collection(), function (usr) {
            return usr.InSales == false && usr.InInstall == false;
        });
    });

    self.SetCurrentUserFirstLoad = ko.computed(function () {
        for (var i = 0; i < self.Collection().length; i++) {
            if (self.Collection()[i].PersonId == HFC.CurrentUser.PersonId)
            {
                self.Collection()[i].IsSelected(true);
            }
        }
    })

    self.SelectedUserIds = ko.computed(function () {
        var ids = $.map(self.Collection(), function (usr) {
            return usr.IsSelected() ? usr.PersonId : null;
        });
        //if ($.inArray(HFC.CurrentUser.PersonId, ids) < 0)
        //    ids.push(HFC.CurrentUser.PersonId);
        return ids;
    });
   
    self.AllSalesSelected = false;
    self.AllInstallerSelected = false;
    self.AllOthersSelected = false;
    self.SelectAllInstallers = function (model, jsevent) {
        $.each(self.InstallUsers(), function (i, u) {
            u.IsSelected(!self.AllInstallerSelected);
        });
        self.AllInstallerSelected = !self.AllInstallerSelected;
        _refreshEvent();
        jsevent.stopPropagation(); //so drop down menu doesnt close right away
    }

    self.SelectAllSales = function (model, jsevent) {
        $.each(self.SalesUsers(), function (i, u) {
            u.IsSelected(!self.AllSalesSelected);
        });
        self.AllSalesSelected = !self.AllSalesSelected;
        _refreshEvent();
        jsevent.stopPropagation(); //so drop down menu doesnt close right away
    }

    self.SelectAllOthers = function (model, jsevent) {
        $.each(self.OtherUsers(), function (i, u) {
            u.IsSelected(!self.AllOthersSelected);
        });
        self.AllOthersSelected = !self.AllOthersSelected;
        _refreshEvent();
        jsevent.stopPropagation(); //so drop down menu doesnt close right away
    }

    self.SelectUser = function (model, jsevent) {
        model.IsSelected(!model.IsSelected());
        _refreshEvent();
        jsevent.stopPropagation(); //so drop down menu doesnt close right away
    }

    self.SelectSalesUser = function (model, jsevent) {
        //$.each(self.SalesUsers(), function (i, u) {
        //    u.IsSelected(false)
        //});

        if (model.IsSelected() == true) {
            model.IsSelected(false);
        }
        else {
            model.IsSelected(true);
        }
        _refreshEvent();
        jsevent.stopPropagation(); //so drop down menu doesnt close right away
    }

    self.SelectInstallUser = function (model, jsevent) {
        //$.each(self.InstallUsers(), function (i, u) {
        //    u.IsSelected(false)
        //});

        if (model.IsSelected() == true) {
            model.IsSelected(false);
        }
        else {
            model.IsSelected(true);
        }

        _refreshEvent();
        jsevent.stopPropagation(); //so drop down menu doesnt close right away
    }

    self.SelectOtherUser = function (model, jsevent) {
        //$.each(self.OtherUsers(), function (i, u) {
        //    u.IsSelected(false)
        //});

        if (model.IsSelected() == true) {
            model.IsSelected(false);
        }
        else {
            model.IsSelected(true);
        }
        _refreshEvent();
        jsevent.stopPropagation(); //so drop down menu doesnt close right away
    }
    
    self.DeleteUser = function (model, jsEvent) {
        if (model.UserId) {
            HFC.AjaxGetJson('/api/users/' + model.UserId + '/CheckUserInUsed', null, function (result) {
                bootbox.dialog({
                    message: result.confirmStr,
                    title: "Warning",
                    buttons: {
                        danger: {
                            label: "Yes, delete",
                            className: "btn-danger",
                            callback: function () {
                                HFC.AjaxDelete('/api/users/' + model.UserId, null, function (result) {
                                    var $tar = jsEvent.tagName ? $(jsEvent) : $(jsEvent.currentTarget);
                                    $tar.parentsUntil("tr").parent().remove();
                                    var text = (model.FullName || "User") + " deleted";
                                    HFC.DisplaySuccess(text);
                                });
                            }
                        },
                        main: {
                            label: "Cancel",
                            className: "btn-default",
                            callback: function () {
                                //Example.show("Primary button");
                            }
                        }
                    }
                });
            });
        }
    }

    function _refreshEvent() {
        if (HFC.Calendar) {
            $("#" + HFC.Calendar.Id).fullCalendar("refetchEvents");
        }
    }
}