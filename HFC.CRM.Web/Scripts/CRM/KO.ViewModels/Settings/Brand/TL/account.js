﻿
HFC.VM.Account = function (leadStatuses, jobStatuses, taxrules, questions, products, productCategories, questionCategories) {
    var self = this;
    var i = 11;

    self.isBusy = ko.observable(false);
  
    self.Manufacturers = ko.observableArray([]);
    self.ProductMargins = ko.observableArray([]);
  
    var loadingColors = false;
    
    self.Colors = ko.observableArray([]);


    HFC.Ajax("GET", '/api/color', {}, function (result) {
        self.Colors(result);
    });


    HFC.Ajax("GET", '/api/manufacturer', {}, function (result) {
        var ar = [];
        for (var i = 0; i < result.length; i++) {
            if (result[i].isCustom == false) {
                ar.push(result[i])
            }
        }
        self.Manufacturers(ar);
    });


    self.ManufacturerContact = ko.observable();

    self.SelectedManufacturer = ko.observable();

    self.CommonMultiplier = ko.observable();


    self.ColorsCategory = ko.observable([]);

    self.CategoryColors = ko.observableArray([]);

    self.ApplyCommonMultiplier = function () {
        if (!confirm("Are You sure?")) {
            return;
        }

        var items = self.ProductMargins();
        for (var j = 0; j < items.length; j++) {
            items[j].Multiplier = self.CommonMultiplier();
        }

        self.ProductMargins([]);
        self.ProductMargins(items);

        self.isBusy(true);

        HFC.AjaxBasic('PUT', '/api/productMargin', JSON.stringify(items), function (result) {
            self.isBusy(false);
            self.CommonMultiplier('');
        });
    };

    self.MultiplierBlur = function (e) {
        self.isBusy(true);
        HFC.Ajax("POST", '/api/productMargin', e, function (result) {
            self.isBusy(false);
        });
    }

    self.SelectedManufacturer.subscribe(function () {
        if (!self.SelectedManufacturer()) return;
        self.isBusy(true);
        HFC.Ajax("GET", '/api/manufacturer/' + self.SelectedManufacturer(), {}, function (result) {
            self.ManufacturerContact(result);

            HFC.Ajax("GET", '/api/productMargin/' + self.SelectedManufacturer(), {}, function (result) {
                self.ProductMargins(result);
                self.isBusy(false);
            });

            self.isBusy(false);

            $('#subnav a:first').tab('show');
        });
    });

    self.SaveManufacturerContact = function () {
        var model = self.ManufacturerContact();
        model.ManufacturerId = self.SelectedManufacturer();
        self.isBusy(true);
        HFC.Ajax("POST", '/api/manufacturer', model, function (result) {
            self.isBusy(false);
            HFC.DisplaySuccess("Contact has been updated");
        });
    };


    
    self.CategoryColors.subscribe(function () {
        if (!self.ColorsCategory() || loadingColors) return;
        HFC.Ajax("PUT", '/api/color/' + self.ColorsCategory(), { colors: self.CategoryColors() }, function (result) {
            
        });
    });

    self.SelectAllColors = function() {
        var colors = self.Colors();
        var ids = [];
        for (var j = 0; j < colors.length; j++) {
            ids.push(colors[j].NamedColorId);
        }

        self.CategoryColors(ids);
    }

    self.UnselectAllColors = function () {
        self.CategoryColors([]);
    }

    self.ColorsCategory.subscribe(function() {
        if (self.ColorsCategory()) {
            loadingColors = true;
            self.CategoryColors([]);
            HFC.Ajax("GET", '/api/color/' + self.ColorsCategory(), {}, function(result) {
                for (var j = 0; j < result.length; j++) {
                    self.CategoryColors.push(result[j].NamedColorId);
                }

                loadingColors = false;
            });
        } else {
            self.CategoryColors([]);
            loadingColors = true;
        }
    });

    self.NewColorName = ko.observable();

    self.AddNewColor = function() {
        if (self.ColorsCategory()) {
            HFC.Ajax("POST", '/api/color', { colorName: self.NewColorName() }, function (result) {
                self.Colors.push({ NamedColorId: result.Id, NamedColor: self.NewColorName() });
                self.NewColorName('');
                self.CategoryColors.push(result.Id);
            });
        }
    }
    //self.QuestionCategories = ko.observableArray([{ name: 'Interested in' }, { name: 'Other Information' }]);
    self.QuestionCategories = ko.observableArray(questionCategories);

    self.QuestionSelectionTypes = ko.observableArray([{ name: 'checkbox', id: 'checkbox' }, { name: 'textbox', id: 'textbox' }]);

    self.Questions = ko.observableArray(questions ? $.map(questions, function (q) {
        return HFC.Models.Question(q);
    }) : null);

    self.GetSubQuestions = function (question) {
        var p = self.Questions();

        var result = $.grep(self.Questions(), function (s) {
            return s.Question === question;
        });
        
        return result;
    }
    
    self.LeadStatuses = ko.observableArray(leadStatuses ? $.map(leadStatuses, function (s) {
        return HFC.Models.Generic(s);
    }) : null);

    self.ChildLeadStatuses = function (parentId) {
        return $.grep(self.LeadStatuses(), function (s) {
            return s.ParentId == parentId;
        });
    }
    self.JobStatuses = ko.observableArray(jobStatuses ? $.map(jobStatuses, function (s) {
        return HFC.Models.Generic(s);
    }) : null);

    self.ChildJobStatuses = function (parentId) {
        return $.grep(self.JobStatuses(), function (s) {
            return s.ParentId == parentId;
        });
    }

    self.Products = ko.observableArray(products ? $.map(products, function (p) {
        return HFC.Models.Product(p);
    }) : null);

    
    self.ProductCategories = ko.observableArray(productCategories);
    
    self.GetProductsByCategory = function (categoryId) {
        return $.grep(self.Products(), function (s) {
            return s.CategoryId == categoryId;
        });
    }
    self.TaxType = ko.observable("Base");

    self.RuleName = ko.observable("Base Tax");
    self.Percent = ko.observable("Percent");

    self.TaxType.subscribe(function (taxTypeVal) {
        
        if (taxTypeVal == "Base") {
            self.RuleName('Base Tax');
        } else if(taxTypeVal =="Custom")
            self.RuleName('AFS');
        else if (taxTypeVal == "City")
            self.RuleName('City');
        else if (taxTypeVal == "County")
            self.RuleName('County');
        self.Percent('');
    });

    self.Cancel = function (model) {
        model.rollback();
    }
    self.Edit = function (model) {
        
        if (!model.beginEdit)
            ko.editable(model);
        model.beginEdit();
        model.InEdit(true);
    }

    self.SaveTax = function (formOrModel) {
        var taxrule = {},
            method = "POST";
        if (formOrModel.tagName) {
            var type = formOrModel.elements["TaxType"].value;
            //we will use case to set only city fields, and include county if there is a value there
            switch(type){
                case "City": 
                    if (formOrModel.elements["ZipCode"])
                        taxrule.ZipCode = formOrModel.elements["ZipCode"].value;                    
                    if (formOrModel.elements["City"])
                        taxrule.City = formOrModel.elements["City"].value;
                    if (formOrModel.elements["State"])
                        taxrule.State = formOrModel.elements["State"].value;
                case "County":
                    if (formOrModel.elements["County"])
                        taxrule.County = formOrModel.elements["County"].value;
                default: break;
            }
            if (type == "City" && !taxrule.ZipCode) {            
                HFC.DisplayAlert("A zip or postal code is required");
                return;            
            }
            else if (type == "County" && !taxrule.County) {
                HFC.DisplayAlert("A county is required");
                return;
            }
            if (formOrModel.elements["Percent"])
                taxrule.Percent = formOrModel.elements["Percent"].value;

            //It should be updated in all situations:
            taxrule.RuleName = formOrModel.elements["RuleName"].value;
            if (type == "Base")
                taxrule.IsBaseTax = true;

        } else {
            taxrule.TaxRuleId = formOrModel.TaxRuleId;
            taxrule.County = formOrModel.County();
            taxrule.ZipCode = formOrModel.ZipCode();
            taxrule.City = formOrModel.City();
            taxrule.State = formOrModel.State();
            taxrule.Percent = formOrModel.Percent();
            taxrule.RuleName = formOrModel.RuleName();
        }
        if (taxrule.TaxRuleId)
            method = "PUT";
        HFC.Ajax(method, '/api/tax', taxrule, function (result) {
            if (result && result.TaxRuleId && taxrule.TaxRuleId != result.TaxRuleId) 
                taxrule.TaxRuleId = result.TaxRuleId;                
            if (method == "POST")
                self.TaxRules.push(HFC.Models.Tax(taxrule));
            else {
                formOrModel.InEdit(false);
                formOrModel.commit();
            }
            
            if(method == "POST")
                HFC.DisplaySuccess("Tax rule added");
            else if(method == "PUT")
                HFC.DisplaySuccess("Tax rule Modified");
        });
    }
    self.DeleteTax = function(model) {
        if(confirm("Do you want to continue?") && model.TaxRuleId) {
            HFC.AjaxDelete('/api/tax/'+model.TaxRuleId, null, function(){
                HFC.DisplaySuccess("Tax rule deleted");
                self.TaxRules.remove(model);
            });
        }
    }

    self.TaxRules = ko.observableArray(taxrules ? $.map(taxrules, function (t) {
        return HFC.Models.Tax(t);
    }) : null);

    self.SaveLeadStatus = function (form) {
        var method = "POST",
            model = this,
            data = {};

        if (model.Id) {
            data.Id = model.Id;
            data.Name = model.Name();
            data.ParentId = model.ParentId;
            method = "PUT";
        } else {
            data.Name = form.elements["Name"].value;
            data.ParentId = $(form.elements["ParentId"]).val();
        }
        HFC.Ajax(method, '/api/leadstatus', data, function (result) {
            localStorage.clear();

            if (result && result.Id && result.Id != data.Id)
                data.Id = result.Id;
            if (method == "POST") {
                data.FranchiseId = -1; //set any number so ko.binding can display the edit/delete buttons
                self.LeadStatuses.push(HFC.Models.Generic(data));
                HFC.DisplaySuccess("Lead status added");
            } else {
                model.InEdit(false);
                model.commit();
                HFC.DisplaySuccess("Lead status updated");
            }
        });
    }

    self.DeleteLeadStatus = function(model) {
        if (confirm("Do you want to continue?") && model.Id) {
            HFC.AjaxDelete('/api/leadstatus/' + model.Id, null, function (result) {
                HFC.DisplaySuccess("Lead status deleted");
                self.LeadStatuses.remove(model);
            });
        }
    }

    self.SaveJobStatus = function (form) {
        var method = "POST",
            model = this,
            data = {};

        if (model.Id) {
            data.Id = model.Id;
            data.Name = model.Name();
            data.ParentId = model.ParentId;
            method = "PUT";
        } else {
            data.Name = form.elements["Name"].value;
            data.ParentId = $(form.elements["ParentId"]).val();
        }

        HFC.Ajax(method, '/api/jobstatus', data, function (result) {
            if (result && result.Id && result.Id != data.Id)
                data.Id = result.Id;
            if (method == "POST") {
                data.FranchiseId = -1; //set any number so ko.binding can display the edit/delete buttons
                self.JobStatuses.push(HFC.Models.Generic(data));
                HFC.DisplaySuccess("Job status added");
            } else {
                model.InEdit(false);
                model.commit();
                HFC.DisplaySuccess("Job status updated");
            }
        });
    }

    self.DeleteJobStatus = function (model) {
        if (confirm("Do you want to continue?") && model.Id) {
            HFC.AjaxDelete('/api/jobstatus/' + model.Id, null, function (result) {
                localStorage.clear();

                HFC.DisplaySuccess("Job status deleted");
                self.JobStatuses.remove(model);
            });
        }
    }


    self.SaveQuestion = function (form) {
        var method = "POST",
            model = this,
            data = {};

        if (model.QuestionId) {
            data.QuestionId = model.QuestionId;
            data.Question = model.Question;
            data.SubQuestion = model.SubQuestion;
            data.SelectionType = model.SelectionType;
            method = "PUT";
        } else {
            data.Question = form.elements["Question"].value;
            data.SubQuestion = form.elements["SubQuestion"].value;
            data.SelectionType = form.elements["SelectionType"].value;

            $(form).find("select[name*='Question']").val('');
            $(form).find("input[name*='SubQuestion']").val('');
            $(form).find("select[name*='SelectionType']").val('');
            
        }
        HFC.Ajax(method, '/api/question', data, function (result) {
            if (result && result.Id && result.Id != data.QuestionId)
                data.QuestionId = result.Id;
            if (method == "POST") {
                data.FranchiseId = -1; //set any number so ko.binding can display the edit/delete buttons
                self.Questions.push(HFC.Models.Generic(data));
                HFC.DisplaySuccess("Question added");

                $(form).find("select[name*='Question']").val('');
                $(form).find("input[name*='SubQuestion']").val('');
                $(form).find("select[name*='SelectionType']").val('');

            } else {
                model.InEdit(false);
                model.commit();
                HFC.DisplaySuccess("Question updated");
            }
        });
    }

    self.DeleteQuestion = function (model) {
        if (confirm("Do you want to continue?") && model.QuestionId) {
            HFC.AjaxDelete('/api/question/' + model.QuestionId, null, function (result) {
                HFC.DisplaySuccess("Question deleted");
                self.Questions.remove(model);
            });
        }
    }

    self.SaveProduct = function (form) {
        var method = "POST",
            model = this,
            data = {};
        if (model.ProductId) {
            data.ProductId = model.ProductId;
            data.ProductType = model.ProductType;
            method = "PUT";
        } else {
            data.CategoryId = form.elements["CategoryId"].value;
            data.ProductType = form.elements["ProductType"].value;

            $(form).find("select[name*='CategoryId']").val('');
            $(form).find("input[name*='ProductType']").val('');
        }

        HFC.Ajax(method, '/api/product', data, function (result) {
            if (result && result.Id && result.Id != data.ProductId)
                data.ProductId = result.Id;
            if (method == "POST") {
                data.FranchiseId = -1; //set any number so ko.binding can display the edit/delete buttons
                self.Products.push(HFC.Models.Generic(data));
                HFC.DisplaySuccess("Product added");

                $(form).find("select[name*='CategoryId']").val('');
                $(form).find("input[name*='ProductType']").val('');

            } else {
                model.InEdit(false);
                model.commit();
                HFC.DisplaySuccess("Product updated");
            }
        });
    }

    self.DeleteProduct = function (model) {
        if (confirm("Do you want to continue?") && model.ProductId) {
            HFC.AjaxDelete('/api/product/' + model.ProductId, null, function (result) {
                HFC.DisplaySuccess("Product deleted");
                self.Products.remove(model);
            });
        }
    }
}