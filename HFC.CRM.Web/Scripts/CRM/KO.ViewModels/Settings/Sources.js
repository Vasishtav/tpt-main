﻿HFC.VM.Sources = function (sources) {
    var self = this;

    self.rawSources = sources;
    
    self.Sources = ko.observableArray(sources ? $.map(sources, function (s) {
        HFC.Models.Campaign(s);
        return s;
    }) : null);


    self.RootSources = ko.computed(function() {
        var result = $.grep(self.Sources(), function (s) {
            return !s.ParentId && !s.isCampaign;
        });

        var level = 1;

        for (var i = 0; i < result.length; i++) {
            result[i].level = level;
        }

        return result;
    });

    self.ChildSources = function (parentId) {
        var result = $.grep(self.Sources(), function (s) {
            return s.ParentId == parentId && !s.isCampaign;
        });

        var level = self.GetLevel(parentId, 0);

        for (var i = 0; i < result.length; i++) {
            result[i].level = level;
        }
        
        return result;
    }

    self.GetLevel = function(parentId, level) {
        level++;

        var found = $.grep(self.Sources(), function (s) {
            return s.SourceId == parentId && !s.isCampaign;
        });

        if (!found || found.length == 0) {
            return level;
        } else {

            return self.GetLevel(found[0].ParentId, level);
        }
    }

    self.GetChildCampaigns = function (parentId) {
        
        var campaings = $.grep(self.Sources(), function (s) {
            return s.ParentId == parentId && s.isCampaign;
        });

        return campaings;
    }

    self.EditableCampaign = ko.observable();
    
    self.Cancel = function (model) {
        model.rollback();
    }

    self.Edit = function (model) {
        if (!model.beginEdit)
            ko.editable(model);
        model.beginEdit();
        model.InEdit(true);
    }

    self.NewCampaign = function (src) {
        self.EditCampaign(HFC.Models.Campaign({ ParentId: src.SourceId, StartDate: new XDate() }));
    }

    self.EditCampaign = function (campaign) {
        self.EditableCampaign(campaign);
        if (!self.EditableCampaign.beginEdit)
            ko.editable(self.EditableCampaign);
        self.EditableCampaign.beginEdit();
    }


    self.SaveCampaign = function (form) {
        var method = "POST",
            url = '/api/leadsources',
            model = self.EditableCampaign(),
            data = {};

        if (model.SourceId) {
            data.SourceId = model.SourceId;
            method = "PUT";
        }

        data.ParentId = model.ParentId;
        data.StartDate = moment(model.StartDate()).format('YYYY-MM-DD');
        data.EndDate = moment(model.EndDate()).format('YYYY-MM-DD') ;
        data.Amount = model.Amount();
        data.Name = HFC.htmlEncode(model.Name());
        data.IsActive = model.IsActive();
        data.isCampaign = true;
        data.Memo = HFC.htmlEncode(model.Memo());            
        
        HFC.Ajax(method, url, data, function (result) {
            if (result && result.Id && result.Id != data.SourceId)
                data.SourceId = result.Id;

            self.EditableCampaign.commit();
            if (method == "POST") {
                
                self.Sources.push(HFC.Models.Campaign(data));
               
                
                HFC.DisplaySuccess("Campaign added");
            } else {
                HFC.DisplaySuccess("Campaign updated");
            }

            localStorage.clear();

            $("#sourceModal").modal("hide");
        });
    }


    self.DeleteCampaign = function (campaign) {
        var src = $.grep(self.Sources(), function (s) {
            return s.SourceId == campaign.SourceId;
        });
        if (src && src.length && confirm("Do you want to continue?")) {
            HFC.AjaxDelete('/api/leadsources/' + campaign.SourceId, null, function (result) {
                HFC.DisplaySuccess("Campaign deleted");
                self.Sources.remove(campaign);
                localStorage.clear();
            });
        }
    }

    self.SaveSource = function (form) {
        var method = "POST",
            model = this,
            data = {};

        if (model.SourceId) {
            data.SourceId = model.SourceId;
            data.Name = model.Name();
            data.ParentId = model.ParentId;
            method = "PUT";
        } else {
            data.Name = form.elements["Name"].value;
            data.ParentId = $(form.elements["ParentId"]).val();
        }

        HFC.Ajax(method, '/api/leadsources', data, function (result) {

            localStorage.clear();

            if (result && result.Id && result.Id != data.Id)
                data.SourceId = result.Id;
            if (method == "POST") {
                data.FranchiseId = -1; //set any number so ko.binding can display the edit/delete buttons
                HFC.Models.Generic(data);
                self.Sources.push(data);
                HFC.DisplaySuccess("Source added");

                form.elements["Name"].value = "";
                $(form.elements["ParentId"]).val("");
            } else {
                model.InEdit(false);
                model.commit();
                HFC.DisplaySuccess("Source updated");
            }
        });
    }
    self.DeleteSource = function (model) {
        if (confirm("Do you want to continue?") && model.SourceId) {
            HFC.AjaxDelete('/api/leadsources/' + model.SourceId, null, function (result) {
                HFC.DisplaySuccess("Source deleted");
                self.Sources.remove(model);

                localStorage.clear();
            });
        }
    }

}