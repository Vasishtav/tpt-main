﻿HFC.VM.Notes = function (notesCollection, postUrl) {
    var self = this;

    self.PostUrl = postUrl;
    self.Collection = ko.observableArray(notesCollection ? $.map(notesCollection, function (n) {
        n.TypeEnum = ko.observable(n.TypeEnum);
        n.CreatedOnUtc = new XDate(n.CreatedOnUtc, true);
        return n;
    }) : null);
    
    self.UpdateNoteType = function (note, typeenum) {
        if (typeenum != window.NoteTypeEnum.History) {
            HFC.AjaxPut(self.PostUrl, { NoteId: note.NoteId, TypeEnum: typeenum }, function () {
                note.TypeEnum(typeenum);
                HFC.DisplaySuccess("Note type successfully updated");
            });
        }
    }
    self.AddNotes = function (form) {
        if (self.PostUrl) {
            var type = parseInt(form.elements["NoteType"].value);
            var msg = $.trim(form.elements["Message"].value);
            if (msg && msg.length) {
                HFC.AjaxPost(self.PostUrl, {
                    TypeEnum: type,
                    Message: HFC.htmlEncode(msg)
                }, function (result) {
                    self.Collection.splice(0, 0, { NoteId: result.NoteId || 0, AvatarSrc: HFC.CurrentUser.AvatarSrc, TypeEnum: ko.observable(type), Message: HFC.htmlEncode(msg), CreatedByPersonId: HFC.CurrentUser.PersonId, CreatorFullName: HFC.CurrentUser.FullName, CreatedOnUtc: new XDate() });
                    form.elements["Message"].value = "";
                });
            }
        }
    }
}