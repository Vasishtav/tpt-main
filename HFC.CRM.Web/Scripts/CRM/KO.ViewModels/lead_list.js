﻿
// Can be deleted -- murugan, march 12, 2019
///*
//* This will handle the interaction on the lead list, such as changing lead status
//* Sample usage: viewModel.Leads = new HFC.VM.LeadList();  or new HFC.VM.LeadList(rawCollection);
//*/
//HFC.VM.LeadList = function (dateRange, lead_statusid, searchTerm, searchFilter, leadsCollection) {
//    var self = this;

//    self.IsBusy = ko.observable();
//    self.Collection = ko.observableArray(leadsCollection ? $.map(leadsCollection, function (l) {
//        return HFC.Models.Lead(l);
//    }) : null);            
    
//    self.ToggleJobs = function (model, jsEvent) {
//        var $el = $(jsEvent.currentTarget);
//        if (!model.JobRowEnabled()) {
//            HFC.AjaxGetJson('/api/jobs/' + model.LeadId, null, function (result) {
//                var jobs = $.map(result, function (j) {
//                    return HFC.Models.Job(j);
//                });
//                model.Jobs(jobs);
//                model.JobRowEnabled(true); //one time enable so it doesnt have to re-render every time you toggle

//                $el.find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
//                $(".row_" + model.LeadId).toggleClass("active");
//                $(".row_job_" + model.LeadId).toggle();
//            });
//        } else {
//            $el.find(".glyphicon").toggleClass("glyphicon-plus").toggleClass("glyphicon-minus");
//            $(".row_" + model.LeadId).toggleClass("active");
//            $(".row_job_" + model.LeadId).toggle();
//        }
//    }

//    self.decodeHtmlEntity = function (str) {
//        if (str) {
//            return str.replace(/&#(\d+);/g, function (match, dec) {
//                return String.fromCharCode(dec);
//            });
//        }
//        return null;
//    };

//    self._applyFilter = function () {
//        if (self.SelectedDate() == null && (self.StartDate.Value == null || !self.StartDate.Value.valid()))
//            HFC.DisplayAlert("Please pick a date to filter");
//        else if (!self.IsBusy()) {
//            self.IsBusy(true);
//            var data = { orderByDirection: self.SortIsDesc() ? "Desc" : "Asc", orderBy: self.SortBy() };
//            if (self.LeadStatusIds() && self.LeadStatusIds().length) {
//                $.each(self.LeadStatusIds(), function (i, s) {
//                    data['leadStatusIds[' + i + ']'] = s;
//                });
//            }
//            if (self.StartDate())
//                data.createdOnUtcStart = moment(self.StartDate.Value[0]).format('YYYY-MM-DD')
//            if (self.EndDate())
//                data.createdOnUtcEnd = moment(self.EndDate.Value[0]).format('YYYY-MM-DD')
//            if (self.SearchTerm())
//                data.searchTerm = self.decodeHtmlEntity(self.SearchTerm());
//            if (self.SearchFilter())
//                data.searchFilter = self.SearchFilter();

//            data.pageIndex = self.PageIndex();
//            data.pageSize = self.PageSize();
//            if (self.LeadStatusIds().length != 0 || data.createdOnUtcStart || data.createdOnUtcEnd || data.searchTerm || data.searchFilter || self.SelectedDate()) {
//                var myParams = {
//                    leadStatusIds: self.LeadStatusIds(), createdOnUtcStart: data.createdOnUtcStart,
//                    createdOnUtcEnd: data.createdOnUtcEnd, searchTerm: data.searchTerm,
//                    searchFilter: data.searchFilter, selectedDate: self.SelectedDate()
//                };
//                var pr = jQuery.param(myParams, false);
//                History.replaceState(myParams, "leadSearch", "?" + pr);
//            }
//            else {
//                History.replaceState(null, "leadSearch", "/leads/");
//            }

//            HFC.AjaxGetJson('/api/leads', data,
//                function (result) {

//                    self.Collection([]);
//                    if (result) {
//                        self.TotalRecords(result.TotalRecords);
//                        if (result.TotalRecords > 0) {
//                            if (result.LeadCollection) {
//                                var leads = $.map(result.LeadCollection, function (item) {
//                                    return HFC.Models.Lead(item);
//                                });
//                                self.Collection(leads);
//                            }
//                        }
//                    } else
//                        self.TotalRecords(0);

//                    //this will allow dropdowns to be dropup if it is too far down at bottom that the menu would require scrolling
//                    $("#crm-list-table .dropdown-toggle").on("click", function (e) {
//                        if (e.clientY > window.innerHeight / 2)
//                            $(this).parent().addClass("dropup")
//                        else
//                            $(this).parent().removeClass("dropup")
//                    })
//                }
//            ).complete(function () { self.IsBusy(false) });
//        }
//    }

//    self.GetURLParameter = function (sParam) {
//        var sPageURL = window.location.search.substring(1);
//        var data = $.parseParams(window.location.href);
//        if (data) {
//            return data[sParam];
//        }
//    };

//    HFC.Ext.Filter(self, self.GetURLParameter("selectedDate"), searchTerm, searchFilter, false, self.GetURLParameter("createdOnUtcStart"), self.GetURLParameter("createdOnUtcEnd"));
   
//    self.LeadStatusIds = ko.observableArray(lead_statusid ? [lead_statusid.toString()] : self.GetURLParameter("leadStatusIds"));

//    self.ExportExcel = function () {
//        if (self.SelectedDate() == null && (self.StartDate.Value == null || !self.StartDate.Value.valid()))
//            HFC.DisplayAlert("Please pick a date to filter");
//        else {
//            var data = {};
//            if (self.LeadStatusIds())
//                data.leadStatusIds = self.LeadStatusIds();
//            if (self.StartDate())
//                data.createdOnUtcStart = self.StartDate();
//            if (self.EndDate())
//                data.createdOnUtcEnd = self.EndDate();
//            if (self.SearchTerm())
//                data.SearchTerm = self.SearchTerm();
//            window.open('/export/leads?' + $.param(data), '_blank');
//        }
//    }

//    self.ApplyFilter = function () {
//        self.PageIndex(0);
//        self._applyFilter();
//    };

//    self.DeleteLead = function (lead) {
//        if (!confirm("Do you want to continue?")) return;
//        HFC.AjaxDelete('/api/leads/' + lead.LeadId, null, function (result) {
//            self.ApplyFilter();
//            HFC.DisplaySuccess("Lead successfully deleted");
//        });
//    }
//}