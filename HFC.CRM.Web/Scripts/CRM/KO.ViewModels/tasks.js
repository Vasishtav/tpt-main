﻿HFC.VM.Tasks = function (collection, jobId) {
    var self = this;

    self.JobId = jobId;
    self.IsBusy = ko.observable();
    self.Collection = ko.observableArray(collection ? $.map(collection, function(c){
        return HFC.Models.Task(c);
    }) : null);

    ////this is for the little checkbox that user can quickly toggle and we save immediately
    //self.MarkComplete = function (model, jsevent) {
    //    var currentState = model.IsCompletedChecked();
    //    HFC.AjaxGet('/api/tasks/' + model.id() + '/mark', { IsComplete: !currentState },
    //        function () {
    //            model.IsCompletedChecked(!currentState); //only mark it as completed if ajax was successful
    //        }
    //    );
    //    return false; //do not bubble up, this will prevent the checkbox from checking with "checked: IsCompletedChecked" binding
    //};

    self.PageIndex = ko.observable(0);
    self.PageSize = ko.observable(15);
    self.TotalRecords = ko.observable();

    self.TotalPage = ko.computed(function () {
        if (self.TotalRecords())
            return Math.ceil(self.TotalRecords() / self.PageSize());
        else
            return 0;
    });

    self.IsPrevDisabled = ko.computed(function () {
        return self.TotalPage() == 0 || self.PageIndex() == 0;
    });
    self.IsNextDisabled = ko.computed(function () {
        return self.TotalPage() == 0 || self.PageIndex() == self.TotalPage() - 1;
    });

    self.ItemsRange = ko.computed(function () {
        if (self.TotalRecords() && self.TotalRecords() > 0) {
            var lastCount = ((self.PageIndex() + 1) * self.PageSize());
            if (lastCount > self.TotalRecords())
                lastCount = self.TotalRecords();
            return (self.PageIndex() * self.PageSize() + 1) + " - " + lastCount;
        } else
            return "";
    });

    self.Remove = function (model) {
        self.Collection.remove(function (item) {
            return item.id() == model.id();
        });
        var total = self.TotalRecords() - 1;
        if (total <= 0)
            total = null;
        self.TotalRecords(total);
    };

    self.fetchData = function (pgIndex) {
        if (!self.IsBusy()) {
            self.IsBusy(true);
            var ids = [HFC.CurrentUser.PersonId],
                data = { pageIndex: self.PageIndex(), pageSize: self.PageSize() };
            if (HFC.KO && HFC.KO.Users)
                ids = HFC.KO.Users.SelectedUserIds();
            if (self.JobId > 0)
                data.JobIds = self.JobId;
            else {
                if (ids && ids.length)
                    data.personIds = ids;
                else {
                    self.Collection([]);
                    return;
                }
            }
            
            HFC.AjaxGetJson('/api/tasks', data,
                function (result) {
                    if (result) {
                        if (result.TotalRecords)
                            self.TotalRecords(result.TotalRecords);
                        else
                            self.TotalRecords(null);

                        if (result.TaskData) {
                            var events = $.map(result.TaskData, function (task) {
                                return HFC.Models.Task(task);
                            });
                            self.Collection(events);
                        }
                    }
                }
            ).always(function () { self.IsBusy(false); });
        }
    }

    self.GoToPrev = function () {
        if (!self.IsBusy() && self.PageIndex() > 0) {
            self.PageIndex(self.PageIndex() - 1);
            self.fetchData();
        }
    };

    self.GoToNext = function () {
        if (!self.IsBusy() && self.PageIndex() < self.TotalPage()) {
            self.PageIndex(self.PageIndex() + 1);
            self.fetchData();
        }
    };

    self.RemoveCompleted = function () {
        if (confirm("This will only clear completed tasks ASSIGNED TO YOU, do you want to continue?")) {
            HFC.AjaxDelete('/api/tasks', null, function () {
                self.PageIndex(0);
                self.fetchData();
                //TODO: refresh calendar list of tasks?
            });
        }
    }

    self.LoadPopover = function (model, jsevent) {
        HFC.Popover.KO.Load(model, jsevent.currentTarget, jsevent.clientX, jsevent.clientY);
    }
    //set up hide popover on scroll in panel-body
    $(".tasks-panel .panel-body").on("scroll", function () {
        $("#" + HFC.Popover.ContainerId).hide();
    });

}