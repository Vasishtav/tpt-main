﻿HFC.VM.Franchise = function (id) {
    var self = this;
    self.FranchiseId = id;

    self.saveFranchise = function (model, success) {
       var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/saveFranchise';
       HFC.AjaxBasic('POST', _url, JSON.stringify(model), function (result) {
           if (success) {
               success(result);
           }
           else
           {
               HFC.DisplayAlert(response.statusText);
           }
       });
    }
    self.saveTerritories = function (model, success) {
        var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/saveTerritories';
        HFC.AjaxBasic('POST', _url, JSON.stringify(model), function (result) {
            if (success) {
                success(result);
            }
        });
    }
    self.saveTerritory = function (model, success) {
        var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/saveTerritory';
        HFC.AjaxBasic('POST', _url, JSON.stringify(model), function (result) {
            if (success) {
                success(result);
            }
        });
    }

    self.checkTerritory = function (model, success) {
        var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/checkTerritory';
        HFC.AjaxBasic('POST', _url, JSON.stringify(model), function (result) {
            if (success) {
                success(result);
            }
        });
    }

    self.deleteTerritory = function (model, success) {
        var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/deleteTerritory';
        HFC.AjaxBasic('POST', _url, JSON.stringify(model), function (result) {
            if (success) {
                success(result);
            }
        });
    }
    self.saveAddress = function (model, success) {
        var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/saveAddress';
        HFC.AjaxBasic('POST', _url, JSON.stringify(model), function (result) {
            if (success) {
                success(result);
            }
        });
    }
    self.saveOwner = function (model, success) {
        var franId = model.FranchiseId;

        var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/saveOwner';
        HFC.AjaxBasic('POST', _url, JSON.stringify(model), function (result) {
            if (success) {
                success(result);
            }
        });
    }

    self.saveToSolatech = function (model, success) {
        var franId = model.FranchiseId;
        //HFC.AjaxBasic('POST', '/api/Franchise/' + franId + '/addtosolatech');
        var _url = HFC.Util.BaseURL() + '/api/Franchise/' + franId + '/addtosolatech';
        HFC.AjaxBasic('POST', _url, null, success)
        //function (result) {
        //model.IsSolaTechEnabled = !model.IsSolaTechEnabled;
        //}
        //);
    }

    self.saveToQB = function (model, success) {
        var franId = model.FranchiseId;
        var _url = HFC.Util.BaseURL() + '/api/Franchise/' + franId + '/addtoqb';
        HFC.AjaxBasic('POST', _url, null, success);
    }

    self.deactivate = function (id, success) {
        var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/deactivate/' + id;
        HFC.AjaxBasic('POST', _url,  null, function (result) {
            if (success) {
                success(result);
            }
        });
    }

    self.removeSuspended = function (id, success) {
        var _url = HFC.Util.BaseURL() + '/controlpanel/franchises/removeSuspended/' + id;
        HFC.AjaxBasic('POST', _url, null, function (result) {
            if (success) {
                success(result);
            }
        });
    }

    self.reset = function () {
        alert('ResetOwnerPerson HFC.VM.Franchise  ');
        var debughere = this;
    }

    self.Edit = function (model) {
        ko.editable(model);
        model.beginEdit();
    }

}