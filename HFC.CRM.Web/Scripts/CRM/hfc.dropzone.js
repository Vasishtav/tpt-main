﻿HFC.LoadFileInfo = function(file) {
	var desc = file.description;
	var $descEl = $(".dz-description", file.previewElement);
	$("p", $descEl).text(desc || "No description");
	$(file.previewElement).find(".dz-creator").text("[" + (new XDate(file.createdOn).toString("M/d/yy h:mm tt")) + "] " + file.creatorFullName);
	$(file.previewElement).find(".dz-details").attr("href", file.Url);
	$(file.previewElement).find(".dz-details img")
        .hoverIntent(function () {
        	$descEl.show();
        }, function () {
        	$descEl.hide();
        });
}
HFC.DropzoneCollection = [];

Dropzone.autoDiscover = false;
ko.bindingHandlers.dropzone = {
	init: function (element, valueAccessor, allBindings, viewModel) {
		setTimeout(function () {
			var uploadDropzone = new Dropzone("#file_" + viewModel.JobId,
			{
				paramName: "file", // The name that will be used to transfer the file
				maxFilesize: 10, // MB
				addRemoveLinks: true,
				thumbnailWidth: 120,
				thumbnailHeight: 120,
				dictRemoveFileConfirmation: "This will permanently remove the file, do you want to continue?",
				acceptedFiles: "image/*,application/pdf,.eps,.tiff,.doc,.docx,.html,.htm,.txt,.csv,.xls,xlsx",
				previewTemplate: '<div class="dz-preview dz-file-preview"><a target="_blank" class="dz-details" style="display:block"><div class="dz-filename"><span data-dz-name></span></div><div class="dz-size" data-dz-size></div><img data-dz-thumbnail /></a><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-success-mark"><span>✔</span></div><div class="dz-error-mark"><span>✘</span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div>',
				init: function () {
					var self = this;
					self.on("addedfile", function (file) {
						var desc = prompt("Please enter a description for this file.");
						element.elements["Description"].value = desc;
					});
					self.on("success", function (file, response) {						
						var desc = element.elements["Description"].value;
						file.fileId = response.FileId;
						file.description = desc;
						file.createdOn = new Date();
						file.creatorFullName = HFC.CurrentUser.FullName;
						file.Url = response.Url;
						file.ThumbUrl = response.ThumbUrl;								
						element.elements["Description"].value = "";
						$(".dz-details", element.previewElement).attr("href", file.Url);
						HFC.DisplaySuccess(file.name + " uploaded");
						$(file.previewElement).popover({
							trigger: 'hover',
							title: file.creatorFullName + ' - ' + new XDate(file.createdOn).toString("M/d/yy h:mm tt"),
							content: file.description || "No description",
							placement: 'auto',
							animation: !HFC.Browser.isMobile()
						});
					});
					self.on("removedfile", function (file) {
						var fileId = file.fileId;
						if (fileId && fileId > 0) {
							HFC.AjaxDelete('/jobs/file/' + viewModel.JobId, { fileId: fileId },
								function () {
									$(file.previewElement).popover('destroy');
									if (uploadDropzone.files.length == 0)
										$(".dz-message", element).show();
									HFC.DisplaySuccess(file.name + " deleted");
								}).error(function () {
									self.options.addedfile.call(self, file); //add it back if it failed deletion for any reason
									self.options.thumbnail.call(self, file, file.ThumbUrl || $(file.previewTemplate).find(".dz-details").attr("href")); //add it back if it failed deletion for any reason								
								});
						}
					});
				}
			});
			var filesArr = ko.utils.unwrapObservable(valueAccessor());
			if (filesArr.length) {
			    $(".dz-message", element).hide();
			    $.each(filesArr, function (i, file) {
					var mockFile = {
						fileId: file.PhysicalFileId,
						name: file.FileName,
						size: file.FileSize || 0,
						ThumbUrl: file.ThumbUrl,
						Url: file.UrlPath,
						description: file.FileDescription,
						creatorFullName: file.AddedByFullName,
						createdOn: new XDate(file.CreatedOnUtc)
					};
					uploadDropzone.options.addedfile.call(uploadDropzone, mockFile);
					$(mockFile.previewElement).popover({
						trigger: 'hover',
						title: mockFile.creatorFullName + ' - ' + new XDate(mockFile.createdOn).toString("M/d/yy h:mm tt"),
						content: mockFile.description || "No description",
						placement: 'auto',
						animation: !HFC.Browser.isMobile()
					});
					$(".dz-details", element.previewElement).attr("href", mockFile.Url);
					if (file.MimeType && file.MimeType.indexOf("image") >= 0)
						uploadDropzone.options.thumbnail.call(uploadDropzone, mockFile, mockFile.ThumbUrl);
				});
			}
			HFC.DropzoneCollection.push(uploadDropzone);
		}, 0);
	}
};

