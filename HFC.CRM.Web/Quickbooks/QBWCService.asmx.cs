﻿//using HFC.CRM.Data;
//using HFC.CRM.Managers.Quickbooks;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text.RegularExpressions;
//using System.Web;
//using System.Web.Services;

//namespace HFC.CRM.Web.Quickbooks
//{
//    /// <summary>
//    /// Quickbooks WebConnector web service for handling CRM sync with Quickbooks
//    /// </summary>
//    [WebService(
//         Namespace = "http://developer.intuit.com/",
//         Name = "QBWCService",
//         Description = "Quickbooks WebConnector web service for handling CRM sync with Quickbooks")]
//    public class QBWCService : System.Web.Services.WebService
//    {        
//        /// <summary>        
//        /// This web method verifies the requesting QBWC version matches this web service allowed QB version
//        /// </summary>
//        /// <returns>
//        /// Possible values: 
//        /// - NULL or string.Empty = QBWC will let the web service update
//        /// - "E:<any text>" = popup ERROR dialog with <any text> 
//        ///					- abort update and force download of new QBWC.
//        /// - "W:<any text>" = popup WARNING dialog with <any text> 
//        ///					- choice to user, continue update or not.
//        ///	</returns>
//        [WebMethod(Description = "This web method verifies the requesting QBWC version matches this web service allowed QB version")]
//        public string clientVersion(string strVersion)
//        {
//            Logger.WriteEntry("Webmethod: clientVersion() called", new Dictionary<string, string> { { "strVerson", strVersion } }, ipAddress: Context.Request.UserHostAddress);
//            string retVal = null;
//            double recommendedVersion = 2.0;
//            double supportedMinVersion = 2.0;
//            double suppliedVersion = Convert.ToDouble(this.parseForVersion(strVersion));
//            if (suppliedVersion < supportedMinVersion)
//            {
//                retVal = "E:You need to upgrade your QBWebConnector";
//            }
//            else if (suppliedVersion < recommendedVersion)
//            {
//                retVal = "W:We recommend that you upgrade your QBWebConnector";
//            }
//            Logger.WriteEntry("Webmethod: clientVersion() finished", new Dictionary<string, string> { { "retVal(string)", retVal } }, ipAddress: Context.Request.UserHostAddress);            
//            return retVal;
//        }


//        /// <summary>
//        /// To verify username and password for the web connector that is trying to connect
//        /// </summary>
//        /// <returns>
//        /// Possible values: 
//        /// string[0] = ticket
//        /// string[1]
//        /// - "" = use current company file
//        /// - "none" = no further request/no further action required
//        /// - "nvu" = not valid user
//        /// - "use this string as the company file path" = use this company file
//        /// string[2] - (optional) contains the number of seconds to wait before 
//        /// the next update.
//        /// string[3] - (optional) contains the number of seconds to be used as the 
//        /// MinimumRunEveryNSeconds time.
//        /// 
//        /// The third and fourth elements allow you to to reduce QBWC updates during 
//        /// peak activity, basically telling QBWC clients not to update so frequently 
//        /// (the third element) or permanently resetting the minimum update time at the 
//        /// QBWC client (the fourth element).
//        /// </returns>
//        [WebMethod(Description = "This web method facilitates verify username and password for the web connector that is trying to connect to this service.")]
//        public string[] authenticate(string strUserName, string strPassword)
//        {
//            Logger.WriteEntry("Webmethod: authenticate() called", new Dictionary<string, string> { { "strUserName", strUserName } });
//            string[] authReturn = new string[2];
//            try
//            {
//                string ticket = null;
//                var qbApp = Authenticator.Authenticate(strUserName, strPassword, Context.Request.UserHostAddress);
//                if (qbApp != null)
//                {
//                    var sessionTicket = SessionPool.Instance.put(qbApp.AppId, Context.Request.UserHostAddress);
//                    authReturn[0] = sessionTicket.SessionId.ToString();

//                    // Use currently open QB company file or company file path                    
//                    // CompanyFilePath is required if user wants to run WebConnector without opening Quickbooks
//                    if (QBManager.HaveAnyWork(sessionTicket))
//                        authReturn[1] = qbApp.CompanyFilePath ?? ""; 
//                }
//                else
//                {
//                    authReturn[0] = "";
//                    authReturn[1] = "nvu";
//                }

//                Logger.WriteEntry("Webmethod: authenticate() finished",
//                    new Dictionary<string, string> { { "authReturn[0]", authReturn[0] }, { "authReturn[1]", authReturn[1] } },
//                    ticket,
//                    Context.Request.UserHostAddress);
//            }
//            catch (UnauthorizedAccessException e)
//            {
//                authReturn[0] = "";
//                authReturn[1] = "nvu";
//                Logger.WriteEntry(e, ipAddress: Context.Request.UserHostAddress); 
//            }
//            catch (Exception e2)
//            {
//                authReturn[0] = "";
//                authReturn[1] = "none";
//                Logger.WriteEntry(e2, ipAddress: Context.Request.UserHostAddress);
//            }
            
//            return authReturn;
//        }

//        /// <summary>
//        /// To facilitate capturing of QuickBooks error and notifying it to web services        
//        /// </summary>
//        /// <param name="ticket">A GUID based ticket string to maintain identity of QBWebConnector </param>
//        /// <param name="message">An error message corresponding to the HRESULT</param>
//        /// <param name="hresult">An HRESULT value thrown by QuickBooks when trying to make connection</param>
//        /// <returns>
//        /// Possible values: 
//        /// - "done" = no further action required from QBWebConnector
//        /// - any other string value = use this name for company file</returns>
//        [WebMethod(Description = "This web method facilitates web service to handle connection error between QuickBooks and QBWebConnector")]
//        public string connectionError(string ticket, string hresult, string message)
//        {
//            var errmsg = new Dictionary<string, string> { { "hresult", hresult }, { "message", message } };
//            Logger.WriteEntry("Webmethod: connectionError() called", errmsg, ticket, Context.Request.UserHostAddress);
//            var session = SessionPool.Instance.get(ticket);

//            // We must not get into an infinte loop, so we count our errors
//            int ce_counter = 0;     
//            if (session != null)
//            {
//                ce_counter = Util.CastObject<int?>(session.Properties["ce_counter"]) ?? 0;
//            }

//            string retVal = interpretHResult(hresult, message, ce_counter);

//            if (session != null)
//            {
//                session.Properties["ce_counter"] = ce_counter + 1;
//                session.LastErrorMsg = string.Join("&", errmsg.Select(s => string.Format("{0}={1}", s.Key, s.Value)));
//                SessionPool.Instance.put(session);
//            }

//            Logger.WriteEntry("Webmethod: connectionError() finished", new Dictionary<string, string> { { "retVal", retVal } }, ticket, Context.Request.UserHostAddress);
//            return retVal;
//        }

//        /// <summary>
//        /// At the end of a successful update session, QBWebConnector will call this web method.
//        /// </summary>
//        [WebMethod(Description = "This web method allows web service to send any summary information at the end of a successful update session.")]
//        public string closeConnection(string ticket)
//        {
//            Logger.WriteEntry("Webmethod: closeConnection() called", new Dictionary<string, string> { { "ticket", ticket } }, ticket, Context.Request.UserHostAddress);            

//            string retVal = "Session closed";
//            SessionPool.Instance.pop(ticket);

//            Logger.WriteEntry("Webmethod: closeConnection() finished", new Dictionary<string, string> { { "retVal", retVal } }, ticket, Context.Request.UserHostAddress);
//            return retVal;
//        }
        
//        /// <summary>
//        /// Facilitates sending information about the last error occured during the update process.        
//        /// </summary>
//        /// <returns>Error message describing last web service error</returns>
//        [WebMethod(Description = "This web method allows web service to send information about the last error occured during the update process.")]
//        public string getLastError(string ticket)
//        {
//            Logger.WriteEntry("Webmethod: getLastError() called", sessionId: ticket);
//            QBSession session = SessionPool.Instance.get(ticket);
//            string retVal = "Error! session does not exist or has expired.";
//            if(session != null)
//                retVal = session.LastErrorMsg;
//            //if (controller.getInteractive(sess) == "NEEDED")
//            //{
//            //    retVal = "Interactive mode";
//            //}
//            //else
//            //{
//            //    retVal = "Error!";
//            //}
            
//            Logger.WriteEntry("Webmethod: getLastError() finished", new Dictionary<string, string> { { "retVal", retVal } }, ticket, Context.Request.UserHostAddress);
//            return retVal;
//        }

//        /// <summary>
//        /// QBWebConnector will call this web method every 60 seconds for service to indicate if interactive session is completed by user.        
//        /// </summary>
//        /// <returns>
//        ///     - DONE = QBWC responds by calling closeConnection()
//        ///     - CONTINUE = QBWC responds by calling sendRequestXML() and resuming the normal sequence
//        ///     - <empty string> = QBWC waits another 60 seconds (default wait time before interactiveDone() call) and checks again
//        ///     - <any other string> = QBWC presents the error, closeConnection() is called
//        /// </returns>
//        [WebMethod(Description = "This web method facilitates web service to let QBWC know if interactive session has ended or not.")]
//        public string isInteractiveDone(string ticket)
//        {
//            //Logger.WriteEntry("Webmethod: isInteractiveDone() called", sessionId: ticket);

//            //string retVal = null, status = null;
//            ////Check status of ecommdb->interactive->interactiveMode 
//            ////Respond with retVal based on status of interactiveMode
//            //if (sess != null)
//            //{
//            //    status = controller.getInteractive(sess);
//            //}
//            //if (status == "DONE")
//            //{
//            //    //Clear the record from database and move on
//            //    controller.removeInteractive(sess);
//            //    retVal = "CONTINUE";
//            //}
//            //else if (status == "CANCELLED" || status == "TIMEDOUT")
//            //{
//            //    //No need to proceed with this update
//            //    controller.removeInteractive(sess);
//            //    retVal = "User cancelled this update session or it timedout.";
//            //}
//            //else
//            //{
//            //    //Keep interactive session going.
//            //    retVal = "";
//            //}
//            //sessionPool.put(ticket, sess);
//            //Logger.WriteEntry("Webmethod: isInteractiveDone() finished", new Dictionary<string, string> { { "retVal", retVal } });
//            //return retVal;

//            //we're not implementing interactive mode so return DONE
//            return "DONE";
//        }
        
//        /// <summary>
//        /// QBWebConnector will call this web method to notify the service if interactive session was rejected and why.
//        /// </summary>
//        /// <returns>
//        ///     - DONE = QBWC responds by calling closeConnection()
//        ///     - CONTINUE = QBWC responds by calling sendRequestXML() and resuming the normal sequence
//        ///     - <empty string> = QBWC waits another 60 seconds (default wait time before interactiveDone() call) and checks again
//        ///     - <any other string> = QBWC presents the error, closeConnection() is called
//        /// </returns>
//        [WebMethod(Description = "This web method facilitates web service to know if QBWC rejected this interactive session and why.")]
//        public string interactiveRejected(string ticket, string reason)
//        {
//            //logEnter("interactiveRejected()", new string[,] { { "wcTicket", wcTicket }, { "reason", reason } });
//            //Session sess = sessionPool.get(wcTicket);
//            //string retVal = null, status = null;
//            ////Checking status of ecommdb->interactive->interactiveMode 
//            ////Responding with retVal based on status of interactiveMode
//            //if (sess != null) controller.getInteractive(sess);
//            //if (status == "DONE")
//            //{
//            //    //Clearing the record from database and move on
//            //    if (sess != null) controller.removeInteractive(sess);
//            //    retVal = "CONTINUE";
//            //}
//            //else if (status == "CANCELLED" || status == "TIMEDOUT")
//            //{
//            //    //No need to proceed with this update
//            //    if (sess != null) controller.removeInteractive(sess);
//            //    retVal = "User cancelled this update session or it timedout.";
//            //}
//            //else
//            //{
//            //    //Keeping interactive session going.
//            //    retVal = "";
//            //}
//            //sessionPool.put(wcTicket, sess);
//            //logExit("interactiveRejected()", new string[,] { { "retVal", retVal } });
//            //return retVal;

//            //we're not using interactive mode so return DONE
//            return "DONE";
//        }

//        /// <summary>
//        /// QBWebConnector will call this web method when service indicates that it wants to go interactive.
//        /// To go interactive, service has to send empty string for sendRequestXML() and then return "Interactive mode" for getLastError()
//        /// </summary>
//        /// <returns>string url to render in the interactive dialog</returns>
//        [WebMethod(Description = "This web method facilitates web service to send url to QBWC to launch during interactive mode.")]
//        public string getInteractiveURL(string ticket, string sessionID)
//        {
//            //logEnter("getInteractiveURL()", new string[,] { { "wcTicket", wcTicket }, { "sessionID", sessionID } });
//            //Session sess = sessionPool.get(wcTicket);
//            //controller.putInteractive(sess, sessionID);
//            //string retVal = null, cState = null;
//            //if (sess != null) cState = sess.getProperty("lastControllerState").ToString();
//            //if (cState == "Preflight")
//            //{
//            //    retVal = "http://localhost/WCECommSample/service/Preflight.aspx?ticket=" + wcTicket;
//            //}
//            //if (cState == "Postflight")
//            //{
//            //    retVal = "http://localhost/WCECommSample/service/Postflight.aspx?ticket=" + wcTicket + "&sessionID=" + sessionID;
//            //}
//            //sessionPool.put(wcTicket, sess);
//            //logExit("getInteractiveURL()", new string[,] { { "retVal", retVal } });
//            //return retVal;
//            return "";
//        }
        
//        /// <summary>
//        /// This web method facilitates web service to send request XML to QuickBooks via QBWebConnector
//        /// </summary>
//        /// <returns>
//        /// - "any_string" = Request XML for QBWebConnector to process
//        /// - "" = No more request XML 
//        /// </returns>
//        [WebMethod(Description = "This web method facilitates web service to send request XML to QuickBooks via QBWebConnector")]
//        public string sendRequestXML(string ticket, string strHCPResponse, string strCompanyFileName, string qbXMLCountry, int qbXMLMajorVers, int qbXMLMinorVers)
//        {
//            Logger.WriteEntry("Webmethod: sendRequestXML() called", 
//                new Dictionary<string,string> { { "strHCPResponse", strHCPResponse}, 
//                                                { "strCompanyFile", strCompanyFileName }, 
//                                                { "qbXMLCountry", qbXMLCountry }, 
//                                                { "qbXMLMajorVers", qbXMLMajorVers.ToString() }, 
//                                                { "qbXMLMinorVers", qbXMLMinorVers.ToString() } },
//                ticket,
//                Context.Request.UserHostAddress);

//            QBSession session = SessionPool.Instance.get(ticket);
//            string request = "";

//            session.MajorVersion = qbXMLMajorVers;
//            session.MinorVersion = qbXMLMinorVers;
//            session.Country = qbXMLCountry;
//            session.CompanyFileName = strCompanyFileName;

//            //request = controller.getNextAction(session);
//            SessionPool.Instance.put(session);

//            Logger.WriteEntry("Webmethod: sendRequestXML() finished", new Dictionary<string, string> { { "request", request } }, ticket, Context.Request.UserHostAddress);
//            return request;
//        }

//        /// <summary>
//        /// This web method facilitates web service to receive response XML from QuickBooks via QBWebConnector
//        /// </summary>
//        /// <returns>
//        /// Greater than zero  = There are more request to send
//        /// 100 = Done. no more request to send
//        /// Less than zero  = Custom Error codes
//        /// </returns>
//        [WebMethod(Description = "This web method facilitates web service to receive response XML from QuickBooks via QBWebConnector")]
//        public int receiveResponseXML(string ticket, string response, string hresult, string message)
//        {
//            Logger.WriteEntry("Webmethod: receiveResponseXML() called", new Dictionary<string, string> { { "response", response }, { "hresult", hresult }, { "message", message } },
//                ticket, Context.Request.UserHostAddress);
//            QBSession session = SessionPool.Instance.get(ticket);
//            if (!string.IsNullOrEmpty(hresult)) 
//                return -101;

//            int retVal = 0;// controller.processLastAction(sess, response);
//            SessionPool.Instance.put(session);
//            Logger.WriteEntry("Webmethod: receiveResponseXML() finished", new Dictionary<string, string> { { "retVal", retVal.ToString() } });
//            return retVal;
//        }

//        #region Private methods

//        private string parseForVersion(string input)
//        {
//            // This method is created just to parse the first two version components
//            // out of the standard four component version number:
//            // <Major>.<Minor>.<Release>.<Build>
//            // 
//            // As long as you get the version in right format, you could use
//            // any algorithm here. 
//            string retVal = "";
//            string major = "";
//            string minor = "";
//            Regex version = new Regex(@"^(?<major>\d+)\.(?<minor>\d+)(\.\w+){0,2}$", RegexOptions.Compiled);
//            Match versionMatch = version.Match(input);
//            if (versionMatch.Success)
//            {
//                major = versionMatch.Result("${major}");
//                minor = versionMatch.Result("${minor}");
//                retVal = major + "." + minor;
//            }
//            else
//            {
//                retVal = input;
//            }
//            return retVal;
//        }

//        /// <summary>
//        /// Interpret QB HResult passed back from QB WebConnector for method connectionError()
//        /// </summary>
//        private string interpretHResult(string hresult, string message, int ce_counter)
//        {
//            string retVal = null;
//            // Here are some example error codes used. 
//            // 0x80040400 - QuickBooks found an error when parsing the provided XML text stream. 
//            const string QB_ERROR_WHEN_PARSING = "0x80040400";
//            // 0x80040401 - Could not access QuickBooks.  
//            const string QB_COULDNT_ACCESS_QB = "0x80040401";
//            // 0x80040402 - Unexpected error. Check the qbsdklog.txt file for possible, additional information. 
//            const string QB_UNEXPECTED_ERROR = "0x80040402";
//            // Add more as you need...

//            var parameters = new Dictionary<string, string> { { "HRESULT", hresult }, { "Message", message } };
//            string errorMsg = "Webmethod: connectionError() called";
//            switch (hresult.Trim())
//            {
//                case QB_ERROR_WHEN_PARSING:
//                case QB_COULDNT_ACCESS_QB:                    
//                case QB_UNEXPECTED_ERROR:
//                    retVal = "DONE";
//                    break;
//                default:
//                    if (ce_counter == 0)
//                    {
//                        errorMsg += ". \nSending empty company file to try again.";
//                        retVal = ""; // Means try again with this company file
//                    }
//                    else
//                    {
//                        errorMsg += ". \nSending DONE to stop.";
//                        retVal = "DONE";
//                    }
//                    break;
//            }
//            return retVal;
//        }

//        #endregion
//    }
//}
