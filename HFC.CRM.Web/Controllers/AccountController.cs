﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using HFC.CRM.Web.Models;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using HFC.CRM.Core;
using HFC.CRM.Managers;
using HFC.CRM.Managers.QBSync.Helpers;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Web.Controllers
{
    [Authorize]
    //[InitializeSimpleMembership]
    public class AccountController : BaseController
    {
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl, bool? sessionExpired = null)
        {
            WebSecurity.Logout();
            SessionManager.Clear();

            SessionManager.CurrentFranchise = null;
            CookieManager.ImpersonateUserId = null;

            if (Request.IsAuthenticated)
            {
                if (SessionManager.ControlPanelPermission.CanAccess ||
                    SessionManager.ProductStrgMangPermission.CanAccess)
                {
                    //return RedirectToLocal("/ControlPanel/franchises");
                    var temp = "/ControlPanel";// "/ControlPanel/franchises"; //"/#!/franchises/"
                    return RedirectToLocal(temp);
                }
                else if (SessionManager.VendorPanelPermission.CanAccess)
                {
                    return RedirectToLocal("/VendorCP");
                }
                else if (SessionManager.PicPanelPermission.CanAccess)
                {
                    return RedirectToLocal("/PICCP");
                }
                else
                {
                    if (returnUrl == "/")
                    {
                        // TODO: need to verify whether it is the right one
                        //returnUrl = "/#/dashboard";
                        //returnUrl = "/ControlPanel";
                    }

                    return RedirectToLocal(returnUrl);
                }
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                if (sessionExpired.HasValue && sessionExpired.Value)
                    ViewBag.ErrorMessage = "Your session has expired, please log in again to continue.";
                ViewBag.LogOffUrl = Constants.OauthEndPoints.LogOffUrl;
                SessionManager.QBToken = null;
                return View("logintp");
            }
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string ReturnUrl)
        {
            //clear any previous session that might be lingering
            SessionManager.Clear();
            WebSecurity.Logout();

            SessionManager.CurrentFranchise = null;
            CookieManager.ImpersonateUserId = null;

            //we add domain name to the end of username if they dont want to type out the full domain, except for admin
            //example: username of bbtestus will be rewritten as bbtestus@bbi.corp
            //          however, for users not in AD, they will type out their full username (which is also their email)
            //example: fran@gmail.com

            try
            {
                if (ModelState.IsValid)
                {
                    string fullUsername = model.UserName;

                    if (Convert.ToString(fullUsername).Contains("@"))
                        fullUsername = fullUsername.Split('@')[0];

                    if (WebSecurity.Login(fullUsername, model.Password, model.RememberMe))
                    {
                        //we can't use session at this point since the user principal is not yet created so we have to pull the data from DB manually
                        var aUser = LocalMembership.GetUser(fullUsername, "Person", "Roles", "OAuthUsers");

                        SessionManager.SecurityUser = aUser;
                        EventLogger.LogEvent(aUser.UserName, "Account Controller Login POST");

                        //if (string.Equals(aUser.UserName, "pic", StringComparison.OrdinalIgnoreCase))
                        //    return RedirectToLocal("/#!/PICconfigurator/1095");

                        if (!aUser.IsAdminInRole() && !aUser.IsInRole(AppConfigManager.AppVendorRole.RoleId) && aUser.FranchiseId.HasValue)
                        {
                            var frnManager = new FranchiseManager(aUser);
                            var isSuspended = frnManager.IsSuspended(aUser.FranchiseId.Value);
                            if (isSuspended) //check If the FE is suspended
                            {
                                ViewBag.ErrorMessage = ContantStrings.LoginSuspended;
                                SessionManager.Clear();
                                WebSecurity.Logout();
                                SessionManager.CurrentFranchise = null;
                                CookieManager.ImpersonateUserId = null;
                                return View("logintp");
                            }
                            return RedirectToLocal("/");
                        }

                        if (aUser.IsAdminInRole() || aUser.IsInRole(AppConfigManager.AppProductStrgMang.RoleId))
                            return RedirectToLocal("/ControlPanel");
                        else if (aUser.IsInRole(AppConfigManager.AppVendorRole.RoleId))
                            return RedirectToLocal("/VendorCP");
                        else if (aUser.IsInRole(AppConfigManager.AppPicRole.RoleId))
                            return RedirectToLocal("/PICCP");
                        else
                            return RedirectToLocal("/");
                    }
                    else
                        ViewBag.ErrorMessage = ContantStrings.LoginSuspended;
                }
                else
                    ViewBag.ErrorMessage = "Please enter user name and password";
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                // If we got this far, something failed, redisplay form
                ViewBag.ErrorMessage = ContantStrings.LoginSuspended;
            }
            return View("logintp");
        }

        private string ADname(string username)
        {
            return username.Replace("@budgetblinds.com", string.Empty).Replace("@tailoredliving.com", string.Empty).Replace("@homefranchiseconcepts.com", string.Empty);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            SessionManager.Clear();
            WebSecurity.Logout();

            SessionManager.CurrentFranchise = null;
            CookieManager.ImpersonateUserId = null;

            return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        public ActionResult EULA()
        {
            ViewBag.Title = "End User License Agreement";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Title = "Contact Corporate";

            return View();
        }

        [AllowAnonymous]
        public ActionResult ErrorBrowser()
        {
            ViewBag.Title = "Error browser compatibility";

            return View();
        }

        //
        // GET: /Account/Manage

        //public ActionResult Manage()
        //{
        //    ViewBag.AvailableRoleCollection = LocalRole.GetRoles(SessionManager.CurrentFranchise.FranchiseId).OrderBy(o => o.RoleName).ToList();

        //    ViewBag.PagePermission = new Permission { CanRead = true, CanUpdate = true };
        //    ViewBag.IsProfileEdit = true;

        //    var user = new UserModel();
        //    var color = CacheManager.ColorPalettes.FirstOrDefault(f => f.ColorId == SessionManager.CurrentUser.ColorId);
        //    user = new UserModel()
        //    {
        //        PersonId = SessionManager.CurrentUser.PersonId,
        //        FirstName = SessionManager.CurrentUser.Person.FirstName,
        //        LastName = SessionManager.CurrentUser.Person.LastName,
        //        Email = SessionManager.CurrentUser.Person.PrimaryEmail,
        //        Title = SessionManager.CurrentUser.Person.WorkTitle,
        //        UserName = SessionManager.CurrentUser.UserName,
        //        IsApproved = SessionManager.CurrentUser.IsApproved,
        //        IsLocked = SessionManager.CurrentUser.IsLockedOut,
        //        BGColor = color.BGColorToRGB(),
        //        FGColor = color.FGColorToRGB(),
        //        RoleIds = SessionManager.CurrentUser.Roles.Select(s => s.RoleId).ToList(),
        //        AvailableOAuthList = SessionManager.CurrentUser.OAuthUsers.Select(s => s.Provider).ToList(),
        //        ActiveOAuthProviders = SessionManager.CurrentUser.OAuthUsers.Where(w => w.IsActive).Select(s => s.Provider).ToList()
        //    };

        //    return View(user);
        //}

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            SessionManager.Clear();
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback()
        {
            HFC.CRM.Managers.GoogleOAuthClient.RewriteRequest();
            ViewBag.ReturnUrl = Request.QueryString["ReturnUrl"];

            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = ViewBag.ReturnUrl }));

            if (!result.IsSuccessful)
            {
                ViewBag.ExtLoginStatus = "We were unable to validate your authentication, please try again.";
                //if authentication fail and the current request isn't secured then its likely SSL requirement
                if (FormsAuthentication.RequireSSL && !Request.IsSecureConnection)
                    ViewBag.ExtLoginStatus = "Unable to authenticate, a secured connection is required.";

                return View("Login");
            }
            else
            {
                string username = LocalMembership.ConfirmAndOrBindAccount(result.ExtraData["email"], result.Provider, result.ProviderUserId,
                    result.ExtraData["accesstoken"], Util.CastObject<short>(result.ExtraData["expires_in"]),
                    result.ExtraData.Keys.Contains("refresh_token") ? result.ExtraData["refresh_token"] : "");

                if (string.IsNullOrEmpty(username))
                {
                    ViewBag.ExtLoginStatus = "We were unable to confirm your account, please contact your admin and make sure your account is created, approved and not locked.";
                    return View("Login");
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(username, true);

                    return this.RedirectToLocal(ViewBag.ReturnUrl);
                    return Login(ViewBag.ReturnUrl);// RedirectToAction("Login", new { ReturnUrl = returnUrl });
                }
            }
        }

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        #endregion Helpers
    }
}