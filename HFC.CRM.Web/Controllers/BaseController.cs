﻿using System.IO;
using System.Web.Mvc;

namespace HFC.CRM.Web.Controllers
{
    using System.Web;

    using Core.Common;

    /// <summary>
    /// Base controller for all requests, so they are checked for 
    /// authorization and valid franchise.  Also for handling common tasks
    /// for all requests in one area.
    /// </summary>
    //[Authorize, ValidateFranchise, Filters.RequireHttps]
    [Authorize, ValidateFranchise, Filters.RequireHttps]
    public class BaseController : Controller
    {
        protected ActionResult ResponseResult(string status)
        {
            return status == ContantStrings.Success ? new HttpStatusCodeResult(System.Net.HttpStatusCode.OK) : new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, status);
        }

        protected ActionResult Forbidden(string message)
        {
            return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden, message);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string ua = filterContext.HttpContext.Request.UserAgent;
            if (ua != null && (ua.ToLower().Contains("iphone") || ua.ToLower().Contains("ipad")))
            {
                return;
            }

            //get browser name from Request
            bool isBrowserSupported = filterContext.HttpContext.Request.Browser.Browser != "IE"
                                      && filterContext.HttpContext.Request.Browser.Browser != "InternetExplorer"
                                      ;

            if (ua.ToLower().Contains("windows") && filterContext.HttpContext.Request.Browser.Browser == "Safari" && filterContext.ActionDescriptor.ActionName != "ErrorBrowser")
            {
                filterContext.Result = this.RedirectToAction("ErrorBrowser", "Account");
            }
            if (!isBrowserSupported && filterContext.ActionDescriptor.ActionName != "ErrorBrowser")
            {
                filterContext.Result = this.RedirectToAction("ErrorBrowser", "Account");
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            HttpResponseBase response = filterContext.HttpContext.Response;

            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.Cache.AppendCacheExtension("no-store, must-revalidate");
            response.AppendHeader("Pragma", "no-cache");
            response.AppendHeader("Expires", "0");

            base.OnActionExecuted(filterContext);
        }
    }
}