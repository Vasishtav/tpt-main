﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HFC.CRM.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                if (SessionManager.CurrentUser == null)
                    throw new Exception("CurrentUser is null");

                if (SessionManager.CurrentFranchise == null)
                    throw new Exception("CurrentFranchise is null");

                ViewBag.Title = "Dashboard";
                return View();
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Environment: " + System.Environment.MachineName);
                sb.Append("<br /><br />");
                sb.Append("Loggedin User: " + SessionManager.SecurityUser.UserName + " (Id: " + SessionManager.SecurityUser.UserId + ")");
                sb.Append("<br /><br />");
                if (SessionManager.CurrentUser != null)
                {
                    sb.Append("Current User: " + SessionManager.CurrentUser.UserName + " (Id: " + SessionManager.CurrentUser.UserId + ")");
                    sb.Append("<br /><br />");
                }
                if (SessionManager.CurrentFranchise != null)
                {
                    sb.Append("Current Franchise: " + SessionManager.CurrentFranchise.Name + " (Id: " + SessionManager.CurrentFranchise.FranchiseId + ")");
                    sb.Append("<br /><br />");
                }

                sb.Append("Error Datetime: " + DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss"));
                sb.Append("<br /><br />");
                sb.Append("Error Message: " + ex.Message.ToString());
                sb.Append("<br /><br />");
                sb.Append("Error StackTrace: " + ex.StackTrace.ToString());
                sb.Append("<br /><br />");

                ExchangeManager mgr = new ExchangeManager();
                List<string> toAddresses = AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking") != null ? AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking").Split(',').ToList() : null;
                if (toAddresses != null && toAddresses.Count > 0)
                    mgr.SendEmail("bbtestus@budgetblinds.com", "Touchpoint Error: Franchise Home Controller", sb.ToString(), toAddresses, null, null);
                EventLogger.LogEvent(ex);
                return View("Error");
            }
        }

    }
}
