﻿using System.Threading.Tasks;
using HFC.CRM.Core;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;

using SelectPdf;

namespace HFC.CRM.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Net.Mime;
    using System.Web.Http;
    using System.Web.Mvc;

    using Core.Common;
    using Data;
    using Managers;
    using Models;
    using Core.Logs;
    using Managers.DTO;
    using Dapper;
    using System.Data.SqlClient;
    using StackExchange.Profiling.Helpers.Dapper;
    using Models.Zillow;
    using RestSharp;
    using System.Net;
    using System.Xml;
    using System.Globalization;

    using System.Web.Hosting;
    using System.Text;
    using HFC.CRM.Data;
    using Serializer.Invoices;
    using Serializer;
    using Serializer.Leads;
    using System.Data;
    using DTO;
    using static Core.Common.ContantStrings;
    using Serializer.PurchaseOrder;
    using Serializer.Shipment;
    using HFC.CRM.Core.Extensions;

    public class ExportController : BaseController //: BaseExportController
    {
        private string error = @"An error occurred.If the problem persists, please contact your system administrator.";
        private OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private OrderManager orderMgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private SourceManager sourcemgr = new SourceManager();
        private ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private readonly PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        #region Print Touchpoint

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult InstallationPacket(int id
            , ExportTypeEnum type = ExportTypeEnum.PDF
            , bool inline = false, string printoptions = null,
             string installPacketOptions = null)
        {
            if (id <= 0 || string.IsNullOrEmpty(printoptions))
            {
                return this.ResponseResult("Invalid Order id");
            }

            var options = printoptions.ToString().Split(new char[] { '/' }).ToList();
            string fileDownloadName = "";
            string data = string.Empty;
            string leaddata = string.Empty;
            string measurementdata = string.Empty;
            string googledata = string.Empty;
            string customerdata = string.Empty;
            string orderdata = string.Empty;

            try
            {
                var leadDetails = options.ToList().Where(c => c.Contains("AddleadCustomer"))
                .FirstOrDefault();
                var googleMap = options.ToList().Where(c => c.ToLower().Contains("googlemap"))
                    .FirstOrDefault();
                var existingMeasurements = options.ToList().Where(c => c.Contains("existingMeasurements"))
                   .FirstOrDefault();

                var AddOrderCustomerInvoice = options.ToList().Where(c => c.Contains("AddOrderCustomerInvoice")).SingleOrDefault().Contains("true");
                var AddOrderInstallerInvoice = options.ToList().Where(c => c.Contains("AddOrderInstallerInvoice")).SingleOrDefault().Contains("true");
                var order = orderMgr.GetOrderByOrderId(id);
                var Details = QuotesMgr.GetSidemarkOppId(order.QuoteKey);

                if (!string.IsNullOrEmpty(leadDetails) && leadDetails.Contains("true"))
                {
                    data = LeadSheetReport.GetLeadCustomerContentForInstallationPacket(order.OpportunityId);
                }

                // This needs to be moved to seperate new page in the output pdf file,
                // so move this after leadsheet is created.

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";

                //using (var dblead = ContextFactory.Create())
                //{
                //    var Lead = dblead.Leads.AsNoTracking().SingleOrDefault(l => l.LeadId == id);
                //    if (type == ExportTypeEnum.PDF)
                //    {
                //        fileDownloadName = string.Format("LeadSheet {0}-{1}.pdf", Lead.LeadId, Lead.LeadNumber);
                //    }
                //    else if (type == ExportTypeEnum.Word)
                //    {
                //        fileDownloadName = string.Format("LeadSheet {0}-{1}.docx", Lead.LeadId, Lead.LeadNumber);
                //    }
                //}
                // fileDownloadName = GetFileDownloadName(id, sourceType, type);
                //fileDownloadName = string.Format("InstallationPacket {0}.pdf", id);


                //for checking conditions for order packet
                var IsleadDetails = options.ToList().Where(c => c.Contains("AddleadCustomer")).SingleOrDefault().Contains("true");
                var IsgoogleMap = options.ToList().Where(c => c.ToLower().Contains("googlemap")).SingleOrDefault().Contains("true");
                if (IsleadDetails && IsgoogleMap && AddOrderCustomerInvoice && !string.IsNullOrEmpty(installPacketOptions))
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2} - Order Packet.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));
                else
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2}.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));

                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                converter.Options.MarginTop = 20;
                converter.Options.MarginBottom = 30;
                converter.Options.MarginLeft = 10;
                converter.Options.MarginRight = 10;

                converter.Options.DisplayHeader = true;
                converter.Header.DisplayOnFirstPage = true;
                converter.Header.DisplayOnOddPages = true;
                converter.Header.DisplayOnEvenPages = true;

                converter.Options.DisplayFooter = true;
                converter.Footer.DisplayOnFirstPage = true;
                converter.Footer.DisplayOnEvenPages = true;
                converter.Footer.DisplayOnOddPages = true;

                converter.Options.PdfPageSize = PdfPageSize.Letter;

                // page numbers can be added using a PdfTextSection object
                //PdfTextSection text = new PdfTextSection(0, 10, "Page {page_number} of {total_pages}", new System.Drawing.Font("Arial", 8));
                //text.HorizontalAlign = PdfTextHorizontalAlign.Right;
                //converter.Footer.Add(text);

                string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();
                PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

                if (string.IsNullOrEmpty(data))
                {
                    doc.RemovePageAt(0);
                }

                //// https://selectpdf.com/docs/ModifyExistingPdf.htm

                var internalNotes = options.ToList().Where(c => c.Contains("AddPrintInternalNotes")) //"internalNotes"))
                   .FirstOrDefault();
                var externalNotes = options.ToList().Where(c => c.Contains("AddPrintExternalNotes")) // "externalNotes"))
                   .FirstOrDefault();

                var directionNotes = options.ToList().Where(c => c.Contains("AddPrintDirectionNotes")) // "externalNotes"))
                   .FirstOrDefault();

                var installChecklist = options.ToList()
                    .Where(c => c.Contains("AddInstallationChecklist"))
                    .FirstOrDefault();
                //var ccmeasurementForm = options.ToList()
                //   .Where(c => c.Contains("ccmeasurementForm"))
                //   .FirstOrDefault();
                //var garagemeasurementForm = options.ToList()
                //   .Where(c => c.Contains("garagemeasurementForm"))
                //   .FirstOrDefault();
                //var closetmeasurementForm = options.ToList()
                //   .Where(c => c.Contains("closetmeasurementForm"))
                //   .FirstOrDefault();
                //var officemeasurementForm = options.ToList()
                //   .Where(c => c.Contains("officemeasurementForm"))
                //   .FirstOrDefault();
                //var floormeasurementForm = options.ToList()
                //  .Where(c => c.Contains("floormeasurementForm"))
                //  .FirstOrDefault();
                //var quote = options.ToList()
                //    .Where(c => c == "quote")
                //    .SingleOrDefault();

                if (!string.IsNullOrEmpty(leadDetails))
                {
                    internalNotes = internalNotes.Contains("true") ? "internalNotes" : "";
                    externalNotes = externalNotes.Contains("true") ? "externalNotes" : "";
                    directionNotes = directionNotes.Contains("true") ? "directionNotes" : "";
                    var notes = GetNotes(id, internalNotes, externalNotes, directionNotes, "Order");
                    if (notes != null) doc.Append(notes);
                }

                if (!string.IsNullOrEmpty(googleMap) && googleMap.ToLower().Contains("true"))
                {
                    var google = GetGooglemap(order.OpportunityId, googleMap, "Opportunity");
                    if (google != null) doc.Append(google);
                }

                //  var AddInstallationChecklist = options.ToList().Where(c => c.Contains("AddInstallationChecklist")).SingleOrDefault().Contains("true");
                //  var AddInstallationProcess = options.ToList().Where(c => c.Contains("AddInstallationProcess")).SingleOrDefault().Contains("true");
                //  var AddWarrantyInformation = options.ToList().Where(c => c.Contains("AddWarrantyInformation")).SingleOrDefault().Contains("true");
                var OrderPrintFormat = options.ToList().Where(c => c.Contains("OrderPrintFormat")).SingleOrDefault();
                PdfDocument docinvoice = new PdfDocument();
                PdfDocument docinstaller = new PdfDocument();

                //for naming install doc
                if (AddOrderInstallerInvoice == true)
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2} - INSTALL DOC.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));

                if (id > 0)
                {
                    if (AddOrderCustomerInvoice)  //Customer Invoice
                    {
                        PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                        if (!string.IsNullOrEmpty(OrderPrintFormat))
                        {
                            var temp = OrderPrintFormat.Split('=');
                            if (temp.Length > 1 && temp[1] == "IncludeDiscount")
                                printVersion = PrintVersion.IncludeDiscount;
                            else if (temp.Length > 1 && temp[1] == "CondensedVersion")
                                printVersion = PrintVersion.CondensedVersion;
                            else
                                printVersion = PrintVersion.ExcludeDiscount;
                        }

                        InvoiceSheetReport InvoiceSheet = new InvoiceSheetReport();
                        docinvoice = InvoiceSheet.createInvoicePDFDoc(id, printVersion);
                        doc.Append(docinvoice);
                    }

                    if (AddOrderInstallerInvoice) //Installer invoice
                    {
                        InstallerDocument installerDocument = new InstallerDocument();
                        docinstaller = installerDocument.createInstallerPDFDoc(id);
                        doc.Append(docinstaller);
                    }
                }

                //installChecklist = installChecklist.Contains("true") ? "installChecklist" : "";
                //var icl = GetInstallationChecklist(installChecklist);
                //if (icl != null) doc.Append(icl);

                var installOptions = installPacketOptions.ToString().Split(new char[] { '/' }).ToList();
                var sp = GetSalesPacketdocumentsPdf(installOptions.ToList());
                if (sp != null)
                    doc.Append(sp);

                var mydocuments = options.Where(c => c.Contains("mydocuments_")).ToList();
                var md = GetMydocumentsPdf(mydocuments);
                if (md != null) doc.Append(md);

                //var installationGuide = GetMydocumentsPdf(mydocuments);
                //if (md != null) doc.Append(md);

                // save pdf document
                byte[] pdf = doc.Save();
                doc.Close();

                Response.AddHeader("Content-Disposition", "inline;filename=\"" + fileDownloadName + "\"");

                return File(pdf, type.Description());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.Content(error);
            }

            // return File(file, type.Description());
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PrintSalesPacket(int id, ExportTypeEnum type = ExportTypeEnum.PDF
            , bool inline = false, string sourceType = "Lead", string printoptions = null,
            string salesPacketOption = null)
        {
            string fileDownloadName = "";
            string data = string.Empty;
            var options = printoptions.ToString().Split(new char[] { '/' }).ToList();

            if (id <= 0 || string.IsNullOrEmpty(printoptions))
            {
                return this.Content(error);
            }

            try
            {

                //// https://selectpdf.com/docs/ModifyExistingPdf.htm
                bool IsPacket = false;
                int quotekey = 0;
                PdfDocument doc = new PdfDocument();

                var leadDetails = options.ToList().Where(c => c.Contains("leadDetails"))
                .FirstOrDefault();
                var internalNotes = options.ToList().Where(c => c.Contains("internalNotes"))
                  .FirstOrDefault();
                var externalNotes = options.ToList().Where(c => c.Contains("externalNotes"))
                   .FirstOrDefault();
                var directionNotes = options.ToList().Where(c => c.Contains("directionNotes"))
                  .FirstOrDefault();
                var googleMap = options.ToList().Where(c => c.Contains("googleMap"))
                    .FirstOrDefault();
                var existingMeasurements = options.ToList().Where(c => c.Contains("existingMeasurements"))
                   .FirstOrDefault();
                var bbmeasurementForm = options.ToList()
                    .Where(c => c.Contains("bbmeasurementForm"))
                    .FirstOrDefault();
                var ccmeasurementForm = options.ToList()
                   .Where(c => c.Contains("ccmeasurementForm"))
                   .FirstOrDefault();
                var garagemeasurementForm = options.ToList()
                   .Where(c => c.Contains("garagemeasurementForm"))
                   .FirstOrDefault();
                var closetmeasurementForm = options.ToList()
                   .Where(c => c.Contains("closetmeasurementForm"))
                   .FirstOrDefault();
                var officemeasurementForm = options.ToList()
                   .Where(c => c.Contains("officemeasurementForm"))
                   .FirstOrDefault();
                var floormeasurementForm = options.ToList()
                  .Where(c => c.Contains("floormeasurementForm"))
                  .FirstOrDefault();

                if (!string.IsNullOrEmpty(leadDetails) && !string.IsNullOrEmpty(googleMap) && !string.IsNullOrEmpty(existingMeasurements) && !string.IsNullOrEmpty(salesPacketOption))
                    IsPacket = true;

                if (!string.IsNullOrEmpty(leadDetails))
                {
                    data = LeadSheetReport.GetLeadCustomerContent(id, options, sourceType);

                    var leadSheet = GetLeadSheetPdf(data);
                    if (leadSheet != null)
                    {
                        var notes = GetNotes(id, internalNotes, externalNotes, directionNotes, sourceType);
                        if (notes != null)
                        {
                            leadSheet.Append(notes);
                        }
                        doc.Append(leadSheet);
                    }
                }

                var google = GetGooglemap(id, googleMap, sourceType);
                if (google != null)
                {
                    doc.Append(google);
                }

                var em = GetExistingMeasurements(id, existingMeasurements, sourceType);
                if (em != null)
                {
                    doc.Append(em);
                }

                var bbm = GetbbmeasurementForm(bbmeasurementForm);
                if (bbm != null)
                {
                    doc.Append(bbm);
                }

                var ccm = GetccmeasurementForm(ccmeasurementForm);
                if (ccm != null) doc.Append(bbm);

                var garage = GetgaragemeasurementForm(garagemeasurementForm);
                if (garage != null) doc.Append(garage);

                var closet = GetclosetmeasurementForm(closetmeasurementForm);
                if (closet != null) doc.Append(closet);

                var office = GetofficemeasurementForm(officemeasurementForm);
                if (office != null) doc.Append(office);

                var floor = GetfloormeasurementForm(floormeasurementForm);
                if (floor != null) doc.Append(floor);

                var quote = options.ToList().Where(c => c == "quote").SingleOrDefault();
                //moved the code to get the quotekey
                var quotekeyvalue = options.ToList().Where(c => c.Contains("quoteKey")).FirstOrDefault();
                string[] arrquotekey = new string[] { };
                if (!string.IsNullOrEmpty(quotekeyvalue))
                {
                    arrquotekey = quotekeyvalue.Split('|');
                    if (arrquotekey != null && arrquotekey.Length == 2 && !string.IsNullOrEmpty(arrquotekey[1]))
                        int.TryParse(arrquotekey[1], out quotekey);//quotekey = Convert.ToInt32(arrquotekey[1]);
                }

                if ((sourceType == "Opportunity" || sourceType == "Quote") && !string.IsNullOrEmpty(quote))
                {
                    var quotePrintFormat = options.ToList().Where(c => c.Contains("quotePrintFormat")).SingleOrDefault();

                    PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                    if (!string.IsNullOrEmpty(quotePrintFormat))
                    {
                        var temp = quotePrintFormat.Split('=');
                        if (temp.Length > 1 && temp[1] == "IncludeDiscount")
                            printVersion = PrintVersion.IncludeDiscount;
                        else if (temp.Length > 1 && temp[1] == "CondensedVersion")
                            printVersion = PrintVersion.CondensedVersion;
                        else
                            printVersion = PrintVersion.ExcludeDiscount;
                    }

                    if (quotekey != 0)
                    {
                        QuoteSheetReport quotesheet = new QuoteSheetReport();
                        var docQuote = quotesheet.createQuotePDFDoc(quotekey, printVersion);
                        doc.Append(docQuote);
                    }
                    else
                    {
                        var oppmgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        var opportunity = oppmgr.Get(id);
                        var quotemgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        var quoteobj = quotemgr.GetQuote(id).Where(q => q.PrimaryQuote == true).FirstOrDefault();
                        if (quoteobj != null) quotekey = quoteobj.QuoteKey;

                        if (quotekey != 0)
                        {
                            QuoteSheetReport quotesheet = new QuoteSheetReport();
                            var docQuote = quotesheet.createQuotePDFDoc(quotekey, printVersion);
                            doc.Append(docQuote);
                        }
                    }

                }

                var salesOptions = salesPacketOption.ToString().Split(new char[] { '/' }).ToList();
                var sp = GetSalesPacketdocumentsPdf(salesOptions.ToList());
                if (sp != null)
                    doc.Append(sp);

                var mydocuments = options.Where(c => c.Contains("mydocuments_")).ToList();

                var md = GetMydocumentsPdf(mydocuments);
                if (md != null && md.Pages.Count > 0) doc.Append(md);

                // save pdf document
                byte[] pdf = doc.Save();

                doc.Close();

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";

                if (sourceType == "Quote" && quotekey != 0)
                    fileDownloadName = GetFileDownloadName(quotekey, sourceType, type, IsPacket);
                else if (sourceType == "Opportunity" && quotekey != 0 && !string.IsNullOrEmpty(quote))
                    fileDownloadName = GetFileDownloadName(quotekey, "Quote", type, false);
                else
                    fileDownloadName = GetFileDownloadName(id, sourceType, type, IsPacket);

                Response.AddHeader("Content-Disposition", "inline;filename=\"" + fileDownloadName + "\"");

                return File(pdf, type.Description());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.Content(error);
            }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PrintQuote(int id, string printOptions = "ExcludeDiscount")
        {
            try
            {
                //for naming pdf
                string fileDownloadName = QuotesMgr.GetQuotebyQuoteKey(id);
                ExportTypeEnum type = ExportTypeEnum.PDF;
                PdfDocument doc = new PdfDocument();

                PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                if (printOptions == "IncludeDiscount")
                    printVersion = PrintVersion.IncludeDiscount;
                else if (printOptions == "ExcludeDiscount")
                    printVersion = PrintVersion.ExcludeDiscount;
                else if (printOptions == "CondensedVersion")
                    printVersion = PrintVersion.CondensedVersion;

                QuoteSheetReport quotesheet = new QuoteSheetReport();
                var docQuote = quotesheet.createQuotePDFDoc(id, printVersion);
                doc.Append(docQuote);

                // save pdf document
                byte[] pdf = doc.Save();

                doc.Close();

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "inline;filename=\"" + fileDownloadName + "\"");

                return File(pdf, type.Description());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.Content(error);
            }
        }

        private string GetFileDownloadName(int id, string sourceType, ExportTypeEnum type, bool IsPacket)
        {
            using (var dblead = ContextFactory.Create())
            {
                string fileDownloadName = string.Empty;
                if (sourceType == "Lead")
                {
                    var Lead = dblead.Leads.AsNoTracking().SingleOrDefault(l => l.LeadId == id);
                    if (type == ExportTypeEnum.PDF)
                    {
                        //fileDownloadName = string.Format("LeadSheet {0}-{1}.pdf", Lead.LeadId, Lead.LeadNumber);
                        fileDownloadName = string.Format("LeadSheet.pdf");
                    }
                    else if (type == ExportTypeEnum.Word)
                    {
                        fileDownloadName = string.Format("LeadSheet {0}-{1}.docx", Lead.LeadId, Lead.LeadNumber);
                    }
                }
                else if (sourceType == "Opportunity")
                {
                    fileDownloadName = OpportunityMgr.GetOppbyOppId(id, IsPacket);
                    // fileDownloadName = string.Format("LeadSheet-opportunity-{0}.pdf", id);
                }
                else if (sourceType == "Quote")
                {
                    fileDownloadName = QuotesMgr.GetQuotebyQuoteKey(id);
                }
                else
                {
                    fileDownloadName = string.Format("LeadSheet-unknown-{0}.pdf", id);
                }

                return fileDownloadName;
            }
        }

        private PdfDocument GetExistingMeasurements(int id, string existingMeasurements, string sourceType = "Lead")
        {
            if (id == 0 || string.IsNullOrEmpty(existingMeasurements) || (sourceType != "Opportunity" && sourceType != "Quote"))
                return null;

            var result = LeadSheetReport.GetExistingMeasurements(id);
            if (string.IsNullOrEmpty(result)) return null;

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            //HtmlToPdf converter = new HtmlToPdf();
            var converter = GetPdfConverter(false);
            var doc = converter.ConvertHtmlString(result, baseUrl);
            return doc;
        }

        private HtmlToPdf GetPdfConverter(bool addPageNumber = true)
        {
            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;

            converter.Options.PdfPageSize = PdfPageSize.Letter;

            // page numbers can be added using a PdfTextSection object
            if (addPageNumber)
            {
                PdfTextSection text = new PdfTextSection(0, 10, "Page {page_number} of {total_pages}", new System.Drawing.Font("Arial", 8));
                text.HorizontalAlign = PdfTextHorizontalAlign.Right;
                converter.Footer.Add(text);
            }

            return converter;
        }

        private PdfDocument GetLeadSheetPdf(string data)
        {
            if (string.IsNullOrEmpty(data)) return null;

            //HtmlToPdf converter = new HtmlToPdf();
            var converter = GetPdfConverter();

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();
            var doc = converter.ConvertHtmlString(data, baseUrl);
            return doc;
        }

        private PdfDocument GetGooglemap(int id, string googleMap, string sourceType = "Lead")
        {
            try
            {
                EventLogger.LogEvent(id + " " + sourceType, "GetGooglemap", LogSeverity.Information);
                if (id == 0 || string.IsNullOrEmpty(googleMap)) return null;

                string result = "";

                if (sourceType == "Lead")
                {
                    result = LeadSheetReport.GetGoogleMapForLead(id);
                }
                else if (sourceType == "Opportunity" || sourceType == "Quote")
                {
                    result = LeadSheetReport.GetGoogleMapForOpportunity(id);
                }
                else if (sourceType == "Order")
                {
                    result = LeadSheetReport.GetGoogleMapForOpportunity(id);
                }

                if (string.IsNullOrEmpty(result)) return null;

                //HtmlToPdf converter = new HtmlToPdf();

                string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

                var converter = GetPdfConverter(false);
                converter.Options.MinPageLoadTime = 3;
                var doc = converter.ConvertHtmlString(result, baseUrl);
                return doc;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        private PdfDocument GetNotes(int id, string internalNotes
            , string externalNotes, string directionnotes, string sourceType = "Lead")
        {
            if (id == 0 || (string.IsNullOrEmpty(internalNotes)
                && string.IsNullOrEmpty(externalNotes)
                && string.IsNullOrEmpty(directionnotes))) return null;

            string result = "";
            if (sourceType == "Lead")
            {
                result = LeadSheetReport.printLeadNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionnotes));
            }
            else if (sourceType == "Opportunity" || sourceType == "Quote")
            {
                result = LeadSheetReport.printOpportunityNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionnotes));
            }
            else if (sourceType == "Account")
            {
                result = LeadSheetReport.printAccountNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionnotes));
            }
            else if (sourceType == "Order")
            {
                result = LeadSheetReport.printOrderNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionnotes));
            }

            if (string.IsNullOrEmpty(result)) return null;

            // We have notes content, so we can create the pdf document with teh notes content.
            string filename = Server.MapPath("/Templates/base/notes.html");
            var data = System.IO.File.ReadAllText(filename);

            if (string.IsNullOrEmpty(data)) return null;

            data = data.Replace("{{notes}}", result);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            var converter = GetPdfConverter(false);
            var doc = converter.ConvertHtmlString(data, baseUrl);
            return doc;
        }

        private PdfDocument GetbbmeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;
            //  string filename = Server.MapPath("/Templates/Base/Brand/BB/BB Measurement Form.pdf");

            var result = sourcemgr.GetStreamIdForDocument(102);
            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetccmeasurementForm(string value)
        {
            // TODO: the Pdf is not yet defined;
            //https://hfcprojects.atlassian.net/wiki/spaces/TOUC/pages/84902001/Print+Measurement+Forms+-+CC
            return null;

            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            var result = sourcemgr.GetStreamIdForDocument(101);

            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 1; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetgaragemeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 3 Garage Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);

            var result = sourcemgr.GetStreamIdForDocument(203);

            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetclosetmeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 1 Closet Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);

            var result = sourcemgr.GetStreamIdForDocument(201);

            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetofficemeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 2 Office Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);

            var result = sourcemgr.GetStreamIdForDocument(202);

            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetfloormeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 4 Floor Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);

            var result = sourcemgr.GetStreamIdForDocument(204);

            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetMydocumentsPdf(List<string> values)
        {
            try
            {
                if (values == null || values.Count == 0) return null;

                PdfDocument doc = new PdfDocument();

                foreach (var item in values)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(item)) continue;

                        var temp = item.Split('|');
                        var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

                        if (count == 0) continue;

                        var temp2 = temp[0].Split('_');
                        var noteid = temp2.Length > 1 ? int.Parse(temp2[1]) : 0;
                        if (noteid == 0) continue;

                        var note = noteMgr.Get(noteid);
                        var data = FileTable.GetFileDataSync(note.AttachmentStreamId.ToString());
                        var stream = new MemoryStream(data.bytes);

                        for (int index = 0; index < count; index++)
                        {
                            var obj = new PdfDocument(stream);
                            doc.Append(obj);
                        }
                    }
                    catch (Exception e)
                    {
                        EventLogger.LogEvent(e);
                    }
                }

                return doc;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        private PdfDocument GetSalesPacketdocumentsPdf(List<string> values)
        {
            if (values == null || values.Count == 0) return null;

            PdfDocument doc = new PdfDocument();

            foreach (var item in values)
            {
                if (string.IsNullOrEmpty(item)) continue;

                var temp = item.Split('|');
                var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

                if (count == 0) continue;

                var streamId = temp[0];
                //  var noteid = temp2.Length > 1 ? int.Parse(temp2[1]) : 0;
                //  if (noteid == 0) continue;

                // var note = noteMgr.Get(noteid);
                var data = FileTable.GetFileDataSync(streamId.ToString());
                var stream = new MemoryStream(data.bytes);

                //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 4 Floor Measurement Form.pdf");
                // PdfDocument pdf = new PdfDocument(stream);

                for (int index = 0; index < count; index++)
                {
                    var obj = new PdfDocument(stream); //PdfDocument(filename);
                    doc.Append(obj);
                }
            }

            return doc;
        }

        #endregion Print Touchpoint

        #region Print MPO

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PrintMPO(int id, bool printVPO, ExportTypeEnum type = ExportTypeEnum.PDF)
        {
            string fileDownloadName = "";
            string data = string.Empty;

            if (id <= 0)
            {
                return this.Content(error);
            }

            try
            {
                PdfDocument doc = new PdfDocument();
                PurchaseOrderSheet poSheet = new PurchaseOrderSheet();

                var docMpo = poSheet.createMPOPDFDoc(id);
                doc.Append(docMpo);
                if (printVPO)
                {
                    var docvpo = poSheet.createVPOPDFDoc(id, 0);
                    doc.Append(docvpo);
                }

                // save pdf document
                byte[] pdf = doc.Save();

                doc.Close();

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";

                //fileDownloadName = "masterPurchaseOrder"; //GetFileDownloadName(id, sourceType, type);
                fileDownloadName = PurchaseOrderMgr.GetMpobyId(id);
                Response.AddHeader("Content-Disposition", "inline;filename=\"" + fileDownloadName + "\"");

                return File(pdf, type.Description());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.Content(error);
            }
        }

        #endregion Print MPO

        #region Print VPO

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PrintVPO(int id, int vpoId, ExportTypeEnum type = ExportTypeEnum.PDF)
        {
            string fileDownloadName = "";
            string data = string.Empty;

            if (id <= 0)
            {
                return this.Content(error);
            }

            try
            {
                PdfDocument doc = new PdfDocument();
                PurchaseOrderSheet poSheet = new PurchaseOrderSheet();
                var docMpo = poSheet.createVPOPDFDoc(id, vpoId);
                doc.Append(docMpo);
                // save pdf document
                byte[] pdf = doc.Save();
                doc.Close();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                fileDownloadName = "masterPurchaseOrder"; //GetFileDownloadName(id, sourceType, type);
                Response.AddHeader("Content-Disposition", "inline;filename=\"" + fileDownloadName + "\"");
                return File(pdf, type.Description());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.Content(error);
            }
        }

        public ActionResult PrintShipnotice(int id, bool printship, ExportTypeEnum type = ExportTypeEnum.PDF)
        {
            string fileDownloadName = "";
            string data = string.Empty;

            if (id <= 0)
            {
                return this.Content(error);
            }
            try
            {
                //PdfDocument doc = new PdfDocument();
                //var sourceType = SessionManager.BrandId;
                //data = PurchaseOrderSheet.GetShippingContent(id, sourceType);
                //var shipSheet = GetLeadSheetPdf(data);
                PdfDocument doc = new PdfDocument();
                ShipmentSheet shipSheet = new ShipmentSheet();
                var shipdoc = shipSheet.createShipmentDoc(id);
                if (shipSheet != null)
                {
                    doc.Append(shipdoc);
                }
                byte[] pdf = doc.Save();

                doc.Close();

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";

                //setting name for print doc
                string date = TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat();
                fileDownloadName = "VPO_xVPO_ShipmentID"+ id + "_GoodReceipt_"+ date;
                Response.AddHeader("Content-Disposition", "inline;filename=\"" + fileDownloadName + "\"");

                return File(pdf, type.Description());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.Content(error);
            }
        }

        #endregion Print VPO

        #region Print VendorInvoice

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PrintVendorInvoice(string invoiceNumber, ExportTypeEnum type = ExportTypeEnum.PDF)
        {
            try
            {
                PdfDocument doc = new PdfDocument();
                VendorInvoiceSheet viSheet = new VendorInvoiceSheet();
                var docVI = viSheet.createVendorInvoiceDoc(invoiceNumber);
                doc.Append(docVI);
                // save pdf document
                byte[] pdf = doc.Save();
                doc.Close();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "inline;filename=\"" + viSheet.VendorInvoiceFiledownloadname(invoiceNumber) + "\"");
                return File(pdf, type.Description());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.Content(error);
            }
        }

        #endregion Print VPO

        #region Download Rawdata

        public ActionResult DownloadRawDataExcel(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                ReportFilter data = new ReportFilter();
                data.StartDate = StartDate;
                data.EndDate = EndDate;
                //var result = reportMgr.getRawDataDetail(data);

                DataTable result = reportMgr.getRawDataDetailDT(data);
                result.TableName = "RawData";

                MemoryStream stream = Serializer.Reports.Excel.ExportDataTable(result);

                string filename = string.Format("RawData_{0:MMddyy}-{1:MMddyy}.xlsx", data.StartDate, data.EndDate);

                if (stream != null)
                {
                    return new OpenSpreadsheetResult(stream, filename);
                }
                return Content("");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Content("");
            }
        }
        #endregion

    }

    public class printoptions
    {
        private string Lead { get; set; }
        private string Account { get; set; }
        private string GoogleMap { get; set; }
    }
}