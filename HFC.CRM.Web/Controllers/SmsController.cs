﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Managers;
using System.Linq;
using System.Web.Mvc;
using Twilio.AspNet.Mvc;
using Twilio.TwiML;
using Newtonsoft.Json;
using HFC.CRM.Data;
using System;

namespace HFC.CRM.Web.Controllers
{
    [AllowAnonymous]
    public class SmsController : TwilioController
    {
        CommunicationManager Communication = new CommunicationManager(null, null);

        [HttpPost]
        public ActionResult Index()
        {
            string requestFrom = Request.Form["From"];
            string requestTo = Request.Form["To"];
            string requestBody = Request.Form["Body"];

            string[] optinstr = new string[] { "yes", "confirm", "ok", "go", "oui" };
            bool boptin = optinstr.Any(s => requestBody.ToLower() == s);

            string[] optoutstr = new string[] { "stop", "arret", "no", "quit", "cancel", "unsubscribe", "end" };
            bool boptout = optoutstr.Any(s => requestBody.ToLower() == s);

            string[] opthelpstr = new string[] { "help", "info", "aide" };
            bool bopthelp = opthelpstr.Any(s => requestBody.ToLower() == s);

            string phoneno = requestFrom.Substring(requestFrom.Length - 10);
            var optinfo = Communication.GetOptingInfo(phoneno);

            var response = new MessagingResponse();

            if (boptin)
            {
                if (optinfo != null)
                    Communication.UpdateOptin(phoneno);

                TextingHistoryManager mgr = new TextingHistoryManager(null, null);
                TextingHistory m = new TextingHistory();
                m.CellPhone = phoneno;
                m.Subject = "Text Opt In Confirm";
                m.Body = requestBody;
                m.SentAt = DateTime.UtcNow;
                m.Status = 2;
                m.TemplateTypeId = 0;
                mgr.Add(m);

                if (optinfo != null && optinfo.BrandId == 1)
                    response.Message("Thank you for Opting In to receive Budget Blinds Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel");
                else if (optinfo != null && optinfo.BrandId == 2)
                    response.Message("Thank you for Opting In to receive Tailored Living Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel");
                else if (optinfo != null && optinfo.BrandId == 3)
                    response.Message("Thank you for Opting In to receive Concrete Craft Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel");
            }
            else if (boptout)
            {
                if (optinfo != null)
                    Communication.UpdateOptout(phoneno);

                TextingHistoryManager mgr = new TextingHistoryManager(null, null);
                TextingHistory m = new TextingHistory();
                m.CellPhone = phoneno;
                m.Subject = "Text Opt Out Confirm";
                m.Body = requestBody;
                m.SentAt = DateTime.UtcNow;
                m.Status = 2;
                m.TemplateTypeId = 0;
                mgr.Add(m);

                if (optinfo != null && optinfo.BrandId == 1)
                    response.Message("You are unsubscribed from Budget Blinds Alerts. No more messages will be sent. Reply HELP/INFO for help or contact techsupport@gohfc.com");
                else if (optinfo != null && optinfo.BrandId == 2)
                    response.Message("You are unsubscribed from Tailored Living Alerts. No more messages will be sent. Reply HELP/INFO for help or contact techsupport@gohfc.com");
                else if (optinfo != null && optinfo.BrandId == 3)
                    response.Message("You are unsubscribed from Concrete Craft Alerts. No more messages will be sent. Reply HELP/INFO for help or contact techsupport@gohfc.com");
            }
            else
            {
                if (optinfo != null && optinfo.BrandId == 1)
                    response.Message("Budget Blinds Account Alerts Keywords: Optin:YES,OUI;Optout:STOP,ARRET,QUIT,END;Help: HELP,INFO,AIDE or email at techsupport@gohfc.com");
                else if (optinfo != null && optinfo.BrandId == 2)
                    response.Message("Tailored Living Account Alerts Keywords: Optin:YES,OUI;Optout:STOP,ARRET,QUIT,END;Help: HELP,INFO,AIDE or email at techsupport@gohfc.com");
                else if (optinfo != null && optinfo.BrandId == 3)
                    response.Message("Concrete Craft Account Alerts Keywords: Optin:YES,OUI;Optout:STOP,ARRET,QUIT,END;Help: HELP,INFO,AIDE or email at techsupport@gohfc.com");
            }

            requestFrom = requestFrom == null ? "" : requestFrom;
            requestTo = requestTo == null ? "" : requestTo;
            requestBody = requestBody == null ? "" : requestBody;

            EventLogger.LogEvent("From: " + requestFrom + ", To: " + requestTo + ", Body: " + requestBody + " Form: " + Request.Form.ToString(), "ReceiveSms");

            return TwiML(response);
        }

        [HttpPost]
        public ActionResult Error()
        {
            EventLogger.LogEvent("Form: " + Request.Form.ToString(), "ReceiveSmsFAILS");
            var response = new MessagingResponse();
            response.Message("An application error has occured.");
            return TwiML(response);
        }
    }
}