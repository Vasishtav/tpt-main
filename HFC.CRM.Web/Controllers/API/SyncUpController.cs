﻿using HFC.CRM.Core.Common;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using HFC.CRM.Web.Filters;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class SyncUpController : BaseApiController
    {
        [AllowAnonymous,HttpGet]
        public IHttpActionResult Get()
        {
            StringBuilder result = new StringBuilder("");

            bool isBusy = CacheManager.GetCache<bool>("CRM:SyncUpIsBusy");

            if (!isBusy)
            {
                CacheManager.SetCache("CRM:SyncUpIsBusy", true, DateTime.Now.AddMinutes(5));
                var status = ExecExchange();
                if (status == DataManager.Success)
                {
                    status = ExecGoogle();
                    if (status != DataManager.Success)
                        result.AppendLine("Google sync failed: " + status);
                }
                else
                    result.AppendLine("Exchange sync failed: " + status);

                CacheManager.SetCache("CRM:SyncUpIsBusy", false);
            }
            else
                result.Append("Sync up is busy");

            if (result.Length > 0)
                return ResponseResult(result.ToString());
            else
                return Ok();
        }
        [AllowAnonymous]
        [HttpGet, ActionName("Exchange")]
        public IHttpActionResult Exchange(int id)
        {
            return ResponseResult(ExecExchange());
        }
        [AllowAnonymous]
        [HttpGet, ActionName("Google")]
        public IHttpActionResult Google(int id)
        {
            return ResponseResult(ExecGoogle());
        }

        private string ExecExchange()
        {
            if (ExchangeManager.IsEnabled)            
                return ExchangeManager.PushUp();
            else
                return DataManager.Success;
        }

        private string ExecGoogle()
        {
            if (GoogleManager.IsEnabled)            
                return GoogleManager.PushUp();            
            else
                return DataManager.Success;
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult ReSyncFailedCalendarAppointments()
        {
            try
            {
                var excmgr = new ExchangeManager();
                var result = excmgr.SyncFailedCalendarAppointmentsByTime();
                var response = excmgr.BatchSyncFailedCalendarAppointments();
                return Json(response);
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[AllowAnonymous]
        //[HttpGet]
        //public IHttpActionResult BatchSyncFailedCalendarAppointments()
        //{
        //    try
        //    {
        //        var excmgr = new ExchangeManager();
        //        var result = excmgr.BatchSyncFailedCalendarAppointments();
        //        return Json(result);
        //    }

        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
