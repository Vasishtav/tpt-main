﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Extensions;
using HFC.CRM.Data.Extensions.EnumAttr;
using HFC.CRM.Managers;
using HFC.CRM.Managers.Comparers;
using Microsoft.Ajax.Utilities;
using HFC.CRM.Web.Filters;

namespace HFC.CRM.Web.Controllers.API
{
    /// <summary>
    /// Class JSBundleController.
    /// </summary>
    [LogExceptionsFilter]
    public class JSBundleController : ApiController
    {
        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>System.String.</returns>
        public static string GetContent(string id)
        {
            List<Type> ModelTypeList = new List<Type>();
            Dictionary<string, string> jsonDataList = new Dictionary<string, string>();
            var split = id.ToLower().Split('|');
            foreach (var key in split)
            {
                switch (key)
                {
                    case "enums_saleadjtypeenum":
                        ModelTypeList.Add(typeof (SaleAdjTypeEnum));
                        break;
                    case "enums_search":
                        ModelTypeList.Add(typeof (SearchFilterEnum));
                        break;
                    case "enums_notetype":
                        ModelTypeList.Add(typeof (NoteTypeEnum));
                        break;
                    case "enums_calendar":
                        ModelTypeList.Add(typeof (DayOfWeekEnum));
                        ModelTypeList.Add(typeof (DayOfWeekIndexEnum));
                        ModelTypeList.Add(typeof (RecurringPatternEnum));
                        ModelTypeList.Add(typeof (MonthEnum));
                        ModelTypeList.Add(typeof (RemindMethodEnum));
                        ModelTypeList.Add(typeof (AppointmentTypeEnum)); /// TODO REMOVE THIS completley, will be apperent in AD integration
                        jsonDataList.Add("lk_AppointmentTypeEnum", Util.JSONSerialize(SessionManager.CurrentFranchise.AppointmentTypesCollection()));
                        ModelTypeList.Add(typeof (EventTypeEnum));
                        break;
                    case "lk_productcategories":
                        jsonDataList.Add("LK_ProductCategories", Util.JSONSerialize(SessionManager.CurrentFranchise.ProductCategoryCollection()));
                        break;
                    case "lk_statuses":
                        jsonDataList.Add("LK_LeadStatuses", Util.JSONSerialize(SessionManager.CurrentFranchise.LeadStatusTypeCollection()));
                        jsonDataList.Add("LK_JobStatuses", Util.JSONSerialize(SessionManager.CurrentFranchise.JobStatusTypeCollection()));
                        break;
                    case "lk_leadsources":
                        var sources = SessionManager.CurrentFranchise.SourceCollection().Concat(SessionManager.CurrentFranchise.SourceCollection().
                                SelectMany(st => st.Children).
                                Where(st => st.FranchiseId == null || st.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)).Distinct(new TypeSourceComparer()).ToList();

                        foreach (var source in sources)
                        {
                            source.Parent = null;
                            source.LeadSources = null;
                        }

                        jsonDataList.Add("LK_LeadSources", Util.JSONSerialize(sources));
                        break;
                    case "lk_concepts":
                        jsonDataList.Add("LK_Concepts", Util.JSONSerialize(CacheManager.ConceptCollection));
                        break;
                    case "lk_states":
                        jsonDataList.Add("LK_States", Util.JSONSerialize(CacheManager.StatesCollection));
                        break;
                    case "lk_questions":
                        jsonDataList.Add("LK_Questions", Util.JSONSerialize(SessionManager.CurrentFranchise.QuestionTypeCollection()));
                        break;
                    case "lk_royaltyprofiles":
                        jsonDataList.Add("LK_Royaltyprofiles", Util.JSONSerialize(CacheManager.RoyaltyProfilesCollection));
                        break;
                    case "enum_timezones":
                        ModelTypeList.Add(typeof (TimeZoneEnum));
                        break;
                }
            }

            string strResp = Process(ModelTypeList, jsonDataList);

            if (!HttpContext.Current.IsDebuggingEnabled)
            {
                Minifier mini = new Minifier();
                CodeSettings settings = new CodeSettings
                {
                    EvalTreatment = EvalTreatment.MakeImmediateSafe,
                    PreserveImportantComments = false
                };

                strResp = mini.MinifyJavaScript(strResp, settings);
            }

            return strResp;
        }

        // GET api/jsbundle/enums-search
        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HttpResponseMessage.</returns>
        public HttpResponseMessage Get(string id)
        {
            if (string.IsNullOrEmpty(id))
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Unknown bundle id");

            var content = GetContent(id);
            if (string.IsNullOrEmpty(content))
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Bundle does not exist");
            
            var response = new HttpResponseMessage
            {
                Content = new StringContent(content, Encoding.UTF8, "application/javascript"),
            };

            response.Headers.CacheControl = new CacheControlHeaderValue
            {
                NoStore = true,
                NoCache = true,
                MaxAge = new TimeSpan(0),
                MustRevalidate = true
            };

            response.Headers.Add("Cache-Control", "no-cache, no-store, must-revalidate");
                
            response.Content.Headers.Add("Last-Modified", DateTime.Now.ToString("R"));
            return response;
        }


        /// <summary>
        /// Processes the specified model type list.
        /// </summary>
        /// <param name="ModelTypeList">The model type list.</param>
        /// <param name="JsonDataList">The json data list.</param>
        /// <returns>System.String.</returns>
        static string Process(List<Type> ModelTypeList, Dictionary<string, string> JsonDataList)
        {
            string strResponse = string.Empty;

            foreach (Type type in ModelTypeList)
            {
                if (type.IsClass)
                {
                    strResponse += GetFunctionBodyForClass(type);
                }
                else if (type.IsEnum)
                {
                    strResponse += GetObjectBodyForEnum(type);
                }
            }

            return JsonDataList.Aggregate(strResponse, (current, item) => current + string.Format("var {0} = JSON.decycle({1});\n\n", item.Key, item.Value));
        }

        /// <summary>
        /// Gets the function body for class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>System.String.</returns>
        static string GetFunctionBodyForClass(Type type)
        {
            StringBuilder sbFunctionBody = new StringBuilder();
            sbFunctionBody.AppendLine("function " + type.Name + "() {");

            foreach (PropertyInfo p in type.GetProperties())
            {
                sbFunctionBody.AppendLine("this." + p.Name + " = '';");
            }

            sbFunctionBody.Append("};");
            sbFunctionBody.Append("\n\n");
            return sbFunctionBody.ToString();
        }

        /// <summary>
        /// Gets the object body for enum.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>System.String.</returns>
        static string GetObjectBodyForEnum(Type type)
        {
            StringBuilder sbFunctionBody = new StringBuilder();
            sbFunctionBody.AppendFormat("var {0} = {{", type.Name);

            int enumLength = type.GetEnumValues().Length;
            int index = 1;

            foreach (var v in Enum.GetValues(type))
            {
                var isHidden = ((Enum)v).GetAttribute<IsNotUIVisibleAttribute>();
                if (isHidden != null)
                {
                    continue;
                }
                //var desc = string.Format("'{0}'", ((Enum)v).Description());
                var numStr = "null";
                if (v.GetType().GetEnumUnderlyingType() == typeof(int))
                    numStr = ((int)v).ToString();
                else
                    numStr = ((byte)v).ToString();
                string strEnumField = v.ToString() + " : " + numStr;//(string.IsNullOrEmpty(desc) ? ((int)v).ToString() : desc);
                if (index < enumLength)
                    strEnumField += ",";

                sbFunctionBody.AppendLine(strEnumField);
                index++;
            }

            sbFunctionBody.Append("};");
            sbFunctionBody.Append("\n\n");

            return sbFunctionBody.ToString();
        }
        
    }
}
