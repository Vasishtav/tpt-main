﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class NotesController : BaseApiController
    {
        public class UrlParam
        {
            public bool includeHistory { get; set; }
            public List<int> leadIds { get; set; }
        }

        readonly LeadsManager leadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        readonly NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        //global search
        [HttpGet]
        public IHttpActionResult GetGlobalNotess(int id)
        {
            try
            {
                Note note = new Note { NoteId = id};
                IList<Note> notes = noteMgr.GetGlobalNotes(note);

                // need to show in descending order of the notes creation date.
                notes = notes.OrderByDescending(p => p.CreatedOn).ToList();

                return Json(notes);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetLeadNotes")]
        public IHttpActionResult GetLeadNotes(int id, bool showDeleted)
        {
            try
            {
                Note note = new Note { LeadId = id };
                IList<Note> notes = noteMgr.GetLeadNotes(note, showDeleted);

                // need to show in descending order of the notes creation date.
                notes = notes.OrderByDescending(p => p.CreatedOn).ToList();

                return Json(notes);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet, ActionName("GetAccountNotes")]
        public IHttpActionResult GetAccountNotes(int id, bool showDeleted)
        {
            try
            {
                Note note = new Note { AccountId = id };
                IList<Note> notes = noteMgr.GetAccountNotes(note, showDeleted);

                // need to show in descending order of the notes creation date.
                notes = notes.OrderByDescending(p => p.CreatedOn).ToList();

                return Json(notes);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet, ActionName("GetOpportunityNotes")]
        public IHttpActionResult GetOpportunityNotes(int id, bool showDeleted)
        {
            try
            {
                Note note = new Note { OpportunityId = id };
                IList<Note> notes = noteMgr.GetOpportunityNotes(note, showDeleted);

                // need to show in descending order of the notes creation date.
                notes = notes.OrderByDescending(p => p.CreatedOn).ToList();

                return Json(notes);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet, ActionName("GetOrderNotes")]
        public IHttpActionResult GetOrderNotes(int id, bool showDeleted)
        {
            try
            {
                Note note = new Note { OrderId = id };
                IList<Note> notes = noteMgr.GetOrderNotes(note, showDeleted);

                // need to show in descending order of the notes creation date.
                notes = notes.OrderByDescending(p => p.CreatedOn).ToList();

                return Json(notes);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetFranchiseDocuments")]
        public IHttpActionResult GetFranchiseDocuments(int id)
        {
            try
            {
                Note note = new Note { FranchiseId = SessionManager.CurrentFranchise.FranchiseId };
                IList<Note> notes = noteMgr.GetFranchiseDocuments(note);

                return Json(notes);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult AddGlobalDocument(Note model)
        {
            try
            {
                var status = "Invalid data. Save Failed. Please try again";
                if (model != null)
                {
                    model.LeadId = (model.LeadId > 0) ? model.LeadId : null;
                    model.AccountId = (model.AccountId > 0) ? model.AccountId : null;
                    model.OpportunityId = (model.OpportunityId > 0) ? model.OpportunityId : null;
                    model.OrderId = (model.OrderId > 0) ? model.OrderId : null;
                    // This api only alows to add Notes NOT an Attachment.
                    model.IsNote = true;
                    model.CreatedOn = DateTime.Now;
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                    status = noteMgr.Add(model);
                }
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpPost]
        public IHttpActionResult AddNewNote(Note model)
        {
            try
            {
                var status = "Invalid data. Save Failed. Please try again";
                if (model != null)
                {
                    model.LeadId = (model.LeadId > 0) ? model.LeadId : null;
                    model.AccountId = (model.AccountId > 0) ? model.AccountId : null;
                    model.OpportunityId = (model.OpportunityId > 0) ? model.OpportunityId : null;
                    model.OrderId = (model.OrderId > 0) ? model.OrderId : null;
                    // This api only alows to add Notes NOT an Attachment.
                    model.IsNote = true;
                    model.CreatedOn = DateTime.Now;
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                    status = noteMgr.Add(model);
                }
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        
        [HttpPost]
        public IHttpActionResult UpdateNote(Note model)
        {

            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                if (model != null)
                {
                    model.IsNote = true;
                    model.UpdatedOn = DateTime.Now;
                    model.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    status = noteMgr.Update(model);
                }
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {

            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                status = noteMgr.Delete(id);
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult RecoverNote(int id)
        {

            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                status = noteMgr.Recover(id);
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult DeleteDocuments(int id)
        {

            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                status = noteMgr.DeleteDocuments(id);
                return Json(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetsalesPacketsDocList(int id, int Id)
        {
            try {
                var result = noteMgr.GetsalesPacketsDocList(Id);

                foreach(var ms in result)
                {
                    ms.Quantity = 1;
                    ms.IsEnabled = false;

                }
                return Json(result);

            }
            catch (Exception e)
            {
                return ResponseResult(EventLogger.LogEvent(e));
            }
        }


        [HttpGet]
        public IHttpActionResult GetInstallPacketsDocList(int id, int Idd)
        {
            try
            {
                var result = noteMgr.GetInstallPacketsDocList(Idd);

                foreach (var ms in result)
                {
                    ms.Quantity = 1;
                    ms.IsEnabled = false;
                }
                return Json(result);

            }
            catch (Exception e)
            {
                return ResponseResult(EventLogger.LogEvent(e));
            }
        }


        [HttpPost]
        public IHttpActionResult DeleteGlobalDocument(int Id){try { return Json(noteMgr.DeleteGlobalDocument(Id)); } catch (Exception e) { return ResponseResult(EventLogger.LogEvent(e)); }}

    }
}
