﻿using System.Threading.Tasks;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.Notes;
using HFC.CRM.Managers;

using System.Web.Http;
using KendoCRUDService.Common;
namespace HFC.CRM.Web.Controllers.API
{
    using System.Collections.Generic;
    using DTO.Search;
    using System;
    using System.Linq;
    using Models;
    using System.Net;
    using Filters;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using Data;
    [LogExceptionsFilter]
    [CompressFilter]
    public class SearchController : BaseApiController
    {
        private readonly SearchManager _searchMgr = new SearchManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private readonly LeadsManager _leadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private readonly PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        private List<int> GetSalesPersonIds(List<int> filter)
        {
            if ((SessionManager.CurrentFranchise.RestrictSalesRole ?? false) &&
                SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId)
                && !SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultOwnerRole) &&
                !SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultManagerRole) &&
                !SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultFranchiseAdminRole) &&
                !SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultCustomerServiceRole))
            {
                return new List<int>() { SessionManager.CurrentUser.PersonId };
            }

            if ((SessionManager.CurrentFranchise.RestrictInstallerRole ?? false) &&
                SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultInstallRole.RoleId))
            {
                return null;
            }

            return filter;
        }
        public List<Calendar> GetFranchiseNotifications(int Id)
        {
            List<Calendar> appointments = CRMDBContext.Calendars.Where(p => p.AssignedPersonId == SessionManager.CurrentUser.PersonId).ToList();
            return appointments;
        }
        private List<int> GetInstallerPersonIds(List<int> filter)
        {
            if ((SessionManager.CurrentFranchise.RestrictSalesRole ?? false) &&
                SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultInstallRole.RoleId)
                && !SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultOwnerRole) &&
                !SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultManagerRole) &&
                !SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultFranchiseAdminRole) &&
                !SessionManager.CurrentUser.IsInRole(AppConfigManager.DefaultCustomerServiceRole))
            {
                return new List<int>() { SessionManager.CurrentUser.PersonId };
            }

            return null;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetSearchLeads([FromUri] SearchFilter filter)
        {
            try
            {
                //IEnumerable<LeadSeachResultModel> leads = Enumerable.Empty<LeadSeachResultModel>();
                var leads = await _searchMgr.GetSearchLeads(false);
                if (leads.Count() > 0)
                {
                    foreach (var item in leads)
                    {
                        if (item.iscommercial == 1)
                        {
                            if (item.CompanyName != null && item.CompanyName != "")
                                item.LeadFullName = item.CompanyName + " " + "/" + " " + item.FirstName + " " + item.LastName;
                            else
                                item.LeadFullName = item.FirstName + " " + item.LastName;
                        }
                        else if (item.CompanyName != null && item.CompanyName != "")
                            item.LeadFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                        else
                            item.LeadFullName = item.FirstName + " " + item.LastName;
                        item.CreatedOnUtc = TimeZoneManager.ToLocal(item.CreatedOnUtc).Date;
                    }
                }

                var leadSeachResultModels = leads as LeadSeachResultNewModel[] ?? leads.ToArray();

                return Json(leadSeachResultModels);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }


        [HttpGet]
        public async Task<IHttpActionResult> GetSearchLeads_forcheck()
        {
            try
            {
                //IEnumerable<LeadSeachResultNewModel> leads = Enumerable.Empty<LeadSeachResultNewModel>();
                var leads = await _searchMgr.GetSearchLeads(true);
                if (leads.Count() > 0)
                {
                    foreach (var item in leads)
                    {
                        if (item.iscommercial == 1)
                        {
                            if (item.CompanyName != null && item.CompanyName != "")
                                item.LeadFullName = item.CompanyName + " " + "/" + " " + item.FirstName + " " + item.LastName;
                            else
                                item.LeadFullName = item.FirstName + " " + item.LastName;
                        }
                        else if (item.CompanyName != null && item.CompanyName != "")
                            item.LeadFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                        else
                            item.LeadFullName = item.FirstName + " " + item.LastName;
                        item.CreatedOnUtc = TimeZoneManager.ToLocal(item.CreatedOnUtc).Date;
                    }
                }

                var leadSeachResultModels = leads as LeadSeachResultNewModel[] ?? leads.ToArray();

                return Json(leadSeachResultModels);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
        public async Task<IHttpActionResult> GetSearchAccounts(int id)
        {
            // When id = 0 we want only active records,
            // otherwise we want all the records including inactive and merged.
            var includeInactiveMerged = id == 0 ? false : true;
            try
            {
                //IEnumerable<AccountSeachResultModel> accounts = Enumerable.Empty<AccountSeachResultModel>();
                var accounts = await _searchMgr.GetSearchAccounts(includeInactiveMerged);
                if (accounts.Count() > 0)
                {
                    foreach (var item in accounts)
                    {
                        if (item.IsCommercial == 1)
                        {
                            if (item.CompanyName != null && item.CompanyName != "")
                                item.AccountFullName = item.CompanyName + " / " + item.FirstName + " " + item.LastName;
                            else
                                item.AccountFullName = item.FirstName + " " + item.LastName;
                        }

                        else if (item.CompanyName != null && item.CompanyName != "")
                            item.AccountFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                        else
                            item.AccountFullName = item.FirstName + " " + item.LastName;
                        item.CreatedOnUtc = TimeZoneManager.ToLocal(item.CreatedOnUtc).Date;
                    }
                }
                return Json(accounts.ToList());

                // var accountSeachResultModels = accounts as AccountSeachResultModel[] ?? accounts.ToArray();
                //var accountSeachResultModels1 = accountSeachResultModels.OrderByDescending(t => t.CreatedOnUtc).ToList();
                // return Json(accountSeachResultModels1);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
        public async Task<IHttpActionResult> GetAppointments([FromUri] SearchFilter filter)
        {
            try
            {
                //Modify function to get data from db


                string assingedto = @"[
                        { text: 'Alex', value: 1, color: '#f8a398' },
                        { text: 'Bob', value: 2, color: '#51a0ed' },
                        { text: 'Charlie', value: 3, color: '#56ca85' }
                    ]";
                return Json(assingedto);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
        [HttpPost]
        public IHttpActionResult SetAppointments(int id, appointment calendarIds)
        {
            return ResponseResult("Success");
        }
        //public System.Web.Mvc.JsonResult Create()
        //{
        //    var tasks = this.desDeserializeObject<IEnumerable<TaskViewModel>>("models");

        //    if (tasks != null)
        //    {
        //        foreach (var task in tasks)
        //        {
        //            TasksRepository.Insert(task);
        //        }
        //    }

        //    return this.Jsonp(tasks);
        //}

        public async Task<IHttpActionResult> GetSearchOrders(int Id, string orderStatusListValue, string dateRangeValue)
        {
            try
            {
                IEnumerable<OrderSeachResultModel> orders = Enumerable.Empty<OrderSeachResultModel>();
                orders = await _searchMgr.GetSearchOrders(Id, orderStatusListValue, dateRangeValue);

                // TP-1698: CLONE - Opportunities and related need record level permissions
                // Opportunity need a special permission to access all the
                // records. only certain roles has that permission. otherwise
                // If the logged in user is a Sales person or installation 
                // person, the can see their records alone.
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "SalesOrder");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "SalesOrderAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    // Need to apply the filter for SalesAgent/Installation.
                    var filtered = orders.Where(x => (x.SalesAgentId ==
                                        SessionManager.CurrentUser.PersonId ||
                                        x.InstallerId == SessionManager.CurrentUser.PersonId)
                                        ).ToList();

                    orders = filtered;
                }


                var orderSeachResultModels = orders as OrderSeachResultModel[] ?? orders.ToArray();

                return Json(orderSeachResultModels);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
        public async Task<IHttpActionResult> GetSearchAccountPayments(int Id, [FromUri] SearchFilter filter)
        {
            try
            {
                IEnumerable<PaymentSeachResultModel> payments = Enumerable.Empty<PaymentSeachResultModel>();
                payments = await _searchMgr.GetSearchAccountPayments(Id);

                var orderSeachResultModels = payments as PaymentSeachResultModel[] ?? payments.ToArray();

                return Json(orderSeachResultModels);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
        public async Task<IHttpActionResult> GetSearchAccountInvoices(int Id, [FromUri] SearchFilter filter)
        {
            try
            {
                IEnumerable<InvoiceSeachResultModel> Invoices = Enumerable.Empty<InvoiceSeachResultModel>();
                Invoices = await _searchMgr.GetSearchAccountInvoices(Id);

                var InvoicesSeachResultModels = Invoices as InvoiceSeachResultModel[] ?? Invoices.ToArray();

                return Json(InvoicesSeachResultModels);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetLeadMatchingAccounts(int id)
        {
            try
            {
                IEnumerable<AccountSeachResultModel> accounts = Enumerable.Empty<AccountSeachResultModel>();
                accounts = await _searchMgr.GetLeadMatchingAccounts(id);


                foreach (var item in accounts)
                {
                    if (item.CompanyName != null && item.CompanyName != "")
                    {
                        item.AccountFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                    }
                    else
                    {
                        item.AccountFullName = item.FirstName + " " + item.LastName;
                    }
                }
                var accountSeachResultModels = accounts as AccountSeachResultModel[] ?? accounts.ToArray();

                return Json(accountSeachResultModels);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }


        //public async Task<IHttpActionResult> GetLeadMatchingAccounts([FromUri] SearchFilter filter)
        [HttpGet]
        public async Task<IHttpActionResult> GetLeadMatchingAccountsTP(int id)// ApplyLeadMatchingAccount(int id)
        {
            try
            {
                IEnumerable<AccountSeachResultModel> accounts = Enumerable.Empty<AccountSeachResultModel>();
                accounts = await _searchMgr.ApplyLeadMatchingAccount(id);
                foreach (var item in accounts)
                {
                    if (item.CompanyName != null && item.CompanyName != "")
                    {
                        item.AccountFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                    }
                    else
                    {
                        item.AccountFullName = item.FirstName + " " + item.LastName;
                    }
                }
                var accountSeachResultModels = accounts as AccountSeachResultModel[] ?? accounts.ToArray();

                return Json(accountSeachResultModels);
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
        public async Task<IHttpActionResult> GetSearchOpportunitys(int Id, [FromUri] SearchFilter filter, int opportunityId = 0)
        {
            try
            {


                IEnumerable<OpportunitySeachResultModel> Opportunitys = Enumerable.Empty<OpportunitySeachResultModel>();

                Opportunitys = await _searchMgr.GetSearchOpportunitys(Id, opportunityId);

                // TP-1698: CLONE - Opportunities and related need record level permissions
                // Opportunity need a special permission to access all the
                // records. only certain roles has that permission. otherwise
                // If the logged in user is a Sales person or installation 
                // person, the can see their records alone.
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "Opportunity");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "OpportunityAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = Opportunitys.Where(x => (x.SalesAgentId ==
                                       SessionManager.CurrentUser.PersonId ||
                                       x.InstallerId == SessionManager.CurrentUser.PersonId)
                                       ).ToList();

                    Opportunitys = filtered;
                }
            
                var opportunitySeachResultModelsSortById = Opportunitys.Where(o => o.AccountName != "").OrderByDescending(p => p.OpportunityID).ToList();

                // for move quote opportunity filter
                if (opportunityId != 0)
                    opportunitySeachResultModelsSortById = opportunitySeachResultModelsSortById.Where(o => o.OpportunityID != opportunityId && o.OpportunityStatus != "Ordering" && o.OpportunityStatus != "Order Accepted").ToList();

                return Json(opportunitySeachResultModelsSortById);

            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
        public async Task<IHttpActionResult> GetInvoices([FromUri] SearchFilter filter)
        {
            int totalRecords = 0;
            try
            {
                var invoices = await _searchMgr.GetInvoices(
                    filter.jobStatusIds,
                    this.GetInstallerPersonIds(filter.salesPersonIds),
                    this.GetSalesPersonIds(filter.salesPersonIds),
                    filter.leadStatusIds,
                    filter.invoiceStatuses,
                    filter.sourceIds,
                    filter.searchTerm, filter.searchFilter, filter.createdOnUtcStart,
                    filter.createdOnUtcEnd, filter.pageIndex, filter.pageSize,
                    filter.orderBy, filter.orderByDirection);

                var invoiceSearchDtos = invoices as InvoiceSearchDTO[] ?? invoices.ToArray();
                if (invoiceSearchDtos.Count() != 0)
                {
                    totalRecords = invoiceSearchDtos[0].TotalCount.HasValue ? invoiceSearchDtos[0].TotalCount.Value : 0;
                }

                return Ok(new
                {
                    Invoices = invoiceSearchDtos,
                    TotalRecords = totalRecords
                });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        public IHttpActionResult GetSavedSearch()
        {
            var results = _searchMgr.GetSavedSearch();
            return Ok(new
            {
                results
            });
        }
        [HttpDelete]
        public IHttpActionResult DeleteSavedSearch(int id)
        {
            var results = _searchMgr.DeleteSavedSearch(id);
            return Ok(new
            {
                results
            });
        }

        public IHttpActionResult SavedSearch(int id, SaveSearchDTO model)
        {
            var results = _searchMgr.SavedSearch(model);
            return Ok(new
            {
                results
            });
        }
        public IHttpActionResult GetVendors()
        {
            List<Vendors> results = new List<Vendors>();
            for (int i = 0; i <= 10; i++)
            {
                results.Add(new Vendors
                {
                    VendorId = 1 + i,
                    VendorName = "Billy" + i,
                    Location = "Area 51 U.S.A" + i,
                    VendorType = "Home Depot " + i,
                    Account_Rep = "Pagani Zonda F Roadster",
                    Rep_Phnone = 9575412458 + i,
                    Comm_Method = "Email" + i,
                });
            }

            var leadSeachResultModels = null ?? results.ToArray();
            return Json(new
            {
                TotalRecords = results.Count(),
                LeadsCollection = leadSeachResultModels

            });
        }


        public async Task<IHttpActionResult> GetSearchProducts([FromUri] SearchFilter filter)
        {
            //Adding for Product data to get
            List<HFC.CRM.Data.Products> result = new List<HFC.CRM.Data.Products>();
            result.Add(new HFC.CRM.Data.Products
            {
                ProductId = 11111,
                ProductName = "Bracket",
                VendorName = "Springs",
                SKU = "123-123-123",
                ProductCategory = "Hardware",
                SubCategory = "Brocket",
                Description = "Mouting Bracket",
                SalesPrice = 69.50m,
                Active = "Yes"
            });
            result.Add(new HFC.CRM.Data.Products
            {
                ProductId = 22222,
                ProductName = "Throw",
                VendorName = "World Trade",
                SKU = "321-321-321",
                ProductCategory = "Decore",
                SubCategory = "Pillow",
                Description = "Mouting Pillow",
                SalesPrice = 79.00m,
                Active = "Yes"
            });
            result.Add(new HFC.CRM.Data.Products
            {
                ProductId = 33333,
                ProductName = "System",
                VendorName = "Home Vendor",
                SKU = "121-212-121",
                ProductCategory = "Trade",
                SubCategory = "Windows",
                Description = "Product Purchase",
                SalesPrice = 54.00m,
                Active = "Yes"
            });
            return Json(result);

        }

    }
    public class appointment
    {
        public string Title { get; set; }
    }
    public static class ControllerExtensions
    {
        public static JsonpResult Jsonp(this Controller controller, object data, string callbackName = "callback")
        {
            return new JsonpResult(callbackName)
            {
                Data = data,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public static T DeserializeObject<T>(this Controller controller, string key) where T : class
        {
            var value = controller.HttpContext.Request.QueryString.Get(key);
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            return javaScriptSerializer.Deserialize<T>(value);
        }
    }
}