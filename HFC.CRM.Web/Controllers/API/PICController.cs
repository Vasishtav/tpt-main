﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HFC.Common.Logging;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Core.Common;
using System.Web.Http;
using RestSharp;
using HFC.CRM.Core;
using HFC.CRM.DTO.PIC;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Helpers;
using System.Dynamic;
using HFC.CRM.Core.Logs;
using System.Diagnostics;
using System.Runtime.Caching;
using System.IO;
using System.Data;

namespace HFC.CRM.Web.Controllers.API
{
    public class PICController : BaseApiController
    {
        QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        PICManager picMgr = new PICManager();
        Stopwatch start = new Stopwatch();

        public static string PICToken()
        {
            string CacheKey = "PICToken";
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
                return cache.Get(CacheKey).ToString();
            else
            {
                var authMgr = new OAuthManager();
                var picToken = authMgr.GetAccessToken();
                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(55);
                cache.Add(CacheKey, picToken.token.access_token, cacheItemPolicy);

                return picToken.token.access_token;
            }
        }

        public IHttpActionResult GetPICProductGroup()
        {
            var prdMgr = new ProductManagerTP();
            var res = prdMgr.GetAllPICBBProducts();
            var rep = JsonConvert.DeserializeObject<dynamic>(res);
            return Json(rep);
            //var prdMgr = new ProductManagerTP();
            //var res = prdMgr.GetAllPICProducts();
            //return Json(res);
            //return Json(new { valid = true, PICItems = res });
        }

        public IHttpActionResult GetPICProductGroupForConfigurator(string franchiseCode, bool vendorConfig = false)
        {
            var prdMgr = new ProductManagerTP();
            if (string.IsNullOrEmpty(franchiseCode))
            {
                franchiseCode = "19001001";
            }
            var res = prdMgr.GetPICProductGroupForConfigurator(franchiseCode, vendorConfig);
            return Json(res);
            //return Json(new { valid = true, PICItems = res });
        }
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.HttpGet]
        public IHttpActionResult ClearModelCache()
        {
            List<string> cacheKeys = MemoryCache.Default.Select(x => x.Key).ToList().Where(x => x.Contains("GetPICProductGroup_")).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                MemoryCache.Default.Remove(cacheKey);
            }
            return Json(true);
        }
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.HttpGet]
        public IHttpActionResult CreateModelCache()
        {
            var prdMgr = new ProductManagerTP();
            prdMgr.CreateModelCache();
            return Json(true);
        }
        public IHttpActionResult GetDatasourceURL(string productid, string modelId)
        {
            var dataSource = picMgr.GetDatasourceURL(productid, modelId);
            return Json(dataSource);
        }

        //public string GetProductGroup()
        //{
        //    string url = AppConfigManager.PICUrl;
        //    var api = url + "/ProductGroup?access_token=" + PICToken();
        //    var client = new RestClient(api);
        //    var request = new RestRequest(Method.GET);
        //    request.AddHeader("cache-control", "no-cache");
        //    IRestResponse response = client.Execute(request);
        //    return temp;
        //}
        public IHttpActionResult GetDataSource(string productid, string modelId)
        {
            var prdInfo = PICManager.GetProductInfo(productid);
            var dataSource = PICManager.GetDataSource(productid, modelId);
            return Json(new { productInfo = prdInfo, dataSource = dataSource });
        }

        public IHttpActionResult GetDataSourceAndPromptAnswers(string productid, string modelId)
        {
            //var prdInfo = PICManager.GetProductInfo(productid);
            var dsurl = picMgr.GetDatasourceURL(productid, modelId);
            var ppa = PICManager.PICProductPromptAnswers(productid, modelId, SessionManager.CurrentFranchise.Code);
            return Json(new { productInfo = "", dataSource = "", promptAnswers = ppa, dsurl = dsurl });
        }

        public IHttpActionResult GetDataSourceAndPromptAnswersByFranchiseId(string productid, string modelId, string franchiseCode)
        {
            var prdInfo = PICManager.GetProductInfo(productid);
            var dsurl = picMgr.GetDatasourceURL(productid, modelId);
            if (string.IsNullOrEmpty(franchiseCode))
            {
                franchiseCode = "19001001";
            }
            var ppa = PICManager.PICProductPromptAnswers(productid, modelId, franchiseCode);
            return Json(new { productInfo = prdInfo, dataSource = "", promptAnswers = ppa, dsurl = dsurl });
        }

        public IHttpActionResult GetModels(string productId)
        {
            var PICModels = PICManager.GetModels(productId);
            if (PICModels.Count > 0)
            {
                return Json(new { error = "", Data = PICModels });
            }
            else
            {
                return Json(new { error = "An error occurred." });
            }
        }

        [System.Web.Mvc.HttpGet]
        public IHttpActionResult PICDatasourceByProduct(string productId)
        {
            var pdbp = PICManager.PICDatasourceByProduct(productId);
            return Json(pdbp);
        }

        [System.Web.Http.AcceptVerbs("GET")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult PICProductPromptAnswers(string productId, string modelId)
        {
            var ppa = PICManager.PICProductPromptAnswers(productId, modelId, SessionManager.CurrentFranchise.Code);
            var dsurl = picMgr.GetDatasourceURL(productId, modelId);
            return Json(new { ppa = ppa, dsurl = dsurl });
        }

        public dynamic GetPICVendor(string PICVendorId)
        {
            string url = AppConfigManager.PICUrl;
            var api = url + "/Vendor/" + PICVendorId + "?access_token=" + PICToken();
            var client = new RestClient(api);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            start.Start();
            IRestResponse response = client.Execute(request);
            start.Stop();
            EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Vendor");
            return Json(response.Content);
        }

        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult PICvalidatePromptAnswers([FromBody]CoreProductVM obj, string fCode)
        {
            string content = "";
            try
            {
                string url = AppConfigManager.PICUrl;
                /// Configurator / PromptAnswers / Validate /{ customerId}/{ productId}/{ modelId}/{ colorId}
                //TODO: for now hard code the franchisecode as these will be later changed as Franchisecode by PIC
                if (string.IsNullOrEmpty(fCode))
                {
                    fCode = "19001001";
                }
                //string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/Configurator/PromptAnswers/Validate/" + fCode + "/" + obj.PicProduct + "/" + obj.ModelId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");

                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "promptAnswers=" + obj.PromptAnswers, ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Validate");
                content = response.Content;
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response.Content);

                return Json(new { valid = true, data = responseObj });
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }
                return Json(new { valid = false, messages = msg });
            }
        }

        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult validatePromptAnswers([FromBody]CoreProductVM obj)
        {
            string content = "";
            try
            {
                string url = AppConfigManager.PICUrl;
                /// Configurator / PromptAnswers / Validate /{ customerId}/{ productId}/{ modelId}/{ colorId}
                //TODO: for now hard code the franchisecode as these will be later changed as Franchisecode by PIC
                string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/Configurator/PromptAnswers/Validate/" + FranchiseCode + "/" + obj.PicProduct + "/" + obj.ModelId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");

                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "promptAnswers=" + obj.PromptAnswers, ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Validate");
                content = response.Content;
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response.Content);

                if (Convert.ToBoolean(responseObj.valid) == true && responseObj.messages.errors != null && responseObj.messages.errors.Count == 0)
                {
                    if (Convert.ToDecimal(responseObj.cost) == 0 || Convert.ToDecimal(responseObj.list) == 0)
                    {
                        return Json(new
                        {
                            valid = false,
                            message = "Price Validation has failed on this product configuration. Please contact your system administrator.",
                            data = responseObj,
                            QuoteKey = obj.QuoteKey,
                            OpportunityId = obj.OpportunityId
                        });
                    }
                    else
                    {
                        var res = QuotesMgr.UpdateCoreProductQuoteconfiguration(obj, responseObj);
                    }
                }

                return Json(new { valid = true, data = responseObj, QuoteKey = obj.QuoteKey, OpportunityId = obj.OpportunityId });
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }
                return Json(new { valid = false, messages = msg });
            }
        }

        /// <summary>
        /// Get Vendor and Product information into PIC service
        /// </summary>
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.AcceptVerbs("GET")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult PICVendorProduct()
        {
            //string url = AppConfigManager.PICUrl;
            //string pToken = PICToken();
            picMgr.PICVendorProduct();
            //ImportVendor(url, pToken);
            //UpdateVendor(url, pToken);

            //var mgr = new PICHFCProductManager();
            //mgr.InsertOrUpdateProductModels(905086);
            return Json("Success");
        }

        /// <summary>
        /// Insert or Update Vendors Information
        /// </summary>
        private bool ImportVendor(string url, string pToken)
        {
            var avapi = url + "/Vendor?access_token=" + pToken;
            var avclient = new RestClient(avapi);
            var avrequest = new RestRequest(Method.GET);
            avrequest.AddHeader("cache-control", "no-cache");
            start.Start();
            var avresponse = avclient.Execute(avrequest);
            start.Stop();
            EventLogger.LogRequest(avrequest, avresponse, start.ElapsedMilliseconds, "PIC", "Vendor");
            var avobj = JsonConvert.DeserializeObject<AllVendorRoot>(avresponse.Content); // Convert Json to Object
            if (avobj.valid == true && avobj.properties != null)
            {
                PICHFCVendorManager _rep = new PICHFCVendorManager();
                _rep.ImportVendor(avobj);
            }

            return true;
        }

        /// <summary>
        /// Update Particular vendor Information
        /// </summary>
        private bool UpdateVendor(string url, string pToken)
        {
            PICHFCVendorManager _rep = new PICHFCVendorManager();
            var VendorList = _rep.GetListVendor();
            foreach (var vlist in VendorList)
            {
                var vapi = url + "/Vendor/" + vlist.PICVendorId + "?access_token=" + pToken;
                var vclient = new RestClient(vapi);
                var vrequest = new RestRequest(Method.GET);
                vrequest.AddHeader("cache-control", "no-cache");
                start.Start();
                var vresponse = vclient.Execute(vrequest);
                start.Stop();
                EventLogger.LogRequest(vrequest, vresponse, start.ElapsedMilliseconds, "PIC", "Vendor");
                var vobj = JsonConvert.DeserializeObject<VendorRoot>(vresponse.Content);  // Convert Json to Object
                if (vobj.valid == true && vobj.properties != null && vobj.properties.Vendor != null)
                {
                    PICHFCVendorData pvd = new PICHFCVendorData();
                    pvd.h = vlist;
                    pvd.v = vobj;
                    _rep.UpdateVendorData(pvd);
                }
                ImportProduct(vlist, url, pToken);
            }

            return true;
        }

        /// <summary>
        /// Insert or Update Product Information
        /// </summary>
        private bool ImportProduct(HFCVendor hv, string url, string pToken)
        {
            var papi = url + "/Vendor/" + hv.PICVendorId + "/Product?access_token=" + pToken;
            var pclient = new RestClient(papi);
            var prequest = new RestRequest(Method.GET);
            prequest.AddHeader("cache-control", "no-cache");
            start.Start();
            var presponse = pclient.Execute(prequest);
            start.Stop();
            EventLogger.LogRequest(prequest, presponse, start.ElapsedMilliseconds, "PIC", "Vendor");
            var pobj = JsonConvert.DeserializeObject<ProductRoot>(presponse.Content);  // Convert Json to Object
            if (pobj.valid == true && pobj.properties != null && pobj.properties.Groups != null)
            {
                PICHFCProductManager _rep = new PICHFCProductManager();
                _rep.ImportProducts(hv.VendorId, pobj);
            }

            return true;
        }

        /// <summary>
        /// Get Vendor and Product information into PIC service
        /// </summary>
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.AcceptVerbs("GET")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult BatchVendorCustomer()
        {
            //string url = AppConfigManager.PICUrl;
            //string pToken = PICToken();
            picMgr.BatchVendorCustomer();
            //ImportVendor(url, pToken);
            //UpdateVendor(url, pToken);

            return Json("Success");
        }
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.HttpPost]
        public IHttpActionResult SyncAllVendorCustomerAccountNumbers()
        {

            var file = HttpContext.Current.Request.Files.Count > 0 ?
                HttpContext.Current.Request.Files[0] : null;

            var arr = new List<string>();
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(file.InputStream))
            {
                string[] headers = sr.ReadLine().Split(',');
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }

            }

            foreach (DataRow row in dt.Rows)
            {
                string cust = row["Customer"].ToString();
                string Vendor = row["Vendor"].ToString();
                string vCust = row["VendorCustomer"].ToString();
                if (Vendor.ToUpper() == "SPR")
                {
                    if (vCust.Trim().Length == 5)
                    {
                        vCust = "0" + vCust;
                    }
                }
                var res = picMgr.PUTBatchVendorCustomer(Vendor, vCust, cust);

                dynamic picJson = JsonConvert.DeserializeObject<dynamic>(res);


                if (!picJson.valid.Value)
                {
                    dynamic err = JsonConvert.DeserializeObject<dynamic>(picJson.error);
                    //var test = picJson.error.Value;

                    arr.Add(picJson.error);
                }



            }
            if (arr.Count > 0)
            {
                return Json(arr);
            }
            return Json("Success");
        }
    }
}