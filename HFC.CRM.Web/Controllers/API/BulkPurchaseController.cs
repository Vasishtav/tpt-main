﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class BulkPurchaseController : BaseApiController
    {
        readonly BulkPurchaseManager bpManager = new BulkPurchaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        readonly PurchaseOrderManager poManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        [HttpGet]
        public IHttpActionResult GetOrders(int id)
        {
            try
            {
                var result = bpManager.GetOrders();

                var orderIds = result.Where(x => x.QuoteLines.Count > 3).Select(y => new { id =  y.OrderID} ).ToList();

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        ////[FromBody]OrderDTO model
        //[HttpPost]
        //[Route("ConvertSalesOrderToxVpo")]
        ////public IHttpActionResult ConvertSalesOrderToxVpo(int id, [FromBody]List<int> selectedOrderIds)
        //public IHttpActionResult ConvertSalesOrderToxVpo(List<int> model)
        //{
        //    try
        //    {
        //        //var result = Case_Mangr.Complete_Convert(model);

        //        var result = "Success";
        //        return Json(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResponseResult(EventLogger.LogEvent(ex));
        //    }
        //}

        [HttpPost]
        public IHttpActionResult PostConvertSalesOrderToxVpo(int id, List<int> models)
        {
            try
            {
                poManager.ConvertOrderToXMasterPurchaseOrders(models);
                
                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }



    }
}
