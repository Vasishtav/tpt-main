﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Managers;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class CaseCommentsController : BaseApiController
    {

        [HttpPost]
        public IHttpActionResult SaveParentComment(int id, [FromUri] string unique, CaseComments Comments)
        {
            try
            {
                // var result = Comments;
                // string val = Comments.module;
                string[] emailids = new string[] { };
                //if (unique != null)
                    //emailids = unique.Split(',');

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "";
                    query = @"insert into CRM.CaseComments (CaseId, PersonId, ParentId, HasChildId, htmlComment, IsReply, IsEdit,CreatedOn, LastUpdatedOn, BelongsTo) values (@CaseId, @PersonId, @ParentId, @HaschildId, @htmlComment, @IsReply, @IsEdit, @CreatedOn, @LastUpdatedOn, @BelongsTo )";
                    int PersonId = 0;

                    if (SessionManager.CurrentUser.Person != null)
                    {
                        PersonId = SessionManager.CurrentUser.Person.PersonId;
                        //UserName = SessionManager.CurrentUser.Person.FirstName + " " + SessionManager.CurrentUser.Person.LastName;
                    }
                    else if (SessionManager.SecurityUser.Person != null)
                    {
                        PersonId = SessionManager.SecurityUser.Person.PersonId;
                        //UserName = UserName = SessionManager.SecurityUser.Person.FirstName + " " + SessionManager.SecurityUser.Person.LastName;
                    }

                    var rrr = connection.Execute(query, new
                    {
                        CaseId = Comments.CaseId,
                        PersonId = PersonId,
                        ParentId = Comments.ParentId,
                        HaschildId = Comments.HasChildId,
                        HtmlComment = Comments.HTMLComment,
                        HelongsTo = Comments.BelongsTo,
                        IsReply = Comments.IsReply,
                        IsEdit = Comments.IsEdit,
                        CreatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        BelongsTo = Comments.BelongsTo
                    });


                    CaseManager ca = new CaseManager( SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    CaseFeedbackDetails details = new CaseFeedbackDetails();
                  //  ca.sendEmailForComments(CaseId,CaseNumber, Comments.HTMLComment, "New", emailids, details);
                    var res = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C Inner Join CRM.Person P on P.PersonId=C.PersonId 
                            where C.Id=(select max(id) from CRM.CaseComments where DeletedOn is null and CaseId = @CaseId)", new { CaseId = Comments.CaseId }).ToList();
                                       
                    res[0].CreatedOn = TimeZoneManager.ToLocal(res[0].CreatedOn);
                    res[0].LastUpdatedOn = TimeZoneManager.ToLocal(res[0].LastUpdatedOn);
                    connection.Close();
                    return Json(res[0]);                                        
                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

            //  return null;

        }

        [HttpPost]
        public IHttpActionResult SaveReplyComment(int id, [FromUri] string unique, int CaseId, CaseComments Comments)
        {
            try
            {
                //var result = Comments;
                //string val = Comments.module;
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "";
                    int VendorId = 0;
                    query = @"insert into CRM.CaseComments (CaseId, PersonId, ParentId, HasChildId, htmlComment, IsReply, IsEdit,CreatedOn, LastUpdatedOn, BelongsTo) values (@CaseId, @PersonId, @ParentId, @HaschildId, @htmlComment, @IsReply, @IsEdit, @CreatedOn, @LastUpdatedOn, @BelongsTo )";
                    int PersonId = 0;

                    if (SessionManager.CurrentUser.Person != null)
                    {
                        PersonId = SessionManager.CurrentUser.Person.PersonId;
                        //UserName = SessionManager.CurrentUser.Person.FirstName + " " + SessionManager.CurrentUser.Person.LastName;
                    }
                    else if (SessionManager.SecurityUser.Person != null)
                    {
                        PersonId = SessionManager.SecurityUser.Person.PersonId;
                        //UserName = UserName = SessionManager.SecurityUser.Person.FirstName + " " + SessionManager.SecurityUser.Person.LastName;
                    }

                    var rrr = connection.Execute(query, new
                    {
                        CaseId = Comments.CaseId,
                        PersonId = PersonId,
                        ParentId = Comments.ParentId,
                        HaschildId = Comments.HasChildId,
                        HtmlComment = Comments.HTMLComment,
                        BelongsTo = Comments.BelongsTo,
                        IsReply = Comments.IsReply,
                        IsEdit = Comments.IsEdit,
                        CreatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                    });

                    // connection.Execute("Update CRM.CaseComments set haschildId=@haschildId,updatedAt=@updatedAt where id=@id", new { haschildId=true,id= Comments.parentId, updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    connection.Execute("Update CRM.CaseComments set haschildId=@haschildId where id=@id", new { haschildId = true, id = Comments.ParentId });

                    // var res = connection.Query<Comments>("Select * from CRM.CaseComments where where id=@id", new { id=id}).ToList();
                    var res = new List<CaseComments>();
                   
                        res = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C 
	                                                            Inner Join CRM.Person P on P.PersonId=C.PersonId 
                                                                        where id=(select max(id) from CRM.CaseComments where DeletedOn is null and CaseId = @CaseId and PersonId=@PersonId)", new { CaseId = Comments.CaseId, PersonId }).ToList();                    

                    var parentcomment = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C 
	                                                                        Inner Join CRM.Person P on P.PersonId=C.PersonId 
                                                                                    where C.id= @id", new { id = Comments.ParentId }).ToList();
                    var ress = connection.Query<Person>("Select * from CRM.Person where PersonId= @id", new { id = parentcomment[0].PersonId }).ToList();
                    connection.Close();

                    // var result = Comments;
                    // string val = Comments.module;
                    string[] emailids = new string[] { };
                    if (unique != null)
                        emailids = unique.Split(',');
                    
                    string[] emailidss = new string[] { };                   

                        if (ress.Count > 0)
                        {
                            List<string> item = new List<string>();
                            item = emailids.ToList();
                            item.Add(ress[0].PrimaryEmail);

                            emailidss = item.ToArray();

                        }
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        //ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "Reply", emailidss);

                        res[0].CreatedOn = TimeZoneManager.ToLocal(res[0].CreatedOn);
                        res[0].LastUpdatedOn = TimeZoneManager.ToLocal(res[0].LastUpdatedOn);                  

                    return Json(res[0]);

                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }

        [HttpPost]
        public IHttpActionResult deleteComment(int id, int parentId)
        {

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    connection.Execute("update CRM.CaseComments set LastUpdatedOn=@LastUpdatedOn,DeletedOn=@DeletedOn where id=@id", new { id = id, LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc), DeletedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    var res = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C 
	                                                                Inner Join CRM.Person P on P.PersonId=C.PersonId 
                                                                        where C.parentId=@parentId and C.DeletedOn is null", new { parentId = parentId}).ToList();
                    if (!(res.Count > 0))
                    {
                        connection.Execute("update CRM.CaseComments set haschildId=@haschildId,LastUpdatedOn=@LastUpdatedOn where id=@id", new { haschildId = false, id = parentId, LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    }
                    connection.Close();
                }
                return Json(true);

            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);

            }

        }

        [HttpPost]
        public IHttpActionResult updateParentComment(int id, [FromUri] string unique, int CaseId, CaseComments Comments)
        {
            DateTime dt = DateTime.UtcNow;
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    connection.Execute("update CRM.CaseComments set HTMLComment=@HTMLComment,LastUpdatedOn=@LastUpdatedOn where id=@id", new { htmlComment = Comments.HTMLComment, id = id, LastUpdatedOn = dt });
                    connection.Close();

                    // var result = Comments;
                    //  string val = Comments.module;
                    string[] emailids = new string[] { };
                    if (unique != null)
                        emailids = unique.Split(',');

                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                       // ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);

                        var rr = TimeZoneManager.ToLocal(dt);
                        return Json(rr);                    
                }                

            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);

            }
        }

        [HttpGet]
        public IHttpActionResult loadInitialComments(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var res = new List<CaseComments>();                    
                        res = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C 
                                                     Inner Join CRM.Person P on P.PersonId = C.PersonId  where CaseId=@CaseId and DeletedOn is null", new {CaseId=id }).ToList();
                        foreach (var cm in res)
                        {
                            cm.CreatedOn = TimeZoneManager.ToLocal(cm.CreatedOn);
                            cm.LastUpdatedOn = TimeZoneManager.ToLocal(cm.LastUpdatedOn);
                        }                                      
                    connection.Close();
                    return Json(res);
                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult FindFranchiseTimeZone(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var TimezoneCode = connection.Query<int>(@"select f.TimezoneCode
                                                    from CRM.[Case] fc 
                                                    join Auth.Users u on u.PersonId = fc.CreatedBy
                                                    join CRM.Franchise f on f.FranchiseId = u.FranchiseId
                                                    where fc.CaseId =@CaseId", new { CaseId = id }).FirstOrDefault();
                    connection.Close();
                    if (TimezoneCode > 0)
                        return Json(((TimeZoneEnum)TimezoneCode).ToString());
                    else
                        return Json("");
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return this.ResponseResult(e);

            }

        }

        [HttpGet]
        public IHttpActionResult getUseridAndName(int id)
        {
            int UserId = 0;
            string UserName = "";
            if (SessionManager.CurrentUser.Person != null)
            {
                UserId = SessionManager.CurrentUser.PersonId;
                UserName = SessionManager.CurrentUser.UserName;
            }
            else if (SessionManager.SecurityUser.Person != null)
            {
                UserId = SessionManager.SecurityUser.PersonId;
                UserName = SessionManager.SecurityUser.UserName;
            }

            return Json(new { id = UserId, name = UserName });
        }

        [HttpGet]
        public IHttpActionResult loadUserlist(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    //           if (module == 1)
                    //           {
                    //               var res = connection.Query(@"select u.UserName name , p.PrimaryEmail content from Auth.Users u 
                    //                                           Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp' 
                    //                                           and u.FranchiseId = ( select FranchiseId from Auth.Users
                    //                                           where PersonId in (select Owner_PersonId from CRM.FranchiseCase where CaseId = @moduleId))
                    //                                            union
                    //                select u.UserName name , p.PrimaryEmail content from Auth.Users u
                    //                Inner join CRM.Person p on p.PersonId = u.PersonId
                    //                Inner join CRM.VendorCaseConfig vcf on vcf.CaseLogin = u.UserName
                    //                Inner Join acct.HFCVendors  hv on vcf.VendorId= hv.VendorIdPk
                    //                Inner Join CRM.QuoteLines ql on ql.VendorId = hv.VendorId
                    //                Inner Join CRM.FranchiseCaseAddInfo fcai on fcai.QuoteLineId= ql.QuoteLineId
                    //                where  fcai.CaseId=@moduleId", new { moduleId = id }).ToList();
                    //               connection.Close();
                    //               return Json(res);
                    //           }
                    //           if (module == 2)
                    //           {
                    //               var res = connection.Query(@"select u.UserName name , p.PrimaryEmail content from Auth.Users u
                    //                   Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp'
                    //                   and u.FranchiseId = ( select FranchiseId from Auth.Users
                    //                   where PersonId in (select Owner_PersonId from CRM.FranchiseCase where CaseId = @moduleId))
                    //                    	union
                    //select u.UserName name , p.PrimaryEmail content from Auth.Users u
                    //Inner join CRM.Person p on p.PersonId = u.PersonId
                    //Inner join CRM.VendorCaseConfig vcf on vcf.CaseLogin = u.UserName
                    //Inner Join acct.HFCVendors  hv on vcf.VendorId= hv.VendorIdPk
                    //Inner Join CRM.QuoteLines ql on ql.VendorId = hv.VendorId
                    //Inner Join CRM.FranchiseCaseAddInfo fcai on fcai.QuoteLineId= ql.QuoteLineId
                    //where  fcai.CaseId=@moduleId", new { moduleId = id }).ToList();
                    //               connection.Close();
                    //               return Json(res);
                    //           }
                    //           if (module == 3 || module == 4)
                    //           {
                    var res = connection.Query("select u.UserName name , p.PrimaryEmail content from Auth.Users u Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp' and u.FranchiseId = ( select FranchiseId from Auth.Users where PersonId in (select createdBy from CRM.[Case] where CaseId = @CaseId))", new { CaseId = id }).ToList();
                    connection.Close();
                    return Json(res);
                    //           }
                   // return Json(false);
                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
    }
}