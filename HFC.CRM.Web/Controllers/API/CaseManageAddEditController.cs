﻿using AddressValidationWebServiceClient;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class CaseManageAddEditController : BaseApiController
    {
        CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        //get vpo drop down value
        [HttpGet]
        public IHttpActionResult GetVPOData(int? id, int? orderid)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                //var query = @"  select pod.PurchaseOrdersDetailId,
                //                CAST(pod.PICPO as varchar) as VPO_PICPO_PODId,
                //                mpo.PurchaseOrderId,mpo.MasterPONumber,o.OrderId,o.OrderNumber,
                //                row_number() over (partition by pod.PICPO order by pod.PurchaseOrdersDetailId asc) rn 
                //                from CRM.PurchaseOrderDetails pod
                //                join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
                //                join CRM.Orders o on mpo.OrderId=o.OrderID
                //                join CRM.Opportunities op on mpo.OpportunityId=op.OpportunityId
                //                join CRM.Accounts a on a.AccountId = op.AccountId
                //                where op.FranchiseId=@FranchiseId and pod.PICPO!= 0  and pod.StatusId not in (8,10)";


                var query = @"select * from(select pod.PurchaseOrdersDetailId,
                                CONCAT(pod.PICPO, ' - ', q.SideMark) VPO_PICPO_PODId,
                                mpo.PurchaseOrderId, mpo.MasterPONumber, o.OrderId, o.OrderNumber,
                                row_number() over(partition by pod.PICPO order by pod.PurchaseOrdersDetailId asc) rn
                                from CRM.PurchaseOrderDetails pod
                                join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId = mpo.PurchaseOrderId
                                join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                join CRM.Orders o on mpox.OrderId = o.OrderID
                                join CRM.Opportunities op on mpox.OpportunityId = op.OpportunityId
                                join CRM.Accounts a on a.AccountId = op.AccountId
                                left
                                join crm.Quote q on q.QuoteKey = mpox.QuoteId
                                where op.FranchiseId = @FranchiseId
                                    and pod.PICPO != 0  and pod.StatusId not in (8, 9, 10) and";

                if (orderid != null && orderid != 0 && (id == null || id == 0)) query = query + "  mpox.OrderId = isnull(@Orderid, mpox.OrderId)) as p where rn = 1";
                else query = query + "  mpo.PurchaseOrderId = isnull(@MPOID, mpo.PurchaseOrderId)) as p where rn = 1";

                //var user = LocalMembership.GetUser(SessionManager.CurrentUser.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                //if (user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId) || user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId))
                //{
                //    query = @"select * from( " + query + " ) as p where rn=1";
                //}
                //else if (user.IsInRole(AppConfigManager.DefaultOwnerRole.RoleId) || user.IsInRole(AppConfigManager.DefaultOwnerRole.RoleId))
                //{
                //    query = @"select * from( " + query + " ) as p where rn=1";
                //}

                //else if (user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId) || user.IsInRole(AppConfigManager.DefaultSalesNoMargineRole.RoleId))
                //{
                //    query = @"select * from( " + query + " and op.SalesAgentId = " + SessionManager.CurrentUser.PersonId + " ) as p where rn=1";
                //}
                //else if (user.IsInRole(AppConfigManager.DefaultInstallRole.RoleId))
                //{
                //    query = @"select * from( " + query + " and op.InstallerId = " + SessionManager.CurrentUser.PersonId + " ) as p where rn=1";
                //}




                connection.Open();
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, MPOID = id, Orderid = orderid }).ToList();


                connection.Close();
                return Json(result);
            }
        }


        //get mpo drop down value
        [HttpGet]
        public IHttpActionResult GetMPOData(int id)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                //var query = @"select mpo.PurchaseOrderId,mpo.MasterPONumber as MPO_MasterPONum_POId from CRM.MasterPurchaseOrder 
                //              mpo join CRM.Opportunities op on mpo.OpportunityId=op.OpportunityId
                //              where op.FranchiseId=@FranchiseId  and  mpo.POStatusId not in (8,10)";

                // other than Cancelled, error and processing everything else should show up in Case managerment - Vasishta via skype.
                var query = @"select distinct mpo.PurchaseOrderId, CONCAT(mpo.MasterPONumber, ' - ', q.SideMark) as MPO_MasterPONum_POId 
                            from CRM.MasterPurchaseOrder mpo 
                                left join CRM.MasterPurchaseOrderextn  mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
	                            join CRM.Opportunities op on mpox.OpportunityId=op.OpportunityId
	                            left join crm.Quote q on q.QuoteKey = mpox.QuoteId
                            where op.FranchiseId=@FranchiseId  and  mpo.POStatusId not in (8, 9, 10)";

                if (id > 0) query = query + " and mpox.OrderId=@OrderId";

                //var user = LocalMembership.GetUser(SessionManager.CurrentUser.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                //if (user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId) || user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId))
                //{

                //}
                //else if (user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId) || user.IsInRole(AppConfigManager.DefaultSalesNoMargineRole.RoleId))
                //{
                //    query = query + " and op.SalesAgentId = " + SessionManager.CurrentUser.PersonId;
                //}
                //else if (user.IsInRole(AppConfigManager.DefaultInstallRole.RoleId))
                //{
                //    query = query + " and op.InstallerId = " + SessionManager.CurrentUser.PersonId;
                //}



                connection.Open();
                var result = connection.Query(query, new
                {
                    FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    OrderId = id
                }).ToList();
                connection.Close();
                return Json(result);
            }

        }

        //get order drop down value
        [HttpGet]
        public IHttpActionResult GetSalesOrderData(int id)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                //var query = @"select o.OrderId,CAST(o.OrderNumber as varchar) as SalesOrderId  from CRM.Orders o
                //              join CRM.MasterPurchaseOrderExtn mpox on mpox.OrderId=o.OrderId
                //              join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = mpox.PurchaseOrderId
                //              join CRM.Opportunities op on mpox.OpportunityId=op.OpportunityId
                //              where op.FranchiseId=@FranchiseId  and  mpo.POStatusId not in (8,9, 10)";

                var query = "";
                if (id > 0)
                    query = @"select  o.OrderId,CAST(o.OrderNumber as varchar) as SalesOrderId  from CRM.Orders o
                              join CRM.Opportunities op on op.OpportunityId = o.OpportunityId
                              join CRM.MasterPurchaseOrderExtn mpoe on mpoe.OrderId = o.OrderID
                              where op.FranchiseId = @FranchiseId and o.OrderStatus not in (6,9) and mpoe.PurchaseOrderId = @PurchaseOrderId";
                else
                    query = @"with cte as 
                                (
		                                select distinct o.orderid,o.orderNumber,
		                                case when o.OrderStatus not in (9,6) 
			                                then 1 else 0 end as voidcancelled,
		                                case when isnull(pod.StatusId,0) not in (8,9,10) and ql.ProductTypeId not in (4,5)
			                                then 1 else 0 end as linecancelled
			                                ,pod.StatusId
		                                from CRM.Orders o
		                                join crm.Quote q on q.QuoteKey=o.QuoteKey
		                                join crm.QuoteLines ql on q.QuoteKey=ql.QuoteKey
		                                join crm.Opportunities op on op.OpportunityId = q.OpportunityId
		                                left join crm.PurchaseOrderDetails pod on pod.QuoteLineId=ql.QuoteLineId
		                                where op.FranchiseId=@FranchiseId
                                ) select distinct
                                OrderId,CAST(OrderNumber as varchar) as SalesOrderId ,
                                case when voidcancelled!=0 and count( case when linecancelled=1 then orderid end)>0 then 1 else 0 end as createcancel
                                from cte 
                                group by orderid,orderNumber,linecancelled,voidcancelled order by orderid";





                // (6,9)remove cancelled and void statuses


                //var user = LocalMembership.GetUser(SessionManager.CurrentUser.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                //if (user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId) || user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId))
                //{

                //}
                //else if (user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId) || user.IsInRole(AppConfigManager.DefaultSalesNoMargineRole.RoleId))
                //{
                //    query = query + " and op.SalesAgentId = " + SessionManager.CurrentUser.PersonId;
                //}
                //else if (user.IsInRole(AppConfigManager.DefaultInstallRole.RoleId))
                //{
                //    query = query + " and op.InstallerId = " + SessionManager.CurrentUser.PersonId;
                //}




                connection.Open();
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, PurchaseOrderId = id }).ToList();

                connection.Close();
                if (id > 0)
                    return Json(result);
                else
                    return Json((from aa in result where aa.createcancel == 1 select aa).ToList());
            }

        }

        //get details on initial page load
        [HttpGet]
        public IHttpActionResult GetUserDetails(int id)
        {
            try
            {
                var Data = Case_Mangr.UserDetails(id);
                return Json(Data);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }

        //get other value on select vpo
        [HttpGet]
        public IHttpActionResult ChangeValue(int id, string orderIds)
        {
            try
            {
                var data = Case_Mangr.GetValue(id, orderIds);
                return Json(data);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        //get lines for non vpo
        [HttpGet]
        public IHttpActionResult getDataForNonVPO(int id, int Mpo)
        {
            try
            {
                var data = Case_Mangr.getDataForNonVPO(id, Mpo);
                return Json(data);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }




        //get other value on select order
        [HttpGet]
        public IHttpActionResult ChangeSalesorderValue(int id, int lineNumber, string orderStr, int poid, int podid)
        {
            try
            {
                var data = Case_Mangr.GetMpoValue(id, lineNumber, orderStr, poid, podid);
                return Json(data);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        //get other value on select mpo
        [HttpGet]
        public IHttpActionResult ChangeonSelect(int id, string orderIds)
        {
            try
            {
                var data = Case_Mangr.GetSalesOrder(id, orderIds);
                return Json(data);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
        //get saved grid value
        [HttpGet]
        public IHttpActionResult GetCase(int id, int CaseId)
        {
            try
            {
                var Value = Case_Mangr.GetData(id, CaseId);
                if (Value != null)
                {
                    foreach (FranchiseCaseAddInfoModel fcai in Value)
                    {
                        fcai.CreatedOn = TimeZoneManager.ToLocal(fcai.CreatedOn);
                        fcai.LastUpdatedOn = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                        //fcai.date = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                        //fcai.LastUpdatedOn = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                    }
                    return Json(Value);
                }
                else return null;
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }

        //get saved grid value
        [HttpGet]
        public IHttpActionResult GetFranchiseCaseVendorView(int id, string FCAIvendorCaseNo)
        {
            try
            {
                var Value = Case_Mangr.GetFranchiseCaseVendorView(id, FCAIvendorCaseNo);
                foreach (FranchiseCaseAddInfoModel fcai in Value)
                {
                    fcai.CreatedOn = TimeZoneManager.ToLocal(fcai.CreatedOn);
                    fcai.LastUpdatedOn = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                    //fcai.date = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                    //fcai.LastUpdatedOn = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                }
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }

        //get quote-line based on option selected to grid
        [HttpGet]
        public IHttpActionResult GetLineData(int id, int Mpoid, string linenos, string lineNo, string OrderId)
        {
            try
            {
                if (linenos == null) linenos = "0";
                var Value = Case_Mangr.GetLineData(id, Mpoid, linenos, lineNo, OrderId);
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        //after selecting quoteline in grid fetch value for display
        [HttpGet]
        public IHttpActionResult GetFullDetails(int id, int OrderId, int POID = 0, int PODID = 0)
        {
            try
            {
                var Value = Case_Mangr.GetOtherLineData(id, OrderId, POID, PODID);
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetLinesForVpoChange(int id, int lineno, int OrderId)
        {
            try
            {
                var Value = Case_Mangr.GetLinesForVpoChange(id, lineno, OrderId);
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetLinesForVpoChangeDropchange(int id)
        {
            try
            {
                var Value = Case_Mangr.GetLinesForVpoChangeDropchange(id);
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }




        [HttpGet]
        public IHttpActionResult GetLinesOrderAndLineno(int id, int lineno)
        {
            try
            {
                var Value = Case_Mangr.GetLinesOrderAndLineno(id, lineno);
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        [HttpGet]
        public IHttpActionResult GetLinesForPurchaseOrderIdAndLineNo(int id, int lineno, int OrderId)
        {
            try
            {
                var Value = Case_Mangr.GetLinesForPurchaseOrderIdAndLineNo(OrderId, id, lineno);
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }





        //get grid value case dropdown
        [HttpGet]
        public IHttpActionResult GetCaseType(int id, int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as Type,Name as Typevalue from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return Json(result);
            }

        }

        //get grid value reason dropdown
        [HttpGet]
        public IHttpActionResult GetCaseReason(int id, int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as ReasonCode,Name as Reasonvalue from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return Json(result);
            }

        }

        //get grid value status dropdown
        [HttpGet]
        public IHttpActionResult GetCaseStatus(int id, int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as Status,Name as Statusvalue from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return Json(result);
            }

        }

        //get grid value resolution dropdown
        [HttpGet]
        public IHttpActionResult GetCaseResolution(int id, int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as Resolution,Name as Resolutionvalue from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return Json(result);
            }

        }

        //save data to db
        [HttpPost]
        public IHttpActionResult SaveFranchiseCase(FranchiseCaseModel model)
        {
            try
            {


                if (model.CaseId == 0)
                {
                    var Data_id = Case_Mangr.Save_Initial(model);
                    var spl = Data_id.Split(new string[] { "|" }, StringSplitOptions.None);
                    //  model.CaseId = Data_id;
                    model.CaseNumber = Convert.ToInt32(spl[0]);
                    model.CaseId = Convert.ToInt32(spl[1]);
                    var Value = Case_Mangr.Update_Data(model);
                    return Json(Value);
                    //  return Json(Data_id);
                }
                else
                {
                    var Value = Case_Mangr.Update_Data(model);
                    return Json(Value);
                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        //get data once after save
        [HttpGet]
        public IHttpActionResult GetSavedValue(int Id)
        {
            try
            {
                var GetDetails = Case_Mangr.GetDetailes(Id);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        //get data once after save
        [HttpGet]
        public IHttpActionResult GetSavedValueHO(int Id)
        {
            try
            {
                var GetDetails = Case_Mangr.GetDetailesHO(Id);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetSavedValueFranchiseVendor(int Id, string vendorcaseid)
        {
            try
            {
                var GetDetails = Case_Mangr.GetDetailesFranchiseVendor(Id, vendorcaseid);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        public IHttpActionResult getVendorId(int Id)
        {
            try
            {
                var GetDetails = Case_Mangr.getVendorId(Id);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        // FOr Franchise Case List        
        [HttpGet]
        public IHttpActionResult GetFranchiseCaseList(int Id, int SelectedCaseType, bool Closed)
        {
            try
            {
                var result = Case_Mangr.GetFranchiseCaseList(SelectedCaseType, Closed, SessionManager.CurrentFranchise.FranchiseId, null);
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "CaseManagement ");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "CaseManagementAllRecordAccess").CanAccess;
                if (result != null)
                {
                    if (specialPermission == false)
                    {
                        // Need to apply the filter for SalesAgent/Installation.
                        var filtered = result.Where(x => (x.SalesAgentId ==
                                            SessionManager.CurrentUser.PersonId ||
                                            x.InstallerId == SessionManager.CurrentUser.PersonId)
                                            ).ToList();

                        result = filtered;
                    }
                }

                if (result != null)
                {
                    foreach (FranchiseCaseList fcl in result)
                    {
                        //  fcl.DateTimeOpened = TimeZoneManager.ToLocal(fcl)
                        fcl.CreatedOn = TimeZoneManager.ToLocal(fcl.CreatedOn);
                    }
                    return Json(result);
                }
                else
                    return Json(new List<FranchiseCaseList>());
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        // FOr Franchise Case List        
        [HttpGet]
        public IHttpActionResult GetAllCaseList(int Id, int SelectedCaseType, int? FranchiseID, int? VendorID, bool Closed)
        {
            try
            {
                var result = Case_Mangr.GetFranchiseCaseList(SelectedCaseType, Closed, FranchiseID, VendorID);
                //var GetPermission = permissionManager.GetUserRolesPermissions();
                //var ModuleData = GetPermission.Find(x => x.ModuleCode == "CaseManagement ");
                //var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "CaseManagementAllRecordAccess").CanAccess;
                //if (result != null)
                //{
                //if (specialPermission == false)
                //{
                //    // Need to apply the filter for SalesAgent/Installation.
                //    var filtered = result.Where(x => (x.SalesAgentId ==
                //                        SessionManager.CurrentUser.PersonId ||
                //                        x.InstallerId == SessionManager.CurrentUser.PersonId)
                //                        ).ToList();

                //    result = filtered;
                //}
                //}

                if (result != null)
                {
                    foreach (FranchiseCaseList fcl in result)
                    {
                        //  fcl.DateTimeOpened = TimeZoneManager.ToLocal(fcl)
                        fcl.CreatedOn = TimeZoneManager.ToLocal(fcl.CreatedOn);
                    }
                    return Json(result);
                }
                else
                    return Json(new List<FranchiseCaseList>());
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //delete particular grid from db
        [HttpGet]
        public IHttpActionResult Delete(int id, int CaseId)
        {
            try
            {
                var result = Case_Mangr.DeleteData(id, CaseId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //clone/copy the grid value to db
        [HttpPost]
        public IHttpActionResult CopyCaseLine(int id, [FromBody] FranchiseCaseAddInfoModel model)
        {
            try
            {
                var result = Case_Mangr.CopyContent(id, model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //attachment code
        [HttpGet]
        public IHttpActionResult getlineIdList(int Id, string Module)
        {
            try
            {
                var res = Case_Mangr.getlineIdList(Id, Module);
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

            //  return null;
        }

        [HttpGet]
        public IHttpActionResult getFranchiseTimeZone(int CaseId)
        {
            try
            {
                return Json(Case_Mangr.getFranchiseTimeZone(CaseId).TimezoneCode.ToString());
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult getfranchiseCaseDocList(int Id, string Module, bool flag)
        {
            try
            {
                var res = Case_Mangr.getfranchiseCaseDocList(Id, Module);

                if (flag == true)
                {
                    var tz = Case_Mangr.getFranchiseTimeZone(Id).TimezoneCode;
                    foreach (var r in res)
                    {
                        if (r.CreatedOn == null)
                        {
                            r.fileSize = r.fileSize.Insert(10, " " + tz.ToString() + " ");
                        }
                        else
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)r.CreatedOn, tz);
                            r.fileSize = r.fileSize.Substring(10, r.fileSize.Length - 10);
                            r.fileSize = lt.Date.GlobalDateFormat() + " " + tz.ToString() + r.fileSize;
                        }
                        //  str.Substring(10, str.Length - 10)



                    }
                }
                else
                {
                    var tz = Case_Mangr.getFranchiseTimeZone(Id).TimezoneCode;
                    foreach (var r in res)
                    {
                        if (r.CreatedOn != null)
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)r.CreatedOn, tz);
                            r.fileSize = r.fileSize.Substring(10, r.fileSize.Length - 10);
                            r.fileSize = lt.Date.GlobalDateFormat() + r.fileSize;
                        }
                        //  str.Substring(10, str.Length - 10)



                    }

                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

            //  return null;
        }


        [HttpGet]
        public IHttpActionResult getAccOrderOpportunity(int id)
        {
            try
            {

                var res = Case_Mangr.getAccOrderOpportunity(id);
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }


        [HttpPost]
        public IHttpActionResult removeFranchiseCaseDetail(int Id)
        {
            try
            {
                var res = Case_Mangr.removeFranchiseCaseDetail(Id);
                return Json(true);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        [HttpPost]
        public IHttpActionResult removeFranchiseCaseDetails(int Id, int CaseId)
        {
            try
            {
                var res = Case_Mangr.removeFranchiseCaseDetails(Id, CaseId);
                return Json(true);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }
        [HttpPost]
        public IHttpActionResult changeLineId(int Id, int VendorId)
        {
            try
            {
                var res = Case_Mangr.changeLineId(Id, VendorId);
                return Json(true);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        [HttpGet]
        public IHttpActionResult GetCaseHistoryForCase(int id)
        {
            try
            {
                var res = Case_Mangr.GetCaseHistoryForCase(id);

                foreach (HistoryTable ht in res)
                {
                    ht.Date = TimeZoneManager.ToLocal(ht.Date);
                    ht.LastUpdatedOn = TimeZoneManager.ToLocal(ht.LastUpdatedOn);
                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }


        [HttpGet]
        public IHttpActionResult GetFollowOrNot(int id)
        {
            try
            {
                var res = Case_Mangr.GetFollowOrNot(id);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        [HttpGet]
        public IHttpActionResult GetFollow(int id)
        {
            try
            {
                var res = Case_Mangr.GetFollowOrNotForVendorView(id);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        [HttpGet]
        public IHttpActionResult UnfollowThisCase(int id, int module)
        {
            try
            {
                var res = Case_Mangr.UnfollowThisCase(id, module);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        [HttpGet]
        public IHttpActionResult FollowThisCase(int id, int module)
        {
            try
            {
                var res = Case_Mangr.FollowThisCase(id, module);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetKanbanData(int Id, int SelectedCaseType, bool Closed)
        {
            try
            {
                var res = Case_Mangr.GetFranchiseKanbanCaseList(SelectedCaseType, Closed, Id);
                foreach (var item in res)
                {
                    item.UserColor = Case_Mangr.GetUserTextColor(SessionManager.CurrentUser.PersonId);
                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateCaseStatus(int id, string CaseStatus)
        {
            try
            {

                var res = Case_Mangr.UpdateCaseStatus(id, CaseStatus);
                return Json(true);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }
        [HttpPost]
        public IHttpActionResult SaveGridLine(int id, [FromBody]FranchiseCaseModel model)
        {
            try
            {
                var result = Case_Mangr.Save_Grid(id, model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //get the grid line data value for vendor-case Management.
        [HttpGet]
        public IHttpActionResult GetFranchiseVendorCaseValue(int id, int caseid)
        {
            try
            {
                var result = Case_Mangr.GetfranchiseCase(caseid);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //get details data for vendor-case Management.
        [HttpGet]
        public IHttpActionResult GetFranchiseVendorCase(int id, int caseid)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    //var GetDetails = Case_Mangr.GetDetailes(caseid);
                    var GetDetails = Case_Mangr.GetFranchiseDetails(caseid);

                    var query = @"select f.* from CRM.VendorCase vc
                                      join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey                                    
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where vc.VendorCaseId = @VendorCaseId ";
                    var res = connection.Query(query, new { VendorCaseId = caseid }).FirstOrDefault();
                    connection.Close();

                    if (GetDetails != null) GetDetails.CreatedOn = TimeZoneManager.ToLocal(GetDetails.CreatedOn, (TimeZoneEnum)res.TimezoneCode);
                    if (GetDetails != null) GetDetails.LastUpdatedOn = TimeZoneManager.ToLocal(GetDetails.LastUpdatedOn, (TimeZoneEnum)res.TimezoneCode);
                    if (GetDetails != null) GetDetails.DateTimeClosed = TimeZoneManager.ToLocal(GetDetails.DateTimeClosed, (TimeZoneEnum)res.TimezoneCode);


                    //if (GetDetails != null) GetDetails.CreatedOn = Util.ConvertDate(GetDetails.CreatedOn, (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), res.TimezoneCode.ToString()), TimeZoneEnum.UTC);
                    //if (GetDetails != null) GetDetails.LastUpdatedOn = Util.ConvertDate(GetDetails.LastUpdatedOn, (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), res.TimezoneCode.ToString()), TimeZoneEnum.UTC);
                    //if (GetDetails != null) GetDetails.DateTimeClosed = Util.ConvertDate(GetDetails.DateTimeClosed, (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), res.TimezoneCode.ToString()), TimeZoneEnum.UTC);
                    return Json(GetDetails);

                }

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //save particular line item for vendor-case Management.
        [HttpPost]
        public IHttpActionResult SaveVendorCase(int id, [FromBody]VendorCaseAddInfoModel model)
        {
            try
            {
                var result = Case_Mangr.SaveVendor(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        [HttpPost]
        public IHttpActionResult SaveVendorCaseVendorView(int id, [FromBody]VendorCaseAddInfoModel model)
        {
            try
            {
                var result = Case_Mangr.SaveVendorVendorView(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //save all (whole line item) for vendor-case Management.
        [HttpPost]
        public IHttpActionResult SaveVendorCaseComplete(int id, [FromBody]List<VendorCaseAddInfoModel> model)
        {
            try
            {
                var result = Case_Mangr.Complete_Convert(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }



        [HttpPost]
        public IHttpActionResult SaveVendorCaseCompleteVendorview(int id, [FromBody]List<VendorCaseAddInfoModel> model)
        {
            try
            {
                var result = Case_Mangr.Complete_ConvertVendorView(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        //List page vendor kendo grid
        [HttpGet]
        public IHttpActionResult GetVendorGridList(int id, int SelectedCase, bool Closed, int FranchiseId)
        {
            try
            {
                var result = Case_Mangr.VendorGridList(SelectedCase, Closed, FranchiseId);

                foreach (var obj in result)
                {
                    obj.Timezone = obj.TimezoneEnum.ToString();
                    obj.CreatedOn = TimeZoneManager.ToLocal(obj.CreatedOn, obj.TimezoneEnum);
                    obj.IncidentDate = TimeZoneManager.ToLocal(obj.IncidentDate, obj.TimezoneEnum);
                    obj.LastUpdatedOn = TimeZoneManager.ToLocal(obj.LastUpdatedOn, obj.TimezoneEnum);
                }
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //Save the vendor case line value.
        [HttpPost]
        public IHttpActionResult SaveCaseVendorDetails(int id, [FromBody]List<VendorCaseDetailInfo> model)
        {
            try
            {
                var result = Case_Mangr.SaveVendorGridData(id, model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //drop down value for tripcharge in vendor case line drop-down
        [HttpGet]
        public IHttpActionResult GetTripChargeValue(int id, int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as TripChargeApproved,Name as TripChargevalue from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return Json(result);
            }

        }
        //get the franchise for the list-grid drop-down box.
        [HttpGet]
        public IHttpActionResult GetFranchiseList(int id)
        {
            try
            {
                var result = Case_Mangr.GetFranchiseData();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        //get the accntid,opporid,caseid,vendorcaseid for the calendar 
        [HttpGet]
        public IHttpActionResult getCompleteDetails(int Id)
        {
            try
            {
                var result = Case_Mangr.GetVenodorDetailsCal(Id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        //get Vendor Case-history details.
        [HttpGet]
        public IHttpActionResult GetVendorCaseHistoryForCase(int id)
        {
            try
            {
                var res = Case_Mangr.GetVendorCaseHistoryForCase(id);
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetdetailsForMpoLineno(int id, int PurchaseOrderId, int LineNo)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var result = connection.Query<PurchaseOrderDetails>(@"Select  pod.* from CRM.PurchaseOrderDetails pod
                                                                    join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
                                                                    join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId
                                                                    join CRM.Orders o on mpox.OrderId=o.OrderID
                                                                    join CRM.Quote q on o.QuoteKey=q.QuoteKey
                                                                    join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                                                                    where mpo.PurchaseOrderId = @PurchaseOrderId and ql.QuoteLineNumber=@QuoteLineNumber",
                                                                    new { PurchaseOrderId = PurchaseOrderId, QuoteLineNumber = LineNo }).FirstOrDefault();


                if (result.PICPO != null && result.PICPO != 0)
                {
                    var res = connection.Query<PurchaseOrderDetails>(@"with cte as
                                                                    (
                                                                    Select  pod.*
                                                                    ,row_number() over (partition by pod.PICPO order by pod.PurchaseOrdersDetailId asc) rn  
                                                                    ,ql.QuoteLineNumber
                                                                    from CRM.PurchaseOrderDetails pod
                                                                    join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
                                                                    join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId
                                                                    join CRM.Orders o on mpox.OrderId=o.OrderID
                                                                    join CRM.Quote q on o.QuoteKey=q.QuoteKey
                                                                    join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                                                                    where mpo.PurchaseOrderId = @PurchaseOrderId and pod.PICPO=@PICPO
                                                                    ) select * from cte where rn=1",
                                                                        new { PurchaseOrderId = PurchaseOrderId, PICPO = result.PICPO }).FirstOrDefault();





                    connection.Close();
                    return Json(res);
                }
                else
                {
                    connection.Close();
                    return Json(result);
                }
            }


        }

        [HttpGet]
        public IHttpActionResult getLineNumber(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var result = connection.Query(@"select ql.QuoteLineId, ql.QuoteLineNumber from CRM.QuoteLines ql
                                                                        join CRM.PurchaseOrderDetails pod on ql.QuoteLineId = pod.QuoteLineId
                                                                        where PurchaseOrdersDetailId = @PurchaseOrdersDetailId",
                                                                       new { PurchaseOrdersDetailId = id }).FirstOrDefault();

                    connection.Close();
                    if (result != null)
                        return Json(new { QuoteLineId = result.QuoteLineId, QuoteLineNumber = result.QuoteLineNumber });
                    else
                        return Json("");



                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Json("");

            }
        }

        [HttpGet]
        public IHttpActionResult getMPOforOrder(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var res = connection.Query<dynamic>(@"with cte as 
                                                            (
		                                                            select distinct o.orderid,
		                                                            case when o.OrderStatus not in (9,6) 
			                                                            then 1 else 0 end as voidcancelled,
		                                                            case when isnull(pod.StatusId,0) not in (8,9,10) and ql.ProductTypeId not in (4,5)
			                                                            then 1 else 0 end as linecancelled
			                                                            ,pod.StatusId
		                                                            from CRM.Orders o
		                                                            join crm.Quote q on q.QuoteKey=o.QuoteKey
		                                                            join crm.QuoteLines ql on q.QuoteKey=ql.QuoteKey
		                                                            left join crm.PurchaseOrderDetails pod on pod.QuoteLineId=ql.QuoteLineId
		                                                            where orderid=@orderid
                                                            )
                                                             select 
                                                            orderid,
                                                            case when voidcancelled !=0 and count( case when linecancelled=1 then orderid end) > 0 then 1 else 0 end as Createcase
                                                            from cte 
                                                            group by orderid,linecancelled,voidcancelled order by Createcase desc", new { orderid = id }).ToList();
                    connection.Close();
                    if (res.Count > 0)
                    {
                        if (res[0].Createcase == 1)
                            return Json(id);
                        else
                            return Json("");
                    }
                    else
                    {
                        return Json("");
                    }


                    //   return Json(true);
                    //    var res1 = connection.Query<PurchaseOrderDetails>(@"select * from CRM.MasterPurchaseOrderExtn where OrderId=@OrderId", new { OrderId = id }).ToList();

                    //    if (res1.Count > 0)
                    //    {
                    //        var result = connection.Query<PurchaseOrderDetails>(@"select pod.* from CRM.PurchaseOrderDetails pod
                    //                                                        join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = pod.PurchaseOrderId
                    //                                                        join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                    //                                                        join CRM.Orders o on o.OrderID = mpox.OrderId
                    //                                                        where o.OrderID=@OrderId  and pod.StatusId not in (8,9,10)",
                    //                                                            new { OrderId = id }).FirstOrDefault();
                    //        if (result != null)
                    //        {
                    //            connection.Close();
                    //            return Json(result.PurchaseOrderId);
                    //        }
                    //        else
                    //        {
                    //            connection.Close();
                    //            return Json("");
                    //        }
                    //    }
                    //    else
                    //    {
                    //        var resultt = connection.Query<PurchaseOrderDetails>(@"select ql.* from CRM.QuoteLines ql
                    //                                                                join CRM.Quote q on q.QuoteKey = ql.QuoteKey
                    //                                                                join CRM.Orders o on o.QuoteKey = q.QuoteKey
                    //                                                                where o.OrderID=@OrderId  and o.OrderStatus <> 6 and o.OrderStatus <> 9",
                    //                                                        new { OrderId = id }).FirstOrDefault();
                    //        connection.Close();
                    //        if (resultt != null)
                    //            return Json(id);
                    //        else
                    //            return Json("");
                    //    }



                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Json("");

            }

        }

        [HttpGet]
        public IHttpActionResult GetAllFranchiseList(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var result = connection.Query<Franchise>(@"select FranchiseId,Name from CRM.Franchise
                    where IsDeleted=0 and IsSuspended=0 order by Name Asc").ToList();

                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllVendorList(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var result = connection.Query<DTO.Product.ProductListNameModel>(@";with cte as (
				  select distinct F.VendorId,V.Name,V.VendorType from Acct.FranchiseVendors F inner join Acct.HFCVendors V on F.VendorId=V.VendorId
				  and F.VendorStatus=1
				  union
				  select distinct V.VendorId, (F.Code + ' - ' + V.Name )as Name,V.VendorType from Acct.FranchiseVendors V inner join crm.Franchise F on V.FranchiseId=F.FranchiseID
				  where VendorType=3 and VendorStatus=1
				  ) select * from cte order by VendorType,Name").ToList();

                return Json(result);
            }
        }
    }
}
