﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HFC.CRM.Web.Filters;
using HFC.CRM.Managers;
using HFC.CRM.Core.Common;
using System.Web.Http;
using HFC.CRM.Data;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class PricingManagementController : BaseApiController
    {
        readonly PricingManager pricingManager = new PricingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        readonly ProductsManager productManager = new ProductsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        readonly VendorManager vendorManager = new VendorManager();
        public IHttpActionResult GetFranchiseLevel()
        {
            var response = pricingManager.Get();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }

        public IHttpActionResult GetVendorDropdown()
        {
            var response = vendorManager.GetVendorPricingDropDown();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }
        
        public IHttpActionResult GetVendorDropdown(int ProductCategory)
        {
            var response = vendorManager.FilterVendorPricingDropDown(ProductCategory);
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }
        public IHttpActionResult GetProductCategoryDropdown()
        {
            var response = productManager.GetProductCategoryDropDown();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }
        public IHttpActionResult GetPricingStrategyDropdown()
        {
            var response = pricingManager.GetPriceStrategyTypeDropDown();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }
        public IHttpActionResult GetAdvancedPricing()
        {
            var response = pricingManager.GetAdvancedPricing();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }
        public IHttpActionResult SaveAdvancedPricing(PricingSettings model)
        {
            var status = "Invalid data";
            if(model.MarkUp == null)
            {
                model.MarkUpFactor = "";
            }
            if (model != null)
            {
                model.IsActive = true;
                status = pricingManager.Save(model);
            }
            var response = pricingManager.GetAdvancedPricing();
            //if (status == ContantStrings.Success)
            //{
                return Json(new { error = status, data = response });
            //}
            //else
            //{
            //    return Json(new { error = "Unknown error occurred.", data = response });
            //}

        }
        
        public IHttpActionResult SaveFranchiseLevel(PricingSettings model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                model.IsActive = true;
                status = pricingManager.FranchiseLevelSave(model);
            }
            return Json(status);
        }
        public IHttpActionResult Delete(int Id)
        {
            var status = "Invalid data";
            if (Id > 0)
            {
                status = pricingManager.PricingDelete(Id);
            }
            return Json(status);
        }
    }
}
