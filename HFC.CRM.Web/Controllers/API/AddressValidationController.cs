﻿using AddressValidationWebServiceClient;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Address;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class AddressValidationController : BaseApiController
    {
        AddressValidationManager Mgr = new AddressValidationManager();
        PICManager pic = new PICManager();

        [HttpGet]
        public IHttpActionResult SmartyStreetSuggession(string prefix, string prefer = "", string country = "us")
        {
            try
            {
                var obj = new List<Suggestion>();
                var charcount = Regex.Matches(prefix, @"[a-zA-Z]").Count;
                if (charcount == 0 || country.ToUpper() != "US") return Json(obj);

                var res = new SmartyStreetResponse();
                res = Mgr.SmartyStreetSuggession(prefix, prefer);

                if (res.suggestions != null)
                {
                    foreach (var item in res.suggestions)
                    {
                        item.city_line = item.city + " " + item.state;
                    }
                    return Json(res);
                }
                return Json(obj);
            }

            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                var obj = new List<Suggestion>();
                return Json(obj);
            }
        }

        [HttpPost]
        public IHttpActionResult GetFEDExAddressValidation(FEDExRequest req)
        {
            try
            {
                var res = Mgr.AddressValidation(req);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult ValidateCustomerAddress(ValidateAddress model)
        {
            try
            {
                ValidateCustomerAddress req = new ValidateCustomerAddress()
                {
                    Address1 = model.address1,
                    Address2 = model.address2,
                    City = model.City,
                    State = model.StateOrProvinceCode,
                    Zip = model.PostalCode,
                    Country = model.CountryCode
                };

                if (req.Address1 == null || req.Address1 == "")
                {
                    ValidateCustomerAddress_Res resp = new ValidateCustomerAddress_Res() { valid = false, message = "address1 is required for Address validation" };
                    return Json(resp);
                }

                if (req.Address2 == null)
                    req.Address2 = "";
                if (req.City == null)
                    req.City = "";
                if (req.State == null)
                    req.State = "";
                if (req.Zip == null)
                    req.Zip = "";
                if (req.Country == null)
                    req.Country = "";

                var res = pic.ValidateCustomerAddress(req);

                ValidateAddressResponse addres = new ValidateAddressResponse();

                if (res.valid == true)
                {
                    addres.Status = "true";
                    addres.Message = "";

                    if (res.addresses.Length > 0)
                    {
                        List<ValidateAddress> lstvadd = new List<ValidateAddress>();
                        ValidateAddress vadd = new ValidateAddress();
                        vadd.address1 = res.addresses[0].Address1;
                        vadd.address2 = res.addresses[0].Address2;
                        vadd.City = res.addresses[0].City;
                        vadd.StateOrProvinceCode = res.addresses[0].State;
                        vadd.PostalCode = res.addresses[0].Zip;
                        vadd.CountryCode = res.addresses[0].Country;
                        lstvadd.Add(vadd);
                        addres.AddressResults = lstvadd;
                    }
                }
                else
                {
                    addres.Status = "false";
                    addres.Message = res.message;
                }

                return Json(addres);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

    }
}
