﻿using HFC.CRM.Core.Common;
using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;

using HFC.CRM.DTO.Address;
using HFC.CRM.DTO.Notes;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using Newtonsoft.Json;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    using System.Collections.Generic;

    using HFC.CRM.Web.Models.Color;
    using HFC.CRM.Web.Filters;

    [LogExceptionsFilter]
    public class ColorController : BaseApiController
    {
        readonly ColorsManager colorsManager = new ColorsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);

        public IHttpActionResult Get()
        {
            return ResponseResult("Success", Json(this.colorsManager.GetColors()));
        }

        public IHttpActionResult Get(int id)
        {
            return ResponseResult("Success", Json(this.colorsManager.GetGategoryColors(id)));
        }

        public IHttpActionResult Post(ColorEditModel model)
        {
            var status = "Invalid data";
            int colorId = 0;
            if (model != null)
            {
                status = colorsManager.AddColor(model.ColorName, out colorId);
                if (status == "Success" && model.CategoryId.HasValue)
                {
                    colorsManager.SetGategoryColors(model.CategoryId.Value, new List<int>() { colorId }, false);
                }
            }
            return ResponseResult(status, Json(new { Id = colorId }));
        }

        public IHttpActionResult Put([FromUri]int id, SetColorsToCategoryModel model)
        {
            colorsManager.SetGategoryColors(id, model != null ? model.Colors : new List<int>());
            return ResponseResult(ContantStrings.Success);
        }

        [HttpGet]
        public IHttpActionResult Headercolor([FromUri]int id, SetColorsToCategoryModel model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var res = connection.Query(" Select * from CRM.AppConfig where GroupName=@GroupName and SubGroupName=@SubGroupName and Name=@Name ", new { GroupName = "UI", SubGroupName = "Header", Name = "Color" }).ToList();
                connection.Close();

                return Json(res[0].Value);


            }
        }


         
     }
}
