﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.IO;
using System.Linq;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    /// <summary>
    /// For more google calendar push notification, go to https://developers.google.com/google-apps/calendar/v3/push
    /// </summary>
    [LogExceptionsFilter]
    public class NotificationsController : BaseApiController
    {    
        #region Example notification HTTP POST from Google
        //POST https://mydomain.com/notifications // Your receiving URL.
        //Content-Type: application/json; utf-8
        //Content-Length: 0
        //X-Goog-Channel-ID: 4ba78bf0-6a47-11e2-bcfd-0800200c9a66
        //X-Goog-Channel-Token: 398348u3tu83ut8uu38
        //X-Goog-Channel-Expiration: 1367869013915
        //X-Goog-Resource-ID:  ret08u3rv24htgh289g
        //X-Goog-Resource-URI: https://www.googleapis.com/calendar/v3/calendars/my_calendar@gmail.com/events
        //X-Goog-Resource-State:  exists
        //X-Goog-Message-Number: 10

        //Header	                Description
        //Always present
        //X-Goog-Channel-ID	        UUID or other unique string you provided to identify this notification channel.
        //X-Goog-Message-Number	    Integer that identifies this message for this notification channel. Value is always 1 for sync messages. Message numbers increase for each subsequent message on the channel, but they are not sequential.
        //X-Goog-Resource-ID	    An opaque value that identifies the watched resource. This ID is stable across API versions.
        //X-Goog-Resource-State	    The new resource state, which triggered the notification. Possible values: sync, exists, or not_exists.
        //X-Goog-Resource-URI	    An API-version-specific identifier for the watched resource.
        //Sometimes present
        //X-Goog-Channel-Expiration	Date and time of notification channel expiration, expressed in human-readable format. Only present if defined.
        //X-Goog-Channel-Token	    Notification channel token that was set by your application, and that you can use to verify the source of notification. Only present if defined.

        //Exponential Backoff: https://developers.google.com/drive/handle-errors
        //The algorithm is set to terminate when n is 5. This ceiling prevents clients from retrying infinitely, and results in a total delay of around 32 seconds before a request is deemed "an unrecoverable error."
        #endregion

        public IHttpActionResult Post()
        {            
            //channel id is the UserId
            var channelId = Request.GetHeaderValue("X-Goog-Channel-ID");
            var userId = Request.GetHeaderValue("X-Goog-Channel-Token");
            var msgNum = Util.CastObject<int>(Request.GetHeaderValue("X-Goog-Message-Number"));            
            var state = Request.GetHeaderValue("X-Goog-Resource-State");
#if DEBUG
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("/logs")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("/logs"));
            var sw = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("/logs/googlepush.log"), true);            
            sw.WriteLine(string.Format("{0} - {1} - {2} - {3}", state, channelId, userId, msgNum));
            sw.Flush();
#endif
            if (state == "sync" || state == "not_exists")
                return Ok();

            //Not necessary, expire date is given to us on subscription and doesn't change until we re-subscribe on expiration
            //DateTime? ExpiresOnUtc = null;            
            //string timestamp = GetFirstHeaderValue("X-Goog-Channel-Expiration");
            //if (!string.IsNullOrEmpty(timestamp))
            //{
            //    DateTime date;
            //    if (DateTime.TryParse(timestamp, out date))
            //    {
            //        ExpiresOnUtc = date.ToUniversalTime();
            //    }                
            //}            

            var isBusy = CacheManager.GetCache<bool>(channelId);

            string result = "Channel " + channelId + " Is Busy";
            try
            {
                if (!isBusy)
                {
                    CacheManager.SetCache(channelId, true);
                    var syncExist = this.CRMDBContext.Sync_Tracker.Where(f => f.ChannelId == channelId).Select(s => s.UserId).FirstOrDefault();
                    if (syncExist != null && syncExist.ToString().Equals(userId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        GoogleManager mgr = new GoogleManager(userId);
                        result = mgr.SyncChannel_Down(msgNum);
                    }
                    else result = "Does not exist";
                    CacheManager.RemoveCache(channelId);
                }
            }
            catch (Exception Ex)
            {
                result = ApiEventLogger.LogEvent(Ex);
                CacheManager.RemoveCache(channelId);
            }
#if DEBUG
            sw.WriteLine(result);                
            sw.Close();
#endif
            if (result == GoogleManager.Success)
                return Ok();
            else if (result == "Does not exist")
                return BadRequest("Channel does not exist"); //this will tell google to stop trying
            else
                return InternalServerError(new Exception(result)); //this will alert google so they can retry
            
        }
        
        

	}
}