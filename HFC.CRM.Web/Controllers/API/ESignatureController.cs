﻿using HFC.CRM.Data.Context;

namespace HFC.CRM.Web.Controllers.API
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Http;

    using HFC.CRM.Core.Common;
    using HFC.CRM.Core.Extensions;
    using HFC.CRM.Data;
    using HFC.CRM.Managers;
    using HFC.CRM.Managers.Docusign;
    using HFC.CRM.Web.Filters;

    [LogExceptionsFilter]
    public class ESignatureController : BaseApiController
    {
        #region Public Methods and Operators

        [HttpGet]
        public IHttpActionResult Quote(int id)
        {
            if (id <= 0)
            {
                return this.ResponseResult("Invalid quote id");
            }

            using (var db = ContextFactory.Create())
            {



                var quote =
                    this.CRMDBContext.JobQuotes.Include(i => i.JobItems).
                        Include(i => i.JobItems.Select(s => s.Options)).
                        SingleOrDefault(
                            f =>
                                f.QuoteId == id && f.Job.IsDeleted == false &&
                                f.Job.Lead.FranchiseId == SessionManager.CurrentFranchise.FranchiseId);

                if (quote != null)
                {
                    var job =
                        db.Jobs.Include(i => i.Customer)
                            .Include(i => i.SalesPerson)
                            .Include(i => i.BillingAddress)
                            .Include(i => i.InstallAddress)
                            .SingleOrDefault(j => j.JobId == quote.JobId);

                    if (job == null)
                    {
                        return this.ResponseResult("Unable to retrieve job");
                    }

                    if (job.SourceId.HasValue)
                    {
                        job.Source = SessionManager.CurrentFranchise.SourceCollection(job.SourceId.Value);
                    }

                    var file = Serializer.Jobs.Quote_old.Serialize(db, SessionManager.CurrentFranchise, job, quote, null,
                        ExportTypeEnum.PDF);

                    string fileDownloadName = string.Format("Quote {0}-{1}.pdf", job.JobNumber, quote.QuoteNumber);

                    var esignatureManager = new ESignatureManager();

                    try
                    {
                        string envelopeId;

                        esignatureManager.RequestSignatureOnDocument(file.ReadFully(),
                            fileDownloadName,
                            string.Format("{0} {1}", job.Customer.FirstName, job.Customer.LastName),
                            job.Customer.PrimaryEmail,
                            out envelopeId);

                        quote.EnvelopeId = envelopeId;
                        DateTime sentTime;
                        quote.EnvelopeStatus = esignatureManager.GetStatus(envelopeId, out sentTime);
                        quote.EnvelopeSentAt = sentTime;

                        db.SaveChanges();

                        return this.ResponseResult("Success");
                    }
                    catch (Exception ex)
                    {
                        return this.ResponseResult(ex.Message);
                    }
                }
            }

            return this.ResponseResult("Unable to retrieve quote");
        }

        [HttpGet]
        public IHttpActionResult Status(int id)
        {
            if (id <= 0)
            {
                return this.ResponseResult("Invalid quote id");
            }
            var quote =
                this.CRMDBContext.JobQuotes.Include(i => i.JobItems).
                    Include(i => i.JobItems.Select(s => s.Options)).
                    SingleOrDefault(f => f.QuoteId == id && f.Job.IsDeleted == false && f.Job.Lead.FranchiseId == SessionManager.CurrentFranchise.FranchiseId);

            if (quote != null)
            {
                if (quote.EnvelopeStatus == "completed")
                {
                    return this.Ok(new { status = "completed" });
                }

                var job = this.CRMDBContext.Jobs.Include(i => i.Customer).Include(i => i.SalesPerson).Include(i => i.BillingAddress).Include(i => i.InstallAddress).SingleOrDefault(j => j.JobId == quote.JobId);

                if (job == null)
                {
                    return this.ResponseResult("Unable to retrieve job");
                }

                var esignatureManager = new ESignatureManager();

                try
                {
                    DateTime sentTime;

                    var status = esignatureManager.GetStatus(quote.EnvelopeId, out sentTime);

                    quote.EnvelopeStatus = status;

                    if (status == "completed")
                    {
                        esignatureManager.DownloadDocument(job.JobId, quote.EnvelopeId);

                        this.CRMDBContext.SaveChanges();
                    }

                    return this.Ok(new { status });
                }
                catch (Exception ex)
                {
                    return this.ResponseResult(ex.Message);
                }
            }

            return this.ResponseResult("Unable to retrieve quote");
        }

        [HttpGet]
        public IHttpActionResult MassUpdate()
        {
            var esignatureManager = new ESignatureManager();

            var sentRequests = this.CRMDBContext.JobQuotes.Where(jq => jq.EnvelopeId != null && jq.EnvelopeStatus == "sent").ToList();

            foreach (var sentRequest in sentRequests)
            {
                try
                {
                    DateTime sentTime;

                    var status = esignatureManager.GetStatus(sentRequest.EnvelopeId, out sentTime);

                    sentRequest.EnvelopeStatus = status;

                    if (status == "completed" && sentRequest.EnvelopeReceivedAt == null)
                    {
                        esignatureManager.DownloadDocument(sentRequest.JobId, sentRequest.EnvelopeId);
                        sentRequest.EnvelopeStatus = "downloaded";
                        sentRequest.EnvelopeReceivedAt = DateTime.Now;
                    }

                    this.CRMDBContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    return this.ResponseResult(ex.Message);
                }
            }

            return this.Ok();
        }
    }

    #endregion

}