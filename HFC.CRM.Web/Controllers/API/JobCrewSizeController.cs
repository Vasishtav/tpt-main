﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class JobCrewSizeController : BaseApiController
    {
        readonly JobCrewSizeManager crewsizemanager = new JobCrewSizeManager();

        [HttpPost]
        public IHttpActionResult AddUpdateCrewSize(JobCrewSize model)
        {
            try
            {
                if (model != null)
                {
                    model.CreatedBy = SessionManager.SecurityUser.PersonId;
                    model = crewsizemanager.AddUpdateCrewSize(model);

                }
                return Json(model);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                JobCrewSize CrewSize;
                CrewSize = crewsizemanager.GetCrewSize(id);
                if (CrewSize == null)
                    CrewSize = new JobCrewSize();
                return Json(CrewSize);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
