﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System.Web.Http;
using HFC.CRM.Web.Filters;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class LeadStatusController : BaseApiController
    {
        readonly FranchiseManager _franMgr = new FranchiseManager(SessionManager.CurrentUser);

        public IHttpActionResult Post(Type_LeadStatus model)
        {
            var status = "Invalid data";
            int statusId = 0;
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = _franMgr.AddLeadStatus(model);

                statusId = model.Id;
            }
            return ResponseResult(status, Json(new { Id = statusId }));
        }

        public IHttpActionResult Put(Type_LeadStatus model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = _franMgr.UpdateLeadStatus(model);
            }
            return ResponseResult(status);
        }

        public IHttpActionResult Delete(int id)
        {
            var status = _franMgr.DeleteLeadStatus(SessionManager.CurrentFranchise.FranchiseId, id);
            return ResponseResult(status);
        }
    }
}
