﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Web.Filters;


namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class PermissionsController : BaseApiController
    {
        public class PermissionModel
        {
            public int PermissionTypeId { get; set; }
            public List<Guid> RoleIds { get; set; }
            public List<int> PersonIds { get; set; }
            public bool CanCreate { get; set; }
            public bool CanRead { get; set; }
            public bool CanUpdate { get; set; }
            public bool CanDelete { get; set; }
        }

        FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);

        public IHttpActionResult Get()
        {
            //return ResponseResult("Success", Json(PermissionProvider.GetUserPermissions(SessionManager.CurrentUser)));
            var permissions = PermissionProviderTP.GetUserPermissions(SessionManager.CurrentUser);

            //var llead = permissions.Where(p => p.Name == "List Leads").FirstOrDefault();
            //llead.CanAccess = false;

            return ResponseResult("Success", Json(permissions));
        }

        //[ValidateAjaxAntiForgeryToken]
        //public IHttpActionResult Post([FromBody]PermissionModel model)
        //{            
        //    string status = null;
        //    if (!FranMgr.Permission_BasePermission.CanCreate)
        //        status = FranchiseManager.Unauthorized;
        //    else            
        //        status = PermissionProvider.CreatePermission(model.RoleIds, model.PersonIds, model.PermissionTypeId, model.CanCreate, model.CanRead, model.CanUpdate, model.CanDelete, SessionManager.CurrentFranchise.FranchiseId);


        //    return ResponseResult(status);
        //}

        //TODO: is it required in TP???
        //[ValidateAjaxAntiForgeryToken]
        //public IHttpActionResult Put(int id, [FromBody] PermissionModel model)
        //{
        //    string status = null;
        //    if (model == null) status = "Invalid CRUD permission";
        //    else
        //    {
        //        if ((SessionManager.CurrentFranchise != null && FranMgr.Permission_BasePermission.CanUpdate)
        //            || PermissionProvider.GetPermission(SessionManager.CurrentUser, "Settings", "Permissions").CanUpdate)
        //        {
        //            status = PermissionProvider.UpdatePermission(id,
        //                SessionManager.CurrentFranchise != null ? SessionManager.CurrentFranchise.FranchiseId : (int?)null,
        //                model.CanCreate,
        //                model.CanRead,
        //                model.CanUpdate,
        //                model.CanDelete);
        //        }
        //        else
        //        {
        //            status = FranchiseManager.Unauthorized;
        //        }
        //    }
            
        //    return ResponseResult(status);
        //}

        //[ValidateAjaxAntiForgeryToken]
        //public IHttpActionResult Delete(int id)
        //{
        //    var status = PermissionProvider.DeletePermission(id, SessionManager.CurrentFranchise.FranchiseId);

        //    return ResponseResult(status);
        //}
    }
}
