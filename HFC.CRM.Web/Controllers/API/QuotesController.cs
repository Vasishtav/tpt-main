﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Core.Logs;
using HFC.CRM.Serializer;
using System.Web;

namespace HFC.CRM.Web.Controllers.API
{
    using DTO.Opportunity;
    using HFC.CRM.Core.Common;

    public class QuotesController : BaseApiController
    {
        private QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private OpportunitiesManager oppMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private LookupManager lkupMgr = new LookupManager();
        private PICManager picManager = new PICManager();
        private PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private AccountsManager actMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public IHttpActionResult GetList(int id)
        {
            var result = QuotesMgr.GetQuote(id);
            var opportunity = oppMgr.Get(id);
            if (result != null)
            {
                return Json(new { quoteList = result, Opportunities = opportunity });
            }
            return OkTP();
        }

        public IHttpActionResult GetDetails(int id, int oppurtunityId)
        {
            if (id == 0)
            {
                var result = QuotesMgr.NewQuote(id, oppurtunityId);
                if (result != null)
                {
                    return Json(
                        result
                    );
                }
            }
            else
            {
                var result = QuotesMgr.GetDetails(id);
                if (result != null)
                {
                    return Json(
                        result
                    );
                }
            }

            return OkTP();
        }


        [HttpPost]
        public IHttpActionResult changeExpiryToDraft(int id)
        {
            try
            {
                var res = QuotesMgr.changeExpiryToDraft(id);
                if (res == true)
                    return Json(true);
                else return Json(false);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(false);
            }

        }

        public IHttpActionResult GetMargin(int id)
        {
            var result = QuotesMgr.GetMargin(id);
            if (result != null)
            {
                return Json(
                    result
                );
            }
            return OkTP();
        }

        [HttpGet]
        public IHttpActionResult GetMarginDetails(int id)
        {
            try
            {
                var quote = QuotesMgr.GetDetails(id);
                var quoteLines = QuotesMgr.GetQuoteMarginDetails(id);
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                var costAudit = QuotesMgr.GetCostChangeAudit(id);

                return Json(new { data = new { quote = quote, quoteLines = quoteLines, DiscountAndPromo = discount, costAudit = costAudit }, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult EstimateSalesTax(int id)
        {
            try
            {
                var res = QuotesMgr.DeleteTax(id);
                var picTaxErr = false;
                var tax = QuotesMgr.GetAndUpdateTax(id);
                dynamic responseObj = JsonConvert.DeserializeObject(tax);

                if (!Convert.ToBoolean(responseObj.valid.Value))
                {
                    var err = string.Empty;
                    foreach (var item in responseObj.error)
                    {
                        if (item.Value.ToString().ToLower().Contains("jurisdictionnotfounderror: "))
                        {
                            picTaxErr = true;
                        }
                        err = err + "<li>" + item.Value + "</li>";
                    }
                    return Json(new { data = "", error = err, PICTaxerror = picTaxErr });
                }
                else
                {
                    var taxInfo = QuotesMgr.GetTaxinfo(id);
                    return Json(new { data = new { Taxinfo = taxInfo }, error = "", PICTaxerror = picTaxErr });
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message, PICTaxerror = false });
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateDiscountAndPromos(int id, List<DiscountsAndPromo> models)
        {
            try
            {
                foreach (var item in models)
                {
                    var response = QuotesMgr.UpdateDiscountAndPromo(item);
                }

                QuotesMgr.RecalculateQuoteLines(id);
                var quote = QuotesMgr.GetDetails(id);
                var quoteLines = QuotesMgr.GetQuoteMarginDetails(id);
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                return Json(new { data = new { quote = quote, quoteLines = quoteLines, DiscountAndPromo = discount }, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult UpdatequoteLineMargin(int id, QuoteMarginReviewVM model)
        {
            try
            {
                var response = QuotesMgr.UpdatequotMargin(id, model);

                var quote = QuotesMgr.GetDetails(id);
                var quoteLines = QuotesMgr.GetQuoteMarginDetails(id);
                var discount = QuotesMgr.GetDiscountAndPromo(id);

                var costAudit = QuotesMgr.GetCostChangeAudit(id);

                return Json(new { data = new { quote = quote, quoteLines = quoteLines, DiscountAndPromo = discount, costAudit = costAudit }, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult GetDiscountDetails(int id)
        {
            try
            {
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                return Json(discount);
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        public IHttpActionResult GetVendorPromoDetails(int id)
        {
            var result = QuotesMgr.GetDiscountAndProm(id);
            if (result != null)
            {
                return Json(
                    result
                );
            }
            return OkTP();
        }

        [HttpPost]
        public IHttpActionResult UpdateQuoteLineItem(int id, List<QuoteLineDetail> qld)
        {
            var result = QuotesMgr.UpdateQuoteLine(id, qld);
            return null;
        }

        [HttpPost]
        public IHttpActionResult SaveQuote(int id, [FromBody]Quote model)
        {
            //var response = QuotesMgr.UpdatePrimaryQuote(opportunityId, id);
            var quote_org = QuotesMgr.GetQuoteData(model.QuoteKey);
            if (model.PrimaryQuote.HasValue && model.PrimaryQuote.Value)
            {
                if (quote_org != null && quote_org.PrimaryQuote.Value != model.PrimaryQuote.Value)
                {
                    var primquote = QuotesMgr.GetQuoteDataforPrimary(model.OpportunityId);
                    return Json(new { quoteKey = model.QuoteKey, error = "Duplicate Primary Quote Check", data = primquote });
                }
            }

            var Status = QuotesMgr.Save(model);

            if (Status != 0)
            {
                var result = QuotesMgr.GetDetails(Status);
                return Json(new { quoteKey = Status, error = "", data = result });
            }
            return Json(new { quoteKey = Status, error = "" });
        }

        //sale date
        [HttpPost]
        public IHttpActionResult SaveSaleDate(int id, DateTime SaleDate)
        {
            var Result = QuotesMgr.SaveSale(id, SaleDate);

            if (Result == null)
            {
                return Json(new { data = "", error = "Failed to Save." });
            }
            return Json(new { data = Result, error = "" });
        }

        [HttpPost]
        public IHttpActionResult SaveQuoteNotes(int id)
        {
            var httpRequest = HttpContext.Current.Request;
            var files = httpRequest.Files;
            var QuoteLevelNotes = httpRequest.Form["QuoteLevelNotes"];
            var OrderInvoiceLevelNotes = httpRequest.Form["OrderInvoiceLevelNotes"];
            var InstallerInvoiceNotes = httpRequest.Form["InstallerInvoiceNotes"];

            QuotesMgr.SaveQuoteNotes(id, QuoteLevelNotes, OrderInvoiceLevelNotes, InstallerInvoiceNotes);

            return Json(true);

            //var Status = QuotesMgr.Save(model);

            //if (Status != 0)
            //{
            //    var result = QuotesMgr.GetDetails(Status);
            //    return Json(new { quoteKey = Status, error = "", data = result });
            //}
            //return Json(new { quoteKey = Status, error = "Quote Sucessfully Saved." });
        }

        [HttpGet]
        public IHttpActionResult GetQuoteLinesTL(int id)
        {
            try
            {
                var res = QuotesMgr.GetQuoteLine(id);

                var prdCateg = lkupMgr.GetMyProductCategories();

                foreach (var qlitem in res)
                {
                    if (qlitem.ProductCategoryId != null && qlitem.ProductCategoryId > 0)
                    {
                        qlitem.ProductCategory = prdCateg.Where(x => x.ProductCategoryId
                           == qlitem.ProductCategoryId)
                               .FirstOrDefault().ProductCategory;
                    }

                    if (qlitem.ProductSubCategoryId != null && qlitem.ProductSubCategoryId > 0)
                    {
                        qlitem.ProductSubCategory = prdCateg.Where(x => x.ProductCategoryId
                          == qlitem.ProductSubCategoryId)
                               .FirstOrDefault().ProductCategory;
                    }

                    if (qlitem.ProductId.HasValue && qlitem.ProductId > 0)
                    {
                        var products = lkupMgr.GetProdcustTL();
                        var discounts = lkupMgr.GetDiscountTL();
                        var p = products.Where(x => x.ProductKey
                            == qlitem.ProductId).FirstOrDefault();
                        if (p != null)
                        {
                            qlitem.ProductName = p.ProductName;
                        }
                    }
                }

                var discount = QuotesMgr.GetDiscountAndPromo(id);
                return Json(new { quoteLines = res, discountAndPromo = discount });
                //return Json (new { data= res,error="" });
            }
            catch (Exception ex)
            {
                throw ex;

                //return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult GetQuoteLines(int id)
        {
            try
            {
                var res = QuotesMgr.GetQuoteLine(id);
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                return Json(new { quoteLines = res, discountAndPromo = discount });
                //return Json (new { data= res,error="" });
            }
            catch (Exception ex)
            {
                throw ex;

                //return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult GetMyProducts()
        {
            try
            {
                var res = QuotesMgr.GetMyProducts();

                return Json(new { data = res, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult getVendorNames()
        {
            try
            {
                var res = QuotesMgr.getVendorNames();

                return Json(res);
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult GetMyProductsOrServiceDiscount(int productType)
        {
            try
            {
                var res = "";
                var prdType = new List<int>();
                if (productType == 2)
                {
                    prdType.Add(2);
                }
                else
                {
                    prdType.Add(3);
                    prdType.Add(4);
                }
                res = QuotesMgr.GetMyProductsOrServiceDiscount(prdType);
                return Json(new { data = res, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        [Route("api/Quotes/0/GetQuoteLineByQuoteLineId")]
        public IHttpActionResult GetQuoteLineByQuoteLineId(int quoteLineId)
        {
            try
            {
                var res = QuotesMgr.GetQuoteLineByQuoteLineId(quoteLineId);

                return Json(new { data = res, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        [Route("api/Quotes/0/CopyQuote")]
        public IHttpActionResult CopyQuote(int id, int quoteKey,bool copyTaxExempt)
        {
            try
            {
                var res = QuotesMgr.CopyQuote(quoteKey, copyTaxExempt);
                var data = QuotesMgr.GetQuote(id);

                return Json(new { data = data, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult updateQuotelineFromConfig(QuoteLinesVM model)
        {
            try
            {
                var res = QuotesMgr.updateQuotelineFromConfig(model);

                return Json(new { error = "", QuoteKey = model.QuoteKey, OpportunityId = model.OpportunityId });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        [HttpPost]
        public bool rearrangeQuotelines(List<QuoteLinesVM> LineModel)
        {
            var res = QuotesMgr.updatequoteLineNumber(LineModel);

            return true;
        }

        [HttpPost]
        public IHttpActionResult updateQuoteline(QuoteLinesVM model)
        {
            try
            {
                decimal unit_price = 0;
                var quoteLines_old = QuotesMgr.GetQuoteLine(model.QuoteKey);
                var ql_old = quoteLines_old.Where(x => x.QuoteLineId == model.QuoteLineId).FirstOrDefault();
                var qld_old = QuotesMgr.GetMargin(model.QuoteLineId).FirstOrDefault();

                if (model.ProductTypeId == 4)
                {
                    if (model.DiscountType == "%")
                    {
                        model.Discount = model.Discount * 100;
                    }

                }

                if (qld_old != null)
                {
                    if (!qld_old.ManualUnitPrice)
                    {
                        ///Calculate orginal unit price, before the discount.
                        //    if (model.UnitPrice == ql_old.UnitPrice)
                        //        model.UnitPrice = (ql_old.UnitPrice.Value / (100 - ql_old.Discount.Value)) * 100;
                    }
                    else
                    {
                        model.ManualUnitPrice = qld_old.ManualUnitPrice;
                    }
                }
                var res = QuotesMgr.updatequoteinlineEdit(model);

                ///Recalcualte the unit price if there is a change in unitprice or discount if changed by user
                if (qld_old.ManualUnitPrice && (ql_old.Discount != model.Discount || model.UnitPrice != ql_old.UnitPrice))
                {
                    var response = QuotesMgr.RecalculateQuoteLines(model.QuoteKey, model.QuoteLineId);
                }
                else
                {
                    var response = QuotesMgr.RecalculateQuoteLines(model.QuoteKey, 0);
                }

                var quote = QuotesMgr.GetDetails(model.QuoteKey);
                var quoteLines = QuotesMgr.GetQuoteLine(model.QuoteKey);
                var discount = QuotesMgr.GetDiscountAndPromo(model.QuoteKey);
                return Json(new
                {
                    data = new { quote = quote, quoteLines = quoteLines, discountAndPromo = discount },
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult updateQuotelineTL(QuoteLinesVM model)
        {
            try
            {
                var res = "Success";
                decimal unit_price = 0;
                var quoteLines_old = QuotesMgr.GetQuoteLine(model.QuoteKey);
                var ql_old = QuotesMgr.getQuoteLinebyId(model.QuoteLineId);
                var details_old = QuotesMgr.GetMargin(model.QuoteLineId).FirstOrDefault();
                if (model.ProductTypeId == 4)
                {
                    if (model.DiscountType == "%")
                    {
                        model.Discount = model.Discount;
                    }

                }
                if (model.ManualUnitPrice)
                {
                    details_old.ManualUnitPrice = true;
                }
                else
                {
                    if (ql_old.ProductTypeId != model.ProductTypeId)
                    {
                        details_old.ManualUnitPrice = false;
                    }
                    else
                    {
                        if (details_old.unitPrice == model.UnitPrice)
                        {
                            if (details_old.ManualUnitPrice)
                            {
                                details_old.ManualUnitPrice = true;
                            }
                            else
                            {
                                details_old.ManualUnitPrice = false;
                            }
                        }
                        else
                        {
                            details_old.ManualUnitPrice = true;
                        }
                    }
                }

                //if ((model.ProductTypeId == 2 || model.ProductTypeId == 3 )
                //    && model.ProductSubCategoryId.HasValue)
                if ((model.ProductTypeId == 2 || model.ProductTypeId == 3)
                    && model.ProductId.HasValue)
                {
                    var prdOrSvc = new MyVendorProductVM();
                    prdOrSvc.ManualUnitPrice = details_old.ManualUnitPrice;
                    prdOrSvc.SuggestedResale = model.SuggestedResale;
                    //if ((model.ProductTypeId == 2 || model.ProductTypeId
                    //var myprdOrService = QuotesMgr.GetMyProductById(model.ProductSubCategoryId.Value);
                    var myprdOrService = QuotesMgr.GetMyProductById(model.ProductId.Value);
                    prdOrSvc.Cost = myprdOrService.Cost.HasValue ? myprdOrService.Cost.Value : 0;
                    //prdOrSvc.Description = myprdOrService.Description;
                    prdOrSvc.Description = model.Description;
                    prdOrSvc.PICGroupId = myprdOrService.TypeProductCategory.PICGroupId.HasValue
                        ? myprdOrService.TypeProductCategory.PICGroupId.Value
                        : 0;
                    prdOrSvc.PICProductId = myprdOrService.TypeProductCategory.PICProductId.HasValue
                        ? myprdOrService.TypeProductCategory.PICProductId.Value
                        : 0;
                    prdOrSvc.ProductCategory = myprdOrService.TypeProductCategory.ProductCategory;

                    // TODO: This should not be the case, now we are not using the
                    // the old way sujjected by Vasishta.
                    //prdOrSvc.ProductId = model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;
                    //prdOrSvc.ProductKey = model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;

                    //prdOrSvc.ProductId = myprdOrService.ProductID;
                    // TODO: why are we using product key and productid interchangeably
                    // it is very confusing.
                    prdOrSvc.ProductId = myprdOrService.ProductKey;
                    prdOrSvc.ProductKey = myprdOrService.ProductKey;

                    prdOrSvc.ProductName = myprdOrService.ProductName;
                    prdOrSvc.ProductTypeId = model.ProductTypeId;
                    prdOrSvc.Quantity = model.Quantity.HasValue ? model.Quantity.Value : 0;
                    prdOrSvc.QuotelineId = model.QuoteLineId;
                    if (details_old.ManualUnitPrice)
                    {
                        prdOrSvc.UnitPrice = model.UnitPrice.HasValue ? model.UnitPrice.Value : 0;
                    }
                    else
                    {
                        prdOrSvc.UnitPrice = myprdOrService.SalePrice.HasValue ? myprdOrService.SalePrice.Value : 0;
                    }
                    prdOrSvc.Vendor = myprdOrService.VendorName;
                    prdOrSvc.VendorId = myprdOrService.VendorID.HasValue ? myprdOrService.VendorID.Value : 0;
                    prdOrSvc.VendorProductSKU = myprdOrService.VendorProductSKU;
                    prdOrSvc.ProductCategoryId = model.ProductCategoryId.HasValue ? model.ProductCategoryId.Value : 0;
                    prdOrSvc.ProductSubcategoryId =
                        model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;
                    prdOrSvc.Discount = model.Discount.HasValue ? model.Discount.Value : 0;

                    res = QuotesMgr.UpdateQuoteConfigurationTL(prdOrSvc);
                }
                else if (model.ProductTypeId == 4)
                {
                    ql_old.ProductTypeId = model.ProductTypeId;
                    ql_old.Discount = model.Discount.HasValue ? model.Discount.Value : 0;
                    ql_old.Quantity = 0;
                    ql_old.ProductCategoryId = model.ProductCategoryId.HasValue ? model.ProductCategoryId.Value : 0;
                    ql_old.ProductSubCategoryId =
                        model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;
                    ql_old.ProductId = model.ProductId;
                    ql_old.ProductName = model.ProductName;
                    ql_old.Description = model.Description;
                    ql_old.DiscountType = model.DiscountType;
                    details_old.Discount = model.Discount.HasValue ? model.Discount.Value : 0;
                    ql_old.DiscountType = model.DiscountType;
                    QuotesMgr.UpdateAndInsertDiscountRow(ql_old, details_old);
                }
                else //if (model.ProductTypeId == 5)
                {
                    var prdCateg = lkupMgr.GetMyProductCategories();
                    var prdOrSvc = new MyVendorProductVM();
                    prdOrSvc.ManualUnitPrice = details_old.ManualUnitPrice;
                    prdOrSvc.SuggestedResale = model.SuggestedResale;
                    //if ((model.ProductTypeId == 2 || model.ProductTypeId
                    prdOrSvc.Cost = model.Cost.HasValue ? model.Cost.Value : 0;
                    prdOrSvc.Description = model.Description;
                    prdOrSvc.PICGroupId = 1000000;
                    prdOrSvc.PICProductId = 10000;
                    if (model.ProductCategoryId != 0)
                        prdOrSvc.ProductCategory = prdCateg.Where(x => x.ProductCategoryId == model.ProductCategoryId)
                            .FirstOrDefault().ProductCategory;
                    //prdOrSvc.ProductName = prdCateg.Where(x => x.ProductCategoryId == model.ProductSubCategoryId.Value)
                    //    .FirstOrDefault().ProductCategory;
                    prdOrSvc.ProductName = model.ProductName;
                    prdOrSvc.ProductTypeId = model.ProductTypeId;
                    prdOrSvc.Quantity = model.Quantity.Value;
                    prdOrSvc.BaseResalePriceWOPromos = model.BaseResalePriceWOPromos;
                    prdOrSvc.QuotelineId = model.QuoteLineId;
                    prdOrSvc.UnitPrice = model.UnitPrice.HasValue ? model.UnitPrice.Value : 0;
                    prdOrSvc.ProductCategoryId = model.ProductCategoryId.HasValue ? model.ProductCategoryId.Value : 0;
                    prdOrSvc.ProductSubcategoryId =
                        model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;
                    prdOrSvc.Discount = model.Discount.HasValue ? model.Discount.Value : 0;

                    res = QuotesMgr.UpdateQuoteConfigurationTL(prdOrSvc);
                }

                var quote = QuotesMgr.GetDetails(model.QuoteKey);
                var quoteLines = QuotesMgr.GetQuoteLine(model.QuoteKey);
                var discount = QuotesMgr.GetDiscountAndPromo(model.QuoteKey);
                return Json(new
                {
                    data = new { quote = quote, quoteLines = quoteLines, discountAndPromo = discount },
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpPost]
        [Route("api/Quotes/SaveQuoteLines")]
        public IHttpActionResult SaveQuoteLines(int id, [FromBody]string quoteLines)
        {
            try
            {
                var res = QuotesMgr.insertQuoteLines(quoteLines);
                //var response = QuotesMgr.RecalculateQuoteLines(id);
                if (res == "Sucessful")
                {
                    var result = QuotesMgr.GetQuoteLine(id);
                    return Json(new { data = result, error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                return Json(new { data = "", error = e.Message });
            }
        }

        [HttpPost]
        [Route("api/Quotes/SaveQuoteLinesMemo")]
        public IHttpActionResult SaveQuoteLinesMemo(int id, [FromBody]string memo)
        {
            try
            {
                var res = QuotesMgr.updateQuotelineMemo(id, memo);
                if (res == "Sucessful")
                {
                    var result = QuotesMgr.GetQuoteLine(id);
                    return Json(new { data = "", error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                return Json(new { data = "", error = e.Message });
            }
        }

        [HttpPost]
        [Route("api/Quotes/SaveMyVendorConfigurationList")]
        public IHttpActionResult SaveMyVendorConfigurationList(int id, [FromBody]List<MyVendorProductVM> configuration)
        {
            try
            {
                //InsertBatchMyProductConfiguration

                foreach (var qq in configuration)
                {
                    if (qq.ProductTypeId == 2)
                        qq.ProductCategoryId = 99997;
                    if (qq.ProductTypeId == 3)
                        qq.ProductCategoryId = 99998;
                    if (qq.ProductTypeId == 4)
                        qq.ProductCategoryId = 99999;
                }

                var res = QuotesMgr.InsertBatchMyProductConfiguration(id, configuration);

                if (res)
                {
                    return Json(new { data = "Successful", error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                return Json(new { data = "", error = e.Message });
            }
        }

        [HttpPost]
        [Route("api/Quotes/SaveMyVendorConfiguration")]
        public IHttpActionResult SaveMyVendorConfiguration(int id, [FromBody]MyVendorProductVM configuration)
        {
            try
            {
                var oldQuoteLine = QuotesMgr.getQuoteLineWithGroupDiscount(configuration.QuotelineId);
                var res = QuotesMgr.UpdateQuoteConfiguration(configuration);

                if (res == "Success")
                {
                    //QuotesMgr.UpdateQuoteLineGroupDiscount(oldQuoteLine.QuoteKey, oldQuoteLine);
                    QuotesMgr.ApplyGroupDiscount(oldQuoteLine.QuoteKey);
                    QuotesMgr.RefreshGroupDiscountTable(oldQuoteLine.QuoteKey);
                    return Json(new { data = "Sucessful", error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                return Json(new { data = "", error = e.Message });
            }
        }

       public class abcd
        {
            int id { get; set; }
            string copyOrDelete { get; set; }
            int QuoteKey { get; set; }

        }
        [HttpPost]
        public IHttpActionResult EraseQuoteLine(int id, List<dynamic> arrObj)
        {
            try
            {

                int QuoteKey = id;
                var qIds = new List<int>();
                foreach ( var obj in arrObj)
                {
                    qIds.Add(Convert.ToInt32(obj.QuoteLineId.Value));
                }
                var res = QuotesMgr.CopyDeleteQuoteline(qIds, "DELETE");
                    var response = QuotesMgr.RecalculateQuoteLines(QuoteKey);
                    QuotesMgr.RefreshGroupDiscountTable(QuoteKey);
                var quote = QuotesMgr.GetDetails(QuoteKey);
                var result = QuotesMgr.GetQuoteLine(QuoteKey);
                return Json(new { data = result, error = "", quote = quote });
                }
            catch(Exception e)
            {
                return Json(new { data = "", error = e.Message });
            }
        }
        [HttpPost]
        public IHttpActionResult MarkQuoteLinesTaxExempt(int id, bool isTaxExempt, List<dynamic> arrObj)
        {
            try
            {

                int QuoteKey = id;
                var qIds = new List<int>();
                foreach (var obj in arrObj)
                {
                    qIds.Add(Convert.ToInt32(obj.QuoteLineId.Value));
                }
                QuotesMgr.MarkQuotelinesTaxExempt(qIds, isTaxExempt);
                var response = QuotesMgr.RecalculateQuoteLines(QuoteKey);
                var quote = QuotesMgr.GetDetails(QuoteKey);

                var result = QuotesMgr.GetQuoteLine(QuoteKey);
                return Json(new { data = result, error = "", quote = quote });

            }
            catch(Exception e)
            {
                return Json(new { data = "", error = e.Message });
            }
        }



        [HttpPost]
        [Route("api/Quotes/CopyDeleteQuoteLine")]
        public IHttpActionResult CopyDeleteQuoteLine(int id, [FromBody]string copyorDelete)
        {
            try
            {
                //obj.copyOrDelete = "COPY";
                //obj.QuoteKey = data.QuoteKey;
                var quotelineobj = new
                {
                    QuoteKey = 0,
                    copyOrDelete = ""
                };

                var obj = JsonConvert.DeserializeAnonymousType(copyorDelete, quotelineobj);
                var qlds = new List<int>();
                qlds.Add(id);
                var res = QuotesMgr.CopyDeleteQuoteline(qlds, obj.copyOrDelete);
                var response = QuotesMgr.RecalculateQuoteLines(obj.QuoteKey);
                if (obj.copyOrDelete == "DELETE")
                    QuotesMgr.RefreshGroupDiscountTable(obj.QuoteKey);

                var quote = QuotesMgr.GetDetails(obj.QuoteKey);

                if (res)
                {
                    var result = QuotesMgr.GetQuoteLine(obj.QuoteKey);
                    return Json(new { data = result, error = "", quote = quote });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                return Json(new { data = "", error = e.Message });
            }
        }

        public bool CreateInvoiceFromConvertOrder(int orderId)
        {
            InvoiceSheetReport pdf = new InvoiceSheetReport();
            var order = ordermgr.GetOrderByOrderId(orderId);
            int invoiceId = ordermgr.CreateInvoice(order);
            Guid filepdf = pdf.createInvoicePDF(invoiceId);
            var result = ordermgr.CreateInvoice(invoiceId, filepdf);

            return true;
        }

        [HttpGet]
        //[Route("api/Quotes/0/ConvertQuoteToOrder")]
        public IHttpActionResult ConvertQuoteToOrderTaxOverride(int id)
        {
            try
            {
                if (!QuotesMgr.QuoteOrderExists(id))
                {
                    //QuotesMgr.UpdateQuoteTax(id);
                    int OrderId;
                    OrderId = QuotesMgr.CreateOrder(id);
                    CreateInvoiceFromConvertOrder(OrderId);

                    return Json(new { data = OrderId, error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "An order already exists for this opportunity." });
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        //[Route("api/Quotes/0/ConvertQuoteToOrder")]
        public IHttpActionResult ConvertQuoteToOrder(int id)
        {
            try
            {
                //var qlList = QuotesMgr.QuoteLinesValidation(id);
                //if (qlList.Where(x => !x.Valid).FirstOrDefault() != null)
                //{
                //    var Validationerror = "Warning!: Quote lines " + String.Join(",", qlList.Where(x => !x.Valid).Select(x => x.QuoteLineNumber).ToList()) + " are recommended for product re-validation.";
                //    return Json(new { data = "", error = Validationerror, PICTaxerror = false });
                //}
                var validQuote = QuotesMgr.ValidateQuote(id, false);
                if (!string.IsNullOrEmpty(validQuote))
                {
                    return Json(new { data = "", error = validQuote });
                }
                var picTaxErr = false;
                //QuotesMgr.UpdateQuoteTax(id);
                var quote = QuotesMgr.GetDetails(id);
                if (!QuotesMgr.QuoteOrderExists(id))
                {
                    int OrderId;
                    if (quote != null && quote.IsTaxExempt)
                    {
                        QuotesMgr.DeleteTax(id);
                        OrderId = QuotesMgr.CreateOrder(id);
                        CreateInvoiceFromConvertOrder(OrderId);
                    }
                    else
                    {
                        //var del = QuotesMgr.DeleteTax(id);
                        var response = QuotesMgr.CreateTaxOnSalesOrder(id);
                        dynamic responseObj = JsonConvert.DeserializeObject(response);

                        if (Convert.ToBoolean(responseObj.valid.Value))
                        {
                            //QuotesMgr.AddorUpdateTax(id, responseObj);
                            OrderId = QuotesMgr.CreateOrder(id);
                            CreateInvoiceFromConvertOrder(OrderId);
                        }
                        else
                        {
                            var err = string.Empty;
                            foreach (var item in responseObj.error)
                            {
                                if (item.Value.ToString().ToLower().Contains("jurisdictionnotfounderror: "))
                                {
                                    picTaxErr = true;
                                }

                                if (item.Value.ToString().ToLower().Contains("input missing required element 'items'."))
                                {
                                    err = "There are no line items to Create Sales Order.";
                                    //picTaxErr = true;
                                }
                                else
                                {
                                    //err = err + "<li>" + item.Value + "</li>";
                                    err = err + " " + item.Value + " ";
                                }
                                //JurisdictionNotFoundError:
                            }
                            return Json(new { data = "", error = err, PICTaxerror = picTaxErr });
                        }
                    }

                    //if (responseObj.valid == true && responseObj.GetType().GetProperty("tax") != null)
                    //{
                    //Adding the tax info
                    //QuotesMgr.AddorUpdateTax(id, responseObj);
                    //}

                    return Json(new { data = OrderId, error = "", PICTaxerror = picTaxErr });
                }
                else
                {
                    return Json(new { data = "", error = "Quote is already Converted.", PICTaxerror = picTaxErr });
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        //[Route("api/Quotes/0/ChangePrimaryQuote")]
        public IHttpActionResult ChangePrimaryQuote(int id, int opportunityId)
        {
            try
            {
                var checkorder = QuotesMgr.CheckOrderExisitbyOpportunity(opportunityId);
                if (checkorder != null && checkorder.QuoteKey != id)
                {
                    return Json(new { data = "", error = "Quote \"" + checkorder.QuoteName + "\" is already converted to Sales Order." });
                }

                var response = QuotesMgr.UpdatePrimaryQuote(opportunityId, id);
                if (response)
                {
                    return Json(new { data = "Sucessful", error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Error changing the primary quote." });
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        //[HttpGet]
        //public IHttpActionResult ClearQuoteslines(int Id, int QuoteKey)
        //{
        //    var res = QuotesMgr.ClearQuoteslines(Id, QuoteKey);

        //    return Json(res);
        //}

        [HttpGet]
        public IHttpActionResult ClearQuoteslines(int Id, int QuoteKey)
        {
            var res = QuotesMgr.ClearQuoteslines(Id, QuoteKey);
            var resultGroupDiscount = QuotesMgr.RefreshGroupDiscountTable(QuoteKey);
            var result = QuotesMgr.GetDetails(QuoteKey);

            return Json(new { quoteKey = QuoteKey, error = "", data = result });
        }

        [HttpGet]
        public IHttpActionResult getTaxExempt(int Id, int OpportunityId)
        {
            var res = QuotesMgr.getTaxExempt(Id, OpportunityId);

            return Json(res);
        }

        [HttpGet]
        public IHttpActionResult changeIsTaxExempt(int Id, int QuoteId, int OpportunityId)
        {
            var res = QuotesMgr.changeIsTaxExempt(Id, QuoteId, OpportunityId);
            return Json(res);
        }

        [HttpGet]
        public IHttpActionResult GetQuoteAndMeasurementDetails(int id, int poId = 0)
        {
            var res = QuotesMgr.GetQuoteDetailsforCoreConfiguration(id);
            var QuoteLinesVM = QuotesMgr.GetQuoteLine(id);
            if (poId != 0)
            {
                PurchaseOrderManager poMgr = new PurchaseOrderManager();
                var poDetails = poMgr.GetPurchaseOrderDetailsLines(poId);
                if (poDetails != null && poDetails.Count > 0)
                {
                    foreach (var item in poDetails)
                    {
                        QuoteLinesVM.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault().ConfigErrors = item.Errors;
                        if (item.PICPO.HasValue)
                        {
                            QuoteLinesVM.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault().PICPO = item.PICPO.Value; ;
                        }
                    }
                }
            }
            if (QuoteLinesVM != null && QuoteLinesVM.Count > 0)
            {
                res.QuoteLinesVM = QuoteLinesVM.Where(x => x.ProductTypeId == 1).ToList();
            }
            List<MeasurementLineItemBB> mh = new List<MeasurementLineItemBB>();
            if (res.Measurements != null)
                mh = JsonConvert.DeserializeObject<List<MeasurementLineItemBB>>(res.Measurements);

            foreach (var qq in mh)
            {
                qq.isAdded = true;
            }

            if (res.QuoteLinesVM != null)
                foreach (var q in res.QuoteLinesVM)
                {
                    //if((mh.Where(x => x.Height == q.Height.ToString() && x.Width == q.Width.ToString() && x.FranctionalValueHeight == q.FranctionalValueHeight &&
                    //x.FranctionalValueWidth == q.FranctionalValueWidth).FirstOrDefault()) == null)
                    if (Convert.ToInt32(q.Width) > 0 || Convert.ToInt32(q.Height) > 0)
                    {
                        //if (q.MeasurementsetId == null || q.MeasurementsetId == 0)
                        //{
                        MeasurementLineItemBB mbb = new MeasurementLineItemBB();
                        mbb.id = 0;
                        mbb.RoomLocation = null;// q.RoomLocation;
                        mbb.RoomName = q.RoomName;
                        mbb.WindowLocation = null;// q.WindowLocation;
                        mbb.MountTypeName = q.MountType;
                        mbb.fileName = "";
                        mbb.Stream_id = "";
                        mbb.QuoteLineId = 0;
                        //mbb.QuoteLineNumber = q.QuoteLineNumber;
                        mbb.isAdded = false;
                        mbb.Width = q.Width.ToString();
                        mbb.FranctionalValueWidth = q.FranctionalValueWidth.ToString();
                        mbb.Height = q.Height.ToString();
                        mbb.FranctionalValueHeight = q.FranctionalValueHeight;

                        mh.Add(mbb);
                    }
                }
            foreach (var it in mh)
            {
                it.FranctionalValueHeight = !string.IsNullOrEmpty(it.FranctionalValueHeight) ? it.FranctionalValueHeight : "";
                it.FranctionalValueWidth = !string.IsNullOrEmpty(it.FranctionalValueWidth) ? it.FranctionalValueWidth : "";
            }
            var mhlist = (from m in mh
                              //orderby m.RoomName, m.Height, m.FranctionalValueHeight, m.Width, m.FranctionalValueWidth, m.MountTypeName
                          group m by new { m.RoomName, m.Height, m.FranctionalValueHeight, m.Width, m.FranctionalValueWidth, m.MountTypeName } into m1
                          select m1.First()).ToList();
            res.Measurements = JsonConvert.SerializeObject(mhlist);
            return Json(res);
        }

        [HttpPost]
        public IHttpActionResult UpdateQuoteStatus(int id, int quoteStatus)
        {
            var res = QuotesMgr.UpdateQuotesStatus(id, quoteStatus);
            return Json(res);
        }

        public IHttpActionResult GetAllQuote(int id, string quoteStatusListValue, string dateRangeValue, bool PrimaryQuote = false)
        {
            var result = QuotesMgr.GetAllQuote(quoteStatusListValue, dateRangeValue, PrimaryQuote);

            if (result != null)
            {
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "Quote");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "QuotesAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = result.Where(x => (x.SalesAgentId ==
                                       SessionManager.CurrentUser.PersonId ||
                                       x.InstallerId == SessionManager.CurrentUser.PersonId)
                                       ).ToList();
                    result = filtered;
                }
                return Json(result);
            }
            return OkTP();
        }

        #region Mass Quote Core config Update

        public string ValidatePromptAnswersforPICConfigurator(CoreProductVM obj)
        {
            //   SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "Step 1 of 4: Validating Product Data");
            string content = "";
            try
            {
                Stopwatch start = new Stopwatch();
                string url = AppConfigManager.PICUrl;
                /// Configurator / PromptAnswers / Validate /{ customerId}/{ productId}/{ modelId}/{ colorId}
                //TODO: for now hard code the franchisecode as these will be later changed as Franchisecode by PIC
                string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/Configurator/PromptAnswers/Validate/" + FranchiseCode + "/" + obj.PicProduct + "/" +
                          obj.ModelId + "?access_token=" + PICManager.PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");

                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "promptAnswers=" + obj.PromptAnswers,
                    ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Validate");
                //  SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
                //content = response.Content;
                return response.Content;
                //dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response.Content);

                //if (Convert.ToBoolean(responseObj.valid) == true && responseObj.messages.errors != null && responseObj.messages.errors.Count == 0)
                //{
                //    var pv = PICManager.GetPICVendor(obj.PicVendor);
                //    dynamic picVendor = JsonConvert.DeserializeObject<ExpandoObject>(pv);
                //    var res = QuotesMgr.UpdateCoreProductQuoteconfiguration(obj, responseObj, picVendor);
                //}
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }

                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        [HttpGet]
        // get coreconfig statuses
        public IHttpActionResult getConfigSaveStatuses(int id)
        {
            try
            {
                //EventLogger.LogEvent(SessionManager.GetConfigstatus(SessionManager.CurrentUser.PersonId.ToString()).ToString());
                //EventLogger.LogEvent("Getting status");
                return Json(SessionManager.GetConfigstatus(SessionManager.CurrentUser.PersonId.ToString()) != null ? SessionManager.GetConfigstatus(SessionManager.CurrentUser.PersonId.ToString()) : "");
                //return Json(SessionManager.GetConfigstatus(SessionManager.CurrentUser.PersonId.ToString()));

            }
            catch (Exception ex)
            {
                var data = SessionManager.GetConfigstatus(SessionManager.CurrentUser.PersonId.ToString());
                if (data != null)
                    EventLogger.LogEvent(data.ToString());
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        [HttpGet]
        public IHttpActionResult clearConfigSaveStatuses(int id)
        {
            //EventLogger.LogEvent("Status Log CLeared");
            SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
            return Json(true);
        }

        /// <summary>
        /// Validate the configurator
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult validatePromptAnswers([FromBody] CoreProductVM obj)
        {
            string content = "";
            try
            {
                string response = ValidatePromptAnswersforPICConfigurator(obj);
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response);
                if (Convert.ToBoolean(responseObj.valid) == true && responseObj.messages.errors != null &&
                    responseObj.messages.errors.Count > 0)
                {
                    if (Convert.ToDecimal(responseObj.cost) == 0 || Convert.ToDecimal(responseObj.list) == 0)
                    {
                        return Json(new
                        {
                            valid = false,
                            message = "Price Validation has failed on this product configuration. Please contact your system administrator.",
                            data = responseObj,
                            QuoteKey = obj.QuoteKey,
                            OpportunityId = obj.OpportunityId
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        valid = true,
                        message = "",
                        data = responseObj,
                        QuoteKey = obj.QuoteKey,
                        OpportunityId = obj.OpportunityId
                    });
                }

                return Json(new
                {
                    valid = true,
                    data = responseObj,
                    QuoteKey = obj.QuoteKey,
                    OpportunityId = obj.OpportunityId
                });
            }
            catch (Exception ex)
            {
                //EventLogger.LogEvent("Config Status cleared - validatePromptAnswers");
                SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }

                return Json(new { valid = false, message = msg });
            }
        }

        /// <summary>
        /// Validate and Save quoteline
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult validateAndSaveConfigurator([FromBody] CoreProductVM obj)
        {
            string content = "";
            try
            {
                //EventLogger.LogEvent("Step 1 of 4: Validating Product Data");
                SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "Step 1 of 4: Validating Product Data");
                string response = ValidatePromptAnswersforPICConfigurator(obj);
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response);

                if (Convert.ToBoolean(responseObj.valid) == true && responseObj.messages.errors != null && responseObj.messages.errors.Count == 0)
                {
                    if (Convert.ToDecimal(responseObj.cost) == 0 || Convert.ToDecimal(responseObj.list) == 0)
                    {
                        return Json(new
                        {
                            valid = false,
                            message = "Price Validation has failed on this product configuration. Please contact your system administrator.",
                            data = responseObj,
                            QuoteKey = obj.QuoteKey,
                            OpportunityId = obj.OpportunityId
                        });
                    }
                    else
                    {
                        var qlVM = new QuoteLinesVM();
                        if (obj.QuotelineId == 0)
                        {
                            var ql = QuotesMgr.InsertEmptyQuoteline(obj.QuoteKey);
                            obj.QuotelineId = ql.QuoteLineId;
                            qlVM.QuoteLineId = ql.QuoteLineId;
                        }
                        else
                        {
                            qlVM.QuoteLineId = obj.QuotelineId;
                        }

                        var QuoteLine = QuotesMgr.getQuoteLineWithGroupDiscount(obj.QuotelineId);
                        if(QuoteLine==null)
                            QuoteLine = qlVM;
                        var res = QuotesMgr.UpdateCoreProductQuoteconfiguration(obj, responseObj);
                        //QuotesMgr.UpdateQuoteLineGroupDiscount(obj.QuoteKey, QuoteLine);
                        QuotesMgr.ApplyGroupDiscount(obj.QuoteKey);
                        QuotesMgr.RefreshGroupDiscountTable(obj.QuoteKey);
                    }
                }
                var qData = QuotesMgr.GetQuoteDetailsforCoreConfiguration(obj.QuoteKey);


                // EventLogger.LogEvent("Config Status cleared - validatePromptAnswers");
                // SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
                return Json(new
                {
                    valid = true,
                    data = responseObj,
                    QuoteKey = obj.QuoteKey,
                    OpportunityId = obj.OpportunityId,
                    QuoteData = qData
                });
            }
            catch (Exception ex)
            {
                //EventLogger.LogEvent("Config Status cleared - validate and save configurator");
                SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }

                return Json(new { valid = false, message = msg });
            }
        }

        #endregion Mass Quote Core config Update

        [HttpGet]
        public IHttpActionResult UpdatequoteLineDescription()
        {
            var result = QuotesMgr.UpdateBulkQuotelineDescription();

            return OkTP();
        }

        [HttpPost]
        public IHttpActionResult UpdateQuoteNoteLines(QuoteLinesVM quoteVM)
        {
            var result = QuotesMgr.UpdateQuoteLineNotes(quoteVM);

            return OkTP();
        }
        //Below API to get the job crew size HTML for current Brand
        [HttpGet]
        public IHttpActionResult GetCrewSize()
        {
            var result = QuotesMgr.GetJobCrewSize();

            return Json(result);
        }
        /// <summary>
        ///  Validate all Quote lines in a nightly job that were created or updated in last 30 days
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult ValidateAllQuotes()
        {
            var result = QuotesMgr.ValidateAllQuotes();

            return Json(result);
        }
        /// <summary>
        /// Update the Quote description for all quotes updated in last 30days
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult UpdatequoteLineDescriptioninBatch()
        {
            var result = QuotesMgr.UpdateQuoteLineDescription();

            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult MoveQuote(MoveOpportunityDTO model)
        {
            try
            {
                if (model == null)
                    throw new Exception("Object should not empty");

                int opportunityId = 0;
                int QuoteKey = 0;
                if (model.OpportunityId == 0)
                {
                    var status = oppMgr.CreateOpportunity(out opportunityId, CopyMoveOpportunityModelToOpportunity(model), 0);
                    model.OpportunityId = opportunityId;
                    //texting
                    var CountryCode = SessionManager.CurrentFranchise.CountryCode;
                    var BrandId = SessionManager.CurrentFranchise.BrandId;
                    var Account = actMgr.Get(model.AccountId);
                    if (model.SendMsg)
                    {
                        var SendOptinMessage = communication_Mangr.ForceOptinMessage(Account.PrimCustomer.CellPhone, BrandId, CountryCode, null, model.AccountId);
                    }
                    if (model.UncheckNotifyviaText)
                    {
                        var unchecknotify = actMgr.UpdateNotifyText(model.AccountId);
                    }
                }
                QuoteKey = QuotesMgr.SaveMoveQuote(model);
                return Json(new { model.OpportunityId, QuoteKey });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        private Opportunity CopyMoveOpportunityModelToOpportunity(MoveOpportunityDTO Model)
        {
            Opportunity opportunitymodel = new Opportunity();

            opportunitymodel.OpportunityName = Model.OpportunityName;
            opportunitymodel.SideMark = Model.SideMark;
            opportunitymodel.OpportunityStatusId = Model.OpportunityStatusId;
            opportunitymodel.SalesAgentId = Model.SalesAgentId;

            opportunitymodel.InstallationAddressId = Model.InstallationAddressId;
            opportunitymodel.BillingAddressId = Model.BillingAddressId;
            opportunitymodel.InstallationAddress = "";
            opportunitymodel.Description = Model.Description;

            opportunitymodel.SourcesTPId = Model.SourcesTPId;
            opportunitymodel.AccountId = Model.AccountId;
            opportunitymodel.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

            //Set Questions Answers
            opportunitymodel.OpportunityQuestionAns = new List<Type_Question>();

            return opportunitymodel;
        }

        [HttpPost]
        public IHttpActionResult SaveJobEstimator([FromBody]JobEstimator model)
        {
            try
            {
                //var res = QuotesMgr.SaveJobEstimator(model);
                //if (res)
                //{
                //    return Json(new { data = "Successful", error = "" });
                //}
                //else
                //{
                return Json(new { data = "", error = "Failed to Save." });
                //}
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }

        [HttpPost]
        public IHttpActionResult ApplyGroupDiscount(int id, List<QuoteGroupDiscount> lstGroupDiscount)
        {
            try
            {
                var result = QuotesMgr.SaveGroupDiscount(id, lstGroupDiscount);
                if (result != null)
                {
                    var QuoteLines = QuotesMgr.ApplyGroupDiscount(id);
                    var quote = QuotesMgr.GetDetails(id);
                    return Json(new { quoteKey = id, data = quote, error = "" });
                }
                else
                {
                    return Json(new { quoteKey = id, data = result, error = "An Error Occured while applying Group Discount" });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { quoteKey = id, data = "", error = "An Error Occured while applying Group Discount" });
            }

        }

    }
}