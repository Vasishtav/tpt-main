﻿using HFC.CRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using HFC.CRM.Cache.Cache;
using HFC.CRM.DTO.Source;
using HFC.CRM.Managers;
using HFC.CRM.Core;
using HFC.CRM.Data.Geolocator;
using HFC.CRM.Data;
using HFC.CRM.DTO.Lookup;
using HFC.CRM.Web.Filters;
using System.Data.SqlClient;
using HFC.CRM.Data.Context;
using Dapper;
using StackExchange.Profiling.Helpers.Dapper;
using HFC.CRM.Web.Models;
using HFC.Common.Logging;
using System.Dynamic;
using HFC.CRM.Web.Models.Measurement;
using HFC.CRM.Core.Membership;
using HFC.CRM.DTO.Product;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Web.Controllers.API
{
    public class MyCRMSourceMappingController : BaseApiController
    {
        MyCRMSourceMappingManager MyCRMSourceMappingManager = new MyCRMSourceMappingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        [HttpGet]
        public IHttpActionResult GetmyCRMSource(int id)
        {
            try
            {
                return Json(MyCRMSourceMappingManager.GetmyCRMSource());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetTPTSource(int id)
        {
            try
            {
                return Json(MyCRMSourceMappingManager.GetTPTSource());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        //SavemyCRMSourcemapping
        [HttpPost]
        public IHttpActionResult SavemyCRMSourcemapping(List<MyCRMSourceMap> MyCRMSourceMap)
        {
            try
            {
                MyCRMSourceMappingManager.SavemyCRMSourcemapping(MyCRMSourceMap);
                return Json("Success");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult ConfirmMyCRM_TPT_SourceMap(int id)
        {
            try
            {
                return Json(MyCRMSourceMappingManager.ConfirmMyCRM_TPT_SourceMap());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetConfirmMyCRM_TPT_SourceMap(int id)
        {
            try
            {
                var res = MyCRMSourceMappingManager.GetConfirmMyCRM_TPT_SourceMap();
                dynamic MyDynamic = new ExpandoObject();
                MyDynamic.Status = res;
                return Json(MyDynamic);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

    }
}
