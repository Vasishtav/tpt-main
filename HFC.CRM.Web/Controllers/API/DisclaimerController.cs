﻿using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class DisclaimerController : BaseApiController
    {
        DisclaimerManager Disclaim_Mangr = new DisclaimerManager();
        // to save
        [HttpPost]
        public IHttpActionResult Post([FromBody]DisclaimerModel model)
        {
            if (model != null)
            {
                var result = Disclaim_Mangr.Save(model);
                return ResponseResult("Success");
            }
            return ResponseResult("Failed");
        } 

        //to fetch the value for the view
        public IHttpActionResult Getvalue(int id)
        {
            var result = Disclaim_Mangr.GetDisclaimer();
            return Json(result);
        }
        //to get for the edit page
        public IHttpActionResult GetDataEdit(int FranchiseId,int Id)
        {
            var result = Disclaim_Mangr.GetEdit(FranchiseId, Id);
            return Json(result);
        }


    }
}
