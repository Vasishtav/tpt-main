﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class EmailSettingsController : BaseApiController
    {
        EmailSettingsManager mailsetmang = new EmailSettingsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        [HttpGet]
        public IHttpActionResult GetTerritoryEmailDetails(int id)
        {
            try
            {
                var result = mailsetmang.GetDetails();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
        [HttpGet]
        public IHttpActionResult GetTerritoryLocation(int id)
        {
            try
            {
                var location = mailsetmang.GetLoaction();
                return Json(location);
            }
            catch(Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
        [HttpGet]
        public IHttpActionResult GetAddressLocation(int Id)
        {
            try
            {
                var location = mailsetmang.GetLocAddress(Id);
                return Json(location);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
        [HttpPost]
        public IHttpActionResult SaveTerritoryDetails(int id,List<TerritoryEmailSettings> model)
        {
            try
            {
                    var result = mailsetmang.SaveData(model);
                    return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
    }
}
