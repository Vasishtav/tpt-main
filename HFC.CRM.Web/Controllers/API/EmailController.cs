﻿using HFC.CRM.Managers;

namespace HFC.CRM.Web.Controllers.API
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Http;

    using Core.Common;
    using Data;
    using Managers.AdvancedEmailManager;
    using Models.Email;
    using System.Data.Entity;
    using System;
    using Properties;
    using HFC.CRM.Web.Filters;
    using Core.Membership;
    using Core;
    using System.Net.Mail;
    using Core.Logs;
    using DTO.SentEmail;
    using Serializer.Leads;
    using SelectPdf;
    using Serializer;
    using Data.Context;
    using System.IO;
    using static Core.Common.ContantStrings;
    using System.Text;
    using HFC.CRM.DTO;

    [LogExceptionsFilter]
    public class EmailController : BaseApiController
    {
        OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        OrderManager orderMgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        SourceManager sourcemgr = new SourceManager();
        EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        #region Public Methods and Operators

        [HttpGet]
        public IHttpActionResult GetUnreadMessageCount()
        {
            if (!string.IsNullOrEmpty(SessionManager.CurrentUser.Domain) && (SessionManager.CurrentFranchise.EnableUnreadMessagesCounter ?? false))
            {
                ExchangeManager mseMgr = new ExchangeManager(dbContext: CRMDBContext);
                int totalCount = 0;
                if (mseMgr.GetUnreadEmailCount(SessionManager.CurrentUser.Email, out totalCount))
                {
                    return Json(new
                    {
                        unreadMessageCount = totalCount
                    });
                }

            }

            return Json(new { unreadMessageCount = 0 });
        }

        [HttpGet]
        public IHttpActionResult TemplateById(int id)
        {

            var emailTemplateManager = new EmailTemplateManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            var emailTemplate = emailTemplateManager.GetEmailTemplateById(id);
            var customFields = emailTemplateManager.GetCustomFieldsByTemplateType((EmailTemplateType)emailTemplate.EmailTemplateTypeId);
            var fieldsJob = emailTemplateManager.GetCustomFieldsByTemplateType(EmailTemplateType.Job);
            return Json(new { emailTemplate, customFields, fieldsJob });
        }

        [HttpPost]
        public IHttpActionResult Save(int id, EmailSendModel model)
        {
            model.Body = HttpUtility.HtmlDecode(model.Body);
            var emailTemplateManager = new EmailTemplateManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            emailTemplateManager.Update(model.EmailTemplateId, model.Subject, model.Body);
            return Json(new { success = true });
        }

        [HttpPost]
        public IHttpActionResult UpdateTemplateType(int id, EmailSendModel model)
        {
            var emailTemplateManager = new EmailTemplateManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            emailTemplateManager.UpdateTemplateType(id, model.EmailTemplateTypeId);
            return Json(new { success = true });
        }

        [HttpPost]
        public IHttpActionResult Send(int id, EmailSendModel model)
        {
            try
            {
                var emailManager = new CustomEmailManager(new DatabaseEmailTemplateProvider(SessionManager.CurrentFranchise, SessionManager.CurrentUser), new SentEmailManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser));
                var emailTemplateManager = new EmailTemplateManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                model.Body = HttpUtility.HtmlDecode(model.Body);
                if (model.SaveBody)
                {
                    emailTemplateManager.Update(model.EmailTemplateId, model.Subject, model.Body);
                }

                var person = new Person();
                if (model.PersonId > 0)
                    person = CRMDBContext.People.SingleOrDefault(x => x.PersonId == model.PersonId);

                if (model.EmailTemplateTypeId == 0)
                {
                    model.EmailTemplateTypeId = CRMDBContext.EmailTemplates.Where(x => x.EmailTemplateId == model.EmailTemplateId).SingleOrDefault().EmailTemplateTypeId;
                }

                var data = new BaseEmailTemplateModel();

                //job
                if (model.EmailTemplateTypeId == 7)
                {
                    data = FillJobTemplate(model.JobId, person);
                }

                //appointment
                if (model.EmailTemplateTypeId == 9)
                {
                    data = FillAppointmentTemplate(model.JobId, person, model.AppointmentId);
                }

                //reminder
                if (model.EmailTemplateTypeId == 8)
                {
                    data = FillReminderTemplate(model.JobId, person, model.AppointmentId, model);
                }

                if (model.BccAddresses == null)
                {
                    model.BccAddresses = new string[0];
                }
                if (model.Attachments == null)
                {
                    model.Attachments = new List<PhysicalFile>();
                }

                var attachments = model.Attachments.Select(file => HttpContext.Current.Server.MapPath(file.UrlPath)).ToList();
                emailManager.SendCustomMessage((short)model.EmailTemplateTypeId, (short)model.EmailTemplateId, model.ToAddresses, model.BccAddresses, model.Subject, model.Body, data, attachments, jobId: model.JobId);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetTemplateAdmin(int id)
        {
            var manager = new EmailTemplateManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            var models = manager.GetEmailTemplatesAdmin();
            return Json(models);
        }

        [HttpPost]
        public IHttpActionResult Add([FromBody] EmailTemplate model)
        {
            var emailTemplateManager = new EmailTemplateManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            emailTemplateManager.Add(model);
            return Json(new { success = true });
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var emailTemplateManager = new EmailTemplateManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            emailTemplateManager.Delete(id);
            return Json(new { success = true });
        }

        private JobTemplateModel FillJobTemplate(int jobId, Person person)
        {
            var job = CRMDBContext.Jobs.Where(x => x.JobId == jobId).Include(v => v.InstallAddress).Include(v => v.BillingAddress).Include(v => v.Invoices).SingleOrDefault();
            var quote = CRMDBContext.JobQuotes.Where(x => x.JobId == jobId).SingleOrDefault();

            string invoiceNumber = null;
            decimal paymentsValue = 0;
            if (job.Invoices != null)
            {
                var inv = job.Invoices.FirstOrDefault();
                if (inv != null)
                {
                    invoiceNumber = inv.InvoiceNumber.ToString();
                    var payments = CRMDBContext.Payments.Where(x => x.InvoiceId == inv.InvoiceId);
                    if (payments != null && payments.Count() > 0)
                    {
                        foreach (var item in payments)
                        {
                            paymentsValue = paymentsValue + item.Amount;
                        }
                    }
                }
            }
            var dto = new JobTemplateDTO();
            dto.FranchiseName = SessionManager.CurrentFranchise.Name;
            dto.JobNumber = job.JobNumber.ToString();
            dto.FirstName = person.FirstName;
            dto.LastName = person.LastName;
            //dto.MI = person.MI;
            dto.HomePhone = person.HomePhone;
            dto.CellPhone = person.CellPhone;
            dto.CompanyName = person.CompanyName;
            dto.WorkTitle = person.WorkTitle;
            dto.WorkPhone = person.WorkPhone;
            dto.SideMark = job.SideMark;
            dto.Hint = job.Hint;
            dto.JobDescription = job.Description;
            dto.FaxPhone = person.FaxPhone;
            dto.BillingAddress1 = job.BillingAddress.Address1;
            dto.BillingAddress2 = job.BillingAddress.Address2;
            dto.BillingCity = job.BillingAddress.City;
            dto.BillingState = job.BillingAddress.State;
            dto.BillingZipCode = job.BillingAddress.ZipCode;
            dto.InstallAddress1 = job.InstallAddress.Address1;
            dto.InstallAddress2 = job.InstallAddress.Address2;
            dto.InstallCity = job.InstallAddress.City;
            dto.InstallState = job.InstallAddress.State;
            dto.InstallZipCode = job.InstallAddress.ZipCode;
            dto.Subtotal = quote.Subtotal.ToString("N2");
            dto.DiscountTotal = quote.DiscountTotal.ToString("N2");
            dto.AdditionalChargesTotal = quote.SurchargeTotal.ToString("N2");
            dto.Total = Math.Abs(quote.NetTotal).ToString("N2");
            dto.TaxTotal = Math.Abs(quote.Taxtotal).ToString("N2");
            var balance = quote.NetTotal;
            if (job.Invoices != null && job.Invoices.Count > 0)
            {
                balance = job.Balance;
            }

            dto.BalanceDue = Math.Abs(balance).ToString("N2");
            dto.PaymentsTotal = paymentsValue.ToString("N2");
            dto.SurchargeTotal = quote.SurchargeTotal.ToString("N2");
            dto.TaxPercent = quote.TaxPercent.ToString("N2");
            dto.InvoiceNumber = invoiceNumber;
            return new JobTemplateModel(dto);
        }

        private AppointmentTemplateModel FillAppointmentTemplate(int jobId, Person person, int appointmentId)
        {
            var cal = (from c in CRMDBContext.Calendars
                       from p in c.Attendees
                       where c.CalendarId == appointmentId
                       select c)
                .Include(i => i.Organizer).FirstOrDefault();
            var job = CRMDBContext.Jobs.Where(x => x.JobId == jobId).Include(v => v.InstallAddress).Include(v => v.BillingAddress).Include(v => v.Invoices).SingleOrDefault();
            var quote = CRMDBContext.JobQuotes.Where(x => x.JobId == jobId).SingleOrDefault();

            string invoiceNumber = null;
            decimal paymentsValue = 0;
            if (job.Invoices != null)
            {
                var inv = job.Invoices.FirstOrDefault();
                if (inv != null)
                {
                    invoiceNumber = inv.InvoiceNumber.ToString();
                    var payments = CRMDBContext.Payments.Where(x => x.InvoiceId == inv.InvoiceId);
                    if (payments != null && payments.Count() > 0)
                    {
                        foreach (var item in inv.Payments)
                        {
                            paymentsValue = paymentsValue + item.Amount;
                        }
                    }
                }
            }
            var dto = new AppointmentTemplateDTO();
            dto.Subject = cal.Subject;
            dto.Location = cal.Location;
            dto.Message = cal.Message;
            dto.StartDate = cal.StartDate.ToString("G");
            dto.EndDate = cal.EndDate.ToString("G");
            dto.EventTypeEnum = cal.EventTypeEnum.ToString();

            var apptType = CacheManager.AppointmentTypes.SingleOrDefault(x => x.AppointmentTypeId == (int)cal.AptTypeEnum) ?? CacheManager.AppointmentTypes.SingleOrDefault(x => x.AppointmentTypeId == 1);
            dto.EventTypeEnum = apptType.Name;
            dto.AppointmentType = apptType.Name;

            dto.OrganizerFirstName = cal.Organizer.FirstName;
            dto.OrganizerLastName = cal.Organizer.LastName;
            //dto.OrganizerMI = cal.Organizer.MI;
            dto.OrganizerHomePhone = cal.Organizer.HomePhone;

            dto.OrganizerCellPhone = cal.Organizer.CellPhone;
            dto.OrganizerCompanyName = cal.Organizer.CompanyName;
            dto.OrganizerWorkTitle = cal.Organizer.WorkTitle;
            dto.OrganizerWorkPhone = cal.Organizer.WorkPhone;
            dto.OrganizerFaxPhone = cal.Organizer.FaxPhone;
            dto.OrganizerEmail = cal.OrganizerEmail;
            dto.AssignedName = cal.AssignedName;

            dto.FranchiseName = SessionManager.CurrentFranchise.Name;
            dto.JobNumber = job.JobNumber.ToString();
            dto.FirstName = person.FirstName;
            dto.LastName = person.LastName;
            //dto.MI = person.MI;
            dto.HomePhone = person.HomePhone;
            dto.CellPhone = person.CellPhone;
            dto.CompanyName = person.CompanyName;
            dto.WorkTitle = person.WorkTitle;
            dto.WorkPhone = person.WorkPhone;
            dto.SideMark = job.SideMark;
            dto.Hint = job.Hint;
            dto.JobDescription = job.Description;
            dto.FaxPhone = person.FaxPhone;
            dto.BillingAddress1 = job.BillingAddress.Address1;
            dto.BillingAddress2 = job.BillingAddress.Address2;
            dto.BillingCity = job.BillingAddress.City;
            dto.BillingState = job.BillingAddress.State;
            dto.BillingZipCode = job.BillingAddress.ZipCode;
            dto.InstallAddress1 = job.InstallAddress.Address1;
            dto.InstallAddress2 = job.InstallAddress.Address2;
            dto.InstallCity = job.InstallAddress.City;
            dto.InstallState = job.InstallAddress.State;
            dto.InstallZipCode = job.InstallAddress.ZipCode;
            dto.Subtotal = quote.Subtotal.ToString("N2");
            dto.DiscountTotal = quote.DiscountTotal.ToString("N2");
            dto.AdditionalChargesTotal = quote.SurchargeTotal.ToString("N2");
            dto.Total = Math.Abs(quote.NetTotal).ToString("N2");
            dto.TaxTotal = Math.Abs(quote.Taxtotal).ToString("N2");
            var balance = quote.NetTotal;
            if (job.Invoices != null && job.Invoices.Count > 0)
            {
                balance = job.Balance;
            }

            dto.BalanceDue = Math.Abs(balance).ToString("N2");
            dto.PaymentsTotal = paymentsValue.ToString("N2");
            dto.SurchargeTotal = quote.SurchargeTotal.ToString("N2");
            dto.TaxPercent = quote.TaxPercent.ToString("N2");
            dto.InvoiceNumber = invoiceNumber;
            return new AppointmentTemplateModel(dto);
        }

        private ReminderTemplateModel FillReminderTemplate(int jobId, Person person, int appointmentId, EmailSendModel model)
        {

            var cal = (from c in CRMDBContext.Calendars
                       from p in c.Attendees
                       where c.CalendarId == appointmentId
                       select c)
                .Include(i => i.Organizer).Include(i => i.Attendees).Include(i => i.Assigned).FirstOrDefault();

            List<string> addresses = new List<string>();
            //addresses.Add(cal.Organizer.PrimaryEmail);
            addresses.Add(cal.Assigned.PrimaryEmail);
            foreach (var item in cal.Attendees)
            {
                addresses.Add(item.PersonEmail);
            }

            var email = CRMDBContext.EmailTemplates.Where(x => x.FranchiseId == SessionManager.CurrentFranchise.FranchiseId && x.EmailTemplateTypeId == 8).SingleOrDefault();
            if (email == null)
            {
                throw new Exception(Resources.ReminderNotFound);
            }
            model.EmailTemplateId = email.EmailTemplateId;
            model.ToAddresses = addresses.Distinct().ToArray();
            model.Body = email.TemplateBody;
            model.Subject = email.TemplateSubject;
            var dto = new ReminderTemplateDTO
            {
                Subject = cal.Subject,
                Location = cal.Location,
                Message = cal.Message,
                StartDate = cal.StartDate.ToString("G"),
                EndDate = cal.EndDate.ToString("G"),
                EventTypeEnum = cal.EventTypeEnum.ToString()
            };

            var apptType = CacheManager.AppointmentTypes.SingleOrDefault(x => x.AppointmentTypeId == (int)cal.AptTypeEnum) ?? CacheManager.AppointmentTypes.SingleOrDefault(x => x.AppointmentTypeId == 1);
            dto.EventTypeEnum = apptType.Name;
            dto.AppointmentType = apptType.Name;

            dto.JobNumber = cal.JobNumber.ToString();
            dto.OrganizerFirstName = cal.Organizer.FirstName;
            dto.OrganizerLastName = cal.Organizer.LastName;
            //dto.OrganizerMI = cal.Organizer.MI;
            dto.OrganizerHomePhone = cal.Organizer.HomePhone;

            dto.OrganizerCellPhone = cal.Organizer.CellPhone;
            dto.OrganizerCompanyName = cal.Organizer.CompanyName;
            dto.OrganizerWorkTitle = cal.Organizer.WorkTitle;
            dto.OrganizerWorkPhone = cal.Organizer.WorkPhone;
            dto.OrganizerFaxPhone = cal.Organizer.FaxPhone;
            dto.OrganizerEmail = cal.OrganizerEmail;
            dto.AssignedName = cal.AssignedName;
            dto.FranchiseName = SessionManager.CurrentFranchise.Name;
            return new ReminderTemplateModel(dto);
        }
        #endregion

        #region ----- Tochpoint - SendEmail By List of Leads Ids -----

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult SendEmailByIds()
        {
            try
            {
                var result = emailMgr.SendRemaindarEmail();
                result = Communication.SendRemainderTextSms();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json("Error");
            }
        }

        //public IHttpActionResult SendEmailByIds()
        //{
        //    try
        //    {
        //        FranchiseManager _franMgr = new FranchiseManager(null);
        //        EmailManager emailMgr = new EmailManager(null, null);

        //        //
        //        //Get all the Calendar Events to whom the email reminders should be sent
        //        var CalendarList = emailMgr.GetEMailReminder();

        //        //To sending Reminder email
        //        //foreach (int leadId in LeadsIds)
        //        foreach (var cal in CalendarList)
        //        {
        //            var franchise = _franMgr.GetFEinfo(cal.FranchiseId);
        //            emailMgr = new EmailManager(null, franchise);
        //            int LeadId = 0;
        //            int AccountId = 0;
        //            if (cal.LeadId != null && cal.LeadId > 0)
        //            {
        //                LeadId = (int)cal.LeadId;
        //            }
        //            if (cal.AccountId != null && cal.AccountId > 0)
        //            {
        //                AccountId = (int)cal.AccountId;
        //                LeadId = 0;
        //            }
        //            if (LeadId != 0 || AccountId != 0)
        //            {
        //                List<string> toAddresses = new List<string>(), ccAddresses = new List<string>(), bccAddresses = new List<string>();

        //                var EmailSettings = emailMgr.GetConfiguredEmails().Where(x => x.EmailType == 3).FirstOrDefault();
        //                if (EmailSettings != null)
        //                {
        //                    if (cal.OpportunityId != null && cal.OpportunityId > 0)
        //                    {
        //                        if (EmailSettings.CopyAssignedSales == true)
        //                        {
        //                            string salesemail = emailMgr.GetSalesPersonEmail((int)cal.OpportunityId);
        //                            if (salesemail != null && salesemail != "")
        //                                ccAddresses.Add(salesemail);
        //                        }
        //                        if (EmailSettings.CopyAssignedInstaller == true)
        //                        {
        //                            string installeremail = emailMgr.GetInstallerEmail((int)cal.OpportunityId);
        //                            if (installeremail != null && installeremail != "")
        //                                ccAddresses.Add(installeremail);
        //                        }
        //                    }
        //                }

        //                string FranchiseDetail = emailMgr.GetFranchiseDetails(LeadId, AccountId);
        //                string[] FranchiseDetails = null;
        //                if (FranchiseDetail.Length > 0)
        //                {
        //                    FranchiseDetails = FranchiseDetail.Split(';');
        //                }

        //                //From email address
        //                var fromEmail = emailMgr.GetFranchiseEmail(cal.FranchiseId, 3);
        //                if (string.IsNullOrEmpty(fromEmail))
        //                {
        //                    fromEmail = FranchiseDetails[4];
        //                }
        //                var fromAddresses = new MailAddress(fromEmail);

        //                CustomerTP LeadCustomer = new CustomerTP();
        //                if (AccountId > 0)
        //                {
        //                    LeadCustomer = emailMgr.GetAccountEmail(AccountId);
        //                }
        //                else if (LeadId > 0)
        //                {
        //                    LeadCustomer = emailMgr.GetLeadEmail(LeadId);
        //                }

        //                //If Valid email then process the email for send
        //                if (LeadCustomer != null || fromAddresses != null)
        //                {
        //                    if (LeadCustomer != null)
        //                    {
        //                        if (LeadCustomer.PrimaryEmail != null && LeadCustomer.PrimaryEmail.Trim() != "")
        //                        {
        //                            toAddresses.Add(LeadCustomer.PrimaryEmail.Trim());
        //                        }
        //                        if (LeadCustomer.SecondaryEmail != null && LeadCustomer.SecondaryEmail.Trim() != "")
        //                        {
        //                            toAddresses.Add(LeadCustomer.SecondaryEmail.Trim());
        //                        }
        //                    }

        //                    //If no from or to emails return
        //                    if (toAddresses == null || toAddresses.Count == 0 || fromEmail == "")
        //                    {
        //                        continue;
        //                    }
        //                    //return Json("Success");

        //                    string TemplateSubject = "", bodyHtml = "";

        //                    //Email Template: 1 Thanks Email; 2 Appointment Confirmation; 3 Reminder Email
        //                    int BrandId = Convert.ToInt16(FranchiseDetails[2]);

        //                    //Take Body Html from EmailTemplate Table
        //                    var emailtemplate = emailMgr.GetHtmlBody(3, BrandId);
        //                    TemplateSubject = emailtemplate.TemplateSubject;
        //                    bodyHtml = emailtemplate.TemplateLayout;
        //                    short TemplateEmailId = (short)emailtemplate.EmailTemplateId;

        //                    //cc all the users from calendarevent
        //                    //ccAddresses = cal.Attendees.Select(x => x.PersonEmail).ToList();

        //                    var AppDate = (DateTimeOffset)cal.StartDate;

        //                    var Franchiseinfo = GetFranchiseInfo(cal.FranchiseId);

        //                    if (Franchiseinfo == null) continue;

        //                    string salesperson = "";

        //                    foreach (var person in cal.Attendees)
        //                    {
        //                        var user = LocalMembership.GetUser((int)person.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
        //                        var res = user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId);
        //                        salesperson = user.Person.FullName;
        //                        if (res == true) break;
        //                    }

        //                    var mailMsg = emailMgr.BuildMailMessageReminder(LeadId, AccountId,
        //                        cal.OpportunityId != null && cal.OpportunityId > 0 ? (int)cal.OpportunityId : 0,
        //                        cal.OrderId != null && cal.OrderId > 0 ? (int)cal.OrderId : 0,
        //                        AppDate.DateTime, cal.Location, salesperson,
        //                        bodyHtml, TemplateSubject
        //                        , TemplateEmailId, BrandId, Franchiseinfo
        //                        , fromEmail, toAddresses, ccAddresses, bccAddresses);
        //                }
        //            }

        //        }
        //        return Json("Success");
        //    }
        //    catch (Exception ex)
        //    {
        //        EventLogger.LogEvent(ex);
        //        return Json("Error");
        //    }

        //}

        //private Franchise GetFranchiseInfo(int FranchiseId)
        //{
        //    var frnManager = new FranchiseManager(null);
        //    if (FranchiseId > 0)
        //    {
        //        var fedata = frnManager.GetFEinfo(FranchiseId);
        //        return fedata;
        //    }
        //    else
        //        return null;
        //}

        #endregion ----- Tochpoint - SendEmail By List of Leads Ids -----


        [HttpPost]
        public IHttpActionResult SendEmail(EmailData data)
        {
            try
            {
                List<string> ccAddresses = new List<string>();
                List<string> bccAddresses = new List<string>();
                string fileDownloadName = "";

                if (data.ccAddresses != "")
                    ccAddresses = data.ccAddresses.Split(',').ToList();

                if (data.bccAddresses != "")
                    bccAddresses = data.bccAddresses.Split(',').ToList();

                int? LeadId = null, AccountId = null, OpportunityId = null, OrderId = null;

                ExchangeManager exchange = new ExchangeManager();
                EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                TerritoryDisplay territoryDisplay = new TerritoryDisplay();

                if (data.Associatedwith == "Lead")
                {
                    LeadId = data.id;

                    if (LeadId != null && LeadId > 0)
                        territoryDisplay = emailMgr.GetTerritoryDisplayByLeadId(LeadId ?? 0);
                }
                if (data.Associatedwith == "Opportunity")
                {
                    var oppInfo = emailMgr.GetOpportunityInfo(data.id);
                    OpportunityId = data.id;
                    AccountId = oppInfo.AccountId;

                    if (OpportunityId != null && OpportunityId > 0)
                        territoryDisplay = emailMgr.GetTerritoryDisplayByOpp(OpportunityId ?? 0);
                }

                if (data.Associatedwith == "Order")
                {
                    var orderInfo = emailMgr.GetOrderInfo(data.id);
                    OrderId = data.id;
                    OpportunityId = orderInfo.OpportunityId;
                    AccountId = orderInfo.AccountId;

                    if (OpportunityId != null && OpportunityId > 0)
                        territoryDisplay = emailMgr.GetTerritoryDisplayByOpp(OpportunityId ?? 0);
                }

                byte[] pdf = null;

                if (data.packetType == "SalesPacket" && data.printOptions != "")
                {
                    pdf = PrintSalesPacket(data.id, data.Associatedwith, data.printOptions, data.salesPacketOption, out fileDownloadName);
                }
                if (data.packetType == "InstallationPacket" && data.printOptions != "")
                {
                    pdf = InstallationPacket(data.id, data.printOptions, data.installPacketOptions, out fileDownloadName);
                }
                var emailtemplate = emailMgr.GetHtmlBody(EmailType.GlobalEmail, SessionManager.CurrentFranchise.BrandId); //10, SessionManager.CurrentFranchise.BrandId);
                var body = emailtemplate.TemplateLayout;
                data.subject = HttpUtility.HtmlDecode(data.subject);
                data.body = HttpUtility.HtmlDecode(data.body);

                string[] BrandNameAbbr = { "BB", "TL", "CC" };
                //To get base server url to send with image 
                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
                string ServerImgPath = "https://" + baseUrl + "/Images/brand/" + BrandNameAbbr[SessionManager.CurrentFranchise.BrandId - 1] + "/";

                #region Email Signature
                var CurrentUser = SessionManager.CurrentUser;
                var CurrentFranchise = SessionManager.CurrentFranchise;

                // Get Company name
                string company = "";
                if (territoryDisplay != null && territoryDisplay.UseFranchiseName == false)
                {
                    if (territoryDisplay.Displayname != "")
                        company = territoryDisplay.Displayname;
                    else
                        company = CurrentFranchise.Name;
                }
                else
                    company = CurrentFranchise.Name;


                // Validate EmailSignature is enabled or not
                bool Emailsignature = false;
                if (CurrentUser.Person.EnableEmailSignature && CurrentUser.Person.AddEmailSignAllEmails)
                    Emailsignature = true;
                else if (CurrentUser.Person.EnableEmailSignature && data.Emailsignature)
                    Emailsignature = true;

                if (Emailsignature)
                {
                    var PCellPhone = CurrentUser.Person.CellPhone;
                    PCellPhone = PCellPhone != null && PCellPhone.Length > 0 ? Convert.ToInt64(PCellPhone).ToString(phoneFormat) : "";

                    var fBusinessPhone = CurrentFranchise.BusinessPhone;
                    fBusinessPhone = fBusinessPhone != null && fBusinessPhone.Length > 0 ? Convert.ToInt64(fBusinessPhone).ToString(phoneFormat) : "";

                    string site = "", logo = "";

                    if (CurrentFranchise.BrandId == 1)
                    {
                        site = BBSitelink;
                        logo = ServerImgPath + "BB-logo.png";
                    }
                    else if (CurrentFranchise.BrandId == 2)
                    {
                        site = TLSitelink;
                        logo = ServerImgPath + "tailored_logo.png";
                    }
                    else if (CurrentFranchise.BrandId == 3)
                    {
                        site = CCSitelink;
                        logo = ServerImgPath + "concrete_logo.png";
                    }

                    StringBuilder sbsign = new StringBuilder();
                    sbsign.Append("<br /><br /><br />");
                    sbsign.Append("<div class='email_signature'>");
                    sbsign.Append("<h1 style='font-size:12pt; font-weight:bold; line-height:9pt; color:#000;'>" + CurrentUser.Person.FullName + "</h1>");
                    sbsign.Append("<p style='font-size:10pt; line-height:7pt; color:#000;'>" + CurrentUser.Person.WorkTitle + " - " + company + "</p>");
                    sbsign.Append("<p style='font-size:9pt; line-height:7pt; color:#000;'>" + CurrentFranchise.Address.ToString() + "</p>");
                    if (PCellPhone != "")
                        sbsign.Append("<p style='font-size:9pt; line-height:7pt; color:#000;'>phone " + fBusinessPhone + " | mobile " + PCellPhone + "</p>");
                    else
                        sbsign.Append("<p style='font-size:9pt; line-height:7pt; color:#000;'>phone " + fBusinessPhone + "</p>");
                    sbsign.Append("<p style='font-size:9pt; line-height:7pt; color:#000;'>" + CurrentUser.Email + " | <a style='color: #3e7d8a; text-decoration:underline;' href='" + site + "'>" + site?.Replace("http:\\", "") + "</a></p>");
                    sbsign.Append("<img src='" + logo + "' style='width:140px;' alt=''/>");
                    sbsign.Append("</div>");
                    data.body = data.body + sbsign.ToString();
                }
                #endregion


                body = body.Replace("{IMAGEPATH}", ServerImgPath).Replace("{bodycontent}", data.body);

                exchange.SendEmailMultipleAttachment(SessionManager.CurrentUser.Person.PrimaryEmail, data.subject, body,
                    data.toAddresses.Split(',').ToList(), ccAddresses,
                             bccAddresses, data.streamid, pdf, fileDownloadName);


                emailMgr.AddSentEmail(data.toAddresses, data.subject, body, true, "", ccAddresses, bccAddresses, SessionManager.CurrentUser.Person.PrimaryEmail, LeadId, AccountId, OpportunityId, null, emailtemplate.EmailTemplateId, OrderId);
                return Json(true);

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex.Message);
            }
        }


        #region Print Touchpoint


        public byte[] InstallationPacket(int id, string printoptions, string installPacketOptions, out string fileDownloadName)
        {

            var options = printoptions.ToString().Split(new char[] { '/' }).ToList();
            fileDownloadName = string.Format("PrintDocuments.pdf");
            string data = string.Empty;
            string leaddata = string.Empty;
            string measurementdata = string.Empty;
            string googledata = string.Empty;
            string customerdata = string.Empty;
            string orderdata = string.Empty;

            try
            {
                var leadDetails = options.ToList().Where(c => c.Contains("AddleadCustomer"))
                .FirstOrDefault();
                var googleMap = options.ToList().Where(c => c.ToLower().Contains("googlemap"))
                    .FirstOrDefault();
                var existingMeasurements = options.ToList().Where(c => c.Contains("existingMeasurements"))
                   .FirstOrDefault();

                var order = orderMgr.GetOrderByOrderId(id);
                var Details = QuotesMgr.GetSidemarkOppId(order.QuoteKey);

                if (!string.IsNullOrEmpty(leadDetails) && leadDetails.Contains("true"))
                {
                    data = LeadSheetReport.GetLeadCustomerContentForInstallationPacket(order.OpportunityId);
                }

                // instantiate a html to pdf converter object 
                HtmlToPdf converter = new HtmlToPdf();

                converter.Options.MarginTop = 20;
                converter.Options.MarginBottom = 30;
                converter.Options.MarginLeft = 10;
                converter.Options.MarginRight = 10;

                converter.Options.DisplayHeader = true;
                converter.Header.DisplayOnFirstPage = true;
                converter.Header.DisplayOnOddPages = true;
                converter.Header.DisplayOnEvenPages = true;

                converter.Options.DisplayFooter = true;
                converter.Footer.DisplayOnFirstPage = true;
                converter.Footer.DisplayOnEvenPages = true;
                converter.Footer.DisplayOnOddPages = true;

                converter.Options.PdfPageSize = PdfPageSize.Letter;

                // page numbers can be added using a PdfTextSection object
                //PdfTextSection text = new PdfTextSection(0, 10, "Page {page_number} of {total_pages}", new System.Drawing.Font("Arial", 8));
                //text.HorizontalAlign = PdfTextHorizontalAlign.Right;
                //converter.Footer.Add(text);

                string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();
                PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

                if (string.IsNullOrEmpty(data))
                {
                    doc.RemovePageAt(0);
                }

                //// https://selectpdf.com/docs/ModifyExistingPdf.htm

                var internalNotes = options.ToList().Where(c => c.Contains("AddPrintInternalNotes")) //"internalNotes"))
                   .FirstOrDefault();
                var externalNotes = options.ToList().Where(c => c.Contains("AddPrintExternalNotes")) // "externalNotes"))
                   .FirstOrDefault();

                var directionNotes = options.ToList().Where(c => c.Contains("AddPrintDirectionNotes")) // "externalNotes"))
                  .FirstOrDefault();

                var installChecklist = options.ToList()
                    .Where(c => c.Contains("AddInstallationChecklist"))
                    .FirstOrDefault();

                if (!string.IsNullOrEmpty(leadDetails))
                {
                    internalNotes = internalNotes.Contains("true") ? "internalNotes" : "";
                    externalNotes = externalNotes.Contains("true") ? "externalNotes" : "";
                    directionNotes = directionNotes.Contains("true") ? "directionNotes" : "";
                    var notes = GetNotes(id, internalNotes, externalNotes, directionNotes, "Order");
                    if (notes != null) doc.Append(notes);
                }

                if (!string.IsNullOrEmpty(googleMap) && googleMap.ToLower().Contains("true"))
                {
                    var google = GetGooglemap(order.OpportunityId, googleMap, "Opportunity");
                    if (google != null) doc.Append(google);
                }

                var AddOrderCustomerInvoice = options.ToList().Where(c => c.Contains("AddOrderCustomerInvoice")).SingleOrDefault().Contains("true");
                var AddOrderInstallerInvoice = options.ToList().Where(c => c.Contains("AddOrderInstallerInvoice")).SingleOrDefault().Contains("true");
                var OrderPrintFormat = options.ToList().Where(c => c.Contains("OrderPrintFormat")).SingleOrDefault();
                PdfDocument docinvoice = new PdfDocument();
                PdfDocument docinstaller = new PdfDocument();
                if (id > 0)
                {
                    if (AddOrderCustomerInvoice)  //Customer Invoice
                    {
                        PrintVersion printVersion = PrintVersion.ExcludeDiscount;
                        if (!string.IsNullOrEmpty(OrderPrintFormat))
                        {
                            var temp = OrderPrintFormat.Split('=');
                            if (temp.Length > 1 && temp[1] == "IncludeDiscount")
                                printVersion = PrintVersion.IncludeDiscount;
                            else if (temp.Length > 1 && temp[1] == "CondensedVersion")
                                printVersion = PrintVersion.CondensedVersion;
                            else
                                printVersion = PrintVersion.ExcludeDiscount;
                        }

                        InvoiceSheetReport InvoiceSheet = new InvoiceSheetReport();
                        docinvoice = InvoiceSheet.createInvoicePDFDoc(id, printVersion);
                        doc.Append(docinvoice);
                    }

                    if (AddOrderInstallerInvoice) //Installer invoice
                    {
                        InstallerDocument installerDocument = new InstallerDocument();
                        docinstaller = installerDocument.createInstallerPDFDoc(id);
                        doc.Append(docinstaller);
                    }
                }

                var installOptions = installPacketOptions.ToString().Split(new char[] { '/' }).ToList();
                var sp = GetSalesPacketdocumentsPdf(installOptions.ToList());
                if (sp != null)
                    doc.Append(sp);


                var mydocuments = options.Where(c => c.Contains("mydocuments_")).ToList();
                var md = GetMydocumentsPdf(mydocuments);
                if (md != null) doc.Append(md);

                //var installationGuide = GetMydocumentsPdf(mydocuments);
                //if (md != null) doc.Append(md);


                // save pdf document 
                //byte[] pdf = doc.Save();

                //added the code to remove pdf. instead of sending blank pdf 
                byte[] pdf = null;
                if (doc.Pages.Count != 0)
                {
                    pdf = doc.Save();
                }


                doc.Close();

                //for naming pdf
                var IsleadDetails = options.ToList().Where(c => c.Contains("AddleadCustomer")).SingleOrDefault().Contains("true");
                var IsgoogleMap = options.ToList().Where(c => c.ToLower().Contains("googlemap")).SingleOrDefault().Contains("true");
                if (AddOrderInstallerInvoice == true)
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2} - INSTALL DOC.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));
                else if (IsleadDetails && IsgoogleMap && AddOrderCustomerInvoice && !string.IsNullOrEmpty(installPacketOptions))
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2} - Order Packet.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));
                else
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2}.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));

                return pdf;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        public byte[] PrintSalesPacket(int id, string sourceType, string printoptions, string salesPacketOption, out string fileDownloadName)
        {
            string data = string.Empty;
            fileDownloadName = string.Format("PrintDocuments.pdf");
            bool IsPacket = false;
            var options = printoptions.ToString().Split(new char[] { '/' }).ToList();

            try
            {

                PdfDocument doc = new PdfDocument();

                var leadDetails = options.ToList().Where(c => c.Contains("leadDetails"))
                .FirstOrDefault();
                var internalNotes = options.ToList().Where(c => c.Contains("internalNotes"))
                  .FirstOrDefault();
                var externalNotes = options.ToList().Where(c => c.Contains("externalNotes"))
                   .FirstOrDefault();
                var directionNotes = options.ToList().Where(c => c.Contains("directionNotes"))
                   .FirstOrDefault();

                var googleMap = options.ToList().Where(c => c.Contains("googleMap"))
                    .FirstOrDefault();
                var existingMeasurements = options.ToList().Where(c => c.Contains("existingMeasurements"))
                   .FirstOrDefault();
                var bbmeasurementForm = options.ToList()
                    .Where(c => c.Contains("bbmeasurementForm"))
                    .FirstOrDefault();
                var ccmeasurementForm = options.ToList()
                   .Where(c => c.Contains("ccmeasurementForm"))
                   .FirstOrDefault();
                var garagemeasurementForm = options.ToList()
                   .Where(c => c.Contains("garagemeasurementForm"))
                   .FirstOrDefault();
                var closetmeasurementForm = options.ToList()
                   .Where(c => c.Contains("closetmeasurementForm"))
                   .FirstOrDefault();
                var officemeasurementForm = options.ToList()
                   .Where(c => c.Contains("officemeasurementForm"))
                   .FirstOrDefault();
                var floormeasurementForm = options.ToList()
                  .Where(c => c.Contains("floormeasurementForm"))
                  .FirstOrDefault();

                if (!string.IsNullOrEmpty(leadDetails) && !string.IsNullOrEmpty(googleMap) && !string.IsNullOrEmpty(existingMeasurements) && !string.IsNullOrEmpty(salesPacketOption))
                    IsPacket = true;

                if (!string.IsNullOrEmpty(leadDetails))
                {
                    data = LeadSheetReport.GetLeadCustomerContent(id, options, sourceType);

                    var leadSheet = GetLeadSheetPdf(data);
                    if (leadSheet != null)
                    {
                        var notes = GetNotes(id, internalNotes, externalNotes, directionNotes, sourceType);
                        if (notes != null)
                        {
                            leadSheet.Append(notes);
                        }
                        doc.Append(leadSheet);
                    }
                }

                var google = GetGooglemap(id, googleMap, sourceType);
                if (google != null)
                {
                    doc.Append(google);
                }

                var em = GetExistingMeasurements(id, existingMeasurements, sourceType);
                if (em != null)
                {
                    doc.Append(em);
                }

                var bbm = GetbbmeasurementForm(bbmeasurementForm);
                if (bbm != null)
                {
                    doc.Append(bbm);
                }

                var ccm = GetccmeasurementForm(ccmeasurementForm);
                if (ccm != null) doc.Append(bbm);

                var garage = GetgaragemeasurementForm(garagemeasurementForm);
                if (garage != null) doc.Append(garage);

                var closet = GetclosetmeasurementForm(closetmeasurementForm);
                if (closet != null) doc.Append(closet);

                var office = GetofficemeasurementForm(officemeasurementForm);
                if (office != null) doc.Append(office);

                var floor = GetfloormeasurementForm(floormeasurementForm);
                if (floor != null) doc.Append(floor);


                var quote = options.ToList().Where(c => c == "quote").SingleOrDefault();

                //moved the code to get the quotekey
                int quotekey = 0;
                var quotekeyvalue = options.ToList().Where(c => c.Contains("quoteKey")).FirstOrDefault();
                if (!string.IsNullOrEmpty(quotekeyvalue))
                {
                    string[] arrquotekey = quotekeyvalue.Split('|');
                    if (arrquotekey != null && arrquotekey.Length == 2 && !string.IsNullOrEmpty(arrquotekey[1]))
                        int.TryParse(arrquotekey[1], out quotekey);//quotekey = Convert.ToInt32(arrquotekey[1]);
                }

                if (sourceType == "Opportunity" && !string.IsNullOrEmpty(quote))
                {
                    var quotePrintFormat = options.ToList().Where(c => c.Contains("quotePrintFormat")).SingleOrDefault();

                    PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                    if (!string.IsNullOrEmpty(quotePrintFormat))
                    {
                        var temp = quotePrintFormat.Split('=');
                        if (temp.Length > 1 && temp[1] == "IncludeDiscount")
                            printVersion = PrintVersion.IncludeDiscount;
                        else if (temp.Length > 1 && temp[1] == "CondensedVersion")
                            printVersion = PrintVersion.CondensedVersion;
                        else
                            printVersion = PrintVersion.ExcludeDiscount;
                    }

                    if (quotekey == 0)
                    {
                        var oppmgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        var opportunity = oppmgr.Get(id);
                        var quotemgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        var quoteobj = quotemgr.GetQuote(id).Where(q => q.PrimaryQuote == true).FirstOrDefault();
                        if (quoteobj != null) quotekey = quoteobj.QuoteKey;
                    }

                    if (quotekey != 0)
                    {
                        QuoteSheetReport quotesheet = new QuoteSheetReport();
                        var docQuote = quotesheet.createQuotePDFDoc(quotekey, printVersion);
                        doc.Append(docQuote);
                    }

                }
                if (salesPacketOption != "")
                {
                    var salesOptions = salesPacketOption.ToString().Split(new char[] { '/' }).ToList();
                    var sp = GetSalesPacketdocumentsPdf(salesOptions.ToList());
                    if (sp != null)
                        doc.Append(sp);
                }
                var mydocuments = options.Where(c => c.Contains("mydocuments_")).ToList();


                var md = GetMydocumentsPdf(mydocuments);
                if (md != null) doc.Append(md);

                // save pdf document 
                //added the code to remove pdf. instead of sending blank pdf 
                byte[] pdf = null;
                if (doc.Pages.Count != 0)
                {
                    pdf = doc.Save();
                }

                doc.Close();

                //for pdf name in email
                if (sourceType == "Lead")
                    fileDownloadName = string.Format("LeadSheet.pdf");
                else if (sourceType == "Opportunity" && quotekey != 0)
                    fileDownloadName = QuotesMgr.GetQuotebyQuoteKey(quotekey);
                else if (sourceType == "Opportunity")
                    fileDownloadName = OpportunityMgr.GetOppbyOppId(id, IsPacket);

                return pdf;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        private PdfDocument GetExistingMeasurements(int id, string existingMeasurements, string sourceType = "Lead")
        {
            if (id == 0 || string.IsNullOrEmpty(existingMeasurements) ||
                sourceType != "Opportunity") return null;

            var result = LeadSheetReport.GetExistingMeasurements(id);
            if (string.IsNullOrEmpty(result)) return null;

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            //HtmlToPdf converter = new HtmlToPdf();
            var converter = GetPdfConverter(false);
            var doc = converter.ConvertHtmlString(result, baseUrl);
            return doc;
        }

        private HtmlToPdf GetPdfConverter(bool addPageNumber = true)
        {
            // instantiate a html to pdf converter object 
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;

            converter.Options.PdfPageSize = PdfPageSize.Letter;

            // page numbers can be added using a PdfTextSection object
            if (addPageNumber)
            {
                PdfTextSection text = new PdfTextSection(0, 10, "Page {page_number} of {total_pages}", new System.Drawing.Font("Arial", 8));
                text.HorizontalAlign = PdfTextHorizontalAlign.Right;
                converter.Footer.Add(text);
            }

            return converter;
        }

        private PdfDocument GetLeadSheetPdf(string data)
        {
            if (string.IsNullOrEmpty(data)) return null;

            //HtmlToPdf converter = new HtmlToPdf();
            var converter = GetPdfConverter();

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();
            var doc = converter.ConvertHtmlString(data, baseUrl);
            return doc;
        }

        private PdfDocument GetGooglemap(int id, string googleMap, string sourceType = "Lead")
        {
            try
            {
                EventLogger.LogEvent(id + " " + sourceType, "GetGooglemap", LogSeverity.Information);
                if (id == 0 || string.IsNullOrEmpty(googleMap)) return null;

                string result = "";

                if (sourceType == "Lead")
                {
                    result = LeadSheetReport.GetGoogleMapForLead(id);
                }
                else if (sourceType == "Opportunity")
                {
                    result = LeadSheetReport.GetGoogleMapForOpportunity(id);
                }
                else if (sourceType == "Order")
                {
                    result = LeadSheetReport.GetGoogleMapForOpportunity(id);
                }

                if (string.IsNullOrEmpty(result)) return null;

                //HtmlToPdf converter = new HtmlToPdf();

                string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

                var converter = GetPdfConverter(false);
                converter.Options.MinPageLoadTime = 3;
                var doc = converter.ConvertHtmlString(result, baseUrl);
                return doc;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        private PdfDocument GetNotes(int id, string internalNotes
            , string externalNotes, string directionNotes
            , string sourceType = "Lead")
        {
            if (id == 0 || (string.IsNullOrEmpty(internalNotes)
                && string.IsNullOrEmpty(externalNotes)
                && string.IsNullOrEmpty(directionNotes))) return null;

            string result = "";
            if (sourceType == "Lead")
            {
                result = LeadSheetReport.printLeadNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionNotes));
            }
            else if (sourceType == "Opportunity")
            {
                result = LeadSheetReport.printOpportunityNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionNotes));

            }
            else if (sourceType == "Account")
            {
                result = LeadSheetReport.printAccountNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionNotes));

            }
            else if (sourceType == "Order")
            {
                result = LeadSheetReport.printOrderNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionNotes));
            }

            if (string.IsNullOrEmpty(result)) return null;

            // We have notes content, so we can create the pdf document with teh notes content.
            string filename = HttpContext.Current.Server.MapPath("/Templates/base/notes.html");
            var data = System.IO.File.ReadAllText(filename);

            if (string.IsNullOrEmpty(data)) return null;

            data = data.Replace("{{notes}}", result);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            var converter = GetPdfConverter(false);
            var doc = converter.ConvertHtmlString(data, baseUrl);
            return doc;
        }

        private PdfDocument GetbbmeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;
            //  string filename = Server.MapPath("/Templates/Base/Brand/BB/BB Measurement Form.pdf");

            var result = sourcemgr.GetStreamIdForDocument(102);
            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetccmeasurementForm(string value)
        {

            // TODO: the Pdf is not yet defined;
            //https://hfcprojects.atlassian.net/wiki/spaces/TOUC/pages/84902001/Print+Measurement+Forms+-+CC
            return null;

            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            var result = sourcemgr.GetStreamIdForDocument(101);

            if (result == null) return null;


            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 1; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetgaragemeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 3 Garage Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);

            var result = sourcemgr.GetStreamIdForDocument(203);

            if (result == null) return null;


            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetclosetmeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 1 Closet Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);

            var result = sourcemgr.GetStreamIdForDocument(201);

            if (result == null) return null;


            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);


            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetofficemeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 2 Office Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);


            var result = sourcemgr.GetStreamIdForDocument(202);

            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetfloormeasurementForm(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 4 Floor Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);


            var result = sourcemgr.GetStreamIdForDocument(204);

            if (result == null) return null;


            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetMydocumentsPdf(List<string> values)
        {
            if (values == null || values.Count == 0) return null;

            PdfDocument doc = new PdfDocument();

            foreach (var item in values)
            {
                if (string.IsNullOrEmpty(item)) continue;

                var temp = item.Split('|');
                var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

                if (count == 0) continue;

                var temp2 = temp[0].Split('_');
                var noteid = temp2.Length > 1 ? int.Parse(temp2[1]) : 0;
                if (noteid == 0) continue;

                var note = noteMgr.Get(noteid);
                var data = FileTable.GetFileDataSync(note.AttachmentStreamId.ToString());
                var stream = new MemoryStream(data.bytes);

                //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 4 Floor Measurement Form.pdf");
                // PdfDocument pdf = new PdfDocument(stream);

                for (int index = 0; index < count; index++)
                {
                    var obj = new PdfDocument(stream); //PdfDocument(filename);
                    doc.Append(obj);
                }
            }

            return doc;
        }

        private PdfDocument GetSalesPacketdocumentsPdf(List<string> values)
        {
            if (values == null || values.Count == 0) return null;

            PdfDocument doc = new PdfDocument();

            foreach (var item in values)
            {
                if (string.IsNullOrEmpty(item)) continue;

                var temp = item.Split('|');
                var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

                if (count == 0) continue;

                var streamId = temp[0];
                //  var noteid = temp2.Length > 1 ? int.Parse(temp2[1]) : 0;
                //  if (noteid == 0) continue;

                // var note = noteMgr.Get(noteid);
                var data = FileTable.GetFileDataSync(streamId.ToString());
                var stream = new MemoryStream(data.bytes);

                //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 4 Floor Measurement Form.pdf");
                // PdfDocument pdf = new PdfDocument(stream);

                for (int index = 0; index < count; index++)
                {
                    var obj = new PdfDocument(stream); //PdfDocument(filename);
                    doc.Append(obj);
                }
            }

            return doc;
        }

        #endregion


    }




}