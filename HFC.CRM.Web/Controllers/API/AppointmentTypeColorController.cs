﻿using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;

namespace HFC.CRM.Web.Controllers.API
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Http;

    using Core.Common;
    using Data;
    using Managers.AdvancedEmailManager;
    using Models.Email;
    using System.Data.Entity;
    using System;
    using Properties;
    using HFC.CRM.Web.Models.Color;
    using HFC.CRM.Managers.DTO;

    [LogExceptionsFilter]
    public class AppointmentTypeColorController : BaseApiController
    {
        #region Public Methods and Operators

        readonly AppointmentTypeColorManager colorManager = new AppointmentTypeColorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        [HttpGet]
        public IHttpActionResult GetColors(int id)
        {
            var colorlist = colorManager.GetColors();
            return Json(colorlist);
        }

        //[HttpPost]
        //public IHttpActionResult EnableColors(int id, AppTypeColorModel model)
        //{
        //    var res = colorManager.EnableColors(model.EnableColor);
        //    return Json(new {success = true});
        //}


        [HttpPost]
        public IHttpActionResult SaveColors(FranchiseAppointment_TypeColors model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                status = colorManager.ColorSave(model);
            }
            return Json(status);
        }

        [HttpPost]
        public IHttpActionResult UpdateColors(FranchiseAppointment_TypeColors model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                status = colorManager.ColorUpdate(model);
            }
            return Json(status);
        }

        #endregion
    }
}