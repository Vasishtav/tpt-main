﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Managers;
using System.Data.SqlClient;
using HFC.CRM.Data.Context;
using StackExchange.Profiling.Helpers.Dapper;
using HFC.CRM.Core.Common;

namespace HFC.CRM.Web.Controllers.API
{
    public class ProcurementController : BaseApiController
    {
        ProcurementDashboardManager mgr = new ProcurementDashboardManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        VendorManager VendorMngr = new VendorManager();
        PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);


        public IHttpActionResult Get(int id, string listStatus, string dateFilter)
        {
            List<int> value = new List<int>();

            if (listStatus != null)
            {
                var v1 = listStatus.Split(',').ToList();
                value = v1.Select(int.Parse).ToList();
            }

            if (dateFilter == null || dateFilter == "null")
                dateFilter = "";

            var result = mgr.Get(id, value, dateFilter);
            //var result = mgr.Get(id, listStatus);List<int>
            var GetPermission = permissionManager.GetUserRolesPermissions();
            var ModuleData = GetPermission.Find(x => x.ModuleCode == "ProcurementDashboard");
            var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "ProcurementDashboardAllRecordAccess").CanAccess;
            if (result != null)
            {
                if (specialPermission == false)
                {
                    // Need to apply the filter for SalesAgent/Installation.
                    var filtered = result.Where(x => (x.SalesAgentId ==
                                        SessionManager.CurrentUser.PersonId ||
                                        x.InstallerId == SessionManager.CurrentUser.PersonId)
                                        ).ToList();

                    result = filtered;
                }

                result = result.OrderByDescending(x => x.PICPO).ToList();
                return Json(result);
            }

            return OkTP();
        }
        /// <summary>
        /// Get all active Vendors for dropdown
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult VendorOption()
        {
            //pass value 2 for Alliance Vendors
            var result = VendorMngr.Getvalue(2);
            return Json(result.Where(x => x.VendorStatus == "Active").ToList());
        }

        //added to get active vendor for drop-down
        [HttpGet]
        public IHttpActionResult VendorNameList(int id)
        {
            var result = mgr.GetVendorname(id);
            return Json(result);
        }
        [HttpGet]
        public IHttpActionResult AdminVendorNameList(int id)
        {
            var result = mgr.GetAdminVendorname(id);
            return Json(result);
        }
        [HttpGet]
        public IHttpActionResult GetData(int id, int franchiseid, string dateFilter)
        {
            var result = mgr.GetData(franchiseid, id, dateFilter);

            if (result != null)
            {
                return Json(
                    result
                );
            }


            return OkTP();
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult SendProcurementIssueEmail()
        {
            var result = mgr.SendProcurementIssueEmail();

            if (result != null)
            {
                return Json(
                    result
                );
            }


            return OkTP();
        }

        //public IHttpActionResult GetData(int id, int franchiseid, string listStatus)
        //{
        //    List<int> value = new List<int>();

        //    if (listStatus != null)
        //    {
        //        var v1 = listStatus.Split(',').ToList();
        //        value = v1.Select(int.Parse).ToList();
        //    }

        //    var result = mgr.GetData(franchiseid, id, value);
        //    //var result = mgr.Get(id, listStatus);List<int>
        //    if (result != null)
        //    {
        //        return Json(
        //            result
        //        );
        //    }


        //    return OkTP();
        //}



        [HttpGet]
        public IHttpActionResult GetFranchiseList(int id)
        {
            var result = mgr.GetAdminFranchisename(id);
            return Json(result);

        }
        [HttpGet]
        public IHttpActionResult Type_DateTime(int id, int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return Json(result);
            }

        }
    }
}
