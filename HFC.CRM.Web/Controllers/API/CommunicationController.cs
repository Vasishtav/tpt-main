﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Net.Mail;
using HFC.CRM.Data;

namespace HFC.CRM.Web.Controllers.API
{

    [LogExceptionsFilter]
    [CompressFilter]
    public class CommunicationController : BaseApiController
    {

        LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        //private string SendEmail(int leadId, int emailType)
        //{
        //    //To send the Lead confirmation after lead saved
        //    if (leadId <= 0)
        //    {
        //        return "Invalid Lead id";
        //    }

        //    List<string> toAddresses = new List<string>(), ccAddresses = null, bccAddresses = null;
        //    string toFranchiseEmail = "";

        //    //From email address
        //    var fromEmail = emailMgr.GetFranchiseEmail(SessionManager.CurrentFranchise.FranchiseId, emailType);// SessionManager.CurrentUser.Person.PrimaryEmail;
        //    if (string.IsNullOrEmpty(fromEmail))
        //    {
        //        fromEmail = SessionManager.CurrentUser.Email;
        //    }
        //    var fromAddresses = new MailAddress(fromEmail, SessionManager.CurrentUser.Person.FullName);

        //    //To get Franchise email and to lead email
        //    toFranchiseEmail = (string)SessionManager.CurrentFranchise.SupportEmail;
        //    var LeadCustomer = emailMgr.GetLeadEmail(leadId);


        //    if (toFranchiseEmail != null && toFranchiseEmail.Trim() != "")
        //    {
        //        toAddresses.Add(toFranchiseEmail);
        //    }

        //    if (LeadCustomer != null)
        //    {
        //        if (LeadCustomer.PrimaryEmail.Trim() != "")
        //        {
        //            toAddresses.Add(LeadCustomer.PrimaryEmail.Trim());
        //        }
        //        if (LeadCustomer.SecondaryEmail.Trim() != "")
        //        {
        //            toAddresses.Add(LeadCustomer.SecondaryEmail.Trim());
        //        }
        //    }

        //    //If no from or to emails return
        //    if (toAddresses == null || toAddresses.Count == 0 || fromEmail == "")
        //    {
        //        return "Success";
        //        //return "No valid from or to email";
        //    }


        //    //short TemplateEmailId = (short)EmailTemplateType.ThankYou;
        //    string TemplateSubject = "";
        //    int JobId = 0;
        //    int FranchiseId = 0;
        //    //Email Template: 1 Thanks Email; 2 Appointment Confirmation
        //    short TemplateEmailId = (short)emailType;
        //    int BrandId = (int)SessionManager.BrandId;

        //    //Take Body Html from EmailTemplate Table
        //    string bodyHtml = emailMgr.GetHtmlBody(TemplateEmailId, BrandId, 0);

        //    TemplateSubject = bodyHtml.Substring(0, bodyHtml.IndexOf(';'));
        //    bodyHtml = bodyHtml.Substring(bodyHtml.IndexOf(';') + 1);

        //    var mailMsg = emailMgr.BuildMailMessage(leadId, bodyHtml, TemplateSubject
        //        , TemplateEmailId, BrandId, FranchiseId
        //        , fromEmail, toAddresses, ccAddresses, bccAddresses);

        //    return "Success";

        //}

        [HttpGet, ActionName("GetLeadCommunication")]
        public IHttpActionResult GetLeadCommunication(int id,string filterOptions)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetCommunicationForLead(id, filterOptions);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetAccountCommunication")]
        public IHttpActionResult GetAccountCommunication(int id, string filterOptions)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetCommunicationForAccount(id, filterOptions);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetOpportunityCommunication")]
        public IHttpActionResult GetOpportunityCommunication(int id , string filterOptions)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetCommunicationForOpportunity(id, filterOptions);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        

        #region ----- Tochpoint/SendEmail -----
        [HttpGet]
        public IHttpActionResult ResendEmail(int id)
        {
            //var value = _franMgr.ResendEmail(id);
            var value = Communication.ResendEmailNew(id);
            if (value == "Success")
                return Json(true);
            else return Json(value);
        }
        #endregion ----- Tochpoint/SendEmail -----
        [HttpGet]
        public IHttpActionResult GetEmaildeatils(int id)
        {
            var value = Communication.GetHistory(id);
            return Json(value);
        }
        [HttpGet]
        public IHttpActionResult GetResendEmailData(int id)
        {
            var emailvalue = Communication.GetEmaildetails(id);
            return Json(emailvalue);
        }

        [HttpGet]
        public IHttpActionResult GetTextingDetails(int id)
        {
            var value = Communication.GetTextingDetails(id);
            return Json(value);
        }

        [HttpPost]
        public IHttpActionResult ResendTexting(int id)
        {
            try
            {
                var value = Communication.ResendTexting(id);
                if (value == "Success")
                    return Json(true);
                else return Json(value);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
            
        }

        [HttpGet]
        public IHttpActionResult GetTextingStatus(int id, string cellPhone)
        {
            try
            {
                var result = Communication.GetOptingInfo(cellPhone);

                if (result != null)
                    return Json(new { optOut = result.Optout, optIn = result.Optin, msgSent = result.IsOptinmessagesent });
                else
                    return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetCommunicationLogType(int id)
        {
            try
            {
                var result = Communication.GetCommunicationLogType();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult SaveNewCommunicationLog(int id,SentEmail Model)
        {
            try
            {
                //var result = Communication.SaveNewLog(Model);
                var result = Communication.SaveNewLogTexting(Model, ContantStrings.AddTypeTwo);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

    }
    
}
