﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Web.Filters;
using HFC.CRM.Data;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class TimezonesController : BaseApiController
    {
        [HttpGet]
        public IHttpActionResult Get(int Id)
        {
            string[] names = Enum.GetNames(typeof(TimeZoneEnum));
            var values = names.Select(x => new { id = (int)Enum.Parse(typeof(TimeZoneEnum), x), name = x }).Where(y => y.id != 0).ToList();
            return ResponseResult("Success", Json(values));
        }

        [HttpGet]
        public IHttpActionResult GetTimezones(int Id = 0)
        {
            if (SessionManager.CurrentFranchise != null)
            {
                var timezones = CacheManager.TimeZoneCollection.Where(t => t.TimeZone.Contains(SessionManager.CurrentFranchise.TimezoneCode.ToString())).ToList();
                return ResponseResult("Success", Json(timezones));
            }
            return ResponseResult("Success");
        }

        [HttpGet]
        public IHttpActionResult GetFranchiseDateTime(int Id = 0)
        {
            return ResponseResult("Success", Json(TimeZoneManager.GetLocal()));
        }
    }
}
