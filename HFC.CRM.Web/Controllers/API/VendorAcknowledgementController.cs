﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.VendorInvoice;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class VendorAcknowledgementController : BaseApiController
    {
       VendorAcknowledgementManager Vendor_Ack_Mgr = new VendorAcknowledgementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        /// <summary>
        /// return Vendor Acknowledgement Details with PurchaseOrderId and VendorReferenceNumber
        /// </summary>
        /// <param name="id"></param>
        /// <param name="VendorReferenceNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetDetails(int id, string vendorReferenceNumber)
        {
            try
            {
                var result = Vendor_Ack_Mgr.GetDetails(id, vendorReferenceNumber);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
    }
}