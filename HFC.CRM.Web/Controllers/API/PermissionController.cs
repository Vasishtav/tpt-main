﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class PermissionController : BaseApiController
    {
        readonly PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public IHttpActionResult GetPermissionSetsList(int id)
        {
            try
            {
                var response = permissionManager.GetPermissionSetList();                
                if (response != null)
                {
                    return Json(response);
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        //public IHttpActionResult GetPermissionSetsById()
        //{
        //     var permissionSets = permissionManager
        //    return Json("");
        //}

        public IHttpActionResult GetPermission(int id)
        {
            try
            {
                // This will get the Franchise permission set
                var response = permissionManager.GetPermissionSet(id, 2);
                if (response != null)
                {
                    return Json(response);
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        [HttpPost]
        public IHttpActionResult UpdatePermissionSet(int id, Permissionset_VM permissionSets)
        {
            try
            {
                var status = permissionManager.UpdatePermissionSet(permissionSets);
                return Json(status);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult UpdatePermissionSetRole(int id, Permissionset_VM permissionSets)
        {
            try
            {
                var status = permissionManager.UpdatePermissionSetRole(permissionSets);
                return Json(status);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        public IHttpActionResult GetUserRolesPermission()
        {
            try
            {
                var response = permissionManager.GetUserRolesPermissions();
                if (response != null)
                {
                    return Json(response);
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        [HttpPost]
        public IHttpActionResult CheckDuplicatePermissionSet(int id, string name)
        {
            try
            {
                var response = permissionManager.CheckDuplicatePermissionSets(id, name);
                return Json(response);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        public IHttpActionResult GetRolesList()
        {
            try
            {
                var response = permissionManager.GetRolesList();
                if (SessionManager.CurrentFranchise != null)
                    response = response.Where(x => x.RoleType == 2).ToList();
                if (response != null)
                {
                    return Json(response);
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        public IHttpActionResult GetPermissionByRolesId(System.Guid id)
        {
            try
            {
                var response = permissionManager.GetPermissionSetbyRole(id);
                if (response != null)
                {
                    return Json(response);
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        public IHttpActionResult GetAssignedUserList(int id)
        {
            try
            {
                var response = permissionManager.GetAssignedUser(id);
                if (response != null)
                {
                    return Json(response);
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        [HttpGet]
        public IHttpActionResult GetRoleName(System.Guid id)
        {
            try
            {
                var value = permissionManager.GetRoleName(id);
                return Json(value);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        [HttpGet]
        public IHttpActionResult DisableEnableDataSet(int id, string Type)
        {
            try
            {
                var value = permissionManager.DisablePermissionSets(id, Type);
                return Json(value);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
    }
}
