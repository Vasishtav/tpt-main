﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HFC.CRM.Data;

using HFC.CRM.Core.Logs;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class TaxController : BaseApiController
    {
        private FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);

        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                var result = FranMgr.GetTaxSettingsByFranchise(FranchiseId);
                var fedata = FranMgr.GetFEinfo(FranchiseId);
                var audit = FranMgr.GetTaxSettingsAudit();
                return Json(new { valid = true, TaxSettings = result, Franchise = fedata,audit= audit });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpPut]
        public IHttpActionResult UpdateSaveTaxSettings(int id, List<TaxSettings> taxSettings)
        {
            int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
            try
            {
                var sav = false;
                if(taxSettings!=null && taxSettings.Count > 0)
                {
                    foreach (var item in taxSettings)
                    {
                        if (item != null)
                        {
                            sav = FranMgr.AddorUpdateTaxSettings(item);
                        }
                    }
                }
                var result = FranMgr.GetTaxSettingsByFranchise(FranchiseId);
                var fedata = FranMgr.GetFEinfo(FranchiseId);
                var audit = FranMgr.GetTaxSettingsAudit();
                return Json(new { valid = sav, TaxSettings = result, Franchise = fedata, audit = audit });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                var result = FranMgr.GetTaxSettingsByFranchise(FranchiseId);
                return Json(new { valid = false, data = result });
            }
        }
        [HttpGet]
        public IHttpActionResult EnableOrDisableTax(int id, bool taxable)
        {
            try
            {
                var resp = FranMgr.EnableOrDisableTax(taxable);
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                var result = FranMgr.GetTaxSettingsByFranchise(FranchiseId);
                var fedata = FranMgr.GetFEinfo(FranchiseId);
                var audit = FranMgr.GetTaxSettingsAudit();
                return Json(new { valid = true, TaxSettings = result, Franchise = fedata, audit = audit });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        /// <summary>
        /// Deletes a franchise tax record
        /// </summary>
        /// <param name="id">TaxRuleId</param>
        /// <returns></returns>
        //public IHttpActionResult Delete(int id)
        //{
        //    var status = "Invalid tax rule id";
        //    if (id > 0)
        //    {
        //        status = FranMgr.DeleteTax(SessionManager.CurrentFranchise.FranchiseId, id);
        //    }

        //    return ResponseResult(status);
        //}

        //public IHttpActionResult Post([FromBody]FranchiseTaxRule rule)
        //{
        //    if (rule.IsBaseTax == true)
        //    {
        //        rule.Default = true;
        //    }
        //    var status = "Invalid tax rule data";
        //    int taxRuleId = 0;
        //    if (rule != null)
        //    {
        //        rule.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
        //        status = FranMgr.AddTax(rule, out taxRuleId);
        //    }

        //    return ResponseResult(status, Json(new { TaxRuleId = taxRuleId }));
        //}

        //public IHttpActionResult Put([FromBody]FranchiseTaxRule rule)
        //{
        //    var status = "Invalid tax rule data";
        //    if (rule != null)
        //    {
        //        rule.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
        //        status = FranMgr.UpdateTax(rule);
        //    }

        //    return ResponseResult(status);
        //}
    }
}