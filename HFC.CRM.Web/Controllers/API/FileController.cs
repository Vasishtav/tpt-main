﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Address;
using HFC.CRM.DTO.Lead;
using HFC.CRM.DTO.Notes;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Net.Mail;
using HFC.CRM.Web.Controllers;
using Newtonsoft.Json;
using HFC.CRM.Core.Membership;
using HFC.CRM.Managers.DTO;
using System.IO;
using System.Web.Helpers;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class FileController : BaseApiController
    {


        LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        CaseManager CaseVg_Mangr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);



        [HttpPost]//ValidateAjaxAntiForgeryToken
        public IHttpActionResult UploadFileTP(int id)
        {
            //, int Id1, string Branch, string Document
            try
            {
                int? leadId = null; int? accountId = null; int? opportunityId = null; int? orderId = null;
                string title = ""; int? category = null;
                bool IsFranchiseSettings = false; bool? IsEnabled = null;
                int Id1 = 0;
                bool? sysWideIsEnabled = null; string sysWideTitle = ""; int? sysWideCategory = null;
                int? sysWideConcept = null; string Document = "";
                string categories = null;

                var httpRequest = HttpContext.Current.Request;
                var files = httpRequest.Files;

                if (httpRequest.Form["leadId"] != null)
                    leadId = Convert.ToInt32(httpRequest.Form["leadId"]);
                if (httpRequest.Form["accountId"] != null)
                    accountId = Convert.ToInt32(httpRequest.Form["accountId"]);
                if (httpRequest.Form["opportunityId"] != null)
                    opportunityId = Convert.ToInt32(httpRequest.Form["opportunityId"]);
                if (httpRequest.Form["orderId"] != null)
                    orderId = Convert.ToInt32(httpRequest.Form["orderId"]);

                if (httpRequest.Form["Id1"] != null)
                    Id1 = Convert.ToInt32(httpRequest.Form["Id1"]);

                if (httpRequest.Form["title"] != null)
                    title = Convert.ToString(httpRequest.Form["title"]);
                if (httpRequest.Form["category"] != null)
                    category = Convert.ToInt32(httpRequest.Form["category"]);

                if (httpRequest.Form["categories"] != null)
                    categories = Convert.ToString(httpRequest.Form["categories"]);

                if (httpRequest.Form["IsFranchiseSettings"] != null)
                    IsFranchiseSettings = Convert.ToBoolean(httpRequest.Form["IsFranchiseSettings"]);
                if (httpRequest.Form["IsEnabled"] != null)
                    IsEnabled = Convert.ToBoolean(httpRequest.Form["IsEnabled"]);

                if (httpRequest.Form["sysWideIsEnabled"] != null)
                    sysWideIsEnabled = Convert.ToBoolean(httpRequest.Form["sysWideIsEnabled"]);
                if (httpRequest.Form["sysWideTitle"] != null)
                    sysWideTitle = Convert.ToString(httpRequest.Form["sysWideTitle"]);
                if (httpRequest.Form["sysWideCategory"] != null)
                    sysWideCategory = Convert.ToInt32(httpRequest.Form["sysWideCategory"]);
                if (httpRequest.Form["sysWideConcept"] != null)
                    sysWideConcept = Convert.ToInt32(httpRequest.Form["sysWideConcept"]);
                if (httpRequest.Form["Document"] != null)
                    Document = Convert.ToString(httpRequest.Form["Document"]);

                var LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var notemgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                if (files != null && files.Count > 0)
                {
                    byte[] fileData = null;
                    using (var binaryReader = new BinaryReader(files[0].InputStream))
                    {
                        fileData = binaryReader.ReadBytes(files[0].ContentLength);
                    }
                    // We are allways getting one file.
                    // TODO: we need to find why we are not getting multiple files.
                    var streamId = LeadMgr.UploadFileTP(fileData, files[0].FileName);

                    if (Document == "SYS-DOC")
                    {
                        string filename = files[0].FileName;
                        notemgr.UpdateGlobalDocumentStreamId(Id1, sysWideTitle, sysWideIsEnabled, sysWideCategory, sysWideConcept, streamId, filename);
                        return Json(true);
                    }



                    // TODO: Update the Note table with this.
                    var newnote = new Note
                    {
                        Title = title,
                        TypeEnum = 0, // (NoteTypeEnum)category,
                        Categories = categories,
                        IsNote = false,
                        AttachmentStreamId = new Guid(streamId),
                        CreatedOn = DateTime.Now,
                        CreatedBy = SessionManager.CurrentUser.PersonId,
                    };

                    newnote.LeadId = (leadId != null && leadId > 0) ? leadId : null;
                    newnote.AccountId = (accountId != null && accountId > 0) ? accountId : null;
                    newnote.OpportunityId = (opportunityId != null && opportunityId > 0) ? opportunityId : null;
                    newnote.OrderId = (orderId != null && orderId > 0) ? orderId : null;
                    newnote.IsEnabled = IsEnabled;

                    if (IsFranchiseSettings)
                    {
                        newnote.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    }
                    //TODO: if all the above are null, then it is an invalid entry. 

                    notemgr.Add(newnote);

                    return Json(new
                    {
                        Message = "Success",
                        Filename = files[0].FileName,
                        FileId = streamId
                    });
                }

                string status = "Invalid file";

                return Json(status);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return Json(ex.Message);
            }
            //string status = "Invalid file";
        }

        [HttpPost]
        public IHttpActionResult UploadAttachment(int id, int VendorCaseId, string Module,bool flag) // , List<franchiseCaseAttachmentDetails> description = null // string fileIconType=null, string fileSize=null, string fileName=null, string color=null
        {
            try
            {
              //  List<franchiseCaseAttachmentDetails> description = new List<franchiseCaseAttachmentDetails>();
                var httpRequest = HttpContext.Current.Request;
                var files = httpRequest.Files;
              // var ee = httpRequest.Form["description"];
                int CaseId = id;
                var description = JsonConvert.DeserializeObject<List<franchiseCaseAttachmentDetails>>(httpRequest.Form["description"]);
                List <franchiseCaseAttachmentDetails> fcad = new List<franchiseCaseAttachmentDetails>();

                if (files != null && files.Count > 0)
                {
                    for (int q = 0; q < files.Count; q++)
                    {
                        byte[] fileData = null;
                        using (var binaryReader = new BinaryReader(files[q].InputStream))
                        {
                            fileData = binaryReader.ReadBytes(files[q].ContentLength);
                        }
                        var streamId = LeadMgr.UploadFileTP(fileData, files[q].FileName);

                        List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
                        string extension = Path.GetExtension(files[q].FileName).ToUpper();
                        var streamId1 = "";
                        VendorCaseId = CaseId;
                        if (ImageExtensions.Contains(extension))
                        {
                            WebImage img = new WebImage(fileData);
                            img.Resize(50, 30);
                            byte[] thump = img.GetBytes();
                            string thumpfileName = DateTime.UtcNow.ToString("MMddyyyy") + "_thump_" + DateTime.UtcNow.ToString("HHMMssfff") + "_" + files[q].FileName;
                            streamId1 = LeadMgr.UploadFileTP(thump, thumpfileName);
                        }

                        var res = Case_Mangr.saveFranchiseCaseAttachmentDetails(CaseId, VendorCaseId, Module, description[q].fileIconType, description[q].fileSize, description[q].fileName, description[q].color, streamId, streamId1, flag);
                        fcad.Add(res);
                    }
                    return Json(fcad);
                }
                return Json("File Not found");
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return Json("Error Occurred");
            }

        }

        [HttpPost]
        public IHttpActionResult UploadAttachmentVendorGovernance(int id,  bool flag) // , List<franchiseCaseAttachmentDetails> description = null // string fileIconType=null, string fileSize=null, string fileName=null, string color=null
        {
            try
            {
                //  List<franchiseCaseAttachmentDetails> description = new List<franchiseCaseAttachmentDetails>();
                var httpRequest = HttpContext.Current.Request;
                var files = httpRequest.Files;
                // var ee = httpRequest.Form["description"];
                int CaseId = id;
                var description = JsonConvert.DeserializeObject<List<CaseAttachments>>(httpRequest.Form["description"]);
                List<CaseAttachments> fcad = new List<CaseAttachments>();

                if (files != null && files.Count > 0)
                {
                    for (int q = 0; q < files.Count; q++)
                    {
                        byte[] fileData = null;
                        using (var binaryReader = new BinaryReader(files[q].InputStream))
                        {
                            fileData = binaryReader.ReadBytes(files[q].ContentLength);
                        }
                        var streamId = LeadMgr.UploadFileTP(fileData, files[q].FileName);

                        List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
                        string extension = Path.GetExtension(files[q].FileName).ToUpper();
                        var streamId1 = "";                        
                        if (ImageExtensions.Contains(extension))
                        {
                            WebImage img = new WebImage(fileData);
                            img.Resize(50, 30);
                            byte[] thump = img.GetBytes();
                            string thumpfileName = DateTime.UtcNow.ToString("MMddyyyy") + "_thump_" + DateTime.UtcNow.ToString("HHMMssfff") + "_" + files[q].FileName;
                            streamId1 = LeadMgr.UploadFileTP(thump, thumpfileName);
                        }

                        var res = CaseVg_Mangr.saveCaseAttachmentDetails(CaseId, description[q].FileIconType, description[q].FileDetails, description[q].FileName,streamId, streamId1, flag);
                        fcad.Add(res);
                    }
                    return Json(fcad);
                }
                return Json("File Not found");
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return Json("Error Occurred");
            }

        }

        //[HttpPost]
        //public IHttpActionResult UploadAttachment(int id, int VendorCaseId, string Module, string fileIconType, string fileSize, string fileName, string color)
        //{
        //    try
        //    {

        //        var httpRequest = HttpContext.Current.Request;
        //        var files = httpRequest.Files;
        //        int CaseId = id;


        //        if (files != null && files.Count > 0)
        //        {
        //            byte[] fileData = null;
        //            using (var binaryReader = new BinaryReader(files[0].InputStream))
        //            {
        //                fileData = binaryReader.ReadBytes(files[0].ContentLength);
        //            }
        //            var streamId = LeadMgr.UploadFileTP(fileData, files[0].FileName);

        //            List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
        //            string extension = Path.GetExtension(files[0].FileName).ToUpper();
        //            var streamId1 = "";
        //            VendorCaseId = CaseId;
        //            if (ImageExtensions.Contains(extension))
        //            {
        //                WebImage img = new WebImage(fileData);
        //                img.Resize(50, 30);
        //                byte[] thump = img.GetBytes();
        //                string thumpfileName = DateTime.UtcNow.ToString("MMddyyyy") + "_thump_" + DateTime.UtcNow.ToString("HHMMssfff") + "_" + files[0].FileName;
        //                streamId1 = LeadMgr.UploadFileTP(thump, thumpfileName);
        //            }

        //            var res = Case_Mangr.saveFranchiseCaseAttachmentDetails(CaseId, VendorCaseId,Module, fileIconType, fileSize, fileName, color, streamId, streamId1);

        //            return Json(res);
        //        }
        //        return Json("File Not found");
        //    }
        //    catch (Exception ex)
        //    {
        //        var message = EventLogger.LogEvent(ex);
        //        return Json(ex.Message);
        //    }

        //}

        [HttpPost]
        public IHttpActionResult UpdateGlobalDocDetails(int id)//            int Id1, string sysWideTitle, int sysWideCategory, int sysWideConcept, bool sysWideIsEnabled)
        {
            try
            {
                var Request = HttpContext.Current.Request;
                int Id = Convert.ToInt32(Request.Form["Id1"]);
                string tiltle = Request.Form["sysWideTitle"];
                int Category = Convert.ToInt32(Request.Form["sysWideCategory"]);
                int Concept = Convert.ToInt32(Request.Form["sysWideConcept"]);
                bool IsEnabled = bool.Parse(Request.Form["sysWideIsEnabled"]);
                var notemgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                notemgr.UpdateGlobalDocumentStreamId(Id, tiltle, IsEnabled, Category, Concept, "", "");
                return Json("Success");
            }
            catch (Exception ex)
            {
                // Response.StatusCode = 400;
                //Response.StatusDescription = ex.Message;
                EventLogger.LogEvent(ex);
                return Json(ex.Message);
            }

        }
    }
}