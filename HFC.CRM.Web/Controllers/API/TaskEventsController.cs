﻿using HFC.CRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using HFC.CRM.Cache.Cache;
using HFC.CRM.DTO.Source;
using HFC.CRM.Managers;
using HFC.CRM.Core;
using HFC.CRM.Data.Geolocator;
using HFC.CRM.Data;
using HFC.CRM.DTO.Lookup;
using HFC.CRM.Web.Filters;
using System.Data.SqlClient;
using HFC.CRM.Data.Context;
using Dapper;
using StackExchange.Profiling.Helpers.Dapper;
using HFC.CRM.Web.Models;
using HFC.Common.Logging;
using System.Dynamic;
using HFC.CRM.Web.Models.Measurement;
using HFC.CRM.Core.Membership;
using Newtonsoft.Json;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Web.Controllers.API
{
    public class TaskEventsController : BaseApiController
    {
        // CalendarManager calmgr = new CalendarManager(SessionManager.CurrentUser);
        // GET: TaskEvents
        [HttpGet]
        public IHttpActionResult GetLeadsList(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Leads>(leadsquery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                   EventLogger.LogEvent(e);
                }

            }
            return Json("Leads");
        }
        public IHttpActionResult GetAccountList(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Accounts>(accountquery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("Accounts");
        }
        public IHttpActionResult GetOpportunityList(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Opportunities>(opportunityquery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("Opportunities");
        }
        public IHttpActionResult GetOrderList(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Orderss>(orderquery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("Orderss");
        }


        public class caseList
        {
            public int CaseId { get; set; }
            public int CaseNumber { get; set; }
        }

        public IHttpActionResult GetCaseList(int Id, string type)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var Query = @"select distinct FC.CaseId,FC.CaseNumber
	                                                from [CRM].[FranchiseCase] FC with (nolock)
	                                                Cross apply
	                                                (select * from CRM.CSVToTable (SalesOrderId)) FranchiseSalesOrder
	                                                Inner join CRM.Orders O with (nolock) on O.OrderId = FranchiseSalesOrder.id
	                                                inner join CRM.Quote q on q.QuoteKey = o.QuoteKey
                                                    inner join CRM.Opportunities opp on opp.OpportunityId = q.OpportunityId
	                                                where opp.FranchiseId = @FranchiseId ";

                    connection.Open();
                    if (type == "order")
                    {
                        var result = connection.Query(Query + "and O.OrderId = @OrderId",
                                new{ FranchiseId = SessionManager.CurrentFranchise.FranchiseId ,   OrderId = Id }).ToList();
                                
                                //select distinct fc.CaseId, fc.CaseNumber from [CRM].[FranchiseCase] fc 
                                //inner join CRM.Orders o on o.OrderId = (SELECT value FROM STRING_SPLIT(fc.SalesOrderId, ',') where value = @OrderId) 
                                //inner join CRM.Quote q on q.QuoteKey = o.QuoteKey
                                //inner join CRM.Opportunities opp on opp.OpportunityId = q.OpportunityId
                                //where opp.FranchiseId = @FranchiseId and o.OrderId = @OrderId"
                        connection.Close();
                        return Json(result);
                    }
                    else if (type == "opportunity")
                    {
                        var result = connection.Query(Query + "and opp.OpportunityId = @OpportunityId",
                                new  {  FranchiseId = SessionManager.CurrentFranchise.FranchiseId , OpportunityId = Id }).ToList();
                     //   select distinct fc.CaseId,fc.CaseNumber from[CRM].[FranchiseCase]  fc
                     //inner join CRM.Orders o on o.OrderId = (SELECT value FROM STRING_SPLIT(fc.SalesOrderId, ',') where value = o.OrderId) and o.OpportunityId=@OpportunityId

                     //inner join CRM.Opportunities opp on opp.OpportunityId = o.OpportunityId
                     //where opp.FranchiseId = @FranchiseId and opp.OpportunityId = @OpportunityId"

             connection.Close();
                        return Json(result);
                    }
                    else if (type == "account")
                    {
                        var result = connection.Query(Query + "and opp.AccountId = @AccountId ", 
                                                    new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, AccountId = Id }).ToList();

                        //select fc.CaseId, fc.CaseNumber from [CRM].[FranchiseCase] fc 
                        //                                join[CRM].[MasterPurchaseOrder] mpo on mpo.PurchaseOrderId = fc.MPO_MasterPONum_POId 
                        //                                left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId  
                        //                                join[CRM].[Opportunities] op on op.OpportunityId = mpox.OpportunityId  
                        //                                where op.FranchiseId = @FranchiseId and op.AccountId = @AccountId
                        connection.Close();
                        return Json(result);
                    }
                    else
                    {
                        var result = connection.Query(Query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                        //                select fc.CaseId, fc.CaseNumber from[CRM].[FranchiseCase] fc
                        //                   join[CRM].[MasterPurchaseOrder] mpo on mpo.PurchaseOrderId = fc.MPO_MasterPONum_POId
                        //                   left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        //                   join[CRM].[Opportunities] op on op.OpportunityId = mpox.OpportunityId
                        //                   where op.FranchiseId = @FranchiseId

                        connection.Close();
                        return Json(result);
                    }


                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("Cases");
        }

        public IHttpActionResult GetVendorCaseList(int Id, string type)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var Query = @"select distinct vc.VendorCaseId as CaseId,vc.VendorCaseNumber as CaseNumber
	                                        from [CRM].[FranchiseCase] FC with (nolock)
	                                        Cross apply
	                                        (select * from CRM.CSVToTable (SalesOrderId)) FranchiseSalesOrder
	                                        Inner join CRM.VendorCase vc on vc.CaseId = FC.CaseId
	                                        Inner join CRM.Orders O with (nolock) on O.OrderId = FranchiseSalesOrder.id
	                                        inner join CRM.Quote q on q.QuoteKey = o.QuoteKey
                                            inner join CRM.Opportunities opp on opp.OpportunityId = q.OpportunityId
	                                        where opp.FranchiseId = @FranchiseId ";
                    connection.Open();
                    if (type == "order")
                    {
                        var result = connection.Query(Query + "and O.OrderId = @OrderId ",
                                                        new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, OrderId = Id }).ToList();

                        //join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = fc.MPO_MasterPONum_POId
                        //left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        //join CRM.Opportunities op on op.OpportunityId = mpox.OpportunityId
                        //where op.FranchiseId = @FranchiseId and mpox.OrderId = @OrderId"
                        connection.Close();
                        return Json(result);
                    }
                    else if (type == "opportunity")
                    {
                        var result = connection.Query(Query + "and opp.OpportunityId = @OpportunityId"
                                                        , new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, OpportunityId = Id }).ToList();
                        //join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = fc.MPO_MasterPONum_POId
                        //left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        //join CRM.Opportunities op on op.OpportunityId = mpox.OpportunityId
                        //where op.FranchiseId = @FranchiseId and op.OpportunityId = @OpportunityId
                        connection.Close();
                        return Json(result);
                    }
                    else if (type == "account")
                    {
                        var result = connection.Query(Query + "and opp.AccountId = @AccountId",
                                             new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, AccountId = Id }).ToList();



                        //join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = fc.MPO_MasterPONum_POId
                        //left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        //join CRM.Opportunities op on op.OpportunityId = mpox.OpportunityId
                        //where op.FranchiseId = @FranchiseId and op.AccountId = @AccountId
                        connection.Close();
                        return Json(result);
                    }
                    else if (type == "case")
                    {
                        var result = connection.Query(Query + "and fc.caseId= @CaseId ",
                                                        new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, CaseId = Id }).ToList();



                        //join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = fc.MPO_MasterPONum_POId
                        //left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        //join CRM.Opportunities op on op.OpportunityId = mpox.OpportunityId
                        connection.Close();
                        return Json(result);
                    }
                    else
                    {
                        var result = connection.Query(Query , new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

                        // join[CRM].[MasterPurchaseOrder]mpo on mpo.PurchaseOrderId = fc.MPO_MasterPONum_POId
                        //left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        //join[CRM].[Opportunities] op on op.OpportunityId = mpox.OpportunityId
                        connection.Close();
                        return Json(result);
                    }

                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("Cases");
        }

        public IHttpActionResult GetOpportunityListByAccount(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Opportunities>(opportunityByAccountsquery, new { AccountId= Id,FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("OpportunitiesByAccount");
        }
        public IHttpActionResult GetOrderListByAccount(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Orderss>(orderByAccountsquery, new { AccountId = Id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("OrderByAccount");
        }
        public IHttpActionResult GetAccountListByOpportunity(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Accounts>(accountByOpportunity, new { OpportunityId = Id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("AccountListByOpportunity");
        }
        public IHttpActionResult GetOrderListByOpportunity(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Orderss>(orderByOpportunity, new { OpportunityId = Id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("OrderByAccount");
        }

        public IHttpActionResult GetAccountListByOrder(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Accounts>(accountByOrder, new { OrderID = Id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("AccountListByOrder");
        }
        public IHttpActionResult GetOpportunityListByOrder(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var result = connection.Query<Opportunities>(opportunityByOrder, new { OrderID = Id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }

            }
            return Json("OpportunityListByOrder");
        }
        public string leadsquery = @"SELECT 
                                     lead.[LeadId] as LeadId,
                                     (select [CRM].[fnGetAccountNameByLeadId](lead.LeadId)) as FullName,
                                     case when lead.[LeadId] is not null 
                                    		 then 
                                    		(select 
                                    		case when PreferredTFN='C' then CellPhone 
                                    		else case when PreferredTFN='H' then HomePhone 
                                    		else case when PreferredTFN='W' then WorkPhone 
                                    		else case when CellPhone is not null then CellPhone 
                                    		else case when HomePhone is not null then HomePhone 
                                    		else case when WorkPhone is not null then WorkPhone 
                                    		else '' 
                                    		end end end end end end
                                    		from CRM.Customer cus
                                    		join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and IsPrimaryCustomer=1
                                    		where lcus.LeadId=lead.[LeadId])
                                    		end as [PhoneNumber],
                                            lead.IsNotifyemails
                                     FROM crm.Leads lead 
                                     INNER JOIN CRM.Type_LeadStatus tls ON tls.id = lead.leadstatusid
                                     INNER JOIN [CRM].[Addresses] a on a.[AddressId]=(select top 1 la.AddressId from crm.LeadAddresses la where la.LeadId = lead.LeadId)
                                     JOIN CRM.LeadCustomers lcus on lead.LeadId=lcus.LeadId and IsPrimaryCustomer=1
                                     INNER JOIN [CRM].[Customer] p on p.CustomerId=lcus.CustomerId
                                     WHERE lead.LeadStatusId<>31422 AND lead.LeadStatusId <> 13 AND lead.LeadStatusId <> 2 AND lead.FranchiseId= @FranchiseId AND ISNULL(lead.IsDeleted,0)=0
                                     order by lead.[CreatedOnUtc] desc, [LastUpdatedOnUtc] desc";

            public string accountquery = @"SELECT
             Account.[AccountId],
            (select [CRM].[fnGetAccountNameByAccountId](Account.AccountId)) as FullName,
			 case when Account.[AccountId] is not null 
		then 
		(select 
		case when PreferredTFN='C' then CellPhone
		else case when PreferredTFN='H' then HomePhone 
		else case when PreferredTFN='W' then WorkPhone
		else case when CellPhone is not null then CellPhone 
		else case when HomePhone is not null then HomePhone 
		else case when WorkPhone is not null then WorkPhone 
		else '' 
		end end end end end end
		from CRM.Customer cus
		join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
		where acus.AccountId=Account.[AccountId]) end as PhoneNumber,
        Account.IsNotifyemails
             FROM[crm].[Accounts] Account
             JOIN CRM.AccountCustomers acccus on Account.AccountId=acccus.AccountId and IsPrimaryCustomer=1
             INNER JOIN[CRM].[Customer] p on p.CustomerId=acccus.CustomerId
             WHERE Account.FranchiseId= @FranchiseId AND ISNULL(Account.IsDeleted,0)=0
              order by isnull(Account.LastUpdatedOnUtc, Account.CreatedOnUtc) desc";

            public string opportunityquery = @"select  op.OpportunityId,
            op.OpportunityName as FullName
            from [CRM].[Opportunities]  op
            WHERE op.FranchiseId= @FranchiseId
            order by op.OpportunityId desc";

            public string orderquery = @"select OrderId, OrderName as FullName
            from [CRM].[Orders] o
            join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
            left join [CRM].[Person] p on op.SalesAgentId=p.PersonId
            left join [CRM].[Accounts] ac on op.AccountId=ac.AccountId
            left join [CRM].[Customer] cu on ac.PersonId=cu.CustomerId
            left join [CRM].[Type_OrderStatus] os on o.OrderStatus=os.OrderStatusId
            where 
            op.FranchiseId=@FranchiseId and o.OrderStatus not in (6,9)";

            public string opportunityByAccountsquery = @"select  op.OpportunityId,
            op.OpportunityName as FullName
            from [CRM].[Opportunities]  op
            WHERE op.FranchiseId= @FranchiseId and op.AccountId=@AccountId
            order by op.OpportunityId desc";

            public string orderByAccountsquery = @"select OrderID, OrderName as FullName from CRM.Orders o
            join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
            where FranchiseId=@FranchiseId and  AccountId=@AccountId";

            string accountByOpportunity= @"select a.AccountId,c.FirstName+' '+c.LastName as Fullname from CRM.Accounts a
            join CRM.Opportunities op on a.AccountId=op.AccountId
            join CRM.AccountCustomers ac on a.AccountId=ac.AccountId
            join CRM.Customer c on ac.CustomerId=c.CustomerId
            where op.OpportunityId=@OpportunityId and ac.IsPrimaryCustomer=1";

            string orderByOpportunity = @"select OrderId, OrderName as FullName from CRM.Orders WHERE OpportunityId=@OpportunityId";

            string accountByOrder = @"select a.AccountId,c.FirstName+' '+c.LastName as Fullname from CRM.Accounts a
            join CRM.Opportunities op on a.AccountId=op.AccountId
            join CRM.Orders o on op.OpportunityId=o.OpportunityId
            join CRM.AccountCustomers ac on a.AccountId=ac.AccountId
            join CRM.Customer c on ac.CustomerId=c.CustomerId
            where o.OrderID=@OrderID and ac.IsPrimaryCustomer=1";
            string opportunityByOrder = @"select op.OpportunityId,OpportunityName from CRM.Opportunities op
            join CRM.Orders o on op.OpportunityId=o.OpportunityId
            where o.OrderID=@OrderID";

            }


    public class Leads
    {
        public string FullName { get; set; }
        public int LeadId { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsNotifyemails { get; set; }
    }
    public class Accounts
    {
        public string FullName { get; set; }
        public int AccountId { get; set; }
        public string PhoneNumber { get; set; }   
        public bool IsNotifyemails { get; set; }
    }
    public class Opportunities
    {
        public string FullName { get; set; }
        public int OpportunityId { get; set; }
    }
    public class Orderss
    {
        public string FullName { get; set; }
        public int OrderId { get; set; }
    }


}
