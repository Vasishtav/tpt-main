﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System.Web.Http;
using HFC.CRM.Web.Filters;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class OpportunityStatusController : BaseApiController
    {
        readonly FranchiseManager _franMgr = new FranchiseManager(SessionManager.CurrentUser);

        public IHttpActionResult Post(Type_OpportunityStatus model)
        {
            var status = "Invalid data";
            int statusId = 0;
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = _franMgr.AddOpportunityStatus(model);

                statusId = model.OpportunityStatusId;
            }
            return ResponseResult(status, Json(new { Id = statusId }));
        }

        public IHttpActionResult Put(Type_OpportunityStatus model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = _franMgr.UpdateOpportunityStatus(model);
            }
            return ResponseResult(status);
        }

        public IHttpActionResult Delete(int id)
        {
            var status = _franMgr.DeleteOpportunityStatus(SessionManager.CurrentFranchise.FranchiseId, id);
            return ResponseResult(status);
        }
    }
}
