﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using HFC.CRM.Data.Context;
using HFC.CRM.Core.Common;
using Dapper;
using StackExchange.Profiling.Helpers.Dapper;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Web.Controllers.API
{
    public class VendorController : BaseApiController
    {
        //VendorManager VendorMngr = new VendorManager();
        private VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        [HttpPost]
        public IHttpActionResult Post([FromBody]FranchiseVendor model)
         {
             if (model.VendorName != null|| model.VendorName!= "")
                {

                if (model.VendorType == 0)
                {
                    if (model.VendorName =="My Vendor")
                    {
                        model.VendorType = 3;
                    }
                    else if (model.VendorName == "Alliance Vendor")
                    {
                        model.VendorType = 1;
                    }
                    else
                    {
                        model.VendorType = 2;
                    }
                }
                if (model.VendorType == 1 && model.IsAlliance==true)
                {
                    model.OrderingMethod = "";model.OrderingName = "";model.ContactDetails = "";
                }
                if(model.VendorType !=1 && model.VendorType != 2)
                {
                    if (model.Currency == 0 && (model.CurrencyValueCode != null || model.CurrencyValueCode != ""))
                    {
                        if (model.CurrencyValueCode == "USD")
                        {
                            model.Currency = 223;
                        }
                        else if (model.CurrencyValueCode == "CAD")
                        {
                            model.Currency = 39;
                        }
                    }
                }
                
                //if (model.CreditLimit != "")
                //{
                //    //var value = Convert.ToString(model.CreditLimit);

                //    model.CreditLimit = model.CreditLimit.Replace("$0", "");
                //    model.CreditLimit = model.CreditLimit.Replace(".00", "");
                //    model.CreditLimit = model.CreditLimit.Replace("$", "");
                //    model.CreditLimit = model.CreditLimit.Replace(",", "");
                //}
                
                var Status = VendorMngr.Save(model);
                    return Json(new { data =  Status });
                //return ResponseResult("Success");
            }

            return ResponseResult("Failed");
        }
        
        [HttpGet]
        public Nullable<int> spVendor_New(Nullable<int> Id)
        {

            using (var conn = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                conn.Open();
                var dto = conn.Query(
                    "[CRM].[spVendor_New]").ToList();

                var vendorNumber = dto[0].VendorNumber;
                //return 1;
                return vendorNumber;
            }
        }
        //displays all value in list kendo
        [HttpGet]
        public IHttpActionResult GetVendors(int id)
        {
            var result = VendorMngr.GetVendors();
            return Json(result);
        }
        //get vendors details to view
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var result = VendorMngr.GetLocalvendor(id);
            return Json(result);
            
        }

        //get vendors details to view
        [HttpGet]
        public IHttpActionResult Get(int id,int FranchiseID)
        {
            var result = VendorMngr.GetLocalvendor(id,FranchiseID);
            return Json(result);

        }

        //to get value based on option selected(drop-down)
        [HttpGet]
        public IHttpActionResult VendorOption(int id)
        {
            var result = VendorMngr.Getvalue(id);
            return Json(result);
        }
        //to fetch product details to vendor view page
        [HttpGet]
        public IHttpActionResult GetProductValue(int id)
        {
            var result = VendorMngr.GetPrdtValue(id);
            return Json(result);
        }
        //on clicking on checkbox value display(in-active)
        [HttpGet]
        public IHttpActionResult GetVendorsClick(int id,int Dropvalue)
        {
            var result = VendorMngr.GetVendorsDatas(id, Dropvalue);
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult GetAccountRelVendor(int id)
        {
            var result = VendorMngr.GetRelatedAccount(id);
            return Json(result);
        }
        
    }
}
