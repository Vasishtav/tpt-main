﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Managers.OAuth;
using HFC.CRM.Web.Filters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

/// <summary>
/// The API namespace.
/// </summary>
namespace HFC.CRM.Web.Controllers.API
{
    /// <summary>
    /// Class LeadImportController.
    /// </summary>
    [LogExceptionsFilter]
    public class LeadImportController : BaseApiController
    {
        /// <summary>
        /// Imports a lead
        /// </summary>
        /// <param name="Id">AuthConsumer Key to identify the caller</param>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [AllowAnonymous, HttpPost]
        public IHttpActionResult Post(string Id, [FromBody]Lead model)
        {
            if (model == null)
                return ResponseResult("Invalid lead data");

            try
            {
                var consumer = (OAuthConsumerDescription)MvcApplication.TokenManager.GetConsumer(Id);

                if (!consumer.IsIPAllowed(HttpContext.Current.Request.UserHostAddress))
                    return Forbidden("Unable to authenticate");
            }
            catch (Exception ex)
            {
                return Forbidden("Unable to locate your account");
            }

            var status = LeadsManager.ImportLead(model);
            Guid res;
            bool validGuid = Guid.TryParse(model.LeadGuid.ToString(), out res);

            if (validGuid && model.LeadGuid.ToString()!= "00000000-0000-0000-0000-000000000000")
            {
                LeadsManager.InsertLeadImportHistory(model, "Success");
            }
            else
            {
                LeadsManager.InsertLeadImportHistory(model, status);
            }
            return ResponseResult(status, Json(new { LeadGuid = model.LeadGuid }));
        }

        /// <summary>
        /// Only allow user to add additional lead info
        /// </summary>
        /// <param name="Id">AuthConsumer Key to identify the caller</param>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [AllowAnonymous, HttpPut]
        public IHttpActionResult Put(string Id, [FromBody]Lead model)
        {
            if (model == null || model.LeadAdditionalInfo == null || model.LeadAdditionalInfo.Count == 0)
                return ResponseResult("Invalid lead data");

            try
            {
                var consumer = (OAuthConsumerDescription)MvcApplication.TokenManager.GetConsumer(Id);

                if (!consumer.IsIPAllowed(HttpContext.Current.Request.UserHostAddress))
                    return Forbidden("Unable to authenticate");
            }
            catch
            {
                return Forbidden("Unable to locate your account");
            }

            var status = LeadsManager.AddAdditionalInfo(model.LeadGuid, model.LeadAdditionalInfo.ToList());
            return ResponseResult(status);
        }

        /// <summary>
        /// Returns a list of acceptable source Ids
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IHttpActionResult.</returns>
        [AllowAnonymous, HttpGet]
        public IHttpActionResult Sources(string Id)
        {
            try
            {
                var consumer = (OAuthConsumerDescription)MvcApplication.TokenManager.GetConsumer(Id);

                if (!consumer.IsIPAllowed(HttpContext.Current.Request.UserHostAddress))
                    return Forbidden("Unable to authenticate");
            }
            catch
            {
                return Forbidden("Unable to locate your account");
            }

            var sources = CacheManager.SourceCollection.Where(w => !w.FranchiseId.HasValue).Select(s => new Type_Source { Id = s.SourceId, Name = s.ToString(), Children = null, Jobs = null, Parent = null });

            return Json(sources);
        }
    }
}