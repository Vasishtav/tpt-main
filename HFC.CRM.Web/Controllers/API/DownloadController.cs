﻿using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class DownloadController : BaseApiController
    {
        [HttpGet]
        public async Task<HttpResponseMessage> Get(string streamid)
        {
            var data = await FileTable.GetFileDataAsync(streamid);
            if (data != null)
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(data.bytes);// new FileStream(data.physicalfilePath, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(data.fileName));
                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                string[] arrActualFileName = data.fileName.Split('_');
                string ActualFileName = ""; 
                for(int i=2;i<arrActualFileName.Length;i++)
                {
                    if(ActualFileName=="")
                        ActualFileName += arrActualFileName[i];
                    else
                        ActualFileName += "_" + arrActualFileName[i];
                }
                result.Content.Headers.ContentDisposition.FileName = ActualFileName;
                return result;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

        }
    }
}
