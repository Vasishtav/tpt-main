﻿/***************************************************************************\
Module Name:  QuestionsController.js - QuestionController AngularJS
Project: HFC
Created on: 15 Oct 2017
Created By:
Copyright:
Description: QuestionController
Change History:
Date  By  Description

\***************************************************************************/

using System;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System.Web.Http;
using System.Collections.Generic;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class QuestionsController : BaseApiController
    {
        /// <summary>
        /// Gets a single lead, will include a lot of lead detail
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        public IHttpActionResult Get(int id)
        {
            try
            {
                 //Added for Qualification/Question Answers
                int BrandId = (int)SessionManager.BrandId;
                List<Type_Question> qualifyQuestion = HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestionsById(BrandId, id);
                var questionList = qualifyQuestion;

                var LeadsDetails = HFC.CRM.Managers.TypeQuestionManager.GetLeadQuestions(id);

                return
                    Ok (
                        new
                        {
                            QuestionList = questionList
                            , LeadDetails = LeadsDetails
                        });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        public IHttpActionResult GetQuestionTypes()
        {
            //Added for Qualification/Question Answers - Gets the Type_Question
            int BrandId = (int)SessionManager.BrandId;
            return Json(HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestions(BrandId));
        }

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        public IHttpActionResult GetQuestionTypesAns(int leadId)
        {
            //Added for Qualification/Question Answers - Gets the Type_Question
            int BrandId = (int)SessionManager.BrandId;
            return Json(HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestionsById(BrandId, leadId));
        }

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpPost, ValidateAjaxAntiForgeryToken, ActionName("SaveQuestionAnswers")]
        public IHttpActionResult SaveAllQuestionAnswer(int id, [FromBody]List<Type_Question> listAnswer)
        {
            TypeQuestionManager QansMgr = new TypeQuestionManager(SessionManager.CurrentUser.PersonId, SessionManager.CurrentFranchise.FranchiseId);
            int answerId = 0;
            string status = "";
            if (listAnswer != null && id > 0)
            {
                status = QansMgr.SaveQuestionAnswers(id, listAnswer, out answerId);
            }
            //Return the reponse
            return ResponseResult("Success", Json(new { AnswerId = answerId }));
        }
    }
}
