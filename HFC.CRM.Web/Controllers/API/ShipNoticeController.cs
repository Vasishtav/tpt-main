﻿using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.Order;
using System.Net.Mail;
using HFC.CRM.Serializer;
namespace HFC.CRM.Web.Controllers.API
{
    public class ShipNoticeController : BaseApiController
    {

        ShipNoticeManager snMgr = new ShipNoticeManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        [HttpGet]
        public IHttpActionResult GetShipNotice(int id)
        {
            /*
             Shipped From = VendorName 
             Shipment Id = 
             Shipped Date = 
             Est Delivery Date = 
             Shipped Via =  ShippedViaTP
             Tracking Info = TrackingNumber / TrackingUrl
             Routing = 
             Vpo/Xvop = MasterPONumber
             Shipped To = ShipToAccount
             Shipped To Address = ShippedTo
             BOL = 
             Total Qty Shipped = TotalQuantity
             Total Boxes Shipped = TotalBoxesTP
             Total Weight = Weight
             Status = ??? to be decided based on child items...
             Total Qty Received = TotalQuantityReceived
             Total Qty Shipped = TotalQuantityShipped
             Total Qty Short Shipped = TotalQuantityShortShipped
             Total Qty Damaged = TtotalQuantityDamaged
             Total Qty Mis Shipped = TotalQuantityMisShipped
             Case Id = "Future development"
             Remake Ref = RemakeReference
             Checked-in by = UpdatedByUserName
             Checked-in date = CheckedinDate

            Details....
            Packing Slip =
            PO Line# = POItem
            Item = ProductNumber
            Item Description = 
            Qty Shippped = QuantityShipped
            UoM = UomDefinition
            Box ID = 
            Sales Order-Side Mark = SideMark 
            sosm like = OrderID
            SO Line # = POItem/QuoteLineNumber
            Qty Received = QuantityReceived
            Qty Damaged = QuantityDamaged
            Qty Short Shipped = QuantityShortShipped
            Qty Mis Shipped = QuantityMisShipped
            Status = 
            Notes = 


             */
            var shipNotice = snMgr.GetValue(id);
            return Json(shipNotice);

        }

        public IHttpActionResult GetDetailsList(int id)
        {

            var shipNotice = snMgr.GetValue(id);
            return Json(shipNotice.ListShipmentDetails);
        }


        public IHttpActionResult GetShipStatus()
        {
            var shipstatus = snMgr.GeTypeShipmentStatus();
            return Json(shipstatus);
        }

        public IHttpActionResult GetRelatedShipNoticeIds(int id)
        {
            var shipNoticeIds = snMgr.GetRelatedShipNoticeIds(id);
            return Json(shipNoticeIds);
        }

        [HttpPost]
        public IHttpActionResult UpdateShipNoticeHeader(int id, ShipNotice model)
        {
            try
            {
                snMgr.UpdateShipNotice(id, model);
                var shipNotice = snMgr.GetValue(id);
                return Json(shipNotice);
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }


        [HttpPost]
        public IHttpActionResult updateShipDetails(int id,int PurchaseOrderId,List<ShipmentDetail> model)
        {
            try
            {
                foreach (var sDetails in model)
                {
                    snMgr.UpdateShipNoticeDetail(id, sDetails);
                }

                snMgr.UpdateSOStatus(PurchaseOrderId);
                var shipNotice = snMgr.GetValue(id);
                return Json(shipNotice);
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }
        [HttpGet]
        public IHttpActionResult Get(int id, string listStatus,string dateFilter)
        {
            List<int> value = new List<int>();

            if (!string.IsNullOrEmpty(listStatus))
            {
                var v1 = listStatus.Split(',').ToArray().ToList();
                value = v1.Select(int.Parse).ToList();
            }

            var result = snMgr.Get(id, value, dateFilter);

            //As of now there is no permisson for the shipments so we are using Procurement Dashboard permission.
            //for filtering the data.
            var GetPermission = permissionManager.GetUserRolesPermissions();
            var ModuleData = GetPermission.Find(x => x.ModuleCode == "ProcurementDashboard");
            var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "ProcurementDashboardAllRecordAccess").CanAccess;

            //var result = mgr.Get(id, listStatus);List<int>
            if (result != null)
            {
                if (specialPermission == false)
                {
                    // Need to apply the filter for SalesAgent/Installation.
                    var filtered = result.Where(x => (x.SalesAgentId ==
                                        SessionManager.CurrentUser.PersonId ||
                                        x.InstallerId == SessionManager.CurrentUser.PersonId)
                                        ).ToList();

                    result = filtered;
                }
                return Json(result);
            }


            return OkTP();
        }
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult SyncShipNotice()
        {

            try
            {
                var result = snMgr.UpdateShipNotice();


                if (result)
                {
                    return Json(ContantStrings.Success);
                }
                else
                {
                    return Json(ContantStrings.FailedByError);
                }

            }
            catch (Exception ex)
            {
                return Json(ContantStrings.FailedByError);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult OneTimeUpdateShipNotice()
        {

            try
            {
                var result = snMgr.OneTimeUpdateShipNotice();


                if (result)
                {
                    return Json(ContantStrings.Success);
                }
                else
                {
                    return Json(ContantStrings.FailedByError);
                }

            }
            catch (Exception ex)
            {
                return Json(ContantStrings.FailedByError);
            }
        }

    }
}