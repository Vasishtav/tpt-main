﻿using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Web.Models;
using HFC.CRM.Managers;
using HFC.CRM.Core.Common;
using System.Web.Http;
using HFC.CRM.Data.Constants;
using HFC.CRM.Managers.AdvancedEmailManager;
using HFC.CRM.Data;
using Newtonsoft.Json;
using HFC.CRM.Core;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data.SavedCalendar;
using Dapper;
using System.Data.SqlClient;
using StackExchange.Profiling.Helpers.Dapper;
using HFC.CRM.Data.Context;

namespace HFC.CRM.Web.Controllers.API
{
    public class CalTasksController : BaseApiController //Controller
    {
        CalendarController calc = new CalendarController();
        TasksController taskcontroller = new TasksController();
        CalendarModel calmodel = new CalendarModel();
        TaskModel taskmodel = new TaskModel();
        CalendarManager calmgr = new CalendarManager(SessionManager.CurrentUser);
        OpportunitiesController oppcontroller = new OpportunitiesController();
        private SchedulerTaskService taskService;
        public CalTasksController()
        {
            taskService = new SchedulerTaskService();
        }
        //****************JsonP************************
        public IHttpActionResult Create()
        {
            var tasks = JsonConvert.DeserializeObject<IEnumerable<TaskViewModel>>("models");

            if (tasks != null)
            {
                foreach (var task in tasks)
                {
                    if (task.eventType == 1)
                    {
                        //Add Task
                        taskmodel = CreateCalendarTaskModel(task);
                        // IHttpActionResult AddTask = taskcontroller.Post(taskmodel);
                    }
                    else
                    {
                        //Add Event
                        calmodel = CreateCalendarEventModel(task);
                        IHttpActionResult AddEvent = calc.Post(calmodel);
                    }
                }
            }
            return Json(tasks);
        }
        public IHttpActionResult Update()
        {
            var tasks = JsonConvert.DeserializeObject<IEnumerable<TaskViewModel>>("models");

            if (tasks != null)
            {
                foreach (var task in tasks)
                {
                    if (task.eventType == 1)
                    {
                        //update Task
                        taskmodel = CreateCalendarTaskModel(task);
                        IHttpActionResult updateTask = taskcontroller.Put(task.TaskID, taskmodel);
                    }
                    else
                    {
                        //update Event
                        calmodel = CreateCalendarEventModelUpdate(task);
                        IHttpActionResult updateEvent = calc.Put(task.TaskID, calmodel);
                    }
                }
            }

            return Json(tasks);
        }
        public IHttpActionResult Delete(string id)
        {
            var tasks = JsonConvert.DeserializeObject<IEnumerable<TaskViewModel>>("models");

            if (tasks != null)
            {
                foreach (var task in tasks)
                {
                    if (task.eventType == 1)
                    {
                        //delete Task
                        taskmodel = CreateCalendarTaskModel(task);
                        IHttpActionResult deleteTask = taskcontroller.Delete(task.TaskID);
                    }
                    else
                    {
                        //delete Event
                        calmodel = CreateCalendarEventModelDelete(task);
                        IHttpActionResult deleteevent = calc.Delete(task.TaskID);
                    }

                }
            }

            return Json(tasks);
        }

        public CalendarModel CreateCalendarEventModel(TaskViewModel model)
        {
            if (model.ReminderId == null) { model.ReminderId = 0; }
            return calmodel = new CalendarModel
            {
                AptTypeEnum = HFC.CRM.Data.AppointmentTypeEnumTP.Appointment,
                AssignedName = null,
                AssignedPersonId = SessionManager.CurrentUser.PersonId,
                AttendeesTP = model.OwnerID.ToList(),
                EnableBorders = false,
                EventRecurring = null,
                EventType = HFC.CRM.Data.EventTypeEnum.Single,
                IsCancelled = null,
                IsDeletable = false,
                IsDeleted = false,
                IsPrivate = model.IsPrivate,
                LastUpdatedByPersonId = SessionManager.CurrentUser.PersonId,
                Location = model.Location,
                Message = model.Description,
                ReminderMethods = null,
                ReminderMinute = short.Parse(model.ReminderId.ToString()),
                allDay = model.IsAllDay,
                start = DateTime.Parse(model.Start.ToString()),
                end = DateTime.Parse(model.End.ToString()),
                id = model.TaskID,
                title = model.Title,
                CreatedOnUtc = DateTime.Now
            };
        }
        public CalendarModel CreateCalendarEventModelDelete(TaskViewModel model)
        {
            return calmodel = new CalendarModel
            {
                AptTypeEnum = HFC.CRM.Data.AppointmentTypeEnumTP.Appointment,
                AssignedName = null,
                AssignedPersonId = model.OrganizerPersonId,
                EnableBorders = false,
                EventRecurring = null,
                EventType = HFC.CRM.Data.EventTypeEnum.Single,
                IsCancelled = null,
                IsDeletable = false,
                IsDeleted = true,
                IsPrivate = model.IsPrivate,
                LastUpdatedByPersonId = SessionManager.CurrentUser.PersonId,
                Location = model.Location,
                Message = model.Description,
                ReminderMethods = null,
                ReminderMinute = short.Parse(model.ReminderId.ToString()),
                allDay = model.IsAllDay,
                //end = DateTime.Parse("27 / 01 / 2018 12:30:15 AM"),
                end = DateTime.Parse(model.End.ToString()),
                id = model.TaskID,
                //start = DateTime.Parse("26 / 01 / 2018 11:30:15 PM "),
                start = DateTime.Parse(model.Start.ToString()),
                startEditable = false,
                title = model.Title,
                CreatedOnUtc = DateTime.Now

            };
        }
        public CalendarModel CreateCalendarEventModelUpdate(TaskViewModel model)
        {
            return calmodel = new CalendarModel
            {
                AptTypeEnum = HFC.CRM.Data.AppointmentTypeEnumTP.Appointment,
                AssignedName = null,
                AssignedPersonId = model.OwnerID.SingleOrDefault().OwnerID,
                EnableBorders = false,
                EventRecurring = null,
                EventType = HFC.CRM.Data.EventTypeEnum.Single,
                IsCancelled = null,
                IsDeletable = false,
                IsDeleted = false,
                IsPrivate = model.IsPrivate,
                LastUpdatedByPersonId = SessionManager.CurrentUser.PersonId,
                Location = model.Location,
                Message = model.Description,
                ReminderMethods = null,
                ReminderMinute = short.Parse(model.ReminderId.ToString()),
                allDay = model.IsAllDay,
                end = DateTime.Parse(model.End.ToString()),
                id = model.TaskID,
                start = DateTime.Parse(model.Start.ToString()),
                startEditable = false,
                title = model.Title,
            };
        }
        public TaskModel CreateCalendarTaskModel(TaskViewModel model)
        {
            if (model.ReminderId == null) { model.ReminderId = 0; }
            return taskmodel = new TaskModel
            {

                id = model.TaskID,
                title = model.Title,
                AttendeesTP = model.OwnerID.ToList(),
                Message = model.Description,
                IsPrivate = model.IsPrivate,
                start = model.Start,
                end = model.End,
                ReminderMinute = short.Parse(model.ReminderId.ToString())
            };
        }


        public IHttpActionResult GetTypeAppointments()
        {
            var appointmentTypes = calmgr.GetTypeAppointments();
            return Json(appointmentTypes);
        }
        public IHttpActionResult GetInstallers()
        {
            var eventInstallers = calmgr.GetSalesAgents(0);
            JsonConvert.SerializeObject(eventInstallers, Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            });
            return Json(eventInstallers);

        }
        public IHttpActionResult GetAccounts(int Id = 0)
        {
            var eventAccounts = calmgr.GetAccounts(Id);
            JsonConvert.SerializeObject(eventAccounts, Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            });
            return Json(eventAccounts);

        }

        public IHttpActionResult GetReminders()
        {

            var reminders = new List<Reminders>
            {
                 new Reminders   { Name= "None", id= 0 },
                    new Reminders   { Name= "5 Minutes", id= 5},
                    new Reminders   { Name= "10 Minutes", id= 10},
                    new Reminders   { Name= "15 Minutes", id= 15 },
                    new Reminders   { Name= "30 Minutes", id= 30},
                    new Reminders   { Name= "45 Minutes", id= 45},
                    new Reminders   { Name= "1 hour", id= 60 },
                    new Reminders   { Name= "2 hours", id= 120},
                    new Reminders   { Name= "4 hours", id= 240},
                    new Reminders   { Name= "8 hours", id= 480},
                    new Reminders   { Name= "1 day", id= 1440},
                    new Reminders   { Name= "2 days", id= 2880},
                    new Reminders   { Name= "3 days", id= 4320},
                    new Reminders   { Name= "4 days", id= 5760},
                    new Reminders   { Name= "1 week", id= 10080},
                    new Reminders   { Name= "2 weeks", id= 20160}
            };
            return Json(reminders);
        }

        //*************IHttpActionResult***********************
        [HttpGet]
        public IHttpActionResult ReadSelection(string PersonIds, DateTime? start = null, DateTime? end = null)
        {
            if (PersonIds == null || PersonIds == "")
                return Json(new List<TaskModel>());

            CalendarFilter filter = new CalendarFilter();

            filter.start = start;
            filter.end = end;

            if (filter.personIds == null)
            {
                filter.personIds = new List<int>();
                filter.personIds = PersonIds.Split(',').Select(int.Parse).ToList();
            };
            List<CalendarModel> events = null, recurMasters = null;
            List<TaskModel> tasks = new List<TaskModel>();

            string status = "";
            if (!String.IsNullOrEmpty(PersonIds))
                status = filter.GetCalendar(out events, out recurMasters);

            if (!String.IsNullOrEmpty(PersonIds))
                filter.GetTasks(out tasks, PersonIds);

            if (tasks.Count() != 0)
            {
                tasks = tasks.OrderBy(o => o.CreatedOnUtc).ToList();
            }

            var appointmentDic = new Dictionary<string, byte>();
            foreach (var type in SessionManager.CurrentFranchise.AppointmentTypesCollection())
            {
                appointmentDic.Add(type.Name, Convert.ToByte(type.AppointmentTypeId));
            }

            try
            {
                var appColorMgr = new AppointmentTypeColorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var appColors = appColorMgr.GetColors();
                if (appColors != null)
                {
                    foreach (var evnt in events)
                    {
                        //evnt.EnableBorders = appColors.EnableBorders;
                        switch ((int)evnt.AptTypeEnum)
                        {
                            case 1:
                                evnt.AppointmentTypeColor = appColors.Sales_Design;
                                break;
                            case 5:
                                evnt.AppointmentTypeColor = appColors.Installation;
                                break;
                            case 6:
                                evnt.AppointmentTypeColor = appColors.Service;
                                break;
                            case 7:
                                evnt.AppointmentTypeColor = appColors.Followup;
                                break;
                            case 8:
                                evnt.AppointmentTypeColor = appColors.Personal;
                                break;
                            case 9:
                                evnt.AppointmentTypeColor = appColors.Vacation;
                                break;
                            case 10:
                                evnt.AppointmentTypeColor = appColors.Holiday;
                                break;
                            case 11:
                                evnt.AppointmentTypeColor = appColors.Meeting_Training;
                                break;
                            case 12:
                                evnt.AppointmentTypeColor = appColors.TimeBlock;
                                break;
                            default:
                                break;
                        }

                    }
                }

            }
            catch (Exception ex)
            {

            }


            if (calmgr == null) { calmgr = new CalendarManager(SessionManager.CurrentUser); }
            if (status == IMessageConstants.Success)
            {
                //  if (filter.IsMainCalendar == true)
                {
                    IQueryable<TaskViewModel> caltasks = null;
                    if (tasks != null && tasks.Count > 0)
                    {
                        CalendarManager calmgr1 = new CalendarManager(SessionManager.CurrentUser);
                        caltasks = tasks.ToList().Select(task => new TaskViewModel
                        {
                            TaskID = task.id,
                            PersonId = task.PersonId,
                            Title = task.title,
                            Start = Convert.ToDateTime(task.start),
                            End = Convert.ToDateTime(task.start),
                            StartTimezone = null,
                            EndTimezone = null,
                            Description = task.Message,
                            IsAllDay = task.allDay,
                            RecurrenceRule = null,
                            RecurrenceException = null,
                            RecurrenceID = null,
                            //OwnerID2 = calmgr.GetAttendees(task.id, true),
                            eventType = 1,
                            IsPrivate = task.IsPrivate,
                            ReminderId = task.ReminderMinute,
                            IsTask = true,
                            Location = "Edit Task",
                            LeadId = task.LeadId,
                            AccountId = task.AccountId == null ? 0 : (int)task.AccountId,
                            OpportunityId = task.OpportunityId == null ? 0 : (int)task.OpportunityId,
                            OrderId = task.OrderId == null ? 0 : (int)task.OrderId,
                            CompletedDateUtc = task.CompletedDateUtc,
                            OrganizerPersonId = task.OrganizerPersonId,
                            OwnerName = "",// calmgr.GetOwnerNameById(task.OrganizerPersonId == 0 ? Int32.Parse(task.OrganizerPersonId.ToString()) : 0),
                            UserRoleColor = calmgr.GetTaskBackGroundColor(task.PersonId),
                            UserTextColor = calmgr.GetUserTextColor(task.PersonId),
                            //AppointmentTypeColor = calmgr.GetTaskBackGroundColor(task.PersonId),
                            Attendee = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == task.PersonId).Email,
                            IsPrivateAccess = calmgr.GetAuthorisedPrivateCalendar(task.id, task.IsPrivate, true),
                        }).AsQueryable();
                    }

                    if (calmgr == null) { calmgr = new CalendarManager(SessionManager.CurrentUser); }
                    var calEvents = events.ToList().Select(evnt => new TaskViewModel
                    {
                        TaskID = evnt.id,
                        Title = evnt.title,
                        Start = evnt.start,
                        End = evnt.end,
                        Description = evnt.Message,
                        IsAllDay = evnt.allDay,
                        RecurrenceRule = evnt.RRRule,
                        RecurrenceException = evnt.RecurrenceException,
                        RecurrenceID = evnt.RecurringEventId,
                        AppointmentTypeName = evnt.AppointmentTypeName,
                        CalName = evnt.CalName,
                        Location = evnt.Location,
                        eventType = 2,
                        EventType = evnt.EventType,
                        ReminderId = evnt.ReminderMinute,
                        IsPrivate = evnt.IsPrivate,
                        IsTask = false,
                        LeadId = evnt.LeadId,
                        AccountId = evnt.AccountId == null ? 0 : (int)evnt.AccountId,
                        AccountName = evnt.AccountName,
                        AccountPhone = evnt.AccountPhone,
                        PhoneNumber = evnt.PhoneNumber,
                        OpportunityId = evnt.OpportunityId == null ? 0 : (int)evnt.OpportunityId,
                        OpportunityName = evnt.OpportunityName,
                        OrderId = evnt.OrderId == null ? 0 : (int)evnt.OrderId,
                        OrganizerPersonId = evnt.OrganizerPersonId,
                        opportunity = null,// calmgr.GetEventOpportunities(evnt.id),
                        OwnerName = evnt.OrganizerName,// calmgr.GetOwnerNameById(Int32.Parse(evnt.OrganizerPersonId.ToString())),
                        Attendee = evnt.PersonId == 0 ? null : CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == evnt.PersonId).Email,
                        AttendeeName = evnt.PersonId == 0 ? null : CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == evnt.PersonId).Person.FullName,
                        //OwnerID2 = calmgr.GetAttendees(evnt.id, false),
                        AppointmentTypeColor = evnt.AppointmentTypeColor,// calmgr.GetTaskBackGroundColor(evnt.PersonId),
                        UserRoleColor = calmgr.GetTaskBackGroundColor(evnt.PersonId),
                        UserTextColor = calmgr.GetUserTextColor(evnt.PersonId),
                        IsPrivateAccess = calmgr.GetAuthorisedPrivateCalendar(evnt.id, evnt.IsPrivate, false),
                        TimeSlot = (int)TimeSlot(evnt.start.ToString(), evnt.end.ToString()),
                    }).AsQueryable();

                    if (caltasks != null)
                    {
                        var mergecalevent = caltasks.Union(calEvents);
                        //mergecalevent = mergecalevent.FirstOrDefault();
                        //mergecalevent = mergecalevent.AsQueryable();
                        return Json(mergecalevent);
                    }
                    else
                    {
                        return Json(calEvents);
                    }

                }

            }
            return Json(tasks);
        }

        double TimeSlot(string start, string end)
        {
            try
            {
                var startspan = TimeSpan.Parse(DateTime.Parse(start).TimeOfDay.ToString());
                var endspan = TimeSpan.Parse(DateTime.Parse(end).TimeOfDay.ToString());

                TimeSpan result = endspan - startspan;
                var diffInMinutes = result.TotalMinutes;
                return diffInMinutes;
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        protected DateTime ConvertToFranchiseDateTime(DateTime date)
        {
            string FrianchiseTimeZone = SessionManager.CurrentFranchise.TimezoneCode.ToString();
            decimal franchiseOffSetTime = CacheManager.TimeZoneCollection.Where(t => t.TimeZone.Contains(FrianchiseTimeZone)).SingleOrDefault().OffSetTime;
            DateTime val = date.AddSeconds(double.Parse(franchiseOffSetTime.ToString()));
            return val;

        }
        public void SetAppointmentColor()
        {

        }
        public IHttpActionResult ReadSalesUsers()
        {
            try
            {
                var usersf = SessionManager.CurrentFranchise.UserCollection();

                var users = SessionManager.CurrentFranchise.UserCollection().Select(s => new
                {
                    s.UserId,
                    s.Person.FullName,
                    s.PersonId,
                    s.ColorId,
                    s.Email,
                    AvatarSrc = s.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc,
                    s.EmailSignature,
                    InSales = s.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId),
                    InInstall = s.IsInRole(AppConfigManager.DefaultInstallRole.RoleId),
                    IsDisabled = s.IsDisabled
                }).ToList();

                if (users != null && users.Count > 0)
                {
                    var userlist = new
                    {
                        Users = users.Where(u => u.IsDisabled != true && u.InSales == true).ToList(),
                        CurrentUser = new { PersonId = SessionManager.CurrentUser.PersonId, user = SessionManager.CurrentUser },
                        DisabledUsers = users.Where(u => u.IsDisabled == true).ToList(),
                        FranchiseIsSolaTechEnabled = SessionManager.CurrentFranchise.isSolaTechEnabled,
                        FranchiseCountryCode = SessionManager.CurrentFranchise.CountryCode,
                        BrandId = SessionManager.BrandId,
                        FranchiseName = SessionManager.CurrentFranchise.Name
                    };
                    return Json(userlist.Users);
                }
                else
                {
                    return Json("Response");
                }
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }
        public IHttpActionResult ReadInstallUsers()
        {
            try
            {
                var usersf = SessionManager.CurrentFranchise.UserCollection();

                var users = SessionManager.CurrentFranchise.UserCollection().Select(s => new
                {
                    s.UserId,
                    s.Person.FullName,
                    s.PersonId,
                    s.ColorId,
                    s.Email,
                    AvatarSrc = s.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc,
                    s.EmailSignature,
                    InSales = s.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId),
                    InInstall = s.IsInRole(AppConfigManager.DefaultInstallRole.RoleId),
                    IsDisabled = s.IsDisabled
                }).ToList();

                if (users != null && users.Count > 0)
                {
                    var userlist = new
                    {
                        Users = users.Where(u => u.IsDisabled != true && u.InInstall == true).ToList(),
                        CurrentUser = new { PersonId = SessionManager.CurrentUser.PersonId, user = SessionManager.CurrentUser },
                        DisabledUsers = users.Where(u => u.IsDisabled == true).ToList(),
                        FranchiseIsSolaTechEnabled = SessionManager.CurrentFranchise.isSolaTechEnabled,
                        FranchiseCountryCode = SessionManager.CurrentFranchise.CountryCode,
                        BrandId = SessionManager.BrandId,
                        FranchiseName = SessionManager.CurrentFranchise.Name
                    };
                    return Json(userlist.Users);
                }
                else
                {
                    return Json("Response");
                }
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }
        public IHttpActionResult ReadOtherUsers()
        {
            try
            {
                var usersf = SessionManager.CurrentFranchise.UserCollection();

                var users = SessionManager.CurrentFranchise.UserCollection().Select(s => new
                {
                    s.UserId,
                    s.Person.FullName,
                    s.PersonId,
                    s.ColorId,
                    s.Email,
                    AvatarSrc = s.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc,
                    s.EmailSignature,
                    InSales = s.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId),
                    InInstall = s.IsInRole(AppConfigManager.DefaultInstallRole.RoleId),
                    IsDisabled = s.IsDisabled
                }).ToList();

                if (users != null && users.Count > 0)
                {
                    var userlist = new
                    {
                        Users = users.Where(u => u.IsDisabled != true && (u.InSales == false && u.InInstall == false)).ToList(),
                        CurrentUser = new { PersonId = SessionManager.CurrentUser.PersonId, user = SessionManager.CurrentUser },
                        DisabledUsers = users.Where(u => u.IsDisabled == true).ToList(),
                        FranchiseIsSolaTechEnabled = SessionManager.CurrentFranchise.isSolaTechEnabled,
                        FranchiseCountryCode = SessionManager.CurrentFranchise.CountryCode,
                        BrandId = SessionManager.BrandId,
                        FranchiseName = SessionManager.CurrentFranchise.Name
                    };
                    return Json(userlist.Users);
                }
                else
                {
                    return Json("Response");
                }
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }
        [System.Web.Http.HttpPost]
        public IHttpActionResult SendReminder(int Id)
        {
            return Json("Success");
        }
        [System.Web.Http.HttpPost]
        public IHttpActionResult SaveCalendar(SaveCalendar savecalendar)
        {
            var status = calmgr.SaveCalendar(savecalendar);
            return Json(status);
        }
        [System.Web.Http.HttpPost]
        public IHttpActionResult ManageSaveCalendar(SaveCalendar savecalendar)
        {
            var status = calmgr.SaveCalendar(savecalendar);
            return Json(status);
        }
        public IHttpActionResult GetManagedCalendarListbyUserId(int id)
        {
            if (id == 0)
            {
                id = SessionManager.CurrentUser.PersonId;
            }
            var data = calmgr.GetManagedCalendarListbyUserId(id);
            //var list = JsonConvert.SerializeObject(data, Formatting.Indented,
            //             new JsonSerializerSettings
            //             {
            //                 ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            //             });
            return Json(data);

        }
        public IHttpActionResult DeleteSavedCalendar(int id)
        {
            string status = "";
            if (calmgr == null) { calmgr = new CalendarManager(SessionManager.CurrentUser); }
            var data = calmgr.DeleteSavedCalendar(id);
            if (data == 1) { status = "Success"; } else { status = "Error"; }
            return Json(status);
        }

        [HttpPut]
        public IHttpActionResult UpdateSavedCalendar(int id, List<SavedCalendarOrder> saveorder)
        {
            if (calmgr == null) { calmgr = new CalendarManager(SessionManager.CurrentUser); }
            var result = calmgr.UpdateSavedCalendar(saveorder);
            return Json(result);
        }
        [HttpGet]
        public IHttpActionResult ReadSavedcalendar(int id)
        {
            var PersonId = SessionManager.CurrentUser.PersonId;
            var FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
            if (calmgr == null) { calmgr = new CalendarManager(SessionManager.CurrentUser); }
            var data = calmgr.GetSavedCalendar(FranchiseId);
            //foreach (var item in data)
            //{
            //    if (item.SaveCalendarId == id)
            //    {
            //        item.active = "active";
            //    }
            //    else
            //    {
            //        item.active = "";
            //        if (id == 0)
            //        {
            //            if (item.IsDefault == true)
            //            {
            //                item.active = "active";
            //            }
            //        }
            //    }
            //}
            return Json(data);
        }

        public bool usercolor(int PersonId, int SaveCalendarid)
        {
            var selectedcolor = "select* from crm.SavedCalendarUsers where SaveCalendarid = @SaveCalendarid and SelectedUserId=@PersonId";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var checkexistViews = connection.Query<SavedCalendarUsers>(selectedcolor, new { PersonId = PersonId, SaveCalendarid = SaveCalendarid }).ToList();
                connection.Close();
                if (checkexistViews != null)
                {
                    if (checkexistViews.Count > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public IHttpActionResult GetSavedCalendarUsers(int SaveCalendarid = 0)
        {

            try
            {
                var users = SessionManager.CurrentFranchise.UserCollection().Select(s => new
                {
                    s.UserId,
                    s.Person.FullName,
                    s.PersonId,
                    s.ColorId,
                    s.Email,
                    AvatarSrc = s.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc,
                    s.EmailSignature,
                    InSales = s.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId),
                    InInstall = s.IsInRole(AppConfigManager.DefaultInstallRole.RoleId),
                    IsDisabled = s.IsDisabled,
                    IsEnable = false,
                    SelectColor = usercolor(s.PersonId, SaveCalendarid) ? "list-group-item active" : "list-group-item ng-binding ng-hide",
                    color = string.Format("background-color:{1}{3};border-color:{1}{3};color:{2}{3}", s.ColorId, CacheManager.ColorPalettes.FirstOrDefault(f => f.ColorId == s.ColorId).BGColorToRGB(), CacheManager.ColorPalettes.FirstOrDefault(f => f.ColorId == s.ColorId).FGColorToRGB(), true ? " !important" : ""),
                }).ToList();
                return Json(users);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());
            }
        }
    }
    public class Reminders
    {
        public int id { get; set; }
        public string Name { get; set; }
    }
}
