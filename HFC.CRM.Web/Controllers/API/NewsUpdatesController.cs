﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class NewsUpdatesController : BaseApiController
    {
        //readonly LeadsManager leadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        //readonly NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        readonly NewsUpdatesManager numanager = new NewsUpdatesManager(SessionManager.CurrentUser);
        
        [HttpPost]
        public IHttpActionResult AddNews(NewsUpdates model)
        {
            try
            {
                var newsId = 0;
                var status = "Invalid data. Save Failed. Please try again";
                if (model != null)
                {
                    // model.CreatedOn = DateTime.Now;
                    //model.CreatedBy = SessionManager.CurrentUser.PersonId; ???why it is not working
                    model.CreatedBy = SessionManager.SecurityUser.PersonId;
                    status = numanager.CreateNews(out newsId,  model);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = newsId }));

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        
        [HttpPost]
        public IHttpActionResult UpdateNews(NewsUpdates model)
        {

            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                if (model != null)
                {
                    
                    model.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                    status = numanager.Update(model);
                }
                return ResponseResult(status,  Json(new { NewsUpdatesId = model.NewsUpdatesId }));

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        [HttpPost, ActionName("ActivateNews")]
        public IHttpActionResult ActivateNews(int id)
        {

            try
            {
                var status = "Invalid data. Activation Failed. Please try again";
                if (id > 0)
                {
                    status = numanager.Activate(id);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = id }));

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost, ActionName("MoveUp")]
        public IHttpActionResult MoveUp(int id)
        {

            try
            {
                var status = "Invalid data. Move up or down Failed. Please try again";
                if (id > 0)
                {
                    status = numanager.MoveUp(id);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = id }));

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost, ActionName("MoveDown")]
        public IHttpActionResult MoveDown(int id)
        {

            try
            {
                var status = "Invalid data. Move up or down Failed. Please try again";
                if (id > 0)
                {
                    status = numanager.MoveDown(id);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = id }));

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost, ActionName("DeactivateNews")]
        public IHttpActionResult DeactivateNews(int id)
        {

            try
            {
                var status = "Invalid data. Deactivation Failed. Please try again";
                if (id > 0)
                {
                    status = numanager.Deactivate(id);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = id }));

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        [HttpGet, ActionName("GetNews")]
        public IHttpActionResult GetNews(int id)
        {
            try
            {
                NewsUpdates news = null;
                IList<NewsUpdates> allNews = new List<NewsUpdates>();
                if (id > 0)
                {
                    news = numanager.Get(id);
                    allNews.Add(news);
                }
                else
                {
                    allNews = numanager.GetAllNews();
                }
                
               // notes = notes.OrderByDescending(p => p.CreatedOn).ToList();

                return Json(allNews);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetCurrentNews")]
        public IHttpActionResult GetCurrentNews(int id)
        {
            try
            {
               
                var news = numanager.GetCurrentNews(SessionManager.CurrentFranchise.BrandId);
                // notes = notes.OrderByDescending(p => p.CreatedOn).ToList();

                return Json(news);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
