﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class ShippingController : BaseApiController
    {
        ShippingManager ShippingMgr = new ShippingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        // GET api/<controller>
        public IHttpActionResult Get(int id)
        {
           var ships = ShippingMgr.Get();
            return Json(ships);
        }

        // GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}
        public IHttpActionResult GetValue(int id,int VendorId,string Status)
        {
            var ships = ShippingMgr.GetVendor(id, VendorId,Status);
            return Json(ships);
        }

        // GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}
        public IHttpActionResult GetValue(int id, int VendorId,int FranchiseID, string Status)
        {
            var ships = ShippingMgr.GetVendor(id, VendorId,FranchiseID, Status);
            return Json(ships);
        }

        // POST api/<controller>
        [HttpPost]
        public IHttpActionResult SaveShipToLocation([FromBody]ShipToLocations data)
        {
            var updateStatus = "";
            if(data.Id>0)
            {
                updateStatus = ShippingMgr.Update(data);
            }
            else
                updateStatus =  ShippingMgr.Add(data);
            return ResponseResult(updateStatus);
        }
        [HttpPost]
        public IHttpActionResult ActivateDeactivateShip(ShipToLocations data)
        {
            var updateStatus = ShippingMgr.ActivateDeactivateShipLocation(data);
            return ResponseResult(updateStatus);
        }
        [HttpGet]
        public IHttpActionResult DuplicateShippingName (int id,string value)
        {
            var status = ShippingMgr.DuplicateShippingName(id,value);
            return Ok(new {status=status});
        }
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
        public IHttpActionResult GetBPValue(int id, int VendorId)
        {
            var ships = ShippingMgr.GetBP(id, VendorId);
            return Json(ships);
        }
    }
}