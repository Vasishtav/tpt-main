﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Address;
using HFC.CRM.DTO.Lead;
using HFC.CRM.DTO.Notes;
using HFC.CRM.DTO.Search;
using HFC.CRM.DTO;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

using HFC.CRM.Data;

using HFC.CRM.DTO.Opportunity;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class OpportunitiesController : BaseApiController
    {
        private OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        private LeadsManager LeadsMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        private PurchaseOrderManager PurchaseOrdersManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private AccountsManager actMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        private readonly PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);


        /// <summary>
        /// Gets a single Opportunity, will include a lot of opportunity detail
        /// </summary>
        /// <param name="id">opportunity Id</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult getAccountName(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                //var result = connection.Query("select op.CustomerId as accountId,FirstName + ' ' + LastName  as Name  from [crm].[OpportunityCustomers] op Inner join [CRM].[Customer] customer on  customer.CustomerId=op.CustomerId where op.OpportunityId=@id", new { id = id }).FirstOrDefault();
                //var result = connection.Query("select op.CustomerId as accountId,FirstName + ' ' + LastName + case when CompanyName is not null and CompanyName!='' then ' / '+CompanyName else '' end  as Name  from[crm].[OpportunityCustomers] op Inner join[CRM].[Customer] customer on customer.CustomerId = op.CustomerId where op.OpportunityId = @id", new { id = id }).FirstOrDefault();
                var result = connection.Query(@"select op.AccountId as accountId,
                                                (select [CRM].[fnGetAccountNameByAccountId](acc.AccountId)) as Name from[crm].AccountCustomers op
                                                Inner join[CRM].Customer customer on customer.CustomerId = op.CustomerId
                                                Inner join CRM.Accounts acc on op.AccountId=acc.AccountId
                                                where op.AccountId = @id and op.IsPrimaryCustomer = 1",
                                                new { id = id }).FirstOrDefault();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult checkOppSplPermission (int id, string userid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var datalist = connection.Query<dynamic>(@"select * from Auth.Permission_Role where RoleId in 
(select RoleId from Auth.UsersInRoles where UserId=@id) and PermissionTypeId=16 and IsAccessible=1", new { id = userid }).ToList();

                if(datalist.Count > 0)
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }
            }

        }
        [HttpGet]
        public IHttpActionResult GetSalesAgentInstaller(int id, string reff)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var datalist = connection.Query<dynamic>(@"select * from CRM.Opportunities
where OpportunityId	=@id", new { id = id }).FirstOrDefault();

                if (datalist != null)
                {
                    return Json(new { datalist.SalesAgentId, datalist.InstallerId });
                }
                else
                {
                    return Json(false);
                }
            }

        }

        [HttpGet]
        public IHttpActionResult getPhoneEmailQuote(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var result = connection.Query(@" select (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as CellPhone,c.PrimaryEmail
                             from  CRM.Opportunities op 
                             join CRM.Accounts ac on op.AccountId=ac.AccountId
                             join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId and IsPrimaryCustomer=1
                             join CRM.Customer c on acc.CustomerId=c.CustomerId
							 where op.OpportunityId=@id",
                                         new { id = id }).FirstOrDefault();

                    connection.Close();

                    return Json(result);

                }
            }catch(Exception e)
            {
                return null;
            }
         

            }

        [HttpGet]
        public IHttpActionResult getPhoneEmail(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                List<EmailPhone> data = new List<EmailPhone>();

                //var result = connection.Query("select op.CustomerId as accountId,FirstName + ' ' + LastName  as Name  from [crm].[OpportunityCustomers] op Inner join [CRM].[Customer] customer on  customer.CustomerId=op.CustomerId where op.OpportunityId=@id", new { id = id }).FirstOrDefault();
                //var result = connection.Query("select op.CustomerId as accountId,FirstName + ' ' + LastName + case when CompanyName is not null and CompanyName!='' then ' / '+CompanyName else '' end  as Name  from[crm].[OpportunityCustomers] op Inner join[CRM].[Customer] customer on customer.CustomerId = op.CustomerId where op.OpportunityId = @id", new { id = id }).FirstOrDefault();
                var result = connection.Query<EmailPhone>(@"select customer.PrimaryEmail,
								case when op.AccountId is not null
                          		then
                          		(select
                          		case when PreferredTFN='C' then CellPhone
                          		else case when PreferredTFN='H' then HomePhone
                          		else case when PreferredTFN='W' then WorkPhone
                          		else case when CellPhone is not null then CellPhone
                          		else case when HomePhone is not null then HomePhone
                          		else case when WorkPhone is not null then WorkPhone
                          		else ''
                          		end end end end end end
                          		from CRM.Customer cus
                          		join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
                          		where acus.AccountId=op.AccountId) end as AccountPhone,
                                (select [CRM].[fnGetAccountNameByAccountId](acc.AccountId)) as Name from[crm].AccountCustomers op
                                Inner join [CRM].Customer customer on customer.CustomerId = op.CustomerId
                                Inner join CRM.Accounts acc on op.AccountId=acc.AccountId
								Inner join CRM.Opportunities opp on opp.AccountId = acc.AccountId
                                where opp.OpportunityId =@id and op.IsPrimaryCustomer = 1",
                                                new { id = id }).FirstOrDefault();

                data.Add(result);
                var result1 = connection.Query<EmailPhone>(@" select Customer.PrimaryEmail,                                                             (select                                                              case when PreferredTFN='C' then CellPhone                                                             else case when PreferredTFN='H' then HomePhone                                                              else case when PreferredTFN='W' then WorkPhone                                                             else case when CellPhone is not null then CellPhone                                                              else case when HomePhone is not null then HomePhone                                                              else case when WorkPhone is not null then WorkPhone                                                              else ''                                                              end end end end end end) as AccountPhone,'' as Name                                                              from CRM.Customer Customer                                                               where Customer.CustomerId in (                                                             select                                                               case when ISNULL(insad.ContactId,0)>0 then ContactId else                                                              (select CustomerId from [CRM].[AccountCustomers] where AccountId=op.AccountId and IsPrimaryCustomer=1) end                                                             from CRM.Opportunities op                                                             join CRM.Addresses insad on op.BillingAddressId=insad.AddressId                                                             where op.OpportunityId=@id                                                             )",
                                               new { id = id }).FirstOrDefault();
                data.Add(result1);

                var result2 = connection.Query<EmailPhone>(@" 		  select Customer.PrimaryEmail,                                                             (select                                                              case when PreferredTFN='C' then CellPhone                                                             else case when PreferredTFN='H' then HomePhone                                                              else case when PreferredTFN='W' then WorkPhone                                                             else case when CellPhone is not null then CellPhone                                                              else case when HomePhone is not null then HomePhone                                                              else case when WorkPhone is not null then WorkPhone                                                              else ''                                                              end end end end end end) as AccountPhone, '' as Name                                                              from CRM.Customer Customer                                                               where Customer.CustomerId in (                                                             select                                                               case when ISNULL(insad.ContactId,0)>0 then ContactId else                                                              (select CustomerId from [CRM].[AccountCustomers] where AccountId=op.AccountId and IsPrimaryCustomer=1) end                                                             from CRM.Opportunities op                                                              join CRM.Addresses insad on op.InstallationAddressId=insad.AddressId                                                             where op.OpportunityId=@id                                                             )",
                                              new { id = id }).FirstOrDefault();

                data.Add(result2);
                connection.Close();
                return Json(data);
            }
        }


        [HttpGet]
        public IHttpActionResult getPhoneEmailforOrder(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                List<EmailPhone> data = new List<EmailPhone>();

                //var result = connection.Query("select op.CustomerId as accountId,FirstName + ' ' + LastName  as Name  from [crm].[OpportunityCustomers] op Inner join [CRM].[Customer] customer on  customer.CustomerId=op.CustomerId where op.OpportunityId=@id", new { id = id }).FirstOrDefault();
                //var result = connection.Query("select op.CustomerId as accountId,FirstName + ' ' + LastName + case when CompanyName is not null and CompanyName!='' then ' / '+CompanyName else '' end  as Name  from[crm].[OpportunityCustomers] op Inner join[CRM].[Customer] customer on customer.CustomerId = op.CustomerId where op.OpportunityId = @id", new { id = id }).FirstOrDefault();
            
                var result1 = connection.Query<EmailPhone>(@" select Customer.PrimaryEmail,                                                             (select                                                              case when PreferredTFN='C' then CellPhone                                                             else case when PreferredTFN='H' then HomePhone                                                              else case when PreferredTFN='W' then WorkPhone                                                             else case when CellPhone is not null then CellPhone                                                              else case when HomePhone is not null then HomePhone                                                              else case when WorkPhone is not null then WorkPhone                                                              else ''                                                              end end end end end end) as AccountPhone,'' Name                                                               from CRM.Customer Customer                                                               where Customer.CustomerId in (                                                             select                                                               case when ISNULL(insad.ContactId,0)>0 then ContactId else                                                              (select CustomerId from [CRM].[AccountCustomers] where AccountId=op.AccountId and IsPrimaryCustomer=1) end                                                             from CRM.Orders o                                                             join CRM.Opportunities op on o.OpportunityId=op.OpportunityId                                                             join CRM.Addresses insad on op.BillingAddressId=insad.AddressId                                                             where o.OrderID=@id)",
                                                             new { id = id }).FirstOrDefault();
                data.Add(result1);

                var result2 = connection.Query<EmailPhone>(@" select Customer.PrimaryEmail,                                                             (select                                                              case when PreferredTFN='C' then CellPhone                                                             else case when PreferredTFN='H' then HomePhone                                                              else case when PreferredTFN='W' then WorkPhone                                                             else case when CellPhone is not null then CellPhone                                                              else case when HomePhone is not null then HomePhone                                                              else case when WorkPhone is not null then WorkPhone                                                              else ''                                                              end end end end end end) as AccountPhone,'' Name                                                               from CRM.Customer Customer                                                               where Customer.CustomerId in (                                                             select                                                               case when ISNULL(insad.ContactId,0)>0 then ContactId else                                                              (select CustomerId from [CRM].[AccountCustomers] where AccountId=op.AccountId and IsPrimaryCustomer=1) end                                                             from CRM.Orders o                                                             join CRM.Opportunities op on o.OpportunityId=op.OpportunityId                                                             join CRM.Addresses insad on op.InstallationAddressId=insad.AddressId                                                             where o.OrderID=@id)",
                                                             new { id = id }).FirstOrDefault();

                data.Add(result2);
                connection.Close();
                return Json(data);
            }
        }
        [HttpGet]
        public IHttpActionResult OpportunitiesStatus(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                // Murugan: added order by so the list display in alphabetical order
                // TODO: is this should be shown in some other order???
                var query = @"select OpportunityStatusID,Name from [CRM].[Type_OpportunityStatus]
                            order by Name";
                connection.Open();
                var result = connection.Query(query).ToList();
                connection.Close();
                if (result == null)
                {
                    return OkTP();
                }
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult InstallationAddresses(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = string.Empty;
                //   var result;
                if (id > 0)
                {
                    // Murugan: added order by, so that the list will be displayed in alphabetical order.
                    query = @"select a.AccountId as RelatedAccountId,0 as AccountId, ad.IsValidated, ad.AddressId as InstallAddressId,
                                cu.FirstName+' '+cu.LastName +' - '+  ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name,
                                A.TaxExemptID,A.IsTaxExempt,A.IsPSTExempt,A.IsGSTExempt,A.IsHSTExempt,A.IsVATExempt
                                from CRM.Accounts A
                                join CRM.AccountCustomers accu on a.AccountId=accu.AccountId and accu.IsPrimaryCustomer=1
                                join CRM.Customer cu on accu.CustomerId=cu.CustomerId
                                JOIN CRM.AccountAddresses acad ON A.AccountId=acad.AccountId
                                JOIN CRM.Addresses ad on acad.AddressId=ad.AddressId and ad.IsDeleted=0
                                where a.AccountId = (select RelatedAccountId from CRM.Accounts where AccountId = @id)
                                union
                                select a.RelatedAccountId,a.AccountId, ad.IsValidated, ad.AddressId as InstallAddressId,
                                ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name,
                                A.TaxExemptID,A.IsTaxExempt,A.IsPSTExempt,A.IsGSTExempt,A.IsHSTExempt,A.IsVATExempt                                
                                from CRM.Accounts A
                                JOIN CRM.AccountAddresses acad ON A.AccountId=acad.AccountId
                                JOIN CRM.Addresses ad on acad.AddressId=ad.AddressId and ad.IsDeleted=0
                                where a.AccountId = @id
                                order by a.AccountId";
                    //query = @"select * from(select a.RelatedAccountId, acad.AccountId,ad.IsValidated, ad.AddressId as InstallAddressId,
                    //           ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name
                    //    from CRM.Addresses ad inner join crm.AccountAddresses acad on acad.AddressId = ad.AddressId left join
                    //        crm.Accounts a on a.AccountId = acad.AccountId where a.AccountId = @id) a
                    //          where a.Name is not null
                    //            order by Name";
                    var result = connection.Query(query, new { id = id }).ToList();

                    connection.Close();
                    if (result == null)
                    {
                        return OkTP();
                    }
                    return Json(result);
                }
                else
                {
                    // Murugan: added order by, so that the list will be displayed in alphabetical order.
                    query = @"select * from(select  acad.AccountId,ad.IsValidated, ad.AddressId as InstallAddressId,a.FranchiseId,     ROW_NUMBER() OVER(PARTITION BY ad.AddressId ORDER BY ad.AddressId DESC) rn,
                                       ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name
                                    from CRM.Addresses ad inner join crm.AccountAddresses acad on acad.AddressId = ad.AddressId left
                                    join crm.Accounts a on a.AccountId = acad.AccountId  ) a
                                    where a.Name is not null and FranchiseId = @FranchiseId and rn= 1
                                    order by Name";
                    var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    if (result == null)
                    {
                        return OkTP();
                    }
                    return Json(result);
                }
            }
        }

        [HttpGet]
        public IHttpActionResult getDisplayTerritory(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @" 	select ac.TerritoryDisplayId,ac.TerritoryId, ac.TerritoryType,te.Name as DisplayTerritoryName  from crm.Accounts ac
	                            left join CRM.Territories te on ac.TerritoryDisplayId = te.TerritoryId where accountId= @accountId";
                var result = connection.Query(query, new { accountId = id });
                connection.Close();
              
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult getInstalltionAddressName(int id)
        {
            if (id == 0)
            {
                return Json("InstallationAddress Not exist");
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select IsValidated, AddressId as InstallAddressId,
                            adr.Address1,adr.Address1,adr.City,adr.State,adr.ZipCode,
                            adr.CountryCode2Digits,
                            adr.ContactId,cus.FirstName+' '+cus.LastName as ContactName,
                            ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name
                            from [CRM].[Addresses] adr left join CRM.Customer cus
                            on adr.ContactId = cus.CustomerId where AddressId=@id";
                var result = connection.Query(query, new { id = id }).FirstOrDefault();
                connection.Close();
                if (result == null)
                {
                    return OkTP();
                }
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAccounts(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                // Murugan. Added order by so that the list will be display name in alphabetical order.
                //var query = @"select AccountId as accountId,FirstName + ' ' + LastName  as Name
                //                from crm.Accounts a
                //                inner join crm.Customer c on a.PersonId=c.CustomerId where a.franchiseid = @franchiseid
                //                order by name";
                var query = @"SELECT
                             Account.[AccountId] as accountId,
                              (select [CRM].[fnGetAccountNameByAccountId](Account.AccountId)) as  Name
                             FROM [crm].[Accounts] Account
                             INNER JOIN CRM.Type_AccountStatus tls ON tls.Accountstatusid = Account.Accountstatusid
                             INNER JOIN [CRM].[Addresses] a
                             on a.[AddressId]=(select top 1 la.AddressId from crm.AccountAddresses la where la.AccountId = Account.AccountId)
                             INNER JOIN [CRM].[Customer] p
                             on p.CustomerId=Account.[PersonId]
                             LEFT JOIN [CRM].[Customer] secondPerson
                             on secondPerson.CustomerId = Account.SecPersonId
                             WHERE Account.FranchiseId= @franchiseid AND ISNULL(Account.IsDeleted,0)=0 order by isnull(Account.LastUpdatedOnUtc,Account.CreatedOnUtc) desc";
                connection.Open();
                var result = connection.Query(query, new { franchiseid = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetSalesAgents(int? id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select distinct per.personid as SalesAgentId,FirstName + ' ' + LastName  as Name ,per.PrimaryEmail as Email
                                    from [Auth].[Users] usr
                                    join [CRM].[Person] per on per.personid = usr.personid
                                    join [Auth].[UsersInRoles] uir on usr.userid = uir.userid
                                    where usr.franchiseid = @FranchiseId and uir.roleid in( '9CFE899F-A036-4D69-A58B-A1BFBDD8DCA0','06547EFF-24AA-454B-A399-9017009B3FBD') and usr.IsDeleted!=1 and (usr.IsDisabled=0 or usr.IsDisabled is null)
                                    order by Name";
                connection.Open();
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                if (result == null)
                    return OkTP();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult getSalesAgentName(int id)
        {
            if (id == 0)
            {
                return Json("SalesAgent Not Found");
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var result = connection.Query("select FirstName + ' ' + LastName  as Name FROM [CRM].[Person]  where PersonId=@id", new { id = id }).FirstOrDefault();
                connection.Close();
                if (result == null)
                    return OkTP();
                return Json(result);
            }
        }

        //getInstalltionAddressName
        [HttpGet]
        public IHttpActionResult getInstallerName(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var result = connection.Query("select FirstName + ' ' + LastName  as Name FROM [CRM].[Person]  where PersonId=@id", new { id = id }).FirstOrDefault();
                connection.Close();
                if (result == null)
                    return OkTP();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetInstallers(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select distinct per.personid as InstallerId,FirstName + ' ' + LastName  as Name ,per.PrimaryEmail as Email
                                from [Auth].[Users] usr
                                join [CRM].[Person] per on per.personid = usr.personid
                                join [Auth].[UsersInRoles] uir on usr.userid = uir.userid
                                where usr.franchiseid = @franchiseid and uir.roleid = '3F1154E2-0718-40DC-8F8E-75DCA2183AC1' and usr.IsDeleted!=1 and (usr.IsDisabled=0 or usr.IsDisabled is null)
                                order by Name";
                connection.Open();
                var result = connection.Query(query, new { franchiseid = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                if (result == null)
                    return OkTP();
                return Json(result);
            }
        }

        //[HttpPost/*, ValidateAjaxAntiForgeryToken*/]
        //public IHttpActionResult Post1([FromBody]Opportunity model)
        //{
        //    //var status = LeadMgr.CreateLead(out leadId, model);

        //    //return ResponseResult(status, Json(new { LeadId = leadId }));

        //    return ResponseResult("Success");
        //}

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpGet]
        public IHttpActionResult GetOpportunityQuestionTypes()
        {
            //Added for Qualification/Question Answers - Gets the Type_Question
            int BrandId = (int)SessionManager.BrandId;
            return Json(HFC.CRM.Managers.TypeQuestionManager.GetOpportunityTypeQuestions(BrandId));
        }

        /// <summary>
        /// Gets a single opportunity, will include a lot of opportunity detail
        /// </summary>
        /// <param name="id">Opportunity Id</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                if (id <= 0)
                {
                    throw new Exception("Opportunity Id is required");
                }

                int totalRecords = 0;

                var opportunity = OpportunityMgr.Get(id);

                var FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString();
                var FranchiseAddressObject = SessionManager.CurrentFranchise.Address;
                var Addresses = OpportunityMgr.GetAddress(new List<int>() { id });
                var States = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 });
                var Countries = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct();
                var OpportunityNotes = OpportunityMgr.GetNotes(new List<int>() { opportunity.OpportunityId }, false).Where(n => n.TypeEnum != NoteTypeEnum.Primary);
                //var NoteTypeEnum = NoteTypeEnum.Internal.ToDictionary<byte>((byte)NoteTypeEnum.Primary);
                var PurchaseOrdersCount = totalRecords;
                var SelectedOpportunityStatus = opportunity.OpportunityStatus.Name;
                var selectedCampaign = opportunity.CampaignName;
                var OpportunityQuestionAns = opportunity.OpportunityQuestionAns;
                var PrimarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault();
                var SecondarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == false).ToList();
                opportunity.AccountTP = actMgr.Get(opportunity.AccountId);

                return
                    Ok(
                        new
                        {
                            Opportunity = opportunity,
                            OpportunityPrimaryNotes = opportunity.OpportunityNotes.FirstOrDefault(n => n.TypeEnum == NoteTypeEnum.Primary),
                            //OpportunityStatuses = SessionManager.CurrentFranchise.OpportunityStatusTypeCollection(),
                            FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString(),
                            FranchiseAddressObject = SessionManager.CurrentFranchise.Address,
                            Addresses = OpportunityMgr.GetAddress(new List<int>() { id }),
                            States = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 }),
                            Countries = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct(),
                            OpportunityNotes = OpportunityMgr.GetNotes(new List<int>() { opportunity.OpportunityId }, false).Where(n => n.TypeEnum != NoteTypeEnum.Primary),
                            NoteTypeEnum = NoteTypeEnum.Internal.ToDictionary<byte>((byte)NoteTypeEnum.Primary),
                            PurchaseOrdersCount = totalRecords,
                            SelectedOpportunityStatus = opportunity.OpportunityStatus.Name,
                            selectedCampaign = opportunity.CampaignName,
                            OpportunityQuestionAns = opportunity.OpportunityQuestionAns,  //Added for Qualification/Question Answers
                            PrimarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault(),
                            SecondarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == false).ToList(),
                            FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting,
                            TextStatus = communication_Mangr.GetOptingInfo(opportunity.AccountTP.PrimCustomer.CellPhone)
                        });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        // GET api/opportunitys

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpGet]
        public IHttpActionResult GetQuestionTypes()
        {
            //Added for Qualification/Question Answers - Gets the Type_Question
            int BrandId = (int)SessionManager.BrandId;
            return Json(HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestions(BrandId));
        }

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpGet]
        public IHttpActionResult GetQuestionTypesAns(int opportunityId)
        {
            //Added for Qualification/Question Answers - Gets the Type_Question
            int BrandId = (int)SessionManager.BrandId;
            return Json(HFC.CRM.Managers.TypeQuestionManager.GetTypeOpportunityQuestionsById(BrandId, opportunityId));
        }

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpPost, ValidateAjaxAntiForgeryToken, ActionName("QandA")]
        public IHttpActionResult AddQuestionAnswer(int id, [FromBody]QuestionAnswer answer)
        {
            TypeQuestionManager QansMgr = new TypeQuestionManager(SessionManager.CurrentUser.PersonId, SessionManager.CurrentFranchise.FranchiseId);
            int answerId = 0,
                opportunityId = id,
                questionId = (int)answer.QuestionId;
            string Answered = (string)answer.Answer;
            //Check message and save
            var status = answer == null ? "Invalid question and answer" : "";
            //
            if (answer != null && opportunityId > 0 && questionId > 0 && Answered != "")
            {
                status = QansMgr.AddAnswer(opportunityId, opportunityId, questionId, Answered, out answerId);
            }
            //Return the reponse
            return ResponseResult(status, Json(new { AnswerId = answerId }));
        }
              
        /// <summary>
        /// Updates the question answers.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpPut, ValidateAjaxAntiForgeryToken, ActionName("QandA")]
        public IHttpActionResult UpdateQuestionAnswers(int id, [FromBody]QuestionAnswer answer)
        {
            TypeQuestionManager QansMgr = new TypeQuestionManager(SessionManager.CurrentUser.PersonId, SessionManager.CurrentFranchise.FranchiseId);
            var status = answer == null ? "Invalid question and answer" : "";
            if (answer != null)
                status = QansMgr.UpdateAnswers(id, (int)answer.AnswerId, (string)answer.Answer);

            return ResponseResult(status);
        }

        [HttpGet]
        public IHttpActionResult Get([FromUri]SearchFilter filter)
        {
            try
            {
                int totalRecords = 0;
                var opportunitys = OpportunityMgr.Get(out totalRecords,
                                            filter.opportunityStatusIds,
                                            //filter.jobStatusIds,
                                            //filter.salesPersonIds,
                                            filter.createdOnUtcStart,
                                            filter.createdOnUtcEnd,
                                            filter.searchFilter,
                                            HttpUtility.HtmlDecode(filter.searchTerm),
                                            filter.orderBy,
                                            filter.orderByDirection,
                                            filter.pageIndex,
                                            filter.pageSize);

                return Json(new { TotalRecords = totalRecords, OpportunityCollection = opportunitys });
            }
            catch (UnauthorizedAccessException ave)
            {
                return Forbidden(ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        // GET api/opportunitys

        [ActionName("Recent")]
        [HttpGet]
        public IHttpActionResult Recent(int id, [FromUri] SearchFilter filter)
        {
            try
            {
                int totalRecords = 0;
                var opportunitys = OpportunityMgr.Get(out totalRecords,
                    filter.opportunityStatusIds,
                    null,
                    null,
                    SearchFilterEnum.Auto_Detect,
                    null,
                    filter.orderBy,
                    filter.orderByDirection,
                    0,
                    10,
                    new[] { "Lost Opportunity", "Bad Opportunity", "Sale Made", "Complete", "Service Complete", "Lost Sale" });

                return Json(new { TotalRecords = totalRecords, OpportunityCollection = opportunitys });
            }
            catch (UnauthorizedAccessException ave)
            {
                //return Forbidden(ave.Message);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        //[ActionName("SaveOpportunity")]
        //[HttpPost/*, ValidateAjaxAntiForgeryToken*/]
        //[HttpPost, ValidateAjaxAntiForgeryToken, ActionName("SaveOpportunity")]
        [HttpPost]
        public IHttpActionResult Post([FromBody]Opportunity model)
        {
            int opportunityId = 0;

            //texting
            var CountryCode = SessionManager.CurrentFranchise.CountryCode;
            var BrandId = SessionManager.CurrentFranchise.BrandId;
            var Account = actMgr.Get(model.AccountId);
            if (model.SendMsg)
            {
                var SendOptinMessage = communication_Mangr.ForceOptinMessage(Account.PrimCustomer.CellPhone, BrandId, CountryCode, null, model.AccountId);
            }
            if (model.UncheckNotifyviaText)
            {
                var unchecknotify = actMgr.UpdateNotifyText(model.AccountId);
            }

            if (model.SourcesTPIdFromCampaign > 0) model.SourcesTPId = model.SourcesTPIdFromCampaign;

            if (model.OpportunityId > 0)
            {
                var updateStatus = OpportunityMgr.Update(model);
                return ResponseResult(updateStatus, Json(new { OpportunityId = model.OpportunityId }));
            }
            if (model.SkipDuplicateCheck == false)
            {
                var dulpicateList = OpportunityMgr.GetDuplicateOpportunitys(SearchDuplicateOpportunityEnum.All, string.Empty, model);
                if (dulpicateList.Count > 0)
                {
                    var dtos = dulpicateList.Select(v => new DuplicateOpportunityModel()
                    {
                        Address = v.Addresses.FirstOrDefault() != null ? v.Addresses.FirstOrDefault().Address1 : "",
                        OpportunityId = v.OpportunityId,
                        CellPhone = TelBuilder(v.PrimCustomer.CellPhone),
                        HomePhone = TelBuilder(v.PrimCustomer.HomePhone),
                        FaxPhone = TelBuilder(v.PrimCustomer.FaxPhone),
                        WorkPhone = TelBuilder(v.PrimCustomer.WorkPhone),
                        PrimaryEmail = v.PrimCustomer.PrimaryEmail,
                        SecondaryEmail = v.PrimCustomer.SecondaryEmail,
                        FullName = v.PrimCustomer.FullName,
                        OpportunityNumber = v.OpportunityNumber
                    }).Take(3);

                    return ResponseResult("Success", Json(new { lstDuplicates = dtos }));
                }
            }

            var status = OpportunityMgr.CreateOpportunity(out opportunityId, model, 0);

            return ResponseResult(status, Json(new { OpportunityId = opportunityId }));
        }

        private string TelBuilder(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            str = "(" + str.Insert(3, ")");
            return str.Insert(8, "-");
        }

        [HttpPost, HttpPut, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Status(int id, [FromBody]Type_OpportunityStatus model)
        {
            var result = OpportunityMgr.UpdateStatus(id, model.OpportunityStatusId);

            return ResponseResult(result);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            return ResponseResult(OpportunityMgr.Delete(id));
        }

        [HttpGet]
        [ActionName("Source")]
        public IHttpActionResult GetSources(int id)
        {
            try
            {
                var opportunitySources = OpportunityMgr.GetSources(id);
                // var SelectedSources = OpportunityMgr.GetSelectedSources(id);

                //var sources = SessionManager.CurrentFranchise.GetSources().ToList();

                /*foreach (var source in sources)
                {
                    this.FilterChilds(source);
                }*/

                return
                    Ok(
                        new
                        {
                            OpportunitySources = opportunitySources,

                            //Sources = sources.Where(s => s.Parent == null)
                        });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        [ActionName("SelectedSources")]
        public IHttpActionResult GetSelectedSources(int id)
        {
            try
            {
                var SelectedSources = OpportunityMgr.GetSelectedSources(id);

                return
                    Ok(
                        new
                        {
                            SelectedSources = SelectedSources,
                        });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        private void FilterChilds(Source source)
        {
            source.Children = source.Children.Where(c => c.FranchiseId == null || c.FranchiseId == SessionManager.CurrentFranchise.FranchiseId).ToList();
            if (source.Children.Any())
            {
                foreach (var child in source.Children)
                {
                    this.FilterChilds(child);
                }
            }
        }

        /// <summary>
        /// This is for adding new source
        /// </summary>
        /// <param name="id">Opportunity Id</param>
        [HttpPost, ValidateAjaxAntiForgeryToken, ActionName("Source")]
        public IHttpActionResult NewSource(int id, [FromBody]List<int> sources)
        {
            var result = OpportunityMgr.UpdateSources(id, sources);
            return ResponseResult(result);
        }

        /// <summary>
        /// Updates a opportunity source
        /// </summary>
        /// <param name="id">Opportunity Source Id</param>
        /// <param name="source">Binds SourceId property</param>
        /// <returns></returns>
        /*[HttpPut, ActionName("Source"), ValidateAjaxAntiForgeryToken]
        public IHttpActionResult UpdateSource(int id, [FromBody]OpportunitySource source)
        {
            var result = OpportunityMgr.UpdateSource(id, source.SourceId);

            return ResponseResult("Success");
        }*/

        /// <summary>
        /// Deletes a opportunity source
        /// </summary>
        /// <param name="id">Opportunity Source Id</param>
        /// <param name="source"></param>
        /// <returns></returns>
        [HttpDelete, ValidateAjaxAntiForgeryToken, ActionName("Source")]
        public IHttpActionResult DeleteSource(int id)
        {
            var result = OpportunityMgr.DeleteSource(id);

            return ResponseResult(result);
        }

        /// <summary>
        /// Updates either primary or secondary person in opportunity
        /// </summary>
        /// <param name="id">OpportunityId</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut, ValidateAjaxAntiForgeryToken, ActionName("Person")]
        public IHttpActionResult UpdatePerson(int id, [FromBody]Person model)
        {
            var status = OpportunityMgr.UpdatePerson(id, model);

            return ResponseResult(status);
        }

        /// <summary>
        /// This will add to secondary customer, we do not need an Add for primary since it is required to create a new opportunity
        /// </summary>
        /// <param name="id">OpportunityId</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, ValidateAjaxAntiForgeryToken, ActionName("Person")]
        public IHttpActionResult AddPerson(int id, [FromBody]Person model)
        {
            int personId = 0;
            var status = OpportunityMgr.AddSecondaryCustomer(id, model, out personId);

            return ResponseResult(status, Json(new { PersonId = personId }));
        }
        

        [HttpPost]
        public IHttpActionResult CheckDuplicates(int id, [FromBody]OpportunityDuplicateFilter filter)
        {
            var lst = OpportunityMgr.GetDuplicateOpportunitys(filter.SearchType, filter.SearchTerm);

            var dtos = lst.Select(v => new DuplicateOpportunityModel()
            {
                Address = v.Addresses.FirstOrDefault() != null ? v.Addresses.FirstOrDefault().Address1 : "",
                OpportunityId = v.OpportunityId,
                CellPhone = TelBuilder(v.PrimCustomer.CellPhone),
                HomePhone = TelBuilder(v.PrimCustomer.HomePhone),
                FaxPhone = TelBuilder(v.PrimCustomer.FaxPhone),
                WorkPhone = TelBuilder(v.PrimCustomer.WorkPhone),
                PrimaryEmail = v.PrimCustomer.PrimaryEmail,
                SecondaryEmail = v.PrimCustomer.SecondaryEmail,
                FullName = v.PrimCustomer.FullName,
                OpportunityNumber = v.OpportunityNumber
            }).Take(3);

            var status = "Success";
            return ResponseResult(status, Json(new { lstDuplicates = dtos }));
        }

        [HttpPost]
        public IHttpActionResult UpdateOpportunityStatus(int opportunityId, int StatusId)
        {
            var res = OpportunityMgr.UpdateOpportunityStatus(opportunityId, StatusId);

            return Json(new { data = res, error = "" });
        }

        [HttpGet]
        public IHttpActionResult checkOpportunityAccess(int id)
        {
             var GetPermission = permissionManager.GetUserRolesPermissions();
            var ModuleData = GetPermission.Find(x => x.ModuleCode == "Opportunity");
            var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "OpportunityAllRecordAccess").CanAccess;

            return Json(specialPermission);

        }

        #region api 2.0

        [HttpGet]
        public IHttpActionResult GetNewOpportunityCount(int id)
        {
            try
            {
                int count = OpportunityMgr.GetNewOpportunityCount();

                return Json(new
                {
                    count = count
                });
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetOpportunityInfo(int opportunityId)
        {
            try
            {
                OpportunityInfoDTO opportunity = OpportunityMgr.GetOpportunityInfo(opportunityId);

                return Json(new
                {
                    opportunity = opportunity
                });
            }
            catch (UnauthorizedAccessException ave)
            {
                return StatusCode(HttpStatusCode.Forbidden, ave.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult CustomerOpportunityUpdate(int id, [FromBody]QuickDispositionVM model)
        {
            string[] splitValue = model.Value.Split('|');
            var opportunityStatusId = Convert.ToInt16(splitValue[0]);
            var dispositionId = Convert.ToInt16(splitValue[1]);
            var status = OpportunityMgr.UpdateOpportunity(id, opportunityStatusId, dispositionId);
            return ResponseResult(status);
        }

        #endregion api 2.0

        public class EmailPhone
        {
            public string PrimaryEmail { get; set; }
            public string AccountPhone { get; set; }
            public string Name { get; set; }
        }
    }
}