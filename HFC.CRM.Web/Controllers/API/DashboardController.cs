﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.DTO.Dashboard;
using HFC.CRM.Web.Filters;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class DashboardController : BaseApiController
    {
        public IHttpActionResult GetVisibleWidget(int id)
        {
            var widgetList = new List<string>();
            widgetList.Add("Tasks");
            widgetList.Add("Appointments");
            widgetList.Add("Recent Leads and Jobs");
            widgetList.Add("Monthly Job Status");
            widgetList.Add("Monthly Top 5 Sales Agents");
            widgetList.Add("Monthly Jobs by Source");
            widgetList.Add("Monthly Sales by Source");
            widgetList.Add("Yearly Top 5 Sales Agents");
            widgetList.Add("Yearly Jobs by Source");

            var res = widgetList.Select(w => new VisibleWidgetsDTO
            {
                Name = w,
                IsVisible = SessionManager.CurrentUser.IsWidgetEnabled(SessionManager.CurrentFranchise.FranchiseId, w)
            }).ToList();
            return Json(new {visibleWodgets = res});
        }
    }
}