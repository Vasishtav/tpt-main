﻿using HFC.Common.Logging;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// The API namespace.
/// </summary>
namespace HFC.CRM.Web.Controllers.API
{
    /// <summary>
    /// Class CampaignsController.
    /// </summary>
    [LogExceptionsFilter]
    public class CampaignsController : BaseApiController
    {
        /// <summary>
        /// The fran MGR
        /// </summary>
        FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        CampaignManager campaignmgr = new CampaignManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private readonly ILog _Log = BaseLog.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // TODO: is this required in TP???
        //[ValidateAjaxAntiForgeryToken]
        //public IHttpActionResult Post([FromBody]MarketingCampaign model)
        //{
        //    string result = FranMgr.AddCampaign(model);

        //    return ResponseResult(result, Json(new { MktCampaignId = model != null ? model.MktCampaignId : 0 }));
        //}

        /// <summary>
        /// Puts the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Put(int id, [FromBody]MarketingCampaign model)
        {
            string result = "Invalid data";
            if (model != null)
            {
                model.MktCampaignId = id;
                result = FranMgr.UpdateCampaign(model);
            }

            return ResponseResult(result);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IHttpActionResult.</returns>
        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Delete(int id)
        {
            return ResponseResult(FranMgr.DeleteCampaign(id));
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult DisableCampaign(int id)
        {
            try
            {
                FranMgr.DisableCampaign();
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public IHttpActionResult DeleteCampaign(int id, List<int> campaingIds)
        {
            try
            {
                campaignmgr.DeleteCampaigs(campaingIds);
                return Json(new { data = "Success", error = ""});
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult GetCampaignByFranchiseTP(int id, bool includeInactive = false)
        {
            try
            {
                if (SessionManager.CurrentFranchise == null ||
                    SessionManager.CurrentFranchise.FranchiseId <= 0)
                {
                    var ex = new Exception("Current Franchise is null");
                    _Log.Error(ex.Message, ex);
                    return ResponseResult(ex.Message);
                }

                var result = campaignmgr.GetCamapaigns(SessionManager.CurrentFranchise.FranchiseId, includeInactive);
                return Json(result);
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpPut]
        public IHttpActionResult UpdateCampaign(Campaign model)
        {
            var status = "Invalid data. Update Failed. Plese try again.";

            if (model != null)
            {
                status = campaignmgr.UpdateCampaign(model);
            }
            return ResponseResult(status);
        }

        [HttpPost]
        public IHttpActionResult AddCampaign(Campaign model)
        {
            var status = "Invalid data. Save Failed. Please try again";
            if (model != null)
            {
                var result = campaignmgr.AddCampaign(model);
                if (result > 0)
                    status = "Success"; 
            }
            return ResponseResult(status);
        }
    }
}
