﻿using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web.Http;
using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.Order;
using System.Net.Mail;
using HFC.CRM.Serializer;
using System.Linq;
using System.Data.SqlClient;
using HFC.CRM.Data.Context;
using StackExchange.Profiling.Helpers.Dapper;
using System.IO;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class OrdersController : BaseApiController
    {
        OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        OpportunitiesManager oppMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                var result = ordermgr.GetOrder(id, FranchiseId);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }
        [HttpGet]
        public IHttpActionResult OrderStatus(int id)
        {
            try
            {
                var result = ordermgr.GetOrderStatues(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet]
        public IHttpActionResult OrderPaymentMethods(int id)
        {
            try
            {
                var result = ordermgr.OrderPaymentMethods(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult OrderReversalReason(int id)
        {
            try
            {
                var result = ordermgr.OrderReversal(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult OrderPayments(int id)
        {
            try
            {
                var result = ordermgr.OrderPayments(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        [HttpGet]
        [ActionName("OrderPaymentsByPaymentId")]
        public IHttpActionResult OrderPaymentsByPaymentId(int id, int orderId)
        {
            try
            {
                var result = ordermgr.OrderPaymentsByPaymentId(id, orderId);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        [HttpPost]

        public IHttpActionResult Post([FromBody]OrderDTO model)
        {
            try
            {
                var order = ordermgr.GetOrder(model.OrderID, SessionManager.CurrentFranchise.FranchiseId);
                var result = ordermgr.OrderUpdate(model);

                if (model.OrderStatus == 5)
                {
                    if (order.SendReviewEmail == true)
                    {
                        return Json(result);
                    }
                    else
                    {
                        var InvoiceHistory = ordermgr.GetInvoiceHistory(model.OrderID);
                        if (model.SendReviewEmail == true && model.IsNotifyemails == true)
                        {
                            if (InvoiceHistory != null)
                                //SendEmail(model.OrderID, 6, model.SendReviewEmail, InvoiceHistory.Streamid.ToString());
                                SendEmail(model.OrderID, EmailType.ThankyouReview, model.SendReviewEmail, InvoiceHistory.Streamid.ToString());
                            else
                                //SendEmail(model.OrderID, 6, model.SendReviewEmail, "");
                                SendEmail(model.OrderID, EmailType.ThankyouReview, model.SendReviewEmail, "");
                        }

                        //text sms
                        if (model.SendReviewEmail == true && model.IsNotifyText == true)
                            Communication.SendOrderTextSms(model.OrderID, EmailType.ThankyouReview);
                    }


                }
                else if (model.OrderStatus == 6)
                {
                    QuotesMgr.UpdateQuotesStatus(model.QuoteKey, 4);
                    oppMgr.UpdateOpportunityStatus(model.OpportunityId, 3);
                }
                else if (model.OrderStatus == 9)
                {
                    QuotesMgr.UpdateQuotesStatus(model.QuoteKey, 2);
                    oppMgr.UpdateOpportunityStatus(model.OpportunityId, 3);
                }

                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPut]
        [ActionName("AddPayment")]
        public IHttpActionResult AddPayment(int id, PaymentDTO model)
        {
            try
            {

                var result = ordermgr.UpdateReversePayment(model);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        public IHttpActionResult updateContractedDate(int id, DateTime date, int quotekey)
        {
            try
            {
                var quotesdetail = QuotesMgr.GetDetails(quotekey);
                if (quotesdetail.PreviousSaleDate == null)
                {
                    var savesale = QuotesMgr.SaveSale(quotekey, date);
                }
                var result = ordermgr.updateContractedDate(id, date);
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }
        [HttpPut]
        [ActionName("SetInstallationAppointment")]
        public IHttpActionResult SetInstallationAppointment(int id, OrderDTO model)
        {
            try
            {
                var result = ordermgr.UpdateOrderStatus(model.OrderID);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpPut]
        [ActionName("CreateInvoice")]
        public IHttpActionResult CreateInvoice(int id, Orders model)
        {
            try
            {
                InvoiceSheetReport pdf = new InvoiceSheetReport();
                int invoiceId = ordermgr.CreateInvoice(model);
                Guid filepdf = pdf.createInvoicePDF(invoiceId);

                //QuoteSheetReport qsr = new QuoteSheetReport();
                //Guid filepdfq = qsr.createQuotePDF(1119);
                var result = ordermgr.CreateInvoice(invoiceId, filepdf);


                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }



        [HttpPut]
        [ActionName("SendInvoiceEmail")]
        public IHttpActionResult SendInvoiceEmail(int id, EmailUrlParam model)
        {
            try
            {
                //EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = EmailManager.CreateMessageWithAttachment(id, model);
                //var result = emailMgr(model);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Convert a order to Master Purchase Order
        /// </summary>
        /// <param name="id"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("OrderToMPO")]
        public IHttpActionResult ConvertToMPO(int id)
        {
            try
            {
                var order = ordermgr.GetOrderByOrderId(id);
                var validQuote = QuotesMgr.ValidateQuote(order.QuoteKey, true);
                if (!string.IsNullOrEmpty(validQuote))
                {
                    return Json(new { data = "", error = validQuote });
                }
                if (!PurchaseOrderMgr.CheckIfMPOExists(id))
                {
                    var result = PurchaseOrderMgr.ConvertOrderToMasterPurchaseOrder(id);
                    return Json(new { data = result, error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "A MPO already exists." });
                }

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = "The system is currently busy, please try again later." });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet]
        public IHttpActionResult GetOrderByOpportunity(int id)
        {
            try
            {
                var result = ordermgr.GetOrderByOpportunityId(id);
                return Json(new { error = "", data = result });

            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }

        }

        [HttpGet]
        public IHttpActionResult GetMasterPOList(int id)
        {
            try
            {
                var res = PurchaseOrderMgr.GetMPOList();

                // TP-1698: CLONE - Opportunities and related need record level permissions
                // Opportunity need a special permission to access all the
                // records. only certain roles has that permission. otherwise
                // If the logged in user is a Sales person or installation
                // person, the can see their records alone.
                if (!this.CanAccess("Opportunity All Record Access"))
                {
                    // Need to apply the filter for SalesAgent/Installation.
                    var filtered = res.Where(x => (x.SalesAgentId ==
                                        SessionManager.CurrentUser.PersonId ||
                                        x.InstallerId == SessionManager.CurrentUser.PersonId)
                                        ).ToList();

                    res = filtered;
                }

                var result = ordermgr.GetOrderByOpportunityId(id);
                return Json(new { error = "", data = result });

            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }

        }

        public string SendEmail(int OrderId, EmailType emailType, bool typestatus, string Streamid = "")
        {
            try
            {
                var SendOrderMail = ordermgr.SendOrdermail(OrderId, emailType, Streamid, typestatus, "");
                return SendOrderMail;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return "Failure";
            }

        }

        // no more needed as we are not updating the order status as part of mpo
        // cancellation. -- murugan
        /// <summary>
        /// This is needed for BuyersRemorse update Job
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult UpdateOrderStatustoOpen()
        {
            ordermgr.UpdateOrderStatustoOpen();
            return Json("success");
        }
        //[HttpPost]
        //public IHttpActionResult UpdateOrderStatus(int orderId, int StatusId)
        //{
        //    var res = ordermgr.UpdateOrderStatus(orderId, StatusId);

        //    return Json(new { data = res, error = "" });
        //}
        [HttpGet]
        public IHttpActionResult SendThankYouReviewEmail(int id)
        {
            var order = ordermgr.GetOrder(id, SessionManager.CurrentFranchise.FranchiseId);
            if (order.OrderStatus == 5)
            {
                if (order.SendReviewEmail == false)
                {
                    var InvoiceHistory = ordermgr.GetInvoiceHistory(order.OrderID);

                    //if (InvoiceHistory != null)
                    //    SendEmail(order.OrderID, 6,true, InvoiceHistory.Streamid.ToString());
                    //else
                    //    SendEmail(order.OrderID, 6,true,"");

                    var temp = InvoiceHistory != null ? InvoiceHistory.Streamid.ToString() : "";
                    SendEmail(order.OrderID, EmailType.ThankyouReview, true, temp);
                }
            }
            var result = ordermgr.UpdateOrderValue(id);
            return Json(result);
        }


        [HttpGet]
        public IHttpActionResult getPhoneEmailOrder(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var result = connection.Query(@"select (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as CellPhone,c.PrimaryEmail
                             from  CRM.Orders o	
							  join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                             join CRM.Accounts ac on op.AccountId=ac.AccountId
                             join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId and IsPrimaryCustomer=1
                             join CRM.Customer c on acc.CustomerId=c.CustomerId
							 where o.OrderID=@id",
                                         new { id = id }).FirstOrDefault();

                    connection.Close();

                    return Json(result);

                }
            }
            catch (Exception e)
            {
                return null;
            }


        }

        /// <summary>
        /// Get customer view data based on order id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetCustomerViewData(int id)
        {
            try
            {
                var result = ordermgr.GetorderCustomerView(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult AcceptTerms(int id, bool flag)
        {
            try
            {
                var result = ordermgr.AcceptTerms(id, flag);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateSign(int id, OrderCustomerView model, string type)
        {
            try
            {
               var result = ordermgr.UpdateSign(id, model,type);
               return Json(result);
                
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult getSignedOrNot(int id)
        {
            try
            {
                var result = ordermgr.GetSignedOrNot(id);
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}