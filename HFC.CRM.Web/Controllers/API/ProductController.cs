﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;

using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    using HFC.CRM.Web.Filters;
    using HFC.CRM.Web.Models.Product;
    using System.Collections.Generic;

    [LogExceptionsFilter]
    public class ProductController : BaseApiController
    {
        readonly ProductsManager productsManager = new ProductsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        
        public IHttpActionResult Post(ProductEditModel model)
        {
            var status = "Invalid data";
            int productId = 0;
            if (model != null)
            {
                status = productsManager.AddProduct(model.CategoryId, model.ProductType, out productId);
            }
            return ResponseResult(status, Json(new { Id = productId }));
        }

        public IHttpActionResult Put(ProductEditModel model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                status = productsManager.UpdateProduct(model.ProductId.Value, model.ProductType);
            }

            return ResponseResult(status);
        }

        public IHttpActionResult Delete(int id)
        {
            var status = productsManager.DeleteProduct(id);
            return ResponseResult(status);
        }
        [HttpGet]
        public IHttpActionResult GetCoreProductModels()
        {
           
            var modelList = productsManager.GetCoreProductModels();
            return Json(modelList);
            
        }
        [HttpPost]
        public IHttpActionResult UpdateCoreProductModel([FromBody] HFCModels model)
        {
            try
            {
                var status = productsManager.UpdateCoreProductModel(model);
                var modelList = productsManager.GetCoreProductModels();
                return Json(new { error = "", modelData = modelList });
            }
            catch(System.Exception ex)
            {
                return Json(new { error = "An error occurred, Please try again later."});
            }
            

        }
    }
}
