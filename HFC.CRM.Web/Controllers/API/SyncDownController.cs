﻿using HFC.CRM.Core.Common;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class SyncDownController : BaseApiController
    {
        [AllowAnonymous]
        public IHttpActionResult Get()
        {
            StringBuilder result = new StringBuilder("");
            
            bool isBusy = CacheManager.GetCache<bool>("CRM:SyncDownIsBusy");

            if (!isBusy)
            {
                CacheManager.SetCache("CRM:SyncDownIsBusy", true, DateTime.Now.AddMinutes(5));
                var status = ExecExchange();                
                if (status == DataManager.Success)
                {
                    status = ExecGoogle();
                    if(status != DataManager.Success)
                        result.AppendLine("Google sync failed: " + status);
                }
                else
                    result.AppendLine("Exchange sync failed: " + status);

                CacheManager.SetCache("CRM:SyncDownIsBusy", false);                
            }
            else
                result.Append("Sync Down is busy");

            if (result.Length > 0)
                return ResponseResult(result.ToString());
            else
                return Ok();
        }

        [AllowAnonymous]
        public IHttpActionResult Exchange(int id)
        {
            return ResponseResult(ExecExchange());
        }

        [AllowAnonymous]
        public IHttpActionResult Google(int id)
        {
            return ResponseResult(ExecGoogle());
        }

        private string ExecExchange()
        {
            if (ExchangeManager.IsEnabled)            
                return ExchangeManager.PullDown();
            else
                return DataManager.Success;
        }

        private string ExecGoogle()
        {
            if (GoogleManager.IsEnabled)            
                return GoogleManager.SyncPushNotificationRequests();            
            else
                return DataManager.Success;
        }

    }
}
