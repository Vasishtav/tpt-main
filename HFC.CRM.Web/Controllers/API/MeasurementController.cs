﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Managers;
using HFC.CRM.Web.Models;
using HFC.CRM.Web.Models.Measurement;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace HFC.CRM.Web.Controllers.API
{
    public class MeasurementController : BaseApiController
    {
        MeasurementManager MeasurementMgr = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        private static AccountsManager acctMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);


        int brandId = SessionManager.BrandId;
        public IHttpActionResult Get(int id)
        {
            //TODO Move all logic to the Measurement Manager
            //MeasurementHeader measHeader = new MeasurementHeader();
            // id = 1;
            var result = MeasurementMgr.Get(id);
            if (result != null)
            {
                if (brandId == 1)
                {
                    var response = result.MeasurementBB;

                    if (response.Count > 0)
                        return Json(response);
                    else
                        return OkTP();
                }
                else if (brandId == 2)
                {
                    var response = result.MeasurementTL;
                    return Json(
                        response
                    );
                }
                else if (brandId == 3)
                {
                    var response = result.MeasurementCC;
                    return Json(
                        response
                    );
                }
            }
            return OkTP();
        }

        [HttpPost]
        public IHttpActionResult Post()
        {
            //TODO move all logic to the Measurement Manager.

            var httpRequest = HttpContext.Current.Request;
            //var files = httpRequest.Files;
            var dto = httpRequest.Form["dto"];
            var postedFiles = httpRequest.Form["Files"];

            MeasurementHeader model = new MeasurementHeader();
            model = JsonConvert.DeserializeObject<MeasurementHeader>(dto);

            List<measurementfiles> m_files = new List<measurementfiles>();
            m_files = JsonConvert.DeserializeObject<List<measurementfiles>>(postedFiles);

            if (model.MeasurementBB != null)
                foreach (measurementfiles mf in m_files)
                    foreach (MeasurementLineItemBB mb in model.MeasurementBB)
                    {
                        if (mb.id == mf.id)
                        {
                            var liststr = MeasurementMgr.UploadFile(mf);
                            mb.Stream_id = liststr[0];
                            mb.thumb_Stream_id = liststr[1];
                            // mb.Stream_id = MeasurementMgr.UploadFile(mf);
                        }
                    }
            if (model.MeasurementTL != null)
                foreach (measurementfiles mf in m_files)
                    foreach (MeasurementLineItemTL mb in model.MeasurementTL)
                    {
                        if (mb.id == mf.id)
                        {
                            var liststr = MeasurementMgr.UploadFile(mf);
                            mb.Stream_id = liststr[0];
                            mb.thumb_Stream_id = liststr[1];
                           // mb.Stream_id = MeasurementMgr.UploadFile(mf);
                        }
                    }
            if (model.MeasurementCC != null)
                foreach (measurementfiles mf in m_files)
                    foreach (MeasurementLineItemCC mb in model.MeasurementCC)
                    {
                        if (mb.id == mf.id)
                        {
                            var liststr = MeasurementMgr.UploadFile(mf);
                            mb.Stream_id = liststr[0];
                            mb.thumb_Stream_id = liststr[1];
                          //  mb.Stream_id = MeasurementMgr.UploadFile(mf);
                        }
                    }
            if (model != null)
            {
                MeasurementMgr.Update(model);

            }
            var result = MeasurementMgr.Get(model.InstallationAddressId.Value);
            if (result != null)
            {
                if (brandId == 1)
                {
                    var response = result.MeasurementBB;
                    return Json(
                        response
                    );
                }
                else if (brandId == 2)
                {
                    var response = result.MeasurementTL;
                    return Json(
                        response
                    );
                }
                else if (brandId == 3)
                {
                    var response = result.MeasurementCC;
                    return Json(
                        response
                    );
                }
            }
            return OkTP();
        }


        [HttpPost]
        public IHttpActionResult SaveMeasurementsFromQuoteLineToAccount(int id, List<MeasurementLineItemBB> measurements)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var res = connection.Query<dynamic>("Select * from CRM.Opportunities Where OpportunityId=(select OpportunityId from CRM.Quote where QuoteKey=@QuoteKey)", new { QuoteKey = id }).ToList();
                    if (res.Count > 0)
                    {
                        int val = res[0].InstallationAddressId;
                        var res_mesHeader = connection.Query<MeasurementHeader>("Select * from CRM.MeasurementHeader where InstallationAddressId=@Id", new { Id = val }).FirstOrDefault();

                        if (res_mesHeader != null)
                        {
                            List<MeasurementLineItemBB> exis_measurement = JsonConvert.DeserializeObject<List<MeasurementLineItemBB>>(res_mesHeader.MeasurementSet);
                            int mea_count = exis_measurement.Count + 1;
                            foreach (var obj in measurements)
                            {
                                var internalNote = connection.Query<dynamic>("select * from CRM.QuoteLines where QuoteLineId= @QuoteLineId", new { QuoteLineId = obj.QuoteLineId }).ToList();
                                var quotee = connection.Query<Quote>("Select * from CRM.Quote where QuoteKey= @QuoteKey", new { QuoteKey = id }).FirstOrDefault();
                                if (internalNote[0].InternalNotes != null && internalNote[0].InternalNotes != "")
                                    obj.Comments = internalNote[0].InternalNotes;
                                else
                                {
                                    obj.Comments = "Added from Quote " + quotee.QuoteID.ToString();
                                }
                                // obj.Comments = internalNote[0].InternalNotes;
                                obj.id = mea_count;
                                mea_count++;
                            }
                            exis_measurement.AddRange(measurements);
                            string json = JsonConvert.SerializeObject(exis_measurement);

                            var res_query = connection.Execute("update CRM.MeasurementHeader set MeasurementSet=@MeasurementSet where Id=@Id", new { MeasurementSet = json, Id = res_mesHeader.Id });

                            var re_qq = connection.Query<dynamic>("select * from CRM.QuoteLines where QuoteKey=(select QuoteKey from CRM.QuoteLines where QuoteLineId= @QuoteLineId) and MeasurementsetId=( select max(MeasurementsetId) from CRM.QuoteLines where QuoteKey=(select QuoteKey from CRM.QuoteLines where QuoteLineId= @QuoteLineId))", new { QuoteLineId = measurements[0].QuoteLineId }).ToList();

                            int meaSetId = Convert.ToInt32(re_qq[0].MeasurementsetId);
                            foreach (var obj in measurements)
                            {

                                meaSetId++;

                                connection.Execute("update CRM.QuoteLines set MeasurementSetId=@MeasurementSetId where QuoteLineId=@QuoteLineId",
                                    new { MeasurementSetId = meaSetId, QuoteLineId = obj.QuoteLineId });
                            }
                            return Json(res[0].InstallationAddressId);
                        }
                        else
                        {
                            int qq = 1;
                            foreach (var obj in measurements)
                            {
                                var internalNote = connection.Query<dynamic>("select * from CRM.QuoteLines where QuoteLineId= @QuoteLineId", new { QuoteLineId = obj.QuoteLineId }).ToList();
                                var quotee = connection.Query<Quote>("Select * from CRM.Quote where QuoteKey= @QuoteKey", new { QuoteKey = id }).FirstOrDefault();
                                if (internalNote[0].InternalNotes != null && internalNote[0].InternalNotes != "")
                                    obj.Comments = internalNote[0].InternalNotes;
                                else
                                {
                                    obj.Comments = "Added from Quote " + quotee.QuoteID.ToString();
                                }
                                obj.id = qq;
                                qq++;
                            }

                            MeasurementHeader MH = new MeasurementHeader();
                            MH.MeasurementSet = JsonConvert.SerializeObject(measurements);
                            MH.IsDeleted = false;
                            MH.CreatedOnUtc = DateTime.UtcNow;
                            MH.CreatedByPersonId = SessionManager.CurrentUser.PersonId;
                            MH.LastUpdatedOnUtc = DateTime.UtcNow;
                            MH.LastUpdatedByPersonId = null;
                            MH.InstallationAddressId = res[0].InstallationAddressId;

                            var res_me = connection.Insert(MH);

                            connection.Execute("Update CRM.Quote set MeasurementId=@MeasurementId Where QuoteKey=@QuoteKey", new { MeasurementId = res_me, QuoteKey = id });

                            var re_qq = connection.Query<dynamic>("select * from CRM.QuoteLines where QuoteKey=(select QuoteKey from CRM.QuoteLines where QuoteLineId= @QuoteLineId) and MeasurementsetId=( select max(MeasurementsetId) from CRM.QuoteLines where QuoteKey=(select QuoteKey from CRM.QuoteLines where QuoteLineId= @QuoteLineId))", new { QuoteLineId = measurements[0].QuoteLineId }).ToList();

                            int meaSetId = Convert.ToInt32(re_qq[0].MeasurementsetId);
                            foreach (var obj in measurements)
                            {
                                meaSetId++;
                                connection.Execute("update CRM.QuoteLines set MeasurementSetId=@MeasurementSetId where QuoteLineId=@QuoteLineId",
                                    new { MeasurementSetId = meaSetId, QuoteLineId = obj.QuoteLineId });
                            }
                            return Json(res[0].InstallationAddressId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return Json(new { valid = false, error = ex.Message, data = "" });

                }
                finally
                {
                    connection.Close();
                }
            }

            return Json(false);
        }

        [HttpPost]
        public IHttpActionResult saveInternalNotesFromConfigurator(int id, string valuee)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var res = connection.Execute("Update CRM.QuoteLines set InternalNotes=@InternalNotes where QuoteLineId=@QuoteLines", new { QuoteLines = id, InternalNotes = valuee });
                    return Json(true);
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return Json(new { valid = false, error = ex.Message, data = "" });
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        [HttpGet]
        public IHttpActionResult GetOpportunityMeasurement(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var query = @"SELECT o.OpportunityId, o.OpportunityName,  o.InstallationAddressId,o.AccountId, 
	                            (select [CRM].[fnGetAccountNameByAccountId](acc.AccountId)) as AccountName
                                 ,ad.IsValidated,
	                            isnull(ad.Address1,'')+' '+isnull(ad.Address2,'')+' '+isnull(ad.City,'')+' '+isnull(ad.State,'')+' '+isnull(ad.ZipCode,'') as InstallationAddress
	                            FROM crm.Opportunities o
                                join CRM.Accounts acc on o.AccountId=acc.AccountId		
	                            join CRM.Addresses ad on ad.AddressId=o.InstallationAddressId
	                            join CRM.AccountCustomers acu on o.AccountId=acu.AccountId 
	                            join CRM.Customer cu on acu.CustomerId=cu.CustomerId                                
	                            WHERE o.opportunityid = @opportunityid and acu.IsPrimaryCustomer=1";
                    var result = connection.Query(query, new { opportunityid = id }).FirstOrDefault();
                    if (result == null)
                    {
                        return Json(new { valid = false, error = "No records found.", data = "" });
                    }

                    return Json(new { valid = true, error = "", data = result });
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return Json(new { valid = false, error = ex.Message, data = "" });
                }

                finally
                {
                    connection.Close();
                }
            }
        }

        [HttpGet]
        public IHttpActionResult InstallationAddresses(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var act = acctMgr.Get(id);
                    var query = string.Empty;

                    // Murugan: added order by, so that the list will be displayed in alphabetical order.
                    query = @"select a.AccountId, ad.AddressId as InstallAddressId,ad.IsValidated
                              ,ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name,
                             (select [CRM].[fnGetAccountNameByAccountId](a.AccountId)) as AccountName
                              from crm.accounts a
                              inner join crm.AccountAddresses aa on aa.AccountId = a.AccountId
                              inner join crm.Addresses ad on ad. AddressId = aa.AddressId
                              where a.accountId = @accountId and ad.IsDeleted not in(1)
                              order by  ad.CreatedOn";
                    var result = connection.Query(query, new { accountId = id }).ToList();
                    if (result == null)
                    {
                        return Json(new { valid = false, error = "No records found.", data = "" });
                    }

                    return Json(new { valid = true, error = "", data = result });
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return Json(new { valid = false, error = ex.Message, data = "" });
                }

                finally
                {
                    connection.Close();
                }
            }
        }
        // for job surfacing

        [HttpGet]
            public IHttpActionResult getMeasurementForJobSurfacing(int id)
        {
            try
            {
                var listStrings = new List<Tuple<string, int, string>>();
                var res = MeasurementMgr.getMeasurementForJobSurfacing(id);
                if(res != null)
                {
                    if(res.MeasurementSet != null && res.MeasurementSet != "")
                    {
                        if (brandId == 2)
                        {
                            var meaSet = JsonConvert.DeserializeObject<List<MeasurementLineItemTL>>(res.MeasurementSet);
                            foreach (var obj in meaSet)
                            {
                                if (obj.Area != null && obj.Area != "")
                                {
                                    listStrings.Add(Tuple.Create(obj.id.ToString(), obj.id, obj.Area));
                                }
                            }

                            return Json(listStrings);
                        }
                        else if (brandId == 3)
                        {
                            var meaSet = JsonConvert.DeserializeObject<List<MeasurementLineItemCC>>(res.MeasurementSet);
                            foreach (var obj in meaSet)
                            {
                                if (obj.Area != null && obj.Area != "")
                                {
                                    listStrings.Add(Tuple.Create(obj.id.ToString(), obj.id, obj.Area));
                                }
                            }

                            return Json(listStrings);
                        }
                        else
                            return Json("");
                    }

                }

                return Json("");

            }
            catch(Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { valid = false, error = ex.Message, data = "" });
            }
        }
        }
   
}
