﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using HFC.CRM.Core.Logs;
using HFC.CRM.Web.Filters;
using System.Net.Mail;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class TasksController : BaseApiController
    {
        public class TaskFilter
        {
            public List<int> jobIds { get; set; }
            public List<int> personIds { get; set; }
            public int? pageIndex { get; set; }
            public int? pageSize { get; set; }
            public DateTimeOffset? createdOnStart { get; set; }
            public DateTimeOffset? createdOnEnd { get; set; }
            public int? TaskId { get; set; }
        }

        TaskManager TaskMgr = new TaskManager(SessionManager.CurrentUser);
        EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        // GET api/tasks
        public IHttpActionResult Get([FromUri]TaskFilter filter)
        {
            try
            {
                int totalRecords = 0;
                List<TaskModel> result = null;
                if (filter != null && ((filter.personIds != null && filter.personIds.Count > 0)))
                {
                    var tasks = TaskMgr.GetTasks(out totalRecords, filter.personIds, filter.pageIndex, filter.pageSize ?? 25, filter.createdOnStart != null ? Convert.ToDateTime((DateTimeOffset)filter.createdOnStart) : DateTime.Now, filter.createdOnEnd != null ? Convert.ToDateTime((DateTimeOffset)filter.createdOnEnd) : DateTime.Now);
                    if (tasks != null)
                    {
                        result = tasks.Select(task => task.ToTaskModel()).ToList();
                    }
                }
                List<Task> result1 = null;
                if (filter.TaskId > 0)
                {
                    result1 = CRMDBContext.Tasks.ToList();
                    result1 = result1.Where(t => t.TaskId == filter.TaskId).ToList();
                    return Json(new
                    {
                        TotalRecords = totalRecords,
                        TaskData = result1
                    });
                }

                return Json(new
                {
                    TotalRecords = totalRecords,
                    TaskData = result
                });
            }
            catch (AccessViolationException ave)
            {
                return Forbidden(ave.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/tasks
        /// <summary>
        /// This is for creating new tasks
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Post([FromBody]TaskModel model)
        {
            string warning = null;

            if (model != null)
            {
                try
                {
                    int taskId = 0;
                    var task = model.ToTask(SessionManager.CurrentFranchise.FranchiseId);
                    if (task.AssignedPersonId == null)
                    {
                        task.AssignedPersonId = SessionManager.CurrentUser.PersonId;
                        task.AssignedName = SessionManager.CurrentUser.UserName;
                    }
                    //task.CreatedOnUtc = ConvertToFranchiseDateTime(DateTime.Parse(model.start.ToString()));
                    task.CreatedOnUtc = DateTime.UtcNow;
                    task.CreatedByPersonId = SessionManager.CurrentUser.PersonId;
                    task.LastUpdatedOnUtc = DateTime.UtcNow;
                    task.LastUpdatedPersonId = SessionManager.CurrentUser.PersonId;
                    task.DueDate = model.end;
                    if (TaskMgr == null) { TaskMgr = new TaskManager(SessionManager.CurrentUser); }

                    //   task.DueDate = ConvertToFranchiseDateTime(DateTime.Parse(task.DueDate.ToString()));
                    // task.DueDate = TimeZoneManager.ToUTC(DateTime.Parse(task.DueDate.Value.DateTime.ToString()));
                    var status = TaskMgr.Create(out taskId, task, out warning);
                    if (status == TaskManager.Success && taskId > 0)
                        return Json(new { Id = taskId, warning });
                    else if (status == CalendarManager.Unauthorized)
                        return Forbidden(status);
                    else
                        return BadRequest(status);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
                return BadRequest();
        }
        protected DateTime ConvertToFranchiseDateTime(DateTime date)
        {
            string FrianchiseTimeZone = SessionManager.CurrentFranchise.TimezoneCode.ToString();
            decimal franchiseOffSetTime = CacheManager.TimeZoneCollection.Where(t => t.TimeZone.Contains(FrianchiseTimeZone)).SingleOrDefault().OffSetTime;
            DateTime val = date.AddSeconds(double.Parse(franchiseOffSetTime.ToString()) * (-1));
            return val;

        }

        // PUT api/tasks/5
        /// <summary>
        /// This is for updating tasks
        /// </summary>
        /// <param name="id">Task Id</param>
        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Put(int id, [FromBody]TaskModel model)
        {
            string warning = null;
            string status = "";

            if (model != null && id > 0)
            {
                try
                {
                    var task = model.ToTask(SessionManager.CurrentFranchise.FranchiseId, id);
                    task.LastUpdatedOnUtc = DateTime.Now;
                    task.LastUpdatedPersonId = SessionManager.CurrentUser.PersonId;
                    //EventToPerson eventperson = new EventToPerson();
                    List<EventToPerson> eventperosn = new List<EventToPerson>(model.AttendeesTP.Count);
                    if (model.Attendees == null)
                    {
                        //  model.Attendees = model.AttendeesTP;
                        foreach (var attendee in model.AttendeesTP)
                        {
                            var EventToPerson = new EventToPerson
                            {
                                PersonId = attendee.OwnerID,
                                PersonName = attendee.Name,
                                TaskId = task.TaskId
                            };
                            // model.Attendees.Take(1);
                            // model.Attendees.Add(EventToPerson);
                            eventperosn.Add(EventToPerson);
                        }
                        task.Attendees = eventperosn;
                    }
                    if (TaskMgr == null) { TaskMgr = new TaskManager(SessionManager.CurrentUser); }
                    status = TaskMgr.Update(task, out warning);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
                status = "Invalid task data";

            return status == "Success"
                ? ResponseResult(status, Json(new { warning, task = TaskMgr.GetTask(id, SessionManager.CurrentFranchise.FranchiseId) }))
                : ResponseResult(status, Json(new { warning }));
        }

        // DELETE api/tasks/5

        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Delete(int? id = null)
        {
            string result = "";
            if (id.HasValue)
            {
                result = TaskMgr.DeleteTasks(id.Value);
            }
            else //remove all completed if theres no id
            {
                result = TaskMgr.ClearCompleted();
            }


            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult Mark(int id, [FromUri]bool IsComplete = false)
        {
            var status = TaskMgr.MarkComplete(id, IsComplete);

            return ResponseResult(status);
        }

        [HttpPut, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Quick(int id, [FromBody]HFC.CRM.Web.Controllers.API.CalendarController.QuickUpdate model)
        {
            var status = TaskMgr.QuickUpdate(id, model.dayDelta, model.minuteDelta);

            return ResponseResult(status);
        }
        [HttpPut, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Reminder(int id)
        {
            //var status = SendEmail(id, 2, null);

            return ResponseResult("");
        }
        [HttpPut, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult EventReminder(int id)
        {
            //var status = SendEmail(id, 2, null);

            return ResponseResult("");
        }

        [HttpPut, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult CancelEvent(int Id)
        {
            var CalendarMgr = new CalendarManager(SessionManager.CurrentUser);
            var status = "";
            if (Id > 0)
            {
                status = CalendarMgr.CancelEvent(Id, DateTime.Now);
            }
            return Json(status);
        }

        //public string SendEmail(int id, int emailType, string model = null)
        //{
        //    //To send the Lead confirmation after lead saved
        //    if (id <= 0)
        //    {
        //        return "Invalid Organizer id";
        //    }

        //    List<string> toAddresses = new List<string>();
        //    List<string> ccAddresses = null;
        //    List<string> bccAddresses = null;
        //    CustomerTP tocustomerAddress = new CustomerTP();

        //    //BUILD - From email address
        //    var fromEmailAddress = emailMgr.GetFranchiseEmail(SessionManager.CurrentFranchise.FranchiseId, emailType);// SessionManager.CurrentUser.Person.PrimaryEmail;
        //    if (string.IsNullOrEmpty(fromEmailAddress))
        //    {
        //        fromEmailAddress = SessionManager.CurrentUser.Email;
        //    }
        //    var fromAddresses = new MailAddress(fromEmailAddress, SessionManager.CurrentUser.Person.FullName);

        //    //BUILD To Email - To get Franchise email and to lead email            
        //    //if (emailType == 4 || emailType == 5) //Order - Account
        //    //{
        //    //    tocustomerAddress = emailMgr.GetAccountEmail(id);
        //    //}
        //    //else
        //    //{
        //    //    tocustomerAddress = emailMgr.GetOrganizerEmail(id);
        //    //}
        //    tocustomerAddress = emailMgr.GetOrganizerEmail(id);
        //    if (tocustomerAddress != null)
        //    {
        //        if (tocustomerAddress.PrimaryEmail.Trim() != "")
        //        {
        //            toAddresses.Add(tocustomerAddress.PrimaryEmail.Trim());
        //        }
        //        if (tocustomerAddress.SecondaryEmail != null)
        //        {
        //            if (tocustomerAddress.SecondaryEmail.Trim() != "")
        //            {
        //                toAddresses.Add(tocustomerAddress.SecondaryEmail.Trim());
        //            }
        //        }

        //    }

        //    //If no from or to emails return
        //    if (toAddresses == null || toAddresses.Count == 0 || fromEmailAddress == "")
        //    {
        //        return "No valid from or to email";
        //    }

        //    //Preparing email Body with formating
        //    string TemplateSubject = "";
        //    int FranchiseId = 0;
        //    //Email Template: 1 Thanks Email; 2 Appointment Confirmation
        //    short TemplateEmailId = (short)emailType;
        //    int BrandId = (int)SessionManager.BrandId;

        //    //Take Body Html from EmailTemplate Table
        //    if (emailMgr == null) { emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise); }
        //    string bodyHtml = emailMgr.GetHtmlBody(TemplateEmailId, BrandId, 0);

        //    TemplateSubject = bodyHtml.Substring(0, bodyHtml.IndexOf(';'));
        //    bodyHtml = bodyHtml.Substring(bodyHtml.IndexOf(';') + 1);

        //    var mailMsg = emailMgr.BuildMailMessage(
        //        id, bodyHtml, TemplateSubject
        //        , TemplateEmailId, BrandId
        //        , FranchiseId, fromEmailAddress
        //        , toAddresses, ccAddresses
        //        , bccAddresses);

        //    return "Success";

        //}
    }
}
