﻿using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class AdminCurrencyController : BaseApiController
    {
        CurrencyManager Currency_manager = new CurrencyManager();

        //get all currency value to grid code(View Grid)
        [HttpGet]
        public IHttpActionResult Currencydetails(int id)
        {
            //var result = Lookup_manager.GetCurrency(value);
            var result = Currency_manager.GetCurrencydata();
            return Json(result);
        }

        //get value for add grid code
        [HttpGet]
        public IHttpActionResult CountryCurrency(int id)
        {
            var result = Currency_manager.GetCurrencyCountry();
            return Json(result);
        }

        //Save grid value
        [HttpPost]
        public IHttpActionResult SaveCurrency(int id, [FromBody]List<CurrencyAdd> model)
        {
            if (model != null)
            {
                foreach(var item in model)
                {
                    if (item.FromType == "*") item.FromTypeValue = 1;
                    else item.FromTypeValue = 2;
                    if (item.ToType == "*") item.ToTypeValue = 1;
                    else item.ToTypeValue = 2;
                }
                var status = Currency_manager.Savedata(model);
                return ResponseResult(status);
            }
            return ResponseResult("Failed");
        }
       
    }
}
