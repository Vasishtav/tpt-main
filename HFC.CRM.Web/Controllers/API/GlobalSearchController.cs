﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using HFC.CRM.Managers.DTO;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class GlobalSearchController : BaseApiController
    {
        SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        [HttpGet, ActionName("GetLeads")]
        public IHttpActionResult GetLeads(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                List<LeadAccountDTO> result = null;
                switch (type.ToLower())
                {
                    case "email":
                    case "phone":
                    case "name":
                    case "address":
                    case "city":
                    case "all":
                        result = searchManager.GetLeads(type.ToLower(), value);
                        break;
                    default:
                        result = new List<LeadAccountDTO>();
                        break;
                }

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetAccounts")]
        public IHttpActionResult GetAccounts(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<LeadAccountDTO> result = null;
                
                switch (type.ToLower())
                {
                    case "email":
                    case "phone":
                    case "name":
                    case "address":
                    case "city":
                    case "all":
                        result = searchManager.GetAccounts(type.ToLower(), value);
                        break;
                    default:
                        result = new List<LeadAccountDTO>();
                        break;
                }

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetOpportunities")]
        public IHttpActionResult GetOpportunities(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<OpportunityDTO> result = null;
                

                switch (type.ToLower())
                {

                    case "name":
                    case "address":
                    case "city":
                    case "id":
                    case "all":
                        result = searchManager.GetOpportunities(type.ToLower(), value);
                        break;
                    default:
                        result = new List<OpportunityDTO>();
                        break;
                }

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetVendors")]
        public IHttpActionResult GetVendors(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<VendorsDTO> result = null;
                
                switch (type.ToLower())
                {
                    case "email":
                    case "phone":
                    case "address":
                    case "city":
                    case "id":
                    case "name":
                    case "all":
                        result = searchManager.GetVendors(type.ToLower(), value);
                        break;
                    default:
                        result = new List<VendorsDTO>();
                        break;
                }

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetContacts")]
        public IHttpActionResult GetContacts(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<ContactsDTO> result = null;
                
                switch (type.ToLower())
                {
                    case "email":
                    case "phone":
                    case "name":
                    case "all":
                        result = searchManager.GetContacts(type.ToLower(), value);
                        break;
                    default:
                        result = new List<ContactsDTO>();
                        break;
                }

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetAppointments")]
        public IHttpActionResult GetAppointments(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<AppointmentDTO> result = null;
                switch (type.ToLower())
                {
                  
                    case "name":
                    case "all":
                        result = searchManager.GetAppointments(type.ToLower(), value);
                        break;
                    default:
                        result = new List<AppointmentDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetTasks")]
        public IHttpActionResult GetTasks(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<TaskDTO> result = null;
                switch (type.ToLower())
                {
                    case "name":
                    case "all":
                        result = searchManager.GetTasks(type.ToLower(), value);
                        break;
                    default:
                        result = new List<TaskDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        private string UnFormat(string phone)
        {
            if (string.IsNullOrEmpty(phone)) return null;

            var result = phone.Replace("-", "")
                            .Replace("(", "")
                            .Replace(")", "")
                            .Replace(" ", "");
            return result;
        }

        [HttpGet, ActionName("GetQuotes")]
        public IHttpActionResult GetQuotes(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<QuoteDTO> result = null;
                switch (type.ToLower())
                {

                    case "name":
                    case "id":
                    case "all":
                        result = searchManager.GetQuotes(type.ToLower(), value);
                        break;
                    default:
                        result = new List<QuoteDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetOrders")]
        public IHttpActionResult GetOrders(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<OrderDTO> result = null;
                switch (type.ToLower())
                {

                    case "name":
                    case "id":
                    case "all":
                        result = searchManager.GetOrders(type.ToLower(), value);
                        break;
                    default:
                        result = new List<OrderDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetPayments")]
        public IHttpActionResult GetPayments(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<PaymentDTO> result = null;
                switch (type.ToLower())
                {
                    case "name":
                    case "id":
                    case "all":
                        result = searchManager.GetPayments(type.ToLower(), value);
                        break;
                    default:
                        result = new List<PaymentDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetNotes")]
        public IHttpActionResult GetNotes(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<NoteDTO> result = null;
                switch (type.ToLower())
                {

                    case "name":
                    case "all":
                        result = searchManager.GetNotes(type.ToLower(), value);
                        break;
                    default:
                        result = new List<NoteDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetMpoVpos")]
        public IHttpActionResult GetMpoVpos(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<MpoVpoDTO> result = null;
                switch (type.ToLower())
                {
                    case "id":
                    case "all":
                        result = searchManager.GetMposVpos(type.ToLower(), value);
                        break;
                    default:
                        result = new List<MpoVpoDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetShipments")]
        public IHttpActionResult GetShipments(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<ShipmentDTO> result = null;
                switch (type.ToLower())
                {
                    case "id":
                    case "all":
                        result = searchManager.GetShipments(type.ToLower(), value);
                        break;
                    default:
                        result = new List<ShipmentDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        [HttpGet, ActionName("GetCases")]
        public IHttpActionResult GetCases(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                IList<CaseDTO> result = null;
                switch (type.ToLower())
                {
                    case "id":
                    case "all":
                        result = searchManager.GetCases(type.ToLower(), value);
                        break;
                    default:
                        result = new List<CaseDTO>();
                        break;
                }
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetAddress")]
        public IHttpActionResult GetAddress(int id, string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                List<AddressDTO> result = null;
                switch (type.ToLower())
                {
                    case "address":
                    case "city":
                    case "all":
                    case "name":
                        result = searchManager.GetAddresses(type.ToLower(), value);
                        break;
                    default:
                        result = new List<AddressDTO>();
                        break;
                }

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
