﻿/***************************************************************************\
Module Name:  Additional Address Controller
Project: HFC-Tochpoint
Created on: 14 Dec 2017
Created By: 
Copyright:
Description: Additional Address
Change History:
Date  By  Description

\***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class AdditionalAddressController : BaseApiController
    {
        public class UrlParam
        {
            public bool includeHistory { get; set; }
            public List<int> leadIds { get; set; }
        }

        readonly AdditionalAddressManager addressManager = new AdditionalAddressManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        [HttpGet, ActionName("GetCountry")]
        public IHttpActionResult GetCountry(int id)
        {
            try
            {
                // TODO:
                var result = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct();
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetState")]
        public IHttpActionResult GetState(int id)
        {
            try
            {
                // TODO:
                var result = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 });
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        //global search
        [HttpGet]
        public IHttpActionResult GetGlobalAddresss(int id)
        {
            try
            {
                var result = addressManager.GetGlobalAddress(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetLeadAddress")]
        public IHttpActionResult GetLeadAddress(int id, bool showDeleted)
        {
            try
            {
                var result = addressManager.GetLeadAddress(id, showDeleted);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllLeadContacts(int id, bool showDeleted)
        {
            try
            {
                var result = addressManager.GetLeadContactsIncludingPrimary(id, showDeleted);

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet, ActionName("GetAccountAddress")]
        public IHttpActionResult GetAccountAddress(int id, bool showDeleted)
        {
            try
            {
                var result = addressManager.GetAccountAddress(id, showDeleted);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet]
        public IHttpActionResult GetAllAccountContacts(int id, bool showDeleted)
        {
            try
            {
                var result = addressManager.GetAccountContactsIncludingPrimary(id, showDeleted);

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        // Not used anywhere....--murugan
        //[HttpGet, ActionName("GetOpportunityAddress")]
        //public IHttpActionResult GetOpportunityAddress(int id)
        //{
        //    try
        //    {
        //        // TODO:
        //        var result = addressManager.GetLeadAddress(id);
        //        return Json(result);
        //    }
        //    catch (UnauthorizedAccessException uae)
        //    {
        //        return Forbidden(uae.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResponseResult(EventLogger.LogEvent(ex));
        //    }
        //}

        [HttpGet]
        public IHttpActionResult GetContact(int id)
        {
            try
            {
                var result = addressManager.GetContactDetails(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpPost]
        public IHttpActionResult AddNewAddress(AddressTP model)
        {
            try
            {
                var status = "Save Failed. Please try again";
                var newaddressid = 0;
                if (model != null)
                {
                    model.LeadId = (model.LeadId > 0) ? model.LeadId : null;
                    model.AccountId = (model.AccountId > 0) ? model.AccountId : null;
                    model.OpportunityId = (model.OpportunityId > 0) ? model.OpportunityId : null;
                    model.CreatedOnUtc = DateTime.Now;
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                    newaddressid = addressManager.AddAddress(model);
                }
                //return ResponseResult(status);
                return ResponseResult("Success", Json(new { newAddressId = newaddressid }));

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpPost]
        public IHttpActionResult UpdateAddress(AddressTP model)
        {

            try
            {
                var status = "Update Failed. Please try again";
                if (model != null)
                {
                    model.CreatedOnUtc = DateTime.Now;
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                    status = addressManager.Update(model); 
                }
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }




        [HttpPost]
        public IHttpActionResult Recover(int id)
        {

            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                status = addressManager.Recover(id);
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        [HttpGet, ActionName("CanbeDeleted")]
        public IHttpActionResult CanbeDeleted(int id)
        {
            try
            {
                var result = addressManager.CanbeDeleted(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {

            try
            {
                var status = "Update Failed. Please try again";
                status = addressManager.Delete(id);

                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult CheckDuplicateAddress([FromBody]AddressTP address)
        {
            var result = addressManager.GetDuplicateAddress(address);
            return Json(result);
        }
    }
}
