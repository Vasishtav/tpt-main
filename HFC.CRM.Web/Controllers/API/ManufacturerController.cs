﻿using HFC.CRM.Core.Common;
using HFC.CRM.Managers;

using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    using HFC.CRM.Data;
    using HFC.CRM.Web.Filters;

    [LogExceptionsFilter]
    public class ManufacturerController : BaseApiController
    {
        readonly ManufacturerManager manufacturerManager = new ManufacturerManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);

        public IHttpActionResult Get()
        {
            return ResponseResult("Success", Json(this.manufacturerManager.GetManufacturers()));
        }

        public IHttpActionResult Get(int id)
        {
            return ResponseResult("Success", Json(this.manufacturerManager.GetManufacturerContact(id)));
        }

        public IHttpActionResult Post(ManufacturerContact model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                status = this.manufacturerManager.SetManufacturerContact(model);
            }
            return ResponseResult(status);
        }
     }
}
