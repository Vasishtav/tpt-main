﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class QuestionController : BaseApiController
    {
        readonly FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);

        public IHttpActionResult Post(Type_Question model)
        {
            var status = "Invalid data";
            int statusId = 0;
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = FranMgr.AddQuestion(model);

                statusId = model.QuestionId;
            }
            return ResponseResult(status, Json(new { Id = statusId }));
        }

        public IHttpActionResult Put(Type_Question model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = FranMgr.UpdateQuestion(model);
            }
            return ResponseResult(status);
        }

        public IHttpActionResult Delete(int id)
        {
            var status = FranMgr.DeleteQuestion(SessionManager.CurrentFranchise.FranchiseId, id);
            return ResponseResult(status);
        }
    }
}
