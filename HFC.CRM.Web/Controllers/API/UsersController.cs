﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.Data.Context;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using HFC.CRM.Web.Properties;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Data.SqlClient;
using Dapper;
using StackExchange.Profiling.Helpers.Dapper;
using System.Text;
using HFC.CRM.DTO.Person;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class UsersController : BaseApiController
    {
        readonly FranchiseManager _franMgr = new FranchiseManager(SessionManager.SecurityUser);
        Franchise CurrentFranchise = SessionManager.CurrentFranchise;
        User CurrentUser = SessionManager.CurrentUser;
        public IHttpActionResult Get()
        {
            try
            {
                if (CurrentUser == null)
                    throw new Exception("CurrentUser is null");

                if (CurrentUser.IsAdminInRole())
                    return Ok();

                if (CurrentFranchise == null && (CurrentUser.IsInRole(AppConfigManager.AppProductStrgMang.RoleId) ||
                    CurrentUser.IsInRole(AppConfigManager.AppPicRole.RoleId)) || CurrentUser.IsInRole(AppConfigManager.AppVendorRole.RoleId))
                {
                    List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
                    return Ok(roles);
                }
                else if (CurrentFranchise == null)
                    throw new Exception("CurrentFranchise is null <br/>");

                var UserCollection = CurrentFranchise.UserCollection();
                if (UserCollection == null || UserCollection.Count == 0)
                    throw new Exception("UserCollection is null");

                // Check QBO is enabled in HFC Admin
                bool isQBEnabled = false;
                var qbapp = _franMgr.GetQbinfo();
                if (qbapp != null) isQBEnabled = qbapp.IsActive;

                List<UserCollectionDTO> users = new List<UserCollectionDTO>();
                foreach (var uc in UserCollection)
                {
                    try
                    {
                        string BGColorToRGB = "rbg(0,0,255)", FGColorToRGB = "rbg(0,0,255)";
                        UserCollectionDTO user = new UserCollectionDTO();
                        user.UserId = uc.UserId;
                        user.FullName = uc.Person.FullName;
                        user.PersonId = uc.PersonId;
                        user.ColorId = uc.ColorId;
                        user.Email = uc.Email;
                        user.AvatarSrc = uc.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                        user.EmailSignature = uc.EmailSignature;
                        user.InSales = uc.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId);
                        user.InInstall = uc.IsInRole(AppConfigManager.DefaultInstallRole.RoleId);
                        user.IsDisabled = uc.IsDisabled;
                        user.SelectColor = uc.UserId == CurrentUser.UserId ? "list-group-item active" : "list-group-item ng-binding ng-hide";
                        if (CacheManager.ColorPalettes != null)
                        {
                            if (CacheManager.ColorPalettes.Where(f => f.ColorId == uc.ColorId).FirstOrDefault() != null)
                                BGColorToRGB = CacheManager.ColorPalettes.Where(f => f.ColorId == uc.ColorId).FirstOrDefault().BGColorToRGB();
                            if (CacheManager.ColorPalettes.Where(f => f.ColorId == uc.ColorId).FirstOrDefault() != null)
                                FGColorToRGB = CacheManager.ColorPalettes.Where(f => f.ColorId == uc.ColorId).FirstOrDefault().FGColorToRGB();
                            user.color = string.Format("background-color:{1}{3};border-color:{1}{3};color:{2}{3}",
                                uc.ColorId, BGColorToRGB, FGColorToRGB, " !important");
                        }
                        else
                        {
                            user.color = string.Format(@"background-color:{1}{3};
                            border-color:{1}{3};color:{2}{3}",
                                uc.ColorId, BGColorToRGB, FGColorToRGB, " !important");
                        }

                        user.FirstName = uc.Person.FirstName;
                        user.Domain = uc.Domain;
                        user.Roles = uc.Roles;
                        users.Add(user);
                    }
                    catch (Exception ex)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Environment: " + System.Environment.MachineName);
                        sb.Append("<br /><br />");
                        sb.Append("Loggedin User: " + SessionManager.SecurityUser.UserName + " (Id: " + SessionManager.SecurityUser.UserId + ")");
                        sb.Append("<br /><br />");
                        sb.Append("Error user Usercollection: " + uc.UserName + " (Id: " + uc.UserId + ")");
                        sb.Append("<br /><br />");
                        if (SessionManager.CurrentUser != null)
                        {
                            sb.Append("Current User: " + SessionManager.CurrentUser.UserName + " (Id: " + SessionManager.CurrentUser.UserId + ")");
                            sb.Append("<br /><br />");
                        }
                        if (SessionManager.CurrentFranchise != null)
                        {
                            sb.Append("Current Franchise: " + SessionManager.CurrentFranchise.Name + " (Id: " + SessionManager.CurrentFranchise.FranchiseId + ")");
                            sb.Append("<br /><br />");
                        }
                        sb.Append("Error Datetime: " + DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss"));
                        sb.Append("<br /><br />");
                        sb.Append("Error Message: " + ex.Message.ToString());
                        sb.Append("<br /><br />");
                        sb.Append("Error StackTrace: " + ex.StackTrace.ToString());
                        sb.Append("<br /><br />");

                        ExchangeManager mgr = new ExchangeManager();
                        List<string> toAddresses = AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking") != null ? AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking").Split(',').ToList() : null;
                        if (toAddresses != null && toAddresses.Count > 0)
                            mgr.SendEmail("bbtestus@budgetblinds.com", "Touchpoint Error: Franchise User Controller", sb.ToString(), toAddresses, null, null);

                    }
                }

                if (users != null && users.Count > 0)
                {
                    return Ok(new
                    {
                        Users = users.Where(u => u.IsDisabled != true).ToList(),
                        CurrentUser = new { PersonId = CurrentUser.PersonId, user = CurrentUser },
                        DisabledUsers = users.Where(u => u.IsDisabled == true).ToList(),
                        FranchiseIsSolaTechEnabled = CurrentFranchise.isSolaTechEnabled,
                        FranchiseCountryCode = CurrentFranchise.CountryCode,
                        BrandId = CurrentFranchise.BrandId,
                        FranchiseName = CurrentFranchise.Name,
                        IsQBEnabled = isQBEnabled,
                        FranciseLevelTexting = CurrentFranchise.EnableTexting,
                        FranciseLevelCase = CurrentFranchise.EnableCase,
                        FranciseLevelVendorMng = CurrentFranchise.EnableVendorManagement,
                        AdminElectronicSign = CurrentFranchise.EnableElectronicSignAdmin,
                        FranciseLevelElectronicSign = CurrentFranchise.EnableElectronicSignFranchise,
                        CurrentUser.Person.EnableEmailSignature,
                        CurrentUser.Person.AddEmailSignAllEmails
                    });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Environment: " + System.Environment.MachineName);
                sb.Append("<br /><br />");
                sb.Append("Loggedin User: " + SessionManager.SecurityUser.UserName + " (Id: " + SessionManager.SecurityUser.UserId + ")");
                sb.Append("<br /><br />");
                if (SessionManager.CurrentUser != null)
                {
                    sb.Append("Current User: " + SessionManager.CurrentUser.UserName + " (Id: " + SessionManager.CurrentUser.UserId + ")");
                    sb.Append("<br /><br />");
                }
                if (SessionManager.CurrentFranchise != null)
                {
                    sb.Append("Current Franchise: " + SessionManager.CurrentFranchise.Name + " (Id: " + SessionManager.CurrentFranchise.FranchiseId + ")");
                    sb.Append("<br /><br />");
                }

                sb.Append("Error Datetime: " + DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss"));
                sb.Append("<br /><br />");
                sb.Append("Error Message: " + ex.Message.ToString());
                sb.Append("<br /><br />");
                sb.Append("Error StackTrace: " + ex.StackTrace.ToString());
                sb.Append("<br /><br />");

                ExchangeManager mgr = new ExchangeManager();
                List<string> toAddresses = AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking") != null ? AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking").Split(',').ToList() : null;
                if (toAddresses != null && toAddresses.Count > 0)
                    mgr.SendEmail("bbtestus@budgetblinds.com", "Touchpoint Error: Franchise User Controller", sb.ToString(), toAddresses, null, null);
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        public IHttpActionResult Get(string userid)
        {
            try
            {
                var uid = new Guid(userid);
                string[] includes = { "Person", "Address", "Roles", "ColorType" };

                var model = LocalMembership.GetUser(uid, includes);
                TimeZoneManager.ConvertToLocal(model);
                return Json(model);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Put(Guid id, [FromBody]UserModel model)
        {
            var status = "Invalid user data";

            if (id != Guid.Empty && model != null)
            {
                var usrPrimaryEmail = GetADDetails(model.UserName);
                if (usrPrimaryEmail == "")
                    return Json("Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.");

                model.Person.TimezoneCode = Convert.ToInt32(model.Person.TimezoneCode) != 0 ? model.Person.TimezoneCode : TimeZoneEnum.UTC;

                model.IsADUser = true;
                model.Person.PrimaryEmail = usrPrimaryEmail;
                FranchiseManager mgr = new FranchiseManager(CurrentUser);

                // Validation -  Check EnableEmailSignature IF false, then update AddEmailSignAllEmails aslo false
                if (model.Person != null && !model.Person.EnableEmailSignature)
                    model.Person.AddEmailSignAllEmails = false;

                //var oldUser = CacheManager.UserCollection.FirstOrDefault(u => u.UserId == id);
                var user = new User()
                {
                    UserId = id,
                    UserName = model.UserName,
                    PersonId = model.PersonId,
                    Email = usrPrimaryEmail,
                    Person = model.Person,
                    Address = model.Address,
                    FranchiseId = SessionManager.CurrentFranchise?.FranchiseId,
                    CalSync = model.CalSync,
                    IsLockedOut = SessionManager.CurrentUser.UserId != id && model.IsLockedOut,
                    IsDisabled = model.IsDisabled,
                    CalendarFirstHour = model.CalendarFirstHour,
                    OAuthUsers = model.OAuthAccounts,
                    Roles = model.RoleIds != null ? model.RoleIds.Select(s => new Role { RoleId = s }).ToList() : new List<Role>(),
                    Comment = model.Comment,
                    Domain = "bbi.corp",
                    DomainPath = @"LDAP://bbi.corp/DC=bbi,DC=corp",
                    Password = string.Empty,
                    PermissionSets = model.PermissionSetIds != null ? model.PermissionSetIds.Select(int.Parse).ToList() : null
                };


                var colorId = mgr.GetOrInsertColor(model.BGColor, model.FGColor);
                if (colorId > 0)
                {
                    user.ColorId = colorId;
                    if (!CurrentUser.IsAdminInRole() && user.Roles.All(a => a.RoleId != AppConfigManager.DefaultRole.RoleId))
                        user.Roles.Add(AppConfigManager.DefaultRole);
                    status = LocalMembership.UpdateUser(user, false, true, true);
                }
                else
                    status = "Unable to retrieve color value";


                //}
                //else
                //    status = FranchiseManager.Unauthorized;

                //}
            }

            return Json(status);
            //return ResponseResult(status);
        }
        [HttpPost]
        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Post([FromBody]UserViewModel data)
        {
            var status = "Invalid user data";
            if (data.PreferredTFN == "") data.PreferredTFN = " ";
            Guid userId = Guid.Empty;
            if (data != null)
            {
                var usrPrimaryEmail = GetADDetails(data.UserName);
                if (usrPrimaryEmail == "")
                    return Json("Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.");

                var person = new Person
                {
                    FirstName = data.FirstName,
                    LastName = data.LastName,
                    CellPhone = data.CellPhone,
                    HomePhone = data.HomePhone,
                    PreferredTFN = data.PreferredTFN,
                    PrimaryEmail = usrPrimaryEmail,
                    TimezoneCode = Convert.ToInt32(data.TimezoneCode) != 0 ? data.TimezoneCode : TimeZoneEnum.UTC
                };

                Address address = new Address();
                if (CurrentUser.IsAdminInRole())
                    address = null;
                else
                {
                    address = new Address
                    {
                        ZipCode = data.Zipcode,
                        CountryCode2Digits = data.CountryCode2Digits
                    };
                }

                var user = new User()
                {
                    CreationDateUtc = DateTime.Now,
                    UserName = data.UserName,
                    Person = person,
                    Email = person.PrimaryEmail,
                    Address = address,
                    FranchiseId = SessionManager.CurrentFranchise?.FranchiseId,
                    Domain = "bbi.corp",
                    DomainPath = @"LDAP://bbi.corp/DC=bbi,DC=corp",
                    Password = string.Empty,
                    IsApproved = true,
                    CalSync = data.CalSync,
                    Comment = data.Comments,
                    CalendarFirstHour = data.Calendar1sthr,
                    SyncStartsOnUtc = DateTime.UtcNow,
                    Roles = data.RoleIds != null ? CacheManager.RoleCollection.Where(w => data.RoleIds.Contains(w.RoleId)).ToList() : new List<Role>(),
                    PermissionSets = data.PermissionSetIds != null ? data.PermissionSetIds.Select(int.Parse).ToList() : null
                };

                var mgr = new FranchiseManager(SessionManager.CurrentUser);
                int colorId = mgr.GetOrInsertColor(data.BGColor, data.FGColor);
                if (colorId > 0)
                {
                    user.ColorId = colorId;
                    if (!CurrentUser.IsAdminInRole() && user.Roles.All(a => a.RoleId != AppConfigManager.DefaultRole.RoleId))
                        user.Roles.Add(AppConfigManager.DefaultRole);
                    status = LocalMembership.CreateUser(user, true, out userId);
                }
                else
                    status = "Unable to retrieve color value";

            }
            var result = new { status, UserId = userId };
            return Json(result);

        }
        public string GetADDetails(string userName)
        {
            string primaryEmail = "";
            string domain = null;

            if (string.IsNullOrEmpty(domain))
                domain = ExchangeManager.Domain;
            ///Changing it to use Configured EWS account rather than the hard coded value
            string adUname = AppConfigManager.GetConfig<string>("Username", "Exchange", "EWS").ToString();
            string adPassword = AppConfigManager.GetConfig<string>("Password", "Exchange", "EWS").ToString();
            using (var context = new PrincipalContext(ContextType.Domain, domain, adUname, adPassword))
            {
                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    try
                    {
                        searcher.QueryFilter.SamAccountName = userName;
                        var result = searcher.FindOne();
                        if (result != null)
                        {
                            DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                            if (de.Properties["mail"] != null && de.Properties["mail"].Value != null)
                                primaryEmail = de.Properties["mail"].Value.ToString();
                            if (de.Properties["GivenName"] != null && de.Properties["GivenName"].Value != null)
                            {
                                var gn = de.Properties["GivenName"].Value.ToString();
                            }
                            if (de.Properties["sn"] != null && de.Properties["sn"].Value != null)
                            {
                                var sn = de.Properties["sn"].Value.ToString();
                            }
                        }
                    }
                    catch (Exception ex) { ApiEventLogger.LogEvent(ex, userName); }
                }
            }
            return primaryEmail;
        }

        public string[] GetADDetail(string userName)
        {
            string primaryEmail = "", GivenName = "", Surname = "";
            string domain = null;

            if (string.IsNullOrEmpty(domain))
                domain = ExchangeManager.Domain;
            ///Changing it to use Configured EWS account rather than the hard coded value
            string adUname = AppConfigManager.GetConfig<string>("Username", "Exchange", "EWS").ToString();
            string adPassword = AppConfigManager.GetConfig<string>("Password", "Exchange", "EWS").ToString();
            using (var context = new PrincipalContext(ContextType.Domain, domain, adUname, adPassword))

            {
                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    try
                    {
                        searcher.QueryFilter.SamAccountName = userName;
                        var result = searcher.FindOne();
                        if (result != null)
                        {
                            DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                            if (de.Properties["mail"] != null && de.Properties["mail"].Value != null)
                                primaryEmail = de.Properties["mail"].Value.ToString();
                            if (de.Properties["GivenName"] != null && de.Properties["GivenName"].Value != null)
                                GivenName = de.Properties["GivenName"].Value.ToString();
                            if (de.Properties["sn"] != null && de.Properties["sn"].Value != null)
                                Surname = de.Properties["sn"].Value.ToString();
                        }
                    }
                    catch (Exception ex) { ApiEventLogger.LogEvent(ex, userName); }
                }
            }
            return new string[] { primaryEmail, GivenName, Surname };
        }

        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Delete(Guid id)
        {
            var status = "Invalid user id";
            if (id != Guid.Empty)
            {
                var mgr = new FranchiseManager(SessionManager.CurrentUser);
                //commented the line as using exsist permission code which is not needed
                //if (mgr.User_BasePermission.CanDelete)
                status = LocalMembership.DeleteUser(id);
            }

            return ResponseResult(status);
        }
        [HttpGet]
        public IHttpActionResult Enable(Guid id)
        {
            var status = "Invalid user id";
            if (id != Guid.Empty)
            {
                var franchiseid = new FranchiseManager(SessionManager.CurrentUser);
                status = LocalMembership.ActivateUser(id);

            }

            return ResponseResult(status);
        }



        [HttpGet]
        public IHttpActionResult TestApi(int id)
        {
            var x = AppConfigManager.ApplicationAdmin.UserName;
            return ResponseResult("Test passed");
        }


        [HttpGet]
        public IHttpActionResult CheckUserPermissionUsed(Guid id, string permissionName)
        {
            var str = string.Empty;
            if (id != Guid.Empty)
            {
                User aUser = null;
                aUser = ContextFactory.Current.Context.Users.FirstOrDefault(f => f.IsDeleted == false && f.UserId == id);
                if (permissionName == "Sales" && ContextFactory.Current.Context.Jobs.Any(v => v.SalesPersonId == aUser.PersonId))
                {

                    str = Resources.UserUsedPermission;
                }
                if (permissionName == "Installation" && ContextFactory.Current.Context.Jobs.Any(v => v.InstallerPersonId == aUser.PersonId))
                {
                    str = Resources.UserUsedPermission;
                }
            }
            return Json(new
            {
                str = str
            });
        }

        [HttpGet]
        public IHttpActionResult CheckUserInUsed(Guid id)
        {
            var confirmStr = Resources.DeleteConfirmSimple;
            if (id != Guid.Empty)
            {
                var mgr = new FranchiseManager(SessionManager.CurrentUser);
                // TODO: is this required in TP???
                //if (mgr.User_BasePermission.CanDelete)
                //{

                //}

                User aUser = null;
                aUser = ContextFactory.Current.Context.Users.FirstOrDefault(f => f.IsDeleted == false && f.UserId == id);

                if (ContextFactory.Current.Context.Jobs.Any(v => v.SalesPersonId == aUser.PersonId || v.InstallerPersonId == aUser.PersonId) ||
                    ContextFactory.Current.Context.Leads.Any(v => v.CustomerId == aUser.PersonId) || ContextFactory.Current.Context.Calendars.Any(v => v.AssignedPersonId == aUser.PersonId) ||
                    ContextFactory.Current.Context.EventToPeople.Any(v => v.PersonId == aUser.PersonId))
                {
                    confirmStr = Resources.DeleteUserConfirm;
                }
            }
            return Json(new
            {
                confirmStr = confirmStr
            });
        }

        [HttpGet]
        public IHttpActionResult GetPerson(int id)
        {
            var status = "Invalid person id";
            if (id != 0)
            {
                var person = ContextFactory.Current.Context.People.AsNoTracking().FirstOrDefault(v => v.PersonId == id);
                return Ok(new
                {
                    Person = person
                });
            }

            return ResponseResult(status);
        }

        [HttpPost]//, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Impersonate(Guid id)
        {
            var status = "Invalid user id";

            if (id != Guid.Empty)
            {
                // TODO : Verify whether this will not break anything - murugan
                //if (SessionManager.SecurityUser.IsInRole(AppConfigManager.DefaultOperatorRole.RoleId, AppConfigManager.AppAdminRole.RoleId))
                if (SessionManager.SecurityUser.IsAdminInRole())
                {
                    CookieManager.ImpersonateUserId = id;
                    status = IMessageConstants.Success;
                }
                else
                    status = IMessageConstants.Unauthorized;
            }


            return ResponseResult(status);
        }

        [HttpPost, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult PostBrandChange(int id)
        {
            SessionManager.BrandIdOld = id;
            SessionManager.BrandId = id;
            return Ok();
        }

        [HttpGet]
        public IHttpActionResult GetRoles(int id)
        {
            var listroles = CacheManager.RoleCollection;
            if (SessionManager.CurrentFranchise != null)
                listroles = listroles.Where(x => x.RoleType == 2).ToList();
            else
                listroles = listroles.Where(x => x.RoleType != 2).ToList();
            return Json(listroles);
        }

        [HttpGet]
        public IHttpActionResult GetWidgets(int id, string roleId = null, string userId = null)
        {
            var widgetTypes = CacheManager.WidgetTypes;

            var result = new List<object>();

            if (!string.IsNullOrEmpty(roleId))
            {
                var role = CacheManager.RoleCollection.First(r => r.RoleId == Guid.Parse(roleId));

                foreach (var widget in widgetTypes)
                {
                    result.Add(new { WidgetName = widget.Name, IsEnabled = role.IsWidgetEnabled(SessionManager.CurrentFranchise.FranchiseId, widget.Name) });
                }
            }
            else if (!string.IsNullOrEmpty(userId))
            {
                var userGuid = Guid.Parse(userId);

                var user =
                    ContextFactory.Current.Context.Users.AsNoTracking()
                        .Include(i => i.Roles.Select(s => s.UserWidgets.Select(s1 => s1.Type_Widgets)))
                        .Include(i => i.UserWidgets.Select(s => s.Type_Widgets))
                        .First(u => u.UserId == userGuid);

                foreach (var widget in widgetTypes)
                {
                    result.Add(new { WidgetName = widget.Name, IsEnabled = user.IsWidgetEnabled(SessionManager.CurrentFranchise.FranchiseId, widget.Name) });
                }
            }
            else
            {
                throw new Exception("Role or User is required");
            }

            return Ok(result);
        }

        [HttpPut]
        public IHttpActionResult UpdateWidgets(List<WidgetSetting> settings, string rId = null, string uId = null)
        {

            Guid? roleId = string.IsNullOrEmpty(rId) ? (Guid?)null : Guid.Parse(rId);
            Guid? userId = string.IsNullOrEmpty(uId) ? (Guid?)null : Guid.Parse(uId);

            using (var db = ContextFactory.Create())
            {

                var widgetTypes = db.Type_Widgets.ToList();

                var userWidgetsToDelete = roleId.HasValue
                    ? db.UserWidgets.Where(u => u.FranchiseId == SessionManager.CurrentFranchise.FranchiseId && u.RoleId == roleId).ToList()
                    : db.UserWidgets.Where(u => u.FranchiseId == SessionManager.CurrentFranchise.FranchiseId && u.UserId == userId).ToList();

                // delete all
                foreach (var toDelete in userWidgetsToDelete)
                {
                    db.UserWidgets.Remove(toDelete);
                }

                db.SaveChanges();

                // if enabled widget is default - do not add it to list
                foreach (var widgetType in widgetTypes)
                {
                    var formElement = settings.First(s => s.WidgetName == widgetType.Name);
                    if (formElement != null)
                    {
                        db.UserWidgets.Add(new UserWidget()
                        {
                            UserId = userId,
                            RoleId = roleId,
                            WidgetId = widgetType.WidgetId,
                            Show = formElement.IsEnabled,
                            FranchiseId = SessionManager.CurrentFranchise.FranchiseId

                        });
                    }

                }

                db.SaveChanges();

                // invalidate
                SessionManager.CurrentUser = null;
                SessionManager.SetRequestCache<User>("CRM_Current_User", null);
            }

            return Ok();
        }

        [HttpGet]
        public IHttpActionResult GetUsers(int id)
        {
            if (CurrentUser.IsAdminInRole())
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    string quser = @"select UserId,UserName,u.Email,IsDisabled,p.FirstName,p.LastName,p.PrimaryEmail from Auth.Users u
                                        join CRM.Person p on u.PersonId=p.PersonId
                                        where u.FranchiseId is null";
                    var usersinfo = connection.Query<UserInfoDTO>(quser).ToList();
                    connection.Close();
                    List<User> Users = new List<User>();
                    foreach (var u in usersinfo)
                    {
                        User us = new User();
                        us.UserId = u.UserId;
                        us.UserName = u.UserName;
                        us.Email = u.Email;
                        us.IsDisabled = u.IsDisabled;
                        us.Person = new Person() { FirstName = u.FirstName, LastName = u.LastName, PrimaryEmail = u.PrimaryEmail };
                        Users.Add(us);
                    }
                    return Json(Users.OrderBy(o => o.Person.FullName).ToList());
                }
            }
            else
            {
                if (SessionManager.CurrentFranchise != null)
                {
                    var owners = SessionManager.CurrentFranchise.FranchiseOwners.Select(v => v.UserId);
                    var model = LocalMembership.GetUsers(SessionManager.CurrentFranchise.FranchiseId, "Person");
                    model.RemoveAll(v => owners.Contains(v.UserId));
                    return Json(model.OrderBy(o => o.Person.FullName).ToList());
                }
                else
                {
                    return Json("");
                }

            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult CreateVendorUser(string username)
        {
            var status = "Invalid user data";

            Guid userId = Guid.Empty;
            if (username != null && username != "")
            {
                var ADDetail = GetADDetail(username);
                if (ADDetail != null && ADDetail[0] == "")
                    return Json("Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.");

                var person = new Person
                {
                    FirstName = ADDetail[1],
                    LastName = ADDetail[2],
                    CellPhone = "",
                    HomePhone = "",
                    PreferredTFN = "",
                    PrimaryEmail = ADDetail[0]
                };

                var RoleIds = new List<Guid>() { AppConfigManager.AppVendorRole.RoleId };

                int ColorId = 0;
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    string qgetcolorid = @"select top 1 tc.ColorId from Auth.Users u
                                        right join [CRM].[Type_Colors] tc on u.ColorId=tc.ColorId
                                        where u.FranchiseId is null and u.UserId is null";
                    ColorId = connection.Query<int>(qgetcolorid).FirstOrDefault();
                    connection.Close();
                }

                var user = new User()
                {
                    CreationDateUtc = DateTime.UtcNow,
                    UserName = username,
                    Person = person,
                    Email = ADDetail[0],
                    Address = null,
                    FranchiseId = null,
                    IsApproved = true,
                    CalSync = false,
                    Comment = "",
                    CalendarFirstHour = 8,
                    Domain = "bbi.corp",
                    DomainPath = @"LDAP://bbi.corp/DC=bbi,DC=corp",
                    Password = string.Empty,
                    ColorId = ColorId,
                    Roles = RoleIds != null ? CacheManager.RoleCollection.Where(w => RoleIds.Contains(w.RoleId)).ToList() : new List<Role>(),
                    PermissionSets = null
                };

                status = LocalMembership.CreateUser(user, true, out userId);

            }
            var result = new { status, UserId = userId };
            return Json(result);

        }

        [HttpGet]
        public IHttpActionResult CreateAdminUser(string username)
        {
            var status = "Invalid user data";

            Guid userId = Guid.Empty;
            if (username != null && username != "")
            {
                var ADDetail = GetADDetail(username);
                if (ADDetail != null && ADDetail[0] == "")
                    return Json("Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.");

                var person = new Person
                {
                    FirstName = ADDetail[1],
                    LastName = ADDetail[2],
                    CellPhone = "",
                    HomePhone = "",
                    PreferredTFN = "",
                    PrimaryEmail = ADDetail[0]
                };

                var RoleIds = new List<Guid>() { AppConfigManager.AppAdminRole.RoleId };

                int ColorId = 0;
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    string qgetcolorid = @"select top 1 tc.ColorId from Auth.Users u
                                        right join [CRM].[Type_Colors] tc on u.ColorId=tc.ColorId
                                        where u.FranchiseId is null and u.UserId is null";
                    ColorId = connection.Query<int>(qgetcolorid).FirstOrDefault();
                    connection.Close();
                }

                var user = new User()
                {
                    CreationDateUtc = DateTime.UtcNow,
                    UserName = username,
                    Person = person,
                    Email = ADDetail[0],
                    Address = null,
                    FranchiseId = null,
                    IsApproved = true,
                    CalSync = false,
                    Comment = "",
                    CalendarFirstHour = 8,
                    Domain = "bbi.corp",
                    DomainPath = @"LDAP://bbi.corp/DC=bbi,DC=corp",
                    Password = string.Empty,
                    ColorId = ColorId,
                    Roles = RoleIds != null ? CacheManager.RoleCollection.Where(w => RoleIds.Contains(w.RoleId)).ToList() : new List<Role>(),
                    PermissionSets = null
                };

                status = LocalMembership.CreateUser(user, true, out userId);

            }
            var result = new { status, UserId = userId };
            return Json(result);

        }

        public class WidgetSetting
        {
            public string WidgetName { get; set; }

            public bool IsEnabled { get; set; }
        }


        public class UserViewModel
        {
            public bool CalSync { get; set; }
            public string UserName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string HomePhone { get; set; }
            public string CellPhone { get; set; }
            public string PreferredTFN { get; set; }
            public string Zipcode { get; set; }
            public string CountryCode2Digits { get; set; }
            public int Calendar1sthr { get; set; }
            public string Comments { get; set; }
            public string BGColor { get; set; }
            public string FGColor { get; set; }
            public List<Guid> RoleIds { get; set; }
            public List<string> PermissionSetIds { get; set; }
            public HFC.CRM.Data.TimeZoneEnum TimezoneCode { get; set; }

        }

    }
}
