﻿using HFC.CRM.Core;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMatrix.WebData;

namespace HFC.CRM.Web.Controllers.API
{
    [AllowAnonymous]
    public class TouchpointConnectController : BaseApiController
    {
        //Event Calendar List
        [HttpGet]
        public IHttpActionResult EventCalendarList(int id)
        {
            try
            {
                var responce = new Response(true, "test");
                return Ok<Response>(responce);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult Login([FromBody]Login model)
        {
            string fullUsername = model.UserName;

            var isAdUser = LocalMembership.GetUser(fullUsername, "Person", "Roles", "OAuthUsers");
            if (!string.IsNullOrEmpty(isAdUser.Domain))
            {
                if (model.UserName.Contains("@budgetblinds.com") || model.UserName.Contains("@tailoredliving.com") || model.UserName.Contains("@homefranchiseconcepts.com"))
                {
                    fullUsername = fullUsername.Replace("@budgetblinds.com", string.Empty);
                    fullUsername = fullUsername.Replace("@tailoredliving.com", string.Empty);
                    fullUsername = fullUsername.Replace("@homefranchiseconcepts.com", string.Empty);
                }
            }

            if (ModelState.IsValid)
            {
                if (!string.Equals(fullUsername, "admin", StringComparison.InvariantCultureIgnoreCase) && fullUsername.IndexOf('@') <= 0
                    && !string.IsNullOrEmpty(ExchangeManager.Domain))
                {
                    fullUsername = string.Format("{0}@{1}", fullUsername, ExchangeManager.Domain);
                }

                if (WebSecurity.Login(fullUsername, model.Password))
                {
                    //var does=WebSecurity.UserExists(FullUsername);
                    //we can't use session at this point since the user principal is not yet created so we have to pull the data from DB manually
                    var aUser = LocalMembership.GetUser(fullUsername, "Person", "Roles", "OAuthUsers");

                    if (!aUser.IsInRole(AppConfigManager.AppAdminRole.RoleId))
                    {
                        if (aUser.FranchiseId.HasValue)
                        {
                            var frnManager = new FranchiseManager(aUser);
                            var isSuspended = frnManager.IsSuspended(aUser.FranchiseId.Value);
                            if (isSuspended)
                            {
                                return Ok<User>(aUser);
                            }
                        }
                    }
                    if (string.Equals(aUser.UserName, "pic@bbi.corp", StringComparison.OrdinalIgnoreCase))
                    {
                        return Ok<User>(aUser);
                    }
                    if (aUser.IsInRole(AppConfigManager.AppAdminRole.RoleId)) return Ok<User>(aUser);
                    else return Ok<User>(aUser);
                }
            }

            // If we got this far, something failed, redisplay form
            return OkTP();
        }
    }

    public class Response
    {
        public bool status;
        public string message;
        public object content;

        public Response(bool status, string message, object content)
        {
            this.status = status;
            this.message = message;
            this.content = content;
        }

        public Response(bool status, string message)
        {
            this.status = status;
            this.message = message;
            this.content = null;
        }
    }

    public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}