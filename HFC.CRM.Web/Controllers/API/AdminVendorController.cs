﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Vendors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class AdminVendorController : BaseApiController
    {
        SessionManager session = new SessionManager();
        // Franchise level
        VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        // HFC admin level
        VendorManager VendorMngrHFC = new VendorManager(SessionManager.SecurityUser, null);

        //to get value at initial load(alliance vendor)
        [HttpGet]
        public IHttpActionResult GetAllianceVendor(int id)
        {

            var result = VendorMngr.GetAlliance(id);
            return Json(result);
        }
        //when vendor type is selected from dropdown
        [HttpGet]
        public IHttpActionResult AdminVendorOption(int id)
        {
            var result = VendorMngr.GetSelectedVendor(id);
            return Json(result);
        }
        //to fetch the status drop down
        [HttpGet]
        public IHttpActionResult GetVendorDropdownStatus(int id)
        {
            var result = VendorMngr.GetStatusDropdown();
            return Json(result);
        }
        //call for Save on add/edit data 
        [HttpPost]
        public IHttpActionResult SaveAdminVendor(int id, [FromBody]AdminAllianceModel model)
        {
            if (model != null && model.Name != null && model.Name != "")
            {
                if (model.VendorIdPk == 0 && model.CreatedBy == 0)
                {
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                }
                if (model.VendorIdPk == 0 && model.VendorTypeName == "Alliance Vendor")
                    id = 0;
                else if (model.VendorIdPk == 0 && model.VendorTypeName != "Alliance Vendor")
                    id = 1;
                if (model.VendorIdPk != 0 && model.VendorTypeName == "Alliance Vendor")
                    model.VendorType = 1;
                else if (model.VendorIdPk != 0 && model.VendorTypeName != "Alliance Vendor")
                    model.VendorType = 2;

                var status = VendorMngr.SaveAllianceVendor(id, model);
                //return Json(status);
                return ResponseResult(status);
            }
            return ResponseResult("Failed");
        }

        public IHttpActionResult GetVendorCaseList()
        {
            var result = VendorMngr.GetVendorCaseConfigList();

            if (result != null)
            {
                return Json(result);
            }
            return OkTP();
        }

        [HttpPost]
        public IHttpActionResult UpdateVendorCase(int id, [FromBody]VendorCaseConfig model)
        {
            try
            {
                if (VendorMngrHFC.CheckUserBelongsToFe(model.CaseLogin) != false)
                {
                    return ResponseResult("This is Franchise User. Please contact HFC Tech Support if the problem persists.");
                }
                else if (VendorMngrHFC.CheckUserBelongsToVendor(model.CaseLogin, model.VendorId) != false)
                {
                    return ResponseResult("This user already belongs to another vendor. Please contact HFC Tech Support if the problem persists.");
                }
                else
                {
                    
                    var usrPrimaryEmail = VendorMngrHFC.GetADDetails(model.CaseLogin);
                    if (usrPrimaryEmail == "")
                        return ResponseResult("Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.");

                    if (!VendorMngrHFC.CheckUserExist(model.CaseLogin))
                        new UsersController().CreateVendorUser(model.CaseLogin);

                    var result = VendorMngrHFC.UpdateVendorCaseConfig(model);
                    return ResponseResult(result);
                }


            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

    }
}
