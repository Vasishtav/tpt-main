﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Source;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class SourceController : BaseApiController
    {
        SourceManager sourcemgr = new SourceManager();

        //get all source to Tp-Source
        [HttpGet]
        public IHttpActionResult GetSourcesTP()
        {
            try
            {
                var result = sourcemgr.GetSourcesTP();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        class globaldoc
        {
            public  int Id { get; set; }
            public string Title { get; set;}
            public string Name { get; set; }
            public int Category { get; set; }
            public int Concept { get; set; }
            public string streamId { get; set; }
            public bool IsEnabled { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedOn { get; set; }
            public string CategoryTitle { get; set; }
            public string ConceptTitle { get; set; }
        }
      
        [HttpGet]
        public IHttpActionResult GetGlobaldocList()
        {
            
            try
            {
               List<globaldoc> gd = new List<globaldoc>();
               var result = sourcemgr.GetGlobaldocList();

                foreach (var q in result)
                {
                    globaldoc gdoc = new globaldoc();
                    gdoc.Id = q.Id;
                    gdoc.Title = q.Title;
                    gdoc.Name = q.Name;
                    gdoc.Category = q.Category;
                    gdoc.Concept = q.Concept;
                    gdoc.streamId = q.streamId;
                    gdoc.IsEnabled = q.IsEnabled;
                    gdoc.CreatedOn = q.CreatedOn.ToString("MM'/'dd'/'yyyy");


                    if (q.Concept == 1)
                        gdoc.ConceptTitle = "Budget Blinds";
                    else if (q.Concept == 2)
                        gdoc.ConceptTitle = "Tailored Living";
                    else if (q.Concept == 3)
                        gdoc.ConceptTitle = "Concrete Crafts";

                    if (q.Category == 1)
                        gdoc.CategoryTitle = "Sales";
                    else if (q.Category == 2)
                        gdoc.CategoryTitle = "Install";
                    else if (q.Category == 3)
                        gdoc.CategoryTitle = "Product Guides";
                    else if (q.Category == 4)
                        gdoc.CategoryTitle = "Help";
                    //  gdoc.Id = q.Id;
                    gd.Add(gdoc);

                }
                return Json(gd);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        //get all source to Tp-Source including in-active data
        [HttpGet]
        public IHttpActionResult GetInactiveSourcesTP()
        {
            try
            {
                var result = sourcemgr.GetInactiveSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get owner list data for Tp-Source
        [HttpGet]
        public IHttpActionResult GetOwnerSources()
        {
            try
            {
                var result = sourcemgr.GetOwnerSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get channel list for Tp-Source
        [HttpGet]
        public IHttpActionResult GetChannelSources()
        {
            try
            {
                var result = sourcemgr.GetChannelSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get source list for Tp-Source
        [HttpGet]
        public IHttpActionResult GetSource()
        {
            try
            {
                var result = sourcemgr.GetSource();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetSourcesHFC()
        {
            try
            {
                var result = sourcemgr.GetSourcesHFC();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get all data to HFC-Source
        [HttpGet]
        public IHttpActionResult GetSourcesHFCmapTP()
        {
            try
            {
                var result = sourcemgr.GetSourcesHFCmapTP();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get all data including in-active data to HFC-Source
        [HttpGet]
        public IHttpActionResult GetInactiveHFCSources()
        {
            try
            {
                var result = sourcemgr.GetInactiveSourcesHFCmapTP();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get TPTSource List for HFC-Source
        [HttpGet]
        public IHttpActionResult GetTPTSource()
        {
            try
            {
                var result = sourcemgr.GetTPTSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get owner list data for HFC-Source
        [HttpGet]
        public IHttpActionResult GetTPTOwnerSources()
        {
            try
            {
                var result = sourcemgr.GetTPTOwnerSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get channel list for HFC-Source
        [HttpGet]
        public IHttpActionResult GetTPTChannelSources()
        {
            try
            {
                var result = sourcemgr.GetTPTChannelSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        //get source list for HFC-Source
        [HttpGet]
        public IHttpActionResult GetTPTSourceList()
        {
            try
            {
                var result = sourcemgr.GetTPTListSource();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        /// <summary>
        /// To Add or update SourcesHFC
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult PostSourcesHFC(SourcesHFC model)
        {

            try
            {
                var result = "";
                if (model!= null)
                {
                    
                    if (model.SourcesHFCId > 0)
                    {
                        result = sourcemgr.UpdateSourceHFC(model);
                    }
                    else
                        result = sourcemgr.AddSourceHFC(model);
                }
                //return Json(result);
                return ResponseResult(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// To Add or update SourcesTP
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult PostSourcesTP(SourcesTP model)
        {

            try
            {
                if (model!=null)
                {
                    if (model.SourcesTPId > 0)
                    {
                        sourcemgr.UpdateSourceTP(model);
                    }
                    else
                        sourcemgr.AddSourceTP(model);
                }
                return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult PostSources([FromBody]List<SourceDTO> model)
        {

            try
            {
                var result = "";
                foreach (var m in model)
                {
                    if (m.SourceId > 0)
                    {
                        result = sourcemgr.UpdateSource(m);
                        if(result== "Source Name Exists Already")
                        {
                            return Json(result);
                        }
                    }
                    else
                        result=sourcemgr.AddSource(m);
                }
                return Json(result);
                //return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult PostTypeChannel([FromBody]List<Type_Channel> model)
        {

            try
            {
                var result = "";
                foreach (var m in model)
                {
                    if (m.ChannelId > 0)
                    {
                         result = sourcemgr.UpdateType_Channel(m);
                        if (result == "Channel Name Exists Already")
                        {
                            return Json(result);
                        }
                    }
                    else
                     result = sourcemgr.AddType_Channel(m);
                }
                return Json(result);
                //return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult PostTypeOwner([FromBody]List<Type_Owner> model)
        {

            try
            {
                foreach (var m in model)
                {
                    if (m.OwnerId > 0)
                    {
                        sourcemgr.UpdateType_Owner(m);
                    }
                    else
                        sourcemgr.AddType_Owner(m);
                }
                return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetLeadSourceMapping(int id)
       {
            try
            {
                var result = sourcemgr.GetLeadSourcesmapTP(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        [HttpPost]
        public IHttpActionResult PostUTMCampaign(UTMCampaign model)
        {
            try
            {
                if (model != null)
                {
                    var result = sourcemgr.SaveUtmCampgn(model);
                    return ResponseResult(result);
                }
                else
                    return ResponseResult("Failed");
            }
            catch(Exception exp)
            {
                EventLogger.LogEvent(exp);
                return ResponseResult(exp);
            }
            
        }
        [HttpGet]
        public IHttpActionResult GetUTMMapping(int id)
        {
            try
            {
                var result = sourcemgr.GetUTMSourcesmap(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult SourceLeadType()
        {
            List<sourceLeadType> response = new List<sourceLeadType>();
            response.Add(new sourceLeadType
            {
                Key = "In Home Consultation",
                Value = "In Home Consultation"
            });
            response.Add(new sourceLeadType
            {
                Key = "Commercial",
                Value = "Commercial"
            });
            response.Add(new sourceLeadType
            {
                Key = "Design Guide",
                Value = "Design Guide"
            });
            response.Add(new sourceLeadType
            {
                Key = "Swatch",
                Value = "Swatch"
            });
            
            return Json(response);
        }
    }
}
