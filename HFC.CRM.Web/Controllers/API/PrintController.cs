﻿using HFC.CRM.Core.Common;
using HFC.CRM.Managers;
using HFC.CRM.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using System.Linq;
using System.Net.Mail;
using HFC.CRM.Web.Controllers;
using System.Threading;

namespace HFC.CRM.Web.Controllers.API
{
    using Core.Logs;
    using DocumentFormat.OpenXml.Wordprocessing;
    using HFC.CRM.Managers.AdvancedEmailManager;
    using HFC.CRM.Web.Filters;
    using System.Web.Mvc;
    /// <summary>
    /// API controller for handling calendar ajax 
    /// </summary>
    [LogExceptionsFilter]
    public class PrintController : BaseApiController
    {
        PrintManager printMgr = new PrintManager(SessionManager.CurrentUser);

        [HttpGet, ActionName("GetMydocumentsForSale")]
        public IHttpActionResult GetMydocumentsForSale(int id)
        {
            try
            {
                var result = printMgr.GetMydocumentsForSales();

                // The UI want the quantity by default as 1.
                foreach (var item in result)
                {
                    item.Quantity = 1;
                }

                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetMydocumentsForInstall")]
        public IHttpActionResult GetMydocumentsForInstall(int id)
        {
            try
            {
                var result = printMgr.GetMydocumentsForInstall();

                // The UI want the quantity by default as 1.
                foreach (var item in result)
                {
                    item.Quantity = 1;
                }

                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

    }
}
