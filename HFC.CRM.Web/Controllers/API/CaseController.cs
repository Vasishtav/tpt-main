﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class CaseController : BaseApiController
    {
        CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        #region Feedback        

        //To save the feedback
        [HttpPost]
        public IHttpActionResult Post([FromBody] CaseFeedbackDetails model)
        {
            try
            {
                var result = Case_Mngr.Save(model);
                //Case_Mngr.UpdateFeedbackHistory(result.CaseId, model);
                return ResponseResult("Success", Json(new { result }));
                //return ResponseResult("Failed");
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }      

        [HttpGet]
        public IHttpActionResult GetConfiguratorFeedbackById(int Id,bool IsVendor)
        {
            try {
                var result = Case_Mngr.GetConfiguratorFeedbackById(Id, IsVendor);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        //feedback list for pic/HFC PM
        public IHttpActionResult GetFeedbackList(bool IncludeClosed)
        {
            try { 
            var result = Case_Mngr.GetFeedbackList(IncludeClosed);
            return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        //feedback list for vendor
        public IHttpActionResult GetFeedbackListByVendor(bool IncludeClosed)
        {
            try { 
            var result = Case_Mngr.GetFeedbackListByVendor(IncludeClosed);
            return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        //feedback list for franchise 
        public IHttpActionResult GetFeedbackListByFranchise(bool IncludeClosed)
        {
            try
            {
                var result = Case_Mngr.GetFeedbackListByFranchise(IncludeClosed);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        #endregion

        #region ChangeRequest

        [HttpPost]
        public IHttpActionResult SaveChangeRequest(int id, [FromBody] CaseChangeDetails model)
        {
            try
            {               
                var result = Case_Mngr.SaveChangeRequest(model);                
                return ResponseResult("Success", Json(new { result }));
                //return ResponseResult("Failed");
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetChangeRequestById(int Id)
        {
            try { 
            var result = Case_Mngr.GetChangeRequestById(Id);
            return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetChangeRequestCaseNumberByVendor(int Id)
        {
            try {
                var result = Case_Mngr.GetChangeRequestCaseNumberByVendor(Id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        //change request list for vendors
        public IHttpActionResult GetChangeRequestListByVendor(bool IncludeClosed)
        {
            try { 
            var result = Case_Mngr.GetChangeRequestListByVendor(IncludeClosed);
            return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        //change request list for PIC/HFC PM
        public IHttpActionResult GetChangeRequestList(bool IsPIC,bool IncludeClosed)
        {
            try { 
            var result = Case_Mngr.GetChangeRequestList(IsPIC, IncludeClosed);
            return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        #endregion

        #region Overview
        [HttpGet]
        //overview list for franchise
        public IHttpActionResult GetOverviewListByFranchise(bool IncludeClosed)
        {
            try
            {
                var result = Case_Mngr.GetOverviewListByFranchise(IncludeClosed);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetOverviewOfCaseFeedbackChangeListByVendor(bool IncludeClosed)
        {
            try
            {
                var result = Case_Mngr.GetOverviewOfCaseFeedbackChangeListByVendor(IncludeClosed);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetOverviewOfCaseFeedbackChangeList(bool IsPIC, bool IncludeClosed)
        {
            try { 
            var result = Case_Mngr.GetOverviewOfCaseFeedbackChangeList(IsPIC, IncludeClosed);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        #endregion

        [HttpGet]
        //change request/feedback lookup list by table name
             //   Table
        	    //FeedbackStatus
        	    //ChangeRequestStatus
        	    //ClosedReason
        	    //ErrorType
        	    //RequestType
        public IHttpActionResult GetFeedbackChangeLookup(string TableName)
        {
            try { 
            var result = Case_Mngr.GetFeedbackChangeLookup(TableName);
            return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetCaseHistoryForCase(int id)
        {
            try
            {
                var res = Case_Mngr.GetCaseHistoryForCase(id);

                foreach (HistoryTable ht in res)
                {
                    ht.Date = TimeZoneManager.ToLocal(ht.Date);
                    ht.LastUpdatedOn = TimeZoneManager.ToLocal(ht.LastUpdatedOn);
                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }
        [HttpGet]
        public IHttpActionResult getCaseAttachmentsList(int Id,bool flag)
        {
            try
            {
                var res = Case_Mngr.getCaseAttachmentsList(Id);

                if (flag == true)
                {
                    var tz = Case_Mngr.getFranchiseTimeZone(Id).TimezoneCode;
                    foreach (var r in res)
                    {
                        if (r.CreatedOn == null)
                        {
                            r.FileDetails = r.FileDetails.Insert(10, " " + tz.ToString() + " ");
                        }
                        else
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)r.CreatedOn, tz);
                            r.FileDetails = r.FileDetails.Substring(10, r.FileDetails.Length - 10);
                            r.FileDetails = lt.Date.GlobalDateFormat() + " " + tz.ToString() + r.FileDetails;
                        }
                        //  str.Substring(10, str.Length - 10)



                    }
                }
                else
                {
                    var tz = Case_Mngr.getFranchiseTimeZone(Id).TimezoneCode;
                    foreach (var r in res)
                    {
                        if (r.CreatedOn != null)
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)r.CreatedOn, tz);
                            r.FileDetails = r.FileDetails.Substring(10, r.FileDetails.Length - 10);
                            r.FileDetails = lt.Date.GlobalDateFormat() + r.FileDetails;
                        }
                        //  str.Substring(10, str.Length - 10)



                    }

                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

            //  return null;
        }

        [HttpPost]
        public IHttpActionResult removeCaseAttachmentsDetail(int Id)
        {
            try
            {
                var res = Case_Mngr.removeCaseAttachmentsDetail(Id);
                return Json(true);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        [HttpPost]
        public IHttpActionResult removeCaseAttachmentsDetails(int Id, int CaseId)
        {
            try
            {
                var res = Case_Mngr.removeCaseAttachmentsDetails(Id, CaseId);
                return Json(true);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        [HttpPost]
        public IHttpActionResult GetTPVersion(int id)
        {
            var result = "";
            //if (!@AppConfigManager.SName.ToLower().Contains("prd") && !@AppConfigManager.SName.ToLower().Contains("pqa"))
            //{
            result = "Touchpoint Version " + @AppConfigManager.AppVersion; //+ " " + @AppConfigManager.SName.ToUpper();
                return Json(result);
            //}
            //return OkTP();
        }
    }
}