﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class FranchiseSettingsController : BaseApiController
    {
        FranchiseManager _franMgr = new FranchiseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        public IHttpActionResult GetEmailsConfiguration()
        {
            var result = emailMgr.GetConfiguredEmails();
            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult UpdateEmailsConfiguration(int id,int DisplayTerritoryId,List<EmailSettings> lstEmailSettings)
        {
            var result = emailMgr.UpdateConfiguredEmails(DisplayTerritoryId,lstEmailSettings);
            return Json(result);
        }

        public IHttpActionResult GetDisplayTerritoryId()
        {
            var result = emailMgr.GetDisplayTerritoryId();
            return Json(result);
        }

        /// <summary>
        /// Get BuyerRemorse value in franchise table
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult BuyerRemorse(int id)
        {
            var value = _franMgr.GetRemorseData();
            return Json(value);
        }

        /// <summary>
        /// Update BuyerRemorse value in franchise table
        /// </summary>
        /// <param name="id"></param>
        /// <param name="BuyerremorseDay"></param>
        /// <param name="EnableElectSign"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult SaveBuyerRemose(int id, int BuyerremorseDay, bool EnableElectSign)
        {
            try
            {
                var result = _franMgr.SaveRemorse(SessionManager.CurrentFranchise.FranchiseId, BuyerremorseDay, id, EnableElectSign);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        //[HttpGet]
        //public IHttpActionResult ResendEmail(int id)
        //{
        //    //var value = _franMgr.ResendEmail(id);
        //    var value = _franMgr.ResendEmailNew(id);
        //    return Json(value);
        //}
    }
}
