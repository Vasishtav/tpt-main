﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;

using HFC.CRM.DTO.Address;
using HFC.CRM.DTO.Notes;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using Newtonsoft.Json;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class CommentsController : BaseApiController
    {

        [HttpPost]
        public IHttpActionResult SaveParentComment(int id, [FromUri] string unique, int module, int moduleId, Comments Comments)
        {
            try
            {
                // var result = Comments;
                // string val = Comments.module;
                string[] emailids = new string[] { };
                if (unique != null)
                    emailids = unique.Split(',');

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "";
                    int VendorId = 0;
                    if (module == 1)
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted,VendorId,belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                    //  VendorId = 0;


                    if (module == 2)
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted,VendorId,belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                    // VendorId = RefId;

                    if (module == 3 || module == 4)
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted,VendorId,belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                    // VendorId = RefId;

                    int UserPersonId = 0;
                    string UserName = "";

                    if (SessionManager.CurrentUser.Person != null)
                    {
                        UserPersonId = SessionManager.CurrentUser.Person.PersonId;
                        UserName = SessionManager.CurrentUser.Person.FirstName + " " + SessionManager.CurrentUser.Person.LastName;
                    }
                    else if (SessionManager.SecurityUser.Person != null)
                    {
                        UserPersonId = SessionManager.SecurityUser.Person.PersonId;
                        UserName = UserName = SessionManager.SecurityUser.Person.FirstName + " " + SessionManager.SecurityUser.Person.LastName;
                    }

                    var rrr = connection.Execute(query, new
                    {
                        module = module,
                        moduleId = moduleId,
                        userId = UserPersonId,
                        parentId = Comments.parentId,
                        haschildId = Comments.haschildId,
                        userImageUrl = Comments.userImageUrl,
                        userName = UserName,
                        htmlComment = Comments.htmlComment,
                        htmlCurrentComment = Comments.htmlComment,
                        belongsTo = Comments.belongsTo,
                        isReply = Comments.isReply,
                        isEdit = Comments.isReply,
                        createdAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        isDeleted = false,
                        VendorId = VendorId
                    });

                    // var res = new List<Comments>();




                    if (module == 1)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "New", emailids);
                        var res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted)", new { module = module, moduleId = moduleId, isDeleted = false }).ToList();
                        res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);
                        connection.Close();
                        return Json(res[0]);

                    }
                    else if (module == 2)
                    {

                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "New", emailids);
                        var res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted)", new { module = module, moduleId = moduleId, isDeleted = false }).ToList();
                        res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);
                        connection.Close();
                        return Json(res[0]);

                    }
                    else if (module == 3)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "New", emailids);
                        var res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted)", new { module = module, moduleId = moduleId, isDeleted = false }).ToList();
                        // res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        // res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);

                        var queryy = @"select f.* from CRM.VendorCase vc
                                     join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where fc.CaseId  = @CaseId ";

                        var ress = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();

                        if (res[0] != null) res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt, (TimeZoneEnum)ress.TimezoneCode);
                        if (res[0] != null) res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt, (TimeZoneEnum)ress.TimezoneCode);
                        // if (ress[0] != null) ress[0].DateTimeClosed = TimeZoneManager.ToLocal(ress[0].DateTimeClosed, (TimeZoneEnum)res.TimezoneCode);

                        connection.Close();
                        return Json(res[0]);
                    }
                    else
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "New", emailids);
                        var res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted)", new { module = module, moduleId = moduleId, isDeleted = false }).ToList();

                        //             var queryy = @"select f.* from CRM.VendorCase vc
                        //                          join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
                        //join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
                        //join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
                        //join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
                        //join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                        //                          join CRM.Orders o on o.QuoteKey=q.QuoteKey
                        //                          join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                        //                          join CRM.Franchise f on f.FranchiseId=op.FranchiseId
                        //                       where fc.CaseId  = @CaseId ";
                        var queryy = @"select Fr.* from CRM.Franchise Fr
									   join CRM.Opportunities op on op.FranchiseId= fr.FranchiseId
										join crm.Orders o on o.OpportunityId= op.OpportunityId
										join crm.FranchiseCase fc on fc.SalesOrderId= o.OrderID
										where fc.CaseId=@CaseId";

                        var ress = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();

                        if (res[0] != null) res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt, (TimeZoneEnum)ress.TimezoneCode);
                        if (res[0] != null) res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt, (TimeZoneEnum)ress.TimezoneCode);
                        // if (ress[0] != null) ress[0].DateTimeClosed = TimeZoneManager.ToLocal(ress[0].DateTimeClosed, (TimeZoneEnum)res.TimezoneCode);

                        connection.Close();
                        return Json(res[0]);
                    }

                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

            //  return null;

        }

        [HttpPost]
        public IHttpActionResult SaveReplyComment(int id, [FromUri] string unique, int module, int moduleId, Comments Comments)
        {
            try
            {
                //var result = Comments;
                //string val = Comments.module;
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    int VendorId = 0;
                    var query = "";

                    var parentobj = connection.Query<Comments>("Select * from CRM.Comments where id= @id", new { id = Comments.parentId }).FirstOrDefault();
                    if (module == 1)
                    {
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted, VendorId, belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                        // VendorId = 0;
                    }

                    if (module == 2)
                    {
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted, VendorId, belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                        //  VendorId = RefId;
                    }

                    if (module == 3 || module == 4)
                    {
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted, VendorId, belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                        //  VendorId = RefId;
                    }

                    int UserPersonId = 0;
                    string UserName = "";

                    if (SessionManager.CurrentUser.Person != null)
                    {
                        UserPersonId = SessionManager.CurrentUser.Person.PersonId;
                        UserName = SessionManager.CurrentUser.Person.FirstName + " " + SessionManager.CurrentUser.Person.LastName;
                    }
                    else if (SessionManager.SecurityUser.Person != null)
                    {
                        UserPersonId = SessionManager.SecurityUser.Person.PersonId;
                        UserName = UserName = SessionManager.SecurityUser.Person.FirstName + " " + SessionManager.SecurityUser.Person.LastName;
                    }

                    connection.Execute(query, new
                    {
                        module = module,
                        moduleId = moduleId,
                        userId = UserPersonId,
                        parentId = Comments.parentId,
                        haschildId = Comments.haschildId,
                        userImageUrl = Comments.userImageUrl,
                        userName = UserName,
                        htmlComment = Comments.htmlComment,
                        htmlCurrentComment = Comments.htmlComment,
                        belongsTo = parentobj.belongsTo,
                        isReply = Comments.isReply,
                        isEdit = Comments.isReply,
                        createdAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        isDeleted = false,
                        VendorId = VendorId
                    });

                    // connection.Execute("Update CRM.Comments set haschildId=@haschildId,updatedAt=@updatedAt where id=@id", new { haschildId=true,id= Comments.parentId, updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    connection.Execute("Update CRM.Comments set haschildId=@haschildId where id=@id", new { haschildId = true, id = Comments.parentId });

                    // var res = connection.Query<Comments>("Select * from CRM.Comments where where id=@id", new { id=id}).ToList();
                    var res = new List<Comments>();
                    if (module == 1)
                        res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted and userId=@userId)", new { module = module, moduleId = moduleId, isDeleted = false, userId = UserPersonId }).ToList();

                    if (module == 2)
                        res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted and userId=@userId)", new { module = module, moduleId = moduleId, isDeleted = false, userId = UserPersonId }).ToList();

                    if (module == 3 || module == 4)
                        res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted and userId=@userId)", new { module = module, moduleId = moduleId, isDeleted = false, userId = UserPersonId }).ToList();


                    var parentcomment = connection.Query<Comments>("Select * from CRM.Comments where id= @id", new { id = Comments.parentId }).ToList();
                    var ress = connection.Query<Person>("Select * from CRM.Person where PersonId= @id", new { id = parentcomment[0].userId }).ToList();
                    connection.Close();

                    // var result = Comments;
                    // string val = Comments.module;
                    string[] emailids = new string[] { };
                    if (unique != null)
                        emailids = unique.Split(',');

                    string[] emailidss = new string[] { };

                    if (module == 1)
                    {

                        if (ress.Count > 0)
                        {
                            List<string> item = new List<string>();
                            item = emailids.ToList();
                            item.Add(ress[0].PrimaryEmail);

                            emailidss = item.ToArray();

                        }
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "Reply", emailidss);

                        res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);

                    }
                    else if (module == 2)
                    {
                        if (ress.Count > 0)
                        {
                            List<string> item = new List<string>();
                            item = emailids.ToList();
                            item.Add(ress[0].PrimaryEmail);

                            emailidss = item.ToArray();

                        }
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "Reply", emailidss);

                        res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);

                    }
                    else if (module == 3 || module == 4)
                    {
                        if (ress.Count > 0)
                        {
                            List<string> item = new List<string>();
                            item = emailids.ToList();
                            item.Add(ress[0].PrimaryEmail);

                            emailidss = item.ToArray();

                        }
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "Reply", emailidss);
                        var queryy = "";
                        if (module == 3)
                            queryy = @"select f.* from CRM.VendorCase vc
                                     join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where fc.CaseId  = @CaseId ";
                        else
                            queryy = @"select Fr.* from CRM.Franchise Fr
                                       join CRM.Opportunities op on op.FranchiseId = fr.FranchiseId
                                        join crm.Orders o on o.OpportunityId = op.OpportunityId
                                        join crm.FranchiseCase fc on fc.SalesOrderId = o.OrderID
                                        where fc.CaseId = @CaseId";
                        connection.Open();
                        var resss = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();
                        connection.Close();

                        if (res[0] != null) res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt, (TimeZoneEnum)resss.TimezoneCode);
                        if (res[0] != null) res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt, (TimeZoneEnum)resss.TimezoneCode);


                        //res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        //res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);

                    }
                    else
                    {
                    }

                    return Json(res[0]);

                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }

        [HttpPost]
        public IHttpActionResult deleteComment(int id, int parentId)
        {

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    connection.Execute("update CRM.Comments set isDeleted=@isDeleted,updatedAt=@updatedAt,deletedAt=@deletedAt where id=@id", new { isDeleted = true, id = id, updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc), deletedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    var res = connection.Query<Comments>("select * from CRM.Comments where parentId=@parentId and isDeleted=@isDeleted", new { parentId = parentId, isDeleted = false }).ToList();
                    if (!(res.Count > 0))
                    {
                        connection.Execute("update CRM.Comments set haschildId=@haschildId,updatedAt=@updatedAt where id=@id", new { haschildId = false, id = parentId, updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    }
                    connection.Close();
                }
                return Json(true);

            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);

            }

        }





        [HttpPost]
        public IHttpActionResult updateParentComment(int id, [FromUri] string unique, int module, int moduleId, Comments Comments)
        {
            DateTime dt = DateTime.UtcNow;
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    connection.Execute("update CRM.Comments set htmlComment= @htmlComment, htmlCurrentComment=@htmlCurrentComment,updatedAt=@updatedAt where id=@id", new { htmlComment = Comments.htmlCurrentComment, htmlCurrentComment = Comments.htmlCurrentComment, id = id, updatedAt = dt });
                    connection.Close();

                    // var result = Comments;
                    //  string val = Comments.module;
                    string[] emailids = new string[] { };
                    if (unique != null)
                        emailids = unique.Split(',');



                    if (Comments.module == 1)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);


                        var rr = TimeZoneManager.ToLocal(dt);
                        return Json(rr);

                    }
                    else if (Comments.module == 2) //|| Comments.module == 4
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);


                        var rr = TimeZoneManager.ToLocal(dt);
                        return Json(rr);
                    }
                    else if (Comments.module == 3)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);


                        var queryy = @"select f.* from CRM.VendorCase vc
                                     join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where fc.CaseId  = @CaseId ";
                        connection.Open();
                        var resss = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();
                        connection.Close();

                        var rr = TimeZoneManager.ToLocal(dt, (TimeZoneEnum)resss.TimezoneCode);
                        return Json(rr);
                    }
                    else if(Comments.module == 4)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);


                        var queryy = @"select Fr.* from CRM.Franchise Fr
									   join CRM.Opportunities op on op.FranchiseId= fr.FranchiseId
										join crm.Orders o on o.OpportunityId= op.OpportunityId
										join crm.FranchiseCase fc on fc.SalesOrderId= o.OrderID
										where fc.CaseId = @CaseId ";
                        connection.Open();
                        var resss = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();
                        connection.Close();

                        var rr = TimeZoneManager.ToLocal(dt, (TimeZoneEnum)resss.TimezoneCode);
                        return Json(rr);
                    }
                    else
                    {

                    }

                }
                return null;

            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);

            }
        }

        [HttpGet]
        public IHttpActionResult loadInitialComments(int id, int module)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var res = new List<Comments>();
                    if (module == 1)
                    {
                        res = connection.Query<Comments>("Select * from CRM.Comments where module in (1,2,3,4) and moduleId = @moduleId and isDeleted=@isDeleted", new { module = module, moduleId = id, isDeleted = false }).ToList();
                        foreach (Comments cm in res)
                        {
                            cm.createdAt = TimeZoneManager.ToLocal(cm.createdAt);
                            cm.updatedAt = TimeZoneManager.ToLocal(cm.updatedAt);
                        }

                    }
                    if (module == 2) //module 4 is for homeoffice view
                    {
                        res = connection.Query<Comments>("Select * from CRM.Comments where module in (1,2,3,4) and moduleId = @moduleId and isDeleted=@isDeleted", new { module = module, moduleId = id, isDeleted = false }).ToList();
                        foreach (Comments cm in res)
                        {
                            cm.createdAt = TimeZoneManager.ToLocal(cm.createdAt);
                            cm.updatedAt = TimeZoneManager.ToLocal(cm.updatedAt);
                        }
                    }
                    if (module == 3)
                    {
                        res = connection.Query<Comments>("Select * from CRM.Comments where module in (1,2,3,4) and moduleId = @moduleId and isDeleted=@isDeleted and belongsTo=@belongsTo", new { module = module, moduleId = id, isDeleted = false, belongsTo = 0 }).ToList();
                        foreach (Comments cm in res)
                        {
                            var queryy = @"select f.* from CRM.VendorCase vc
                                     join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where fc.CaseId  = @CaseId ";
                            var resss = connection.Query(queryy, new { CaseId = id }).FirstOrDefault();

                            cm.createdAt = TimeZoneManager.ToLocal(cm.createdAt, (TimeZoneEnum)resss.TimezoneCode);
                            cm.updatedAt = TimeZoneManager.ToLocal(cm.updatedAt, (TimeZoneEnum)resss.TimezoneCode);

                        }
                    }
                    if (module == 4) //module 4 is for homeoffice view
                    {
                        res = connection.Query<Comments>("Select * from CRM.Comments where module in (1,2,3,4) and moduleId = @moduleId and isDeleted=@isDeleted", new { module = module, moduleId = id, isDeleted = false }).ToList();
                        foreach (Comments cm in res)
                        {
                            //                 var queryy = @"select f.TimezoneCode from CRM.VendorCase vc
                            //                          join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
                            //join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
                            //join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
                            //join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
                            //join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                            //                          join CRM.Orders o on o.QuoteKey=q.QuoteKey
                            //                          join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                            //                          join CRM.Franchise f on f.FranchiseId=op.FranchiseId
                            //                       where fc.CaseId  = @CaseId ";

                            var queryy = @"select Fr.TimezoneCode from CRM.Franchise Fr
									   join CRM.Opportunities op on op.FranchiseId= fr.FranchiseId
										join crm.Orders o on o.OpportunityId= op.OpportunityId
										join crm.FranchiseCase fc on fc.SalesOrderId= o.OrderID
										where fc.CaseId=@CaseId";
                            var result = connection.Query(queryy, new { CaseId = id }).FirstOrDefault();
                            cm.createdAt = TimeZoneManager.ToLocal(cm.createdAt, (TimeZoneEnum)result.TimezoneCode);
                            cm.updatedAt = TimeZoneManager.ToLocal(cm.updatedAt, (TimeZoneEnum)result.TimezoneCode);
                        }
                    }
                    connection.Close();
                    return Json(res);
                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult FindFranchiseTimeZone(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var TimezoneCode = connection.Query<int>(@"select f.TimezoneCode
                                                    from CRM.FranchiseCase fc 
                                                    join Auth.Users u on u.PersonId = fc.Owner_PersonId
                                                    join CRM.Franchise f on f.FranchiseId = u.FranchiseId
                                                    where fc.CaseId =@CaseId", new { CaseId = id }).FirstOrDefault();
                    connection.Close();
                    if (TimezoneCode > 0)
                        return Json(((TimeZoneEnum)TimezoneCode).ToString());
                    else
                        return Json("");
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return this.ResponseResult(e);

            }

        }

        [HttpGet]
        public IHttpActionResult getUseridAndName(int id)
        {
            int UserId = 0;
            string UserName = "";
            if (SessionManager.CurrentUser.Person != null)
            {
                UserId = SessionManager.CurrentUser.PersonId;
                UserName = SessionManager.CurrentUser.UserName;
            }
            else if (SessionManager.SecurityUser.Person != null)
            {
                UserId = SessionManager.SecurityUser.PersonId;
                UserName = SessionManager.SecurityUser.UserName;
            }

            return Json(new { id = UserId, name = UserName });
        }

        [HttpGet]
        public IHttpActionResult loadUserlist(int id, int module)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    if (module == 1)
                    {
                        var res = connection.Query(@"select u.UserName name , p.PrimaryEmail content from Auth.Users u 
                                                    Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp' 
                                                    and u.FranchiseId = ( select FranchiseId from Auth.Users
                                                    where PersonId in (select Owner_PersonId from CRM.FranchiseCase where CaseId = @moduleId))
                                                     union
									                select u.UserName name , p.PrimaryEmail content from Auth.Users u
									                Inner join CRM.Person p on p.PersonId = u.PersonId
									                Inner join CRM.VendorCaseConfig vcf on vcf.CaseLogin = u.UserName
									                Inner Join acct.HFCVendors  hv on vcf.VendorId= hv.VendorIdPk
									                Inner Join CRM.QuoteLines ql on ql.VendorId = hv.VendorId
									                Inner Join CRM.FranchiseCaseAddInfo fcai on fcai.QuoteLineId= ql.QuoteLineId
									                where  fcai.CaseId=@moduleId", new { moduleId = id }).ToList();
                        connection.Close();
                        return Json(res);
                    }
                    if (module == 2)
                    {
                        var res = connection.Query(@"select u.UserName name , p.PrimaryEmail content from Auth.Users u
                            Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp'
                            and u.FranchiseId = ( select FranchiseId from Auth.Users
                            where PersonId in (select Owner_PersonId from CRM.FranchiseCase where CaseId = @moduleId))
                             	union
									select u.UserName name , p.PrimaryEmail content from Auth.Users u
									Inner join CRM.Person p on p.PersonId = u.PersonId
									Inner join CRM.VendorCaseConfig vcf on vcf.CaseLogin = u.UserName
									Inner Join acct.HFCVendors  hv on vcf.VendorId= hv.VendorIdPk
									Inner Join CRM.QuoteLines ql on ql.VendorId = hv.VendorId
									Inner Join CRM.FranchiseCaseAddInfo fcai on fcai.QuoteLineId= ql.QuoteLineId
									where  fcai.CaseId=@moduleId", new { moduleId = id }).ToList();
                        connection.Close();
                        return Json(res);
                    }
                    if (module == 3 || module == 4)
                    {
                        var res = connection.Query("select u.UserName name , p.PrimaryEmail content from Auth.Users u Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp' and u.FranchiseId = ( select FranchiseId from Auth.Users where PersonId in (select Owner_PersonId from CRM.FranchiseCase where CaseId = @moduleId))", new { moduleId = id }).ToList();
                        connection.Close();
                        return Json(res);
                    }
                    return Json(false);
                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

    }
}