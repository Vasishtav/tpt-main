﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Web.Filters;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class AddressController : BaseApiController
    {
        public class ParamObj
        {
            public List<int> leadIds { get; set; }
            public bool includeStates { get; set; }
        }

        private LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public IHttpActionResult Get([FromUri]ParamObj filters)
        {
            try
            {
                List<AddressTP> result = null;
                if (filters != null && filters.leadIds != null)
                {
                    result = LeadMgr.GetAddress(filters.leadIds);
                }

                return Ok(new
                {
                    //FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString(),
                    //Addresses = result,
                    States = filters.includeStates ? CacheManager.StatesCollection.Select(s => new
                    {
                        s.StateAbv,
                        s.StateName,
                        s.CountryISO2
                    }) : null,
                    Countries = filters.includeStates ? CacheManager.StatesCollection.Select(s => new {
                        s.Type_Country.ISOCode2Digits,
                        s.Type_Country.Country,
                        s.Type_Country.ZipCodeMask
                    }).Distinct() : null
                });
            }
            catch (UnauthorizedAccessException uea)
            {
                return Forbidden(uea.Message);
            } 
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
