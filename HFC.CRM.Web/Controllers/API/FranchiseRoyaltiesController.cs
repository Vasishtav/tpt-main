﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    using System.Collections.Generic;
    using System.Linq;

    using HFC.CRM.Web.Models.Royalty;
    using HFC.CRM.Web.Filters;

    [LogExceptionsFilter]
    public class FranchiseRoyaltiesController : BaseApiController
    {
        readonly FranchiseManager franMgr = new FranchiseManager(SessionManager.SecurityUser);

        public IHttpActionResult Get(int id)
        {
            try
            {
                var royalties = this.franMgr.GetRoyalties(id).Where(t => t.FranchiseId == id).ToList();

                /*foreach (var territory in royalties)
                {
                    territory.FranchiseRoyalties = territory.FranchiseRoyalties.Where(fr => fr.)
                }*/


                var ids = royalties.Select(r => r.TerritoryId);
				
                var nextLevels = new Dictionary<int, string>();
                foreach (var i in ids)
                {
                    string nextLevelName;
                    if (this.franMgr.GetNextRoyaltyLevelName(i, out nextLevelName) == "Success")
                    {
                        if (nextLevelName != null)
                        {
                            nextLevels.Add(i, nextLevelName);
                        }
                    }
                }

                if (nextLevels.Count > 0)
                {
                    return Ok(new
                    {
                        NextLevels = true
                    });
                }
                else
                {
                    return Ok(new
                    {
                        NextLevels = false
                    });
                }

                //return Ok(new
                //{
                //    Royalties = royalties,
                //    NextLevels = nextLevels
                //});
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        [ActionName("GetRoyalties")]
        public IHttpActionResult GetRoyalties(int id)
        {
            try
            {
                var royalties = this.franMgr.GetRoyalties(id).Where(t => t.FranchiseId == id).ToList();

                return Ok(new
                {
                    Royalties = royalties
                });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        [ActionName("ApplyTemplate")]
        public IHttpActionResult ApplyTemplate([FromBody]ApplyTemplateModel model)
        {
            try
            {
                return ResponseResult(this.franMgr.AppyRoyaltyTemplate(model.TerritoryId, model.RoyaltyProfileId, model.StartDate));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        [HttpGet]
        [ActionName("GetNextRoyaltyLevel")]
        public IHttpActionResult GetNextRoyaltyLevel(int id)
        {
            try
            {
                string nextRoyaltyName;
                var status = this.franMgr.GetNextRoyaltyLevelName(id, out nextRoyaltyName);
                return ResponseResult(status, Json(new { nextRoyaltyName }));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        [HttpPost]
        [ActionName("AddNextRoyaltyLevel")]
        public IHttpActionResult AddNextRoyaltyLevel(int id, [FromBody]AddNextRoyaltyModal modal)
        {
            try
            {
                return ResponseResult(this.franMgr.AddNextRoyaltyLevel(id, modal.Monthes, modal.Amount));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        public IHttpActionResult Post([FromBody]Franchise model)
        {
            try
            {
                return ResponseResult(this.franMgr.Add(model), Json(new { model = model }));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        public IHttpActionResult Put([FromBody]FranchiseRoyalty model)
        {
            try
            {
                return ResponseResult(this.franMgr.UpdateRoyalty(model.RoyaltyId, model));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                return ResponseResult(this.franMgr.DeleteRoyalty(id));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }
        
	}
}