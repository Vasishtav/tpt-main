﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// The API namespace.
/// </summary>
namespace HFC.CRM.Web.Controllers.API
{
    /// <summary>
    /// Class JobStatusController.
    /// </summary>
    /// 
    [LogExceptionsFilter]
    public class JobStatusController : BaseApiController
    {
        /// <summary>
        /// The fran MGR
        /// </summary>
        FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);

        /// <summary>
        /// Posts the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        public IHttpActionResult Post(Type_JobStatus model)
        {
            var status = "Invalid data";
            int statusId = 0;
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = FranMgr.AddJobStatus(model);

                statusId = model.Id;
            }
            return ResponseResult(status, Json(new { Id = statusId }));
        }

        /// <summary>
        /// Puts the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        public IHttpActionResult Put(Type_JobStatus model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = FranMgr.UpdateJobStatus(model);
            }
            return ResponseResult(status);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IHttpActionResult.</returns>
        public IHttpActionResult Delete(int id)
        {
            var status = FranMgr.DeleteJobStatus(SessionManager.CurrentFranchise.FranchiseId, id);
            return ResponseResult(status);
        }
    }
}
