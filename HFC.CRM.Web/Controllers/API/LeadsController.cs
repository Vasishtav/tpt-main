﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Address;
using HFC.CRM.DTO.Lead;
using HFC.CRM.DTO.Notes;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Net.Mail;
using HFC.CRM.Web.Controllers;
using Newtonsoft.Json;
using HFC.CRM.Core.Membership;
using HFC.CRM.Managers.DTO;
using System.IO;
using System.Web.Helpers;
using HFC.Common;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class LeadsController : BaseApiController
    {
        private LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        private CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        /// <summary>
        /// Gets a single lead, will include a lot of lead detail
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        public IHttpActionResult Get(int id)
        {
            //var permission = PermissionProviderTP.GetPermissionTP(SessionManager.CurrentUser, "List Leads");
            //if (permission == null || !permission.CanAccess)
            //{
            //    return Forbidden(ContantStrings.Unauthorized);
            //}
            //if (!CanAccess("List Lead"))
            //{
            //    return Forbidden(ContantStrings.Unauthorized);
            //}

            try
            {
                var lead = LeadMgr.Get(id);

                var result = JsonConvert.SerializeObject(lead, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                  ,
                    PreserveReferencesHandling = PreserveReferencesHandling.None
                });

                if (lead != null && lead.FranchiseId != SessionManager.CurrentFranchise.FranchiseId)
                {
                    throw new AccessViolationException("Sorry, you do not have permission");
                }
                return
                    Ok(
                        new
                        {
                            Lead = lead,
                            //LeadPrimaryNotes = lead.LeadNotes.FirstOrDefault(n => n.TypeEnum == NoteTypeEnum.Primary),

                            FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString(),
                            FranchiseAddressObject = SessionManager.CurrentFranchise.Address,
                            Addresses = LeadMgr.GetAddressTP(id),  //LeadMgr.GetAddress(new List<int>() { id }),
                            States = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 }),
                            Countries = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct(),

                            NoteTypeEnum = NoteTypeEnum.Internal.ToDictionary<byte>((byte)NoteTypeEnum.Primary),

                            SelectedLeadStatus = lead.LeadStatus.Name,
                            selectedCampaign = lead.CampaignName,

                            PrimarySource = lead.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault(),
                            SecondarySource = lead.SourcesList.Where(x => x.IsPrimarySource == false).ToList(),
                            FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting,
                            TextStatus = communication_Mangr.GetOptingInfo(lead.PrimCustomer.CellPhone)
                        });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost/*, ValidateAjaxAntiForgeryToken*/]
        public IHttpActionResult Post([FromBody]Lead model)
        {
            int leadId = 0;
            var ld = LeadMgr.GetLeadByLeadId(model.LeadId);
            //texting
            var FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting;
            var CountryCode = SessionManager.CurrentFranchise.CountryCode;
            var BrandId = SessionManager.CurrentFranchise.BrandId;
            if (model.PrimCustomer.CellPhone != null)
            {
                var TextStatus = communication_Mangr.GetOptingInfo(model.PrimCustomer.CellPhone);
                if (FranciseLevelTexting && model.IsNotifyText && (TextStatus == null || TextStatus.IsOptinmessagesent == false))
                {
                    if (model.SendText && TextStatus == null)
                        communication_Mangr.SendOptinMessage(model.PrimCustomer.CellPhone, BrandId, CountryCode, model.LeadId, null);
                    else if(model.SendText && TextStatus.IsOptinmessagesent == false)
                        communication_Mangr.ForceOptinMessage(model.PrimCustomer.CellPhone, BrandId, CountryCode, model.LeadId, null);
                }
            }
            //check for saving empty cell phone
            if (ld != null)
            {
                model.LeadGuid = ld.LeadGuid;
            }

            if (model.SourcesTPIdFromCampaign > 0) model.SourcesTPId = model.SourcesTPIdFromCampaign;

            if (model.LeadId > 0)
            {
                // TP-3904: Lead Created date has changed to past while change the lead status.
                // The created on date should not be modifed by based what sent from the UI.
                model.CreatedOnUtc = ld.CreatedOnUtc;
                model.CreatedByPersonId = ld.CreatedByPersonId;

                var updateStatus = LeadMgr.Update(model);
                return ResponseResult(updateStatus, Json(new { LeadId = model.LeadId }));
            }
            if (model.SkipDuplicateCheck == false)
            {
                var dulpicateList = LeadMgr.GetDuplicateLeads(SearchDuplicateLeadEnum.All, string.Empty, model);
                if (dulpicateList.Count > 0)
                {
                    var dtos = dulpicateList.Select(v => new DuplicateLeadModel()
                    {
                        Address = v.Addresses.FirstOrDefault() != null ? v.Addresses.FirstOrDefault().Address1 : "",
                        LeadId = v.LeadId,
                        CellPhone = TelBuilder(v.PrimCustomer.CellPhone),
                        HomePhone = TelBuilder(v.PrimCustomer.HomePhone),
                        FaxPhone = TelBuilder(v.PrimCustomer.FaxPhone),
                        WorkPhone = TelBuilder(v.PrimCustomer.WorkPhone),
                        PrimaryEmail = v.PrimCustomer.PrimaryEmail,
                        SecondaryEmail = v.PrimCustomer.SecondaryEmail,
                        FullName = v.PrimCustomer.FullName,
                        LeadNumber = v.LeadNumber
                    }).Take(3);

                    return ResponseResult("Success", Json(new { lstDuplicates = dtos }));
                }
            }

            var status = LeadMgr.CreateLead(out leadId, model);

            //Sending email - Thanks Email After save Lead
            //SendEmail(leadId, 1);
           
            return ResponseResult(status, Json(new { LeadId = leadId }));
        }

        private string TelBuilder(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            str = "(" + str.Insert(3, ")");
            return str.Insert(8, "-");
        }

        /// <summary>
        /// Updates either primary or secondary person in lead
        /// </summary>
        /// <param name="id">LeadId</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut, ValidateAjaxAntiForgeryToken, ActionName("Person")]
        public IHttpActionResult UpdatePerson(int id, [FromBody]Person model)
        {
            var status = LeadMgr.UpdatePerson(id, model);

            return ResponseResult(status);
        }

        /// <summary>
        /// This will add to secondary customer, we do not need an Add for primary since it is required to create a new lead
        /// </summary>
        /// <param name="id">LeadId</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, ValidateAjaxAntiForgeryToken, ActionName("Person")]
        public IHttpActionResult AddPerson(int id, [FromBody]Person model)
        {
            int personId = 0;
            var status = LeadMgr.AddSecondaryCustomer(id, model, out personId);

            return ResponseResult(status, Json(new { PersonId = personId }));
        }

        // The following is not used anymore as address is handled by separate component

        //[HttpPut, ValidateAjaxAntiForgeryToken, ActionName("Address")]
        //public IHttpActionResult UpdateAddress(int id, [FromBody]Address address)
        //{
        //    var status = LeadMgr.UpdateAddress(id, address);

        //    return ResponseResult(status);
        //}

        //[HttpPost, ValidateAjaxAntiForgeryToken, ActionName("Address")]
        //public IHttpActionResult AddAddress(int id, [FromBody]Address address)
        //{
        //    int addressId = 0;
        //    var status = LeadMgr.AddAddress(id, address, out addressId);

        //    return ResponseResult(status, Json(new { AddressId = addressId }));
        //}

        //[HttpDelete, ValidateAjaxAntiForgeryToken, ActionName("Address")]
        //public IHttpActionResult DeleteAddress(int id, [FromBody]Address address)
        //{
        //    var status = "Invalid address";
        //    if (address != null)
        //    {
        //        status = LeadMgr.DeleteAddress(id, address.AddressId);
        //    }

        //    return ResponseResult(status);
        //}

        [HttpPut, ActionName("ConvertSelectLead")]
        public IHttpActionResult ConvertSelectLead(int id, [FromBody]ConvertLead request)
        {
            try
            {
                int brandId = (int)SessionManager.BrandId;
                int newaccountId = 0;
                int sourceAccountId = 0;
                if (request.selectedaccountid > 0)
                {
                    // merge lead to existing account
                    AccountMgr.MergeLeadToAccount(brandId, request.leadId, request.selectedaccountid, "");
                    sourceAccountId = request.selectedaccountid;
                }
                else
                {
                    // create new account for the lead
                    newaccountId = AccountMgr.ConvertLeadToAccount(out newaccountId, brandId, request.leadId, "");
                    sourceAccountId = newaccountId;
                }

                if (request.addopportunity > 0 && (request.selectedaccountid > 0 || newaccountId > 0))
                {
                    // add opportunity for the account
                    OpportunityMgr.ConvertLeadToOpportunity(brandId, request.leadId, ""
                        , request.opportunityName, request.SalesAgentId, sourceAccountId);
                }

                var status = "Success";
                return ResponseResult(status, Json(new { status, newaccountId }));
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult ValidateDuplicateLeadTP(int id, [FromBody]LeadDuplicationDTO model)
        {
            var result = LeadMgr.ValidateLeadDuplication(model);
            return Json(result);

            //var status = "Success";
            //return ResponseResult(status, Json(new { lstDuplicates = dtos }));
        }

        [HttpPost]
        public IHttpActionResult CheckDuplicates(int id, [FromBody]LeadDuplicateFilter filter)
        {
            var lst = LeadMgr.GetDuplicateLeads(filter.SearchType, filter.SearchTerm);

            var dtos = lst.Select(v => new DuplicateLeadModel()
            {
                Address = v.Addresses.FirstOrDefault() != null ? v.Addresses.FirstOrDefault().Address1 : "",
                LeadId = v.LeadId,
                CellPhone = TelBuilder(v.PrimCustomer.CellPhone),
                HomePhone = TelBuilder(v.PrimCustomer.HomePhone),
                FaxPhone = TelBuilder(v.PrimCustomer.FaxPhone),
                WorkPhone = TelBuilder(v.PrimCustomer.WorkPhone),
                PrimaryEmail = v.PrimCustomer.PrimaryEmail,
                SecondaryEmail = v.PrimCustomer.SecondaryEmail,
                FullName = v.PrimCustomer.FullName,
                LeadNumber = v.LeadNumber
            }).Take(3);

            var status = "Success";
            return ResponseResult(status, Json(new { lstDuplicates = dtos }));
        }

        [HttpGet]
        public IHttpActionResult CheckDuplicatesLeadTP(string duplicateCheck)
        {
            var lst = LeadMgr.CheckDuplicateLeadData(duplicateCheck);
            return Json(lst);
        }

        [HttpGet]
        public IHttpActionResult GetEmailStatus(int id, int leadid)
        {
            try
            {
                var result = LeadMgr.GetLeadEmail(leadid);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        #region ----- Tochpoint/SendEmail -----

        [HttpGet]
        public IHttpActionResult SendEmailToLead(int id, int leadid)
        {
            try
            {
                var lead = LeadMgr.Get(leadid); var result="";
                if (lead.IsNotifyemails == true)
                {
                     result = LeadMgr.SendThankYouEmail(leadid, EmailType.ThankyouInquiry); //, 1);
                     return Json(result);
                }
                else
                {
                    result = "Email option is not enabled";
                }

                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        #endregion ----- Tochpoint/SendEmail -----

        #region api 2.0

        [HttpPost]
        public IHttpActionResult CustomerLeadUpdate(int id, [FromBody]QuickDispositionVM model)
        {
            string[] splitValue = model.Value.Split('|');
            var leadStatusId = Convert.ToInt16(splitValue[0]);
            var dispositionId = Convert.ToInt16(splitValue[1]);
            var status = LeadMgr.UpdateLead(id, leadStatusId, dispositionId);
            return ResponseResult(status);
        }

        #endregion api 2.0

        [HttpPost]//ValidateAjaxAntiForgeryToken
        public IHttpActionResult UploadFileTP(int id)
        {
            //, int Id1, string Branch, string Document
            try
            {
                int? leadId = null; int? accountId = null; int? opportunityId = null; int? orderId = null;
                string title = ""; int? category = null;
                bool IsFranchiseSettings = false; bool? IsEnabled = null;
                int Id1 = 0;
                bool? sysWideIsEnabled = null; string sysWideTitle = ""; int? sysWideCategory = null;
                int? sysWideConcept = null; string Document = "";

                var httpRequest = HttpContext.Current.Request;
                var files = httpRequest.Files;

                if (httpRequest.Form["leadId"] != null)
                    leadId = Convert.ToInt32(httpRequest.Form["leadId"]);
                if (httpRequest.Form["accountId"] != null)
                    accountId = Convert.ToInt32(httpRequest.Form["accountId"]);
                if (httpRequest.Form["opportunityId"] != null)
                    opportunityId = Convert.ToInt32(httpRequest.Form["opportunityId"]);
                if (httpRequest.Form["orderId"] != null)
                    orderId = Convert.ToInt32(httpRequest.Form["orderId"]);

                if (httpRequest.Form["Id1"] != null)
                    Id1 = Convert.ToInt32(httpRequest.Form["Id1"]);

                if (httpRequest.Form["title"] != null)
                    title = Convert.ToString(httpRequest.Form["title"]);
                if (httpRequest.Form["category"] != null)
                    category = Convert.ToInt32(httpRequest.Form["category"]);

                if (httpRequest.Form["IsFranchiseSettings"] != null)
                    IsFranchiseSettings = Convert.ToBoolean(httpRequest.Form["IsFranchiseSettings"]);
                if (httpRequest.Form["IsEnabled"] != null)
                    IsEnabled = Convert.ToBoolean(httpRequest.Form["IsEnabled"]);

                if (httpRequest.Form["sysWideIsEnabled"] != null)
                    sysWideIsEnabled = Convert.ToBoolean(httpRequest.Form["sysWideIsEnabled"]);
                if (httpRequest.Form["sysWideTitle"] != null)
                    sysWideTitle = Convert.ToString(httpRequest.Form["sysWideTitle"]);
                if (httpRequest.Form["sysWideCategory"] != null)
                    sysWideCategory = Convert.ToInt32(httpRequest.Form["sysWideCategory"]);
                if (httpRequest.Form["sysWideConcept"] != null)
                    sysWideConcept = Convert.ToInt32(httpRequest.Form["sysWideConcept"]);
                if (httpRequest.Form["Document"] != null)
                    Document = Convert.ToString(httpRequest.Form["Document"]);

                var LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var notemgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                if (files != null && files.Count > 0)
                {
                    byte[] fileData = null;
                    using (var binaryReader = new BinaryReader(files[0].InputStream))
                    {
                        fileData = binaryReader.ReadBytes(files[0].ContentLength);
                    }
                    // We are allways getting one file.
                    // TODO: we need to find why we are not getting multiple files.
                    var streamId = LeadMgr.UploadFileTP(fileData, files[0].FileName);

                    if (Document == "SYS-DOC")
                    {
                        string filename = files[0].FileName;
                        notemgr.UpdateGlobalDocumentStreamId(Id1, sysWideTitle, sysWideIsEnabled, sysWideCategory, sysWideConcept, streamId, filename);
                        return Json(true);
                    }

                    // TODO: Update the Note table with this.
                    var newnote = new Note
                    {
                        Title = title,
                        TypeEnum = (NoteTypeEnum)category,
                        IsNote = false,
                        AttachmentStreamId = new Guid(streamId),
                        CreatedOn = DateTime.Now,
                        CreatedBy = SessionManager.CurrentUser.PersonId
                    };

                    newnote.LeadId = (leadId != null && leadId > 0) ? leadId : null;
                    newnote.AccountId = (accountId != null && accountId > 0) ? accountId : null;
                    newnote.OpportunityId = (opportunityId != null && opportunityId > 0) ? opportunityId : null;
                    newnote.OrderId = (orderId != null && orderId > 0) ? orderId : null;
                    newnote.IsEnabled = IsEnabled;

                    if (IsFranchiseSettings)
                    {
                        newnote.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    }
                    //TODO: if all the above are null, then it is an invalid entry.

                    notemgr.Add(newnote);

                    return Json(new
                    {
                        Message = "Success",
                        Filename = files[0].FileName,
                        FileId = streamId
                    });
                }

                string status = "Invalid file";

                return Json(status);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return Json(ex.Message);
            }
            //string status = "Invalid file";
        }

        //[HttpPost]
        //public IHttpActionResult UploadAttachment( int id, int VendorCaseId, string fileIconType, string fileSize,string fileName, string color)
        //{
        //    try
        //    {
        //       // int CaseId = id; int VendorCaseId = 0; string fileIconType = ""; string fileSize = ""; string fileName = ""; string color = "";

        //        var httpRequest = HttpContext.Current.Request;
        //        var files = httpRequest.Files;

        //       // if (httpRequest.Form["CaseId"] != null)
        //           int  CaseId = id;
        //        //if (httpRequest.Form["VendorCaseId"] != null)
        //        //    VendorCaseId = Convert.ToInt32(httpRequest.Form["VendorCaseId"]);

        //        //if (httpRequest.Form["fileIconType"] != null)
        //        //    fileIconType = Convert.ToString(httpRequest.Form["fileIconType"]);
        //        //if (httpRequest.Form["fileSize"] != null)
        //        //    fileSize = Convert.ToString(httpRequest.Form["fileSize"]);
        //        //if (httpRequest.Form["fileName"] != null)
        //        //    fileName = Convert.ToString(httpRequest.Form["fileName"]);
        //        //if (httpRequest.Form["color"] != null)
        //        //    color = Convert.ToString(httpRequest.Form["color"]);

        //        if (files != null && files.Count > 0)
        //        {
        //            byte[] fileData = null;
        //            using (var binaryReader = new BinaryReader(files[0].InputStream))
        //            {
        //                fileData = binaryReader.ReadBytes(files[0].ContentLength);
        //            }
        //            var streamId = LeadMgr.UploadFileTP(fileData, files[0].FileName);

        //            List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
        //            string extension = Path.GetExtension(files[0].FileName).ToUpper();
        //            var streamId1 = "";
        //            VendorCaseId = CaseId;
        //            if (ImageExtensions.Contains(extension))
        //            {
        //                WebImage img = new WebImage(fileData);
        //                img.Resize(50, 30);
        //                byte[] thump = img.GetBytes();
        //                string thumpfileName = DateTime.UtcNow.ToString("MMddyyyy") + "_thump_" + DateTime.UtcNow.ToString("HHMMssfff") + "_" + files[0].FileName;
        //                streamId1 = LeadMgr.UploadFileTP(thump, thumpfileName);
        //            }

        //            var res = Case_Mangr.saveFranchiseCaseAttachmentDetails(CaseId, VendorCaseId, fileIconType, fileSize, fileName, color, streamId, streamId1);

        //            return Json(res);
        //        }
        //        return Json("File Not found");
        //    }
        //    catch (Exception ex)
        //    {
        //        var message = EventLogger.LogEvent(ex);
        //        return Json(ex.Message);
        //    }

        //}

        [HttpPost]
        public IHttpActionResult UpdateGlobalDocDetails(int id)//            int Id1, string sysWideTitle, int sysWideCategory, int sysWideConcept, bool sysWideIsEnabled)
        {
            try
            {
                var Request = HttpContext.Current.Request;
                int Id = Convert.ToInt32(Request.Form["Id1"]);
                string tiltle = Request.Form["sysWideTitle"];
                int Category = Convert.ToInt32(Request.Form["sysWideCategory"]);
                int Concept = Convert.ToInt32(Request.Form["sysWideConcept"]);
                bool IsEnabled = bool.Parse(Request.Form["sysWideIsEnabled"]);
                var notemgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                notemgr.UpdateGlobalDocumentStreamId(Id, tiltle, IsEnabled, Category, Concept, "", "");
                return Json("Success");
            }
            catch (Exception ex)
            {
                // Response.StatusCode = 400;
                //Response.StatusDescription = ex.Message;
                EventLogger.LogEvent(ex);
                return Json(ex.Message);
            }
        }
    }

    public class ConvertLead

    {
        public int leadId { get; set; }

        public int selectedaccountid { get; set; }
        public int addnewaccount { get; set; }
        public int addopportunity { get; set; }
        public string opportunityName { get; set; }

        public int SalesAgentId { get; set; }
    }
}