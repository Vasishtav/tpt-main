﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.webOE;
using HFC.CRM.Managers;
using HFC.CRM.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Managers.AdvancedEmailManager;
using Newtonsoft.Json;
using HFC.CRM.Web.Filters;
using System.Dynamic;
using HFC.CRM.DTO.PurchaseOrder;
using HFC.CRM.DTO.BulkPurchase;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class PurchaseOrdersController : BaseApiController
    {
        private OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private PICManager picMgr = new PICManager();
        private OpportunitiesManager oppMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public class GetParams
        {
            public DateTimeOffset? startDate { get; set; }
            public DateTimeOffset? endDate { get; set; }
            public int? statusEnum { get; set; }
            public string orderBy { get; set; }
            public OrderByEnum orderDir { get; set; }
            public int pageNum { get; set; }
            public int pageSize { get; set; }

            public int? leadId { get; set; }
        }

        private readonly PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public IHttpActionResult Get([FromUri]GetParams param)
        {
            // TODO: do we need this???
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();

            try
            {
                int totalRecords;
                var purchaseOrders = this.PurchaseOrderMgr.Get(out totalRecords, param.statusEnum, param.startDate, param.endDate, param.pageNum, param.pageSize, param.orderBy, param.leadId, param.orderDir);
                var orderStatus = this.PurchaseOrderMgr.GetOrderStatus();

                return Ok(new
                {
                    PurchaseOrders = purchaseOrders,
                    OrderStatus = orderStatus,
                    TotalRecords = totalRecords
                });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        public IHttpActionResult Get(int id)
        {
            // TODO: do we need this???
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();

            try
            {
                GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                return Ok(this.PurchaseOrderMgr.Get(id));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpDelete, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Delete(int id)
        {
            var status = this.PurchaseOrderMgr.Cancel(id);
            return ResponseResult(status);
        }

        /// <summary>
        /// Add new Order from quote id
        /// </summary>
        /// <param name="id">quote id</param>
        /// <returns>action result</returns>
        [HttpGet, ActionName("AddOrder")]
        public IHttpActionResult AddOrder(int id)
        {
            var status = PurchaseOrderMgr.AddOrder(id);

            if (status == PurchaseOrderManager.Success)
                return ResponseResult(status);

            return ResponseResult(status);
        }

        /// <summary>
        /// Update existing order item
        /// </summary>
        /// <param name="id">order id</param>
        /// <param name="orderItem">order item data</param>
        /// <returns>action result</returns>
        [HttpPut, ValidateAjaxAntiForgeryToken, ActionName("OrderItem")]
        public IHttpActionResult UpdateOrderItem(int id, [FromBody]OrderItem orderItem)
        {
            var status = "Invalid order item";

            if (orderItem != null)
            {
                status = PurchaseOrderMgr.UpdateOrderItem(id, orderItem);
            }

            return ResponseResult(status);
        }

        /// <summary>
        /// get order review for order id
        /// </summary>
        /// <param name="id">order id</param>
        /// <returns></returns>
        [HttpGet, ActionName("OrderReview")]
        public IHttpActionResult GetOrderReview(int id)
        {
            try
            {
                var order = PurchaseOrderMgr.GetOrderReview(id);
                return Ok(order);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// update order status for order
        /// </summary>
        /// <param name="id">order id</param>
        /// <param name="orderStatus">order status type</param>
        /// <returns></returns>
        [HttpPut, ValidateAjaxAntiForgeryToken, ActionName("OrderStatus")]
        public IHttpActionResult UpdateOrderStatus(int id, [FromBody]Type_OrderStatus orderStatus)
        {
            var status = "Invalid order status";

            if (orderStatus != null)
            {
                status = PurchaseOrderMgr.UpdateOrderStatus(id, orderStatus);
            }

            return ResponseResult(status);
        }

        [HttpPost]
        public IHttpActionResult ConfirmAddress(int id, MasterPurchaseOrder model)
        {
            try
            {
                var result = PurchaseOrderMgr.ConfirmAddress(id);
                return Json(new { error = "", data = result });
            }
            catch (Exception ex)
            {
                return Json(new { errors = ex.Message, data = "" });
            }
        }

        [HttpGet]
        public IHttpActionResult GetMPOList(int id)
        {
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();

            try
            {
                var result = PurchaseOrderMgr.GetMPOList();

                // TP-1698: CLONE - Opportunities and related need record level permissions
                // Opportunity need a special permission to access all the
                // records. only certain roles has that permission. otherwise
                // If the logged in user is a Sales person or installation
                // person, the can see their records alone.
                if (!this.CanAccess("Opportunity All Record Access"))
                {
                    // Need to apply the filter for SalesAgent/Installation.
                    var filtered = result.Where(x => (x.SalesAgentId ==
                                        SessionManager.CurrentUser.PersonId ||
                                        x.InstallerId == SessionManager.CurrentUser.PersonId)
                                        ).ToList();

                    result = filtered;
                }

                return Json(new { error = "", data = result });
            }
            //catch (UnauthorizedAccessException uae)
            //{
            //    return Forbidden(uae.Message);
            //}
            catch (Exception ex)
            {
                return Json(new { errors = ex.Message, data = "" });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetMPOnew(int id)
        {
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();
            MasterPurchaseOrder result = new MasterPurchaseOrder();
            try
            {
                result = PurchaseOrderMgr.GetMPO(id);
                if (result.POStatusId > 1 && result.CoreProductVPOs != null && result.CoreProductVPOs.Count > 0)
                {
                    result.RequestObject = JsonConvert.SerializeObject(PurchaseOrderMgr.GetOrderObject(result.OrderId));
                }

                return Json(new { error = "", data = result });
            }
            //catch (UnauthorizedAccessException uae)
            //{
            //    return Forbidden(uae.Message);
            //}
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = result });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult MPO_PIC_ASYNC(int id)
        {
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();
            MasterPurchaseOrder result = new MasterPurchaseOrder();
            try
            {
                result = PurchaseOrderMgr.GetMPO(id);

                var res = PurchaseOrderMgr.GetOrderObject(result.OrderId);

                return Json(new { error = "", data = res });
            }
            //catch (UnauthorizedAccessException uae)
            //{
            //    return Forbidden(uae.Message);
            //}
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetSalesOrdersByXMpo(int id)
        {
            // XMpoDTO result = new XMpoDTO();
            IList<OrderBulkPurchaseDTO> result = new List<OrderBulkPurchaseDTO>();
            try
            {
                result = PurchaseOrderMgr.GetSalesOrdersByXMpo(id);

                return Json(new { error = "", data = result });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = result });
            }
        }

        [HttpGet]
        public IHttpActionResult GetXMpo(int id)
        {
            XMpoDTO result = new XMpoDTO();
            try
            {
                result = PurchaseOrderMgr.GetXmpo(id);

                return Json(new { error = "", data = result });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = result });
            }
        }

        [HttpGet]
        public IHttpActionResult GetMPO(int id)
        {
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();
            MasterPurchaseOrder result = new MasterPurchaseOrder();
            try
            {
                result = PurchaseOrderMgr.GetMPO(id);
                //if (result.POStatusId > 1 && result.CoreProductVPOs!=null && result.CoreProductVPOs.Count>0)
                //{
                //    result.RequestObject = JsonConvert.SerializeObject(PurchaseOrderMgr.GetOrderObject(result.OrderId));
                //}

                return Json(new { error = "", data = result });
            }
            //catch (UnauthorizedAccessException uae)
            //{
            //    return Forbidden(uae.Message);
            //}
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = result });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult GetShippingTerritories(int id)
        {
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();

            try
            {
                var result = PurchaseOrderMgr.GetMPO(id);
                return Json(new { error = "", data = result });
            }
            //catch (UnauthorizedAccessException uae)
            //{
            //    return Forbidden(uae.Message);
            //}
            catch (Exception ex)
            {
                return Json(new { errors = ex.Message, data = "" });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult CheckMPOCancelled(int id)
        {
            try
            {
                var result = PurchaseOrderMgr.CheckMPOCancelled(id);

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Submit MPO to PIC
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mpoId"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult SubmitXMpo(int id)
        {
            var response = new XMpoDTO(); // changed class name tooo
            try
            {
                var result = PurchaseOrderMgr.SubmitXMpo(id);

                if (result != "Success")
                {
                    return Json(new { error = result, data = "" });
                }
                else
                {
                    response = PurchaseOrderMgr.GetXmpo(id);   // changer from GetMPO to GetXmpo
                    return Json(new { error = "", data = response });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }


        /// <summary>
        /// Submit MPO to PIC
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mpoId"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult SubmitMPO(int id, int mpoId)
        {
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();
            var response = new MasterPurchaseOrder();
            try
            {
                //var result = PurchaseOrderMgr.ConvertOrdertoMPO(id);
                var result = PurchaseOrderMgr.SubmitMPO(id, mpoId);

                if (result != "Success")
                {
                    return Json(new { error = result, data = "" });
                }
                else
                {
                    response = PurchaseOrderMgr.GetMPO(mpoId);
                    return Json(new { error = "", data = response });
                }
            }
            //catch (UnauthorizedAccessException uae)
            //{
            //    return Forbidden(uae.Message);
            //}
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult CancelVPO(int id, string VPO, string cancelReason)
        {
            //if (!PermissionProvider.GetPermission(SessionManager.CurrentUser, "Orders").CanRead)
            //    return Ok();
            var response = new MasterPurchaseOrder();

            try
            {
                if (!string.IsNullOrEmpty(VPO))
                {
                    var vpos = JsonConvert.DeserializeObject<List<int>>(VPO);
                    if (vpos.Count > 0)
                    {
                        foreach (var item in vpos)
                        {
                            var result = PurchaseOrderMgr.CancelVPO(item, id, cancelReason);
                        }
                    }
                }

                //var result = PurchaseOrderMgr.CancelVPO(VPO,id);

                //if (result != "Success")
                //{
                //    return Json(new { error = result, data = "" });
                //}
                //else
                //{
                response = PurchaseOrderMgr.GetMPO(id);
                return Json(new { error = "", data = response });
                //}
                //return Json(new { error = "", data = "" });
            }
            //catch (UnauthorizedAccessException uae)
            //{
            //    return Forbidden(uae.Message);
            //}
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult CancelMPO(int id)
        {
            var response = new MasterPurchaseOrder();
            try
            {
                var result = PurchaseOrderMgr.CancelMPO(id);

                if (result != "Success")
                {
                    return Json(new { error = result, data = "" });
                }
                else
                {
                    response = PurchaseOrderMgr.GetMPO(id);
                    return Json(new { error = "", data = response });
                }
            }
            //catch (UnauthorizedAccessException uae)
            //{
            //    return Forbidden(uae.Message);
            //}
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet]
        public IHttpActionResult CancelXMpo(int id,string cancelReason)
        {
            var response = new XMpoDTO();
            try
            {
                var result = PurchaseOrderMgr.CancelXMpo(id, cancelReason);

                if (result != "Success")
                {
                    return Json(new { error = result, data = "" });
                }
                else
                {
                    response = PurchaseOrderMgr.GetXmpo(id);
                    return Json(new { error = "", data = response });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Get PIC Async status
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult CheckAsyncMPO()
        {
            try
            {
                var result = PurchaseOrderMgr.CheckAsyncMPO();

                if (result)
                {
                    return Json(ContantStrings.Success);
                }
                else
                {
                    return Json(ContantStrings.FailedByError);
                }
            }
            catch (Exception ex)
            {
                return Json(ContantStrings.FailedByError);
            }
        }

        [System.Web.Http.AcceptVerbs("GET")]
        public IHttpActionResult GetMPOHeader(int id)
        {
            try
            {
                var response = PurchaseOrderMgr.GetMPOHeader(id);
                return Json(new { error = "", data = response });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult validateAndSaveConfigurator(int Id, [FromBody] CoreProductVM obj)
        {
            string content = "";
            try
            {
                string response = picMgr.ValidatePromptAnswersforPICConfigurator(obj);
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response);

                if (Convert.ToBoolean(responseObj.valid) == true && responseObj.messages.errors != null && responseObj.messages.errors.Count == 0)
                {
                    if (Convert.ToDecimal(responseObj.cost) == 0 || Convert.ToDecimal(responseObj.list) == 0)
                    {
                        return Json(new
                        {
                            valid = false,
                            message = "Price Validation has failed on this product configuration. Please contact your system administrator.",
                            data = responseObj,
                            QuoteKey = obj.QuoteKey,
                            OpportunityId = obj.OpportunityId
                        });
                    }
                    else
                    {
                        var qld = new QuoteLineDetail();
                        var qldlist = QuotesMgr.GetMargin(obj.QuotelineId);
                        if (qldlist != null && qldlist.Count > 0)
                        {
                            qld = qldlist.FirstOrDefault();
                        }
                        var quoteline = QuotesMgr.getQuoteLinebyId(obj.QuotelineId);
                        var old_quoteline = quoteline;

                        var oldPICResponse = JsonConvert.DeserializeObject<dynamic>(quoteline.PICPriceResponse);

                        if (Convert.ToDecimal(responseObj.list) != Convert.ToDecimal(oldPICResponse.list))
                        {
                            return Json(new
                            {
                                valid = false,
                                message = "Touchpoint was unable to update product configuration. Customer facing price points will be affected.  Please make your product configuration corrections in the quote.",
                                data = responseObj,
                                QuoteKey = obj.QuoteKey,
                                OpportunityId = obj.OpportunityId
                            });
                        }
                        //if (Convert.ToDecimal(responseObj.cost) == Convert.ToDecimal(oldPICResponse.cost))
                        //{
                        var qcpdList = PurchaseOrderMgr.UpdateCoreProductQuoteconfiguration(obj.QuotelineId, obj.QuoteKey, responseObj);

                        quoteline.PICPriceResponse = response;
                        quoteline.Width = obj.Width;
                        quoteline.FranctionalValueWidth = obj.FranctionalValueWidth;
                        quoteline.Height = obj.Height;
                        quoteline.FranctionalValueHeight = obj.FranctionalValueHeight;
                        quoteline.MountType = obj.MountType;
                        quoteline.RoomName = obj.RoomName;
                        quoteline.Quantity = obj.Quantity;

                        if (((IDictionary<string, object>)responseObj).ContainsKey("color"))
                        {
                            quoteline.Color = responseObj.color.Description;
                        }
                        if (((IDictionary<string, object>)responseObj).ContainsKey("fabric"))
                        {
                            quoteline.Fabric = responseObj.fabric.Description;
                        }

                        string desc = string.Empty;
                        if (!string.IsNullOrEmpty(obj.ProductName))
                        {
                            desc = "Product: <b>" + obj.ProductName + "</b>; ";
                        }
                        if (!string.IsNullOrEmpty(obj.ModelDescription) && !string.Equals(obj.ModelDescription, "ALL", StringComparison.OrdinalIgnoreCase))
                        {
                            desc = desc + "Model: <b>" + obj.ModelDescription + "</b>; ";
                        }
                        desc = desc + QuotesMgr.getCoreProductDesciption(qcpdList);

                        if (desc == string.Empty)
                        {
                            quoteline.Description = obj.Description;
                        }
                        else
                        {
                            quoteline.Description = desc;
                        }

                        qld.BaseCost = Convert.ToDecimal(responseObj.cost);
                        //}

                        qld.BaseResalePrice = Convert.ToDecimal(responseObj.list);

                        //////////////////////////////////////////////////////

                        //Apply Currency exchange to the base price and cost
                        //1-Multiply
                        //2 - Divide
                        if (quoteline.CurrencyExchangeId != null)
                        {
                            if (quoteline.ConversionCalculationType == 1)
                            {
                                qld.BaseCost = Convert.ToDecimal(qld.BaseCost * quoteline.ConversionRate);
                            }
                            else if (quoteline.ConversionCalculationType == 2)
                            {
                                qld.BaseCost = Convert.ToDecimal(qld.BaseCost / quoteline.ConversionRate);
                            }
                        }

                        var org_dapList = QuotesMgr.GetDiscountAndPromo(obj.QuoteKey).Where(x => x.QuoteLineId == obj.QuotelineId).ToList();

                        var datetime = DateTime.UtcNow;
                        var user = SessionManager.CurrentUser.PersonId;
                        var dnpList = new List<DiscountsAndPromo>();

                        if (responseObj != null && responseObj.Promos != null && responseObj.Promos.Count > 0)
                        {
                            foreach (var item in responseObj.Promos)
                            {
                                var dnp = new DiscountsAndPromo();
                                dnp.Discount = Convert.ToDecimal(item.retail) + Convert.ToDecimal(item.surch);
                                if (quoteline.CurrencyExchangeId != null)
                                {
                                    //1-Multiply
                                    //2 - Divide
                                    if (quoteline.ConversionCalculationType == 1)
                                    {
                                        dnp.Discount = Convert.ToDecimal(dnp.Discount * quoteline.ConversionRate);
                                    }
                                    else if (quoteline.ConversionCalculationType == 2)
                                    {
                                        dnp.Discount = Convert.ToDecimal(dnp.Discount / quoteline.ConversionRate);
                                    }
                                }
                                dnp.QuoteKey = obj.QuoteKey;
                                dnp.CreatedBy = user;
                                dnp.CreatedOn = datetime;
                                dnp.LastUpdatedBy = user;
                                dnp.LastUpdatedOn = datetime;
                                dnp.Cost = Convert.ToDecimal(responseObj.cost);
                                dnpList.Add(dnp);
                            }
                        }

                        var diffPromos = false;

                        if (org_dapList != null && org_dapList.Count > 0 && dnpList == null)
                        {
                            diffPromos = true;
                        }
                        else if (org_dapList != null && org_dapList.Count > 0 && dnpList == null)
                        {
                            diffPromos = true;
                        }
                        else if (org_dapList != null && org_dapList.Count > 0 && dnpList != null && dnpList.Count > 0)
                        {
                            for (int i = 0; org_dapList.Count <= i; i++)
                            {
                                for (int j = 0; dnpList.Count <= j; j++)
                                {
                                    if (org_dapList[i].QuoteLineId == dnpList[j].QuoteLineId && org_dapList[i].Description == dnpList[j].Description && org_dapList[i].Cost == dnpList[j].Cost && org_dapList[i].PromoStartDate == dnpList[j].PromoStartDate && org_dapList[i].PromoEndtDate == dnpList[j].PromoEndtDate)
                                    {
                                        org_dapList[i].exists = true;
                                    }
                                }
                            }
                            for (int i = 0; dnpList.Count <= i; i++)
                            {
                                for (int j = 0; org_dapList.Count <= j; j++)
                                {
                                    if (org_dapList[i].QuoteLineId == dnpList[j].QuoteLineId && org_dapList[i].Description == dnpList[j].Description && org_dapList[i].Cost == dnpList[j].Cost && org_dapList[i].PromoStartDate == dnpList[j].PromoStartDate && org_dapList[i].PromoEndtDate == dnpList[j].PromoEndtDate)
                                    {
                                        dnpList[i].exists = true;
                                    }
                                }
                            }

                            diffPromos = org_dapList.Where(x => !x.exists).FirstOrDefault().exists;
                            diffPromos = dnpList.Where(x => !x.exists).FirstOrDefault().exists;
                        }
                        if (diffPromos)
                        {
                            if (org_dapList.Where(x => x.DiscountAmountPassed.HasValue && x.DiscountPercentPassed.HasValue).ToList().Count > 0)
                            {
                                return Json(new
                                {
                                    valid = false,
                                    message = "Touchpoint was unable to update product configuration. A change to the vendor promotional discount will affect customer facing price points. Please make your product configuration corrections in the quote.",
                                    data = responseObj,
                                    QuoteKey = obj.QuoteKey,
                                    OpportunityId = obj.OpportunityId
                                });
                            }
                        }
                        quoteline.QuoteLineDetail = qld;
                        PurchaseOrderMgr.SavequoteData(quoteline, Id);
                        PurchaseOrderMgr.AuditMPOEditErrorConfig(quoteline, Id, old_quoteline);

                        var qData = QuotesMgr.GetQuoteDetailsforCoreConfiguration(obj.QuoteKey);
                        var QuoteLinesVM = QuotesMgr.GetQuoteLine(obj.QuoteKey);
                        if (QuoteLinesVM != null && QuoteLinesVM.Count > 0)
                        {
                            qData.QuoteLinesVM = QuoteLinesVM.Where(x => x.ProductTypeId == 1).ToList();
                        }
                            var poDetails = PurchaseOrderMgr.GetPurchaseOrderDetailsLines(Id);
                            if (poDetails != null && poDetails.Count > 0)
                            {
                                foreach (var item in poDetails)
                                {
                                    QuoteLinesVM.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault().ConfigErrors = item.Errors;
                                    if (item.PICPO.HasValue)
                                    {
                                        QuoteLinesVM.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault().PICPO = item.PICPO.Value; ;
                                    }
                                }
                            }
                        qData.QuoteLinesList = QuotesMgr.GetCoreConfigQuoteLines(obj.QuoteKey);

                        return Json(new
                        {
                            valid = true,
                            data = responseObj,
                            QuoteKey = obj.QuoteKey,
                            OpportunityId = obj.OpportunityId,
                            QuoteData = qData
                        });

                        /////////////////////////////////////////////////////

                        //var qlVM = new QuoteLinesVM();
                        //if (obj.QuotelineId == 0)
                        //{
                        //    var ql = QuotesMgr.InsertEmptyQuoteline(obj.QuoteKey);
                        //    obj.QuotelineId = ql.QuoteLineId;
                        //    qlVM.QuoteLineId = ql.QuoteLineId;
                        //}
                        //else
                        //{
                        //    qlVM.QuoteLineId = obj.QuotelineId;
                        //}
                    }
                }
                else
                {
                    return Json(new
                    {
                        valid = true,
                        data = responseObj,
                        QuoteKey = obj.QuoteKey,
                        OpportunityId = obj.OpportunityId
                    });
                }
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }

                return Json(new { valid = false, messages = msg });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult SyncAllVPOStatus()
        {
            try
            {
                var result = PurchaseOrderMgr.SyncVPOStatus();

                if (result)
                {
                    return Json(ContantStrings.Success);
                }
                else
                {
                    return Json(ContantStrings.FailedByError);
                }
            }
            catch (Exception ex)
            {
                return Json(ContantStrings.FailedByError);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult SaveNotestoVendor(int id, VPODetail model)
        {
            try
            {
                var response = PurchaseOrderMgr.SaveNotesToVendor(model);

                if (response)
                {
                    return Json(new { error = "" });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveMyProductInlineVPO(int id, VPODetail model)
        {
            try
            {
                var response = PurchaseOrderMgr.SaveMyProductInlineVPO(model);
                var result = PurchaseOrderMgr.GetMPO(model.PurchaseOrderId);
                if (response)
                {
                    return Json(new { error = "", data = result });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Update and confirm ship address
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult AddUpdateShipAddress(int id, MasterPurchaseOrder model)
        {
            try
            {
                var response = PurchaseOrderMgr.AddUpdateShipAddress(model);
                var result = PurchaseOrderMgr.GetMPO(model.PurchaseOrderId);
                if (response)
                {
                    return Json(new { error = "", data = result });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        [HttpPost]
        public IHttpActionResult OnCancelUpdateStatus(int id, MasterPurchaseOrder model)
        {
            try
            {
                PurchaseOrderMgr.UpdateMasterPOCancelReason(model);
                PurchaseOrderMgr.CancelMPO(model.PurchaseOrderId);

                //When an mpo/xmpo cancelled if all the orderline items are in Open Status,
                //the order status also become Open otherwise it it may be in Purchase Order.
                //ordermgr.UpdateOrderStatus(model.OrderId, 6);
                ordermgr.UpdateOrderStatusToOpen(model.OrderId); //7 - Open


                //if (model.CancelReason.Equals("Customer Cancellation"))
                //{
                //    ordermgr.UpdateOrderStatus(model.OrderId, 6);
                //    QuotesMgr.UpdateQuotesStatus(model.QuoteId, 4);
                //    oppMgr.UpdateOpportunityStatus(model.OpportunityId, 3);
                //}
                //else if (model.CancelReason.Equals("Incorrect Measurement / Product Configuration") || model.CancelReason.Equals("Entered  Incorrectly"))
                //{
                //    ordermgr.UpdateOrderStatus(model.OrderId, 9);
                //    QuotesMgr.UpdateQuotesStatus(model.QuoteId, 2);
                //    oppMgr.UpdateOpportunityStatus(model.OpportunityId, 3);
                //}
                //PurchaseOrderMgr.CancelMPO(model.PurchaseOrderId);
                var orderData = ordermgr.GetOrderByOrderId(model.OrderId);

                var mpo = PurchaseOrderMgr.GetMPO(model.PurchaseOrderId);

                return Json(new { error = "", data = orderData, MPO = mpo });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }
        [HttpPost]
        public IHttpActionResult AddUpdateXmpoShipAddress(int id, XMpoDTO model)
        {
            try
            {
                var response = PurchaseOrderMgr.AddUpdateXmpoShipAddress(model);
                var result = PurchaseOrderMgr.GetXmpo(model.PurchaseOrderId);
                if (response)
                {
                    return Json(new { error = "", data = result });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch(Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }
        [HttpGet]
        public IHttpActionResult GetMPORelatedIds(int id)
        {
            try
            {
                var result = PurchaseOrderMgr.GetMPORelatedIds(id);
                if (result!=null)
                {
                    return Json(new { error = "", data = result });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult SyncAllDBVPOPICResponse(int id)
        {
            try
            {
                var result = PurchaseOrderMgr.SyncDBVPOPICResponse();

                if (result)
                {
                    return Json(ContantStrings.Success);
                }
                else
                {
                    return Json(ContantStrings.FailedByError);
                }
            }
            catch (Exception ex)
            {
                return Json(ContantStrings.FailedByError);
            }
        }
    }
}