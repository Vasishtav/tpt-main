﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using System;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class InvoicesController : BaseApiController
    {
        public class GetParams
        {
            public DateTimeOffset? startDate { get; set; }
            public DateTimeOffset? endDate { get; set; }
            public InvoiceStatusEnum[] statusEnum { get; set; }
            public string orderBy { get; set; }
            public OrderByEnum orderDir { get; set; }
            public int pageNum { get; set; }
            public int pageSize { get; set; }
            public string searchTerm { get; set; }
            public InvoiceSearchFilterEnum searchFilter { get; set; }
        }

        private InvoiceManager InvoiceMgr = new InvoiceManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public IHttpActionResult Get([FromUri]GetParams param)
        {
            int totalRecords = 0;
            try
            {
                var invoices = InvoiceMgr.Get(out totalRecords, param.searchFilter, param.searchTerm, param.statusEnum, param.startDate, param.endDate, param.pageNum, param.pageSize, param.orderBy, param.orderDir);

                //do a little clean up so we dont have to send too many unnecessary data
                foreach (var invoice in invoices)
                {
                    invoice.JobQuote.Job = null;
                    invoice.Job.JobQuotes = null;
                }

                return Ok(new
                {
                    Invoices = invoices,
                    TotalRecords = totalRecords
                });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                var invoice = InvoiceMgr.Get(id);
                if (invoice != null)
                    return Ok(InvoiceMgr.Get(id));
                else
                    return ResponseResult("Not Exist");
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpPost]
        public IHttpActionResult Post(InvoiceModel InvoiceModel)
        {
            return Json(
                InvoiceMgr.BillQuote(InvoiceModel.quoteId, InvoiceModel.jobId)
                );
        }

        [HttpDelete, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Delete(int id)
        {
            var status = InvoiceMgr.Delete(id);
            return ResponseResult(status);
        }

    }
}
