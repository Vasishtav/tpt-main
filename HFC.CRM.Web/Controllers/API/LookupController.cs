﻿using HFC.CRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using HFC.CRM.Cache.Cache;
using HFC.CRM.DTO.Source;
using HFC.CRM.Managers;
using HFC.CRM.Core;
using HFC.CRM.Data.Geolocator;
using HFC.CRM.Data;
using HFC.CRM.DTO.Lookup;
using HFC.CRM.Web.Filters;
using System.Data.SqlClient;
using HFC.CRM.Data.Context;
using Dapper;
using StackExchange.Profiling.Helpers.Dapper;
using HFC.CRM.Web.Models;
using HFC.Common.Logging;
using System.Dynamic;
using HFC.CRM.Web.Models.Measurement;
using HFC.CRM.Core.Membership;
using HFC.CRM.DTO.Product;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class LookupController : BaseApiController
    {
        private readonly ILog _Log = BaseLog.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private StatusChangesManager statusChangesMgr = new StatusChangesManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        private LookupManager Lookup_manager = new LookupManager();

        public class SaleAdjCategory
        {
            public string Category { get; set; }
            public string Label { get; set; }
            public decimal Value { get; set; }
        }

        /// <summary>
        /// Gets a list of category or specific category by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Category(int id)
        {
            if (id > 0)
            {
                var cat = SessionManager.CurrentFranchise.ProductCategoryCollection(id);
                return Json(cat != null ? new { Id = cat.Id, Name = cat.Category } : null);
            }
            else
            {
                return Json(SessionManager.CurrentFranchise.ProductCategoryCollection());
            }
        }

        [HttpGet]
        public IHttpActionResult SaleAdj(int id)
        {
            var taxRules = SessionManager.CurrentFranchise.GetTaxRules();

            var saCategories = new List<SaleAdjCategory>
                               {
                                   new SaleAdjCategory { Label = "Surcharge", Category = "Installation" },
                                   new SaleAdjCategory { Label = "Surcharge", Category = "Labor" },
                                   new SaleAdjCategory { Label = "Surcharge", Category = "Mileage" },
                                   new SaleAdjCategory { Label = "Surcharge", Category = "Misc/Other" },
                                   new SaleAdjCategory { Label = "Surcharge", Category = "Shipping" },
                                   new SaleAdjCategory { Label = "Discount", Category = "Coupon" },
                                   new SaleAdjCategory { Label = "Discount", Category = "Credit" },
                                   new SaleAdjCategory { Label = "Discount", Category = "Discount" },
                                   new SaleAdjCategory { Label = "Discount", Category = "Gift Certificate" }
                                   //,
                                   //new SaleAdjCategory { Label = "Tax", Category = "Base Tax" }
                               };
            decimal baseTax = 0;
            if (taxRules.FirstOrDefault(v => v.County == null && v.ZipCode == null && v.Default == true) != null)
            {
                baseTax = taxRules.FirstOrDefault(v => v.County == null && v.ZipCode == null).Percent;
            }
            saCategories.Add(new SaleAdjCategory { Label = "Tax", Category = "Base Tax", Value = baseTax });

            foreach (var tax in taxRules.Where(v => v.County != null))
            {
                saCategories.Add(new SaleAdjCategory { Label = "Tax", Category = tax.RuleName, Value = tax.Percent });
            }
            saCategories.Add(new SaleAdjCategory { Label = "Tax", Category = "County" });

            foreach (var tax in taxRules.Where(v => v.County == null && v.ZipCode != null))
            {
                saCategories.Add(new SaleAdjCategory { Label = "Tax", Category = string.Format("{0}", tax.RuleName), Value = tax.Percent });
            }
            saCategories.Add(new SaleAdjCategory { Label = "Tax", Category = "City" });

            foreach (var tax in taxRules.Where(v => v.County == null && v.ZipCode == null && v.City == null && (v.Default == false || v.Default == null)))
            {
                saCategories.Add(new SaleAdjCategory { Label = "Tax", Category = string.Format("{0}", tax.RuleName), Value = tax.Percent });
            }
            return Json(new
            {
                SaleAdjTypeEnum = SaleAdjTypeEnum.Surcharge.ToDictionary<byte>(),
                SaleAdjCategories = saCategories
            });
        }

        /// <summary>
        /// Gets a list of territory or specific territory by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Territory(int id)
        {
            if (id > 0)
            {
                var ter = SessionManager.CurrentFranchise.Territories.SingleOrDefault(w => w.TerritoryId == id);
                return Json(ter != null ? new { Id = ter.TerritoryId, Name = ter.Name } : null);
            }
            else
                return Json(SessionManager.CurrentFranchise.Territories.Select(s => new { Id = s.TerritoryId, Name = s.Name }));
        }

        /// <summary>
        /// Gets list of sales agent or specific agent by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult SalesAgent(int id)
        {
            if (id > 0)
            {
                var per = SessionManager.CurrentFranchise.UserCollection(id);
                return Json(per != null ? new { Id = per.PersonId, Name = per.Person.FullName } : null);
            }
            else
                return Json(SessionManager.CurrentFranchise.UserCollection(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId).Select(s => new { Id = s.PersonId, Name = s.Person.FullName }));
        }

        /// <summary>
        /// Gets the parent lead status or specific lead status by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult LeadStatus(int id)
        {
            var statuses = SessionManager.CurrentFranchise.LeadStatusTypeCollection();
            var lstStatusId = statusChangesMgr.ValidStatus(1, id);
            var validStatusList = statuses.Where(x => lstStatusId.Contains(x.Id)).ToList();

            var finalStatusValue = validStatusList.Select(s => new { s.Id, s.Name }).ToList();
            finalStatusValue.RemoveAll(x => x.Id == 31422);
            return Json(finalStatusValue);
        }

        /// <summary>
        /// Get size of attachments attahched to common Email feature
        /// </summary>
        /// <param name="streamId"></param>
        /// <returns>size in MB</returns>
        [HttpGet]
        public IHttpActionResult GetSizeAttachment(string streamId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //string streamidJoined = String.Join(", ", streamId);
                    string[] id = streamId.Split(',');
                    var query = @"select round((CAST(cached_file_size AS float)/1024)/1024,2) as cached_file_size from FileTableTb where stream_id in ('" + streamId.Trim() + "') ";
                    // var query = @" select round((CAST(sum(cached_file_size) AS float)/1024)/1024,2) as sum from FileTableTb where stream_id in ('" + streamId.Trim() + "') ";
                    connection.Open();
                    var response = connection.Query(query).ToList();
                    connection.Close();
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Get attachments for common  Email feature
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetAttachmentList(int id, string type)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = "";
                    //var query = @"Declare @selected bit = 0
                    //              select title,AttachmentStreamId, ft.name as FileName,(select @selected  )as status  from crm.note n left join filetabletb ft
                    //               on ft.stream_id = n.AttachmentStreamId   ";
                    //if (type == "Opportunity")
                    //    query += "where OpportunityId = @value and IsNote=0 and AttachmentStreamId is not null";
                    //else if (type == "Account")
                    //    query += "where AccountId = @value and IsNote=0 and AttachmentStreamId is not null";
                    //else if(type == "Lead")
                    //    query += "where leadid = @value and IsNote=0 and AttachmentStreamId is not null";
                    //else
                    //    query += "where OrderId = @value and IsNote=0 and AttachmentStreamId is not null";
                    if (type == "Lead")
                    {
                        query = @"select title, AttachmentStreamId, ft.name as FileName,0 as status from CRM.Note nop
                                left join dbo.FileTableTb ft on ft.stream_id = nop.AttachmentStreamId
                                where nop.LeadId = @value and IsNote = 0 and AttachmentStreamId is not null";
                    }
                    else if (type == "Account")
                    {
                        query = @"select title,AttachmentStreamId, ft.name as FileName,0 as status from CRM.Note nop
                                 join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                 where nop.OrderId in
                                 (select OrderId from CRM.Orders o
                                 join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                                 join CRM.Accounts acc on op.AccountId = acc.AccountId
                                 where acc.AccountId=@value)
                                 and IsNote=0 and AttachmentStreamId is not null
                                 union
                                 select title,AttachmentStreamId, ft.name as FileName,0 as status
                                 from CRM.Note nop
                                 join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                 where nop.OpportunityId in (select OpportunityId from CRM.Opportunities where AccountId=@value)
                                 and IsNote=0 and AttachmentStreamId is not null
                                 union
                                 select title,AttachmentStreamId, ft.name as FileName,0 as status
                                 from CRM.Note nop
                                 left join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                 where nop.AccountId=@value and IsNote=0 and AttachmentStreamId is not null";
                    }
                    else if (type == "Opportunity")
                    {
                        query = @"select title,AttachmentStreamId, ft.name as FileName,0 as status from CRM.Note nop
                                  join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                  where nop.AccountId in (select AccountId from CRM.Opportunities where OpportunityId=@value)
                                  and IsNote=0 and AttachmentStreamId is not null
                                  union
                                  select title,AttachmentStreamId, ft.name as FileName,0 as status
                                  from CRM.Note nop
                                  left join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                  where nop.OpportunityId=@value and IsNote=0 and AttachmentStreamId is not null
                                  union
                                  select title,AttachmentStreamId, ft.name as FileName,0 as status
                                  from CRM.Note nop
                                  join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                  where nop.OrderId in
                                  (select OrderId from CRM.Orders o
                                  join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                                  where op.OpportunityId=@value)
                                  and IsNote=0 and AttachmentStreamId is not null";
                    }
                    else
                    {
                        query = @"select title,AttachmentStreamId, ft.name as FileName,0 as status from CRM.Note nop
                                  join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                  where nop.AccountId in
                                  (select AccountId from CRM.Orders o
                                  join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                                  where OrderId=@value)
                                  and IsNote=0 and AttachmentStreamId is not null
                                  union
                                  select title,AttachmentStreamId, ft.name as FileName,0 as status
                                  from CRM.Note nop
                                  join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                  where nop.OpportunityId in (select OpportunityId from CRM.Orders where OrderId=@value)
                                  and IsNote=0 and AttachmentStreamId is not null
                                  union
                                  select title,AttachmentStreamId, ft.name as FileName,0 as status
                                  from CRM.Note nop
                                  left join  dbo.FileTableTb ft  on ft.stream_id = nop.AttachmentStreamId
                                  where nop.OrderId=@value and IsNote=0 and AttachmentStreamId is not null";
                    }
                    connection.Open();
                    var response = connection.Query(query, new { value = id }).ToList();
                    connection.Close();
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetPrimaryEmail(int id, string type)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"select case when PrimaryEmail is null then ''
                                  when PrimaryEmail = '' then ''
			                      else PrimaryEmail end as PrimaryEmail
                                  from CRM.Opportunities op
                                  Inner join CRM.Accounts acc on op.AccountId = acc.AccountId
                                  Inner join CRM.AccountCustomers acccus on acc.AccountId = acccus.AccountId and acccus.IsPrimaryCustomer = 1
                                  Inner join[CRM].[Customer]  cus on cus.CustomerId=acccus.CustomerId
								  left join [CRM].[Orders] o on op.OpportunityId=o.OpportunityId ";
                    if (type == "Opportunity")
                        query += "where op.OpportunityId = @value";
                    else if (type == "Account")
                        query += "where acc.AccountId = @value";
                    else
                        query += "where o.OrderID = @value";

                    query += " AND acc.Franchiseid = " + SessionManager.CurrentFranchise.FranchiseId;

                    connection.Open();
                    var response = connection.Query(query, new { value = id }).ToList();
                    connection.Close();
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult AccountStatus(int id)
        {
            //  var result="";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    if (id > 0)
                    {
                        connection.Open();
                        var query = "select * from [CRM].[Type_AccountStatus] where leadstatusid = @id";
                        var result = connection.Query(query, new { id = id }).ToList();
                        connection.Close();
                        return Json(result);
                    }
                    else
                    {
                        connection.Open();
                        var query = "select * from CRM.Type_AccountStatus";
                        var result = connection.Query(query).ToList();
                        connection.Close();
                        return Json(result);
                    }
                }
                catch (Exception e)
                {
                    _Log.Error(e.Message, e);
                    return ResponseResult(e.Message);
                }
            }
        }

        /// <summary>
        /// Gets the parent job status or specific job status by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult JobStatus(int id)
        {
            if (id > 0)
            {
                var status = SessionManager.CurrentFranchise.JobStatusTypeCollection(id);
                return Json(status != null ? new { status.Id, status.Name } : null);
            }
            else
            {
                var statuses = SessionManager.CurrentFranchise.JobStatusTypeCollection();
                return Json(statuses.Where(w => !w.ParentId.HasValue).Select(s => new { s.Id, s.Name }));
            }
        }

        
        [HttpGet, Obsolete]
        public IHttpActionResult GetCampaignByFranchise()
        {
            try
            {
                int franchiseid = 0;

                if (SessionManager.CurrentFranchise != null)
                {
                    franchiseid = SessionManager.CurrentFranchise.FranchiseId;
                }
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"SELECT ow.Name AS Owner, tc.Name AS Channel, s.Name AS Source, c.Name AS Campaign,
                                CONVERT(date, c.StartDate ) AS CampaignStartDate,CONVERT(date, c.EndDate) AS CampaignEndDate,
                                c.IsDeleted AS CampaignIsDeleted, c.IsActive AS CampaignIsActive, c.SourcesTPId,
                                c.CampaignId,c.Memo,c.Amount FROM CRM.SourcesTP st left JOIN
                                crm.Campaign c  ON st.SourcesTpId = c.SourcesTpId INNER JOIN CRM.Sources s ON
                                s.sourceId = st.sourceId INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId INNER JOIN
                                CRM.Type_Owner ow ON ow.ownerid = st.ownerid 
                                where c.Franchiseid = @franchiseid 
                                    and ( c.IsActive !=0 and c.IsActive is not null)
	                                and isnull(c.IsDeleted, 0) = 0;";
                    connection.Open();
                    var response = connection.Query(query, new { franchiseid = franchiseid }).ToList();
                    connection.Close();
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        

        [HttpGet]
        public IHttpActionResult GetCampaignByFranchiseInactiveData()
        {
            try
            {
                int franchiseid = 0;

                if (SessionManager.CurrentFranchise != null)
                {
                    franchiseid = SessionManager.CurrentFranchise.FranchiseId;
                }
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"SELECT ow.Name AS Owner, tc.Name AS Channel, s.Name AS Source, c.Name AS Campaign,
                                c.StartDate AS CampaignStartDate, c.EndDate AS CampaignEndDate,
                                c.IsDeleted AS CampaignIsDeleted, c.IsActive AS CampaignIsActive, c.SourcesTPId,
                                c.CampaignId,c.Memo,c.Amount FROM CRM.SourcesTP st left JOIN
                                crm.Campaign c  ON st.SourcesTpId = c.SourcesTpId INNER JOIN CRM.Sources s ON
                                s.sourceId = st.sourceId INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId INNER JOIN
                                CRM.Type_Owner ow ON ow.ownerid = st.ownerid where c.Franchiseid = @franchiseid;";
                    connection.Open();
                    var response = connection.Query(query, new { franchiseid = franchiseid }).ToList();
                    connection.Close();
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllSources()
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @" SELECT st.SourcesTPId,   s.Name as Source, tc.Name AS Channel, ow.Name as Owner FROM crm.SourcesTP st INNER JOIN CRM.Sources s ON s.sourceId = st.sourceId INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId INNER JOIN CRM.Type_Owner ow ON ow.ownerid = st.ownerid;";
                    connection.Open();
                    var response = connection.Query(query).ToList();
                    connection.Close();
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllDropdownValues()
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"Select SourcesHFCId,typo.Name+' -> '+typc.Name+' -> '+stp.Name as HFCSourceName
                                  from CRM.SourcesHFC SHF
                                  inner join CRM.Type_Owner typo on typo.OwnerId=SHF.OwnerId
                                  inner join CRM.Type_Channel typc on typc.ChannelId=SHF.ChannelId
                                  inner join CRM.Sources stp on stp.SourceId=SHF.SourceId
                                  where SHF.IsActive=1 order by SourcesHFCId";
                    connection.Open();
                    var response = connection.Query(query).ToList();
                    connection.Close();
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetSourcePickList()
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    dynamic obj = new ExpandoObject();
                    var queryOwner = @"select OwnerId,[Name],IsActive from CRM.Type_Owner order by Name Asc";
                    var queryChannel = @"select ChannelId,[Name],IsActive from CRM.Type_Channel order by Name Asc";
                    var querySources = @"select SourceId,[Name],IsActive from CRM.Sources order by Name Asc";
                    obj.Owner = connection.Query(queryOwner).ToList();
                    obj.Channel = connection.Query(queryChannel).ToList();
                    obj.Source = connection.Query(querySources).ToList();
                    connection.Close();
                    return Json(obj);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetRelatedAccounts(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"SELECT
                                  [AccountId]
                                  ,(select [CRM].[fnGetAccountNameByAccountId](AccountId)) as FullName
                                  FROM CRM.[Accounts]
                                  WHERE FranchiseId= @FranchiseId AND AccountId!=@accountid AND ISNULL(IsDeleted,0)=0 order by isnull(LastUpdatedOnUtc,CreatedOnUtc) desc";
                    int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    connection.Open();
                    var response = connection.Query(query, new { FranchiseId = FranchiseId, accountid = id }).ToList();

                    connection.Close();
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult SourceTp(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select stp.SourcesTPId as SourceId, s.Name as SourceName
                            ,c.Name as ChannelName,o.name + ' - ' + c.Name + ' - ' + s.Name as name
                            , o.name as ownerName
                            from crm.SourcesTP stp
                            join crm.Type_Channel c on c.ChannelId = stp.channelid AND ISNULL(stp.IsActive,0)=1 AND ISNULL(stp.IsDeleted,0)=0 AND ISNULL(c.IsActive,0)=1 AND ISNULL(c.IsDeleted,0)=0
                            join crm.sources s on s.SourceId = stp.SourceId AND ISNULL(s.IsActive,0)=1 AND ISNULL(s.IsDeleted,0)=0
                            join crm.type_owner o on o.OwnerId = stp.OwnerId AND ISNULL(o.IsActive,0)=1 AND ISNULL(o.IsDeleted,0)=0";
                connection.Open();

                if (id > 0)
                {
                    query += @" join crm.Campaign ca on ca.SourcesTPId = stp.SourcesTPId AND ISNULL(ca.IsActive,0)=1 AND ISNULL(ca.IsDeleted,0)=0
                            where ca.CampaignId =@campaignId order by name asc";
                    var result1 = connection.Query(query, new { campaignId = id }).ToList();
                    connection.Close();
                    return Json(result1);
                }
                else
                {
                    query += @" order by name asc";
                    var result = connection.Query(query).ToList();
                    connection.Close();
                    return Json(result);
                }
                //var query = "select a.Name + ' - ' + b.Name as name, convert(varchar,a.SourceId)+ ' | ' + convert(varchar,b.SubsourceId) as Value  from CRM.Sources a join CRM.Subsource b on b.SourceId = a.SourceId";
            }
        }

        [HttpGet]
        public IHttpActionResult Type_Source(int id)
        {
            if (id > 0)
            {
                var result = FranchiseManager.GetSourcesThree();
                result = result.Where(x => x.ParentId == id).ToList();
                return Json(result);
            }
            else
            {
                var result = FranchiseManager.GetSourcesThree();
                result = result.Where(x => x.ParentId == null).ToList();

                return Json(result.Where(w => !w.ParentId.HasValue).Select(s => new { s.SourceId, s.Name }));
            }
        }

        /// <summary>
        /// Gets product by manufacturer Id
        /// </summary>
        /// <param name="id">Manufacturer Id</param>
        /// <returns></returns>
        ///
        [HttpGet]
        public IHttpActionResult Products(int id)
        {
            if (string.IsNullOrEmpty(AppConfigManager.HostDomain))
                return null;
            else
            {
                switch (AppConfigManager.HostDomain)
                {
                    case "budgetblinds":
                        return ProductBB(id);

                    case "tailoredliving":
                        return ProductTL(id);

                    case "concretecraft":
                        return ProductTL(id);

                    case "":
                    default:
                        return null;
                }
            }
        }

        [HttpGet]
        public IHttpActionResult CustomProducts(int id)
        {
            try
            {
                var ProductTypes = this.CRMDBContext.Type_Products.Where(w => !w.isCustom.Value).Select(s => new { s.ProductTypeId, s.TypeName }).Distinct().OrderBy(c => c.TypeName).ToList();

                var Manufacturers = this.CRMDBContext.Manufacturers.Where(w => !w.isCustom.Value).Select(c => c.Name).Distinct().ToList();
                Manufacturers = Manufacturers.OrderBy(c => c).ToList();
                var vendors = this.CRMDBContext.Vendors.Where(w => w.IsManual.HasValue && w.IsManual == true && w.IsActive == false).Select(s => s.Name).ToList();
                Manufacturers = Manufacturers.Except(vendors).ToList();
                return Json(new { ProductTypes, Manufacturers });
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        private IHttpActionResult ProductBB(int id)
        {
            var mgr = new ProductsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            try
            {
                var vendorGuids = (from v in this.CRMDBContext.Vendors where v.IsActive.Value select v.VendorGuid).ToList();

                var mfgContacts = (from c in this.CRMDBContext.ManufacturerContacts where c.FranchiseId == mgr.Franchise.FranchiseId && !c.IsActive.Value select c.ManufacturerId).ToList();

                var list = (from p in this.CRMDBContext.Products.Include("Type_Product").Include("Manufacturer")
                            where
                                (!p.isCustom.Value && p.IsActive)
                                && (p.ManufacturerId.HasValue && vendorGuids.Contains(p.Manufacturer.ManufacturerGuid) && !mfgContacts.Contains(p.Manufacturer.ManufacturerId))
                            select
                                new
                                {
                                    p.ProductGuid,
                                    p.Name,
                                    ProductType = new { p.ProductType.ProductTypeId, p.ProductType.TypeName },
                                    Manufacturer = new { p.Manufacturer.ManufacturerId, p.Manufacturer.Name },
                                    ProductRev = p.Revision,
                                    ProductLastUpdated = p.LastUpdated
                                }).ToList();

                return Json(list);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        private IHttpActionResult ProductTL(int id)
        {
            var mgr = new ProductsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            try
            {
                var products = mgr.GetProducts(id);
                if (products != null)
                {
                    return Json(products.Select(s => new
                    {
                        s.ProductId,
                        s.CategoryId,
                        CategoryName = CacheManager.GetCategoryName(s.CategoryId ?? 0),
                        s.ProductTypeId,
                        ProductType = CacheManager.GetProductTypeName(s.ProductTypeId ?? 0),
                        s.IsTaxable,
                        s.MSRP,
                        s.ListPrice,
                        s.JobberPrice,
                        s.UnitCost,
                        s.JobberDiscountPercent
                    }).OrderBy(o => o.ProductType));
                }
                else
                    return Ok();
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Gets county for USA
        /// </summary>
        /// <param name="id">Use any value - This is ignored</param>
        /// <param name="q">Search query</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult County(int id, string q)
        {
            using (var db = new GeoLocatorEntities())
            {
                var list = db.ZipPostalCodes.Where(w => w.County.StartsWith(q)).Select(s => new { id = s.County, text = s.County }).Distinct().Take(10).ToList();
                return Json(new
                {
                    counties = list
                    //, totalRecords = xxx
                    //, pageNum = xxx
                });
            }
        }

        [HttpGet]
        public IHttpActionResult TerritoryByZipcode(string zip)
        {
            using (var db = new GeoLocatorEntities())
            {
                if (SessionManager.CurrentFranchise.CountryCode == "US")
                {
                    return Json(new { Territory = db.TerritoryZipCodes.FirstOrDefault(w => w.Zip == zip) });
                }

                if (SessionManager.CurrentFranchise.CountryCode == "CA")
                {
                    return Json(new { Territory = db.TerritoryPostalCodes.FirstOrDefault(w => w.Zip == zip) });
                }

                throw new Exception("Unsupported country code");
            }
        }

        /// <summary>
        /// Gets zip code or postal code
        /// </summary>
        /// <param name="id">Use any value - this is ignored</param>
        /// <param name="q">Search query</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult ZipCodes(int id, string q, string country)
        {
            // using (var db = new GeoLocatorEntities())
            //{
            //bool isCanada = country == "CA";
            //var query = "select c.* from gisadmin.HFCCanada c where c.Zip like @q";
            //if (string.IsNullOrEmpty(q))
            //{
            //    q = q.Replace(" ", "");
            //}

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmGeoLocatorConnectionString))
                {
                    connection.Open();

                    var list = connection.Query<ZipPostalCode>("Select z.* from CRM.ZipCodes z where z.ZipCode like @q", new { q = string.Format($"{q}%") }).
                            Select(s => new { id = s.ZipCode, text = s.ZipCode, City = s.PrimaryCity, State = s.State, s.CountryCode }).
                            Distinct().
                            Take(10).
                            ToList();

                    //var list = isCanada
                    //    ? connection.Query<TerritoryPostalCode>(query, new { q = string.Format($"{q}%") }).ToList().Select(s => new { id = s.Zip, text = s.Zip, City = s.PO_NAME, State = s.State, CountryCode = SessionManager.CurrentFranchise.CountryCode }).
                    //       Distinct().
                    //       Take(10).
                    //       ToList()
                    //    : connection.Query<ZipPostalCode>("Select z.* from CRM.ZipCodes z where z.ZipCode like @q", new { q = string.Format($"{q}%") }).
                    //        Select(s => new { id = s.ZipCode, text = s.ZipCode, City = s.PrimaryCity, State = s.State, s.CountryCode }).
                    //        Distinct().
                    //        Take(10).
                    //        ToList();

                    //if (list.Count == 0 && isCanada && q.Length > 3)
                    //{
                    //    q = q.Substring(0, 3);
                    //    list = connection.Query<TerritoryPostalCode>(query, new { q = string.Format($"{q}%") }).
                    //      ToList().
                    //      Select(s => new { id = s.Zip, text = s.Zip, City = s.PO_NAME, State = s.State, CountryCode = SessionManager.CurrentFranchise.CountryCode }).
                    //      Distinct().
                    //      Take(10).
                    //      ToList();
                    //}
                    connection.Close();

                    return Json(new
                    {
                        zipcodes = list
                        //, totalRecords = xxx
                        //, pageNum = xxx
                    });
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public IHttpActionResult ZipCodesforFranchise(int id, string q, string country)
        {
            //bool isCanada = country == "CA";
            //var query = "select c.* from gisadmin.HFCCanada c where c.Zip like @q";
            //if (string.IsNullOrEmpty(q))
            //{
            //    q = q.Replace(" ", "");
            //}
            //query += @" or c.Zip like @q";

            using (var connection = new SqlConnection(ContextFactory.CrmGeoLocatorConnectionString))
            {
                connection.Open();

                var list = connection.Query<ZipPostalCode>("Select z.* from CRM.ZipCodes z where z.ZipCode like @q", new { q = string.Format($"{q}%") }).
                            Select(s => new { id = s.ZipCode, text = s.ZipCode, City = s.PrimaryCity, State = s.State, s.CountryCode }).
                            Distinct().
                            Take(10).
                            ToList();

                //var list = isCanada
                //        ? connection.Query<TerritoryPostalCode>(query, new { q = string.Format($"{q}%") }).ToList().Select(s => new { id = s.Zip, text = s.Zip, City = s.PO_NAME, State = s.State, CountryCode = country }).
                //           Distinct().
                //           Take(10).
                //           ToList()
                //        : connection.Query<ZipPostalCode>("Select z.* from CRM.ZipCodes z where z.ZipCode like @q", new { q = string.Format($"{q}%") }).
                //            Select(s => new { id = s.ZipCode, text = s.ZipCode, City = s.PrimaryCity, State = s.State, s.CountryCode }).
                //            Distinct().
                //            Take(10).
                //            ToList();

                //if (list.Count == 0 && isCanada && q.Length > 3)
                //{
                //    q = q.Substring(0, 3);
                //    list = connection.Query<TerritoryPostalCode>(query, new { q = string.Format($"{q}%") }).
                //      ToList().
                //      Select(s => new { id = s.Zip, text = s.Zip, City = s.PO_NAME, State = s.State, CountryCode = country }).
                //      Distinct().
                //      Take(10).
                //      ToList();
                //}
                connection.Close();

                return Json(new
                {
                    zipcodes = list
                });
            }
        }

        [HttpGet]
        public IHttpActionResult SalesStep(int leadId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var query = "select * from crm.SalesStep where leadid = @leadId";
                    var result = connection.Query<Disposition>(query, new { leadstatusid = leadId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        // TODO: this api will be obsolete soon, as it is missusing the usual pattern
        // in this api. A new api is exposed, please use GetDispositionsTP instead.
        /// <summary>
        /// Gets list of Disposition
        /// </summary>
        /// <param name="id">Lead Status id from crm.Type_LeadStatus</param>
        /// <returns></returns>
        [HttpGet]
        [Obsolete("This api is deprecated, please use GetDispositionsTP instead.")]
        public IHttpActionResult Dispositions(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var query = "select * from crm.disposition where leadstatusid = @leadstatusid";
                    var result = connection.Query<Disposition>(query, new { leadstatusid = id }).ToList();

                    connection.Close();

                    return Json(result);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Gets list of Dispositions or a speicifc Disposition
        /// </summary>
        /// <param name="id">Dispoition Id, when 0 all the recoreds returned
        /// otherwise the speicified record</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetDispositionsTP(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var query = "select * from crm.disposition ";// where leadstatusid = @leadstatusid";
                    if (id > 0) query += "where dispositionid = @dpid";

                    var result = connection.Query<Disposition>(query, new { dpid = id }).ToList();

                    connection.Close();

                    return Json(result);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        [HttpGet]
        public IHttpActionResult GetDispositions()
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    dynamic DyObj = new ExpandoObject();
                    DyObj.DispositionId = 0;
                    DyObj.QuickDisposition = "Qualified - Set Appointment";

                    var query = @"SELECT disp.DispositionId, CONCAT(tls.Name ,' - ' ,  disp.Name) as QuickDisposition      FROM crm.disposition disp INNER JOIN CRM.Type_LeadStatus tls ON tls.id = disp.Leadstatusid;";
                    connection.Open();
                    var result = connection.Query(query).ToList();
                    if (result != null && result.Count() > 0)
                    {
                        result.Insert(0, DyObj);
                    }

                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message, ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult QuickDispositions_1()
        {
            List<QuickDispositionVM> result = new List<QuickDispositionVM>();
            result.Add(new QuickDispositionVM
            {
                name = "Qualified - Set Appointment",
                Value = "7|0|1"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Competitor",
                Value = "2|1|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - DIY",
                Value = "2|2|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Pricing",
                Value = "2|3|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Too small",
                Value = "2|4|0"
            });

            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Unresponsive",
                Value = "2|5|0"
            });

            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Not serviceable",
                Value = "2|6|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Bad lead",
                Value = "2|7|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Call back later",
                Value = "5|8|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Not sure at this time",
                Value = "5|9|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Project on hold",
                Value = "5|10|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Not reachable",
                Value = "5|11|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Call attempted",
                Value = "5|12|2"
            });
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult QuickDispositions()
        {
            List<QuickDispositionVM> result = new List<QuickDispositionVM>();
            result.Add(new QuickDispositionVM
            {
                name = "Qualified - Set Appointment",
                Value = "7|0|1"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Outside Service Area",
                Value = "2|2|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - DIY",
                Value = "2|1|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Incorrect Lead Data",
                Value = "2|3|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Product Scope - Brand",
                Value = "2|4|0"
            });

            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Unresponsive",
                Value = "2|7|0"
            });

            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Product Scope - Local",
                Value = "2|5|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Cost Gap",
                Value = "2|6|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Budget Reallocation",
                Value = "5|8|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Competitive Shopping",
                Value = "5|9|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Researching",
                Value = "5|10|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Project Hold",
                Value = "5|11|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Unresponsive",
                Value = "5|12|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Inactive - Bad Data",
                Value = "13|13|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Inactive - Duplicate",
                Value = "13|14|0"
            });
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult Campaigns()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    //var query = "select * from crm.campaign where ISNULL(isActive,0) = 1 and FranchiseId = @FranchiseId";
                    var query = @"select CampaignId, name from crm.campaign 
                                where ISNULL(isActive,0) = 1 and FranchiseId = @FranchiseId
                                order by name ";
                    var result = connection.Query<Campaign>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        [HttpGet]
        public IHttpActionResult GetMyProductCategories(int id)
        {
            var categories = Lookup_manager.GetMyProductCategories();
            var products = Lookup_manager.GetProdcustTL();
            var discounts = Lookup_manager.GetDiscountTL();

            var disclist = new List<FranchiseProducts>();
            foreach (var item in discounts)
            {
                var disc = new FranchiseProducts();
                disc.Discount = item.DiscountValue;
                disc.Description = item.Description;
                disc.ProductName = item.Name;
                disc.ProductID = item.DiscountId;
                disc.ProductKey = item.DiscountId;
                disc.ProductType = 4;
                disc.DiscountType = item.DiscountFactor;
                products.Add(disc);
            }

            var result = new
            {
                Categories = categories,
                Products = products,
                Discounts = discounts
            };
            return Json(result);
        }

        //to fetch product category
        [HttpGet]
        public IHttpActionResult ProductCategory(int id, int Dropcategory)
        {
            var result = Lookup_manager.GetProductList(id, Dropcategory);
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult ProductCategoryforAdmin(int id, int Dropcategory)
        {
            var result = Lookup_manager.GetProductListforAdmin(id, Dropcategory);
            return Json(result);
        }

        //to fetch product sub-category based on category selected
        [HttpGet]
        public IHttpActionResult ProductSubCategory(int id)
        {
            var result = Lookup_manager.GetSubCategoryList(id);
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult ProductSubCategoryforAdmin(int id, int Dropvalue)
        {
            var result = Lookup_manager.GetSubCategoryListforAdmin(id, Dropvalue);
            return Json(result);
        }

        //to get Product status
        [HttpGet]
        public IHttpActionResult ProductStatus(int id)
        {
            var result = Lookup_manager.GetStatus();
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult ProductVendorName(int id)
        {
            var result = Lookup_manager.GetProductVendorname(id);
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult GetVendorType(int id)
        {
            var result = Lookup_manager.GetVendor(id);
            return Json(result);
        }

        //check the name while edit data in Vendors
        [HttpGet]
        public IHttpActionResult CheckName(int id, string value)
        {
            var result = Lookup_manager.GetEditVendName(value);
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult ChckLocalVendor(int id, string value)
        {
            var result = Lookup_manager.GetLocalvalue(value);
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult GetVendorName(int id, string value)
        {
            var result = Lookup_manager.GetVendName(value);
            return Json(result);
        }

        //to get vendor status value
        [HttpGet]
        public IHttpActionResult VendorStatus(int id)
        {
            var result = Lookup_manager.GetVendorStatus();
            return Json(result);
        }

        //to fetch currency value
        [HttpGet]
        public IHttpActionResult GetCurrencyValue(string value)
        {
            var result = Lookup_manager.GetCurrency(value);
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult Account_Type()
        {
            try
            {
                List<AccountType> result = new List<AccountType>();
                result.Add(new AccountType
                {
                    name = "Prospect",
                    Id = 1
                });
                result.Add(new AccountType
                {
                    name = "Key/Commercial",
                    Id = 2
                });
                result.Add(new AccountType
                {
                    name = "Customer",
                    Id = 3
                });
                return Json(result);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the parent sources or specific source by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Source(string owner = "Franchise", string Channel = null)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = "select distinct sourcename from crm.sourcestp where ownername = 'franchise' and channelname = @channel";
                    var result = connection.Query<Source>(query, new { channel = Channel }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            //if (id > 0)
            //{
            //    var source = SessionManager.CurrentFranchise.SourceCollection(id);
            //    return Json(source != null ? new { source.SourceId, source.Name } : null);
            //}
            //else
            //{
            //    var sources = SessionManager.CurrentFranchise.SourceCollection();
            //    sources = sources.Where(x => x.ParentId == null).ToList();
            //    return Json(sources.Where(w => !w.ParentId.HasValue).Select(s => new { s.SourceId, s.Name }));
            //}
        }

        /// <summary>
        /// Get list of Sub sources to a source
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult SubSources(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    //var query = "select * from crm.Subsource where sourceid = @sourceId ";
                    //var result = connection.Query<Subsource>(query, new { sourceId  = id}).ToList();
                    var query = "select distinct subsourcename from crm.sourcestp where ownername = 'franchise' and channelname = 'social' and sourcename = 'Instagram'";
                    var result = connection.Query<Subsource>(query, new { }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// get the list of sales step using lead id
        /// </summary>

        //public IHttpActionResult GetSalesStep(int leadId)
        //{
        //         using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        //    {
        //        try
        //        {
        //            connection.Open();
        //            var query = "select * from crm.SalesStep where LeadId = @leadid";
        //            var result = connection.Query<Subsource>(query, new { leadid = leadId }).ToList();
        //            connection.Close();
        //            return Json(result);
        //        }
        //        catch (Exception e)
        //        {
        //            throw;
        //        }

        //    }

        //}

        [HttpGet]
        public IHttpActionResult Channels(string owner = "Franchise"/*int id, int ssid, int cid*/)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    //   var query = @"select * from crm.Type_Channel where ChannelId in (
                    //select ChannelId from crm.ChannelSourceSubsourceCampaignMapping where
                    //   campaignid = @campaignId and sourceid = @sourceId and subsourceid = @subSourceId) ";
                    //   var result = connection.Query<Type_Channel2>(query, new { sourceId = id,
                    //       subSourceId = ssid, campaignId = cid}).ToList();
                    var query = "select distinct channelname from crm.sourcestp where ownername = @franchiseowner";
                    var result = connection.Query<Source>(query, new { franchiseowner = owner }).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Gets manufacturers by product type
        /// </summary>
        /// <param name="id">Product Type Id</param>
        [HttpGet]
        public IHttpActionResult Manufacturers(int id)
        {
            var mfgIds = (CacheManager.ProductCollection.Where(w => w.ProductTypeId == id).Select(s => s.ManufacturerId).Distinct().ToList());

            var mfgList = CacheManager.ManufacturerCollection.Where(w => mfgIds.Contains(w.ManufacturerId)).Select(s => new
            {
                s.ManufacturerId,
                s.Name
            });

            return Json(mfgList);
        }

        /// <summary>
        /// Gets a list of product styles by product
        /// </summary>
        /// <param name="id">Product Id</param>
        [HttpGet]
        public IHttpActionResult ProductStyles(Guid id)
        {
            var styleList = (from p in this.CRMDBContext.Products
                             from s in p.ProductStyles
                             where p.ProductGuid == id
                             orderby p.Revision
                             //&& (!s.FranchiseId.HasValue || s.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)
                             select new
                             {
                                 s.Style.Id,
                                 s.Style.Name
                                 //, MarginPercent = s.MarginPercent ?? SessionManager.CurrentFranchise.MarginPercent
                                 //, JobberDiscountPercent = s.JobberDiscountPercent ?? (p.JobberDiscountPercent > 0 ? p.JobberDiscountPercent : SessionManager.CurrentFranchise.JobberDiscountPercent)
                             }).Distinct().ToList();

            //var styleList = (from p in db.Products
            //                 where (p.ProductGuid == id)
            //                 select new
            //                 {
            //                     Id = p.ProductId,
            //                     Name = "Style Test"
            //                 }).Distinct().ToList();

            return Json(styleList);
        }

        /// <summary>
        /// get list of Product Types.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult ProductTypes(int id)
        {
            List<Type_Product> productTypeList = CacheManager.ProductTypeCollection.OrderBy(o => o.TypeName).ToList();

            var productList = (from productType in productTypeList select new { ProductTypeId = productType.ProductTypeId, TypeName = productType.TypeName }).Distinct();

            return Json(productList);
        }

        [HttpGet]
        public IHttpActionResult GetLeadStatusLookup(int id, long version = 0)
        {
            var result = new LookupDTO<ICacheable>()
            {
                Added = CacheManager.LeadStatusTypeCollection.Where(ls => (ls.FranchiseId == null || ls.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)),
            };

            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult GetOrderStatusLookup(int id, long version = 0)
        {
            var result = new LookupDTO<ICacheable>()
            {
                // Added = CacheManager.OrderStatusTypeCollection.Where(ls => (ls.IsDeleted == null)),
            };

            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult GetJobStatusLookup(int id, long version = 0)
        {
            var result = new LookupDTO<ICacheable>()
            {
                Added = CacheManager.JobStatusTypeCollection.Where(ls => (ls.FranchiseId == null || ls.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)),
            };

            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult GetConceptLookup(int id, long version = 0)
        {
            var result = new LookupDTO<ICacheable>()
            {
                Added = CacheManager.ConceptCollection,
            };

            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult GetSourceLookup(int id, long version = 0)
        {
            var sw = new Stopwatch();
            sw.Start();

            var list = new List<SourceDTO>();


            // TODO : not necessary in TP - murugan
            ////var sources = FranchiseManager.GetSourcesThree();

            ////foreach (var parent in sources.Where(v => v.ParentId == null))
            ////{
            ////    parent.Children = new List<SourceDTO>();
            ////    list.Add(parent);
            ////    //MYC-2252- Remove the campaign checkas we need to show the campaigns in Sources
            ////    foreach (var chield in sources.Where(v => v.ParentId == parent.SourceId && ((v.IsActive == true && v.IsCampaign == true) || (v.IsCampaign != true))))
            ////    {
            ////        //Display the grand child or 3rd level campaign as well
            ////        chield.Children = new List<SourceDTO>();
            ////        foreach (var gc in sources.Where(x => x.ParentId == chield.SourceId && ((x.IsActive == true && x.IsCampaign == true) || (x.IsCampaign != true))))
            ////        {
            ////            chield.Children.Add(gc);
            ////        }

            ////        parent.Children.Add(chield);
            ////    }
            ////}

            var result = new LookupDTO<ICacheable>()
            {
                Added = list,
                Took = sw.Elapsed.Milliseconds
            };

            sw.Stop();

            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult Type_AccountStatus(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select * from CRM.Type_AccountStatus";
                connection.Open();
                var result = connection.Query(query).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult Type_Appointments(int id)
        {
            var appointmentDic = new Dictionary<string, byte>();
            foreach (var type in SessionManager.CurrentFranchise.AppointmentTypesCollection())
            {
                appointmentDic.Add(type.Name, Convert.ToByte(type.AppointmentTypeId));
            }
            return Json(new { AppointmentTypeEnum = appointmentDic });
        }

        [HttpGet]
        public IHttpActionResult Reminders(int id)
        {
            var reminders = new Dictionary<string, int>(16);
            reminders.Add("Disabled", 0);
            reminders.Add("5 minutes", 5);
            reminders.Add("10 minutes", 10);
            reminders.Add("15 minutes", 15);
            reminders.Add("30 minutes", 30);
            reminders.Add("45 minutes", 45);
            reminders.Add("1 hour", 60);
            reminders.Add("2 hours", 120);
            reminders.Add("4 hours", 240);
            reminders.Add("8 hours", 480);
            reminders.Add("1 day", 1440);
            reminders.Add("2 days", 2880);
            reminders.Add("3 days", 4320);
            reminders.Add("4 days", 5760);
            reminders.Add("1 week", 10080);
            reminders.Add("2 weeks", 20160);
            return Json(reminders);
        }

        //[HttpGet]
        //public IHttpActionResult GetPICToken()
        //{
        //    var authMgr = new OAuthManager();
        //    var picToken = authMgr.GetAccessToken();

        //    return Json(new { PICAccessToken = picToken.access_token });
        //}
        /// <summary>
        /// List of Room Location
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetRoomLocation()
        {
            List<RoomLocationVM> response = new List<RoomLocationVM>();
            response.Add(new RoomLocationVM
            {
                Key = "Basement",
                Value = "Basement",
                AbbrValue = "BSMT"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Bathroom",
                Value = "Bathroom",
                AbbrValue = "BTH"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Bedroom",
                Value = "Bedroom",
                AbbrValue = "BR"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Bedroom 1",
                Value = "Bedroom 1",
                AbbrValue = "BR1"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Bedroom 2",
                Value = "Bedroom 2",
                AbbrValue = "BR2"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Bedroom 3",
                Value = "Bedroom 3",
                AbbrValue = "BR3"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Bedroom 4",
                Value = "Bedroom 4",
                AbbrValue = "BR4"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Den",
                Value = "Den",
                AbbrValue = "DEN"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Dining Room",
                Value = "Dining Room",
                AbbrValue = "DR"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Family  Room",
                Value = "Family  Room",
                AbbrValue = "FAM"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Formal Dining Room",
                Value = "Formal Dining Room",
                AbbrValue = "FDR"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Informal Dining Room",
                Value = "Informal Dining Room",
                AbbrValue = "IDR"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Garage",
                Value = "Garage",
                AbbrValue = "GAR"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Game Room",
                Value = "Game Room",
                AbbrValue = "GAME"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Kitchen",
                Value = "Kitchen",
                AbbrValue = "KIT"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Laundry",
                Value = "Laundry",
                AbbrValue = "LAU"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Living Room",
                Value = "Living Room",
                AbbrValue = "LIV"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Master Bathroom",
                Value = "Master Bathroom",
                AbbrValue = "MBTH"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Master Bedroom",
                Value = "Master Bedroom",
                 AbbrValue = "MBR"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Media Room",
                Value = "Media Room",
                 AbbrValue = "MED"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Office",
                Value = "Office",
                 AbbrValue = "OFF"
            });

            response.Add(new RoomLocationVM
            {
                Key = "Play Room",
                Value = "Play Room",
                AbbrValue = "PLAY"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Studio",
                Value = "Studio",
                 AbbrValue = "STD"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Sunroom",
                Value = "Sunroom",
                AbbrValue = "SUN"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Theater Room",
                Value = "Theater Room",
                 AbbrValue = "THTR"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Common Area",
                Value = "Common Area",
                AbbrValue = "COM"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Conference  Room",
                Value = "Conference  Room",
                AbbrValue = "CONF"
            });
            response.Add(new RoomLocationVM
            {
                Key = "File Room",
                Value = "File Room",
                 AbbrValue = "FILE"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Fitness Room",
                Value = "Fitness Room",
                 AbbrValue = "FIT"
            });
            response.Add(new RoomLocationVM
            {
                Key = "IT Room",
                Value = "IT Room",
                 AbbrValue = "IT"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Laboratory",
                Value = "Laboratory",
                 AbbrValue = "LAB"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Lobby",
                Value = "Lobby",
                AbbrValue = "LBY"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Reception",
                Value = "Reception",
                AbbrValue = "REC"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Supply Room",
                Value = "Supply Room",
                 AbbrValue = "SUPP"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Warehouse",
                Value = "Warehouse",
                AbbrValue = "WHSE"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Entry",
                Value = "Entry",
                AbbrValue = "ENT"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Stair",
                Value = "Stair",
                AbbrValue = "STR"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Bonus",
                Value = "Bonus",
                AbbrValue = "BON"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Loft",
                Value = "Loft",
                AbbrValue = "LFT"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Great Room",
                Value = "Great Room",
                AbbrValue = "GRT"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Powder Room",
                Value = "Powder Room",
                AbbrValue = "POW"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Slider",
                Value = "Slider",
                AbbrValue = "SLD"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Nook",
                Value = "Nook",
                AbbrValue = "NK"
            });
            response.Add(new RoomLocationVM
            {
                Key = "Other",
                Value = "Other",
                AbbrValue = "OTR"
            });

            return Json(response);
        }

        [HttpGet]
        public IHttpActionResult GetWindowLocation()
        {
            List<KeyValuePair<string, string>> data = new List<KeyValuePair<string, string>>();
            var value = "";
            for (var i = 1; i < 51; i++)
            {
                value = "Window " + i + " / " + "W" + i;
                data.Add(new KeyValuePair<string, string>(value, value));
            }
            for(var j = 1; j < 5; j++)
            {

                value = "Door " + j + " / " + "D" + j;
                data.Add(new KeyValuePair<string, string>(value, value));
            }
            return Json(data);
            //new KeyValuePair<string, string>("Select","Select"),
            //new KeyValuePair<string, string>("Window 1 / W1","Window 1 / W1"),
            //new KeyValuePair<string, string>("Window 2 / W2","Window 2 / W2"),
            //new KeyValuePair<string, string>("Window 3 / W3","Window 3 / W3"),
            //new KeyValuePair<string, string>("Window 4 / W4","Window 4 / W4"),
            //new KeyValuePair<string, string>("Window 5 / W5","Window 5 / W5"),
            //new KeyValuePair<string, string>("Window 6 / W6","Window 6 / W6"),
            //new KeyValuePair<string, string>("Window 7 / W7","Window 7 / W7"),
            //new KeyValuePair<string, string>("Window 8 / W8","Window 8 / W8"),
            //new KeyValuePair<string, string>("Window 9 / W9","Window 9 / W9"),
            //new KeyValuePair<string, string>("Window 10 / W10","Window 10 / W10"),
            //new KeyValuePair<string, string>("Window 11 / W11","Window 11 / W11"),
            //new KeyValuePair<string, string>("Window 12 / W12","Window 12 / W12"),
            //new KeyValuePair<string, string>("Window 13 / W13","Window 13 / W13"),
            //new KeyValuePair<string, string>("Window 14 / W14","Window 14 / W14"),
            //new KeyValuePair<string, string>("Window 15 / W15","Window 15 / W15"),
            //new KeyValuePair<string, string>("Window 16 / W16","Window 16 / W16"),
            //new KeyValuePair<string, string>("Window 17 / W17","Window 17 / W17"),
            //new KeyValuePair<string, string>("Window 18 / W18","Window 18 / W18"),
            //new KeyValuePair<string, string>("Window 19 / W19","Window 19 / W19"),
            //new KeyValuePair<string, string>("Window 20 / W20","Window 20 / W20"),
            //new KeyValuePair<string, string>("Door 1 / D1","Door 1 / D1"),
            //new KeyValuePair<string, string>("Door 2 / D2","Door 2 / D2"),
            //new KeyValuePair<string, string>("Door 3 / D3","Door 3 / D3"),
            //new KeyValuePair<string, string>("Door 4 / D4","Door 4 / D4"),

            //return Json(response);
        }

        [HttpGet]
        public IHttpActionResult GetMountType()
        {
            List<MountTypeVM> response = new List<MountTypeVM>();

            //response.Add(new MountTypeVM
            //{
            //    Key = "Select",
            //    Value = "Select"
            //});
            response.Add(new MountTypeVM
            {
                Key = "IB",
                Value = "IB"
            });
            response.Add(new MountTypeVM
            {
                Key = "OB",
                Value = "OB"
            });
            response.Add(new MountTypeVM
            {
                Key = "Partial IB",
                Value = "Partial IB"
            });
            response.Add(new MountTypeVM
            {
                Key = "Partial OB",
                Value = "Partial OB"
            });
            response.Add(new MountTypeVM
            {
                Key = "Wall Mounted",
                Value = "Wall Mounted"
            });
            response.Add(new MountTypeVM
            {
                Key = "Ceiling Mounted",
                Value = "Ceiling Mounted"
            });
            return Json(response);
        }

        [HttpGet]
        public IHttpActionResult GetFractionalInch()
        {
            List<FractionalInchVM> response = new List<FractionalInchVM>();

            response.Add(new FractionalInchVM
            {
                Key = "1/8",
                Value = "1/8"
            });
            response.Add(new FractionalInchVM
            {
                Key = "1/4",
                Value = "1/4"
            });
            response.Add(new FractionalInchVM
            {
                Key = "3/8",
                Value = "3/8"
            });

            response.Add(new FractionalInchVM
            {
                Key = "1/2",
                Value = "1/2"
            });
            response.Add(new FractionalInchVM
            {
                Key = "5/8",
                Value = "5/8"
            });
            response.Add(new FractionalInchVM
            {
                Key = "3/4",
                Value = "3/4"
            });
            response.Add(new FractionalInchVM
            {
                Key = "7/8",
                Value = "7/8"
            });
            return Json(response);
        }

        public IHttpActionResult GetSystem()
        {
            var response = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Closet","Closet"),
                new KeyValuePair<string, string>("Office","Office"),
                new KeyValuePair<string, string>("Garage","Garage"),
                new KeyValuePair<string, string>("Floor","Floor")
            };
            return Json(response);
        }

        public IHttpActionResult GetShape()
        {
            var response = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Square","Square"),
                new KeyValuePair<string, string>("Rectangle","Rectangle"),
                new KeyValuePair<string, string>("Circle","Circle"),
                new KeyValuePair<string, string>("1/2 Circle","1/2 Circle"),
                new KeyValuePair<string, string>("Oval","Oval")
            };
            return Json(response);
        }

        [HttpGet]
        public IHttpActionResult GetStorageType()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = @"select a.StorageType as Id , a.StorageType as Name, a.System as Value from CRM.[MeasurementConfiguration] a
                               ";
                    var result = connection.Query<dynamic>(query).ToList();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        [HttpGet]
        public IHttpActionResult GetMeasurementConfiguration(string storageName)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = @"select a.Width,a.Height,a.Depth,a.Laptop,a.Desktop,a.Quantity,a.UseTP,a.Store from CRM.[MeasurementConfiguration] a
                                where a.StorageType = @parentName";
                    var result = connection.Query<MeasurementConfiguration>(query, new { parentName = storageName }).FirstOrDefault();
                    connection.Close();
                    return Json(result);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        [HttpGet]
        public IHttpActionResult GetQuoteStatus(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                if (id > 0)
                {
                    var query = @"select q.QuoteStatusId,q.QuoteStatus from [CRM].[Type_QuoteStatus] q
                                where q.QuoteStatusId = @id";
                    var result1 = connection.Query(query, new { campaignId = id }).ToList();
                    connection.Close();
                    return Json(result1);
                }
                else
                {
                    var query = @"select * from [CRM].[Type_QuoteStatus]";
                    var result1 = connection.Query(query, new { campaignId = id }).ToList();
                    connection.Close();
                    return Json(result1);
                }
            }
        }

        [HttpGet]
        public IHttpActionResult GetEmails()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var query = @"select p.PersonId, p.PrimaryEmail from Auth.Users u
                             join CRM.Person p on u.PersonId=p.PersonId
                             WHERE u.Domain IS NOT NULL
                             AND isnull(u.IsDisabled,0)=0 AND u.IsDeleted=0 AND u.FranchiseId = @franchiseId";
                var response = connection.Query(query, new { franchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return Json(response);
            }
        }

        // TODO: is this required in TP???
        //[HttpGet]
        //public IHttpActionResult GetRoles()
        //{
        //    var permission = PermissionProvider.GetPermission(SessionManager.CurrentUser, "Settings", "Roles");
        //    if (permission.CanRead)
        //    {
        //        List<Role> roles = LocalRole.GetRoles(SessionManager.CurrentFranchise.FranchiseId, true).ToList();
        //        return Json(roles);
        //    }
        //    return ResponseResult("No Roles Found");
        //}

        [HttpGet]
        public IHttpActionResult GetAddress(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var query = @"select a.Address1,a.Address2,a.City,a.State,a.ZipCode,a.CountryCode2Digits from CRM.Addresses a where AddressId =@addressId";
                var response = connection.Query(query, new { addressId = id }).FirstOrDefault();
                connection.Close();
                return Json(response);
            }
        }

        [HttpGet]
        public IHttpActionResult ReferralType(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                if (id > 0)
                {
                    var query = @"select * from [CRM].[Type_Referral]
                                where Id = @id";
                    var result1 = connection.Query(query, new { Id = id }).ToList();
                    connection.Close();
                    return Json(result1);
                }
                else
                {
                    var query = @"select * from [CRM].[Type_Referral]";
                    var result1 = connection.Query(query).ToList();
                    connection.Close();
                    return Json(result1);
                }
            }
        }

        [HttpGet]
        public IHttpActionResult ChannelBySource(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select distinct stp.SourcesTPId as SourceId,
                            s.Name as SourceName,o.Name as OriginationName ,
                            c.Name as ChannelName    ,o.name +' - '+ c.Name + ' - ' + s.Name as name from crm.SourcesTP stp
                            join crm.Type_Channel c on c.ChannelId = stp.channelid
                            join crm.sources s on s.SourceId = stp.SourceId
                            join CRM.Type_Owner o on o.OwnerId = stp.OwnerId

                            where stp.SourcesTPId=@sourceId order by 3 asc";
                connection.Open();

                if (id > 0)
                {
                    var result = connection.Query(query, new { sourceId = id }).ToList();
                    connection.Close();
                    return Json(result);
                }
                else
                    return null;
            }
        }

        [HttpGet]
        public IHttpActionResult GetAttendees(int? id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (SessionManager.CurrentFranchise == null)
                {
                    return OkTP();
                }
                var query = @"select distinct per.personid as SalesAgentId,FirstName + ' ' + LastName  as FullName ,usr.Email
                                    from [Auth].[Users] usr
                                    join [CRM].[Person] per on per.personid = usr.personid
                                    join [Auth].[UsersInRoles] uir on usr.userid = uir.userid
                                    where usr.franchiseid = @FranchiseId and usr.Domain is not null and usr.addressid is not null and ISNULL(usr.IsDeleted,0)=0 and ISNULL(usr.IsDisabled,0)=0 
                                    order by FullName";
                connection.Open();
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                if (result == null)
                {
                    return OkTP();
                }
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetShip()
        {
            ShipDetails ship = new ShipDetails();
            ship.ShipToName = "Main Office";
            ship.ShipToLocation = "Office";
            ship.Address1 = "1927";
            ship.Address2 = "N Glassell St";
            ship.City = "Orange";
            ship.State = " CA";
            ship.Country = "USA";
            ship.PostalCode = "92865";
            ship.TerritoryID = "11,12,13";
            return Json(ship);
        }

        [HttpGet]
        public IHttpActionResult Type_ShipToLocVendor(int id, int TerritoryValue)
        {
            var result = Lookup_manager.GetLOVShip(id, TerritoryValue);
            return Json(result);
        }

        //[HttpGet]
        //public IHttpActionResult ShipAddress(int id,int AddressValue)
        //{
        //    var result = Lookup_manager.GetAddress(id, AddressValue);
        //        return Json(result);
        //}

        [HttpGet]
        public IHttpActionResult Type_ShipToLocation()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from [CRM].[Type_ShipToLocations]";
                var result1 = connection.Query(query).ToList();
                connection.Close();
                return Json(result1);
            }
        }

        [HttpGet]
        public IHttpActionResult GetWorkStation()
        {
            var response = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Laptop","Laptop"),
                new KeyValuePair<string, string>("Desktop","Desktop"),
            };
            return Json(response);
        }

        // this is only for demo purpose, we will remove it later

        [HttpGet]
        public IHttpActionResult GetTerritoryByFranchiseId(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var result = connection.GetList<Territory>("where franchiseid = @franchiseId", new { franchiseId = Id }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetDemoOwner()
        {
            List<OwnerVM> OwnerSetUp = new List<OwnerVM>();
            OwnerSetUp.Add(new OwnerVM
            {
                FirstName = "John",
                LastName = "Jackson"
            });
            OwnerSetUp.Add(new OwnerVM
            {
                FirstName = "James",
                LastName = "Jackson"
            });
            return Json(OwnerSetUp);
        }

        [HttpGet]
        public IHttpActionResult GetAttendeesByTaskId(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select PersonEmail from [CRM].[EventToPeople] where TaskId=@id ";
                var result = connection.Query<string>(query, new { id = id }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetLeadByLeadId(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from crm.Customer where customerid in (
                            select customerid from crm.LeadCustomers where LeadId = @leadId and IsPrimaryCustomer = 1)";
                var customer = connection.Query<CustomerTP>(query, new { leadId = id }).FirstOrDefault();
                connection.Close();
                return Json(customer);
            }
        }

        [HttpGet]
        public IHttpActionResult LoginUserDetails(int id)
        {
            UserVM user = new UserVM();
            user.FullName = SessionManager.SecurityUser.Person.FullName;
            user.RoleName = SessionManager.SecurityUser.Roles.First().RoleName;
            user.PersonId = SessionManager.SecurityUser.PersonId;
            return Json(user);
        }

        //Get Lov from common table
        [HttpGet]
        public IHttpActionResult Type_LookUpValues(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from [CRM].[Type_LookUpValues] where TableId =@tableId ORDER BY  CASE WHEN Name = 'Other' THEN    1 ELSE null END,name ASC;";
                var result = connection.Query(query, new { tableId = id }).ToList();
                connection.Close();
                return Json(result);
            }
        }
        [HttpGet]
        public IHttpActionResult TaxTypes()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"
                            SELECT lv.Name, 
                                   lv.SortOrder AS TaxTypeId
                            FROM crm.Type_LookUpValues lv
                                 INNER JOIN CRM.Type_LookUpTable tlut ON lv.TableId = tlut.Id
                            WHERE tlut.TableName = 'TaxType'
                            ORDER BY lv.SortOrder ASC;";
                var result = connection.Query(query).ToList();
                connection.Close();
                return Json(result);
            }
        }
        [HttpGet]
        public IHttpActionResult VendorNameList(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select hv.VendorIdPk,hv.VendorId, hv.Name,hv.VendorType from Acct.HFCVendors hv
                              join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
                              where FranchiseId=@FranchiseId and fv.VendorStatus=1
                              union
                              select fv.VendorIdPk,fv.VendorId, fv.Name,fv.VendorType from Acct.FranchiseVendors fv
                              where FranchiseId=@FranchiseId and fv.VendorStatus=1 and VendorType=3
                              order by [Name] asc";
                var result = connection.Query<ProductListNameModel>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetFESalesPerson(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var user = LocalMembership.GetUser(SessionManager.CurrentUser.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                if (user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId) || user.IsInRole(AppConfigManager.DefaultOwnerRole.RoleId))
                {
                    var query = @"select distinct p.PersonId, p.FirstName+' '+p.LastName as PersonName from Auth.Users u
                              join Auth.UsersInRoles ur on u.UserId=ur.UserId
                              join CRM.Person p on u.PersonId=p.PersonId
                              where u.FranchiseId=@FranchiseId and RoleId in( '9CFE899F-A036-4D69-A58B-A1BFBDD8DCA0','06547EFF-24AA-454B-A399-9017009B3FBD')";
                    var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                else if (user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId))
                {
                    var query = @"select distinct p.PersonId, p.FirstName+' '+p.LastName as PersonName from Auth.Users u
                                  join Auth.UsersInRoles ur on u.UserId=ur.UserId
                                  join CRM.Person p on u.PersonId=p.PersonId
                                  where u.FranchiseId=@FranchiseId and u.PersonId=@PersonId and RoleId in( '9CFE899F-A036-4D69-A58B-A1BFBDD8DCA0','06547EFF-24AA-454B-A399-9017009B3FBD')";
                    var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, PersonId = SessionManager.CurrentUser.PersonId }).ToList();
                    connection.Close();
                    return Json(result);
                }
                return Json("");
            }
        }

        [HttpGet]
        public IHttpActionResult Type_DateTime(int id, int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).Skip(5).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetFESource(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select s.SourceId,s.Name from(
                                select distinct SourceId from CRM.LeadSources ls
                                join CRM.Leads l on l.LeadId=ls.LeadId
                                where l.FranchiseId=@FranchiseId) as lsource
                                join CRM.SourcesTP stp on lsource.SourceId=stp.SourcesTPId
                                join CRM.Sources s on stp.SourceId=s.SourceId
                                union
                                select s.SourceId,s.Name from(
                                select distinct SourceId from CRM.AccountSources accs
                                join CRM.Accounts acc on accs.AccountId=acc.AccountId
                                where acc.FranchiseId=@FranchiseId) as Accsource
                                join CRM.SourcesTP stp on Accsource.SourceId=stp.SourcesTPId
                                join CRM.Sources s on stp.SourceId=s.SourceId
                                order by Name asc";
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetFECampaign(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select CampaignId,Name from CRM.Campaign where FranchiseId=@FranchiseId
                              order by Name asc";
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetFEZip(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select ZipCode from CRM.Leads l
                              CROSS APPLY (SELECT top 1 * FROM CRM.LeadAddresses where LeadId=l.LeadId) lad
                              join CRM.Addresses ad on lad.AddressId=ad.AddressId
                              where l.FranchiseId=@FranchiseId
                              union
                              select ZipCode from CRM.Accounts acc
                              CROSS APPLY (SELECT top 1 * FROM CRM.AccountAddresses where AccountId=acc.AccountId) aad
                              join CRM.Addresses ad on aad.AddressId=ad.AddressId
                              where acc.FranchiseId=@FranchiseId";
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetFETerritory(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select TerritoryId,[Name],Code from CRM.Territories where FranchiseId=@FranchiseId
                              order by Name asc";
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetShiptoLocationByFranchiseId(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as ShipToLocationId,ShipToName as ShipToName,stl.AddressId,STUFF(
                                                                      COALESCE(', ' + NULLIF(Address1, ''), '')  +
                                    		                          COALESCE(', ' + NULLIF(Address2, ''), '')  +
                                                                      COALESCE(', ' + NULLIF(City, ''), '') +
                                    		                          COALESCE(', ' + NULLIF([State], ''), '') +
                                                                      COALESCE(', ' + NULLIF(ZipCode, ''), '') +
                                                                      COALESCE(', ' + NULLIF(CountryCode2Digits , ''), ''),
                                                                      1, 1, '') AS Address from CRM.ShipToLocations stl
																INNER JOIN crm.Addresses a ON a.AddressId = stl.AddressId where FranchiseId=@FranchiseId
																order by stl.ShipToName ASC";
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return Json(result);
            }
        }

        [HttpGet]
        public IHttpActionResult GetRolesModel(int id)
        {
            var response = new List<PermissionSet>
           {
               new PermissionSet {PermissionSetId=1,Name="Lead",ReportsTo=null,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=2,Name="Convert Lead",ReportsTo=1,Category="SpecialModule"},
               new PermissionSet {PermissionSetId=3,Name="Account",ReportsTo=null,Create=true,Read=true,Update=true,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=4,Name="Opportunity",ReportsTo=null,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=5,Name="Quote",ReportsTo=null,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=1005,Name="Convert Quote To Order",ReportsTo=5,Category="SpecialModule"},
               new PermissionSet {PermissionSetId=1005,Name="Margin Review Summary",ReportsTo=5,Category="SpecialModule"},
               new PermissionSet {PermissionSetId=1005,Name="Margin Review Line Detail",ReportsTo=5,Category="SpecialModule"},
               new PermissionSet {PermissionSetId=9,Name="Override Tax Exempt",ReportsTo=5,Category="SpecialModule"},
               new PermissionSet {PermissionSetId=10,Name="Sales Order",ReportsTo=null,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=11,Name="Convert SO to MPO",ReportsTo=10,Category="SpecialModule"},
               new PermissionSet {PermissionSetId=12,Name="Invoice",ReportsTo=null,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=13,Name="Payment",ReportsTo=null,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=14,Name="Reverse Payment",ReportsTo=10,Category="SpecialModule"},
               new PermissionSet {PermissionSetId=15,Name="Purchase Order",ReportsTo=null,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=16,Name="Measurement",ReportsTo=null,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=17,Name="Franchise Settings",ReportsTo=null,Category="GroupModule"},
               new PermissionSet {PermissionSetId=18,Name="Appointment Type Settings",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=19,Name="Dashboard Settings",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=20,Name="Email Settings",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=21,Name="Legal Disclaimer",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=22,Name="Manage Documents",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=23,Name="Add/Edit Ship to Locations",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=24,Name="Buyer's Remorse Settings",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=25,Name="Advanced Pricing Settings",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=26,Name="Security Users and Permissions",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=27,Name="QuickBooks Settings (QBO)",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
               new PermissionSet {PermissionSetId=28,Name="QuickBooks Settings (QBD)",ReportsTo=17,Create=false,Read=true,Update=false,Delete=false,Category="Module"},
           };
            return Json(response);
        }

        [HttpGet]
        public IHttpActionResult Type_ShipToLocBPVendor(int id)
        {
            var result = Lookup_manager.GetLOVShipBP(id);
                return Json(result);
        }
    }
}