﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;

using HFC.CRM.DTO.Address;
using HFC.CRM.DTO.Notes;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;
using Newtonsoft.Json;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class AccountsController : BaseApiController
    {
        AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        PurchaseOrderManager PurchaseOrdersManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        /// <summary>
        /// Gets a single Account, will include a lot of Account detail
        /// </summary>
        /// <param name="id">Account Id</param>
        /// <returns></returns>
        public IHttpActionResult Get(int id)
        {
            try
            {
                var Account = AccountMgr.Get(id);
                if (Account == null)
                {

                    throw new Exception("Account Id is required");
                }
                int totalRecords = 0;
                var Accountresult = JsonConvert.SerializeObject(Account, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               ,
                    PreserveReferencesHandling = PreserveReferencesHandling.None
                });
                return
                    Ok(
                        new
                        {
                            Account = Account,
                            FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString(),
                            FranchiseAddressObject = SessionManager.CurrentFranchise.Address,
                            Addresses = AccountMgr.GetAddress(new List<int>() { id }),
                            States = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 }),
                            Countries = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct(),
                            PurchaseOrdersCount = totalRecords,
                            SelectedAccountStatus = Account.Type_AccountStatus.Name,
                            selectedCampaign = Account.CampaignName,
                            AccountQuestionAns = Account.AccountQuestionAns,  //Added for Qualification/Question Answers
                            PrimarySource = Account.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault(),
                            SecondarySource = Account.SourcesList.Where(x => x.IsPrimarySource == false).ToList(),
                            FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting,
                            TextStatus = communication_Mangr.GetOptingInfo(Account.PrimCustomer.CellPhone)
                        });
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        private string TelBuilder(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            str = "(" + str.Insert(3, ")");
            return str.Insert(8, "-");
        }
        [HttpPost/*, ValidateAjaxAntiForgeryToken*/]
        public IHttpActionResult Post([FromBody]AccountTP model)
        {
            int accountId = 0;
            //texting
            var FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting;
            var CountryCode = SessionManager.CurrentFranchise.CountryCode;
            var BrandId = SessionManager.CurrentFranchise.BrandId;
            if (model.PrimCustomer.CellPhone != null)
            {
                var TextStatus = communication_Mangr.GetOptingInfo(model.PrimCustomer.CellPhone);
                if (FranciseLevelTexting && model.IsNotifyText && (TextStatus == null || TextStatus.IsOptinmessagesent == false))
                {
                    if(model.SendText && TextStatus == null)
                        communication_Mangr.SendOptinMessage(model.PrimCustomer.CellPhone, BrandId, CountryCode, null, model.AccountId);
                    else if (model.SendText && TextStatus.IsOptinmessagesent == false)
                        communication_Mangr.ForceOptinMessage(model.PrimCustomer.CellPhone, BrandId, CountryCode, null, model.AccountId);
                }
            }

            //return ResponseResult("Success", Json(new { AccountId = 33 }));

            if (model.SourcesTPIdFromCampaign > 0) model.SourcesTPId = model.SourcesTPIdFromCampaign;

            if (model.AccountId > 0)
            {
                // TP-3904: Lead Created date has changed to past while change the lead status.
                var acct = AccountMgr.GetAccount(model.AccountId);
                model.CreatedOnUtc = acct.CreatedOnUtc;
                model.CreatedByPersonId = acct.CreatedByPersonId;

                var updateStatus = AccountMgr.Update(model);
                return ResponseResult(updateStatus, Json(new { accountId = model.AccountId }));
            }
            if (model.SkipDuplicateCheck == false)
            {

                var dulpicateList = AccountMgr.GetDuplicateAccount(SearchDuplicateAccountEnum.All, string.Empty, model);
                if (dulpicateList.Count > 0)
                {

                    var dtos = dulpicateList.Select(v => new DuplicateOpportunityModel()
                    {
                        Address = v.Addresses.FirstOrDefault() != null ? v.Addresses.FirstOrDefault().Address1 : "",
                        OpportunityId = v.OpportunityId,
                        CellPhone = TelBuilder(v.PrimCustomer.CellPhone),
                        HomePhone = TelBuilder(v.PrimCustomer.HomePhone),
                        FaxPhone = TelBuilder(v.PrimCustomer.FaxPhone),
                        WorkPhone = TelBuilder(v.PrimCustomer.WorkPhone),
                        PrimaryEmail = v.PrimCustomer.PrimaryEmail,
                        SecondaryEmail = v.PrimCustomer.SecondaryEmail,
                        FullName = v.PrimCustomer.FullName,
                        OpportunityNumber = v.AccountNumber
                    }).Take(3);

                    return ResponseResult("Success", Json(new { lstDuplicates = dtos }));
                }
            }

             accountId = AccountMgr.CreateAccount(out accountId, model, 0);

            return ResponseResult("Success", Json(new { accountId = accountId }));
        }

        [HttpPost, ValidateAjaxAntiForgeryToken, ActionName("SaveQuestionAnswers")]
        public IHttpActionResult SaveAllQuestionAnswer(int id, [FromBody]List<Type_Question> listAnswer)
        {
            TypeQuestionManager QansMgr = new TypeQuestionManager(SessionManager.CurrentUser.PersonId, SessionManager.CurrentFranchise.FranchiseId);
            int answerId = 0;
            string status = "";
            if (listAnswer != null && id > 0)
            {
                status = QansMgr.SaveAccountQuestionAnswers(id, listAnswer, out answerId);
            }
            //Return the reponse
            return ResponseResult(status, Json(new { AnswerId = answerId }));
        }
        [HttpPost]
        public IHttpActionResult CustomerAccountUpdate(int id, [FromBody]QuickDispositionVM model)
        {
            string[] splitValue = model.Value.Split('|');
            var accountStatusId = Convert.ToInt16(splitValue[0]);
            var dispositionId = Convert.ToInt16(splitValue[1]);
            var status = AccountMgr.UpdateAccount(id, accountStatusId, dispositionId);
            return ResponseResult(status);
        }


        [HttpGet]
        public IHttpActionResult GetAccQuoteNames(int id, int OpportunityId)
        {
            string[] res = AccountMgr.GetAccQuoteNames(id, OpportunityId);
            return Json(res);
        }

        //get account details based on email-id entered in lead
        [HttpGet]
        public IHttpActionResult GetAcctDetails(int id,string value)
       {
            try
            {
                if (value != null)
                {
                    var AcctDet = AccountMgr.GetMatchDetails(value);
                    return Json(AcctDet);
                }
                else return null;
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }
        //get account details based on (Phone-number) entered in lead
        [HttpGet]
        public IHttpActionResult GetAcctPhneDetails(int id,string value)
        {
            try
            {
                if (value != null)
                {
                    var AcctDet = AccountMgr.GetMatchPhneDetails(value);
                    return Json(AcctDet);
                }
                else return null;
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }
        [HttpGet]
        public IHttpActionResult GetAcctAddrDetails(int id,string Addrvalue)
        {
            try
            {
                if (Addrvalue != "")
                {
                    var AcctDet = AccountMgr.GetMatchAddrDetails(Addrvalue);
                    return Json(AcctDet);
                }
                else return null;
            }
            catch(Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult getTerritoryForFranchise()
        {
            try
            {
                var res = AccountMgr.getTerritoryForFranchise();
                return Json(res);

            }
            catch(Exception ex) {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAccountTypeStatus(int id)
        {
            try
            {
                var result = AccountMgr.GetAccountType();
                return Json(result);
            }
            catch(Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
    }
}