﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.DTO.Source;
using HFC.CRM.Managers;
using System.Web.Http;
using HFC.CRM.Web.Filters;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class OpportunitySourcesController : BaseApiController
    {
        private FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);

        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Post(Source model)
        {
            var status = "Invalid data";
            int statusId = 0;
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = FranMgr.AddSource(model);

                statusId = model.SourceId;
            }
            return ResponseResult(status, Json(new { Id = statusId }));
        }
        /// <summary>
        /// Update Existing Campaign
        /// </summary>
        /// <param name="Campaign"></param>
        /// <returns></returns>

        [HttpPost]
        public IHttpActionResult UpdateCampaign(Campaign model)
        {
            var status = "Invalid data. Update Failed. Plese try again.";

            if (model != null)
            {
                if (SessionManager.CurrentFranchise != null)
                {
                    model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                }
                model.LastUpdatedOn = DateTime.Now;
                model.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                status = FranMgr.UpdateCampaign(model);
            }
            return ResponseResult(status);
        }

        /// <summary>
        /// Add New Campaign
        /// </summary>
        /// <param name="Campaign"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult AddNewCampaign(Campaign model)
        {
            var status = "Invalid data. Save Failed. Please try again";
            if (model != null)
            {
                if (SessionManager.CurrentFranchise != null)
                {
                    model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                }
                model.LastUpdatedOn = DateTime.Now;
                model.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                status = FranMgr.AddNewCampaign(model);
            }
            return ResponseResult(status);
        }


        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Put(Source model)
        {
            var status = "Invalid data";
            if (model != null)
            {
                model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                status = FranMgr.UpdateSource(model);
            }
            return ResponseResult(status);
        }

        /// <summary>
        /// Deletes a source from a franchise
        /// </summary>
        /// <param name="id">Source Id</param>
        /// <returns></returns>
        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Delete(int id)
        {
            var status = FranMgr.DeleteSource(SessionManager.CurrentFranchise.FranchiseId, id);
            return ResponseResult(status);
        }

        // TODO: is this required in TP???
        //public async Task<IHttpActionResult> GetSourses()
        //{
        //    var permissions = PermissionProvider.GetPermission(SessionManager.CurrentUser, "Settings", "Sources");

        //    var sourses = await FranMgr.GetSources();
        //    var list = new List<SourceDTO>();

        //    if (permissions.CanRead)
        //    {
        //        foreach (var parent in sourses.Where(v => v.ParentId == null))
        //        {
        //            list.Add(parent);
        //            list.AddRange(sourses.Where(v => v.ParentId == parent.SourceId && v.IsCampaign == true));

        //            foreach (var chield in sourses.Where(v => v.ParentId == parent.SourceId && v.IsCampaign != true))
        //            {
        //                list.Add(chield);
        //                list.AddRange(sourses.Where(v => v.ParentId == chield.SourceId && v.ParentId != parent.SourceId && v.IsCampaign == true));
        //            }
        //        }
        //    }

        //    return Ok(new
        //    {
        //        sourses = list,
        //        permissions = permissions
        //    });
        //}
    }
}
