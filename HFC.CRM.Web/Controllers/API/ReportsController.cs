﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO;
using HFC.CRM.DTO.SentEmail;
using HFC.CRM.Managers;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;


namespace HFC.CRM.Web.Controllers.API
{
    public class ReportsController : BaseApiController
    {
        ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        [HttpGet]
        public IHttpActionResult getSalesAgent(DateTime? startdate = null, DateTime? enddate = null)
        {
            try
            {
                DateTime sd = startdate != null ? Convert.ToDateTime(startdate) : DateTime.Now.AddDays(-365);
                DateTime ed = enddate != null ? Convert.ToDateTime(enddate) : DateTime.Now;
                var result = reportMgr.getSalesAgent(sd, ed);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }

        [HttpGet]
        public IHttpActionResult getMarketing(DateTime startdate, DateTime enddate)
        {
            try
            {
                var result = reportMgr.getMarketing(startdate, enddate);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }

        [HttpGet]
        public IHttpActionResult getMonthlyStatistics(DateTime startdate, DateTime enddate)
        {
            try
            {
                var result = reportMgr.getMonthlyStatistics(startdate, enddate);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }

        [HttpGet]
        public IHttpActionResult getRAWData(DateTime startdate, DateTime enddate)
        {
            try
            {
                var result = reportMgr.getRAWData(startdate, enddate);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getSalesandPurchasingDetail(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getSalesandPurchasingDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getFranchisePerformance_SalesAgent(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getFranchisePerformance_SalesAgent(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getFranchisePerformance_Territory(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getFranchisePerformance_Territory(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getFranchisePerformance_Franchise(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getFranchisePerformance_Franchise(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getFranchisePerformance_Zip(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getFranchisePerformance_Zip(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getFranchisePerformance_Vendor(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getFranchisePerformance_Vendor(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getFranchisePerformance_ProductCategory(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getFranchisePerformance_ProductCategory(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getSalesSummaryByMonth(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getSalesSummaryByMonth(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult GetUseTaxDetail(ReportFilter data)
        {
            try
            {
                var result = reportMgr.GetUseTaxDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        public IHttpActionResult GetPICProductGroupForConfigurator(string franchiseCode, int? vendorId = null)
        {

            if (string.IsNullOrEmpty(franchiseCode))
            {
                franchiseCode = "19001001";
            }
            var res = reportMgr.GetPICProductGroupForConfigurator(franchiseCode, vendorId);
            return Json(res);
            //return Json(new { valid = true, PICItems = res });
        }

        [HttpPost]
        public IHttpActionResult getVendorSalesPerformance(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getVendorSalesPerformance(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getMarketingPerformance(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getMarketingPerformance(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getSalesTax(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getSalesTax(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getSalesTaxSummary(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getSalesTaxSummary(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        [HttpPost]
        public IHttpActionResult getSalesSummaryDetail(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getSalesSummaryDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getOpenArDetail(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getOpenArDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getSalesJournalDetail(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getSalesJournalDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult GetOrdersPaidInFullDetail(ReportFilter data)
        {
            try
            {
                var Result = reportMgr.GetOrdersPaidInFullDetail(data);
                return Json(Result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getPPCMatchBack(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getPPCMatchBack(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getPaymentReport(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getPaymentReport(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult getIncomeSummaryReport(ReportFilter data)
        {
            try
            {
                var result = reportMgr.getIncomeSummaryReport(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        public byte[] MSRReport_PDF(int TerritoryID, DateTime Mth)
        {
            var data = reportMgr.getMSRHTML(TerritoryID, Mth);

            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;

            converter.Options.PdfPageSize = PdfPageSize.Letter;

            string baseUrl = HostingEnvironment.MapPath("/Templates/Base/");

            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            return pdf;
        }

        public string MSRReport_Filename(int TerritoryID, DateTime Mth)
        {
            List<MSR_DM> msrData = reportMgr.getMSRData(TerritoryID, Mth);
            string filename = "";
            if (msrData != null && msrData.Count > 0)
                filename = msrData.Select(x => x.Territory).FirstOrDefault().Split('-').Last().Trim() + "_" + Mth.ToString("MMMyyyy") + "_MSR.pdf";
            else
                filename = Mth.ToString("MMMyyyy") + "_MSR.pdf";
            return filename;
        }

        [HttpGet]
        public HttpResponseMessage getMSRReport_PDF(int TerritoryID, DateTime Mth)
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(MSRReport_PDF(TerritoryID, Mth));// new FileStream(data.physicalfilePath, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping("MSR_Report.pdf"));
                return result;

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        [HttpGet]
        public HttpResponseMessage getMSRReport_PDFDownload(int TerritoryID, DateTime Mth)
        {
            try
            {
                var stream = new MemoryStream(MSRReport_PDF(TerritoryID, Mth));// new FileStream(data.physicalfilePath, FileMode.Open);
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(stream);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = MSRReport_Filename(TerritoryID, Mth);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        [HttpPost]
        public IHttpActionResult getMSRReport_Email(EmailData_MSR data)
        {
            try
            {
                List<string> ccAddresses = new List<string>();
                List<string> bccAddresses = new List<string>();

                if (data.ccAddresses != "")
                    ccAddresses = data.ccAddresses.Split(',').ToList();

                if (data.bccAddresses != "")
                    bccAddresses = data.bccAddresses.Split(',').ToList();

                int? LeadId = null, AccountId = null, OpportunityId = null, OrderId = null;

                ExchangeManager exchange = new ExchangeManager();
                EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                byte[] pdf = MSRReport_PDF(data.TerritoryID, data.Mth);

                data.subject = HttpUtility.HtmlDecode(data.subject);
                data.body = HttpUtility.HtmlDecode(data.body);

                exchange.SendEmailMultipleAttachment(SessionManager.CurrentUser.Person.PrimaryEmail, data.subject, data.body,
                    data.toAddresses.Split(',').ToList(), ccAddresses,
                     bccAddresses, null, pdf, MSRReport_Filename(data.TerritoryID, data.Mth));


                emailMgr.AddSentEmail(data.toAddresses, data.subject, data.body, true, "", ccAddresses, bccAddresses, SessionManager.CurrentUser.Person.PrimaryEmail, LeadId, AccountId, OpportunityId, null, null, OrderId);
                return Json(true);

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAgingReport(int id)
        {
            try
            {
                var result = reportMgr.GetAgingReport();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
    }
}
