﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Models;
using HFC.CRM.Web.Filters;
using Newtonsoft.Json;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class FranchiseController : BaseApiController
    {
        readonly FranchiseManager _franMgr = new FranchiseManager(SessionManager.SecurityUser);

        [ActionName("list")]
        public IHttpActionResult Get([FromUri]FranchiseSearchFilter filter)
        {

            try
            {
                int totalRecords;
                var result = _franMgr.Get(out totalRecords
                  , false
                  , filter.pageIndex
                  , filter.pageSize
                  , filter.searchTerm
                  , filter.ShowInActive
                  );

                var fanchises = result.Select(f => new
                {
                    Name = f.Name,
                    IsSuspended = f.IsSuspended,
                    FranchiseOwners = string.Join(" & ", f.FranchiseOwners.Select(s => s.User.Person.FullName)),
                    Address = f.Address.ToString(false, true),
                    Code = f.Code,
                    FranchiseId = f.FranchiseId,
                    BrandId = f.BrandId,
                    Users = f.Users.Where(v => (f.FranchiseOwners.Any() && v.UserId != f.FranchiseOwners.First().UserId) || !f.FranchiseOwners.Any()).ToList().OrderBy(x => x.UserName),
                    //Users = f.Users.ToList(),
                    Owner = f.FranchiseOwners.FirstOrDefault(),
                    Brand = f.Brand,
                    ownername = f.owner,
                    CountryCode=f.Address.CountryCode2Digits,

                });

                //return Json(new { TotalRecords = totalRecords, FranchiseCollection = fanchises });
                return Json(fanchises.ToList());
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);

                //return this.InternalServerError(ex);
            }


        }

        public IHttpActionResult Get(int id)
        {
            try
            {

                //var model = (id != 0) ? _franMgr.Get(id) : InitFranchise();
                if (id == 0) InitFranchise();
                return Ok(new
                {
                    Franchise = _franMgr.Get(id)
                });
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
                //return ResponseResult(EventLogger.LogEvent(ex));
            }
        }



        [HttpPost]
        public IHttpActionResult SaveFranchise([FromBody]Franchise model)
        {
            try
            {
                int franchiseId = 0;
                _franMgr.InitFranchise(model);
                if (model.lstOwnerName != null)
                {
                    if (model.lstOwnerName.Count != 0)
                        model.OwnerName = JsonConvert.SerializeObject(model.lstOwnerName);
                    else model.OwnerName = null;
                }
                //model.BrandId = SessionManager.BrandIdOld;
                if (model.FranchiseId > 0)
                {
                    var successResult = _franMgr.Update(model);
                    return ResponseResult(successResult);
                }
                else
                {
                    var successResult = _franMgr.Add(out franchiseId, model);
                    CacheManager.UserCollection = null;
                    return ResponseResult(successResult, Json(new { FranchiseId = franchiseId }));
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
        [HttpPost]
        public IHttpActionResult AddressPost(Address model)
        {

            try
            {
                return ResponseResult(_franMgr.UpdateAddress(model), Json(new { model = model }));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpGet]
        [Route("api/Royalties")]
        public IHttpActionResult GetRoyalties()
        {
            try
            {
                return ResponseResult("Success", Json("test"));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        [HttpPost]
        public IHttpActionResult AssignedTerritory(string id)
        {
            {
                try
                {
                    var firstOrDefault = this.CRMDBContext.vw_Territory.FirstOrDefault(v => v.Zipcode == id);
                    if (firstOrDefault != null)
                    {
                        var currentTerritory = SessionManager.CurrentFranchise.Territories.FirstOrDefault(f => f.TerritoryId == firstOrDefault.TerritoryId);
                        if (currentTerritory != null)
                        {
                            return Json(new { territoryId = firstOrDefault.TerritoryId });
                        }
                        else
                        {
                            return Json(new { territoryId = 0 });
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return this.ResponseResult(ex);
                    //return Json(new { territoryId = -1 });
                }
                return Json(new { territoryId = -1 });
            }
        }

        [HttpPost]
        public IHttpActionResult InvoiceOption(int id, [FromBody]FranchiseInvoiceOptions model)
        {
            try
            {
                var entity = this.CRMDBContext.FranchiseInvoiceOptions.FirstOrDefault(v => v.FranchiseId == SessionManager.CurrentFranchise.FranchiseId);

                if (entity != null)
                {
                    entity.LongLegal = model.LongLegal;
                    entity.ShortLegal = model.ShortLegal;
                    entity.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                }
                else
                {
                    entity = new FranchiseInvoiceOptions();
                    entity.LongLegal = model.LongLegal;
                    entity.ShortLegal = model.ShortLegal;
                    entity.isDeleted = false;
                    entity.IsVisible = true;
                    entity.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                    this.CRMDBContext.FranchiseInvoiceOptions.Add(entity);
                }

                this.CRMDBContext.SaveChanges();

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
            return ResponseResult("Success");
        }

        [HttpGet]
        public IHttpActionResult InvoiceOption(int id)
        {
            var entity = this.CRMDBContext.FranchiseInvoiceOptions.FirstOrDefault(v => v.FranchiseId == SessionManager.CurrentFranchise.FranchiseId);
            return Ok(new
            {
                entity = entity
            });
        }



        [HttpPost]
        public IHttpActionResult AddToQB(int id)
        {
            try
            {
                var franchise = _franMgr.Get(id);

                if (franchise != null)
                {
                    _franMgr.AddQBApp(id);

                    return ResponseResult("Success");
                }

                return ResponseResult("Franchise id not valid");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        private Franchise InitFranchise()
        {
            var model = new Franchise()
            {
                Address = new Address(),
                FranchiseOwners = new List<FranchiseOwner>
                {
                    new FranchiseOwner()
                    {
                        User = new User()
                        {
                            Address = new Address(),
                            Person = new Person()
                        }
                    }
                }
            };

            return _franMgr.InitFranchise(model);
        }

        [HttpGet]
        public IHttpActionResult GetTerritory(string Zipcode, string Country)
        {
            var TerritoryName = _franMgr.GetTerritoryName(Zipcode, Country);
            return Json(TerritoryName);
        }

        [HttpPost]
        public IHttpActionResult SaveTerritory(TerritoriesModel model)
        {
            var territoryId = 0;
            var fe = _franMgr.GetFEinfo(model.FranchiseId);
            model.BrandId = fe.BrandId;

            var status = _franMgr.AssignTerritory(TerritoriesModel.MapFrom(model), model.IsInUsed, out territoryId);

            return Json(new { Result = status, TerritoryId = territoryId });
        }

        [HttpPost]
        public IHttpActionResult DeleteTerritory(TerritoriesModel model)
        {
            var status = "failed- Code not available";
            if (model.Code != null)
                status = _franMgr.DeleteTerritory(TerritoriesModel.MapFrom(model));

            return Json(new { Result = status });
        }

        [HttpGet]
        public IHttpActionResult UpdateFranchiseOwner()
        {
            var result = _franMgr.UpdateOwnerInfoIntoJson();
            return Json(result);
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult UpdateMinionins()
        {
            var result = _franMgr.updateminion();
            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult ReActivateFranchise(int id)
        {

            // var franchise = SessionManager.CurrentFranchise;
            var result = _franMgr.ReactivateFranchise(id);

            return Json(result);
        }
        [HttpGet]
        public IHttpActionResult DeactivationReasons(int id)
        {
            var result = _franMgr.GetDeactivationReasons();
            return Json(result);
        }

        [HttpPost]
        public IHttpActionResult deactivateTP(int id, DeactivateFranchiseModel model)
        {
            var status = _franMgr.Deactivated(model.FranchiseId, model.DeactivationReasonId, model.DeactivationAdditionalReason);
            return Json(status);
        }
        [HttpPost]
        public IHttpActionResult removeSuspended(int id, List<TerritoriesModel> lstTerritories)
        {

            var territory = _franMgr.GetUnexpiredRoyalty(lstTerritories[0].FranchiseId);

            //for(int i = 0;i< lstTerritories.Count(); i++)
            //{
            //    for(int j = 0;j<lstTerritories[i].FranchiseRoyalties.Count();j++)
            //    {
            //        territory[i].FranchiseRoyalties[j]
            //    }
            //}
            foreach (var item in lstTerritories)
            {
                if (item.FranchiseRoyalties != null)
                {
                    foreach (var subitem in item.FranchiseRoyalties)
                    {
                        var terr = territory.Where(x => x.TerritoryId == item.TerritoryId).FirstOrDefault();
                        var franchiseRoyalty = terr.FranchiseRoyalties.Where(x => x.RoyaltyId == subitem.RoyaltyId).FirstOrDefault();
                        franchiseRoyalty.EndOnUtc = DateTime.Parse(subitem.EndDate);

                    }
                }
            }
            var status = _franMgr.RemoveSuspend(territory);
            return Json(new { Result = status });
        }
        [HttpGet]
        public IHttpActionResult getUnexpiredRoyalties(int id)
        {
            var status = _franMgr.GetUnexpiredRoyalty(id);
            List<TerritoriesModel> lstTerritories = new List<TerritoriesModel>();
            foreach (var item in status)
            {
                var terr = new TerritoriesModel
                {
                    TerritoryId = item.TerritoryId,
                    Minion = item.Minion,
                    Name = item.Name,
                    FranchiseId = id,
                    BrandId = item.BrandId
                };

                terr.FranchiseRoyalties = new List<FranchiseRoyaltiesModel>();
                foreach (var royalty in item.FranchiseRoyalties)
                {
                    var rmdoel = new FranchiseRoyaltiesModel
                    {
                        RoyaltyId = royalty.RoyaltyId,
                        Name = royalty.Name,
                        Amount = royalty.Amount,
                        StartOnUtc = royalty.StartOnUtc,
                        EndOnUtc = royalty.EndOnUtc,
                        EndDate = royalty.EndOnUtc.GetValueOrDefault().GlobalDateFormat()

                    };
                    terr.FranchiseRoyalties.Add(rmdoel);
                }

                lstTerritories.Add(terr);
            }
            return Json(lstTerritories);
        }

        #region Batch Job to update All FE's to PIC
        [HttpGet]
        public IHttpActionResult SyncAllFranchiseCodeToPIC()
        {
            try
            {
                _franMgr.SyncAllFranchiseCodeToPIC();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

            return Json(true);
        }
        #endregion

        #region Fe Setting's MSR Reports Code's
        [HttpGet]
        public IHttpActionResult GetFeTerritories()
        {
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                int brandId = SessionManager.CurrentFranchise.BrandId;
                var result = _franMgr.GetTerritorydatas(brandId, FranchiseId);
                return Json(result);
            }
            catch(Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        public IHttpActionResult GetMonthsValue()
        {
            try
            {
                var result = _franMgr.GetMonthSets();
                return Json(result);
            }
            catch(Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateTerritoryDate([FromBody]Territory model)
        {
            try
            {
                var result = _franMgr.SaveorUpdateTerritory(model);
                return Json(result);
            }
            catch(Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
        #endregion
    }
}