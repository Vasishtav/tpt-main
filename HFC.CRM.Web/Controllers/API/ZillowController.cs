﻿using HFC.CRM.Web.Models.Zillow;
using RestSharp;
using System;
using System.Globalization;
using System.Net;
using System.Xml;
using System.Web.Http;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core;

namespace HFC.CRM.Web.Controllers.API
{
    public class ZillowController : BaseApiController
    {
        // GET: Zillow

        [HttpGet]
        public IHttpActionResult Get(string address, string citystatezip)
        {
            try
            {
                Zilloresponce z = new Zilloresponce();

                var client = new RestClient(AppConfigManager.GetZillowURL + AppConfigManager.GetZillowKey + "&address=" + address + "&citystatezip=" + citystatezip + "");
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(response.Content);

                    XmlNodeList nodeListMessage = doc.GetElementsByTagName("message");

                    XmlNodeList nodelistResponse = doc.GetElementsByTagName("response");

                    string code = "";

                    if (nodeListMessage.Count > 0)
                    {
                        code = nodeListMessage[0]["code"].InnerText;
                    }

                    if (code == "0" && nodelistResponse.Count > 0 && nodelistResponse[0]["results"] != null && nodelistResponse[0]["results"].ChildNodes.Count > 0)
                    {
                        z.yearBuilt = nodelistResponse[0]["results"].ChildNodes[0]["yearBuilt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["yearBuilt"].InnerText : "";
                        z.lotSizeSqFt = nodelistResponse[0]["results"].ChildNodes[0]["lotSizeSqFt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lotSizeSqFt"].InnerText : "";
                        z.finishedSqFt = nodelistResponse[0]["results"].ChildNodes[0]["finishedSqFt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["finishedSqFt"].InnerText : "";
                        z.bedrooms = nodelistResponse[0]["results"].ChildNodes[0]["bedrooms"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["bedrooms"].InnerText : "";
                        z.bathrooms = nodelistResponse[0]["results"].ChildNodes[0]["bathrooms"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["bathrooms"].InnerText : "";
                        z.lastSoldDate = nodelistResponse[0]["results"].ChildNodes[0]["lastSoldDate"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lastSoldDate"].InnerText : "";
                        z.lastSoldPrice = nodelistResponse[0]["results"].ChildNodes[0]["lastSoldPrice"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lastSoldPrice"].InnerText : "";
                        z.zestimate = nodelistResponse[0]["results"].ChildNodes[0]["zestimate"] != null && nodelistResponse[0]["results"].ChildNodes[0]["zestimate"]["amount"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["zestimate"]["amount"].InnerText : "";
                        z.zillowlink = nodelistResponse[0]["results"].ChildNodes[0]["links"] != null && nodelistResponse[0]["results"].ChildNodes[0]["links"]["homedetails"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["links"]["homedetails"].InnerText : "";

                        z.lotSizeSqFt = z.lotSizeSqFt != "" ? string.Format("{0:n0}", Convert.ToInt32(z.lotSizeSqFt)) : "";
                        z.finishedSqFt = z.finishedSqFt != "" ? string.Format("{0:n0}", Convert.ToInt32(z.finishedSqFt)) : "";

                        z.lastSoldPrice = z.lastSoldPrice != "" ? Convert.ToDecimal(z.lastSoldPrice).ToString("C0", CultureInfo.CurrentCulture) : "$0";
                        z.zestimate = z.zestimate != "" ? Convert.ToDecimal(z.zestimate).ToString("C0", CultureInfo.CurrentCulture) : "$0";

                        return Json(new { status = true, ZilloResponce = z });
                    }
                    else
                    {
                        return Json(new { status = false });
                    }
                }
                else
                {
                    return Json(new { status = false });
                }
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        //[HttpGet("GetZillowInfo/{address}/{citystatezip}")]
        //[Route("GetZillowInfo/{address}/{citystatezip}")]
        //public IHttpActionResult GetZillowInfo(string address, string citystatezip)
        //{
        //    Zilloresponce z = new Zilloresponce();

        //    var client = new RestClient(AppConfigManager.GetZillowURL+AppConfigManager.GetZillowKey+"&address="  + address + "&citystatezip=" + citystatezip + "");
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);

        //    if (response.StatusCode == HttpStatusCode.OK)
        //    {
        //        XmlDocument doc = new XmlDocument();
        //        doc.LoadXml(response.Content);

        //        XmlNodeList nodeListMessage = doc.GetElementsByTagName("message");

        //        XmlNodeList nodelistResponse = doc.GetElementsByTagName("response");

        //        string code = "";

        //        if (nodeListMessage.Count > 0)
        //        {
        //            code = nodeListMessage[0]["code"].InnerText;
        //        }

        //        if (code == "0" && nodelistResponse.Count > 0 && nodelistResponse[0]["results"] != null && nodelistResponse[0]["results"].ChildNodes.Count > 0)
        //        {
        //            z.yearBuilt = nodelistResponse[0]["results"].ChildNodes[0]["yearBuilt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["yearBuilt"].InnerText : "";
        //            z.lotSizeSqFt = nodelistResponse[0]["results"].ChildNodes[0]["lotSizeSqFt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lotSizeSqFt"].InnerText : "";
        //            z.finishedSqFt = nodelistResponse[0]["results"].ChildNodes[0]["finishedSqFt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["finishedSqFt"].InnerText : "";
        //            z.bedrooms = nodelistResponse[0]["results"].ChildNodes[0]["bedrooms"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["bedrooms"].InnerText : "";
        //            z.bathrooms = nodelistResponse[0]["results"].ChildNodes[0]["bathrooms"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["bathrooms"].InnerText : "";
        //            z.lastSoldDate = nodelistResponse[0]["results"].ChildNodes[0]["lastSoldDate"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lastSoldDate"].InnerText : "";
        //            z.lastSoldPrice = nodelistResponse[0]["results"].ChildNodes[0]["lastSoldPrice"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lastSoldPrice"].InnerText : "";
        //            z.zestimate = nodelistResponse[0]["results"].ChildNodes[0]["zestimate"] != null && nodelistResponse[0]["results"].ChildNodes[0]["zestimate"]["amount"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["zestimate"]["amount"].InnerText : "";
        //            z.zillowlink = nodelistResponse[0]["results"].ChildNodes[0]["links"] != null && nodelistResponse[0]["results"].ChildNodes[0]["links"]["homedetails"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["links"]["homedetails"].InnerText : "";

        //            z.lotSizeSqFt = z.lotSizeSqFt != "" ? string.Format("{0:n0}", Convert.ToInt32(z.lotSizeSqFt)) : "";
        //            z.finishedSqFt = z.finishedSqFt != "" ? string.Format("{0:n0}", Convert.ToInt32(z.finishedSqFt)) : "";

        //            z.lastSoldPrice = z.lastSoldPrice != "" ? Convert.ToDecimal(z.lastSoldPrice).ToString("C0", CultureInfo.CurrentCulture) : "$0";
        //            z.zestimate = z.zestimate != "" ? Convert.ToDecimal(z.zestimate).ToString("C0", CultureInfo.CurrentCulture) : "$0";
        //        }
        //    }

        //    return ResponseResult("Success", Json(new { z }));
        //}
    }
}