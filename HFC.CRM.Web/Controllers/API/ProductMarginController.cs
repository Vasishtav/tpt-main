﻿using HFC.CRM.Core.Common;
using HFC.CRM.Managers;

using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    using System.Collections.Generic;

    using DocumentFormat.OpenXml.Office2010.ExcelAc;

    using HFC.CRM.Managers.DTO;
    using HFC.CRM.Web.Filters;
    [LogExceptionsFilter]
    public class ProductMarginController : BaseApiController
    {
        readonly ProductsManager productsManager = new ProductsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);

        public IHttpActionResult Get(int id)
        {
            return ResponseResult(ContantStrings.Success, Json(productsManager.GetProductWithMargins(id)));
        }

        public IHttpActionResult Post(ProductMarginDTO dto)
        {
            var status = "Invalid data";
            if (dto != null)
            {
                status = productsManager.UpdateProductMargin(new List<ProductMarginDTO>() { dto });
            }
            return ResponseResult(status, Json(true));
        }

        public IHttpActionResult Put(List<ProductMarginDTO> dtos)
        {
            var status = "Invalid data";
            if (dtos != null)
            {
                status = productsManager.UpdateProductMargin(dtos);
            }
            return ResponseResult(status, Json(true));
        }
    }
}
