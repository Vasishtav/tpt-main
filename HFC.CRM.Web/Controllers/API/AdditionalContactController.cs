﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using HFC.CRM.Web.Models;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    public class AdditionalContactController : BaseApiController
    {
        public class UrlParam
        {
            public bool includeHistory { get; set; }
            public List<int> leadIds { get; set; }
        }

        readonly AdditionalContactManager contactManager = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        //global search
        public IHttpActionResult GetGlobalContactss(int id)
        {
            try
            {
                var result = contactManager.GetGlobalContacts(id);
                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetLeadContacts")]
        public IHttpActionResult GetLeadContacts(int id, bool showDeleted)
        {
            try
            {
                var result = contactManager.GetLeadContacts(id, showDeleted);
                //Note note = new Note { LeadId = id };
                //IList<Note> notes =  noteMgr.GetLeadNotes(note);

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult Recover(int id)
        {

            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                status = contactManager.Recover(id);
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("GetAccountContacts")]
        public IHttpActionResult GetAccountContacts(int id, bool showDeleted)
        {
            try
            {
                var result = contactManager.GetAccountContacts(id, showDeleted);

                return Json(result);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet, ActionName("GetOpportunityContacts")]
        public IHttpActionResult GetOpportunityContacts(int id)
        {
            try
            {
                // TODO:
                var result = contactManager.GetLeadContacts(id, false);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpGet, ActionName("CanbeDeleted")]
        public IHttpActionResult CanbeDeleted(int id)
        {
            try
            {
                var result = contactManager.CanbeDeleted(id);
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpPost]
        public IHttpActionResult AddNewContact(CustomerTP model)
        {
            try
            {
                var status = "Save Failed. Please try again";
                if (model != null)
                {
                    model.LeadId = (model.LeadId > 0) ? model.LeadId : null;
                    model.AccountId = (model.AccountId > 0) ? model.AccountId : null;
                    model.OpportunityId = (model.OpportunityId > 0) ? model.OpportunityId : null;
                    model.CreatedOn = DateTime.Now;
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                    status = contactManager.Add(model); // noteMgr.Add(model);
                }
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpPost]
        public IHttpActionResult UpdateContact(CustomerTP model)
        {

            try
            {
                var status = "Update Failed. Please try again";
                if (model != null)
                {
                    model.UpdatedOn = DateTime.Now;
                    model.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    status = contactManager.Update(model);  //noteMgr.Update(model);
                }
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {

            try
            {
                var status = "Update Failed. Please try again";
                status = contactManager.Delete(id);
                
                return ResponseResult(status);

            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
