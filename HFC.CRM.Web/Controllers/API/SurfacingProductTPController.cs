﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    public class SurfacingProductTPController: BaseApiController
    {
        SurfacingProductManagerTP Prdt_Mngr = new SurfacingProductManagerTP(SessionManager.CurrentUser);

        //To save the product
        [HttpPost]
        public IHttpActionResult Post([FromBody]SurfacingProducts model)
        {
            
                FranchiseDiscount Fran_Dis = new FranchiseDiscount();
                if (model.ProductType == 4 & model.ProductID != 0)
                {
                    Fran_Dis.DiscountId = model.ProductID;
                    Fran_Dis.Name = model.ProductName;
                    Fran_Dis.Description = model.Description;
                    Fran_Dis.Status = model.ProductStatus;
                    Fran_Dis.DiscountValue = model.Discount;
                    if (model.DiscountType == "%")
                        model.DiscountType = "1";
                    Fran_Dis.DiscountFactor = model.DiscountType;
                    var Status = Prdt_Mngr.SaveDiscount(Fran_Dis);
                    return ResponseResult("Success", Json(new { Status }));
                }
                else if (model.ProductID != 0)
                {
                    //model.ProductSurfaceSetupForCC.
                    if (model.VendorName == "")
                    {
                        model.VendorName = model.VendorID.ToString();
                    }
                    var Status = Prdt_Mngr.Save(model);
                    return ResponseResult("Success", Json(new { Status }));
                }           
            return ResponseResult("Failed");
        }

        //To Generate the product id automatically
        [HttpGet]
        public Nullable<int> GetProductNoNew(int Id)
        {
            var data = Prdt_Mngr.GetProductNumberNew();
            return data;
        }

        //list to kendo
        [HttpGet]
        public IHttpActionResult GetSearchProducts(int id)
        {
            var result = Prdt_Mngr.GetListData();
            return Json(result);
        }

        //for the view for single product
        [HttpGet]
        public IHttpActionResult Get(int id, int Productkey)
        {
            var result = Prdt_Mngr.GetProduct(Productkey);
            return Json(result);

        }
        //for the edit page the data is been listed
        [HttpGet]
        public IHttpActionResult GetParticular(int id, int ProductId)
        {
            var result = Prdt_Mngr.GetDetails(ProductId);
            return Json(result);
        }
        //dropdown selection for the product and list the details
        [HttpGet]
        public IHttpActionResult ProductOption(int id)
        {
            var result = Prdt_Mngr.GetListDetails(id);
            return Json(result);
        }
        //to get all product when checkbox is clicked
        [HttpGet]
        public IHttpActionResult GetallPrdtDetails(int id, int DropValue)
        {
            var result = Prdt_Mngr.GetListDataAll(id, DropValue);
            return Json(result);
        }
        //to get core producttype
        [HttpGet]
        public IHttpActionResult GetProductType(int id)
        {
            var result = Prdt_Mngr.GetProductType();
            return Json(result);
        }

        [HttpPost]
        public IHttpActionResult SaveTemp(int id, [FromBody] SurfacingProducts model)
        {
            if (model.ProductID != 0)
            {
                var Id = Prdt_Mngr.SaveTempdata(model);
                return Json(Id);
            }
            return null;
        }

        [HttpGet]
        public IHttpActionResult GetTempProduct(int id, string Guid)
        {
            if (Guid != null || Guid != "")
            {
                var ProductData = Prdt_Mngr.GetTempProd(Guid);
                return Json(ProductData);
            }
            return null;
        }

        /// <summary>
        /// Get the Product History Price data by Product Key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetHistoryProductPricingLog(int id)
        {
            try
            {
                var PricingLog = Prdt_Mngr.GetHistoryProductPricingLog(id);
                return Json(PricingLog);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
    }
}