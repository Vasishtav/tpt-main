﻿using HFC.CRM.Core.Common;
using HFC.CRM.Managers;

using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    using System.Collections.Generic;

    using HFC.CRM.Web.Models.Color;
    using HFC.CRM.Web.Filters;
    [LogExceptionsFilter]
    public class InfraController : BaseApiController
    {
        readonly ColorsManager colorsManager = new ColorsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);

        //public IHttpActionResult Get()
        //{
        //    //return ResponseResult("Success", Json(this.colorsManager.GetColors()));
        //}

        //public IHttpActionResult Get(int id)
        //{
        //    return ResponseResult("Success", Json(this.colorsManager.GetGategoryColors(id)));
        //}
        [HttpPost, ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Post(int brandId)
        {
            //var status = "Invalid data";
            //int colorId = 0;
            //if (model != null)
            //{
            //    status = colorsManager.AddColor(model.ColorName, out colorId);
            //    if (status == "Success" && model.CategoryId.HasValue)
            //    {
            //        colorsManager.SetGategoryColors(model.CategoryId.Value, new List<int>() { colorId }, false);
            //    }
            //}
            //return ResponseResult(status, Json(new { Id = colorId }));

            SessionManager.BrandId = brandId;
            return Ok();
        }

        [HttpGet]
        public IHttpActionResult SetBRandID(int brandId)
        {

            SessionManager.BrandId = brandId;
            return ResponseResult(ContantStrings.Success);
            //return Ok();
        }

        public IHttpActionResult Put([FromUri]int id, SetColorsToCategoryModel model)
        {
            colorsManager.SetGategoryColors(id, model != null ? model.Colors : new List<int>());
            return ResponseResult(ContantStrings.Success);
        }
     }
}
