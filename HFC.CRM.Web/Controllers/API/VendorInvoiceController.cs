﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.VendorInvoice;
using HFC.CRM.Managers;
using HFC.CRM.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    [LogExceptionsFilter]
    [CompressFilter]
    public class VendorInvoiceController : BaseApiController
    {
        VendorInvoiceManager Vendor_invoivce = new VendorInvoiceManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        /// <summary>
        /// Return dashboard info based on role
        /// </summary>
        /// <param name="id">VendorId</param>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult VendorInvoiceDashboard(int id, int ppvFilter, string dateFilter)
        {
            try
            {
                var result = Vendor_invoivce.VendorInvoiceDashboard(id, dateFilter);
                if (result.Count > 0)
                {
                    if (ppvFilter == 1) // ppvFilter==1  ppv Yes
                        result = result.Where(x => x.PPV == true).ToList();
                    else if (ppvFilter == 2) // ppvFilter==2  ppv No
                        result = result.Where(x => x.PPV == false).ToList();

                    // Get Summay info group by Vendor Name and return top 3 vendor based on CountofVariance high
                    var SummaryDashboard = result.GroupBy(x => x.VendorName).Select(x =>
                    new
                    {
                        Vendorname = x.Key,
                        VarianceAmount = x.Sum(y => y.VarianceAmount),
                        CountofVariance = x.Count(y => y.PPV == true)
                    }).OrderByDescending(y => y.CountofVariance).Take(3).ToList();

                    return Json(new { SummaryDashboard, result });
                }
                return Json(new { result });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// return Vendor invoice info contains (VInvoice,vInvoicedetail)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Getdetails(int id, string invoiceNumber)
        {
            try
            {
                var result = Vendor_invoivce.getdetail(invoiceNumber);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Vendor Reviced invoice return all the info 
        /// contains (VInvoice,vInvoicedetail,Audit trail, History)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Getdetailswithhistory(int id, string invoiceNumber)
        {
            try
            {
                var result = Vendor_invoivce.getdetailwithhistory(invoiceNumber);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Save the Reviced invoice details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult SaveVendorInvoiceDetail(VendorInvoiceDTO model)
        {
            try
            {
                var result = Vendor_invoivce.SaveVendorInvoiceDetail(model);
                return Json("Saved Successfully");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Enable the Vendor Revised Invoice once user click convert icon in vendor invoice
        /// </summary>
        /// <param name="id"></param>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult EnableRevisedInvoice(int id, string invoiceNumber)
        {
            try
            {
                var result = Vendor_invoivce.EnableRevisedInvoice(invoiceNumber);
                return Json("Saved Successfully");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpGet]
        public IHttpActionResult GridValue(int id, string invoiceNumber)
        {
            var data_rslt = Vendor_invoivce.GridValue(id, invoiceNumber);
            return Json(data_rslt);
        }
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult updateVendorInvoice()
        {
            var data_rslt = Vendor_invoivce.OneTimeUpdateVendorInvoice();
            return Json(true);
        }
    }
}