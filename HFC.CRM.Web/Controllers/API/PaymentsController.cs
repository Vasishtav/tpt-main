﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Web.Controllers.API
{
    using DTO.Order;
    using HFC.CRM.Web.Filters;
    using Serializer;
    using System.Net.Mail;
    using System.Web;
    using static ContantStrings;

    [LogExceptionsFilter]
    public class PaymentsController : BaseApiController
    {
        private PaymentManager PayMgr = new PaymentManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        /// <summary>
        /// Gets a list of sourcePayments by invoice id
        /// </summary>
        /// <param name="id">Dynamic_Invoice Id</param>
        /// <returns></returns>
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(PayMgr.GetPayments(id));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet]
        public IHttpActionResult FranchiseInvoiceOptions(int id)
        {
            try
            {
                id = SessionManager.CurrentFranchise.FranchiseId;
                return Ok(PayMgr.GetInvoicePrintOptions(id));
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        public IHttpActionResult Post([FromBody]DTO.Order.PaymentDTO model)
        {
            try
            {
                //  return Json("success");
                var result = ordermgr.AddOrderPayment(model);
                //var Payments = ordermgr.GetPaymentByOrderId(model.OrderID);
                PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                if (model.IncludeDiscount)
                    printVersion = PrintVersion.IncludeDiscount;
                else
                    printVersion = PrintVersion.ExcludeDiscount;

                InvoiceSheetReport pdf = new InvoiceSheetReport();
                Orders order = new Orders();
                order.OrderID = model.order.OrderID;
                int invoiceId = ordermgr.CreateInvoice(order, true);
                var printSignature = false;

                if (SessionManager.CurrentFranchise != null)
                    if (SessionManager.CurrentFranchise.EnableElectronicSignAdmin && SessionManager.CurrentFranchise.EnableElectronicSignFranchise)
                        printSignature = true;

                Guid filepdf = pdf.createInvoicePDF(invoiceId, printVersion, printSignature);
                ordermgr.CreateInvoice(invoiceId, filepdf);

                if (model.Sendemail == true && model.IsNotifyemails == true)
                    PayMgr.SendOrderSummaryEmail(model.OrderID, EmailType.OrderSummary, filepdf.ToString());  //4, filepdf.ToString());
                return Json(result);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Forbidden(uae.Message);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        [HttpGet]
        public IHttpActionResult ReversePaymentsAmount(int id, int OrderId)
        {
            try
            {
                var Payments = ordermgr.GetReverseAmountByOrderId(OrderId);
                return Json(Payments);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Post1(int? jobId, int? quoteId, [FromBody]Payment model)
        {
            Invoice invoice = null;

            if (model.InvoiceId == 0 && jobId.HasValue && quoteId.HasValue)
            {
                var newInvoice = new InvoiceManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise).BillQuote(quoteId.Value, jobId.Value);
                model.InvoiceId = newInvoice.InvoiceId;
            }

            var status = PayMgr.Add(model, out invoice);

            if (status == PaymentManager.Success)
                return Json(new
                {
                    LastUpdated = invoice.LastUpdated,
                    StatusEnum = invoice.StatusEnum,
                    PaidInFullOn = invoice.PaidInFullOn,
                    PaymentId = model.PaymentId,
                    CreatedOn = new DateTimeOffset(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, TimeSpan.Zero),
                    PaymentDate = new DateTimeOffset(model.PaymentDate.Year, model.PaymentDate.Month, model.PaymentDate.Day, model.PaymentDate.Hour, model.PaymentDate.Minute, model.PaymentDate.Second, TimeSpan.Zero)
                });
            else
                return ResponseResult(status);
        }

        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Put(int id, [FromBody]Payment model)
        {
            model.PaymentId = id;
            Invoice invoice = null;
            var status = PayMgr.Update(model, out invoice);

            if (status == PaymentManager.Success)
                return Json(new
                {
                    LastUpdated = invoice.LastUpdated,
                    StatusEnum = invoice.StatusEnum,
                    PaidInFullOn = invoice.PaidInFullOn,
                    PaymentDate = new DateTimeOffset(model.PaymentDate.Year, model.PaymentDate.Month, model.PaymentDate.Day, model.PaymentDate.Hour, model.PaymentDate.Minute, model.PaymentDate.Second, TimeSpan.Zero)
                });
            else
                return ResponseResult(status);
        }

        [ValidateAjaxAntiForgeryToken]
        public IHttpActionResult Delete(int id)
        {
            Invoice invoice = null;
            var status = PayMgr.Delete(id, out invoice);

            if (status == PaymentManager.Success)
                return Json(new
                {
                    LastUpdated = invoice.LastUpdated,
                    StatusEnum = invoice.StatusEnum,
                    PaidInFullOn = invoice.PaidInFullOn
                });
            else
                return ResponseResult(status);
        }

        [HttpGet]
        public IHttpActionResult CalculatePayments(int id)
        {
            using (var db = ContextFactory.Create())
            {
                var invoice = PayMgr.CalculatePayments(id, db);
                return Json(new
                {
                    LastUpdated = invoice.LastUpdated,
                    StatusEnum = invoice.StatusEnum,
                    PaidInFullOn = invoice.PaidInFullOn
                });
            }
        }

        //public string SendEmail(int OrderId, int emailType, string streamid = "")
        //{

           
        //}
    }
}
