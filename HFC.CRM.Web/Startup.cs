﻿
namespace HFC.CRM.Web
{
    using HFC.CRM.Web.Hubs;

    using Owin;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Temporarly comment out reminders completley to see spikes go away
            app.MapSignalR();
            // ReminderTicker.Instance.Start();

            SelectPdf.GlobalProperties.LicenseKey = "l7ymt6Wiprekp6akt6avuae3pKa5pqW5rq6urg==";
        }
    }
}