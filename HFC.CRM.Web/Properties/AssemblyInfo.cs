﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("HFC CRM")]
[assembly: AssemblyDescription("HFC CRM")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HFC Inc")]
[assembly: AssemblyProduct("HFC CRM")]
[assembly: AssemblyCopyright("HFC")]
[assembly: AssemblyTrademark("HFC")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("925C7D42-728E-4204-8AC5-88F30F6EE1FB")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision

[assembly: AssemblyVersion("2.2.00.*")]
//[assembly: AssemblyVersion("0.3.24.*")]
//[assembly: AssemblyVersion("0.2.28.*")]
//[assembly: AssemblyFileVersion("0.0.1.21.*")]

