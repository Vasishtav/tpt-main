﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using System.Web;
//using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.Unity;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Diagnostics;
using System.Web.Configuration;

namespace HFC.Common.Logging
{

	public class LoggingExtension : UnityContainerExtension
	{
		protected override void Initialize()
		{
			

			try
			{

				//check to see if we can resolve http context
				//if so we are a web site.
				if(HttpContext.Current != null)
					this.Container.RegisterType<ILog, WebLog>();
				else
					this.Container.RegisterType<ILog, BaseLog>();

                //Configuration cfg = null; 


                ConfigurationSection loggingConfiguration =  null;


                if (HttpContext.Current == null)
                    loggingConfiguration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).GetSection("loggingConfiguration"); // whatever you are doing currently
                else
                    loggingConfiguration = WebConfigurationManager.GetSection("loggingConfiguration") as ConfigurationSection;  //this should do the trick


                // = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);


                //if we do not specify a loggingConfiguration in the config file sections then 
                //we will self configure using one of the 2 defaults.

                

                DictionaryConfigurationSource config = null;

                if (loggingConfiguration == null)
                {
                    
#if DEBUG
                    config = GetDebugLoggingConfig();
#else           
                    config = GetReleaseLoggingConfig();                  
#endif
                    
                }
                else
                {
                    config = GetDefaultLoggingConfig(loggingConfiguration);
                    
                }

                //EnterpriseLibraryContainer.Current = EnterpriseLibraryContainer.CreateDefaultContainer(config);


			}
			catch(Exception)
			{
				this.Container.RegisterType<ILog, BaseLog>();

			}
			
		}


        public DictionaryConfigurationSource GetDebugLoggingConfig()
        {
            //build a config where we write all events to the debug stream
            //and only log warnings or above to the database;
            var builder = new ConfigurationSourceBuilder();
            var configSource = new DictionaryConfigurationSource();
            builder.UpdateConfigurationWithReplace(configSource);
            builder.ConfigureLogging().LogToCategoryNamed("General").WithOptions.SetAsDefaultCategory()
                .SendTo.Custom<DebugTraceListener>("Debug Listener")
                .Filter(SourceLevels.All)
                .SendTo.Custom<dbTraceListener>("DB Listener")
                .Filter(SourceLevels.Warning);
            builder.UpdateConfigurationWithReplace(configSource);
            return configSource;
            


        }

        public DictionaryConfigurationSource GetReleaseLoggingConfig()
        {
            var builder = new ConfigurationSourceBuilder();
            var configSource = new DictionaryConfigurationSource();
            builder.UpdateConfigurationWithReplace(configSource);
            builder.ConfigureLogging().LogToCategoryNamed("General").WithOptions.SetAsDefaultCategory()
                .SendTo.Custom<dbTraceListener>("DB Listener")
                .Filter(SourceLevels.Information);
            builder.UpdateConfigurationWithReplace(configSource);
            return configSource;

        }

        public DictionaryConfigurationSource GetDefaultLoggingConfig(ConfigurationSection cfg)
        {
            var builder = new ConfigurationSourceBuilder();
            var configSource = new DictionaryConfigurationSource();
            builder.AddSection("loggingConfiguration", cfg);

            //builder.UpdateConfigurationWithReplace(configSource);
            //builder.ConfigureUnityLogging().LogToCategoryNamed("General").WithOptions.SetAsDefaultCategory()
            //    .SendTo.Custom<dbTraceListener>("DB Listener")
            //    .Filter(SourceLevels.Information);
            builder.UpdateConfigurationWithReplace(configSource);
            return configSource;

        }

	}
}
