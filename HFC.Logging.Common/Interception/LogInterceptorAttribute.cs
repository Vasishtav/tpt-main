﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity.InterceptionExtension;
using System.Diagnostics;

namespace HFC.Common.Logging
{
    // Summary:
    //    HandlerAttribute is Base class for handler attributes used in the attribute-driven interception
    //     policy.
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class LogInterceptorAttribute : HandlerAttribute
    {
        protected TraceEventType _Level;

        public TraceEventType Level
        {
            get
            {
                return _Level;
            }

        }

        public LogInterceptorAttribute(TraceEventType Level)
        {
            this._Level = Level;

        }



        // Summary:
        //     Derived classes implement this method. When called, it creates a new call
        //     handler as specified in the attribute configuration.
        //
        // Parameters:
        //   container:
        //     The Microsoft.Practices.Unity.IUnityContainer to use when creating handlers,
        //     if necessary.
        //
        // Returns:
        //     A new call handler object.
        public override ICallHandler CreateHandler(Microsoft.Practices.Unity.IUnityContainer container)
        {
            return new LogInterceptorCallHandler(TraceEventType.Error);

        }
    }

}
