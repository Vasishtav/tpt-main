﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity.InterceptionExtension;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace HFC.Common.Logging
{

    // Summary:
    //    Handlers implement  ICallHandler interface and are called for each invocation of the
    //     pipelines that they're included in.
    public class LogInterceptorCallHandler : ICallHandler
    {

        public static readonly HFC.Common.Logging.ILog _log = HFC.Common.Logging.BaseLog.GetLogger(typeof(LogInterceptorCallHandler));
        public TraceEventType _Level;

        public LogInterceptorCallHandler(TraceEventType Level)
        {
            _Level = Level;
        }

        // Summary:
        //     Implement this method to execute your handler processing.
        //
        // Parameters:
        //   input:
        //     Inputs to the current call to the target.
        //
        //   getNext:
        //     Delegate to execute to get the next delegate in the handler chain.
        //
        // Returns:
        //     Return value from the target.
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            // Actual call to method
            IMethodReturn result = getNext()(input, getNext);

            if (result.Exception != null)
            {

                BaseLogEntry logEntry = new BaseLogEntry();
                logEntry.CallingMethodClass = this.GetType().Name;
                logEntry.AppDomainName = AppDomain.CurrentDomain.FriendlyName;
                logEntry.Message = "Exception occured";
                logEntry.Severity = this._Level;
                logEntry.StackTrace = result.Exception.StackTrace ?? string.Empty;
                logEntry.Message = result.Exception.Message;
                _log.Write(logEntry);
            }
            return result;

        }

        public int Order { get; set; }
    }






}
