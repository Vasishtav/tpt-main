﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
//using System.Diagnostics;
//using Microsoft.Practices.Unity;
//using HFC.Infrastructure.Global;

namespace HFC.Common.Logging
{
    public class BaseLog : ILog
	{
		protected Type _ClassName = typeof(object);

		public Type ClassName { get { return _ClassName; } set { _ClassName = value; } }

		protected bool? _isDebug, _isInfo, _isWarn, _isError, _isFatal;

		public static ILog GetLogger(Type className)

		{
			try
			{
                ILog l = new BaseLog();
                    //ServiceLocator.Current.GetInstance<ILog>();
				l.ClassName = className;
				return l;
			}
			catch (Exception ex)
			{
				try
				{
                    ConfigureDefaultLogger();
                    ILog l = new BaseLog();
					l.ClassName = className;
					return l;

				}
				catch //(Exception Ex)
				{

					ConfigureDefaultLogger();

					ILog l = ServiceLocator.Current.GetInstance<ILog>();
					l.ClassName = className;
					LogEntry error = new LogEntry();
					error.Message = "Error in Logger bootstrap.  Check your config!";
					error.Title = "Invalid Common Configuration!";
					error.ExtendedProperties.Add("ErrorMessage", ex.Message);
					error.ExtendedProperties.Add("Stack", ex.StackTrace);
					error.Severity = TraceEventType.Critical;
					error.AppDomainName = AppDomain.CurrentDomain.FriendlyName;
					Logger.Write(error);
					return l;

				}

			}
		}


        public static void ConfigureDefaultLogger()
		{

           LogWriter defaultWriter = new LogWriter(new LoggingConfiguration());
           Logger.SetLogWriter(defaultWriter);
           defaultWriter.Write("logtest", "General");

            defaultWriter.Configure(config => {
                config.LogSources.Clear();
                config.AddLogSource("General", SourceLevels.All, true)
                    .AddTraceListener( new dbTraceListener() );

        });

		}

		public BaseLog()
		{
		}

		public string Debug(string message)
		{
			return CreateMessage(TraceEventType.Verbose, message);

		}

		public string Debug(string message, Exception ex)
		{
			return CreateMessage(TraceEventType.Verbose, message, ex);
		}

		public string Info(string message)
		{
			return CreateMessage(TraceEventType.Information, message);
		}

		public string Info(string message, Exception ex)
		{
			return CreateMessage(TraceEventType.Information, message, ex);
		}

		public string Warn(string message)
		{
			return CreateMessage(TraceEventType.Warning, message);
		}

		public string Warn(string message, Exception ex)
		{
			return CreateMessage(TraceEventType.Warning, message, ex);
		}

		public string Error(string message)
		{
			return CreateMessage(TraceEventType.Error, message);

		}

		public string Error(string message, Exception ex)
		{
			return CreateMessage(TraceEventType.Error, message, ex);
		}

        public string Error(string message, System.Data.Entity.Validation.DbEntityValidationException ex)
        {
            return CreateMessage(TraceEventType.Error, message, ex);
        }

        public string Error(string message, System.Data.Entity.Infrastructure.DbUpdateException ex)
        {
            return CreateMessage(TraceEventType.Error, message, ex);
        }

		public string Critical(string message)
		{
			return CreateMessage(TraceEventType.Critical, message);

		}

		public string Critical(string message, Exception ex)
		{
			return CreateMessage(TraceEventType.Critical, message, ex);
		}

		protected virtual string CreateMessage(TraceEventType Level, string message)
		{
			try
			{
				BaseLogEntry Entry = new BaseLogEntry();
				Entry.Message = message;
				Entry.Severity = Level;
				Entry.StackTrace = GetStack();
				Entry.CallingMethodClass = this.ClassName.FullName;
				Entry.PathName = string.Format("Machine:{0} Serverity:{2} Class:{1}", Environment.MachineName, this.ClassName.FullName, Entry.LoggedSeverity);
                
				Logger.Write(Entry);
				return Entry.LogID; 

			}
			/*catch (Microsoft.Practices.ServiceLocation.ActivationException e)
			{
				throw e;

			}*/
			catch { return string.Empty; }

		}

		protected virtual string GetStack()
		{
			StringBuilder sb = new StringBuilder();
			try
			{
				var trace = new StackTrace(true).GetFrames();

				for (int i = 2; i < trace.Count() && i < 5; i++)
				{
					StackFrame r = trace[i];
					sb.AppendFormat("Filename: {0} Method: {1} Line: {2} Column: {3}  ",
						r.GetFileName(), r.GetMethod(), r.GetFileLineNumber(),
						r.GetFileColumnNumber());
					sb.AppendLine();


				}
			}
			catch (Exception Ex)
			{
				sb.Clear();
				sb.AppendFormat("Error {0} while attempting stack trace", Ex.Message);
				sb.AppendLine();
				sb.Append(Ex.StackTrace);

			}
			return sb.ToString();


		}

		protected virtual string CreateMessage (TraceEventType Level, string message, Exception ex)
		{
			try
			{

				BaseLogEntry Entry = new BaseLogEntry();
				Entry.StackTrace = (ex == null || ex.StackTrace == null) ? GetStack() : ex.StackTrace;
				Entry.Severity = Level;
				Entry.Message = message;
                Entry.AddErrorMessage(GetExceptionMessage(ex));
                Entry.ErrorType = ex.GetType().ToString();
				Entry.CallingMethodClass = this.ClassName.FullName;

                if (ex.InnerException != null)
                {
                    List<Exception> Exceptions = UnravelExceptions(ex);
                    Entry.InnerExceptionMessage = Exceptions.Last().Message;
                    Entry.InnerExceptionStackTrace = Exceptions.Last().Message;
                }


				Logger.Write(Entry);
				return Entry.LogID; 
			}
			catch (Exception Ex)
			{
				try
				{

					string sSource;
					string sLog;
					string sEvent;

					sSource = System.Reflection.Assembly.GetEntryAssembly().CodeBase;
					sLog = "Application";

					StringBuilder sb = new StringBuilder();
					sb.AppendLine("Error writing Log!");
					sb.AppendLine(Ex.Message);
					sb.AppendLine(Ex.StackTrace);
					if (ex.InnerException != null)
					{
						if(ex.InnerException != null)
							sb.AppendLine(ex.InnerException.Message);
						if (ex.StackTrace != null)
							sb.AppendLine(ex.InnerException.StackTrace);


					}
					sEvent = sb.ToString();

					if (!EventLog.SourceExists(sSource))
						EventLog.CreateEventSource(sSource, sLog);

					EventLog.WriteEntry(sSource, sEvent);
					EventLog.WriteEntry(sSource, sEvent,
						EventLogEntryType.Error);

					return string.Empty;

				}
				/*catch (Microsoft.Practices.ServiceLocation.ActivationException e)
				{
					throw e;

				}*/
				catch { return string.Empty; }
			}

		}


		public string Write(BaseLogEntry logEvent)
		{
			try
			{
				Logger.Write(logEvent);
				return logEvent.LogID;
			}
			catch
			{ return string.Empty; }
		}



		public bool HasDebugLevel
		{
			get
			{
				if(!_isDebug.HasValue)
					_isDebug = Logger.Writer.TraceSources.Values.Any(x =>(x.Level == SourceLevels.All) || ( x.Level >= SourceLevels.Verbose && x.Level != SourceLevels.Off));

				return _isDebug.Value;				
			}

		}

		public bool HasInfoLevel
		{
			get
			{
				if (!_isInfo.HasValue)
					_isInfo = Logger.Writer.TraceSources.Values.Any(x =>(x.Level == SourceLevels.All) || (x.Level >= SourceLevels.Information && x.Level != SourceLevels.Off));

				return _isInfo.Value;
			}

		}

		public bool HasWarningLevel
		{
			get
			{
				if (!_isWarn.HasValue)
					_isWarn = Logger.Writer.TraceSources.Values.Any(x =>(x.Level == SourceLevels.All) || ( x.Level >= SourceLevels.Warning && x.Level != SourceLevels.Off));

				return _isWarn.Value;
			}

		}

		public bool HasErrorLevel
		{
			get
			{
				if (!_isError.HasValue)
					_isError = Logger.Writer.TraceSources.Values.Any(x =>(x.Level == SourceLevels.All) || ( x.Level >= SourceLevels.Error && x.Level != SourceLevels.Off));

				return _isError.Value;
			}

		}

		public bool HasCriticalLevel
		{
			get
			{
				if (!_isFatal.HasValue)
					_isFatal = Logger.Writer.TraceSources.Values.Any(x =>(x.Level == SourceLevels.All) || ( x.Level >= SourceLevels.Critical && x.Level != SourceLevels.Off));

				return _isFatal.Value;
			}

		}

        protected string GetExceptionMessage(Exception ex)
        {
            string s = string.Empty;

            if(ex != null && !string.IsNullOrEmpty(ex.Message))
            {
                s =  ex.Message;
            }

            return s;
        }

        protected List<Exception> UnravelExceptions(Exception ex)
        {
            List<Exception> Exceptions = new List<Exception>();
            Exceptions.Add(ex);
            //walk down the inner exception stack.
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                Exceptions.Add(ex);
            }

            return Exceptions;
        }




    }
}
