﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;

namespace HFC.Common.Logging
{
    [ConfigurationElementType(typeof(CustomTraceListenerData))]
   public  class DebugTraceListener : CustomTraceListener 
    {
        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {

            if ((this.Filter == null) || this.Filter.ShouldTrace(eventCache, source, eventType, id, null, null, data, null))
            {
                if (data is BaseLogEntry)
                {
                    BaseLogEntry logEntry = data as BaseLogEntry;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("**********************************************************************");
                    sb.AppendLine(string.Format("{0} {1} : {2}", DateTime.Now.ToString().PadRight(20), logEntry.LoggedSeverity, logEntry.CallingMethodClass));
                    sb.AppendLine(string.Format("Message: {0}", logEntry.Message));
                    if(logEntry.StackTrace.Length > 0)
                        sb.AppendLine(string.Format("Stack: {0}", logEntry.StackTrace));
                    if (logEntry.InnerExceptionMessage.Length > 0)
                        sb.AppendLine(string.Format("Inner Exception: {0}", logEntry.InnerExceptionMessage));
                    if (logEntry.InnerExceptionStackTrace.Length > 0)
                        sb.AppendLine(string.Format("Inner Exception Stack: {0}", logEntry.InnerExceptionStackTrace));
                    sb.AppendLine("**********************************************************************");
                    Debug.Write(sb.ToString());
                }
                else if (data is string)
                {
                    Write(data as string);
                }
                else
                {
                    base.TraceData(eventCache, source, eventType, id, data);
                }
            }
        }

        public override void Write(string message)
        {
            Debug.Write(message);
        }

        public override void WriteLine(string message)
        {
            Debug.WriteLine(message);
        }

    }
}
