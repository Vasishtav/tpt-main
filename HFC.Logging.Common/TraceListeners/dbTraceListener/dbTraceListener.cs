﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks; 
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;

using HFC.Common.Logging;

namespace HFC.Common.Logging
{

    /// <summary>
    /// A <see cref="System.Diagnostics.TraceListener"/> that writes to the CareMC_WH a database, formatting the output with an <see cref="ILogFormatter"/>.
    /// </summary>
    [ConfigurationElementType(typeof(CustomTraceListenerData))]
    public class dbTraceListener : CustomTraceListener
    {
        elmahContext db { get; set; }
        public dbTraceListener()
        {
            db = new elmahContext();
        }
        public override void Write(string message)
        {
            BaseLogEntry MC = new BaseLogEntry();
            MC.Message = message;
            TrySaveException(MC);

        }


        /// <summary>
        /// Writes a message to the underlying CareMC database
        /// </summary>
        /// <param name="message">The Message</param>
        public override void WriteLine(string message)
        {
            Write(message);
        }


        /// <summary>
        /// Delivers the trace data to the underlying database.
        /// </summary>
        /// <param name="eventCache">The context information provided by <see cref="System.Diagnostics"/>.</param>
        /// <param name="source">The name of the trace source that delivered the trace data.</param>
        /// <param name="eventType">The type of event.</param>
        /// <param name="id">The id of the event.</param>
        /// <param name="data">The data to trace.</param>
        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {

            if ((this.Filter == null) || this.Filter.ShouldTrace(eventCache, source, eventType, id, null, null, data, null))
            {
                if (data is BaseLogEntry)
                {
                    BaseLogEntry logEntry = data as BaseLogEntry;
                    
                    TrySaveException(logEntry);
                }
                else if (data is string)
                {
                    Write(data as string);
                }
                else
                {
                    base.TraceData(eventCache, source, eventType, id, data);
                }
            }
        }

        /// <summary>
        /// Try to save error in DB
        /// </summary>
        /// <param name="logEntry">The LogEntry to store in the database</param>
        private void TrySaveException(BaseLogEntry logEntry)
        {

            try
            {
                SaveException(logEntry);
            }
            catch (Exception Ex)
            {

                string sSource;
                string sLog;
                string sEvent;

                sSource = System.Reflection.Assembly.GetEntryAssembly().CodeBase;
                sLog = "Application";

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error writing DBTraceListener!");
                sb.AppendLine(Ex.Message);
                sb.AppendLine(Ex.StackTrace);
                sEvent = sb.ToString();

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);
                EventLog.WriteEntry(sSource, sEvent);
                EventLog.WriteEntry(sSource, sEvent,
                EventLogEntryType.Error);
            }
        }




        private ELMAH_Error MapToELMAH_Error(BaseLogEntry logEntry)
        {
            return new ELMAH_Error()
            {
                ErrorId = Guid.NewGuid(),
                Application = logEntry.AppDomainName,
                Host = logEntry.MachineName,
                Type = logEntry.ErrorType,
                Source = logEntry.ProcessName,
                Message = logEntry.Win32ThreadId,
                User = (!string.IsNullOrEmpty(logEntry.User)) ? logEntry.User : string.Empty,
                AllXml = logEntry.StackTrace,
                StatusCode = (int)logEntry.Severity,
                TimeUtc = DateTime.Now,
            };
        }

        private void SaveException(BaseLogEntry logEntry)
        {
            ELMAH_Error _ELMAH_Error = MapToELMAH_Error(logEntry);
            db.ELMAH_Errors.Add(_ELMAH_Error);
            SaveChangesAsync(db);
        }

        public void SaveChangesAsync(elmahContext db)
        {
            try
            {
                //db.SaveChangesAsync();
                //db.SaveChangesAsync().ConfigureAwait(continueOnCapturedContext: true);
                db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                var EntityValidationErrors = e.EntityValidationErrors;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException e)
            {
                var errData = e.Data;
                var errEntries = e.Entries;
            }
            catch (Exception e)
            {
                var errData = e.InnerException.Data;
            }
        }
    }
}
