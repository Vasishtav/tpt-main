﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using CorVel.Logging.Common;
using CareMC.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;


namespace CorVel.Logging.Common
{

    /// <summary>
    /// A <see cref="System.Diagnostics.TraceListener"/> that writes to the CareMC_WH a database, formatting the output with an <see cref="ILogFormatter"/>.
    /// </summary>
    [ConfigurationElementType(typeof(CustomTraceListenerData))]
    public class CareMCMongoDBTraceListener : CustomTraceListener
    {
        const string _WriteLogStoredProcName = "dbo.WP_EventLog";

        //TODO: we need to decide how we want to support CareMC.Configuration so that we can 
        //decrypt the connection strings.
        private readonly SqlDatabase _Database = new SqlDatabase(AppSettings.getObject().get_DecryptItems("CareMC_WH"));  //hardcoded for now.  We need to get the DAL project up and running.


        /// <summary>
        /// Writes a message to the underlying CareMC database
        /// </summary>
        /// <param name="message">The Message</param>
        public override void Write(string message)
        {
            CareMCLogEntry MC = new CareMCLogEntry();
            MC.Message = message;
            ExecuteStoredProcedure(MC);

        }


        /// <summary>
        /// Writes a message to the underlying CareMC database
        /// </summary>
        /// <param name="message">The Message</param>
        public override void WriteLine(string message)
        {
            Write(message);
        }


        /// <summary>
        /// Delivers the trace data to the underlying database.
        /// </summary>
        /// <param name="eventCache">The context information provided by <see cref="System.Diagnostics"/>.</param>
        /// <param name="source">The name of the trace source that delivered the trace data.</param>
        /// <param name="eventType">The type of event.</param>
        /// <param name="id">The id of the event.</param>
        /// <param name="data">The data to trace.</param>
        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {

            if ((this.Filter == null) || this.Filter.ShouldTrace(eventCache, source, eventType, id, null, null, data, null))
            {
                if (data is CareMCLogEntry)
                {
                    CareMCLogEntry logEntry = data as CareMCLogEntry;
                    if (ValidateParameters(logEntry))
                        ExecuteStoredProcedure(logEntry);
                }
                else if (data is string)
                {
                    Write(data as string);
                }
                else
                {
                    base.TraceData(eventCache, source, eventType, id, data);
                }
            }


        }

        /// <summary>
        /// Declare the supported attributes for <see cref="FormattedDatabaseTraceListener"/>
        /// </summary>
        protected override string[] GetSupportedAttributes()
        {
            return new string[] { };
        }

        /// <summary>
        /// Validates that enough information exists to attempt executing the stored procedures
        /// </summary>
        /// <param name="logEntry">The LogEntry to validate.</param>
        /// <returns>A Boolean indicating whether the parameters for the LogEntry configuration are valid.</returns>
        private bool ValidateParameters(CareMCLogEntry logEntry)
        {
            bool valid = true;

            if (_WriteLogStoredProcName == null ||
                _WriteLogStoredProcName.Length == 0)
            {
                //TODO:Need to decide if we need to validate the internal logEntryMembers


                return false;
            }

            return valid;
        }

        /// <summary>
        /// Executes the stored procedures
        /// </summary>
        /// <param name="logEntry">The LogEntry to store in the database</param>
        private void ExecuteStoredProcedure(CareMCLogEntry logEntry)
        {

            try
            {
                using (DbConnection connection = _Database.CreateConnection())
                {
                    connection.Open();
                    try
                    {
                        using (DbTransaction transaction = connection.BeginTransaction())
                        {
                            try
                            {
                                int logID = Convert.ToInt32(ExecuteWriteLogStoredProcedure(logEntry, _Database, transaction));
                                logEntry.ExtendedProperties.Add("LogID", logID);

                                transaction.Commit();
                            }
                            catch
                            {
                                transaction.Rollback();
                                throw;
                            }
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                //something really bad happened and I was not able to log for some reason.
                //attempt to write to the event log

                string sSource;
                string sLog;
                string sEvent;

                sSource = System.Reflection.Assembly.GetEntryAssembly().CodeBase;
                sLog = "Application";

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Error writing DBTraceListener!");
                sb.AppendLine(Ex.Message);
                sb.AppendLine(Ex.StackTrace);
                sEvent = sb.ToString();

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);

                EventLog.WriteEntry(sSource, sEvent);
                EventLog.WriteEntry(sSource, sEvent,
                    EventLogEntryType.Error);


            }
        }





        /// <summary>
        /// Executes the WriteLog stored procedure
        /// </summary>
        /// <param name="logEntry">The LogEntry to store in the database.</param>
        /// <param name="db">An instance of the database class to use for storing the LogEntry</param>
        /// <param name="transaction">The transaction that wraps around the execution calls for storing the LogEntry</param>
        /// <returns>An integer for the LogEntry Id</returns>
        private int ExecuteWriteLogStoredProcedure(CareMCLogEntry logEntry, Microsoft.Practices.EnterpriseLibrary.Data.Database db, DbTransaction transaction)
        {
            DbCommand cmd = db.GetStoredProcCommand(_WriteLogStoredProcName);
            db.AddInParameter(cmd, "THREAD", DbType.Int32, logEntry.Win32ThreadId);
            db.AddInParameter(cmd, "SESSION_ID", DbType.AnsiString, logEntry.SessionID ?? string.Empty);
            db.AddInParameter(cmd, "PATH_NAME", DbType.AnsiString, logEntry.PathName ?? string.Empty);
            db.AddInParameter(cmd, "DSCP", DbType.AnsiString, string.Format("{0} - MachineName: {1};", logEntry.LoggedSeverity, logEntry.MachineName));
            db.AddInParameter(cmd, "MESSAGE", DbType.AnsiString, logEntry.CallingMethodClass + " - Message :\r\n " + logEntry.Message + " - Error Message: \r\n" + logEntry.ErrorMessages ?? string.Empty);
            db.AddInParameter(cmd, "NOTIF_QUEUE_REQUEST_DATE", DbType.DateTime, logEntry.NotifyQueueRequestDate.HasValue ? (object)logEntry.NotifyQueueRequestDate.Value : (object)DBNull.Value);
            db.AddInParameter(cmd, "STACK_TRACE", DbType.AnsiString, logEntry.StackTrace);
            db.AddOutParameter(cmd, "EVENT_ID", DbType.Int32, 4);
            db.ExecuteNonQuery(cmd, transaction);
            int logId = Convert.ToInt32(cmd.Parameters[cmd.Parameters.Count - 1].Value, CultureInfo.InvariantCulture);
            return logId;
        }


    }
}
