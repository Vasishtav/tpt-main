﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Data.Entity.Validation;
using System.Diagnostics.CodeAnalysis;

namespace HFC.Common.Logging
{
    public interface ILog
    {
        Type ClassName { get; set; }
        string Debug(string message);
		string Debug(string message, Exception ex);

		string Info(string message);
		string Info(string message, Exception ex);

		string Warn(string message);
		string Warn(string message, Exception ex);

		string Error(string message);
		string Error(string message, Exception ex);
        string Error(string message, System.Data.Entity.Validation.DbEntityValidationException ex);
        string Error(string message, System.Data.Entity.Infrastructure.DbUpdateException ex);

		string Critical(string message);
		string Critical(string message, Exception ex);

		bool HasDebugLevel { get; }
		bool HasInfoLevel { get; }
		bool HasWarningLevel { get; }
		bool HasErrorLevel { get; }
		bool HasCriticalLevel { get; }



		string Write(BaseLogEntry logEvent);
	 
    }
}
