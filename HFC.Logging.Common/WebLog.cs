﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Web;

namespace HFC.Common.Logging
{
	public class WebLog : BaseLog
	{
		protected override string CreateMessage(System.Diagnostics.TraceEventType Level, string message)
		{
			try
			{
				BaseLogEntry Entry = new BaseLogEntry();
				Entry.Message = message;
				Entry.Severity = Level;
				Entry.StackTrace = GetStack();
				Entry.CallingMethodClass = this.ClassName.FullName;
				HttpContext ctx = HttpContext.Current;
				Entry.PathName = ctx.Request.Path;
                //logEntry.SessionID = (ctx.Request.QueryString["SID"] != null) ? ctx.Request.QueryString["SID"].ToString() : string.Empty;
                /// TODO: Create HFC Web Object where I get the session instance (franchise and user and maybe the AntiForgery Token)
				Logger.Write(Entry);
				if (Entry.ExtendedProperties.ContainsKey("LogID"))
				{
					try
					{
						return ((int)Entry.ExtendedProperties["LogID"]).ToString();
					}
					catch
					{
						return string.Empty;

					}
				}
				else
					return string.Empty;
			}
			catch { return string.Empty; }
		}

       


		protected override string CreateMessage(System.Diagnostics.TraceEventType Level, string message, Exception ex)
		{
			try
			{
				BaseLogEntry logEntry = new BaseLogEntry();
                logEntry.StackTrace = (ex == null || ex.StackTrace == null) ? GetStack() : ex.StackTrace;
				logEntry.Severity = Level;
				logEntry.Message = message;
                logEntry.AddErrorMessage(GetExceptionMessage(ex));
				logEntry.CallingMethodClass = this.ClassName.FullName;

                if (ex.InnerException != null)
                {
                    List<Exception> ExceptionStack = UnravelExceptions(ex);
                    logEntry.InnerExceptionMessage = ExceptionStack.Last().Message;
                    logEntry.InnerExceptionStackTrace = ExceptionStack.Last().StackTrace;
                }

				HttpContext ctx = HttpContext.Current;

				logEntry.PathName = ctx.Request.Path;
                //logEntry.SessionID = (ctx.Request.QueryString["SID"] != null) ? ctx.Request.QueryString["SID"].ToString() : string.Empty;
				Logger.Write(logEntry);
				if (logEntry.ExtendedProperties.ContainsKey("LogID"))
				{
					try
					{
						return ((int)logEntry.ExtendedProperties["LogID"]).ToString();
					}
					catch
					{
						return string.Empty;

					}
				}
				else
					return string.Empty;
			}
			catch { return string.Empty; }
		}
	}
}
