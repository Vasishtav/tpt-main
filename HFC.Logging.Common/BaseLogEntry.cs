﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;


namespace HFC.Common.Logging
{
    public class BaseLogEntry : LogEntry 
    {
		public string CallingMethodClass
		{
			get
			{
				if (ExtendedProperties.ContainsKey("CallingMethodClass"))
					return ExtendedProperties["CallingMethodClass"].ToString();
				else
					return string.Empty;
			}
			set
			{
				if (ExtendedProperties.ContainsKey("CallingMethodClass"))
					ExtendedProperties["CallingMethodClass"] = value;
				else
					ExtendedProperties.Add("CallingMethodClass", value);
			}
		}

		public string LogID
		{
			get
			{
				if (ExtendedProperties.ContainsKey("LogID"))
				{
					try
					{
						return ((int)ExtendedProperties["LogID"]).ToString();
					}
					catch
					{
						return string.Empty;
					}
				}
				else
					return string.Empty;
			}


		}

        public string StackTrace 
		{
			get
			{
				if (ExtendedProperties.ContainsKey("StackTrace") && ExtendedProperties["StackTrace"] != null)
					return ExtendedProperties["StackTrace"].ToString();
				else
					return string.Empty;
			}
			set
			{
				if (ExtendedProperties.ContainsKey("StackTrace"))
					ExtendedProperties["StackTrace"] = value;
				else
					ExtendedProperties.Add("StackTrace", value);
			}
		}

        public string InnerExceptionStackTrace
        {
            get
            {
                if (ExtendedProperties.ContainsKey("InnerExceptionStackTrace") && ExtendedProperties["InnerExceptionStackTrace"] != null)
                    return ExtendedProperties["InnerExceptionStackTrace"].ToString();
                else
                    return string.Empty;
            }
            set
            {
                if (ExtendedProperties.ContainsKey("InnerExceptionStackTrace"))
                    ExtendedProperties["InnerExceptionStackTrace"] = value;
                else
                    ExtendedProperties.Add("InnerExceptionStackTrace", value);
            }
        }

        public string InnerExceptionMessage
        {
            get
            {
                if (ExtendedProperties.ContainsKey("InnerExceptionMessage") && ExtendedProperties["InnerExceptionMessage"] != null)
                    return ExtendedProperties["InnerExceptionMessage"].ToString();
                else
                    return string.Empty;
            }
            set
            {
                if (ExtendedProperties.ContainsKey("InnerExceptionMessage"))
                    ExtendedProperties["InnerExceptionMessage"] = value;
                else
                    ExtendedProperties.Add("InnerExceptionMessage", value);
            }




        }

		public string PathName
		{
			get
			{
				if (ExtendedProperties.ContainsKey("PathName") && ExtendedProperties["PathName"] != null)
					return ExtendedProperties["PathName"].ToString();
				else
					return string.Empty;
			}
			set
			{
				if (ExtendedProperties.ContainsKey("PathName"))
					ExtendedProperties["PathName"] = value;
				else
					ExtendedProperties.Add("PathName", value);
			}
		}

		public DateTime? NotifyQueueRequestDate
		{
			get
			{
				if (ExtendedProperties.ContainsKey("NotifyQueueRequestDate") && ExtendedProperties["NotifyQueueRequestDate"] != null)
					return (DateTime?)ExtendedProperties["NotifyQueueRequestDate"];
				else
					return (DateTime?)null;
			}
			set
			{
				if (ExtendedProperties.ContainsKey("NotifyQueueRequestDate"))
					ExtendedProperties["NotifyQueueRequestDate"] = value;
				else
					ExtendedProperties.Add("NotifyQueueRequestDate", value);
			}
		}

        public string ErrorType { get; set; }
        public string User { get; set; }
        public string Franchise { get; set; }
    }
}
