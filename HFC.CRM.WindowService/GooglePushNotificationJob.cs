﻿using HFC.CRM.Managers;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.WindowService
{
    [DisallowConcurrentExecution]
    public class GooglePushNotificationJob : IJob
    {

        public void Execute(IJobExecutionContext context)
        {
            GoogleManager.SyncPushNotificationRequests();
        }
    }
}
