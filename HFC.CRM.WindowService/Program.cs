﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.WindowService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            try
            {
                ServicesToRun = new ServiceBase[] 
                { 
                    new CRMScheduler() 
                };
#if DEBUG            
                RunInteractive(ServicesToRun);
#else
                ServiceBase.Run(ServicesToRun);
#endif
            }
            catch (Exception ex)
            {
#if DEBUG
                using (var sw = new System.IO.StreamWriter("C:\\hfccrm_windows_service_error.log"))
                {
                    sw.WriteLine(ex.Message);
                    sw.WriteLine("Stack Trace:");
                    sw.WriteLine(ex.StackTrace);
                    sw.Flush();
                    sw.Close();
                }
#endif
            }
        }

        /// <summary>
        /// This is only for debugging, use break points to step through
        /// </summary>
        /// <param name="servicesToRun"></param>
        static void RunInteractive(ServiceBase[] servicesToRun)
        {
            Console.WriteLine("Services running in interactive mode.");
            Console.WriteLine();

            MethodInfo onStartMethod = typeof(ServiceBase).GetMethod("OnStart", BindingFlags.Instance | BindingFlags.NonPublic);
            
            foreach (ServiceBase service in servicesToRun)
            {
                Console.Write("Starting {0}...", service.ServiceName);
                onStartMethod.Invoke(service, new object[] { new string[] { } });
                Console.Write("Started");
            }
        }

    }
}
