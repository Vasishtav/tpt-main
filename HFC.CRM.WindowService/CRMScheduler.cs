﻿using HFC.CRM.Managers;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.WindowService
{
    public partial class CRMScheduler : ServiceBase
    {        
        private IScheduler scheduler;
        public static ExchangeSubscriptionManager EWSSubscriptionMgr;

        public CRMScheduler()
        {
            InitializeComponent();

            EWSSubscriptionMgr = new ExchangeSubscriptionManager();
            var schedulerFactory = new StdSchedulerFactory();
            scheduler = schedulerFactory.GetScheduler();            
        }
        
        protected override void OnStart(string[] args)
        {
            DateTimeOffset runTime = DateBuilder.EvenMinuteDate(DateTimeOffset.UtcNow);                        

            IJobDetail exchangeSubJob = JobBuilder.Create<ExchangeSubscribeJob>()
                .WithIdentity("exchangeSubscribeJob", "syncGroup")
                .RequestRecovery(false)
                .Build();
            ITrigger ewsSubTrigger = TriggerBuilder.Create()
                .WithIdentity("ewsSubscribeTrigger", "syncGroup")
                .StartAt(runTime)
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(ExchangeManager.ReconnectTimeout).RepeatForever())
                .Build();
            scheduler.ScheduleJob(exchangeSubJob, ewsSubTrigger);

            //Check every ReconnectTimeout minute for newly enabled google user to set up push notification
            IJobDetail googleSubscribeJob = JobBuilder.Create<GooglePushNotificationJob>()
                .WithIdentity("googleSubscribeJob", "syncGroup")
                .Build();
            ITrigger googleSubTrigger = TriggerBuilder.Create()
                .WithIdentity("googleSubTrigger", "syncGroup")
                .StartAt(runTime)
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(GoogleManager.ReconnectTimeout).RepeatForever())
                .Build();
            scheduler.ScheduleJob(googleSubscribeJob, googleSubTrigger);

            //Check every minute for CRM events to push to either google and/or exchange
            IJobDetail upSyncJob = JobBuilder.Create<SyncJob>()
                .WithIdentity("upSyncJob", "syncGroup")
                .RequestRecovery(false)
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("everyMinuteTrigger", "syncGroup")
                .StartAt(runTime)
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(1).RepeatForever())
                .Build();
            scheduler.ScheduleJob(upSyncJob, trigger);
            
            scheduler.Start();
        }

        //protected override void OnPause()
        //{
        //    scheduler.PauseAll();
        //    base.OnPause();
        //}

        //protected override void OnContinue()
        //{
        //    scheduler.ResumeAll();
        //    base.OnContinue();
        //}
        private void StopEverything()
        {
            if (EWSSubscriptionMgr != null)
            {
                EWSSubscriptionMgr.Disconnect();
            }
            scheduler.Shutdown(true); 
        }

        protected override void OnStop()
        {
            StopEverything();
            base.OnStop();
        }

        protected override void OnShutdown()
        {
            StopEverything();
            base.OnShutdown();
        }
    }
}
