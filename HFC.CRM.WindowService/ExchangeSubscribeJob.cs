﻿using HFC.CRM.Managers;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.WindowService
{
    /// <summary>
    /// This job will subscribe to Exchange Streaming subscription and Google push notification
    /// </summary>
    [DisallowConcurrentExecution]
    public class ExchangeSubscribeJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            if (CRMScheduler.EWSSubscriptionMgr != null)
            {
                CRMScheduler.EWSSubscriptionMgr.SubscribeUsers();
            }
        }
    }
}
