﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace HFC.CRM.QBSync.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            UnityConfig.RegisterComponents();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            EventLogger.LogEvent("QBO API Pool_Started " + SessionManager.CacheTime.ToString());
        }

        protected void Application_BeginRequest()
        {
            var now = DateTime.Now;
            var cacheTime = SessionManager.CacheTime;

            if (now < cacheTime)
            {
                EventLogger.LogEvent("QBO API Application_BeginRequest cache lost " + SessionManager.CacheTime.ToString());
            }

            //if (DateTime.Now - SessionManager.CacheLogTime > TimeSpan.FromMinutes(5))
            //{
            //    SessionManager.CacheLogTime = DateTime.Now;
            //    var data = SessionManager.GetCacheSize();
            //    EventLogger.LogEvent("QBO API Application_BeginRequest log data: " + String.Join(", ", data.ToArray()));
            //}
        }


    }
}
