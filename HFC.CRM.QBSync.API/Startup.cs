﻿
using Owin;

namespace HFC.CRM.QBSync.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Temporarly comment out reminders completley to see spikes go away
            app.MapSignalR();
            //ReminderTicker.Instance.Start();
        }
    }
}