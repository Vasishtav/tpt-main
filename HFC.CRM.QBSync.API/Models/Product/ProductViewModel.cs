﻿
namespace HFC.CRM.QBSync.API.Models.Product
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int? ProductTypeId { get; set; }

        public string ProductType { get; set; }

        public int? FranchiseId { get; set; }
    }
}