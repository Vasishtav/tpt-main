﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;

namespace HFC.CRM.QBSync.API.Filters
{
    public class AuthAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var qbToken = actionContext.Request.Headers.FirstOrDefault(x => x.Key == "qbToken");
            if (qbToken.Value != null && qbToken.Value.Count() > 0)
            {
                SessionManager.QBTokenApi = qbToken.Value.FirstOrDefault();
            }
        }
    }
}
