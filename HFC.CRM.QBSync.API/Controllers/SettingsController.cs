﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using DotNetOpenAuth.OpenId.RelyingParty;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.Data.Context;
using HFC.CRM.Managers;
using HFC.CRM.Managers.Quickbooks;
using HFC.CRM.QBSync.API.Models.Product;
using HFC.CRM.Managers.QBSync;
using Intuit.Ipp.Core;
using HFC.CRM.QBSync.API.Filters;
using HFC.CRM.Core.Logs;
using Newtonsoft.Json;
using Intuit.Ipp.OAuth2PlatformClient;
using System.Threading.Tasks;
using System.Security.Claims;
using Intuit.Ipp.Security;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Data;

namespace HFC.CRM.QBSync.API.Controllers
{
    [LogExceptionsFilter]
    public class SettingsController : BaseController
    {
        public string Email { get; set; }

        public static string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
        public static string clientsecret = ConfigurationManager.AppSettings["clientsecret"].ToString();
        public static string redirectUrl = ConfigurationManager.AppSettings["redirectUrl"].ToString();
        public static string environment = ConfigurationManager.AppSettings["appEnvironment"].ToString();

        //Instantiate OAuth2Client object with clientId, clientsecret, redirectUrl and environment
        public static OAuth2Client auth2Client = new OAuth2Client(clientid, clientsecret, redirectUrl, environment);

        //Generate authorize url to get the OAuth2 code
        [AllowAnonymous]
        public ActionResult InitiateAuth()
        {

            List<OidcScopes> scopes = new List<OidcScopes>();
            scopes.Add(OidcScopes.Accounting);
            string authorizeUrl = auth2Client.GetAuthorizationURL(scopes);
            return Redirect(authorizeUrl);
        }

        [AllowAnonymous]
        public async Task<ActionResult> OAuthIndex()
        {
            string code = Request.QueryString["code"] ?? "none";
            string realmId = Request.QueryString["realmId"] ?? "none";

            var tokenResponse = await auth2Client.GetBearerTokenAsync(code);

            var access_token = tokenResponse.AccessToken;
            var access_token_expires_at = tokenResponse.AccessTokenExpiresIn;

            var refresh_token = tokenResponse.RefreshToken;
            var refresh_token_expires_at = tokenResponse.RefreshTokenExpiresIn;

            //var principal = User as ClaimsPrincipal;
            OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(access_token);
            ServiceContext serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, oauthValidator);
            DataService commonServiceQBO = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Invoice> inService = new QueryService<Intuit.Ipp.Data.Invoice>(serviceContext);
            var In = inService.ExecuteIdsQuery("SELECT * FROM Invoice").FirstOrDefault();

            //OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(principal.FindFirst(access_token)?.Value);
            //var apptoken = principal.FindFirst(access_token)?.Value;

            //OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(apptoken);

            //ServiceContext serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, oauthValidator);
            //serviceContext.IppConfiguration.MinorVersion.Qbo = "23";


            return View();
        }


        //[AllowAnonymous]
        //public ActionResult QBINTRNL()
        //{
        //    if (!SessionManager.SecurityUser.IsInRole(new Guid[] { Guid.Parse("D6BAAB7B-D94F-41D3-88E9-601FD1C27398") }))
        //    {
        //        return RedirectToAction("QBO");
        //    }
        //    var app = this.CRMDBContext.QBApps.Where(x => x.FranchiseId == SessionManager.CurrentFranchise.FranchiseId).FirstOrDefault();
        //    ViewBag.CurrentFranchiseId = SessionManager.CurrentFranchise.FranchiseId;
        //    ViewBag.OwnerID = app.OwnerID;
        //    ViewBag.SyncUrl = ConfigurationManager.AppSettings["syncUrl"] + "/?Owner=" + app.OwnerID + "&k=" + app.ConsumerKey + "&s=" + app.ConsumerSecret + "&Site=" + AppConfigManager.HostDomain;
        //    return this.View();
        //}

        /// <summary>
        /// The Quickbooks Manual syncrhonication view.
        /// </summary>
        /// <returns></returns>
        //[AllowAnonymous]
        //public ActionResult QBO()
        //{
        //    Franchise currentFranchise = SessionManager.CurrentFranchise;
        //    if (currentFranchise == null)
        //        return RedirectToAction("", "");

        //    QBApp app = CRMDBContext.QBApps.FirstOrDefault(x => x.FranchiseId == currentFranchise.FranchiseId);

        //    if (app == null)
        //        throw new ApplicationException(string.Format("The QBApp entry for franchise id {0} could not be found.", currentFranchise.FranchiseId));

        //    if (ConfigurationManager.AppSettings["syncUrl"] == null)
        //        throw new ApplicationException("The syncUrl setting is missing from the configuration settings.");
        //    ViewBag.Email = SessionManager.QbEmail;
        //    ViewBag.CurrentFranchiseId = currentFranchise.FranchiseId;
        //    ViewBag.OwnerID = app.OwnerID;
        //    ViewBag.SyncUrl = string.Format("{0}/?Owner={1}&Site={2}", AppConfigManager.SyncUrl, app.OwnerID, AppConfigManager.HostDomain);
        //    ViewBag.SyncApi = AppConfigManager.SyncUrl;
        //    return View();
        //}

        /// <summary>
        /// Action Results for Index, uses DotNetOpenAuth for creating OpenId Request with Intuit
        /// and handling response recieved. 
        /// </summary>
        /// <returns></returns>
        /// 
        private static OpenIdRelyingParty openid = new OpenIdRelyingParty();
        [AllowAnonymous]
        public RedirectResult OpenId(string qbToken)
        {
            EventLogger.LogEvent(qbToken, "Setting Controller OpenId call started", LogSeverity.Information);
            SessionManager.QBTokenApi = qbToken;
            var openid_identifier = ConfigurationManager.AppSettings["openid_identifier"].ToString(); ;
            var response = openid.GetResponse();
            if (response == null)
            {
                // Stage 2: user submitting Identifier
                Identifier id;
                if (Identifier.TryParse(openid_identifier, out id))
                {
                    //Exception handling was removed as there was nothing than swallowing exception.
                    // try
                    // {
                    IAuthenticationRequest request = openid.CreateRequest(openid_identifier);
                    FetchRequest fetch = new FetchRequest();
                    fetch.Attributes.Add(new AttributeRequest(WellKnownAttributes.Contact.Email));
                    fetch.Attributes.Add(new AttributeRequest(WellKnownAttributes.Name.FullName));
                    request.AddExtension(fetch);
                    request.RedirectToProvider();

                    EventLogger.LogEvent(openid_identifier, "Setting Controller OpenId response==null", LogSeverity.Information);
                    //  }
                    // catch (ProtocolException ex)
                    // {
                    //     throw ex;
                    // }
                }
            }
            else
            {
                if (response.FriendlyIdentifierForDisplay == null)
                {
                    Response.Redirect(ConfigurationManager.AppSettings["syncUrl"] + "/settings/OpenId");
                }

                // Stage 3: OpenID Provider sending assertion response, storing the response in Session object is only for demonstration purpose
                //Session["FriendlyIdentifier"] = response.FriendlyIdentifierForDisplay;
                FetchResponse fetch = response.GetExtension<FetchResponse>();
                if (fetch != null)
                {
                    SessionManager.OpenIdResponse = true;
                    SessionManager.QbEmail = fetch.GetAttributeValue(WellKnownAttributes.Contact.Email);// emailAddresses.Count > 0 ? emailAddresses[0] : null;
                    //Session["FriendlyName"] = fetch.GetAttributeValue(WellKnownAttributes.Name.FullName);//fullNames.Count > 0 ? fullNames[0] : null;

                    //get the Oauth Access token for the user from OauthAccessTokenStorage.xml
                    //OauthAccessTokenStorageHelper.GetOauthAccessTokenForUser(SessionManager.QbEmail, this);
                    EventLogger.LogEvent(SessionManager.QbEmail, "Setting Controller OpenId response!=null", LogSeverity.Information);
                }
            }

            string query = Request.Url.Query;
            EventLogger.LogEvent(query, "Setting Controller OpenId Request.Url.Query", LogSeverity.Information);
            if (!string.IsNullOrWhiteSpace(query) && query.ToLower().Contains("disconnect=true"))
            {
                Session["accessToken"] = "dummyAccessToken";
                Session["accessTokenSecret"] = "dummyAccessTokenSecret";
                Session["Flag"] = true;
                return Redirect("/CleanupOnDisconnect/Index");
            }

            return Redirect(ConfigurationManager.AppSettings["crmURL"] + "/settings/QBO");
        }


        public ActionResult PurgeCompany()
        {
            var dSQL = string.Format("DELETE t FROM [{0}].[dbo].[SynchLog] t INNER JOIN [QB].[Apps] a on a.[OwnerID]=t.[OwnerID] WHERE a.[FranchiseId] = {1}", ConfigurationManager.AppSettings["qbodb"].ToString(), SessionManager.CurrentFranchise.FranchiseId.ToString());
            var result = ContextFactory.Current.Context.Database.SqlQuery<object>(dSQL).FirstOrDefault();
            return Redirect("/settings/qbo");
        }



        private String _oauthVerifyer, _realmid, _dataSource;

        /// <summary>
        /// Action Results for Index, OAuthToken, OAuthVerifyer and RealmID is recieved as part of Response
        /// and are stored inside Session object for future references
        /// NOTE: Session storage is only used for demonstration purpose only.
        /// </summary>
        /// <returns>View Result.</returns>
        /// 
        ///
        [AllowAnonymous]
        public ActionResult Callback(string qbToken)
        {
            try
            {
                EventLogger.LogEvent(qbToken, "Setting Controller Callback call started", LogSeverity.Information);
                SessionManager.QBTokenApi = qbToken;
                if (Request.QueryString.HasKeys())
                {
                    var profile = new Profile();
                    // This value is used to Get Access Token.
                    _oauthVerifyer = Request.QueryString["oauth_verifier"].ToString();

                    _realmid = Request.QueryString["realmId"].ToString();
                    profile.RealmId = _realmid;

                    //If dataSource is QBO call QuickBooks Online Services, else call QuickBooks Desktop Services
                    _dataSource = Request.QueryString["dataSource"].ToString();
                    switch (_dataSource.ToLower())
                    {
                        case "qbo": profile.DataSource = (int)IntuitServicesType.QBO; break;
                        //case "qbd": profile.DataSource = (int)IntuitServicesType.QBD; break;
                    }

                    profile.OAuthAccessToken = Request.QueryString["oauth_token"];
                    //profile.DataSource = int.Parse(_dataSource);
                    SessionManager.Profile = profile;
                    getAccessToken();

                    EventLogger.LogEvent(_oauthVerifyer + "|" + _realmid + "|" + _dataSource, "Setting Controller Callback QueryString HasKeys", LogSeverity.Information);
                    //Production applications should securely store the Access Token.
                    //In this template, encrypted Oauth access token is persisted in OauthAccessTokenStorage.xml


                    // This value is used to redirect to Default.aspx from Cleanup page when user clicks on ConnectToInuit widget.

                }
                return Redirect("/settings/Close");
            }
            catch (OAuthException e)
            {
                EventLogger.LogEvent(e);
                ViewBag.error = "Can not auth to QBO, please try again!";
                return View();
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                throw;
            }
        }

        /// <summary>
        /// Action Result for Index, This flow will create OAuthConsumer Context using Consumer key and Consuler Secret key
        /// obtained when Application is added at intuit workspace. It creates OAuth Session out of OAuthConsumer and Calls 
        /// Intuit Workpsace endpoint for OAuth.
        /// </summary>
        /// <returns>Redirect Result.</returns>
        [AllowAnonymous]
        public string OAuth(string qbToken)
        {
            EventLogger.LogEvent("", "Setting Controller OAuth call started", LogSeverity.Information);
            oauth_callback_url = Request.Url.GetLeftPart(UriPartial.Authority) + ConfigurationManager.AppSettings["oauth_callback_url"] + "?qbToken=" + qbToken;

            IOAuthSession session = CreateSession();
            SessionManager.QBTokenApi = qbToken;
            IToken requestToken = session.GetRequestToken();
            SessionManager.QBRequestToken = requestToken;
            oauthLink = Constants.OauthEndPoints.AuthorizeUrl + "?oauth_token=" + requestToken.Token + "&oauth_callback=" + UriUtility.UrlEncode(oauth_callback_url);
            EventLogger.LogEvent(oauthLink, "Setting Controller oauthLink", LogSeverity.Information);
            return oauthLink;
        }

        /// <summary>
        /// Gets the OAuth Token
        /// </summary>
        private void getAccessToken()
        {
            EventLogger.LogEvent("", "Setting Controller getAccessToken call started", LogSeverity.Information);
            IOAuthSession clientSession = CreateSession();
            //Exception handling was removed as there was nothing than swallowing exception.
            // try
            // {
            if (SessionManager.QBRequestToken == null)
            {
                throw new OAuthException();
            }

            IToken accessToken = clientSession.ExchangeRequestTokenForAccessToken(SessionManager.QBRequestToken, _oauthVerifyer);



            var profile = SessionManager.Profile;
            profile.OAuthAccessToken = accessToken.Token;
            profile.OAuthAccessTokenSecret = accessToken.TokenSecret;
            SessionManager.Profile = profile;

            EventLogger.LogEvent(JsonConvert.SerializeObject(profile), "Setting Controller profile call started", LogSeverity.Information);
            //Session["Flag"] = true;

            // Remove the Invalid Access token since we got the new access token
            //Session.Remove("InvalidAccessToken");
            //Session["accessTokenSecret"] = accessToken.TokenSecret;

            //   }
            // catch (Exception ex)
            //  {

            //Handle Exception if token is rejected or exchange of Request Token for Access Token failed.
            //   throw ex;
            // }

        }

        private String consumerSecret, consumerKey, oauthLink, RequestToken, TokenSecret, oauth_callback_url;

        /// <summary>
        /// Gets the Access Token
        /// </summary>
        /// <returns>Returns OAuth Session</returns>
        protected IOAuthSession CreateSession()
        {
            EventLogger.LogEvent("", "Setting Controller CreateSession call started", LogSeverity.Information);
            oauthLink = Constants.OauthEndPoints.IdFedOAuthBaseUrl;
            consumerKey = ConfigurationManager.AppSettings["consumerKey"];
            consumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
            OAuthConsumerContext consumerContext = new OAuthConsumerContext
            {
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                SignatureMethod = SignatureMethod.HmacSha1
            };

            EventLogger.LogEvent(oauthLink + "|" + consumerKey + "|" + consumerSecret, "Setting Controller consumerContext call started", LogSeverity.Information);
            return new OAuthSession(consumerContext,
                                            Constants.OauthEndPoints.IdFedOAuthBaseUrl + Constants.OauthEndPoints.UrlRequestToken,
                                            oauthLink,
                                            Constants.OauthEndPoints.IdFedOAuthBaseUrl + Constants.OauthEndPoints.UrlAccessToken);
        }

        [AllowAnonymous]
        public ActionResult Close()
        {
            EventLogger.LogEvent("", "Setting Controller Close call started", LogSeverity.Information);
            return View();
        }
    }
}