﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;

namespace HFC.CRM.QBSync.API.Controllers
{
    /// <summary>
    /// Base controller for all requests, so they are checked for authorization and valid franchise.  Also for handling common tasks for all requests in one area.
    /// </summary>
    [Authorize, Filters.RequireHttps]
    public class BaseController : Controller
    {
        /// <summary>
        /// DB db for this controller
        /// </summary>
        protected CRMContext CRMDBContext = ContextFactory.Current.Context;

        protected ActionResult ResponseResult(string status)
        {
            return status == ContantStrings.Success ? new HttpStatusCodeResult(System.Net.HttpStatusCode.OK) : new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, status);
        }

        protected ActionResult Forbidden(string message)
        {
            return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden, message);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string ua = filterContext.HttpContext.Request.UserAgent;
            if (ua != null && (ua.ToLower().Contains("iphone") || ua.ToLower().Contains("ipad")))
            {
                return; /// потому-что хотелось как лутче
            }
            if (ua == null)
                ua = "";
            //get browser name from Request
            bool isBrowserSupported = filterContext.HttpContext.Request.Browser.Browser != "IE"
                                      && filterContext.HttpContext.Request.Browser.Browser != "InternetExplorer"
                                      //&& filterContext.HttpContext.Request.Browser.Browser != "Firefox"
                                      /// There is one loud guy who is fine with firefox and we can see that chrome doesn't work on his machine. My guess due to extentions and plugins he has...
                                      /// 
                                      ;

            if (ua.ToLower().Contains("windows") && filterContext.HttpContext.Request.Browser.Browser == "Safari" && filterContext.ActionDescriptor.ActionName != "ErrorBrowser")
            {
                //filterContext.Result = this.RedirectToAction("ErrorBrowser", "Account");
            }
            if (!isBrowserSupported && filterContext.ActionDescriptor.ActionName != "ErrorBrowser")
            {
                //filterContext.Result = this.RedirectToAction("ErrorBrowser", "Account");
            }

            var cookie = "";
            if (Request.Headers["Cookie"] != null)
            {
                cookie = Request.Headers["Cookie"].ToString();
            }
            
            //if (!String.IsNullOrEmpty(cookie))
            //{
            //    try
            //    {
            //        using (var client = new WebClient())
            //        {
            //            client.Headers.Add(HttpRequestHeader.Cookie, cookie);
            //            SessionManager.UserName =
            //                client.DownloadString(ConfigurationManager.AppSettings["crmUrl"] + "/api/quickbooks/currentUser");
            //        }
            //    }
            //    catch
            //    {

                   
            //    }
            //}
            

        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            HttpResponseBase response = filterContext.HttpContext.Response;

            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.Cache.AppendCacheExtension("no-store, must-revalidate");

            base.OnActionExecuted(filterContext);
        }
    }
}