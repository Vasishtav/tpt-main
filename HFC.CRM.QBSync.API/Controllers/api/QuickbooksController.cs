﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Cors;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.DTO.Sync;
using HFC.CRM.Managers.QBSync;
using HFC.CRM.Managers.Quickbooks;
using HFC.CRM.QBSync.API.Filters;
using QBResponse = HFC.CRM.Managers.Quickbooks.QBResponse;
using HFC.CRM.QBSync.API.Models;
using System.Diagnostics;
using Newtonsoft.Json;

namespace HFC.CRM.QBSync.API.Controllers.api
{
    [LogExceptionsFilter]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class QuickbooksController : BaseApiController
    {

        // Return QB app info - Raj
        [AllowAnonymous]
        [HttpPost, HttpGet, ActionName("GetQBApp")]
        public IHttpActionResult GetQBApp(int id)
        {
            EventLogger.LogEvent(id.ToString(), "QuickbooksController GetQBApp call started", LogSeverity.Information);
            var QBApp = new QBManager().GetQBApp(id);
            EventLogger.LogEvent(JsonConvert.SerializeObject(QBApp), "QuickbooksController GetQBApp call ended", LogSeverity.Information);
            return ResponseResult(ContantStrings.Success, Json<QBApps>(QBApp));
            //return ResponseResult(ContantStrings.Success, Json<QBApps>(new QBManager().GetQBApp(id)));
        }

        //[AllowAnonymous]
        //[Auth]
        //[HttpPost, HttpGet, ActionName("GetState2")]
        //public IHttpActionResult GetState2(QBSyncStateClient syncState)
        //{
        //    try
        //    {
        //        QBApp app = CRMDBContext.QBApps.FirstOrDefault(x => x.FranchiseId == syncState.Id);
        //        var jsonResult = Json(QBSyncManager.GetSyncState(app.OwnerID));
        //        return ResponseResult(ContantStrings.Success, jsonResult);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResponseResult(EventLogger.LogEvent(ex));
        //    }
        //}

        // Return Current state of franchise QB sync - Raj

        [AllowAnonymous]
        [Auth]
        [HttpPost, HttpGet, ActionName("GetState")]
        public IHttpActionResult GetState(int id)
        {
            try
            {
                //EventLogger.LogEvent(id.ToString(), "QuickbooksController GetState call started", LogSeverity.Information);
                QBApps app = new QBManager().GetQBApp(id);
                if (app != null)
                {
                    var jsonResult = Json(QBSyncManager.GetSyncState(app.OwnerID, null));
                    return ResponseResult(ContantStrings.Success, jsonResult);
                }
                else
                {
                    throw new Exception("Need to Enable QB Sync for this franchise!");
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        // QB Presync check if all the invoice have valid data -Raj
        [AuthAttribute]
        [AllowAnonymous]
        [Auth]
        [HttpPost, HttpGet, ActionName("PreSyncCheck")]
        public IHttpActionResult PreSyncCheck(int id, bool ignoreCache = false)
        {
            try
            {
                EventLogger.LogEvent(id.ToString(), "QuickbooksController PreSyncCheck call started", LogSeverity.Information);
                QBApps app = new QBManager().GetQBApp(id);
                if (app != null)
                {
                    EventLogger.LogEvent(JsonConvert.SerializeObject(app), "PreSyncCheck app check", LogSeverity.Information);
                    var log = QBSyncManager.PreSyncCheck(id, app.OwnerID);
                    EventLogger.LogEvent(JsonConvert.SerializeObject(log), "QuickbooksController PreSyncCheck call ended", LogSeverity.Information);
                    return ResponseResult(ContantStrings.Success, Json(log));
                }
                else
                {
                    return ResponseResult(ContantStrings.SyncNotEnabled);
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }

        // Save QB app info -Raj
        [AllowAnonymous]
        [HttpPost, HttpGet, ActionName("SaveQBApp")]
        public IHttpActionResult SaveQBApp(QBApps app)
        {
            try
            {
                EventLogger.LogEvent(JsonConvert.SerializeObject(app), "QuickbooksController SaveQBApp call started", LogSeverity.Information);
                new QBManager().SaveQBApp(app);
                return ResponseResult(ContantStrings.Success);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        //[AllowAnonymous]
        //[HttpPost, HttpGet, ActionName("QBSyncLockState")]
        //public IHttpActionResult QBSyncLockState(QBCredential login)
        //{
        //    try
        //    {
        //        using (var handler = QBSyncManager.For(login))
        //        {
        //            return ResponseResult(ContantStrings.Success, Json(handler.LockState()));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResponseResult(EventLogger.LogEvent(ex));
        //    }
        //}

        //[AllowAnonymous]
        //[HttpPost, HttpGet, ActionName("QBSyncUnLock")]
        //public IHttpActionResult QBSyncUnLock(QBCredential login)
        //{
        //    try
        //    {
        //        using (var handler = QBSyncManager.For(login))
        //        {
        //            handler.UnLock();
        //            return ResponseResult(ContantStrings.Success);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResponseResult(EventLogger.LogEvent(ex));
        //    }
        //}

        //[AllowAnonymous]
        //[HttpPost, HttpGet, ActionName("QuickBooksOnlineSync")]
        //public IHttpActionResult QuickBooksOnlineSync(QBSyncLogData data)
        //{
        //    QBCredential login = data.Credential;
        //    using (var handler = QBSyncManager.For(login))
        //    {
        //        if (handler.TryLock())
        //        {
        //            try
        //            {
        //                object res = handler.Sync(data.SyncLog);
        //                return res == null
        //                    ? ResponseResult(ContantStrings.Unauthorized)
        //                    : ResponseResult(ContantStrings.Success, Json(res));
        //            }
        //            catch (Exception)
        //            {
        //                //we can not do unlock on finally, because it's not end of sync!
        //                handler.UnLock();
        //                return ResponseResult(ContantStrings.FailedByError);
        //            }
        //        }

        //        return ResponseResult(ContantStrings.Locked);
        //    }
        //}

        // QB Online Sync -Raj
        [AuthAttribute]
        [AllowAnonymous]
        [HttpPost, ActionName("Sync")]
        public IHttpActionResult Sync(int id, [FromBody] Dictionary<string, string> body)
        {
            try
            {
                EventLogger.LogEvent(id.ToString(), "QB Online Sync call started", LogSeverity.Information);
                if (String.IsNullOrEmpty(SessionManager.QbEmail))
                {
                    throw new Exception("Need log in to intuit!");
                }
                var profile = SessionManager.Profile;
                if (String.IsNullOrEmpty(profile.OAuthAccessToken))
                {
                    throw new QBSyncValidationException("Neet to authorize to QBO!");
                }

                QBApps app = new QBManager().GetQBApp(id);
                Franchise fe = new QBManager().GetFranchise(id);

                if (app != null)
                {
                    //var task = new QBSyncTask(app.OwnerID, SessionManager.QbEmail, new QBSyncInfoManager(), profile, fe);
                    //task.Start();
                    EventLogger.LogEvent(id.ToString(), "QB Online Sync call ended", LogSeverity.Information);
                    return ResponseResult(ContantStrings.Success);
                }
                else
                {
                    return ResponseResult(ContantStrings.SyncNotEnabled);
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                StackTrace stackTrace = new StackTrace();
                var log = new { Exception = ex, Stack = stackTrace.ToString() };
                var logStr = JsonConvert.SerializeObject(log);
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        // Disconnect QB online -Raj
        [AllowAnonymous]
        [HttpPost, ActionName("Disconect")]
        public IHttpActionResult Disconect(int id)
        {
            try
            {
                EventLogger.LogEvent(id.ToString(), "QuickbooksController Disconect call started", LogSeverity.Information);
                //SessionManager.FranchiseId = id;
                RestHelper.disconnectRealm(SessionManager.Profile);
                SessionManager.QBRequestToken = null;
                SessionManager.QbEmail = null;
                SessionManager.Profile = null;
                EventLogger.LogEvent(id.ToString(), "QuickbooksController Disconect call ended", LogSeverity.Information);
                return ResponseResult(ContantStrings.Success);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        // QB desktop sync data return
        [AllowAnonymous]
        [HttpPost, HttpGet, ActionName("QBSync")]
        public IHttpActionResult QBSync(QBCredential credentials)
        {
            try
            {
                EventLogger.LogEvent(JsonConvert.SerializeObject(credentials), "QuickbooksController QB desktop sync call started", LogSeverity.Information);
                using (QBSyncManager handler = QBSyncManager.For(credentials))
                {
                    try
                    {
                        object syncResult = handler.Sync();

                        if (syncResult == null)
                            return ResponseResult(ContantStrings.Unauthorized);

                        EventLogger.LogEvent(JsonConvert.SerializeObject(syncResult), "QuickbooksController QB desktop sync call ended", LogSeverity.Information);
                        return ResponseResult(ContantStrings.Success, Json(syncResult));
                    }
                    catch (Exception exception)
                    {
                        EventLogger.LogEvent(exception);
                        return ResponseResult(ContantStrings.FailedByError);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        //[AllowAnonymous]
        //[HttpPost, HttpGet, ActionName("QBResp")]
        //public void QBResp(QBResponse response)
        //{
        //    new QBManager().SaveQBResponse(response);
        //}
    }
}