﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.Data.Context;
using HFC.CRM.QBSync.API.Filters;
using HFC.CRM.QBSync.API.Models;
using Newtonsoft.Json;
using System;

namespace HFC.CRM.QBSync.API.Controllers.api
{
    /// <summary>
    /// Class StatusCodeResult.
    /// </summary>
    public class StatusCodeResult : IHttpActionResult
    {
        /// <summary>
        /// Gets the request.
        /// </summary>
        /// <value>The request.</value>
        public HttpRequestMessage Request { get; private set; }
        /// <summary>
        /// Gets or sets the reason phrase.
        /// </summary>
        /// <value>The reason phrase.</value>
        public string ReasonPhrase { get; set; }
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>The status code.</value>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusCodeResult" /> class.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="code">The code.</param>
        /// <param name="reasonPhrase">The reason phrase.</param>
        public StatusCodeResult(HttpRequestMessage request, HttpStatusCode code, string reasonPhrase)
        {
            Request = request;
            StatusCode = code;
            ReasonPhrase = reasonPhrase;
        }

        /// <summary>
        /// Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>A task that, when completed, contains the <see cref="T:System.Net.Http.HttpResponseMessage" />.</returns>
        public System.Threading.Tasks.Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            var response = Request.CreateResponse(this.StatusCode);
            response.ReasonPhrase = this.ReasonPhrase.Replace("\r\n", " "); //reasonphrase doesnt like new line characters;
            return System.Threading.Tasks.Task.FromResult(response);
        }
    }

    /// <summary>
    /// Class BaseApiController.
    /// </summary>
    [LogExceptionsFilter]
    [System.Web.Http.Authorize, System.Web.Mvc.RequireHttps]
    public class BaseApiController : ApiController
    {
        /// <summary>
        /// DB db for this controller
        /// </summary>
        protected CRMContext CRMDBContext = ContextFactory.Current.Context;

        /// <summary>
        /// Statuses the code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="reasonPhrase">The reason phrase.</param>
        /// <returns>IHttpActionResult.</returns>
        public IHttpActionResult StatusCode(HttpStatusCode code, string reasonPhrase)
        {
            if (string.IsNullOrEmpty(reasonPhrase))
                return StatusCode(code);
            else
                return new StatusCodeResult(this.Request, code, reasonPhrase);
        }

        /// <summary>
        /// Jsons the specified content.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content">The content.</param>
        /// <param name="serializerSettings">The serializer settings.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns>System.Web.Http.Results.JsonResult&lt;T&gt;.</returns>
        protected override System.Web.Http.Results.JsonResult<T> Json<T>(T content, JsonSerializerSettings serializerSettings, Encoding encoding)
        {
            //this will set the default for ignoring reference
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            //serializerSettings.NullValueHandling = NullValueHandling.Ignore;

            return base.Json<T>(content, serializerSettings, encoding);
        }

        

        /// <summary>
        /// Forbiddens the specified reason phrase.
        /// </summary>
        /// <param name="reasonPhrase">The reason phrase.</param>
        /// <returns>IHttpActionResult.</returns>
        protected IHttpActionResult Forbidden(string reasonPhrase = null)
        {
            return StatusCode(HttpStatusCode.Forbidden, reasonPhrase);
        }

        protected override System.Web.Http.Results.OkNegotiatedContentResult<T> Ok<T>(T content)
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            return base.Ok<T>(content);
        }

        //reference: http://api.jquery.com/jquery.ajax/
        /// <summary>
        /// "json": Evaluates the response as JSON and returns a JavaScript object. The JSON data is parsed in a strict manner; any malformed JSON is rejected and a parse error is thrown. As of jQuery 1.9, an empty response is also rejected; the server should return a response of null or {} instead. (See json.org for more information on proper JSON formatting.)
        /// </summary>
        /// <returns>IHttpActionResult.</returns>
        protected new IHttpActionResult Ok()
        {
            var accept = Request.GetHeaderValue("accept") ?? string.Empty;

            if (accept.Contains("application/json") || accept.Contains("text/javascript") || accept.Contains("application/x-javascript"))
                return Json<bool>(true);
            else
                return base.Ok();
        }

        /// <summary>
        /// Responses the result.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="successResult">The success result.</param>
        /// <returns>IHttpActionResult.</returns>
        protected IHttpActionResult ResponseResult(string status, IHttpActionResult successResult = null)
        {
            if (status == IMessageConstants.Success)
                return successResult ?? Ok();
            else if (status == IMessageConstants.Unauthorized)
                return Forbidden(status);
            else
                return StatusCode(HttpStatusCode.BadRequest, status);
        }

        protected IHttpActionResult ResponseResult(Exception ex)
        {
            var message = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                ReasonPhrase = ex.Message.Replace("\r\n", " "),
                Content = new StringContent(ex.StackTrace.ToString())
            };
            throw new HttpResponseException(message);
        }

        //protected HttpResponseMessage SuccessResponse()
        //{
        //    return CreateResponse();
        //}

        //protected HttpResponseMessage SuccessResponseJson()
        //{
        //    return JsonResponse("[]");
        //}

        //protected IHttpActionResult BadResponse(string reasonPhrase = null)
        //{
        //    return BadRequest(reasonPhrase);// CreateResponse(HttpStatusCode.BadRequest, reasonPhrase);
        //}

        //protected HttpResponseMessage JsonResponse(string jsonStr)
        //{
        //    return CreateResponse(stringContent: jsonStr, mediaType: ResponseTypeEnum.Json);
        //}

        //protected HttpResponseMessage JsonResponse(object data)
        //{
        //    return JsonResponse(JsonConvert.SerializeObject(data));
        //}

        //protected HttpResponseMessage CreateResponse(HttpStatusCode statusCode = HttpStatusCode.OK, string reasonPhrase = null, string stringContent = null, ResponseTypeEnum mediaType = ResponseTypeEnum.Text)
        //{
        //    var response = new HttpResponseMessage(statusCode); 
        //    if(!string.IsNullOrEmpty(reasonPhrase))
        //        response.ReasonPhrase = reasonPhrase.Replace("\r\n", " "); //reasonphrase doesnt like new line characters
        //    if (!string.IsNullOrEmpty(stringContent))
        //        response.Content = new StringContent(stringContent, Encoding.UTF8, mediaType.Description());

        //    return response;
        //}
    }
}